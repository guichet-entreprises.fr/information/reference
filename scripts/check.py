﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Le référentiel d'information de Guichet Entreprises est mis à disposition
# selon les termes de la licence Creative Commons Attribution - Pas de
# Modification 4.0 International.

# Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
# suivante :
# http://creativecommons.org/licenses/by-nd/4.0/
# ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
# Mountain View, California, 94041, USA.
# -----------------------------------------------------------------------------

import logging
import sys
import os

from pygereference import folder
from pygereference import ref
from pygereference import refxl
from pygereference import check

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
# -----------------------------------------------------------------------------
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]

# -----------------------------------------------------------------------------
# Set up the logging system
# -----------------------------------------------------------------------------
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

# -----------------------------------------------------------------------------
# Main script call only if this script is runned directly
# -----------------------------------------------------------------------------
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    ref.set_ref_folder(os.path.join(__get_this_folder(), "..", "content/"))

    include_folder = os.path.join(__get_this_folder(),
                                  "..", "content_include/")
    desc_folder = os.path.join(__get_this_folder(),
                               "..", "content", "descriptions")

    print("-----------------------------")
    print("Ref Folder=%s" % ref.get_ref_folder())
    print("-----------------------------")

    # missing_src = list_missing_files(ref_folder, "DS|DQP", lang='src')
    # for filename in missing_src:
    #     print(filename)

    # change the file and folder names
    # check_key_name("DQP032", main_folder, ref_folder)
    # check_all_key_name(main_folder, ref_folder)

    fun_list = []
    # fun_list.append(correct_md)
    # fun_list.append(check_title)
    # fun_list.append(check.var_ge)
    # fun_list.append(check.header_ge)
    # fun_list.append(check_num)
    # fun_list.append(check_toc)
    # fun_list.append(beautify)
    # fun_list.append(delete_num)
    # fun_list.append(correct_title)
    # fun_list.append(check.convert_pdf)
    # fun_list.append(check.code_ape)
    # fun_list.append(check.check_newline)
    # fun_list.append(check.sanitize_var_ge)
    # fun_list.append(check.sanitize_var_ge)
    # fun_list.append(check.check_disclaimer)
    # fun_list.append(check.correct_filename)
    # fun_list.append(check.check_sources)
    # fun_list.append(check.check_in_git)
    fun_list.append(check.check_last_update)

    langs = ['es', 'fr', 'en']
    folder.apply_to_fileref(key_filter="", lang=langs,
                            apply_fun=fun_list,
                            search_folders=[include_folder],
                            save=True,
                            backup=False,
                            refresh=False,
                            )

    # text_init = "Dernière mise à jour : "
    # result = {}

    # from pymdtools import translate

    # for lang in translate.eu_lang_list():
    #     logging.info("Trad : %s", lang)
    #     result[lang] = translate.translate_txt(text_init, dest=lang)

    # print(result)

    # xlfile = os.path.join(__get_this_folder(), "reference_ABO2.xlsx")
    # ref_xl = refxl.read_ref_from(xlfile)
    # fun_list.append(check.check_from_ref(ref_xl))

    # folder.apply_to_fileref(key_filter="", lang="pt",
    #                         apply_fun=fun_list,
    #                         search_folders=[include_folder],
    #                         save=False,
    #                         backup=False,
    #                         refresh=False,
    #                         )

    # folder.apply_to_fileref(key_filter="", lang="es", apply_fun=fun_list,
    #                         search_folders=[include_folder],
    #                         save=False,
    #                         backup=False,
    #                         refresh=False,
    #                         )

    # fun_list_desc = []
    # fun_list_desc.append(check.sanitize_description)
    # folder.apply_to_description(key_filter="", apply_fun=fun_list_desc,
    #                             search_folders=[include_folder, desc_folder],
    #                             save=True,
    #                             backup=False,
    #                             refresh=False,
    #                             )

    # folder.apply_to_fileref(base_path=ref_folder,
    #                         key_filter="DS", lang="fr",
    #                         apply_fun=fun_list,
    #                         save=True,
    #                         backup=False,
    #                         search_folders=[include_folder],
    #                         )
    # folder.apply_to_fileref(base_path=ref_folder,
    #                         key_filter="DQP", lang="fr",
    #                         apply_fun=fun_list,
    #                         save=True,
    #                         backup=False,
    #                         search_folders=[include_folder],
    #                         )

    # folder.apply_to_fileref(base_path=ref_folder,
    #                         key_filter="", lang="fr",
    #                         apply_fun=fun_list, save=True,
    #                         search_folders=[include_folder],
    #                         )
    # folder.apply_to_fileref(base_path=ref_folder,
    #                         key_filter="", lang="en",
    #                         apply_fun=fun_list, save=True,
    #                         search_folders=[include_folder],
    #                         )

    logging.info('Finished')
    # ------------------------------------


# -----------------------------------------------------------------------------
# Call main function if the script is main
# Exec only if this script is runned directly
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    __set_logging_system()
    __main()
