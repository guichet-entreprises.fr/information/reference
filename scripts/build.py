﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Le référentiel d'information de Guichet Entreprises est mis à disposition
# selon les termes de la licence Creative Commons Attribution - Pas de
# Modification 4.0 International.

# Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
# suivante :
# http://creativecommons.org/licenses/by-nd/4.0/
# ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
# Mountain View, California, 94041, USA.
# -----------------------------------------------------------------------------

import logging
import sys
import os

from pygereference import ref
from pygereference import release

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
# -----------------------------------------------------------------------------
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]

# -----------------------------------------------------------------------------
# Set up the logging system
# -----------------------------------------------------------------------------
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

# -----------------------------------------------------------------------------
# Main script call only if this script is runned directly
# -----------------------------------------------------------------------------
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    ref_folder = os.path.join(__get_this_folder(), "..", "content/")
    include_folder = os.path.join(
        __get_this_folder(), "..", "content_include/")
    ref.set_ref_folder(ref_folder)
    ref.get_ref(refresh=True)

    # release.build_automated_list(os.path.join(
    #     ref.get_ref_folder(), "descriptions"))
    # release.build_preformat_description(os.path.join(
    #     ref.get_ref_folder(), "descriptions"), search_folders=[include_folder])

    # the_list = ref.list_description_files()
    # print(the_list)

    root_path = os.path.join(__get_this_folder(), "..", "build")
    release.build(os.path.join(root_path, "reference"), generate_pdf=True)

    # all = ref.get_ref(refresh=True)

    # print(ref.get_ref_langs())

    # list_org = ref.get_ref_organised(lang='fr',
    #                                  keys_order=['category', 'domain'])
    # pprint.pprint(list_org)

    # release.create_zip(
    #     os.path.join(root_path, "reference-122.zip"),
    #     ["reference"],
    #     root_path)

    # uploads = release.gitlab_upload(
    #     os.path.join(root_path, "reference-122.zip"))
    # print(uploads)

    # description = release.release_description(uploads)
    # print(release.gitlab_description_upload(description, "2019.09.30"))

    # print(release.gitlab_get_releases())

    logging.info('Finished')
    # ------------------------------------


# -----------------------------------------------------------------------------
# Call main function if the script is main
# Exec only if this script is runned directly
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    __set_logging_system()
    __main()
