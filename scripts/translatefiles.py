﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Le référentiel d'information de Guichet Entreprises est mis à disposition
# selon les termes de la licence Creative Commons Attribution - Pas de
# Modification 4.0 International.

# Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
# suivante :
# http://creativecommons.org/licenses/by-nd/4.0/
# ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
# Mountain View, California, 94041, USA.
# -----------------------------------------------------------------------------

import logging
import sys
import os
import re

import pymdtools.common as common
import pymdtools.mdfile as mdfile
import pymdtools.filetools as filetools
import pymdtools.translate as trans
from pygereference import ref
from pygereference import check


def translate_ref(lang_dest, lang_start="fr", key_filter=None, refresh=True,
                  overwrite=False):
    refs = ref.get_ref(refresh=refresh)

    keys_start = ref.get_ref(lang=lang_start, refresh=refresh)

    # filter keys
    if key_filter:
        pattern = re.compile(key_filter)
        sub_list = []
        for k in keys_start:
            if pattern.search(k) is not None:
                sub_list.append(k)
        keys_start = sub_list
    else:
        keys_start = list(keys_start.keys())

    keys_start.sort()
    count = 0
    count_max = len(keys_start)

    for key in keys_start:
        count += 1
        logging.info("%03d / %03d : Key=%s", count, count_max, key)
        if lang_start not in refs[key]:
            logging.warning("  /!\\ The lang=%s is missing in key=%s",
                            lang_start, key)
            continue
        if lang_dest in refs[key] and not overwrite:
            logging.info("  --> Already done")
            continue
        data = refs[key][lang_start]

        pattern = os.path.split(data['filename'])[1].replace(
            ".%s.md" % lang_start, ".%s.md")
        # print("filename=%s" % data['filename'])
        # print("pattern=%s" % pattern)

        translate_files(data['filename'], pattern,
                        lang_start=lang_start, lang_dest=lang_dest,
                        overwrite=overwrite)
        # print(refs[key][lang_start])


def translate_files(filename, filename_pattern,
                    lang_start, lang_dest=None,
                    replace_list=None,
                    overwrite=False):
    logging.info("Start tranlation of the file %s", filename)
    filename = common.check_is_file_and_correct_path(filename)
    base_path = os.path.split(filename)[0]

    start_content = mdfile.MarkdownContent(filename)

    if lang_dest is None:
        lang_dest = trans.eu_lang_list()
    if not isinstance(lang_dest, list):
        lang_dest = [lang_dest]

    for lang in lang_dest:
        if lang == lang_start:
            continue
        logging.info("lang=%s", lang)

        dest_filename = os.path.join(base_path, filename_pattern % lang)

        if os.path.isfile(dest_filename):
            if not overwrite:
                logging.info(" --> Already done")
                continue

            dest_md = mdfile.MarkdownContent(filename=dest_filename)
            if dest_md['translation'] != "Auto":
                continue

        logging.info("Translate for %s into file %s", lang, dest_filename)
        result_trans = trans.translate_md(start_content.content,
                                          src=lang_start, dest=lang)

        result = mdfile.MarkdownContent(
            filename=dest_filename, content=result_trans)
        result['lang'] = lang
        result['translation'] = "Auto"
        result['domain'] = trans.translate_txt(start_content['domain'],
                                               src=lang_start, dest=lang)

        result = check.sanitize_var_ge(result, result['key'], lang=lang)
        result = check.check_disclaimer(result)
        result.set_include_file("generated.txt")

        if replace_list is not None:
            for rplc in replace_list:
                result.content = result.content.replace(
                    rplc[0], rplc[1] % lang)

        result.write()


def translate_files_txt(filename, filename_pattern,
                        lang_start, lang_dest=None,
                        replace_list=None,
                        overwrite=False):
    logging.info("Start tranlation of the file %s", filename)
    filename = common.check_is_file_and_correct_path(filename)
    base_path = os.path.split(filename)[0]

    if lang_dest is None:
        lang_dest = trans.eu_lang_list()
    if not isinstance(lang_dest, list):
        lang_dest = [lang_dest]

    start_content = filetools.FileContent(filename)

    for lang in lang_dest:
        if lang == lang_start:
            continue
        logging.info("lang=%s", lang)

        dest_filename = os.path.join(base_path, filename_pattern % lang)

        if os.path.isfile(dest_filename) and not overwrite:
            logging.info(" --> Already done")
            continue
        logging.info("Translate for %s into file %s", lang, dest_filename)
        result_trans = trans.translate_txt(start_content.content,
                                           src=lang_start, dest=lang)

        result = filetools.FileContent(
            filename=dest_filename, content=result_trans)

        if replace_list is not None:
            for rplc in replace_list:
                result.content = result.content.replace(
                    rplc[0], rplc[1] % lang)

        result.write()


# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
# -----------------------------------------------------------------------------
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]

# -----------------------------------------------------------------------------
# Set up the logging system
# -----------------------------------------------------------------------------
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

# -----------------------------------------------------------------------------
# Main script call only if this script is runned directly
# -----------------------------------------------------------------------------
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    include_folder = os.path.join(
        __get_this_folder(), "..", "content_include/")

    ref.set_ref_folder(os.path.join(__get_this_folder(), "..", "content/"))

    translate_ref("de", lang_start="fr", key_filter="", refresh=False,
                  overwrite=False)

    # translate_files(os.path.join(include_folder, "disclaimer-trans.fr.md"),
    #                 "disclaimer-trans.%s.md", lang_start='fr',
    #                 replace_list=[
    #                     ("disclaimer-trans-fr", "disclaimer-trans-%s")],
    #                 overwrite=True)

    # translate_files(os.path.join(include_folder, "disclaimer-eu.fr.md"),
    #                 "disclaimer-eu.%s.md", lang_start='fr',
    #                 replace_list=[
    #                     ("disclaimer-eu-fr", "disclaimer-eu-%s")],
    #                 overwrite=True)

    # translate_files_txt(os.path.join(include_folder, "license-short.fr.txt"),
    #                     "license-short.%s.txt", lang_start='fr',
    #                     replace_list=[],
    #                     overwrite=True)

    logging.info('Finished')
    # ------------------------------------


# -----------------------------------------------------------------------------
# Call main function if the script is main
# Exec only if this script is runned directly
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    __set_logging_system()
    __main()
