@ECHO off
REM # -----------------------------------------------------------------------------
REM # 
REM # Le référentiel d'information de Guichet Entreprises est mis à disposition
REM # selon les termes de la licence Creative Commons Attribution - Pas de
REM # Modification 4.0 International.
REM #
REM # Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
REM # suivante :
REM # http://creativecommons.org/licenses/by-nd/4.0/
REM # ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
REM # Mountain View, California, 94041, USA.
REM # 
REM # -----------------------------------------------------------------------------
CALL %*
GOTO EOF
REM -------------------------------------------------------------------------------
:PRINT_LINE <textVar>
(
    SET "LINE_TO_PRINT=%~1"
    SETLOCAL EnableDelayedExpansion
    @ECHO !LINE_TO_PRINT!
    ENDLOCAL
    exit /b
)
REM -------------------------------------------------------------------------------
:CONFIGURE_DISPLAY
(
    CHCP 65001
    MODE 100,40
    exit /b
)
REM -------------------------------------------------------------------------------
:CLEAR_SCREEN
(
	CLS
    CALL :PRINT_LINE "╔══════════════════════════════════════════════════════════════════════════════════════════════════╗"
    CALL :PRINT_LINE "║                      ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗┌┐┌┌┬┐┬─┐┌─┐┌─┐┬─┐┬┌─┐┌─┐┌─┐                        ║"
    CALL :PRINT_LINE "║                      ║ ╦│ │││  ├─┤├┤  │   ║╣ │││ │ ├┬┘├┤ ├─┘├┬┘│└─┐├┤ └─┐                        ║"
    CALL :PRINT_LINE "║                      ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝┘└┘ ┴ ┴└─└─┘┴  ┴└─┴└─┘└─┘└─┘                        ║"
    CALL :PRINT_LINE "╚══════════════════════════════════════════════════════════════════════════════════════════════════╝"
    IF EXIST "%~dp0/logo.bat" (
        CALL "%~dp0/logo.bat" :PRINT_LOGO
    )
    exit /b
)
REM -------------------------------------------------------------------------------
:LINE_BREAK
(
	CALL :PRINT_LINE "├──────────────────────────────────────────────────────────────────────────────────────────────────┤"
    exit /b
)
REM -------------------------------------------------------------------------------
:UPDATE_PIP
(
    python -V
    pip -V
    python -m pip install --upgrade pip wheel setuptools
    exit /b
)
REM -------------------------------------------------------------------------------
:INSTALL_REQUIREMENTS <requirementsFile>
(
    SET "REQUIRE_FILE=%~1"
    SETLOCAL EnableDelayedExpansion
    CALL :PRINT_LINE "   Install requirements !REQUIRE_FILE!" 
    CALL :UPDATE_PIP
    pip install -r !REQUIRE_FILE!
    exit /b
)
REM -------------------------------------------------------------------------------
:INSTALL_EDITABLE
(
    CALL :PRINT_LINE "   Install editable version" 
    CALL :UPDATE_PIP
    pip install -e .
    exit /b
)
REM -------------------------------------------------------------------------------
:PYTHON_SETUP <setupAction>
(
    SET "SETUP_ACTION=%~1"
    SETLOCAL EnableDelayedExpansion
    CALL :PRINT_LINE "   Launch python setup !SETUP_ACTION!" 
    python setup.py !SETUP_ACTION!
    exit /b
)
REM -------------------------------------------------------------------------------
:PYTHON_LAUNCH <filename>
(
    SET "PY_FILE=%~1"
    SETLOCAL EnableDelayedExpansion
    CALL :PRINT_LINE "   python !PY_FILE!" 
    python !PY_FILE!
    exit /b
)
:EOF