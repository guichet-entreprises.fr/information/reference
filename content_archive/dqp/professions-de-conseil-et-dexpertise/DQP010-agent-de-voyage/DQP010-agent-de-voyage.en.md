﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP010" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Travel agent" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="travel-agent" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/travel-agent.html" -->
<!-- var(last-update)="2020-04-15 17:20:58" -->
<!-- var(url-name)="travel-agent" -->
<!-- var(translation)="Auto" -->


Travel agent
============

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:20:58<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definition of activity
-------------------------

A travel agent is a professional whose business is to advise clients and to organize and sell:

- trips or stays that can be individual or collective;
- all services provided during these trips or stays (such as the issuance of title of transport, booking of accommodations and catering, etc.);
- the tourist information services (such as visits to museums or historical monuments).

*For further*: Article L. 211-1 of the Code of Tourism.

2 °. Professional qualifications
--------------------------------

### at. national requirements

#### national legislation

To operate as a travel agent, the professional must be registered in the directory of travel operators and stays (see below "Registration in the register of travel operators and stays").

**To note**

This registration must be mentioned in the teaching professional in all contract documentation, performed within advertisements and on its website.

*For further*: Articles L. 211-18, L. 141-3 and R. 211-1 and R. 211-2 of the Tourism Code.

#### Training

No special qualifications are required for professional could operate as a travel agent. However, the holder of one of the following qualifications is strongly advised to exercise this activity:

- a patent senior technician (BTS) marked "Tourist";
- BTS in management of business units;
- mention professional license "Tourism Professions."

### Registration in the register of travel operators and accommodation

**Competent authority**

The professional must submit its registration application in writing or electronically to the license commission[Atout France](http://www.atout-france.fr/).

**Vouchers**

Demand for professional must be accompanied by proof of insurance and guarantees (see below "4.. Insurance and guarantees"). The application must also include:

- marital status;
- his profession and home address;
- the address of its registered office and, where appropriate, the activities of its secondary schools;
- when the applicant is a corporation, the request should include:- the name of his company,
  + its legal form,
  + the share capital, the address of its registered office and its secondary schools,
  + Vital and residence of or legal guardians, or statutory.

**Procedure and time**

Upon receipt of the request, the license commission shall provide a receipt to the applicant and then has a period of one month to proceed or not the registration of the professional.

Once the business is registered, the Commission shall issue a registration certificate mentioning the registration number and the registration date.

**To note**

The application for registration must be renewed every three years.

*For further*: Articles R. 211-20 and R. 211-21 of the Tourism Code; under "Registrations" on the[official website Atout France](http://www.atout-france.fr/).

#### Costs associated with the qualification

The cost of training varies according to the intended curriculum. For more information, it is advisable to approach the institutions concerned.

3 °. Insurance and guarantees
-----------------------------

In view of its registration in travel operators and residence, the professional must justify with regard to its customers:

- a financial guarantee used to repay funds received packages of tourism. Can be a tourist package stay compound is:- at least two transport services, accommodation or other tourist services representing a significant proportion of the package,
  + a benefit up to twenty-four hours or including an overnight stay,
  + a benefit sold or offered for sale at an inclusive price;
- have purchased insurance professional liability according[model](https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=b645d64b-4c68-4da2-9a98-76e03c0cd083&groupId=10157)Available online at Atout France.

The amount of the financial guarantee may not be less than EUR 200 000 and must be calculated from the volume of business (including taxes) of all transactions.

The method of calculating the financial guarantee shall be determined by the order of 23 December 2009 on setting conditions of the financial guarantee of travel agents and other operators in the sale of travel and holidays.

*For further*: Article L. 211-18 of the Tourism Code.

**Contract of sale of travel and subsistence**

The professional who sells a travel service or residence must inform its customers prior to the conclusion of the contract, the entire contents related to transport services, price and payment terms, the contract cancellation conditions the conditions for crossing borders. This information can not be changed by the professional when reserves this right.

In addition, the sales contract must be in writing and in duplicate (one for the purchaser) and mention include:

- the identity and address of the organizer, the seller, the guarantor and insurance of the travel service provided;
- the rights and obligations of the seller and his client;
- information about the trip (destination, dates, transport characteristics, visits, etc.);
- the terms of settlement;
- the possible remedies and the cancellation policy.

*For further*: Article L. 211-8 of the Tourism Code.

