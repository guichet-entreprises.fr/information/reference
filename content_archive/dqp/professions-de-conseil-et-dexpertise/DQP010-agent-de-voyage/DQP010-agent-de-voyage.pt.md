﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP010" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profissões de assessoria e especialização" -->
<!-- var(title)="Agente de viagens" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profissoes-de-assessoria-e-especializacao" -->
<!-- var(title-short)="agente-de-viagens" -->
<!-- var(url)="https://www.guichet-qualifications.fr/pt/dqp/profissoes-de-assessoria-e-especializacao/agente-de-viagens.html" -->
<!-- var(last-update)="2020-04-15 17:20:58" -->
<!-- var(url-name)="agente-de-viagens" -->
<!-- var(translation)="Auto" -->


Agente de viagens
=================

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:20:58<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

Um agente de viagens é um profissional cujo negócio é aconselhar seus clientes, bem como organizar e vender:

- Viagens ou estadias que podem ser individuais ou em grupo;
- todos os serviços prestados durante essas viagens ou estadias (como emissão de autorizações de transporte, reserva de hospedagem e jantar, etc.);
- serviços turísticos (como visitas a museus ou monumentos históricos).

*Para ir mais longe* Artigo 211-1 do Código do Turismo.

Dois graus. Qualificações profissionais
---------------------------------------

### a. Requisitos nacionais

#### Legislação nacional

Para exercer a atividade de agente de viagens, o profissional deve estar cadastrado no diretório de operadores de viagens e férias (veja infra "Cadastro no cadastro de operadores de viagens e férias").

**Note que**

Esse registro deve ser mencionado no signo do profissional, em todos os documentos contratuais, nos anúncios realizados e em seu site.

*Para ir mais longe* Artigos L. 211-18, L. 141-3 e R. 211-1 e R. 211-2 do Código do Turismo.

#### Treinamento

Não são necessárias qualificações especiais para o profissional que deseja trabalhar como agente de viagens. No entanto, a realização de um dos seguintes diplomas é fortemente aconselhável para o exercício desta atividade:

- Um Certificado de Técnico Sênior (BTS) marcado como "Turismo";
- um BTS em gestão de unidades de negócios;
- uma licença profissional marcada como "Trades turismo".

### Registro no cadastro de operadores de viagens e residências

**Autoridade competente**

O profissional deve enviar seu pedido de registro por escrito ou eletronicamente para a junta de registro[Trump França](http://www.atout-france.fr/).

**Documentos de suporte**

A solicitação do profissional deve estar acompanhada de documentos de seguro e garantia (ver infra "4o. Seguro e garantias"). Sua aplicação também deve mencionar:

- seu estado civil;
- sua profissão eo endereço de sua casa;
- O endereço de sua sede e, se necessário, as atividades de suas escolas secundárias;
- Quando o requerente for uma pessoa jurídica, sua solicitação deve incluir:- O nome de sua empresa,
  + sua forma legal,
  + o capital social, o endereço de sua sede e escolas secundárias,
  + estado civil e a residência dos representantes legais ou estatutários.

**Procedimento e prazos**

Após o recebimento da solicitação, a junta de inscrição dá um recibo ao requerente e, em seguida, tem um mês para proceder com a inscrição do profissional ou não.

Uma vez que o profissional é registrado, a comissão emite um certificado de registro informando seu número de inscrição e data de registro.

**Note que**

Este pedido de inscrição deve ser renovado a cada três anos.

*Para ir mais longe* Artigos R. 211-20 e R. 211-21 do Código do Turismo; Seção "Inscrições" na seção[Site oficial de Trump França](http://www.atout-france.fr/).

#### Custos associados à qualificação

O custo do treinamento varia de acordo com o curso previsto. Para mais informações, é aconselhável aproximar-se das instituições em questão.

Três graus. Seguro e garantias
------------------------------

Para se cadastrar no cadastro de operadores de viagem e residência, o profissional deve justificar aos seus clientes:

- uma garantia financeira usada para pagar os fundos recebidos para pacotes turísticos. Um pacote turístico pode ser um pacote composto por:- pelo menos dois serviços de transporte, hospedagem ou outros serviços turísticos, que compõem uma parte significativa do pacote,
  + desempenho de mais de 24 horas ou incluindo uma noite '
  + Um serviço vendido ou oferecido à venda a um preço all-inclusive;
- ter tomado seguro de responsabilidade profissional de acordo com o[Modelo](https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=b645d64b-4c68-4da2-9a98-76e03c0cd083&groupId=10157) disponível online no site da Trump France.

O montante da garantia financeira não pode ser inferior a 200.000 euros e deve ser calculado a partir do volume de negócios (todos os impostos incluídos) de todas as transações realizadas.

Os termos de cálculo da garantia financeira são estabelecidos pelo decreto de 23 de dezembro de 2009 relativo às condições para fixação da garantia financeira dos agentes de viagens e demais operadores para a venda de viagens e estadias.

*Para ir mais longe* Artigo 211-18 do Código do Turismo.

**Contrato de vendas de viagens e feriados**

O profissional que vende um serviço de viagem ou estadia deve informar seus clientes, antes da celebração do contrato, sobre o conteúdo integral dos serviços relativos ao transporte, o preço e as condições de pagamento, as condições de cancelamento do contrato condições de contrato e de travessia de fronteira. Essas informações só podem ser alteradas pelo profissional quando ele se reserva esse direito.

Além disso, o contrato de venda deve ser escrito e duplicado (um dos quais é dado ao comprador) e incluir:

- A identidade e endereço do organizador, vendedor, fiador e seguro do serviço de viagem prestado;
- Os direitos e obrigações do vendedor e de seu cliente;
- Informações de viagem (destino, data, características de transporte, visitas, etc.);
- Termos de liquidação
- diferentes remédios e condições de cancelamento.

*Para ir mais longe* Artigo 211-8 do Código do Turismo.

