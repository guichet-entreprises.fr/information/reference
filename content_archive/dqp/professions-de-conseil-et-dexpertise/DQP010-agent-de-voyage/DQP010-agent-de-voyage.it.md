﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP010" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professioni di consulenza e competenza" -->
<!-- var(title)="Agente di viaggio" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professioni-di-consulenza-e-competenza" -->
<!-- var(title-short)="agente-di-viaggio" -->
<!-- var(url)="https://www.guichet-qualifications.fr/it/dqp/professioni-di-consulenza-e-competenza/agente-di-viaggio.html" -->
<!-- var(last-update)="2020-04-15 17:20:58" -->
<!-- var(url-name)="agente-di-viaggio" -->
<!-- var(translation)="Auto" -->


Agente di viaggio
=================

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:20:58<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

Un agente di viaggio è un professionista il cui compito è quello di consigliare i suoi clienti, nonché di organizzare e vendere:

- Viaggi o soggiorni che possono essere individuali o di gruppo;
- tutti i servizi forniti durante questi viaggi o soggiorni (come il rimissione di permessi di trasporto, la prenotazione di alloggi o ristoranti, ecc.);
- servizi turistici (come visite a musei o monumenti storici).

*Per andare oltre* Articolo L. 211-1 del Codice del Turismo.

Due gradi. Qualifiche professionali
-----------------------------------

### a. Requisiti nazionali

#### Legislazione nazionale

Per svolgere l'attività dell'agenzia di viaggi, il professionista deve essere iscritto nell'elenco degli operatori turistici e turistici (vedi infra "Registrazione nel registro degli operatori di viaggio e di vacanza").

**Si noti che**

Questa registrazione deve essere menzionata nel segno del professionista, in tutti i documenti contrattuali, negli annunci effettuati e sul suo sito web.

*Per andare oltre* Articoli L. 211-18, L. 141-3 e R. 211-1 e R. 211-2 del Codice del Turismo.

#### Formazione

Non sono richieste qualifiche speciali per il professionista che desidera lavorare come agente di viaggio. Tuttavia, per l'esercizio di questa attività si consiglia vivamente di prestare uno dei seguenti diplomi:

- Un certificato di tecnico senior (BTS) contrassegnato come "Turismo";
- un BTS nella gestione delle business unit;
- una licenza professionale contrassegnata come "Tourism Trades".

### Iscrizione nel registro degli operatori di viaggio e di residenza

**Autorità competente**

Il professionista deve inviare la sua domanda di registrazione per iscritto o elettronicamente al consiglio di registrazione[Trump Francia](http://www.atout-france.fr/).

**Documenti di supporto**

La richiesta del professionista deve essere accompagnata da documenti di assicurazione e garanzia (vedi infra "4o. Assicurazioni e garanzie"). La sua domanda dovrebbe anche menzionare:

- il suo stato civile;
- la sua professione e l'indirizzo della sua casa;
- l'indirizzo della sua sede centrale e, se necessario, le attività delle sue scuole secondarie;
- Nel caso in cui il richiedente sia una persona giuridica, la sua domanda deve comprendere:- Il nome della sua azienda,
  + la sua forma giuridica,
  + il capitale sociale, l'indirizzo della sua sede e delle scuole secondarie,
  + stato civile e la residenza dei rappresentanti legali o statutari.

**Procedura e scadenze**

Al ricevimento della domanda, il consiglio di registrazione dà una ricevuta al richiedente e quindi ha un mese per procedere con la registrazione del professionista o meno.

Una volta registrato il professionista, la commissione rilascia un certificato di registrazione che indica il suo numero di registrazione e la data di registrazione.

**Si noti che**

Questa domanda di registrazione deve essere rinnovata ogni tre anni.

*Per andare oltre* Articoli R. 211-20 e R. 211-21 del Codice del Turismo; Sezione "Registrazioni" nella sezione[Sito ufficiale di Trump France](http://www.atout-france.fr/).

#### Costi associati alla qualifica

Il costo della formazione varia a seconda del corso previsto. Per maggiori informazioni, si consiglia di avvicinarsi alle istituzioni interessate.

Tre gradi. Assicurazioni e garanzie
-----------------------------------

Per registrarsi presso il registro degli operatori di viaggio e di soggiorno, il professionista deve giustificare ai suoi clienti:

- garanzia finanziaria utilizzata per rimborsare i fondi ricevuti per i pacchetti turistici. Un pacchetto turistico può essere un pacchetto composto da:- almeno due servizi di trasporto, alloggio o altri servizi turistici, che costituiscono una parte significativa del pacchetto,
  + prestazioni superiori a 24 ore o tra cui una notte '
  + Un servizio venduto o offerto in vendita ad un prezzo all inclusive;
- hanno stipulato un'assicurazione professionale di responsabilità civile in base al[Modello](https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=b645d64b-4c68-4da2-9a98-76e03c0cd083&groupId=10157) disponibile online sul sito web di Trump France.

L'importo della garanzia finanziaria non può essere inferiore a 200.000 euro e deve essere calcolato in base al volume delle attività (tutte le imposte incluse) di tutte le transazioni effettuate.

I termini di calcolo della garanzia finanziaria sono fissati dal decreto del 23 dicembre 2009 relativo alle condizioni per la fissazione della garanzia finanziaria delle agenzie di viaggio e di altri operatori per la vendita di viaggi e soggiorni.

*Per andare oltre* Articolo L. 211-18 del Codice del Turismo.

**Contratto di vendita di viaggi e vacanze**

Il professionista che vende un servizio di viaggio o di soggiorno deve informare i suoi clienti, prima della conclusione del contratto, dell'intero contenuto dei servizi relativi al trasporto, del prezzo e delle condizioni di pagamento, delle condizioni di cancellazione del contratto condizioni contrattuali e di attraversamento delle frontiere. Queste informazioni possono essere modificate dal professionista solo quando si riserva questo diritto.

Inoltre, il contratto di vendita deve essere scritto e duplicato (uno dei quali è dato all'acquirente) e includere:

- l'identità e l'indirizzo dell'organizzatore, venditore, garante e assicurazione del servizio di viaggio fornito;
- i diritti e gli obblighi del venditore e del suo cliente;
- Informazioni di viaggio (destinazione, data, caratteristiche di trasporto, visite, ecc.);
- Termini di regolamento
- diversi rimedi e condizioni di cancellazione.

*Per andare oltre* Articolo L. 211-8 del Codice del Turismo.

