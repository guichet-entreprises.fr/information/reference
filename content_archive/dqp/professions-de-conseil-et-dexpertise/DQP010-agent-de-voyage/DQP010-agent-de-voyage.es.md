﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP010" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Agente de viajes" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="agente-de-viajes" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/agente-de-viajes.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agente-de-viajes" -->
<!-- var(translation)="Auto" -->


Agente de viajes
================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

Un agente de viajes es un profesional cuyo negocio es asesorar a sus clientes, así como organizar y vender:

- Viajes o estancias que pueden ser individuales o grupales;
- todos los servicios prestados durante estos viajes o estancias (tales como la expedición de permisos de transporte, la reserva de alojamiento y comedor, etc.);
- servicios turísticos (como visitas a museos o monumentos históricos).

*Para ir más allá* Artículo L. 211-1 del Código de Turismo.

Dos grados. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de agencia de viajes, el profesional debe estar registrado en el directorio de operadores de viajes y vacaciones (véase infra "Registro en el registro de operadores de viajes y vacaciones").

**Tenga en cuenta que**

Este registro debe mencionarse en el signo del profesional, en todos los documentos contractuales, en los anuncios realizados y en su sitio web.

*Para ir más allá* Artículos L. 211-18, L. 141-3 y R. 211-1 y R. 211-2 del Código de Turismo.

#### Entrenamiento

No se requieren cualificaciones especiales para el profesional que desee trabajar como agente de viajes. Sin embargo, se recomienda encarecidamente la celebración de uno de los siguientes diplomas para el ejercicio de esta actividad:

- Un Certificado de Técnico Superior (BTS) marcado como "Turismo";
- un BTS en la gestión de unidades de negocio;
- una licencia profesional marcada como "Comercios Turísticos".

### Inscripción en el registro de operadores de viajes y residencias

**Autoridad competente**

El profesional debe enviar su solicitud de inscripción por escrito o electrónicamente a la junta de registro[Trump Francia](http://www.atout-france.fr/).

**Documentos de apoyo**

La solicitud del profesional debe ir acompañada de documentos de seguro y garantía (ver infra "4o. Seguros y garantías"). Su solicitud también debe mencionar:

- su estado civil;
- su profesión y la dirección de su casa;
- La dirección de su sede central y, en su caso, las actividades de sus escuelas secundarias;
- Cuando el solicitante sea una persona jurídica, su solicitud deberá incluir:- El nombre de su compañía,
  + su forma jurídica,
  + el capital social, la dirección de su sede y las escuelas secundarias,
  + estado civil y la residencia de los representantes legales o legales.

**Procedimiento y plazos**

Una vez recibida la solicitud, la junta de registro entrega un recibo al solicitante y luego tiene un mes para proceder con el registro del profesional o no.

Una vez registrado el profesional, la comisión emite un certificado de registro indicando su número de registro y fecha de registro.

**Tenga en cuenta que**

Esta solicitud de inscripción debe renovarse cada tres años.

*Para ir más allá* Artículos R. 211-20 y R. 211-21 del Código de Turismo; "Registros" en la sección "Registros" en el[Sitio web oficial de Trump Francia](http://www.atout-france.fr/).

#### Costos asociados con la calificación

El costo de la formación varía en función del curso previsto. Para obtener más información, es aconsejable acercarse a las instituciones interesadas.

Tres grados. Seguros y garantías
--------------------------------

Para inscribirse en el registro de operadores de viajes y residencia, el profesional debe justificar a sus clientes:

- una garantía financiera utilizada para devolver los fondos recibidos por los paquetes turísticos. Un paquete turístico puede ser un paquete compuesto por:- al menos dos servicios de transporte, alojamiento u otros servicios turísticos, que forman una parte significativa del paquete,
  + rendimiento de más de veinticuatro horas o incluyendo una noche'
  + Un servicio vendido u ofrecido a la venta a un precio todo incluido;
- han contratado un seguro de responsabilidad civil profesional de acuerdo con el[Modelo](https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=b645d64b-4c68-4da2-9a98-76e03c0cd083&groupId=10157) disponible en línea en el sitio web de Trump Francia.

El importe de la garantía financiera no puede ser inferior a 200.000 euros y debe calcularse a partir del volumen de negocio (todos los impuestos incluidos) de todas las operaciones realizadas.

Las condiciones de cálculo de la garantía financiera se establecen mediante el decreto de 23 de diciembre de 2009 relativo a las condiciones para fijar la garantía financiera de las agencias de viajes y otros operadores para la venta de viajes y estancias.

*Para ir más allá* Artículo L. 211-18 del Código de Turismo.

**Contrato de viaje y venta vacacional**

El profesional que vende un servicio de viaje o estancia deberá informar a sus clientes, antes de la celebración del contrato, sobre todo el contenido de los servicios relacionados con el transporte, el precio y las condiciones de pago, las condiciones de cancelación del contrato condiciones de cruce fronterizo. Esta información sólo puede ser cambiada por el profesional cuando se reserva este derecho.

Además, el contrato de venta debe estar escrito y duplicado (uno de los cuales se da al comprador) e incluir:

- La identidad y dirección del organizador, vendedor, garante y seguro del servicio de viaje prestado;
- Los derechos y obligaciones del vendedor y de su cliente;
- Información de viaje (destino, fecha, características de transporte, visitas, etc.);
- Condiciones de liquidación
- diferentes recursos y condiciones de cancelación.

*Para ir más allá* Artículo L. 211-8 del Código de Turismo.

