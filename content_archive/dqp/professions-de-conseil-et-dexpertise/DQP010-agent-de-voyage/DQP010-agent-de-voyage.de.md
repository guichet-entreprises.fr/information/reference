﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP010" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Beratungs- und Fachberufe" -->
<!-- var(title)="Reisebüro" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="beratungs-und-fachberufe" -->
<!-- var(title-short)="reiseburo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/de/dqp/beratungs-und-fachberufe/reiseburo.html" -->
<!-- var(last-update)="2020-04-15 17:20:59" -->
<!-- var(url-name)="reiseburo" -->
<!-- var(translation)="Auto" -->


Reisebüro
=========

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:20:59<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

Ein Reisebüro ist ein Profi, dessen Geschäft es ist, seine Kunden zu beraten sowie zu organisieren und zu verkaufen:

- Reisen oder Aufenthalte, die einzeln oder gruppengleich sein können;
- alle Dienstleistungen, die während dieser Reisen oder Aufenthalte erbracht werden (z. B. Ausstellung von Transportgenehmigungen, Buchung von Unterkunft und Verpflegung usw.);
- touristischedienstleistungen (z. B. Besuche von Museen oder historischen Denkmälern).

*Um weiter zu gehen* Artikel L. 211-1 des Tourismusgesetzbuches.

Zwei Grad. Berufsqualifikationen
--------------------------------

### a. Nationale Anforderungen

#### Nationale Rechtsvorschriften

Um die Tätigkeit des Reisebüros ausüben zu können, muss der Fachmann im Verzeichnis der Reiseveranstalter und Reiseveranstalter eingetragen sein (siehe infra "Registrierung im Reise- und Urlaubsregister").

**Beachten Sie, dass**

Diese Registrierung ist im Zeichen des Fachmanns, in allen Vertragsunterlagen, in den durchgeführten Anzeigen und auf seiner Website anzugeben.

*Um weiter zu gehen* Artikel L. 211-18, L. 141-3 und R. 211-1 und R. 211-2 des Tourismusgesetzbuches.

#### Ausbildung

Für den Fachmann, der als Reisebüro arbeiten möchte, sind keine besonderen Qualifikationen erforderlich. Für die Ausübung dieser Tätigkeit wird jedoch dringend empfohlen, eines der folgenden Diplome zu besitzen:

- Ein Senior Technician es Certificate (BTS) mit der Aufschrift "Tourismus";
- ein BTS im Geschäftsbereichsmanagement;
- eine Berufslizenz mit der Aufschrift "Tourismus-Trades".

### Eintragung in das Reise- und Aufenthaltsregister

**Zuständige Behörde**

Der Fachmann muss seinen Antrag auf Eintragung schriftlich oder elektronisch an die Registrierungsstelle richten.[Trump Frankreich](http://www.atout-france.fr/).

**Belege**

Der Antrag des Fachmanns muss mit Versicherungs- und Garantieunterlagen versehen sein (siehe infra "4o. Versicherungen und Garantien"). In seinem Antrag sollte auch Folgendes erwähnt werden:

- seinen Familienstand;
- seinen Beruf und die Adresse seiner Heimat;
- Die Anschrift des Hauptsitzes und gegebenenfalls die Tätigkeit seiner Sekundarschulen;
- Ist der Antragsteller eine juristische Person, so muss sein Antrag Folgendes umfassen:- Der Name seines Unternehmens,
  + seine Rechtsform,
  + sozialhauptstadt, die Anschrift des Hauptsitzes und der weiterführenden Schulen,
  + Familienstand und den Wohnsitz der gesetzlichen oder gesetzlichen Vertreter.

**Verfahren und Fristen**

Nach Eingang des Antrags gibt die Registrierungsstelle dem Anmelder eine Quittung und hat dann einen Monat Zeit, um mit der Registrierung des Fachmanns fortzufahren oder nicht.

Sobald der Fachmann registriert ist, stellt die Kommission eine Registrierungsbescheinigung mit seiner Registrierungsnummer und dem Registrierungsdatum aus.

**Beachten Sie, dass**

Dieser Antrag auf Eintragung muss alle drei Jahre erneuert werden.

*Um weiter zu gehen* Artikel R. 211-20 und R. 211-21 des Tourismusgesetzes; Abschnitt "Registrierungen" auf der[Offizielle Website von Trump France](http://www.atout-france.fr/).

#### Mit der Qualifizierung verbundene Kosten

Die Kosten für die Ausbildung variieren je nach dem geplanten Kurs. Weitere Informationen sind ratsam, sich den betreffenden Institutionen anzunähern.

Drei Grad. Versicherungen und Garantien
---------------------------------------

Um sich im Register der Reiseveranstalter und Aufenthaltsunternehmen zu registrieren, muss der Fachmann gegenüber seinen Kunden Folgendes begründen:

- eine finanzielle Garantie, die zur Rückzahlung der für Touristenpakete erhaltenen Mittel verwendet wird. Ein Touristenpaket kann ein Paket sein, das aus:- mindestens zwei Transport-, Unterkunfts- oder sonstige touristische Dienstleistungen, die einen wesentlichen Teil des Pakets ausmachen,
  + Leistung von mehr als 24 Stunden oder einschließlich einer Nacht
  + Eine Dienstleistung, die zum All-inclusive-Preis verkauft oder zum Verkauf angeboten wird;
- Berufshaftpflichtversicherung nach dem[Modell](https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=b645d64b-4c68-4da2-9a98-76e03c0cd083&groupId=10157) online auf der Trump France-Website verfügbar.

Die Höhe der finanziellen Garantie darf nicht unter 200.000 Euro liegen und muss aus dem Geschäftsvolumen (einschließlich aller Steuern) aller getätigten Transaktionen berechnet werden.

Die Berechnungsbedingungen der finanziellen Garantie werden durch das Dekret vom 23. Dezember 2009 in Bezug auf die Bedingungen für die Festsetzung der finanziellen Garantie für Reisebüros und andere Reiseveranstalter für den Verkauf von Reisen und Aufenthalten festgelegt.

*Um weiter zu gehen* Artikel L. 211-18 des Tourismusgesetzbuches.

**Reise- und Urlaubskaufvertrag**

Der Fachmann, der eine Reise- oder Aufenthaltsleistung verkauft, hat seine Kunden vor Vertragsschluss über den gesamten Inhalt der Transportleistungen, den Preis und die Zahlungsbedingungen, die Rücktrittsbedingungen des Vertrages zu informieren. Vertrags- und Grenzübergangsbedingungen. Diese Informationen können vom Fachmann nur geändert werden, wenn er sich dieses Recht vorbehält.

Darüber hinaus muss der Kaufvertrag schriftlich und dupliziert sein (einer davon wird dem Käufer gegeben) und Folgendes umfassen:

- Die Identität und Anschrift des Veranstalters, Verkäufers, Bürgens und der Versicherung der angebotenen Reiseleistung;
- Die Rechte und Pflichten des Verkäufers und seines Kunden;
- Reiseinformationen (Ziel, Datum, Beförderungsmerkmale, Besuche usw.);
- Abrechnungsbedingungen
- unterschiedliche Abhilfemaßnahmen und Stornierungsbedingungen.

*Um weiter zu gehen* Artikel L. 211-8 des Tourismusgesetzbuches.

