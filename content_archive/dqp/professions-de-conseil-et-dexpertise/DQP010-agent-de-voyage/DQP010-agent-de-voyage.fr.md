﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP010" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Agent de voyage" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="agent-de-voyage" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/agent-de-voyage.html" -->
<!-- var(last-update)="2020-04-15 17:20:58" -->
<!-- var(url-name)="agent-de-voyage" -->
<!-- var(translation)="None" -->


# Agent de voyage

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:58<!-- end-var -->

## 1°. Définition de l’activité

Un agent de voyage est un professionnel dont l'activité consiste à conseiller ses clients ainsi qu'à organiser et vendre :

* des voyages ou séjours qui peuvent être individuels ou collectifs ;
* l'ensemble des services fournis au cours de ces voyages ou séjours (telles que la délivrance de titre de transports, la réservation des lieux d'hébergement et de restauration, etc.) ;
* les services d'accueil touristique (telles que les visites de musées ou de monuments historiques).

*Pour aller plus loin* : article L. 211-1 du Code du tourisme.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité d'agent de voyage, le professionnel doit être enregistré au répertoire des opérateurs de voyages et de séjours (cf. infra « Immatriculation au registre des opérateurs de voyages et de séjours »).

**À noter**

Cette immatriculation doit être mentionnée dans l'enseigne du professionnel, dans l'ensemble des documents contractuels, au sein des publicités effectuées et sur son site internet.

*Pour aller plus loin* : articles L. 211-18, L. 141-3 et R. 211-1 et R. 211-2 du Code du tourisme.

#### Formation

Aucune qualification particulière n'est requise pour le professionnel souhaitant exercer l'activité d'agent de voyage. Toutefois, être titulaire de l'un des diplômes suivant est fortement conseillé pour l'exercice de cette activité :

* un brevet de technicien supérieur (BTS) mention « Tourisme » ;
* un BTS en management de unités commerciales ;
* une licence professionnelle mention « Métiers du tourisme ».

### Immatriculation au registre des opérateurs de voyage et de séjour

**Autorité compétente** 

Le professionnel doit adresser sa demande d'immatriculation par écrit ou par voie électronique à la commission d'immatriculation [Atout France](http://www.atout-france.fr/).

**Pièces justificatives** 

La demande du professionnel doit être accompagnée des justificatifs d'assurance et de garanties (cf. infra « 4°. Assurance et garanties »). Sa demande doit également mentionner :

* son état civil ;
* sa profession et l'adresse de son domicile ;
* l'adresse de son siège social et le cas échéant, les activités de ses établissements secondaires ;
* lorsque le demandeur est une personne morale, sa demande doit mentionner :
  * la dénomination sociale de son entreprise,
  * sa forme juridique,
  * le capital social, l'adresse de son siège social et de ses établissements secondaires,
  * l'état civil et le domicile du ou de ses représentants légaux ou statutaires.

**Procédure et délais** 

Dès réception de la demande, la commission d'immatriculation remet un récépissé au demandeur et dispose alors d'un délai d'un mois pour procéder ou non à l'immatriculation du professionnel.

Dès lors que le professionnel est immatriculé, la commission lui délivre un certificat d'immatriculation mentionnant son numéro d'immatriculation et la date d'enregistrement.

**À noter**

Cette demande d'immatriculation doit être renouvelée tous les trois ans.

*Pour aller plus loin* : articles R. 211-20 et R. 211-21 du Code du tourisme ; rubrique « Immatriculations » sur le [site officiel d'Atout France](http://www.atout-france.fr/).

#### Coûts associés à la qualification

Le coût de la formation varie selon le cursus envisagé. Pour plus d'informations, il est conseillé de se rapprocher des établissements concernés.

## 3°. Assurance et garanties

En vue de son immatriculation au registre des opérateurs de voyage et de séjour, le professionnel doit justifier à l'égard de ses clients :

* d'une garantie financière affectée au remboursement des fonds reçus au titre des forfaits de séjours touristiques. Peut constituer un forfait touristique un séjour composé soit :
  * d'au moins deux prestations de transport, d'hébergement ou autre services touristiques représentant une part significative dans le forfait,
  * d'une prestation dépassant vingt-quatre heures ou incluant une nuitée,
  * d'une prestation vendue ou offerte à la vente à un prix tout compris ;
* avoir souscrit une assurance de responsabilité civile professionnelle selon le [modèle](https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=b645d64b-4c68-4da2-9a98-76e03c0cd083&groupId=10157) disponible en ligne sur le site d'Atout France.

Le montant de la garantie financière ne peut être inférieure à 200 000 euros et doit être calculé à partir du volume d'affaires (toutes taxes comprises) de l'ensemble des opérations effectuées.

Les modalités de calcul de la garantie financière sont fixées par l'arrêté du 23 décembre 2009 relatif aux conditions de fixation de la garantie financière des agents de voyages et autres opérateurs de la vente de voyages et de séjours.

*Pour aller plus loin* : article L. 211-18 du Code du tourisme.

**Contrat de vente de voyage et de séjour**

Le professionnel qui vend une prestation de voyage ou de séjour doit informer ses clients, préalablement à la conclusion du contrat, sur l'ensemble du contenu des prestations relatives au transports, au prix et modalités de paiement, aux conditions d'annulation du contrat et aux conditions de franchissement des frontières. Ces informations ne peuvent être modifiées par le professionnel que lorsqu'il se réserve ce droit.

En outre, le contrat de vente doit être écrit et établi en double exemplaire (dont l'un remis à l'acheteur) et mentionner notamment :

* l'identité et l'adresse de l'organisateur, du vendeur, du garant et de l'assurance de la prestation de voyage fournie ;
* les droits et obligations du vendeur et de son client ;
* les informations relatives au voyage (destination, date, caractéristiques des transports, les visites, etc.) ;
* les modalités de règlement ;
* les différents recours possibles et les conditions d'annulation.

*Pour aller plus loin* : article L. 211-8 du Code du tourisme.