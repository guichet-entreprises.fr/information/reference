﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP073" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Auditor (solicitud subsidiaria)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="auditor-solicitud-subsidiaria" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/auditor-solicitud-subsidiaria.html" -->
<!-- var(last-update)="2020-04-15 17:21:02" -->
<!-- var(url-name)="auditor-solicitud-subsidiaria" -->
<!-- var(translation)="Auto" -->


Auditor (solicitud subsidiaria)
===============================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:02<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Auditor (CAC) es un profesional cuya misión principal es verificar que las cuentas anuales de una empresa a la que ha sido nombrado voluntaria u obligatoriamente sean regulares y sinceras, y dar una imagen real de la resultados de las operaciones del año pasado, así como la riqueza financiera y patrimonial de la persona o entidad al final de este ejercicio. Esta auditoría toma la forma de una auditoría que dará o no la certificación de las cuentas presentadas en la junta general de la empresa.

Nombrado por seis años, renovable dentro de la duración máxima de los mandatos en poder de los PIA, el CAC también está obligado a informar sobre los principales acontecimientos en la vida de la empresa, como una ampliación de capital, transformación o pago de dividendos.

Durante su misión, se le pedirá que revele al fiscal cualquier irregularidad que haya conocido.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ejercer su profesión, el CAC debe estar cualificado profesionalmente y incluirse en la lista elaborada por el Consejo Superior de la Oficina de Auditores.

Para saber cómo ejercer la profesión de auditor en Francia, es aconsejable consultar la lista[Auditor](https://www.guichet-qualifications.fr/fr/professions-reglementees/commissaire-aux-comptes/).

#### Costos asociados con la calificación

Se paga la formación de diplomas para llevar a cabo el papel del CAC. Para más información, es aconsejable consultar con las escuelas y universidades que los proporcionan.

### b. Nacionales de la UE o del EEE: aplicación subsidiaria del sistema de reconocimiento de cualificaciones profesionales

El régimen general de reconocimiento de cualificaciones profesionales establece que un nacional de la UE o del EEE legalmente establecido que actúe como auditor podrá estar exento de las condiciones de diplomado, prácticas profesionales y certificado de aptitud para ser auditor. Para ello, debe justificar la celebración de una cualificación expedida en un Estado de la UE o del EEE para el ejercicio de esta actividad y haber superado una prueba de aptitud.

Para obtener más información, consulte la lista[Auditor](https://www.guichet-qualifications.fr/fr/professions-reglementees/commissaire-aux-comptes/).

No obstante, no obstante, dado que, en casos excepcionales y por una razón específica, el nacional no cumple los requisitos para la aplicación del principio anterior, el artículo 10 de la Directiva de 7 de septiembre de 2005 declarará aplicable.

Como tal, el profesional tendrá que justificar:

- Poseer un certificado de competencia o un certificado de formación expedido por ese Estado para llevar a cabo esta actividad;
- han estado trabajando durante al menos un año en los últimos diez años en un Estado de la UE o del EEE que no regula el acceso a esta profesión.

Además, el nacional debe tener las habilidades de idiomas necesarias para ejercer su profesión en Francia.

*Para ir más allá*: Artículo L. 822-1-1, L. 822-1-2, R. 822-1 y R. 822-6 del Código de Comercio; Artículo 10 de la[Directiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32005L0036&from=FR) Parlamento Europeo y del Consejo, de 7 de septiembre de 2005, sobre el reconocimiento de las cualificaciones profesionales.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Reglas éticas

Se debe seguir un conjunto de reglas éticas, que incluyen:

- Secreto profesional con respecto a los datos que procesa;
- la integridad ejerciendo la propia profesión con honestidad y rectitud;
- independencia, con el fin de evitar cualquier conflicto de intereses entre su misión de certificar cuentas y otras misiones que él o miembros de su red pudieran llevar a cabo en nombre de la entidad cuyas cuentas certifique, o entidades que Relacionado;
- incompatibilidad de ejercicios con:- una actividad o acto que infrinja su independencia,
  - su trabajo como empleado,
  - actividad comercial.

*Para ir más allá*: Artículos L. 822-10 y artículos subsiguientes R. 822-20 y posteriores, y Apéndice 8-1 del Código de Comercio.

### b. Sanciones penales

Las sanciones penales pueden recaer en el CAC, que llevaría a cabo su actividad o utilizaría el título de auditor sin estar en la lista del Consejo Superior de la Oficina de auditores o que no respetaría una situación de incompatibilidad. En ambos casos, se enfrenta a una pena de un año de prisión y una multa de 15.000 euros.

*Para ir más allá*: Artículo L. 820-5 del Código de Comercio.

4°. Seguro
-------------------------------

### a. Obligación de someterse a formación profesional continua

Con el fin de mantener y mejorar sus conocimientos en las áreas de contabilidad, finanzas y derecho, los CAC deben someterse a 40 horas de formación profesional continua por año, aceptación de una misión.

Esta obligación se cumplirá una vez que el CAC haya participado en capacitaciones y seminarios cuyos programas se definen respectivamente por el[Compañía Nacional de Auditores](https://www.cncc.fr/) y por el Ministro de Justicia.

*Para ir más allá*: Artículos L. 822-4, R. 822-22 y A. 822-28-1 y el siguiente del Código de Comercio.

### b. Obligación de contrato de seguro de responsabilidad civil profesional

El CAC Liberal tiene la obligación de comprar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

En cualquier caso, el ACC debe presentar una garantía financiera de más de 76.224, 51 euros al año por reclamación.

*Para ir más allá*: Artículos L. 822-17, R. 822-36 y A. 822-28-31 del Código de Comercio.

