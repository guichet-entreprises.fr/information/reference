﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP073" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Consultancy and expertise" -->
<!-- var(title)="Statutory auditor (subsidiary application)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="consultancy-and-expertise" -->
<!-- var(title-short)="statutory-auditor-subsidiary-application" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/consultancy-and-expertise/statutory-auditor-subsidiary-application.html" -->
<!-- var(last-update)="2020-04-15 17:21:02" -->
<!-- var(url-name)="statutory-auditor-subsidiary-application" -->
<!-- var(translation)="Auto" -->


Statutory auditor (subsidiary application)
================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:02<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Auditor (CAC) is a professional whose main mission is to verify that the annual accounts of a company to which he has been appointed voluntarily or obligatory are regular and sincere, and give a true picture of the results of last year's operations as well as the financial and wealth of the individual or entity at the end of this financial year. This audit takes the form of an audit that will or will not lead to the certification of the accounts submitted at the company's general meeting.

Appointed for six years, renewable within the maximum duration for mandates held in PIAs, the ACC is also required to report on major events in the life of the company, such as a capital increase, transformation or dividend payment.

During his mission, he will be required to disclose to the public prosecutor any wrongdoing he may have known about.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To practice its profession, the ACC must be professionally qualified and placed on the list drawn up by the High Council of the Office of the Auditors.

To find out how to practice the profession of auditor in France, it is advisable to refer to the listing[Auditor](https://www.guichet-qualifications.fr/fr/professions-reglementees/commissaire-aux-comptes/).

#### Costs associated with qualification

Training for diplomas to carry out the role of CAC is paid for. For more information, it is advisable to check with the schools and universities providing them.

### b. EU or EEA nationals: subsidiary application of the professional qualifications recognition scheme

The general scheme for recognising professional qualifications provides that a legally established EU or EEA national who acts as an auditor may be exempt from the conditions of diploma, professional internship and certificate of fitness to be an auditor. To do so, it must justify holding a qualification issued in an EU or EEA state for the exercise of this activity and having passed an aptitude test.

For more information, see the listing[Auditor](https://www.guichet-qualifications.fr/fr/professions-reglementees/commissaire-aux-comptes/).

However, by derogation, since, in exceptional cases and for a specific reason, the national does not meet the requirements for the application of the above principle, Article 10 of the directive of 7 September 2005 will find to apply.

As such, the professional will have to justify either:

- Hold a certificate of competency or a training certificate issued by that state to carry out this activity;
- have been working for at least one year in the last ten years in an EU or EEA state that does not regulate access to this profession.

In addition, the national must have the language skills necessary to practice his profession in France.

*To go further* Article L. 822-1-1, L. 822-1-2, R. 822-1 and R. 822-6 of the Code of Commerce; Article 10 of the[Directive 2005/36/EC](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32005L0036&from=FR) Parliament and the Council of 7 September 2005 on the recognition of professional qualifications.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Ethical Rules

A set of ethical rules must be followed, including:

- Professional secrecy with respect to the data it processes;
- integrity by exercising one's profession with honesty and righteousness;
- independence, in order to avoid any conflict of interest between its mission of certifying accounts and other missions that it or members of its network could carry out on behalf of the entity whose accounts it certifies, or entities that are Related;
- exercise incompatibility with:- an activity or act that infringes on its independence,
  - his job as an employee,
  - commercial activity.

*To go further* Articles L. 822-10 and subsequent articles R. 822-20 and beyond, and Appendix 8-1 of the Code of Commerce.

### b. Criminal sanctions

Criminal sanctions may fall to the ACC, which would carry out its activity or use the title of auditor without being on the list of the High Council of the Office of the Auditors or which would not respect a situation of incompatibility. In both cases, he faces a one-year prison sentence and a fine of 15,000 euros.

*To go further* Article L. 820-5 of the Code of Commerce.

4°. Insurance
---------------------------------

### a. Obligation to undergo continuing vocational training

In order to maintain and improve their knowledge in the areas of accounting, finance and law, ACCs must undergo 40 hours of continuous vocational training per year, completed in the 18 months preceding acceptance of a mission.

This obligation will be met once the ACC has participated in training and seminars whose programmes are defined respectively by the[National Company of Auditors](https://www.cncc.fr/) and by the Minister of Justice.

*To go further* Articles L. 822-4, R. 822-22 and A. 822-28-1 and the following of the Code of Commerce.

### b. Obligation to take out professional liability insurance

The Liberal ACC has an obligation to purchase professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

In any event, the ACC must present a financial guarantee of more than 76,224, 51 euros per year per claim.

*To go further* Articles L. 822-17, R. 822-36 and A. 822-28-31 of the Code of Commerce.

