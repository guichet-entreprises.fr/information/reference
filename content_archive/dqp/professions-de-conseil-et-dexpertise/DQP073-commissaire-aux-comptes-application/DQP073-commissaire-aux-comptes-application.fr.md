﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP073" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Professions de conseil et d'expertise" -->
<!-- var(title)="Commissaire aux comptes (application subsidiaire)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="professions-de-conseil-et-d-expertise" -->
<!-- var(title-short)="commissaire-aux-comptes-application" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/professions-de-conseil-et-d-expertise/commissaire-aux-comptes-application-subsidiaire.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="commissaire-aux-comptes-application-subsidiaire" -->
<!-- var(translation)="None" -->


# Commissaire aux comptes (application subsidiaire)

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

Le commissaire aux comptes (CAC) est un professionnel dont la mission principale est de vérifier que les comptes annuels d'une entreprise auprès de laquelle il a été nommé de manière volontaire ou obligatoire sont réguliers et sincères, et donnent une image fidèle du résultat des opérations de l'exercice écoulé ainsi que de la situation financière et du patrimoine de la personne ou de l'entité à la fin de cet exercice. Cette vérification intervient sous la forme d'un audit qui conduira ou non à la certification des comptes remis lors de l'assemblée générale de l'entreprise.

Nommé pour six exercices, renouvelables dans la limite d'une durée maximale s'agissant des mandats détenus dans les EIP, le CAC est également amené à établir des rapports en cas d’événements majeurs dans la vie de l'entreprise tels qu'une augmentation de capital, une transformation ou encore le paiement de dividende.

Au cours de sa mission, il sera tenu de révéler au procureur de la République tous faits délictueux dont il aurait eu connaissance.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer sa profession, le CAC doit être qualifié professionnellement et inscrit sur la liste établie par le Haut Conseil du commissariat aux comptes.

Pour connaître les modalités d'exercice de la profession de commissaire aux comptes en France, il est conseillé de se reporter à la fiche [Commissaire aux comptes](https://www.guichet-qualifications.fr/fr/professions-reglementees/commissaire-aux-comptes/).

#### Coûts associés à la qualification

Les formations menant aux diplômes permettant d'exercer la fonction de CAC sont payantes. Pour plus d'informations, il est conseillé de se renseigner auprès des écoles et des établissements universitaires les dispensant.

### b. Ressortissant de l'UE ou de l'EEE : application subsidiaire du régime de reconnaissance de qualifications professionnelles

Le régime général de reconnaissance de qualifications professionnelles prévoit que le ressortissant de l'UE ou de l'EEE légalement établi et exerçant l'activité de commissaire aux comptes peut être dispensé des conditions de diplôme, de stage professionnel et des épreuves du certificat d'aptitude aux fonctions de commissaire aux comptes. Il doit, pour cela, justifier être titulaire d'une qualification délivrée dans un État de l'UE ou de l'EEE pour l'exercice de cette activité et avoir passé avec succès un examen d'aptitude.

Pour plus d'informations, se reporter à la fiche [Commissaire aux comptes](https://www.guichet-qualifications.fr/fr/professions-reglementees/commissaire-aux-comptes/).

Toutefois, par dérogation, dès lors qu'exceptionnellement et pour un motif spécifique le ressortissant ne remplit pas les conditions requises à l'application du principe précité, l'article 10 de la directive du 7 septembre 2005 trouvera à s'appliquer.

À ce titre, le professionnel devra justifier soit :

* être titulaire d'une attestation de compétence ou d'un titre de formation délivré par cet État et permettant d'exercer cette activité ;
* avoir exercé cette activité pendant au moins un an au cours des dix dernières années au sein d'un État de l'UE ou de l'EEE qui ne réglemente pas l'accès à cette profession.

En outre, le ressortissant doit posséder les connaissances linguistiques nécessaires à l'exercice de sa profession en France.

*Pour aller plus loin* : article L. 822-1-1, L. 822-1-2, R. 822-1 et R. 822-6 du Code de commerce ; article 10 de la [directive 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32005L0036&from=FR) du Parlement et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

### a. Règles déontologiques

Un ensemble de règles déontologiques sont à respecter, parmi lesquelles :

* le secret professionnel à l'égard des données qu'il traite ;
* l'intégrité en exerçant sa profession avec honnêteté et droiture ;
* l'indépendance, afin d'éviter tout conflit d'intérêts entre sa mission de certification des comptes et d'autres missions que lui-même ou des membres de son réseau pourraient exercer pour le compte de l'entité dont il certifie les comptes, ou d'entités qui lui sont liées ;
* l'incompatibilité d'exercice avec :
  * une activité ou un acte portant atteinte à son indépendance,
  * son emploi de salarié,
  * une activité commerciale.

*Pour aller plus loin* : articles L. 822-10 et suivants, articles R. 822-20 et suivants, et annexe 8-1 du Code de commerce.

### b. Sanctions pénales

Des sanctions pénales peuvent incomber au CAC qui exercerait son activité ou ferait usage du titre de commissaire aux comptes sans être inscrit sur la liste du Haut Conseil du commissariat aux comptes ou qui ne respecterait pas une situation d'incompatibilité. Dans ces deux cas, il encourt une peine d'un an de prison et 15 000 euros d'amende.

*Pour aller plus loin* : article L. 820-5 du Code de commerce.

## 4°. Assurance

### a. Obligation de suivre une formation professionnelle continue

Afin d'entretenir et de perfectionner leurs connaissances dans les domaines liés à la comptabilité, la finance et le droit, les CAC doivent suivre une formation professionnelle continue de 40 heures par an, accomplie dans les dix-huit mois qui précèdent l'acceptation d'une mission.

Cette obligation sera satisfaite dès lors que le CAC aura participé à des formations et des séminaires dont les programmes sont définis respectivement par la [Compagnie nationale des commissaires aux comptes](https://www.cncc.fr/) et par le ministre de la Justice.

*Pour aller plus loin* : articles L. 822-4, R. 822-22 et A. 822-28-1 et suivants du Code de commerce.

### b. Obligation de souscrire une assurance de responsabilité civile professionnelle

Le CAC exerçant à titre libéral a l'obligation de souscrire une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

En tout état de cause, le CAC devra impérativement présenter une garantie financière supérieure à 76 224, 51 euros par année et par sinistre.

*Pour aller plus loin* : articles L. 822-17, R. 822-36 et A. 822-28-31 du Code de commerce.