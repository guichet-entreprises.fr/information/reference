﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP250" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sécurité" -->
<!-- var(title)="Sapeur-pompier" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="securite" -->
<!-- var(title-short)="sapeur-pompier" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/securite/sapeur-pompier.html" -->
<!-- var(last-update)="2020-04-15 17:22:22" -->
<!-- var(url-name)="sapeur-pompier" -->
<!-- var(translation)="None" -->


# Sapeur-pompier

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:22<!-- end-var -->

## 1°. Définition de l’activité

Le sapeur-pompier professionnel (SPP) est un agent de la fonction publique territoriale dont l'activité consiste à prévenir et lutter contre les incendies. Le sapeur-pompier est également chargé de prévenir et d'assurer la protection des personnes en matière d'accidents, sinistres, catastrophes ou urgences.

La profession est réglementée par un cadre d'emploi regroupant l'ensemble des fonctionnaires exerçant l'activité de sapeur-pompier professionnel.

**À noter **

La présente fiche ne traitera que de l'activité de sapeur-pompier à titre professionnel et non de celle à titre volontaire (SPV).

*Pour aller plus loin* : article L. 1424-2 du Code général des collectivités territoriales ; décret n° 90-850 du 25 septembre 1990 portant dispositions communes à l'ensemble des sapeurs-pompiers professionnels.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'intéressé souhaitant exercer la profession de SPP doit :

* avoir au moins dix-huit ans ;
* être de nationalité française ;
* être titulaire d'un diplôme de niveau V (équivalent au certificat d'aptitude professionnelle (CAP), brevet d'études professionnelles (BEP), ou diplôme national du brevet (DNB)) ;
* être médicalement apte à accomplir les missions qui lui sont dévolues (selon les critères du profil médical individuel établis par l'arrêté du 6 mai 2000 fixant les conditions d'aptitude médicale des sapeurs-pompiers professionnels et volontaires et les conditions d'exercice de la médecine professionnelle et préventive au sein des services départementaux d'incendie et de secours).

Dès lors que l'intéressé rempli ces conditions, il peut se présenter à l'un des concours de recrutement externe suivants :

* le concours de sapeur-pompier non officier (SPPNO) de Sapeur ou celui de Caporal ;
* le concours de sapeur-pompier officier (SPPO) de lieutenant ou celui de capitaine. Le cas échéant, l'intéressé doit être titulaire d'un diplôme de niveau bac +2 pour candidater au concours de lieutenant et de niveau bac +3 en vue du concours de capitaine.

**À noter **

L'accès à la profession de sapeur-pompier professionnel peut également se faire par concours interne dès lors que le candidat justifie d'une expérience professionnelle d'au moins trois ans au sein de cette activité.

#### Formation

La formation de sapeur-pompier peut être délivrée par l'un des établissements suivants :

* l’École nationale supérieure des officiers de sapeurs-pompiers ;
* l’École d'application de sécurité civile ;
* les établissements publics interdépartementaux d'incendie et de secours ;
* les services départementaux d'incendie et de secours ;
* le Centre national de la fonction publique territoriale ;
* les organismes de formation ayant passé convention avec l'un des établissements ci-dessus ;
* les unités militaires investies à titre permanent de missions de sécurité civile ;
* les organismes de formation de sécurité civile.

**Concours de recrutement de sapeur-pompier professionnel**

**Procédure**

Le candidat qui souhaite se présenter à l'un des concours susvisés doit adresser une demande de dossier d'inscription au Service départemental d'incendie et de secours (SDIS) organisateur du concours soit par voie postale soit, si l'autorité le propose, de manière électronique.

**Pièces justificatives**

Cette demande doit être accompagnée des documents suivants :

* un justificatif de nationalité française du candidat ;
* une attestation sur l'honneur que le candidat à satisfait aux obligations de service national prévues par l'article L. 111-2 du Code du service national, à savoir, le recensement, la journée défense et citoyenneté et l'appel sous les drapeaux ;
* la copie de ses diplômes ou titres de formation ;
* une attestation certifiant sur l'honneur l'exactitude des informations fournies.

**Issue de la procédure**

Le SDIS dresse la liste des candidats admis à concourir et leur indique la date et les modalités des épreuves.

Les différents concours sont composés :

* d'une épreuve d'aptitude physique ;
* de différentes épreuves d'admissibilité ;
* d'une épreuve d'admission.

**À noter**

La nature des épreuves du concours d'admission dépend de la fonction envisagée.

À l'issue du concours le candidat ayant réussi le concours de recrutement est inscrit sur une liste d'aptitude (valable un an sur l'ensemble du territoire national et renouvelable deux fois) en vue d'être recruté au sein d'un Service départemental d'incendie et de secours (SDIS).

*Pour aller plus loin* : articles 5 et suivants du décret n° 2013-593 du 5 juillet 2013 relatif aux conditions générales de recrutement et d'avancement de grade et portant dispositions statutaires diverses applicables aux fonctionnaires de la fonction publique territoriale ; décret n° 2012-520 du 20 avril 2012 portant statut particulier du cadre d'emplois des sapeurs et caporaux de sapeurs-pompiers professionnels.

**Formation initiale des sapeurs-pompiers professionnels**

Le candidat inscrit sur une liste d'aptitude doit rechercher un poste au sein d'un SDIS. Une fois recruté, il doit suivre une formation professionnelle en vue d'acquérir et entretenir ses compétences opérationnelles, administratives et techniques, nécessaires à l'exercice de sa profession.

Cette formation est composée :

* d'une formation d'intégration leur permettant d'exercer leurs fonctions au sein des SDIS ;
* d'une formation de professionnalisation elle-même composée :
  * d'une formation d'adaptation à l'emploi,
  * d'une formation de maintien et de perfectionnement des acquis,
  * d'une formation spécialisée (pour plus d'informations, consulter le [site du ministère de l'Intérieur](https://www.interieur.gouv.fr/Le-ministere/Securite-civile/Documentation-technique/Les-sapeurs-pompiers/La-formation-des-sapeurs-pompiers/Les-referentiels-de-formation-des-sapeurs-pompiers-professionnels-et-des-sapeurs-pompiers-volontaires/Les-formations-specialisees).

Toutefois, le professionnel peut en être dispensé dès lors qu'il est déjà titulaire des connaissances visées par la formation. Le cas échéant, il pourra bénéficier de la procédure de reconnaissance de ses attestations, titres et diplômes (RATD) ou de la procédure de validation des acquis de l'expérience (VAE).

Pour plus d'informations vous pouvez consulter le [site officiel de la VAE](http://www.vae.gouv.fr/).

*Pour aller plus loin* : arrêté du 30 septembre 2013 relatif aux formations des sapeurs-pompiers professionnels.

#### Coûts associés à la qualification

L'accès à la profession par concours est le plus souvent gratuit. Pour plus d'informations il est conseillé de se rapprocher des organismes concernés.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

**Incompatibilités**

Nul ne peut exercer la fonction de sapeur-pompier professionnel s'il :

* ne possède pas la nationalité française ;
* ne jouit pas de ses droits civiques et s'il a fait l'objet d'une condamnation incompatible avec l'exercice de sa profession ;
* ne remplit pas les conditions d'aptitude physique nécessaires à l'exercice de la profession de sapeur-pompier.

**À noter**

Pour exercer en France, le ressortissant UE doit également satisfaire à ces conditions au sein de l’État membre dont il est ressortissant.

**Déontologie**

Le sapeur-pompier professionnel doit au cours de son activité, respecter les principes qui incombent à chaque fonctionnaire notamment :

* la dignité, l'impartialité, l'intégrité, la probité ;
* la laïcité ;
* la neutralité ;
* le secret professionnel ;
* l'obligation de faire cesser ou de prévenir tout conflit d'intérêts dans lequel il se trouve ou pourrait se trouver.

En cas de manquement à ces règles, le SPP s'expose à une sanction disciplinaire et à ce titre, pourra être suspendu de ses fonctions.

*Pour aller plus loin* : articles 5, 25 et suivants de la loi du 13 juillet 1983 précitée.

**Règles professionnelles**

Le sapeur-pompier professionnel est soumis lors de toute intervention, au respect des méthodes et techniques mises en place pour l'exercice de sa profession. Ces règles sont regroupées dans des guides nationaux de référence des techniques professionnelles (GNR).

Ces guides sont disponibles sur le [site officiel du ministère de l'Intérieur](https://www.interieur.gouv.fr/).

## 4°. Garanties

**Garanties accordées au professionnel**

Le sapeur-pompier professionnel, en tant que fonctionnaire des collectivités territoriales bénéficient de garanties durant l'exercice de son activité et notamment :

* le respect de sa liberté d'opinion ;
* l'assurance de ne faire l'objet d'aucune discrimination au cours de sa carrière ni de harcèlement ;
* le droit syndical ;
* le droit à la formation professionnelle.

*Pour aller plus loin* : loi du 13 juillet 1983 précitée.