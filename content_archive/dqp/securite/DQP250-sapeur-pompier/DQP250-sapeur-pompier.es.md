﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP250" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Seguridad" -->
<!-- var(title)="Bombero" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="seguridad" -->
<!-- var(title-short)="bombero" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/seguridad/bombero.html" -->
<!-- var(last-update)="2020-04-15 17:22:22" -->
<!-- var(url-name)="bombero" -->
<!-- var(translation)="Auto" -->


Bombero
=======

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:22<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Bombero Profesional (PPS) es un Oficial de Servicio Público Territorial cuyo trabajo es prevenir y combatir incendios. El bombero también es responsable de prevenir y proteger a las personas de accidentes, desastres, desastres o emergencias.

La profesión está regulada por un marco de empleo que comprende a todos los servidores públicos que trabajan como bomberos profesionales.

**Tenga en cuenta que **

Esta hoja informativa se ocupará únicamente de la actividad de un bombero a nivel profesional y no de la actividad de forma voluntaria (SPV).

*Para ir más allá*: Artículo L. 1424-2 del Código General de Autoridades Locales; Decreto 90-850, de 25 de septiembre de 1990, por el que se establecen disposiciones comunes a todos los bomberos profesionales.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La persona interesada en ejercer como PPS debe:

- Tener al menos dieciocho años de edad
- Ser de nacionalidad francesa;
- poseer un diploma de Nivel V (equivalente al Certificado de Cualificación Profesional (CAP), la Patente de Estudios Profesionales (BEP) o el Diploma Nacional de Patentes (DNB);
- ser médicamente aptos para llevar a cabo las misiones que se le han asignado (de acuerdo con los criterios del perfil médico individual establecido por la orden de 6 de mayo de 2000 por la que se establecen los requisitos de aptitud médica de los bomberos profesionales y voluntarios y para la práctica de la medicina profesional y preventiva en los servicios de bomberos y rescate del departamento).

Si la persona cumple con estas condiciones, puede participar en uno de los siguientes concursos de contratación externos:

- Bombero sin cargo de Sapper (SPPNO) o del cabo;
- El Oficial Bombero (SPPO) Competencia de Teniente o Capitán. Si es así, el interesado debe tener un título de bachillerato para solicitar la competencia de teniente y de nivel 3 para la competición de capitán.

**Tenga en cuenta que **

El acceso a la profesión de bombero profesional también se puede realizar mediante competencia interna siempre y cuando el candidato justifique una experiencia profesional de al menos tres años en esta actividad.

#### Entrenamiento

La capacitación de los bomberos puede ser impartida por una de las siguientes instituciones:

- Escuela Superior Nacional de Oficiales de Bomberos;
- La Escuela de Cumplimiento de la Seguridad Civil;
- Instalaciones públicas interdepartamentales de bomberos y rescate;
- Departamento de Bomberos y Servicios de Rescate;
- El Centro Nacional para el Servicio Público Territorial;
- Organizaciones de formación que han firmado un acuerdo con una de las instituciones mencionadas;
- Unidades militares en misiones permanentes de seguridad civil;
- agencias de capacitación en seguridad civil.

**Concurso profesional de reclutamiento de bomberos**

**Procedimiento**

Los solicitantes que deseen participar en uno de los concursos anteriormente seguidos deben solicitar un expediente de inscripción ante el Servicio Departamental de Bomberos y Salvamento (SDIS) que organice el concurso ya sea por correo o, si la autoridad lo propone, de manera Electrónico.

**Documentos de apoyo**

Esta solicitud debe ir acompañada de los siguientes documentos:

- una prueba de la nacionalidad francesa del candidato;
- certificado de honor de que el candidato cumple con las obligaciones de servicio nacional en virtud de la Sección L. 111-2 del Código Nacional de Servicios, a saber, el censo, el día de la defensa y la ciudadanía y la llamada bajo las banderas;
- Copiando sus diplomas o títulos de formación
- un certificado que certifique la exactitud de la información proporcionada sobre el honor.

**Resultado del procedimiento**

El SDIS enumera los candidatos que son elegibles para competir y les dice la fecha y los procedimientos de las pruebas.

Los distintos concursos están compuestos por:

- Una prueba de acondicionamiento físico
- Diferentes pruebas de elegibilidad
- una prueba de admisión.

**Tenga en cuenta que**

La naturaleza de los exámenes de ingreso depende de la función prevista.

Al final de la competencia, el candidato exitoso se coloca en una lista de aptitudes (válida por un año a nivel nacional y renovable dos veces) con el fin de ser reclutado en un Servicio Departamental de Bomberos y Bomberos (SDIS).

*Para ir más allá*: Artículos 5 y siguientes del Decreto No 2013-593, de 5 de julio de 2013, relativo a las condiciones generales de contratación y ascenso de rango y a la aplicación de diversas disposiciones legales a los funcionarios territoriales; Decreto No 2012-520 de 20 de abril de 2012 con el estatuto especial del marco de empleo de los bomberos profesionales.

**Formación inicial de bomberos profesionales**

El candidato en una lista de aptitudes debe buscar un puesto dentro de un SDIS. Una vez contratado, debe recibir formación profesional para adquirir y mantener sus habilidades operativas, administrativas y técnicas, necesarias para el ejercicio de su profesión.

Esta formación consiste en:

- Formación en integración que les permita desempeñar sus funciones dentro del SDIS;
- formación de profesionalización en sí que consiste en:- Formación para el ajuste del empleo,
  - capacitación para mantener y desarrollar habilidades,
  - capacitación especializada (para más información, véase el[Sitio web del Ministerio del Interior](https://www.interieur.gouv.fr/Le-ministere/Securite-civile/Documentation-technique/Les-sapeurs-pompiers/La-formation-des-sapeurs-pompiers/Les-referentiels-de-formation-des-sapeurs-pompiers-professionnels-et-des-sapeurs-pompiers-volontaires/Les-formations-specialisees).

Sin embargo, el profesional puede quedar exento siempre y cuando ya posea los conocimientos cubiertos por la formación. Si es necesario, podrá beneficiarse del procedimiento de reconocimiento de sus certificados, títulos y diplomas (RATD) o del procedimiento de validación de la experiencia (VAE).

Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr/).

*Para ir más allá* : decreto de 30 de septiembre de 2013 relativo a la formación de bomberos profesionales.

#### Costos asociados con la calificación

El acceso a la profesión por concurso suele ser gratuito. Para obtener más información es aconsejable acercarse a las organizaciones interesadas.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

**Incompatibilidades**

Nadie puede realizar la función de bombero profesional si:

- no tiene nacionalidad francesa;
- no goza de sus derechos civiles y si ha sido objeto de una condena incompatible con el ejercicio de su profesión;
- no cumple con los requisitos de aptitud física necesarios para ejercer la profesión de bombero.

**Tenga en cuenta que**

Para ejercer en Francia, el nacional de la UE también debe cumplir estas condiciones en el Estado miembro del que es nacional.

**Ética**

El bombero profesional debe, durante el transcurso de su actividad, respetar los principios que son responsabilidad de cada servidor público, incluyendo:

- dignidad, imparcialidad, integridad, probidad;
- laicismo;
- Neutralidad
- Secreto profesional
- la obligación de cesar o prevenir cualquier conflicto de intereses en el que pueda estar.

En caso de incumplimiento de estas normas, el SPP puede enfrentar se enfrentar a medidas disciplinarias y, como tales, puede ser suspendido de sus funciones.

*Para ir más allá*: Artículos 5, 25 y siguientes de la mencionada Ley de 13 de julio de 1983.

**Reglas profesionales**

El bombero profesional está sujeto durante cualquier intervención, para respetar los métodos y técnicas puestas en marcha para el ejercicio de su profesión. Estas normas se agrupan en guías nacionales de referencia para técnicas profesionales (GNR).

Estas guías están disponibles en el[web oficial del Ministerio del Interior](https://www.interieur.gouv.fr/).

4°. Garantiza
----------------------------------

**Garantías profesionales**

El bombero profesional, como servidor público de las autoridades locales, goza de garantías durante el ejercicio de su actividad, entre ellas:

- respeto por su libertad de opinión;
- asegurarse de que no se le discrimine durante su carrera o que se le acosen;
- Derecho sindical;
- derecho a la formación profesional.

*Para ir más allá* La ley del 13 de julio de 1983, supra.

