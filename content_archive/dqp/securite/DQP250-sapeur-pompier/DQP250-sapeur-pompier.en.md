﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP250" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Security" -->
<!-- var(title)="Firefighter" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="security" -->
<!-- var(title-short)="firefighter" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/security/firefighter.html" -->
<!-- var(last-update)="2020-04-15 17:22:22" -->
<!-- var(url-name)="firefighter" -->
<!-- var(translation)="Auto" -->


Firefighter
===========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:22<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The Professional Firefighter (PPS) is a Territorial Public Service Officer whose work is to prevent and fight fires. The firefighter is also responsible for preventing and protecting people from accidents, disasters, disasters or emergencies.

The profession is regulated by an employment framework comprising all public servants working as professional firefighters.

**Note that **

This fact sheet will deal only with the activity of a firefighter in a professional capacity and not with the activity on a voluntary basis (SPV).

*For further information*: Article L. 1424-2 of the General Code of Local Authorities; Decree 90-850 of 25 September 1990 making provisions common to all professional firefighters.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The person interested in practising as a PPS must:

- Be at least eighteen years old
- Be a French national;
- hold a Level V diploma (equivalent to the Certificate of Professional Qualification (CAP), Professional Studies Patent (BEP), or National Patent Diploma (DNB);
- be medically fit to carry out the missions assigned to it (according to the criteria of the individual medical profile established by the order of 6 May 2000 setting the medical fitness requirements of professional and volunteer firefighters and conditions for the practice of professional and preventive medicine in the department's fire and rescue services).

If the individual meets these conditions, he or she may enter one of the following external recruitment competitions:

- Sapper's non-commissioned firefighter (SPPNO) or Corporal's;
- The Officer Firefighter (SPPO) Competition of Lieutenant or Captain. If so, the person concerned must have a baccalaureate level degree to apply for the lieutenant and bac level 3 competition for the captain's competition.

**Note that **

Access to the profession of professional firefighter can also be done by internal competition as long as the candidate justifies a professional experience of at least three years in this activity.

#### Training

Firefighter training can be delivered by one of the following institutions:

- National Higher School of Fire Officers;
- The School of Civil Security Enforcement;
- Inter-departmental public fire and rescue facilities;
- Department of Fire and Rescue Services;
- The National Centre for The Territorial Public Service;
- Training organizations that have signed an agreement with one of the above institutions;
- Military units on permanent civilian security missions;
- civil security training agencies.

**Professional Firefighter Recruitment Competition**

**Procedure**

Applicants wishing to enter one of the above-tracked competitions must apply for a registration file with the Departmental Fire and Rescue Service (SDIS) organising the competition either by post or, if the authority proposes, in a manner Electronic.

**Supporting documents**

This request must be accompanied by the following documents:

- a proof of the candidate's French nationality;
- a certificate of honour that the candidate meets the national service obligations under Section L. 111-2 of the National Service Code, namely, the census, the defence and citizenship day and the call under the flags;
- Copying his diplomas or training titles
- a certificate certifying the accuracy of the information provided on the honour.

**Outcome of the procedure**

The SDIS lists the candidates who are eligible to compete and tells them the date and procedures of the tests.

The various competitions are composed:

- A fitness test
- Different eligibility tests
- an admission test.

**Please note**

The nature of the entrance exams depends on the function envisaged.

At the end of the competition, the successful candidate is placed on an aptitude list (valid for one year nationwide and renewable twice) in order to be recruited into a Departmental Fire and Fire Service (SDIS).

*For further information*: Articles 5 and following of Decree No. 2013-593 of 5 July 2013 relating to the general conditions of recruitment and advancement of rank and making various statutory provisions applicable to territorial civil servants; Decree No. 2012-520 of April 20, 2012 with the special status of the employment framework of professional firefighters.

**Initial training of professional firefighters**

The candidate on an aptitude list must look for a position within an SDIS. Once recruited, he must undergo vocational training in order to acquire and maintain his operational, administrative and technical skills, necessary for the practice of his profession.

This training consists of:

- Integration training to enable them to carry out their duties within the SDIS;
- professionalization training itself consisting of:- Employment adjustment training,
  - training to maintain and develop skills,
  - specialized training (for more information, see the[Ministry of the Interior website](https://www.interieur.gouv.fr/Le-ministere/Securite-civile/Documentation-technique/Les-sapeurs-pompiers/La-formation-des-sapeurs-pompiers/Les-referentiels-de-formation-des-sapeurs-pompiers-professionnels-et-des-sapeurs-pompiers-volontaires/Les-formations-specialisees).

However, the professional may be exempted as long as he already holds the knowledge covered by the training. If necessary, he will be able to benefit from the procedure of recognition of his certificates, titles and diplomas (RATD) or the procedure for validation of experience (VAE).

For more information you can see[VAE's official website](http://www.vae.gouv.fr/).

*For further information*: decree of 30 September 2013 relating to the training of professional firefighters.

#### Costs associated with qualification

Access to the profession by competition is usually free. For more information it is advisable to get closer to the organizations concerned.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

**Incompatibilities**

No one may perform the function of a professional firefighter if they:

- does not have French nationality;
- does not enjoy his civil rights and if he has been the subject of a conviction incompatible with the practice of his profession;
- does not meet the physical fitness requirements necessary to practice the profession of firefighter.

**Please note**

To practise in France, the EU national must also meet these conditions within the Member State of which he is a national.

**Ethics**

The professional firefighter must, during the course of his activity, respect the principles that are the responsibility of each public servant, including:

- dignity, impartiality, integrity, probity;
- secularism;
- Neutrality
- Professional secrecy
- the obligation to cease or prevent any conflict of interest in which he or she may be.

In the event of a breach of these rules, the SPP may face disciplinary action and, as such, may be suspended from its duties.

*For further information*: Sections 5, 25 and following of the aforementioned Act of July 13, 1983.

**Professional rules**

The professional firefighter is subject during any intervention, to respect the methods and techniques put in place for the practice of his profession. These rules are grouped into national reference guides for professional techniques (GNR).

These guides are available on the[official website of the Ministry of the Interior](https://www.interieur.gouv.fr/).

4°. Guarantees
----------------------------------

**Professional guarantees**

The professional firefighter, as a public servant of the local authorities, enjoy guarantees during the exercise of his activity, including:

- respect for his freedom of opinion;
- ensuring that you are not discriminated against during your career or harassed;
- Trade union law;
- the right to vocational training.

*For further information*: The law of July 13, 1983, above.

