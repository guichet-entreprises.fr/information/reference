﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP031" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Precision sports instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="precision-sports-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/precision-sports-instructor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="precision-sports-instructor" -->
<!-- var(translation)="Auto" -->


Precision sports instructor
=================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The sports host of skill designs and conducts actions of awakening, discovery, initiation and animation to the activities for which he is trained: bowling, bowling, bowling, sports-balls, archery, sports shooting, billiards with pockets (billiards English pool or snooker), carambola, petanque, etc. He exercises independently and is responsible for the educational program he sets up. It ensures the safety of third parties and the public in its care (from young children to seniors).

*To go further* :

- decrees of 15 April 2009, respectively, creating additional capitalisable units "pocket billiards," "carambole" and "petanque" associated with BPJEPS;
- decree of 12 July 2007 establishing the certificate of specialisation "bowling and bowling sport" associated with the BPJEPS;
- decree of 30 April 2013 establishing the certificate of specialisation "sport-balls" associated with the BPJEPS;
- decree of 22 January 2008 creating the certificate of specialisation "archery" associated with the BPJEPS;
- decree of 4 January 2008 creating the certificate of specialisation "sports shooting" associated with the BPJEPS.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The activity of address sports facilitator is governed by articles L. 212-1 and following of the Code of Sport which require certifications including the Professional Youth, Popular Education and Sport (BPJEPS) certificate.

As a sports teacher, the sports facilitator must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- registered in the national directory of professional certifications.

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*To go further* Articles L. 212-1, R. 212-84 and D. 212-11 and following of the Sports Code.

#### Training

There is no BPJEPS specializing in address sports. To become a sports facilitator in this sector, it is therefore necessary to obtain a generalist BPJEPS and to complete this training by obtaining a certificate of specialisation (CS) and/or a complementary capitalisable unit (UCC).

The BPJEPS, a level IV diploma (bachelor's degree), attests to the possession of professional skills essential to the practice of the profession of facilitator in the field of the specialty concerned.

This diploma can be obtained through continuing vocational training, apprenticeship or validation of experience (VAE). For more information, please visit the official VAE website.

**Conditions for obtaining additional capitalized units (UCC) "pocket billiards," "carambole" and "petanque":**

To obtain one (or more) of the "pocket billiards," "carambole" and/or petanque UCC, the candidate must hold the specialty "physical activities for all" PJEPS. Once this patent has been obtained, the applicant may, if the necessary requirements, register to take the training at one (or more) of the UCC that interests him.

**Good to know**

Obtaining the UCC "pocket billiards" or "carambole" allows the holder to initiate independently the practitioners in his charge in the discipline concerned.

**Conditions for access to training for the "physical activities for all" training:**

- Be of age
- Have a certificate of first aid training (type PSC1);
- Have a medical certificate of non-contradictory to the practice of physical and sports activities, dating back less than three months to the date of entry into training;
- have the certificate of success with the prior requirements related to the candidate's personal sport practice, issued by the Regional Director of Youth, Sports and Recreation or by an expert appointed by him. These pre-requirements are broken down as follows:- an endurance test known as the "Luc Léger test" for which men must reach at least the announcement of tier 7 and women the one on tier 5;
  - a motor skills test detailed in the February 24, 2003 order;
  - pass the selection tests.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Appendix V of the decree of 24 February 2003.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

The training is usually done alternately.

*To go further* : decree of 24 February 2003 establishing the speciality "physical activities for all" of the professional certificate of youth, popular education and sport.

**Conditions for access to training leading to the "pocket billiards" and "carambole" UCC:**

- Hold the Level 1 "Civic Prevention and Relief" Education Unit (PSC1);
- produce the certificate, issued by the national technical director of the billiard table, of success in a technical test organized by the French Pool Federation including a 30-minute technical demonstration event to verify the level of the candidate in the discipline.

Under certain conditions, it is possible to obtain an exemption for the technical test. For more information, it is advisable to refer to The Appendix V of the decrees of 15 April 2009, respectively creating the complementary capitalisable units "pocket billiards" and "carambola" associated with the professional youth certificate, popular education and sport.

Depending on the training centre, other documents may be required (for example, a copy of the participation in the defence preparation appeal day): it is advisable to check with the relevant training organization.

*To go further* orders of 15 April 2009 above.

**Conditions for access to training leading to the "petanque" UCC:**

*To go further* : decree of 29 April 2005 establishing a complementary capitalisable unit "petanque" associated with the professional certificate of youth, popular education and sport.

**Conditions for access to training leading to the CS "bowling and bowling sport," "ball sport," "archery" or "sports shooting":**

To obtain one (or more) of the CS "bowling and bowling sport," "ball sport," "archery" or "sports shooting," the candidate must hold one of the BPJEPS at which the CS is associated.

Thus, the incumbent candidate:

- BJPES specialty "physical activities" is eligible for CS training in bowling and bowling, "ball sport," "archery" or "sports shooting";
- the specialty of "all-public recreation" is eligible for training in the CS "archery" and "sports shooting";
- BPJEPS specialty "cultural animation" is eligible for training at the CS "sports shooting";
- specialty "social animation" PJEPS is eligible for archery training at the CS.

###### Conditions for access to training of the specialty BPJEPS "physical activities for all"

See above.

**Conditions for access to training for the specialty "all-public leisure" PJEPS:**

- Be of age
- Have a certificate of first aid training (type PSC1);
- Have a medical certificate of non-contradictory to the practice of physical and sports activities, dating back less than three months to the date of entry into training;
- have the certificate of success with the prior requirements related to the candidate's personal practice, issued by the Regional Director of Youth, Sports and Recreation or by an expert appointed by him. These pre-requirements are broken down as follows:- an endurance test known as the "Luc Léger test" for which men must reach at least the announcement of tier 7 and women the one on tier 5;
  - a motor skills test detailed in the February 24, 2003 order;
  - pass the selection tests.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Appendix V of the decree of 24 February 2003.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

The training is usually done alternately.

*To go further* : decree of 24 February 2003 establishing the speciality "physical activities for all" of the professional certificate of youth, popular education and sport.

**Conditions for access to the training of the BPJEPS speciality "cultural animation":**

- Be of age
- Fill out a standardized registration form with photo ID
- pass the training entrance exams;
- communicate a file, which is intended to be examined during an interview set up by the training organization, including:- A summary of his volunteer and/or professional experiences in animation;
  - communicate a description of one's personal practice in a technical medium of cultural activity;
  - communicate a letter outlining his motivations for the job of cultural animator.

Under certain conditions, it is possible to obtain exemptions for certain tests (especially for the disclosure of the file). For more information, it is advisable to refer to Appendix III of the decree of 23 July 2004 creating the speciality "cultural animation" of the professional patent of youth, popular education and sport.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

*To go further* decree of 23 July 2004 and its annexes.

**Conditions for access to training of the BPJEPS specialty "social animation":**

- Be of age
- Fill out a standardized registration form with photo ID
- pass the training entrance exams;
- submit a file, during an interview set up by the training organization, to justify an experience of at least six months in the field of animation and including:- a summary of his volunteer and/or professional experiences in animation,
  - a letter outlining his motivations for the job of social facilitator.

Under certain conditions, it is possible to obtain exemptions for certain tests (especially for the disclosure of the file). For more information, it is advisable to refer to Appendix III of the decree of 13 December 2005 creating the speciality "social animation" of the professional patent of youth, popular education and sport.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

*To go further* 13 December 2005.

*Conditions for access to training leading to the Certificate of Specialization "Bowling and Bowling" certification:

- Be a specialty "physical activities for all" PJEPS;
- Fill out the standard registration form with a photo ID
- pass the training entrance exams;
- produce a certificate, issued by the national technical director placed with the French Bowling and Bowling Federation, of success in technical tests aimed at demonstrating the candidate's abilities in bowling and bowling and consist of:- a test of two bowling games,
  - a test of two pieces of pins with a mallet,
  - an interview about the candidate's motivations.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Appendix V of the decree of 12 July 2007.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

*To go further* : decree of 12 July 2007 establishing the certificate of specialisation "bowling and bowling" associated with the professional certificate of youth, popular education and sport and its annexes.

**Conditions for access to training leading to the CS "sport-balls":**

- Be a specialty "physical activities for all" PJEPS;
- Fill out the standard registration form with a photo ID
- pass the training entrance exams;
- Be the holder of the teaching unit**"Civic-level prevention and relief** 1" (PSC1) or its equivalent;
- communicate a medical certificate of non-contradictory to the practice and supervision of the sport-ball activity.

*To go further* : decree of 30 April 2013 creating the certificate of specialisation "sport-balls" associated with the professional certificate of youth, popular education and sport and its annexes.

**Conditions for access to training leading to the CS "archery":**

- be a specialty OF "physical activities for all," "social animation" or "all-public leisure";
- Fill out the standard registration form with a photo ID
- pass the training entrance exams;
- Hold the Level 1 Civic Prevention and Relief Education Unit (PSC1);
- provide a medical certificate of non-contradictory to the sport, less than three months old;
- produce a certificate, issued by the National Technical Director of Archery, justifying an experience as an archery practitioner.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Article 5 of the decree of 22 January 2008.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

*To go further* : decree of 22 January 2008 creating the certificate of specialisation "archery" associated with the professional certificate of youth, popular education and sport and its appendix.

**Conditions for access to training leading to the CS "sports shooting":**

- be a specialty OF "physical activities for all," "cultural animation" or "all-public leisure";
- Hold the Level 1 Civic Prevention and Relief Education Unit (PSC1);
- Fill out the standard registration form with a photo ID
- pass the training entrance exams;
- provide a medical certificate of non-contradictory to the sport, less than three months old;
- hold the shooting logbook.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

*To go further* : decree of 4 January 2008 creating the certificate of specialisation "sports shooting" associated with the professional certificate of youth, popular education and sport and its annexes.

#### Costs associated with qualification

Training to obtain BPJEPS, regardless of specialty, or CS is paid for. Their cost varies depending on the specialty and training centre envisaged. For [more details](http://foromes.calendrier.sports.gouv.fr/#/formation), it is advisable to get closer to the training centre in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as an address sports host in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

4°. Reporting requirement (for the purpose of obtaining the professional sports educator card)
------------------------------------------------------------------------------------------------------------------

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

#### Competent authority

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the [official website](https://eaps.sports.gouv.fr).

#### Time

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

#### Supporting documents

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

###### Cost

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

5°. Qualifications process and formalities
----------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

#### Competent authority

The prior declaration of activity should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP) of the department where the declarer wants to perform his performance.

#### Time

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

#### Supporting documents

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

#### Competent authority

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

#### Time

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

#### Supporting documents for the first declaration of activity

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France,
  - documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

#### Evidence for a renewal of activity declaration

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

**Good to know: compensation measures**

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

