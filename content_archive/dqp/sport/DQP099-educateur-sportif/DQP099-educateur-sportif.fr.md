﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP099" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Educateur sportif" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="educateur-sportif" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sport/educateur-sportif.html" -->
<!-- var(last-update)="2020-04-28 17:36:38" -->
<!-- var(url-name)="educateur-sportif" -->
<!-- var(translation)="None" -->


# Educateur sportif

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:36:38<!-- end-var -->

## 1° . Définition de l’activité

L’éducateur sportif est un professionnel qui enseigne, anime ou encadre une activité physique ou sportive. Il entraîne ses pratiquants. Il exerce son activité à titre d’occupation principale ou secondaire, de façon habituelle, saisonnière ou occasionnelle.

Il est capable de mobiliser les connaissances techniques et pédagogiques propres à l'activité qu’il encadre, en fonction du diplôme dont il est titulaire. Il maîtrise les techniques de sa pratique dans des conditions assurant la sécurité des pratiquants et des tiers ainsi que les comportements à observer et les gestes à exécuter en cas d'incident ou d'accident.

L’éducateur sportif est responsable de la sécurité du public dont il a la charge et des tiers. Il veille au bon état du matériel utilisé. Il s’adapte aux caractéristiques de son public : il donne des indications et corrige les gestes et les postures pour l’aider à progresser.

*Pour aller plus loin* : article L. 212-1 et R. 212-1 du Code du sport.

## 2°. Qualifications professionnelles

### Exigences nationales

#### Législation nationale

L’activité d’éducateur sportif est soumise à l’application de l’article L. 212-1 du Code du sport qui exige l’obtention de certifications spécifiques.

En qualité d’enseignant du sport, l’éducateur sportif doit être titulaire d’un diplôme, d’un titre à finalité professionnelle ou d’un certificat de qualification :

* garantissant sa compétence en matière de sécurité des pratiquants et des tiers dans l’activité physique ou sportive considérée ;
* enregistré au [répertoire nationale des certifications professionnelles](http://www.rncp.cncp.gouv.fr/) (RNCP).

La liste des diplômes, titres à finalité professionnelle et certificats de qualification permettant d’exercer comme éducateur sportif est établie à [l’annexe II-1 (article A. 212-1) du Code du sport](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8231A63F40B1958AFA1DE7E71AB6ED79.tpdila09v_3?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000033170986&dateTexte=20170119&categorieLien=cid#LEGIARTI000033170986). Cette liste précise les conditions et limites d’exercice applicables aux titulaires de chaque diplôme, en fonction des réglementations spécifiques à chaque activité.

Les qualifications qui permettent d’exercer comme éducateur sportif sont :

* le brevet professionnel de la jeunesse, de l’éducation populaire et du sport (BPJEPS) spécialité « éducateur sportif » ;
* le diplôme d’État de la jeunesse, de l’éducation populaire et du sport (DEJEPS) et le diplôme d’État supérieur de la jeunesse, de l’éducation populaire et du sport (DESJEPS) ;
* le certificat de qualification professionnelle (CQP). À titre d’exemples, on peut citer :
  * le CQP « moniteur de vol à plat en soufflerie »,
  * le CQP « animateur d’athlétisme »,
  * le CQP « animateur d’escalade sur structure artificielle ».

Les diplômes étrangers peuvent être admis en équivalence aux diplômes français par le ministre chargé des sports, après avis de la commission de reconnaissance des qualifications placée auprès du ministre.

*Pour aller plus loin* : articles L. 212-1 et suivants, R. 212-1 et suivants, R. 212-84, A. 212-1 et suivants, annexe II-1 (art. A. 212-1) du Code du sport.

**Bon à savoir**

Pour certaines disciplines, le DEJEPS ou le DESJEPS est parfois exigé pour fonctionner en plus grande autonomie comme par exemple pour l'enseignement et la pratique de la plongée. Pour plus d’informations, il est conseillé de se référer à la fiche « Moniteur de plongée subaquatique ».

#### Formation

##### BPJEPS spécialité « éducateur sportif »

Le BPJEPS est un diplôme d'État enregistré au [RNCP](http://www.rncp.cncp.gouv.fr/) et classé au niveau IV de la nomenclature des niveaux de certification. Il atteste l'acquisition d'une qualification dans l'exercice d'une activité professionnelle en responsabilité à finalité éducative ou sociale, dans les domaines d'activités physiques, sportives, socio-éducatives ou culturelles.

Le BPJEPS est délivré au titre de la spécialité « animateur » ou de la spécialité « éducateur sportif » et d’une mention disciplinaire ou pluridisciplinaire. Chaque mention est établie par un arrêté qui :

* définit le référentiel professionnel : il s’agit de la présentation du secteur professionnel, de la description de l'emploi et de la fiche descriptive d'activités ;
* définit le référentiel de certification qui est composé de l'ensemble des unités constitutives du diplôme et fixe pour chaque unité les compétences professionnelles, les objectifs intermédiaires de premier rang ainsi que les épreuves certificatives de ces objectifs ;
* précise les exigences préalables à l'entrée en formation, les exigences préalables à la mise en situation professionnelle et les modalités des épreuves certificatives ;
* le cas échéant, fixe des mesures d'équivalence ou de dispense ;
* le cas échéant, fixe les conditions de la vérification du maintien des acquis professionnels liés à la sécurité des pratiquants et des tiers.

Le BPJEPS est délivré soit par la voie d'unités capitalisables (UC), soit par la validation d'acquis de l'expérience (VAE), ces modalités pouvant être cumulées. Pour plus d’informations, vous pouvez consulter le [site officiel de la VAE](http://www.vae.gouv.fr).

Ce diplôme est préparé soit par la voie de la formation initiale, soit par la voie de l'apprentissage, soit par la voie de la formation continue. Lorsque la formation est suivie dans le cadre de la formation initiale, sa durée minimale est de 900 heures dont 600 heures en centre.

Des certificats complémentaires, qui attestent de compétences professionnelles répondant à un besoin spécifique et respectant les mêmes exigences que celles fixées pour le diplôme, peuvent être associés au BPJEPS spécialité « éducateur sportif ». Ils sont délivrés dans les mêmes conditions que celles figurant dans le diplôme.

*Pour aller plus loin* : articles D. 212-20 à D. 212-31, A. 212-47 à A. 212-47-4 du Code du sport.

###### Compétences professionnelles

Le BPJEPS est obtenu par capitalisation de quatre UC dont deux transversales à tous les BPJEPS spécialité « éducateur sportif » et deux spécifiques à la mention choisie. Les quatre UC sont définies par les compétences professionnelles et les objectifs intermédiaires (OI) suivants :

* UC 1 : encadrer tout public dans tout lieu et toute structure :
  * OI 1-1 : communiquer dans les situations de la vie professionnelle,
  * OI 1-2 : prendre en compte les caractéristiques des publics dans leurs environnements dans une démarche d'éducation à la citoyenneté,
  * OI 1-3 : contribuer au fonctionnement d'une structure ;
* UC 2 : mettre en œuvre un projet d'animation s'inscrivant dans le projet de la structure :
  * OI 2-1 : concevoir un projet d'animation,
  * OI 2-1 : conduire un projet d'animation,
  * OI 2-3 : évaluer un projet d'animation ;
* UC 3 : conduire une séance, un cycle d'animation ou d'apprentissage dans le champ de la mention :
  * OI 3-1 : concevoir la séance, le cycle d'animation ou d'apprentissage,
  * OI 3-2 : conduire la séance, le cycle d'animation ou d'apprentissage,
  * OI 3-3 : évaluer la séance, le cycle d'animation ou d'apprentissage ;
* UC 4 : mobiliser les techniques de la mention ou de l'option pour mettre en œuvre une séance, un cycle d'animation ou d'apprentissage :
  * OI 4-1 : conduire une séance ou un cycle en utilisant les techniques de la mention ou de l'option,
  * OI 4-2 : maîtriser et faire appliquer les règlements de la mention ou de l'option,
  * OI 4-3 : garantir des conditions de pratique en sécurité.

###### Conditions d’accès

Pour les tests d’exigence préalables, le candidat doit déposer, un mois avant la date fixée pour les épreuves auprès de l’organisme de formation chargé de les organiser, un dossier qui comprend :

* une fiche d'inscription avec photographie ;
* la photocopie d'une pièce d'identité en cours de validité ;
* la ou les attestations justifiant de l'allègement de certaines épreuves fixées par l'arrêté portant création de la spécialité, de la mention du diplôme, ou du certificat complémentaire visé ;
* un certificat médical de non contre-indication à la pratique sportive datant de moins d'un an, sauf disposition contraire prévue par l'arrêté portant création de la spécialité, de la mention ou du certificat complémentaire visé ;
* pour les personnes en situation de handicap, l'avis d'un médecin agréé par la Fédération française handisport ou par la Fédération française de sport adapté ou désigné par la commission des droits et de l'autonomie des personnes handicapées sur la nécessité d'aménager le cas échéant les tests d'exigences préalables selon la certification visée.

Pour l'inscription dans une formation, le candidat doit déposer auprès de l'organisme de formation, qui en contrôle la conformité, un mois avant la date fixée pour l'entrée en formation un dossier qui comprend :

* une fiche d'inscription avec photographie ;
* la photocopie d'une pièce d'identité en cours de validité ;
* une copie de l'attestation de recensement ou du certificat individuel de participation à la journée défense et citoyenneté ;
* la ou les attestations justifiant de la satisfaction aux exigences préalables fixées par l'arrêté portant création de la spécialité, de la mention du diplôme, ou du certificat complémentaire visé ;
* les pièces justifiant des dispenses et équivalences de droit ;
* pour une inscription à un certificat complémentaire, la photocopie du diplôme autorisant l'inscription en formation ou une attestation d'inscription à la formation conduisant à ce diplôme ;
* la ou les autres pièces prévues par l'arrêté portant création de la spécialité, de la mention, ou du certificat complémentaire visé ;
* pour les personnes en situation de handicap, l'avis d'un médecin agréé par la Fédération française handisport ou par la Fédération française de sport adapté ou désigné par la commission des droits et de l'autonomie des personnes handicapées sur la nécessité d'aménager le cas échéant la formation ou les épreuves certificatives selon la certification visée.

Pour le dossier de VAE, il est conseillé de se reporter aux [articles A. 212-41 et suivants du Code du sport](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031831233&cidTexte=LEGITEXT000006071318&dateTexte=20170119).

Pour plus d’informations, il est conseillé de se reporter à la fiche correspondant à la discipline pratiquée.

*Pour aller plus loin* : articles A. 212-35 et suivants du Code du sport.

#### Coûts associés à la qualification

La formation menant à l’obtention du BPJEPS est payante. Le coût varie selon les mentions et les organismes de formation.

Pour plus de précisions, il est conseillé de se rapprocher de l’organisme de formation considéré.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Les ressortissants de l’Union européenne (UE)* ou de l’Espace économique européen (EEE)* légalement établis dans un de ces États peuvent exercer la même activité en France de manière temporaire et occasionnelle à la condition d’avoir adressé au préfet de département du lieu d’exécution de la prestation une déclaration préalable d’activité.

Si l’activité ou la formation y conduisant n’est pas réglementée dans l’État membre d’origine ou l’État du lieu d’établissement, le ressortissant doit également justifier y avoir exercé cette activité pendant au moins l’équivalent d’une année à temps complet au cours des dix dernières années précédant la prestation.

Les ressortissants européens désireux d’exercer en France de manière temporaire ou occasionnelle doivent posséder les connaissances linguistiques nécessaires à l’exercice de l’activité en France, en particulier afin de garantir la sécurité des activités physiques et sportives et sa capacité à alerter les secours.

*Pour aller plus loin* : articles L. 212-7 et R. 212-92 à R. 212-94 du Code du sport.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Le ressortissant d’un État de l’UE ou de l’EEE peut s’établir en France pour y exercer de façon permanente, s’il remplit l’une des quatre conditions suivantes :

**Si l’État membre d’origine réglemente l’accès ou l’exercice de l’activité :**
* être titulaire d’une attestation de compétences ou d’un titre de formation délivré par l’autorité compétente d’un État de l’UE ou de l’EEE qui atteste d’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France ;
* être titulaire d’un titre acquis dans un État tiers et admis en équivalence dans un État de l’UE ou de l’EEE et justifier avoir exercé cette activité pendant au moins deux ans à temps complet dans cet État.

**Si l’État membre d’origine ne réglemente ni l’accès, ni l’exercice de l’activité :**
* justifier avoir exercé l’activité dans un État de l’UE ou de l’EEE, à temps complet pendant deux ans au moins au cours des dix dernières années, ou, en cas d’exercice à temps partiel, justifier d’une activité d’une durée équivalente et être titulaire d’une attestation de compétences ou d’un titre de formation délivré par l’autorité compétente d’un de ces États, qui atteste d’une préparation à l’exercice de l’activité, ainsi qu’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France ;
* être titulaire d’un titre attestant d’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France, délivré par l’autorité compétente d’un État de l’UE ou de l’EEE et sanctionnant une formation réglementée visant spécifiquement l’exercice de tout ou partie des activités mentionnées à l’article L. 212-1 du Code du sport et consistant en un cycle d’études complété, le cas échéant, par une formation professionnelle, un stage ou une pratique professionnelle.

Si le ressortissant remplit l’une des quatre conditions précitées, l’obligation de qualification requise pour exercer est réputée satisfaite.

Néanmoins, si les qualifications professionnelles du ressortissant présentent une différence substantielle avec les qualifications requises en France qui ne permettrait pas de garantir la sécurité des pratiquants et des tiers, il peut être amené à se soumettre à une épreuve d’aptitude ou accomplir un stage d’adaptation (cf. infra « Bon à savoir : mesures de compensation »).

Le ressortissant doit posséder la connaissance de la langue française nécessaire à l’exercice de son activité en France, en particulier afin de garantir la sécurité des activités physiques et sportives et sa capacité à alerter les secours.

*Pour aller plus loin* : articles L. 212-7 et R. 212-88 à R. 212-90 du Code du sport.

## 3°. Conditions d’honorabilité

Il est interdit d’exercer en tant qu’éducateur sportif en France pour les personnes ayant fait l’objet d’une condamnation pour tout crime ou pour l’un des délits suivants :

* torture et actes de barbarie ;
* agressions sexuelles ;
* trafic de stupéfiants ;
* mise en danger d’autrui ;
* proxénétisme et infractions qui en résultent ;
* mise en péril des mineurs ;
* usage illicite de substance ou plante classées comme stupéfiants ou provocation à l’usage illicite de stupéfiants ;
* infractions prévues aux articles L. 235-25 à L. 235-28 du Code du sport ;
* à titre de peine complémentaire à une infraction en matière fiscale : condamnation à une interdiction temporaire d’exercer, directement ou par personne interposée, pour son compte ou le compte d’autrui, toute profession industrielle, commerciale ou libérale (article 1750 du Code général des impôts).

De plus, nul ne peut enseigner, animer ou encadrer une activité physique ou sportive auprès de mineurs, s’il a fait l’objet d’une mesure administrative d’interdiction de participer, à quelque titre que ce soit, à la direction et à l’encadrement d’institutions et d’organismes soumis aux dispositions législatives ou réglementaires relatives à la protection des mineurs accueillis en centre de vacances et de loisirs, ainsi que de groupements de jeunesse, ou s’il a fait l’objet d’une mesure administrative de suspension de ces mêmes fonctions.

*Pour aller plus loin* : article L. 212-9 du Code du sport.

## 4°. Démarche et formalités de reconnaissance de qualifications

### a. Obligation de déclaration (en vue de l’obtention de la carte professionnelle d’éducateur sportif)

Toute personne souhaitant exercer l’une des professions régies par l’article L. 212-1 du Code du sport, doit déclarer son activité au préfet du département du lieu où elle compte exercer à titre principal. Cette déclaration déclenche l’obtention d’une carte professionnelle.

La déclaration doit être renouvelée tous les cinq ans.

**Autorité compétente**

La déclaration doit être adressée à la direction départementale de la cohésion sociale (DDCS) ou direction départementale de la cohésion sociale et de la protection des populations (DDCSPP) du département d’exercice ou du principal exercice, ou directement en ligne sur le [site officiel](https://eaps.sports.gouv.fr).

**Délais**

Dans le mois suivant le dépôt du dossier de déclaration, la préfecture envoie un accusé de réception au déclarant. La carte professionnelle, valable cinq ans, est ensuite adressée au déclarant.

**Pièces justificatives**

Les pièces justificatives à fournir sont les suivantes :

* formulaire de déclaration Cerfa 12699*02 ;
* une copie d’une pièce d’identité en cours de validité ;
* une photo d’identité ;
* une déclaration sur l’honneur attestant de l’exactitude des informations figurant dans le formulaire ;
* une copie de chacun des diplômes, titres, certificats invoqués ;
* une copie de l’autorisation d’exercice, ou, le cas échéant, de l’équivalence de diplôme ;
* un certificat médical de non contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées, datant de moins d’un an.

En cas de renouvellement de déclaration, il faut joindre :

* le formulaire Cerfa 12699*02 ;
* une photo d’identité ;
* une copie de l’attestation de révision en cours de validité pour les qualifications soumises à l’obligation de recyclage ;
* un certificat médical de non contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées, datant de moins d’un an.

De plus, dans tous les cas, la préfecture demandera elle-même la communication d’un extrait de moins de trois mois du casier judiciaire du déclarant pour vérifier l’absence d’incapacité ou d’interdiction d’exercer.

**Coût**

Gratuit.

*Pour aller plus loin* : articles L. 212-11, R. 212-85 et A. 212-176 à A. 212-178 du Code du sport.

### b. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE exerçant une activité temporaire et occasionnelle (LPS)

Les ressortissants de l’UE ou de l’EEE légalement établis dans l’un de ces États et souhaitant exercer en France de manière temporaire ou occasionnelle doivent effectuer une déclaration préalable d’activité, avant la première prestation de services.

Si le prestataire souhaite effectuer une nouvelle prestation en France, cette déclaration préalable doit être renouvelée.

Afin d’éviter des dommages graves pour la sécurité des bénéficiaires, le préfet peut, lors de la première prestation, procéder à une vérification préalable des qualifications professionnelles du prestataire.

**Autorité compétente**

La déclaration préalable d’activité doit être adressée à la direction départementale en charge de la cohésion sociale (DDCS) ou à la direction départementale en charge de la cohésion sociale et de la protection des populations (DDCSPP) du département où le déclarant veut effectuer sa prestation.

**Délais**

Dans le mois suivant la réception du dossier de déclaration, le préfet notifie au prestataire :

* soit une demande d’informations complémentaires (dans ce cas, le préfet dispose de deux mois pour donner sa réponse) ;
* soit un récépissé de déclaration de prestation de services s’il ne procède pas à la vérification des qualifications. Dans ce cas, la prestation de services peut débuter ;
* soit qu’il procède à la vérification des qualifications. Dans ce cas, le préfet délivre ensuite au prestataire un récépissé lui permettant de débuter sa prestation ou, si la vérification des qualifications fait apparaître des différences substantielles avec les qualifications professionnelles requises en France, le préfet soumet le prestataire à une épreuve d’aptitude (cf. infra « Bon à savoir : mesures de compensation »).

Dans tous les cas, en l’absence de réponse dans les délais précités, le prestataire est réputé exercer légalement son activité en France.

**Pièces justificatives**

Le dossier de déclaration préalable d’activité doit contenir :

* un exemplaire du formulaire de déclaration dont le modèle est fourni à l’annexe II-12-3 du Code du sport ;
* une photo d’identité ;
* une copie d’une pièce d’identité ;
* une copie de l’attestation de compétences ou du titre de formation ;
* une copie des documents attestant que le déclarant est légalement établi dans l’État membre d’établissement et qu’il n’encourt aucune interdiction, même temporaire, d’exercer (traduits en français par un traducteur agréé) ;
* dans le cas où ni l’activité, ni la formation, conduisant à cette activité n’est réglementée dans l’État membre d’établissement, une copie de toutes pièces justifiant que le déclarant a exercé cette activité dans cet État pendant au moins l’équivalent de deux ans à temps complet au cours des dix dernières années (traduites en français par un traducteur agréé) ;
* l’un des trois documents au choix (à défaut, un entretien sera organisé):
  * une copie d’une attestation de qualification délivrée à l’issue d’une formation en français,
  * une copie d’une attestation de niveau en français délivré par une institution spécialisée,
  * une copie d’un document attestant d’une expérience professionnelle acquise en France.

**Coût**

Gratuit.

**Voies de recours**

Tout recours contentieux doit être exercé dans les deux mois de la notification de la décision auprès du tribunal administratif compétent.

*Pour aller plus loin* : articles R. 212-92 et suivants, A. 212-182-2 et suivants et annexe II-12-3 du Code du sport.

### c. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE en vue d’un exercice permanent (LE)

Tout ressortissant de l’UE ou de l’EEE qualifié pour y exercer tout ou partie des activités mentionnées à l’article L. 212-1 du Code du sport, et souhaitant s’établir en France, doit en faire préalablement la déclaration au préfet du département dans lequel il compte exercer à titre principal.

Cette déclaration permet au déclarant d’obtenir une carte professionnelle et ainsi d’exercer en toute légalité en France dans les mêmes conditions que les ressortissants français.

La déclaration doit être renouvelée tous les cinq ans.

En cas de différence substantielle avec la qualification requise en France, le préfet peut saisir, pour avis, la commission de reconnaissance des qualifications placée auprès du ministre chargé des sports. Il peut aussi décider de soumettre le ressortissant à une épreuve d’aptitude ou à un stage d’adaptation (cf. infra : « Bon à savoir : mesures de compensation »).

**Autorité compétente**

La déclaration doit être adressée à la direction départementale en charge de la cohésion sociale (DDCS) ou à la direction départementale en charge de la cohésion sociale et de la protection des populations (DDCSPP).

**Délais**

La décision du préfet de délivrer la carte professionnelle intervient dans un délai de trois mois à compter de la présentation du dossier complet par le déclarant. Ce délai peut être prorogé d’un mois sur décision motivée. Si le préfet décide de ne pas délivrer la carte professionnelle ou de soumettre le déclarant à une mesure de compensation (épreuve d’aptitude ou stage), sa décision doit être motivée.

**Pièces justificatives pour la première déclaration d’activité**

Le dossier de déclaration d’activité doit contenir :

* un exemplaire du formulaire de déclaration dont le modèle est fourni à l’annexe II-12-2-a du Code du sport ;
* une photo d’identité ;
* une copie d’une pièce d’identité en cours de validité ;
* un certificat médical de non contre-indication à la pratique et à l’encadrement des activités physiques ou sportives, datant de moins d’un an (traduit par un traducteur agréé) ;
* une copie de l’attestation de compétences ou du titre de formation, accompagnée de documents décrivant le cursus de formation (programme, volume horaire, nature et durée des stages effectués), traduit en français par un traducteur agréé ;
* le cas échéant, une copie de toutes pièces justifiant de l’expérience professionnelle (traduites en français par un traducteur agréé) ;
* si le titre de formation a été obtenu dans un État tiers, les copies des pièces attestant que ce titre a été admis en équivalence dans un État de l’UE ou de l’EEE qui réglemente l’activité ;
* l’un des trois documents au choix (à défaut, un entretien sera organisé) :
  * une copie d’une attestation de qualification délivrée à l’issue d’une formation en français,
  * une copie d’une attestation de niveau en français délivré par une institution spécialisée,
  * une copie d’un document attestant d’une expérience professionnelle acquise en France,
  * les documents attestant que le déclarant n’a pas fait l’objet, dans l’État membre d’origine, d’une des condamnations ou mesures mentionnées aux articles L. 212-9 et L. 212-13 du Code du sport (traduits en français par un traducteur agréé).

**Pièces justificatives pour un renouvellement de déclaration d’activité**

Le dossier de renouvellement de déclaration d’activité doit contenir :

* un exemplaire du formulaire de renouvellement de déclaration dont le modèle est fourni à l’annexe II-12-2-b du Code du sport ;
* une photo d’identité ;
* un certificat médical de non contre-indication à la pratique et à l’encadrement des activités physiques ou sportives, datant de moins d’un an.

**Coût**

Gratuit.

**Voies de recours**

Tout recours contentieux doit être exercé dans les deux mois de la notification de la décision auprès du tribunal administratif compétent.

*Pour aller plus loin* : articles R. 212-88 à R. 212-91, A. 212-182, et les annexes II-12-2-a et II-12-b du Code du sport.

### d. Mesures de compensation

S’il existe une différence substantielle entre la qualification du requérant et celle requise en France pour exercer la même activité, le préfet saisit la commission de reconnaissance des qualifications, placée auprès du ministre chargé des sports. Cette commission, après examen et instruction du dossier, émet, dans le mois de sa saisine, un avis qu’elle adresse au préfet. 

Dans son avis, la commission peut :

* estimer qu’il existe effectivement une différence substantielle entre la qualification du déclarant et celle requise en France. Dans ce cas, la commission propose de soumettre le déclarant à une épreuve d’aptitude ou un stage d’adaptation. Elle définit la nature et les modalités précises de ces mesures de compensation (nature des épreuves, modalités de leur organisation et de leur évaluation, période d’organisation, contenu et durée du stage, types de structures pouvant accueillir le stagiaire, etc.) ;
* estimer qu’il n’y a pas de différence substantielle entre la qualification du déclarant et celle requise en France.

À réception de l’avis de la commission, le préfet notifie sa décision motivée au déclarant (il n’est pas obligé de suivre l’avis de la commission) :

* s’il exige qu’une mesure de compensation soit effectuée, le déclarant dispose d’un délai d’un mois pour choisir entre la ou les épreuve(s) d’aptitude et le stage d’adaptation. Le préfet délivre ensuite une carte professionnelle au déclarant qui a satisfait aux mesures de compensation. En revanche, si le stage ou l’épreuve d’aptitude ne sont pas satisfaisants, le préfet notifie sa décision motivée de refus de délivrance de la carte professionnelle à l’intéressé ;
* s’il n’exige pas de mesure de compensation, le préfet délivre une carte professionnelle à l’intéressé.

*Pour aller plus loin* : articles R. 212-84 et D. 212-84-1 du Code du sport.

### e. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

**Conditions**

L’intéressé ne peut recourir à SOLVIT que s’il établit :

* que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
* qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

**Procédure**

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

* si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
* s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

**Pièces justificatives**

Pour saisir SOLVIT, le ressortissant doit communiquer :

* ses coordonnées complètes ;
* la description détaillée de son problème ;
* l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

**Délai**

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

**Coût**

Gratuit.

**Informations supplémentaires**

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris, ([site officiel](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).