﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP099" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Sportpädagoge" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="sportpadagoge" -->
<!-- var(url)="https://www.guichet-qualifications.fr/de/dqp/sport/sportpadagoge.html" -->
<!-- var(last-update)="2020-04-28 17:36:38" -->
<!-- var(url-name)="sportpadagoge" -->
<!-- var(translation)="Auto" -->


Sportpädagoge
=============

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-28 17:36:38<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
(1) Definieren der Aktivität
----------------------------

Der Sportpädagoge ist ein Profi, der eine körperliche oder sportliche Aktivität unterrichtet, erleichtert oder überwacht. Er bildet seine Praktizierenden aus. Er arbeitet als Haupt- oder Nebenberuf, in der Regel saisonal oder gelegentlich.

Er ist in der Lage, das technische und pädagogische Wissen zu mobilisieren, das für die von ihm betreute Tätigkeit spezifisch ist, je nach Diplom, das er besitzt. Er beherrscht die Techniken seiner Praxis unter Bedingungen, die die Sicherheit von Praktikern und Dritten gewährleisten, sowie die zu beobachtenden Verhaltensweisen und die Handlungen, die im Falle eines Vorfalls oder Unfalls durchzuführen sind.

Der Sportpädagoge ist für die Sicherheit der Öffentlichkeit, für die er verantwortlich ist, und der Dritten verantwortlich. Es sorgt für den guten Zustand der verwendeten Ausrüstung. Er passt sich den Eigenschaften seines Publikums an: Er gibt Anweisungen und korrigiert Gesten und Haltungen, um ihm beim Fortschritt zu helfen.

*Um weiter zu gehen* Artikel L. 212-1 und R. 212-1 des Sportkodex.

Zwei Grad. Berufsqualifikationen
--------------------------------

### Nationale Anforderungen

#### Nationale Rechtsvorschriften

Die Tätigkeit des Sportpädagogen unterliegt der Anwendung von Artikel L. 212-1 des Sportkodex, der die Erlangung spezifischer Zertifizierungen erfordert.

Als Sportlehrer muss der Sportpädagoge ein Diplom, eine Berufsbezeichnung oder eine Qualifikationsbescheinigung besitzen:

- Gewährleistung seiner Kompetenz in Bezug auf die Sicherheit von Praktikern und Dritten bei der betreffenden körperlichen oder sportlichen Tätigkeit;
- aufgezeichnet auf der[Nationales Verzeichnis professioneller Zertifizierungen](http://www.rncp.cncp.gouv.fr/) (RNCP).

Die Liste der Diplome, Berufsbezeichnungen und Befähigungsnachweise für die Ausübung als Sportpädagoge wird unter[Anlage II-1 (Artikel A. 212-1) des Sportkodex](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8231A63F40B1958AFA1DE7E71AB6ED79.tpdila09v_3?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000033170986&dateTexte=20170119&categorieLien=cid#LEGIARTI000033170986). Diese Liste legt die Bedingungen und Übungsgrenzen fest, die für die Inhaber jedes Grades gelten, entsprechend den spezifischen Vorschriften für jede Aktivität.

Voraussetzungen für die Ausübung des Sportpädagogen:

- Das Berufszeugnis für Jugend, Volksbildung und Sport (BPJEPS) Spezialität "Sportpädagoge";
- das Staatsdiplom für Jugend, Volksbildung und Sport (DEJEPS) und das Staatsdiplom für Jugend, Volksbildung und Sport (DESJEPS);
- Certificate of Professional Qualification (CQP). Beispiele hierfür sind:- CQP "Flachfluglehrer im Windkanal"
  + CQP "Athletik-Moderator"
  + CQP "künstlich strukturierter Klettervermittler".

Ausländische Diplome können vom für Sport zuständigen Minister nach Stellungnahme der Kommission für die Anerkennung von Beim Minister vermittelten Qualifikationen in Gleichnahme zu französischen Diplomen zugelassen werden.

*Um weiter zu gehen* Artikel L. 212-1 und Folge, R. 212-1 und darüber hinaus, R. 212-84, A. 212-1 und im Folgenden, Anlage II-1 (Art. A. 212-1) des Sportkodex.

**Gut zu wissen**

Für einige Disziplinen sind DEJEPS oder DESJEPS manchmal verpflichtet, in größerer Autonomie zu arbeiten, z. B. für den Unterricht und das Tauchen. Für weitere Informationen ist es ratsam, auf das Blatt "Unterwasser-Tauchmonitor" zu verweisen.

#### Ausbildung

##### BPJEPS Spezialität "Sportpädagoge"

Das BPJEPS ist ein Staatsdiplom, das in der[RNCP](http://www.rncp.cncp.gouv.fr/) und auf Stufe IV der Nomenklatur der Zertifizierungsstufe eingestuft. Sie bescheinigt den Erwerb einer Qualifikation bei der Ausübung einer beruflichen Tätigkeit in der Verantwortung für pädagogische oder soziale Zwecke in den Bereichen körperliche, sportliche, sozio-pädagogische oder kulturelle Tätigkeiten.

Der BPJEPS wird unter der Spezialität "Animator" oder der Spezialität "Sportpädagoge" und einer disziplinarischen oder multidisziplinären Erwähnung herausgegeben. Jede Erwähnung erfolgt durch eine Bestellung, die:

- definiert das berufsspezifische Repository: es ist die Darstellung des Berufssektors, die Stellenbeschreibung und das Tätigkeitsbeschreibungsblatt;
- definiert das Zertifizierungs-Repository, das aus allen Einheiten besteht, aus denen das Diplom besteht, und legt für jede Einheit die beruflichen Fähigkeiten, die Zwischenziele des ersten Ranges und die Zertifikatsprüfungen dieser Ziele
- legt Anforderungen an die Vorschulung, vorprofessionelle Anforderungen und Zertifikatsverfahren fest;
- Legt gegebenenfalls Äquivalenz- oder Freistellungsmaßnahmen fest;
- legt erforderlichenfalls die Bedingungen für die Überprüfung der Aufrechterhaltung beruflicher Leistungen im Zusammenhang mit der Sicherheit von Praktikern und Dritten fest.

Der BPJEPS wird entweder mittels kapitalisierbarer Einheiten (UC) oder durch Validierung von Erfahrungen (VAE) ausgegeben, diese Bedingungen können kumuliert werden. Weitere Informationen finden Sie unter[Offizielle Website von VAE](http://www.vae.gouv.fr).

Dieses Diplom wird entweder durch Erstausbildung, durch Lernen oder durch Weiterbildung erstellt. Wenn die Ausbildung im Rahmen der Erstausbildung abgeschlossen wird, beträgt die Mindestdauer 900 Stunden, von denen 600 Stunden im Zentrum sind.

Zusätzliche Zertifikate, die professionelle Fähigkeiten bescheinigen, die einem bestimmten Bedarf entsprechen und die gleichen Anforderungen erfüllen wie die für das Diplom festgelegten, können mit der Spezialität "Sportpädagoge" PJEPS in Verbindung gebracht werden. Sie werden unter den gleichen Bedingungen ausgestellt wie die im Diplom.

*Um weiter zu gehen* Artikel D. 212-20 bis D. 212-31, A. 212-47 bis A. 212-47-4 des Sportkodex.

###### Berufliche Fähigkeiten

Der BPJEPS wird durch Kapitalisierung von vier UC erhalten, von denen zwei Querschnittsmittel für alle Fach-"Sportpädagogen" BPJEPS und zwei spezifisch für die gewählte Bezeichnung sind. Die vier UCs werden durch die folgenden beruflichen Fähigkeiten und Zwischenziele (IOs) definiert:

- UC 1: Beaufsichtigung jeder Öffentlichkeit an jedem Ort und in jeder Struktur:- OI 1-1: Kommunikation in beruflichen Lebenssituationen,
  + OI 1-2: Berücksichtigung der Merkmale des Publikums in seinem Umfeld in einem staatsbürgerlichen Bildungsprozess,
  + OI 1-3: Beitrag zum Betrieb einer Struktur;
- UC 2: Implementieren Eines Animationsprojekts als Teil des Projekts der Struktur:- OI 2-1: Entwerfen eines Animationsprojekts,
  + OI 2-1: Durchführung eines Animationsprojekts,
  + OI 2-3: Auswertung eines Animationsprojekts;
- UC 3: Durchführung einer Sitzung, eines Animations- oder Lernzyklus im Bereich der Erwähnung:- OI 3-1: Entwerfen der Sitzung, des Animations- oder Lernzyklus,
  + OI 3-2: Führen Sie die Sitzung, den Zyklus der Animation oder des Lernens,
  + OI 3-3: Bewerten sie die Sitzung, den Animations- oder Lernzyklus;
- UC 4: Mobilisierung der Techniken der Erwähnung oder Option, um eine Sitzung, eine Animation oder einen Lernzyklus zu implementieren:- OI 4-1: Durchführung einer Sitzung oder eines Zyklus mit den Techniken der Erwähnung oder Option,
  + OI 4-2: Master und durchsetzung der Regeln der Erwähnung oder Option,
  + OI 4-3: Sorgen Sie für sichere Übungsbedingungen.

###### Zugangsbedingungen

Für Vorabtests muss der Bewerber eine Datei bei der für die Organisation zuständigen Ausbildungsstelle einreichen, die einen Monat vor dem für die Tests festgelegten Datum zuständig ist, der Folgendes umfasst:

- Ein Anmeldeformular mit einem Foto
- Eine Fotokopie eines gültigen Ausweisstücks
- die Bescheinigungoderoderin, die die Befreiung bestimmter Prüfungen rechtfertigt, die durch das Dekret zur Herstellung der Spezialität, die Erwähnung des betreffenden Diploms oder der betreffenden Zusatzbescheinigung festgelegt werden;
- ein ärztliches Attest, das der Sportpraxis nicht im Widerspruch zu steht, das nicht älter als ein Jahr ist, sofern in dem Dekret zur Schaffung der betreffenden Spezialität, Erwähnung oder ergänzenden Bescheinigung nichts anderes bestimmt ist;
- für Menschen mit Behinderungen die Stellungnahme eines Arztes, der von der französischen Handisport-Föderation oder dem französischen Verband für angepassten Sport zugelassen oder vom Ausschuss für die Rechte und Autonomie von Menschen mit Behinderungen ernannt wurde, über die Notwendigkeit Vorabtests zu entwickeln, wie dies gemäß der Zertifizierung erforderlich ist.

Für die Anmeldung zu einer Schulung muss sich der Antragsteller bei der Ausbildungsorganisation, die die Einhaltung überwacht, einen Monat vor dem für die Teilnahme an der Schulung festgelegten Datum anmelden, eine Datei, die Folgendes enthält:

- Ein Anmeldeformular mit einem Foto
- Eine Fotokopie eines gültigen Ausweisstücks
- eine Kopie der Volkszählungsbescheinigung oder der individuellen Teilnahmebescheinigung am Tag der Verteidigung und der Staatsbürgerschaft;
- die Bescheinigungen oder Bescheinigungen, die die Erfüllung der früheren Anforderungen rechtfertigen, die durch das Dekret zur Herstellung der Spezialität, die Erwähnung des betreffenden Diploms oder der betreffenden Zusatzbescheinigung festgelegt sind;
- Dokumente, die Ausnahmen und Rechtsäquivalenz rechtfertigen;
- zur Eintragung eines Zusatzzeugnisses, einer Fotokopie des Diploms, das die Registrierung in der Ausbildung genehmigt, oder einer Bescheinigung über die Registrierung für die Ausbildung, die zu diesem Diplom führt;
- die in der Bestellung zur Herstellung der Spezialität, der Nennung oder der betreffenden ergänzenden Bescheinigung vorgesehenen oder sonstigen Unterlagen;
- für Menschen mit Behinderungen die Stellungnahme eines Arztes, der von der französischen Handisport-Föderation oder dem französischen Verband für angepassten Sport zugelassen oder vom Ausschuss für die Rechte und Autonomie von Menschen mit Behinderungen ernannt wurde, über die Notwendigkeit Um Schulungen oder Zertifikatsprüfungen nach der Zertifizierung zu arrangieren.

Für die VAE-Datei ist es ratsam, auf die[Artikel A. 212-41 und der nachfolgende Sportkodex](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031831233&cidTexte=LEGITEXT000006071318&dateTexte=20170119).

Für weitere Informationen ist es ratsam, auf das Blatt zu verweisen, das der praktizierten Disziplin entspricht.

*Um weiter zu gehen* Artikel A. 212-35 und die folgenden aus dem Sportkodex.

#### Mit der Qualifizierung verbundene Kosten

Die Ausbildung, die zum PJEPS führt, wird bezahlt. Die Kosten variieren je nach Erwähnung und Ausbildungsorganisationen.

Für weitere Informationen ist es ratsam, näher an die betreffende Ausbildungsorganisation zu kommen.

### b. EU-Bürger: für vorübergehende und gelegentliche Ausübung (Kostenlose Erbringung von Dienstleistungen)

Staatsangehörige der Europäischen Union (EU)* Europäischer Wirtschaftsraum (EWR)* in einem dieser Staaten niedergelassene Unternehmen können die gleiche Tätigkeit in Frankreich vorübergehend und gelegentlich ausüben, sofern sie dem Präfekten der Abteilung der Lieferung eine vorherige Erklärung über ihre Tätigkeit übermittelt hat.

Ist die dort geführte Tätigkeit oder Ausbildung weder im Herkunftsmitgliedstaat noch im Niederlassungsort geregelt, so muss der Staatsangehörige auch rechtfertigen, dass er diese Tätigkeit dort mindestens für ein Vollzeitjahr im in den letzten zehn Jahren vor der Leistung.

Europäische Staatsangehörige, die vorübergehend oder gelegentlich in Frankreich praktizieren möchten, müssen über die für die Ausübung der Tätigkeit in Frankreich erforderlichen Sprachkenntnisse verfügen, insbesondere um die Sicherheit der Tätigkeiten zu gewährleisten. und seine Fähigkeit, Rettungsdienste zu alarmieren.

*Um weiter zu gehen* Artikel L. 212-7 und R. 212-92 bis R. 212-94 des Sportkodex.

### c. EU-Bürger: für eine ständige Ausübung (Freie Niederlassung)

Ein Staatsangehöriger eines EU- oder EWR-Staates kann sich in Frankreich niederlassen, um dauerhaft zu praktizieren, wenn er eine der folgenden vier Bedingungen erfüllt:

**Wenn der Herkunftsmitgliedstaat den Zugang oder die Ausübung der Tätigkeit regelt:**

- im Besitz eines Befähigungszeugnisses oder eines Ausbildungszeugnisses, das von der zuständigen Behörde einer EU oder eines EWR ausgestellt wurde und der ein Qualifikationsniveau bescheinigt, das mindestens dem Niveau entspricht, das unmittelbar unter dem in Frankreich geforderten Niveau liegt;
- Inhaber eines in einem Drittstaat erworbenen und gleichwertigen Titels in einem EU- oder EWR-Staat sein und die Durchführung dieser Tätigkeit für mindestens zwei Jahre in diesem Staat rechtfertigen.

**Wenn der Herkunftsmitgliedstaat den Zugang oder die Ausübung der Tätigkeit nicht regelt:**

- rechtfertigen, in einem EU- oder EWR-Staat tätig gewesen zu sein, in den letzten zehn Jahren mindestens zwei Jahre lang Vollzeit oder, im Falle einer Teilzeitbeschäftigung, eine Tätigkeit gleicher Dauer zu rechtfertigen und im Besitz eines Zertifikats zu sein von der zuständigen Behörde eines dieser Staaten ausgestellte Qualifikation, die eine Bereitschaft zur Ausübung der Tätigkeit bescheinigt, sowie ein Qualifikationsniveau, das mindestens dem Niveau entspricht, das unmittelbar unter dem in Frankreich;
- Inhaber eines Qualifikationszeugnisses sein, das mindestens dem Niveau entspricht, das unmittelbar unter dem in Frankreich geforderten Niveau liegt, das von der zuständigen Behörde eines EU- oder EWR-Staates ausgestellt wird, und eine regulierte Ausbildung sanktionieren, die auf die insbesondere die Ausübung aller oder eines Teils der in Artikel L. 212-1 des Sportkodex genannten Tätigkeiten, die aus einem Studienzyklus bestehen, der gegebenenfalls durch eine Berufsausbildung, ein Praktikum oder eine Berufspraxis ergänzt wird.

Erfüllt der Staatsangehörige eine der vier oben genannten Voraussetzungen, so gilt die für die Ausübung der Praxis erforderliche Qualifikationsvoraussetzung als erfüllt.

Weichen die Beruflichen Qualifikationen des Staatsangehörigen jedoch erheblich von den in Frankreich vorgeschriebenen Qualifikationen ab, die die Sicherheit von Praktikern und Dritten nicht gewährleisten würden, kann er verpflichtet sein, sich Eignungsprüfung oder Abschluss eines Anpassungskurses (siehe unten "Gut zu wissen: Vergütungsmaßnahmen").

Der Staatsangehörige muss über die Kenntnisse der französischen Sprache verfügen, die für seine Tätigkeit in Frankreich erforderlich sind, insbesondere um die Sicherheit körperlicher und sportlicher Aktivitäten und seine Fähigkeit, die Rettungsdienste zu alarmieren, zu gewährleisten.

*Um weiter zu gehen* Artikel L. 212-7 und R. 212-88 bis R. 212-90 des Sportkodex.

Drei Grad. Bedingungen der Ehrenfähigkeit
-----------------------------------------

Es ist verboten, als Sportpädagoge in Frankreich für Personen zu praktizieren, die wegen einer Straftat oder einer der folgenden Straftaten verurteilt wurden:

- Folter und Barbareiakte;
- Sexuelle Übergriffe;
- Drogenhandel;
- Gefährdung anderer;
- Zuhälterei und die sich daraus ergebenden Straftaten;
- Gefährdung von Minderjährigen;
- unerlaubte Verwendung von Stoffen oder Pflanzen, die als Betäubungsmittel oder als Provokation für den unerlaubten Gebrauch von Betäubungsmitteln eingestuft sind;
- Verstöße gegen die Artikel L. 235-25 bis L. 235-28 des Sportkodex;
- als eine Zulage zu einer Steuerstraftat: ein vorübergehendes Verbot, im Namen seiner person oder anderer einen gewerblichen, gewerblichen oder liberalen Beruf auszuüben ( Abschnitt 1750 des Allgemeinen Steuergesetzbuches).

Darüber hinaus darf niemand eine körperliche oder sportliche Tätigkeit mit Minderjährigen unterrichten, erleichtern oder überwachen, wenn ihm eine Verwaltungsmaßnahme untersagt wurde, die ihm die Teilnahme an der Leitung und Aufsicht von Einrichtungen und Einrichtungen, die Rechtsvorschriften oder Verordnungen über den Schutz von Minderjährigen in einem Ferien- und Freizeitzentrum sowie von Jugendgruppen unterliegen, oder wenn es Gegenstand einer Verwaltungsmaßnahme war, diese die gleichen Funktionen.

*Um weiter zu gehen* Artikel L. 212-9 des Sportkodex.

Es ist ein Vier-Grad-Eins. Qualifikationsprozess und Formalitäten
-----------------------------------------------------------------

### a. Meldepflicht (zum Zwecke der Erlangung der Professionellen Sportpädagogenkarte)

Wer einen der in Artikel L. 212-1 des Sportkodex geregelten Berufe ausüben möchte, muss seine Tätigkeit dem Präfekten der Abteilung des Ortes, an dem er als Schulleiter ausüben will, melden. Diese Erklärung löst den Erhalt einer Visitenkarte aus.

Die Erklärung muss alle fünf Jahre erneuert werden.

**Zuständige Behörde**

Die Erklärung sollte an die Direktion soziales Zusammenhalt (DDCS) oder an die Direktion für sozialen Zusammenhalt und Bevölkerungsschutz (DDCSPP) der Praxisabteilung oder der Hauptübung oder direkt in der Linie auf der[offizielle Website](https://eaps.sports.gouv.fr).

**Zeit**

Innerhalb eines Monats nach Einreichung der Anmeldedatei sendet die Präfektur eine Bestätigung an den Registranten. Die Visitenkarte, die fünf Jahre gültig ist, wird dann an den Registranten adressiert.

**Belege**

Die zur Verfügung zu stellenden Belege sind:

- Cerfa 12699*02 ;
- Eine Kopie einer gültigen ID
- Ein Lichtbildausweis
- eine Erklärung über die Ehre, die die Richtigkeit der Informationen in der Form bescheinigt;
- Eine Kopie jedes der beglaubigten Diplome, Titel und Befähigungszeugnisse;
- eine Kopie der Ermächtigung zur Ausübung oder, falls erforderlich, der Gleichwertigkeit des Diploms;
- ein ärztliches Attest über das nicht widersprechende zeugnisende Der Ausübung und der Überwachung der betreffenden körperlichen oder sportlichen Tätigkeiten, die weniger als ein Jahr alt sind.

Wenn Sie eine Rücksendung haben, wenden Sie sich bitte an:

- Form Form Cerfa 12699*02 ;
- Ein Lichtbildausweis
- eine Kopie der gültigen Überprüfungsbescheinigung für Qualifikationen, die der Recyclingpflicht unterliegen;
- ein ärztliches Attest über das nicht widersprechende zeugnisende Der Ausübung und der Überwachung der betreffenden körperlichen oder sportlichen Tätigkeiten, die weniger als ein Jahr alt sind.

Darüber hinaus wird die Präfektur in allen Fällen selbst die Freigabe eines Auszugs von weniger als drei Monaten aus dem Strafregister des Registranten beantragen, um zu überprüfen, ob es keine Behinderung oder ein Verbot der Praxis gibt.

**Kosten**

kostenlos.

*Um weiter zu gehen* Artikel L. 212-11, R. 212-85 und A. 212-176 bis A. 212-178 des Sportkodex.

### b. eine Voranmeldung der Tätigkeit für EU-Bürger, die vorübergehende und gelegentliche Tätigkeiten ausüben (LPS)

EU- oder EWR-Staatsangehörige, die in einem dieser Staaten rechtmäßig niedergelassen sind und vorübergehend oder gelegentlich in Frankreich praktizieren möchten, müssen vor der ersten Erbringung von Dienstleistungen eine vorherige Erklärung ihrer Tätigkeit abgeben.

Wenn der Antragsteller in Frankreich eine neue Leistung beziehen möchte, muss diese vorherige Erklärung erneuert werden.

Um schwerwiegende Schäden für die Sicherheit der Begünstigten zu vermeiden, kann der Präfekt während der ersten Leistung eine Vorläufige Überprüfung der beruflichen Qualifikationen des Antragstellers durchführen.

**Zuständige Behörde**

Die vorherige Tätigkeitserklärung sollte an die für den sozialen Zusammenhalt zuständige Direktion (DDCS) oder an die für den sozialen Zusammenhalt und bevölkerungsschutzzuständige Direktion (DDCSPP) der Abteilung gerichtet werden, in der die Der Alleinspieler möchte seine Leistung vollbringen.

**Zeit**

Innerhalb eines Monats nach Erhalt der Erklärungsakte benachrichtigt der Präfekt den Antragsteller:

- oder ein Ersuchen um weitere Informationen (in diesem Fall hat der Präfekt zwei Monate Zeit, um seine Antwort zu geben);
- oder eine Quittung für eine Leistungsauszugserklärung, wenn sie keine Qualifikationsprüfung durchführt. In diesem Fall kann die Servicebereitstellung beginnen;
- oder dass sie die Qualifikationsprüfung durchführt. In diesem Fall stellt der Präfekt dem Antragsteller dann eine Quittung aus, die es ihm ermöglicht, seine Leistung zu beginnen, oder, wenn die Überprüfung der Qualifikationen erhebliche Unterschiede zu den in Frankreich erforderlichen beruflichen Qualifikationen zeigt, Präfekt unterwirft den Kläger einem Eignungstest (siehe infra "Gut zu wissen: Entschädigungsmaßnahmen").

In allen Fällen gilt der Antragsteller in Frankreich, wenn innerhalb der vorgenannten Fristen keine Antwort erfolgt, als rechtlich tätig.

**Belege**

Die Aktivitätsvorberichtsdatei muss Folgendes enthalten:

- eine Kopie des Erklärungsformulars in Anhang II-12-3 des Sportkodex;
- Ein Lichtbildausweis
- Eine Kopie einer ID
- eine Kopie des Bestellterzeugnisses oder des Ausbildungstitels;
- Eine Kopie der Dokumente, aus denen hervorgeht, dass der Registrant im Mitgliedstaat des Organs rechtmäßig niedergelassen ist und dass ihm kein Verbot, auch nur vorübergehend, die Ausübung (übersetzt ins Französische durch einen beglaubigten Übersetzer) unterliegt;
- für den Fall, dass weder die Tätigkeit noch die Ausbildung, die zu dieser Tätigkeit führt, im Betriebsmitgliedstaat geregelt ist, eine Kopie aller Dokumente, die rechtfertigen, dass der Registrant diese Tätigkeit in diesem Staat mindestens zwei Jahre lang ausgeübt hat Vollzeit in den letzten zehn Jahren (übersetzt ins Französische von einem zertifizierten Übersetzer);
- eines der drei Dokumente zur Auswahl (falls nicht, wird ein Interview geführt):- Eine Kopie eines nach der Ausbildung in Französisch ausgestellten Qualifikationszeugnisses,
  + Eine Kopie eines von einer spezialisierten Institution ausgestellten französischen Zertifikats,
  + eine Kopie eines Dokuments, aus dem die in Frankreich erworbenen Berufserfahrung hervorgeht.

**Kosten**

kostenlos.

**Heilmittel**

Alle Rechtsstreitigkeiten müssen innerhalb von zwei Monaten nach Zustellung der Entscheidung an das zuständige Verwaltungsgericht ausgeübt werden.

*Um weiter zu gehen* Artikel R. 212-92 und nachfolgenden, A. 212-182-2 und nachfolgende Artikel und Anhang II-12-3 des Sportkodex.

### c. eine Voranmeldung der Tätigkeit für EU-Bürger für eine ständige Ausübung (LE)

Jeder EU- oder EWR-Staatsangehörige, der berechtigt ist, die in Artikel L. 212-1 des Sportkodex genannten Tätigkeiten ganz oder teilweise auszuüben, und sich in Frankreich niederlassen möchte, muss dem Präfekten der Abteilung, in der er beabsichtigt, eine Erklärung abgeben. als Auftraggeber.

Diese Erklärung ermöglicht es dem Registranten, einen Berufsausweis zu erhalten und somit in Frankreich unter den gleichen Bedingungen wie französische Staatsangehörige legal zu praktizieren.

Die Erklärung muss alle fünf Jahre erneuert werden.

Im Falle einer wesentlichen Abzweiderung von der in Frankreich erforderlichen Qualifikation kann der Präfekt den Ausschuss für die Anerkennung von Qualifikationen dem Sportminister zur Beratung vorlegen. Sie können auch beschließen, den Staatsangehörigen einer Eignungsprüfung oder einem Unterbringungskurs zu unterziehen (siehe unten: "Gut zu wissen: Entschädigungsmaßnahmen").

**Zuständige Behörde**

Die Erklärung sollte an die für den sozialen Zusammenhalt zuständige Direktion (DDCS) oder an die für den sozialen Zusammenhalt und den Bevölkerungsschutz (DDCSPP) zuständige Direktion gerichtet werden.

**Zeit**

Die Entscheidung des Präfekten, die Visitenkarte ausstellen zu lassen, fällt innerhalb von drei Monaten nach Einreichung der vollständigen Akte durch den Registranten. Diese Frist kann aufgrund einer mit Gründen versehenen Entscheidung um einen Monat verlängert werden. Entscheidet sich der Präfekt, die Berufskarte nicht auszulegen oder den Anmelder einer Entschädigungsmaßnahme (Fitnesstest oder Praktikum) zu unterziehen, so muss seine Entscheidung begründet sein.

**Belege für die erste Tätigkeitserklärung**

Die Aktivitätsberichtsdatei sollte Folgendes enthalten:

- eine Kopie des Erklärungsformulars in Anhang II-12-2-a des Sportkodex;
- Ein Lichtbildausweis
- Eine Kopie einer gültigen ID
- ein ärztliches Attest über das nicht widersprechende zeugnisende Zeugnis für die Ausübung und Überwachung körperlicher oder sportlicher Tätigkeiten, das weniger als ein Jahr alt ist (übersetzt von einem zertifizierten Übersetzer);
- Eine Kopie des Bestelltheits oder Ausbildungstitels, begleitet von Dokumenten, die den Ausbildungsgang beschreiben (Programm, Stundenvolumen, Art und Dauer der praktikapten Praktika), übersetzt ins Französische von einem beglaubigten Übersetzer;
- Wenn ja, eine Kopie von Beweisen, die die Arbeitserfahrung rechtfertigen (von einem beglaubigten Übersetzer ins Französische übersetzt);
- Wenn das Ausbildungsdokument in einem Drittstaat eingeholt wurde, Kopien der Dokumente, aus denen hervorgeht, dass der Titel als Äquivalenz in einem EU- oder EWR-Staat, der die Tätigkeit regelt, zugelassen wurde;
- eines der drei Dokumente zur Auswahl (falls nicht, wird ein Interview geführt):- Eine Kopie eines nach der Ausbildung in Französisch ausgestellten Qualifikationszeugnisses,
  + Eine Kopie eines von einer spezialisierten Institution ausgestellten französischen Zertifikats,
  + Eine Kopie eines Dokuments, aus dem die in Frankreich erworbene Berufserfahrung hervorgeht,
  + Dokumente, aus denen hervorgeht, dass der Registrant gegenstand keiner der in den Artikeln L. 212-9 und L. 212-13 des Sportkodex (von einem beglaubigten Übersetzer ins Französische übersetzten) Verurteilungen oder Maßnahmen im Herkunftsmitgliedstaat war.

**Nachweis einer Erneuerung der Tätigkeitserklärung**

Die Aktivitätserneuerungsdatei muss Folgendes enthalten:

- Eine Kopie des Formulars für die Rücksendung, nach dem Vorbild des Spielplans II-12-2-b des Sportkodex;
- Ein Lichtbildausweis
- ein ärztliches Attest über das nicht widersprechende zeugnisende Zeugnis für die Ausübung und Überwachung körperlicher oder sportlicher Tätigkeiten, das weniger als ein Jahr alt ist.

**Kosten**

kostenlos.

**Heilmittel**

Alle Rechtsstreitigkeiten müssen innerhalb von zwei Monaten nach Zustellung der Entscheidung an das zuständige Verwaltungsgericht ausgeübt werden.

*Um weiter zu gehen* Artikel R. 212-88 bis R. 212-91, A. 212-182, und Spielpläne II-12-2-a und II-12-b des Sportkodex.

### d. Ausgleichsmaßnahmen

Besteht ein wesentlicher Unterschied zwischen der Qualifikation des Antragstellers und der in Frankreich zur Durchführung derselben Tätigkeit erforderlichen Qualifikation, so verweist der Präfekt die Kommission für die Anerkennung von Qualifikationen, die beim Sportminister eingesetzt wird. Diese Kommission erlässt nach Prüfung und Untersuchung der Akte innerhalb des Monats nach ihrer Verweisung eine Mitteilung, die sie an den Präfekten sendet.

In ihrer Stellungnahme kann die Kommission:

- ist der Ansicht, dass es in der Tat einen wesentlichen Unterschied zwischen der Qualifikation des Registranten und der in Frankreich erforderlichen ist. In diesem Fall schlägt die Kommission vor, den Registranten einem Eignungstest oder einem Anpassungskurs zu unterziehen. Er legt die Art und die genauen Modalitäten dieser Ausgleichsmaßnahmen fest (Art der Tests, die Bedingungen ihrer Organisation und Bewertung, die Dauer der Organisation, den Inhalt und die Dauer des Praktikums, die Arten von Strukturen, die den Auszubildenden aufnehmen können; usw.) ;
- dass es keinen wesentlichen Unterschied zwischen der Qualifikation des Registranten und der in Frankreich erforderlichen

Nach Eingang der Stellungnahme der Kommission notifiziert der Präfekt den Registranten über seine begründete Entscheidung (er ist nicht verpflichtet, dem Rat der Kommission zu folgen):

- Wenn der Registrant eine Entschädigung verlangt, hat er einen Monat Zeit, um zwischen dem Eignungstest und dem Unterkunftskurs zu wählen. Der Präfekt stellt dann dem Registranten, der die Entschädigungsmaßnahmen eingehalten hat, eine Visitenkarte aus. Ist der Kurs oder die Eignungsprüfung hingegen nicht zufriedenstellend, so notifiziert der Präfekt seine begründete Entscheidung, der betreffenden Person die Ausstellung der Berufskarte zu verweigern;
- wenn er keine Entschädigung verlangt, stellt der Präfekt dem Betroffenen eine Visitenkarte aus.

*Um weiter zu gehen* Artikel R. 212-84 und D. 212-84-1 des Sportkodex.

### e. Abhilfemaßnahmen

#### Französisches Hilfszentrum

Das ENIC-NARIC-Zentrum ist das französische Informationszentrum für die akademische und berufliche Anerkennung von Diplomen.

#### Solvit

SOLVIT ist eine Dienstleistung, die von der nationalen Verwaltung jedes Mitgliedstaats der Europäischen Union oder einer Vertragspartei des EWR-Abkommens erbracht wird. Ziel ist es, eine Lösung für einen Streit zwischen einem EU-Bürger und der Verwaltung eines anderen dieser Staaten zu finden. SOLVIT greift insbesondere in die Anerkennung von Berufsqualifikationen ein.

**Bedingungen**

Der Betroffene kann SOLVIT nur verwenden, wenn er

- dass die öffentliche Verwaltung eines EU-Staates seine Rechte nach dem EU-Recht als Bürger oder Unternehmen eines anderen EU-Staates nicht geachtet hat;
- dass sie noch keine Klage eingeleitet hat (Verwaltungsmaßnahmen werden nicht als solche betrachtet).

**Verfahren**

Der Staatsangehörige muss eine [Online-Beschwerdeformular](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Sobald seine Akte eingereicht wurde, kontaktiert SOLVIT ihn innerhalb einer Woche, um gegebenenfalls zusätzliche Informationen anzufordern und zu überprüfen, ob das Problem in seine Zuständigkeit fällt.

Am Ende der 10-Wochen-Frist präsentiert SOLVIT eine Lösung:

- Wenn diese Lösung den Streit über die Anwendung des europäischen Rechts beilegt, wird die Lösung akzeptiert und der Fall abgeschlossen;
- wenn es keine Lösung gibt, wird der Fall als ungelöst abgeschlossen und an die Europäische Kommission verwiesen.

**Belege**

Um solVIT zu betreten, muss der Staatsangehörige Folgendes mitteilen:

- Vollständige Kontaktdaten
- Detaillierte Beschreibung seines Problems
- alle Indizbeweise (z. B. Korrespondenz und Entscheidungen der zuständigen Verwaltungsbehörde).

**Zeit**

SOLVIT ist entschlossen, innerhalb von zehn Wochen nach der Übernahme des Falles durch das SOLVIT-Zentrum in dem Land, in dem das Problem aufgetreten ist, eine Lösung zu finden.

**Kosten**

kostenlos.

**Weitere Informationen**

SOLVIT in Frankreich: Generalsekretariat für europäische Angelegenheiten, 68 rue de Bellechasse, 75700 Paris, ([offizielle Website](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

