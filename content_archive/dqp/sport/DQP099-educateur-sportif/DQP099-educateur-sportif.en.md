﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP099" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Sports educator" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="sports-educator" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/sports-educator.html" -->
<!-- var(last-update)="2020-04-28 17:36:38" -->
<!-- var(url-name)="sports-educator" -->
<!-- var(translation)="Auto" -->


Sports educator
===============

Latest update: : <!-- begin-var(last-update) -->2020-04-28 17:36:38<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
(1) Defining the activity
-------------------------

The sports educator is a professional who teaches, facilitates or supervises a physical or sports activity. He trains his practitioners. He works as a principal or secondary occupation, usually, seasonally or occasionally.

He is able to mobilize the technical and pedagogical knowledge specific to the activity he supervises, depending on the diploma he holds. He masters the techniques of his practice in conditions that ensure the safety of practitioners and third parties as well as the behaviours to be observed and the actions to be performed in the event of an incident or accident.

The sports educator is responsible for the safety of the public for which he is responsible and of the third parties. It ensures the good condition of the equipment used. He adapts to the characteristics of his audience: he gives directions and corrects gestures and postures to help him progress.

*To go further* Article L. 212-1 and R. 212-1 of the Code of Sport.

Two degrees. Professional qualifications
----------------------------------------

### National requirements

#### National legislation

The activity of sports educator is subject to the application of Article L. 212-1 of the Code of Sport which requires the obtaining of specific certifications.

As a sports teacher, the sports educator must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](http://www.rncp.cncp.gouv.fr/) (RNCP).

The list of diplomas, professional titles and certificates of qualification to practise as a sports educator is drawn up at[Appendix II-1 (Article A. 212-1) of the Code of Sport](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8231A63F40B1958AFA1DE7E71AB6ED79.tpdila09v_3?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000033170986&dateTexte=20170119&categorieLien=cid#LEGIARTI000033170986). This list specifies the conditions and exercise limits applicable to the holders of each degree, according to the specific regulations for each activity.

Qualifications for practising as a sports educator are:

- The professional certificate of youth, popular education and sport (BPJEPS) specialty "sports educator";
- the State Diploma of Youth, Popular Education and Sport (DEJEPS) and the State Diploma of Youth, Popular Education and Sport (DESJEPS);
- Certificate of Professional Qualification (CQP). Examples include:- CQP "flat-flying instructor in wind tunnel"
  + CQP "Athletics Facilitator"
  + CQP "artificially structured climbing facilitator."

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*To go further* Articles L. 212-1 and Following, R. 212-1 and Beyond, R. 212-84, A. 212-1 and following, Appendix II-1 (Art. A. 212-1) of the Code of Sport.

**Good to know**

For some disciplines, DEJEPS or DESJEPS are sometimes required to operate in greater autonomy, such as for teaching and diving. For more information, it is advisable to refer to the "Underwater Diving Monitor" sheet.

#### Training

##### BPJEPS specialty "sports educator"

The BPJEPS is a state diploma registered in the[RNCP](http://www.rncp.cncp.gouv.fr/) and classified at level IV of the certification level nomenclature. It attests to the acquisition of a qualification in the exercise of a professional activity in responsibility for educational or social purposes, in the fields of physical, sports, socio-educational or cultural activities.

The BPJEPS is issued under the speciality "animator" or the speciality "sports educator" and a disciplinary or multidisciplinary mention. Each mention is made by an order that:

- defines the professional repository: it is the presentation of the professional sector, the job description and the activity description sheet;
- defines the certification repository, which is made up of all the units that make up the diploma and sets out for each unit the professional skills, the intermediate objectives of the first rank and the certificate tests of these Goals
- specifies pre-training requirements, pre-professional requirements and certificate procedures;
- If necessary, sets equivalency or exemption measures;
- if necessary, sets the conditions for verifying the maintenance of professional achievements related to the safety of practitioners and third parties.

The BPJEPS is issued either by means of capitalisable units (UC) or by validation of experience (VAE), these terms can be accumulated. For more information, you can see[VAE's official website](http://www.vae.gouv.fr).

This diploma is prepared either through initial training, through learning or through continuing education. When the training is completed as part of the initial training, its minimum duration is 900 hours, of which 600 hours are in the centre.

Additional certificates, which attest to professional skills that meet a specific need and meet the same requirements as those set for the diploma, may be associated with the specialty "sports educator" PJEPS. They are issued under the same conditions as those in the diploma.

*To go further* Articles D. 212-20 to D. 212-31, A. 212-47 to A. 212-47-4 of the Code of Sport.

###### Professional skills

The BPJEPS is obtained by capitalization of four UC, two of which are cross-sectional to all specialty "sports educator" BPJEPS and two specific to the chosen designation. The four UCs are defined by the following professional skills and intermediate objectives (IOs):

- UC 1: to supervise any public in any place and structure:- OI 1-1: communicating in professional life situations,
  + OI 1-2: taking into account the characteristics of audiences in their environments in a citizenship education process,
  + OI 1-3: contributing to the operation of a structure;
- UC 2: implement an animation project as part of the structure's project:- OI 2-1: designing an animation project,
  + OI 2-1: conduct an animation project,
  + OI 2-3: evaluate an animation project;
- UC 3: conduct a session, an animation or learning cycle in the field of mention:- OI 3-1: design the session, the animation or learning cycle,
  + OI 3-2: lead the session, the cycle of animation or learning,
  + OI 3-3: evaluate the session, the animation or learning cycle;
- UC 4: mobilize the techniques of mention or option to implement a session, an animation or learning cycle:- OI 4-1: Conduct a session or cycle using the techniques of mention or option,
  + OI 4-2: Master and enforce the rules of the mention or option,
  + OI 4-3: Ensure safe practice conditions.

###### Terms of access

For pre-required tests, the candidate must file a file with the training body responsible for organising them one month before the date set for the tests, which includes:

- A registration form with a photograph
- A photocopy of a valid piece of identification
- the certificate or certificates justifying the relief of certain tests set by the decree creating the specialty, the mention of the diploma, or the supplementary certificate concerned;
- a medical certificate of non-contradictory to the sport practice less than one year old, unless otherwise stipulated in the decree creating the specialty, mention or supplementary certificate concerned;
- for people with disabilities, the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need to develop pre-test tests as required in accord with the certification.

For registration in a training course, the applicant must file with the training organization, which monitors compliance, one month before the date set for entry into training a file that includes:

- A registration form with a photograph
- A photocopy of a valid piece of identification
- A copy of the census certificate or the individual certificate of participation in the defence and citizenship day;
- the certificate or certificates justifying the satisfaction of the prior requirements set by the decree creating the specialty, the mention of the diploma, or the supplementary certificate concerned;
- Documents justifying exemptions and equivalences of law;
- for registration for a supplementary certificate, a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- the or other documents provided for by the order creating the specialty, the mention, or the supplementary certificate concerned;
- for people with disabilities, the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need to arrange training or certificate tests as required according to the certification.

For the VAE file, it is advisable to refer to the[Articles A. 212-41 and the following of the Code of Sport](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031831233&cidTexte=LEGITEXT000006071318&dateTexte=20170119).

For more information, it is advisable to refer to the sheet corresponding to the discipline practiced.

*To go further* Articles A. 212-35 and the following from the Code of Sport.

#### Costs associated with qualification

Training leading to the PJEPS is paid for. The cost varies depending on the mentions and the training organizations.

For more details, it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Free Service Delivery)

European Union (EU) nationals* European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity there for at least the equivalent of a full-time year in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Free Establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

Three degrees. Conditions of honorability
-----------------------------------------

It is forbidden to practise as a sports educator in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

It's a four-degree one. Qualifications process and formalities
--------------------------------------------------------------

### a. Reporting obligation (for the purpose of obtaining the professional sports educator card)

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Time**

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

**Supporting documents**

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

**Cost**

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

**Competent authority**

The prior declaration of activity should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP) of the department where the declarer wants to perform his performance.

**Time**

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

**Supporting documents**

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  + A copy of a French-level certificate issued by a specialized institution,
  + a copy of a document attesting to professional experience acquired in France.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### c. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

**Competent authority**

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

**Time**

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

**Supporting documents for the first declaration of activity**

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  + A copy of a French-level certificate issued by a specialized institution,
  + A copy of a document attesting to professional experience acquired in France,
  + documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

**Evidence for a renewal of activity declaration**

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182, and Schedules II-12-2-a and II-12-b of the Code of Sport.

### d. Compensation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Time**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris, ([official website](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

