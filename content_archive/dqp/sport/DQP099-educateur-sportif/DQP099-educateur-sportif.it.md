﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP099" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Educatore sportivo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="educatore-sportivo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/it/dqp/sport/educatore-sportivo.html" -->
<!-- var(last-update)="2020-04-15 17:22:46" -->
<!-- var(url-name)="educatore-sportivo" -->
<!-- var(translation)="Auto" -->


Educatore sportivo
==================

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:22:46<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
(1) Definizione dell'attività
-----------------------------

L'educatore sportivo è un professionista che insegna, facilita o supervisiona un'attività fisica o sportiva. Allena i suoi praticanti. Lavora come occupazione principale o secondaria, di solito, stagionalmente o occasionalmente.

È in grado di mobilitare le conoscenze tecniche e pedagogiche specifiche per l'attività che supervisiona, a seconda del diploma che detiene. Egli padroneggia le tecniche della sua pratica in condizioni che garantiscono la sicurezza dei professionisti e dei terzi, nonché i comportamenti da osservare e le azioni da eseguire in caso di incidente o incidente.

L'educatore sportivo è responsabile della sicurezza del pubblico di cui è responsabile e dei terzi. Garantisce le buone condizioni dell'apparecchiatura utilizzata. Si adatta alle caratteristiche del suo pubblico: dà indicazioni e corregge gesti e posture per aiutarlo a progredire.

*Per andare oltre* Articolo L. 212-1 e R. 212-1 del Codice dello Sport.

Due gradi. Qualifiche professionali
-----------------------------------

### Requisiti nazionali

#### Legislazione nazionale

L'attività di educatore sportivo è subordinata all'applicazione dell'articolo L. 212-1 del Codice dello Sport che richiede l'ottenimento di certificazioni specifiche.

Come insegnante di sport, l'educatore sportivo deve avere un diploma, un titolo professionale o un certificato di qualificazione:

- garantire la propria competenza in termini di sicurezza dei professionisti e dei terzi nell'attività fisica o sportiva in esame;
- registrato al[elenco nazionale delle certificazioni professionali](http://www.rncp.cncp.gouv.fr/) (RNCP).

L'elenco dei diplomi, dei titoli professionali e dei certificati di qualificazione per esercitare la professione di educatore sportivo è[Appendice II-1 (articolo A. 212-1) del Codice dello Sport](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8231A63F40B1958AFA1DE7E71AB6ED79.tpdila09v_3?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000033170986&dateTexte=20170119&categorieLien=cid#LEGIARTI000033170986). Questo elenco specifica le condizioni e i limiti di esercizio applicabili ai titolari di ogni grado, in base alle norme specifiche per ogni attività.

Le qualifiche per la pratica come educatore sportivo sono:

- Il certificato professionale di gioventù, educazione popolare e sport (BPJEPS) specialità "educatore sportivo";
- il Diploma di Stato della Gioventù, l'Educazione Popolare e lo Sport (DEJEPS) e il Diploma di Stato della Gioventù, dell'Educazione Popolare e dello Sport (DESJEPS);
- Certificato di qualifica professionale (CQP). Gli esempi includono:- CQP "istruttore di volo piatto in galleria del vento"
  + CQP "Athletics Facilitator"
  + CQP "facilitatore di arrampicata strutturato artificialmente".

I diplomi stranieri possono essere ammessi in equivalenza ai diplomi francesi dal ministro responsabile dello sport, dopo il parere della Commissione per il riconoscimento delle qualifiche posto al ministro.

*Per andare oltre* Articoli L. 212-1 e successivi, R. 212-1 e oltre, R. 212-84, A. 212-1 e successivi, Appendice II-1 (Art. A. 212-1) del Codice dello Sport.

**Buono a sapersi**

Per alcune discipline, DEJEPS o DESJEPS sono talvolta tenuti ad operare in una maggiore autonomia, come per l'insegnamento e le immersioni. Per ulteriori informazioni, si consiglia di fare riferimento al foglio "Underwater Diving Monitor".

#### Formazione

##### BPJEPS specialità "educatore sportivo"

Il BPJEPS è un diploma di Stato registrato nel[RNCP (RNCP)](http://www.rncp.cncp.gouv.fr/) e classificati al livello IV della nomenclatura a livello di certificazione. Essa attesta l'acquisizione di una qualifica nell'esercizio di un'attività professionale in responsabilità a fini educativi o sociali, nei settori delle attività fisiche, sportive, socio-educative o culturali.

Il BPJEPS è rilasciato sotto la specialità "animatore" o la specialità "educatore sportivo" e una menzione disciplinare o multidisciplinare. Ogni menzione è fatta da un ordine che:

- definisce il repository professionale: è la presentazione del settore professionale, la descrizione del lavoro e il foglio di descrizione dell'attività;
- definisce il repository di certificazione, che è costituito da tutte le unità che compongono il diploma e stabilisce per ogni unità le competenze professionali, gli obiettivi intermedi di primo rango e le prove di certificato di questi Obiettivi
- specifica i requisiti di pre-formazione, i requisiti pre-professionali e le procedure di certificazione;
- Se necessario, stabilisce misure di equivalenza o di esenzione;
- se necessario, stabilisce le condizioni per verificare il mantenimento dei risultati professionali relativi alla sicurezza dei professionisti e dei terzi.

Il BPJEPS è emesso sia per mezzo di unità capitali (UC) o per convalida dell'esperienza (VAE), questi termini possono essere accumulati. Per ulteriori informazioni, è possibile[Sito ufficiale di VAE](http://www.vae.gouv.fr).

Questo diploma viene preparato sia attraverso la formazione iniziale, attraverso l'apprendimento o attraverso l'istruzione continua. Quando la formazione è completata come parte della formazione iniziale, la sua durata minima è di 900 ore, di cui 600 ore sono al centro.

Certificati aggiuntivi, che attestano competenze professionali che soddisfano una specifica esigenza e soddisfano gli stessi requisiti stabiliti per il diploma, possono essere associati alla specialità "sport educatore" PJEPS. Essi sono emessi alle stesse condizioni di quelli del diploma.

*Per andare oltre* Articoli da D. 212-20 a D. 212-31, da 212-47 ad A. 212-47-4 del Codice dello Sport.

###### Competenze professionali

Il BPJEPS è ottenuto con la maiuscola di quattro UC, due dei quali sono trasversali a tutte le specialità "sport educator" BPJEPS e due specifiche per la designazione scelta. Le quattro CC sono definite dalle seguenti competenze professionali e obiettivi intermedi (IO):

- UC 1: per supervisionare qualsiasi pubblico in qualsiasi luogo e struttura:- OI 1-1: comunicare in situazioni di vita professionale,
  + OI 1-2: tenendo conto delle caratteristiche del pubblico nei loro ambienti in un processo di educazione alla cittadinanza,
  + OI 1-3: contribuire al funzionamento di una struttura;
- UC 2: implementare un progetto di animazione come parte del progetto della struttura:- OI 2-1: progettazione di un progetto di animazione,
  + OI 2-1: condurre un progetto di animazione,
  + OI 2-3: valutare un progetto di animazione;
- UC 3: condurre una sessione, un'animazione o un ciclo di apprendimento nel campo della menzione:- OI 3-1: progettare la sessione, il ciclo di animazione o di apprendimento,
  + OI 3-2: guidare la sessione, il ciclo di animazione o di apprendimento,
  + OI 3-3: valutare la sessione, l'animazione o il ciclo di apprendimento;
- UC 4: mobilitare le tecniche di menzione o opzione per implementare una sessione, un ciclo di animazione o di apprendimento:- OI 4-1: Condurre una sessione o un ciclo utilizzando le tecniche di menzione o opzione,
  + OI 4-2: Padroneggia e applica le regole della menzione o dell'opzione,
  + OI 4-3: Garantire condizioni di sicurezza.

###### Condizioni di accesso

Per i test preliminari, il candidato deve presentare un fascicolo all'organismo di formazione incaricato di organizzarli un mese prima della data fissata per i test, che include:

- Un modulo di registrazione con una fotografia
- Una fotocopia di un documento di identità valido
- il certificato o i certificati che giustificano l'alleggerimento di talune prove stabilite dal decreto che crea la specialità, la menzione del diploma o il certificato supplementare in questione;
- un certificato medico non contraddittorio alla pratica sportiva di età inferiore a un anno, salvo diversa disposizione nel decreto che crea la specialità, la menzione o il certificato supplementare in questione;
- per le persone con disabilità, parere di un medico approvato dalla Federazione francese Handisport o dalla Federazione francese di sport adattato o nominato dal Comitato per i diritti e l'autonomia delle persone con disabilità sulla necessità sviluppare test pre-test come richiesto in accordo con la certificazione.

Per la registrazione a un corso di formazione, il richiedente deve presentare una domanda con l'organizzazione di formazione, che monitora la conformità, un mese prima della data impostata per l'immissione nella formazione di un file che include:

- Un modulo di registrazione con una fotografia
- Una fotocopia di un documento di identità valido
- Una copia del certificato di censimento o del certificato individuale di partecipazione alla giornata della difesa e della cittadinanza;
- il certificato o i certificati che giustificano la soddisfazione dei requisiti preventivi stabiliti dal decreto che crea la specialità, la menzione del diploma o il certificato supplementare in questione;
- Documenti che giustificano esenzioni ed equivalenze di diritto;
- per la registrazione di un certificato complementare, una fotocopia del diploma che autorizza la registrazione in formazione o un certificato di registrazione per la formazione che porta a tale diploma;
- i o altri documenti previsti dall'ordine che ha creato la specialità, la menzione o il certificato supplementare in questione;
- per le persone con disabilità, parere di un medico approvato dalla Federazione francese Handisport o dalla Federazione francese di sport adattato o nominato dal Comitato per i diritti e l'autonomia delle persone con disabilità sulla necessità per organizzare i test di formazione o di certificazione come richiesto in base alla certificazione.

Per il file VAE, si consiglia di fare riferimento alla[Articoli A. 212-41 e quanto segue del Codice dello Sport](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031831233&cidTexte=LEGITEXT000006071318&dateTexte=20170119).

Per ulteriori informazioni, si consiglia di fare riferimento al foglio corrispondente alla disciplina praticata.

*Per andare oltre* Articoli A. 212-35 e quanto segue dal Codice dello Sport.

#### Costi associati alla qualifica

La formazione che porta al PJEPS è pagata. Il costo varia a seconda delle menzioni e delle organizzazioni di formazione.

Per maggiori dettagli, si consiglia di avvicinarsi all'organizzazione di formazione in questione.

### b. Cittadini dell'UE: per esercizi olfatto temporaneo e occasionale (Consegna gratuita dei servizi)

Cittadini dell'Unione europea (UE)* Spazio economico europeo (AEA)* stabilita legalmente in uno di questi Stati può svolgere la stessa attività in Francia temporaneamente e occasionalmente a condizione che abbia inviato una precedente dichiarazione di attività al prefetto del dipartimento della consegna.

Se l'attività o la formazione che vi conduce non è regolamentata nello Stato membro di origine o nello stato del luogo di stabilimento, il cittadino deve anche giustificare l'aver svolto tale attività per almeno l'equivalente di un anno a tempo pieno nel negli ultimi dieci anni prima del beneficio.

I cittadini europei che desiderano esercitare in Francia in modo temporaneo o occasionale devono disporre delle competenze linguistiche necessarie per svolgere l'attività in Francia, in particolare al fine di garantire la sicurezza delle attività e la sua capacità di allertare i servizi di emergenza.

*Per andare oltre* Articoli Da L. 212-7 e Da R. 212-92 a R. 212-94 del Codice dello Sport.

### c. Cittadini dell'UE: per un esercizio permanente (Free Establishment)

Un cittadino di uno Stato dell'UE o del SEE può stabilirsi in Francia per praticare in modo permanente, se soddisfa una delle seguenti quattro condizioni:

**Se lo Stato membro d'origine disciplina l'accesso o l'esercizio dell'attività:**

- detenere un certificato di competenza o un certificato di formazione rilasciato dall'autorità competente di uno Stato dell'UE o del CEA che certifica un livello di qualificazione almeno equivalente al livello immediatamente inferiore a quello richiesto in Francia;
- essere titolare di un titolo acquisito in un terzo Stato e ammesso in equivalenza a uno Stato dell'UE o del CEA e giustificare l'aver svolto questa attività per almeno due anni a tempo pieno in tale Stato.

**Se lo Stato membro d'origine non disciplina l'accesso o l'esercizio dell'attività:**

- giustificare essere stato attivo in uno Stato dell'UE o del SEA, a tempo pieno per almeno due anni negli ultimi dieci anni, o, in caso di esercizio a tempo parziale, giustificando un'attività di durata equivalente e tenendo un certificato l'autorità competente di uno di questi Stati, che attesta una disponibilità per l'esercizio dell'attività, nonché un livello di qualificazione almeno equivalente al livello immediatamente inferiore a quello richiesto in Francia;
- essere il titolare di un certificato di qualificazione almeno equivalente al livello immediatamente inferiore a quello richiesto in Francia, rilasciato dall'autorità competente di uno Stato dell'UE o del CEA e sanzionare la formazione regolamentata in particolare l'esercizio di tutte o parte delle attività menzionate nell'articolo L. 212-1 del Codice dello Sport, costituito da un ciclo di studi integrato, se necessario, dalla formazione professionale, dal tirocinio o dalla pratica professionale.

Se il cittadino soddisfa una delle quattro condizioni di cui sopra, il requisito di qualificazione richiesto per esercitare è considerato soddisfatto.

Tuttavia, se le qualifiche professionali del cittadino differiscono sostanzialmente dalle qualifiche richieste in Francia che non garantirebbero la sicurezza dei professionisti e dei terzi, egli può essere tenuto a presentare un test attitudinale o il completamento di un corso di aggiustamento (vedi sotto "Buono a saper sapere: misure di compensazione").

Il cittadino deve avere la conoscenza della lingua francese necessaria per svolgere la sua attività in Francia, in particolare al fine di garantire la sicurezza delle attività fisiche e sportive e la sua capacità di allertare i servizi di emergenza.

*Per andare oltre* Articoli Da L. 212-7 e R. 212-88 a R. 212-90 del Codice dello Sport.

Tre gradi. Condizioni di onorabilità
------------------------------------

È vietato esercitarsi come educatore sportivo in Francia per le persone che sono state condannate per qualsiasi reato o per uno qualsiasi dei seguenti reati:

- tortura e atti di barbarie;
- Assalti sessuali;
- traffico di droga;
- Mettere in pericolo gli altri;
- pimping e i reati che ne derivano;
- minacciare i minori;
- uso illecito di sostanze o piante classificate come narcotici o provocazioni all'uso illecito di stupefacenti;
- violazioni degli articoli da L. 235-25 a L. 235-28 del Codice dello Sport;
- come punizione complementare a un reato fiscale: divieto temporaneo di esercitare, direttamente o per persona interposto, per conto proprio, di qualsiasi professione industriale, commerciale o liberale ( sezione 1750 del codice tributario generale).

Inoltre, nessuno può insegnare, facilitare o supervisionare un'attività fisica o sportiva con minori, se è stato oggetto di una misura amministrativa che gli vieta di partecipare, in qualsiasi veste, alla gestione e alla supervisione degli istituti e gli organismi legislativi o di regolamenti relativi alla protezione dei minori in un centro vacanze e di svago, nonché ai gruppi giovanili, o se è stato oggetto di una misura amministrativa per sospendere tali le stesse funzioni.

*Per andare oltre* Articolo L. 212-9 del Codice dello Sport.

È uno di quattro gradi. Processo e formalità delle qualifiche
-------------------------------------------------------------

### a. Obbligazione di segnalazione (ai fini dell'ottenimento della tessera di educatore sportivo professionale)

Chiunque desideri praticare una qualsiasi delle professioni disciplinate dall'articolo L. 212-1 del Codice dello Sport deve dichiarare la sua attività al prefetto del dipartimento del luogo in cui intende esercitarsi come principale. Questa dichiarazione attiva l'ottenimento di un biglietto da visita.

La dichiarazione deve essere rinnovata ogni cinque anni.

**Autorità competente**

La dichiarazione deve essere indirizzata alla Direzione dipartimentale della coesione sociale (DDCS) o alla direzione dipartimentale per la coesione sociale e la protezione della popolazione (DDCSPP) del dipartimento di pratica o dell'esercizio principale, o direttamente in linea sul[sito ufficiale](https://eaps.sports.gouv.fr).

**Tempo**

Entro un mese dalla presentazione del fascicolo di dichiarazione, la prefettura invia un riconoscimento al dichiarante. Il biglietto da visita, valido per cinque anni, viene quindi indirizzato al dichiarante.

**Documenti di supporto**

I documenti giustificativi da fornire sono:

- Cerfa 12699*02 ;
- Una copia di un ID valido
- Un documento d'identità con foto
- Una dichiarazione sull'onore che attesta l'esattezza delle informazioni nella forma;
- Una copia di ciascuno dei diplomi, titoli, certificati invocati;
- Una copia dell'autorizzazione alla pratica o, se necessario, l'equivalenza del diploma;
- un certificato medico non contraddittorio alla pratica e alla supervisione delle attività fisiche o sportive in questione, meno di un anno.

Se hai un rinnovo del reso, contatta:

- Modulo Cerfa 12699*02 ;
- Un documento d'identità con foto
- Una copia del certificato di revisione valido per le qualifiche soggette al requisito di riciclaggio;
- un certificato medico non contraddittorio alla pratica e alla supervisione delle attività fisiche o sportive in questione, meno di un anno.

Inoltre, in tutti i casi, la prefettura chiederà il rilascio di un estratto di meno di tre mesi dalla fedina penale del dichiarante per verificare che non vi sia alcuna disabilità o divieto di pratica.

**Costo**

Gratuito.

*Per andare oltre* Articoli L. 212-11, R. 212-85 e A. 212-176 a 212-178 del Codice dello Sport.

### b. Effettuare una predichiarazione dell'attività per i cittadini dell'UE impegnati in attività temporanee e occasionali (LPS)

I cittadini dell'UE o del SEA stabiliti legalmente in uno di questi Stati che desiderano esercitare in Francia su base temporanea o occasionale devono fare una dichiarazione preventiva di attività prima della prima prestazione di servizi.

Se il ricorrente desidera trarne un nuovo vantaggio in Francia, questa dichiarazione preventiva deve essere rinnovata.

Al fine di evitare gravi danni alla sicurezza dei beneficiari, il prefetto può, durante il primo beneficio, effettuare un controllo preliminare delle qualifiche professionali del richiedente.

**Autorità competente**

La precedente dichiarazione di attività deve essere indirizzata alla Direzione dipartimentale responsabile della coesione sociale (DDCS) o alla Direzione dipartimentale responsabile della coesione sociale e della protezione della popolazione (DDCSPP) del dipartimento in cui dichiarante vuole eseguire la sua performance.

**Tempo**

Entro un mese dalla ricezione del fascicolo di dichiarazione, il prefetto notifica al richiedente:

- o una richiesta di ulteriori informazioni (in questo caso, il prefetto ha due mesi per dare la sua risposta);
- o una ricevuta per una dichiarazione di erogazione del servizio se non effettua un controllo delle qualifiche. In questo caso, la fornitura del servizio può iniziare;
- o che stia conducendo il controllo delle qualifiche. In questo caso, il prefetto emette quindi al richiedente una ricevuta che gli consente di iniziare la sua prestazione o, se la verifica delle qualifiche rivela differenze sostanziali con le qualifiche professionali richieste in Francia, prefetto sottopone il richiedente a un test attitudino (vedi infra "Buono a sapersi: misure di compensazione").

In tutti i casi, in assenza di una risposta entro i termini summenzionati, il richiedente è considerato legalmente attivo in Francia.

**Documenti di supporto**

Il file di pre-segnalazione dell'attività deve includere:

- Una copia del modulo di dichiarazione fornito nel programma II-12-3 del Codice dello Sport;
- Un documento d'identità con foto
- Una copia di un ID
- Una copia del certificato di competenza o titolo di formazione;
- Una copia dei documenti che attestano che il dichiarante è legalmente stabilito nello Stato membro dell'istituzione e che non è incorrere in alcun divieto, nemmeno temporaneo, di esercitare (tradotto in francese da un traduttore certificato);
- nel caso in cui né l'attività né la formazione che conducono a questa attività sia regolamentata nello Stato membro dell'istituzione, una copia di tutti i documenti che giustifichino che il dichiarante ha svolto tale attività in tale Stato per almeno l'equivalente di due anni a tempo pieno negli ultimi dieci anni (tradotto in francese da un traduttore certificato);
- uno dei tre documenti tra cui scegliere (in caso contrario, si terrà un colloquio):- Una copia di un certificato di qualifica rilasciato dopo la formazione in francese,
  + Una copia di un certificato di livello francese rilasciato da un istituto specializzato,
  + una copia di un documento che attesta un'esperienza professionale acquisita in Francia.

**Costo**

Gratuito.

**Rimedi**

Eventuali contenziosi devono essere essuasi entro due mesi dalla notifica della decisione al giudice amministrativo competente.

*Per andare oltre* Articoli R. 212-92 e successivi, A. 212-182-2 e articoli successivi e Appendice II-12-3 del Codice dello Sport.

### c. Effettuare una predichiarazione di attività per i cittadini dell'UE per un esercizio permanente (LE)

Qualsiasi cittadino dell'UE o del SEE qualificato per svolgere tutte o parte delle attività menzionate nell'articolo L. 212-1 del codice dello sport e che desideri stabilirsi in Francia, deve fare una dichiarazione al prefetto del dipartimento in cui intende esercizio come principale.

Questa dichiarazione consente al dichiarante di ottenere una carta professionale e quindi di praticare legalmente in Francia alle stesse condizioni dei cittadini francesi.

La dichiarazione deve essere rinnovata ogni cinque anni.

In caso di differenza sostanziale rispetto alla qualifica richiesta in Francia, il prefetto può deferire il comitato di riconoscimento delle qualifiche al ministro dello sport per la consulenza. Possono anche decidere di sottoporre il cittadino a un test attitudino o a un corso di alloggio (vedi sotto: "Buono a sapersi: misure compensative").

**Autorità competente**

La dichiarazione deve essere indirizzata alla Direzione dipartimentale responsabile della coesione sociale (DDCS) o alla Direzione dipartimentale responsabile della coesione sociale e della protezione della popolazione (DDCSPP).

**Tempo**

La decisione del prefetto di rilasciare il biglietto da visita rientra in tre mesi dalla presentazione del fascicolo completo da parte del dichiarante. Tale termine può essere prorogato di un mese su decisione motivata. Se il prefetto decide di non emettere la carta professionale o di sottoporre il dichiarante a una misura di compensazione (prova di idoneità o stage), la sua decisione deve essere motivata.

**Documenti giustificativi per la prima dichiarazione di attività**

Il file di rapporto attività deve includere:

- Una copia del modulo di dichiarazione fornito nel programma II-12-2-a del Codice dello Sport;
- Un documento d'identità con foto
- Una copia di un ID valido
- un certificato medico non contraddittorio alla pratica e alla supervisione di attività fisiche o sportive, di età inferiore a un anno (tradotto da un traduttore certificato);
- Una copia del certificato di competenza o titolo formativo, accompagnata da documenti che descrivono il corso di formazione (programma, volume orario, natura e durata dei tirocini effettuati), tradotti in francese da un traduttore certificato;
- In tal caso, una copia di qualsiasi prova che giustifichi l'esperienza lavorativa (tradotta in francese da un traduttore certificato);
- Se il documento di formazione è stato ottenuto in un terzo Stato, copie dei documenti che attestano che il titolo è stato ammesso come equivalenza in uno Stato dell'UE o del SEA che regola l'attività;
- uno dei tre documenti tra cui scegliere (in caso contrario, si terrà un colloquio):- Una copia di un certificato di qualifica rilasciato dopo la formazione in francese,
  + Una copia di un certificato di livello francese rilasciato da un istituto specializzato,
  + Una copia di un documento che attesta l'esperienza professionale acquisita in Francia,
  + documenti che attestano che il dichiarante non è stato oggetto di nessuna delle condanne o delle misure di cui agli articoli L. 212-9 e L. 212-13 del codice dello sport (tradotto in francese da un traduttore certificato) nello Stato membro di origine.

**Prove di una dichiarazione di attività di rinnovo**

Il file di rinnovo dell'attività deve includere:

- Una copia del modulo di rinnovo del reso, modellato sul programma II-12-2-b del Codice dello Sport;
- Un documento d'identità con foto
- un certificato medico non contraddittorio alla pratica e alla supervisione di attività fisiche o sportive, meno di un anno.

**Costo**

Gratuito.

**Rimedi**

Eventuali contenziosi devono essere essuasi entro due mesi dalla notifica della decisione al giudice amministrativo competente.

*Per andare oltre* Articoli da R. 212-88 a R. 212-91, A. 212-182 e Orari II-12-2-a e II-12-b del Codice dello Sport.

### d. Misure di compensazione

Se vi è una differenza sostanziale tra la qualifica del richiedente e quella richiesta in Francia di svolgere la stessa attività, il prefetto fa riferimento alla commissione per il riconoscimento delle qualifiche, collocata al ministro responsabile dello sport. Questa commissione, dopo aver esaminato e indagato il file, rilascia, entro il mese del suo rinvio, un avviso che invia al prefetto.

A suo parere, la commissione può:

- ritiene che vi sia effettivamente una sostanziale differenza tra la qualifica del dichiarante e quella richiesta in Francia. In questo caso, la commissione propone di sottoporre il dichiarante a un test attitudinale o a un corso di adeguamento. Definisce la natura e le modalità precise di queste misure di compensazione (la natura delle prove, i termini della loro organizzazione e valutazione, il periodo di organizzazione, il contenuto e la durata del tirocinio, i tipi di strutture che possono ospitare il tirocinante, ecc.) ;
- che non vi è alcuna differenza sostanziale tra la qualifica del dichiarante e quella richiesta in Francia.

Al ricevimento del parere della commissione, il prefetto notifica al dichiarante la sua decisione motivata (non è obbligato a seguire il parere della commissione):

- Se il dichiarante richiede un indennizzo, ha un mese di tempo per scegliere tra il test attitudine e il corso di alloggio. Il prefetto rilascia quindi un biglietto da visita al dichiarante che ha rispettato le misure di compensazione. D'altra parte, se il corso o il test attitudine non è soddisfacente, il prefetto notifica la sua decisione motivata di rifiutare di emettere la carta professionale alla persona interessata;
- se non ha bisogno di un risarcimento, il prefetto rilascia un biglietto da visita alla persona interessata.

*Per andare oltre* Articoli R. 212-84 e D. 212-84-1 del Codice dello Sport.

### e. Rimedi

#### Centro di assistenza francese

Il Centro ENIC-NARIC è il centro francese per informazioni sul riconoscimento accademico e professionale dei diplomi.

#### Solvit

SOLVIT è un servizio fornito dall'Amministrazione nazionale di ogni Stato membro dell'Unione europea o da parte dell'accordo DEL TESORo. Il suo obiettivo è quello di trovare una soluzione a una controversia tra un cittadino dell'UE e l'amministrazione di un altro di questi Stati. SOLVIT interviene in particolare nel riconoscimento delle qualifiche professionali.

**Condizioni**

L'interessato può utilizzare SOLVIT solo se stabilisce:

- che la pubblica amministrazione di uno Stato dell'UE non ha rispettato i propri diritti ai sensi del diritto dell'UE in qualità di cittadino o di un'altra impresa di un altro Stato dell'UE;
- che non ha già avviato un'azione legale (l'azione amministrativa non è considerata come tale).

**Procedura**

Il cittadino deve completare un [modulo di reclamo online](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una volta che il suo fascicolo è stato inviato, SOLVIT lo contatta entro una settimana per richiedere, se necessario, ulteriori informazioni e per verificare che il problema rientri nelle sue competenze.

Alla fine del periodo di 10 settimane, SOLVIT presenta una soluzione:

- Se questa soluzione risolve la controversia sull'applicazione del diritto europeo, la soluzione viene accettata e il caso viene chiuso;
- in caso di soluzione, il caso viene chiuso come irrisolto e deferito alla Commissione europea.

**Documenti di supporto**

Per entrare in SOLVIT, il cittadino deve comunicare:

- Dati di contatto completi
- Descrizione dettagliata del suo problema
- tutte le prove nel fascicolo (ad esempio, corrispondenza e decisioni ricevute dall'autorità amministrativa competente).

**Tempo**

SOLVIT si impegna a trovare una soluzione entro dieci settimane il caso è stato rilevato dal centro SOLVIT nel paese in cui si è verificato il problema.

**Costo**

Gratuito.

**Ulteriori informazioni**

SOLVIT in Francia: Segretariato generale per gli affari europei, 68 rue de Bellechasse, 75700 Parigi, ([sito ufficiale](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

