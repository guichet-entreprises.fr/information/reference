﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP099" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Esporte" -->
<!-- var(title)="Educador esportivo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="esporte" -->
<!-- var(title-short)="educador-esportivo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/pt/dqp/esporte/educador-esportivo.html" -->
<!-- var(last-update)="2020-04-28 17:36:38" -->
<!-- var(url-name)="educador-esportivo" -->
<!-- var(translation)="Auto" -->


Educador esportivo
==================

Última atualização: : <!-- begin-var(last-update) -->2020-04-28 17:36:38<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
(1) Definindo a atividade
-------------------------

O educador esportivo é um profissional que ensina, facilita ou supervisiona uma atividade física ou esportiva. Ele treina seus praticantes. Ele trabalha como diretor ou ocupação secundária, geralmente, sazonalmente ou ocasionalmente.

Ele é capaz de mobilizar os conhecimentos técnicos e pedagógicos específicos para a atividade que supervisiona, dependendo do diploma que possui. Ele domina as técnicas de sua prática em condições que garantam a segurança dos praticantes e terceiros, bem como os comportamentos a serem observados e as ações a serem realizadas em caso de incidente ou acidente.

O educador esportivo é responsável pela segurança do público pelo qual é responsável e dos terceiros. Garante as boas condições do equipamento utilizado. Ele se adapta às características de seu público: dá instruções e corrige gestos e posturas para ajudá-lo a progredir.

*Para ir mais longe* Artigos L. 212-1 e R. 212-1 do Código do Esporte.

Dois graus. Qualificações profissionais
---------------------------------------

### Requisitos nacionais

#### Legislação nacional

A atividade de educador esportivo está sujeita à aplicação do artigo 212-1 do Código Esportivo, que exige a obtenção de certificações específicas.

Como professor de esportes, o educador esportivo deve ter um diploma, um título profissional ou um certificado de qualificação:

- garantir sua competência em termos de segurança de praticantes e terceiros na atividade física ou esportiva em estudo;
- gravado no[diretório nacional de certificações profissionais](http://www.rncp.cncp.gouv.fr/) (RNCP).

A lista de diplomas, títulos profissionais e certificados de qualificação para exercer a prática como educador esportivo é elaborada em[Apêndice II-1 (Artigo A. 212-1) do Código do Esporte](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8231A63F40B1958AFA1DE7E71AB6ED79.tpdila09v_3?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000033170986&dateTexte=20170119&categorieLien=cid#LEGIARTI000033170986). Esta lista especifica as condições e os limites de exercício aplicáveis aos titulares de cada grau, de acordo com as normas específicas para cada atividade.

As qualificações para exercer como educador esportivo são:

- O certificado profissional de juventude, educação popular e esporte (BPJEPS) especialidade "educador esportivo";
- o Diploma Estadual da Juventude, Educação Popular e Esporte (DEJEPS) e o Diploma Estadual de Juventude, Educação Popular e Esporte (DESJEPS);
- Certificado de Qualificação Profissional (CQP). Exemplos incluem:- CQP "instrutor de vôo plano em túnel de vento"
  + CQP "Facilitador de Atletismo"
  + CQP "facilitador de escalada artificialmente estruturado".

Os diplomas estrangeiros podem ser admitidos em equivalência aos diplomas franceses pelo Ministro responsável pelo esporte, após o parecer da Comissão para o Reconhecimento das Qualificações colocado com o Ministro.

*Para ir mais longe* Artigos L. 212-1 e Seguinte, R. 212-1 e Além, R. 212-84, A. 212-1 e seguinte, Apêndice II-1 (Art. A. 212-1) do Código do Esporte.

**É bom saber**

Para algumas disciplinas, o DEJEPS ou o DESJEPS são, por vezes, obrigados a operar com maior autonomia, como para o ensino e o mergulho. Para obter mais informações, é aconselhável consultar a folha "Underwater Diving Monitor".

#### Treinamento

##### BPJEPS especialidade "educador esportivo"

O BPJEPS é um diploma estadual registrado no[RNCP](http://www.rncp.cncp.gouv.fr/) e classificada no nível IV da nomenclatura de nível de certificação. Atesta a aquisição de qualificação no exercício de atividade profissional responsável por fins educativos ou sociais, nas áreas de atividades físicas, esportivas, socioeducativas ou culturais.

O BPJEPS é emitido a especialidade animador ou a especialidade educador esportivo e uma menção disciplinar ou multidisciplinar. Cada menção é feita por uma ordem que:

- define o repositório profissional: é a apresentação do setor profissional, a descrição do trabalho e a ficha de descrição da atividade;
- define o repositório de certificação, que é composto por todas as unidades que compõem o diploma e define para cada unidade as habilidades profissionais, os objetivos intermediários do primeiro escalão e os testes de certificado destes Objetivos
- especifica requisitos de pré-treinamento, requisitos pré-profissionais e procedimentos de certificado;
- Se necessário, estabelece medidas de equivalência ou isenção;
- se necessário, estabelece condições para verificar a manutenção de realizações profissionais relacionadas à segurança dos praticantes e terceiros.

O BPJEPS é emitido por meio de unidades capitalizadas (UC) ou por validação de experiência (VAE), estes termos podem ser acumulados. Para mais informações, você pode ver[Site oficial da VAE](http://www.vae.gouv.fr).

Este diploma é preparado tanto através da formação inicial, através da aprendizagem ou através da educação continuada. Quando o treinamento é concluído como parte do treinamento inicial, sua duração mínima é de 900 horas, das quais 600 horas estão no centro.

Certificados adicionais, que atestam habilidades profissionais que atendam a uma necessidade específica e atendam aos mesmos requisitos estabelecidos para o diploma, podem estar associados à especialidade "educador esportivo" PJEPS. Eles são emitidos as mesmas condições que os do diploma.

*Para ir mais longe* Artigos D. 212-20 a D. 212-31, A. 212-47 a A. 212-47-4 do Código do Esporte.

###### Habilidades profissionais

O BPJEPS é obtido por capitalização de quatro UC, duas das quais são transversais para todas as especialidades "educadora esportiva" BPJEPS e duas específicas para a designação escolhida. As quatro UCs são definidas pelas seguintes habilidades profissionais e objetivos intermediários (IOs):

- UC 1: supervisionar qualquer público em qualquer lugar e estrutura:- OI 1-1: comunicação em situações da vida profissional,
  + OI 1-2: levando em conta as características do público em seus ambientes em um processo de educação de cidadania,
  + OI 1-3: contribuindo para o funcionamento de uma estrutura;
- UC 2: implementar um projeto de animação como parte do projeto da estrutura:- OI 2-1: projetando um projeto de animação,
  + OI 2-1: conduzir um projeto de animação,
  + OI 2-3: avaliar um projeto de animação;
- UC 3: realizar uma sessão, um ciclo de animação ou aprendizagem no campo da menção:- OI 3-1: projete a sessão, a animação ou o ciclo de aprendizagem,
  + OI 3-2: lidere a sessão, o ciclo de animação ou aprendizado,
  + OI 3-3: avaliar a sessão, a animação ou o ciclo de aprendizagem;
- UC 4: mobilizar as técnicas de menção ou opção para implementar uma sessão, um ciclo de animação ou aprendizagem:- OI 4-1: Realizar uma sessão ou ciclo usando as técnicas de menção ou opção,
  + OI 4-2: Dominar e impor as regras da menção ou opção,
  + OI 4-3: Assegure condições seguras de prática.

###### Termos de acesso

Para as provas pré-requeridas, o candidato deverá apresentar um arquivo junto ao órgão de treinamento responsável por organizá-los um mês antes da data definida para as provas, que inclui:

- Um formulário de registro com uma fotografia
- Uma fotocópia de uma peça de identificação válida
- o certificado ou certificados que justifiquem a isenção de determinados testes estabelecidos pelo decreto que cria a especialidade, a menção do diploma ou o certificado suplementar em causa;
- um atestado médico de não contraditório à prática esportiva com menos de um ano de idade, salvo estipulado de outra forma no decreto que cria a especialidade, menção ou certificado complementar em causa;
- para pessoas com deficiência, o parecer de um médico aprovado pela Federação Francesa de Handisport ou pela Federação Francesa de Esporte Adaptado ou nomeado pelo Comitê dos Direitos e Autonomia das Pessoas com Deficiência sobre a necessidade para desenvolver testes pré-teste conforme necessário de acordo com a certificação.

Para a inscrição em um curso de treinamento, o candidato deve se apresentar à organização de treinamento, que monitora o cumprimento, um mês antes da data definida para a entrada no treinamento de um arquivo que inclui:

- Um formulário de registro com uma fotografia
- Uma fotocópia de uma peça de identificação válida
- Cópia do certificado censitário ou do certificado individual de participação no dia da defesa e da cidadania;
- o certificado ou certificados que justifiquem a satisfação dos requisitos prévios estabelecidos pelo decreto que cria a especialidade, a menção do diploma ou o certificado suplementar em causa;
- Documentos que justifiquem isenções e equivalências da lei;
- para inscrição de certificado complementar, fotocópia do diploma autorizando a inscrição em formação ou certificado de registro para a formação que leva a esse diploma;
- os ou outros documentos previstos na ordem de criação da especialidade, da menção ou do certificado complementar em causa;
- para pessoas com deficiência, o parecer de um médico aprovado pela Federação Francesa de Handisport ou pela Federação Francesa de Esporte Adaptado ou nomeado pelo Comitê dos Direitos e Autonomia das Pessoas com Deficiência sobre a necessidade para organizar testes de treinamento ou certificado, conforme necessário, de acordo com a certificação.

Para o arquivo VAE, é aconselhável consultar o[Artigos A. 212-41 e o seguinte do Código do Esporte](https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000031831233&cidTexte=LEGITEXT000006071318&dateTexte=20170119).

Para mais informações, é aconselhável consultar a folha correspondente à disciplina praticada.

*Para ir mais longe* Artigos A. 212-35 e o seguinte do Código do Esporte.

#### Custos associados à qualificação

O treinamento que leva ao PJEPS é pago. O custo varia dependendo das menções e das organizações de treinamento.

Para mais detalhes, é aconselhável aproximar-se da organização de treinamento em questão.

### b. Cidadãos da UE: para exercícios temporários e ocasionais (Free Service Delivery)

Cidadãos da União Europeia (UE)* Área Econômica Europeia (EEE)* legalmente estabelecida em um desses estados pode realizar a mesma atividade na França de forma temporária e ocasional, na condição de ter enviado uma declaração prévia de atividade ao prefeito do departamento de entrega.

Se a atividade ou treinamento que leva a lá não for regulamentada no Estado-Membro de origem ou no estado do local de estabelecimento, o nacional também deve justificar ter realizado essa atividade lá pelo menos o equivalente a um ano integral no nos últimos dez anos antes do benefício.

Os cidadãos europeus que desejam praticar na França de forma temporária ou ocasional devem ter as habilidades linguísticas necessárias para realizar a atividade na França, em particular, a fim de garantir a segurança das atividades e sua capacidade de alertar os serviços de emergência.

*Para ir mais longe* Artigos L. 212-7 e R. 212-92 a R. 212-94 do Código de Esporte.

### c. Cidadãos da UE: para um exercício permanente (Estabelecimento Livre)

Um cidadão de um estado da UE ou da EEE pode se estabelecer na França para praticar permanentemente, se ele cumprir uma das quatro condições seguintes:

**Se o Estado-Membro de origem regulamentar o acesso ou o exercício da atividade:**

- possuir um certificado de competência ou um certificado de treinamento emitido pela autoridade competente de um estado da UE ou da EEE que certifica um nível de qualificação pelo menos equivalente ao nível imediatamente inferior ao exigido na França;
- ser o detentor de um título adquirido em um terceiro estado e admitido em equivalência a um estado da UE ou EEE e justificar ter realizado esta atividade por pelo menos dois anos em tempo integral naquele estado.

**Se o Estado-Membro de origem não regular o acesso ou o exercício da atividade:**

- justificar ter sido ativo em um estado da UE ou EEE, em tempo integral por pelo menos dois anos nos últimos dez anos, ou, no caso de exercício de meio período, justificando uma atividade de duração equivalente e segurando um certificado qualificação emitida pela autoridade competente de um desses estados, que atesta uma prontidão para o exercício da atividade, bem como um nível de qualificação pelo menos equivalente ao nível imediatamente inferior ao exigido em França;
- ser detentor de um certificado de qualificação pelo menos equivalente ao nível imediatamente inferior ao exigido na França, emitido pela autoridade competente de um estado da UE ou eEE e sancionando o treinamento regulamentado destinado a especificamente o exercício de toda ou parte das atividades mencionadas no artigo L. 212-1 do Código esportivo, que consiste em um ciclo de estudo complementado, se necessário, por formação profissional, estágio ou prática profissional.

Se o nacional atender a uma das quatro condições mencionadas acima, o requisito de qualificação exigido para a prática é considerado satisfeito.

No entanto, se as qualificações profissionais do nacional diferem substancialmente das qualificações exigidas na França que não garantiriam a segurança dos praticantes e terceiros, ele pode ser obrigado a submeter-se a um teste de aptidão ou conclusão de um curso de ajuste (veja abaixo "Bom saber: medidas de compensação").

O nacional deve ter o conhecimento da língua francesa necessário para realizar sua atividade na França, em particular, a fim de garantir a segurança das atividades físicas e esportivas e sua capacidade de alertar os serviços de emergência.

*Para ir mais longe* Artigos L. 212-7 e R. 212-88 a R. 212-90 do Código de Esporte.

Três graus. Condições de honorabilidade
---------------------------------------

É proibido praticar como educador esportivo na França para pessoas que tenham sido condenadas por qualquer crime ou por qualquer um dos seguintes crimes:

- tortura e atos de barbárie;
- agressões sexuais;
- tráfico de drogas;
- Colocando em perigo os outros;
- e as ofensas resultantes;
- colocando em perigo menores;
- uso ilícito de substância ou planta classificada como narcóticos ou provocação ao uso ilícito de entorpecentes;
- violações dos artigos L. 235-25 a L. 235-28 do Código esportivo;
- como punição complementar a um delito tributário: proibição temporária de praticar, diretamente ou por pessoa interposta, em nome de si mesmo ou de outros, qualquer profissão industrial, comercial ou liberal ( Seção 1750 do Código Tributário Geral).

Além disso, ninguém pode ensinar, facilitar ou supervisionar uma atividade física ou esportiva com menores, caso tenha sido objeto de uma medida administrativa que o proíba de participar, em qualquer função, na gestão e supervisão das instituições e órgãos sujeitos à legislação ou regulamentos relativos à proteção de menores em um centro de férias e lazer, bem como grupos de jovens, ou se foi objeto de uma medida administrativa para suspender estes mesmas funções.

*Para ir mais longe* Artigo 212-9 do Código do Esporte.

É um de quatro graus. Processo de qualificação e formalidades
-------------------------------------------------------------

### a. Obrigação de notificação (com a finalidade de obter o cartão de educador esportivo profissional)

Quem quiser exercer qualquer uma das profissões regidas pelo artigo L. 212-1 do Código do Esporte deve declarar sua atividade ao prefeito do departamento do local onde pretende exercer como diretor. Esta declaração desencadeia a obtenção de um cartão de visita.

A declaração deve ser renovada a cada cinco anos.

**Autoridade competente**

A declaração deve ser endereçada à Diretoria Departamental de Coesão Social (DDCS) ou Diretoria Departamental de Coesão Social e Proteção populacional (DDCSPP) do departamento de prática ou ao exercício principal, ou diretamente em linha no[site oficial](https://eaps.sports.gouv.fr).

**Tempo**

Dentro de um mês após a apresentação do arquivo da declaração, a prefeitura envia um reconhecimento ao inscrito. O cartão de visita, válido por cinco anos, é então endereçado ao inscrito.

**Documentos de suporte**

Os documentos de suporte a serem fornecidos são:

- Cerfa 12699*02 ;
- Uma cópia de um ID válido
- Um documento de identificação com foto
- Uma declaração sobre a honra atestando a exatidão das informações no formulário;
- Uma cópia de cada um dos diplomas, títulos, certificados invocados;
- Cópia da autorização para a prática, ou, se necessário, da equivalência do diploma;
- atestado médico de não contraditório à prática e supervisão das atividades físicas ou esportivas em causa, menor de um ano de idade.

Se você tiver uma renovação de retorno, você deve entrar em contato:

- Formulário Cerfa 12699*02 ;
- Um documento de identificação com foto
- Cópia do certificado de revisão válido para qualificações sujeitas ao requisito de reciclagem;
- atestado médico de não contraditório à prática e supervisão das atividades físicas ou esportivas em causa, menor de um ano de idade.

Além disso, em todos os casos, a própria prefeitura solicitará a liberação de um extrato de menos de três meses da ficha criminal do inscrito para verificar se não há incapacidade ou proibição de prática.

**Custo**

Livre.

*Para ir mais longe* Artigos L. 212-11, R. 212-85 e A. 212-176 a A. 212-178 do Código esportivo.

### b. Fazer uma pré-declaração de atividade para os cidadãos da UE envolvidos em atividades temporárias e ocasionais (LPS)

Os cidadãos da UE ou da EEE legalmente estabelecidos em um desses Estados que desejam praticar na França de forma temporária ou ocasional devem fazer uma declaração prévia de atividade antes da primeira prestação de serviços.

Se o requerente deseja fazer um novo benefício na França, esta declaração prévia deve ser renovada.

Para evitar danos graves à segurança dos beneficiários, o prefeito poderá, durante o primeiro benefício, realizar uma verificação preliminar das qualificações profissionais do requerente.

**Autoridade competente**

A declaração prévia de atividade deve ser dirigida à Diretoria Departamental responsável pela Coesão Social (DDCS) ou à Diretoria Departamental responsável pela Coesão Social e Proteção populacional (DDCSPP) do departamento onde o declarer quer realizar sua performance.

**Tempo**

Dentro de um mês após o recebimento do arquivo da declaração, o prefeito notifica o requerente:

- ou um pedido de mais informações (neste caso, o prefeito tem dois meses para dar sua resposta);
- ou um recibo de uma declaração de prestação de serviços se ele não realizar uma verificação de qualificação. Neste caso, a prestação de serviços pode começar;
- ou que está conduzindo a verificação de qualificações. Neste caso, o prefeito emite então ao requerente um recibo que lhe permite iniciar sua performance ou, se a verificação das qualificações revelar diferenças substanciais com as qualificações profissionais exigidas na França, o prefeito submete o requerente a um teste de aptidão (ver infra "Bom saber: medidas de compensação").

Em todos os casos, na ausência de uma resposta dentro dos prazos acima mencionados, o requerente é considerado legalmente ativo na França.

**Documentos de suporte**

O arquivo de pré-relatório de atividades deve incluir:

- Cópia do formulário de declaração previsto no Calendário II-12-3 do Código do Esporte;
- Um documento de identificação com foto
- Uma cópia de uma id
- Cópia do certificado de competência ou título de treinamento;
- Uma cópia dos documentos atestando que o inscrito está legalmente estabelecido no Estado-Membro da instituição e que não incorre em qualquer proibição, mesmo temporária, de praticar (traduzido para o francês por um tradutor certificado);
- no caso de que nem a atividade nem o treinamento que leve a esta atividade sejam regulamentados no Estado-Membro do Estabelecimento, uma cópia de quaisquer documentos que justifiquem que o inscrito tenha realizado essa atividade naquele Estado por pelo menos o equivalente a dois anos tempo integral ao longo dos últimos dez anos (traduzido para o francês por um tradutor certificado);
- um dos três documentos para escolher (se não, uma entrevista será realizada):- Uma cópia de um certificado de qualificação emitido após o treinamento em francês,
  + Uma cópia de um certificado de nível francês emitido por uma instituição especializada,
  + uma cópia de um documento atestando a experiência profissional adquirida na França.

**Custo**

Livre.

**Remédios**

Qualquer litígio deve ser exercido no prazo de dois meses após a notificação da decisão ao tribunal administrativo competente.

*Para ir mais longe* Artigos R. 212-92 e seguintes, A. 212-182-2 e artigos subsequentes e Apêndice II-12-3 do Código do Esporte.

### c. Faça uma pré-declaração de atividade para os cidadãos da UE para um exercício permanente (LE)

Qualquer cidadão da UE ou da EEE qualificado para realizar toda ou parte das atividades mencionadas no artigo L. 212-1 do Código do Esporte, e desejando se estabelecer na França, deve fazer uma declaração ao prefeito do departamento no qual ele pretende exercer como um diretor.

Esta declaração permite ao inscrito obter um cartão profissional e, assim, praticar legalmente na França as mesmas condições que os franceses.

A declaração deve ser renovada a cada cinco anos.

No caso de uma diferença substancial da qualificação exigida na França, o prefeito pode encaminhar o comitê de reconhecimento de qualificações ao Ministro do Esporte para aconselhamento. Eles também podem decidir submeter o nacional a um teste de aptidão ou um curso de acomodação (veja abaixo: "Bom saber: medidas de compensação").

**Autoridade competente**

A declaração deve ser endereçada à Diretoria Departamental responsável pela Coesão Social (DDCS) ou à Diretoria Departamental responsável pela Coesão Social e Proteção populacional (DDCSPP).

**Tempo**

A decisão do prefeito de emitir o cartão de visita vem dentro de três meses após a apresentação do arquivo completo pelo inscrito. Esse prazo pode ser prorrogado por um mês por decisão fundamentada. Caso o prefeito decida não emitir o cartão profissional ou submeter o declarante a uma medida de compensação (teste de aptidão ou estágio), sua decisão deve ser motivada.

**Documentos de apoio para a primeira declaração de atividade**

O arquivo de relatório de atividades deve incluir:

- Cópia do formulário de declaração fornecido no Calendário II-12-2-a do Código do Esporte;
- Um documento de identificação com foto
- Uma cópia de um ID válido
- atestado médico de não contraditório à prática e supervisão de atividades físicas ou esportivas, menor de um ano de idade (traduzido por um tradutor certificado);
- Cópia do certificado de competência ou título de formação, acompanhado de documentos que descrevem o curso de formação (programa, volume por hora, natureza e duração dos estágios realizados), traduzido para o francês por um tradutor certificado;
- Se assim for, uma cópia de qualquer evidência que justifique a experiência de trabalho (traduzida para o francês por um tradutor certificado);
- Se o documento de treinamento tiver sido obtido em um terceiro estado, cópias dos documentos que comem que o título foi admitido como equivalência em um estado da UE ou eEE que regula a atividade;
- um dos três documentos para escolher (se não, uma entrevista será realizada):- Uma cópia de um certificado de qualificação emitido após o treinamento em francês,
  + Uma cópia de um certificado de nível francês emitido por uma instituição especializada,
  + Uma cópia de um documento atestando a experiência profissional adquirida na França,
  + documentos que comam que o inscrito não foi objeto de nenhuma das condenações ou medidas referidas nos artigos L. 212-9 e L. 212-13 do Código do Esporte (traduzido para o francês por um tradutor certificado) no Estado-Membro de origem.

**Evidência para renovação da declaração de atividade**

O arquivo de renovação de atividade deve incluir:

- Cópia do formulário de renovação de retorno, modelado no Calendário II-12-2-b do Código do Esporte;
- Um documento de identificação com foto
- atestado médico de não contraditório à prática e supervisão de atividades físicas ou esportivas, menor de um ano de idade.

**Custo**

Livre.

**Remédios**

Qualquer litígio deve ser exercido no prazo de dois meses após a notificação da decisão ao tribunal administrativo competente.

*Para ir mais longe* Os artigos R. 212-88 a R. 212-91, A. 212-182 e Horários II-12-2-a e II-12-b do Código do Esporte.

### d. Medidas de compensação

Se houver uma diferença substancial entre a qualificação do requerente e a exigida na França para realizar a mesma atividade, o prefeito refere-se à comissão de reconhecimento de qualificações, colocada com o Ministro encarregado do esporte. Esta comissão, após analisar e investigar o arquivo, emite, no mês de seu encaminhamento, um aviso que envia ao prefeito.

Em seu parecer, a comissão pode:

- acreditam que há, de fato, uma diferença substancial entre a qualificação do inscrito e a necessária na França. Neste caso, a comissão propõe submeter o inscrito a um teste de aptidão ou um curso de ajuste. Define a natureza e as modalidades precisas dessas medidas de compensação (a natureza dos testes, os termos de sua organização e avaliação, o período de organização, o conteúdo e a duração do estágio, os tipos de estruturas que podem acomodar o estagiário, etc.) ;
- que não há diferença substancial entre a qualificação do inscrito e a necessária na França.

Após o recebimento do parecer da comissão, o prefeito notifica o inscrito de sua decisão fundamentada (ele não é obrigado a seguir o conselho da comissão):

- Se o inscrito exigir indenização, ele tem um mês para escolher entre o teste de aptidão e o curso de acomodação. O prefeito, então, emite um cartão de visita ao registrante que cumpriu as medidas de compensação. Por outro lado, se o curso ou o teste de aptidão não for satisfatório, o prefeito notifica sua decisão fundamentada de recusar a emissão do cartão profissional ao interessado;
- se ele não exigir indenização, o prefeito emite um cartão de visita ao interessado.

*Para ir mais longe* Artigos R. 212-84 e D. 212-84-1 do Código do Esporte.

### e. Remédios

#### Centro de assistência francês

O Centro ENIC-NARIC é o centro francês de informações sobre o reconhecimento acadêmico e profissional de diplomas.

#### Solvit

O SOLVIT é um serviço prestado pela Administração Nacional de cada Estado-Membro da União Europeia ou parte do acordo eEE. Seu objetivo é encontrar uma solução para uma disputa entre um nacional da UE e a administração de outro desses Estados. A SOLVIT intervém em particular no reconhecimento das qualificações profissionais.

**Condições**

O interessado só pode usar o SOLVIT se estabelecer:

- que a administração pública de um Estado da UE não respeitou seus direitos o direito da UE como cidadão ou negócio de outro Estado da UE;
- que ainda não iniciou ação judicial (ação administrativa não é considerada como tal).

**Procedimento**

O nacional deve completar um [formulário de reclamação on-line](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Uma vez que seu arquivo tenha sido apresentado, a SOLVIT entra em contato com ele dentro de uma semana para solicitar, se necessário, informações adicionais e verificar se o problema está dentro de sua competência.

Ao final do período de 10 semanas, a SOLVIT apresenta uma solução:

- Se essa solução resolver a disputa sobre a aplicação do direito europeu, a solução será aceita e o caso será encerrado;
- se não houver solução, o caso é encerrado como não resolvido e encaminhado à Comissão Europeia.

**Documentos de suporte**

Para entrar no SOLVIT, o nacional deve comunicar:

- Detalhes completos de contato
- Descrição detalhada de seu problema
- todas as provas nos autos (por exemplo, correspondências e decisões recebidas da autoridade administrativa competente).

**Tempo**

A SOLVIT está empenhada em encontrar uma solução dentro de dez semanas a partir do dia em que o caso foi assumido pelo centro SOLVIT no país em que o problema ocorreu.

**Custo**

Livre.

**Mais informações**

SOLVIT na França: Secretaria Geral para Assuntos Europeus, 68 rue de Bellechasse, 75700 Paris, ([site oficial](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

