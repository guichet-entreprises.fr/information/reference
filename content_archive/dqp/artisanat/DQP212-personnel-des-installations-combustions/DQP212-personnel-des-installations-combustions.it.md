﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP212" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artigianato" -->
<!-- var(title)="Personale presso impianti di combustione superiori a 400 kW" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artigianato" -->
<!-- var(title-short)="personale-presso-impianti-di-combustione" -->
<!-- var(url)="https://www.guichet-qualifications.fr/it/dqp/artigianato/personale-presso-impianti-di-combustione-superiori-a-400-kw.html" -->
<!-- var(last-update)="2020-04-15 17:20:35" -->
<!-- var(url-name)="personale-presso-impianti-di-combustione-superiori-a-400-kw" -->
<!-- var(translation)="Auto" -->


Personale presso impianti di combustione superiori a 400 kW
===========================================================

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:20:35<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

Il personale degli impianti di combustione di cui sopra i 400 Kilowatt (kW) è un professionista che si occupa dell'installazione, della manutenzione e del funzionamento di un impianto (di solito una caldaia) utilizzando un prodotto combustibile ai fini della produzione di energia elettrica e potenza nominale superiore a 400 kW. Questi combustibili possono essere liquidi, gassosi, carbone o lignite.

Durante questa attività, il professionista è tenuto a garantire che l'impianto mantenga buone prestazioni energetiche.

Due gradi. Qualifiche professionali
-----------------------------------

### a. Requisiti nazionali

#### Legislazione nazionale

Per questa attività non sono richieste qualifiche professionali, tuttavia, è richiesto il responsabile dell'installazione, della manutenzione e del funzionamento di queste caldaie:

- Calcolo dei resi delle attrezzature
- Effettuare controlli periodici dell'efficienza energetica del dispositivo;
- per mantenere aggiornato un libro delle caldaie.

**Rendimenti**

Il professionista deve calcolare le prestazioni della caldaia per la quale è responsabile della caldaia al momento di ogni riavvio e almeno ogni tre mesi durante il periodo di funzionamento. Queste rese variano a seconda della data in cui il dispositivo viene commissionato e del fluido di trasporto del calore utilizzato.

Pertanto, deve garantire che la caldaia soddisfi i valori minimi impostati:

- 14 settembre 1998, entro il[tavolo](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837152&cidTexte=LEGITEXT000006074220&dateTexte=20180426) Articolo R. 224-23 del codice ambiente;
- 14 settembre 1998[tavolo](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837153&cidTexte=LEGITEXT000006074220&dateTexte=20180426) Articolo R. 224-24 del codice ambiente.

Per eseguire questi calcoli, il professionista deve essere in possesso dei seguenti dispositivi di controllo:

- un indicatore della temperatura dei gas di combustione all'uscita della caldaia;
- Un analizzatore di gas di combustione portatile che mostra anidride carbonica o contenuto di ossigeno;
- Un dispositivo manuale per misurare l'indice di annerimento;
- un indicatore depressometro
- un indicatore per stimare il funzionamento dell'impianto, nonché un indicatore del flusso di carburante o del fluido che trasporta calore;
- Un registratore a pressione a vapore per una caldaia con una potenza nominale di oltre 2 megawatt (MW);
- un indicatore di temperatura del fluido termico per una caldaia con una potenza compresa tra 400 kW e 2MW.

Il professionista dovrebbe anche controllare gli elementi per migliorare l'efficienza energetica della struttura.

*Per andare oltre* Articoli R. 224-21 e quanto segue del codice d'ambiente.

**Mantenere aggiornato un registro delle caldaie**

Per ogni caldaia o caldaia in carica, il professionista deve mantenere un registro caldaie con le caratteristiche efficienze dell'impianto, così come tutti gli elementi per migliorarne l'efficienza. Energia.

*Per andare oltre* Articolo R. 224-29 del codice ambiente.

**Controlli periodici**

Il gestore di una caldaia deve avere un controllo periodico dell'efficienza energetica effettuato da un organismo accreditato di non più di due anni.

Questo controllo periodico dovrebbe includere:

- calcolare le prestazioni caratteristiche della caldaia e controllare il rispetto minimo dei valori minimi imposti da parte dell'impianto (cfr. supra "2 gradi). a. Rendimenti");
- Controllo dell'esistenza e del corretto funzionamento dei dispositivi di misurazione e controllo;
- Controllo delle buone condizioni degli impianti situati nell'impianto, per la distribuzione dell'energia termica;
- un opuscolo della caldaia.

Al termine di questa ispezione, l'agenzia presenta una relazione di controllo all'operatore che deve conservarne una copia per almeno cinque anni.

Inoltre, l'operatore deve monitorare le emissioni inquinanti dell'impianto. Tale controllo è inteso a valutare le concentrazioni di inquinanti atmosferici emessi dall'impianto e deve essere effettuato in accordo con i termini e le condizioni dell'ordine dell'ordine del 2 ottobre 2009 relativi al controllo delle caldaie con un potere nominale 400 kilowatt e meno di 20 megawatt.

*Per andare oltre* Articoli da R. 224-31 a R. 224-41-1 del codice ambientale.

Tre gradi. Disposizioni specifiche per gli impianti classificati per la protezione dell'ambiente (ICPE)
-------------------------------------------------------------------------------------------------------

A causa della sua natura inquinante, l'attività del professionista dovrebbe rientrare nelle norme relative agli impianti classificati per la protezione dell'ambiente (ICPE).

Questa attività rientra nell'ambito della rubrica 2910. Combustione, esclusi gli impianti coperti dalle sezioni 2770, 2771 e 2971 della nomenclatura ICPE. Come tale, il professionista sarà tenuto a prendere misure che variano a seconda della natura della sua attività.

**Quando il suo impianto consuma solo gas naturale, gas petrolifero liquefatto, olio combustibile domestico, carbone, olio combustibile pesante, biomassa**

Il professionista sarà tenuto a eseguire:

- Una richiesta di autorizzazione se la potenza dell'installazione è maggiore o uguale a 20MW;
- una dichiarazione con controllo periodico quando la potenza dell'installazione è maggiore di 2MW ma inferiore a 20MW.

**Quando la sua struttura consuma combustibili diversi da quelli sopra menzionati**

Il professionista sarà tenuto a eseguire:

- una richiesta di autorizzazione se l'installazione:- ha più di 20 MW di energia,
  + ha una capacità superiore a 0,1 MW ma inferiore a 20 MW e non utilizza biomassa (un prodotto composto da materiale vegetale),
  + utilizza il biogas, ha una capacità nominale di oltre 0,1 MW e che il biogas è prodotto da un impianto autorizzato o da diversi impianti classificati sotto la voce 2781-1 (impianto di metanizzazione non spreco sostanze vegetali pericolose o crude)
  + una dichiarazione periodica di monitoraggio quando l'impianto, con una capacità nominale superiore a 0,1 MW ma inferiore a 20MW, utilizza solo il biogas prodotto da un impianto soggetto a dichiarazione di cui alla rubrica 2781-1;
- un record se l'impianto ha una potenza nominale superiore a 0,1 MW ma inferiore a 20MW e utilizza il biogas prodotto da una struttura registrata.

**Si noti che**

Quando la struttura è soggetta a dichiarazione, l'operatore è tenuto a rispettare tutte le disposizioni stabilite nel[Appendice I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=2022C3B3EA1760F66B2BDCCC394B8CC9.tplgfr22s_2?idArticle=LEGIARTI000032067612&cidTexte=LEGITEXT000005624321&dateTexte=20180426) dell'ordine del 25 luglio 1997 sui requisiti generali per gli impianti classificati per la protezione dell'ambiente soggetti alla dichiarazione di cui alla rubrica 2910 (Combustion).

Si consiglia di fare riferimento alla nomenclatura dell'ICPE, disponibile sul[Aida](https://aida.ineris.fr/) per ulteriori informazioni.

