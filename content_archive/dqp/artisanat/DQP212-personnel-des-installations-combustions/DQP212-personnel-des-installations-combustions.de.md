﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP212" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Handwerk" -->
<!-- var(title)="Mitarbeiter an Verbrennungsanlagen über 400kW" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="handwerk" -->
<!-- var(title-short)="mitarbeiter-an-verbrennungsanlagen" -->
<!-- var(url)="https://www.guichet-qualifications.fr/de/dqp/handwerk/mitarbeiter-an-verbrennungsanlagen-uber-400kw.html" -->
<!-- var(last-update)="2020-04-15 17:20:35" -->
<!-- var(url-name)="mitarbeiter-an-verbrennungsanlagen-uber-400kw" -->
<!-- var(translation)="Auto" -->


Mitarbeiter an Verbrennungsanlagen über 400kW
=============================================

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:20:35<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

Mitarbeiter von Verbrennungsanlagen über 400 Kilowatt (kW) sind ein Fachmann, der mit der Installation, Wartung und dem Betrieb einer Anlage (in der Regel eines Kessels) beschäftigt ist, indem sie ein brennbares Produkt zum Zweck der Energieerzeugung und Nennleistung von mehr als 400 kW. Diese Brennstoffe können flüssig, gasförmig, Kohle oder Braunkohle sein.

Während dieser Aktivität ist der Fachmann verpflichtet, sicherzustellen, dass die Anlage eine gute Energieeffizienz aufrechterhält.

Zwei Grad. Berufsqualifikationen
--------------------------------

### a. Nationale Anforderungen

#### Nationale Rechtsvorschriften

Für diese Tätigkeit sind keine beruflichen Qualifikationen erforderlich, jedoch ist der für die Installation, Wartung und den Betrieb dieser Kessel verantwortliche Fachmann erforderlich:

- Berechnung der Geräterückgabe
- regelmäßige Überprüfungen der Energieeffizienz des Geräts durchführen lassen;
- um ein Kesselbuch auf dem neuesten Stand zu halten.

**Erträge**

Der Fachmann muss die Leistung des Kessels berechnen, für den er zum Zeitpunkt jedes Neustarts und mindestens alle drei Monate während der Betriebsdauer für den Kessel verantwortlich ist. Diese Erträge variieren je nach Inbetriebnahme des Gerätes und der verwendeten wärmeführenden Flüssigkeit.

Daher muss sichergestellt werden, dass der Kessel die festgelegten Mindestwerte erfüllt:

- 14. September 1998, innerhalb der[Tabelle](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837152&cidTexte=LEGITEXT000006074220&dateTexte=20180426) Artikel R. 224-23 des Umweltkodex;
- 14. September 1998 in der[Tabelle](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837153&cidTexte=LEGITEXT000006074220&dateTexte=20180426) Artikel R. 224-24 des Umweltgesetzbuches.

Um diese Berechnungen durchzuführen, muss der Fachmann im Besitz der folgenden Steuergeräte sein:

- ein Indikator für die Temperatur von Verbrennungsgasen am Kesselausgang;
- Ein tragbarer Verbrennungsgasanalysator, der den Kohlendioxid- oder Sauerstoffgehalt anzeigt;
- Ein manuelles Gerät zur Messung des Schwärzindexes;
- ein Indikatordepressometer
- einen Indikator zur Schätzung des Betriebs der Anlage sowie einen Indikator für den Kraftstofffluss oder wärmetragende Flüssigkeit;
- Ein Dampfdruckschreiber für einen Kessel mit einer Nennleistung von mehr als 2 Megawatt (MW);
- eine wärmeführende Flüssigkeitstemperaturanzeige für einen Kessel mit einer Leistung zwischen 400 kW und 2 MW.

Der Fachmann sollte auch die Elemente überprüfen, um die Energieeffizienz der Anlage zu verbessern.

*Um weiter zu gehen* Artikel R. 224-21 und die folgenden Desmaus des Umweltgesetzbuches.

**Halten Sie ein Kesselbuch auf dem neuesten Stand**

Für jeden Kessel oder Kesselsatz muss der Fachmann ein Kesselbuch mit den charakteristischen Effizienzen der Anlage sowie alle Elemente führen, um seine Effizienz zu verbessern. Energie.

*Um weiter zu gehen* Artikel R. 224-29 des Umweltgesetzbuches.

**Regelmäßige Kontrollen**

Der Betreiber eines Kessels muss eine regelmäßige Energieeffizienzprüfung durch eine akkreditierte Stelle von nicht mehr als zwei Jahren durchführen lassen.

Diese regelmäßige Prüfung sollte Folgendes umfassen:

- Berechnung der charakteristischen Leistung des Kessels und Kontrolle der Einhaltung der vorgeschriebenen Mindestwerte der Anlage (siehe oben "2 Grad"). a) Erträge");
- Kontrolle der Existenz und ordnungsgemäßen Funktion von Mess- und Steuergeräten;
- Überprüfung des guten Zustands der Anlagen in der Anlage für die Verteilung der thermischen Energie;
- ein Kesselraumheft.

Am Ende dieser Inspektion legt die Agentur dem Betreiber, der eine Kopie mindestens fünf Jahre aufbewahren muss, einen Kontrollbericht vor.

Darüber hinaus muss der Betreiber die Schadstoffemissionen der Anlage überwachen lassen. Diese Kontrolle dient der Bewertung der von der Anlage emittierten Luftschadstoffe und muss im Einklang mit den Bedingungen des Zeitplans der Verordnung vom 2. Oktober 2009 über die Steuerung von Kesseln mit Nennleistung durchgeführt werden. 400 Kilowatt und weniger als 20 Megawatt.

*Um weiter zu gehen* Artikel R. 224-31 bis R. 224-41-1 des Umweltgesetzbuches.

Drei Grad. Spezifische Bestimmungen für Einrichtungen, die für den Umweltschutz (ICPE) eingestuft sind
------------------------------------------------------------------------------------------------------

Aufgrund ihrer umweltschädlichen Art dürfte die Tätigkeit des Berufstätigen unter die Vorschriften über Einrichtungen fallen, die zum Schutz der Umwelt (ICPE) eingestuft sind.

Diese Tätigkeit fällt in den Anwendungsbereich der Position 2910. Verbrennung, mit Ausnahme der Einrichtungen der Abschnitte 2770, 2771 und 2971 der ICPE-Nomenklatur. Als solcher wird der Profi verpflichtet, Schritte zu ergreifen, die je nach Art seiner Tätigkeit variieren.

**Wenn seine Anlage nur Erdgas, Flüssiggas, inländisches Heizöl, Kohle, Schweröl, Biomasse verbraucht**

Der Profi muss entweder:

- Ein Antrag auf Genehmigung, wenn die Leistung der Anlage größer oder gleich 20 MW ist;
- eine Anweisung mit regelmäßiger Steuerung, wenn die Leistung der Anlage größer als 2 MW, aber weniger als 20 MW ist.

**Wenn seine Anlage andere Kraftstoffe als die oben genannten verbraucht**

Der Profi muss entweder:

- eine Anforderung für die Berechtigung, wenn die Installation:- hat mehr als 20MW Leistung,
  + hat eine Leistung von mehr als 0,1 MW, aber weniger als 20 MW und verwendet keine Biomasse (ein Produkt, das aus Pflanzenmaterial besteht),
  + verwendet Biogas, hat eine Nennleistung von mehr als 0,1 MW und dass Biogas von einer zugelassenen Anlage oder mehreren Einrichtungen der Position 2781-1 (Nicht-Abfall-Methanisierungsanlage) erzeugt wird. gefährliche oder rohe Pflanzenstoffe)
  + eine regelmäßige Überwachungserklärung, wenn die Anlage mit einer Nennleistung von mehr als 0,1 MW, aber weniger als 20 MW, nur Biogas verwendet, das von einer Anlage erzeugt wird, die der Anmeldung unter der Position 2781-1 unterliegt;
- eine Aufzeichnung, wenn die Anlage eine Nennleistung von mehr als 0,1 MW, aber weniger als 20 MW hat und Biogas verwendet, das von einer registrierten Anlage erzeugt wird.

**Beachten Sie, dass**

Wenn die Anlage der Erklärung unterliegt, ist der Betreiber verpflichtet, alle Bestimmungen der[Anhang I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=2022C3B3EA1760F66B2BDCCC394B8CC9.tplgfr22s_2?idArticle=LEGIARTI000032067612&cidTexte=LEGITEXT000005624321&dateTexte=20180426) in der Verordnung vom 25. Juli 1997 über allgemeine Anforderungen an Einrichtungen, die für den Umweltschutz eingestuft sind und unter der Position 2910 (Verbrennung) deklariert sind.

Es ist ratsam, auf die Nomenklatur des ICPE zu verweisen, die auf der[Aida](https://aida.ineris.fr/) für weitere Informationen.

