﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP212" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Skilled trades" -->
<!-- var(title)="Staff at combustion facilities above 400kW" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="skilled-trades" -->
<!-- var(title-short)="staff-at-combustion-facilities" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/skilled-trades/staff-at-combustion-facilities-above-400kw.html" -->
<!-- var(last-update)="2020-04-15 17:20:34" -->
<!-- var(url-name)="staff-at-combustion-facilities-above-400kw" -->
<!-- var(translation)="Auto" -->


Staff at combustion facilities above 400kW
==========================================

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:20:34<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Defining the activity
------------------------

Staff at combustion facilities above 400 Kilowatts (kW) are a professional who is engaged in the installation, maintenance and operation of a facility (usually a boiler) using a combustible product for the purpose of energy production and rated power in excess of 400 kW. These fuels can be liquid, gaseous, coal or lignite.

During this activity, the professional is required to ensure that the facility maintains good energy performance.

Two degrees. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

No professional qualifications are required for this activity, however, the professional responsible for the installation, maintenance and operation of these boilers is required:

- Calculating equipment returns
- Have periodic checks of the device's energy efficiency carried out;
- to keep up-to-date a boiler book.

**Yields**

The professional must calculate the performance of the boiler for which he is responsible for the boiler at the time of each restart and at least every three months during the operating period. These yields vary depending on the date the device is commissioned and the heat-carrying fluid used.

Thus, it must ensure that the boiler meets the minimum values set:

- 14 September 1998, within the[Table](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837152&cidTexte=LEGITEXT000006074220&dateTexte=20180426) Article R. 224-23 of the Environment Code;
- 14 September 1998 in the[Table](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837153&cidTexte=LEGITEXT000006074220&dateTexte=20180426) Article R. 224-24 of the Environment Code.

To perform these calculations, the professional must be in possession of the following control devices:

- an indicator of the temperature of combustion gases at the boiler exit;
- A portable combustion gas analyzer showing carbon dioxide or oxygen content;
- A manual device to measure the blackening index;
- an indicator depressometer
- an indicator to estimate the operation of the facility as well as an indicator of fuel flow or heat-carrying fluid;
- A steam pressure recorder for a boiler with a rated power of more than 2 megawatts (MW);
- a heat-carrying fluid temperature indicator for a boiler with a power of between 400 kW and 2MW.

The professional should also check the elements to improve the energy efficiency of the facility.

*To go further* Articles R. 224-21 and the following of the Environment Code.

**Keep a boiler book up to date**

For each boiler or boiler set in charge, the professional must maintain a boiler book with the characteristic efficiencies of the installation, as well as all the elements to improve its efficiency. Energy.

*To go further* Article R. 224-29 of the Environment Code.

**Periodic checks**

The operator of a boiler must have a periodic energy efficiency check carried out by an accredited body of no more than two years.

This periodic check should include:

- calculating the characteristic performance of the boiler and controlling the facility's compliance with the minimum values imposed (see supra "2 degrees). a. Yields");
- Controlling the existence and proper functioning of measuring and control devices;
- Checking the good condition of the facilities located in the facility's facility, for the distribution of thermal energy;
- a boiler room booklet.

At the end of this inspection, the agency submits a control report to the operator who must keep a copy for at least five years.

In addition, the operator must have the facility's polluting emissions monitored. This control is intended to assess the concentrations of air pollutants emitted by the facility and must be carried out in accord with the terms and conditions of the schedule of the order of 2 October 2009 relating to the control of boilers with a nominal power 400 kilowatts and less than 20 megawatts.

*To go further* Articles R. 224-31 to R. 224-41-1 of the Environment Code.

Three degrees. Provisions specific to facilities classified for environmental protection (ICPE)
-----------------------------------------------------------------------------------------------

Due to its polluting nature, the professional's activity is likely to fall under the regulations relating to facilities classified for the protection of the environment (ICPE).

This activity is within the scope of heading 2910. Combustion, excluding facilities covered by sections 2770, 2771 and 2971 of the ICPE nomenclature. As such, the professional will be required to take steps that vary depending on the nature of his activity.

**When its facility consumes only natural gas, liquefied petroleum gas, domestic fuel oil, coal, heavy fuel oil, biomass**

The professional will be required to perform either:

- A request for permission if the power of the installation is greater than or equal to 20MW;
- a statement with periodic control when the power of the installation is greater than 2MW but less than 20MW.

**When its facility consumes fuels different from those mentioned above**

The professional will be required to perform either:

- a request for permission if the installation:- has more than 20MW of power,
  + has a capacity of more than 0.1 MW but less than 20MW and does not use biomass (a product composed of plant material),
  + uses biogas, has a rated capacity of more than 0.1MW and that biogas is produced by a licensed facility or by several facilities classified under heading 2781-1 (non-waste methanisation facility hazardous or raw plant matter)
  + a periodic monitoring statement when the installation, with a rated capacity of more than 0.1 MW but less than 20MW, uses only biogas produced by a facility subject to declaration under heading 2781-1;
- a record if the facility has a rated power of more than 0.1MW but less than 20MW, and uses biogas produced by a registered facility.

**Note that**

When the facility is subject to declaration, the operator is required to comply with all of the provisions set out in the[Appendix I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=2022C3B3EA1760F66B2BDCCC394B8CC9.tplgfr22s_2?idArticle=LEGIARTI000032067612&cidTexte=LEGITEXT000005624321&dateTexte=20180426) of the order of 25 July 1997 on general requirements for facilities classified for environmental protection subject to declaration under heading 2910 (Combustion).

It is advisable to refer to the nomenclature of the ICPE, available on the[Aida](https://aida.ineris.fr/) for more information.

