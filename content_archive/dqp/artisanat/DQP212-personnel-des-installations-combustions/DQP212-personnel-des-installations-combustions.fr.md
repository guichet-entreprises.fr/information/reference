﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP212" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artisanat" -->
<!-- var(title)="Personnel des installations de combustions supérieures à 400kW" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artisanat" -->
<!-- var(title-short)="personnel-des-installations-combustions" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/artisanat/personnel-des-installations-de-combustions-superieures-a-400kw.html" -->
<!-- var(last-update)="2020-04-30 11:23:49" -->
<!-- var(url-name)="personnel-des-installations-de-combustions-superieures-a-400kw" -->
<!-- var(translation)="None" -->


# Personnel des installations de combustions supérieures à 400kW

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-30 11:23:49<!-- end-var -->

## 1°. Définition de l’activité

Le personnel des installations de combustions supérieures à 400 Kilowatts (kW) est un professionnel dont l'activité consiste à installer, entretenir et exploiter une installation (le plus souvent une chaudière) utilisant un produit combustible dans le but de produire de l'énergie et dont la puissance nominale dépasse 400 kW. Ces combustibles peuvent être liquides, gazeux, du charbon ou du lignite.

Au cours de cette activité, le professionnel est tenu de s'assurer du maintien de bonnes performances énergétiques de l'installation.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Aucune qualification professionnelle n'est requise pour l'exercice de cette activité, toutefois, le professionnel chargé de l'installation, de l'entretien et de l'exploitation de ces chaudières est tenu :

* de calculer les rendements de l'équipement ;
* de faire réaliser les contrôles périodiques de l'efficacité énergétique de l'appareil ;
* de tenir à jour un livret de chaufferie.

**Rendements**

Le professionnel doit procéder, au moment de chaque remise en marche de la chaudière et au moins tous les trois mois pendant la période de fonctionnement, au calcul des rendements de la chaudière dont il a la charge. Ces rendements varient selon la date de mise en service de l'appareil et le fluide caloporteur utilisé.

Ainsi, il doit s'assurer que la chaudière respecte les valeurs minimales fixées :

* en cas de mise en service après le 14 septembre 1998, au sein du [tableau](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837152&cidTexte=LEGITEXT000006074220&dateTexte=20180426) fixé à l'article R. 224-23 du Code de l'environnement ;
* en cas de mise en service avant le 14 septembre 1998 au sein du [tableau](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837153&cidTexte=LEGITEXT000006074220&dateTexte=20180426) fixé à l'article R. 224-24 du Code de l'environnement.

Pour effectuer ces calculs, le professionnel doit être en possession des appareils de contrôle suivants :

* un indicateur de la température des gaz de combustion à la sortie de la chaudière ;
* un analyseur portatif des gaz de combustion indiquant la teneur en dioxyde de carbone ou en dioxygène ;
* un appareil manuel de mesure de l'indice de noircissement ;
* un déprimomètre indicateur ;
* un indicateur permettant d'estimer l'allure de fonctionnement de l'installation ainsi qu'un indicateur du débit de combustible ou de fluide caloporteur ;
* un enregistreur de pression de vapeur pour une chaudière de puissance nominale supérieure à 2 mégawatts (MW) ;
* un indicateur de température du fluide caloporteur pour une chaudière d'une puissance comprise entre 400 kW et 2MW.

Le professionnel doit également vérifier les éléments permettant d'améliorer l'efficacité énergétique de l'installation.

*Pour aller plus loin* : articles R. 224-21 et suivants du Code de l'environnement.

**Tenir à jour un livret de chaufferie**

Pour chaque chaudière ou ensemble de chaudières dont il a la charge, le professionnel doit tenir à jour un livret de chaufferie comportant les rendements caractéristiques de l'installation, ainsi que l'ensemble des éléments permettant d'améliorer son efficacité énergétique.

*Pour aller plus loin* : article R. 224-29 du Code de l'environnement.

**Contrôles périodiques**

L'exploitant d'une chaudière doit faire réaliser, par un organisme accrédité, un contrôle périodique n'excédant pas deux ans, portant sur l'efficacité énergétique.

Ce contrôle périodique doit comporter les éléments suivants :

* le calcul du rendement caractéristique de la chaudière et le contrôle de conformité de l'installation aux valeurs minimales imposées (cf. supra « 2°. a. Rendements ») ;
* le contrôle de l'existence et du bon fonctionnement des appareils de mesure et de contrôle ;
* la vérification du bon état des installations situées dans le local où se trouve l'installation, et destinées à la distribution de l'énergie thermique ;
* un livret de chaufferie.

À l'issue de ce contrôle, l'organisme remet un rapport de contrôle à l'exploitant qui doit en conserver un exemplaire pendant au moins cinq ans.

En outre, l'exploitant doit faire réaliser un contrôle des émissions polluantes de l'installation. Ce contrôle vise à évaluer les concentrations de polluants atmosphériques émises par l'installation et doit s'effectuer selon les modalités prévues à l'annexe de l'arrêté du 2 octobre 2009 relatif au contrôle des chaudières dont la puissance nominale est supérieure à 400 kilowatts et inférieure à 20 mégawatts.

*Pour aller plus loin* : articles R. 224-31 à R. 224-41-1 du Code de l'environnement.

## 3°. Dispositions spécifiques aux installations classées pour le protection de l'environnement (ICPE)

Du fait de sa nature polluante, l'activité du professionnel est susceptible de relever de la réglementation relative aux installations classées pour la protection de l'environnement (ICPE).

Cette activité entre dans le champ de la rubrique 2910. Combustion, à l'exclusion des installations visées par les rubriques 2770, 2771 et 2971 de la nomenclature des ICPE. À ce titre, le professionnel sera tenu d'effectuer des démarches variant selon la nature de son activité.

**Lorsque son installation consomme exclusivement du gaz naturel, des gaz de pétrole liquéfié, du fioul domestique, du charbon, des fiouls lourds, de la biomasse**

Le professionnel sera tenu d'effectuer soit :

* une demande d'autorisation si la puissance de l'installation est supérieure ou égale à 20MW ;
* une déclaration avec contrôle périodique lorsque la puissance de l'installation est supérieure à 2MW mais inférieure à 20MW.

**Lorsque son installation consomme des combustibles différents de ceux cités ci-dessus**

Le professionnel sera tenu d'effectuer soit :

* une demande d'autorisation si l'installation :
  * possède une puissance supérieure à 20MW,
  * possède une puissance supérieure à 0,1 MW mais inférieure à 20MW et n'utilisant pas de biomasse (produit composé de matière végétale), 
  * utilise du biogaz, possède une puissance nominale supérieure à 0,1MW et que le biogaz est produit par une installation soumise à autorisation ou par plusieurs installations classées au titre de la rubrique 2781-1 (installation de méthanisation de déchets non dangereux ou de matière végétale brute),
  * une déclaration avec contrôle périodique lorsque l'installation, d'une puissance nominale supérieure à 0,1 MW mais inférieure à 20MW, n'utilise que du biogaz produit par une installation soumise à déclaration au titre de la rubrique 2781-1 ;
* un enregistrement dès lors que l'installation possède une puissance nominale supérieure à 0,1MW mais inférieure à 20MW, et utilise du biogaz produit par une installation soumise à enregistrement.

**À noter**

Lorsque l'installation est soumise à déclaration, l'exploitant est tenu de respecter l'ensemble des dispositions fixées à l'[annexe I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=2022C3B3EA1760F66B2BDCCC394B8CC9.tplgfr22s_2?idArticle=LEGIARTI000032067612&cidTexte=LEGITEXT000005624321&dateTexte=20180426) de l'arrêté du 25 juillet 1997 relatif aux prescriptions générales applicables aux installations classées pour la protection de l'environnement soumises à déclaration sous la rubrique n° 2910 (Combustion).

Il est conseillé de se reporter à la nomenclature des ICPE, disponible sur le site de l'[Aida](https://aida.ineris.fr/) pour de plus amples informations.