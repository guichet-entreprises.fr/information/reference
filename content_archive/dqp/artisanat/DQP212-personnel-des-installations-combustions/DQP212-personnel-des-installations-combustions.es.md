﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP212" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artesanía" -->
<!-- var(title)="Personal en instalaciones de combustión superiores a 400kW" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artesania" -->
<!-- var(title-short)="personal-en-instalaciones-de-combustion" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/artesania/personal-en-instalaciones-de-combustion-superiores-a-400kw.html" -->
<!-- var(last-update)="2020-04-15 17:20:34" -->
<!-- var(url-name)="personal-en-instalaciones-de-combustion-superiores-a-400kw" -->
<!-- var(translation)="Auto" -->


Personal en instalaciones de combustión superiores a 400kW
==========================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:34<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

El personal de las instalaciones de combustión superior a 400 kilovatios (kW) es un profesional que se dedica a la instalación, mantenimiento y operación de una instalación (normalmente una caldera) utilizando un producto combustible con el propósito de producción de energía y potencia nominal superior a 400 kW. Estos combustibles pueden ser líquidos, gaseosos, carbón o lignito.

Durante esta actividad, el profesional está obligado a garantizar que la instalación mantenga un buen rendimiento energético.

Dos grados. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

No se requieren cualificaciones profesionales para esta actividad, sin embargo, se requiere el profesional responsable de la instalación, mantenimiento y operación de estas calderas:

- Cálculo de los retornos de equipos
- Realizar comprobaciones periódicas de la eficiencia energética del dispositivo;
- para mantener actualizado un libro de calderas.

**Rendimientos**

El profesional debe calcular el rendimiento de la caldera de la que es responsable de la caldera en el momento de cada reinicio y al menos cada tres meses durante el período de funcionamiento. Estos rendimientos varían dependiendo de la fecha en que se pone en marcha el dispositivo y el fluido portador de calor utilizado.

Por lo tanto, debe asegurarse de que la caldera cumple con los valores mínimos establecidos:

- 14 de septiembre de 1998, en el marco de la[Mesa](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837152&cidTexte=LEGITEXT000006074220&dateTexte=20180426) Artículo R. 224-23 del Código de Medio Ambiente;
- 14 de septiembre de 1998 en el[Mesa](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837153&cidTexte=LEGITEXT000006074220&dateTexte=20180426) Artículo R. 224-24 del Código de Medio Ambiente.

Para realizar estos cálculos, el profesional debe estar en posesión de los siguientes dispositivos de control:

- un indicador de la temperatura de los gases de combustión en la salida de la caldera;
- Un analizador de gas de combustión portátil que muestra dióxido de carbono u contenido de oxígeno;
- Un dispositivo manual para medir el índice de ennegrecente;
- un indicador dedesprestosómetro
- un indicador para estimar el funcionamiento de la instalación, así como un indicador del flujo de combustible o fluido portador de calor;
- Una grabadora de presión de vapor para una caldera con una potencia nominal de más de 2 megavatios (MW);
- un indicador de temperatura del fluido portador de calor para una caldera con una potencia de entre 400 kW y 2MW.

El profesional también debe comprobar los elementos para mejorar la eficiencia energética de la instalación.

*Para ir más allá* Artículos R. 224-21 y siguientes del Código de Medio Ambiente.

**Mantenga un libro de calderas actualizado**

Para cada caldera o caldera a cargo, el profesional debe mantener un libro de calderas con las eficiencias características de la instalación, así como todos los elementos para mejorar su eficiencia. Energía.

*Para ir más allá* Artículo R. 224-29 del Código de Medio Ambiente.

**Comprobaciones periódicas**

El operador de una caldera debe tener un control periódico de la eficiencia energética llevado a cabo por un organismo acreditado de no más de dos años.

Esta comprobación periódica debe incluir:

- calcular el rendimiento característico de la caldera y controlar el cumplimiento de la instalación con los valores mínimos impuestos (véase supra "2 grados). a. Rendimientos");
- Controlar la existencia y el buen funcionamiento de los dispositivos de medición y control;
- Comprobación del buen estado de las instalaciones ubicadas en las instalaciones de la instalación, para la distribución de energía térmica;
- un folleto de la sala de calderas.

Al final de esta inspección, la agencia presenta un informe de control al operador que debe conservar una copia durante al menos cinco años.

Además, el operador debe tener las emisiones contaminantes de la instalación monitoreadas. Este control tiene por objeto evaluar las concentraciones de contaminantes atmosféricos emitidas por la instalación y debe llevarse a cabo de conformidad con los términos y condiciones del calendario del auto de 2 de octubre de 2009 relativo al control de las calderas con una potencia nominal 400 kilovatios y menos de 20 megavatios.

*Para ir más allá* Artículos R. 224-31 a R. 224-41-1 del Código de Medio Ambiente.

Tres grados. Disposiciones específicas para instalaciones clasificadas para la protección del medio ambiente (ICPE)
-------------------------------------------------------------------------------------------------------------------

Debido a su naturaleza contaminante, es probable que la actividad del profesional esté comprendida en la normativa relativa a las instalaciones clasificadas para la protección del medio ambiente (ICPE).

Esta actividad está comprendida en el ámbito de aplicación de la partida 2910. Combustión, excluyendo las instalaciones cubiertas por los artículos 2770, 2771 y 2971 de la nomenclatura del ICPE. Como tal, el profesional tendrá que tomar medidas que varían dependiendo de la naturaleza de su actividad.

**Cuando su instalación consume sólo gas natural, gas licuado de petróleo, combustible doméstico, carbón, combustible pesado, biomasa**

El profesional deberá realizar:

- Una solicitud de permiso si la potencia de la instalación es mayor o igual que 20MW;
- una declaración con control periódico cuando la potencia de la instalación es superior a 2MW pero inferior a 20MW.

**Cuando su instalación consume combustibles diferentes a los mencionados anteriormente**

El profesional deberá realizar:

- una solicitud de permiso si la instalación:- tiene más de 20MW de potencia,
  + tiene una capacidad superior a 0,1 MW pero inferior a 20MW y no utiliza biomasa (un producto compuesto de material vegetal),
  + biogás, tiene una capacidad nominal superior a 0,1MW y que el biogás es producido por una instalación autorizada o por varias instalaciones clasificadas en la partida 2781-1 (instalaciones de methanización sin residuos materia vegetal peligrosa o cruda)
  + una declaración periódica de seguimiento cuando la instalación, con una capacidad nominal superior a 0,1 MW pero inferior a 20MW, utilice únicamente biogás producido por una instalación sujeta a declaración en la partida 2781-1;
- un registro si la instalación tiene una potencia nominal de más de 0,1MW pero menos de 20MW, y utiliza biogás producido por una instalación registrada.

**Tenga en cuenta que**

Cuando la instalación está sujeta a declaración, el operador está obligado a cumplir con todas las disposiciones establecidas en el[Apéndice I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=2022C3B3EA1760F66B2BDCCC394B8CC9.tplgfr22s_2?idArticle=LEGIARTI000032067612&cidTexte=LEGITEXT000005624321&dateTexte=20180426) del auto de 25 de julio de 1997 sobre los requisitos generales para las instalaciones clasificadas para la protección del medio ambiente sujetas a declaración en la partida 2910 (Combustión).

Es aconsejable referirse a la nomenclatura del ICPE, disponible en el[Aida](https://aida.ineris.fr/) para obtener más información.

