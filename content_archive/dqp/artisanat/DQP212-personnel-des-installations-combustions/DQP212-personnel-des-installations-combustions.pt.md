﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP212" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artesanato" -->
<!-- var(title)="Pessoal em instalações de combustão acima de 400kW" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artesanato" -->
<!-- var(title-short)="pessoal-em-instalacoes-de-combustao" -->
<!-- var(url)="https://www.guichet-qualifications.fr/pt/dqp/artesanato/pessoal-em-instalacoes-de-combustao-acima-de-400kw.html" -->
<!-- var(last-update)="2020-04-15 17:20:35" -->
<!-- var(url-name)="pessoal-em-instalacoes-de-combustao-acima-de-400kw" -->
<!-- var(translation)="Auto" -->


Pessoal em instalações de combustão acima de 400kW
==================================================

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:20:35<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

Funcionários em instalações de combustão acima de 400 Kilowatts (kW) são um profissional que está envolvido na instalação, manutenção e operação de uma instalação (geralmente uma caldeira) usando um produto combustível para fins de produção de energia e potência nominal superior a 400 kW. Esses combustíveis podem ser líquidos, gasosos, carvão ou lignito.

Durante essa atividade, o profissional é obrigado a garantir que a instalação mantenha um bom desempenho energético.

Dois graus. Qualificações profissionais
---------------------------------------

### a. Requisitos nacionais

#### Legislação nacional

Não são necessárias qualificações profissionais para esta atividade, no entanto, é necessário o profissional responsável pela instalação, manutenção e operação dessas caldeiras:

- Cálculo de devoluções de equipamentos
- Realizar verificações periódicas da eficiência energética do dispositivo;
- para manter atualizado um livro de caldeiras.

**Rendimentos**

O profissional deve calcular o desempenho da caldeira para a qual é responsável pela caldeira no momento de cada reinicialização e pelo menos a cada três meses durante o período de operação. Esses rendimentos variam dependendo da data em que o dispositivo é encomendado e do fluido de transporte térmico utilizado.

Assim, deve garantir que a caldeira atenda aos valores mínimos definidos:

- 14 de setembro de 1998, dentro do[Tabela](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837152&cidTexte=LEGITEXT000006074220&dateTexte=20180426) Artigo R. 224-23 do Código Ambiental;
- 14 de setembro de 1998 no[Tabela](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=8E4E5291F783C4F7A74428D636FFB264.tplgfr22s_2?idArticle=LEGIARTI000006837153&cidTexte=LEGITEXT000006074220&dateTexte=20180426) Artigo R. 224-24 do Código do Meio Ambiente.

Para realizar esses cálculos, o profissional deve estar de posse dos seguintes dispositivos de controle:

- indicador da temperatura dos gases de combustão na saída da caldeira;
- Um analisador portátil de gás de combustão mostrando dióxido de carbono ou teor de oxigênio;
- Um dispositivo manual para medir o índice de escureamento;
- um indicador depressômetro
- um indicador para estimar o funcionamento da instalação, bem como um indicador de fluxo de combustível ou fluido de transporte térmico;
- Um gravador de pressão a vapor para uma caldeira com potência nominal de mais de 2 megawatts (MW);
- um indicador de temperatura de fluido de transporte térmico para uma caldeira com uma potência entre 400 kW e 2MW.

O profissional também deve verificar os elementos para melhorar a eficiência energética da instalação.

*Para ir mais longe* Artigos R. 224-21 e o seguinte do Código do Meio Ambiente.

**Mantenha um livro de caldeiras atualizado**

Para cada caldeira ou caldeira responsável, o profissional deve manter um livro de caldeiras com as eficiências características da instalação, bem como todos os elementos para melhorar sua eficiência. Energia.

*Para ir mais longe* Artigo R. 224-29 do Código Do Meio Ambiente.

**Verificações periódicas**

O operador de uma caldeira deve ter uma verificação periódica de eficiência energética realizada por um organismo credenciado de no número superior a dois anos.

Esta verificação periódica deve incluir:

- cálculo do desempenho característico da caldeira e controle do cumprimento da instalação com os valores mínimos impostos (ver supra "2 graus). a. Rendimentos");
- Controle da existência e do bom funcionamento dos dispositivos de medição e controle;
- Verificação das boas condições das instalações localizadas nas instalações da instalação, para a distribuição de energia térmica;
- um livreto da sala de caldeiras.

Ao final desta inspeção, a agência envia um relatório de controle ao operador que deve manter uma cópia por pelo menos cinco anos.

Além disso, o operador deve ter as emissões poluentes da instalação monitoradas. Este controle destina-se a avaliar as concentrações de poluentes atmosféricos emitidos pela instalação e deve ser realizado de acordo com os termos e condições do cronograma da ordem de 2 de Outubro de 2009 relativos ao controle de caldeiras com potência nominal 400 quilowatts e menos de 20 megawatts.

*Para ir mais longe* Artigos R. 224-31 a R. 224-41-1 do Código Ambiental.

Três graus. Disposições específicas para instalações classificadas para proteção ambiental (ICPE)
-------------------------------------------------------------------------------------------------

Devido à sua natureza poluidora, é provável que a atividade do profissional se enquadranas as normas relativas às instalações classificadas para a proteção do meio ambiente (ICPE).

Esta atividade está dentro do escopo da posição 2910. A combustão, excluindo as instalações abrangidas pelas seções 2770, 2771 e 2971 da nomenclatura do ICPE. Sendo assim, o profissional será obrigado a tomar medidas que variam dependendo da natureza de sua atividade.

**Quando sua instalação consome apenas gás natural, gás liquefeito de petróleo, óleo combustível doméstico, carvão, óleo combustível pesado, biomassa**

O profissional será obrigado a realizar qualquer um:

- Pedido de permissão se a potência da instalação for maior ou igual a 20MW;
- uma declaração com controle periódico quando a potência da instalação for superior a 2MW, mas inferior a 20MW.

**Quando sua instalação consome combustíveis diferentes dos mencionados acima**

O profissional será obrigado a realizar qualquer um:

- um pedido de permissão se a instalação:- tem mais de 20MW de potência,
  + tem capacidade superior a 0,1 MW, mas menos de 20MW e não utiliza biomassa (produto composto de material vegetal),
  + utiliza biogás, tem uma capacidade nominal de mais de 0,1MW e que o biogás é produzido por uma instalação licenciada ou por várias instalações classificadas a posição 2781-1 (instalação de metanoização não resíduos matéria vegetal perigosa ou crua)
  + uma declaração de monitoramento periódico quando a instalação, com capacidade nominal superior a 0,1 MW, mas inferior a 20MW, utiliza apenas o biogás produzido por uma instalação sujeita à declaração a posição 2781-1;
- um recorde se a instalação tiver uma potência nominal de mais de 0,1MW, mas inferior a 20MW, e usar biogás produzido por uma instalação registrada.

**Note que**

Quando a instalação está sujeita à declaração, o operador é obrigado a cumprir todas as disposições estabelecidas no[Apêndice I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=2022C3B3EA1760F66B2BDCCC394B8CC9.tplgfr22s_2?idArticle=LEGIARTI000032067612&cidTexte=LEGITEXT000005624321&dateTexte=20180426) da ordem de 25 de Julho de 1997 sobre os requisitos gerais para instalações classificadas para proteção ambiental sujeitas à declaração a posição 2910 (Combustão).

É aconselhável consultar a nomenclatura do ICPE, disponível no[Aida](https://aida.ineris.fr/) para mais informações.

