﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP241" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Handwerk" -->
<!-- var(title)="Fahrrad-Reparatur" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="handwerk" -->
<!-- var(title-short)="fahrrad-reparatur" -->
<!-- var(url)="https://www.guichet-qualifications.fr/de/dqp/handwerk/fahrrad-reparatur.html" -->
<!-- var(last-update)="2020-04-15 17:20:39" -->
<!-- var(url-name)="fahrrad-reparatur" -->
<!-- var(translation)="Auto" -->


Fahrrad-Reparatur
=================

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:20:39<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

Der Fahrrad-Reparaturmeister ist ein Profi, der sich auf die Reparatur und Wartung von Fahrrädern spezialisiert hat. Es kann nach oben gehen und Zyklen anpassen, sowie ihre Ausrüstung steuern und überarbeiten (Bremsen, Reifen, Sättel, etc.).

Zwei Grad. Berufsqualifikationen
--------------------------------

Die betroffene Person, die als Fahrradwerkstatt arbeiten möchte, muss eine berufliche Qualifikation oder Ausübung unter der effektiven und dauerhaften Kontrolle einer Person mit dieser Qualifikation besitzen.

Um als beruflich qualifizierter Mensch betrachtet zu werden, muss die Person einen der folgenden Diplome oder Ausbildungstitel besitzen:

- Certificate of Professional Fitness (CAP) "Vehicle Maintenance Mechanic Option Cycles and Motorcycles";
- Berufsabsen "Kfz-Wartungsoption Motorräder";
- Master-Zertifikat (BM) "Zyklus- und Motorrad-Reparaturmechaniker";
- Certificate of Professional Qualification (CQP) "Cycle Mechanic".

In Ermangelung eines dieser Diplome oder Titel muss der Betroffene eine effektive dreijährige Berufserfahrung im Gebiet der Europäischen Union (EU) oder des Europäischen Wirtschaftsraums (EWR) rechtfertigen, die er als Unternehmensführer erworben hat. selbständig oder in der Arbeit der Fahrradwerkstatt beschäftigt. In diesem Fall wird dem Betroffenen empfohlen, sich an die Handwerkskammer (CMA) zu wenden, um eine Bescheinigung über die berufliche Qualifikation anzufordern.

*Um weiter zu gehen* :[Artikel 16](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006513344&cidTexte=JORFTEXT000000193678) Gesetz 96-603 vom 5. Juli 1996 über die Entwicklung und Förderung des Handels und des Handwerks; Dekret 98-246 vom 2. April 1998 über die berufliche Qualifikation, die für die Tätigkeiten von Artikel 16 des Gesetzes 96-603 vom 5. Juli 1996 erforderlich ist.

#### Ausbildung

Das CQP ist ein Titel, der nach einer Weiterbildung, nach einem Professionalisierungsvertrag oder durch Validierung von Erfahrungen (VAE) zugänglich ist. Weitere Informationen finden Sie unter[Offizielle Website von VAE](http://www.vae.gouv.fr/).

#### Mit der Qualifizierung verbundene Kosten

Das Training ist in der Regel kostenlos. Für weitere Informationen ist es ratsam, näher an das betreffende Ausbildungszentrum zu kommen.

### b. EU- oder EWR-Bürger: für vorübergehende und gelegentliche Übungen (Free Service Delivery (LPS)

Jeder Staatsangehörige eines EU-Mitgliedstaats oder des EWR, der in diesem Staat niedergelassen und rechtmäßig in der Tätigkeit einer Fahrradwerkstatt tätig ist, kann die gleiche Tätigkeit vorübergehend und gelegentlich in Frankreich ausüben.

Er muss zunächst den Antrag durch Erklärung an die CMA des Ortes stellen, an dem er die Dienstleistung ausführen möchte.

Für den Fall, dass der Beruf weder im Rahmen der Tätigkeit noch im Rahmen der Ausbildung in dem Land, in dem der Berufstätige niedergelassen ist, geregelt ist, muss er diese Tätigkeit während der zehn Jahre mindestens ein Jahr lang ausgeübt haben. in einem oder mehreren EU-Mitgliedstaaten.

Bestehen erhebliche Unterschiede zwischen der beruflichen Qualifikation des Staatsangehörigen und der in Frankreich erforderlichen Ausbildung, so kann die zuständige CMA verlangen, dass sich die betreffende Person einer Eignungsprüfung unterziehen muss.

*Um weiter zu gehen* Artikel 17-1 des Gesetzes vom 5. Juli 1996; Artikel 2 der[Dekret vom 2. April 1998](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000388449&categorieLien=cid) geändert durch die[Dekret vom 4. Mai 2017](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7D50D037AAEEDE8367FE375890CB8510.tplgfr39s_2?cidTexte=JORFTEXT000034598573&dateTexte=20170506).

### c. EU- oder EWR-Bürger: für eine ständige Ausübung (Freie Niederlassung)

Um die Tätigkeit der Fahrradwerkstatt in Frankreich dauerhaft ausüben zu können, muss der EU- oder EWR-Staatsangehörige eine der folgenden Bedingungen erfüllen:

- die gleichen beruflichen Qualifikationen haben wie ein Franzose (siehe oben "2 Grad"). a. Berufliche Qualifikationen");
- ein Für die Ausübung einer Fahrradwerkstatttätigkeit in einem EU- oder EWR-Staat erforderliche Beleg oder ein Ausbildungszeugnis besitzen, wenn dieser Staat den Zugang oder die Ausübung dieser Tätigkeit in seinem Hoheitsgebiet regelt;
- über ein Bestelltheitszeugnis oder ein Ausbildungszeugnis verfügen, das seine Vorbereitung auf die Ausübung der Tätigkeit der Fahrradwerkstatt bescheinigt, wenn dieses Zertifikat oder titelin einem EU- oder EWR-Staat erworben wurde, der den Zugang nicht regelt oder Die Ausübung dieser Tätigkeit
- ein Diplom, einen Titel oder ein Zeugnis in einem Drittstaat erworben und von einem EU- oder EWR-Staat in Äquivalenz zugelassen haben, sofern die Person als Fahrradwerkstatt in dem Staat gearbeitet hat, der Gleichwertigkeit.

Sobald der Staatsangehörige eines EU- oder EWR-Staates eine der oben genannten Voraussetzungen erfüllt, kann er eine Bescheinigung über die Anerkennung einer berufsqualifizierenden Qualifikation beantragen (siehe unten "5o"). b. Beantragung einer Bescheinigung über die berufliche Qualifikation des EU- oder EWR-Staatsangehörigen für eine dauere Übung (LE))

Bestehen erhebliche Unterschiede zwischen der beruflichen Qualifikation des Staatsangehörigen und der in Frankreich erforderlichen Ausbildung, so kann die zuständige CMA verlangen, dass sich die betreffende Person Entschädigungsmaßnahmen unterwirft (siehe infra "5o). a. Gut zu wissen: Ausgleichsmaßnahmen").

*Um weiter zu gehen* Artikel 17 und 17-1 des Gesetzes 96-603 vom 5. Juli 1996, Artikel 3 bis 3-2 des Dekrets vom 2. April 1998 geändert durch das Dekret vom 4. Mai 2017.

Drei Grad. Bedingungen der Ehrenfähigkeit
-----------------------------------------

Niemand darf als Fahrradwerkstatt arbeiten, wenn er Gegenstand von:

- ein Verbot der direkten oder indirekten Führung, Verwaltung, Verwaltung oder Kontrolle eines gewerblichen oder handwerklichen Unternehmens;
- eine Strafe des Verbots der beruflichen oder sozialen Tätigkeit für eines der in Artikel 131-6 des Strafgesetzbuches vorgesehenen Straftaten oder Vergehen.

*Um weiter zu gehen* Artikel 19 III des Gesetzes 96-603 vom 5. Juli 1996.

Es ist ein Vier-Grad-Eins. Versicherung
---------------------------------------

Die liberale Fahrradwerkstatt muss eine Berufshaftpflichtversicherung abschließen. Wenn er dagegen als Arbeitnehmer praktiziert, ist diese Versicherung nur optional. In diesem Fall ist es Sache des Arbeitgebers, eine solche Versicherung für seine Arbeitnehmer für die während ihrer beruflichen Tätigkeit begangenen Handlungen abzuschließen.

Fünf Grad. Anerkennungsverfahren und Formalitäten für die Anerkennung
---------------------------------------------------------------------

### a. eine Voranmeldung der Tätigkeit für EU- oder EWR-Staatsangehörige für eine vorübergehende und gelegenheitsbezogene Übung (LPS) beantragen

**Zuständige Behörde**

Die CMA des Ortes, an dem der Staatsangehörige die Leistung ausüben möchte, ist für die Erteilung der vorherigen Tätigkeitserklärung zuständig.

**Belege**

Dem Antrag auf voraber Tätigkeitsbericht ist eine vollständige Akte mit den folgenden Belegen beigefügt:

- Eine Fotokopie eines gültigen Ausweises
- eine Bescheinigung, die die rechtsstaatliche Rechtseinrichtung des Staatsangehörigen in einem EU- oder EWR-Staat rechtfertigt;
- ein Dokument, das die berufliche Qualifikation des Staatsangehörigen rechtfertigt, der nach Ihrer Wahl sein kann:- Eine Kopie eines Diploms, Titels oder Zeugnisses,
  + Ein Kompetenzzertifikat,
  + Unterlagen, die die Berufserfahrung des Staatsangehörigen bezeugen.

**Was Sie wissen sollten**

Bei Bedarf müssen die Stücke von einem zertifizierten Übersetzer ins Französische übersetzt werden.

**Beachten Sie, dass**

Wenn die Akte unvollständig ist, hat die CMA eine Frist von fünfzehn Tagen, um den Staatsangehörigen zu informieren und alle fehlenden Dokumente anzufordern.

**Ergebnis des Verfahrens**

Nach Erhalt aller Dokumente in der Akte hat die CMA einen Monat Zeit, um zu entscheiden:

- entweder die Leistung zu genehmigen, wenn der Staatsangehörige drei Jahre Berufserfahrung in einem EU- oder EWR-Staat rechtfertigt, und dieser Entscheidung ein Zeugnis der beruflichen Qualifikation beizufügen;
- oder die Bestimmung zu genehmigen, wenn die beruflichen Qualifikationen des Staatsangehörigen als ausreichend erachtet werden;
- entweder eine Eignungsprüfung oder einen Anpassungskurs vorzuschreiben, wenn erhebliche Unterschiede zwischen den beruflichen Qualifikationen des Staatsangehörigen und denen in Frankreich erforderlich sind. Im Falle einer Verweigerung der Durchführung dieser Ausgleichsmaßnahme oder im Falle eines Scheiterns bei der Ausführung ist der Staatsangehörige nicht in der Lage, die Erbringung von Dienstleistungen in Frankreich durchzuführen.

Das Schweigen, das die zuständige Behörde in diesen Zeiten bewahrt hat, ist eine Genehmigung wert, mit der Erbringung von Dienstleistungen zu beginnen.

*Um weiter zu gehen* Artikel 2 des Dekrets vom 2. April 1998; Artikel 2 der[17. Oktober 2017](https://www.legifrance.gouv.fr/eli/arrete/2017/10/17/ECOI1719273A/jo) über die Einreichung der Erklärung und die Anträge des Dekrets 98-246 vom 2. April 1998 und des Titels I des Dekrets 98-247 vom 2. April 1998.

### b. Beantragung einer Bescheinigung über die Anerkennung der Berufsqualifikation für den EU- oder EWR-Staatsangehörigen im Falle einer dauerithepten Ausübung (LE)

Die Betroffene, die ein anderes als das in Frankreich erforderliche Diplom oder seine Berufserfahrung anerkennen möchte, kann eine Bescheinigung über die Anerkennung einer Berufsqualifikation beantragen.

**Zuständige Behörde**

Der Antrag ist an die zuständige CMA des Ortes zu richten, an dem sich die Person niederlassen möchte.

**Verfahren**

Ein Antragsbeleg wird dem Antragsteller innerhalb eines Monats nach Erhalt von der CMA zugesandt. Ist die Akte unvollständig, bittet die CMA die betroffene Person, sie innerhalb von 14 Tagen nach Einreichung der Akte auszufüllen. Eine Quittung wird ausgestellt, sobald sie vollständig ist.

**Belege**

Der Antrag auf Zertifizierung der Berufsqualifikation ist eine Datei mit den folgenden Belegen:

- Ein Antrag auf ein Zertifikat für die berufliche Qualifikation
- einen Nachweis der beruflichen Qualifikation in Form eines Befähigungszeugnisses oder eines Diploms oder eines Berufszeugnisses;
- Eine Fotokopie des gültigen Personalausweises des Antragstellers
- Wurden im Hoheitsgebiet eines EU- oder EWR-Staates Arbeitserfahrung erworben, so eine Bescheinigung über Art und Dauer der Tätigkeit, die von der zuständigen Behörde im Herkunftsmitgliedstaat ausgestellt wurde;
- wenn die Berufserfahrung in Frankreich erworben wurde, die Beweise für die Ausübung der Tätigkeit für drei Jahre.

**Was Sie wissen sollten**

Gegebenenfalls müssen alle Belege von einem beglaubigten Übersetzer ins Französische übersetzt werden.

Die CMA kann weitere Informationen über ihre Ausbildung oder Berufserfahrung anfordern, um festzustellen, ob wesentliche Unterschiede zu der in Frankreich erforderlichen beruflichen Qualifikation bestehen könnten. Wenn sich die CMA an das International Centre for Educational Studies (Ciep) wenden soll, um zusätzliche Informationen über das Ausbildungsniveau eines Diploms oder Zeugnisses oder einer ausländischen Bezeichnung zu erhalten, muss der Antragsteller eine Gebühr entrichten. Zusätzliche.

**Zeit**

Innerhalb von drei Monaten nach Erhalt kann die CMA beschließen,

- Anerkennung der beruflichen Qualifikation und Erteilung der Zertifizierung der beruflichen Qualifikation;
- den Staatsangehörigen einer Entschädigungsmaßnahme vorlegen und ihn über diese Entscheidung unterrichten;
- die Erteilung des Zeugnisses der berufsqualifizierenden Qualifikation zu verweigern.

**Was Sie wissen sollten**

In Ermangelung einer Entscheidung innerhalb von vier Monaten gilt der Antrag auf eine Bescheinigung über die berufliche Qualifikation als erworben.

**Heilmittel**

Lehnt die CMA den Antrag der CMA auf berufliche Qualifikation ab, kann der Antragsteller die Entscheidung anfechten. Sie kann somit innerhalb von zwei Monaten nach Zustellung der Ablehnung der CMA folgende Form bilden:

- einen anmutigen Appell an den Präfekten der zuständigen CMA-Abteilung;
- eine Klage vor dem zuständigen Verwaltungsgericht.

**Kosten**

kostenlos.

**Gut zu wissen: Ausgleichsmaßnahmen**

Die CMA notifiziert den Kläger von seiner Entscheidung, ihn eine der Ausgleichsmaßnahmen durchführen zu lassen. In dieser Entscheidung sind die Fächer aufgeführt, die nicht unter die vom Antragsteller bescheinigte Qualifikation fallen und deren Kenntnisse für die Ausübung in Frankreich zwingend erforderlich sind.

Der Antragsteller muss dann zwischen einem Anpassungskurs von bis zu drei Jahren und einem Eignungstest wählen.

Die Eignungsprüfung erfolgt in Form einer Prüfung vor einer Jury. Sie wird innerhalb von sechs Monaten nach Eingang der Entscheidung der Klägerin, sich für die Veranstaltung zu entscheiden, organisiert. Andernfalls gilt die Qualifikation als erworben und die CMA stellt ein Berufsqualifikationszeugnis auf.

Am Ende des Anpassungskurses sendet der Antragsteller der CMA ein Zertifikat, das bescheinigt, dass er dieses Praktikum ordnungsgemäß absolviert hat, begleitet von einer Bewertung der Organisation, die ihn betreut hat. Die CMA stellt auf der Grundlage dieses Zertifikats innerhalb eines Monats ein Zertifikat über die berufliche Qualifikation aus.

Die Entscheidung über die Anwendung einer Entschädigungsmaßnahme kann von der betroffenen Person angefochten werden, die innerhalb von zwei Monaten nach Zustellung der Entscheidung beim Präfekten eine Verwaltungsbeschwerde einlegen muss. Wird seine Berufung abgewiesen, kann er dann eine Klage einleiten.

**Kosten**

Es kann eine feste Gebühr für die Untersuchung des Falles erhoben werden. Für weitere Informationen ist es ratsam, näher an die entsprechende CMA zu kommen.

*Um weiter zu gehen* Artikel 3 bis 3-2 des Dekrets 98-246 vom 2. April 1998; Dekret vom 28. Oktober 2009 durch die Dekrete 97-558 vom 29. Mai 1997 und Nr. 98-246 vom 2. April 1998 über das Verfahren zur Anerkennung der Berufsqualifikationen eines Berufsstaatsangehörigen eines Mitgliedstaats der Gemeinschaft oder einem anderen Vertragsstaat des Abkommens über den Europäischen Wirtschaftsraum.

### c. Abhilfemaßnahmen

#### Französisches Hilfszentrum

Das ENIC-NARIC-Zentrum ist das französische Informationszentrum für die akademische und berufliche Anerkennung von Diplomen.

#### Solvit

SOLVIT ist eine Dienstleistung, die von der nationalen Verwaltung jedes Mitgliedstaats der Europäischen Union oder einer Vertragspartei des EWR-Abkommens erbracht wird. Ziel ist es, eine Lösung für einen Streit zwischen einem EU-Bürger und der Verwaltung eines anderen dieser Staaten zu finden. SOLVIT greift insbesondere in die Anerkennung von Berufsqualifikationen ein.

**Bedingungen**

Der Betroffene kann SOLVIT nur verwenden, wenn er

- dass die öffentliche Verwaltung eines EU-Staates seine Rechte nach dem EU-Recht als Bürger oder Unternehmen eines anderen EU-Staates nicht geachtet hat;
- dass sie noch keine Klage eingeleitet hat (Verwaltungsmaßnahmen werden nicht als solche betrachtet).

**Verfahren**

Der Staatsangehörige muss eine[Online-Beschwerdeformular](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Sobald seine Akte eingereicht wurde, kontaktiert SOLVIT ihn innerhalb einer Woche, um gegebenenfalls zusätzliche Informationen anzufordern und zu überprüfen, ob das Problem in seine Zuständigkeit fällt.

**Belege**

Um solVIT zu betreten, muss der Staatsangehörige Folgendes mitteilen:

- Vollständige Kontaktdaten
- Detaillierte Beschreibung seines Problems
- alle Indizbeweise (z. B. Korrespondenz und Entscheidungen der zuständigen Verwaltungsbehörde).

**Zeit**

SOLVIT ist entschlossen, innerhalb von zehn Wochen nach der Übernahme des Falles durch das SOLVIT-Zentrum in dem Land, in dem das Problem aufgetreten ist, eine Lösung zu finden.

**Kosten**

kostenlos.

**Ergebnis des Verfahrens**

Am Ende der 10-Wochen-Frist präsentiert SOLVIT eine Lösung:

- Wenn diese Lösung den Streit über die Anwendung des europäischen Rechts beilegt, wird die Lösung akzeptiert und der Fall abgeschlossen;
- wenn es keine Lösung gibt, wird der Fall als ungelöst abgeschlossen und an die Europäische Kommission verwiesen.

**Weitere Informationen**

SOLVIT in Frankreich: Generalsekretariat für europäische Angelegenheiten, 68 rue de Bellechasse, 75700 Paris ([offizielle Website](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

