﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP241" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artesanato" -->
<!-- var(title)="Reparador de ciclos" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artesanato" -->
<!-- var(title-short)="reparador-de-ciclos" -->
<!-- var(url)="https://www.guichet-qualifications.fr/pt/dqp/artesanato/reparador-de-ciclos.html" -->
<!-- var(last-update)="2020-04-15 17:20:39" -->
<!-- var(url-name)="reparador-de-ciclos" -->
<!-- var(translation)="Auto" -->


Reparador de ciclos
===================

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:20:39<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

O reparador de bicicletas é um profissional especializado no reparo e manutenção de bicicletas. Ele pode subir e ajustar ciclos, bem como controlar e revisar seus equipamentos (freios, pneus, selas, etc.).

Dois graus. Qualificações profissionais
---------------------------------------

O interessado em trabalhar como reparador de ciclos deve ter uma qualificação profissional ou exercício sob o controle efetivo e permanente de uma pessoa com essa qualificação.

Para ser considerado uma pessoa qualificada profissionalmente, o indivíduo deve ter um dos seguintes diplomas ou títulos de formação:

- Certificado de Aptidão Profissional (CAP) "Ciclos e Motocicletas mecânicos de manutenção veicular";
- Bacharel em "motocicletas de opção de manutenção de veículos automotores";
- certificado de mestre (BM) "mecânico de reparo de ciclos e motocicletas";
- Certificado de Qualificação Profissional (CQP) "mecânico de ciclo".

Na ausência de um desses diplomas ou títulos, o interessado deve justificar um efetivo de três anos de experiência profissional no território da União Europeia (UE) ou da Área Econômica Europeia (EEE) adquirida como líder empresarial, autônomo ou assalariado no trabalho de reparador de ciclo. Neste caso, o interessado é aconselhado a entrar em contato com a Câmara de Comércio e Artesanato (CMA) para solicitar um certificado de qualificação profissional.

*Para ir mais longe* :[Artigo 16º](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006513344&cidTexte=JORFTEXT000000193678) Lei 96-603, de 5 de julho de 1996, sobre o desenvolvimento e promoção do comércio e artesanato; Decreto 98-246 de 2 de abril de 1998 relativo à qualificação profissional exigida para as atividades do artigo 16 da Lei 96-603 de 5 de julho de 1996.

#### Treinamento

O CQP é um título acessível após treinamento contínuo, após um contrato de profissionalização ou por validação de experiência (VAE). Para mais informações, você pode ver[Site oficial da VAE](http://www.vae.gouv.fr/).

#### Custos associados à qualificação

O treinamento geralmente é gratuito. Para mais detalhes, é aconselhável aproximar-se do centro de treinamento em questão.

### b. Cidadãos da UE ou EEE: para exercícios temporários e ocasionais (Free Service Delivery (LPS)

Qualquer cidadão de um Estado-membro da UE ou do EEE estabelecido e legalmente envolvido na atividade de reparador de ciclos nesse estado pode realizar a mesma atividade na França, de forma temporária e ocasional.

Ele deve primeiro fazer o pedido por declaração à CMA do local em que deseja realizar o serviço.

Caso a profissão não seja regulamentada, seja no decorrer da atividade ou no contexto de formação, no país em que o profissional está legalmente estabelecido, ele deve ter realizado essa atividade por pelo menos um ano durante os dez anos antes do benefício, em um ou mais Estados-Membros da UE.

Quando houver diferenças substanciais entre a qualificação profissional do nacional e a formação necessária na França, a CMA competente pode exigir que o interessado se submeta a um teste de aptidão.

*Para ir mais longe* Artigo 17º-1º da Lei de 5 de Julho de 1996; Artigo 2º do[decreto de 2 de abril de 1998](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000388449&categorieLien=cid) modificado pelo[decreto de 4 de maio de 2017](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7D50D037AAEEDE8367FE375890CB8510.tplgfr39s_2?cidTexte=JORFTEXT000034598573&dateTexte=20170506).

### c. Cidadãos da UE ou EEE: para um exercício permanente (Estabelecimento Livre)

Para realizar a atividade de reparador de ciclos na França de forma permanente, a UE ou o nacional da EEE devem atender a uma das seguintes condições:

- têm as mesmas qualificações profissionais exigidas para um francês (ver acima "2 graus). a. Qualificações profissionais");
- possuir um certificado de competência ou certificado de treinamento necessário para o exercício da atividade de reparador de ciclos em um estado da UE ou EEE quando esse Estado regulamenta relejos ou exercício desta atividade em seu território;
- possui um certificado de competência ou um certificado de treinamento que certifica sua preparação para o exercício da atividade de reparador de ciclos quando este certificado ou título tiver sido obtido em um estado da UE ou EEE que não regula o acesso ou O exercício desta atividade
- ter um diploma, título ou certificado adquirido em um terceiro estado e admitido em equivalência por um estado da UE ou EEE na condição adicional de que a pessoa tem trabalhado como reparador de ciclos no estado que admitiu Equivalência.

Uma vez que o nacional de um estado da UE ou EEE cumpra uma das condições acima, ele ou ela poderá solicitar um certificado de reconhecimento de qualificação profissional (veja abaixo "5o). b. Solicitar um certificado de qualificação profissional para a UE ou eEE nacional para um exercício permanente (LE))

Quando houver diferenças substanciais entre a qualificação profissional do nacional e a formação exigida na França, a CMA competente pode exigir que o interessado se submeta a medidas de compensação (ver infra "5o. a. Bom saber: medidas de compensação").

*Para ir mais longe* 17 e 17-1 da Lei 96-603 de 5 de Julho de 1996, Artigos 3º a 3-2 do decreto de 2 de abril de 1998 alterado pelo decreto de 4 de maio de 2017.

Três graus. Condições de honorabilidade
---------------------------------------

Ninguém pode trabalhar como reparador de ciclos se ele ou ela for o assunto de:

- a proibição de executar, gerenciar, administrar ou controlar direta ou indiretamente uma empresa comercial ou artesanal;
- a pena de proibição de atividade profissional ou social para qualquer um dos crimes ou contravenções prevista susceptino no artigo 131-6 do Código Penal.

*Para ir mais longe* Artigo 19 III da Lei 96-603 de 5 de julho de 1996.

É um de quatro graus. Seguro
----------------------------

O reparador de ciclo liberal deve fazer um seguro de responsabilidade profissional. Por outro lado, se ele pratica como empregado, este seguro é apenas opcional. Neste caso, cabe ao empregador realizar esse seguro para seus empregados pelos atos realizados durante sua atividade profissional.

Cinco graus. Procedimentos e formalidades de reconhecimento de qualificação
---------------------------------------------------------------------------

### a. Solicitar uma pré-declaração de atividade para a UE ou eEE nacional para um exercício temporário e casual (LPS)

**Autoridade competente**

O CMA do local em que o nacional deseja realizar o benefício é competente para emitir a declaração prévia de atividade.

**Documentos de suporte**

A solicitação de um pré-relatório de atividade é acompanhada por um arquivo completo contendo os seguintes documentos de suporte:

- Uma fotocópia de um ID válido
- um certificado que justifique que o nacional esteja legalmente estabelecido em um estado da UE ou eEE;
- um documento que justifique a qualificação profissional do nacional que pode ser, a seu ver:- Uma cópia de um diploma, título ou certificado,
  + Um certificado de competência,
  + qualquer documentação atestando a experiência profissional do nacional.

**O que saber**

Se necessário, as peças devem ser traduzidas para o francês por um tradutor certificado.

**Note que**

Quando o arquivo está incompleto, o CMA tem um prazo de quinze dias para informar o nacional e solicitar todos os documentos faltantes.

**Resultado do procedimento**

Após o recebimento de todos os documentos do arquivo, o CMA tem um mês para decidir:

- ou autorizar o benefício quando o nacional justificar três anos de experiência de trabalho em um estado da UE ou eEE, e anexar a essa decisão um certificado de qualificação profissional;
- ou autorizar a provisão quando as qualificações profissionais nacionais forem consideradas suficientes;
- seja para impor um teste de aptidão ou um curso de ajuste quando houver diferenças substanciais entre as qualificações profissionais do nacional e as exigidas na França. Em caso de recusa em realizar esta medida de compensação ou em caso de falha na sua execução, o nacional não poderá realizar a prestação de serviços na França.

O silêncio mantido pela autoridade competente nestes tempos vale autorização para iniciar a prestação de serviços.

*Para ir mais longe* Artigo 2º do decreto de 2 de Abril de 1998; Artigo 2º do[17 de outubro de 2017](https://www.legifrance.gouv.fr/eli/arrete/2017/10/17/ECOI1719273A/jo) quanto à apresentação da declaração e aos pedidos previstos no Decreto 98-246 de 2 de abril de 1998 e título I do Decreto 98-247 de 2 de abril de 1998.

### b. Solicitar um certificado de reconhecimento de qualificação profissional para a UE ou eEE nacional no caso de um exercício permanente (LE)

O interessado em ter um diploma reconhecido diferente do exigido na França ou sua experiência profissional pode solicitar um certificado de reconhecimento de qualificação profissional.

**Autoridade competente**

A solicitação deve ser dirigida à CMA apropriada do local em que a pessoa deseja se estabelecer.

**Procedimento**

Um recibo de solicitação é enviado ao requerente dentro de um mês após o recebimento do CMA. Se o arquivo estiver incompleto, o CMA pede ao interessado para completá-lo dentro de uma quinzena após o arquivamento do arquivo. Um recibo é emitido assim que estiver completo.

**Documentos de suporte**

O pedido de certificação de qualificação profissional é um arquivo com os seguintes documentos de suporte:

- Um pedido de certificado de qualificação profissional
- uma comprovação de qualificação profissional sob a forma de certificado de competência ou diploma ou certificado de formação profissional;
- Uma fotocópia do ID válido do requerente
- Se a experiência de trabalho tiver sido adquirida no território de um Estado da UE ou da EEE, um certificado sobre a natureza e a duração da atividade emitida pela autoridade competente no Estado-Membro de origem;
- se a experiência profissional foi adquirida na França, as provas do exercício da atividade por três anos.

**O que saber**

Se necessário, todos os documentos de suporte devem ser traduzidos para o francês por um tradutor certificado.

O CMA pode solicitar mais informações sobre sua formação ou experiência profissional para determinar a possível existência de diferenças substanciais com a qualificação profissional exigida na França. Além disso, se o CMA se aproximar do Centro Internacional de Estudos Educacionais (Ciep) para obter informações adicionais sobre o nível de formação de um diploma ou certificado ou uma designação estrangeira, o candidato terá que pagar uma taxa Adicionais.

**Tempo**

Dentro de três meses após o recebimento, a CMA pode decidir:

- Reconhecer a qualificação profissional e emitir certificação de qualificação profissional;
- submeter o nacional a uma medida de compensação e notificá-lo dessa decisão;
- recusar-se a emitir o certificado de qualificação profissional.

**O que saber**

Na ausência de uma decisão no prazo de quatro meses, considera-se que o pedido de certificado de qualificação profissional foi adquirido.

**Remédios**

Se o CMA recusar o pedido de qualificação profissional da CMA, o requerente poderá contestar a decisão. Pode, portanto, no prazo de dois meses após a notificação da recusa da CMA, formar:

- um apelo gracioso ao prefeito do departamento de CMA relevante;
- um desafio legal perante o tribunal administrativo relevante.

**Custo**

Livre.

**Bom saber: medidas de compensação**

O CMA notifica o requerente de sua decisão de fazê-lo executar uma das medidas de compensação. Esta decisão lista os assuntos não abrangidos pela qualificação atestada pelo requerente e cujo conhecimento é imperativo para a prática na França.

Em seguida, o candidato deve escolher entre um curso de ajuste de até três anos e um teste de aptidão.

O teste de aptidão toma a forma de um exame perante um júri. É organizado no prazo de seis meses após o recebimento da decisão do requerente pelo requerente de optar pelo evento. Caso contrário, considera-se que a qualificação foi adquirida e o CMA estabelece um certificado de qualificação profissional.

Ao final do curso de ajuste, o candidato envia ao CMA um certificado certificando que concluiu este estágio de forma válida, acompanhado de uma avaliação da organização que o supervisionou. O CMA emite, com base neste certificado, um certificado de qualificação profissional no prazo de um mês.

A decisão de utilizar uma medida de compensação pode ser contestada pelo interessado que deverá interpor recurso administrativo ao prefeito no prazo de dois meses após a notificação da decisão. Se sua apelação for rejeitada, ele pode então iniciar um desafio legal.

**Custo**

Uma taxa fixa que cobre a investigação do caso pode ser cobrada. Para obter mais informações, é aconselhável aproximar-se do CMA relevante.

*Para ir mais longe* Artigos 3º a 3-2 do Decreto 98-246 de 2 de Abril de 1998; decreto de 28 de outubro de 2009 nos Decretos 97-558 de 29 de Maio de 1997 e nº 98-246 de 2 de Abril de 1998 relativos ao procedimento de reconhecimento das qualificações profissionais de um profissional nacional de um Estado-Membro da Comunidade ou outra parte estatal ao acordo da Área Econômica Europeia.

### c. Remédios

#### Centro de assistência francês

O Centro ENIC-NARIC é o centro francês de informações sobre o reconhecimento acadêmico e profissional de diplomas.

#### Solvit

O SOLVIT é um serviço prestado pela Administração Nacional de cada Estado-Membro da União Europeia ou parte do acordo eEE. Seu objetivo é encontrar uma solução para uma disputa entre um nacional da UE e a administração de outro desses Estados. A SOLVIT intervém em particular no reconhecimento das qualificações profissionais.

**Condições**

O interessado só pode usar o SOLVIT se estabelecer:

- que a administração pública de um Estado da UE não respeitou seus direitos sob o direito da UE como cidadão ou negócio de outro Estado da UE;
- que ainda não iniciou ação judicial (ação administrativa não é considerada como tal).

**Procedimento**

O nacional deve completar um[formulário de reclamação on-line](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Uma vez que seu arquivo tenha sido apresentado, a SOLVIT entra em contato com ele dentro de uma semana para solicitar, se necessário, informações adicionais e verificar se o problema está dentro de sua competência.

**Documentos de suporte**

Para entrar no SOLVIT, o nacional deve comunicar:

- Detalhes completos de contato
- Descrição detalhada de seu problema
- todas as provas nos autos (por exemplo, correspondências e decisões recebidas da autoridade administrativa competente).

**Tempo**

A SOLVIT está empenhada em encontrar uma solução dentro de dez semanas a partir do dia em que o caso foi assumido pelo centro SOLVIT no país em que o problema ocorreu.

**Custo**

Livre.

**Resultado do procedimento**

Ao final do período de 10 semanas, a SOLVIT apresenta uma solução:

- Se essa solução resolver a disputa sobre a aplicação do direito europeu, a solução será aceita e o caso será encerrado;
- se não houver solução, o caso é encerrado como não resolvido e encaminhado à Comissão Europeia.

**Mais informações**

SOLVIT na França: Secretaria Geral para Assuntos Europeus, 68 rue de Bellechasse, 75700 Paris ([site oficial](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

