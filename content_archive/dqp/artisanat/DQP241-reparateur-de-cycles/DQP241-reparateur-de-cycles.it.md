﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP241" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artigianato" -->
<!-- var(title)="Riparatore di cicli" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artigianato" -->
<!-- var(title-short)="riparatore-di-cicli" -->
<!-- var(url)="https://www.guichet-qualifications.fr/it/dqp/artigianato/riparatore-di-cicli.html" -->
<!-- var(last-update)="2020-04-15 17:20:39" -->
<!-- var(url-name)="riparatore-di-cicli" -->
<!-- var(translation)="Auto" -->


Riparatore di cicli
===================

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:20:39<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

Il riparatore di biciclette è un professionista specializzato nella riparazione e manutenzione delle biciclette. Può salire e regolare i cicli, così come controllare e rivedere le loro attrezzature (freni, pneumatici, selle, ecc.).

Due gradi. Qualifiche professionali
-----------------------------------

L'interessato che desidera lavorare come riparatore di biciclette deve avere una qualifica professionale o un esercizio fisico sotto il controllo effettivo e permanente di una persona con tale qualifica.

Per essere considerata una persona professionalmente qualificata, l'individuo deve essere in possesso di uno dei seguenti diplomi o titoli di formazione:

- Certificato di Professional Fitness (CAP) "Vehicle Maintenance Mechanic Option Cycles and Motorcycles";
- Laurea di primo livello professionale "motociclette per la manutenzione dei veicoli a motore";
- master's certificate (BM) "meccanico di riparazione di cicli e moto";
- Certificato di qualifica professionale (CQP) "meccanico di ciclo".

In assenza di uno di questi diplomi o titoli, l'interessato deve giustificare un'effettiva esperienza professionale nel territorio dell'Unione europea (UE) o dello Spazio economico europeo (AEA) acquisito come leader imprenditoriale, lavoratori autonomi o retribuiti nel lavoro di riparatore di ciclo. In questo caso, si consiglia all'interessato di contattare la Camera dei Mestieri e dell'Artigianato (CMA) per richiedere un certificato di qualifica professionale.

*Per andare oltre* :[Articolo 16](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006513344&cidTexte=JORFTEXT000000193678) legge 96-603 del 5 luglio 1996 sullo sviluppo e la promozione del commercio e dell'artigianato; Decreto 98-246 del 2 aprile 1998 relativo alla qualificazione professionale richiesta per le attività dell'articolo 16 della legge 96-603 del 5 luglio 1996.

#### Formazione

Il CQP è un titolo accessibile dopo una formazione continua, dopo un contratto di professionalizzazione o per convalida dell'esperienza (VAE). Per ulteriori informazioni, è possibile[Sito ufficiale di VAE](http://www.vae.gouv.fr/).

#### Costi associati alla qualifica

La formazione è di solito gratuita. Per maggiori dettagli, si consiglia di avvicinarsi al centro di formazione in questione.

### b. Cittadini dell'UE o del CEE: per esercizi olfatto temporaneo e occasionale (Consegna gratuita dei servizi (LPS)

Qualsiasi cittadino di uno Stato membro dell'UE o del CEA stabilito e legalmente impegnato nell'attività di riparatore di ciclo in tale Stato può svolgere la stessa attività in Francia, su base temporanea e occasionale.

Egli deve prima presentare la richiesta per dichiarazione alla CMA del luogo in cui desidera svolgere il servizio.

Nel caso in cui la professione non sia regolamentata, né nel corso dell'attività né nel contesto della formazione, nel paese in cui il professionista è legalmente stabilito, deve aver svolto tale attività per almeno un anno nel corso dei dieci anni prima del beneficio, in uno o più Stati membri dell'UE.

In caso di differenze sostanziali tra la qualifica professionale del nazionale e la formazione richiesta in Francia, la CMA competente può richiedere che la persona interessata sottoponga a un test attitudinale.

*Per andare oltre* L'articolo 17-1 della legge del 5 luglio 1996; L'articolo 2 del[decreto del 2 aprile 1998](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000388449&categorieLien=cid) modificato dal[decreto del 4 maggio 2017](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7D50D037AAEEDE8367FE375890CB8510.tplgfr39s_2?cidTexte=JORFTEXT000034598573&dateTexte=20170506).

### c. Cittadini dell'UE o del CEE: per un esercizio permanente (Free Establishment)

Per svolgere l'attività di riparatore di cicli in Francia in modo permanente, l'UE o il SEE nazionale devono soddisfare una delle seguenti condizioni:

- hanno le stesse qualifiche professionali di quelle richieste per un francese (vedi sopra "2 gradi). a. Qualifiche professionali");
- detenere un certificato di competenza o di formazione necessario per l'esercizio dell'attività del riparatore di ciclo in uno Stato dell'UE o del CEA quando tale Stato regola l'accesso o l'esercizio di tale attività sul suo territorio;
- hanno un certificato di competenza o un certificato di formazione che ne certifica la preparazione per l'esercizio dell'attività di riparatore di ciclo quando il certificato o il titolo è stato ottenuto in uno Stato dell'UE o del L'esercizio di questa attività
- hanno un diploma, un titolo o un certificato acquisito in un terzo Stato e ammessi in equivalenza da uno Stato dell'UE o del CEA a condizione che la persona abbia lavorato come riparatore di cicli nello Stato che ha ammesso Equivalenza.

Una volta che il cittadino di uno Stato dell'UE o del SEE soddisfa una delle suddette condizioni, potrà richiedere un certificato di riconoscimento della qualifica professionale (vedi sotto "5o). b. Richiedere un certificato di qualifica professionale per l'UE o il cittadino del CEA per un esercizio permanente (LE))

Qualora vi siano differenze sostanziali tra la qualifica professionale del nazionale e la formazione richiesta in Francia, la CMA competente può richiedere che la persona interessata sottoponga a misure compensative (cfr. infra "5o. a. Buono a sapersi: misure di compensazione").

*Per andare oltre* Articoli 17 e 17-1 della legge 96-603 del 5 luglio 1996, articoli da 3 a 3-2 del decreto del 2 aprile 1998 modificati con decreto del 4 maggio 2017.

Tre gradi. Condizioni di onorabilità
------------------------------------

Nessuno può lavorare come riparatore di cicli se lui o lei è il soggetto di:

- divieto di gestione diretta o indiretta, gestione, amministrazione o controllo di un'impresa commerciale o artigianale;
- una sanzione di attività professionale o sociale per uno qualsiasi dei reati o delitti previsti dall'articolo 131-6 del codice penale.

*Per andare oltre* Articolo 19 III della legge 96-603 del 5 luglio 1996.

È uno di quattro gradi. Assicurazione
-------------------------------------

Il riparatore di cicli liberale deve prendere un'assicurazione responsabilità civile professionale. D'altra parte, se esercita come dipendente, questa assicurazione è solo facoltativa. In questo caso, spetta al datore di lavoro prendere tale assicurazione per i suoi dipendenti per gli atti compiuti durante la loro attività professionale.

Cinque gradi. Procedure e formalità di riconoscimento delle qualifiche
----------------------------------------------------------------------

### a. Richiedere una predichiarazione dell'attività per l'UE o il cittadino del CEA per un esercizio temporaneo e occasionale (LPS)

**Autorità competente**

La CMA del luogo in cui il cittadino desidera svolgere il beneficio è competente per rilasciare la dichiarazione preliminare di attività.

**Documenti di supporto**

La richiesta di una pre-relazione dell'attività è accompagnata da un file completo contenente i seguenti documenti giustificativi:

- Una fotocopia di un documento d'identità valido
- un certificato che giustifica che il cittadino è legalmente stabilito in uno Stato dell'UE o del CEE;
- un documento che giustifica la qualifica professionale del cittadino che può essere, a vostra scelta:- Una copia di un diploma, titolo o certificato,
  + Un certificato di competenza,
  + documentazione che attesti l'esperienza professionale del cittadino.

**Cosa sapere**

Se necessario, i pezzi devono essere tradotti in francese da un traduttore certificato.

**Si noti che**

Quando il fascicolo è incompleto, il CMA ha un periodo di quindici giorni per informare il nazionale e richiedere tutti i documenti mancanti.

**Esito della procedura**

Alla ricezione di tutti i documenti nel file, la CMA ha un mese per decidere:

- autorizzare il beneficio qualora il cittadino giustifichi tre anni di esperienza lavorativa in uno Stato dell'UE o del CEA e collegare a tale decisione un certificato di qualifica professionale;
- o autorizzare la disposizione quando le qualifiche professionali del cittadino sono ritenute sufficienti;
- sia per imporre un test attitudinale o un corso di adeguamento quando vi sono differenze sostanziali tra le qualifiche professionali del nazionale e quelle richieste in Francia. In caso di rifiuto di effettuare questa misura di compensazione o in caso di fallimento nella sua esecuzione, il cittadino non sarà in grado di effettuare la fornitura di servizi in Francia.

Il silenzio tenuto dall'autorità competente in questi tempi merita l'autorizzazione ad avviare la fornitura di servizi.

*Per andare oltre* articolo 2 del decreto del 2 aprile 1998; L'articolo 2 del[giovedì 17 ottobre 2017](https://www.legifrance.gouv.fr/eli/arrete/2017/10/17/ECOI1719273A/jo) per quanto riguarda la presentazione della dichiarazione e le richieste previste dal decreto 98-246 del 2 aprile 1998 e il titolo I del decreto 98-247 del 2 aprile 1998.

### b. Richiedere un certificato di riconoscimento della qualifica professionale per l'UE o il cittadino del CEA in caso di esercizio permanente (LE)

L'interessato che desidera un diploma riconosciuto diverso da quello richiesto in Francia o dalla sua esperienza professionale può richiedere un certificato di riconoscimento della qualifica professionale.

**Autorità competente**

La richiesta deve essere indirizzata alla CMA appropriata del luogo in cui la persona desidera stabilirsi.

**Procedura**

Una ricevuta della domanda viene inviata al richiedente entro un mese dalla ricezione da parte della CMA. Se il file è incompleto, la CMA chiede alla persona interessata di completarlo entro una quindicina di giorni dalla presentazione del file. Una ricevuta viene emessa non appena è completa.

**Documenti di supporto**

La domanda di certificazione di qualifica professionale è un file con i seguenti documenti giustificativi:

- Una domanda per un certificato di qualifica professionale
- una prova di qualifica professionale sotto forma di un certificato di competenza o di diploma o di un certificato di formazione professionale;
- Una fotocopia del documento d'identità valido del richiedente
- Se l'esperienza lavorativa è stata acquisita sul territorio di uno Stato dell'UE o del CEA, un certificato sulla natura e la durata dell'attività rilasciata dall'autorità competente nello Stato membro di origine;
- se l'esperienza professionale è stata acquisita in Francia, le prove dell'esercizio dell'attività per tre anni.

**Cosa sapere**

Se necessario, tutti i documenti giustificativi devono essere tradotti in francese da un traduttore certificato.

La CMA può richiedere ulteriori informazioni sulla sua formazione o esperienza professionale per determinare la possibile esistenza di differenze sostanziali con la qualifica professionale richiesta in Francia. Inoltre, se la CMA si rivolgerà all'International Centre for Educational Studies (Ciep) per ottenere ulteriori informazioni sul livello di formazione di un diploma o di un certificato o di una designazione straniera, il richiedente dovrà pagare una tassa Ulteriori.

**Tempo**

Entro tre mesi dalla ricezione, la CMA può decidere di:

- Riconoscere la qualifica professionale e la certificazione di emissione della qualifica professionale;
- sottoporre il cittadino a una misura di compensazione e informarlo di tale decisione;
- rifiutare di rilasciare il certificato di qualifica professionale.

**Cosa sapere**

In assenza di una decisione entro quattro mesi, si ritiene che sia stata acquisita la domanda di certificato di qualifica professionale.

**Rimedi**

Se la CMA rifiuta la domanda di qualificazione professionale della CMA, il richiedente può contestare la decisione. Essa può pertanto, entro due mesi dalla notifica del rifiuto della CMA, formare:

- un grazioso appello al prefetto del relativo dipartimento CMA;
- una contestazione legale dinanzi al giudice amministrativo competente.

**Costo**

Gratuito.

**Buono a sapersi: misure di compensazione**

La CMA notifica al ricorrente la sua decisione di fargli eseguire una delle misure di compensazione. Questa decisione elenca i temi non contemplati dalla qualifica attestata dal richiedente e le cui conoscenze sono imperative per la pratica in Francia.

Il richiedente deve quindi scegliere tra un corso di adeguamento fino a tre anni e un test attitudinale.

Il test attitudino prende la forma di un esame davanti a una giuria. Essa è organizzata entro sei mesi dalla ricezione da parte del CMA della decisione della ricorrente di optare per l'evento. In caso contrario, si ritiene che la qualifica sia stata acquisita e la CMA stabilisce un certificato di qualifica professionale.

Al termine del corso di adeguamento, il richiedente invia alla CMA un certificato che attua di aver validamente completato questo stage, accompagnato da una valutazione dell'organizzazione che lo ha supervisionato. La CMA rilascia, sulla base di questo certificato, un certificato di qualifica professionale entro un mese.

La decisione di utilizzare una misura di compensazione può essere impugnata dalla persona interessata che deve presentare un ricorso amministrativo al prefetto entro due mesi dalla notifica della decisione. Se il suo ricorso viene respinto, può quindi avviare una contestazione legale.

**Costo**

Può essere addebitata una tassa fissa che copra l'indagine del caso. Per ulteriori informazioni, si consiglia di avvicinarsi alla cMA pertinente.

*Per andare oltre* Articoli da 3 a 3-2 del decreto 98-246 del 2 aprile 1998; decreto del 28 ottobre 2009 ai sensi dei decreti 97-558 del 29 maggio 1997 e n. 98-246 del 2 aprile 1998 in relazione alla procedura di riconoscimento delle qualifiche professionali di un cittadino professionale di uno Stato membro della Comunità o un'altra parte statale dell'accordo dello Spazio economico europeo.

### c. Rimedi

#### Centro di assistenza francese

Il Centro ENIC-NARIC è il centro francese per informazioni sul riconoscimento accademico e professionale dei diplomi.

#### Solvit

SOLVIT è un servizio fornito dall'Amministrazione nazionale di ogni Stato membro dell'Unione europea o da parte dell'accordo DEL TESORo. Il suo obiettivo è quello di trovare una soluzione a una controversia tra un cittadino dell'UE e l'amministrazione di un altro di questi Stati. SOLVIT interviene in particolare nel riconoscimento delle qualifiche professionali.

**Condizioni**

L'interessato può utilizzare SOLVIT solo se stabilisce:

- che la pubblica amministrazione di uno Stato dell'UE non ha rispettato i propri diritti ai sensi del diritto dell'UE in qualità di cittadino o di un'altra impresa di un altro Stato dell'UE;
- che non ha già avviato un'azione legale (l'azione amministrativa non è considerata come tale).

**Procedura**

Il cittadino deve completare un[modulo di reclamo online](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una volta che il suo fascicolo è stato inviato, SOLVIT lo contatta entro una settimana per richiedere, se necessario, ulteriori informazioni e per verificare che il problema rientri nelle sue competenze.

**Documenti di supporto**

Per entrare in SOLVIT, il cittadino deve comunicare:

- Dati di contatto completi
- Descrizione dettagliata del suo problema
- tutte le prove nel fascicolo (ad esempio, corrispondenza e decisioni ricevute dall'autorità amministrativa competente).

**Tempo**

SOLVIT si impegna a trovare una soluzione entro dieci settimane il caso è stato rilevato dal centro SOLVIT nel paese in cui si è verificato il problema.

**Costo**

Gratuito.

**Esito della procedura**

Alla fine del periodo di 10 settimane, SOLVIT presenta una soluzione:

- Se questa soluzione risolve la controversia sull'applicazione del diritto europeo, la soluzione viene accettata e il caso viene chiuso;
- in caso di soluzione, il caso viene chiuso come irrisolto e deferito alla Commissione europea.

**Ulteriori informazioni**

SOLVIT in Francia: Segretariato generale per gli affari europei, 68 rue de Bellechasse, 75700 Parigi ([sito ufficiale](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

