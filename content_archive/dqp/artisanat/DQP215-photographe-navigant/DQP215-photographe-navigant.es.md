﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP215" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artesanía" -->
<!-- var(title)="Fotógrafo de pasajes aéreos" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artesania" -->
<!-- var(title-short)="fotografo-de-pasajes-aereos" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/artesania/fotografo-de-pasajes-aereos.html" -->
<!-- var(last-update)="2020-04-15 17:20:35" -->
<!-- var(url-name)="fotografo-de-pasajes-aereos" -->
<!-- var(translation)="Auto" -->


Fotógrafo de pasajes aéreos
===========================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:35<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

La actividad de un fotógrafo de vuelo consiste en el profesional para tomar fotos a bordo de aviones de aviación civil.

*Para ir más allá* Artículo 1 del Decreto del 8 de julio de 1955 sobre la patente y la licencia de un equipo aéreo profesional en aviación civil.

Dos grados. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo su actividad, el fotógrafo de vuelo debe:

- poseer una patente o licencia como fotógrafo aéreo profesional en aviación civil. La licencia es obligatoria si el profesional realiza esta actividad como principal y habitual en un avión especialmente equipado para la fotografía aérea;
- estar inscrito en el registro C de aircrew profesional en la categoría De trabajo aéreo (C-T, A) (véase a continuación "Registro del Registro de Aircrew Profesional").

#### Entrenamiento

Para obtener la licencia y la licencia del piloto, el profesional debe:

- Tener veintiún años;
- cumplir con los siguientes requisitos de aptitud médica (normas de aptitud): requisito general de aptitud, condición de la visión, condición de percepción del color, condiciones auditivas, de acuerdo con los términos y condiciones establecidos en el artículo 14 del octavo pedido Julio de 1955;
- Total 50 horas de tiempo de vuelo en un avión como fotógrafo, incluyendo como pasante;
- han cumplido con los exámenes teóricos y prácticos organizados por la Escuela Nacional de Aviación Civil ([Enac](http://www.enac.fr/)) ;
- proporcionar un extracto de su contrato de trabajo o una promesa de contrato que justifique su empleo como fotógrafo de vuelo o si es autónomo, un permiso de trabajo en el sector de la fotografía aérea.

Los exámenes teóricos se componen de pruebas escritas y orales puntuadas de entre veinte. Las pruebas prácticas se llevan a cabo en tierra y en vuelo.

**Renovación de la licencia**

La licencia del fotógrafo de vuelo profesional tiene una validez de doce meses y puede renovarse por el mismo período siempre que el profesional:

- cumple los requisitos de aptitud establecidos en el artículo 14 del 8 de julio de 1955;
- justifica haber volado 15 horas en los 12 meses anteriores a su solicitud de renovación como fotógrafo de vuelo o miembro de la tripulación de vuelo de un avión asignado a la fotografía aérea. En caso contrario, el profesional tendrá que someterse a un control de las pruebas prácticas de vuelo frente a un instructor;

*Para ir más allá* : decreto de 8 de julio de 1955, supra.

#### Inscripción en el registro de aircrew profesional

**Autoridad competente**

El profesional que posea un certificado y una licencia de fotógrafo de vuelo debe solicitara a la Dirección General de Aviación Civil (DGAC).

**Documentos de apoyo**

Su solicitud debe incluir el[Formulario Cerfa 47-0049](https://www.formulaires.modernisation.gouv.fr/gf/Cerfa_15792.do) completado y firmado, así como los siguientes documentos:

- Una fotocopia de su dNI válido
- una tarjeta de informe número 3 de su historial criminal de menos de tres meses de edad;
- El número, la naturaleza y la fecha de la concesión de licencias;
- cualquier documentación que justifique que cumple con las condiciones médicas requeridas;
- Cuando el solicitante sea un miembro militar, un certificado de la autoridad militar que certifique que está autorizado a llevar a cabo una actividad principal pagada fuera de las fuerzas armadas;
- una declaración sobre el honor de no ser miembro del servicio público (excepto para los servidores públicos que justifiquen la provisión o el destransporte fuera de marco durante un mínimo de seis meses);
- Cuando el profesional actúe en nombre de otros, una copia del contrato de trabajo (certificado como conforme) mencionando la fecha de contratación del solicitante como marino;
- cuando el profesional trabaja por cuenta propia, un extracto de su inscripción en el Registro Mercantil (RCS) o, en su defecto, un certificado de declaración a los servicios fiscales para su actividad aérea.

**Procedimiento**

Una vez recibida la solicitud en su totalidad, la DGAC registra al profesional.

**Tenga en cuenta que**

El registro puede suspenderse:

- a petición del profesional tan pronto como deje de estar activo debido a un accidente o enfermedad relacionada con el trabajo;
- por lo tanto, ya que el profesional ha dejado de trabajar por más de un año (excepto si el detención se debe a un accidente de trabajo o enfermedad).

*Para ir más allá* Artículo D. 421-2 del Código de Aviación Civil; decreto de 21 de enero de 1988 relativo a los procedimientos de registro de aeronaves profesionales en la aviación civil.

#### Costos asociados con la calificación

La formación que conduce a la licencia del fotógrafo de patentes y vuelos suele ser gratuita. Es aconsejable acercarse al Enac para obtener más información.

