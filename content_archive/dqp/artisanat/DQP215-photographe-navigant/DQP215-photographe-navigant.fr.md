﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP215" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artisanat" -->
<!-- var(title)="Photographe navigant" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artisanat" -->
<!-- var(title-short)="photographe-navigant" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/artisanat/photographe-navigant.html" -->
<!-- var(last-update)="2020-04-30 11:23:49" -->
<!-- var(url-name)="photographe-navigant" -->
<!-- var(translation)="None" -->


# Photographe navigant

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-30 11:23:49<!-- end-var -->

## 1°. Définition de l’activité

L'activité de photographe navigant consiste pour le professionnel à réaliser des clichés à bord d'aéronefs de l'aviation civile.

*Pour aller plus loin* : article 1 de l'arrêté du 8 juillet 1955 relatif au brevet et licence de photographe navigant professionnel de l'aéronautique civile.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer son activité, le photographe navigant doit :

* être titulaire d'un brevet ou d'une licence de photographe navigant professionnel de l'aéronautique civile. La licence est exigée dès lors que le professionnel exerce cette activité à titre principal et habituel au sein d'un aéronef équipé spécialement pour la photographie aérienne ;
* être inscrit sur le registre C du personnel navigant professionnel catégorie Travail aérien (C-T, A) (cf. infra « Inscription au registre du personnel navigant professionnel »).

#### Formation

Pour obtenir le brevet et la licence de photographe navigant, le professionnel doit :

* être âgé de vingt et un ans révolus ;
* satisfaire aux conditions médicales d'aptitude (standards d'aptitude) suivantes : condition d'aptitude générale, condition de vision, condition de perception des couleurs, conditions d'audition, selon les modalités fixées à l'article 14 de l'arrêté du 8 juillet 1955 ;
* totaliser 50 heures de vol à bord d'un aéronef en qualité de photographe et notamment en qualité de stagiaire ;
* avoir satisfait aux examens théoriques et pratiques organisés par l'École nationale de l'aviation civile ([Enac](http://www.enac.fr/)) ;
* fournir un extrait de son contrat de travail ou une promesse de contrat justifiant de son engagement en tant que photographe navigant ou s'il travaille à son compte, une autorisation de travail dans le secteur de la photographie aérienne.

Les examens théoriques sont composés d'épreuves écrite et orales notées sur vingt. Les épreuves pratiques sont quant à elles effectuées au sol et en vol.

**Renouvellement de la licence** 

La licence de photographe navigant professionnel est valable douze mois et peut être renouvelée pour la même période dès lors que le professionnel :

* remplit les conditions d'aptitudes fixées à l'article 14 de l'arrêté du 8 juillet 1955 ;
* justifie avoir effectué 15 heures de vols dans les douze mois précédant sa demande de renouvellement en qualité de photographe navigant ou de membre d'équipage de conduite d'un aéronef affecté à la photographie aérienne. À défaut, le professionnel devra subir un contrôle portant sur les épreuves pratiques en vol, devant un instructeur ;

*Pour aller plus loin* : arrêté du 8 juillet 1955 précité.

#### Inscription au registre du personnel navigant professionnel

**Autorité compétente** 

Le professionnel titulaire d'un brevet et d'une licence de photographe navigant doit adresser une demande d'inscription auprès de la Direction générale de l'aviation civile (DGAC).

**Pièces justificatives** 

Sa demande doit comporter le [formulaire Cerfa 47-0049](https://www.formulaires.modernisation.gouv.fr/gf/Cerfa_15792.do) rempli et signé ainsi que les documents suivants :

* une photocopie de sa pièce d'identité en cours de validité ;
* un bulletin n° 3 de son casier judiciaire datant de moins de trois mois ;
* le numéro, la nature et la date de délivrance de sa licence ;
* tout document justifiant qu'il satisfait aux conditions médicales requises ;
* lorsque le demandeur est un militaire, un certificat de l'autorité militaire attestant qu'il est autorisé à exercer une activité principale rémunérée en dehors de l'armée ;
* une déclaration sur l'honneur de non appartenance à la fonction publique (sauf pour les fonctionnaires justifiant d'une mise en disposition hors cadre ou d'une mise en disponibilité pour une durée minimale de six mois) ;
* lorsque le professionnel agit pour le compte d'autrui, une copie du contrat de travail (certifiée conforme) mentionnant la date d'engagement du demandeur en qualité de navigant ;
* lorsque le professionnel travaille à son compte, un extrait de son inscription au registre du commerce et des sociétés (RCS) ou à défaut, une attestation de déclaration aux services fiscaux pour son activité aérienne.

**Procédure** 

Une fois la demande reçue complète, la DGAC procède à l'enregistrement du professionnel.

**À noter**

L'inscription au registre peut être suspendue soit :

* à la demande du professionnel dès qu'il cesse d'exercer son activité en raison d'un accident de travail ou d'une maladie ;
* d'office dès lors que le professionnel a cessé d'exercer son activité pendant plus d'un an (excepté si l'arrêt est dû à un accident de travail ou une maladie).

*Pour aller plus loin* : article D. 421-2 du Code de l'aviation civile ; arrêté du 21 janvier 1988 relatif aux modalités d'inscription aux registres du personnel navigant professionnel de l'aéronautique civile.

#### Coûts associés à la qualification

La formation menant au brevet et à la licence de photographe navigant est le plus souvent gratuite. Il est conseillé de se rapprocher de l'Enac pour de plus amples informations.
