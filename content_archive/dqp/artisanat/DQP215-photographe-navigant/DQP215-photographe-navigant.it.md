﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP215" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artigianato" -->
<!-- var(title)="Fotografo del biglietto aereo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artigianato" -->
<!-- var(title-short)="fotografo-del-biglietto-aereo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/it/dqp/artigianato/fotografo-del-biglietto-aereo.html" -->
<!-- var(last-update)="2020-04-15 17:20:35" -->
<!-- var(url-name)="fotografo-del-biglietto-aereo" -->
<!-- var(translation)="Auto" -->


Fotografo del biglietto aereo
=============================

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:20:35<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

L'attività di un fotografo di volo è costituita dal professionista per scattare foto a bordo di aerei dell'aviazione civile.

*Per andare oltre* Articolo 1 del decreto dell'8 luglio 1955 sulla brevetto e la licenza di un equipaggio professionale nell'aviazione civile.

Due gradi. Qualifiche professionali
-----------------------------------

### a. Requisiti nazionali

#### Legislazione nazionale

Per svolgere la sua attività, il fotografo di volo deve:

- licenza come fotografo aereo professionista nell'aviazione civile. La licenza è necessaria se il professionista svolge questa attività come principale e consuetudine in un aeromobile appositamente attrezzato per la fotografia aerea;
- essere registrati nel registro C dell'equipaggio professionale nella categoria Air Work (C-T, A) (vedi sotto "Registrazione del registro di Professional Aircrew").

#### Formazione

Per ottenere la licenza e la licenza del pilota, il professionista deve:

- Avere ventun anni;
- soddisfare i seguenti requisiti di idoneità medica (standard di idoneità): requisiti generali di idoneità, condizione di vista, condizione di percezione del colore, condizioni uditive, secondo i termini e le condizioni stabiliti nell'articolo 14 dell'ottavo ordine luglio 1955;
- Totale 50 ore di volo su un aereo come fotografo, anche come stagista;
- hanno soddisfatto gli esami teorici e pratici organizzati dalla Scuola Nazionale di Aviazione Civile ([Enac](http://www.enac.fr/)) ;
- fornire un estratto dal suo contratto di lavoro o una promessa di contratto che giustifica il suo lavoro come fotografo di volo o, se è un lavoratore autonomo, un permesso di lavoro nel settore della fotografia aerea.

Gli esami teorici sono composti da prove scritte e orali segnate su venti. Test pratici vengono effettuati a terra e in volo.

**Rinnovo della licenza**

La licenza del fotografo di volo professionale è valida per dodici mesi e può essere rinnovata per lo stesso periodo, purché il professionista:

- soddisfa i requisiti di attitudine stabiliti nell'articolo 14 del decreto dell'8 luglio 1955;
- giustifica aver volato 15 ore nei 12 mesi precedenti la sua richiesta di rinnovo come fotografo di volo o membro dell'equipaggio di volo di un aeromobile assegnato alla fotografia aerea. In caso contrario, il professionista dovrà sottoporsi a un controllo sui test pratici di volo di fronte a un istruttore;

*Per andare oltre* : decreto dell'8 luglio 1955, sopra.

#### Iscrizione al registro degli equipaggi professionisti

**Autorità competente**

Il professionista in possesso di un certificato e della licenza di un fotografo di volo deve applicarsi alla Direzione generale dell'aviazione civile (DGAC).

**Documenti di supporto**

La sua domanda deve includere[Cerfa forma 47-0049](https://www.formulaires.modernisation.gouv.fr/gf/Cerfa_15792.do) completati e firmati, nonché i seguenti documenti:

- Una fotocopia del suo documento d'identità valido
- una carta di rapporto numero 3 della sua fedina penale di meno di tre mesi;
- il numero, la natura e la data della licenza;
- qualsiasi documentazione che giustifichi il rispetto delle condizioni mediche richieste;
- Nel caso in cui il richiedente sia un membro militare, un certificato dell'autorità militare che attestava la sua autorizzazione a svolgere un'attività principale retribuita al di fuori dell'esercito;
- una dichiarazione sull'onore della mancata appartenenza al servizio pubblico (fatta eccezione per i dipendenti pubblici che giustificano la fornitura o il licenziamento per un minimo di sei mesi);
- Qualora il professionista agisca per conto di altri, una copia del contratto di lavoro (certificato conforme) menzionando la data di ingaggio del richiedente come marittimo;
- quando il professionista è un lavoratore autonomo, un estratto dalla sua iscrizione al Registro del Commercio e delle Imprese (RCS) o, in mancanza di ciò, un certificato di dichiarazione ai servizi fiscali per la sua attività aerea.

**Procedura**

Una volta che l'applicazione è completamente ricevuta, il DGAC registra il professionista.

**Si noti che**

La registrazione può essere sospesa:

- su richiesta del professionista non appena cessa di essere attivo a causa di un infortunio sul lavoro o di una malattia;
- pertanto, dal momento che il professionista ha cessato di lavorare per più di un anno (tranne se lo stop è dovuto a un infortunio sul lavoro o malattia).

*Per andare oltre* articolo D. 421-2 del codice dell'aviazione civile; decreto del 21 gennaio 1988 relativo alle procedure di registrazione degli equipaggi professionali nell'aviazione civile.

#### Costi associati alla qualifica

La formazione che porta alla licenza del brevetto e del fotografo di volo è di solito gratuita. Si consiglia di avvicinarsi all'Enac per ulteriori informazioni.

