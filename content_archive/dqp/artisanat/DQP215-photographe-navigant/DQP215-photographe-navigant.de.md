﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP215" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Handwerk" -->
<!-- var(title)="Airfare-Fotograf" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="handwerk" -->
<!-- var(title-short)="airfare-fotograf" -->
<!-- var(url)="https://www.guichet-qualifications.fr/de/dqp/handwerk/airfare-fotograf.html" -->
<!-- var(last-update)="2020-04-15 17:20:35" -->
<!-- var(url-name)="airfare-fotograf" -->
<!-- var(translation)="Auto" -->


Airfare-Fotograf
================

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:20:35<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

Die Tätigkeit eines Flugfotografen besteht darin, dass der Profi an Bord von Zivilluftfahrtflugzeugen fotografiert.

*Um weiter zu gehen* Artikel 1 des Dekrets vom 8. Juli 1955 über das Patent und die Lizenz einer professionellen Flugbesatzung in der Zivilluftfahrt.

Zwei Grad. Berufsqualifikationen
--------------------------------

### a. Nationale Anforderungen

#### Nationale Rechtsvorschriften

Zur Durchführung seiner Tätigkeit muss der Flugfotograf:

- ein Patent oder eine Lizenz als professioneller Luftfotograf in der Zivilluftfahrt besitzen. Die Lizenz ist erforderlich, wenn der Fachmann diese Tätigkeit als Haupt- und üblich in einem speziell für Luftaufnahmen ausgerüsteten Luftbild ausübt;
- im C-Register der professionellen Flugbesatzung in der Kategorie "Luftarbeit" (C-T, A) eingetragen werden (siehe unten "Registering the Register of Professional Aircrew").

#### Ausbildung

Um die Pilotenlizenz und -lizenz zu erhalten, muss der Fachmann:

- Seien Sie 21 Jahre alt;
- erfüllen die folgenden medizinischen Fitnessanforderungen (Fitnessstandards): allgemeine Eignungsanforderungen, Sehzustand, Farbwahrnehmungszustand, Hörbedingungen gemäß den Insertionsbedingungen nach Artikel 14 der 8. Ordnung Juli 1955;
- Insgesamt 50 Stunden Flugzeit in einem Flugzeug als Fotograf, auch als Praktikant;
- die theoretischen und praktischen Prüfungen der National School of Civil Aviation ([Enac](http://www.enac.fr/)) ;
- einen Auszug aus seinem Arbeitsvertrag oder ein Vertragsversprechen vorzulegen, das seine Anstellung als Flugfotograf rechtfertigt, oder, wenn er selbständig ist, eine Arbeitserlaubnis im Bereich der Luftaufnahmen.

Die theoretischen Prüfungen bestehen aus schriftlichen und mündlichen Prüfungen, die aus zwanzig bewertet wurden. Praktische Tests werden am Boden und im Flug durchgeführt.

**Lizenzerneuerung**

Die Lizenz des professionellen Flugfotografen ist zwölf Monate gültig und kann für den gleichen Zeitraum verlängert werden, solange der Fachmann:

- erfüllt die Eignungsanforderungen nach Artikel 14 des Dekrets vom 8. Juli 1955;
- rechtfertigt, dass er 15 Stunden in den 12 Monaten vor seiner Verlängerungsbewerbung als Flugfotograf oder Flugbesatzungsmitglied eines Flugzeugs geflogen ist, das der Luftaufnahme zugeordnet ist. Andernfalls muss sich der Fachmann einer Kontrolle der praktischen Flugtests vor einem Instruktor unterziehen;

*Um weiter zu gehen* : Dekret vom 8. Juli 1955, oben.

#### Eintragung in das Register der professionellen Flugbesatzung

**Zuständige Behörde**

Der Fachmann, der über eine Bescheinigung und eine Flugfotografenlizenz verfügt, muss bei der Generaldirektion Zivilluftfahrt (GDAC) gelten.

**Belege**

Sein Antrag muss die[Cerfa Form 47-0049](https://www.formulaires.modernisation.gouv.fr/gf/Cerfa_15792.do) sowie die folgenden Dokumente:

- Eine Fotokopie seines gültigen Ausweises
- eine Meldekarte Nummer 3 seines Vorstrafenregisters, die weniger als drei Monate alt ist;
- Anzahl, Art und Datum der Lizenzierung;
- jede Dokumentation, die es rechtfertigt, dass sie die erforderlichen medizinischen Bedingungen erfüllt;
- Ist der Antragsteller Militärangehöriger, so eine Bescheinigung der Militärbehörde, aus der hervorgeht, dass er befugt ist, eine bezahlte Haupttätigkeit außerhalb des Militärs auszuüben;
- eine Erklärung über die Ehrung der Nichtmitgliedschaft im öffentlichen Dienst (mit Ausnahme von Beamten, die eine außervertragliche Bestimmung oder Entlassung für mindestens sechs Monate rechtfertigen);
- Handelt der Berufstätige im Namen anderer, so wird eine Kopie des Arbeitsvertrags (als konform bescheinigt) unter Angabe des Datums des Engagements des Antragstellers als Seemann erwähnt;
- wenn der Berufstätige selbständig ist, ein Auszug aus seiner Eintragung in das Handels- und Gesellschaftsregister (RCS) oder, falls dies nicht der Fall ist, eine Bescheinigung über die Erklärung an die Steuerbehörden für seine Lufttätigkeit.

**Verfahren**

Sobald der Antrag vollständig eingegangen ist, registriert die DGAC den Fachmann.

**Beachten Sie, dass**

Die Registrierung kann entweder ausgesetzt werden:

- auf Antrag des Berufstätigen, sobald er aufgrund eines arbeitsbedingten Unfalls oder einer Krankheit nicht mehr tätig ist;
- daher, da der Berufstätige seit mehr als einem Jahr nicht mehr arbeitet (außer wenn die Arbeitsniederlegung auf einen Arbeitsunfall oder eine Krankheit zurückzuführen ist).

*Um weiter zu gehen* Artikel D. 421-2 des Zivilluftfahrtgesetzbuches; Dekret vom 21. Januar 1988 über die Verfahren zur Registrierung von Berufsbesatzungen in der Zivilluftfahrt.

#### Mit der Qualifizierung verbundene Kosten

Schulungen, die zum Patent und zur Lizenz des Flugfotografen führen, sind in der Regel kostenlos. Es ist ratsam, näher an die Enac für weitere Informationen zu kommen.

