﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP215" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Skilled trades" -->
<!-- var(title)="Airfare photographer" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="skilled-trades" -->
<!-- var(title-short)="airfare-photographer" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/skilled-trades/airfare-photographer.html" -->
<!-- var(last-update)="2020-04-15 17:20:35" -->
<!-- var(url-name)="airfare-photographer" -->
<!-- var(translation)="Auto" -->


Airfare photographer
====================

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:20:35<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Defining the activity
------------------------

The activity of a flight photographer consists of the professional to take pictures on board civil aviation aircraft.

*To go further* Article 1 of the July 8, 1955 decree on the patent and license of a professional aircrew in civil aviation.

Two degrees. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out his activity, the flight photographer must:

- hold a patent or license as a professional air photographer in civil aviation. The licence is required if the professional performs this activity as a principal and usual in an aircraft specially equipped for aerial photography;
- be registered on the C register of professional aircrew in the Air Work category (C-T, A) (see below "Registering the Register of Professional Aircrew").

#### Training

To obtain the pilot's license and licence, the professional must:

- Be twenty-one years old;
- meet the following medical fitness requirements (fitness standards): general fitness requirement, vision condition, colour perception condition, hearing conditions, according to the terms and conditions set out in Article 14 of the 8th order July 1955;
- Total 50 hours of flying time on an aircraft as a photographer, including as an intern;
- have met the theoretical and practical examinations organised by the National School of Civil Aviation ([Enac](http://www.enac.fr/)) ;
- provide an excerpt from his employment contract or a contract promise justifying his employment as a flight photographer or if he is self-employed, a work permit in the aerial photography sector.

The theoretical examinations are composed of written and oral tests scored out of twenty. Practical tests are carried out on the ground and in flight.

**Licence renewal**

The professional flight photographer's licence is valid for twelve months and can be renewed for the same period as long as the professional:

- fulfils the aptitude requirements set out in Article 14 of the 8 July 1955 decree;
- justifies having flown 15 hours in the 12 months prior to his renewal application as a flight photographer or flight crew member of an aircraft assigned to aerial photography. Failing that, the professional will have to undergo a check on practical flight tests in front of an instructor;

*To go further* : decree of July 8, 1955, above.

#### Registration in the register of professional aircrew

**Competent authority**

The professional holding a certificate and a flight photographer's licence must apply to the Directorate General of Civil Aviation (DGAC).

**Supporting documents**

His application must include the[Cerfa form 47-0049](https://www.formulaires.modernisation.gouv.fr/gf/Cerfa_15792.do) completed and signed as well as the following documents:

- A photocopy of his valid ID
- a report card number 3 of his criminal record less than three months old;
- The number, nature and date of licensing;
- any documentation justifying that it meets the required medical conditions;
- Where the applicant is a military member, a certificate from the military authority certifying that he or she is authorized to carry out a paid main activity outside the military;
- a declaration on the honour of non-membership in the public service (except for public servants justifying out-of-frame provision or layoff for a minimum of six months);
- Where the professional acts on behalf of others, a copy of the employment contract (certified as compliant) mentioning the applicant's date of engagement as a seafarer;
- when the professional is self-employed, an extract from his registration in the Register of Trade and Companies (RCS) or, failing that, a certificate of declaration to the tax services for his air activity.

**Procedure**

Once the application is fully received, the DGAC registers the professional.

**Note that**

Registration may be suspended either:

- at the request of the professional as soon as he ceases to be active due to a work-related accident or illness;
- therefore, since the professional has ceased to work for more than a year (except if the stoppage is due to a work accident or illness).

*To go further* Article D. 421-2 of the Civil Aviation Code; decree of 21 January 1988 relating to the procedures for registering professional aircrew in civil aviation.

#### Costs associated with qualification

Training leading to the patent and flight photographer's license is usually free. It is advisable to get closer to the Enac for more information.

