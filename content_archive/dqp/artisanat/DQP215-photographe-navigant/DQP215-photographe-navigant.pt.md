﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP215" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artesanato" -->
<!-- var(title)="Fotógrafo de passagens aéreas" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artesanato" -->
<!-- var(title-short)="fotografo-de-passagens-aereas" -->
<!-- var(url)="https://www.guichet-qualifications.fr/pt/dqp/artesanato/fotografo-de-passagens-aereas.html" -->
<!-- var(last-update)="2020-04-15 17:20:35" -->
<!-- var(url-name)="fotografo-de-passagens-aereas" -->
<!-- var(translation)="Auto" -->


Fotógrafo de passagens aéreas
=============================

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:20:35<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

A atividade de um fotógrafo de voo consiste no profissional para tirar fotos a bordo de aeronaves da aviação civil.

*Para ir mais longe* Artigo 1º do decreto de 8 de julho de 1955 sobre a patente e licença de uma tripulação profissional na aviação civil.

Dois graus. Qualificações profissionais
---------------------------------------

### a. Requisitos nacionais

#### Legislação nacional

Para realizar sua atividade, o fotógrafo de voo deve:

- possuem uma patente ou licença como fotógrafo aéreo profissional na aviação civil. A licença é exigida se o profissional realizar essa atividade como diretor e habitual em uma aeronave especialmente equipada para fotografia aérea;
- estar registrado no registro C de tripulantes profissionais na categoria Trabalho Aéreo (C-T, A) (veja abaixo "Registrando o Registro de Aeronaves Profissionais").

#### Treinamento

Para obter a licença e licença do piloto, o profissional deve:

- Ter vinte e um anos de idade;
- atender aos seguintes requisitos de aptidão médica (padrões de aptidão): exigência geral de aptidão, condição de visão, condição de percepção de cores, condições auditivas, de acordo com os termos e condições estabelecidos no artigo 14 da 8ª ordem julho de 1955;
- Total de 50 horas de voo em uma aeronave como fotógrafo, inclusive como estagiário;
- atenderam aos exames teóricos e práticos organizados pela Escola Nacional de Aviação Civil ([Enac](http://www.enac.fr/)) ;
- fornecer um trecho de seu contrato de trabalho ou uma promessa de contrato justificando seu emprego como fotógrafo de voo ou se ele é autônomo, uma licença de trabalho no setor de fotografia aérea.

Os exames teóricos são compostos por provas escritas e orais pontuadas a partir de vinte. Testes práticos são realizados no solo e em vôo.

**Renovação de licença**

A licença do fotógrafo de voo profissional é válida por doze meses e pode ser renovada pelo mesmo período que o profissional:

- cumpre os requisitos de aptidão previstos no artigo 14.o do decreto de 8 de Julho de 1955;
- justifica ter voado 15 horas nos 12 meses anteriores ao seu pedido de renovação como fotógrafo de voo ou membro da tripulação de uma aeronave designada para fotografia aérea. Caso contrário, o profissional terá que passar por uma verificação em testes práticos de voo na frente de um instrutor;

*Para ir mais longe* : decreto de 8 de julho de 1955, acima.

#### Inscrição no registro de tripulantes profissionais

**Autoridade competente**

O profissional portando certificado e uma licença de fotógrafo de voo deve se inscrever na Direção Geral de Aviação Civil (DGAC).

**Documentos de suporte**

Sua aplicação deve incluir o[Cerfa forma 47-0049](https://www.formulaires.modernisation.gouv.fr/gf/Cerfa_15792.do) concluídos e assinados, bem como os seguintes documentos:

- Uma fotocópia de sua identidade válida
- um boletim número 3 de sua ficha criminal com menos de três meses de idade;
- O número, a natureza e a data do licenciamento;
- qualquer documentação que justifique que ele atenda às condições médicas necessárias;
- Quando o requerente for militar, um certificado da autoridade militar certificando que ele ou ela está autorizado a realizar uma atividade principal remunerada fora do exército;
- uma declaração sobre a honra de não adesão ao serviço público (exceto para os servidores públicos que justificam a provisão ou demissão fora de prazo por um mínimo de seis meses);
- Quando o profissional atua em nome de terceiros, uma cópia do contrato de trabalho (certificado como compatível) mencionando a data de contratação do requerente como marinheiro;
- quando o profissional é autônomo, extrato de seu registro no Registro de Comércio e Empresas (RCS) ou, caso contrário, um certificado de declaração aos serviços fiscais para sua atividade aérea.

**Procedimento**

Uma vez que a solicitação é totalmente recebida, o DGAC registra o profissional.

**Note que**

As inscrições podem ser suspensas:

- a pedido do profissional, assim que ele deixar de ser ativo devido a um acidente ou doença relacionada ao trabalho;
- portanto, uma vez que o profissional deixou de trabalhar por mais de um ano (exceto se a paralisação for por acidente de trabalho ou doença).

*Para ir mais longe* Artigo D. 421-2 do Código de Aviação Civil; decreto de 21 de janeiro de 1988 relativo aos procedimentos de registro de tripulação profissional na aviação civil.

#### Custos associados à qualificação

O treinamento que leva à patente e à licença de fotógrafo de voo geralmente é gratuito. É aconselhável se aproximar do Enac para mais informações.

