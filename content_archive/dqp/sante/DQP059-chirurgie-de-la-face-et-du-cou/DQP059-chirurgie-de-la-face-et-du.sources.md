# **QP059 CHIRURGIE DE LA FACE ET DU COU (MEDECIN SPECIALISTE)**

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000033975530&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000033897070&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000036515567&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000033896785&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000033897070&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000033896143&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000021342930&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020930148&categorieLien=id

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000006525231&cidTexte=LEGITEXT000006071191&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000006911737&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/eli/arrete/2011/3/22/ESRS1106857A/jo

https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027343762&categorieLien=id

https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000036237037

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000033896785&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000033896894&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000006196408&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000006196409&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000006196410&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000006198778&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000006198779&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000006198780&cidTexte=LEGITEXT000006072665&dateTexte=20180530
 
https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000006198781&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000006198782&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000006196412&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000035971051&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000020897548&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000032886528&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000029000436&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000025076559&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idSectionTA=LEGISCTA000021503628&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000021536345&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000035971051&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000022052461&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A255A8C4AA06F0BFEDBE87EC16CEE9F5.tplgfr40s_2?idArticle=LEGIARTI000006912524&cidTexte=LEGITEXT000006072665&dateTexte=20180530

https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021776754&dateTexte=&categorieLien=id

https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021906923&dateTexte=20180523


