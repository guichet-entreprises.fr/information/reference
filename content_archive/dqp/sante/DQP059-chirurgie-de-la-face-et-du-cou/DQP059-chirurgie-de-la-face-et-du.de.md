﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP059" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Gesundheit" -->
<!-- var(title)="Gesichts- und Nackenchirurgie" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="gesundheit" -->
<!-- var(title-short)="gesichts-und-nackenchirurgie" -->
<!-- var(url)="https://www.guichet-qualifications.fr/de/dqp/gesundheit/gesichts-und-nackenchirurgie.html" -->
<!-- var(last-update)="2020-04-15 17:21:19" -->
<!-- var(url-name)="gesichts-und-nackenchirurgie" -->
<!-- var(translation)="Auto" -->


Gesichts- und Nackenchirurgie
=============================

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:21:19<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

Der Gesichts- und Halschirurg ist ein qualifizierter Spezialist in der Behandlung von schlaffe und schlaffe Haut und Muskeln des Gesichts und Halses. Der Chirurg greift bei Rhinoplastik, Facelift, abgelösten Ohren oder Gesichtslähmung ein.

Es ist eine plastische Chirurgie (hauptsächlich in Bezug auf die Haut und nicht-viszerale Weichteile) für ästhetische oder restaurative Zwecke.

Zwei Grad. Berufsqualifikationen
--------------------------------

### a. Nationale Anforderungen

#### Nationale Rechtsvorschriften

Nach Art. L. 4111-1 des Gesetzes über die öffentliche Gesundheit müssen die Betroffenen, um in Frankreich als Arzt rechtlich zu praktizieren, kumulativ die folgenden drei Voraussetzungen erfüllen:

- das französische Staatsdiplom des Doktors der Medizin oder ein Diplom, eine Bescheinigung oder einen anderen Titel gemäß Artikel L. 4131-1 des Kodex für öffentliche Gesundheit besitzen (siehe infra "Gut zu wissen: automatische Anerkennung des Diploms");
- französischer Staatsangehörigkeit, andorranischer Staatsangehörigkeit oder Staatsangehöriger eines Mitgliedstaats der Europäischen Union (EU) oder Vertragspartei des Abkommens über den Europäischen Wirtschaftsraum (EWR) oder Marokko sein, vorbehaltlich der Anwendung der Vorschriften aus dem Gesundheitsgesetzbuch öffentlichen oder internationalen Verpflichtungen. Dieser Zustand gilt jedoch nicht für einen Arzt mit dem französischen Staatsdiplom des Arztes der Medizin;
- mit Ausnahmen in den Vorstand eines der Abteilungsräte des Ärztekollegiums aufgenommen werden (siehe infra 5o. a. "Bitte um Aufnahme in die Reihenfolge der Ärzteliste").

Personen, die die Qualifikationen des Diploms oder der Staatsangehörigkeit nicht erfüllen, können jedoch auf individuellen Befehl des für Gesundheit zuständigen Ministers als Arzt praktizieren (siehe unten: "5. c. Falls erforderlich, eine individuelle Übungserlaubnis beantragen").

*Um weiter zu gehen* Artikel L. 4111-1, L. 4112-6, L. 4112-7 und L. 4131-1 des Gesetzes über die öffentliche Gesundheit.

**Beachten Sie, dass**

Ohne all diese Bedingungen zu erfüllen, ist die Ausübung des Arztberufs illegal und mit zwei Jahren Haft und einer Geldstrafe von 30.000 Euro geahndet.

*Um weiter zu gehen* Artikel L. 4161-1 und L. 4161-5 des Gesetzes über die öffentliche Gesundheit.

**Gut zu wissen: Automatische Diplomanerkennung**

Nach Art. L. 4131-1 des Gesundheitsgesetzbuchs können EU- oder EWR-Bürger als Arzt praktizieren, wenn sie einen der folgenden Titel besitzen:

- von einem EU- oder EWR-Staat im Einklang mit den EU-Verpflichtungen ausgestellte und im Anhang des Dekrets vom 13. Juli 2009 aufgeführte Dokumente, in dem Listen und Bedingungen für die Anerkennung der EU Ausbildung von Ärzten und Fachärzten, die von EU-Mitgliedstaaten oder Parteien der EWR-Vereinbarung gemäß Artikel L. 4131-1 des Gesetzes über die öffentliche Gesundheit ausgestellt wurden;
- ärztliche Ausbildungszeugnisse, die von einem EU- oder EWR-Staat im Einklang mit den EU-Verpflichtungen ausgestellt werden und nicht in der obigen Liste aufgeführt sind, wenn ihnen eine Bescheinigung dieses Staates beigefügt ist, die bescheinigt, dass sie die Ausbildung sanktionieren einhaltung dieser Verpflichtungen und werden von ihm mit den Ausbildungstiteln auf dieser Liste gleichgestellt;
- ärztliche Ausbildungszeugnisse, die von einer staatlichen ärztlichen Ausbildung der EU oder des EWR ausgestellt wurden, die in diesem Staat vor den Indenkten des oben genannten Dekrets und nicht im Einklang mit den EU-Verpflichtungen begonnen wurde, wenn sie mit den EU-Verpflichtungen ein Zertifikat eines dieser Staaten, aus dem hervorgeht, dass sich der Inhaber der Ausbildungstitel in diesem Staat in diesem Staat mindestens drei aufeinanderfolgende Jahre lang tatsächlich und rechtmäßig der Ausübung des Arztberufs in der betreffenden Fachrichtung gewidmet hat. fünf Jahre vor der Ausstellung des Zertifikats;
- Arztausbildungszeugnisse der ehemaligen Tschechoslowakei, der ehemaligen Sowjetunion oder des ehemaligen Jugoslawiens oder die vor dem Unabhängigkeitsdatum der Tschechischen Republik, der Slowakei, Estlands begonnene Ausbildung sanktionieren, Lettland, Litauen oder Slowenien, wenn ihnen eine Bescheinigung der zuständigen Behörden eines dieser Staaten beigefügt ist, die bescheinigt, dass sie die gleiche Rechtsgültigkeit haben wie die von diesem Staat ausgestellten Ausbildungsdokumente. Dieser Bescheinigung ist eine Bescheinigung beigefügt, aus der die gleiche Bescheinigung hervorgeht, dass der Inhaber in diesem Staat den Beruf des Arztes in der betreffenden Fachrichtung mindestens drei aufeinanderfolgende Jahre in diesem Staat ausgeübt hat. fünf Jahre vor der Ausstellung des Zertifikats;
- ärztliche Ausbildungszeugnisse, die von einem EU- oder EWR-Staat ausgestellt wurden, die nicht in der oben genannten Liste aufgeführt sind, wenn ihnen eine von den zuständigen Behörden dieses Staates ausgestellte Bescheinigung beigefügt ist, aus der hervorgeht, dass der Inhaber des Ausbildungszeugnisses niedergelassen ist. in seinem Hoheitsgebiet zum in dem vorgenannten Dekret genannten Zeitpunkt und dass es das Recht erworben hat, die Tätigkeit eines Allgemeinmediziners im Rahmen seines nationalen Systems der sozialen Sicherheit auszuüben;
- ärztliche Ausbildungszeugnisse, die von einer staatlichen ärztlichen Ausbildung der EU oder des EWR ausgestellt wurden, die in diesem Staat vor den Indenkten des oben genannten Dekrets begonnen wurde und nicht im Einklang mit den EU-Verpflichtungen, den Beruf des Arztes in dem Staat auszuüben, der sie ausgestellt hat, wenn der Arzt rechtfertigt, in frankreich in den letzten fünf Jahren drei aufeinander folgende Vollzeitjahre von Krankenhausfunktionen in der entsprechenden Spezialität durchgeführt zu haben Ausbildung zum Associate Attaché, Associate Practitioner, Associate Assistant oder akademischen Funktionen als Associate Clinic Head of Universities oder Associate Assistant of Universities, sofern er ernannt wurde Krankenhausfunktionen gleichzeitig;
- Italiens ärztliche Fachausbildungszeugnisse auf der vorgenannten Liste sanktionieren die medizinische Fachausbildung, die in diesem Staat nach dem 31. Dezember 1983 und vor dem 1. Januar 1991 begonnen wurde, wenn sie von einer Bescheinigung der staatlichen Behörden, aus der hervorgeht, dass ihr Inhaber in diesem Staat in den zehn Jahren vor zehn Jahren tatsächlich und rechtmäßig den Beruf des Arztes in der betreffenden Fachrichtung ausgeübt hat Ausstellung des Zertifikats.

*Um weiter zu gehen* Artikel L. 4131-1 des Gesetzes über die öffentliche Gesundheit; 13. Juli 2009 Dekret mit Listen und Bedingungen für die Anerkennung von ärztlichen und fachärztlichen Ausbildungszeugnissen, ausgestellt von EU-Mitgliedstaaten oder Parteien der EWR-Vereinbarung, die unter Artikel L. 4131-1 des Gesundheitsgesetzbuchs fallen Öffentlich.

#### Ausbildung

Das Medizinstudium besteht aus drei Zyklen mit einer Gesamtdauer von neun bis elf Jahren, je nach gewähltem Kurs.

Die Ausbildung, die an der Universität stattfindet, umfasst viele Praktika und wird durch zwei Wettbewerbe unterbrochen:

- die erste tritt am Ende des ersten Jahres auf. Dieses Studienjahr, genannt das "erste Jahr der gemeinsamen Gesundheitsstudien" (PACES) ist üblich für Studenten in Medizin, Pharmazie, Odontologie, Physiotherapie und Hebammen. Am Ende dieses ersten Wettbewerbs werden die Schüler nach ihren Ergebnissen gewertet. Diejenigen, die unter dem Numerus clausus einen nützlichen Rang haben, dürfen ihr Studium fortsetzen und gegebenenfalls eine Ausbildung zur Praxis der Medizin fortsetzen;
- der zweite tritt am Ende des zweiten Zyklus (d. h. am Ende des sechsten Studienjahres) auf: Dieser Wettbewerb wird als "National Class Tests" (ECN) bezeichnet und wurde früher "Boarding School" genannt. Am Ende dieses Wettbewerbs wählen die Studierenden auf der Grundlage ihres Rankings ihre Spezialität und/oder ihre Einsatzstadt aus. Die Dauer der folgenden Studien hängt von der gewählten Fachrichtung ab.

Um einen Staatsabschluss (DE) als Doktor der Medizin zu erlangen, muss der Student alle seine Praktika, sein Diplom der Fachwissenschaften (DES) validieren und seine Abschlussarbeit erfolgreich unterstützen.

*Um weiter zu gehen* Artikel L. 632-1 des Bildungsgesetzbuches.

**Gut zu wissen**

Medizinstudenten müssen Obligatorische Impfungen durchführen. Weitere Informationen finden Sie in Abschnitt R. 3112-1 des Gesundheitsgesetzes.

**Allgemeindiplom in Medizinwissenschaften**

Der erste Zyklus wird durch das allgemeine Ausbildungsdiplom in Denkwissenschaften sanktioniert. Es besteht aus sechs Semestern und entspricht dem Lizenzniveau. Die ersten beiden Semester entsprechen THE PACES.

Ziel der Ausbildung ist es,

- Erwerb der wissenschaftlichen Grundkenntnisse, die für die anschließende Beherrschung des für die Ausübung medizinischer Berufe notwendigen Wissens und Know-hows unerlässlich sind. Diese wissenschaftliche Basis ist breit und umfasst die Biologie, bestimmte Aspekte der exakten Wissenschaften und verschiedene Disziplinen der Geistes- und Sozialwissenschaften;
- der grundlegende Ansatz des gesunden Mannes und des kranken Mannes, einschließlich aller Aspekte der Semiologie.

Es umfasst theoretische, methodische, angewandte und praktische Lehren sowie den Abschluss von Praktika einschließlich eines vierwöchigen Einführungskurses in einem Krankenhaus.

*Um weiter zu gehen* : Beschluss vom 22. März 2011 über das Bildungsprogramm für das allgemeine Ausbildungsdiplom in den Medizinischen Wissenschaften.

**Vertiefte Ausbildung in Medizinwissenschaften**

Der zweite Zyklus des medizinwissenschaftlichen Studiums wird durch das Diplom der vertieften Ausbildung in den Medizinischen Wissenschaften sanktioniert. Es besteht aus sechs Semestern Ausbildung und entspricht dem Master-Niveau.

Sein Ziel ist es, die allgemeinen Fähigkeiten zu erwerben, die es den Studierenden ermöglichen, anschließend postgraduale Aufgaben in Krankenhäusern und ambulanten Einrichtungen wahrzuführen und die beruflichen Fähigkeiten der Ausbildung in sie werden sich während ihrer Spezialisierung verpflichten.

Die Fähigkeiten, die erworben werden müssen, sind die von Kommunikator, Kliniker, Mitarbeiter, Mitglied eines multiprofessionellen Gesundheitsteams, Akteur im Bereich der öffentlichen Gesundheit, Wissenschaftler und ethischer und ethischer Führer. Der Schüler muss auch lernen, reflexiv zu sein.

Die Lektionen konzentrieren sich auf das, was häufig oder ernst ist oder ein Problem der öffentlichen Gesundheit und was klinisch vorbildlich ist.

Die Ziele der Ausbildung sind:

- den Erwerb von Wissen über pathophysiologische Prozesse, Pathologie, therapeutische Grundlagen und Prävention, die die im vorangegangenen Zyklus erworbenen Prozesse ergänzen und vertiefen;
- Ausbildung im wissenschaftlichen Prozess
- Klinische Serthepierung
- allgemeine Fähigkeiten zur Vorbereitung auf das postgraduale Medizinstudium.

Neben theoretischen und praktischen Lehren umfasst die Ausbildung den Abschluss von 36 Monaten Praktikum und 25 Wachleuten.

*Um weiter zu gehen* : Beschluss vom 8. April 2013 über den Lehrplan für den ersten und zweiten Studienzyklus.

**Fachdiplom (DES)**

Da die[Gestoppt](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000036237037) November 27, 2017 nach der Reform der postgradualen medizinischen Studien, das Diplom der Spezialisierten Studien (DES) ermöglicht es dem Arzt, die Spezialität seiner Wahl zu praktizieren.

Um die Spezialität der Gesichts- und Halschirurgie zu praktizieren, muss der Fachmann die DES Oto-Rhino-Laryngologie (ORL) - Zerviko-Gesichtschirurgie (CCF) - halten.

Diese DES besteht aus zwölf Semestern, darunter:

- mindestens neun in der Fachrichtung, darunter mindestens fünf in einem Praktikum mit Universitätsaufsicht;
- mindestens drei an einem Praktikumsplatz ohne universitäre Aufsicht.

**Beachten Sie, dass**

Während der Ausbildung kann sich der Student an einer der folgenden fachlichen Querschnittsausbildungen (TSF) einschreiben:

- Onkologie;
- orbito-palpebro-lacrymal Chirurgie;
- allergische Erkrankung
- Schlafen.

Das DES gliedert sich in drei Phasen: die Basisphase, die Vertiefungsphase und die Konsolidierungsphase.

**Die Basisphase**

Mit einem Zwei-Sem-Eins-Menü ermöglicht es dem Bewerber, die Grundkenntnisse der gewählten Spezialität zu erwerben und:

- ein Praktikum in einem lizenzierten Krankenhaus in der Entric- und Cervico-Gesichtschirurgie in einem Bereich außerhalb der funktionellen Explorationen;
- ein Praktikum in einem Hauptklinik in einer anderen chirurgischen Fachrichtung.

**Die Vertiefungsphase**

Mit einem Dauer von sechs Semestern ermöglicht es dem Bewerber, die Kenntnisse und Fähigkeiten zu erwerben, die notwendig sind, um die gewählte Spezialität zu praktizieren und:

- vier Praktika in einem lizenzierten Krankenhaus in HNO und CCF;
- ein Praktikum in einem zugelassenen Krankenhaus in Neurochirurgie, Kiefer-Gesichtschirurgie, Thorax- und Herz-Kreislauf-Chirurgie, Gefäßchirurgie, plastischer, rekonstruktiver und restauratorischer Chirurgie und als Komplementär ENT und CCF;
- ein kostenloses Praktikum.

**Die Konsolidierungsphase**

Der zweijährige Prozess ermöglicht es dem Kandidaten, sein Wissen zu konsolidieren und:

- ein einjähriges Praktikum an einem Senior Erz- und CCF-akkreditierten Standort;
- ein jahrbaue:- an einem Ort, der als Hauptverpflichteter in HNO und CCF zugelassen ist,
  + in Form eines gemischten oder gekoppelten Praktikums, an Orten und/oder bei einem Hochschulpraktikum, akkreditiert als Rektor in HNO und CCL.

*Um weiter zu gehen* : Anlage I und II des Dekrets vom 27. November 2017 zur Änderung des Dekrets vom 12. April 2017 über die Organisation des dritten Studienzyklus und des Dekrets vom 21. April 2017 über Kenntnisse, Fähigkeiten und Modelle der Diplomausbildung und Erstellung einer Liste dieser Diplome und der bereichsübergreifenden Fachoptionen und der Ausbildung des postgradualen Medizinstudiums.

#### Mit der Qualifizierung verbundene Kosten

Die Ausbildung, die zur Erlangung des DOKTOR der Medizin führte, ist bezahlt. Die Kosten variieren je nach den Universitäten, die die Lehren anbieten. Für weitere Informationen ist es ratsam, näher an die betreffende Universität zu kommen.

### b. EU- oder EWR-Bürger: für vorübergehende und gelegentliche Ausübungen (Kostenlose Erbringung von Dienstleistungen)

Ein Arzt, der Mitglied eines EU- oder EWR-Staates ist und in einem dieser Staaten niedergelassen ist und rechtlich tätig ist, kann in Frankreich vorübergehend und gelegentlich tätig werden, sofern er zuvor vorherige Erklärung an den Nationalrat des Ärztekollegiums (siehe infra "5o. b. eine vorherige Tätigkeitserklärung für EU- oder EWR-Bürger abgeben, die vorübergehende und gelegentliche Tätigkeiten ausüben").

**Was Sie wissen sollten**

Eine Registrierung auf dem Dienstplan des Ärztekollegiums ist für Ärzte in einer kostenlosen Dienstleistungssituation (LPS) nicht erforderlich. Infolgedessen sind sie nicht verpflichtet, Ordinalabgaben zu zahlen. Der Arzt wird einfach auf einer spezifischen Liste des Nationalrats der Ärztekammer registriert.

Der Voranmeldung ist eine Erklärung über die sprachkenntnissen Fähigkeiten beizufügen, die für die Erbringung des Dienstes erforderlich sind. In diesem Fall muss die Kontrolle der Sprachkenntnisse in einem angemessenen Verhältnis zu der Tätigkeit stehen, die nach Anerkennung der beruflichen Qualifikation durchzuführen und durchzuführen ist.

Wenn Ausbildungstitel keine automatische Erkennung erhalten (siehe s.o. "2.0"). a. Nationale Rechtsvorschriften") werden die beruflichen Qualifikationen des Anbieters überprüft, bevor die erste Dienstleistung erbracht wird. Bei erheblichen Unterschieden zwischen den Qualifikationen der betreffenden Person und der in Frankreich erforderlichen Ausbildung, die der öffentlichen Gesundheit schaden könnte, wird der Antragsteller einer Prüfung unterzogen.

Der Arzt in LPS-Situation ist verpflichtet, die in Frankreich geltenden Berufsregeln, einschließlich aller ethischen Regeln, einzuhalten (siehe infra "3." Ehrenbedingungen und ethische Regeln"). Sie unterliegt der Disziplinargerichtsbarkeit des Ärztekollegiums.

**Beachten Sie, dass**

Die Dienstleistung wird unter dem französischen Berufstitel Arzt durchgeführt. Werden jedoch Ausbildungsabschlüsse nicht anerkannt und qualifikationen nicht überprüft, so erfolgt die Leistung unter der Berufsbezeichnung des Betriebsstaats, um Verwechslungen zu vermeiden. Mit dem französischen Berufstitel.

*Um weiter zu gehen* Artikel L. 4112-7 des Gesetzes über die öffentliche Gesundheit.

### c. EU- oder EWR-Bürger: für eine ständige Ausübung (Freie Niederlassung)

**Die automatische Anerkennung von Diplomen, die in einem EU-Staat**

Artikel L. 4131-1 des Gesetzes über die öffentliche Gesundheit schafft eine Regelung für die automatische Anerkennung bestimmter Diplome oder Titel in Frankreich, falls vorhanden, zusammen mit Bescheinigungen, die in einem EU- oder EWR-Staat erworben wurden (siehe "supra"2). a. Nationale Rechtsvorschriften").

Es ist Sache des Fachrates der Ärztekollegium, die Ordnungsmäßigkeit der Diplome, Titel, Zeugnisse und Bescheinigungen zu überprüfen, die automatische Anerkennung zu gewähren und dann über den Antrag auf Eintragung in die Liste des Ordens zu entscheiden. .

*Um weiter zu gehen* Artikel L. 4131-1 des Gesetzes über die öffentliche Gesundheit; 13. Juli 2009 Dekret über die Listen und Bedingungen für die Anerkennung der Ausbildungsdokumente von Ärzten und Fachärzten, die von den Mitgliedstaaten der Europäischen Union oder den Parteien des Abkommens über den Europäischen Wirtschaftsraum gemäß dem 2. Abschnitt L. 4131-1 des Gesetzes über die öffentliche Gesundheit.

**Die abfällige Regelung: Vorabgenehmigung**

Hat der EU- oder EWR-Staatsangehörige keinen Anspruch auf die automatische Anerkennung seiner Befähigungsnachweise, so fällt er unter ein Genehmigungssystem (siehe unten "5o"). c. Falls erforderlich, eine individuelle Übungserlaubnis beantragen").

Personen, die keine automatische Anerkennung erhalten, aber eine Ausbildungsbezeichnung besitzen, um als Arzt juristisch zu praktizieren, können individuell in der betreffenden Spezialität praktizieren. Gesundheitsminister, nach Beratung durch eine Kommission, die sich aus Fachleuten zusammen.

Zeigt die Prüfung der durch die Ausbildungszeugnisse bescheinigten Berufsqualifikationen und die Berufserfahrung erhebliche Unterschiede zu den Für den Zugang zum Beruf in der betreffenden Fachrichtung erforderlichen Qualifikationen und seine Ausübung in Frankreich muss sich der Betroffene einer Entschädigungsmaßnahme unterwerfen.

Je nach dem in Frankreich erforderlichen Qualifikationsniveau und dem Niveau der betreffenden Person kann die zuständige Behörde entweder

- Dem Antragsteller die Wahl zwischen einem Anpassungskurs oder einem Eignungstest anbieten;
- einen Anpassungskurs oder Eignungstest auferlegen
- einen Anpassungskurs und einen Eignungstest auferlegen.

*Um weiter zu gehen* Artikel L. 4131-1-1 des Gesetzes über die öffentliche Gesundheit.

Drei Grad. Ehrenbedingungen, ethische Regeln, Ethik
---------------------------------------------------

### a. Einhaltung des Ethikkodex der Ärzte

Die Bestimmungen des Code of Medical Ethics sind von allen Ärzten, die in Frankreich praktizieren, unabhängig davon, ob sie im Verwaltungsrat des Ordens tätig sind oder von dieser Verpflichtung befreit sind (siehe oben: "5. a. Antrag auf Aufnahme in die Liste der Ärzte).

**Was Sie wissen sollten**

Alle Bestimmungen des Ethikkodex sind in den Abschnitten R. 4127-1 bis R. 4127-112 des Public Health Code kodifiziert.

Als solche muss der Arzt die Grundsätze der Moral, Der Wahrscheinlichkeit und des Engagements respektieren, die für die Praxis der Medizin unerlässlich sind. Er unterliegt auch der ärztlichen Schweigepflicht und muss selbständig trainieren.

*Um weiter zu gehen* Artikel R. 4127-1 bis R. 4127-112 des Gesetzes über die öffentliche Gesundheit.

### b. Kumulative Tätigkeiten

Der Arzt darf eine andere Tätigkeit nur ausüben, wenn eine solche Kombination mit den grundsätzen der beruflichen Unabhängigkeit und Würde vereinbar ist, die ihm auferlegt werden. Die Anhäufung von Aktivitäten sollte es ihm nicht erlauben, seine Rezepte oder seinen medizinischen Rat zu nutzen.

Somit kann der Arzt die medizinische Übung nicht mit einer anderen Gesundheitsaktivität kombinieren. Insbesondere ist es ihm untersagt, als Optiker, Sanitäter oder Manager eines Krankenwagenunternehmens, Hersteller oder Verkäufer von Medizinprodukten, Eigentümer oder Manager eines Hotels für Kuratoren, fitnessstudio, Spa, Massagepraxis zu praktizieren.

Ebenso ist es einem Arzt, der einen Wahl- oder Verwaltungsauftrag erfüllt, untersagt, ihn zur Erhöhung seiner Klientel zu verwenden.

*Um weiter zu gehen* Artikel R. 4127-26 und R. 4127-27 des Public Health Code.

### c. Ehrenbedingungen

Um zu praktizieren, muss der Arzt bescheinigen, dass kein Verfahren, das zu einer Verurteilung oder einer Sanktion führen könnte, die seine Aufnahme in die Kammer beeinträchtigen könnte, gegen ihn ist.

*Um weiter zu gehen* Artikel R. 4112-1 des Gesetzes über die öffentliche Gesundheit.

### d. Verpflichtung zur kontinuierlichen beruflichen Weiterentwicklung

Ärzte müssen an einem mehrjährigen kontinuierlichen Weiterbildungsprogramm teilnehmen. Das Programm konzentriert sich auf die Bewertung von Berufspraktiken, die Verbesserung der Fähigkeiten, die Verbesserung der Qualität und Sicherheit der Pflege, die Aufrechterhaltung und Aktualisierung von Kenntnissen und Fähigkeiten.

Alle Handlungen, die von Ärzten unter ihrer Verpflichtung zur professionellen Entwicklung durchgeführt werden, werden in einem spezifischen Dokument nachverfolgt, das die Einhaltung dieser Verpflichtung bescheinigt.

*Um weiter zu gehen* Artikel L. 4021-1 und die nachfolgenden Artikel R. 4021-4 und die folgenden Artikel des Gesetzes über die öffentliche Gesundheit.

### e. Körperliche Eignung

Ärzte dürfen nicht mit Gebrechen oder Pathologie, die mit der Berufspraxis unvereinbar sind, vorhanden sein (siehe infra "5.0). a. Antrag auf Aufnahme in die Liste der Ärzte).

*Um weiter zu gehen* Artikel R. 4112-2 des Gesetzes über die öffentliche Gesundheit.

Es ist ein Vier-Grad-Eins. Versicherung
---------------------------------------

### a. Verpflichtung zum Versicherungsschutz

Als Gesundheitsfachmann muss ein freizügiger Arzt eine Berufshaftpflichtversicherung abschließen.

Wenn er dagegen als Arbeitnehmer praktiziert, ist diese Versicherung nur optional. In diesem Fall ist es Sache des Arbeitgebers, eine solche Versicherung für seine Arbeitnehmer für die während ihrer beruflichen Tätigkeit begangenen Handlungen abzuschließen.

*Um weiter zu gehen* Artikel L. 1142-2 des Gesetzes über die öffentliche Gesundheit.

### b. Verpflichtung zur Eintrittinshilfe in die unabhängige Pensionskasse der Ärzte Frankreichs (CARMF)

Jeder Arzt, der im Verwaltungsrat des Ordens eingetragen ist und in liberaler Form praktiziert (auch in Teilzeit und auch wenn er auch beschäftigt ist), ist verpflichtet, dem CARMF beizutreten.

**Zeit**

Der Betroffene muss sich innerhalb eines Monats nach Beginn seiner tätigkeitsliberalen Tätigkeit beim CARMF anmelden.

**Bedingungen**

Die betroffene Person hat das vom Departmental Council des Ärztekollegiums ausgefüllte, datierte und gegengezeichnete Erklärungsformular zurückzugeben. Dieses Formular kann von der CARMF-Website heruntergeladen werden.

**Was Sie wissen sollten**

Im Falle einer Praxis in einer liberalen Praxisgesellschaft (SEL) ist die Mitgliedschaft im CARMF auch für alle dort praktizierenden Berufspartner verpflichtend.

### c. Meldepflicht der Krankenversicherung

Sobald der Arzt in liberaler Form tätig ist, muss er seine Tätigkeit bei der Primary Health Insurance Fund (CPAM) anmelden.

**Bedingungen**

Die Registrierung beim CPAM kann online auf der offiziellen Website von Medicare erfolgen.

**Belege**

Der Registrant muss eine vollständige Datei bereitstellen, die Folgendes enthält:

- Kopieren einer gültigen Identifikationsvorrichtung
- ein professioneller Bankidentitätsauszug (RIB)
- gegebenenfalls den Titel(en), um den Zugang zum Sektor zu ermöglichen.

Weitere Informationen finden Sie im Abschnitt über die Installation von Ärzten auf der Website der Krankenversicherung.

Fünf Grad. Anerkennungsverfahren und Formalitäten für die Anerkennung
---------------------------------------------------------------------

### a. Anmeldung am Ärztekollegium beantragen

Die Registrierung im Vorstand des Ordens ist obligatorisch, um die Tätigkeit eines Arztes in Frankreich rechtlich auszuüben.

Die Eintragung auf die Tafel des Ordens gilt nicht:

- EU- oder EWR-Staatsangehörige, die niedergelassen sind und in einem Mitgliedstaat oder einer Partei rechtmäßig als Arzt tätig sind, wenn sie in Frankreich vorübergehend und gelegentlich in ihrem Beruf tätig sind (siehe s. b) EU- oder EWR-Bürger: für vorübergehende und gelegentliche Übungen);
- Ärzte, die den aktiven Führungskräften des Gesundheitsdienstes der Streitkräfte angehören;
- Ärzte, die den Status eines Beamten oder eines Holding-Beauftragten einer örtlichen Behörde haben, sind nicht verpflichtet, bei der Wahrnehmung ihrer Aufgaben Medizin zu praktizieren.

*Um weiter zu gehen* Artikel L. 4112-5 bis L. 4112-7 des Gesetzes über die öffentliche Gesundheit.

**Beachten Sie, dass**

Die Registrierung auf der Tafel des Ordens ermöglicht die automatische und kostenlose Ausstellung der Health Professional Card (CPS). Der CPS ist ein elektronischer Personalausweis. Es ist durch einen vertraulichen Code geschützt und enthält unter anderem die Identifikationsdaten des Arztes (Identität, Beruf, Spezialität). Weitere Informationen finden Sie auf der Website der Regierung.[Französische Agentur für digitale Gesundheit](http://esante.gouv.fr/).

**Zuständige Behörde**

Der Antrag auf Eintragung ist an den Vorsitzenden des Vorstands des Ärztekollegiums der Abteilung gerichtet, in dem der Betroffene seinen beruflichen Aufenthalt einrichten möchte.

Der Antrag kann direkt beim Fachrat des betreffenden Beschlusses eingereicht oder per Einschreiben mit Empfangsantrag an ihn gerichtet werden.

*Um weiter zu gehen* Artikel R. 4112-1 des Gesetzes über die öffentliche Gesundheit.

**Was Sie wissen sollten**

Im Falle der Verlegung seines beruflichen Wohnsitzes außerhalb der Dienststelle ist der Praktiker verpflichtet, seine Abberufung von der Anweisung der Abteilung, in der er tätig war, und seine Eintragung auf Anordnung seines neuen beruflichen Wohnsitzes zu beantragen.

*Um weiter zu gehen* Artikel R. 4112-3 des Gesetzes über die öffentliche Gesundheit.

**Verfahren**

Nach Eingang des Antrags ernennt der Kreistag einen Berichterstatter, der den Antrag führt und einen schriftlichen Bericht erstellt.

Die Kammer überprüft die Titel des Bewerbers und fordert die Offenlegung des Bulletins 2 des Vorstrafenregisters des Antragstellers. Insbesondere wird überprüft, ob der Kandidat:

- erfüllt die notwendigen Bedingungen der Moral und Unabhängigkeit (siehe oben "3.3. c. Ehrenbedingungen, ethische Regeln"
- die erforderlichen Kompetenzanforderungen erfüllt;
- keine Behinderung oder einen pathologischen Zustand, der mit der Berufsausübung unvereinbar ist (siehe oben "3." e. Körperliche Eignung").

Bei ernsthaften Zweifeln an der beruflichen Kompetenz des Antragstellers oder dem Vorliegen einer Behinderung oder eines mit der Berufsausübung unvereinbaren pathologischen Zustands verweist der Landrat die Angelegenheit an den Regional- oder Regionalrat Know-how. Wenn nach Ansicht des Gutachtens eine berufliche Unzulänglichkeit vorliegt, die die Ausübung des Berufs gefährlich macht, lehnt der Abteilungsrat die Registrierung ab und legt die Ausbildungspflichten des Praktikers fest.

Eine Entscheidung über die Verweigerung der Registrierung kann nicht getroffen werden, ohne dass die Person mindestens fünfzehn Tage im Voraus durch ein empfohlenes Schreiben mit einem Antrag auf Empfangsbestätigung aufgefordert wird, um dies der Kammer zu erläutern.

Der Beschluss des Kollegiums wird innerhalb einer Woche dem Betroffenen, dem Nationalrat des Ärztekollegiums und dem Generaldirektor der regionalen Gesundheitsbehörde (ARS) mitgeteilt. Die Mitteilung erfolgt in Empfehlung mit Empfangsantrag.

In der Anmeldung werden Rechtsmittel gegen die Entscheidung genannt. Die Ablehnungsentscheidung muss gerechtfertigt sein.

*Um weiter zu gehen* Artikel R. 4112-2 und R. 4112-4 des Gesetzes über die öffentliche Gesundheit.

**Zeit**

Der Vorsitzende bestätigt den Eingang der vollständigen Akte innerhalb eines Monats nach seiner Registrierung.

Der Abteilungsrat des Kollegiums muss innerhalb von drei Monaten nach Eingang der vollständigen Antragsunterlagen über den Antrag auf Eintragung entscheiden. Wird eine Antwort innerhalb dieses Zeitraums nicht beantwortet, gilt der Antrag auf Registrierung als abgelehnt.

Dieser Zeitraum wird für Drittstaatsangehörige auf sechs Monate erhöht, wenn eine Untersuchung außerhalb des großstädtischen Frankreichs durchgeführt werden soll. Die betroffene Person wird dann benachrichtigt.

Sie kann auch vom Departementsrat um nicht mehr als zwei Monate verlängert werden, wenn ein Gutachten angeordnet wurde.

*Um weiter zu gehen* Artikel L. 4112-3 und R. 4112-1 des Gesetzes über die öffentliche Gesundheit.

**Belege**

Der Antragsteller muss eine vollständige Antragsdatei einreichen, die Folgendes enthält:

- zwei Kopien des standardisierten Fragebogens ausgefüllt, datiert und unterschrieben, begleitet von einem Lichtbildausweis. Dieser Fragebogen ist in den Abteilungsräten des Kollegiums erhältlich oder kann direkt von der[offizielle Website](https://www.conseil-national.medecin.fr/) Nationalrat des Ärztekollegiums;
- eine Fotokopie eines gültigen Personalausweises oder erforderlichenfalls einer von einer zuständigen Behörde ausgestellten Staatsangehörigkeitsbescheinigung;
- gegebenenfalls eine Fotokopie der Familienaufenthaltskarte eines gültigen EU-Bürgers, der gültigen Langzeitaufenthalts-EC-Karte oder der Aufenthaltskarte mit gültigem Flüchtlingsstatus;
- Wenn ja, eine Fotokopie der gültigen europäischen Kreditkarte;
- eine Kopie der Ausbildungsgänge, die beigefügt sind, wenn erforderlich durch eine Übersetzung durch einen beglaubigten Übersetzer, zu kopieren:- wenn der Antragsteller EU- oder EWR-Staatsangehöriger ist, die ausgestellte Bescheinigung oder Bescheinigung (siehe oben "2. a. Nationale Anforderungen"),
  + Antragsteller erhält eine individuelle Ausübungserlaubnis (siehe s. c. EU- oder EWR-Bürger: für eine ständige Ausübung"), eine Kopie dieser Genehmigung,
  + Wenn der Antragsteller ein in einem fremden Staat ausgestelltes Diplom vorlegt, dessen Gültigkeit auf französischem Hoheitsgebiet anerkannt ist, so ist die Kopie der Titel, denen diese Anerkennung untergeordnet werden kann;
- für Staatsangehörige eines fremden Staates einen Strafregisterauszug oder ein gleichwertiges Dokument, das weniger als drei Monate alt ist und von einer zuständigen Behörde des Herkunftsstaats ausgestellt wurde. Dieser Teil kann für NATIONALs von EU- oder EWR-Staaten, die einen Nachweis der Moral oder Der Ehrenfähigkeit für den Zugang zur medizinischen Tätigkeit verlangen, durch eine Bescheinigung ersetzt werden, die weniger als drei Monate von der zuständigen Behörde des Staates stammt. die Bestätigung, dass diese moralischen oder ehrenwürdigen Bedingungen erfüllt sind;
- eine Erklärung über die Ehre des Klägers, in der bescheinigt wird, dass kein Verfahren, das zu einer Verurteilung oder Sanktion führen könnte, die die Aufnahme in die Kammer beeinträchtigen könnte, gegen ihn ist;
- eine von der Behörde ausgestellte Registrierungs- oder Registrierungsbescheinigung, bei der der Antragsteller zuvor registriert oder registriert war, oder, falls dies nicht der Fall ist, eine Ehrenerklärung des Antragstellers, aus der hervorgeht, dass er nie registriert war oder eingetragen ist oder, falls dies nicht der Fall ist, eine Registrierungs- oder Registrierungsbescheinigung in einem EU- oder EWR-Staat;
- alle Nachweise, dass der Antragsteller über die für die Ausübung des Berufs erforderlichen Sprachkenntnisse verfügt;
- Ein Lebenslauf
- Verträge und Vermerke für die Ausübung des Berufs sowie für die Verwendung der Geräte und der Räumlichkeiten, in denen der Antragsteller tätig ist;
- Wird die Tätigkeit in Form von SEL oder einer professionellen Zivilgesellschaft (SCP) ausgeübt, so werden die Satzung enden und ihre möglichen Billigungen;
- Handelt es sich bei dem Antragsteller um einen Öffentlichen Bediensteten oder Beamten, so ist die Ernennungsreihenfolge;
- wenn der Antragsteller Professor der Universitäten ist - Krankenhauspraktiker (PU-PH), Universitätsdozent - Krankenhausarzt (MCU-PH) oder Krankenhausarzt (PH), die Reihenfolge der Ernennung zum Krankenhausarzt und, falls erforderlich, die Erlassung oder Reihenfolge der Ernennung als Universitätsprofessor oder Dozent an Universitäten.

Weitere Informationen finden Sie auf der offiziellen Website des Nationalrats des Ärztekollegiums.

*Um weiter zu gehen* Artikel L. 4113-9 und R. 4112-1 des Gesetzes über die öffentliche Gesundheit.

**Heilmittel**

Der Antragsteller oder der Nationalrat des Ärztekollegiums kann die Entscheidung, die Registrierung zu registrieren oder abzulehnen, innerhalb von 30 Tagen nach Bekanntgabe des Beschlusses oder der stillschweigenden Ablehnungsentscheidung anfechten. Die Beschwerde wird beim territorial zuständigen Regionalrat eingelegt.

Der Regionalrat muss innerhalb von zwei Monaten nach Eingang des Antrags entscheiden. In Ermangelung einer Entscheidung innerhalb dieser Frist gilt die Beschwerde als zurückgewiesen.

Die Entscheidung des Regionalrats ist auch Gegenstand der Berufung innerhalb von 30 Tagen an den Nationalrat der Ärztekammer. Gegen den Beschluss selbst kann beim Staatsrat Berufung eingelegt werden.

*Um weiter zu gehen* Artikel L. 4112-4 und R. 4112-5 des Gesetzes über die öffentliche Gesundheit.

**Kosten**

Die Registrierung im Verwaltungsrat des Kollegiums ist kostenlos, aber sie schafft eine Verpflichtung zur Zahlung der obligatorischen Ordinalabgaben, deren Betrag jährlich festgesetzt wird und die im ersten Quartal des laufenden Kalenderjahres zu entrichten sind. Die Bezahlung kann online auf der offiziellen Website des Nationalrats des Ärztekollegiums erfolgen. Als Indiz dafür, dass sich dieser Beitrag 2017 auf 333 Euro beläuft.

*Um weiter zu gehen* Artikel L. 4122-2 des Gesetzes über die öffentliche Gesundheit.

### b. eine Voranmeldung der Tätigkeit für EU- oder EWR-Bürger, die vorübergehende und gelegentliche Tätigkeiten ausüben (LPS)

Jeder EU- oder EWR-Staatsangehörige, der in einem dieser Staaten niedergelassen ist und die Tätigkeit eines Arztes in einem dieser Staaten rechtmäßig ausübt, kann vorübergehend oder gelegentlich in Frankreich tätig sein, wenn er die vorherige Erklärung abgibt (siehe s. EWR: für vorübergehende und gelegentliche Übungen").

Die Voraberklärung muss jedes Jahr erneuert werden.

**Beachten Sie, dass**

Jede Änderung der Situation des Antragstellers ist unter denselben Bedingungen mitzuteilen.

*Um weiter zu gehen* Artikel L. 4112-7 und R. 4112-9-2 des Gesetzes über die öffentliche Gesundheit.

**Zuständige Behörde**

Die Erklärung muss vor der ersten Zustellung an den Nationalrat des Ärztekollegiums gerichtet werden.

*Um weiter zu gehen* Artikel R. 4112-9 des Gesetzes über die öffentliche Gesundheit.

**Berichts- und Empfangsbedingungen**

Die Erklärung kann per Post oder direkt online auf der offiziellen Website des Ärztekollegiums gesendet werden.

Wenn der Nationalrat des Ärztekollegiums die Erklärung und alle erforderlichen Belege erhält, sendet er dem Antragsteller eine Quittung mit Angabe seiner Registrierungsnummer und Disziplin.

**Beachten Sie, dass**

Der Dienstleister informiert die zuständige nationale Krankenversicherungsbehörde über seine Erbringung von Dienstleistungen, indem er eine Kopie des Empfangs oder auf andere Weise zusendet.

*Um weiter zu gehen* Artikel R. 4112-9-2 und R. 4112-11 des Gesetzes über die öffentliche Gesundheit.

**Zeit**

Innerhalb eines Monats nach Erhalt der Erklärung teilt der Nationalrat des Ordens dem Antragsteller mit:

- ob er mit der Erbringung von Dienstleistungen beginnen kann oder nicht;
- dass er nachweisen muss, dass er die fehlenden Kenntnisse und Fähigkeiten erworben hat, indem er sich einer Eignungsprüfung unterwirft, wenn die Überprüfung der beruflichen Qualifikationen einen wesentlichen Unterschied zu der in der Frankreich. Wenn er diese Prüfung erfüllt, wird er innerhalb eines Monats darüber informiert, dass er mit der Erbringung von Dienstleistungen beginnen kann;
- Gründe für die Verzögerung bei der Überprüfung seiner Akte, wenn die Aktenüberprüfung auf eine Schwierigkeit hindeutet, die weitere Informationen erfordert. Er hat dann einen Monat Zeit, um die angeforderten zusätzlichen Informationen zu erhalten. In diesem Fall informiert der Nationalrat den Kläger vor Ablauf des zweiten Monats nach Erhalt dieser Informationen nach Prüfung seiner Akte:- ob sie mit der Servicebereitstellung beginnen kann oder nicht,
  + dass er nachweisen muss, dass er die fehlenden Kenntnisse und Fähigkeiten erworben hat, unter anderem durch die Untergabung einer Eignungsprüfung, wenn die Überprüfung der beruflichen Qualifikationen des Antragstellers einen wesentlichen Unterschied zu Ausbildung in Frankreich erforderlich.

Im letzteren Fall wird er, wenn er diese Kontrolle erfüllt, innerhalb eines Monats darüber informiert, dass er mit der Erbringung von Dienstleistungen beginnen kann. Andernfalls wird ihm mitgeteilt, dass er mit der Erbringung der Leistungen nicht beginnen kann.

In Ermangelung einer Antwort des Nationalrats des Ordens innerhalb dieser Fristen kann die Leistungserbringung beginnen.

*Um weiter zu gehen* Artikel R. 4112-9-1 des Gesetzes über die öffentliche Gesundheit.

**Belege**

Der Voranmeldung sind eine Erklärung über die für die Erbringung der Dienstleistung erforderlichen Sprachkenntnisse und die folgenden Belege beizufügen:

- das Vorab-Service-Lieferberichtsformular, modelliert in der Bestellung vom 20. Januar 2010 über die vorherige Meldung der Leistungserbringung für die Praxis von Arzt, Zahnarzt und Zahnärzte Hebamme, ausgefüllt, datiert und unterschrieben. Die angeforderten Informationen beziehen sich auf:- Die Identität des Antragstellers
  + über den betreffenden Beruf,
  + über die Berufsversicherung,
  + und zur Verlängerung, zu Zeiten der Erbringung von Dienstleistungen und zu beruflichen Tätigkeiten;
- Kopieren eines gültigen Ausweises oder eines Dokuments, das die Staatsangehörigkeit des Antragstellers bescheinigt;
- Kopieren des Schulungsdokuments oder der Titel, gegebenenfalls begleitet von einer Übersetzung durch einen beglaubigten Übersetzer;
- eine Bescheinigung der zuständigen Behörde des EU-Abrechnungsstaats oder des EWR, aus der hervorgeht, dass die Person in diesem Staat rechtmäßig niedergelassen ist und dass ihr die Ausübung nicht untersagt ist, gegebenenfalls begleitet von einer französischen Übersetzung von einem zertifizierten Übersetzer eingerichtet.

**Beachten Sie, dass**

Die Kontrolle der Sprachkenntnisse muss in einem angemessenen Verhältnis zu der Tätigkeit stehen, die nach Anerkennung der beruflichen Qualifikation durchzuführen und durchzuführen ist.

*Um weiter zu gehen* Artikel L. 4112-7 des Gesetzes über die öffentliche Gesundheit; 20. Januar 2010 Bestellung auf die vorherige Erklärung der Erbringung von Dienstleistungen für die Praxis des Arztes, Zahnarzt und Hebamme.

**Kosten**

kostenlos.

### c. Lassen Sie erforderlichenfalls eine individuelle Genehmigung für die Ausübung

**Für EU- oder EWR-Bürger**

EU- oder EWR-Bürger mit ausbildungsbewilligung können eine Einzelgenehmigung beantragen:

- von einem dieser Staaten ausgestellt, die nicht von der automatischen Anerkennung profitieren (siehe s. c. EU- und EWR-Bürger: für eine ständige Übung;00
- von einem Drittstaat ausgestellt, aber von einem EU- oder EWR-Mitgliedstaat anerkannt, sofern sie es rechtfertigen, in diesem Mitgliedstaat für einen Zeitraum, der drei Jahren Vollzeit entspricht, als Arzt in der Fachrichtung tätig zu sein.

Ein Authorization Board (CAE) überprüft die Ausbildung und Arbeitserfahrung des Bewerbers.

Sie kann eine Ausgleichsmaßnahme vorschlagen:

- wenn die Ausbildung mindestens ein Jahr unter der des französischen Eds liegt, wenn sie im Wesentlichen unterschiedliche Fächer abdeckt oder wenn eine oder mehrere Teile der beruflichen Tätigkeit, deren Ausübung dem vorgenannten Diplom unterliegt, im entsprechenden Beruf im Herkunftsmitgliedstaat nicht vorhanden sind oder in diesem Staat nicht unterrichtet wurden;
- Ausbildung und Erfahrung des Antragstellers sind nicht geeignet, diese Unterschiede zu decken.

Je nach dem in Frankreich erforderlichen Qualifikationsniveau und dem Niveau der betreffenden Person kann die zuständige Behörde entweder

- Dem Antragsteller die Wahl zwischen einem Anpassungskurs oder einem Eignungstest anbieten;
- einen Anpassungskurs oder Eignungstest auferlegen
- einen Anpassungskurs und einen Eignungstest auferlegen.

Zweck der Eignungsprüfung ist es, durch schriftliche oder mündliche Prüfungen oder praktische Übungen die Eignung des Antragstellers zu überprüfen, als Arzt in der betreffenden Spezialität zu praktizieren. Es handelt sich um Themen, die nicht durch die Ausbildung oder Erfahrung des Bewerbers abgedeckt sind.

Ziel des Anpassungskurses ist es, Interessierten die Möglichkeit zu geben, die für die Ausübung des Arztberufs erforderlichen Fähigkeiten zu erwerben. Es wird unter der Verantwortung eines Arztes durchgeführt und kann durch eine optionale zusätzliche theoretische Ausbildung begleitet werden. Die Dauer des Praktikums beträgt nicht mehr als drei Jahre. Es kann in Teilzeit durchgeführt werden.

*Um weiter zu gehen* Artikel L. 4111-2 II, L. 4131-1-1, R. 4111-17 bis R. 4111-20 und R. 4131-29 des Public Health Code.

**Für Staatsangehörige eines Drittstaates**

Es kann eine individuelle Zulassung zur Ausübung der Praxis gelten, sofern sie ein ausreichendes Niveau der französischen Sprachkenntnisse rechtfertigt, Personen mit einem Ausbildungsabschluss:

- von einem EU- oder EWR-Staat ausgestellt werden, dessen Erfahrung mit allen Mitteln bezeugt wird;
- ausgestellt von einem Drittstaat, der die Ausübung des Arztberufs im Land des Abschlusses erlaubt:- ob sie anonyme Tests zur Überprüfung der grundlegenden und praktischen Kenntnisse erfüllen. Weitere Informationen zu diesen Veranstaltungen finden Sie auf der offiziellen Website des National Management Centre (NMC),
  + wenn sie drei Jahre in einer akkreditierten Abteilung oder Organisation für die Ausbildung von Praktikanten rechtfertigen.

**Beachten Sie, dass**

Ärzte mit einem speziellen Abschluss, der im Rahmen des Auslandspraktikums erworben wurde, gelten als die Kenntnisprüfungstests erfüllt.

*Um weiter zu gehen* Artikel L. 4111-2 (I und I bis), D. 4111-1, D. 4111-6 und R. 4111-16-2 des Public Health Code.

**Zuständige Behörde**

Der Antrag wird in zwei Exemplaren pro empfohlenem Schreiben mit einem Antrag auf Empfangsbestätigung an die Abteilung "Exercise Authorization Commission, CAE) der NMC gerichtet.

Die Genehmigung zur Ausübung wird vom für Gesundheit zuständigen Minister nach Mitteilung der EAC erteilt.

*Um weiter zu gehen* Artikel R. 4111-14 und R. 4131-29 des Gesetzes über die öffentliche Gesundheit; Dekret vom 25. Februar 2010, in dem die Zusammensetzung der Den K.O. für die Prüfung der anträge zur Ausübung der Berufe Arzt, Zahnarzt, Hebamme und Apotheker in Frankreich einzureichenden Akten festgelegt ist.

**Zeit**

Die NMC bestätigt den Eingang der Anfrage innerhalb eines Monats nach Erhalt.

Das Schweigen, das für einen bestimmten Zeitraum nach Erhalt der vollständigen Akte aufbewahrt wird, ist die Entscheidung wert, die Klage abzuweisen. Diese Frist ist:

- vier Monate für Anträge von EU- oder EWR-Staatsangehörigen mit einem Abschluss aus einem dieser Staaten;
- sechs Monate für Anträge von Drittstaatsangehörigen mit einem Diplom aus einem EU- oder EWR-Staat;
- ein Jahr für andere Anwendungen. Diese Frist kann durch eine Entscheidung der Ministerialbehörde, die spätestens einen Monat vor Ablauf dieser Behörde mitgeteilt wird, um zwei Monate verlängert werden, wenn es erhebliche Schwierigkeiten bei der Beurteilung der Berufserfahrung des Bewerbers gibt.

*Um weiter zu gehen* Artikel R. 4111-2, R. 4111-14 und R. 4131-29 des Public Health Code.

**Belege**

Die Anwendungsdatei muss Folgendes enthalten:

- ein Antragsformular für die Zulassung zur Ausübung des Berufs nach dem Vorbild von Anhang 1 des Beschlusses vom 25. Februar 2010, ausgefüllt, datiert und unterzeichnet, das gegebenenfalls die Spezialität enthält, in der der Anmelder Einreicht;
- Eine Fotokopie eines gültigen Ausweises
- eine Kopie des Ausbildungstitels, die die Ausübung des Berufs im Status des Erlangungsstatus ermöglicht, sowie, falls erforderlich, eine Kopie des Fachausbildungstitels;
- Gegebenenfalls eine Kopie der zusatzdiplome;
- alle sachdienlichen Beweise, die eine kontinuierliche Ausbildung, Erfahrung und Fähigkeiten rechtfertigen, die während der beruflichen Ausübung in einem EU- oder EWR-Staat oder in einem Drittstaat erworben wurden (Funktionsbescheinigungen, Tätigkeitsbericht, operative Bewertung usw.). ) ;
- im Rahmen von Funktionen, die in einem anderen Staat als Frankreich ausgeübt werden, geht eine Erklärung der zuständigen Behörde dieses Staates weniger als ein Jahr zurück, in der das Fehlen von Sanktionen gegen den Antragsteller bescheinigt wird.

Je nach Situation des Antragstellers sind zusätzliche Belege erforderlich. Weitere Informationen finden Sie auf der offiziellen Website der
Cng.

**Was Sie wissen sollten**

Die Belege müssen in französischer Sprache verfasst oder von einem beglaubigten Übersetzer übersetzt werden.

*Um weiter zu gehen* : Erlass vom 25. Februar 2010, in dem die Zusammensetzung der Den zuständigen Genehmigungskommissionen für die Prüfung der für die Praxis in Frankreich eingereichten Anträge der Berufe Arzt, Zahnarzt, Hebamme und Apotheker festgelegt wird; 17. November 2014 Nr. DGOS/RH1/RH2/RH4/318.

### d. Abhilfemaßnahmen

#### Französisches Hilfszentrum

Das ENIC-NARIC-Zentrum ist das französische Informationszentrum für die akademische und berufliche Anerkennung von Diplomen.

#### Solvit

SOLVIT ist eine Dienstleistung, die von der nationalen Verwaltung jedes EU-Mitgliedstaats oder jeder Vertragspartei des EWR-Abkommens erbracht wird. Ziel ist es, eine Lösung für einen Streit zwischen einem EU-Bürger und der Verwaltung eines anderen dieser Staaten zu finden. SOLVIT greift insbesondere in die Anerkennung von Berufsqualifikationen ein.

**Bedingungen**

Der Betroffene kann SOLVIT nur verwenden, wenn er

- dass die öffentliche Verwaltung eines EU-Staates seine Rechte nach dem EU-Recht als Bürger oder Unternehmen eines anderen EU-Staates nicht geachtet hat;
- dass sie noch keine Klage eingeleitet hat (Verwaltungsmaßnahmen werden nicht als solche betrachtet).

**Verfahren**

Der Staatsangehörige muss eine[Online-Beschwerdeformular](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Sobald seine Akte eingereicht wurde, kontaktiert SOLVIT ihn innerhalb einer Woche, um gegebenenfalls zusätzliche Informationen anzufordern und zu überprüfen, ob das Problem in seine Zuständigkeit fällt.

**Belege**

Um solVIT zu betreten, muss der Staatsangehörige Folgendes mitteilen:

- Vollständige Kontaktdaten
- Detaillierte Beschreibung seines Problems
- alle Indizbeweise (z. B. Korrespondenz und Entscheidungen der zuständigen Verwaltungsbehörde).

**Zeit**

SOLVIT ist entschlossen, innerhalb von zehn Wochen nach der Übernahme des Falles durch das SOLVIT-Zentrum in dem Land, in dem das Problem aufgetreten ist, eine Lösung zu finden.

**Kosten**

kostenlos.

**Ergebnis des Verfahrens**

Am Ende der 10-Wochen-Frist präsentiert SOLVIT eine Lösung:

- Wenn diese Lösung den Streit über die Anwendung des europäischen Rechts beilegt, wird die Lösung akzeptiert und der Fall abgeschlossen;
- wenn es keine Lösung gibt, wird der Fall als ungelöst abgeschlossen und an die Europäische Kommission verwiesen.

**Weitere Informationen**

SOLVIT in Frankreich: Generalsekretariat für europäische Angelegenheiten, 68 rue de Bellechasse, 75700 Paris ([offizielle Website](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

