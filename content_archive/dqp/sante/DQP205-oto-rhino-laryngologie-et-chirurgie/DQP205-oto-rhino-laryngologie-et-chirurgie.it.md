﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP205" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Salute" -->
<!-- var(title)="Chirurgia oto-rinoceronte-laringologia e cervice-facciale" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="salute" -->
<!-- var(title-short)="chirurgia-oto-rinoceronte-laringologia" -->
<!-- var(url)="https://www.guichet-qualifications.fr/it/dqp/salute/chirurgia-oto-rinoceronte-laringologia-e-cervice-facciale.html" -->
<!-- var(last-update)="2020-04-15 17:21:55" -->
<!-- var(url-name)="chirurgia-oto-rinoceronte-laringologia-e-cervice-facciale" -->
<!-- var(translation)="Auto" -->


Chirurgia oto-rinoceronte-laringologia e cervice-facciale
=========================================================

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:21:55<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

L'otorinolaringoiatria e la chirurgia cervico-facciale è una disciplina chirurgica che supporta disturbi del collo e della laringe (ENT) del collo e del viso.

Due gradi. Qualifiche professionali
-----------------------------------

### a. Requisiti nazionali

#### Legislazione nazionale

Ai sensi dell'articolo L. 4111-1 del codice di sanità pubblica, al fine di esercitare legalmente la professione di medico in Francia, gli interessati devono soddisfare cumulativamente le seguenti tre condizioni:

- detenuto il diploma di stato francese di medico di medicina o un titolo o un diploma che conferisce il titolo di medico;
- essere di nazionalità francese, cittadinanza andorrana o cittadino di uno Stato membro dell'Unione europea (UE) o parte dell'accordo sullo Spazio economico europeo (AEA) o Marocco, subordinatamente all'applicazione delle norme derivate dal codice sanitario impegni pubblici o internazionali. Tuttavia, questa condizione non si applica a un medico con il diploma di stato francese di medico di medicina;
- essere incluso nella Classifica del Collegio dei Medici (vedi infra "5.0). a. Richiesta di inclusione nell'elenco del Collegio dei Medici").

Tuttavia, le persone che non soddisfano le qualifiche del diploma o della nazionalità possono essere autorizzate a esercitare la professione di medico per ordine individuale del Ministro della Sanità (vedi infra "5o. c. Se necessario, richiedere un permesso di esercizio individuale").

*Per andare oltre* Articoli L. 4111-1, L. 4112-6, L. 4112-7 e L. 4131-1 del codice di salute pubblica.

**Si noti che**

In mancanza di tutte queste condizioni, la pratica della professione di medico è illegale e punibile con due anni di reclusione e una multa di 30.000 euro.

**Buono a sapersi: riconoscimento automatico del diploma**

Ai sensi dell'articolo L. 4131-1 del codice di sanità pubblica, i cittadini dell'UE o del SEE possono esercitare la professione di medico se detengono uno dei seguenti titoli:

- documenti di formazione dei medici rilasciati da uno Stato dell'UE o del CEE in conformità con gli obblighi dell'UE ed elencati nell'allegato del decreto del 13 luglio 2009 che stabilisce elenchi e condizioni per il riconoscimento dell'UE formazione di medici e medici specialisti rilasciati dagli Stati membri dell'UE o di parti dell'accordo SEE oggetto dell'articolo L. 4131-1 del codice per la sanità pubblica;
- certificati di formazione medica rilasciati da uno Stato dell'UE o del CEA in conformità con gli obblighi dell'UE, non nell'elenco di cui sopra, se sono accompagnati da un certificato di tale Stato che attestata la sanzione della formazione conforme a questi obblighi e sono da lui assimilati ai titoli di formazione in questo elenco;
- Certificati di formazione medica rilasciati da uno Stato dell'UE o del CEA la formazione medica è iniziato in tale Stato prima delle date del suddetto decreto e non conforme agli obblighi dell'UE, se accompagnato un certificato di uno di questi stati che attestata che il titolare dei titoli di formazione si è dedicato, in tale Stato, in modo efficace e legittimo, alla pratica della professione di medico nella specialità interessata per almeno tre anni consecutivi nel Cinque anni prima dell'emissione del certificato;
- certificati di formazione dei medici rilasciati dall'ex Cecoslovacchia, dall'ex Unione Sovietica o dall'ex Iugoslavia o che hanno autorizzato la formazione iniziata prima della data di indipendenza della Repubblica ceca, della Slovacchia, dell'Estonia, Lettonia, Lituania o Slovenia, se accompagnate da un certificato delle autorità competenti di uno di questi Stati che certifica di avere la stessa validità giuridica dei documenti di formazione rilasciati da tale Stato. Il presente certificato è accompagnato da un certificato rilasciato dalle stesse autorità che indica che il titolare ha esercitato in tale Stato, in modo efficace e legittimo, la professione di medico nella specialità interessata per almeno tre anni consecutivi nel Cinque anni prima dell'emissione del certificato;
- certificati di formazione medica rilasciati da uno Stato dell'UE o del CEA non nell'elenco di cui sopra se sono accompagnati da un certificato rilasciato dalle autorità competenti di tale stato che attestava che il titolare del certificato di formazione è stato stabilito sul suo territorio alla data indicata nel suddetto decreto e che ha acquisito il diritto di svolgere le attività di un medico generico nell'ambito del suo regime nazionale di sicurezza sociale;
- I certificati di formazione medica rilasciati da uno Stato dell'UE o del CEA, ha iniziato in tale Stato prima delle date del suddetto decreto e non in linea con gli obblighi dell'UE, ma di esercitare legalmente la professione di medico nello stato che li ha emessi, se il medico giustifica aver svolto in Francia nei cinque anni precedenti tre anni consecutivi a tempo pieno di funzioni ospedaliere nella specialità corrispondente formazione come associato, addetto associato, assistente associato o funzioni accademiche come capo della clinica associata delle università o assistente associato delle università, a condizione che sia stato nominato funzioni ospedaliere allo stesso tempo;
- I certificati di formazione medica specialistica dell'Italia sulla suddetta lista di consulenza specialistica sono iniziati in tale stato dopo il 31 dicembre 1983 e prima del 1o gennaio 1991, se accompagnati da un certificato rilasciato dalle autorità statali che indica che il suo titolare ha esercitato in tale stato, in modo efficace e legittimo, la professione di medico nella specialità interessata per almeno sette anni consecutivi nei dieci anni precedenti emissione del certificato.

*Per andare oltre* Articolo L. 4131-1 del codice di sanità pubblica; Decreto del 13 luglio 2009 che stabilisce gli elenchi e le condizioni per il riconoscimento dei documenti di formazione di medici e medici specialisti emessi dagli Stati membri dell'Unione europea o delle parti dell'accordo sullo Spazio economico europeo di cui al 2 Sezione L. 4131-1 del Codice di sanità pubblica.

#### Formazione

Gli studi medici consistono in tre cicli con una durata totale compresa tra nove e undici anni, a seconda del corso scelto.

La formazione, che si svolge presso l'università, comprende molti stage ed è punteggiata da due concorsi:

- il primo si verifica alla fine del primo anno. Quest'anno di studio, chiamato il "primo anno di studi sanitari comuni" (PACES) è comune agli studenti in medicina, farmacia, odontologia, fisioterapia e ostetriche. Alla fine di questo primo concorso, gli studenti sono classificati in base ai loro risultati. Coloro che si trovano in un rango utile sotto il numerus clausus sono autorizzati a continuare i loro studi e a scegliere, se necessario, di continuare la formazione che porta alla pratica della medicina;
- il secondo si verifica alla fine del secondo ciclo (cioè alla fine del sesto anno di studio): questo concorso è chiamato test di classificazione nazionali (ECN) o precedentemente "collegi". Alla fine di questo concorso, gli studenti scelgono, in base alla loro classifica, la loro specialità e / o la loro città di assegnazione. La durata degli studi che seguono varia a seconda della specialità scelta.

Per ottenere una laurea statale (DE) come medico di medicina, lo studente deve convalidare tutti i suoi stage, il suo diploma di studi specializzati (DES) e sostenere con successo la sua tesi.

*Per andare oltre* Articolo L. 632-1 del codice di istruzione.

**Buono a sapersi**

Gli studenti di medicina devono effettuare vaccinazioni obbligatorie. Per ulteriori informazioni, fare riferimento alla sezione R. 3112-1 del Codice di integrità pubblica.

**Diploma di istruzione generale in scienze mediche**

Il primo ciclo è sancito dal diploma di formazione generale in scienze mediche. Si compone di sei semestri e corrisponde al livello di licenza. I primi due semestri corrispondono aI PACES.

L'obiettivo della formazione è quello di:

- l'acquisizione delle conoscenze scientifiche di base, essenziali per la successiva padronanza delle conoscenze e del know-how necessarie per la pratica delle professioni mediche. Questa base scientifica è ampia e comprende la biologia, alcuni aspetti delle scienze esatte e diverse discipline delle scienze umane e sociali;
- l'approccio fondamentale dell'uomo sano e dell'uomo malato, compresi tutti gli aspetti della semiologia.

Esso comprende insegnamenti teorici, metodologici, applicati e pratici, nonché il completamento di tirocini, tra cui un corso di assistenza introduttiva di quattro settimane in un ospedale.

*Per andare oltre* : ordine del 22 marzo 2011 relativo al programma di istruzione per il diploma di formazione generale in scienze mediche.

**Laurea di formazione approfondita in scienze mediche**

Il secondo ciclo di studi medici è sancito dal diploma di formazione approfondita in scienze mediche. Si compone di sei semestri di formazione e corrisponde al livello del maestro.

Il suo obiettivo è quello di acquisire le competenze generiche che consentono agli studenti di svolgere successivamente compiti post-laurea in ospedali e ambienti ambulatoriali e di acquisire le competenze professionali della formazione in si impegneranno durante la loro specializzazione.

Le competenze da acquisire sono quelle di comunicatore, medico, co-operatore, membro di un team di assistenza sanitaria multi-professionale, attore di salute pubblica, scienziato e leader etico ed etico. Lo studente deve anche imparare ad essere riflessivo.

Le lezioni sono incentrate su ciò che è comune o grave o un problema di salute pubblica e ciò che è clinicamente esemplare.

Gli obiettivi della formazione sono:

- l'acquisizione di conoscenze sui processi patofisiologici, la patologia, le basi terapeutiche e la prevenzione completando e approfondendo quelle acquisite nel ciclo precedente;
- formazione nel processo scientifico
- Apprendimento del ragionamento clinico
- competenze generiche per prepararsi agli studi medici post-laurea.

Oltre agli insegnamenti teorici e pratici, la formazione comprende il completamento di trentasei mesi di stage e venticinque guardie.

*Per andare oltre* : ordine dell'8 aprile 2013 relativo al curricolo per il primo e il secondo ciclo di studi medici.

**Diploma di Studi Specializzati (DES)**

L'accesso al terzo ciclo è attraverso NCT. Per praticare come otorinolaricologo, il professionista deve ottenere Otorinolaringologia (ORL) e Chirurgia Cervico-facciale (CCF) DES.

Questo diploma è composto da dodici semestri, tra cui:

- almeno nove semestri nella specialità;
- almeno tre semestri in un luogo di tirocinio senza supervisione universitaria.

La formazione del DES è suddivisa in tre fasi: la fase di base, la fase di approfondimento e la fase di consolidamento.

**La fase di base**

Con due semestri, permette al candidato di acquisire le conoscenze di base della specialità scelta e di eseguire:

- uno stage in un ospedale autorizzato in un'organizzazione senior di ent e CCF in un settore di esplorazione non funzionale;
- uno stage in un ospedale con licenza principale in un'altra specialità chirurgica.

**La fase di approfondimento**

Con la durata di sei semestri, permette al candidato di acquisire le conoscenze e le competenze necessarie per praticare la specialità scelta e per eseguire:

- quattro tirocini in un ospedale autorizzato in OTorino e FFS, tra cui almeno uno nel settore dell'esplorazione funzionale;
- uno stage in un ospedale autorizzato in neurochirurgia, chirurgia maxillo-facciale, chirurgia toracica e cardiovascolare, chirurgia vascolare, chirurgia plastica, ricostruttiva e ricostruttiva e come complementare ENT e CCF;
- uno stage gratuito.

**La fase di consolidamento**

Il processo biennale consente al candidato di consolidare le sue conoscenze ed eseguire:

- uno stage di un anno in una località accreditata senior orL e CCF;
- uno stage di un anno in un luogo ospedaliero accreditato come preside in Otorinolaringoiatra e CCF, o sotto forma di tirocinio misto o accoppiato in luoghi e/o con un praticante qualificatore universitario, accreditato come preside in Otorinolaringoiatra e CCF.

Il DES ha diritto alla qualifica di uno specialista in otorinolaringoiatra e CCF.

*Per andare oltre* :[Appendice II](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000036237037) del decreto del 27 novembre 2017 che modifica il decreto del 12 aprile 2017 relativo all'organizzazione del terzo ciclo di studi medici e al decreto del 21 aprile 2017 relativo alle conoscenze, alle competenze e ai modelli di formazione dei diplomi di studio e stabilire l'elenco di questi diplomi e le opzioni specializzate trasversali e la formazione degli studi medici post-laurea.

#### Costi associati alla qualifica

La formazione che porta alla laurea del medico è pagata e il costo varia a seconda dell'istituzione scelta. Per ulteriori informazioni, si consiglia di verificare con le istituzioni interessate.

### b. Cittadini dell'UE: per esercizi olfatto temporaneo e occasionale (Consegna gratuita dei servizi (LPS))

Un medico membro di uno Stato dell'UE o del SEA che è stabilito e pratica legalmente in uno di questi Stati può compiere atti della sua professione in Francia su base temporanea e occasionale, a condizione che abbia dichiarazione preventiva al Consiglio nazionale del Collegio dei Medici (cfr. infra "5o. b. Fare una predichiarazione per i cittadini dell'UE o del BEE impegnati in attività temporanee e occasionali").

**Cosa sapere**

La registrazione nell'elenco dei medici del College of Physicians non è richiesta per il professionista del servizio libero (LPS). Pertanto, non è tenuto a pagare quote ordinali. Il medico è semplicemente registrato su una lista specifica gestita dal Consiglio Nazionale del Collegio dei Medici.

La pre-dichiarazione deve essere accompagnata da una dichiarazione relativa alle competenze linguistiche necessarie per svolgere il servizio. In questo caso, il controllo della conoscenza della lingua deve essere proporzionato all'attività da svolgere e svolgere una volta riconosciuta la qualifica professionale.

Quando i titoli di formazione non ricevono il riconoscimento automatico (vedere supra "2.0). a. Legislazione nazionale"), le qualifiche professionali del fornitore vengono verificate prima della fornitura del primo servizio. In caso di differenze sostanziali tra le qualifiche della persona interessata e la formazione richiesta in Francia che potrebbe danneggiare la salute pubblica, il richiedente è sottoposto a una prova.

Il medico in situazione LPS è tenuto a rispettare le regole professionali applicabili in Francia, comprese tutte le regole etiche (vedi infra "3". Condizioni di onorabilità, regole etiche, etica"). È soggetto alla giurisdizione disciplinare del Collegio dei Medici.

**Si noti che**

Il servizio viene eseguito sotto il titolo professionale francese di medico. Tuttavia, quando le qualifiche di formazione non sono riconosciute e le qualifiche non sono state verificate, le prestazioni vengono effettuate sotto il titolo professionale dello Stato di Istituzione, al fine di evitare confusione Con il titolo professionale francese.

*Per andare oltre* Articolo L. 4112-7 del Codice di sanità pubblica.

### c. Cittadini dell'UE: per un esercizio permanente (Free Establishment (LE))

**Il riconoscimento automatico dei diplomi ottenuti in uno Stato dell'UE**

L'articolo L. 4131-1 del codice di sanità pubblica crea un regime per il riconoscimento automatico in Francia di alcuni diplomi o titoli, se presenti, accompagnati da certificati, ottenuti in uno stato dell'UE o del SEE (vedi sopra "2". a. Legislazione nazionale").

Spetta al consiglio dipartimentale del collegio dei medici competenti verificare la regolarità dei diplomi, dei titoli, dei certificati e dei certificati, concedere il riconoscimento automatico e quindi decidere sulla domanda di registrazione nell'elenco dell'Ordine. .

*Per andare oltre* Articolo L. 4131-1 del codice di sanità pubblica; Decreto del 13 luglio 2009 che stabilisce gli elenchi e le condizioni per il riconoscimento dei documenti di formazione di medici e medici specialisti emessi dagli Stati membri dell'Unione europea o delle parti dell'accordo sullo Spazio economico europeo di cui al 2 Sezione L. 4131-1 del Codice di sanità pubblica.

**Il regime dispregiativa: autorizzazione preventiva**

Se l'UE o il cittadino del SEE non si qualificano per il riconoscimento automatico delle proprie credenziali, ricade su un regime di autorizzazione (cfr. sotto "5o). c. Se necessario, richiedere un permesso di esercizio individuale").

Le persone che non ricevono il riconoscimento automatico, ma che detengono una designazione di formazione per esercitare legalmente la professione di medico, possono essere autorizzate individualmente a praticare nella specialità interessata Ministro della Salute, dopo la consulenza di una commissione composta da professionisti.

Se l'esame delle qualifiche professionali attestato dalle credenziali di formazione e dall'esperienza professionale mostra differenze sostanziali con le qualifiche richieste per l'accesso alla professione nella specialità interessata e il suo esercizio in Francia, l'interessato deve sottoporsi a una misura di compensazione.

A seconda del livello di qualifica richiesto in Francia e di quello detenuto dall'interessato, l'autorità competente può:

- offrire al richiedente la possibilità di scegliere tra un corso di adeguamento o un test attitudinale;
- richiedono un corso di regolazione e/o un test attitudinale.

*Per andare oltre* Articolo L. 4131-1-1 del codice di sanità pubblica.

Tre gradi. Condizioni di onorabilità, regole etiche, etica
----------------------------------------------------------

### a. Conformità al Codice Etico dei Medici

Le disposizioni del Codice di Etica Medica sono richieste per tutti i medici che esercitano in Francia, che siano nel consiglio di amministrazione dell'Ordine o siano esenti da tale obbligo (cfr. infra "5o. a. Richiesta di inclusione nell'elenco del Collegio dei Medici").

**Cosa sapere**

Tutte le disposizioni del Codice Etico sono codificate nelle sezioni da 4127-1 a R. 4127-112 del Codice di Sanità Pubblica.

Come tale, il medico deve rispettare i principi della moralità, probità e dedizione essenziali per la pratica della medicina. Egli è anche soggetto a segretezza medica e deve esercitare in modo indipendente.

*Per andare oltre* Articoli da R. da 4127-1 a R. 4127-112 del codice di sanità pubblica.

### b. Attività cumulative

Il medico può svolgere qualsiasi altra attività solo se tale combinazione è compatibile con i principi di indipendenza professionale e dignità impostigli. L'accumulo di attività non dovrebbe permettergli di approfittare delle sue prescrizioni o dei suoi consigli medici.

Pertanto, il medico non può combinare l'esercizio medico con un'altra attività vicino al campo di salute. In particolare, gli è proibito praticare come ottico, paramedico o manager di una società di ambulanze, produttore o venditore di dispositivi medici, proprietario o gestore di un hotel per curatori, una palestra, un centro benessere, una pratica di massaggio.

Allo stesso modo, un medico che assolve un mandato elettivo o amministrativo è vietato utilizzarlo per aumentare la propria clientela.

*Per andare oltre* Articoli R. 4127-26 e R. 4127-27 del codice di sanità pubblica.

### c. Condizioni di onorabilità

Al fine di praticare, il medico deve certificare che nessun procedimento che potrebbe dare luogo a una condanna o una sanzione che potrebbe influenzare la sua inclusione nel consiglio di amministrazione sono contro di lui.

*Per andare oltre* Articolo R. 4112-1 del codice sanitario pubblico.

### d. Obbligazione per lo sviluppo professionale continuo

I medici devono partecipare a un programma di sviluppo professionale continuo pluriennale. Il programma si concentra sulla valutazione delle pratiche professionali, sul miglioramento delle competenze, sul miglioramento della qualità e della sicurezza delle cure, sul mantenimento e sull'aggiornamento delle conoscenze e delle competenze.

Tutte le azioni svolte dai medici sotto l'obbligo di svilupparsi professionalmente sono riconducibili in un documento specifico che attesti il rispetto di tale obbligo.

*Per andare oltre* Articoli L. 4021-1 e articoli successivi R. 4021-4 e seguenti del codice di salute pubblica.

### e. Attitudine fisica

I medici non devono presentare infermità o patologia incompatibile con la pratica della professione (vedi infra "5.0). a. Richiesta di inclusione nell'elenco del Collegio dei Medici").

*Per andare oltre* Articolo R. 4112-2 del codice sanitario pubblico.

È uno di quattro gradi. Assicurazione
-------------------------------------

### a. Obbligo di assumere un'assicurazione responsabilità civile professionale

In qualità di professionista sanitario impegnato nella prevenzione, nella diagnosi o nell'assistenza, il medico deve, se pratica in veste liberale, prendere un'assicurazione di responsabilità civile professionale.

Se esercita come dipendente, spetta al datore di lavoro prendere tale assicurazione per i suoi dipendenti per gli atti compiuti durante questa attività.

*Per andare oltre* Articolo L. 1142-2 del codice di sanità pubblica.

### b. Obbligo di aderire alla cassa pensione indipendente dei medici di Francia (CARMF)

Qualsiasi medico iscritto nel consiglio di amministrazione dell'Ordine e esercitato in forma liberale (anche a tempo parziale e anche se è impiegato) ha l'obbligo di aderire al CARMF.

**Tempo**

L'interessato deve registrarsi presso il CARMF entro un mese dall'inizio della sua attività liberale.

**Termini**

L'interessato deve restituire il modulo di dichiarazione, compilato, datato e controfirmato dal consiglio dipartimentale del Collegio dei Medici. Questo modulo può essere scaricato dal[CARMF](http://www.carmf.fr/).

**Cosa sapere**

In caso di pratica in una società di pratica liberale (SEL), l'appartenenza al CARMF è obbligatoria anche per tutti i partner professionali che vi esercitano.

### c. Obbligo di segnalazione Medicare

Una volta riportata la lista dell'Ordine, il medico che esercita in forma liberale deve dichiarare la sua attività con il Fondo di assicurazione sanitaria primaria

**Termini**

La registrazione al CPAM può essere effettuata online sul sito ufficiale di Medicare.

**Documenti di supporto**

Il dichiarante deve fornire un file completo che includa:

- Copia di un documento di identità valido
- un estratto conto di identità bancaria professionale (RIB)
- se necessario, il titolo (s) per consentire l'accesso al settore 2.

Per ulteriori informazioni, fare riferimento alla sezione sull'installazione dei medici sul sito web dell'assicurazione sanitaria.

Cinque gradi. Procedure e formalità di riconoscimento delle qualifiche
----------------------------------------------------------------------

### a. Richiedere la registrazione sulla tabella del Collegio dei Medici

La registrazione sul consiglio di amministrazione dell'Ordine è obbligatoria per svolgere legalmente l'attività di un medico in Francia.

L'iscrizione sulla bacheca dell'Ordine non si applica:

- Cittadini dell'UE o del SEE che sono stabiliti e che praticano legalmente come medici in uno Stato dell'UE o del SEE quando compiono atti della loro professione in Francia, su base temporanea e occasionale (vedi supra "2o. b. cittadini dell'UE: per esercizi temporanei e occasionali);
- medici appartenenti ai dirigenti attivi del Servizio Sanitario delle Forze Armate;
- i medici che, con lo status di dipendente pubblico o di un incarico di un'autorità locale, non sono chiamati, nel corso dei loro doveri, a praticare la medicina.

*Per andare oltre* Articoli da L. da 4112 a 5 a L. 4112-7 del Codice di Sanità Pubblica.

**Si noti che**

La registrazione sul consiglio di amministrazione dell'Ordine consente l'emissione automatica e gratuita della Health Professional Card (CPS). Il CPS è una carta d'identità aziendale elettronica. È protetto da un codice confidenziale e contiene, tra le altre cose, i dati di identificazione del medico (identità, occupazione, specialità). Per ulteriori informazioni, si consiglia di consultare il sito web del governo[Agenzia francese per la salute digitale](http://esante.gouv.fr/).

**Autorità competente**

La domanda di registrazione è indirizzata al presidente del consiglio del Consiglio di amministrazione del Collegio dei Medici del Dipartimento in cui la persona interessata desidera stabilire la sua residenza professionale.

La domanda può essere presentata direttamente al consiglio dipartimentale dell'Ordine interessato o inviatagli per posta registrata con richiesta di notifica.

*Per andare oltre* Articolo R. 4112-1 del codice sanitario pubblico.

**Cosa sapere**

In caso di trasferimento della sua residenza professionale al di fuori del dipartimento, il professionista è tenuto a richiedere il suo allontanamento dall'ordine del dipartimento in cui esercitava la sua pratica e la sua registrazione sull'ordine della sua nuova residenza professionale.

*Per andare oltre* Articolo R. 4112-3 del codice di sanità pubblica.

**Procedura**

Al ricevimento della domanda, il consiglio di contea nomina un relatore che conduce la domanda e fa una relazione scritta.

Il consiglio verifica i titoli del candidato e chiede la divulgazione del bollettino 2 della fedina penale del richiedente. In particolare, verifica che il candidato:

- soddisfa le condizioni di moralità e di indipendenza necessarie (vedi supra "3.3. c. Condizioni di onorabilità");
- soddisfa i requisiti di competenza necessari;
- non presenta una disabilità o una condizione patologica incompatibile con la pratica della professione (vedi supra "3". e. Attitudine fisica").

In caso di seri dubbi sulla competenza professionale del richiedente o sull'esistenza di una disabilità o di una condizione patologica incompatibile con la prassi della professione, il consiglio di contea rinvia la questione al Consiglio regionale o interregionale. Competenza. Se, a parere della perizia, c'è un'inadeguatezza professionale che rende pericolosa la pratica della professione, il consiglio dipartimentale rifiuta la registrazione e specifica gli obblighi di formazione del praticante.

Nessuna decisione di rifiutare la registrazione può essere presa senza che la persona sia invitata almeno due settimane prima da una lettera raccomandata che richiede la notifica di ricezione di comparire dinanzi al Consiglio per spiegare.

La decisione del Consiglio del Collegio viene notificata entro una settimana all'individuo, al Consiglio Nazionale del Collegio dei Medici e al Direttore Generale dell'Agenzia Regionale della Sanità (ARS). La notifica è per lettera raccomandata con richiesta di avviso di ricevimento.

La notifica menziona rimedi contro la decisione. La decisione di rifiutare deve essere giustificata.

*Per andare oltre* Articoli R. 4112-2 e R. 4112-4 del Codice di sanità pubblica.

**Tempo**

Il Presidente riconosce la ricezione del fascicolo completo entro un mese dalla sua registrazione.

Il consiglio dipartimentale del collegio deve decidere sulla domanda di registrazione entro tre mesi dalla ricezione del fascicolo completo della domanda. Se una risposta non riceve risposta entro questo lasso di tempo, la domanda di registrazione è considerata respinta.

Questo periodo è aumentato a sei mesi per i cittadini di paesi terzi quando si deve effettuare un'indagine al di fuori della Francia metropolitana. L'interessato viene quindi informato.

Può anche essere prorogato per un periodo non superiore a due mesi dal consiglio di dipartimento quando è stato ordinato un parere di esperti.

*Per andare oltre* Articoli L. 4112-3 e R. 4112-1 del codice di sanità pubblica.

**Documenti di supporto**

Il richiedente deve presentare un fascicolo completo di domanda che comprenda:

- due copie del questionario standardizzato con un documento d'identità con foto completato, datato e firmato, disponibili nei consigli dipartimentali del Collegio o direttamente scaricabili[sito ufficiale](https://www.conseil-national.medecin.fr/) Consiglio Nazionale del Collegio dei Medici;
- una fotocopia di un documento d'identità valido o, se necessario, di un certificato di nazionalità rilasciato da un'autorità competente;
- Se del caso, una fotocopia della carta di residenza familiare di un cittadino dell'UE valida, della carta residente CE valida a lungo termine o della carta residente con status di rifugiato valido;
- In tal caso, una fotocopia della carta di credito europea valida;
- una copia, accompagnata se necessario da una traduzione da parte di un traduttore certificato, dei corsi di formazione a cui sono allegati:- nel caso in cui il richiedente sia un cittadino dell'UE o del SEE, il certificato o i certificati forniti (cfr. sopra "2. a. Requisiti nazionali"),
  + al richiedente viene concesso un permesso di esercizio individuale (cfr. supra "2. v. cittadini dell'UE: per un esercizio permanente"), una copia di tale autorizzazione,
  + Quando il richiedente presenta un diploma emesso in uno Stato straniero la cui validità è riconosciuta sul territorio francese, la copia dei titoli a cui tale riconoscimento può essere subordinato;
- per i cittadini di uno Stato straniero, un estratto di fedina penale o un documento equivalente di meno di tre mesi, rilasciato da un'autorità competente dello Stato di origine. Questa parte può essere sostituita, per i cittadini dell'UE o del BEE, che richiedono la prova della moralità o dell'onorificenza per l'accesso all'attività medica, con un certificato, di età inferiore a tre mesi, dall'autorità competente dello Stato. certificando che queste condizioni morali o di onorabilità sono soddisfatte;
- una dichiarazione sull'onore della ricorrente che attesta che nessun procedimento che possa dare luogo a una condanna o a una sanzione che potrebbe influire sulla quotazione nel consiglio di amministrazione è contro di lui;
- un certificato di delisting, registrazione o registrazione rilasciato dall'autorità con cui il richiedente è stato precedentemente registrato o registrato o, in mancanza di tale firma, una dichiarazione d'onore da parte del richiedente che ha dichiarato di non essere mai stata registrata o registrato o, in mancanza di tale, un certificato di registrazione in uno stato dell'UE o del CEA;
- tutte le prove che il richiedente dispone delle competenze linguistiche necessarie per esercitare la professione;
- Un curriculum
- contratti e approvazioni per la pratica della professione, nonché quelli relativi all'uso delle attrezzature e dei locali in cui il richiedente esercita;
- Se l'attività è svolta sotto forma di SEL o di una società civile professionale (SCP), gli statuti di tale società e le loro possibili approvazioni;
- Se il richiedente è un funzionario pubblico o un funzionario pubblico, l'ordine di nomina;
- se la ricorrente è docente di università - praticante ospedaliero (PU-PH), docente universitario - medico ospedaliero (MCU-PH) o medico ospedaliero (PH), l'ordine di nomina come medico ospedaliero e, se necessario, il decreto o l'ordine di nomina come professore universitario o docente presso le università.

Per ulteriori informazioni, si prega di visitare il sito ufficiale del Consiglio Nazionale del Collegio dei Medici.

*Per andare oltre* Articoli L. 4113-9 e R. 4112-1 del codice di sanità pubblica.

**Rimedi**

Il richiedente o il Consiglio nazionale del Collegio dei medici può impugnare la decisione di registrare o rifiutare la registrazione entro 30 giorni dalla notifica della decisione o dalla decisione implicita di respingerla. Il ricorso viene presentato dinanzi al Consiglio regionale territorialmente competente.

Il consiglio regionale deve decidere entro due mesi dalla ricezione della domanda. In assenza di una decisione entro tale termine, il ricorso è considerato respinto.

Anche la decisione del Consiglio regionale è oggetto di ricorso, entro 30 giorni, al Consiglio nazionale del Collegio dei Medici. La decisione stessa può essere impugnata al Consiglio di Stato.

*Per andare oltre* Articoli L. 4112-4 e R. 4112-5 del codice di salute pubblica.

**Costo**

La registrazione nel consiglio di amministrazione del collegio è gratuita, ma crea l'obbligo di pagare le quote ordinali obbligatorie, il cui importo è fissato annualmente e che deve essere pagato nel primo trimestre dell'anno civile in corso. Il pagamento può essere effettuato online sul sito ufficiale del Consiglio Nazionale del Collegio dei Medici. Come indicazione, nel 2017, l'importo di questo contributo ammonta a 333 euro.

*Per andare oltre* Articolo L. 4122-2 del codice di sanità pubblica.

### b. Fare un pre-report per un esercizio temporaneo e occasionale (LPS)

Qualsiasi cittadino dell'UE o del SEE che sia stabilito e pratici legalmente come medico in uno di questi Stati può esercitare in Francia su base temporanea o occasionale se fa la dichiarazione precedente (vedi supra "2o. b. cittadini dell'UE: per esercizi temporanei e occasionali"). La dichiarazione anticipata deve essere rinnovata ogni anno.

**Si noti che**

Qualsiasi cambiamento nella situazione del richiedente deve essere notificato alle stesse condizioni.

*Per andare oltre* Articoli L. 4112-7 e R. 4112-9-2 del codice di salute pubblica.

**Autorità competente**

La dichiarazione deve essere indirizzata, prima della prima consegna del servizio, al Consiglio nazionale del Collegio dei Medici.

**Termini di rendiconto e ricevimento**

La dichiarazione può essere inviata per posta o direttamente fatta online sul sito ufficiale del Collegio dei Medici.

Quando il Consiglio nazionale del Collegio dei Medici riceve la dichiarazione e tutti i documenti giustificativi necessari, invia al richiedente una ricevuta specificando il suo numero di registrazione e disciplina.

**Si noti che**

Il prestatore di servizi informa l'agenzia nazionale competente di assicurazione malattia della sua prestazione di servizi inviando una copia della ricevuta o con qualsiasi altro mezzo.

*Per andare oltre* Articoli R. 4112-9-2 e R. 4112-11 del codice di salute pubblica.

**Documenti di supporto**

La dichiarazione deve includere:

- Le[modulo di dichiarazione](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=83B6B69A79BA3CA1D089890DFF32CC1E.tplgfr34s_3idArticle=LEGIARTI000036145868&cidTexte=LEGITEXT000036145857&dateTexte=20180115) Completato e firmato;
- Copiare un documento di identificazione valido o qualsiasi documento che attesti la nazionalità del richiedente;
- Copia del titolo di formazione e della designazione di formazione specialistica che consente al richiedente di svolgere la sua attività;
- un certificato di meno di tre mesi che giustifica che il professionista è legalmente accertato e non è soggetto ad alcun divieto di esercitare, anche su base temporanea, la sua professione;
- in cui il professionista ha acquisito la sua formazione in un terzo Stato e riconosciuto in uno Stato dell'UE o del BEE:- Il riconoscimento di queste credenziali di formazione di base e specialistiche,
  + qualsiasi documentazione che giustifichi la sua attività di esercitamento da tre anni a tempo pieno o a tempo parziale;
- in tal caso, una copia dell'affermazione di cui sopra.

*Per andare oltre* articolo R. 4112-9 e seguito del codice sanitario pubblico; 4 dicembre 2017 sulla precedente dichiarazione di fornitura di servizio per le professioni mediche e i farmacisti.

**Tempo**

Entro un mese dalla ricezione della dichiarazione, il Consiglio nazionale dell'Ordine informa il richiedente:

- Se può iniziare a fornire servizi;
- quando la verifica delle qualifiche professionali mostra una differenza sostanziale con la formazione richiesta in Francia, deve dimostrare di aver acquisito le conoscenze e le competenze mancanti sottomettendosi a un calvario Attitudine. Se soddisfa questo controllo, viene informato entro un mese che può iniziare la fornitura di servizi;
- quando la revisione del file evidenzia una difficoltà che richiede ulteriori informazioni, i motivi del ritardo nella revisione del file. Ha quindi un mese per ottenere le informazioni supplementari richieste. In questo caso, prima della fine del secondo mese dalla ricezione di queste informazioni, il Consiglio nazionale informa il richiedente, dopo aver esaminato il suo fascicolo:- se può iniziare la fornitura di servizi,
  + quando la verifica delle qualifiche professionali del ricorrente mostra una differenza sostanziale con la formazione richiesta in Francia, deve dimostrare di aver acquisito le conoscenze e le competenze mancanti, tra cui sottoposto a un test attitudine.

In quest'ultimo caso, se soddisfa tale controllo, viene informato entro un mese che può iniziare la fornitura di servizi. In caso contrario, viene informato che non può iniziare la consegna dei servizi.

In assenza di una risposta da parte del Consiglio nazionale dell'Ordine entro questi tempi, l'erogazione dei servizi può iniziare.

*Per andare oltre* Articolo R. 4112-9-1 del codice di sanità pubblica.

### c. Se necessario, chiedere l'autorizzazione individuale per esercitare

**Per i cittadini dell'UE o del CEE**

I cittadini dell'UE o del CEA con licenza di formazione possono richiedere l'autorizzazione individuale:

- emessi da uno di questi stati che non beneficiano del riconoscimento automatico (vedi supra "2.0). v. Cittadini dell'UE: per un esercizio permanente);00
- rilasciato da un terzo Stato, ma riconosciuto da uno Stato membro dell'UE o del CEA, a condizione che giustifichino la pratica come medico nella specialità per un periodo equivalente a tre anni a tempo pieno in tale Stato membro.

Un comitato di autorizzazione (CAE) esamina la formazione e l'esperienza lavorativa del richiedente.

Può proporre una misura di compensazione:

- quando la formazione è inferiore di almeno un anno a quella della d.C. francese, quando copre argomenti sostanzialmente diversi, o quando una o più componenti dell'attività professionale il cui esercizio è soggetto al suddetto diploma non esistono nella professione corrispondente nello Stato membro d'origine o non sono stati insegnati in tale Stato;
- formazione e l'esperienza del richiedente non sono suscettibili di coprire queste differenze.

A seconda del livello di qualifica richiesto in Francia e di quello detenuto dall'interessato, l'autorità competente può:

- offrire al richiedente la possibilità di scegliere tra un corso di adeguamento o un test attitudinale;
- richiedono un corso di regolazione o un test attitudinale.

Le**test attitudino** è inteso a verificare, attraverso prove scritte o orali o esercizi pratici, l'idoneità del richiedente a esercitare la professione di medico nella specialità pertinente. Si tratta di argomenti che non sono coperti dalla formazione o dall'esperienza del richiedente.

Le**corso di adattamento** ha lo scopo di consentire alle parti interessate di acquisire le competenze necessarie per praticare la professione del medico. Viene effettuata sotto la responsabilità di un medico e può essere accompagnata da una formazione teorica aggiuntiva opzionale. La durata del tirocinio non supera i tre anni. Può essere fatto part-time.

*Per andare oltre* Articoli L. 4111-2 II, L. 4131-1-1, R. 4111-17 a R. 4111-20 e R. 4131-29 del Codice di sanità pubblica.

**Per i cittadini di un terzo Stato**

Può essere applicata l'autorizzazione individuale alla pratica, a condizione che giustifichino un livello sufficiente di conoscenza della lingua francese, persone con un diploma di formazione:

- emesso da uno Stato dell'UE o del SEA la cui esperienza è attestata con qualsiasi mezzo;
- emesso da un terzo stato che consente la pratica della professione di medico nel paese di laurea:- se soddisfano test anonimi per verificare le conoscenze di base e pratiche. Per ulteriori informazioni su questi eventi, visitare il sito ufficiale del Centro Nazionale di Gestione (NMC),
  + se giustificano tre anni di mansioni in un dipartimento o organizzazione accreditata per la formazione degli stagisti.

**Si noti che**

I medici con una laurea specializzata ottenuta nell'ambito dello stage straniero sono considerati aver soddisfatto i test di verifica delle conoscenze.

*Per andare oltre* Articoli L. 4111-2 (I e I bis), D. 4111-1, D.4111-6 e R. 4111-16-2 del codice di sanità pubblica.

**Autorità competente**

La richiesta è indirizzata in due copie, per lettera raccomandata con richiesta di notifica all'unità responsabile delle commissioni di autorizzazione all'esercizio (CAE) della NMC.

L'autorizzazione all'esercizio è rilasciata dal ministro responsabile per la salute dopo la notifica da parte dell'EAC.

*Per andare oltre* Articoli R. 4111-14 e R. 4131-29 del codice di salute pubblica; decreto del 25 febbraio 2010 che stabilisce la composizione del fascicolo da fornire alle AUTORITÀ competenti CAE per l'esame delle domande presentate per l'esercizio in Francia delle professioni di medico, chirurgo dentale, ostetrica e farmacista.

**Tempo**

La NMC riconosce la ricezione della richiesta entro un mese dalla ricezione.

Il silenzio mantenuto per un certo periodo di tempo dalla ricezione del fascicolo completo vale la decisione di respingere la domanda. Questo termine è:

- quattro mesi per le domande presentate da cittadini dell'UE o del CEE con una laurea presso uno di questi Stati;
- sei mesi per domande da parte di cittadini terzi con un diploma da uno Stato dell'UE o del CEE;
- un anno per altre applicazioni. Tale periodo può essere prorogato di due mesi, per decisione dell'autorità ministeriale notificata non oltre un mese prima della scadenza di quest'ultimo, in caso di grave difficoltà nel valutare l'esperienza professionale del candidato.

*Per andare oltre* Articoli R. 4111-2, R. 4111-14 e R. 4131-29 del codice di salute pubblica.

**Documenti di supporto**

Il file della domanda deve contenere:

- un modulo di domanda per l'autorizzazione a praticare la professione, modellato nel programma 1 dell'Ordine del 25 febbraio 2010, compilato, datato e firmato, mostrando, se del caso, la specialità in cui la ricorrente presenta;
- Una fotocopia di un documento d'identità valido
- Una copia del titolo di formazione che consente la pratica della professione nello stato di ottenimento e, se necessario, una copia del titolo di formazione specialistica;
- Se necessario, una copia dei diplomi supplementari;
- qualsiasi prova utile che giustifichi la formazione continua, l'esperienza e le competenze acquisite durante l'esercizio professionale in uno Stato dell'UE o del SEA, o in uno stato terzo (certificati di funzioni, relazione di attività, valutazione operativa, ecc. ) ;
- nell'ambito di funzioni svolte in uno Stato diverso dalla Francia, una dichiarazione dell'autorità competente di tale Stato, di meno di un anno, che attesta l'assenza di sanzioni nei confronti della ricorrente.

A seconda della situazione del richiedente, è necessaria un'ulteriore documentazione di supporto. Per ulteriori informazioni, si prega di visitare il sito ufficiale della NMC.

**Cosa sapere**

I documenti giustificativi devono essere scritti in francese o tradotti da un traduttore certificato.

*Per andare oltre* : decreto del 25 febbraio 2010 che stabilisce la composizione del fascicolo da fornire alle commissioni di autorizzazione competenti per l'esame delle domande presentate per la pratica in Francia delle professioni del medico, chirurgo dentale, ostetrica e farmacista; 17 novembre 2014 n. DGOS/RH1/RH2/RH4/2014/318.

### d. Rimedi

#### Centro di assistenza francese

Il Centro ENIC-NARIC è il centro francese per informazioni sul riconoscimento accademico e professionale dei diplomi.

#### Solvit

SOLVIT è un servizio fornito dall'Amministrazione nazionale di ogni Stato membro dell'UE o parte dell'accordo DEL SEA. Il suo obiettivo è quello di trovare una soluzione a una controversia tra un cittadino dell'UE e l'amministrazione di un altro di questi Stati. SOLVIT interviene in particolare nel riconoscimento delle qualifiche professionali.

**Condizioni**

L'interessato può utilizzare SOLVIT solo se stabilisce:

- che la pubblica amministrazione di uno Stato dell'UE non ha rispettato i propri diritti ai sensi del diritto dell'UE in qualità di cittadino o di un'altra impresa di un altro Stato dell'UE;
- che non ha già avviato un'azione legale (l'azione amministrativa non è considerata come tale).

**Procedura**

Il cittadino deve compilare un modulo di reclamo online. Una volta che il suo fascicolo è stato inviato, SOLVIT lo contatta entro una settimana per richiedere, se necessario, ulteriori informazioni e per verificare che il problema rientri nelle sue competenze.

**Documenti di supporto**

Per entrare in SOLVIT, il cittadino deve comunicare:

- Dati di contatto completi
- Descrizione dettagliata del suo problema
- tutte le prove nel fascicolo (ad esempio, corrispondenza e decisioni ricevute dall'autorità amministrativa competente).

**Tempo**

SOLVIT si impegna a trovare una soluzione entro dieci settimane il caso è stato rilevato dal centro SOLVIT nel paese in cui si è verificato il problema.

**Costo**

Gratuito.

**Esito della procedura**

Alla fine del periodo di 10 settimane, SOLVIT presenta una soluzione:

- Se questa soluzione risolve la controversia sull'applicazione del diritto europeo, la soluzione viene accettata e il caso viene chiuso;
- in caso di soluzione, il caso viene chiuso come irrisolto e deferito alla Commissione europea.

**Ulteriori informazioni**

SOLVIT in Francia: Segreteria generale per gli affari europei, 68 rue de Bellechasse, 75700, Parigi,([sito ufficiale](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

