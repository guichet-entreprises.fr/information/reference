﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP205" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Saúde" -->
<!-- var(title)="Oto-rinoceronte-laringologia e cirurgia cervico-facial" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="saude" -->
<!-- var(title-short)="oto-rinoceronte-laringologia-cirurgia" -->
<!-- var(url)="https://www.guichet-qualifications.fr/pt/dqp/saude/oto-rinoceronte-laringologia-e-cirurgia-cervico-facial.html" -->
<!-- var(last-update)="2020-04-15 17:21:55" -->
<!-- var(url-name)="oto-rinoceronte-laringologia-e-cirurgia-cervico-facial" -->
<!-- var(translation)="Auto" -->


Oto-rinoceronte-laringologia e cirurgia cervico-facial
======================================================

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:21:55<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

Otorrinolaringologia e cirurgia cervicofacial é uma disciplina cirúrgica que suporta distúrbios de pescoço e face de orelha, nariz e laringe (ENT).

Dois graus. Qualificações profissionais
---------------------------------------

### a. Requisitos nacionais

#### Legislação nacional

De acordo com o artigo 4111-1 do Código de Saúde Pública, para exercer legalmente a prática como médico na França, os interessados devem cumprir cumulativamente as seguintes três condições:

- realizar o diploma estatal francês de doutor em medicina ou um título ou diploma que confere o título de médico;
- seja de nacionalidade francesa, cidadania andorrana ou nacional de um Estado-Membro da União Europeia (UE) ou parte do acordo sobre a Área Econômica Europeia (EEE) ou Marrocos, sujeito à aplicação das regras derivadas do Código sanitário compromissos públicos ou internacionais. No entanto, essa condição não se aplica a um médico com o diploma estatal francês de médico médico;
- ser incluído na Tabela do Colégio de Médicos (ver infra "5.0). a. Solicitar inclusão na lista do Colégio de Médicos").

No entanto, as pessoas que não cumprirem as qualificações de diploma ou nacionalidade podem ser autorizadas a praticar como médico por ordem individual do Ministro da Saúde (ver infra "5o. c. Se necessário, solicite uma licença de exercício individual").

*Para ir mais longe* Artigos L. 4111-1, L. 4112-6, L. 4112-7 e L. 4131-1 do Código de Saúde Pública.

**Note que**

Ao não cumprir todas essas condições, a prática da profissão de médico é ilegal e punível com dois anos de prisão e multa de 30.000 euros.

**Bom saber: reconhecimento automático do diploma**

De acordo com o artigo 4131-1 do Código de Saúde Pública, os cidadãos da UE ou da EEE podem exercer a advocacia como médico se tiverem um dos seguintes títulos:

- documentos de treinamento médico emitidos por um estado da UE ou da EEE de acordo com as obrigações da UE e listados no anexo do decreto de 13 de Julho de 2009 que estabelece listas e condições para o reconhecimento da UE formação de médicos e médicos especialistas emitidos por Estados-Membros da UE ou partes no acordo EEE abrangido pelo artigo 4131-1 do Código de Saúde Pública;
- certificados de treinamento médico emitidos por um estado da UE ou da EEE de acordo com as obrigações da UE, não na lista acima, se forem acompanhados de um certificado daquele estado certificando que eles estão sancionando o treinamento em conformidade com essas obrigações e são assimilados por ele aos títulos de treinamento desta lista;
- os certificados de treinamento médico emitidos por um estado da UE ou eee treinamento médico sancionador iniciado naquele estado antes das datas do decreto acima mencionado e não de acordo com as obrigações da UE, se acompanhado um certificado de um desses estados certificando que o titular dos títulos de formação dedicou-se, naquele Estado, de forma eficaz e legal, ao ato da profissão de médico na especialidade em causa por pelo menos três anos consecutivos no Cinco anos antes da emissão do certificado;
- certificados de treinamento médico emitidos pela antiga Tchecoslováquia, a antiga União Soviética ou a antiga Iugoslávia ou que o treinamento de sanções começou antes da data de independência da República Tcheca, Eslováquia, Estônia, Letônia, Lituânia ou Eslovênia, se acompanhadas de um certificado das autoridades competentes de um desses estados, certificando que eles têm a mesma validade legal que os documentos de treinamento emitidos por esse estado. Este certificado é acompanhado de um certificado emitido pelas mesmas autoridades indicando que o titular exerceu naquele Estado, de forma efetiva e legal, a profissão de médico na especialidade em causa por pelo menos três anos consecutivos no Cinco anos antes da emissão do certificado;
- certificados de treinamento médico emitidos por um estado da UE ou eEE que não estão na lista acima se estiverem acompanhados de um certificado emitido pelas autoridades competentes daquele estado certificando que o titular do certificado de treinamento foi estabelecido em seu território na data estabelecida no referido decreto e que adquiriu o direito de exercer as atividades de um clínico geral sob o seu regime nacional de seguridade social;
- certificados de treinamento médico emitidos por um estado da UE ou eee treinamento médico sancionador iniciado naquele estado antes das datas no decreto acima mencionado e não em consonância com as obrigações da UE, mas permitindo exercer legalmente a profissão de médico no estado que as emitiu, se o médico justificar ter realizado na França nos últimos cinco anos três anos consecutivos de funções hospitalares em tempo integral na especialidade correspondente formação como adido associado, clínico associado, assistente associado ou funções acadêmicas como chefe de clínica associada das universidades ou assistente associado das universidades, desde que tenha sido nomeado funções hospitalares ao mesmo tempo;
- Os certificados de treinamento médico especializados da Itália na lista acima mencionada santem a formação médica especializada iniciada naquele estado após 31 de dezembro de 1983 e antes de 1 de Janeiro de 1991, se acompanhada por um certificado emitido pelas autoridades estaduais indicando que seu titular exerceu naquele estado, de forma efetiva e legal, a profissão de médico na especialidade em causa por pelo menos sete anos consecutivos nos dez anos anteriores emissão do certificado.

*Para ir mais longe* Artigo 4131-1 do Código de Saúde Pública; Decreto de 13 de Julho de 2009 que estabelece as listas e condições para o reconhecimento dos documentos de formação de médicos e médicos especialistas emitidos pelos Estados-Membros da União Europeia ou partes do acordo sobre a Área Econômica Europeia referido no 2º Seção L. 4131-1 do Código de Saúde Pública.

#### Treinamento

Os estudos médicos consistem em três ciclos com duração total entre nove e onze anos, dependendo do curso escolhido.

A capacitação, que acontece na universidade, inclui muitos estágios e é pontuada por duas competições:

- a primeira ocorre no final do primeiro ano. Este ano de estudo, chamado de "primeiro ano de estudos comuns de saúde" (PACES) é comum a estudantes de medicina, farmácia, odontologia, fisioterapia e parteiras. Ao final desta primeira competição, os alunos são classificados de acordo com seus resultados. Aqueles em uma posição útil o numero clausus podem continuar seus estudos e escolher, se necessário, continuar o treinamento que leve à prática da medicina;
- o segundo ocorre ao final do segundo ciclo (ou seja, ao final do sexto ano de estudo): este concurso é chamado de testes de classificação nacional (ECNs) ou anteriormente "internatos". Ao final desta competição, os alunos escolhem, com base em seu ranking, sua especialidade e/ou sua cidade de atribuição. A duração dos estudos que se seguem varia dependendo da especialidade escolhida.

Para obter um diploma estadual (DE) como doutor em medicina, o aluno deve validar todos os seus estágios, seu diploma de estudos especializados (DES) e apoiar sua tese com sucesso.

*Para ir mais longe* Artigo 632-1 do Código educacional.

**É bom saber**

Os estudantes de medicina devem realizar a vacinação obrigatória. Para obter mais informações, consulte a Seção R. 3112-1 do Código de Saúde Pública.

**Diploma de educação geral em ciências médicas**

O primeiro ciclo é sancionado pelo diploma de formação geral em ciências médicas. Consiste em seis semestres e corresponde ao nível da licença. Os dois primeiros semestres correspondem aos PACES.

O objetivo do treinamento é:

- a aquisição do conhecimento científico básico, essencial para o domínio subsequente do conhecimento e do know-how necessários para a prática das profissões médicas. Essa base científica é ampla e abrange biologia, certos aspectos das ciências exatas e diversas disciplinas das ciências humanas e sociais;
- a abordagem fundamental do homem saudável e do homem doente, incluindo todos os aspectos da semiologia.

Inclui ensinamentos teóricos, metodológicos, aplicados e práticos, bem como a realização de estágios, incluindo um curso de quatro semanas de introdução em um hospital.

*Para ir mais longe* : ordem de 22 de março de 2011 referente ao regime de formação para o diploma geral de formação em ciências médicas.

**Curso de formação em ciências médicas**

O segundo ciclo de estudos médicos é sancionado pelo diploma de formação aprofundada em ciências médicas. Consiste em seis semestres de treinamento e corresponde ao nível de mestrado.

Seu objetivo é adquirir as habilidades genéricas que permitem aos alunos posteriormente desempenhar funções de pós-graduação em hospitais e ambientes ambulatoriais e adquirir as habilidades profissionais da formação em eles se comprometerão durante sua especialização.

As habilidades a serem adquiridas são as de comunicador, clínico, co-operador, membro de uma equipe multiprofissional de saúde, ator de saúde pública, cientista e líder ético e ético. O aluno também deve aprender a ser reflexivo.

As aulas são focadas no que é comum ou grave ou um problema de saúde pública e o que é clinicamente exemplar.

Os objetivos do treinamento são:

- a aquisição de conhecimentosobre processos fisiopatológicos, patologia, bases terapêuticas e prevenção complementando e aprofundando os adquiridos durante o ciclo anterior;
- treinamento no processo científico
- Aprendizado do raciocínio clínico
- habilidades genéricas para se preparar para a pós-graduação em medicina.

Além dos ensinamentos teóricos e práticos, a formação inclui a conclusão de trinta e seis meses de estágios e vinte e cinco guardas.

*Para ir mais longe* : ordem de 8 de abril de 2013 referente ao currículo do primeiro e segundo ciclos de estudos médicos.

**Diploma de Estudos Especializados (DES)**

O acesso ao terceiro ciclo é através da NCT. Para exercer ootorrinolaringologista, o profissional deve obter Otorrinolaringologia (ORL) e Cirurgia Cervico-facial (CCF) DES.

Este diploma consiste em doze semestres, incluindo:

- pelo menos nove semestres na especialidade;
- pelo menos três semestres em um lugar de estágio sem supervisão universitária.

A formação do DES é dividida em três fases: a fase base, a fase de aprofundamento e a fase de consolidação.

**A fase base**

Com duração de dois semestres, permite ao candidato adquirir o conhecimento básico da especialidade escolhida e realizar:

- estágio em hospital licenciado em local de ent sênior e CCF em um setor de exploração não funcional;
- um estágio em um hospital licenciado principal em outra especialidade cirúrgica.

**A fase de aprofundamento**

Com duração de seis semestres, permite ao candidato adquirir os conhecimentos e habilidades necessários para a prática da especialidade escolhida e para realizar:

- quatro estágios em um hospital licenciado em ENT e SBB, incluindo pelo menos um no setor de exploração funcional;
- estágio em hospital licenciado em neurocirurgia, cirurgia maxilofacial, cirurgia torácica e cardiovascular, cirurgia vascular, cirurgia plástica, reconstrutiva e restauradora e complementar ENT e CCF;
- um estágio gratuito.

**A fase de consolidação**

O processo de dois anos permite ao candidato consolidar seus conhecimentos e realizar:

- um estágio de um ano em um local credenciado pela ORL sênior e ccf;
- um estágio de um ano, seja em um local hospitalar credenciado como diretor em ENT e CCF, ou a forma de um estágio misto ou acoplado em vagas e/ou com um profissional universitário trainee-master, credenciado como diretor em ENT e CCF.

O DES é elegível para a qualificação de um especialista em ENT e CCF.

*Para ir mais longe* :[Apêndice II](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000036237037) do decreto de 27 de novembro de 2017 que altera o decreto de 12 de abril de 2017 relativo à organização do terceiro ciclo de estudos médicos e ao decreto de 21 de abril de 2017 relativo aos modelos de conhecimento, habilidades e formação dos diplomas de estudo e estabelecer a lista desses diplomas e as opções e treinamento selecionais e de formação transversal dos estudos médicos de pós-graduação.

#### Custos associados à qualificação

O treinamento que leva ao diploma de médico é pago e o custo varia dependendo da instituição escolhida. Para mais informações, é aconselhável verificar com as instituições envolvidas.

### b. Cidadãos da UE: para exercícios temporários e ocasionais (Free Service Delivery (LPS))

Um médico que é membro de um estado da UE ou EEE que esteja estabelecido e legalmente pratica em um desses estados pode realizar atos de sua profissão na França de forma temporária e ocasional, desde que ele tenha anteriormente declaração prévia ao Conselho Nacional do Colégio de Médicos (ver infra "5o. b. Fazer uma pré-declaração para cidadãos da UE ou da EEE envolvidos em atividades temporárias e ocasionais").

**O que saber**

Não é necessária inscrição na lista do Colégio de Médicos para o profissional de serviço gratuito (LPS). Portanto, não é necessário pagar dívidas ordinais. O médico é simplesmente registrado em uma lista específica mantida pelo Conselho Nacional do Colégio de Médicos.

A pré-declaração deve ser acompanhada de uma declaração sobre as habilidades linguísticas necessárias para a realização do serviço. Neste caso, o controle da proficiência linguística deve ser proporcional à atividade a ser realizada e realizada uma vez reconhecida a qualificação profissional.

Quando os títulos de treinamento não recebem reconhecimento automático (ver supra "2.0). a. Legislação nacional"), as qualificações profissionais do prestador são verificadas antes da prestação do primeiro serviço. No caso de diferenças substanciais entre as qualificações do interessado e o treinamento necessário na França que possam prejudicar a saúde pública, o requerente é submetido a um teste.

O médico em situação de LPS é obrigado a respeitar as regras profissionais aplicáveis na França, incluindo todas as regras éticas (ver infra "3). Condições de honorabilidade, regras éticas, ética"). Está sujeito à jurisdição disciplinar do Colégio de Médicos.

**Note que**

O serviço é realizado o título profissional francês de médico. No entanto, quando as qualificações de treinamento não são reconhecidas e as qualificações não foram verificadas, o desempenho é realizado o título profissional do Estado de Estabelecimento, a fim de evitar confusão Com o título profissional francês.

*Para ir mais longe* Artigo 4112-7 do Código de Saúde Pública.

### c. Cidadãos da UE: para um exercício permanente (Estabelecimento Livre (LE))

**O reconhecimento automático dos diplomas obtidos em um estado da UE**

O artigo 4131-1 do Código de Saúde Pública cria um regime de reconhecimento automático na França de certos diplomas ou títulos, se houver, acompanhados de certificados, obtidos em um estado da UE ou eee (ver acima "2". a. Legislação Nacional").

Cabe ao conselho departamental do colégio de médicos responsável verificar a regularidade dos diplomas, títulos, certificados e certificados, conceder o reconhecimento automático e, em seguida, decidir sobre o pedido de inscrição na lista da Ordem. .

*Para ir mais longe* Artigo 4131-1 do Código de Saúde Pública; Decreto de 13 de Julho de 2009 que estabelece as listas e condições para o reconhecimento dos documentos de formação de médicos e médicos especialistas emitidos pelos Estados-Membros da União Europeia ou partes do acordo sobre a Área Econômica Europeia referido no 2º Seção L. 4131-1 do Código de Saúde Pública.

**O regime pejorativo: autorização prévia**

Se a UE ou o eee nacional não se qualificar para o reconhecimento automático de suas credenciais, ele ou ela se enquadra em um regime de autorização (veja abaixo "5o). c. Se necessário, solicite uma licença de exercício individual").

Indivíduos que não recebem reconhecimento automático, mas que possuem uma designação de treinamento para exercer legalmente a prática como médico podem ser individualmente autorizados a praticar na especialidade em causa Ministro da Saúde, após assessoria de uma comissão composta por profissionais.

Se o exame das qualificações profissionais atestados pelas credenciais de formação e pela experiência profissional mostra diferenças substanciais com as qualificações necessárias para o acesso à profissão na especialidade em questão e seu exercício na França, o interessado deve submeter-se a uma medida de compensação.

Dependendo do nível de qualificação exigido na França e do detido pela pessoa em causa, a autoridade competente pode:

- Oferecer ao candidato uma escolha entre um curso de ajuste ou um teste de aptidão;
- requerem um curso de ajuste e/ou teste de aptidão.

*Para ir mais longe* Artigo 4131-1-1 do Código de Saúde Pública.

Três graus. Condições de honorabilidade, regras éticas, ética
-------------------------------------------------------------

### a. Conformidade com o Código de Ética dos Médicos

As disposições do Código de Ética Médica são exigidas para todos os médicos que praticam na França, sejam eles no conselho da Ordem ou estão isentos dessa obrigação (ver infra "5o. a. Solicitar inclusão na lista do Colégio de Médicos").

**O que saber**

Todas as disposições do Código de Ética estão codificadas nos seções R. 4127-1 a R. 4127-112 do Código de Saúde Pública.

Assim, o médico deve respeitar os princípios de moralidade, probidade e dedicação essenciais à prática da medicina. Ele também está sujeito a sigilo médico e deve exercer-se independentemente.

*Para ir mais longe* Artigos R. 4127-1 a R. 4127-112 do Código de Saúde Pública.

### b. Atividades cumulativas

O médico só pode exercer qualquer outra atividade se tal combinação for compatível com os princípios de independência profissional e dignidade impostos a ele. O acúmulo de atividades não deve permitir que ele aproveite suas prescrições ou seus conselhos médicos.

Assim, o médico não pode combinar o exercício médico com outra atividade próxima ao campo da saúde. Em particular, ele está proibido de praticar como oftalmologista, paramédico ou gerente de uma empresa de ambulâncias, fabricante ou vendedor de dispositivos médicos, proprietário ou gerente de um hotel para curadores, academia, spa, prática de massagem.

Da mesma forma, um médico que cumpra um mandato eletivo ou administrativo é proibido de usá-lo para aumentar sua clientela.

*Para ir mais longe* Os artigos R. 4127-26 e R. 4127-27 do Código de Saúde Pública.

### c. Condições de honorabilidade

Para exercer a prática, o médico deve certificar que nenhum processo que possa dar origem a uma condenação ou uma sanção que possa afetar sua inclusão no conselho são contra ele.

*Para ir mais longe* Artigo 4112-1 do Código de Saúde Pública.

### d. Obrigação de desenvolvimento profissional contínuo

Os médicos devem participar de um programa de desenvolvimento profissional contínuo de vários anos. O programa tem como foco avaliar as práticas profissionais, melhorar as habilidades, melhorar a qualidade e a segurança do cuidado, manter e atualizar conhecimentos e habilidades.

Todas as ações realizadas pelos médicos sua obrigação de se desenvolver profissionalmente são traçadas em um documento específico que atesta o cumprimento dessa obrigação.

*Para ir mais longe* Artigos L. 4021-1 e artigos subsequentes R. 4021-4 e o seguinte do Código de Saúde Pública.

### e. Aptidão física

Os médicos não devem apresentar enfermidade ou patologia incompatível com o desprovido da profissão (ver infra "5.0). a. Solicitar inclusão na lista do Colégio de Médicos").

*Para ir mais longe* Artigo 4112-2 do Código de Saúde Pública.

É um de quatro graus. Seguro
----------------------------

### a. Obrigação de fazer seguro de responsabilidade profissional

Como profissional de saúde engajado na prevenção, diagnóstico ou cuidado, o médico deve, se praticar em uma capacidade liberal, fazer um seguro de responsabilidade profissional.

Se ele se exercita como empregado, cabe ao empregador fazer esse seguro para seus funcionários pelos atos realizados durante essa atividade.

*Para ir mais longe* Artigo 1142-2 do Código de Saúde Pública.

### b. Obrigação de aderir ao fundo de pensão independente dos médicos da França (CARMF)

Qualquer médico registrado no conselho da Ordem e praticando na forma liberal (mesmo que em tempo integral e mesmo que ele também esteja empregado) tem a obrigação de aderir ao CARMF.

**Tempo**

O interessado deve se cadastrar no CARMF no prazo de um mês após o início de sua atividade liberal.

**Termos**

O interessado deverá devolver o formulário de declaração, preenchido, datado e contraassinado pelo conselho departamental do Colégio de Médicos. Este formulário pode ser baixado a partir do[CARMF](http://www.carmf.fr/).

**O que saber**

No caso de uma prática em uma empresa de prática liberal (SEL), a adesão ao CARMF também é obrigatória para todos os parceiros profissionais que ali praticam.

### c. Obrigação de notificação do Medicare

Uma vez na lista da Ordem, o médico que pratica de forma liberal deve declarar sua atividade com o Fundo de Seguro De Saúde Primária

**Termos**

As inscrições no CPAM podem ser feitas online no site oficial do Medicare.

**Documentos de suporte**

O inscrito deve fornecer um arquivo completo, incluindo:

- Copiando uma peça válida de identificação
- um extrato de identidade bancária profissional (RIB)
- se necessário, o título (s) para permitir o acesso ao Setor 2.

Para obter mais informações, consulte a seção sobre a instalação de médicos no site do Seguro de Saúde.

Cinco graus. Procedimentos e formalidades de reconhecimento de qualificação
---------------------------------------------------------------------------

### a. Solicitar registro na Mesa do Colégio de Médicos

O registro no conselho da Ordem é obrigatório para exercer legalmente a atividade de um médico na França.

A inscrição no quadro da Ordem não se aplica:

- Cidadãos da UE ou eee que estão estabelecidos e que estão legalmente praticando como médicos em um estado da UE ou EEE quando realizam atos de sua profissão na França, de forma temporária e ocasional (ver supra "2o. b. Cidadãos da UE: para exercícios temporários e ocasionais);
- médicos pertencentes aos executivos ativos do Serviço de Saúde das Forças Armadas;
- médicos que, com status de servidor público ou agente de uma autoridade local, não são chamados, no curso de suas funções, a exercer a medicina.

*Para ir mais longe* Artigos L. 4112-5 a L. 4112-7 do Código de Saúde Pública.

**Note que**

O cadastro no quadro da Ordem permite a emissão automática e gratuita do Cartão Profissional de Saúde (CPS). O CPS é um cartão eletrônico de identidade. É protegido por um código confidencial e contém, entre outras coisas, os dados de identificação do médico (identidade, ocupação, especialidade). Para obter mais informações, recomenda-se consultar o site do governo[Agência Francesa de Saúde Digital](http://esante.gouv.fr/).

**Autoridade competente**

O pedido de registro é endereçado ao Presidente do Conselho do Colégio de Médicos do Departamento no qual o interessado deseja estabelecer sua residência profissional.

A solicitação pode ser submetida diretamente ao conselho departamental da Ordem em causa ou enviada a ela por correio registrado com solicitação de aviso de recebimento.

*Para ir mais longe* Artigo 4112-1 do Código de Saúde Pública.

**O que saber**

No caso de transferência de sua residência profissional fora do departamento, o praticante é obrigado a solicitar seu afastamento da ordem do departamento onde estava praticando e seu registro na ordem de sua nova residência profissional.

*Para ir mais longe* Artigo 4112-3 do Código de Saúde Pública.

**Procedimento**

Após o recebimento do pedido, o conselho municipal nomeia um relator que conduz o pedido e faz um relatório por escrito.

O conselho verifica os títulos do candidato e solicita a divulgação do boletim 2 da ficha criminal do candidato. Em particular, verifica-se que o candidato:

- cumpre as condições necessárias de moralidade e independência (ver supra "3.3. c. Condições de honorabilidade");
- atende aos requisitos de competência necessários;
- não apresenta incapacidade ou condição patológica incompatível com o desprovido da profissão (ver supra "3". e. Aptidão física").

Em caso de séria dúvida sobre a competência profissional do requerente ou a existência de deficiência ou condição patológica incompatível com o ato da profissão, o conselho municipal encaminha a questão ao conselho regional ou inter-regional Experiência. Se, na opinião do laudo pericial, houver uma inadequação profissional que torne perigosa a prática da profissão, o conselho departamental recusa o registro e especifica as obrigações de formação do profissional.

Nenhuma decisão de recusar o registro pode ser tomada sem que a pessoa seja convidada com pelo menos quinze dias de antecedência por uma carta recomendada solicitando que o aviso de recebimento compareça perante o Conselho para explicar.

A decisão do Conselho Universitário é notificada no prazo de uma semana ao indivíduo, ao Conselho Nacional do Colégio de Médicos e ao Diretor Geral da Agência Regional de Saúde (ARS). A notificação é por carta recomendada com solicitação de aviso de recebimento.

A notificação menciona recursos contra a decisão. A decisão de recusar deve ser justificada.

*Para ir mais longe* Os artigos R. 4112-2 e R. 4112-4 do Código de Saúde Pública.

**Tempo**

O Presidente reconhece o recebimento do arquivo completo no prazo de um mês após o seu registro.

O conselho departamental do Colégio deve decidir sobre o pedido de registro dentro de três meses após o recebimento do arquivo completo da solicitação. Se uma resposta não for respondida dentro desse prazo, o pedido de registro será considerado rejeitado.

Esse período é aumentado para seis meses para nacionais de países terceiros quando uma investigação deve ser realizada fora da França metropolitana. O interessado é então notificado.

Também pode ser prorrogado por um período não superior a dois meses pelo conselho departamental quando um parecer pericial tiver sido ordenado.

*Para ir mais longe* Artigos L. 4112-3 e R. 4112-1 do Código de Saúde Pública.

**Documentos de suporte**

O requerente deve enviar um arquivo de inscrição completo, incluindo:

- duas cópias do questionário padronizado com um documento de identificação com foto preenchido, datado e assinado, disponível nos conselhos departamentais do Colégio ou diretamente para download no[site oficial](https://www.conseil-national.medecin.fr/) Conselho Nacional do Colégio de Médicos;
- Uma fotocópia de um DOCUMENTO DE IDENTIDADE válido ou, se necessário, um certificado de nacionalidade emitido por uma autoridade competente;
- Se aplicável, uma fotocópia do cartão de residência familiar de um cidadão da UE válido, o cartão de residente-CE de longo prazo válido ou o cartão de residente com status de refugiado válido;
- Nesse caso, uma fotocópia do cartão de crédito europeu válido;
- uma cópia, acompanhada se necessário por uma tradução de um tradutor certificado, dos cursos de formação aos quais estão anexados:- quando o requerente é um cidadão da UE ou EEE, o certificado ou certificadofornecido (ver acima "2. a. Requisitos nacionais"),
  + o requerente recebe uma permissão de exercício individual (ver supra "2. v. Cidadãos da UE: para um exercício permanente"), uma cópia desta autorização,
  + Quando o requerente apresenta um diploma emitido em um estado estrangeiro cuja validade seja reconhecida em território francês, a cópia dos títulos aos quais esse reconhecimento pode ser subordinado;
- para nacionais de um estado estrangeiro, um extrato de antecedentes criminais ou um documento equivalente com menos de três meses de idade, emitido por uma autoridade competente do Estado de origem. Esta parte pode ser substituída, para cidadãos da UE ou da EEE que exijam prova de moralidade ou honorabilidade para acesso à atividade médica, por um certificado, com menos de três meses de idade, da autoridade competente do Estado. certificando que essas condições morais ou de honra são atendidas;
- uma declaração sobre a honra do requerente certificando que nenhum processo que possa dar origem a uma condenação ou sanção que possa afetar a listagem no conselho está contra ele;
- um certificado de deslistagem, registro ou registro emitido pela autoridade com a qual o requerente foi previamente registrado ou registrado ou, sem isso, uma declaração de honra do requerente certificando que ele ou ela nunca foi registrado ou registrado ou, caso contrário, um certificado de registro ou registro em um estado da UE ou EEE;
- todas as evidências de que o candidato possui as habilidades linguísticas necessárias para exercer a profissão;
- Um currículo
- contratos e endossamentos para o desempenho da profissão, bem como aqueles relativos ao uso do equipamento e às instalações em que o requerente pratica;
- Se a atividade for realizada a forma de SEL ou de sociedade civil profissional (SCP), os estatutos dessa empresa e seus possíveis endossamentos;
- Se o requerente for servidor público ou funcionário público, a ordem de nomeação;
- se o candidato for professor de universidades - médico hospitalar (PU-PH), professor universitário - médico-hospitalar (MCU-PH) ou médico hospitalar (PH), a ordem de nomeação como médico hospitalar e, se necessário, o decreto ou ordem de nomeação como professor universitário ou professor em universidades.

Para mais informações, visite o site oficial do Conselho Nacional do Colégio de Médicos.

*Para ir mais longe* Artigos L. 4113-9 e R. 4112-1 do Código de Saúde Pública.

**Remédios**

O requerente ou o Conselho Nacional do Colégio de Médicos pode contestar a decisão de registrar ou recusar o registro no prazo de 30 dias após a notificação da decisão ou a decisão implícita de rejeitá-la. O recurso é apresentado ao conselho regional territorialmente competente.

O conselho regional deve decidir dentro de dois meses após o recebimento do pedido. Na ausência de decisão nesse prazo, o recurso é considerado indeferido.

A decisão do conselho regional também está sujeita a recurso, no prazo de 30 dias, ao Conselho Nacional do Colégio de Médicos. A decisão em si pode ser apelada ao Conselho de Estado.

*Para ir mais longe* Artigos L. 4112-4 e R. 4112-5 do Código de Saúde Pública.

**Custo**

A inscrição no conselho do Colégio é gratuita, mas cria a obrigação de pagar as obrigações ordinárias, sendo o valor definido anualmente e que deve ser pago no primeiro trimestre do ano civil vigente. O pagamento pode ser feito online no site oficial do Conselho Nacional do Colégio de Médicos. Como indicação, em 2017, o valor dessa contribuição é de 333 euros.

*Para ir mais longe* Artigo 4122-2 do Código de Saúde Pública.

### b. Faça um pré-relatório para um exercício temporário e casual (LPS)

Qualquer cidadão da UE ou da EEE que esteja estabelecido e legalmente pratica como médico em um desses estados pode exercer na França de forma temporária ou ocasional se fizer a declaração anterior (ver supra "2o. b. Cidadãos da UE: para exercícios temporários e ocasionais"). A declaração antecipada deve ser renovada a cada ano.

**Note que**

Qualquer alteração na situação do requerente deve ser notificada nas mesmas condições.

*Para ir mais longe* Artigos L. 4112-7 e R. 4112-9-2 do Código de Saúde Pública.

**Autoridade competente**

A declaração deve ser endereçada, antes da primeira prestação de serviço, ao Conselho Nacional do Colégio de Médicos.

**Termos de comunicação e recebimento**

A declaração pode ser enviada por correio ou diretamente online no site oficial do Colégio de Médicos.

Quando o Conselho Nacional do Colégio de Médicos recebe a declaração e todos os documentos de apoio necessários, ele envia ao requerente um recibo especificando seu número de inscrição e disciplina.

**Note que**

O prestador de serviços informa à agência nacional de seguros de saúde relevante de sua prestação de serviços, enviando uma cópia do recibo ou por qualquer outro meio.

*Para ir mais longe* Artigos R. 4112-9-2 e R. 4112-11 do Código De Saúde Pública.

**Documentos de suporte**

A declaração deve incluir:

- O[forma de declaração](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=83B6B69A79BA3CA1D089890DFF32CC1E.tplgfr34s_3idArticle=LEGIARTI000036145868&cidTexte=LEGITEXT000036145857&dateTexte=20180115) Concluído e assinado;
- Cópia de uma documento válida de identificação ou qualquer documento que comteste a nacionalidade do requerente;
- Copiar o título de treinamento e a designação de treinamento especializado permitindo que o candidato realize sua atividade;
- um certificado de menos de três meses justificando que o profissional está legalmente estabelecido e não está sujeito a qualquer proibição de exercer, mesmo que temporariamente, sua profissão;
- onde o profissional adquiriu sua formação em um terceiro estado e reconhecido em um estado da UE ou EEE:- Reconhecimento dessas credenciais básicas e especializadas de treinamento,
  + qualquer documentação que justifique que ele vem praticando há três anos em tempo integral ou meio período;
- se assim for, uma cópia da declaração acima mencionada.

*Para ir mais longe* O artigo 4112-9 e o seguinte do Código de Saúde Pública; 4 de dezembro de 2017 sobre a declaração prévia de prestação de serviços para as profissões médicas e farmacêuticos.

**Tempo**

Dentro de um mês após o recebimento da declaração, o Conselho Nacional da Ordem informa ao requerente:

- Se ele pode ou não começar a prestar serviços;
- quando a verificação das qualificações profissionais mostra uma diferença substancial com o treinamento exigido na França, ele deve provar ter adquirido os conhecimentos e habilidades faltantes submetendo-se a uma provação Aptidão. Se ele cumprir esse cheque, ele é informado dentro de um mês que ele pode iniciar a prestação de serviços;
- quando a revisão do arquivo destaca uma dificuldade em exigir mais informações, as razões para a demora na revisão do arquivo. Ele então tem um mês para obter as informações adicionais solicitadas. Neste caso, antes do final do segundo mês a partir do recebimento dessas informações, o Conselho Nacional informa o requerente, após a revisão de seu arquivo:- se ele pode ou não começar a prestação de serviços,
  + quando a verificação das qualificações profissionais do requerente mostra uma diferença substancial com o treinamento exigido na França, ele deve demonstrar que adquiriu os conhecimentos e habilidades faltantes, incluindo submeter-se a um teste de aptidão.

Neste último caso, se ele cumprir esse controle, ele é informado dentro de um mês que pode iniciar a prestação de serviços. Caso contrário, ele é informado de que não pode iniciar a prestação de serviços.

Na ausência de uma resposta do Conselho Nacional da Ordem dentro desses prazos, a prestação de serviços pode começar.

*Para ir mais longe* Artigo 4112-9-1 do Código de Saúde Pública.

### c. Se necessário, procure autorização individual para exercer

**Para cidadãos da UE ou EEE**

Cidadãos da UE ou da EEE com licença de treinamento podem solicitar autorização individual:

- emitido por um desses estados que não se beneficia do reconhecimento automático (ver supra "2.0). v. Cidadãos da UE: para um exercício permanente);00
- emitidos por um terceiro Estado, mas reconhecidos por um Estado-Membro da UE ou da EEE, desde que justifiquem a prática como médico na especialidade por um período equivalente a três anos em tempo integral naquele Estado-Membro.

Um Conselho Deliberativo (CAE) analisa a formação e a experiência de trabalho do requerente.

Pode propor uma medida de compensação:

- onde o treinamento é pelo menos um ano a menos que o da ED francesa, quando abrange assuntos substancialmente diferentes, ou quando um ou mais componentes da atividade profissional cujo exercício está sujeito ao diploma acima mencionado não existem na profissão correspondente no Estado-Membro de origem ou não tenham sido ensinadas nesse estado;
- treinamento e experiência do candidato não são propensos a cobrir essas diferenças.

Dependendo do nível de qualificação exigido na França e do detido pela pessoa em causa, a autoridade competente pode:

- Oferecer ao candidato uma escolha entre um curso de ajuste ou um teste de aptidão;
- requerem um curso de ajuste ou um teste de aptidão.

O**teste de aptidão** destina-se a verificar, por meio de testes escritos ou orais ou exercícios práticos, a aptidão do candidato para exercer a prática como médico na especialidade pertinente. Trata-se de assuntos que não são cobertos pelo treinamento ou experiência do candidato.

O**curso de adaptação** destina-se a permitir que os interessados adquiram as habilidades necessárias para exercer a profissão de médico. É realizado a responsabilidade de um médico e pode ser acompanhado por treinamento teórico adicional opcional. A duração do estágio não excede três anos. Pode ser feito em part-time.

*Para ir mais longe* Artigos L. 4111-2 II, L. 4131-1-1, R. 4111-17 a R. 4111-20 e R. 4131-29 do Código De Saúde Pública.

**Para nacionais de um terceiro estado**

A autorização individual para a prática pode ser aplicada, desde que solicitem um nível suficiente de proficiência na língua francesa, pessoas com diploma de formação:

- emitido por um estado da UE ou da EEE cuja experiência seja atestada por qualquer meio;
- emitido por um terceiro estado que permite a prática da profissão de médico no país de graduação:- se eles cumprem testes anônimos para verificar conhecimentos básicos e práticos. Para obter mais informações sobre esses eventos, visite o site oficial do Centro Nacional de Gestão (NMC),
  + se eles justificam três anos de funções em um departamento ou organização credenciada para a formação de estagiários.

**Note que**

Os médicos com diploma especializado obtido como parte do estágio estrangeiro são considerados como tendo cumprido os testes de verificação de conhecimento.

*Para ir mais longe* Artigos L. 4111-2 (I e I bis), D. 4111-1, D.4111-6 e R. 4111-16-2 do Código de Saúde Pública.

**Autoridade competente**

A solicitação é endereçada em duas cópias, mediante ofício recomendado com solicitação de aviso de recebimento à unidade responsável pelas comissões de autorização de exercício (CAE) do NMC.

A autorização para exercer é emitida pelo Ministro responsável pela saúde após notificação do Cedeao.

*Para ir mais longe* Os artigos R. 4111-14 e R. 4131-29 do Código De Saúde Pública; decreto de 25 de fevereiro de 2010 que estabelece a composição do arquivo a ser fornecido aos CAEs competentes para o exame dos pedidos submetidos ao exercício na França das profissões de médico, cirurgião dentista, parteira e farmacêutico.

**Tempo**

O NMC reconhece o recebimento da solicitação dentro de um mês após o recebimento.

O silêncio mantido por um determinado período de tempo a partir do recebimento do arquivo completo vale a decisão de indeferir o pedido. Este prazo é:

- quatro meses para pedidos de cidadãos da UE ou do EEE com diploma de um desses estados;
- seis meses para pedidos de cidadãos terceirizados com diploma de um estado da UE ou eEE;
- um ano para outras aplicações. Esse prazo pode ser prorrogado por dois meses, por decisão da autoridade ministerial notificada no máximo um mês antes do término deste último, em caso de grave dificuldade na avaliação da experiência profissional do candidato.

*Para ir mais longe* Os artigos R. 4111-2, R. 4111-14 e R. 4131-29 do Código de Saúde Pública.

**Documentos de suporte**

O arquivo de aplicação deve conter:

- um formulário de solicitação de autorização para exercer a profissão, modelado no Cronograma 1 da Ordem de 25 de Fevereiro de 2010, preenchido, datado e assinado, mostrando, se houver, a especialidade em que o requerente arquiva;
- Uma fotocópia de um ID válido
- Cópia do título de formação que permite a prática da profissão no estado de obtenção, bem como, se necessário, uma cópia do título de formação de especialista;
- Se necessário, uma cópia dos diplomas adicionais;
- qualquer evidência útil que justifique treinamento contínuo, experiência e habilidades adquiridas durante o exercício profissional em um estado da UE ou EEE, ou em um terceiro estado (certificados de funções, relatório de atividades, avaliação operacional, etc. ) ;
- no contexto das funções desempenhadas em outro estado que não a França, uma declaração da autoridade competente daquele Estado, com menos de um ano de idade, atestando a ausência de sanções contra o requerente.

Dependendo da situação do requerente, é necessária uma documentação adicional de apoio. Para obter mais informações, visite o site oficial da NMC.

**O que saber**

Os documentos de apoio devem ser escritos em francês ou traduzidos por um tradutor certificado.

*Para ir mais longe* : decreto de 25 de fevereiro de 2010 que estabelece a composição do arquivo a ser fornecido às comissões de autorização competentes para o exame dos pedidos submetidos à prática na França das profissões de médico, cirurgião dentista, parteira e farmacêutico; 17 de novembro de 2014 no. DGOS/RH1/RH2/RH4/2014/318.

### d. Remédios

#### Centro de assistência francês

O Centro ENIC-NARIC é o centro francês de informações sobre o reconhecimento acadêmico e profissional de diplomas.

#### Solvit

O SOLVIT é um serviço prestado pela Administração Nacional de cada Estado-membro da UE ou parte do acordo EEE. Seu objetivo é encontrar uma solução para uma disputa entre um nacional da UE e a administração de outro desses Estados. A SOLVIT intervém em particular no reconhecimento das qualificações profissionais.

**Condições**

O interessado só pode usar o SOLVIT se estabelecer:

- que a administração pública de um Estado da UE não respeitou seus direitos o direito da UE como cidadão ou negócio de outro Estado da UE;
- que ainda não iniciou ação judicial (ação administrativa não é considerada como tal).

**Procedimento**

O nacional deve preencher um formulário de reclamação online. Uma vez que seu arquivo tenha sido apresentado, a SOLVIT entra em contato com ele dentro de uma semana para solicitar, se necessário, informações adicionais e verificar se o problema está dentro de sua competência.

**Documentos de suporte**

Para entrar no SOLVIT, o nacional deve comunicar:

- Detalhes completos de contato
- Descrição detalhada de seu problema
- todas as provas nos autos (por exemplo, correspondências e decisões recebidas da autoridade administrativa competente).

**Tempo**

A SOLVIT está empenhada em encontrar uma solução dentro de dez semanas a partir do dia em que o caso foi assumido pelo centro SOLVIT no país em que o problema ocorreu.

**Custo**

Livre.

**Resultado do procedimento**

Ao final do período de 10 semanas, a SOLVIT apresenta uma solução:

- Se essa solução resolver a disputa sobre a aplicação do direito europeu, a solução será aceita e o caso será encerrado;
- se não houver solução, o caso é encerrado como não resolvido e encaminhado à Comissão Europeia.

**Mais informações**

SOLVIT na França: Secretaria Geral para Assuntos Europeus, 68 rue de Bellechasse, 75700, Paris,([site oficial](http://www.sgae.gouv.fr/cms/sites/sgae/accueil.html)).

