﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP185" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Monitor de espeleología" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="monitor-de-espeleologia" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/monitor-de-espeleologia.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="monitor-de-espeleologia" -->
<!-- var(translation)="Auto" -->


Monitor de espeleología
=======================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
(1) Definición de la actividad
------------------------------

El instructor de espeleología es un profesional que supervisa y acompaña a cualquier tipo de público en la práctica de la espeleología. Enseña técnicas para superar obstáculos más o menos verticales a la exploración de este entorno.

La espeleología es una actividad multidisciplinar con un fuerte valor educativo, que combina aspectos científicos, medioambientales, deportivos y de ocio. Su objetivo es explorar entornos kársticos y subterráneos, naturales, artificiales o antropogénicos con el fin de contribuir activamente al estudio, conocimiento y conservación de los terrenos de práctica de la espeleología, teniendo en cuenta al mismo tiempo elementos del patrimonio superficial.

Esta disciplina requiere progresión y cruces que pueden implicar caminar en terrenos variados, reptation, natación, buceo submarino, resbalones, escalada y desescalamiento, descenso y ascenso en un fraccionado, o no, y otras técnicas de evolución en agrés (mano de mano, línea de vida, tirolina, básculas fijas) que pueden requerir la implementación de técnicas de seguro de todo tipo. Abrir ciertas cavidades y cruzar pasajes estrechos puede conducir a la implementación de técnicas de desobstrucción.

De acuerdo con técnicas específicas relacionadas con la diversidad de obstáculos, la disciplina requiere un equipo adecuado, incluyendo descensos, bloqueadores, arneses, lomos, cascos, picos, dispositivos de control de caídas, dispositivos de iluminación, ropa aislada, trajes independientes, cuerdas, cables, conectores, etc.

Para más información, es aconsejable visitar[el sitio web de la Federación Francesa de Espeleología](http://www.ffspeleo.fr/speleologie-16.html) (FFS).

2°. Cualificaciones profesionales
=========================================

### Requisitos nacionales

#### Legislación nacional

La actividad del monitor de espeleología está sujeta a la aplicación del artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas.

Como profesor de deportes, el instructor de espeleología debe poseer un diploma, un título profesional o un certificado de calificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

Los diplomas para el ejercicio de la actividad de instructor de espeleología son el Diploma Estatal de Juventud, Educación Popular y Deporte (DEJEPS) especialidad "desarrollo deportivo" mención "espeleología" y el diploma de estado superior de la Juventud, Educación Popular y Deporte (DESJEPS) especialidad "rendimiento deportivo" mención "caving".

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos L. 212-1, R. 212-84, A. 212-1 y Apéndice II-1 del Código del Deporte.

**Tenga en cuenta que**

La formación de los profesionales es impartida por el CREPS (Centro de Recursos de Experiencia y Rendimiento Deportivo) Ródano-Alpes. Para obtener más información, puede ver[Sitio web de FFS](http://www.ffspeleo.fr/devenir-professionnel-28.html).

**Es bueno saber**

El entorno específico. La práctica de la espeleología, independientemente del área de evolución, constituye una actividad realizada en un entorno específico. Implica el cumplimiento de medidas especiales de seguridad. Por lo tanto, sólo las organizaciones bajo la tutela del Ministerio del Deporte pueden formar a futuros profesionales.

*Para ir más allá*: Artículos L. 212-2 y R. 212-7 del Código del Deporte.

#### Entrenamiento

#### ESPECIALIDAD DEJEPS "desarrollo deportivo" mención "caving"

DEJEPS es un diploma estatal registrado en el Nivel III[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Da fe de la adquisición de una cualificación en el ejercicio de una actividad profesional de coordinación y supervisión con fines educativos en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Este diploma se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr) y el Artículo D. 212-40 del Código del Deporte.

*Para ir más allá*: Artículo D. 212-35 del Código del Deporte.

**Prerrogativas DEJEPS:**

- Diseñar programas de desarrollo deportivo
- Coordinar la implementación de un proyecto de desarrollo deportivo;
- llevar a cabo un enfoque seguro para el desarrollo deportivo en la espeleología;
- llevar a cabo acciones de formación que tienen en cuenta todos los requisitos de seguridad asociados con el medio ambiente;
- Diseñar y coordinar la implementación de programas de desarrollo de espeleología;
- organizar, animar y enseñar actividades y técnicas de espeleología en todas las cavidades, en todos los lugares de formación naturales y artificiales, para todos los públicos y con respeto al medio natural;
- la realización de una progresión en cuerdas y antlaentos en la espeleología en todas las cavidades, en todos los lugares de entrenamiento naturales y artificiales, para todos los públicos y con respeto al medio natural;
- iniciar proyectos de exploración, desarrollo y gestión de sitios de cuevas;
- contribuir a la experiencia técnica, educativa y ambiental en la espeleología;
- participar en la gestión sostenible de los sitios de práctica.

*Para ir más allá*: Artículo 2 del decreto del 29 de diciembre de 2011 por el que se crea la designación "caving" de la especialidad deJEPS "desarrollo deportivo".

Condiciones de acceso a la formación que conduce a DEJEPS:

- Ser mayores de edad
- Proporcione un formulario de registro con una fotografía
- Proporcione una fotocopia de un documento de identidad válido
- Si es necesario, presentar el certificado o certificados que justifiquen el aligeramiento de determinadas pruebas;
- Proporcionar un certificado médico de no contradictorio para la práctica deportiva de menos de un año de edad;
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Desarrollar pruebas de precertificación, si es necesario, de acuerdo con la certificación.
- Proporcionar copias del certificado censal y del certificado individual de participación en el Día de la Defensa y la Ciudadanía;
- Si es necesario, proporcione los documentos que justifiquen exenciones y equivalencias de la ley;
- para un certificado complementario, presentar una fotocopia del diploma que automese la inscripción en la formación o un certificado de inscripción para la formación que conduzca a dicho diploma;
- producir una lista de veinte razas en cavidades de clase cuatro, realizadas en cuatro macizos kársticos diferentes. Esta lista es atestiguada por el Director Técnico Nacional de Caving y es objeto de una entrevista de 30 a 45 minutos durante la cual el candidato debe demostrar su experiencia y práctica en la espeleología;
- proporcionar el certificado de éxito de la prueba técnica que consiste en una prueba técnica en un acantilado o estructura artificial y una prueba práctica de exploración en la cavidad. Este certificado es expedido por el Director Técnico Nacional de Caving;
- producir una lista de cuatro carreras de espeleología, con equipo y equipo de cavidad seguido de una entrevista de 20 minutos;
- establecer una sesión introductoria de seguridad de espeleología con un grupo que dure al menos 2 horas en una cavidad mínima de clase tres seguida de una entrevista sobre el análisis de esta sesión que dura de 20 a 30 minutos;
- realizar una exploración importante que implica una eliminación de la tripulación en los bloqueadores, utilizando el método de equilibrio, con una técnica de su elección en menos de 3 minutos y 30 segundos. Estas tres últimas condiciones le permiten comprobar los requisitos para el entorno pedagógico.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35 y A. 212-36 del Código del Deporte y ordenados el 29 de diciembre de 2011 creando el "caving" de la especialidad deJEPS "desarrollo deportivo".

**Es bueno saber**

Los titulares del "desarrollo deportivo" SPECIALty DEJEPS mencionan el "canyonismo" pueden supervisar las actividades de espeleología. Para este diploma, los instructores e instructores de canyonismo de la Federación Francesa de espeleología están exentos de la validación de los requisitos antes de entrar en la formación (prueba de entrada).

Para más información, es aconsejable consultar la hoja "monitor de barranquismo" y visitar el[Sitio web de FFS](http://www.ffspeleo.fr/devenir-professionnel-28.html).

**Curso de reciclaje**

La licencia para ejercer se limita a un plazo de seis años, renovable. Al final de este período, la renovación de la licencia para ejercer se concede a los titulares de la especialidad DEJEPS "desarrollo deportivo", mención "caving" que han tomado durante este último período, un curso de actualización.

*Para ir más allá*: Artículo 10 del decreto del 29 de diciembre de 2011 por el que se crea la etiqueta "caving" del "desarrollo deportivo" de LA SPECIALty DEJEPS.

##### DeSJEPS especialidad "rendimiento deportivo" mención "espeleología"

El DESJEPS es un diploma estatal superior inscrito en el Nivel II de la[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Atestigua la adquisición de una cualificación en el ejercicio de una actividad profesional de especialización técnica y gestión con fines educativos en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Este diploma se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr) y el Artículo D. 212-56 del Código del Deporte.

*Para ir más allá*: Artículo D. 212-51 del Código del Deporte.

**Prerrogativas DESJEPS**

La posesión del DESJEPS atestigua las habilidades relacionadas con la preparación, el pilotaje, la gestión y la evaluación:

- proyectos de exploración, desarrollo y gestión de sitios;
- experiencia técnica, pedagógica y ambiental en espeleología;
- Practicar proyectos de desarrollo
- grupos de coaching de todos los niveles, en todas las cavidades, sitios de práctica, todos los sitios de entrenamiento natural y artificial y todos los sitios de entrenamiento.

*Para ir más allá*: Artículo 3 del decreto del 15 de diciembre de 2006 por el que se crea la etiqueta "caving" de la especialidad "rendimiento deportivo" DESJEPS.

**Condiciones de acceso a la formación que conduzca a DESJEPS**

El interesado deberá:

- Ser mayores de edad
- Proporcione un formulario de registro con una fotografía
- Proporcione una fotocopia de un documento de identidad válido
- Si es necesario, presentar el certificado o certificados que justifiquen el aligeramiento de determinadas pruebas;
- Proporcionar un certificado médico de no contradictorio para la práctica deportiva de menos de un año de edad;
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Desarrollar pruebas de precertificación, si es necesario, de acuerdo con la certificación.
- Proporcionar copias del certificado censal y del certificado individual de participación en el Día de la Defensa y la Ciudadanía;
- Si es necesario, proporcione los documentos que justifiquen exenciones y equivalencias de la ley;
- para un certificado complementario, presentar una fotocopia del diploma que automese la inscripción en la formación o un certificado de inscripción para la formación que conduzca a dicho diploma;
- Ser capaz de evolucionar y evolucionar un grupo de forma segura en todas las cavidades y sitios de formación. Este requisito es verificado por una prueba técnica en un acantilado o estructura artificial y una entrevista sobre la experiencia de la cueva del candidato, su proyecto, su experiencia, su conocimiento de la técnica, la tecnología, medio ambiente, pedagogía, organización y gestión en relación con la espeleología;
- producir una lista de logros de cuatro carreras de espeleología, con equipo y desprecintado de la cavidad seguido de una entrevista sobre el análisis de esta duración que dura de 20 a 30 minutos;
- establecer una sesión educativa de seguridad en espeleología con un grupo que dure un mínimo de 2 horas en una cavidad mínima de clase 4 seguida de una entrevista sobre el análisis de esta sesión que dura de 20 a 30 minutos;
- realizar una exploración importante que implica una autorización de la tripulación en los bloqueadores, utilizando un método de equilibrio, con una técnica de su elección, en menos de 30 minutos y 30 segundos. Estas tres últimas condiciones nos permiten verificar el cumplimiento de los requisitos preeducativos.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35 y A. 212-36 del Código del Deporte; decreto de 15 de diciembre de 2006 por el que se crea la designación de "espeleología" de la SPECIALty DESJEPS "rendimiento deportivo".

**Curso de reciclaje**

La licencia para ejercer se limita a un plazo de seis años, renovable. Al final de este período, la renovación de la licencia para ejercer se concede a los titulares de la SPECIALty DESJEPS "rendimiento deportivo", mención "caving" que han tomado un curso de actualización durante este último período.

*Para ir más allá*: Artículo 7-1 del decreto del 15 de diciembre de 2006 por el que se crea la "espeleología" del DESJEPS "rendimiento deportivo".

#### Costos asociados con la calificación

La formación que conduce al DEJEPS cuesta unos 10.000 euros. Para el DESJEPS, el coste es de unos 3.900 euros.

Para más detalles, es aconsejable acercarse a la[Centro de Recursos, Experiencia y Rendimiento Deportivo (CREPS)](http://www.creps-rhonealpes.sports.gouv.fr/).

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como monitor de espeleología en Francia para las personas que han sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Obligación de presentación de informes (con el fin de obtener la tarjeta de educador deportivo profesional)

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial](https://eaps.sports.gouv.fr).

**hora**

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

**Documentos de apoyo**

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

**Autoridad competente**

El prefecto del Departamento de Isáre.

**hora**

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

**Documentos de apoyo**

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y siguientes, A. 212-215 y Apéndice II-12-3 del Código del Deporte.

### c. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

**Autoridad competente**

El prefecto del departamento de Isére.

**hora**

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

**Documentos de apoyo para la primera declaración de actividad**

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

**Evidencia para una declaración de renovación de la actividad**

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182, A. 212-215 y Listas II-12-2-a y II-12-b del Código del Deporte.

### d. Medidas de compensación

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

