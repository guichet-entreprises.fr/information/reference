﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP228" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Profesional de los contadores" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="profesional-de-los-contadores" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/profesional-de-los-contadores.html" -->
<!-- var(last-update)="2021-07" -->
<!-- var(url-name)="profesional-de-los-contadores" -->
<!-- var(translation)="Auto" -->


Profesional de los contadores
=============================

Actualización más reciente: : <!-- begin-var(last-update) -->Julio 2021<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El contador es un profesional cuya misión es revisar y apreciar las cuentas, dar fe de la regularidad y sinceridad de las cuentas de resultados, y también mantener, centralizar, abrir, detener, monitorear, enderezar y consolidar cuenta para empresas y organizaciones.

También se permiten actividades auxiliares directamente relacionadas con el trabajo contable a los profesionales contables (consultas, realizar todos los estudios y trabajos de carácter estadístico, económico, administrativo, legal, social o fiscal y avisar a cualquier autoridad u organismo público o privado que les autorice) pero sin poder ser el foco principal de su actividad y sólo si son empresas en las que se llevan a cabo misiones contabilidad de carácter permanente u habitual o en la medida en que tales consultas, estudios, obras u opiniones estén directamente relacionados con el trabajo contable del que son responsables.

*Para ir más allá*: Artículos 2 y 22 de la Ordenanza 45 2138, de 19 de septiembre de 1945, por la que se crea el Colegio de Contadores Públicos y se regula el título y la profesión de contador público.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de contable está reservada a cualquier persona en la Mesa de la Orden de Contadores, que justifica:

- disfrutando de sus derechos civiles;
- no haber sido condenado a una sentencia penal o correccional;
- que presenta las garantías de moralidad que el Consejo de la Orden considera necesarias;
- Alternativamente:- tienen un título en francés en contabilidad,
  - o tener cuarenta años de edad y justificar quince años de actividad en la ejecución de trabajos de organización o revisión contable, incluyendo al menos 5 años en funciones o misiones que impliquen el ejercicio de importantes responsabilidades de orden administrativo, financiero y contable
  - o, si es nacional de un Estado miembro de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) para justificar un certificado de competencia o un certificado de formación reconocido como equivalente y expedido en dicho Estado y, en su caso, tener con una prueba de aptitud (véase el punto b), o, si no es nacional, justificar un título reconocido equivalente al Diploma de Contabilidad (CED) y haber superado con éxito una prueba de aptitud.

*Para ir más allá*: Artículos 3, 7*Bis*, 26 y 27 de la Ordenanza 45-2138, de 19 de septiembre de 1945; Decreto No 2012-432 de 30 de marzo de 2012.

Cuando se trata del ejercicio de una actividad contable corporativa, los profesionales pueden constituir:

- Sociedades contables: entidades con personalidad jurídica, con excepción de las formas jurídicas que confieren a sus socios la condición de comerciante. Los contadores deben, directa o indirectamente, por una empresa inscrita en la Orden, poseer más de la mitad del capital y más de dos tercios de los derechos de voto. Ninguna persona o grupo de intereses, fuera de la Orden, tendrá, directamente o por persona interpuesta, una parte del capital o derechos de voto que puedan poner en peligro el ejercicio de la profesión, la independencia de los socios contables o su cumplimiento de las normas inherentes a su condición y ética. La oferta pública de valores financieros sólo está permitida para valores excluyendo el acceso, incluso diferido o condicional, al capital. Los gerentes, el presidente de la empresa simplificada, el presidente del consejo de administración o los miembros del consejo de administración deben contadores, miembros de la empresa. La sociedad que es miembro de la Orden comunica anualmente a los consejos de la Orden, desde el cual informa de la lista de sus asociados y de cualquier cambio realizado en esta lista;
- Sociedades de cartera contables: entidades cuyo objeto principal sea la tenencia de los valores de los contables colegiados, así como la participación en cualquier grupo de derecho extranjero que tenga como objeto el ejercicio de la profesión de contador colegiado. Estas empresas, inscritas en el tablero de pedidos, pueden tener actividades auxiliares directamente relacionadas con su objeto y destinadas exclusivamente a empresas o grupos en los que tengan participaciones.;
- asociaciones de gestión y contabilidad: entidades creadas por iniciativa de cámaras territoriales de comercio e industria, cámaras de oficios o cámaras de agricultura, u organizaciones profesionales de industriales, comerciantes, artesanos, agricultores o profesionales. Ninguna asociación puede ser registrada en la junta si tiene menos de trescientos miembros en el momento de la solicitud. Los directores y directores de estas asociaciones deben justificar el incumplimiento de sus obligaciones fiscales y sociales;
- Sucursales contables: nacionales de otros Estados miembros de la UE u otros Estados partes en el Acuerdo sobre el Espacio Económico Europeo, así como entidades jurídicas constituidas de conformidad con legislación de uno de estos Estados, que tiene su sede legal, sede o institución principal en uno de estos Estados, que ejercen legalmente la profesión de contabilidad, tiene derecho a constituir, para el ejercicio de ramas que no tienen personalidad jurídica. Las sucursales no son miembros del Colegio de Contadores Públicos. Están en la pizarra. Su trabajo está bajo la responsabilidad de un contador que interactúe dentro de la rama y representante ordinal específicamente designado en esta capacidad al consejo regional del Colegio de Contadores Públicos por los constituyen;
- sociedades profesionales de práctica: el objetivo de la práctica conjunta de varias de las profesiones de abogado, abogado en el Consejo de Estado y el Tribunal de Casación, subastador judicial, funcionario judicial, notario, administrador judicial, agente judicial, consultor de propiedad industrial, auditor y contable. La sociedad de práctica sindicación multiprofesional debe incluir al menos un miembro de cada una de las profesiones que ejerce. La empresa puede tomar cualquier forma social, excepto aquellos que dan a sus socios el estatus de un comerciante. Se rige por las normas específicas de la forma social elegida. Cualquiera que sea la forma de acción social elegida por la sociedad multiprofesional que ejerce, e incluso cuando no se ha incorporado como una empresa de ejercicio liberal, ciertas disposiciones del Título I de la Ley 90-1258 de 31 de diciembre de 1990 son Aplicable. Todo el capital y los derechos de voto están en manos de las siguientes personas:- 1) toda persona física que practique, dentro o fuera de la sociedad, una de las profesiones mencionadas en el artículo 31-3 de la Ley 90-1258, de 31 de diciembre de 1990, y que practique conjuntamente en la sociedad,
  - 2) toda persona jurídica cuyo capital total y derechos de voto sea directa o indirectamente propiedad de una o más de las personas mencionadas en el 1o,
  - 3) toda persona física o jurídica, establecida legalmente en otro Estado miembro de la Unión Europea o parte en el acuerdo sobre el Espacio Económico Europeo o en la Confederación Suiza, que realmente lleve a cabo, en uno de estos Estados, una actividad un estatuto legislativo o reglamentario o subordinado a la posesión de una cualificación nacional o internacional reconocida, cuyo ejercicio en Francia está comprendido en una de las profesiones mencionadas en el artículo 31-3 y que se ejerce conjuntamente en la sociedad ; para las personas jurídicas, todo el capital y los derechos de voto se mantienen en las condiciones previstas en el 1o o 2o;
- sociedades de cartera financiera de las profesiones liberales: constituidas entre particulares o corporaciones que ocestan una o más profesiones profesionales sujetas a estatuto legislativo o reglamentario o cuyo título está protegido o las personas 6o del artículo 6, del artículo 5, de la Ley 90-1258, de 31 de diciembre de 1990, sociedades de capital financiero para la participación de acciones o acciones de sociedades mencionadas en el artículo 1, párrafo primero, de la Ley 90-1258, de 31 Diciembre de 1990 con la finalidad del ejercicio de la misma profesión así como de la participación en cualquier grupo de Derecho extranjero con la finalidad del ejercicio de la misma profesión. Estas empresas podrán realizar cualquier otra actividad sujeta a la finalidad exclusiva de las empresas o grupos de los que posean intereses. Estas sociedades pueden constituirse en forma de sociedades de responsabilidad limitada, sociedades limitadas, acciones simplificadas o sociedades limitadas regidas por el Libro II del Código de Comercio. Más de la mitad del capital y el derecho de voto deben estar ocupados por personas de la misma profesión que las sociedades en poder de las acciones o acciones. Los gerentes, el presidente, los ejecutivos, el presidente del consejo de administración, los miembros del consejo de administración, el presidente del consejo de supervisión y los consejeros, así como al menos dos tercios de los miembros del consejo de administración o el consejo de supervisión de la corporación simplificada, debe ser elegido entre los que están autorizados a formar estas empresas.

*Para ir más allá*: Artículos 7, 7 *Ter*, 7 *D* 45-2138 de 19 de septiembre de 1945 y los artículos 31-1 a 31-2, y los artículos 31-3 y 31-3 de la Ley 90-1258, de 31 de diciembre de 1990, relativo al ejercicio en forma de sociedades profesionales sujetas a estatuto legislativo o reglamentario o de las cuales el título está protegido y a las sociedades de cartera financieras de las profesiones liberales.

#### Entrenamiento

Para solicitar al Colegio como contable, el candidato, que no utiliza los procedimientos mencionados en los artículos 7 *Bis*, 26 o 27 de la Ordenanza 45-2138, de 19 de septiembre de 1945, deben haber validado sucesivamente los siguientes diplomas, excepto las exenciones concedidas a los titulares de determinados títulos y diplomas:

- licenciado en contabilidad y gestión (DCG) (B.A.3);
- El grado de grado en contabilidad y gestión (DSCG) a nivel de máster (B.A. 5);
- el diploma de contabilidad (DEC) en el nivel de bac 8, después de una pasantía de tres años en una firma de contabilidad. Para este diploma, el candidato debe pasar una prueba escrita sobre la revisión legal y contractual de las cuentas, una prueba oral ante un jurado, y apoyar su tesis al final de su pasantía.

Estos diplomas también se pueden obtener a través del Procedimiento de Validación de Experiencia (VAE).

*Para ir más allá*: Artículo 3 de la Ordenanza 45-2138 de 19 de septiembre de 1945, Artículos 45 y siguientes del Decreto No 2012-432 de 30 de marzo de 2012.

#### Costos asociados con la calificación

Hay una tarifa fija por grado y por evento. En 2018, para el DCG, el precio por evento se fija en[20 euros](http://www.enseignementsup-recherche.gouv.fr/cid74649/www.enseignementsup-recherche.gouv.fr/cid74649/diplomes-comptables-superieurs-d.c.g.-d.s.c.g.-d.e.c.html) 14 eventos. En cuanto al DSCG, será[30 euros](http://www.enseignementsup-recherche.gouv.fr/cid74649/www.enseignementsup-recherche.gouv.fr/cid74649/diplomes-comptables-superieurs-d.c.g.-d.s.c.g.-d.e.c.html) 8 eventos. Además, se pagan los diversos cursos que se preparan para estos diplomas. Para obtener más información, es aconsejable acercarse a las organizaciones que las proporcionan.

El procedimiento de reconocimiento de las cualificaciones profesionales de los nacionales de la UE o del EEE es gratuito.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

La profesión de contable o parte de las actividades contables podrá ser realizada en Francia de forma temporal y ocasional por cualquier nacional de un Estado miembro de la UE o parte en el Acuerdo EEE, con sujeción a:

- 1) estar legalmente establecido, de forma permanente, en uno de los Estados mencionados para llevar a cabo la totalidad o parte de la actividad de contable;
- 2) cuando esta ocupación o la formación que la lleve a cabo no estén reguladas en el Estado de establecimiento, que haya ejercido más esta ocupación en uno o varios de los Estados mencionados durante al menos un año en los diez años anteriores a la rendimiento que pretende lograr en Francia.

La prestación de contabilidad se lleva a cabo bajo el título profesional del Estado de establecimiento cuando dicho título existe en ese Estado. Este título está indicado en la lengua oficial del Estado de Establecimiento. En los casos en que este título profesional no exista en el Estado del establecimiento, el demandante menciona su diploma o título de formación en la lengua oficial de ese Estado. La ejecución de este beneficio contable está sujeta a una declaración escrita al Consejo Superior del Colegio de Contadores Públicos antes del primer beneficio. La declaración escrita especifica la cobertura del seguro u otros medios de protección personal o colectiva con respecto a la responsabilidad profesional de este proveedor. La declaración prevista para la realización de la primera prestación en Francia de forma temporal y ocasional se repite en caso de cambio importante en los elementos de la declaración y se renueva cada año si el reclamante planea llevar a cabo esta actividad durante el año en cuestión. Tras la recepción de esta declaración, el Consejo Superior del Colegio de Contadores Públicos envía una copia al Consejo Regional del Colegio de Contadores Públicos en el que debe llevarse a cabo la provisión de conocimientos contables. Tras la recepción de esta transmisión, el consejo regional registra al solicitante de registro para el año considerado en el consejo de la Orden.

*Para ir más allá*: Artículo 26-1 de la Ordenanza 45-2138 de 19 de septiembre de 1945.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Cualquier nacional de un Estado miembro de la UE o parte en el acuerdo EEE que cumpla una de las dos condiciones podrá incluirse en el pedido como contable, sin tener un título en contabilidad:

- 1) poseer un certificado de competencia o cualificaciones de formación contemplados en el artículo 11 de la Directiva modificada 2005/36/CE del Parlamento Europeo y del Consejo, de 7 de septiembre de 2005, relativa al reconocimiento de cualificaciones profesionales que permitan el ejercicio de la profesión en un Estado miembro de la UE o parte en el Acuerdo EEE. Este certificado o título es expedido por la autoridad competente de ese Estado y sanciona la formación adquirida en la UE o en el EEE, ya sea por un tercer país, siempre que se proporcione un certificado de la autoridad competente del Estado miembro de la UE o sea parte en el acuerdo EEE que haya reconocido el título, certificado u otro título, que certifique que el titular tiene, en ese Estado, al menos tres años de trabajo a tiempo completo o de un período de tiempo parcial equivalente durante el diez Años. Los certificados de competencia o documentos de capacitación son emitidos por el estado de origen, designado de conformidad con las disposiciones legislativas, reglamentarias o administrativas de ese Estado;
- 2) haber trabajado a tiempo completo como contable durante un año o a tiempo parcial durante un período total equivalente al menos en los diez años anteriores en un Estado miembro de la UE o en una parte en el Acuerdo EEE que no regule la y que tengan uno o más certificados de competencia o credenciales de prueba de formación expedidas por otro estado que no regule esta profesión. Estos certificados de solvencia o credenciales de formación cumplen las siguientes condiciones:- (a) serán expedidos por la autoridad competente del Estado mencionado en el párrafo primero, designado de conformidad con las disposiciones legislativas, reglamentarias o administrativas de dicho Estado,
  - b) certificar la preparación del propietario para el ejercicio de la profesión de que se trate. Sin embargo, la condición de una experiencia laboral de un año mencionada no es necesaria cuando el título o los documentos de formación del solicitante sancionan la formación regulada directamente orientada al ejercicio de la profesión. Contabilidad.

A menos que los conocimientos, habilidades y habilidades adquiridos durante la experiencia laboral, ya sea a tiempo completo o a tiempo parcial, o aprendizaje permanente, y que haya sido validado por una autoridad competente para este fin en un Estado miembro de la UE o parte en el Acuerdo Europeo eee o en un tercer país designado de conformidad con las disposiciones legislativas, reglamentarias o administrativas de ese Estado, es probable que esta verificación sea innecesaria, el interesado debe ser someterse a una prueba de aptitud:

- 1) cuando la formación que justifique abarque temas sustancialmente diferentes de los del programa de diplomas contables franceses;
- 2) cuando el Estado en el que haya obtenido un certificado de competencia o un documento de formación contemplado en el artículo 11 de la CE 2005/36/modificado por el Parlamento Europeo y el Consejo el 7 de septiembre de 2005 relativo al reconocimiento de las cualificaciones profesionales que utiliza o del Estado en el que ha ejercido no regula esta profesión ni la regula de manera sustancialmente diferente de la normativa francesa.

*Para ir más allá*: Artículo 26 de la Ordenanza 45-2138 de 19 de septiembre de 1945 y artículos 97 a 99 y 103 del Decreto No 2012-432 de 30 de marzo de 2012.

En el momento de una solicitud en este sentido, ya sea para un establecimiento o para una prestación temporal y ocasional de servicios en Francia, la autoridad competente concede acceso parcial a las actividades de contabilidad cuando todas las condiciones se llenan:

- 1) el profesional está plenamente cualificado para llevar a cabo en un Estado miembro de la UE o parte en el Acuerdo EEE la actividad profesional para la que se solicita acceso parcial;
- 2) las diferencias entre la actividad profesional legalmente realizada en el Estado miembro de la UE o parte en el Acuerdo EEE y la profesión de contable en Francia son tan importantes que la aplicación de medidas de compensación equivaldría a imponer el solicitante de seguir el programa completo de educación y formación necesario en Francia para tener pleno acceso a la profesión en Francia;
- 3) el trabajo solicitado puede separarse objetivamente de otras actividades de la profesión contable en Francia, ya que puede llevarse a cabo de forma autónoma en el Estado miembro de origen.

El acceso parcial podrá denegarse si esta denegación está justificada por razones imperiosas de interés general, si es adecuada para garantizar la consecución del objetivo perseguido y si no va más allá de lo necesario para alcanzar este objetivo. 
La actividad profesional se lleva a cabo bajo el título profesional del Estado de origen cuando se ha concedido acceso parcial. Los profesionales que tienen acceso parcial indican claramente a los destinatarios de los servicios el alcance de sus actividades profesionales.

Las disposiciones de esto no se aplican a los profesionales que reciben el reconocimiento automático de sus cualificaciones profesionales de conformidad con los artículos 49*Bis* y 49*Ter* 2005/36/CE modificada por el Parlamento Europeo y el Consejo, de 7 de septiembre de 2005, relativa al reconocimiento de las cualificaciones profesionales.

Las solicitudes de acceso parcial a una institución se examinan con arreglo al mismo procedimiento que las solicitudes previstas en el artículo 26 de la Orden 45-2138, de 19 de septiembre de 1945.

Los profesionales que han sido autorizados a participar parcialmente en la actividad contable no son miembros del Colegio de Contadores Públicos. Se colocan en el consejo de la Orden de acuerdo con las condiciones establecidas en la Sección II de la Ordenanza 45-2138 de 19 de septiembre de 1945. Están sujetos a las disposiciones legislativas y reglamentarias relativas a la profesión de contador público. Pagan cuotas sobre la misma base y en las mismas condiciones que los miembros de la Orden.

*Para ir más allá*: Artículo 26-0 de la Ordenanza 45-2138 de 19 de septiembre de 1945 y artículos 97 a 99-1 y 103 del Decreto No 2012-432 de 30 de marzo de 2012.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Los profesionales de la contabilidad deben cumplir con las disposiciones legislativas y reglamentarias que rigen su profesión y los reglamentos internos del Colegio, que se establecen por decisión del Consejo Superior. En todos los casos, los profesionales de la contabilidad se responsabilpan de su trabajo y actividades. La responsabilidad propia de las sociedades miembros de la Orden y de las asociaciones de gestión y contabilidad deja la responsabilidad personal de cada contador o profesional que haya sido autorizado a realizar parcialmente la actividad experiencia contable debido al trabajo que realiza en nombre de estas empresas, sucursales o asociaciones. El trabajo y las actividades deben ir acompañados de la firma personal del contador, del empleado o profesional que haya sido autorizado a llevar a cabo parcialmente la actividad contable, así como el visado o la firma social. Los miembros de la la Orden, que, al ser socios o accionistas de una sociedad reconocida por ella, ejercen su actividad en dicha sociedad, así como los miembros de la Orden asalariados por un colega o sociedad que figure en el consejo de administración, de una sociedad multiprofesional de práctica, una sucursal o una asociación de gestión y contabilidad, y los profesionales que han sido autorizados a participar parcialmente en la actividad contable pueden llevar a cabo sus deberes o mandatos en su propio nombre y en su propio nombre. directamente encomendados por clientes o miembros. Ejercen este derecho en las condiciones de los convenios que finalmente los vinculan a esas empresas o a sus empleadores.

Los profesionales de la contabilidad están obligados por el secreto profesional en el ejercicio de sus funciones. Sin embargo, se desvinculan en los casos de información abierta contra ellos o de enjuiciamientos interpuestos contra ellos por las autoridades públicas o en acciones interpuestas ante las salas disciplinarias de la Orden.

También están sujetos a las normas de incompatibilidad. Como tal, no pueden:

- ser empleado, excepto para otro contador o una empresa que esté legalmente practicando contabilidad pública, un miembro de la Compañía Nacional de Auditores, una sucursal contable o una asociación de gestión y gestión. Contabilidad;
- Llevar a cabo otra actividad comercial o un acto de intermediación salvo que se realice de forma accesoria y no sea susceptible de comprometer el ejercicio de la profesión o la independencia de los contables socios así como el respeto por estos últimos de las normas inherentes a su estatuto y su ética. Las condiciones y límites al ejercicio de estas actividades para la realización de estos actos se establecen mediante norma profesional aprobada por decreto del 12 de marzo de 2021 del Ministerio de Economía;
- tienen el mandato de recibir, retener o emitir fondos o valores, o de renunciar. Sin embargo, como cómplice, contables, contables, contables, sucursales, asociaciones de gestión y contabilidad, empleados mencionados en los artículos 83 ter y 83 trimestre, y empresas multiprofesionales la cuenta bancaria del pedido podrá, a través de la cuenta bancaria de su cliente o miembro, proceder al cobro amistoso de sus deudas y al pago de sus deudas, para lo cual se les ha confiado un mandato, en las condiciones establecidas por decreto. La emisión de fondos puede efectuarse cuando corresponda al pago de deudas fiscales o sociales para las que se haya confiado un mandato al profesional;
- como agente de negocios, asumir una misión de representación ante los tribunales de la orden judicial o administrativa, realizar trabajos contables, contables o contables para las empresas en las que se directa o indirectamente tienen intereses sustanciales.

Por último, los profesionales de la contabilidad están sujetos a un código de ética. Este Código de ética contiene reglas relacionadas con los deberes generales, deberes con clientes o miembros, deberes de fraternidad y deberes para el Colegio.

*Para ir más allá*: Artículos 12, 21, 22 de la Ordenanza 45-2138 de 19 de septiembre de 1945, Artículos 141 y siguientes del Decreto No 2012-432 de 30 de marzo de 2012 y ordenado el 3 de mayo de 2012 relativo a la normativa interna del Colegio de Contadores Públicos.

4°. Legislación social y seguro
----------------------------------------------------

En lo que respecta a la legislación social, los profesionales de la contabilidad deben cumplir las disposiciones legales y reglamentarias aplicables en Francia en materia social en función del modo de práctica elegido. Además, deben contribuir al fondo de subsidios de vejez de contadores y auditores, incluso si están afiliados al sistema general de seguridad social.

*Para ir más allá* : Código de la Seguridad Social y artículo 27 *Bis* 45-2138 de 19 de septiembre de 1945.

En lo que respecta a la legislación en materia de seguros, los profesionales de la contabilidad están obligados, si se establecen en Francia, a justificar un contrato de seguro para garantizar la responsabilidad civil trabajo y actividades. El importe de las garantías de seguro consumidos no puede ser inferior, por asegurado, a 500.000 euros por siniestralidad y a un millón de euros al año de seguro. Las Partes podrán acordar disposiciones más favorables.

Cuando las consecuencias pecuniarias de la responsabilidad civil no están cubiertas por dicho contrato, están garantizadas por un contrato de seguro suscrito por el Consejo Superior de la Orden en beneficio de quien será propiedad. Los profesionales de la contabilidad participan en el pago de primas para este contrato.

*Para ir más allá*: Artículo 17 de la Ordenanza 45-2138 de 19 de septiembre de 1945 y artículos 134 y artículos del Decreto No 2012-432 de 30 de marzo de 2012.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Declaración Anticipada/Tarjeta Profesional

No se requiere ninguna declaración previa de los profesionales de la contabilidad con sede en Francia, con la excepción de los nacionales de la UE o del EEE que deseen utilizarla de forma gratuita y ocasional.

*Para ir más allá*: Artículos 3, 7, 7*Bis*, 7 *Ter*, 7 *D*, 26, 26-1 y 27 de la Ordenanza 45-2138 de 19 de septiembre de 1945 y los artículos 31-1 a 31-2, y los artículos 31-3 y siguientes de la Ley 90-1258, de 31 de diciembre de 1990.

### b. Autoridad competente

El Consejo Superior del Colegio de Contadores Públicos (CSOEC) y los consejos regionales del Colegio de Contadores Públicos constituyen las autoridades competentes para la inclusión de la Orden de Contadores Públicos y el reconocimiento de Calificaciones.

      Conseil supérieur de l’Ordre des experts-comptables

      Immeuble Le Jour

      200-2016, rue Raymond Losserand 75680 Paris cede

Site internet de l'Ordre des experts-comptables : [experts-comptables.fr](https://www.experts-comptables.fr/page-d-accueil)

En cuanto a la formación inicial, la CSOEC tiene en su sitio web páginas dedicadas a:

- [Currículo](https://www.experts-comptables.fr/devenir-expert-comptable/le-cursus) ;
- [pasantía](https://www.experts-comptables.fr/devenir-expert-comptable/le-stage).

En cuanto al reconocimiento de las cualificaciones profesionales, la CSOEC tiene en su sitio web una página[hoja informativa sobre el tema](https://www.experts-comptables.fr/devenir-expert-comptable/la-reconnaissance-des-qualifications).

Los pasos y más información se pueden obtener poniéndose en contacto con el departamento de formación de la CSOEC:

Correo electrónico:[communication@cs.experts-comptables.org](mailto:communication@cs.experts-comptables.org)

o Dominique Nechelis:[dnechelis@cs.experts-comptables.org](mailto:dnechelis@cs.experts-comptables.org)

o Sophie Parisot:[sparisot@cs.experts-comptables.org](mailto:sparisot@cs.experts-comptables.org)

Por último, un[formulario de contacto](https://www.experts-comptables.fr/devenir-expert-comptable/contact-formation) también está disponible en el sitio web del Consejo Superior del Colegio de Contadores Públicos.

### c. Tiempos de respuesta

Con respecto a la inscripción en el consejo de la Orden, se solicita la inscripción al Consejo Regional de la Orden en la circunscripción en la que se basa el candidato. El consejo regional debe pronunciarse en un plazo de tres meses. La decisión del consejo regional debe notificarse al candidato y al comisionado regional del Gobierno en un plazo de diez días libres. Podrá ser remitido al Comité Nacional del Cuadro, ya sea por el interesado en caso de denegación de registro o si no es por el Comisionado Regional del Gobierno.

En cuanto al registro de las asociaciones de gestión y contabilidad, una comisión nacional es responsable de decidir sobre la inclusión de las asociaciones de gestión y contabilidad en el consejo y de mantener una lista de estas asociaciones. La Comisión establece dentro del plazo y las condiciones de recurso para otros profesionales de la contabilidad.

*Para ir más allá*: Artículos 42 y 42 *Bis* 45-2138 de 19 de septiembre de 1945 y los artículos 106 y siguientes del Decreto No 2012-432 de 30 de marzo de 2012.

Con respecto al reconocimiento de cualificaciones profesionales, la CSOEC reconoce la recepción del expediente del solicitante en el plazo de un mes a partir de la recepción. El dictamen motivado sobre una solicitud de reconocimiento de cualificaciones profesionales debe enviarse al interesado en el plazo de tres meses a partir de la presentación de su expediente completo.

*Para ir más allá*: Artículos 98 y 99 del Decreto No 2012-432 de 30 de marzo de 2012.

### d. Documentos de apoyo

Por lo que respecta a la inscripción en el consejo de la Orden, la solicitud de inclusión en las secciones y listas del cuadro debe ir acompañada de documentos que justifiquen que la persona cumple las condiciones establecidas en la sección 3 de la ordenanza de 19 de septiembre de 1945.

*Para ir más allá*: Artículo 116 del Decreto No 2012-432 de 30 de marzo de 2012.

Cuando se trata del reconocimiento de cualificaciones profesionales, los documentos que se proporcionarán son:

- 1) documentos que establezcan su estado civil, nacionalidad y residencia;
- 2) documentos para verificar que cumplen los requisitos de las disposiciones del artículo 26 o del artículo 26, 1o o 2o, o los del artículo 26-0 del auto de 19 de septiembre de 1945, tales como copias de los certificados de competencia o evidencia de credenciales de formación que proporcionen acceso a la profesión de contable o permitan el ejercicio parcial de la actividad contable;
- 3) un documento o certificado de las autoridades del país nacional que certifique que el candidato cumple las condiciones morales establecidas en el artículo 3 y 2o de la ordenanza de 19 de septiembre de 1945;
- 4) un documento o certificado, si alguno de un banco o compañía de seguros de un Estado miembro de la UE u otro Estado parte en el acuerdo EEE, que establezca que el solicitante está asegurado contra los riesgos financieros asociados con el responsabilidad de conformidad con los requisitos del artículo 17 de la orden de 19 de septiembre de 1945. A tal fin, debe mencionarse la naturaleza de las prestaciones aseguradas y el importe anual de las garantías de seguro consumidos. Este importe debe estar relacionado con la obligación de seguro impuesta a los miembros de la Orden en virtud de los artículos 134 a 140 del decreto del 30 de marzo de 2012.

Los documentos presentados van acompañados, en su caso, de su traducción al francés por un traductor jurado o facultado para intervenir ante las autoridades judiciales y administrativas de un Estado miembro de la UE u otro Estado parte en el acuerdo. Eee. No se requieren originales.

*Para ir más allá*: Artículo 97 del Decreto No 2012-432 de 30 de marzo de 2012.

### e. Remedios

#### Nacional

Con respecto al registro en la lista de la Orden, una decisión de denegación podrá, en el plazo de un mes a partir de la notificación, ser remitida al Comité Nacional del cuadro, ya sea por el interesado en caso de denegación de registro, o de otro modo por el Comisionado Regional del Gobierno. El tribunal debe pronunciarse en un plazo de seis meses. Si la decisión no tuvo lugar al final de este período, la lista es legal. En caso de que el Comité Nacional de la mesa deba denegar la información en la junta, dicha decisión podrá ser recurrida ante el tribunal administrativo en las condiciones del derecho común.

*Para ir más allá* : Código de Justicia Administrativa, Artículos 43 y 44 de la Ordenanza 45-2138 de 19 de septiembre de 1945 y artículos 106 y artículos del Decreto No 2012-432 de 30 de marzo de 2012.

Por lo que respecta al reconocimiento de las cualificaciones profesionales, en caso de decisión de denegar el reconocimiento de cualificaciones profesionales, dicha decisión podrá ser apelada ante el tribunal administrativo del condiciones de derecho común.

*Para ir más allá* En: Código de Justicia Administrativa.

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

### f. Costos

La inscripción en la lista de la Orden de Contadores Públicos supone el pago de la cuota anual de membresía profesional. El reconocimiento de las cualificaciones profesionales de los nacionales de la UE o del EEE es gratuito.