﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP072" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Auditor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="auditor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/auditor.html" -->
<!-- var(last-update)="2020-04-15 17:21:01" -->
<!-- var(url-name)="auditor" -->
<!-- var(translation)="Auto" -->


Auditor
=======

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:01<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Auditor ("CAC") es un profesional cuya misión principal es certificar que las cuentas anuales son regulares y sinceras y dar una imagen precisa de los resultados de las operaciones del año pasado, así como la situación financiera y activos de la empresa al final de este año fiscal. Interviene en una entidad con la que fue nombrado voluntaria u obligatoriamente Durante su misión, se verá obligado a revelar al fiscal cualquier irregularidad de la que hubiera sido consciente.

El ACC también está obligado a informar sobre los principales acontecimientos en la vida de la empresa, como una ampliación de capital, una transformación o el pago de dividendos.

En principio, el ACC se designa por seis años. Sin embargo, las disposiciones limitan la posibilidad de renovar el mandato del CAC en algunos sectores e imponen una rotación de los signatarios (Pie de Comercio y filiales del PIE, entidades jurídicas privadas no comerciales con actividad económica y determinados umbrales, las asociaciones que reciben subvenciones que superan un umbral: véanse los artículos L. 822-14 y L.823-3-1 del Código de Comercio).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ejercer su profesión, el CAC debe ser incluido en la lista del Consejo Superior de la Oficina del Comisionado de Auditores ("H3C").

Para ello, deberá cumplir las siguientes condiciones:

- ser nacional francés o ser nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE) u otro Estado extranjero cuando admita a los nacionales franceses a ejercer el control legal de la Cuentas
- no haber sido condenado penalmente o sometido a sanciones disciplinarias que conduzcan a una supresión o por actos similares;
- no haber sido condenado por actos contrarios al honor o a la probidad;
- no haber sido objeto de quiebra personal ni de ninguna de las medidas de prohibición o decomiso del Libro VI del Código de Comercio.

*Para ir más allá*: Artículos L. 822-1 y L. 822-1-1 del Código de Comercio.

#### Entrenamiento

Para estar en la lista acc, el individuo tendrá que justificar:

- o poseer el Certificado de Capacidad para Actuar como Auditor (CAFCAC);
- o poseer el Diploma de Contabilidad (DEC);
- ya sea que haya superado una prueba de aptitud, para que el nacional aprobado por las autoridades de su Estado de la UE o el EEE ejerza el control jurídico de las cuentas o, aunque no se haya aprobado, cumpla las condiciones de título, diploma y formación práctica para obtener dicha acreditación de conformidad con lo dispuesto en la Directiva 2006/43/CE;
- o han aprobado una prueba de aptitud para el nacional de un Estado no perteneciente a la UE que admita a los nacionales franceses para ejercer el control legal de las cuentas, poseer un diploma o título del mismo nivel que la CAFCAC o el DEC y justificar una experiencia profesional de tres años en el campo de la auditoría jurídica;
- han realizado una pasantía profesional de un período establecido por el Reglamento con un auditor o una persona aprobada por un Estado miembro de la Unión Europea para ejercer el control jurídico de las cuentas.

Para asistir a la CAFCAC, tendrá que justificar ser titular:

- un máster y han superado las pruebas del certificado preparatorio para las funciones del CAC, cuyo programa se especifica en un[decreto del 5 de marzo de 2013](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027143986&categorieLien=id) ;
- es decir, un máster en contabilidad o finanzas, que valida al menos cuatro de las siete pruebas obligatorias para un título de posgrado en contabilidad y gestión (DSCG);
- DSCG.

Además, es imperativo que justifique la realización de un certificado de realización de una pasantía profesional, la duración de este último debe ser de tres años.

Para asistir al DEC, el interesado deberá justificar:

- Poseer el DSCG o Diploma de Contabilidad de Posgrado (DECS);
- han realizado una pasantía que en principio es de tres años, en una empresa de contabilidad o en estructuras similares.

*Para ir más allá*: Artículos 63 y siguientes. Decreto No 2012-432 de 30 de marzo de 2012.

El CAFCAC incluye una prueba de elegibilidad y una prueba de admisión, cuyos términos y condiciones están[Artículo A. 822-1](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=58CC1389C7F69724803027999EB5D10B.tplgfr33s_1?idArticle=LEGIARTI000027146372&cidTexte=LEGITEXT000005634379&dateTexte=20171207) Código de Comercio.

*Para ir más allá*: Artículos R. 822-2 y siguientes del Código de Comercio; decreto de 5 de marzo de 2013 por el que se establece el programa del certificado de idoneidad para las funciones de auditor y el certificado preparatorio de las funciones de auditor.

#### Costos asociados con la calificación

Se paga la formación de diplomas para llevar a cabo el papel del CAC. Para más información, es aconsejable consultar con las escuelas y universidades que los proporcionan.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

No existe ningún reglamento para un nacional de un Estado miembro de la Unión Europea (UE) o parte del Espacio Económico Europeo (EEE) que desee ejercer como CAC en Francia, de forma temporal y casual.

Por lo tanto, sólo las medidas adoptadas para el libre establecimiento de nacionales de la UE o del EEE (véase más adelante "5. Los pasos y procedimientos para el reconocimiento de la cualificación") encontrarán que se aplicarán.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de un Estado de la UE o del EEE que opere legalmente como CAC en ese Estado podrá establecerse en Francia para llevar a cabo la misma actividad de forma permanente.

Tendrá que solicitar su inscripción en la lista del CAC del Consejo Superior de la Oficina del Comisionado de Auditores, a cambio de haber superado previamente una prueba de aptitud (véase infra "5o. a. Solicitar el registro en la lista del CAC para los nacionales de la UE o del EEE para un ejercicio permanente (LE)).

*Para ir más allá*: Artículos L. 822-1-2 y R. 822-6 del Código de Comercio.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Reglas éticas

Se debe seguir un conjunto de reglas éticas, que incluyen:

- El secreto profesional de los datos que procesa;
- Integridad, incluida la justificación de no ser el autor de actos incriminatorios o disciplinarios;
- independencia e imparcialidad, a fin de evitar cualquier conflicto de intereses entre sus misiones de supervisión y asesoramiento;
- incompatibilidad de ejercicios con:- una actividad o acto que infrinja su independencia,
  - su trabajo como empleado,
  - una actividad empresarial,
  - y cualquier situación de auto-revisión.

*Para ir más allá*: Artículos L. 822-10 y siguientes, R. 822-20 y siguientes, y Apéndice 8-1 del Código de Comercio.

### b. Sanciones penales

Podrán aplicarse sanciones penales contra el CAC, que funcionaría sin estar en la lista del Consejo Superior de Auditores o que no respetaría las normas éticas antes mencionadas. En ambos casos, se enfrenta a una pena de un año de prisión y una multa de 15.000 euros.

También se prevén sanciones penales en las situaciones establecidas en los artículos L.820-6 y L.820-7 del Código de Comercio, especialmente cuando el profesional se abstiene de revelar los hechos delictivos que conoce durante su misión.

*Para ir más allá*: Artículos L. 820-5 a L.820-7 del Código de Comercio.

4°. Seguro
-------------------------------

### a. Obligación de someterse a formación profesional continua

Con el fin de mantener y mejorar sus conocimientos en las áreas de contabilidad, finanzas y derecho, los CAC deben completar 120 horas de formación profesional continua durante tres años consecutivos. Se completan al menos 20 horas en el mismo año (véase el artículo A. 822-28-2 del Código de Comercio). El H3C define las directrices generales y las diversas áreas en las que puede referirse el requisito de la educación continua. Garantiza el cumplimiento de las obligaciones de los auditores en este ámbito.

Cualquier persona en la lista que no haya servido como auditor durante tres años y que no haya cumplido con el requisito de educación continua durante este período está obligada a someterse a educación para aceptar una misión de certificación.

*Para ir más allá*: Artículos L. 822-4 y R. 822-22, y Artículos A. 822-28-1 y el siguiente del Código de Comercio.

### b. Obligación de contrato de seguro de responsabilidad civil profesional

El CAC Liberal tiene la obligación de comprar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

En cualquier caso, el ACC debe proporcionar una garantía financiera de más de 76.224,51 euros al año por reclamación.

*Para ir más allá*: Artículos L. 822-17, R. 822-36 y A. 822-31 del Código de Comercio.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar el registro en la lista del CAC para los nacionales de la UE o del EEE para un ejercicio permanente (LE)

#### El éxito en la prueba de aptitud

El nacional sólo podrá ser sometido a la prueba de aptitud después de hacer una solicitud por escrito al Ministro de Justicia. Esto debe ir acompañado de un archivo duplicado y los siguientes documentos justificativos:

- Una identificación válida
- cualquier diploma, certificado o título que acredite las cualificaciones profesionales del nacional;
- cualquier certificado de pasantía o formación para evaluar el contenido y el nivel de educación postsecundaria.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

Una vez recibido el expediente, el Ministro de Justicia emitirá un recibo al nacional y tendrá cuatro meses para decidir sobre la solicitud de la prueba de aptitud. El silencio guardado dentro de este tiempo valdrá la pena aceptarlo.

Una vez que se le permite realizar el examen, el nacional debe aprobar un examen escrito y oral en francés, con una puntuación media de 10 o más.

*Para ir más allá*: Artículos R. 822-6, A. 822-20 y los siguientes del Código de Comercio.

#### Inscripción en la lista de CAC

**Autoridad competente**

Una vez que el nacional haya superado la prueba de aptitud, tendrá que solicitar su inclusión final en la lista del CAC al Consejo Superior de la Oficina del Comisionado de Auditores.

**Documentos de apoyo**

La solicitud se puede hacer por correo postal o directamente en línea en el[Sitio web de la Compañía Nacional de Auditores](https://www.cncc.fr/liste-cac.html). Además de su solicitud motivada, el nacional deberá presentar todos los siguientes elementos justificadores:

- Una identificación válida
- Un CV
- Un certificado de éxito en la prueba de aptitud;
- un certificado de incompatibilidad con el ejercicio de la profesión;
- un certificado de honor que justifique que no está sujeto a quiebra personal o al autor de hechos que han dado lugar a una condena penal.

**Resultado del procedimiento**

Una vez recibidas las monedas, el Consejo Superior emitirá un recibo al nacional. Si, en el plazo de cuatro meses a partir de la recepción, la autoridad competente no ha decidido sobre la solicitud, se considerará que se concede el registro.

De este modo, el nacional podrá ejercer legalmente la función de CAC en Francia, después de haber sido juramentado ante el primer presidente del Tribunal de Apelación territorialmente competente.

*Para ir más allá*: Artículos L. 822-1, L. 822-1-1, L.822-1-2, R.822-9 y R. 822-12 del Código de Comercio.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

