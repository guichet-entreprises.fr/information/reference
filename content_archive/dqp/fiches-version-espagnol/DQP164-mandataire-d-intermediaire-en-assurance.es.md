﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP164" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Agente intermediario de seguros" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="agente-intermediario-de-seguros" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/agente-intermediario-de-seguros.html" -->
<!-- var(last-update)="2020-04-15 17:21:08" -->
<!-- var(url-name)="agente-intermediario-de-seguros" -->
<!-- var(translation)="Auto" -->


Agente intermediario de seguros
===============================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:08<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Agente Intermediario de Seguros (MIA) es un profesional que actúa bajo un mandato escrito con un intermediario, cuya actividad es la presentación, propuesta o asistencia en la conclusión de una transacción de seguro. Siempre que tenga un contrato de cobro, el MIA también puede cobrar primas de seguro.

Como agente, el MIA no puede:

- Nombrar a otra persona
- Gestionar contratos
- encargarse de la liquidación de reclamaciones.

*Para ir más allá*: Artículo L.511-1 del Código de Seguros.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Cualquier persona que desee ejercer la profesión de MIA debe justificar una capacidad profesional en el seguro de Nivel II, obligatorio para presentar productos de seguro.

**Tenga en cuenta que**

Cuando el profesional desee actuar como intermediario de seguros como auxiliar, tendrá que justificar únicamente una capacidad profesional en el seguro de nivel III.

Para obtener más información sobre los niveles de capacidad profesional en seguros, es aconsejable visitar el[Orias](https://www.orias.fr/web/guest/assurance2).

#### Entrenamiento

Para ejercer la profesión de MIA, el interesado debe justificar:

- o una pasantía profesional cuyo programa mínimo de formación se establece en el[decretado a partir del 11 de julio de 2008](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000019216662). No puede ser inferior a 150 horas y debe llevarse a cabo:- ya sea con una compañía de seguros o un intermediario,
  - ya sea en un centro de formación elegido por el empleador o el director;

**Tenga en cuenta que**

Al final de la pasantía, el profesional estará sujeto a un control de las habilidades adquiridas.

- o un año de experiencia como ejecutivo en una función relacionada con la producción o gestión de contratos de seguros o capitalización, dentro de una compañía de seguros o un intermediario de seguros;
- dos años de experiencia en una función relacionada con la producción o gestión de contratos de seguros o capitalización dentro de estas mismas empresas o intermediarios;
- ya sea la posesión de un diploma o un título inscrito en el Directorio Nacional de Certificaciones Profesionales y correspondiente simultáneamente:- nivel de formación III de la nomenclatura de formación utilizada por la Comisión Nacional de Certificación Profesional,
  - Especialidad formativa 313 relacionada con las actividades de finanzas, banca, seguros e inmuebles;
- o un certificado de cualificación profesional inscrito en el directorio nacional de certificaciones profesionales y correspondiente a la especialidad de formación 313 relativa a las actividades de hacienda, banca, seguros e inmuebles.

Cuando el profesional sea un intermediario de seguros como un incidente, tendrá que justificar:

- ya sea haber realizado una formación de duración razonable, adaptada a los productos y contratos que presentan o proponen, sancionados por la expedición de un certificado de formación;
- o tener seis meses de experiencia en una función relacionada con la producción o gestión de contratos de seguros o capitalización dentro de una compañía de seguros o intermediario de seguros;
- ya sea la posesión de un diploma o un título inscrito en el Directorio Nacional de Certificaciones Profesionales y correspondiente simultáneamente:- nivel de formación III de la nomenclatura de formación utilizada por la Comisión Nacional de Certificación Profesional,
  - Especialidad formativa 313 relacionada con las actividades de finanzas, banca, seguros e inmuebles;
- o un certificado de cualificación profesional inscrito en el Registro Nacional de Certificaciones Profesionales y correspondiente a la especialidad de formación 313 relativa a las actividades de hacienda, banca, seguros e inmuebles .

*Para ir más allá*: Artículos R. 512-10 a R. 512-12, y A. 512-7 del Código de Seguros.

#### Costo de la formación

El entrenamiento para convertirse en UN MIA vale la pena. Para obtener más información, es aconsejable acercarse a las instituciones que emiten los títulos o certificados anteriores o a un centro de formación.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Un nacional de un Estado de la Unión Europea (UE) o parte en el Espacio Económico Europeo (EEE), establecido y jurídicamente operativo MIA en uno de estos Estados, puede llevar a cabo la misma actividad en Francia de forma temporal y ocasional.

Debe simplemente informar previamente al Registro de su Estado, que velará por que la información se transmita al propio Registro Francés (véase infra "5o. a. Informar al Registro único del Estado de la UE o del EEE").

*Para ir más allá* : Directiva europea de intermediación de seguros de 9 de diciembre de 2002 conocida como "[DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) ».

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

El nacional de un Estado de la UE o del EEE, establecido y que lleve a cabo legalmente las actividades de MIA en uno de estos Estados, podrá llevar a cabo la misma actividad en Francia de forma permanente.

Debe simplemente informar previamente al Registro de su Estado, que velará por que la información se transmita al propio Registro Francés (véase infra "5o. a. Informar al Registro único del Estado de la UE o del EEE").

*Para ir más allá* : Directiva europea de intermediación de seguros de 9 de diciembre de 2002 conocida como "[DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) ».

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El MIA está obligado a cumplir con las condiciones de honor que son responsabilidad de su profesión, de conformidad con las disposiciones de la[Artículo L. 322-2 del Código de Seguros](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073984&idArticle=LEGIARTI000006797493&dateTexte=&categorieLien=cid). En particular, no debe haber sido condenado por menos de diez años por delitos, blanqueo de dinero, corrupción o evasión fiscal, o la supresión de funciones oficiales públicas o ministeriales.

**Tenga en cuenta que**

Los Orias solicitan directamente un extracto del boletín 2 del historial penal del MIA para el ejercicio de la profesión.

*Para ir más allá*: Artículos L. 322-2, L. 512-4 y R. 514-1 del Código de Seguros.

4°. Requisito de inscripción y seguro
----------------------------------------------------------

### a. Solicitar inscripción en el registro de Orias

El interesado que desee ejercer en Francia en la profesión de MIA debe estar inscrito en el Registro único de intermediarios en seguros, banca y finanzas (Orias).

**Documentos de apoyo**

El registro está precedido por el[abrir una cuenta](https://www.orias.fr/espace-professionnel) Orias y el envío de un archivo que contiene todos los siguientes documentos justificativos:

- Una copia del documento de identidad de la persona o un extracto de KBis si está inscrito en el Registro de Comercio y Sociedades;
- un documento que justifique su capacidad profesional como se especifica en el párrafo "2." a. Entrenamiento":- un folleto de pasantías,
  - Un certificado de formación,
  - Un certificado de oficina,
  - Un diploma o certificado
- cualquier documento que acredite la existencia de una o más órdenes;
- En caso de cobro de fondos, un certificado de garantía financiera;
- liquidación de las tasas de registro.

**Renovación del registro**

El procedimiento de inscripción en orias debe renovarse cada año y con cualquier cambio en la situación profesional de la persona. En este último caso, el MIA tendrá que mantener a los Orias informados un mes antes del cambio o en el plazo de un mes a partir del evento de modificación.

La solicitud de renovación debe tener lugar antes del 31 de enero de cada año y debe incluir:

- Liquidación de las tasas de registro
- en caso de cobro, un certificado de garantía financiera que cubra el período comprendido entre el 1 de marzo del año N y el 28 de febrero del año No. 1.

**Qué saber**

Las plantillas de certificado están disponibles directamente en el[Orias](https://www.orias.fr/web/guest/en-savoir-plus-ias).

**Costo**

La cuota de inscripción se fija en 30 euros pagaderos directamente en línea en el sitio web de Orias. En caso de impago de estos gastos, orias envía una carta informando al interesado de que dispone de un plazo de treinta días a partir de la fecha de recepción del correo para abonar la suma. No liquidar esta cantidad:

- No se tendrá en cuenta la solicitud de primer registro en orias;
- el MIA será eliminado del registro cuando fuera una solicitud de renovación.

*Para ir más allá*: Artículo R. 512-5 del Código de Seguros.

### b. Obligación de contrato de seguro de responsabilidad civil profesional

Como profesional en una actividad liberal, el individuo está obligado a tomar un seguro de responsabilidad civil profesional.

Sin embargo, podrá beneficiarse de la póliza de seguro de responsabilidad civil profesional del intermediario de seguros por el que está obligado, si éste lo declara como asegurado adicional.

### c. Obligación de sacar un seguro de garantía financiera

Cualquier persona que practice la profesión de MIA, siempre y cuando cobre fondos, incluso de manera casual, está obligada a tomar un seguro de garantía financiera, asignado al reembolso de estos fondos a sus asegurados.

El importe mínimo de la garantía financiera debe ser de al menos 115.000 euros, y no podrá ser inferior al importe medio mensual de los fondos recaudados por el MIA, calculado sobre la base de los fondos recaudados en los últimos doce meses anteriores al mes de fecha de suscripción o renovación del compromiso de fianza.

*Para ir más allá*: Artículos L. 512-7, R. 512-15 y A. 512-5 del Código de Seguros.

### d. Sanciones penales

Toda persona interesada en cumplir una de las obligaciones previstas en los apartados 3 y 4 será sancionada con una pena de dos años de prisión o una multa de 6.000 euros, o dos penas acumulativas.

*Para ir más allá*: Artículo L. 514-1 del Código de Seguros.

5°. Procedimientos y formalidades de reconocimiento de cualificaciones
--------------------------------------------------------------------------------

### a. Informar al Registro único del Estado de la UE o del EEE

**Autoridad competente**

Un nacional de un Estado miembro de la UE o del EEE que haya participado en la actividad de MIA en ese Estado y que desee ejercer el autoservicio o el establecimiento en Francia debe informar en primer lugar al registro único de su Estado.

**Procedimiento**

El nacional deberá facilitar la siguiente información al Registro único de su estado:

- Su nombre, dirección y, en su caso, número de registro;
- El Estado miembro en el que desea operar en caso de LPS o establecerse en caso de LE;
- La categoría de intermediario de seguros a la que pertenece, a saber, la del MIA;
- de seguros, si es necesario.

**hora**

El Registro único del Estado de la UE o del EEE, al que el nacional ha notificado su intención de operar en Francia, dispone de un mes para facilitar a los Orias la información que le conceda.

El nacional podrá iniciar su actividad en el MIA en Francia en el plazo de un mes a partir del que el Registro único de su Estado le haya informado de la comunicación realizada a los Orias.

*Para ir más allá*: Artículos L. 515-1 y L. 515-2 del Código de Seguros, artículo 6 de la Directiva Europea "DIA1" de 9 de diciembre de 2002 y artículos 4 y 6 de la Directiva Europea sobre Intermediación de Seguros de 20 de enero de 2016["DIA2"](https://www.orias.fr/documents/10227/26917/2016-01-20_Directive%20europeenne%20sur%20la%20distribution%20assurances.pdf).

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

