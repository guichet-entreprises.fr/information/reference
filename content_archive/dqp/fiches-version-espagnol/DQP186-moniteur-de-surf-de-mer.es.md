﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP186" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Monitor de surf en el mar" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="monitor-de-surf-en-el-mar" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/monitor-de-surf-en-el-mar.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="monitor-de-surf-en-el-mar" -->
<!-- var(translation)="Auto" -->


Monitor de surf en el mar
=========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El instructor de surf marino supervisa y anima de forma independiente las actividades de surf (surf, shortboarding, longboarding, bodyboard, bodysurfing, rodillera, skimboard) para cualquier público. Garantiza la seguridad tanto de los profesionales como de terceros, así como el mantenimiento y mantenimiento de los equipos.

*Para ir más allá* : decreto de 9 de julio de 2002 por el que se establecen las actividades náuticas especializadas del certificado profesional de juventud, educación popular y deporte (BPJEPS), decreto de 27 de abril de 2007 por el que se crea la mención "surf" del Diploma Estatal de Juventud, Educación Popular y Deporte (DEJEPS), decreto de 27 de abril de 2007 por el que se crea la designación "surf" del Diploma Estatal de Juventud, Educación Popular y Deporte (DESJEPS) especialidad "rendimiento deportivo".

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La actividad del instructor de surf está sujeta a la aplicación del Artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas. Como profesor de deportes, un instructor de surf marino debe poseer un diploma, un título profesional o un certificado de calificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el [directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Las cualificaciones para los instructores de surf son BPJEPS, DEJEPS y DESJEPS.

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos L. 212-1 y R. 212-84 del Código del Deporte.

**Bueno saber: el entorno específico**

La práctica del surf marítimo, independientemente del área de evolución, constituye una actividad realizada en un entorno específico que implica el respeto de medidas de seguridad específicas. Por lo tanto, sólo las organizaciones bajo la tutela del Ministerio del Deporte pueden formar a futuros profesionales.

*Para ir más allá*: Artículos L. 212-2 y R. 212-7 del Código del Deporte.

#### Entrenamiento

##### Las "actividades acuáticas" de BPJEPS mencionan el surf monovalente

Las actividades náuticas de BPJEPS atestian la posesión de las competencias profesionales esenciales para el ejercicio del instructor de actividades náuticas. Es un grado de nivel 4 (como una licenciatura técnica, una licenciatura tradicional o un certificado de técnico).

Para practicar como instructor de surf, el candidato debe elegir la designación monovalente "surf" de la especialidad BPJEPS "actividades acuáticas".

La especialidad BPJEPS "actividades acuáticas" mención monovalente "surf" se prepara alternativamente por la formación inicial, el aprendizaje o la educación continua. En la formación inicial, la duración mínima en un centro de formación es de 600 horas.

Para obtener más información sobre la validación de la experiencia (VAE), puede visitar el[sitio web oficial](http://www.vae.gouv.fr/) Vae.

**Prerrogativas****

Las actividades acuáticas BPJEPS mención de surf permite supervisar las actividades de surf (shortboard, longboard, bodyboard, bodysurfing, kneeboard, skimboard) para cualquier público y en cualquier lugar de práctica de la actividad.

###### Condiciones de acceso a la formación que conducen al PJEPS

El interesado deberá:

- Ser mayores el día de la entrada en la formación;
- presentar un certificado médico de no contradictorio con la práctica y enseñanza de las actividades de surf, que se remonta a menos de tres meses a la fecha de entrada en la formación;
- Poseer la unidad de enseñanza de Nivel 1 de Prevención y Alivio Cívico (PSC1) o su equivalente;
- Poseer el Certificado de Capacitación Suplementaria de Primeros Auxilios con Equipo (AFCPSAM) o su equivalente;
- reportar un certificado de éxito a una prueba de 100 metros de estilo libre, inicio de buceo y recuperación de un objeto sumergido a una profundidad de 2 metros, emitido por una persona que posee el certificado estatal de educador deportivo opción "actividades de natación", un socorrista (o alguien con un título equivalente);
- presentar el certificado de éxito en un evento consistente en realizar una o más maniobras en una onda utilizando su altura y longitud funcional, realizadas en apoyo a la elección del candidato entre los siguientes: shortboard, longboard, Bodyboard. Las modalidades de este evento, evaluadas por la Dirección Técnica Nacional de la Federación Francesa de Surf, se definen en función de las condiciones del mar;
- producir la prueba de éxito en el evento que consiste en un curso de al menos 400 metros en el mar desde el borde, incluyendo uno o más cruces de barras bodysurf. Los términos de esta prueba son definidos por el jurado de acuerdo con las condiciones del mar. Se permite el uso de aletas y un traje, con la excepción de cualquier otro material;
- requisitos preeducativos:- Ser capaz de ofrecer a los profesionales situaciones que promuevan el aprendizaje de la seguridad activa,
  - saber cómo gestionar el área de práctica frente a las limitaciones de seguridad,
  - Ser capaz de responder adecuadamente para gestionar la seguridad del grupo,
  - identificar elementos meteorológicos y naturales para adaptar proyectos de animación,
  - Ser capaz de hacer cumplir las regulaciones relacionadas con la supervisión de las actividades náuticas y cumplir con la normativa relativa a los lugares de práctica,
  - identificar riesgos específicos relacionados con comportamientos y riesgos de práctica,
  - ser capaz de aplicar los principios de higiene y seguridad de los equipos y equipos, gestionar la seguridad de los profesionales e implementar el rescate y la asistencia en el entorno náutico.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá* : decreto de 9 de julio de 2002 por el que se establece la especialidad "actividades náuticas" del certificado profesional de juventud, educación popular y deporte.

##### El desarrollo deportivo especializado DeJEps menciona el surf

DEJEPS es un diploma certificado de Nivel 3 (Bac 2/3). Da fe de la adquisición de una cualificación en el ejercicio de una actividad profesional de coordinación y supervisión con fines educativos en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Este diploma se puede obtener en formación inicial tomando 700 horas de formación en un centro de formación y 500 horas en estructura alterna o a través del VAE. Para obtener más información, puede ver[sitio web oficial](http://www.vae.gouv.fr/) Vae.

**Tenga en cuenta que**

El titular de la mención DEJEPS de navegación se considera para supervisar una actividad que opera en un entorno específico. Como tal, está sujeto a las medidas específicas de seguridad a que se refiere el artículo L. 212-2 del Código del Deporte.

###### Condiciones de acceso a la formación que conducen al "surf" deJEPS

El interesado deberá:

- Rellene el formulario de registro estándar con un documento de identidad con fotografía
- Ser mayores el día de la entrada en la formación;
- certificar la capacitación en la unidad de capacitación "Equipo de Nivel 1 de Primeros Auxilios" (PSE1), o su equivalente, al día con la educación continua con la producción del certificado de reentrenamiento anual;
- comunicar un certificado médico de no contradictorio con la enseñanza y la práctica del surf, de menos de tres meses de edad;
- cumplir con los requisitos de la situación pedagógica: poder evaluar los riesgos objetivos asociados a la actividad del practicante, anticipar los riesgos potenciales para el practicante, dominar el comportamiento y las acciones a realizar En caso de incidente o accidente, evaluar los riesgos objetivos relacionados con el entorno de práctica y prevenir comportamientos de riesgo;
- comunicar el certificado, expedido por una persona con un diploma conferido el título de socorrista, de éxito a un curso de estilo libre de 100 metros con un inicio de inmersión y la recuperación de un objeto submarino a una profundidad de dos metros;
- presentar un certificado de éxito en un evento consistente en una actuación evaluada por la Dirección Técnica Nacional de la Federación Francesa de Surf de acuerdo con los criterios de sentencia vigente en competencia, realizada sobre un apoyo a la elección del candidato shortboard, longboard o bodyboard;
- producir un certificado de éxito en un mínimo de 400 metros de natación a nivel del mar desde el borde, incluyendo uno o más cruces de barras y una acción que salva vidas con una tabla. Los términos de esta ruta son definidos por el jurado de acuerdo con las condiciones del mar;
- presentar un certificado de éxito en una entrevista realizada a partir de un archivo entregado al jurado por el candidato al comienzo de la prueba. Este archivo traza una experiencia, como atestigua el director técnico nacional de surf, en coordinación de equipos, enseñanza, formación o formación.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable referirse a los artículos 4, 6 y siguientes del decreto de 27 de abril de 2007 por el que se crea la palabra "surf" de DEJEPS, especialidad de desarrollo deportivo.

##### EL rendimiento deportivo especializado DESJEPS menciona el surf

El DESJEPS es un título certificado en el nivel 2 (bac 3/4). Se prepara como una alternancia en la formación inicial, el aprendizaje o la educación continua a través del VAE.

###### Condiciones de acceso a la formación que conducen a la designación de "surf" DESJEPS

El interesado deberá:

- Rellene el formulario de registro estándar con un documento de identidad con fotografía
- Ser mayores el día de la entrada en la formación;
- certificar la formación en la unidad docente pSE1, o su equivalente, actualizada con la educación continua con la producción del certificado de readaptación anual;
- comunicar un certificado médico de no contradictorio con la enseñanza y la práctica del surf, de menos de tres meses de edad;
- cumplir con los requisitos de la situación pedagógica: poder evaluar los riesgos objetivos asociados a la actividad del practicante, anticipar los riesgos potenciales en los que incurre, dominar el comportamiento y las acciones a realizar en caso de incidente o accidente, evaluar los riesgos objetivos asociados con el entorno de práctica y prevenir comportamientos de riesgo;
- producir un certificado, expedido por una persona con un diploma que confiere el título de socorrista, de éxito en un curso de estilo libre de 100 metros con un inicio de inmersión y la recuperación de un objeto sumergido a una profundidad de 2 metros;
- presentar un certificado de éxito, expedido por el Director Técnico Nacional de la Federación Francesa de Surf, de una prueba oral basada en el análisis de un documento de vídeo. Esta prueba evalúa la capacidad del candidato para observar, analizar y diagnosticar para diseñar la formación de un surfista;
- pasar un evento consistente en una actuación evaluada por la Dirección Técnica Nacional de la Federación Francesa de Surf de acuerdo con las sentencias vigentes en competencia y llevada a cabo sobre el apoyo elegido por el candidato del shortboard, el longboard y el bodyboard;
- completar un evento que consiste en un recorrido marítimo mínimo de 400 metros desde el borde, incluyendo uno o más cruces de barras y una acción de rescate con una tabla. Los términos de esta prueba son definidos por el jurado de acuerdo con las condiciones del mar;
- pasar una entrevista de un archivo presentado por el candidato al jurado al comienzo de la prueba, relatando una experiencia en coordinación de equipos, enseñanza, capacitación o capacitación. La experiencia en cuestión es atestiguada por el director técnico nacional de surf.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable referirse a los artículos 4, 6 y siguientes del decreto de 27 de abril de 2007 por el que se crea la designación "surf" del DESJEPS DESJEPS.

#### Costos asociados con la calificación

Se pagan capacitaciones que conducen a la especialidad "actividades acuáticas" PJEPS, los deJEPS que mencionan el "surf" o el DESJEPS mencionando el "surf". Sus costos varían dependiendo de la organización de capacitación. Para [más detalles](http://foromes.calendrier.sports.gouv.fr/#/formation), es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como instructor de surf marino en Francia para personas que han sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Declaración Anticipada/Tarjeta Profesional

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el [sitio web oficial](https://eaps.sports.gouv.fr).

#### hora

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

#### Documentos de apoyo

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

### b. Nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

#### Autoridad competente

La declaración previa de actividad debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde el Departamento en el que declarante quiere realizar su actuación.

**hora**

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

#### Documentos de apoyo

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### c. Nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

#### hora

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

#### Documentos de apoyo para la primera declaración de actividad

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

#### Evidencia para una declaración de renovación de la actividad

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

### d. Medidas de compensación

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

