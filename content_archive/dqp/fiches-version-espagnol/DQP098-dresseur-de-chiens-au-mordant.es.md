﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP098" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Entrenador de perros con mordida" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="entrenador-de-perros-con-mordida" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/entrenador-de-perros-con-mordida.html" -->
<!-- var(last-update)="2020-04-15 17:20:53" -->
<!-- var(url-name)="entrenador-de-perros-con-mordida" -->
<!-- var(translation)="Auto" -->


Entrenador de perros con mordida
================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:53<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El entrenador de perros mordiendo es un profesional cuya actividad consiste en entrenar y entrenar perros para atacar en el curso de vigilancia, vigilancia o transporte de fondos.

*Para ir más allá*: Artículo L. 211-17 del Código Rural y Pesca Marina.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Sólo el profesional con un certificado de capacidad puede realizar la actividad de entrenador de perros en la mordida y obtener el equipo necesario para este entrenamiento.

*Para ir más allá*: Artículo L. 211-17 del Código Rural y Pesca Marina.

#### Entrenamiento

**Certificado de capacidad para entrenar perros con mordida**

Para obtener el Certificado de Capacidad, el profesional debe justificar:

- poseer uno de los siguientes diplomas, títulos o certificados:- un diploma de instructor o instructor cynotechnician expedido por el Ministerio del Interior (policía nacional),
  - un certificado técnico cinótécnico (primer o segundo grado) o un certificado técnico superior (BTS) del Ejército Citotécnico expedido por el Ministerio de Defensa (Ejército),
  - un certificado de maestro de perros elemental o superior emitido por el Ministerio de Defensa (Fuerza Aérea),
  - un certificado técnico citotécnico (primer o segundo grado) expedido por el Ministerio de Defensa (Armada Nacional),
  - un certificado técnico cinótécnico de primer grado (módulo de entrenador jefe del grupo cyno) o un certificado técnico citotécnico de segundo grado (módulo de profundización) expedido por el Ministerio de Defensa (gendarmería),
  - un certificado de instructor del club autorizado para practicar disciplinas, incluyendo la mordedura emitida por la compañía canina central para la mejora de las razas caninas en Francia;
- poseer un certificado de conocimientos y habilidades del director regional de alimentación, agricultura y silvicultura en la región de Auvernia-Ródano-Alpes.

Una vez que la persona cumple con estas condiciones, debe solicitar al prefecto del departamento para obtener un certificado de capacidad (ver infra "5o. a. Solicitar un certificado de capacidad de entrenador de perros para morder").

*Para ir más allá*: Artículo R. 211-9 del Código Rural y Pesca Marina; Artículo 1 y Apéndice 1 de la Orden del 17 de julio de 2000 sobre el Certificado de Capacidad para la Formación de Perros en Mordedura: Prueba de Conocimientos y Habilidades Requeridas.

**Certificado de conocimientos y habilidades**

Para obtener esta certificación, el profesional debe someterse a una evaluación de sus conocimientos y habilidades.

Para ello, el interesado debe solicitar el registro de esta evaluación ante la institución pública local de educación agrícola y formación profesional de Combrailles - Saint-Gervais-d'Auvergne, que le enviará un expediente especificar los documentos justificativos necesarios, así como las condiciones de admisibilidad.

Esta evaluación consta de dos unidades que se ocupan de la capacidad del candidato para:

- Unidad 1: Realizar de forma segura las tareas asociadas con el ejercicio del entrenamiento canino para morder. Esta unidad se evalúa mediante una situación práctica;
- Unidad 2: movilizar el conocimiento legislativo, científico y técnico de la formación de perros mordedores para explicar las prácticas profesionales. Esta unidad se evalúa a través de una prueba oral.

El punto de referencia de los conocimientos y habilidades evaluados se establece en el[Apéndice 4](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=430B59CA50974DC3225518ECCE5946A1.tplgfr22s_2?idArticle=LEGIARTI000020755413&cidTexte=LEGITEXT000020751384&dateTexte=20180427) 17 de julio de 2000 supra.

El certificado es expedido por el Director de la Dirección Regional de Alimentación, Agricultura y Silvicultura (Draaf) al profesional que ha validado las dos unidades de ensayo.

**Tenga en cuenta que**

El candidato que ha validado sólo una unidad debe retomar la unidad faltante. Se puede hacer un llamamiento al Director de Agricultura y Alimentación Forestal de la región de Auvernia-Ródano-Alpes en un plazo de dos meses a partir de la fecha de notificación de los resultados.

*Para ir más allá*: Artículos 2 y siguientes del auto de 17 de julio de 2000 supra.

#### Costos asociados con la calificación

La tasa de evaluación es asumida por el solicitante y resulta en que el estado cobre una tarifa por los servicios prestados.

*Para ir más allá*: Artículo R. 211-10 del Código Rural y Pesca Marina.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido que realice la actividad de morder a un entrenador de perros podrá ejercer la misma práctica temporal y ocasional de forma ocasional. actividad en Francia.

Cuando el estado de origen del nacional no regule el acceso a la actividad o a su ejercicio, el nacional deberá justificar haber trabajado como adiestrador de perros durante al menos un año en los últimos diez años.

Una vez que la persona cumpla estas condiciones, el interesado deberá, antes de su primer servicio de entrega, hacer una declaración al director regional de alimentación, agricultura y silvicultura de la región de Auvernia-Ródano-Alpes (véase infra "5o). b. Predeclaración de los nacionales de la UE para un ejercicio temporal e informal (LPS)").

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones requeridas para el acceso a la profesión y a su práctica en Francia, el interesado podrá ser sometido, a su elección, a un examen o un curso de ajuste.

*Para ir más allá*: Artículos L. 211-17, párrafo 2, L. 204-1 y R. 211-9 del Código Rural y Pesca Marina.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si:

- posee un certificado de formación o un certificado de competencia expedido por una autoridad competente de otro Estado miembro que regula el acceso a la profesión o a su ejercicio;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Una vez que el profesional cumpla una de las dos condiciones anteriores, el director regional de alimentación, agricultura y silvicultura de la región de Auvernia-Ródano-Alpes le otorgará el certificado de conocimientos y habilidades.

Cuando ni el acceso a la actividad ni su ejercicio estén regulados en el Estado miembro de la UE o del EEE, la persona deberá justificar haber realizado esta actividad durante al menos un año en los últimos diez años.

En caso de diferencias sustanciales entre la formación recibida por el nacional y la requerida en Francia para llevar a cabo esta actividad, el director de los Draaf d'Auvergne-Ródano-Alpes podrá decidir someterlo a una medida de compensación (véase más adelante "5o. a. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos R. 211-9 y R. 204-2 del Código Rural y Pesca Marina.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Nadie puede obtener artículos o equipos gratuitos o caros para entrenar perros en mordedura sin tener el certificado de capacidad.

Al adquirir estos objetos o materiales, el profesional está obligado a presentar su certificado al vendedor. La venta está registrada en un registro específico conservado por el vendedor y puesta a disposición de la policía o las autoridades administrativas en caso de control.

**Tenga en cuenta que**

Estas obligaciones no se aplican a la policía, la gendarmería, las fuerzas armadas, las aduanas o los servicios públicos de emergencia que utilizan perros mordidos.*Para ir más allá*: Artículo L. 211-17 del Código Rural y Pesca Marina.

4°. Seguro
-------------------------------

El entrenador de perros con un bocado liberal debe tomar un seguro de responsabilidad civil profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar un certificado de capacidad de adiestramiento de perros para morder

#### Autoridad competente

El profesional debe solicitar al prefecto del departamento en el que esté domiciliado.

#### Documentos de apoyo

Su expediente de solicitud debe incluir los siguientes documentos:

- su nombre, fecha de nacimiento y domicilio;
- Un currículum para establecer su experiencia en el entrenamiento canino;
- El nombre y la dirección del establecimiento o club en el que opera el profesional;
- una declaración de honor que certifique la no condena por delitos relacionados con la protección y la salud de los animales;
- una de las pruebas para establecer los conocimientos y habilidades necesarios para otorgar el Certificado de Capacidad (véase supra "2". a. Certificado de capacidad para entrenar perros en mordedura");
- Cuando la actividad se lleve a cabo en el contexto de una empresa de supervisión, custodia o de transporte de dinero, una copia de la autorización de explotación de la sociedad de que se trate, prevista en el artículo 7 de la citada Ley de 12 de julio de 1983;
- Copia de la declaración de actividad de los establecimientos o clubes afectados;
- una copia del documento de identidad o cualquier otro documento reconocido equivalente para justificar la identidad y la residencia del solicitante.
Retrasos y procedimientos

El certificado es emitido por el prefecto del departamento previa notificación del director encargado de la protección de la población. El silencio guardado por el prefecto vale la pena rechazar la solicitud.

*Para ir más allá*: Artículo R. 211-9 del Código Rural y Pesca Marina.

#### Bueno saber: medidas de compensación

En caso de diferencias sustanciales entre la formación recibida por el nacional y la necesaria para llevar a cabo la actividad de entrenador de perros en Francia, el prefecto podrá decidir someter al nacional a una medida de compensación. Si es necesario, este último tendrá que realizar un curso de ajuste o una prueba de aptitud.

La prueba de aptitud debe completarse en el plazo de seis meses a partir de la decisión del prefecto y debe abarcar la totalidad o parte de la evaluación de conocimientos para obtener el Certificado de Capacidad (cf.[Apéndice 5](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=69322FA4B23BDCE8162539EAC1AF8C64.tplgfr22s_2?idArticle=LEGIARTI000020755415&cidTexte=LEGITEXT000020751384&dateTexte=20180430) 17 de julio de 2000).

El curso de adaptación, que dura hasta tres años, debe llevarse a cabo dentro de una empresa anfitriona. Este último firma un acuerdo con el candidato y el centro de evaluación que detalla los términos de esta pasantía.

*Para ir más allá*: Artículos R. 211-9, R. 204-3 y R. 204-5 del Código de Pesca Rural y Marina; Artículo 7 bis del Auto de 17 de julio de 2000 supra.

### b. Predeclaración del nacional de la UE para el ejercicio temporal y casual (LPS)

#### Autoridad competente

El nacional debe hacer una declaración previa al director regional de alimentación, agricultura y silvicultura en la región de Auvernia-Ródano-Alpes (Draaf).

#### Documentos de apoyo

Su solicitud debe incluir los siguientes documentos, si los hubiere, con su traducción al francés:

- Prueba de nacionalidad
- un certificado que certifica que está legalmente establecido dentro de un Estado miembro de la UE y realiza la actividad de morder a un adiestrador de perros;
- Un documento que certifique que no está sujeto a ninguna prohibición de fumar;
- prueba de sus cualificaciones profesionales
- cuando el Estado miembro no regule el acceso a la actividad o a su ejercicio, se demuestre por cualquier medio que el solicitante haya participado en esta actividad durante al menos un año en los últimos diez años.

#### Tiempo y procedimiento

En caso de diferencia sustancial entre las cualificaciones profesionales del nacional y las exigidas en Francia para llevar a cabo la actividad de adiestramiento de perros en mordedura, el director del Draaf d'Auvergne-Ródano-Alpes podrá decidir someterlo a un aptitud dentro de un mes de la decisión.

**Tenga en cuenta que**

En ausencia de una respuesta más allá de un mes, el solicitante puede comenzar a prestar servicios.

*Para ir más allá*: Artículo R. 204-1 del Código Rural y Pesca Marina.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

#### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

#### Procedimiento

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

#### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

#### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

#### Costo

Gratis.

#### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

#### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

