﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP168" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Manipulador de electroradiología médica" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="manipulador-de-electroradiologia" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/manipulador-de-electroradiologia-medica.html" -->
<!-- var(last-update)="2020-04-15 17:21:38" -->
<!-- var(url-name)="manipulador-de-electroradiologia-medica" -->
<!-- var(translation)="Auto" -->


Manipulador de electroradiología médica
=======================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:38<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Manipulador de Electroradiología Médica ("Manipulador ERM") es un profesional cuyas principales misiones son:

- realizar electrorradiología médica para diagnosticar;
- preparar la punción, el cateterismo, la inyección, la explosión y el equipo médico-quirúrgico;
- para realizar tratamientos con radiación ionizante o no ionizante.

El manipulador de ERM también participa en:

- Transmisión escrita de información sobre la realización de exámenes y tratamientos;
- Monitoreo clínico del paciente durante la misión;
- la ejecución de la atención requerida por el acto realizado.

**Qué saber**

El manipulador de ERM no es competente para interpretar las imágenes y dar a conocer al paciente un diagnóstico.

*Para ir más allá*: Artículos L. 4351-1, R. 4351-1 a R. 4351-3 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El acceso a la profesión de manipulador de ERM está restringido a los titulares de:

- Diploma estatal francés de manipulador de electrorradiología médica (DEMEM) expedido por el Ministerio de Asuntos Sociales y Salud;
- Diploma de Técnico Graduado en Imagen Médica y Radiología Terapéutica (DTS IMRT) emitido por el Ministerio de Educación Superior e Investigación.

*Para ir más allá*: Artículo L. 4351-3 del Código de Salud Pública.

#### Entrenamiento

El acceso a estos dos cursos se realiza directamente después del bachillerato: a modo de competición por el DE de manipulador de electrorradiología médica y por el estudio del expediente escolar con entrevista ante un jurado para el DTS IMRT.

Ambos cursos duran tres años y confieren el grado de licenciatura al estudiante.

Para DEMEM, la enseñanza se compone de dos partes:

- un teórico de 2.100 horas;
- la otra práctica de 2.100 horas.

Para más información, es aconsejable acercarse a los institutos de formación ([IFMEM](https://www.sup-admission.com/sante/paramedical/manip/radio-4/#tabs2)) que ofrecen la formación que conduce a DEMEM, y[Escuelas](https://www.sup-admission.com/sante/paramedical/manip/radio-4/#tabs3) que conduce al DEG IMRT.

*Para ir más allá* : orden de 14 de junio de 2012 relativa al diploma estatal de manipulador de electrorradiología médica.

#### Costos asociados con la calificación

La formación que conduce a uno de estos títulos está pagada. Para más información, es aconsejable consultar con las instituciones dispensadoras.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

El nacional de un Estado de la UE o del EEE, que actúe legalmente como manipulador del ERM en uno de estos Estados, podrá utilizar su título profesional en Francia, ya sea temporal u ocasionalmente.

Tendrá que solicitar, antes de su primera actuación, mediante declaración dirigida al prefecto de la región en la que desea realizar la entrega (véase infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá justificar haberla realizado en uno o varios Estados miembros durante al menos un año, en los diez años antes de la actuación.

*Para ir más allá*: Artículo L. 4351-8 del Código de Salud Pública.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Todo nacional de un Estado de la UE o del EEE, establecido y jurídicamente manipulador de ERM en ese Estado, podrá llevar a cabo la misma actividad en Francia de forma permanente si:

- posee un certificado de formación expedido por una autoridad competente de otro Estado miembro, que regula el acceso o el ejercicio de la profesión;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación o el ejercicio de la profesión;
- posee un diploma, título o certificado adquirido en un tercer Estado pero reconocido y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que el interesado haya estado practicando durante tres años la actividad del manipulador ERM en el Estado que ha equivalencia admitida.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar una autorización individual para ejercer desde el prefecto regional en el que desee ejercer su profesión (véase infra "5o). b. Solicitar un permiso de ejercicio para el nacional de la UE o del EEE para la actividad permanente (LE) ").

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, el prefecto podrá someterlo a medidas de compensación.

*Para ir más allá*Artículo L. 4351-4 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Reglas de buenas prácticas

La Sociedad Europea de Radioterapia y Oncología (ESTRO) ha redactado un Código de ética y conducta para guiar al manipulador del ERM en el ejercicio de su profesión.

En particular, el Código especifica que el manipulador de ERM debe:

- Garantizar la seguridad y la calidad de la radioterapia
- Respetar y proteger los mejores intereses del paciente
- Respetar la confidencialidad médica
- garantizar el consentimiento informado del paciente.

### b. Sanciones penales

La práctica ilegal de la profesión de manipulador ERM y el uso sin título de formación son delitos castigados con un año de prisión y una multa de 15.000 euros.

*Para ir más allá*: Artículos L. 4353-1 y L. 4353-2 del Código de Salud Pública.

4°. Seguro
-------------------------------

Como profesional de la salud, el manipulador liberal de ERM debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

**Autoridad competente**

El prefecto regional es responsable de decidir sobre la solicitud de una declaración previa de actividad.

**Documentos de apoyo**

La solicitud se realiza mediante la presentación de un archivo que incluye los siguientes documentos:

- Una copia de un documento de identidad válido
- Una copia del título de formación que permite ejercer la profesión en el estado de obtención;
- un certificado, de menos de tres meses de edad, de la autoridad competente del Estado de la UNIÓN o del EEE, que certifique que el interesado está legalmente establecido en ese Estado y que, cuando se expide el certificado, no existe ninguna prohibición, ni siquiera temporal, Ejercicio
- cualquier prueba que justifique que el nacional haya ejercido la profesión en un Estado de la UE o del EEE durante un año en los últimos diez años, cuando dicho Estado no regule la formación, el acceso a la profesión solicitada o su ejercicio;
- cuando el certificado de formación haya sido expedido por un tercer Estado y reconocido en un Estado de la UE o del EEE distinto de Francia:- reconocimiento del título de formación establecido por las autoridades estatales que han reconocido este título,
  - cualquier prueba que justifique que el nacional ha ejercido la profesión en ese estado durante tres años;
- Si es así, una copia de la declaración anterior, así como la primera declaración hecha;
- un certificado de responsabilidad civil profesional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**hora**

Una vez recibido el expediente, el prefecto regional dispondrá de un mes para decidir sobre la solicitud e informará al nacional:

- que puede comenzar la actuación. A partir de entonces, el prefecto registrará al solicitante en el directorio Adeli;
- que estará sujeto a una medida de compensación si existen diferencias sustanciales entre la formación o la experiencia profesional del nacional y las requeridas en Francia;
- no podrá iniciar la actuación;
- cualquier dificultad que pueda retrasar su decisión. En este último caso, el prefecto podrá tomar su decisión en el plazo de dos meses a partir de la resolución de esta dificultad, y a más tardar tres meses de notificación al nacional.

El silencio del prefecto dentro de estos plazos valdrá la pena aceptar la solicitud de declaración.

**Tenga en cuenta que**

La rentabilidad es renovable cada año o en cada cambio en la situación del solicitante.

*Para ir más allá*: Artículos R. 4351-25 y R. 4331-12 a R. 4331-15 del Código de Salud Pública.

### b. Solicitar un permiso de ejercicio para la UE o el EEE nacional para la actividad permanente (LE)

**Autoridad competente**

La autorización de ejercicio es expedida por el prefecto regional, previa areserva por la comisión de los manipuladores de electrorradiología médica.

**Documentos de apoyo**

La solicitud de autorización se realiza mediante la presentación de un expediente que contiene todos los siguientes documentos:

- el[formulario de solicitud de autorización para la práctica](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1F8B7371FFFC32398488D9490CC59F7F.tplgfr24s_3?idArticle=LEGIARTI000021777495&cidTexte=LEGITEXT000021777492&dateTexte=20180213) ;
- Una copia de un documento de identidad válido
- Una copia del título de formación
- Si es necesario, una copia de los diplomas adicionales;
- cualquier prueba que justifique la formación, la experiencia y las aptitudes adquiridas en la UE o en el Estado del EEE;
- una declaración de la autoridad competente del Estado de la UE o del EEE que justifique la ausencia de sanciones contra el nacional;
- Una copia de los certificados de las autoridades en la que se especifica el nivel de formación, el detalle y el volumen por hora de los cursos seguidos, así como el contenido y la duración de las prácticas validadas;
- cualquier documento que justifique que el nacional ha sido manipulador durante un año en los últimos diez años, en un Estado de la UE o del EEE, donde ni el acceso ni el ejercicio están regulados en ese Estado.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

El prefecto reconoce la recepción del expediente en el plazo de un mes y decidirá tras tener la opinión de la comisión de manipuladores ERM. Este último es responsable de examinar los conocimientos y habilidades del nacional adquiridos durante su formación o durante su experiencia profesional. Puede someter al nacional a una medida de compensación.

El silencio guardado por el prefecto de la región en un plazo de cuatro meses es una decisión de rechazar la solicitud de autorización.

**Bueno saber: la compensación se msures**

Si bien el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia laboral revela diferencias sustanciales con las cualificaciones necesarias para acceder a la profesión de manipulador ERM y su en Francia, el interesado tendrá que someterse a una medida de indemnización.

En función del nivel de cualificación exigido en Francia y del que posea el interesado, la autoridad competente podrá:

- Ofrecer al solicitante la opción de elegir entre un curso de ajuste o una prueba de aptitud;
- requieren un curso de ajuste y/o una prueba de aptitud.

*Para ir más allá*: Artículos R. 4351-22 a R. 4351-24 del Código de Salud Pública; decreto de 20 de enero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización pertinentes para el examen de las solicitudes presentadas para el ejercicio en Francia de las profesiones de asesor genético, enfermero, masajista-kinesiterapeuta, pedicura-podólogo, terapeuta ocupacional, manipulador en electroradiología médica y manipulador ERM.

### c. Registro en el directorio Adeli

Un nacional que desee ejercer como manipulador de ERM en Francia está obligado a registrar su autorización para ejercer en el directorio Adeli ("Automatización de las Listas").

**Autoridad competente**

El registro en el directorio de Adeli se realiza con la Agencia Regional de Salud (ARS) del lugar de práctica.

**hora**

La solicitud de inscripción se presenta en el plazo de un mes a partir de la toma del cargo del nacional, independientemente del modo de práctica (liberal, asalariado, mixto).

**Documentos de apoyo**

En apoyo de su solicitud de registro, el manipulador del ERM debe presentar un expediente que contenga:

- el título original que acredite la formación del manipulador del ERM emitido por el Estado de la UE o el EEE (traducido al francés por un traductor certificado, si procede);
- Id
- Formulario de ciervo 10906-06 completado, fechado y firmado.

**Resultado del procedimiento**

El número Adeli del nacional se mencionará directamente en la recepción del expediente, emitido por el ARS.

**Costo**

Gratis.

*Para ir más allá*: Artículo L. L4351-10 del Código de Salud Pública.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

