﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP201" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Logopeda" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="logopeda" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/logopeda.html" -->
<!-- var(last-update)="2020-04-15 17:21:52" -->
<!-- var(url-name)="logopeda" -->
<!-- var(translation)="Auto" -->


Logopeda
========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:52<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El logopeda es un profesional de la salud cuya actividad consiste en llevar a cabo un control del habla y el lenguaje del paciente, con el objetivo de prevenir y tratar a través de la rehabilitación todo el lenguaje, la comunicación y los trastornos del habla que puede Presente.

El profesional sólo puede hacer ejercicio con receta médica excepto en caso de emergencia y en ausencia de un médico.

**Tenga en cuenta que**

Un profesional que ejerce ilegalmente la profesión de patólogo del habla y el lenguaje se enfrenta a una pena de un año de prisión y una multa de 15.000 euros por títulos de usurpación.

*Para ir más allá*: Artículos L. 4341-1 y R. 4341-1 y siguientes del Código de Salud Pública, Artículo 433-17 del Código Penal.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad del logopeda, el profesional debe:

- Estar calificado profesionalmente
- registrar su diploma en la agencia regional de salud[Ars](https://www.ars.sante.fr/) ;
- para hacer una pasantía con un terapeuta del habla con licencia (ver infra "2 grados). Pasantía con un practicante").

*Para ir más allá*: Artículo L. 4341-2 del Código de Salud Pública.

#### Entrenamiento

Para ser reconocida como una persona profesionalmente calificada, la persona debe poseer una de las siguientes calificaciones, diplomas o certificados:

- Un certificado de capacidad de patólogo del habla y el lenguaje
- un diploma o certificado de estudios de logonior antes de la creación del certificado anterior.

**Certificado de habilidad del patólogo del habla y el lenguaje**

El acceso a la formación está disponible a través de la competencia en los centros de formación en terapia del habla. Para obtener más información, visite el sitio web oficial de las universidades que ofrecen formación en terapia del habla.

Pueden acceder a la formación para obtener el certificado de capacidad del lenguaje del habla, los candidatos titulares:

- Una licenciatura
- Licenciado en acceso a la educación universitaria
- Título francés o extranjero admitido como licenciatura.

Además, la formación también es accesible para el candidato a través del procedimiento de validación de experiencia (VAE). Para más información es aconsejable consultar el sitio web oficial de la[Vae](http://www.vae.gouv.fr/).

El candidato también puede beneficiarse de la certificación con dispensa siempre y cuando tenga:

- un certificado de aptitud para enseñar a los niños con pérdida auditiva, reconocido por el Ministro de Salud;
- diplomado como profesor especialista para niños sordos, reconocido por el Ministro de Educación;
- un reeducador de disléxicos, reconocido por uno u otro de los ministros anteriores.

El curso de diez semestres consta de dos ciclos de seis y cuatro semestres que permiten al futuro profesional adquirir los conocimientos necesarios para la práctica de la terapia del habla, a saber:

- durante el primer ciclo, todos los conocimientos generales relacionados con la ciencia, la salud pública y el enfoque humano y la patología y fisiopatología del paciente;
- durante el segundo ciclo, todo el conocimiento práctico del razonamiento clínico y la intervención terapéutica.

El certificado se expide al candidato que:

- completó con éxito todas las pruebas de los dos ciclos y llevó a cabo las prácticas previstas durante su formación;
- obtuvo el Certificado de Habilidades Clínicas emitido al final del último semestre de formación;
- apoyaron una presentación ante un jurado profesional en los últimos seis meses.

*Para ir más allá*: Artículo L. 4341-3 del Código de Salud Pública; Decreto No 2013-798, de 30 de agosto de 2013, relativo al régimen educativo del certificado de habla y lengua.

**Prácticas con un practicante**

El futuro profesional deberá, una vez que el certificado de patología del habla y el lenguaje, completar una pasantía con un profesional con licencia que haya estado practicando durante al menos tres años de manera liberal o en una institución de atención médica pública o privada.

*Para ir más allá*: Artículos D. 4341-6 a D. 4341-10 del Código de Salud Pública.

#### Costos asociados con la calificación

La capacitación para el Certificado de Capacidad de Idioma del Habla se paga y el costo varía dependiendo de la institución que emite la capacitación. Para más información es aconsejable acercarse al establecimiento en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido podrá realizar la misma actividad temporal y ocasional en Francia sin tener que registrarse en el ARS.

Para ello, la persona debe justificar la experiencia laboral como logopeda durante al menos un año en los últimos diez años, cuando ni la profesión ni la formación están reguladas en el Estado miembro en el que está legalmente establecido.

Tan pronto como cumpla estas condiciones, debe, antes de su primera actuación en Francia, hacer una declaración previa para ejercer su profesión en Francia (véase infra "5o. b. Solicitud de preinformación para el nacional de la UE para un ejercicio temporal e informal (LPS)").

**Tenga en cuenta que**

Cuando existan diferencias sustanciales entre la formación profesional y la experiencia del nacional y las necesarias para llevar a cabo la actividad de patología del habla y el lenguaje en Francia, el prefecto regional podrá decidir someterlo a un orden. adaptabilidad o un curso de ajuste (ver infra "5 grados). Bueno saber: medidas de compensación").

*Para ir más allá*: Artículo L. 4341-7 del Código de Salud Pública.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Todo patólogo nacional de la UE que prosiga el habla y el lenguaje de la UE en un Estado miembro podrá llevar a cabo la misma actividad de forma permanente en Francia.

Para ello, el interesado deberá ser el titular:

- un certificado de formación que le permita ejercer la actividad de patólogo del lenguaje del habla, expedido por un Estado miembro, siempre que dicho Estado regule el acceso a la profesión o a su ejercicio;
- cuando ni la profesión ni su formación estén reguladas en el Estado miembro, un documento de formación que acredite la preparación de la profesión de patólogo del habla y del lenguaje y un certificado que justifique que el profesional ha desempeñado esta función durante al menos un año en los últimos diez años;
- un certificado de formación expedido por un tercer Estado y reconocido en un Estado miembro de la UE distinto de Francia y que permite la práctica legal de la profesión de patólogo del habla y del lenguaje, así como un certificado que justifica que el profesional haya llevado a cabo esta actividad durante al menos tres años en ese Estado miembro o parte.

En cuanto al nacional que desee ejercer de forma temporal y ocasional, el prefecto puede decidir someterlo a una prueba de aptitud o a un curso de adaptación (véase infra "5o. Bueno saber: medidas de compensación").

Una vez que el profesional cumpla con estos requisitos, tendrá que solicitar el reconocimiento de sus cualificaciones profesionales (ver más abajo "5 grados). c. Solicitud de autorización para ejercer para el nacional europeo un ejercicio permanente (LE)).

*Para ir más allá*: Artículo L. 4341-4 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

**Reglas profesionales**

El logopeda debe ejercer su especialidad de forma independiente e independiente y respetar el secreto profesional que incumbe al ejercicio de su profesión.

*Para ir más allá*: Artículos L. 4341-1 y L. 4344-2 del Código de Salud Pública.

**Prohibiciones**

El profesional de la salud o estudiante destinado a tal actividad no puede recibir ningún beneficio en efectivo o en especie, directa o indirectamente de empresas que producen o comercializan productos apoyados por la seguridad Social.

Además, el profesional no puede cobrar los honorarios de un profesional de la salud si no cumple con todos los requisitos para una de estas ocupaciones.

Si se incumplen estas disposiciones, el profesional se enfrenta a una pena de dos años de prisión y una multa de 75.000 euros. En caso de condena, también puede imponerse una prohibición temporal de ejercer hasta diez años de forma complementaria.

*Para ir más allá*: Artículo L. 4113-5, L. 4113-6 y L. 4113-8 del Código de Salud Pública.

**Incompatibilidades**

Nadie puede trabajar como terapeuta del habla si:

- está en una condición médica que hace que sea peligroso ejercer su profesión;
- está sujeto a una prohibición temporal o permanente de la práctica en Francia o en el extranjero.

En caso necesario, el Director General del LRA podrá denegar el registro del profesional. No obstante, cuando la persona esté sujeta a tal prohibición en un tercer Estado de la UE o del EEE, el Director General de la ARS podrá autorizarle a ejercer.

*Para ir más allá*: Artículo L. 4343-3 del Código de Salud Pública.

4°. Fondos de seguros y garantías
------------------------------------------------------

### Seguro de responsabilidad civil

El terapeuta del habla como profesional está obligado a tomar un seguro para cubrirlo contra los riesgos incurridos durante su actividad.

**Tenga en cuenta que**

Si el ejercicio profesional como empleado, corresponde al empleador tomar dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

### Fondo de garantía

Además, el individuo está obligado a hacer una contribución anual a tanto alzado al fondo para garantizar los daños resultantes de la prevención, el diagnóstico o la atención por parte de los profesionales de la salud. El importe de esta contribución es fijado anualmente por los ministros responsables de la salud y la economía y varía entre 15 y 25 euros al año.

*Para ir más allá*: Artículos L. 1142-2 del Código de Salud Pública y L. 426-1 del Código de Seguros.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar el registro

**Autoridad competente y documentos justificativos**

El profesional debe proporcionar al ARS la siguiente información:

- su estado civil;
- El nombre y la dirección de la institución u organización que le haya expedido su diploma o título de formación;
- título de su título de entrenamiento.

Además, la organización que ha otorgado la designación de formación al profesional, la reenvía al ARS.

**Costo**

Gratis.

El ARS registra el título de formación del profesional y emite una tarjeta de salud profesional (CPS).

*Para ir más allá*: Artículos D. 4343-1 y D. 4333-1 a D. 4333-6-1 del Código de Salud Pública.

### b. Solicitud de informe previo del nacional de la UE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe solicitar al prefecto de la región en la que desee desempeñar su actuación.

**Documentos de apoyo**

La solicitud del nacional deberá incluir los siguientes documentos con, en su caso, su traducción al francés:

- El formulario de declaración adjunto al anexo de la Orden de 8 de diciembre de 2017 mencionado a continuación, cumplimentado y firmado;
- Una copia de su identificación válida
- Una copia de su título de formación que permite el ejercicio de la actividad patopatología del habla y el lenguaje dentro del país de obtención;
- un certificado de menos de tres meses de edad que certifique que el profesional está legalmente establecido en ese Estado miembro y no incurre en ninguna prohibición de hacerlo;
- cuando el Estado no regule el acceso a la profesión o a su ejercicio, un documento que justifique que el profesional ha estado involucrado en esta actividad durante al menos un año en los últimos diez años;
- cuando el certificado de formación haya sido expedido por un tercer Estado y reconocido por un Estado miembro distinto de Francia:- Reconocimiento del título de formación por parte del Estado miembro,
  - cualquier documento que justifique que el profesional ha estado activo durante al menos tres años en ese estado,
  - copia de la primera y anteriores declaraciones.

**Retrasos y resultados del procedimiento**

El prefecto informa al solicitante en el plazo de un mes a partir de la recepción de su solicitud:

- Que puede comenzar su prestación de servicio;
- que debe ser sometido a una auditoría de sus habilidades profesionales y, en caso de una diferencia sustancial entre su formación y la requerida para el ejercicio de esta actividad en Francia, que debe someterse a una medida de compensación (véase más adelante " 5°. c. Bueno saber: medidas de compensación");
- que no puede comenzar su prestación de servicios.

El silencio guardado por el prefecto más allá de un período de un mes vale la pena el permiso para realizar la prestación de servicios.

El prefecto registra al nacional en una lista en particular y le envía un recibo con su número de registro.

**Tenga en cuenta que**

La declaración debe renovarse cada año en las mismas condiciones. En caso de cambio de circunstancias, el nacional deberá informar al prefecto regional.

*Para ir más allá*: Artículos L. 4341-17, R. 4331-12 a R. 4331-15 del Código de Salud Pública; 8 de diciembre de 2017 orden sobre la declaración previa de prestación de servicios para asesores genéticos, médicos y preparadores de farmacias y farmacias hospitalarias, así como para ocupaciones en el Libro III de Parte IV del Código de Salud Pública.

### c. Solicitud de autorización para ejercer para el nacional europeo un ejercicio permanente (LE)

**Autoridad competente**

La solicitud deberá enviarse en doble copia por carta recomendada con notificación de recepción, a la Dirección Regional de Juventud, Deportes y Cohesión Social (DRJSCS), asegurando a la secretaría del Comité de Patólogos del Habla y Lengua cuya composición Sección R. 4341-17 del Código de Salud Pública.

**Documentos de apoyo**

La solicitud debe incluir lo siguiente, si lo hubiera, con su traducción al francés:

- el[Forma](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DDB2C307150C1EBD51D563B436CD57D5.tplgfr40s_3?idArticle=LEGIARTI000021936246&cidTexte=LEGITEXT000021936243&dateTexte=20180221) Solicitud completada y firmada;
- Una copia de su identificación válida
- Una copia de su título de formación que permite el ejercicio de la profesión en su país de obtención y, en su caso, sus diplomas adicionales, así como un certificado que menciona el nivel y el detalle de la formación;
- cualquier prueba de experiencia profesional, formación continua en un Estado miembro de la UE o en el EEE;
- cualquier documento que justifique que no está sujeto a ninguna sanción;
- cuando el Estado miembro no regule el acceso a la profesión o a su ejercicio, una prueba de que el profesional ha sido terapeuta del habla durante al menos un año en los últimos diez años;
- cuando el solicitante posea un certificado de formación expedido por un tercer Estado y reconocido por un Estado miembro, el reconocimiento del título de formación por el Estado miembro.

**Resultado del procedimiento**

El prefecto de la región del lugar de establecimiento del profesional reconoce la recepción del expediente en el plazo de un mes a partir de su recepción y emite la autorización de ejercicio previa notificación de la citada comisión. Esta autorización permite al profesional obtener una tarjeta profesional para llevar a cabo la actividad de logopeda en Francia en las mismas condiciones que el ciudadano francés.

**Tenga en cuenta que**

El silencio guardado por el prefecto después de cuatro meses desde la recepción del expediente completo vale la pena rechazar la solicitud.

**Costo**

Gratis.

*Para ir más allá*: Artículos R. 4341-13 a R. 4341-15 del Código de Salud Pública; decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización pertinentes para el examen de las solicitudes presentadas para la práctica en Francia de las profesiones de psicomotricista, logopeda, ortopedista, audiotésico y óptico-lunetier.

**Bueno saber: medidas de compensación**

El prefecto regional podrá, con el fin de verificar sus habilidades profesionales, someter al nacional a una prueba de aptitud o a un curso de adaptación.

La prueba de aptitud consiste en un examen que es objeto de preguntas escritas y orales sobre temas no enseñados al nacional. El curso de adaptación, por otro lado, debe ser practicado en una institución de salud pública o privada o en un profesional.

*Para ir más allá*: Artículo L. 4341-4 del Código de Salud Pública; decreto de 30 de marzo de 2010 por el que se establece la organización de la prueba de aptitud y el curso de adaptación para la práctica en Francia de las profesiones de psicomotricia, logopeda, ortopedista, audioprotésico, optician-lunetier por Estados miembros de la Unión Europea o partes en el Acuerdo sobre el Espacio Económico Europeo.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

