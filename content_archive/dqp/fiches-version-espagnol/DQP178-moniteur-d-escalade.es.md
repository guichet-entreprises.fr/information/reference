﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP178" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Instructor de escalada" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="instructor-de-escalada" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/instructor-de-escalada.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="instructor-de-escalada" -->
<!-- var(translation)="Auto" -->


Instructor de escalada
======================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El instructor de escalada enseña, anima y supervisa, de forma independiente, las actividades de escalada con todo el público. Dependiendo de su cualificación, puede llevar a cabo su actividad en:

- Estructuras de escalada artificial;
- Sitios naturales de escalada en rocas;
- Sitios de escalada deportiva natural a una altitud inferior a 1500 metros;
- los cursos, incluyendo los cursos acrobáticos en altura, y la vía ferrata situada a una altitud de menos de 1500 metros.

Proporciona entrenamiento para atletas, y participa en la gestión y mantenimiento de equipos y equipos específicos para las actividades de escalada.

*Para ir más allá* : decreto de 2 de mayo de 2006 por el que se crea el Certificado de Especialización (CS) "actividades de escalada" asociadas al certificado profesional de juventud, educación popular y deporte (BPJEPS); Decreto de 29 de diciembre de 2011 por el que se crea la mención "escalamiento" del Diploma Estatal de Juventud, Educación Popular y Deporte (DEJEPS) especialidad "desarrollo deportivo"; decreto de 31 de enero de 2012 por el que se crea la denominación deJEPS de especialidad "desarrollo deportivo" de "escalada en entornos naturales".

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La actividad del instructor de escalada está sujeta a la aplicación del artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas. Como profesor de deportes, el instructor de escalada debe poseer un diploma, un título profesional o un certificado de cualificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el [directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Las cualificaciones para las cualificaciones de instructor de escalada incluyen el Certificado de Cualificación Profesional (CQP) "facilitador de escalada de estructura artificial", la CS asociada con las "actividades de escalada" de LA PJEPS, la "escalada en entornos" de DEJEPS DEJEPS "escalada" y el diploma de estado profesional para jóvenes, educación popular y deporte (DESJEPS) "escalada".

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos L. 212-1 y R. 212-84 del Código del Deporte.

**Bueno saber: el entorno específico**

La práctica de la escalada en los sitios deportivos más allá del primer relevo, "cursos de aventura" y escalada "vía ferrata" constituye una actividad llevada a cabo en un entorno específico, que implica el cumplimiento de medidas especiales de seguridad. Por lo tanto, sólo las organizaciones bajo la tutela del Ministerio del Deporte pueden formar a futuros profesionales.

*Para ir más allá*: Artículos L. 212-2 y R. 212-7 del Código del Deporte.

#### Entrenamiento

##### CQP "facilitador de escalada en estructura artificial"

**Prerrogativas**

Este CQP permite la supervisión autónoma de las actividades de escalada en estructuras de escalada artificial, para todos los públicos, desde la iniciación hasta los primeros niveles de competición, hasta un límite de 360 horas al año.

Las estructuras de escalada artificial son equipos deportivos que consisten en una estructura de escalada construida específicamente con varias características de construcción, y diseñadas para una variedad de propósitos de escalada.

*Para ir más allá*: Apéndice I "Implementación de LOS CQPs" del Convenio Colectivo Nacional del Deporte de 7 de julio de 2005 prorrogado por orden de 21 de noviembre de 2006.

###### Condiciones de acceso a la formación que conducen al CQP

El interesado deberá:

- Tener 18 años el día de la entrada en formación;
- Poseer un certificado de primeros auxilios (PSC1) u otra cualificación equivalente;
- presentar un certificado médico de no contradictorio para la práctica de una escalada de menos de 3 meses en la fecha de presentación del expediente de registro;
- producir un certificado que justifique una experiencia de entrenamiento de escalada o co-entrenamiento de 50 horas, emitido por la persona a cargo de la estructura en la que se llevó a cabo el experimento;
- elaborar un certificado de éxito en las pruebas técnicas de entrada en formación expedidas por el director técnico nacional de la Federación Francesa de Montañismo y Escalada, consistente en escalar en la estructura de escalada artificial de las rutas enumeradas 6b en el plomo y bloques clasificados 5c y para garantizar su seguridad y la de su socio.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Apéndice I "Implementación de LOS CQPs" del Convenio Colectivo Nacional del Deporte de 7 de julio de 2005 prorrogado por orden de 21 de noviembre de 2006.

##### CS "actividades de escalada" asociadas con BPJEPS

Las "actividades de escalada" de la CS pueden estar asociadas con el PJEPS:

- especialidades "actividades físicas para todos";
- especialidades "actividades náuticas";
- especialidad "actividades de forma y fuerza".

**Prerrogativas**

Las "actividades de escalada" de la CS facultan a su titular para:

- autónomamente realizar servicios de descubrimiento, iniciación y animación en las disciplinas de escalada con cualquier público no técnico de relés de pared;
- la realización de ciclos introductorios y de aprendizaje en la escalada garantizando a los profesionales las condiciones óptimas de seguridad;
- participar en la gestión y mantenimiento de equipos y equipos específicos para las actividades de escalada.

El titular de la CS podrá estar obligado a llevar a cabo su actividad como trabajador en altura de los cursos acrobáticos en altura (HAP). Tiene en cuenta las especificidades del público (especialmente las personas con discapacidad), las adaptaciones que necesitan, las regulaciones, los controles y las relaciones específicas de este tipo de público.

Los sitios de práctica en los que opera son:

- Estructuras de escalada artificial;
- Sitios naturales de escalada en rocas;
- sitios de escalada deportiva limitados a Discovery Area 1 con una longitud de cuerda y un máximo de 35 metros de altura desde el suelo;
- cursos, incluidos los cursos acrobáticos en alturas, excluyendo la vía ferrata.

###### Condiciones de acceso a la formación que conducen a las "actividades de escalada" de la CS

El interesado deberá:

- celebrar una especialidad bpJEPS "actividades físicas para todos", "actividades acuáticas" o "actividades de gimnasio de forma y fuerza";
- pasar la prueba de selección organizada antes del entrenamiento que consiste en: cadena de al menos una ruta de escalada nominal 6a (de cabeza a vista) de tres propuestos, realizar una maniobra de enlace rápido en la parte superior de una de las pistas respetando la cadena de seguro, abseil en la parte superior de un carril y bajar autoasegurado, encadenar un paso de bloque en el lado 5a y garantizar la seguridad de un escalador líder.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá* : orden de 2 de mayo de 2006 por la que se establecen las "actividades de escalada" de la CS asociadas con el PJEPS.

##### DEJEPS mención "escalada"

DEJEPS es un diploma certificado de Nivel 3 (Bac 2/3). Por la especialidad "desarrollo deportivo", atestigua la adquisición de una cualificación en el ejercicio de una actividad profesional de coordinación y supervisión con fines educativos en los ámbitos de las actividades físicas y deportivas.

Este diploma se prepara alternativamente mediante la formación inicial, el aprendizaje o la educación continua. Se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr/).

Cuando se completa la formación como parte de la formación inicial, su duración mínima es de 1.200 horas, incluidas 700 horas en el centro de formación.

*Para ir más allá*: Artículo A. 212-49 del Código del Deporte.

**Prerrogativas**

La posesión de la DEJEPS menciona "escalada" atestigua las siguientes habilidades, en el campo de la escalada en bloques naturales y en los sitios deportivos naturales hasta el primer relevo situado a una altitud de menos de 1500 metros y la escalada en estructuras Artificial:

- Diseñar programas de desarrollo deportivo
- Coordinar la ejecución de un proyecto de desarrollo
- Llevar a cabo un enfoque de desarrollo deportivo
- realizar actividades de capacitación.

La mención DEJEPS "escalada" autoriza a su titular a enseñar, animar y supervisar la escalada, excluyendo:

- Sitios naturales a una altitud de 1500 metros o más;
- vía ferrata;
- escalando en lugares deportivos más allá del primer relevo y en canchas de aventura.

La obtención de la mención DEJEPS "escalamiento" autoriza al titular a ejercer durante seis años, esta autorización puede renovarse después de una pasantía de actualización.

*Para ir más allá*: Artículo 2 del Decreto de 29 de diciembre de 2011 por el que se crea la palabra "escalada" de DEJEPS; Apéndice II-1 del Código del Deporte.

**Condiciones de acceso a la formación que conduzcan a una escalada deJEPS**:

El interesado deberá:

- Proporcione al formulario de registro una fotografía de identidad y una fotocopia de un documento de identidad válido;
- Proporcionar un certificado médico de no contradictorio para la práctica deportiva de menos de un año de edad;
- producir copias del certificado censal y del certificado individual de participación en la jornada de defensa y ciudadanía;
- para producir el certificado "Nivel 1 de Prevención Cívica y Alivio" (PSC 1) o uno de los siguientes certificados válidos: "Certificado de Capacitación de Primeros Auxilios" (AFPS), "Nivel 1 Equipo de Primeros Auxilios" (PES 1), "Primeros Auxilios en Equipo de Nivel 2" (PES 2), "Certificado de Capacitación en Acciones de Emergencia y Cuidado" (AFGSU) nivel 1 o nivel 2, "Certificado de salvavidas de trabajo (TWU);
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Si es necesario, para desarrollar las pruebas pre-requeridas;
- producir una experiencia de entrenamiento de escalada de 150 horas en los últimos cinco años emitida por la persona a cargo de la estructura en la que se llevó a cabo el experimento;
- presentar un certificado de participación en tres concursos oficiales certificados por el director técnico nacional de montaña y escalada;
- presentar el certificado de éxito en la primera prueba técnica consistente en la realización de dos rutas de un nivel 6c y 7a para hombres, 6b y 6c para mujeres, expedido por el director técnico nacional de montaña y escalada;
- producir el certificado de éxito en la segunda prueba técnica consistente en la construcción de un bloque de nivel 6b para hombres y 6a para mujeres, emitido por el Director Técnico Nacional de Montaña y Escalada;
- cumplir con los requisitos prepedagógicos verificados por la introducción de una sesión de aprendizaje sobre los sitios de seguridad de la escalada natural, seguida de una entrevista de 30 minutos. Los requisitos son: poder evaluar los riesgos objetivos asociados con la práctica de la disciplina, anticipar los riesgos potenciales para el practicante, dominar el comportamiento y las acciones a realizar en caso de incidente o accidente, para implementar una sesión de aprendizaje de escalada en sitios naturales seguros;
- si es necesario, los documentos que justifican exenciones y equivalencias de la ley.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35, A. 212-36, A. 212-52-1 del Código del Deporte, orden de 29 de diciembre de 2011 por la que se crea la denominación dejeps especialidad "desarrollo deportivo".

##### DEJEPS menciona "escalada en ambientes naturales"

**Prerrogativas**

La mención SPECIALty DEJEPS "desarrollo deportivo" "escalada en entornos naturales" faculta a su titular para enseñar, animar, supervisar la escalada y capacitar a los profesionales:

- en todos los sitios naturales y en via ferrata situados a una altitud de menos de 1500 metros;
- estructuras artificiales.

Solo se excluyen los parajes naturales a una altitud de 1500 metros o más.

La graduación permite practicar por un período de seis años, renovado después de una pasantía de actualización.

*Para ir más allá*: Artículo 2 del Decreto de 31 de enero de 2012 por el que se crea el deJEPS "escalada natural"; Apéndice II-1 del Código del Deporte.

**Condiciones de acceso a la formación que conducen a la escalada de DEJEPS en entornos naturales**

El interesado deberá:

- Proporcione al formulario de registro una fotografía de identidad y una fotocopia de un documento de identidad válido;
- Proporcionar un certificado médico de no contradictorio para la práctica deportiva de menos de un año de edad;
- producir copias del certificado censal y del certificado individual de participación en la jornada de defensa y ciudadanía;
- para producir el certificado "Nivel 1 de Prevención Cívica y Alivio" (PSC 1) o uno de los siguientes certificados válidos: "Certificado de Capacitación de Primeros Auxilios" (AFPS), "Nivel 1 Equipo de Primeros Auxilios" (PES 1), "Primeros Auxilios en Equipo de Nivel 2" (PES 2), "Certificado de Capacitación en Acciones de Emergencia y Cuidado" (AFGSU) nivel 1 o nivel 2, "Certificado de salvavidas de trabajo (TWU);
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Si es necesario, para desarrollar las pruebas pre-requeridas;
- producir una experiencia de entrenamiento de escalada de 200 horas en los últimos cinco años, entregada por la persona a cargo de la estructura en la que se llevó a cabo el experimento;
- realizar una entrevista de hasta 30 minutos organizada por uno de los establecimientos públicos responsables de la formación de escalada, tramitando un expediente sobre la finalización autónoma de ocho carriles sobre el terreno en los últimos cinco años aventura de una altura mínima de 200 metros de un nivel de TD para hombres y TD para mujeres, ocho carriles con una altura mínima de 200 metros de nivel de ED para hombres y TD para mujeres y un carril mínimo de 400 metros de un nivel de TD para Hombres y mujeres;
- producir los certificados de éxito en las dos pruebas técnicas consistentes en la realización de dos vías de nivel 6c y 7a para los hombres, 6b y 6c para las mujeres y que consisten en la realización de un bloque de nivel 6b para los hombres y 6a para las mujeres;
- los requisitos prepedagógicos verificados por la introducción de una sesión de aprendizaje de seguridad primero de la cuerda, seguida de una entrevista de 45 minutos, incluyendo 15 minutos durante los cuales el candidato expresa la lista de sus logros;
- si es necesario, los documentos que justifican exenciones y equivalencias de la ley.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35, A. 212-36, A. 212-52-1 del Código del Deporte, orden de 31 de enero de 2012 por el que se crea la denominación dejeps especialidad "desarrollo deportivo".

##### DESJEPS mención "escalada"

El DESJEPS es un título certificado en el nivel 2 (bac 3/4). Por la especialidad "rendimiento deportivo", atestigua la adquisición de una cualificación en el ejercicio de una actividad profesional de especialización técnica y gestión con fines educativos en los ámbitos de las actividades físicas y deportivas.

Se prepara como una alternancia en la formación inicial, el aprendizaje o la educación continua. Se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr/).

*Para ir más allá*: Artículos D. 212-51 y los siguientes artículos del Código del Deporte.

**Prerrogativas**

La posesión del DESJEPS atestigua, en el campo de la escalada en bloques naturales y sitios deportivos naturales hasta el primer relevo situado a una altitud de menos de 1500 metros y en estructuras artificiales de las siguientes habilidades:

- Preparar un proyecto de rendimiento estratégico
- pilotando un proyecto de formación
- Ejecución de un proyecto deportivo
- Evaluar un sistema de formación
- organizar actividades de formación para formadores.

Este diploma autoriza al titular a enseñar, animar, supervisar la escalada y capacitar a los participantes. Sin embargo, no puede ejercer:

- naturales a una altitud de 1.500 metros o más;
- vía ferrata;
- escalando en lugares deportivos más allá del primer relevo y en canchas de aventura.

La obtención de la mención DESJEPS "escalamiento" autoriza al titular a ejercer durante seis años, esta autorización puede renovarse después de una pasantía de actualización.

*Para ir más allá*: Apéndice II-1 del Código del Deporte.

**Condiciones de acceso a la formación que conducen al CLIMBing DESJEPS**

El interesado deberá:

- Proporcione al formulario de registro una fotografía de identidad y una fotocopia de un documento de identidad válido;
- Proporcionar un certificado médico de no contradictorio para la práctica deportiva de menos de un año de edad;
- producir copias del certificado censal y del certificado individual de participación en la jornada de defensa y ciudadanía;
- para producir el certificado "Nivel 1 de Prevención Cívica y Alivio" (PSC 1) o uno de los siguientes certificados válidos: "Certificado de Capacitación de Primeros Auxilios" (AFPS), "Nivel 1 Equipo de Primeros Auxilios" (PES 1), "Primeros Auxilios en Equipo de Nivel 2" (PES 2), "Certificado de Capacitación en Acciones de Emergencia y Cuidado" (AFGSU) nivel 1 o nivel 2, "Certificado de salvavidas de trabajo (TWU);
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Si es necesario, para desarrollar las pruebas pre-requeridas;
- producir una experiencia de entrenamiento de escalada de 300 horas de uno o más atletas regionales en los últimos cinco años impartida por el Director Técnico Nacional de Montaña y Escalada;
- presentar el certificado de éxito de la prueba pedagógica consistente en realizar una sesión de desarrollo deportivo o entrenamiento de 30 minutos seguida de una entrevista de 20 minutos en las disciplinas de escalada, Director Técnico Nacional de Montaña y Escalada;
- para producir el certificado de éxito de la prueba, que consiste en analizar un documento de vídeo en una de las disciplinas de escalada para evaluar la capacidad del candidato para observar, analizar y diagnosticar con el fin de proponer una entrenamiento para un atleta o grupo de atletas regionales, impartido por el Director Técnico Nacional de Montaña y Escalada;
- presentar la prueba de éxito de la prueba consistente en el establecimiento de una sesión de capacitación a nivel interregional en una de las disciplinas de escalada a elección del candidato de un mínimo de 1 hora y un máximo de 2 horas seguida de un Entrevista de 20 minutos. Esta prueba permite comprobar los siguientes requisitos prepedagógicos: poder evaluar los riesgos objetivos asociados a la práctica de la disciplina y la actividad del practicante, dominar el comportamiento y los gestos realizar en caso de incidente o accidente e implementar una sesión de capacitación a nivel interregional en una de las disciplinas de escalada;
- presentar la prueba de éxito de la prueba consistente en el establecimiento de una sesión de capacitación a nivel interregional en una de las disciplinas de escalada a elección del candidato de un mínimo de 1 hora y un máximo de 2 horas seguida de un Entrevista de 20 minutos. Esta prueba le permite comprobar los requisitos prepedagógicos;
- si es necesario, los documentos que justifican exenciones y equivalencias de la ley.

Bajo ciertas condiciones, es posible obtener una exención para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35, A. 212-36 y A. 212-52-1 del Código del Deporte; decreto de 29 de diciembre de 2011 por el que se crea la designación de "escalada" del DESJEPS de SPECIALty.

#### Costos asociados con la calificación

Se paga la capacitación para obtener la CS asociada con los PJEPS, DEJEPS y DESJEPS. Sus costos varían dependiendo de la organización de capacitación. Para [más detalles](http://foromes.calendrier.sports.gouv.fr/#/formation), es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como instructor de escalada en Francia para personas que hayan sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Declaración Anticipada/Tarjeta Profesional

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial](https://eaps.sports.gouv.fr).

#### hora

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

#### Documentos de apoyo

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

#### b. Predeclaración de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

#### Autoridad competente

La declaración previa de actividad debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde el Departamento en el que declarante quiere realizar su actuación.

#### hora

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

#### Documentos de apoyo

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

#### c. Predeclaración de actividad de los nacionales de la UE para el ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

#### hora

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

#### Documentos de apoyo para la primera declaración de actividad

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

#### Evidencia para una declaración de renovación de la actividad

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, que data de menos de un año.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

### d. Medidas de compensación

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

