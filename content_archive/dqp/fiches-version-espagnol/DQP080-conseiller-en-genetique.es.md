﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP080" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Asesor de genética" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="asesor-de-genetica" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/asesor-de-genetica.html" -->
<!-- var(last-update)="2020-04-15 17:21:03" -->
<!-- var(url-name)="asesor-de-genetica" -->
<!-- var(translation)="Auto" -->


Asesor de genética
==================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:03<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El asesor genético es un profesional que, dentro de un equipo multidisciplinar y bajo la responsabilidad de un genetista, tiene la misión de:

- Evaluar el riesgo de transmisión
- informar a los pacientes y sus familias con o es probable que sean diagnosticados con enfermedades genéticas;
- médica y socialmente.

**Tenga en cuenta que**

Estos profesionales están presentes en todos los servicios de genética médica del hospital y operan en centros de salud públicos o privados.

*Para ir más allá*: Artículo L. 1132-1 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ejercer como asesor genético, la persona debe tener:

- un máster nacional (B.A. 5) en ciencias de la salud, mencionando la patología humana, especialidad de asesoramiento genético y medicina predictiva, publicado por la Universidad de Aix-Marseille II;
- autorización expedida por el prefecto regional (véase a continuación "Solicitud de Autorización para El Ejercicio").

*Para ir más allá*: Artículo L. 1132-2 y el siguiente del Código de Salud Pública.

#### Entrenamiento

**Consultoría de Diploma saque sano de Especialidad Nacional en Genética**

Este diploma está disponible para estudiantes de:

- ocupaciones paramédicas:- Enfermeras
  - Psicólogos
  - fisioterapeutas;
- profesiones científicas:- Ingenieros
  - Estudiantes de formación en biología en una licenciatura o nivel equivalente (B.A. 3);
- profesiones médicas:- médicos, farmacéuticos, dentistas,
  - Parteras
  - estudiantes que han completado el tercer año de medicina, farmacia o escuela de odontología.

La formación de dos años consta de módulos:

- adquirir:- Conocimiento general de la genética,
  - Los principios de la relación cuidador-paciente,
  - la ética de la profesión,
  - Psicología
- asesoramiento genético y patologías que abordan los aspectos genéticos y médicos de las enfermedades genéticas;
- para obtener el conocimiento adicional de la especialidad elegida.

Para más información, es aconsejable consultar el[Universidad aix-marseille II](https://medecine.univ-amu.fr/).

**Solicitud de permiso para ejercer**

Cuando el individuo no justifique la celebración del diploma mencionado anteriormente, pero haya ocupado cargos asignados al consejo de genética, podrá solicitar permiso para ejercer con el prefecto regional.

**Documentos de apoyo**

Para obtener esta autorización, el profesional debe enviar por correo un archivo duplicado que incluya:

- información de contacto completa (nombre, nombre, copia de identificación)
- Una carta de solicitud de autorización para ejercer como asesor genético;
- copiando sus diplomas.

**hora**

Una vez recibido su expediente por el prefecto regional, el solicitante recibirá un acuse de recibo y notificación que le autoriza rábase o no a ejercer la profesión.

Si no responde después de un retraso de dos meses, se denegará su solicitud.

*Para ir más allá* Decreto No 2007-1429, de 3 de octubre de 2007, relativo a la profesión de asesor genético y por el que se modifica el Código de Salud Pública (disposiciones reglamentarias).

**Solicitud de certificación para una prueba de ADN y toma de huellas dactilares**

El profesional, que desea realizar una revisión de las características genéticas de una persona o su identificación de ADN para fines médicos, debe solicitar la acreditación con[Agencia de Biomedicina](https://www.agence-biomedecine.fr/agrement-praticiens-genetique).

Esta acreditación tiene una validez de cinco años.

*Para ir más allá* Decreto 97-109, de 6 de febrero de 1997, sobre las condiciones de aprobación de las personas con derecho a realizar identificaciones de ADN en el marco de un procedimiento judicial o del procedimiento extrajudicial de identificación de personas Difuntos.

#### Costos asociados con la calificación

La formación que conduce al título de asesor genético se paga y el costo varía dependiendo del curso previsto. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE), establecido y practica legalmente como asesor genético, podrá ejercer en Francia un ocasional, la misma actividad.

Para ello, deberá enviar una declaración previa al prefecto de la región (véase infra "5o) antes de la prestación de los servicios. a. Solicitud de pre-informe para un ejercicio temporal e informal (LPS).

No obstante, cuando ni la actividad profesional ni la formación estén reguladas en la UE o en el Estado del EEE, el interesado podrá ejercer en Francia de forma temporal y ocasional si justifica haber realizado dicha actividad, durante el menos de un año en los últimos diez años antes del beneficio.

*Para ir más allá*: Artículos L. 1132-5 y R. 1132-4 del Código de Salud Pública.

### c. Nacionales de la UE: para un ejercicio permanente (Ejercicio libre)

Para llevar a cabo la actividad de asesor genético, de forma permanente en el territorio francés, el nacional de un Estado miembro de la UE o del EEE debe ser el titular:

- o un certificado de formación expedido por un Estado miembro que regule el acceso a la profesión o que la disque a ella y permita el acceso a estas funciones en ese Estado;
- cuando ni la actividad profesional ni la formación estén reguladas en la UE o en el Estado del EEE, un certificado de formación que justifique que la persona que está en formación y haya llevado a cabo esta actividad durante un año, a tiempo completo o a tiempo parcial En la última década
- una designación de formación para acceder a la profesión y la prueba de que la persona ha estado en el trabajo durante tres años a tiempo completo o a tiempo parcial.

El asesor genético debe solicitar primero al prefecto regional un permiso de ejercicio (véase más adelante "5 grados). b. Solicitud de autorización de práctica para el nacional para un ejercicio permanente (LE)).

*Para ir más allá*: Artículo L. 1132-3 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

**Reglas profesionales**

El profesional está sujeto a las siguientes reglas profesionales:

- respeto por la vida y la persona humana. Como tal, el profesional debe actuar en el mejor interés de sus pacientes y respetar su dignidad y privacidad;
- Realizar un seguimiento de la atención de los pacientes
- Respetar el secreto profesional
- cumplir con la prescripción médica del médico
- Proporcionar toda la información y explicaciones necesarias a las personas que la consultan de manera apropiada, inteligible, precisa y justa;
- no discriminación.

*Para ir más allá*: Artículo R. 1132-7 y el siguiente del Código de Salud Pública.

**Tenga en cuenta que**

El profesional de la genética también está sujeto, al examinar las características genéticas de una persona con fines médicos, a las buenas prácticas[decreto del 27 de mayo de 2013](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027513617&categorieLien=id) definir las reglas de buenas prácticas para examinar las características genéticas de una persona con fines médicos.

**Actividades acumuladas**

El asesor genético sólo podrá realizar cualquier otra actividad profesional si tal combinación es compatible con la dignidad y calidad requeridas por su profesión.

*Para ir más allá*: Artículo R. 1132-6 del Código de Salud Pública.

**Sanciones penales**

El consejero genético se enfrenta a una pena de un año de prisión y una multa de 15.000 euros si:

- estudia las características genéticas de una persona con fines no médicos o de investigación o sin obtener el consentimiento;
- desvia de sus fines médicos o de investigación, la información recopilada sobre una persona a través del examen de sus características genéticas;
- busca la identificación de una persona por sus huellas dactilares de ADN fuera de una medida de investigación o investigación durante un procedimiento para verificar un registro civil por parte de las autoridades diplomáticas o consulares.

*Para ir más allá*: Artículo R. 1132-7 y el siguiente del Código de Salud Pública.

4°. Seguro
-------------------------------

Como profesional de la salud liberal, el asesor genético debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de pre-informe para un ejercicio temporal e informal (LPS)

Antes de cualquier prestación de servicios, el nacional de un Estado de la UE o del EEE debe solicitar una declaración previa al prefecto de la región en la que desee ejercer.

Esta declaración debe ir acompañada de los siguientes documentos justificativos:

- Una copia de su documento de identidad válido o cualquier documento que acredite su nacionalidad;
- Una copia de su título de entrenamiento
- un certificado de menos de tres meses que certifique que está legalmente establecido en un Estado miembro y que no hay prohibición de ejercer;
- cuando ni la actividad profesional ni la formación estén reguladas en la UE o en el Estado del EEE, como prueba de que ha estado en esta actividad durante un año en los últimos diez años a tiempo completo o a tiempo parcial;
- cuando el certificado de formación haya sido expedido por un tercer Estado a la UE pero reconocido en un Estado miembro distinto de Francia:- reconocimiento del título de formación establecido por el estado que lo reconoció,
  - cualquier documentación que justifique que ha estado practicando durante tres años a tiempo completo o a tiempo parcial.

**Procedimiento**

En el plazo de un mes a partir de la recepción de la declaración, el prefecto de la región informa al solicitante:

- que puede iniciar la prestación de servicios sin verificación previa de sus cualificaciones profesionales;
- diferencia sustancial entre sus cualificaciones profesionales y la formación requerida en Francia, por lo que el solicitante debe someterse a una prueba de aptitud para demostrar que ha adquirido los conocimientos necesarios para El ejercicio de la actividad en Francia;
- que no puede comenzar su prestación de servicios.

Durante este mismo período, si una dificultad causa un retraso en su toma de decisiones, el prefecto regional informa al profesional y debe tomar su decisión:

- Dentro de los dos meses siguientes a la resolución de la dificultad;
- tres meses a partir de la fecha en que se informó al demandante de la existencia de esta dificultad.

**hora**

Si el prefecto regional no responde en el plazo de un mes, el solicitante puede comenzar a prestar servicios.

**Tenga en cuenta que**

La declaración es renovable cada año. Si la situación cambia, el profesional debe declararlos.

*Para ir más allá*: Artículos R. 1132-4 y R. 4331-12 del Código de Salud Pública; 8 de diciembre de 2017 orden sobre la declaración previa de servicios para asesores genéticos, médicos y preparadores de farmacias y farmacias, así como para ocupaciones en el Libro III de Parte IV del Código de Salud Pública.

### b. Solicitud de autorización de ejercicio para el nacional para un ejercicio permanente (LE)

**Autoridad competente**

El consejero genético debe presentar su solicitud, por carta recomendada, en doble copia al prefecto regional tras el asesoramiento del Comité de Asesores Genéticos.

**Documentos de apoyo**

La solicitud toma la forma de un archivo con los siguientes documentos justificativos:

- Un[formulario de solicitud de autorización para la práctica](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0CC409C087758F57A925D6603C395546.tplgfr40s_2?idArticle=LEGIARTI000021777495&cidTexte=LEGITEXT000021777492&dateTexte=20180112), completado y firmado;
- Una fotocopia del documento de identidad válido del solicitante
- Copiar el título de formación para ejercer la profesión;
- cualquier documento de menos de un año de edad, justificando la formación y la experiencia profesional del solicitante.

*Para ir más allá*: Artículo L. 1132-3 , auto de 20 de enero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización pertinentes para el examen de las solicitudes presentadas para la práctica en Francia de las profesiones de los asesores genéticos, enfermera, masajista, pedicura-podólogo, terapeuta ocupacional, manipulador en electrorología médica y dietista.

**Procedimiento**

El profesional deberá, en el plazo de dos meses, indicar su elección al prefecto de la comarca que le indicará la lista de direcciones regionales de juventud, deportes y cohesión social competentes para organizarlas.

El profesional tendrá que presentar una solicitud para las pruebas en papel libre, así como una copia de la decisión del prefecto regional especificando la naturaleza y duración de la prueba.

**Retrasos y resultados del procedimiento**

Dentro de un mes de comenzar, el profesional recibe una citación a la prueba de aptitud o al curso de ajuste.

Al final de la prueba, el prefecto regional autoriza, en caso de éxito, al profesional a ejercer la profesión de asesor genético. En caso contrario, se le niega el permiso para ejercer.

**Bueno saber: medidas de compensación**

El prefecto regional responsable de expedir la autorización de ejercicio podrá decidir que el nacional actuará a su elección:

- o una prueba de aptitud, que consiste en una serie de preguntas escritas y clasificadas;
- un curso de ajuste en un centro de atención médica, y dando lugar a un[informe de evaluación](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000022023573&fastPos=2&fastReqId=1524346737&categorieLien=cid&oldAction=rechTexte) del director de enseñanza del aprendiz.

*Para ir más allá* : decreto de 24 de marzo de 2010 por el que se establecen las modalidades de organización de la prueba de aptitud y el curso de adaptación para el ejercicio en Francia de asesor genético, masajista, pedicura-podólogo, terapeuta ocupacional, manipulador electrorodia médica y dietista por nacionales de los Estados miembros de la Unión Europea o parte en el acuerdo sobre el Espacio Económico Europeo.

### c. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

