﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP162" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Salvavidas" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="salvavidas" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/salvavidas.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="salvavidas" -->
<!-- var(translation)="Auto" -->


Salvavidas
==========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El socorrista es un profesional responsable de garantizar la seguridad física y sanitaria de los nadadores. Es posible que pueda rescatar a un nadador en dificultad es en cualquier momento, aplicar técnicas de reanimación e implementar primeros auxilios, si es necesario.

El socorrista también diseña y lleva a cabo acciones de despertar, descubrimiento, aprendizaje multidisciplinar y enseñanza de natación codificada. Organiza la seguridad de las actividades acuáticas y los practicantes, y gestiona una estación de rescate. Tiene que intervenir con todos los públicos (desde niños muy pequeños hasta personas mayores).

*Para ir más allá* : Apéndice I del decreto modificado del 8 de noviembre de 2010 por el que se crea la especialidad "actividades acuáticas y natación" del certificado profesional para jóvenes, educación popular y deporte.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La actividad de los socorristas está sujeta a la aplicación del Artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas, incluyendo la Juventud Profesional, la Educación Popular y el Deporte ( BPJEPS) o el Diploma Estatal de Juventud, Educación Popular y Deporte (DEJEPS).

Como profesor de deportes, el socorrista debe poseer un diploma, un título profesional (acompañado de un certificado de especialización "rescate y seguridad en ambientes acuáticos"):

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos L. 212-1 y R. 212-84 del Código del Deporte.

#### Entrenamiento

El socorrista debe tener la especialidad "actividades acuáticas y natación" PJEPS y el Certificado de Especialización (CS) "Rescate del Agua y Seguridad". Este diploma está clasificado como un nivel IV, es decir, una licenciatura. Atestigua la posesión de competencias profesionales esenciales para el ejercicio de la profesión de animador en el ámbito de la especialidad de que se trate.

Los titulares de uno de los desarrollos deportivos DEJEPS mencionan "carrera de natación", "natación sincronizada", "buceo" o "waterpolo" también pueden practicar como socorrista siempre que también sean titulares de la CS de Rescate Y Seguridad Acuática. Este diploma está clasificado como Nivel III, es decir, nivel bac 2.

En todos los casos, el diploma (BPJEPS o DEJEPS) se puede obtener a través de un contrato de aprendizaje, educación continua, un contrato de profesionalización o validación de experiencia (VAE). Para obtener más información, puede ver[sitio web oficial](http://www.vae.gouv.fr/) Vae.

**Condiciones de acceso a la formación que conducen a las "actividades acuáticas y natación" PJEPS**:

El interesado deberá:

- Ser mayores de edad
- Poseer un Certificado de Capacitación de Primeros Auxilios en Equipos de Nivel 1 (PSE1 o su equivalente), al día con la educación continua, con la producción del certificado de reentrenamiento anual;
- aprobar los exámenes de ingreso en la organización de capacitación;
- presentar un certificado médico, de menos de tres meses de edad, establecido de conformidad con el modelo de la Lista III del orden de 8 de noviembre de 2010 mencionado anteriormente;
- para cumplir con los eventos deportivos (pruebas previas a los requisitos) detallados en la Lista III del orden del 8 de noviembre de 2010, que son los siguientes:- una prueba de rendimiento deportivo (ir 800 metros en estilo libre en menos de 16 minutos). El éxito de este evento es atestiguado por el Director Regional de Juventud, Deportes y Cohesión Social,
  - tres pruebas separadas de rescate de agua. Para más detalles, es aconsejable consultar el auto de 8 de noviembre de 2010 mencionado anteriormente.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse al Apéndice V del auto de 8 de noviembre de 2010 mencionado anteriormente.

La formación para la profesión de actividades acuáticas y natación se alterna normalmente y dura entre nueve y dieciocho meses.

*Para ir más allá* : decreto de 8 de noviembre de 2010 por el que se crea la especialidad "actividades acuáticas y natación" del certificado profesional de juventud, educación popular y deporte y sus anexos.

###### Condiciones de acceso a la formación que conducen a la designación DEJEPS "carrera de natación"

El interesado deberá:

- Rellene el formulario de registro estándar con un documento de identidad con fotografía
- Ser mayores de edad
- producir capacitación en primeros auxilios de nivel 1 (PSE1) o su formación equivalente y actualizada;
- presentar un certificado médico de no contradictorio con la práctica y la enseñanza de la "raza natatoria" de menos de tres meses de edad;
- pasar las pruebas de acceso a la formación;
- cumplir con los requisitos educativos requeridos: poder evaluar los riesgos asociados con la práctica de la disciplina, rescatar al practicante y presentar el programa deportivo federal en "carrera de natación", los conceptos del plan de carrera del nadador y el principios fundamentales de la formación. Estas habilidades se comprueban durante la realización de una sesión de entrenamiento deportivo de 20 minutos seguida de un máximo de 30 minutos de entrevista;
- presentar el certificado de éxito, expedido por el director técnico de natación, a la prueba de seguridad realizada a una distancia de 50 metros (sin gafas de natación, clip nasal o uso de la escalera) que consiste en un inicio libre desde el borde de la piscina, un curso de natación libre de 25 metros, una inmersión "pato" con una búsqueda de un maniquí regulador sumergido a 25 metros del punto de partida a una profundidad de entre 1,80 y 3 metros, el ascenso del muñeco a la superficie, un remolque de un persona para una distancia de 25 metros al borde de la piscina y luego a la salida de la víctima;
- para presentar el certificado de éxito en una de las tres pruebas (cuyo contenido se detalla en el artículo 3 del decreto de 15 de marzo de 2010 supra) para justificar un nivel técnico y un dominio del entorno de competición en "carrera de natación", " nadar con aletas" o "natación de manos";
- certificado de éxito, expedido por el Director Técnico Nacional de Natación o por el Director Técnico Nacional, a la prueba medley de 400 metros realizada de acuerdo con las normas de la Federación Internacional de Natación o, para personas con discapacidad en virtud de la Federación Francesa de Handisport, de acuerdo con las normas adaptadas, de la Federación Internacional de Natación;
- presentar el certificado de éxito, expedido por el Director Técnico Nacional de Natación o el Director Técnico Nacional de Estudios y Deportes Subacuáticos, a una de las dos pruebas de rendimiento correspondientes al calendario definido como el orden del 15 de marzo de 2010 en "carrera de natación" o "natación con aletas";
- presentar un certificado, expedido por el Director Técnico Nacional de Natación, el Director Técnico Nacional o, en su defecto, por el presidente de una federación que sea miembro del Consejo Interfederal de Actividades Acuáticas de acuerdo con la Federación natación francesa, justificando una experiencia educativa (voluntario o profesional) en natación 800 horas, ya sea en un club de una federación deportiva acreditada o dentro de un polo en la lista elaborada por el Ministro responsable de En virtud de la Sección R. 221-26 del Código del Deporte durante un mínimo de tres años en los últimos cinco años antes de la entrada en formación.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más detalles, es aconsejable referirse a los artículos 4, 6 y siguientes del decreto de 15 de marzo de 2010 mencionado anteriormente.

**Condiciones de acceso a la formación que conduzcan a la designación deJEPS "natación sincronizada":**

El interesado deberá:

- Rellene el formulario de registro estándar con un documento de identidad con fotografía
- Ser mayores de edad
- producir capacitación en primeros auxilios de nivel 1 (PSE1) o su formación equivalente y actualizada;
- presentar un certificado médico de no contradictorio con la práctica y enseñanza de la natación sincronizada, de menos de tres meses de edad;
- pasar las pruebas de acceso a la formación;
- cumplir con los requisitos educativos requeridos: poder evaluar los riesgos asociados con la práctica de la disciplina, rescatar al practicante y presentar el programa deportivo federal en natación sincronizada, los conceptos de planes de carrera y el principios fundamentales de la formación. Estas habilidades se comprueban durante la realización de una sesión de entrenamiento deportivo de 20 minutos, seguida de un máximo de 30 minutos de entrevista;
- presentar el certificado de éxito, expedido por el director técnico de natación, a la prueba de seguridad realizada a una distancia de 50 metros (sin gafas de natación, clip nasal o uso de la escalera) que consiste en un inicio libre desde el borde de la piscina, un curso de natación libre de 25 metros, una inmersión "pato" con una búsqueda de un maniquí regulador sumergido a 25 metros del punto de partida a una profundidad de entre 1,80 y 3 metros, el ascenso del muñeco a la superficie, un remolque de un persona para una distancia de 25 metros al borde de la piscina y luego a la salida de la víctima del agua;
- presentar el certificado de éxito, expedido por el Director Técnico Nacional de Natación, de un curso que justifique un nivel técnico correspondiente a las competencias dirigidas en el pase de competición de "natación sincronizada";
- presentar el certificado de éxito, expedido por el Director Técnico Nacional de Natación, a una prueba correspondiente a un solo técnico que dura un minuto y 30 segundos incluyendo elementos técnicos realizados de acuerdo con las normas de la Federación Natación Internacional en un orden establecido (para más detalles sobre el contenido de esta prueba, es aconsejable referirse al artículo 3 del decreto de 15 de marzo de 2010 supra);
- presentar el certificado de éxito, expedido por el Director Técnico Nacional de Natación, a una prueba que comprende cuatro figuras impuestas, según se define de acuerdo con las normas de la Federación Internacional de Natación:- Figura 420: caminata hacia atrás,
  - Figura 355 e: Marsopa de giro de 360 grados,
  - Figura 301 d: barracuda gira 180 grados,
  - Figura 140: pierna doblada flamenca;
- presentar el certificado, expedido por el Director Técnico Nacional de Natación para estructuras afiliadas a la Federación Francesa de Natación, o por el Director Técnico Nacional o, en su defecto, por el presidente de una federación miembro de la Consejo Interfederal de actividades acuáticas de acuerdo con la Federación Francesa de Natación por sus estructuras afiliadas, justificando una experiencia educativa (voluntaria o profesional) en natación sincronizada de 800 horas ya sea dentro de un club autorizado de la federación deportiva o dentro de una pole en la lista elaborada por el Ministro de Deportes en virtud del artículo R. 221-26 del Código de Deporte durante un mínimo de tres años en los últimos cinco años anteriores entrar en el entrenamiento.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más detalles, es aconsejable referirse a los artículos 4, 6 y siguientes del decreto de 15 de marzo de 2010 mencionado anteriormente.

**Condiciones de acceso a la formación que conduzcan a la designación de "buceo" DEJEPS:**

El interesado deberá:

- Rellene el formulario de registro estándar con un documento de identidad con fotografía
- Ser mayores de edad
- producir capacitación en primeros auxilios de nivel 1 (PSE1) o su formación equivalente y actualizada;
- presentar un certificado médico de no contradictorio con la práctica y la enseñanza del buceo, de menos de tres meses de edad;
- pasar las pruebas de acceso a la formación;
- cumplir con los requisitos educativos requeridos: poder anticipar los riesgos asociados con la práctica de la disciplina, rescatar al practicante y presentar el programa federal de deportes de buceo, los conceptos del plan de carrera del buceador y el principios fundamentales de la formación. Estas habilidades se comprueban durante la realización de una sesión de entrenamiento deportivo que dura 1 hora y 15 minutos, seguida de una entrevista de hasta 45 minutos;
- presentar el certificado de éxito, expedido por el director técnico de natación, a la prueba de seguridad No 1 llevada a cabo a una distancia de 50 metros (sin gafas de natación, clip nasal o uso de la escalera) que consiste en un inicio libre desde el borde de la piscina , un campo de natación libre de 25 metros, una inmersión "pato" con una búsqueda de un muñeco regulador sumergido a 25 metros del punto de partida a una profundidad entre 1,80 y 3 metros, el ascenso del muñeco a la superficie, un remolque de un persona para una distancia de 25 metros al borde de la piscina y luego a la salida de la víctima del agua;
- presentar el certificado de éxito, expedido por el Director Técnico de Natación, a la prueba de seguridad No 2, incluyendo la elevación de un muñeco regulador sumergido a una profundidad de entre 3 y 5 metros (sin usar gafas de natación o gafas de natación, o clip de la nariz)
- presentar el certificado de éxito, expedido por el Director Técnico Nacional de Natación, para llevar a cabo cinco ejercicios realizados en el trampolín de 1 metro, un nivel correspondiente a las competencias cubiertas en el pase de competición de "buceo" emitido por la Federación Francesa de Natación;
- presentar un certificado, expedido por el Director Técnico Nacional de Natación, que justifique una práctica oficial en el concurso de buceo;
- presentar un certificado, expedido por el Director Técnico Nacional de Natación, que justifique una demostración de cuatro inmersiones a 1 metro y dos inmersiones realizadas a 3 metros de acuerdo con las reglas de la Federación Internacional de Natación ( Para más detalles sobre el contenido de esta demostración, es aconsejable referirse al artículo 3 del decreto de 15 de marzo de 2010 supra);
- presentar un certificado, expedido por el Director Técnico Nacional de Natación para estructuras afiliadas a la Federación Francesa de Natación o por el director técnico nacional o, en su defecto, por el presidente de una federación miembro de la asesoramiento interfederal de actividades acuáticas de acuerdo con la Federación Francesa de Natación para sus estructuras afiliadas, justificando una experiencia pedagógica de buceo de 500 horas (voluntario o profesional), o dentro de un club de una federación deportiva acreditada, ya sea dentro de un polo en la lista elaborada por el Ministro de Deportes en virtud del artículo R. 221-26 del Código del Deporte, que dura un mínimo de tres años en los cinco años anteriores a la entrada en formación .

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más detalles, es aconsejable referirse a los artículos 4, 6 y siguientes del decreto de 15 de marzo de 2010 mencionado anteriormente.

**Condiciones de acceso a la formación que conduzcan a la designación de "polo de agua" DEJEPS:**

El interesado deberá:

- Rellene el formulario de registro estándar con un documento de identidad con fotografía
- Ser mayores de edad
- Poseer el Certificado de Primeros Auxilios en un equipo de Nivel 1 (o su equivalente) el día de la educación continua;
- pasar las pruebas de acceso a la formación;
- comunicar un certificado médico de no contradictorio con la enseñanza y la práctica del waterpolo, de menos de tres meses de edad;
- cumplir con los requisitos pedagógicos requeridos: ser capaz de evaluar los riesgos objetivos asociados con la práctica de la disciplina, anticipar los riesgos potenciales para el practicante y dominar el comportamiento y las acciones a realizar en caso de un incidente o Accidente. Estas habilidades se comprueban en una sesión de 30 minutos, seguida de una entrevista de 20 minutos;
- ser capaz de rescatar al jugador de waterpolo, presentar el programa deportivo federal y los conceptos del plan de carrera del jugador de polo. Estas habilidades se comprueban durante la realización de una sesión de desarrollo de deportes de waterpolo de 20 minutos, seguida de un máximo de 30 minutos de entrevista;
- presentar el certificado de éxito, expedido por el director técnico de natación, a la prueba de seguridad No 1 llevada a cabo a una distancia de 50 metros (sin gafas de natación, clip nasal o uso de la escalera) que consiste en un inicio libre desde el borde de la piscina , un campo de natación libre de 25 metros, una inmersión "pato" con una búsqueda de un muñeco regulador sumergido a 25 metros del punto de partida a una profundidad entre 1,80 y 3 metros, el ascenso del muñeco a la superficie, un remolque de un persona para una distancia de 25 metros al borde de la piscina y luego a la salida de la víctima del agua;
- para llevar a cabo un curso de tiro que justifique un nivel técnico correspondiente a las habilidades dirigidas en el pase de competición de "polo de agua" emitido por la Federación Francesa de Natación. El éxito de esta prueba es objeto de un certificado expedido por el Director Técnico Nacional de Natación;
- presentar un certificado de experiencia de una práctica mínima de waterpolo durante tres temporadas deportivas dentro de un equipo equivalente al nivel "nacional 3", "nacional 2" o "nacional 1" y justificando la participación efectiva en el campo de la 10 partidos por temporada. Este certificado es expedido por el Director Técnico Nacional de Natación;
- presentar un certificado de experiencia educativa (voluntario o profesional) en waterpolo de 800 horas, ya sea dentro de un club de una federación deportiva acreditada o dentro de un polo en la lista elaborada por el Ministro responsable de los deportes en Artículo R. 221-26 del Código del Deporte, durante un mínimo de tres años en los últimos cinco años. Esta certificación es establecida por el director técnico nacional de natación para las estructuras afiliadas a la Federación Francesa de Natación o por el director técnico nacional o, en su defecto, por el presidente de una federación que es miembro de la junta. actividades acuáticas interfederales de acuerdo con la Federación Francesa de Natación para sus estructuras afiliadas.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más detalles, es aconsejable consultar los artículos 4, 6 y siguientes del auto de 15 de marzo de 2010 mencionado anteriormente.

**Condiciones para el acceso a la formación que conduzca a la CS "Rescate y Seguridad del Agua" para "Actividades Acuáticas de Natación" de PJEPS o para los titulares de DEJEPS mencionando "carrera de natación", "natación sincronizada", " "buceo" o "polo de agua"**  :

Las personas que tienen una "actividades de agua y natación" o un "curso de natación" DEJEPS especializado, "natación sincronizada", "buceo" o "polo de agua" también deben sostener el CS "Rescate y Seguridad del Agua" para poder practicar como socorrista.

Condiciones de acceso a la formación que conduzcan al Certificado de Especialización:

- presentar un certificado médico, de menos de tres meses de edad, que acredite las habilidades físicas asociadas con la práctica del rescate acuático, cuyo modelo se adjunta al decreto del 15 de marzo de 2010 por el que se establece la CS;
- producir capacitación en primeros auxilios de nivel 1 (PSE1) o su formación equivalente y actualizada;
- producir los certificados de éxito en tres pruebas técnicas descritas en la Lista IV de la orden del 15 de marzo de 2010 que consisten en:- un curso continuo de agua de 100 metros en una piscina,
  - un salvavidas con aletas, máscara y snorkel, continuo, 250 metros, en piscina,
  - un de rescatar a una persona en un ambiente acuático.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más detalles, es aconsejable remitirse a los artículos 7 y 8 del decreto de 15 de marzo de 2010 mencionado anteriormente.

*Para ir más allá* : decreto del 15 de marzo de 2010 por el que se crea el certificado de especialización "rescate y seguridad en ambientes acuáticos" asociado con la especialidad BPJEPS "actividades acuáticas y natación" y desarrollo deportivo especializado deJEPS (...).

**Es bueno saber**

Las personas con el Diploma Estatal Superior de Educación Juvenil, Educación Popular y Deporte (DESJEPS) rendimiento deportivo especializado también pueden practicar como socorrista. Este diploma estatal de Nivel II es emitido por el Director Regional de Juventud y Deportes. Para practicar en el campo, optará por una de las menciones de "carrera de natación", "natación sincronizada", "buceo" o "polo de agua" del DESJEPS y también tendrá que sostener el CS "Rescate y Seguridad del Agua".[Más información](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/des-jeps/Reglementation-11081/La-specialite-performance-sportive-du-DES-JEPS-et-les-mentions-unites-capitalisables-complementaire-et-certificats-de-specialisation-s-y-rapportant/) sobre los requisitos de admisión y la formación previa a la graduación.

*Para ir más allá*: Artículos D. 212-35 y siguientes del Código del Deporte, ordenado el 20 de noviembre de 2006 por el que se organiza el Diploma Estatal de Juventud, Educación Popular y Especialidad Deporte "Desarrollo Deportivo" emitido por el Ministerio de Juventud y Juventud 1 de julio de 2008.

###### El certificado de socorrista (CAEPMNS)

El Certificado de Salvavidas (CAEPMNS) certifica que las personas con un diploma de socorrista siguen proporcionando suficiente seguridad de socorrista rescate hídrico y seguridad pública.

La capacidad de practicar como socorrista debe evaluarse y revisarse periódicamente. Este es el propósito de obtener o renovar el CAEPMNS.

En principio, la aptitud para ejercer la profesión se verifica antes del 31 de diciembre del quinto año después de la graduación conferiendo el título de socorrista (es decir, BPJEPS, DEJEPS o DESJEPS acompañado por la CS "rescate y seguridad en el medio acuático") o la emisión del último certificado de aptitud para ejercer como socorrista.

###### Autoridad competente

El CAEPMNS es entregado por el Director Regional de Juventud, Deportes y Cohesión Social o por el Director de Juventud, Deportes y Cohesión Social, tras una sesión de formación seguida de una evaluación.

###### Validez de CaEPMNS

El CAEPMNS se emite por un período de cinco años. Sin embargo, en el caso de una razón legítima, la validez del certificado puede prorrogarse hasta cuatro meses, es decir, hasta el 30 de abril del año siguiente. Es el director regional de Juventud, Deportes y Cohesión Social o el director de Juventud, Deportes y Cohesión Social quien decide, en su caso, ampliar la validez del CAEPMNS.

###### Fecha de entrada en vigor de CAEPMNS

La validez del CAEPMNS se extiende a partir del 1 de enero del año siguiente a su emisión. Sin embargo, si el registro para la sesión de formación se produce después de la expiración del período de validez del certificado anterior, el período de validez se extiende a partir de la fecha de emisión.

###### Requisitos de inscripción para la formación de caEPMNS

Para obtener CAEPMNS, el candidato debe registrarse para una sesión de capacitación, seguida de una evaluación. El archivo de registro debe incluir:

- Una solicitud gratuita de registro;
- La copia completa del archivo de registro preparado por la organización de capacitación acompañada de un documento de identidad con fotografía;
- Fotocopia del documento de identidad de la persona
- fotocopia del diploma conferido el título de socorrista (BPJEPS, DEJEPS o DESJEPS, acompañado por la CS);
- una fotocopia del certificado de "primeros auxilios del equipo de Nivel 1" o su equivalente, con una fotocopia del certificado anual de educación continua;
- un certificado médico de no contradictorio con la profesión de socorrista, de menos de tres meses de edad, basado en el modelo del Apéndice II del auto de 23 de octubre de 2015 relativo al certificado de aptitud para ejercer profesión de socorrista;
- fotocopia de la última CAEPMNS, si la hubiera.

Se pueden solicitar algunas piezas adicionales (sobres liberados... Para obtener más información, es aconsejable consultar con el centro de formación en cuestión.

El archivo debe archivarse ante el organizador de la sesión, al menos dos meses antes de la fecha de inicio de la sesión.

###### Condiciones de la sesión de capacitación y evaluación que condujo a la finalización o renovación de CAEPMNS

Entrenamiento de hasta 21 horas. Su objetivo es mantener las habilidades y adquirir nuevos conocimientos relacionados con la evolución de la profesión. Al final de la sesión de formación, se organiza una evaluación. Incluye dos pruebas, realizadas por el candidato con camiseta y pantalones cortos (usando el traje, gafas de piscina, máscara, clip nasal o cualquier otro equipo que esté prohibido):

- un evento de natación gratuito con aletas llevadas a cabo continuamente, a una distancia de 250 metros;
- un curso de rescate, cuyos detalles se especifican en el artículo 7 del decreto de 23 de octubre de 2015 supra.

*Para ir más allá* : orden de 23 de octubre de 2015 relativo al certificado de aptitud para ejercer la profesión de socorrista.

#### Costos asociados con la calificación

La formación para BPJEPS, DEJEPS o DESJEPS se paga y el costo varía en función de la referencia elegida y de la organización de formación. Del mismo modo, se paga la formación que conduce a la obtención del CAEPMNS (unos 200 euros, como indicación). Para [más detalles](https://foromes.calendrier.sports.gouv.fr/#/formation), es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como socorrista en Francia para personas que hayan sido condenadas por cualquier delito o por uno de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Requisito de notificación (con el fin de obtener la tarjeta de educador deportivo profesional)
-----------------------------------------------------------------------------------------------------------------------

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el [sitio web oficial](https://eaps.sports.gouv.fr).

#### hora

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

#### Documentos de apoyo

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

5°. Proceso de cualificaciones y formalidades
-------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

#### Autoridad competente

La declaración previa de actividad debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde el Departamento en el que declarante quiere realizar su actuación.

#### hora

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

#### Documentos de apoyo

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

#### hora

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

#### Documentos de apoyo para la primera declaración de actividad

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

#### Evidencia para una declaración de renovación de la actividad

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

**Bueno saber: medidas de compensación**

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

