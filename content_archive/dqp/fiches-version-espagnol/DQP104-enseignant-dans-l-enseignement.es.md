﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP104" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Profesor de secundaria" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="profesor-de-secundaria" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/profesor-de-secundaria.html" -->
<!-- var(last-update)="2020-04-15 17:21:07" -->
<!-- var(url-name)="profesor-de-secundaria" -->
<!-- var(translation)="Auto" -->


Profesor de secundaria
======================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:07<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El profesor de secundaria es un profesional responsable de transmitir todos sus conocimientos en una o más disciplinas, para asegurar el seguimiento individual y la evaluación de los estudiantes de la escuela media y secundaria.

Los profesores certificados y los profesores asociados pueden realizar esta actividad.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El acceso al cuerpo de los funcionarios de la Educación Nacional, se realiza por medio de concursos cuya naturaleza depende de la disciplina enseñada.

Por lo tanto, para acceder al cuerpo de profesores certificados, el profesional debe tener:

- Un certificado de aptitud para la enseñanza de segundo grado (CAPES) para disciplinas generales;
- Un Certificado de Aptitud para la Educación Física y Deportiva (CAPEPS) para la enseñanza de la educación física y deportiva (EPS);
- Un certificado de aptitud para la cátedra de educación técnica (CAPET) para disciplinas técnicas;
- certificado de aptitud para la enseñanza del segundo grado agrícola (CAPESA).

**Tenga en cuenta que**

El profesional que desee enseñar en una escuela secundaria vocacional de educación pública debe obtener el Concurso de Acceso al Cuerpo de Profesores Profesionales de Secundaria (CAPLP).

Para acceder al cuerpo de profesores asociados, el profesional debe superar el concurso de agregación (véase a continuación "Concurso de agregación").

#### Entrenamiento

**Para el acceso al cuerpo de profesores certificados**

El acceso a los cuerpos de los profesores certificados se puede hacer:

- a través de un concurso para obtener un certificado de aptitud para el profesorado;
- por medio de inscribirse en una lista de aptitudes. Si es necesario, los candidatos son reclutados dentro de un nombramiento para nueve mandatos otorgados el año anterior entre profesionales:- licenciatura (B.A. 3) o un grado que les permita ejercer en la disciplina deseada,
  - por lo menos cuarenta años de edad,
  - han estado en este negocio durante al menos diez años, cinco de los cuales son como licenciatarios.

Tras la contratación por inscripción en la lista de aptitudes, el profesional deberá completar una pasantía de un año para ser titular.

*Para ir más allá*: Artículos 5, 27 y 28 del Decreto 72-581, de 4 de julio de 1972, sobre el estatuto especial de los profesores certificados.

**Certificado de Aptitud para la Enseñanza de Segundo Grado (CAPES)**

Existen tres concursos de acceso a CAPES:

- el concurso externo accesible a los candidatos que posean un máster (B.A. 4 o bac 5) o un título o diploma reconocido como equivalente por el Ministro de Educación;
- competencia interna, accesible a funcionarios estatales con tres años de experiencia profesional en un servicio público, y titulares de una licenciatura (B.A. 3) o un título o diploma reconocido como equivalente por el Ministro a cargo del Ministro a cargo de Educación
- el tercer concurso, a disposición de los candidatos que requieran al menos cinco años de práctica en el sector privado.

Estos concursos, que se pueden preparar en una escuela superior de enseñanza y educación (ESPE), incluyen pruebas de elegibilidad y admisión y se organizan en diferentes secciones. La lista de estas secciones se establece en el[Artículo 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000032790151&cidTexte=LEGITEXT000027363503&dateTexte=20180503) 19 de abril de 2013 citado a continuación.

La naturaleza de las pruebas de elegibilidad y admisión para estos concursos varía dependiendo de la sección en la que se registre el candidato. El programa de eventos se establece en los anexos[Ⅰ](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034792126&cidTexte=LEGITEXT000027363503&dateTexte=20180503),[Ⅱ.](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034792128&cidTexte=LEGITEXT000027363503&dateTexte=20180503) Y[Ⅳ](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000029052849&cidTexte=LEGITEXT000027363503&dateTexte=20180503) 19 de abril de 2013.

Una vez admitido, el candidato debe realizar una pasantía de un año en una academia, definida por el Ministro responsable de la educación con el fin de adquirir las habilidades necesarias para llevar a cabo la función de profesor. Al final de esta pasantía, el profesor es nombrado por el rector de la academia en la jurisdicción desde la que completó su pasantía. Este mandato le da al profesional el CAPES.

*Para ir más allá* Decreto 72-581, de 4 de julio de 1972, sobre el estatuto especial de los profesores certificados; decreto de 19 de abril de 2013 por el que se establece la organización de los concursos para el certificado de aptitud para la cátedra de segundo grado.

**Certificados específicos de aptitud**

El profesor que desee ejercer en una disciplina específica podrá solicitar los distintos concursos con el fin de obtener:

- CAPEPS para enseñar educación física y deportiva. La lista de los diversos eventos de los concursos CAPEPS se adjunta a los anexos[Ⅰ](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000027363612&cidTexte=LEGITEXT000027363489&dateTexte=20180503),[Ⅱ.](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034719692&cidTexte=LEGITEXT000027363489&dateTexte=20180503) y III de la[Detenido](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?cidTexte=JORFTEXT000027361487&dateTexte=20180503) 19 de abril de 2013 en el que se establece n.o en la que se organizan las competiciones del Certificado de Aptitud para la Educación Física y Deportiva;
- CAPET para la enseñanza de una disciplina técnica. Si es así, el candidato puede inscribirse en una de las secciones[Artículo 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000032621389&cidTexte=LEGITEXT000027363387&dateTexte=20180503) del decreto de 19 de abril de 2013 por el que se establecen las modalidades de organización de los concursos para el certificado de aptitud para la cátedra de educación técnica. Los términos y condiciones de las distintas pruebas de CAPET se establecen en los anexos[Ⅰ](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034792361&cidTexte=LEGITEXT000027363387&dateTexte=20180503),[Ⅱ.](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000032621394&cidTexte=LEGITEXT000027363387&dateTexte=20180503) Y[Ⅳ](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034792363&cidTexte=LEGITEXT000027363387&dateTexte=20180503) del auto de 19 de abril de 2013;
- CAPESA o CAPETA, para enseñar en una escuela secundaria agrícola. La lista de secciones del concurso se establece en el[Artículo 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000027998111&cidTexte=LEGITEXT000022193686&dateTexte=20180503) del decreto de 14 de abril de 2010 por el que se establecen las secciones y procedimientos de organización de los concursos para el Certificado de Cualificación para la Enseñanza del Segundo Agrícola y el Certificado de Cualificación para la Cátedra de Educación Técnica Agrícola. Las modalidades y el programa de los ensayos CAPESA y CAPETA se establecen en los anexos de la citada sentencia;
- CAPLP para enseñar en una escuela secundaria vocacional. Las diversas secciones y posibles opciones se adjuntan a la[Artículo 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000029052855&cidTexte=LEGITEXT000027364754&dateTexte=20180503) del decreto de 19 de abril de 2013 por el que se establecen las secciones y procedimientos para la organización de los concursos del certificado de aptitud para los profesores de bachillerato profesional. El programa de eventos en estos concursos se establece en los anexos[Ⅰ](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000034792376&cidTexte=LEGITEXT000027364754&dateTexte=20180503),[Ⅱ.](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4DFC64B4232B935DDC881F9DA1296C2B.tplgfr22s_2?idArticle=LEGIARTI000036029690&cidTexte=LEGITEXT000027364754&dateTexte=20180503)III y IV de la orden.

El profesional, una vez admitido en uno de estos concursos, debe realizar una pasantía en las mismas condiciones que las requeridas para el CAPES (véase supra "Certificado de Aptitud para la Cátedra de Enseñanza de Segundo Grado (CAPES)").

**En vista del acceso a los órganos de los profesores asociados**

**Concurso de Agregación**

El profesional que desee convertirse en profesor asociado puede participar en el concurso de agregación. Para participar en este concurso, el candidato debe ser el titular:

- para participar en el concurso externo, un máster (B.A. 4 o bac 5), un título o un diploma reconocido como equivalente por el Ministro de Educación;
- Un doctorado para participar en el concurso externo especial;
- para participar en el concurso interno, un máster o un título o diploma reconocido como equivalente y justificar haber ocupado un puesto de servicio público durante al menos cinco años.

La agregación se emite al candidato exitoso:

- Las pruebas de la competencia interna, externa o externa específica;
- una pasantía de un año en una institución de educación superior.

Las diversas secciones en las que el candidato puede inscribirse se adjuntan a la[Artículo 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=17683F2E931605CDD4B005F3BB126986.tplgfr22s_2?idArticle=LEGIARTI000034264480&cidTexte=LEGITEXT000022746527&dateTexte=20180502) 28 de diciembre de 2009 se establecen las secciones y modalidades de organización de los concursos de agregación.

El programa de eventos para los distintos concursos de agregación se establece en los anexos[Ⅰ](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=17683F2E931605CDD4B005F3BB126986.tplgfr22s_2?idArticle=LEGIARTI000034792739&cidTexte=LEGITEXT000022746527&dateTexte=20180502),[I bis](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=17683F2E931605CDD4B005F3BB126986.tplgfr22s_2?idArticle=LEGIARTI000032946014&cidTexte=LEGITEXT000022746527&dateTexte=20180502) Y[Ⅱ.](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=17683F2E931605CDD4B005F3BB126986.tplgfr22s_2?idArticle=LEGIARTI000034792742&cidTexte=LEGITEXT000022746527&dateTexte=20180502) 28 de diciembre de 2009.

*Para ir más allá* Decreto 72-580, de 4 de julio de 1972, sobre el estatuto especial de los profesores asociados de educación de segundo grado.

#### Costos asociados con la calificación

El costo de la formación varía en función del curso previsto. Es aconsejable acercarse a las instituciones interesadas, para obtener más información.

### b. Nacionales de la UE: para la entrega temporal y ocasional (entrega gratuita de servicios (LPS)) o permanente (liquidación libre (LE))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) podrá trabajar en Francia como profesor de escuela secundaria, de forma temporal e informal (LPS) o (LE) en las mismas condiciones que el nacional francés.

*Para ir más allá* : Decreto No 2010-311, de 22 de marzo de 2010, sobre la forma de contratar y recibir a los nacionales de los Estados miembros de la Unión Europea u otro Estado Parte en el Acuerdo sobre el Espacio Económico en el Organismo, un marco de empleo o un puesto de trabajo de la función Servicio público francés.

3°. Reglas profesionales
---------------------------------

El profesional que ejerce la actividad de profesor de secundaria está bajo la autoridad del rector de la academia en la que ejerce. Como servidor público, debe llevar a cabo su actividad con dignidad, imparcialidad, integridad y probidad.

En caso de incumplimiento de estas obligaciones, el profesional incurre en las siguientes sanciones disciplinarias:

- Una advertencia
- Culpar
- Deslistarse del gráfico de progreso
- una bajada del escalón;
- una exclusión temporal del cargo por un máximo de 15 días;
- una degradación
- una exclusión temporal del cargo durante tres meses a dos años;
- jubilación automática;
- una revocación.

*Para ir más allá*: Artículo 37 del Decreto 72-581, de 4 de julio de 1972, sobre el estatuto especial de los docentes certificados y el artículo 14 del Decreto 72-580, de 4 de julio de 1972, relativo al estatuto especial de los profesores asociados de enseñanza de segundo grado; Artículo 25 de la Ley 83-634, de 13 de julio de 1983, que establece el derecho y las obligaciones de los servidores públicos.

