﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP004" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Servicios funerarios" -->
<!-- var(title)="Recepcionista funerario" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="servicios-funerarios" -->
<!-- var(title-short)="recepcionista-funerario" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/servicios-funerarios/recepcionista-funerario.html" -->
<!-- var(last-update)="2020-04-15 17:22:27" -->
<!-- var(url-name)="recepcionista-funerario" -->
<!-- var(translation)="Auto" -->


Recepcionista funerario
=======================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:27<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Oficial de Recepción Funeraria es un profesional de la funeraria pública cuya actividad es dar la bienvenida, informar y asesorar a las familias.

Este profesional puede establecer y vender artículos funerarios, recibir llamadas telefónicas y referir a las familias a un maestro de ceremonias o a un funeral.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de un agente de recepción funeraria, el profesional debe:

- capacitación (ver infra "2. Formación") para justificar su capacidad profesional;
- práctica en una instalación de despacho de la prefectura emitida en las condiciones prescritas por las Secciones R. 2223-56 a R. 2223-65 del Código General de Gobierno Local.

**Tenga en cuenta que**

Los profesionales que han servido como oficiales de recepción funeraria durante un año a partir del 10 de mayo de 1995 se consideran justificados en su formación profesional.

*Para ir más allá*: Artículos D. 2223-35 y R. 2223-50 del Código General de Gobierno Local.

#### Entrenamiento

Para ejercer, el profesional debe completar la formación dentro de los seis meses siguientes a su inicio de su actividad. El curso de 40 horas cubre:

- legislación y reglamentos funerarios, así como higiene y seguridad (4 p.m.);
- La Psicología y Sociología del Dolor (8 a.m.);
- el protocolo del funeral, las prácticas y el simbolismo de los diversos ritos funerarios, incluida la cremación (4 p.m.).

Al final de su formación, el profesional recibe un certificado de formación profesional por parte de la organización o del centro nacional del servicio público territorial.

Esta formación debe ser importada por una organización de formación acreditada. Cuando se habla con los funcionarios de servicio público, la capacitación es proporcionada por el Centro Nacional de Servicio Público Territorial.

*Para ir más allá*: Artículos R. 2223-44 y R. 2223-53 del Código General de Gobierno Local.

#### Costos asociados con la calificación

La capacitación que conduce a la actividad del oficial de recepción funeraria se paga y su costo varía dependiendo de la institución en cuestión. Es aconsejable acercarse a esta propiedad para obtener más información.

### b. Nacionales de la UE: para el ejercicio temporal y ocasional (Entrega gratuita de servicios (LPS)

Todo nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE), legalmente establecido y que actúe como oficial de recepción funeraria, podrá ejercer el mismo servicio ocasional y temporal de forma ocasional y temporal. actividad en Francia.

Para ello, el interesado deberá:

- Cuando ni el acceso a la actividad ni su ejercicio estén regulados en ese Estado miembro, justifiquen haber ejercido esta actividad en un Estado miembro durante al menos un año en los últimos diez años;
- práctica en una institución autorizada por el prefecto del departamento donde se encuentra su sede central (véanse los artículos R. 2223-56 a R. 2223-65 del Código General de Autoridades Locales).

*Para ir más allá*: Artículo L. 2223-47 del Código General de Autoridades Locales.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Cualquier nacional de un Estado de la UE o del EEE legalmente establecido y que ejerza la actividad de un oficial de recepción funeraria podrá llevar a cabo la misma actividad en Francia de forma permanente.

Para ello, el individuo debe justificar una experiencia adquirida en los últimos diez años:

- ya sea como ejecutivo o como independiente de:- tres años consecutivos en esta actividad,
  - dos años consecutivos, siempre que justifique la formación previa y posea un certificado reconocido por el Estado miembro en el que haya ejercido,
  - dos años consecutivos desde que justifica haber realizado esta actividad durante tres años como empleado;
- ya sea como empleado y justificar haber recibido formación previa sancionada por un certificado expedido en dicho Estado miembro.

Si el nacional no justifica ninguna de estas experiencias, tendrá que justificar:

- Poseer un certificado de competencia o un certificado de formación que le permita ejercer la actividad de un oficial de recepción funeraria expedido en un Estado miembro que regule el ejercicio de esta actividad;
- han estado haciendo esto durante al menos un año en los últimos diez años y tienen un certificado de competencia o título de formación cuando el Estado miembro no regula el acceso o el ejercicio de esta actividad. Esta experiencia no es necesaria si el certificado certifica la formación regulada.

Una vez que el profesional cumple con estos requisitos, debe solicitar el reconocimiento de calificación (ver más abajo "5 grados). a. Solicitud de reconocimiento de cualificación para un ejercicio permanente (LE)).

*Para ir más allá*: Artículo L. 2223-48 del Código General de Autoridades Locales.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El profesional está obligado por las regulaciones funerarias nacionales.

Como tal, debe incluir:

- garantizar el cumplimiento de las disposiciones de información familiar, incluidas las menciones obligatorias de las cotizaciones y órdenes de compra emitidas en los artículos R. 2223-24 a R. 2223-32-1 del Código General de Autoridades Locales (información relacionados con el profesional y la naturaleza de los servicios ofrecidos);
- proponer y cumplir con los requisitos de financiamiento funerario (ver Sección R. 2223-33 del Código General de Autoridades Locales).

*Para ir más allá*: Artículos R. 2223-23-5 y siguientes del Código General de Autoridades Locales.

4°. Sanciones penales
------------------------------------------

El profesional es multado con 75.000 euros si ofrece servicios de preparación para un funeral o dentro de los dos meses siguientes a la muerte con el fin de obtener el pedido de suministros o beneficios relacionados con una muerte. También está prohibida la barandilla pública para los mismos beneficios.

Además, se castiga con una pena de cinco años de prisión y una multa de 75.000 euros para que un profesional ofrezca directa o indirectamente cualquier beneficio (ofertas, regalos, promesas, etc.) a personas con conocimiento de una muerte en la oportunidad de su actividad para obtener la conclusión de beneficios relacionados con la muerte o para recomendar los servicios del profesional.

*Para ir más allá*: Artículos L. 2223-33 y L. 2223-35 del Código General de Gobierno Local.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de calificación para un ejercicio permanente (LE)

**Autoridad competente**

El profesional debe presentar su solicitud al prefecto del departamento donde practica.

**Documentos de apoyo**

La solicitud se realiza presentando un archivo con los siguientes documentos justificativos:

- Una identificación válida
- Un certificado de competencia o un certificado de formación expedido por una autoridad competente;
- cualquier prueba, en su caso, de que el nacional haya trabajado como oficial de recepción fúnebre durante un año a tiempo completo o a tiempo parcial en los últimos diez años en un Estado miembro que no regule la profesión.

**Procedimiento**

El prefecto reconoce la recepción de la solicitud en el plazo de un mes y le informa en caso de falta de documento. El prefecto tiene las cualificaciones profesionales verificadas y, si es necesario, puede decidir someterlo a una medida de compensación (véase "Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos R. 2223-133 y siguientes del Código General de Autoridades Locales.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la formación recibida por el profesional y la necesaria para llevar a cabo la actividad de un oficial de recepción funeraria, el prefecto del departamento podrá exigir que se someta a la elección de una prueba de aptitud o curso de adaptación.

El curso de alojamiento consiste en el nacional que trabaja con un profesional como oficial de recepción funeraria durante un máximo de dos años. La prueba de aptitud, por otra parte, implica la verificación de todos los conocimientos profesionales establecidos en el artículo 2 de la Orden de 25 de agosto de 2009 por la que se aplican las medidas de auditoría de conocimientos y compensatorias para la reconocimiento de cualificaciones profesionales en el sector funerario.

*Para ir más allá*: Artículo L. 2223-50 del Código General de Autoridades Locales.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

