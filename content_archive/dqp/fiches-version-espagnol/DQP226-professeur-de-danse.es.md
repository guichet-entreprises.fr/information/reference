﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP226" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Profesor de danza" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="profesor-de-danza" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/profesor-de-danza.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="profesor-de-danza" -->
<!-- var(translation)="Auto" -->


Profesor de danza
=================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El profesor de danza es un profesional encargado de enseñar danza clásica, contemporánea o jazz. Transmite los conocimientos fundamentales necesarios para una práctica artística autónoma de los estudiantes.

Dependiendo del caso, ofrece actividades de despertar, iniciación y realización de aprendizaje inicial, especialmente en el contexto de los cursos que conducen al certificado de estudios coreográficos de instituciones educativas especializadas. Comunidades.

Acompaña el desarrollo de prácticas artísticas amateurs, en particular actuando como asesor y asistencia en la formulación de proyectos. Participa en la realización de las acciones llevadas a cabo por la estructura que le emplea y su inscripción en la vida cultural local.

También se le puede exigir que intervenga en cursos de preparación preprofesional o de formación profesional.

*Para ir más allá*: Artículo L. 362-1 del Código de Educación; Apéndice I del decreto de 20 de julio de 2015 relativo a los diversos caminos a la profesión de profesor de danza.

**Es bueno saber**

El trabajo de animador de danza se refiere a todas las formas de danzas fuera de las danzas clásicas, contemporáneas y jazz. Dirige talleres y sesiones grupales. Introduce al público a la danza en un proceso de descubrimiento y mediación cultural que promueve tanto escuchar un ritmo como aprender desde un paso básico, sin la cuestión del desarrollo o validación de las habilidades de los practicantes. Además, garantiza la seguridad de los profesionales y participa en el proyecto general de la estructura en la que practica. Para más información, es aconsejable consultar el[Sitio web de la Federación Francesa de Danza](http://ffdanse.fr/index.php/formation/cqp-animateur-danse) (FFD).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La práctica de enseñar o enseñar danza clásica, contemporánea o jazz está sujeta a la aplicación del Artículo L. 362-1 del Código de Educación. Para ejercer como profesor de danza, el profesional debe tener:

- Diploma de maestro de danza emitido por el Estado o certificado de aptitud para las tareas de los maestros de danza;
- un diploma francés o extranjero reconocido como equivalente;
- una exención concedida debido a la fama particular o experiencia confirmada en la enseñanza de la danza de la que puede hacer uso de sí mismo.

En el desempeño de sus funciones públicas de enseñanza de danza, los agentes del Estado, la Opera Nacional de París y los Conservatorios Nacionales Superiores de Música, así como los agentes de las autoridades locales están exentos de la obligación para obtener el Diploma estatal (DE) como profesor de danza cuando su estatus especial prevé la obtención de una CA emitida por el estado.

**Tenga en cuenta que**

Las personas que habían estado enseñando danza durante más de tres años a partir del 11 de julio de 1989 pueden estar exentas de obtener el ED del maestro DANCE. La exención se considera adquirida cuando no se ha notificado a la persona ninguna decisión en contrario al final de un período de tres meses a partir de la presentación de la solicitud.

*Para ir más allá*: Artículos L. 362-1, L. 362-3 y L. 362-4 del Código de Educación.

La animación de otras formas de danza no está regulada. Sin embargo, un facilitador de danza certificado de cualificación profesional (CQP) fue creado por la animación de la Comisión Mixta Nacional de Formación en Empleo (CPNEF) con el fin de profesionalizar a los ponentes en animaciones de baile. Para más información, es aconsejable consultar el[Sitio web de FFD](http://ffdanse.fr/index.php/formation/cqp-animateur-danse).

##### Obtención de plenos derechos del profesor de danza

Algunas personas se benefician en su totalidad del diploma de maestro de danza emitido por el estado. Se trata de artistas coreográficos que han recibido formación educativa y justifican una actividad profesional de al menos tres años en una de las siguientes instituciones:

- el ballet de la Opera Nacional de París;
- los ballets de los teatros del encuentro de los teatros de ópera municipales de Francia;
- Centros coreográficos nacionales;
- empresas de un Estado miembro de la Unión Europea (UE)* u otro Estado parte en el Acuerdo del Espacio Económico Europeo (EEE)*, cuya lista se establece en[Apéndice V del decreto del 20 de julio de 2015 sobre los diversos caminos hacia la profesión de profesor de danza](http://www.esmd.fr/IMG/pdf/annexes_arrete_du_20_juillet_2015_extrait_du_bo_no_248.pdf).

Para calificar como DE de profesor de danza, los coreógrafos deben presentar un certificado de seguimiento de la formación educativa. Este certificado es emitido por el prefecto regional y menciona la opción en la que se otorga el ED.

*Para ir más allá*: Artículo L. 362-1 y el siguiente del Código de Educación; Artículo 18 y Apéndice V del decreto de 20 de julio de 2015 relativo a los diversos caminos a la profesión de profesor de danza.

#### Entrenamiento

##### Profesor de DANCE DE

Este diploma está registrado en el[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) y clasificado en el nivel III de la nomenclatura de nivel de certificación.

La formación está organizada por el Centro Nacional de Danza (CND). Para más información, es aconsejable consultar el[Sitio web de CND](http://www.cnd.fr/formation/diplome_etat).

Tiene tres opciones: danza clásica, danza contemporánea y danza jazz.

Este diploma se puede obtener en su totalidad a través de la validación de la experiencia (VAE). Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr). En el contexto de un VAE, el candidato también tendrá que ser puesto en una situación profesional.

*Para ir más allá*: Artículo L. 362-1 del Código de Educación; orden de 20 de julio de 2015 relativo a los diversos caminos a la profesión de profesor de danza en virtud del artículo L. 362-1 del Código de Educación.

###### Prerrogativas

El ed de profesor de danza da fe de las siguientes habilidades:

- tener los conocimientos asociados necesarios para la transmisión de su género coreográfico. Esto implica:- los elementos fundamentales y específicos de su género coreográfico,
  - conocimiento anatómico y fisiológico del movimiento,
  - conocimiento musical y conocimiento del ritmo corporal,
  - desarrollar un proyecto educativo. Esto significa tener en cuenta la realidad de los estudiantes y las características de la asignatura que se enseña;
- implementar su proyecto educativo. Esto implica:- para construir y animar una situación de aprendizaje colectivo,
  - para llevar a cabo secuencias de aprendizaje en sus dimensiones técnicas y artísticas,
  - para movilizar el conocimiento asociado,
  - para evaluar,
  - prácticas más amplias.

Este diploma permite al titular enseñar:

- en escuelas privadas de danza o en escuelas públicas de música, danza y teatro bajo la jurisdicción de las autoridades locales;
- en otras estructuras que ofrecen enseñanzas de danza, incluidas asociaciones, instituciones socioculturales y clubes deportivos;
- instituciones de educación superior bajo la tutela del Ministerio de Cultura o el Ministerio de Educación Superior.

*Para ir más allá* : Apéndice I del Decreto de 20 de julio de 2015 sobre los distintos caminos a la profesión de profesor de danza en virtud del artículo L. 362-1 del Código de Educación.

###### Entrenamiento de sensano

El entrenamiento dura al menos 600 horas. Se organiza en cuatro unidades de entrenamiento (UF):

- una UF de formación musical (100 horas como mínimo);
- una enseñanza de historia de la danza UF (50 horas como mínimo);
- una enseñanza de anatomía-fisiología UF (50 horas como mínimo);
- una UF docente (400 horas como mínimo) con tres opciones: danza clásica, danza contemporánea, danza jazz.

*Para ir más allá*: Artículo 10 y Apéndice II de la Orden del 20 de julio de 2015 sobre Diferentes Caminos a la profesión de Profesor de Danza en virtud del Artículo L. 362-1 del Código de Educación.

###### Condiciones de acceso

El acceso a la formación ed de profesor de danza está condicionado a la realización de un examen de Aptitud Técnica (EAT) con tres opciones: danza clásica, danza contemporánea y danza jazz. Bajo ciertas condiciones, esta revisión puede estar exenta.

Este examen está abierto a candidatos que tienen al menos 18 años de edad al 31 de diciembre del año de aprobación.

Para inscribirse en la formación que conduce al ED de profesor de danza, el interesado debe presentar con la Dirección Regional de Asuntos Culturales de su lugar de residencia un expediente que incluya:

- Una solicitud de registro que cumple con un formulario estándar
- Dos fotografías de identidad;
- dos sobres sellados con el nombre, el nombre y la dirección del candidato;
- Una fotocopia del DNI
- un extracto de antecedentes penales (boletín 3) o un certificado de no condena expedido por una autoridad competente del estado de origen del candidato, de menos de tres meses de edad;
- un certificado de no contradictorio para un movimiento de danza practicar menos de tres meses de edad;
- el certificado de éxito de la EAT expedido por el centro de examen o, en su caso, los documentos justificativos necesarios para expedir la exención de las pruebas de la EAT u obtener las equivalencias de las unidades docentes.

El archivo de registro debe enviarse dos meses antes de la fecha establecida para la entrada en la formación.

Una vez finalizado el expediente, el Director Regional de Asuntos Culturales emite al candidato un folleto de capacitación que menciona, si es necesario, la exención y equivalencias de las unidades docentes concedidas. Este folleto le permite solicitarlo a un centro de formación autorizado.

*Para ir más allá*: Artículos 3, 7.8 y 9 de la Orden del 20 de julio de 2015 sobre Diferentes Caminos a la profesión de Profesor de Danza en virtud del Artículo L. 362-1 del Código de Educación.

##### CA a las funciones de profesor de danza

La CA es un título de posgrado inscrito en el[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) , en el nivel I de la nomenclatura de nivel de certificación.

Es emitido por el Conservatorio Nacional de Música y Danza (CNSMD) en Lyon. Para más información, es aconsejable consultar el[Sitio web de CNSMD](http://www.cnsmd-lyon.fr/fr-2/les-formations/departement-de-formation-a-lenseignement-danse).

El Ca valida los conocimientos y habilidades generales y profesionales correspondientes al final del 2o ciclo de estudios de posgrado preparándose para la enseñanza del arte coreográfico.

La CA puede obtenerse a través de la formación inicial, el aprendizaje, la formación profesional continua o el vaE.

*Para ir más allá*: Artículos 1 y siguientes del Decreto No 2016-1421 de 20 de octubre de 2016 relativo a la Ca para las funciones de profesor de danza; Artículo 1 del decreto de 6 de enero de 2017 relativo a la CA para las funciones de profesor de danza y por el que se establecen las condiciones para la autorización de las instituciones de educación superior para expedir este diploma.

###### Condiciones de acceso

El interesado deberá:

- Pasar la entrada
- tener un título estatal como profesor de danza, o su dispensa, o un título equivalente en la disciplina en cuestión;
- poseer un diploma nacional profesional como bailarín en la disciplina, o un diploma que valide un posgrado o una licenciatura en danza, en la disciplina correspondiente al género coreográfica esperada;
- tener una licenciatura o un diploma francés o extranjero admitido en exento o equivalencia;
- justifican una actividad profesional como artista de danza en la disciplina de que se trate, que puede ser atestiguada por 500 horas como artista coreográfico asalariado, y una experiencia docente que dura al menos 300 Horas.

*Para ir más allá*: Artículo 2 del Decreto de 6 de enero de 2017 relativo al certificado de idoneidad para las funciones de profesor de danza y por el que se establecen las condiciones para la autorización de las instituciones de enseñanza superior para expedir este título.

##### Animador de baile CQP

###### Entrenamiento de sensano

El CQP se basa en tres unidades de certificación:

- Preparación de intervenciones: el facilitador prepara tanto su sesión como un ciclo relacionado con el proyecto de estructura;
- la animación: el anfitrión da la bienvenida a la audiencia, supervisa el grupo y evalúa su acción;
- la inclusión en el proyecto de la estructura: el facilitador se identifica en el organigrama de su estructura y participa en el funcionamiento de su estructura, así como en la preparación y realización de un proyecto colectivo.

La evaluación de las diferentes unidades implica una entrevista individual, la producción de una animación y la producción de un texto escrito de unas diez páginas.

###### Condiciones de acceso:

- Ser mayores de edad
- Poseer el Certificado de Prevención y Socorro Cívico de Nivel 1 (PSC1);
- tienen las habilidades técnicas (música, movimiento, técnicas de baile practicadas por el candidato) y la motivación para involucrarse en un proceso de formación profesional;
- Ser capaces de situarse en relación con las habilidades esperadas y los arreglos de validación en el plan de estudios propuesto;

Por último, definir oportunidades de ayuda formativa basada en los diplomas o habilidades ya adquiridos por el candidato, sin eximirlo de las pruebas de evaluación.

Para más información, es aconsejable consultar el[Sitio web de FFD](http://ffdanse.fr/index.php/formation/cqp-animateur-danse).

#### Costos asociados con la calificación

Se pagan las capacitaciones que conducen al profesor de danza ED, a la CA y al facilitador de danza CQP. Sus costos varían de la organización de la capacitación a la organización de la formación.

Para más detalles, es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

El nacional de un Estado miembro de la Unión Europea (UE) u otro Estado parte en el Espacio Económico Europeo (EEE) legalmente establecido en uno de estos Estados podrá enseñar danza clásica, contemporánea o jazz en Francia siempre que se haya enviado una declaración previa de actividad a la Dirección General de Creación Artística (véase más adelante "b. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales) (LPS)).

Si la actividad o formación que la dirige no está regulada en el estado en el que está establecida, el nacional también debe justificar haber llevado a cabo esta actividad en uno o varios Estados miembros o partes en el Acuerdo EEE durante el equivalente al menos un año a tiempo completo en los diez años anteriores al beneficio.

La interpretación o ejecución se lleva a cabo bajo el título profesional vigente en el Estado miembro de establecimiento del demandante cuando dicho título existe en dicho Estado para el ejercicio de la profesión de profesor de danza. Este título se indica en la lengua oficial o en una de las lenguas oficiales de ese estado. Si ese título profesional no existe, menciona su título de formación.

**Es bueno saber**

Si viaja, el demandante está sujeto a normas de conducta profesionales, reglamentarias o administrativas directamente relacionadas con las cualificaciones profesionales, como la definición de la profesión, el uso de títulos y la mala conducta. profesionales que tienen un vínculo directo y específico con la protección y seguridad de los consumidores, así como con las disposiciones disciplinarias aplicables en Francia a los profesionales que practican la misma profesión.

*Para ir más allá*: Artículo L. 362-1-1 del Código de Educación; Artículos 1 a 4 del Decreto de 23 de diciembre de 2008 sobre las condiciones de práctica de la profesión de profesor de danza aplicables a los nacionales de un Estado miembro de la UE u otro Estado parte en el Acuerdo EEE.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

El nacional de un Estado de la UE o del EEE puede establecerse en Francia para trabajar permanentemente como profesor de danza clásica, contemporánea o jazz si es propietario de:

- un certificado de competencia o certificado de formación expedido por las autoridades competentes de un Estado miembro u otro Estado parte en el Acuerdo EEE que regule el acceso a la profesión de profesor de danza o de su ejercicio, y que permita ejercer legalmente esta profesión en ese estado;
- un certificado de formación expedido por un tercer Estado, que haya sido reconocido en un Estado miembro u otro Estado parte en el Acuerdo EEE y que le haya permitido ejercer legalmente en dicho Estado durante un período mínimo de tres años, siempre que experiencia laboral está certificada por el estado en el que fue adquirida;
- un certificado de competencia o un certificado de formación expedido por las autoridades competentes de un Estado miembro u otro Estado parte en el Acuerdo EEE, que no regule el acceso o el ejercicio de la profesión de profesor de danza y que acredite su preparación para el ejercicio de la profesión cuando justifique el ejercicio de esta actividad a tiempo completo durante al menos un año o a tiempo parcial por un período total de equivalente, en los últimos diez años en un Estado miembro o parte en el acuerdo sobre Eee. Esta justificación no es necesaria cuando la formación que conduce a esta profesión está regulada en el Estado miembro o en el Acuerdo EEE en el que ha sido validada.

Sin embargo, si los conocimientos, habilidades y habilidades adquiridas por el solicitante durante su experiencia laboral o aprendizaje permanente han sido debidamente validados por una organización para este propósito competente en un Estado miembro o en un tercer país no es probable que suba, total o parcialmente, diferencias sustanciales en la formación, el Ministro responsable de la cultura podrá exigir que el solicitante se someta a medidas de indemnización, consistentes en la curso de adaptación o prueba de aptitud (véase más adelante "b. Solicitar el reconocimiento de cualificaciones profesionales para los nacionales europeos para el ejercicio permanente (LE)").

*Para ir más allá*: Artículo L. 362-1-1 del Código de Educación.

3°. Condiciones de honorabilidad
-----------------------------------------

La práctica de la enseñanza de la danza clásica, contemporánea o jazz en Francia está prohibida para las personas que han sido condenadas a una pena de prisión sin una sentencia condicional de más de cuatro meses por delitos:

- Violación
- Agresión sexual
- agresión sexual a un menor;
- Proxenetismo.

*Para ir más allá*: Artículo L. 362-5 del Código de Educación; Artículos 222-22 a 222-33, 225-5 a 225-10 y 227-22 a 227-28 del Código Penal.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Un nacional de un Estado de la UE o del EEE que desee ejercer como profesor de danza de forma temporal y ocasional en Francia debe hacer una declaración previa.

La solicitud deberá renovarse anualmente si el demandante tiene previsto ejercer su actividad durante el año de que se trate o en caso de un cambio material en su situación.

*Para ir más allá*: Artículos 1 y siguientes del Decreto de 23 de diciembre de 2008 sobre las condiciones de ejercicio de la profesión de profesor de danza aplicable a los nacionales de un Estado miembro de la Comunidad Europea u otro Estado parte en el Acuerdo EEE.

#### Autoridad competente

La declaración previa de actividad debe dirigirse a la Dirección General de Creación Artística.

#### Documentos de apoyo

El solicitante de registro debe adjuntar a su declaración un expediente que consta de los siguientes documentos:

- Formulario Cerfa 14531Completado, fechado y firmado;
- Copiar el ID
- información sobre la cobertura del seguro u otros medios de protección personal o colectiva con respecto a la responsabilidad personal (estos documentos no deben tener más de tres meses);
- un certificado que certifica que el demandante está legalmente establecido en un Estado miembro para ejercer como profesor de danza en una o más de las opciones (danza clásica, contemporánea o jazz) y que no incurre en ninguna prohibición ejercicio temporal;
- prueba de cualificaciones profesionales en una o más de las opciones (danza clásica, contemporánea o jazz);
- cuando la profesión de profesor de danza no esté regulada en el Estado miembro del establecimiento del demandante, las pruebas de que el demandante ha estado enseñando durante al menos el equivalente a dos años a tiempo completo en los diez años antes de presentar la declaración. Esta justificación no es necesaria cuando la formación que conduce a esta ocupación está regulada en el Estado de la UE o en el EEE en el que se ha validado.

#### Costo

Gratis.

### b. Solicitar el reconocimiento de cualificaciones profesionales para los nacionales de la UE para el ejercicio permanente (LE)

Los nacionales de un Estado de la UE o del EEE que hayan obtenido sus cualificaciones profesionales en uno de estos Estados y que deseen establecerse en Francia para ejercer como profesorderos de danza, deben solicitar el reconocimiento de su cualificaciones en una o más opciones (clásica, contemporánea, jazz).

#### Autoridad competente

La solicitud de reconocimiento de cualificaciones profesionales debe dirigirse a la Dirección General de Creación Artística.

#### hora

La Dirección General de Creación Artística emite un recibo en el plazo de un mes a partir de la recepción de la solicitud de reconocimiento de cualificaciones profesionales. Si es así, les informa de cualquier documento que falte.

El Ministro responsable de la cultura decide sobre la solicitud por decisión motivada en un plazo de cuatro meses a partir de la recepción del expediente completo.

Después de revisar el expediente y si la persona es reconocida como calificada para servir como profesora de danza, el Ministro le emite un certificado de reconocimiento de las cualificaciones profesionales.

#### Medidas de compensación

Si el Ministro considera que los conocimientos adquiridos por la persona durante su experiencia profesional no es probable que rellenen, total o parcialmente, diferencias sustanciales en la formación, puede proponerle que se someta a un o un curso de ajuste en dos casos:

- cuando se determina que la formación del solicitante se refiere a temas sustancialmente diferentes de los del programa de educación general del profesor de danza;
- cuando la formación regulada en el Estado de Origen no incluye una o más de las opciones y esta diferencia se caracteriza por una formación específica requerida para el profesor de danza ED y en temas sustancialmente diferentes de los cubiertos por el certificado o la designación de formación a que se hace referencia.

En este caso, la decisión motivada del Ministro establece que el individuo debe dar a conocer su elección en el plazo de dos meses y establece la duración de la pasantía y las asignaturas de la prueba de aptitud a la que será sometida la persona.

Sobre la base del informe de evaluación de la pasantía o del resultado obtenido en la prueba de aptitud, el Ministro de Cultura decide sobre la solicitud en el plazo de un mes y pone a la persona certificado de reconocimiento de cualificaciones profesionales. profesor de baile.

#### Documentos de apoyo

Los solicitantes deben adjuntar un expediente consistente en el formulario Cerfa 14531 a su solicitud de reconocimiento de cualificaciones profesionales.y los siguientes documentos justificativos, si los hubiera, traducidos al francés:

- Una copia del documento de identidad
- Una descripción de la experiencia profesional del solicitante
- el contenido de los estudios y prácticas realizados durante la formación inicial, indicando el número de horas por asignatura para las enseñanzas teóricas, la duración de las prácticas, el campo en el que se realizaron y, en su caso, el resultado de la evaluaciones realizadas y, en su caso, el registro de cursos de educación continua que indiquen el contenido y la duración de estas prácticas. Estos elementos son emitidos y atestiguados por la estructura de formación en cuestión;
- si el solicitante posee un certificado de competencia o un certificado de formación expedido por las autoridades competentes de un Estado de la UE o del EEE que regula el acceso a la profesión o a su práctica y que permite que la profesión sea ejercitada legalmente en Este estado, una copia de este certificado o título;
- si el solicitante posee un título de formación expedido por un tercer Estado reconocido por un Estado de la UE o del EEE y que ha permitido ejercer la profesión en dicho Estado durante al menos tres años:- copiando el reconocimiento por un estado de la UE o del EEE del certificado de formación expedido por un tercer Estado,
  - una copia del documento que certifique el ejercicio por el solicitante de la profesión de profesor de danza durante un mínimo de tres años en el estado que haya reconocido el título de formación,
  - cuando el período mínimo de tres años no se haya realizado en el Estado que haya reconocido el diploma, certificado o título, el titular deberá ser reconocido por el Ministro de Cultura, habida cuenta de los conocimientos y cualificaciones atestiguados por dicho diploma, certificado o título y por toda la formación y experiencia profesional adquirida;
- si el solicitante posee un certificado de competencia o un certificado de formación expedido por las autoridades competentes de un Estado de la UE o del EEE que no regula el acceso o el ejercicio de la profesión y el solicitante justifica el ejercicio de esta actividad a tiempo completo durante dos años en los últimos diez años:- Una copia de este certificado o título,
  - evidencia por cualquier medio de que la persona ha servido como profesor de danza durante al menos dos años a tiempo completo en los diez años anteriores a la solicitud. Esta evidencia no es necesaria cuando la formación que conduce a esta ocupación está regulada en el estado de la UE o en el EEE en el que se ha validado.

Cuando el interesado no puede facilitar estos documentos, presenta un reconocimiento o certificación, expedida por las autoridades competentes del Estado de origen, de la existencia de estos documentos. En caso de duda grave, podrá solicitarse la confirmación de la autenticidad de los diplomas, certificados o títulos expedidos.

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 362-1-1 y los siguientes artículos del Código de Educación; Artículos 5 a 11 del auto de 23 de diciembre de 2008 supra.

### c. Si es necesario, solicitar el reconocimiento de la equivalencia al maestro de danza ED

El reconocimiento de equivalencia se otorga a los titulares de otro grado en educación dance.

La autoridad competente verifica que la cualificación resultante del título en poder corresponde bien al nivel de requisito establecido por el repositorio de certificación del profesor de danza.

El solicitante deberá demostrar en todos los documentos que existe una correspondencia en términos de:

- nivel de danza en el momento de la entrada en formación con el nivel de la PRUEBA de la EAT;
- el volumen por hora y el contenido de las enseñanzas que siguieron con el volumen y el contenido por hora de las unidades de enseñanza que constituyen el maestro de danza ED.

Cualquier pieza escrita en un idioma extranjero debe ir acompañada de un traductor jurado.

#### Autoridad competente

La solicitud debe dirigirse a la Dirección General de Creación Artística. Emite un acuse de recibo tan pronto como se completa el archivo.

La aplicación es escuchada por la Inspección de Creación Artística.

La concesión de la exención se pronuncia por decreto ministerial.

#### hora

La respuesta a la solicitud se notifica en un plazo de diez meses a partir de la fecha del acuse de recibo. La decisión de rechazar debe estar justificada.

*Para ir más allá*: Artículo 25 y Apéndice IV del Auto de 20 de julio de 2015 mencionado anteriormente.

### d. Si es necesario, solicite una exención del ED del profesor de danza

Se puede conceder una exención debido a la fama particular o la experiencia confirmada en la enseñanza de la danza.

#### Autoridad competente

La solicitud debe dirigirse a la Dirección General de Creación Artística. Emite un acuse de recibo tan pronto como se completa el archivo.

La aplicación es escuchada por la Inspección de Creación Artística.

La concesión de la exención se pronuncia por decreto ministerial. La decisión de rechazar debe estar justificada.

#### hora

La respuesta a la solicitud se notifica en un plazo de diez meses a partir de la fecha del acuse de recibo.

#### Fama especial

El artista coreográfico (intérprete, coreógrafo, coreógrafo asistente, ensayo o maestro de ballet) debe ser capaz de justificar:

- Capacitación de alto nivel en la disciplina en cuestión;
- Notoriedad de las empresas y lugares donde ocurrió;
- la amplitud, diversidad y singularidad de su carrera artística;
- expresión de su notoriedad en los medios de comunicación y en la comunidad profesional.

La aplicación debe estar respaldada por elementos significativos tales como: contratos de trabajo, recibos de pago, obleas de la empresa, programas de habitaciones, recortes de prensa, grabaciones de interpretación audiovisual, enlaces a sitios.

#### Experiencia confirmada en la enseñanza de la danza

El profesional debe justificar:

- un curso de formación para certificar la adquisición de un alto nivel de dominio técnico en la disciplina de que se trate por la solicitud;
- una amplia práctica pedagógica:- equivalente según al menos cinco años a tiempo completo (3.600 horas) en los diez años anteriores a la solicitud,
  - con audiencias diversas, en particular en términos de edad y nivel técnico,
  - basado en la capacidad de construir el apoyo de los estudiantes en curso.

La solicitud debe estar respaldada por documentos justificativos tales como contratos, recibos de pago, diplomas, premios, programas de instituciones educativas, sesiones de formación, certificados, cartas de recomendación.

Cualquier pieza escrita en un idioma extranjero debe ir acompañada de un traductor jurado.

*Para ir más allá*: Artículo 25 y Apéndice IV del Auto de 20 de julio de 2015 mencionado anteriormente.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

