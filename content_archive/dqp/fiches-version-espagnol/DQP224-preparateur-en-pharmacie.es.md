﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP224" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Preparador de farmacias" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="preparador-de-farmacias" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/preparador-de-farmacias.html" -->
<!-- var(last-update)="2020-04-15 17:22:02" -->
<!-- var(url-name)="preparador-de-farmacias" -->
<!-- var(translation)="Auto" -->


Preparador de farmacias
=======================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:02<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El preparador de farmacias es un profesional sanitario responsable de la preparación de recetas médicas y la gestión de las existencias (suministro, almacenamiento, control de las entregas, etc.) de los medicamentos del dispensario, bajo la responsabilidad del farmacéutico.

Dentro del dispensario, también desempeña un papel como comerciante, ya que debe ser capaz de asesorar y vender productos a los clientes.

*Para ir más allá*: Artículo L. 4241-1 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de preparador de farmacia está reservada:

- Titulares de certificados de preparador de farmacia profesional;
- personas que han recibido un permiso de ejercicio expedido por el Ministro de Salud.

*Para ir más allá*: Artículos L. 4241-4 y L. 4241-6 del Código de Salud Pública.

#### Entrenamiento

El certificado de preparador de farmacia profesional es un diploma disponible después de un grado S o STL (ciencia y tecnología de laboratorio) de dos años.

El estudiante seguirá una enseñanza teórica de 800 horas en un centro de formación mientras alterna con una enseñanza práctica seguida directamente en el dispensario.

*Para ir más ley*n: Sección D. 4241-3 del Código de Salud Pública.

#### Costos asociados con la calificación

El entrenamiento vale la pena. Para más información, es aconsejable acercarse a los establecimientos dispensadores.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Un nacional de un estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), que está practicando legalmente como preparador de farmacias en uno de estos estados, puede utilizar su título profesional en Francia, ya sea temporal o temporalmente. Casual.

Tendrá que solicitarlo, antes de su primera actuación, mediante declaración dirigida al prefecto de la región en la que desea realizar la entrega (véase infra "4o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que esté legalmente establecida, el profesional deberá justificar haber la llevado a cabo en uno o más Estados de la UE o del EEE durante al menos un año, durante el transcurso de diez años antes de la actuación.

*Para ir más allá*: Artículo L. 4241-11 del Código de Salud Pública.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Todo nacional de un Estado de la UE o del EEE establecido y que practique legalmente la preparación de farmacias en ese Estado podrá llevar a cabo la misma actividad en Francia de forma permanente si:

- posee un certificado de formación expedido por una autoridad competente en otro Estado de la UE o del EEE, que regula el acceso o el ejercicio de la profesión;
- ha trabajado a tiempo completo o a tiempo parcial durante dos años en los últimos diez años en otro Estado de la UE o del EEE que no regula la formación o la práctica;
- posee un título expedido por una autoridad competente de un tercer Estado y reconocido por un Estado de la UE o del EEE, y justifica haber ejercido la actividad o profesión durante al menos tres años en el Estado de la UE o del EEE.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar una autorización individual para ejercer desde el prefecto de la región en la que desea ejercer su profesión (véase infra "4o. b. Solicitar un permiso de ejercicio para el nacional de la UE o del EEE para la actividad permanente (LE) ").

*Para ir más allá*: Artículo L. 4241-7 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Aunque no tienen las mismas prerrogativas que los farmacéuticos, los preparadores de farmacias están preocupados por las reglas del Código de ética de los farmacéuticos. Por lo tanto, deben respetar las normas relativas al funcionamiento del dispensario en la publicidad y la comunicación, el secreto profesional, la vida y la persona humana.

4°. Procedimientos y formalidades de reconocimiento de cualificación
-----------------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

**Autoridad competente**

El prefecto de la región es competente para decidir sobre la solicitud de una declaración previa de actividad.

**Documentos de apoyo**

La solicitud se realiza mediante la presentación de un archivo que incluye los siguientes documentos:

- Una copia de un documento de identidad válido
- Una copia del título de formación que permite ejercer la profesión en el estado de obtención;
- un certificado, de menos de tres meses de edad, de la autoridad competente del Estado de la UNIÓN o del EEE, que certifique que el interesado está legalmente establecido en ese Estado y que, cuando se expide el certificado, no existe ninguna prohibición, ni siquiera temporal, Ejercicio
- cualquier prueba que justifique que el nacional haya ejercido la profesión en un Estado de la UE o del EEE durante un año en los últimos diez años, cuando dicho Estado no regule la formación, el acceso a la profesión solicitada o su ejercicio;
- cuando el certificado de formación haya sido expedido por un tercer Estado y reconocido en un Estado de la UE o del EEE distinto de Francia:- reconocimiento del título de formación establecido por las autoridades estatales que han reconocido este título,
  - cualquier prueba que justifique que el nacional ha ejercido la profesión en ese estado durante tres años;
- Si es así, una copia de la declaración anterior, así como la primera declaración hecha;
- un certificado de responsabilidad civil profesional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**hora**

Una vez recibido el expediente, el prefecto de la región tendrá un mes para decidir sobre la solicitud e informará al nacional:

- Puede empezar la actuación.
- que estará sujeto a una medida de compensación si existen diferencias sustanciales entre la formación o la experiencia profesional del nacional y las requeridas en Francia;
- no podrá iniciar la actuación;
- cualquier dificultad que pueda retrasar su decisión. En este último caso, el prefecto regional podrá tomar su decisión en el plazo de dos meses a partir de la resolución de esta dificultad, y a más tardar tres meses después de la notificación al nacional.

El silencio del prefecto dentro de estos plazos valdrá la pena aceptar la solicitud de declaración.

**Tenga en cuenta que**

La rentabilidad es renovable cada año o en cada cambio en la situación del solicitante.

*Para ir más allá*: Artículo R. 4241-13 del Código de Salud Pública; 8 de diciembre de 2017 orden sobre la declaración previa de prestación de servicios para asesores genéticos, médicos y preparadores de farmacias y farmacias hospitalarias, así como para ocupaciones en el Libro III de Parte IV del Código de Salud Pública.

### b. Solicitar un permiso de ejercicio para la UE o el EEE nacional para la actividad permanente (LE)

**Autoridad competente**

La autorización de ejercicio es expedida por el prefecto de la región, previa asesoramiento del comité de preparadores de farmacia.

**Documentos de apoyo**

La solicitud de autorización se realiza mediante la presentación de un expediente en dos copias, incluidos todos los siguientes documentos:

- el [formulario de solicitud de autorización para la práctica](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021862830&dateTexte=20180319) Completado y firmado;
- Una fotocopia de un documento de identidad válido
- Una copia de su título de formación que le permite trabajar como preparador de farmacia y, si es necesario, una fotocopia de diplomas adicionales;
- todos los documentos para justificar su formación continua y su experiencia profesional adquirida en un Estado miembro;
- una declaración del Estado de la UE o del EEE de que el nacional no está sujeto a ninguna sanción;
- Una copia de todos sus certificados mencionando el nivel de capacitación recibido, y los detalles de las horas y el volumen de las enseñanzas siguieron;
- cuando ni el acceso a la formación ni su ejercicio estén regulados en el Estado miembro, ninguna documentación que justifique que ha sido preparadora de farmacias durante dos años en los últimos diez años;
- cuando el certificado de formación haya sido expedido por un tercer Estado pero reconocido en un Estado miembro, el reconocimiento del título de formación por parte del Estado miembro.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

El prefecto reconoce la recepción del archivo en el plazo de un mes y decidirá después de tener la opinión del comité de preparadores de farmacia. Este último es responsable de examinar los conocimientos y habilidades del nacional adquiridos durante su formación o durante su experiencia profesional. Puede someter al nacional a una medida de compensación.

El silencio guardado por el prefecto de la región en un plazo de cuatro meses merece la pena rechazar la solicitud de autorización.

*Para ir más allá*: Artículos R. 4241-9 a 4241-12 del Código de Salud Pública; decreto de 19 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a la comisión de autorización competente para el examen de las solicitudes presentadas para la práctica en Francia de la profesión de preparador de farmacias y preparador de farmacias Hospital.

**Bueno saber: medidas de compensación**

Si el examen de las cualificaciones profesionales atestiguadas por las cualificaciones de formación y la experiencia profesional muestra diferencias sustanciales con las cualificaciones necesarias para el acceso y el ejercicio en Francia, el interesado deberá someterse a una medida de compensación.

En función del nivel de cualificación exigido en Francia y del que posea el interesado, la autoridad competente podrá:

- Ofrecer al solicitante la opción de elegir entre un curso de ajuste o una prueba de aptitud;
- requieren un curso de ajuste y/o una prueba de aptitud.

*Para ir más allá* : decreto de 24 de marzo de 2010 por el que se establece la organización de la prueba de aptitud y el curso de adaptación para la práctica en Francia de preparador de farmacias y preparador de farmacia hospitalaria por nacionales de los Estados miembros Unión Europea o parte en el acuerdo del Espacio Económico Europeo.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

