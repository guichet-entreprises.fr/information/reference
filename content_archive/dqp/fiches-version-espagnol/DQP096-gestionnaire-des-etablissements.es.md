﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP096" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Servicios funerarios" -->
<!-- var(title)="Gerente de instalaciones funerarias" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="servicios-funerarios" -->
<!-- var(title-short)="gerente-de-instalaciones-funerarias" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/servicios-funerarios/gerente-de-instalaciones-funerarias.html" -->
<!-- var(last-update)="2020-04-15 17:22:28" -->
<!-- var(url-name)="gerente-de-instalaciones-funerarias" -->
<!-- var(translation)="Auto" -->


Gerente de instalaciones funerarias
===================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El administrador funerario es un profesional que administra una funeraria.

En particular, como profesional funerario, es responsable de determinar con las familias, la organización y las modalidades del servicio funerario y asegurar el buen funcionamiento de las actividades realizadas dentro de su institución.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ser un gestor funerario, el profesional debe:

- Poseer un diploma para ejercer la profesión de consejero funerario;
- autorización para su establecimiento (véase infra "5 grados). b. Solicitud de autorización."

*Para ir más allá*: Artículos L. 2223-23 y D. 2223-55-2 del Código General de Gobierno Local.

#### Entrenamiento

Para llevar a cabo su actividad legalmente, el administrador de una funeraria debe obtener un diploma dentro de los 12 meses siguientes a la fecha de establecimiento del establecimiento.

Este diploma se expide tan pronto como el candidato ha completado con éxito la siguiente formación:

- Formación teórica de 140 horas sobre los siguientes temas:- higiene, seguridad y ergonomía,
  - legislación funeraria y regulación,
  - psicología y sociología del duelo,
  - prácticas funerarias y ritos,
  - diseñando y organizando una ceremonia,
  - entrenando a un equipo,
  - productos, servicios y asesoramiento en venta,
  - Regulación del comercio;
- 70 horas de formación práctica, realizada en una empresa autorizada, dirección o asociación, al final de la cual se realizará una evaluación escrita;
- Entrenamiento adicional de 42 horas.

Al final de su formación, el profesional se somete a un examen, organizado por las organizaciones de formación.

*Para ir más allá*: Artículo D. 2223-55-3 y siguientes del Código General de Autoridades Locales;[decretado 30 de abril de 2012](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025789769&dateTexte=20180103) Decreto No 2012-608 de 30 de abril de 2012 sobre diplomas en el sector funerario.

**Solicitud de autorización de la institución**

La autorización sólo se expide al gerente cuando se cumplen las siguientes condiciones:

- el gerente no debe haber sido objeto de una condena definitiva (véase infra "3o. Condiciones de honorabilidad, normas éticas");
- Todo el personal y el gerente de la instalación deben tener las cualificaciones profesionales mínimas requeridas;
- las instalaciones técnicas y los vehículos que transporten organismos deben cumplir lo dispuesto en los artículos R. 2223-67 a D. 2223-121 del Código General de Autoridades Locales;
- el gerente debe estar al día con respecto a los impuestos y las cotizaciones a la seguridad social.

*Para ir más allá*: Artículo L. 2223-23 del Código General de Autoridades Locales.

#### Costos asociados con la calificación

Se paga la capacitación que conduce al diploma de consejero funerario. Para más información, es aconsejable acercarse a las agencias que lo dispensan.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o una parte en el Espacio Económico Europeo podrá trabajar en Francia como consejero funerario, de forma temporal y ocasional si cumple una de las siguientes condiciones:

- Estar legalmente establecidos en ese Estado para llevar a cabo la misma actividad;
- han estado en esta actividad durante un año en los últimos diez años antes de la entrega, cuando ni la formación ni la actividad están reguladas en ese estado;
- han recibido una[Despeje](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000006390299&dateTexte=&categorieLien=cid) por la prefectura competente.

*Para ir más allá*: Artículos L. 2223-23 y L. 2223-47 del Código General de Gobierno Local.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

El nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si justifica:

- Tres años consecutivos de experiencia profesional
- un certificado de competencia expedido por una autoridad competente cuando la profesión esté regulada en ese Estado;
- tiempo completo o a tiempo parcial, durante un año en los últimos diez años, cuando el Estado no regula la profesión o la formación.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar al prefecto territorialmente competente el reconocimiento de sus cualificaciones profesionales (véase infra "4o. a. Solicitar el reconocimiento de las cualificaciones profesionales del nacional para un ejercicio permanente (LE)).

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales entre la cualificación del profesional y la necesaria para ejercer en Francia, el interesado podrá ser sometido a una prueba de aptitud o a una pasantía adaptación (véase infra "4.00. a. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos L. 2223-48 y L. 2223-50 del Código General de Gobierno Local.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

**Integridad**

Para dirigir una funeraria, el profesional no debe haber sido sometido a:

- una sentencia de prisión final con o sin sentencia condicional, registrada en el boletín 2 de su historial penal o una condena dictada por un tribunal extranjero por uno de los siguientes delitos o delitos menores:- ilegalmente llevando a cabo una actividad profesional o social regulada,
  - corrupción o tráfico de influencias,
  - intimidación de una persona en un servicio público,
  - Estafa
  - violación de la confianza,
  - violación de entierro o violación de respeto debido a los muertos,
  - robo o recepción,
  - agresión moral o agresión sexual,
  - Agresiones dolosas;
- bancarrota personal.

*Para ir más allá*: Artículo L. 2223-24 del Código General de Autoridades Locales.

**Reglas profesionales**

El profesional está obligado por las regulaciones funerarias nacionales.

Como tal, debe incluir:

- garantizar el cumplimiento de las disposiciones de información familiar, incluidas las menciones obligatorias de las cotizaciones y órdenes de compra emitidas, según lo establecido en los artículos R. 2223-24 a R. 2223-32-1 del Código General de Autoridades Locales (información relacionados con el profesional y la naturaleza de los servicios ofrecidos);
- proponer y cumplir con los requisitos de financiamiento funerario (ver Sección R. 2223-33 del Código General de Autoridades Locales).

*Para ir más allá*: Artículo R. 2223-23-5 y siguientes del Código General de Autoridades Locales.

4°. Seguros y sanciones
--------------------------------------------

Un gerente de establecimientos funerarios, que opera sin obtener una autorización, se castiga con una multa de 75.000 euros.

El profesional incurre en la misma sanción, si ofrece servicios en previsión de un funeral o dentro de los dos meses de la muerte, con el fin de obtener el pedido de suministros o beneficios relacionados con una muerte. También está prohibido el control de la vía pública para obtener los mismos beneficios.

Además, una pena de cinco años de prisión y una multa de 75.000 euros se castigan ofreciendo cualquier beneficio directa o indirectamente (ofertas, regalos, promesas, etc.) a personas con conocimiento de una muerte en la oportunidad de obtener beneficios relacionados con la muerte o de recomendar los servicios de un profesional.

*Para ir más allá*: Artículos L. 2223-33 y L. 2223-35 del Código General de Gobierno Local.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de reconocimiento de cualificaciones profesionales para los nacionales de la UE para el ejercicio permanente (LE)

**Autoridad competente**

El prefecto del lugar de instalación del nacional es competente para decidir sobre la solicitud de reconocimiento de cualificaciones profesionales.

**Documentos de apoyo**

La solicitud se realiza presentando un archivo con los siguientes documentos justificativos:

- Una identificación válida
- Un certificado de competencia o certificado de formación expedido por una autoridad competente;
- cualquier prueba, en su caso, de que el nacional haya trabajado como consejero funerario durante un año a tiempo completo o a tiempo parcial en los últimos diez años en un Estado miembro que no regule la profesión.

**Procedimiento**

El prefecto confirmará la recepción del expediente completo en el plazo de un mes e informará al nacional en caso de que falten documentos. A partir de entonces, dispondrá de cuatro meses para decidir sobre la solicitud de reconocimiento o someter al nacional a una medida de compensación.

**Bueno saber: medidas de compensación**

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a una medida de compensación, que puede ser:

- Un curso de adaptación
- una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

*Para ir más allá*: Artículo R. 2223-133 y siguientes del Código General de Autoridades Locales; orden de 25 de agosto de 2009 por la que se aplican medidas compensatorias y de verificación de conocimientos para el reconocimiento de cualificaciones profesionales en el sector funerario.

### b. Solicitud de autorización

**Autoridad competente**

El profesional debe dirigir su solicitud al prefecto del departamento en el que tiene su sede la empresa, o por el prefecto de policía de la ciudad de París.

**Documentos de apoyo**

Su solicitud debe incluir:

- una declaración en la que se menciona el nombre de la institución, su forma jurídica, su actividad, su sede, así como el estado civil y la calidad de su gerente. Si es necesario, un extracto de su registro en el registro de operaciones y empresas (RCS) o el directorio de operaciones;
- Una lista de actividades para las que se solicita autorización;
- Prueba de que la institución está cumpliendo con las obligaciones fiscales y de seguridad social requeridas;
- Prueba de la capacidad, ejercicio y formación profesional del gerente y de todo el personal;
- El estado actualizado del personal empleado en la instalación
- dependiendo del caso:- un certificado de conformidad del vehículo para las actividades de transporte de carrocerías antes de que se establezca la cerveza,
  - para la gestión y el uso de una cámara funeraria, un certificado de cumplimiento de la junta funeraria,
  - una copia del diploma thanatopracteur para el personal de cuidados de conservación,
  - en caso de actividad relacionada con la gestión de un crematorio, un certificado de cumplimiento del crematorio.

**Procedimiento**

Una vez finalizada la solicitud, la autorización se concede por un período de seis años, en todo el territorio y se publica en la colección de las obras de la prefectura. Sin embargo, cuando el profesional no justifique al menos dos años de experiencia profesional en la actividad para la que se solicita la autorización, este plazo se limita a un año.

**Tenga en cuenta que**

La autorización de la institución puede suspenderse hasta por un año o retirarse si:

- El profesional ya no cumple con las condiciones necesarias para su entrega;
- La actividad para la que se emitió ya no se lleva a cabo dentro de la institución;
- institución o sus actividades están infringiendo el orden público.

*Para ir más allá*: Artículos L. 2223-25 y R. 2223-56 y siguientes del Código General de Autoridades Locales.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

