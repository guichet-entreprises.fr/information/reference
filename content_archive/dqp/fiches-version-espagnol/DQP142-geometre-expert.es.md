﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP142" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Construcción" -->
<!-- var(title)="Geometros-experto" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="construccion" -->
<!-- var(title-short)="geometros-experto" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/construccion/geometros-experto.html" -->
<!-- var(last-update)="2020-04-15 17:20:47" -->
<!-- var(url-name)="geometros-experto" -->
<!-- var(translation)="Auto" -->


Geometros-experto
=================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:47<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El topógrafo-experto es un profesional cuya misión es:

- llevar a cabo estudios topográficos y trabajar estableciendo los límites de la tierra en el uso de la tierra de las misiones de planificación del uso de la tierra;
- realizar cualquier operación técnica o estudios sobre la evaluación, gestión o gestión de los activos de la tierra.

*Para ir más allá*: Artículo 1 de la Ley 46-942, de 7 de mayo de 1946, por la que se establece la Orden de los Topógrafos Expertos.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ejercer como agrimensor experto, la persona debe estar registrada por orden de topógrafos-expertos.

Como tal, debe:

- 25 años por inscripción
- Ser titular:- o el diploma de experto sapodon-atonio de tierras expedido por el Ministro de Educación Nacional,
  - o un título de ingeniería topógrafo de una escuela de ingeniería;
- no ser o haber sido afectado por la bancarrota personal, reparación o liquidación;
- no haber sido condenado o desestimado penalmente por actos contrarios a la probidad, el honor o las normas aplicables a la profesión de topógrafo-experto;
- no estar sujeto a una prohibición temporal o permanente de ejercer como agrimensor experto.

El nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Espacio Económico Europeo (EEE) debe cumplir las siguientes condiciones:

- Tener las habilidades de idiomas necesarias para ejercer la profesión en Francia;
- No haber sido objeto de una sanción de la misma naturaleza que las mencionadas anteriormente;
- han sido objeto de un reconocimiento de cualificación (véase infra "5 grados). a. Solicitud de pre-informe para un ejercicio temporal o casual (LPS)).

*Para ir más allá*: Artículo 16 del Decreto 96-478, de 31 de mayo de 1996, relativo a la regulación de la profesión de topógrafo-experto y código de funciones profesionales.

#### Entrenamiento

Hay tres formas de adquirir el título de topógrafo-experto:

- un título de ingeniería de topógrafo;
- El diploma de experto en agrimensores de tierras del gobierno;
- El proceso de validación de la experiencia (VAE). Para más información, es aconsejable consultar el[Sitio web oficial de VAE](http://www.vae.gouv.fr/).

Además, los futuros topógrafos deben completar una pasantía obligatoria de dos años y escribir una tesis.

**Diploma de ingeniero topógrafo (nivel bac 5)**

La principal vía hacia la profesión de topógrafo-experto es el máster en ingeniería topógrafo (bac 5) en una escuela de educación superior, seguido de dos años de prácticas profesionales.

Tres escuelas en Francia ofrecen formación como ingenieros topógrafos:

- ESGT (Escuela Superior de Topógrafos y Topógrafos) en Le Mans;
- ESTP (Escuela Especial de Obras Públicas) en París;
- INSA (Instituto Nacional de Ciencias Aplicadas) en Estrasburgo.

Los titulares de un certificado de técnico superior (BTS) después de dos años de clase preparatoria pueden acceder a este curso, y los titulares de un título profesional "técnico topógrafo".

La formación como ingeniero topógrafo tiene una duración de tres años e incluye varias pasantías profesionales. Una primera pasantía en el extranjero (cuatro a doce semanas), una segunda pasantía durante el tercer año de formación (de cuatro a seis meses) en una empresa y durante la cual el ingeniero estudiantil llevará a cabo una obra de finalización (TFE) concluida por la defensa un libro de memorias.

Al final de esta formación, el ingeniero topógrafo también tendrá que completar una pasantía profesional de dos años en una empresa de topógrafos-expertos.

**Diploma de tierra experto en agrimensores emitido por el gobierno (DPLG)**

Este diploma está disponible para los candidatos que han realizado una pasantía profesional, validado las unidades de formación requeridas y cumple con los requisitos de la tesis.

Además de la pasantía, el candidato debe completar las siguientes unidades de capacitación:

- ejercicio de la profesión de topógrafo-experto y delegación de servicio público (dieciséis días de formación);
- derecho (dieciséis días de entrenamiento);
- ciencias de la medición y la geomática (dieciséis días de formación);
- planificación del uso de la tierra (ocho días de capacitación);
- desarrollo de la propiedad (ocho días de formación).

Además, durante los tres años siguientes a la validación de la formación y la validación de su pasantía profesional, el candidato debe apoyar, ante un jurado, una disertación sobre el ejercicio de la profesión de topógrafo-experto y dirigida a evaluar sus habilidades y la calidad de su trabajo.

Al final de estas pruebas, el diploma de experto agrimensor de tierras de DPLG se otorga al candidato que:

- apoyó con éxito su breve;
- tiene el certificado de validación de las unidades de formación prescritas, así como el certificado de finalización de la pasantía expedido por el consejo regional de la Orden de los Topógrafos-expertos;
- justifica los siguientes grados:- máster en las siguientes especialidades: ciencias de la ingeniería, urbanismo, arquitectura y comercio paisajístico, geomática, topografía,
  - un título general de ingeniería,
  - un título de fin de estudio del Instituto Topométrico del Conservatorio Nacional de Artes y Oficios (CNAM),
  - una licenciatura y cinco años de práctica profesional,
  - un topógrafo-topógrafo de BTS y seis años de práctica profesional,
  - un título de bachillerato y ocho años de práctica profesional;
- ha estado trabajando como agrimensor experto durante al menos 15 años;
- si es así, es capaz de presentar documentos que acrediten la práctica profesional establecida por los sucesivos empleadores.

El experto certificado puede referirse a su nombre como "DPLG Geometer-Expert.

*Para ir más allá* : decreto de 8 de diciembre de 2015 relativo al diploma de tierra topógrafo-experto emitido por el gobierno.

**Prácticas de dos años**

Para ser inscrito en la Orden de los Expertos Agrimensores, una pasantía de trabajo profesional debe ser seguida por el futuro topógrafo-experto, bajo la supervisión y responsabilidad de un miembro de la Orden o una administración aprobada por Ministro de Educación Superior y Arquitectura.

La solicitud de prácticas se realiza por carta certificada al presidente del consejo regional de la región de la que depende la oficina principal del aprendiz. Una vez recibida la solicitud, el presidente del consejo regional indicará los términos y documentos necesarios para el proceso de registro.

Durante la pasantía, el aprendiz será evaluado y, como tal, deberá presentar un informe de pasantías al consejo regional del Colegio cada año.

Al final de la pasantía, el consejo regional del Colegio emitirá un aviso y emitirá al candidato con un certificado de finalización de la pasantía.

Durante la pasantía, el aprendiz debe completar módulos de formación de un total de dieciséis días sobre los siguientes temas:

- La ética profesional y la ética;
- límite;
- Propiedad pública
- servidumbres;
- Divisiones de tierras;
- Condominio;
- Experiencia/mediación
- Uso sostenible de la tierra
- Depósito unificado de tierras
- gestión/contabilidad aplicada a la profesión.

**Es bueno saber**

Para el sector DPLG, estos cursos se combinan con los cursos antes mencionados.

Algunos candidatos pueden, si solicitan, beneficiarse de una reducción de la duración de las prácticas de hasta un año si justifican quince años de experiencia profesional, incluidos al menos cinco años en puestos directivos.

*Para ir más allá*: Artículo 6 del Decreto No 2010-1406, de 12 de noviembre de 2010, sobre el diploma de experto sapodon-apoder de tierras del gobierno.

#### Costos asociados con la calificación

Se paga la formación que conduce al diploma de ingeniero topógrafo y su costo varía según el curso profesional previsto. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la UE o del EEE que esté establecido y practique legalmente la actividad de topógrafo-experto podrá llevar a cabo la misma actividad en Francia de forma temporal y ocasional, sin estar inscrito en la Orden de los Topógrafos Expertos.

Deberá solicitarlo al consejo regional en cuyo caso la prestación deberá realizarse con antelación a la prestación de servicios (véase infra "5o). a. Solicitud de pre-informe para un ejercicio temporal o casual (LPS)).

El candidato también debe tener las habilidades de idiomas necesarias para ejercer la profesión en Francia.

*Para ir más allá*: Artículo 2-1 de la Ley de 7 de mayo de 1946; Artículo 39 del Decreto 96-478, de 31 de mayo de 1996, relativo a la regulación de la profesión de topógrafo-experto y código de funciones profesionales.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Para ejercer permanentemente en territorio francés, el nacional de un Estado miembro de la UE o del EEE debe estar en posesión de:

- ya sea un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado que regula el ejercicio de la profesión;
- o un certificado que indique que ha estado en el negocio durante un año en los últimos diez años antes de la solicitud en un estado que no regula la profesión.

Una vez que cumpla una de estas condiciones, podrá solicitar el reconocimiento de cualificación del Ministro responsable de la planificación urbana (véase infra "5o. b. Solicitud de reconocimiento de la cualificación profesional del nacional para un ejercicio permanente (LE)).

**Tenga en cuenta que**

Un topógrafo experto que haya solicitado el reconocimiento también puede solicitar su inclusión en el Orden de los Topógrafos Expertos (véase a continuación "4". Inscripción a la Orden de Topógrafos-Expertos y Seguros").

*Para ir más allá*: Artículo 7-1 del Decreto 31 de mayo de 1996.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Reglas éticas y éticas

El agrimensor-experto está sujeto, a lo largo de su desempeño, al cumplimiento de las normas disciplinarias. El Consejo Regional de la Orden en el que se registrará garantiza el cumplimiento de las siguientes normas:

- garantía de independencia en cada una de sus misiones. Cabe señalar que la pertenencia a la Orden es incompatible con una oficina de funcionarios públicos o ministeriales;
- la obligación de asesorar y ser transparente a sus clientes;
- la obligación hasta la fecha y firmar todos los planes y documentos elaborados, así como de sellarlo;
- El deber del recuerdo
- Secreto profesional
- posible uso de la publicidad únicamente con fines informativos.

**Qué saber**

Cualquier publicidad del topógrafo-experto se presentará al consejo regional de la Orden antes de su emisión.

*Para ir más allá*: Artículo 8 de la Ley de 7 de mayo de 1946.

### b. Sanciones disciplinarias

En caso de sanción, el profesional inmobiliario puede incurrir en una sanción disciplinaria, a saber:

- Una advertencia
- Culpar
- Una suspensión de hasta un año
- una eliminación de la pasantía o mesa de la universidad, lo que implica una prohibición de ejercer la profesión.

*Para ir más allá*: Artículos 23, 23-1 y 24 de la Ley del 7 de mayo de 1946.

### c. Sanciones penales

Cualquier persona que practique como agrimensor experto sin cumplir sus condiciones puede ser procesada por el ejercicio ilegal de la profesión y castigada con penas por el delito de usurpación (véase el artículo 433-17 del Código Penal).

La violación del secreto profesional también se castiga con un año de prisión y una multa de 15.000 euros

*Para ir más allá*: Artículos 226-13 y 226-14 del Código Penal.

4°. Inscripción para el Colegio de Expertos Agrimensores y Seguros
---------------------------------------------------------------------------------------

### a. Solicitud de registro en el Colegio de Topógrafos-Expertos

La inscripción a la Orden de los Topógrafos Expertos es obligatoria para llevar a cabo la actividad legalmente en territorio francés.

**Autoridad competente**

El topógrafo-experto debe enviar una solicitud al presidente del consejo regional de la cabalgata en la que desee presentar una solicitud, que incluya:

- Un formulario de información proporcionado por el consejo regional en tres copias;
- Una copia de un diploma que emita el título de topógrafo-experto o una decisión ministerial que reconozca la calificación o, en su defecto, el acuse de recibo del expediente completo de la solicitud de reconocimiento por el Ministro responsable de la planificación;
- tres fotografías de identidad;
- Un certificado de pago de la tasa de reclamación;
- Una tarjeta de registro civil válida y una identificación;
- un extracto del expediente penal o un documento equivalente;
- para el nacional, un título en francés o un diploma profundo en francés, o, si es necesario, un certificado de posesión de las competencias del idioma en el ejercicio de la profesión en Francia establecido por el topógrafo-experto solicitante ha completado una pasantía.

Una vez recibida la solicitud en su totalidad, el presidente del consejo regional reconoce la solicitud en el plazo de un mes.

**Costos**

El importe de la compensación por los gastos de archivo es fijado cada año por el Consejo Superior tras la aprobación del Comisario del Gobierno (como indicación, fue de 200 euros en 2017).

**Remedio**

El solicitante podrá presentar un recurso ante el Consejo Superior de la Orden en el plazo de dos meses a partir de la notificación de la decisión, que tendrá cuatro meses para decidir.

Una vez que el topógrafo-experto está registrado ante el consejo regional y recibe un número de registro a la Orden emitida por el Consejo Superior, así como una tarjeta profesional.

*Para ir más allá* : orden de 12 de noviembre de 2009 relativo a la inclusión en el consejo de la Orden de los Topógrafos-Expertos.

### b. Seguros

Como profesional, el topógrafo-experto debe, en la apertura de cualquier sitio de construcción, tomar un seguro de responsabilidad civil profesional. Si ejerce como empleado, corresponde al empleador obtener dicho seguro para sus empleados por los actos realizados durante esta actividad.

*Para ir más allá*: Artículos L. 241-1 y siguientes del Código de Seguros.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de pre-informe para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El Consejo de la Orden de los Topógrafos es responsable de decidir sobre la solicitud de declaración previa.

**Documentos de apoyo**

La aplicación es un archivo con los siguientes documentos justificativos:

- un certificado de seguro de responsabilidad civil profesional;
- un certificado que certifique que el agrimensor experto está legalmente establecido en un Estado miembro de la UE o del EEE para ejercer su profesión;
- prueba por cualquier medio de que el nacional ha participado en esta actividad, a tiempo completo o a tiempo parcial, en los últimos diez años, cuando ni la actividad profesional ni la formación están reguladas en la UE o en el Estado del EEE.

*Para ir más allá*: Artículo 2-1 de la Ley de 7 de mayo de 1946; Artículos 39 y siguientes del decreto de 31 de mayo de 1996.

### b. Solicitud de reconocimiento de la cualificación profesional del nacional para un ejercicio permanente (LE)

**Autoridad competente**

El Ministro de Planificación es responsable de decidir sobre la solicitud de reconocimiento de la cualificación del nacional.

**Documentos de apoyo**

La solicitud debe enviarse por duplicado e incluir:

- Una tarjeta de registro civil y una identificación de menos de tres meses de edad;
- Una copia de los diplomas, certificados o títulos que acrediten la formación recibida, así como una descripción del contenido de los estudios realizados, las prácticas realizadas y el número correspondiente de horas;
- un certificado de competencia o un documento de formación, o cualquier otro documento que justifique que el topógrafo-experto haya ejercido durante un año en los últimos diez años en un Estado miembro de la UE o del EEE.

**hora**

El Ministro responsable de urbanismo dispondrá de tres meses a partir de la recepción del expediente completo para decidir sobre la solicitud. Puede decidir reconocer las cualificaciones profesionales del nacional y, en este caso, permitirle llevar a cabo la actividad de topógrafo-experto en Francia, o someterla a una medida de compensación.

**Tenga en cuenta que**

La falta de respuesta en un plazo de tres meses equivale al rechazo de la solicitud.

**Bueno saber: medidas de compensación**

El Ministro de Planificación podrá decidir que el nacional completará un curso de ajuste por un máximo de tres años o que se someterá a una prueba de aptitud. Este último consiste en la presentación y discusión de un expediente sobre la experiencia profesional y los conocimientos del candidato para el ejercicio de la profesión. El expediente se entrega al interesado antes de que se presente a un jurado, que luego dará a conocer los resultados de la prueba al Ministro responsable de la planificación urbana.

Una vez calificado, el solicitante podrá solicitar el registro en la Orden de los Topógrafos Expertos (véase supra "4o. a. Solicitud de registro ante la Orden de los Topógrafos-Expertos").

*Para ir más allá*: Artículo 2-1 de la Ley de 7 de mayo de 1946; Artículos 7 y siguientes del decreto de 31 de mayo de 1996.

### c. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

