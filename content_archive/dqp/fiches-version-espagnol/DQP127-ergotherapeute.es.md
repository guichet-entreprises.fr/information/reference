﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP127" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Terapeuta ocupacional" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="terapeuta-ocupacional" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/terapeuta-ocupacional.html" -->
<!-- var(last-update)="2020-04-15 17:21:29" -->
<!-- var(url-name)="terapeuta-ocupacional" -->
<!-- var(translation)="Auto" -->


Terapeuta ocupacional
=====================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:29<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El terapeuta ocupacional es un profesional de la salud cuya actividad consiste, a través de actividades y aprendizaje, de permitir a las personas con discapacidad adaptar su estilo de vida para adquirir o recuperar su independencia después de una lesión, enfermedad o cualquier impedimento físico.

Tiene en cuenta los problemas físicos a los que se enfrenta su paciente, pero también los factores psicosociales y ambientales relacionados.

*Para ir más allá*: Artículo L. 4331-1 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ejercer la profesión de terapeuta ocupacional, el profesional debe:

- Poseer un diploma estatal como terapeuta ocupacional
- para registrar su diploma (ver infra "5 grados). a. Obligación de registrar su diploma").

**Tenga en cuenta que**

Profesionales que:

- trabajó como enfermera en un trabajo de terapeuta ocupacional antes del 11 de abril de 1983 en un centro de salud pública para personas con trastornos mentales;
- justifican una experiencia laboral de tres años como terapeuta ocupacional, en los diez años anteriores al 23 de noviembre de 1986, y después de haber completado una verificación de conocimientos dentro de los tres años posteriores a esa fecha.

*Para ir más allá*: Artículos L. 4331-2, L. 4331-5 y L. 4333-1 del Código de Salud Pública.

#### Entrenamiento

**Terapeuta ocupacional del Diploma Estatal (DE)**

Este curso de tres años consta de:

- 2000 horas de formación teórica, en forma de conferencias (794 horas) y trabajo dirigido (1206 horas);
- formación clínica y situacional (1260 horas).

Los términos y contenidos de la capacitación se establecen en el[Apéndice III](http://solidarites-sante.gouv.fr/fichiers/bo/2010/10-07/ste_20100007_0001_p000.pdf) 5 de julio de 2010.

El ed de terapeuta ocupacional es emitido por el prefecto regional a los candidatos que han completado con éxito el examen al final de la capacitación.

Una vez obtenido el diploma, el profesional está obligado a registrarse en la Agencia Regional de Salud (ARS) (ver infra "5 grados). a. Obligación de registrar el diploma").

*Para ir más allá*: Artículo D. 4331-2 y posteriores; 5 de julio de 2010 por el diploma del terapeuta ocupacional estatal.

#### Costos asociados con la calificación

La formación para adquirir el ED de terapeuta ocupacional se paga y el costo varía dependiendo del curso previsto. Es aconsejable acercarse a las instituciones interesadas para obtener más información.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Todo nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido podrá, de forma temporal y casual, realizar la misma actividad en Francia.

Cuando el Estado no regule el acceso o el ejercicio de la profesión, el nacional deberá justificar haber llevado a cabo esta actividad en uno o varios Estados miembros durante al menos un año en los últimos diez años.

Para ello, el profesional debe, antes de su primera actuación en Francia, hacer una declaración previa con el prefecto de la región en la que desea llevarla a cabo (véase infra "5o. b. Predeclaración del nacional para un ejercicio temporal y ocasional en Francia (LPS)").

El nacional está obligado por las normas profesionales que rigen el ejercicio de la profesión en Francia.

Además, en caso de diferencias sustanciales entre la formación del nacional y la necesaria para el ejercicio de la actividad de terapeuta ocupacional en Francia, que pueda perjudicar la salud pública, el prefecto regional podrá decidir someterlo a una prueba Aptitud.

*Para ir más allá*: Artículo L. 4331-6 del Código de Salud Pública.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Todo nacional de un Estado miembro de la UE, legalmente establecido y que actúe como terapeuta ocupacional, podrá llevar a cabo la misma actividad de forma permanente en Francia.

Para ello, debe ser el titular:

- un certificado de formación expedido por un Estado miembro que regula el acceso a la profesión de terapeuta ocupacional;
- cuando el Estado no regula el acceso o el ejercicio de la profesión, un documento de formación que justifique que el nacional ha recibido formación para llevar a cabo esta actividad y un certificado que certifique que ha participado en esta actividad durante un período de tiempo menos de un año en los últimos diez años;
- un certificado de formación expedido por un tercer Estado pero reconocido por un Estado miembro y que permita la práctica del trabajo de terapeuta ocupacional.

Cuando existan diferencias sustanciales entre la formación recibida por el nacional y la requerida para la profesión de terapeuta ocupacional en Francia, el prefecto regional podrá decidir someterlo a una medida de compensación (véase infra "5 . c. Solicitud de autorización de práctica para el nacional para un ejercicio permanente en Francia (LE) ").

Además, el nacional debe tener las habilidades de idiomas necesarias para ejercer su profesión en Francia.

*Para ir más allá*: Artículos L.4331-4 y L. 4333-1 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

**Privacidad del paciente y confidencialidad profesional**

El terapeuta ocupacional, como profesional de la salud, está obligado a respetar la privacidad de sus pacientes y el secreto profesional. Además, debe informar a sus pacientes de su estado de salud y, en particular, de:

- Los diferentes tratamientos que se ofrecen
- Los riesgos
- posibles alternativas en caso de denegación.

*Para ir más allá*: Artículos L. 1110-4 y L. 1111-2 del Código de Salud Pública.

**Código de ética**

Las normas éticas aplicables a la profesión de terapeuta ocupacional no están codificadas. Sin embargo, los profesionales cumplen con el Código de ética internacional disponible en la federación mundial de terapeutas ocupacionales ([WFOT](http://www.wfot.org/ResourceCentre.aspx#)).

Para obtener más información, puede ver[sitio web de la Asociación Nacional Francesa de Terapeutas Ocupacionales](http://www.anfe.fr) (ANFE).

4°. Seguros y sanciones
--------------------------------------------

**Seguro**

El terapeuta ocupacional, como profesional de la salud, está obligado a tomar un seguro de responsabilidad civil profesional por los riesgos incurridos durante el transcurso de su actividad.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

**Sanciones penales**

La práctica ilegal de la profesión de terapeuta ocupacional, así como el uso del título de terapeuta ocupacional, sin ser calificado profesionalmente (usurpación de títulos) se castiga con un año de prisión y una multa de 15.000 euros.

*Para ir más allá*: Artículo L. 4334-2 del Código de Salud Pública; Artículo 433-17 del Código Penal.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Obligación de registrar su diploma

#### Autoridad competente

El profesional con un terapeuta ocupacional estatal o autorización de ejercicio debe solicitar al LRA para el registro.

#### Documentos de apoyo

Para ello, deberá transmitir la siguiente información, considerada validada y certificada por la organización que haya expedido su diploma, certificado de formación o autorización de ejercicio:

- El estado civil del titular del diploma y todos los datos para identificarlo;
- El nombre y la dirección de la institución que impartió la capacitación;
- título de su formación.

#### Resultado del procedimiento

Después de comprobar las exposiciones, el LRA registra el diploma.

**Tenga en cuenta que**

La inscripción sólo es posible para un departamento, sin embargo, si el profesional desea ejercer en varios departamentos, se le colocará en la lista del departamento en el que se encuentra el lugar principal de su actividad.

#### Costo

Gratis.

*Para ir más allá*: Artículos D. 4333-1 a D. 4333-6-1 del Código de Salud Pública.

### b. Predeclaración del nacional para un ejercicio temporal e informal en Francia (LPS)

#### Autoridad competente

El nacional envía su solicitud al prefecto de la región en la que desea prestar sus servicios.

#### Documentos de apoyo

Su solicitud debe incluir:

- el[formulario de declaración](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000036171877&dateTexte=20180412) adjunto al anexo del decreto del 8 de diciembre de 2017;
- Una copia de su documento de identidad válido y un documento que acredite la nacionalidad del solicitante;
- Una copia de su título de formación que permita el ejercicio de la función de terapeuta ocupacional y, si es necesario, para las enfermeras una copia de su certificado de formación como especialista;
- un certificado de menos de tres meses de edad de la autoridad competente del Estado miembro, que certifique que el profesional no está prohibido ejercer la profesión;
- cuando el Estado miembro no regule el acceso a la profesión o a su ejercicio, cualquier prueba de que haya ejercido en ese Estado durante un año en los últimos diez años;
- cuando el documento de formación haya sido expedido por un tercer Estado y reconocido en un Estado miembro, el reconocimiento de dicho título y una prueba que certifique que ha participado en esta actividad durante tres años, y en caso afirmativo, una copia de dicha declaración y la primera declaración.

#### Tiempo y procedimiento

El prefecto reconoce la recepción de la solicitud en el plazo de un mes e informa al solicitante:

- Que puede comenzar su prestación de servicio;
- No puede comenzar su prestación de servicio;
- que debido a una diferencia sustancial entre su formación y la necesaria para trabajar como terapeuta ocupacional en Francia, debe someterse a una prueba de aptitud para demostrar que tiene los conocimientos y habilidades necesarios para practicar en Francia.

*Para ir más allá* : Orden de 8 de diciembre de 2017 relativa a la declaración previa de prestación de servicios para asesores genéticos, médicos y preparadores de farmacias y farmacias hospitalarias, así como para ocupaciones en el Libro III de Parte IV del Código de Salud Pública.

### c. Solicitud de autorización para ejercer para el nacional un ejercicio permanente en Francia (LE)

#### Autoridad competente

El nacional deberá presentar su solicitud en doble copia por carta recomendada con notificación de recepción, a la secretaría del comité de terapeutas ocupacionales compuesto:

- Director Regional de Juventud, Deportes y Cohesión Social;
- Director General del LRA;
- Un médico
- dos terapeutas ocupacionales, uno de los cuales está practicando en un instituto de formación.

#### Documentos de apoyo

Su aplicación debe incluir lo siguiente, en su caso, con su traducción certificada al francés:

- el[Formulario de solicitud de autorización](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=73C160E5135B9F63FAC92C921782D8AC.tplgfr33s_2idArticle=LEGIARTI000021777495&cidTexte=LEGITEXT000021777492&dateTexte=20180412) Completado y firmado;
- Una fotocopia de su dNI válido
- Una copia de su título de formación que permita el ejercicio de la profesión, si la hubiera, para las enfermeras, una copia del título de formación especializada;
- Si es necesario, una copia de los diplomas adicionales;
- cualquier documentación que justifique la educación continua, la experiencia o la competencia adquirida durante su práctica profesional;
- una declaración de la autoridad competente del Estado miembro en la que se indique que el nacional no está sujeto a ninguna sanción;
- un certificado expedido por el Estado miembro en el que se detalla la formación recibida (volumen por hora e instrucciones seguidas);
- cuando el Estado no regule el acceso a la actividad o a su ejercicio, prueba de que ha estado involucrado en esta actividad durante al menos dos años en los últimos diez años;
- cuando el nacional ha obtenido su designación de formación en un tercer Estado pero reconocido dentro de un Estado miembro, el reconocimiento de su título de formación.

#### Tiempo y procedimiento

El prefecto regional expide la autorización para ejercer previa notificación de la comisión de terapeutas ocupacionales. Reconoce la recepción de la solicitud en el plazo de un mes a partir de la recepción. Sin embargo, vale la pena aceptar la solicitud el silencio guardado por el prefecto más allá de un período de cuatro meses. Sin embargo, también puede prever que el nacional tendrá que someterse a una medida de compensación (véase infra).

**Tenga en cuenta que**

Una vez que el profesional tiene licencia para ejercer, se le requiere registrarse en el ARS (ver supra "5 grados). a. Obligación de registrar su diploma").

*Para ir más allá* : decreto de 20 de enero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización pertinentes para el examen de las solicitudes presentadas para el ejercicio en Francia de las profesiones de asesor genético, enfermero, masajista-kinesiterapeuta, pedicura-podólogo, terapeuta ocupacional, manipulador en electroradiología médica y dietista.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la formación recibida por el profesional y la requerida para la profesión de terapeuta ocupacional en Francia, el prefecto regional podrá decidir someterla a la elección, a una prueba de aptitud un examen escrito, o un curso de ajuste. Este último deberá llevarse a cabo en un centro de salud público o privado acreditado por el ARS y bajo la responsabilidad de un profesional que haya sido terapeuta ocupacional durante al menos tres años.

*Para ir más allá* : decreto de 24 de marzo de 2010 por el que se establece la organización de la prueba de aptitud y el curso de adaptación para la práctica en Francia de las profesiones de consejero genético, masajista, pedicura-podólogo, terapeuta ocupacional, manipulador electrorodia médica y dietista por nacionales de los Estados miembros de la Unión Europea o parte en el acuerdo sobre el Espacio Económico Europeo.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

