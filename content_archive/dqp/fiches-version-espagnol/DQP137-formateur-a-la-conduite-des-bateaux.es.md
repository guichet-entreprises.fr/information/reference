﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP137" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sector marítimo" -->
<!-- var(title)="Entrenador en la gestión de embarcaciones de recreo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sector-maritimo" -->
<!-- var(title-short)="entrenador-en-la-gestion-de-embarcaciones" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sector-maritimo/entrenador-en-la-gestion-de-embarcaciones-de-recreo.html" -->
<!-- var(last-update)="2020-04-15 17:22:16" -->
<!-- var(url-name)="entrenador-en-la-gestion-de-embarcaciones-de-recreo" -->
<!-- var(translation)="Auto" -->


Entrenador en la gestión de embarcaciones de recreo
===================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:16<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El entrenador de barcos de recreo es un profesional encargado de enseñar embarcaciones de recreo motor con una potencia de más de 4,5 kilovatios, en aguas marítimas y/o interiores.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para entrenar el barco de recreo, el profesional debe cumplir los siguientes requisitos:

- no ser objeto de ninguna sentencia penal o correccional, la lista de los cuales se establece en el[Anexo](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DE910C5FCE5C806408F70446FC91C244.tplgfr21s_2?idArticle=LEGIARTI000021871175&cidTexte=LEGITEXT000006056685&dateTexte=20180509) Decreto No 2007-1167, de 2 de agosto de 2007, sobre licencias de conducir y capacitación en lanchas motoras;
- han tenido una licencia para operar lanchas a motor en aguas marítimas e interiores durante al menos tres años, o un título reconocido como equivalente,[Detenido](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024497884) 21 de julio de 2011 sobre la obtención de un certificado de conducción de barco de recreo por equivalencia con un título profesional o cualificación;
- Cumplir con los requisitos de edad de la licencia de embarcación de recreo motor;
- cumplir los requisitos de aptitud establecidos en la Lista VI de la orden de 28 de septiembre de 2007 sobre la licencia de barco de recreo, la acreditación de establecimientos de formación y la expedición de licencias de enseñanza;
- profesionalmente calificados (ver infra "2.2). a. Capacitación").

*Para ir más allá*: Artículo L. 5272-3 del Código de Transportes y artículo 32 del Decreto No 2007-1167, de 2 de agosto de 2007.

#### Entrenamiento

Para ser reconocido como profesionalmente calificado, el profesional debe ser titular:

- un certificado de formación en primeros auxilios;
- Es:- un título o diploma de nivel V mínimo que requiera formación en educación o animación educativa o deportiva. Mientras esta formación no esté relacionada con la navegación, el profesional también debe tomar formación adicional en navegación,
  - cualificación náutica profesional. Dado que esta cualificación no se refiere a la enseñanza o animación de carácter educativo o deportivo, también debe recibir formación adicional en pedagogía;
- un certificado de operador de radiocomunicaciones marinas restringido para el servicio móvil marítimo, un certificado de operador restringido o un certificado de operador general.

Una vez que el profesional cumple con estos requisitos, debe someterse a una formación de evaluación para obtener una licencia de enseñanza (véase a continuación "Formación de evaluación").

*Para ir más allá*: Artículo L. 5272-3 del Código de Transporte.

**Licencia de barco de recreo motorizado**

Con el fin de entrenar la navegación recreativa, el profesional debe poseer la licencia para conducir embarcaciones de recreo que incluya:

- para aguas marítimas:- la opción "costera" para la navegación hasta seis millas de un refugio,
  - La extensión "offshore" para la navegación más allá de seis millas de un refugio;
- aguas interiores:- La opción "aguas interiores" para buques de menos de 20 metros de eslore,
  - la extensión del "gran placer de las aguas interiores" para la navegación en un barco de una longitud de veinte metros o más.

Este permiso es expedido por el prefecto del departamento en el que el departamento que ha solicitado un permiso en su domicilio, candidatos que tienen al menos dieciséis años de edad y que han pasado un examen teórico y práctico en una institución aprobada.

Esta revisión consta de:

- una o dos pruebas teóricas escritas, cuyo programa varía según la opción elegida y se fija a los artículos[1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DE910C5FCE5C806408F70446FC91C244.tplgfr21s_2?idArticle=LEGIARTI000028531760&cidTexte=LEGITEXT000006057103&dateTexte=20180509) Y[2](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DE910C5FCE5C806408F70446FC91C244.tplgfr21s_2?idArticle=LEGIARTI000028531762&cidTexte=LEGITEXT000006057103&dateTexte=20180509) del auto de 28 de septiembre de 2007 supra;
- formación práctica común a ambas opciones. Los objetivos pedagógicos de esta formación se fijan en el[Apéndice I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DE910C5FCE5C806408F70446FC91C244.tplgfr21s_2?idArticle=LEGIARTI000023750270&cidTexte=LEGITEXT000006057103&dateTexte=20180509) 28 de septiembre de 2007.

*Para ir más allá*: Artículos 2 a 21 y apéndice III del decreto de 2 de agosto de 2007 supra.

**Formación en evaluación**

Para obtener una licencia de enseñanza, el profesional que cumpla con las condiciones anteriores debe completar un curso de formación de evaluación en una escuela acreditada para la formación en embarcaciones de recreo.

El programa de esta formación se establece en la Lista III del orden de 28 de septiembre de 2007 mencionado anteriormente y tiene por objeto verificar la capacidad del candidato para:

- Conocer y comprender los fundamentos del proceso de "formación/evaluación"
- Utilice un repositorio de formación para ser retransmitido como un programa;
- Combine cada secuencia de aprendizaje con un dispositivo de evaluación y toma de decisiones;
- asegurarse de que los datos de entrada de entrenamiento se actualizan.

#### Costos asociados con la calificación

El costo de la calificación varía dependiendo de la institución que imparte la capacitación. Es aconsejable acercarse a las instituciones interesadas para obtener más información.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Todo nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE), legalmente establecido y ejerciendo la actividad de formadores en la explotación de embarcaciones de recreo, podrá ejercer en Francia, como temporal y casual, la misma actividad.

Cuando el Estado miembro de la UE o del EEE no regule el acceso a la actividad o a su ejercicio, el interesado deberá justificar haber realizado esta actividad durante al menos un año en los últimos diez años.

Una vez que el nacional cumpla estas condiciones, deberá, antes de su primera prestación de servicios, hacer una declaración previa para la verificación de sus cualificaciones profesionales. Si estas cualificaciones se refieren sólo a una parte de la actividad de formación, podrá beneficiarse del acceso parcial a la actividad (véase a continuación "Declaración previa para la empresa nacional para la entrega gratuita de servicios (LPS)").

*Para ir más allá* sección L. 5272-3 del Código de Transporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Cualquier nacional de la UE o del EEE, legalmente establecido y formativo de la embarcación de recreo, podrá llevar a cabo la misma actividad en Francia de forma permanente.

Cuando el Estado regule el acceso y la práctica de esta profesión, el profesional deberá justificar la celebración de un certificado de competencia o un título para llevar a cabo esta actividad;

Si el Estado no regula el acceso a la profesión o a su ejercicio, el nacional deberá justificar haber ejercido esta actividad durante un año en los últimos diez años, la profesión de formador a la conducta de embarcaciones de recreo y la celebración de un certificado de competencia o un documento de formación que acredite su preparación para el ejercicio de esta profesión.

**Tenga en cuenta que**

También podrá ejercer, el profesor titular de un título que permita el ejercicio de esta profesión, expedido en un tercer país pero reconocido por un Estado miembro, si justifica haber ejercido esta actividad durante al menos tres años en el Estado que aceptó la validez del título.

Una vez que el profesional cumple con estas condiciones, debe solicitar una licencia de enseñanza en las mismas condiciones que el nacional francés (véase infra "5o). a. Solicitar permiso para enseñar").

**Bueno saber: medidas de compensación**

En caso de diferencias sustanciales entre la formación recibida por el nacional y la necesaria para ejercer en Francia, el prefecto podrá decidir someterlo a una prueba de aptitud o a un curso de adaptación.

La prueba de aptitud consiste en la formación teórica y práctica en una de las opciones básicas de la licencia de placer. Al final de estas formaciones, el profesional realiza una entrevista ante un jurado con el fin de evaluar sus cualidades pedagógicas y de expresión.

El curso de adaptación, cuyo período de duración es fijado por el prefecto, debe llevarse a cabo en el seno de una institución acreditada. Durante esta pasantía, el profesional debe asistir al menos a tres sesiones de capacitación completas en una de las opciones básicas de licencia y proporcionar dos como capacitador. Al final de esta pasantía, se le expide un certificado de seguimiento de la pasantía y un informe de pasantía escrito por el instructor de la escuela.

*Para ir más allá*: Artículo 32 del Decreto de 2 de agosto de 2007 mencionado anteriormente; Artículo 8 del auto de 28 de septiembre de 2007 supra.

3°. Reglas profesionales
---------------------------------

El entrenador de embarcaciones de recreo debe asegurarse de que el buque tenga a bordo, armamento y equipos de seguridad adaptados a sus características.

*Para ir más allá*: Artículo D. 4211-4 del Código de Transporte; orden de 11 de abril de 2012 relativa al armamento y equipo de seguridad de los barcos de recreo que navegan o se estacionan en aguas interiores.

4°. Sanciones
----------------------------------

El profesional que entrenaría la lancha motora sin autorización para impartir la validez, se enfrenta a una pena de un año de prisión y una multa de 15.000 euros.

*Para ir más allá*: Artículo L. 5273-3 del Código de Transporte.

5°. Proceso de reconocimiento de cualificación y trámites
-------------------------------------------------------------------

### a. Solicitar permiso para enseñar

**Autoridad competente**

El profesional debe solicitar al prefecto del departamento en el que el departamento que ha solicitado la acreditación de la institución de formación que lo emplea en su sede.

**Documentos de apoyo**

Su solicitud debe incluir:

- Una fotocopia de su dNI
- Una fotografía de identidad
- Una fotocopia de sus diversos títulos de conducción para barcos de recreo y barcos de recreo;
- Una fotocopia del certificado de formación de primeros auxilios o título equivalente;
- Una fotocopia de su título o título
- Si procede y, en su caso, una fotocopia de su certificado restringido como operador de radio marítimo, su certificado de operador restringido o su certificado de operador general;
- Un certificado de seguimiento del curso de formación para la evaluación;
- un certificado médico de menos de seis meses para justificar su aptitud física, cuyo modelo se fija en la Lista VII del auto de 28 de septiembre de 2007.

**Tenga en cuenta que**

La fotocopia del título, diploma o título náutico se sustituye por la fotocopia de los certificados de trabajo de los distintos empleadores, siempre y cuando el profesional haya estado en esta actividad durante al menos tres años durante los cinco Años.

**Resultado del procedimiento**

La autorización de enseñanza emitida por profesionales tiene una validez de cinco años.

*Para ir más allá*: Artículo 13 del Decreto de 2 de agosto de 2007 supra.

### b. Predeclaración de los nacionales de la UE para la libre prestación de servicios (LPS)

**Autoridad competente**

El nacional debe solicitar por cualquier medio al prefecto responsable de la expedición de la autorización de enseñanza (véase más arriba "5.0). a. Solicitar permiso para enseñar").

**Documentos de apoyo**

Su solicitud deberá contener los siguientes documentos, en su caso, con su traducción al francés:

- Un documento de identidad del solicitante y prueba de nacionalidad;
- un certificado que certifique que está legalmente establecido en un Estado miembro de la UE o del EEE para entrenar la navegación a motor y que no está prohibido la práctica;
- prueba de sus cualificaciones profesionales
- cuando el Estado no regule el acceso a la actividad o a su ejercicio, una prueba que certifique que el profesional ha estado involucrado en esta actividad durante al menos un año en los últimos diez años;
- prueba de que el nacional no es objeto de condena por los delitos mencionados en el[Anexo](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DE910C5FCE5C806408F70446FC91C244.tplgfr21s_2?idArticle=LEGIARTI000021871175&cidTexte=LEGITEXT000006056685&dateTexte=20180509) 2 de agosto de 2007 antes mencionado.

**Tiempo y procedimiento**

El prefecto verifica las cualificaciones profesionales del solicitante y, en el plazo máximo de un mes, le informa de su decisión. En ausencia de una respuesta más allá de este tiempo, el profesional puede comenzar su prestación de servicio.

En caso de diferencias sustanciales entre la formación del profesional y la requerida en Francia para llevar a cabo esta actividad, el prefecto podrá decidir, en el plazo de un mes, someterlo a una prueba de aptitud. Esta prueba de aptitud toma la forma de una sesión de formación teórica y práctica realizada ante un jurado sobre una de las opciones básicas del permiso de placer. El candidato debe entrevistar al jurado para evaluar su conocimiento del entorno administrativo de la licencia recreativa.

**Tenga en cuenta que**

Cada año el profesional está obligado a solicitar la renovación de su autorización proporcionando los siguientes documentos:

- El permiso para enseñar expira
- Una fotografía de identidad
- un certificado médico que justifica su condición física.

*Para ir más allá*: Artículo 32 bis del Decreto de 2 de agosto de 2007 y 13.3 del artículo 13 del Decreto de 28 de septiembre de 2007 antes mencionado.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

