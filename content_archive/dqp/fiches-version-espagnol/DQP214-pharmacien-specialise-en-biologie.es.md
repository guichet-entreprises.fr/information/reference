﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP214" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Farmacéutico especializado en biología médica" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="farmaceutico-especializado-en-biologia" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/farmaceutico-especializado-en-biologia-medica.html" -->
<!-- var(last-update)="2020-04-15 17:21:59" -->
<!-- var(url-name)="farmaceutico-especializado-en-biologia-medica" -->
<!-- var(translation)="Auto" -->


Farmacéutico especializado en biología médica
=============================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:59<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El farmacéutico especializado en biología médica es un profesional de la salud cuya actividad consiste en realizar un examen médico para la prevención, diagnóstico y gestión de pacientes con trastornos patológicos.

El farmacéutico especialista (también conocido como biólogo médico) está obligado a llevar a cabo este examen en un laboratorio de biología médica.

*Para ir más allá*: Artículo L. 6211-1 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ejercer como farmacéutico especializado en biología médica, el profesional debe ser:

- un francés, nacional andorrano o nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE);
- titular de un título estatal francés como médico de farmacia o farmacéutico, así como:- una especialidad en biología médica,
  - una titulación en biología médica emitida por el Consejo Nacional de la Orden de Farmacéuticos (ver infra "2o. a. Diploma de Estudios Especializados en Biología Médica (DESBM) ",
  - una autorización de ejercicio individual expedida por el Ministro responsable de la salud, en las condiciones del artículo L. 4111-2 del Código de Salud Pública;
- registrado sin el orden de los farmacéuticos (ver infra "5 grados). a. Inscripción en la lista de farmacéuticos."

Además, algunos profesionales pueden tener su área de especialización reconocida (ver "Reconocimiento de un área de especialización").

*Para ir más allá*: Artículo L. 6213-1 del Código de Salud Pública.

**Reconocimiento de un área especializada**

Como biólogo médico, el profesional que, a partir del 30 de mayo de 2013 (la fecha efectiva de[La ordenanza](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021683301&dateTexte=20180409) 2010-49 de 13 de enero de 2010 sobre biología médica):

- practicó biología médica en un centro de salud durante al menos dos años en los últimos 10 años, o que comenzó a ejercer entre el 13 de enero de 2008 y el 13 de enero de 2010 y practicó esta actividad durante dos años antes del 13 de enero de 2012. Sin embargo, el profesional que ha practicado la biología médica en un campo de especialización sólo puede practicar dentro de este campo;
- ejercido como veterinario, que comenzó una especialización en biología médica antes del 13 de enero de 2010, se adjudicó su especialización el 13 de enero de 2016;
- Se desempeñó como Director o Director Adjunto de un Centro Nacional de Enfermedades Transmisibles y autorizado para ejercer biología médica por el Ministro de Salud;
- fue autorizado por el Ministro responsable de la salud, en un área de especialización de su laboratorio;
- ha servido durante al menos tres años como biólogo médico en una estructura o laboratorio de biología médica.

Si es necesario, el profesional tendrá que solicitar el reconocimiento de su área de especialización (ver infra "5 grados. a. Solicitud de reconocimiento de un área especializada").

Además, el director o subdirector de un centro nacional para el control de las enfermedades transmisibles también tendrá que solicitar una autorización de ejercicio (véase infra "5o. c. Solicitud de autorización para la práctica del director o subdirector de un centro nacional de enfermedades transmisibles").

*Para ir más allá*: Artículos L. 6213-2 y L. 6213-2-1 del Código de Salud Pública.

#### Entrenamiento

Para saber cómo formarse para el título de farmacéutico, es aconsejable consultar el listado " [Farmacéutico](https://www.guichet-qualifications.fr/fr/professions-reglementees/pharmacien/) ».

**Diplomado en Biología Médica (DESBM)**

Para ejercer como biólogo médico, el farmacéutico debe tener el DES "Biología Médica".

Esta formación está disponible para los estudiantes de farmacia que han pasado el concurso de prácticas y permite al futuro especialista adquirir los conocimientos y la práctica necesarios para llevar a cabo sus funciones.

Consta de ocho semestres, de los cuales al menos tres están en prácticas con supervisión universitaria y al menos uno en un lugar de prácticas sin supervisión universitaria.

###### Tenga en cuenta que

Como parte de su proyecto profesional, el futuro biólogo médico puede solicitar una formación transversal especializada (TSF) entre las siguientes:

- bioinformática médica;
- genética y medicina molecular bioclínica;
- hematología bioclínica;
- higiene - prevención de infecciones, resistencia;
- medicina y biología reproductiva - andrología;
- Nutrición aplicada
- Farmacología médica/terapéutica;
- terapia celular/transfusión.

La formación del DES se divide en tres fases: la fase base, la fase de profundización y la fase de consolidación.

**La fase base**

Con una duración de cuatro semestres, permite al candidato adquirir los conocimientos básicos de la especialidad elegida y realizar:

- tres pasantías en un lugar acreditado senior en biología médica y deben abarcar los campos de la bioquímica molecular, la hematología y la bacteriología-virología;
- una pasantía en un lugar con licencia senior en biología médica y en un campo diferente de la pasantía anterior.

**La fase de profundización**

Con una duración de dos semestres, permite al candidato adquirir los conocimientos y habilidades necesarios para practicar la especialidad elegida y completar dos pasantías en un lugar aprobado como director de biología médica y beneficiarse de la acreditación. para esta área.

**La fase de consolidación**

Con una duración de dos semestres, permite al candidato consolidar sus conocimientos y realizar:

- una pasantía de un semestre en un lugar con licencia de alto nivel en biología médica;
- una pasantía de un semestre en un lugar con acreditación funcional en biología médica o una principalmente acreditada en otra especialidad y secundaria en biología médica.

*Para ir más allá* : Decreto de 27 de noviembre de 2017 por el que se modifica el Decreto de 12 de abril de 2017 relativo a la organización del tercer ciclo de estudios médicos y al decreto de 21 de abril de 2017 relativo a los conocimientos, habilidades y modelos de formación de los diplomas de estudio y estableciendo la lista de estos diplomas y las opciones especializadas transversales y la formación de los estudios médicos de postgrado.

#### Costos asociados con la calificación

Se paga la formación que conduce al diploma de farmacéutico y diploma de especialización en biología médica, y el costo varía dependiendo de la institución elegida. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado de la UE o EEE legalmente establecido que practique como farmacéutico especializado en biología médica podrá realizar la misma actividad de forma temporal y ocasional.

Para ello, tendrá que hacer una declaración previa y justificar tener las habilidades linguísticas necesarias para ejercer en Francia.

Para conocer los términos de esta declaración anticipada, es aconsejable referirse al párrafo "5." "Procedimientos de Reconocimiento de Cualificación" de la Tarjeta " [Farmacéutico](https://www.guichet-qualifications.fr/fr/professions-reglementees/pharmacien/) ».

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Todo nacional de un Estado de la UE, legalmente establecido y que actúe como farmacéutico, podrá llevar a cabo la misma actividad de forma permanente.

Como tal, el nacional puede beneficiarse de:

- esquema automático de reconocimiento de graduación (véase "Bien saber: reconocimiento automático del diploma");
- autorización individual para ejercer. Si es necesario, la profesión tendrá que solicitar la autorización (ver infra "5 grados). d. Si es necesario, solicite una autorización individual para ejercer").

###### Es bueno saber

Reconocimiento automático del diploma

Podrá ejercer como farmacéutico, nacional de un Estado miembro de la UE o del EEE, si posee una de las siguientes cualificaciones o diplomas:

- un certificado de formación de farmacéuticoexpedido por un Estado miembro y que figura en el[Anexo](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=529A28DB27A7E812110245674BD5769B.tplgfr34s_1?idArticle=LEGIARTI000027964612&cidTexte=LEGITEXT000006055521&dateTexte=20180416) del decreto de 13 de febrero de 2007 por el que se establece la lista de diplomas, certificados y otros títulos farmacéuticos expedidos por los Estados miembros de la Unión Europea, los Estados Partes en el Acuerdo sobre el Espacio Económico Europeo y la Confederación Suiza a que se refiere el artículo L . 4221-4 (1) del Código de Salud Pública;
- un certificado de formación de farmacéuticoexpedido por un Estado miembro de la UE o del EEE que no esté en esta lista, acompañado de un certificado de formación;
- un certificado de formación de farmacéuticoexpedido por un Estado miembro de la UE o del EEE sancionando la formación que comenzó antes del 13 de febrero de 2007 y acompañado de un certificado de dicho Estado, que certifica que el nacional ha participado en esta actividad durante al menos tres años consecutivos en los cinco años anteriores a la expedición del certificado;
- un título expedido por un Estado de la UE o del EEE, sancionando la formación farmacéutica iniciada antes de una de las fechas mencionadas en el decreto de 13 de febrero de 2007, y permitiendo la práctica jurídica como farmacéutico en dicho Estado. El nacional debe justificar haber pasado tres años en la función hospitalaria en Francia como agregado asociado, profesional asociado, asistente asociado o en la función universitaria como jefe de clínica asociada de la universidades o asistentes asociados de universidades.

*Para ir más allá*: Artículos L. 4221-2, L. 4221-4 y L. 4221-5 del Código de Salud Pública; decreto de 13 de febrero de 2007 por el que se establece la lista de diplomas, certificados y otros títulos farmacéuticos expedidos por los Estados miembros de la Unión Europea, los Estados Partes en el Acuerdo sobre el Espacio Económico Europeo y la Confederación Suiza contemplados en el artículo L. 4221-4 (1) del Código de Salud Pública.

*Para ir más allá*: Artículo L. 6213-1 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Cumplimiento del Código de ética de los farmacéuticos

El farmacéutico especializado en biología médica está sujeto a las normas profesionales aplicables a los farmacéuticos.

Todas las disposiciones del Código de ética de los farmacéuticos están codificadas en las secciones R. 4235-1 a R. 4235-77 del Código de Salud Pública.

Como tal, el profesional debe respetar los principios de dignidad, no discriminación, secreto profesional o independencia.

*Para ir más allá*: Artículo L. 6213-7 del Código de Salud Pública.

### b. Actividades acumulativas

El farmacéutico sólo puede realizar cualquier otra actividad si tal combinación es compatible con los principios de independencia y dignidad profesional que se le imponen.

Un farmacéutico que cumple una función electiva o administrativa también tiene prohibido utilizarlo para aumentar su clientela.

*Para ir más allá*: Artículos R. 4235-4 y R. 4235-23 del Código de Salud Pública.

### c. Obligación para el desarrollo profesional continuo

Los farmacéuticos especializados en biología médica deben participar anualmente en un programa de desarrollo profesional en curso. Este programa tiene como objetivo mantener y actualizar sus conocimientos y habilidades, así como mejorar sus prácticas profesionales.

Como tal, el profesional de la salud (salario o liberal) debe justificar su compromiso con el desarrollo profesional. El programa está en forma de formación (presente, mixta o no presental) en análisis, evaluación y mejora de la práctica y gestión de riesgos. Toda la formación se registra en un documento personal que contiene certificados de formación.

*Para ir más allá*: Artículo R. 4235-11 del Código de Salud Pública; Decreto No 2016-942, de 8 de julio de 2016, relativo a la organización del desarrollo profesional continuo de los profesionales sanitarios.

4°. Seguro
-------------------------------

### a. Obligación de constete de un seguro de responsabilidad civil profesional

Como profesional de la salud, un farmacéutico que proactúe a modo liberal debe disponer de un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

### b. Fondo de Seguro de Vejez de Farmacéuticos (CAVP)

Cualquier farmacéutico inscrito en el Consejo de Administración del Colegio de Farmacéuticos y que actúe en forma liberal (incluso a tiempo parcial e incluso si también está empleado) tiene la obligación de adherirse al CAVP.

El individuo debe informar a la OPC en el plazo de un mes a partir del inicio de su actividad liberal.

*Para ir más allá*: Artículo R. 643-1 del Código de la Seguridad Social; el sitio de la[Cavp](https://www.cavp.fr/).

### c. Obligación de Informes de Seguros Médicos

Una vez en la lista de la Orden, el farmacéutico que interactúe en forma liberal debe declarar su actividad en el Fondo de Seguro Médico Primario (CPAM).

**Términos**

El registro en el CPAM se puede hacer en línea en el sitio web oficial de Medicare.

**Documentos de apoyo**

El solicitante de registro debe proporcionar un archivo completo que incluya:

- Copiar una identificación válida
- un declaración de identidad bancaria profesional (RIB).

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de registro en la Orden Nacional de Farmacéuticos

La inscripción en el consejo de la Orden es obligatoria para ejercer legalmente la actividad de farmacéutico en Francia.

El registro no se aplica:

- inspectores de salud pública de farmacéuticos, inspectores de organismos regionales de salud, inspectores de la Agencia Nacional para la Seguridad de los Medicamentos y Productos Sanitarios;
- farmacéuticos que son funcionarios públicos o el ministerio de salud similar;
- farmacéuticos que son funcionarios públicos o el ministerio similar a cargo de la educación superior, que, además, no se dedican a la actividad farmacéutica;
- farmacéuticos pertenecientes al marco activo del ejército, el servicio de salud marítima y aérea.

**Tenga en cuenta que **

El registro en el consejo de la Orden permite la emisión automática y gratuita de la Tarjeta Profesional de La Salud (CPS). El CPS es un documento electrónico de identidad comercial. Está protegido por un código confidencial y contiene, entre otras cosas, datos de identificación farmacéutico (identidad, ocupación, especialidad). Para obtener más información, se recomienda consultar el sitio web del gobierno de la Agencia Francesa de Salud Digital.

**Autoridad competente**

El artículo R. 4222-1 dispone que la solicitud de registro se dirige, según sea el caso, al presidente del consejo regional de la región en la que el farmacéutico desea ejercer, o al presidente del consejo central de la sección.

**Documentos de apoyo**

El solicitante debe presentar, por cualquier medio, una solicitud completa de registro que incluya:

- dos copias del cuestionario estandarizado con una identificación con foto completa, fechada y firmada, disponible en los consejos departamentales del Colegio o directamente descargable desde el sitio web oficial del Consejo Nacional del Colegio de Farmacéuticos;
- Una fotocopia de un documento de identidad válido o, en su caso, un certificado de nacionalidad expedido por una autoridad competente;
- En su caso, una fotocopia de la tarjeta de residencia familiar de un ciudadano de la UE válido, la tarjeta válida de residente-EC de larga duración o la tarjeta de residente con estatus de refugiado válido;
- En caso afirmativo, una fotocopia de la tarjeta de visita europea válida;
- una copia, acompañada si es necesario por una traducción de un traductor certificado, de los cursos de formación a los que se adjuntan:- cuando el solicitante sea nacional de la UE o del EEE, el certificado o certificado sinvisado (véase más arriba "2. a. Requisitos nacionales"),
  - solicitante recibe un permiso de ejercicio individual (véase supra "2. c. Nacionales de la UE y del EEE: para un ejercicio permanente"), copiando esta autorización,
  - Cuando el solicitante presente un diploma expedido en un Estado extranjero cuya validez se reconozca en territorio francés, la copia de los títulos a los que pueda subordinarse dicho reconocimiento;
- nacionales de un Estado extranjero, un extracto de antecedentes penales o un documento equivalente de menos de tres meses de edad, expedido por una autoridad competente del Estado de origen. Esta parte puede sustituirse, para los nacionales de la UE o del EEE que requieran prueba de moralidad o honorabilidad para el acceso a la actividad médica, por un certificado, de menos de tres meses de edad, de la autoridad competente del Estado. certificando que se cumplen estas condiciones morales o de honor;
- una declaración sobre el honor del solicitante que certifique que ningún procedimiento que pudiera dar lugar a una condena o sanción que pudiera afectar a la lista en la junta está en su contra;
- un certificado de registro o registro expedido por la autoridad con la que el solicitante fue registrado o registrado previamente o, en su defecto, una declaración de honor del solicitante que certifique que nunca fue registrado o registrado o, en su defecto, un certificado de registro o registro en un Estado de la UE o del EEE;
- todas las pruebas de que el solicitante tiene las habilidades de idiomas necesarias para ejercer la profesión;
- un currículum.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**hora**

El Consejo Regional o Central decide sobre la solicitud de registro en un plazo de tres meses a partir de la recepción del expediente completo de solicitud. Si no se responde a una respuesta dentro de este plazo, la solicitud de registro se considera rechazada.

Este período se incrementa a seis meses para los nacionales de terceros países cuando se llevará a cabo una investigación fuera de la Francia metropolitana. A continuación, se notifica al interesado.

Si se concede el registro, la decisión adoptada por la junta se notifica al interesado por carta recomendada, acompañada de su certificado de registro.

La decisión del consejo regional también está sujeta a recurso, en un plazo de 30 días, ante el Consejo Nacional del Colegio de Farmacéuticos. La propia decisión puede ser apelada ante el Consejo de Estado.

**Costo**

La inscripción en el consejo de la Universidad es gratuita, pero crea la obligación de pagar las cuotas ordinales obligatorias, cuyo importe se establece anualmente y que debe pagarse en el primer trimestre del año calendario en curso.

*Para ir más allá*: Artículos L. 4222-1 en L. 4222-8, y R. 4222-1 en R. 4222-4-3 del Código de Salud Pública.

### b. Solicitud de reconocimiento de especialización

**Autoridad competente**

El profesional deberá presentar su solicitud al Centro Nacional de Gestión mediante carta recomendada con notificación de recepción en:

Centro Nacional de Gestión, Departamento de Competencia, Autorizaciones de Ejercicio, Movilidad y Desarrollo Profesional, 21 B, rue Leblanc 75737 Paris Cedex 15

**Documentos de apoyo**

Su solicitud debe incluir los siguientes documentos:

- Se prevé una carta de solicitud motivada que se adenda al área de especialización;
- Una fotocopia de un documento de identidad válido
- Un CV detallado y las publicaciones de cualquier solicitante
- Una copia de todos sus diplomas
- un certificado que justifique que el nacional ha ejercido biología médica durante al menos dos años en los últimos diez años y antes del 13 de enero de 2012;
- para el subdirector de un centro nacional de enfermedades transmisibles, su doctorado en la práctica o universidad, o su título de ingeniería en relación con la biología médica;
- para el farmacéutico que prestaba en un hospital o universidad, un documento que justifica que ha estado practicando bien en una institución de este tipo durante al menos tres años y mencionando su condición.

**Tiempo y procedimiento**

El Centro Nacional de Gestión confirma la recepción de la solicitud en el plazo de un mes a partir de la recepción y la envía a la Comisión Nacional de Biología Médica. Si el solicitante no es respondido en el plazo de cuatro meses a partir de la recepción del expediente completo, se le concede el reconocimiento.

*Para ir más allá*: Artículo R. 6213-2 del Código de Salud Pública; orden de 14 de febrero de 2017 por la que se establece la composición del expediente que se facilitará a la Comisión Nacional de Biología Médica en virtud del artículo L. 6213-12 del Código de Salud Pública y se definen las áreas de especialización mencionadas en el artículo R. 6213-1 del mismo Código.

### c. Solicitud de autorización para ejercer para el director o subdirector de un centro nacional de enfermedades transmisibles

**Autoridad competente**

El profesional debe presentar su solicitud al Centro Nacional de Gestión mediante carta recomendada con notificación de recepción.

**Documentos de apoyo**

Su solicitud debe incluir:

- Una carta de solicitud motivada
- Una fotocopia de un documento de identidad válido
- Un currículum que describe su formación profesional y, si es necesario, todas sus publicaciones;
- Una copia de su solicitud de permiso para desempeñar las funciones de director o subdirector de laboratorio;
- una fotocopia de todos sus diplomas, títulos o certificaciones en el campo de la biología médica.

**Tiempo y procedimiento**

El Centro Nacional de Gestión confirma la recepción de la solicitud en el plazo de un mes a partir de la recepción. La falta de respuesta al Ministro de Salud más allá de cuatro meses es la razón del rechazo de la solicitud de licencia.

*Para ir más allá* : R. 6213-5 a R. 6213-7 del Código de Salud Pública, ordenado el 14 de febrero de 2017 supra; V de la Sección 9 de la Ordenanza No 2010-49 de 13 de enero de 2010 relativa a la biología médica.

### d. Si es necesario, solicitar autorización individual para ejercer

Si el nacional no está en virtud del régimen de reconocimiento automático, debe solicitar una licencia para ejercer.

**Autoridad competente**

La solicitud se dirige en dos copias, por carta recomendada con solicitud de notificación de recepción a la unidad responsable de las comisiones de autorización de ejercicio (CAE) del Centro Nacional de Gestión (NMC).

Debe añadir a su petición:

- el[Forma](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4349F01DDAE283C55604F0C28FE06D3D.tplgfr23s_3?idArticle=LEGIARTI000029734948&cidTexte=LEGITEXT000029734898&dateTexte=20180118) Solicitud de permiso para ejercer la profesión;
- Una fotocopia de un documento de identidad válido
- Una copia del título de formación que permita el ejercicio de la profesión en el estado de obtención, así como, en su caso, una copia del título de formación especializada;
- Si es necesario, una copia de los diplomas adicionales;
- cualquier prueba útil que justifique la formación continua, la experiencia y las habilidades adquiridas durante el ejercicio profesional en un Estado de la UE o del EEE, o en un tercer estado (certificados de funciones, informe de actividad, evaluación operativa, etc. ) ;
- en el contexto de funciones desempeñadas en un Estado distinto de Francia, una declaración de la autoridad competente de dicho Estado, de menos de un año de edad, que acredite la ausencia de sanciones contra el solicitante.

Dependiendo de la situación del solicitante, se requiere documentación adicional de apoyo. Para obtener más información, consulte el sitio web oficial de la[Gnc](http://www.cng.sante.fr/).

**Qué saber**

Los documentos de apoyo deben estar escritos en francés o traducidos por un traductor certificado.

**hora**

El NMC confirma la recepción de la solicitud en el plazo de un mes a partir de la recepción.

El silencio guardado durante un cierto período de tiempo a partir de la recepción del expediente completo merece la decisión de desestimar la solicitud. Este retraso se incrementa a:

- cuatro meses para las solicitudes de nacionales de la UE o del EEE con un título de uno de estos Estados;
- seis meses para las solicitudes de terceros nacionales con un diploma de un Estado de la UE o del EEE;
- un año para otras aplicaciones.

Este plazo podrá prorrogarse por dos meses, mediante decisión de la autoridad ministerial notificada a más tardar un mes antes de la expiración de esta última, en caso de dificultad grave para evaluar la experiencia profesional del candidato.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la formación y la experiencia laboral del nacional y las necesarias para ejercer en Francia, el NMC podrá decidir:

- Sugerir que el solicitante elija entre un curso de ajuste o una prueba de aptitud;
- para imponer un curso de ajuste y/o una prueba de aptitud.

**La prueba de aptitud** tiene por objeto verificar, mediante pruebas escritas u orales o ejercicios prácticos, la capacidad del solicitante para ejercer como farmacéutico en la especialidad de que se trate. Se ocupa de temas que no están cubiertos por la formación o experiencia del solicitante.

**El curso de adaptación** tiene por objeto permitir a las partes interesadas adquirir las competencias necesarias para ejercer la profesión de farmacéutico. Se lleva a cabo bajo la responsabilidad de un farmacéutico y puede ir acompañado de formación teórica adicional opcional. La duración de la pasantía no exceda de tres años. Se puede hacer a tiempo parcial.

*Para ir más allá* : decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización competentes para el examen de las solicitudes presentadas para el ejercicio en Francia de las profesiones de médico, cirujano dental, partera y Farmacéutico.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

