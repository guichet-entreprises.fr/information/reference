﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP183" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Instructor de esquí alpino" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="instructor-de-esqui-alpino" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/instructor-de-esqui-alpino.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="instructor-de-esqui-alpino" -->
<!-- var(translation)="Auto" -->


Instructor de esquí alpino
==========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El instructor nacional de esquí alpino supervisa todo tipo de público en la práctica del esquí alpino y actividades relacionadas (incluido el snowboard) en todas las clases de progresión de esquí alpino.

Se puede ejercitar en la zona segura de las laderas y fuera de las laderas, con la excepción de las zonas glaciares sin marcar y el terreno cuyo uso utiliza técnicas de montañesa.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La actividad de instructor de esquí alpino está sujeta a la aplicación del artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas.

Como profesor de deportes, el instructor de esquí alpino debe poseer un diploma, un título profesional o un certificado de cualificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- registrado en el Registro Nacional de Certificaciones Profesionales (RNCP). Para más información, es aconsejable consultar el[Sitio web de RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Los requisitos para ejercer como instructor de esquí son el Diploma Estatal de Instructor Nacional de Esquí y el Diploma Estatal de Instructor Nacional de Esquí especializado en capacitación. El Diploma Estatal de Montaña de Montañería-Guía también permite a su titular conducir y acompañar a las personas en los viajes de esquí alpino y esquí fuera de pista y enseñar técnicas de esquí. Para obtener más información sobre este último diploma, se recomienda consultar la hoja "Guía de alta montaña".

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos L. 212-1, R. 212-84, A. 212-1 y Apéndice II-1 del Código del Deporte.

**Bueno saber: el entorno específico**

El esquí alpino es una actividad en un entorno específico que implica el cumplimiento de medidas de seguridad específicas. Por lo tanto, sólo las organizaciones bajo la tutela del Ministerio del Deporte pueden formar a futuros profesionales.

*Para ir más allá*: Artículos L. 212-2 y R. 212-7 del Código del Deporte.

#### Entrenamiento

##### Diploma Estatal de Esquí - Instructor Nacional de Esquí Alpino

El diploma estatal de instructor nacional de esquí alpino está registrado en el Nivel III de la[Directorio Nacional de Certificaciones Profesionales (RNCP)](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Este diploma se expide al final de un curso que comprende un ciclo general común a la enseñanza, el entrenamiento y la formación de deportes de montaña y entrenamiento específico para el esquí alpino. Los programas respetan el principio de alternancia basado en la articulación de períodos de aprendizaje en centros de formación y situaciones profesionales bajo tutoría pedagógica.

La capacitación es imparte por la Escuela Nacional de Esquí y Montañismo (ENSA).

La unidad de formación de "prácticas competitivas" y la formación general común a la enseñanza, el coaching y la formación de deportes de montaña se pueden obtener mediante la validación de la experiencia (VAE).

*Para ir más allá*: Artículos D. 212-68 y los siguientes artículos del Código del Deporte; Artículo 27 del decreto de 11 de abril de 2012 relativo a la formación específica del Diploma Estatal de Instructor Nacional de Esquí de Esquí Alpino.

**Prerrogativas**

El Diploma Estatal de Instructor Nacional de Esquí de Esquí Alpino atestigua las habilidades necesarias para la supervisión, animación, enseñanza y formación en seguridad del esquí alpino y sus actividades spin-off. La actividad de entrenamiento y animación implica acompañamiento en la zona de esquí.

Este diploma permite al titular practicar de forma independiente e independiente, con cualquier tipo de equipo de esquí alpino y cualquier tipo de equipo derivado de este equipo, dentro y fuera de las pistas, con la excepción de las zonas glaciares y el terreno sin marcar. cuya asistencia utiliza técnicas de montañeses.

*Para ir más allá*: Artículo 1 del Decreto de 11 de abril de 2012 sobre la formación específica del Diploma Estatal de Instructor Nacional de Esquí alpino.

**Bueno saber: actividades derivadas**

Las actividades derivadas del esquí alpino son:

- gravedad dedeslizamiento o moverse sobre la nieve utilizando engranajes de varias formas para cualquier tipo de audiencia;
- que se practican en entornos de montaña nevado, excluyendo las zonas glaciares sin marcar y la tierra cuyo uso utiliza técnicas de montañesa.

Además de las formas habituales de esquí alpino (esquí, esquí fuera de pista, carreras de esquí, free ride, free style, esquí de estilo libre, esquí, esquí, salto), las actividades spin-off más practicadas son el snowboard en todas sus actividades. telemark, ciclismo de esquí y raquetas de nieve.

*Para ir más allá* : Apéndice VIII del decreto de 11 de abril de 2012 relativo a la formación específica del Diploma Estatal de Instructor Nacional de Esquí de Esquí Alpino.

**Entrenamiento de sensano**

La formación va precedida de una prueba de acceso técnico que consiste en un eslalon. El curso de formación se lleva a cabo en el siguiente orden cronológico:

- ciclo preparatorio, que dura un mínimo de 70 horas durante dos semanas. Un examen al final de este ciclo garantiza que el candidato cumpla con los requisitos preeducativos. El director regional de juventud, deportes y cohesión social expide al candidato admitido en el ciclo preparatorio un folleto de formación que consta de los tres tiempos de formación sucesivos;
- el curso de sensibilización educativa, que dura un mínimo de 25 días. Permite el tema de los contenidos técnicos y pedagógicos y se lleva a cabo en escuelas de esquí y estructuras federales de formación de la Federación Francesa de Esquí (FFS) bajo la autoridad del director de la escuela o el presidente de la estructura federal. sesiones de entrenamiento. Está disponible para los titulares de un folleto de capacitación válido, incluyendo el primer tiempo de capacitación;
- Eurotest: se trata de un evento de performance que valida la capacidad técnica y consiste en un eslalon gigante en el esquí alpino. El éxito de esta prueba abre la puerta a la apertura del segundo tiempo de entrenamiento;
- el ciclo de pregrado, con una duración mínima de 140 horas repartidas en cuatro semanas, que consiste en una unidad de formación sobre los fundamentos de la educación sobre esquí alpino en entornos de montaña nevada;
- El curso de solicitud, que dura un mínimo de 25 días. Se lleva a cabo en una escuela de esquí o en una estructura de entrenamiento federal de la FFS bajo la autoridad del director de la escuela o el presidente de la estructura de entrenamiento federal, después de la validación de un acuerdo de pasantía. Está disponible para los titulares de un folleto de capacitación válido;
- el segundo ciclo, que dura cinco semanas, que consta de tres unidades de entrenamiento (UF):- UF "prácticas competitivas", con una duración mínima de 35 horas repartidas en una semana,
  - UF "dominio técnico y pedagógico de la educación sobre esquí alpino, dominio técnico en la seguridad de las actividades derivadas, incluido el snowboard", con una duración mínima de 70 horas repartidas en dos semanas,
  - UF "profundizar la seguridad en las pistas, fuera de las pistas y el entorno de montaña nevada, incluyendo pruebas de Euroseguridad", con una duración mínima de 70 horas durante dos semanas.

**Condiciones de acceso a la prueba de acceso técnico**

El interesado deberá:

- Tener diecisiete años antes del 31 de diciembre del año natural en el que se lleva a cabo la prueba;
- proporcionar la solicitud de registro en una impresión estandarizada con un documento de identidad con fotografía reciente, una fotocopia del documento nacional de identidad o pasaporte, y tres sobres de pegatinas de 23 x 16 cm con una tasa de en vigor y redactado en el nombre y la dirección del candidato;
- Proporcionar un certificado médico de no contradictorio con la práctica y enseñanza del esquí de menos de un año de edad en la fecha de cierre del primer registro;
- para los candidatos de nacionalidad francesa nacidos a partir de 1979 para hombres y de 1983 para mujeres, proporcionar una fotocopia del certificado censal o el certificado individual de participación en el día de la defensa y la ciudadanía;
- para menores, proporcionar permiso parental o el del tutor legal.

**Condiciones de acceso al ciclo preparatorio**

El interesado deberá:

- Tener al menos 18 años de edad en el primer día de entrenamiento
- Proporcionar la solicitud de registro en una impresión estandarizada con dos fotografías de identidad recientes y una fotocopia del documento nacional de identidad o pasaporte;
- Proporcionar el certificado de éxito en la prueba de acceso técnico de menos de tres años de edad en la fecha de cierre del primer registro;
- Proporcionar una fotocopia de la unidad de enseñanza de Prevención y Socorro Cívico de Nivel 1 (PSC1) o su equivalente;
- proporcionar un certificado médico de no contradictorio con la práctica y enseñanza del esquí alpino de menos de un año de edad en la fecha de cierre del primer registro;
- proporcionar tres sobres de pegatinas de 23 x 16 cm, postenvejecidos a la tarifa actual y etiquetados en el nombre y la dirección del candidato, y un sobre engomado de 21 x 29,7 cm sellado a la tarifa actual para enviar un recomendado con acusado de Recepción.

**Condiciones de postgrado**

El interesado deberá:

- Tener un folleto de capacitación, incluyendo el segundo tiempo de capacitación
- han validado el Eurotest por menos de cinco años o han estado en posesión de un certificado de exención de la prueba de euro expedido durante menos de cinco años;
- haber completado y validado al menos 25 días del curso de concienciación educativa;
- Aprobar el examen del ciclo preparatorio;
- Proporcionar el certificado o certificados de prácticas expedidos por el director técnico de la escuela de esquí;
- han validado el Eurotest por menos de 5 años o han estado en posesión de un certificado de exención expedido por menos de cinco años.

**Condiciones de acceso al 2o ciclo**

El interesado deberá:

- ser certificado para pasar la prueba de la formación general común a las profesiones docentes, de coaching y de formación de los deportes de montaña o al certificado de éxito en las pruebas del examen de la formación general común comercios deportivos de montaña o certificación del éxito en los exámenes de la parte común de la patente del educador deportivo estatal;
- han cumplido con la evaluación de las "prácticas competitivas" de la UF;
- para la UF "dominio técnico y pedagogía de la educación sobre esquí alpino, dominio técnico en la seguridad de las actividades derivadas del snowboard", habiendo satisfecho la evaluación de la UF de las "prácticas competitivas" y habiendo llevado a cabo y validado al menos 25 días de la Prácticas educativas para la aplicación;
- para la UF "profundizar la seguridad en las pistas, fuera de las pistas y el entorno nevado de la montaña, incluyendo la prueba de Euroseguridad", después de haber llevado a cabo al menos seis salidas de esquí fuera de pista o esquí de gira.

*Para ir más allá*: Artículos 7, 9, 13, Apéndice II de la orden del 11 de abril de 2012 sobre la formación específica del Diploma Estatal de Instructor Nacional de Esquí Alpino.

**Reciclaje**

Los titulares del Diploma Estatal de Instructor Nacional de Esquí de Esquí Alpino están sujetos cada seis años a un curso de actualización organizado por la Escuela Nacional de Deportes de Montaña, sede de la ENSA. El reciclaje debe tener lugar antes del 31 de diciembre del 6o año siguiente a la graduación o la última readaptación.

*Para ir más allá*: Artículo 1 del Decreto de 11 de abril de 2012 sobre la formación específica del Diploma Estatal de Instructor Nacional de Esquí alpino.

##### Diploma Estatal de Esquí - Instructor Nacional de Esquí Alpino especializado en formación

El diploma estatal de instructor nacional de esquí alpino especializado en formación está registrado en el nivel II del RNCP.

Este diploma se expide al final de un curso que comprende un ciclo general común a la enseñanza, el entrenamiento y la formación de deportes de montaña y entrenamiento específico para el esquí alpino. Los programas respetan el principio de alternancia basado en la articulación de períodos de aprendizaje en centros de formación y situaciones profesionales bajo tutoría pedagógica.

La formación es imparte por ENSA.

*Para ir más allá*: Artículos D. 212-68 y los siguientes artículos del Código del Deporte.

**Prerrogativas**

El diploma estatal de instructor de esquí alpino especializado en la formación atestigua las habilidades requeridas en el esquí alpino y sus actividades derivadas para la formación, entrenamiento y desarrollo de carreras de esquí. Permite al titular impartir formación a los entrenadores que compiten en ella, así como la gestión y promoción de estructuras de formación.

**Entrenamiento de sensano**

El entrenamiento es de un total de 490 horas, incluyendo 350 horas en ENSA y 140 horas en una recepción. El curso de formación se lleva a cabo en el siguiente orden cronológico:

- UF "optimización del rendimiento en actividades de esquí alpino y spin-off" que dura 245 horas;
- el curso educativo de 140 horas en una situación. Está disponible para los candidatos con un folleto de capacitación válido que ha certificado uno de los módulos de la 1a UF;
- UF "gestiona un proyecto de formación de capacitación competitiva de 500 horas en actividades de esquí alpino y spin-off".

**Condiciones de acceso**

El interesado deberá:

- Proporcionar una copia del Diploma Estatal de Instructor Nacional de Esquí Alpino o el Certificado Estatal de 1o grado de la opción de educador deportivo "esquí alpino";
- Proporcione una copia del diploma de trazador regional del FFS;
- proporcionar un certificado del FFS que certifica una clasificación en puntos FIS de setenta y cinco puntos o menos de setenta y cinco puntos para las mujeres y ochenta puntos para los hombres en descenso, eslalon, gigante o super gigante o un ranking de menos de ochenta y cinco puntos para las mujeres y menos de noventa puntos para los hombres en dos de las cuatro disciplinas mencionadas anteriormente. Los candidatos que no puedan justificar tal clasificación podrán beneficiarse previa solicitud de una exención concedida por el Director General de la ENSA, previa notificación de la sección permanente de esquí alpino de la Comisión de Formación y Empleo del Consejo Superior deportes de montaña;
- Proporcionar un dossier de cinco a diez páginas sobre la experiencia del candidato durante su carrera deportiva y profesional, así como su motivación y proyecto profesional;
- Proporcionar un formulario de registro estandarizado con una fotocopia de un documento de identidad, un documento de identidad con fotografía y dos sobres sellados;
- proporcionar un certificado médico de no contradictorio con la práctica, la enseñanza y la formación del esquí alpino de menos de un año de edad en la fecha de cierre de la inscripción.

#### Costos asociados con la calificación

Se paga la formación que conduce al diploma estatal de instructor nacional de esquí alpino. Su coste es de unos 4.500 euros.

Para más detalles, es aconsejable acercarse a la[Ensa](http://www.ensa.sports.gouv.fr/).

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecido en uno de estos Estados puede llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del Departamento de Isáre.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte; Artículo 29-1 del Decreto de 11 de abril de 2012 sobre la formación específica del Diploma Estatal de Instructor Nacional de Esquí de Esquí Alpino.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

#### Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

#### Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como instructor de esquí alpino en Francia para personas que hayan sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Obligación de presentación de informes (con el fin de obtener la tarjeta de educador deportivo profesional)

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial](https://eaps.sports.gouv.fr).

**hora**

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

**Documentos de apoyo**

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

**Autoridad competente**

La declaración previa de actividad debe dirigirse al prefecto del Departamento de Isáre. El expediente de declaración es remitido por el prefecto a los Comercios Nacionales de Esquí y Montañismo, que lo envía para su asesoramiento a la sección permanente de esquí alpino del Comité de Formación y Empleo del Consejo Superior de Deportes de Montaña.

**hora**

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

**Documentos de apoyo**

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte; Artículos 29-1 y siguientes del auto de 11 de abril de 2012 relativo a la formación específica del Diploma Estatal de Instructor Nacional de Esquí Alpino.

### c. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

**Autoridad competente**

La declaración debe dirigirse al prefecto del Departamento de Isáre. El expediente de declaración es remitido por el prefecto a los Comercios Nacionales de Esquí y Montañismo, que lo envía para su asesoramiento a la sección permanente de esquí alpino del Comité de Formación y Empleo del Consejo Superior de Deportes de Montaña.

**hora**

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

**Documentos de apoyo para la primera declaración de actividad**

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

**Evidencia para una declaración de renovación de la actividad**

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y siguientes, anexos II-12-2a y II-12-2b del Código del Deporte; Artículos 29-1 y siguientes del auto de 11 de abril de 2012 relativo a la formación específica del Diploma Estatal de Instructor Nacional de Esquí Alpino.

### d. Medidas de compensación

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión remite la sección permanente del esquí alpino al Comité de Formación y Empleo del Consejo Superior de Deportes de Montaña después de la revisión e investigación del caso. En el mes de su remisión, emitió un aviso al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-90-1 y R. 212-92 del Código del Deporte; Artículos 29-1 y siguientes del decreto de 11 de abril de 2012 relativo a la formación específica del Diploma Estatal de Instructor Nacional de Esquí Alpino.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

