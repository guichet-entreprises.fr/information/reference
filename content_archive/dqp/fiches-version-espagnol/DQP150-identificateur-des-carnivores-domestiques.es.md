﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP150" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Identificación de carnívoros domésticos" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="identificacion-de-carnivoros-domesticos" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/identificacion-de-carnivoros-domesticos.html" -->
<!-- var(last-update)="2020-04-15 17:20:54" -->
<!-- var(url-name)="identificacion-de-carnivoros-domesticos" -->
<!-- var(translation)="Auto" -->




Identificación de carnívoros domésticos
=======================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:54<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La identificación de carnívoros domésticos, a saber, gatos, perros y hurones, implica marcar, asignando al animal un número de identificación exclusivo y no reutilizable realizado por tatuaje o transpondedor, seguido de registro del animal en el índice nacional de identificación de carnívoros nacionales (ICAD).

La misión del identificador de carnívoro doméstico es marcar al animal y completar el documento de identificación previa para el animal, una copia de la cual se entregará al propietario y otra copia al I-CAD.

Para obtener más información sobre los pasos relacionados con la identificación de carnívoros nacionales,[I-CAD](https://www.i-cad.fr/).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El título de identificador de los carnívoros nacionales está reservado:

- Personas que han recibido una autorización del Ministro de Agricultura;
- veterinarios, que tienen derecho.

*Para ir más allá*: Artículos L. 212-10 y R. 212-65 del Código de Pesca Rural y Marina.

#### Entrenamiento

La identificación de carnívoros domésticos es diferente dependiendo de si es llevado a cabo por un veterinario o un tatuador autorizado.

El veterinario que actúa como identificador de todos los carnívoros domésticos, realiza el tatuaje por dermograma o alicates y por la implantación de un chip electrónico.

El artista calificado del tatuaje sólo puede marcar a los perros de menos de 4 meses de edad por tatuaje de pinza.

##### Veterinaria

Para conocer todo sobre la formación y el acceso a la profesión veterinaria en Francia, es aconsejable consultar la ficha de cualificación prevista para este fin.

##### Tatuadores calificados

El interesado, que desea obtener el título de tatuador cualificado, debe pasar un examen teórico y práctico, cuyos términos y condiciones son establecidos por el Ministerio de Agricultura y son ejecutados por el[Sociedad Central Canina](http://www.scc.asso.fr/La-formation-des-tatoueurs-agrees) (SCC).

Ambos eventos están abiertos a criadores de perros, cazadores, oficiales de la compañía de caza y maestros de la tripulación.

El examen teórico es en forma de un cuestionario de opción múltiple. El candidato podrá asistir a una jornada de formación organizada por la Sociedad Canina Central, antes de la aprobación de la prueba.

Una vez que el candidato es admitido en la prueba teórica, el CCS se preparará para la prueba práctica como parte de un contrato de patrocinio entre el candidato y un artista de tatuajes experimentado, conocido como un "patrocinador".  Este patrocinador supervisará al candidato durante la finalización de al menos 10 tatuajes.

Cuando se considera que el candidato es apto para pasar la prueba práctica, el patrocinador del candidato se pone en contacto con el DD (CS)PP de su departamento, para que la prueba práctica sea validada por un oficial de administración, tras la observación de un tatuaje, llevada a cabo por el Candidato.

Csc remite el expediente del solicitante a la Subdivisión Alimentaria del Departamento de Agricultura.

La autorización del solicitante sólo es efectiva después de que su expediente haya sido validado por la Comisión Nacional de Habilitación.

*Para ir más allá*: Artículo 35 de la orden del 1 de agosto de 2012 sobre la identificación de carnívoros nacionales y por el que se establece cómo aplicar el expediente de identificación de los carnívoros nacionales.

#### Costos asociados con la capacitación

Se paga la formación de identificadores, distintos de los veterinarios. Para más detalles, es aconsejable acercarse a la Sociedad Canina Central.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal o ocasional (prestación gratuita de servicios (LPS))

Un nacional de un Estado miembro de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), que actúa como identificador de los carnívoros nacionales en uno de estos Estados, puede utilizar su título profesional en Francia, como temporal u ocasional, siempre y cuando haga una declaración a la Dirección General de Alimentación del Ministerio de Agricultura (véase infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año, durante los diez años que preceder el rendimiento.

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y a su práctica en Francia, el interesado podrá ser sometido a una prueba de aptitud (cf. infra "5 grados. a. Bueno saber: medida de compensación").

*Para ir más allá*: Artículo L. 204-1 del Código Rural y Pesca Marina.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre (LE))

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si:

- posee un certificado de formación o un certificado de competencia expedido por una autoridad competente de otro Estado miembro que regula el acceso o el ejercicio de la profesión;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Una vez que cumpla una de las dos condiciones anteriores, tendrá que obtener una autorización del Ministro responsable de agricultura (véase infra "5o. b. Obtener una autorización para los nacionales de la UE o del EEE para un ejercicio permanente (LE) ").

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones requeridas para el acceso a la profesión y a su práctica en Francia, el interesado podrá ser sometido a una prueba de aptitud o a una etapa de adaptación (véase infra "5.0). b. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos R. 204-2, R. 204-3 y R. 212-65 del Código de Pesca Rural y Marina.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Los veterinarios que llevan a cabo una misión para identificar a los carnívoros nacionales están sujetos a las[Código de ética aplicable a la profesión](https://www.veterinaire.fr/la-profession/code-de-deontologie.html).

Para los identificadores distintos de los veterinarios, tienen una obligación ética, incluido el respeto de la confidencialidad de los datos de identificación y garantizar que se mantenga la dignidad del animal marcado.

4°. Seguro
-------------------------------

En caso de ejercicio liberal, el identificador de los carnívoros nacionales está obligado a obtener un seguro de responsabilidad civil profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus trabajadores por los actos realizados en ocasiones.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

**Autoridad competente**

La declaración previa de actividad deberá enviarse, antes del primer beneficio, a la Dirección General de Alimentación del Ministerio de Agricultura.

**Renovación de la predeclaración**

Debe renovarse una vez al año y con cada cambio en la situación laboral.

**Documentos de apoyo**

La declaración previa del nacional deberá transmitirse por cualquier medio a la autoridad competente e incluir los siguientes documentos justificativos:

- Prueba de la nacionalidad del profesional
- un certificado que lo certifique:- está legalmente establecido en un Estado de la UE o del EEE,
  - ejerce una o más profesiones cuya práctica en Francia requiere la celebración de un certificado de capacidad,
  - no incurre en una prohibición de ejercer, aunque sea temporal, al expedir el certificado;
- prueba de sus cualificaciones profesionales
- cuando ni la actividad profesional ni la formación estén reguladas en la UE o en el Estado del EEE, prueba por cualquier medio de que el nacional ha estado involucrado en esta actividad durante un año, a tiempo completo o a tiempo parcial, en los últimos diez años;

Esta declaración anticipada incluye información relativa al seguro u otros medios de protección personal o colectiva suscritos por el solicitante de registro para cubrir su responsabilidad profesional.

A estos documentos se adjunta, según sea necesario, su traducción al idioma francés.

**hora**

La Dirección de Alimentos dispone de un mes a partir de la recepción del expediente para tomar su decisión:

- Permitir que el proveedor realice su primer servicio;
- someter a la persona a una medida de compensación en forma de prueba de aptitud, si resulta que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de las requeridas para el ejercicio de la profesión en Francia (véase más adelante: "Bueno saber: medida de compensación");
- informarles de una o más dificultades que puedan retrasar la toma de decisiones. En este caso, la Dirección General de Alimentación tendrá dos meses para decidir, a partir de la resolución de la dificultad o dificultades. En ausencia de una respuesta de la Dirección de Alimentos dentro de estos plazos, la prestación de servicios puede comenzar.

**Qué saber**

La persona que se dedica a la actividad de identificación de carnívoros nacionales, de forma ocasional o temporal, sin haber hecho la declaración previa o mediante la presentación de una declaración incompleta, es responsable de una sanción de la quinta clase siempre y cuando Artículos 131-13 y siguientes del Código Penal.

**Costo**

Gratis.

**Bueno saber: medida de compensación**

Para obtener permiso para ejercer, el interesado puede estar obligado a someterse a una prueba de aptitud si parece que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de las ejercer la profesión en Francia.

La prueba de aptitud cubre todo o parte del examen mencionado en el párrafo "2." a. Capacitación", dependiendo de lo que sea necesario para establecer que se dominan los conocimientos y las cualificaciones pertinentes.

*Para ir más allá*: Artículos R. 204-1 y siguientes del Código Rural y Pesca Marina; Artículo 34 de la orden del 1 de agosto de 2012 sobre la identificación de los carnívoros nacionales y por el que se establecen las modalidades para la aplicación del índice nacional nacional de identificación de los carnívoros.

### b. Obtener una autorización para los nacionales de la UE o del EEE para un ejercicio permanente (LE)

Los nacionales de la UE o del EEE que desempeñan funciones no veterinarias deben obtener una autorización para identificar a los carnívoros nacionales.

**Autoridad competente**

La solicitud de autorización se dirige al Ministro de Agricultura (Dirección General de Alimentación) acompañado de un expediente que incluye:

- Una fotocopia del DNI o pasaporte
- un extracto de antecedentes penales
- Un currículum que detalla sus diversas actividades;
- cualquier indicación sobre su profesión y el marco en el que ejerce.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

En apoyo de su solicitud, el nacional es sometido a un examen teórico y práctico, que necesariamente incluye el tatuaje de un perro. El Director General de Alimentos es responsable de organizar el examen.

**Resultado del procedimiento**

La decisión del Ministro responsable de la agricultura de expedir la autorización se toma tras el asesoramiento de una comisión que comprende:

- El director general de alimentos o su representante;
- el presidente de la Orden Nacional Veterinaria o su representante;
- el presidente de la Asociación de Tatuadores de Francia o su representante;
- el presidente de la Asociación de Tatuadores de Mascotas Registradas o su representante.

La autorización se expide por un año, renovable automáticamente siempre que el nacional realice al menos diez tatuajes por año.

El silencio del Ministro responsable de la agricultura al final de un período de dos meses es la decisión de desestimar la solicitud de autorización.

**Bueno saber: medidas de compensación**

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede tener que someterse a la medida de su elección, a saber:

- un curso de adaptación de hasta tres años
- o una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

**Curso de adaptación**

El Director General de Alimentación establece los términos y condiciones del curso de adaptación en un acuerdo con el pasante y la empresa anfitriona.

La elección de la empresa depende del pasante entre los profesionales propuestopor por el director general de alimentación. El Director General de Alimentación lleva a cabo una evaluación de las competencias adquiridas durante esta pasantía.

**Prueba de aptitud**

La prueba de aptitud cubre todo o parte del examen mencionado en el párrafo "2." a. Capacitación", dependiendo de lo que sea necesario para establecer que se dominan los conocimientos y las cualificaciones pertinentes.

*Para ir más allá*: Artículo R. 204-2 y siguiente del Código Rural y la pesca marina; Artículo R. 212-65 y siguiente del Código Rural y Pesca Marina; Artículo 34 de la orden de 1 de agosto de 2012.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

