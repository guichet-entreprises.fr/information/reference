﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP182" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Instructor de buceo subacuático" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="instructor-de-buceo-subacuatico" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/instructor-de-buceo-subacuatico.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="instructor-de-buceo-subacuatico" -->
<!-- var(translation)="Auto" -->


Instructor de buceo subacuático
===============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
(1) Definición de la actividad
------------------------------

El instructor de buceo submarino dirige y supervisa, de forma independiente, las actividades de aprendizaje, descubrimiento y enseñanza en buceo subacuático en buceo o sin buceador según su especialidad.

El buceo subacuático consiste en explorar el mundo submarino. Cubre las actividades de buceo (un dispositivo individual que permite a un buceador bucear libremente con una reserva de gas transpirable comprimido), sin buceo y senderismo submarino.

El instructor de buceo ayuda a los buceadores a practicar su actividad, en todos los ambientes, naturales o artificiales. Los hace descubrir, proteger y mejorar los ambientes submarinos. Asegura su seguridad, los supervisa y los acompaña en la exploración. También puede introducirlos en otras actividades culturales o deportivas relacionadas con la práctica del buceo subacuático. Participa en la operación del centro de buceo que le emplea, especialmente en las áreas de recepción de clientes, administración del centro, puesta en marcha y mantenimiento de equipos y soportes náuticos. Puede estar involucrado en la operación o gestión de una tienda de buceo asociada a ella.

En cuanto al instructor de buceo sin buceo, interviene en las actividades submarinas de apnea, pesca submarina, natación con aletas, senderismo submarino, natación en aguas bravas, hockey submarino, tiro dirigido y otras actividades sin traje de buceo, dependiendo del soporte técnico para el que esté especializado.

*Para ir más allá* : decreto de 1 de diciembre de 2016 por el que se crea la designación de "buceo subacuático" del certificado profesional de la especialidad de juventud, educación popular y deporte (BPJEPS) "educador deportivo".

2°. Cualificaciones profesionales
-----------------------------------------

### Requisitos nacionales

#### Legislación nacional

La actividad de instructor de buceo subacuático está sujeta a la aplicación del Artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas.

Como profesor de deportes, el instructor de buceo subacuático debe poseer un diploma, un título profesional o un certificado de cualificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

Los diplomas para ejercer la actividad de instructor de buceo subacuático son la especialidad BPJEPS "educador deportivo" mención "buceo subacuático", el diploma estatal de juventud, educación popular y deporte (DEJEPS) especialidad " desarrollo deportivo" mención "buceo subacuático" y el Diploma Estatal de Juventud, Educación Popular y Deporte (DESJEPS) especialidad "rendimiento deportivo" mención "buceo subacuático".

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos A. 212-1 Apéndice II-1, L. 212-1 y R. 212-84 del Código del Deporte.

**Es bueno saber**

El entorno específico.*La práctica del buceo subacuático en el buceo, en todos los lugares, y en la apnea, en el entorno natural y en una fosa de buceo, es una actividad llevada a cabo en un entorno específico. Implica el cumplimiento de medidas especiales de seguridad. Por lo tanto, sólo las organizaciones bajo la tutela del Ministerio del Deporte pueden formar a futuros profesionales.

*Para ir más allá*: Artículos L. 212-2 y R. 212-7 del Código del Deporte.

#### Entrenamiento

##### BPJEPS especialidad "educador deportivo" mención "buceo subacuático"

El BPJEPS es un diploma estatal registrado en el[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) y clasificado en el nivel IV de la nomenclatura de nivel de certificación. Da fe de la adquisición de una cualificación en el ejercicio de una actividad profesional encargada de fines educativos o sociales, en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Hay dos opciones: opción A "en un traje" y opción B "sin traje de buceo."

Este diploma se puede obtener en su totalidad a través de la validación de la experiencia (VAE). Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr). En el contexto de un VAE, el candidato también tendrá que ser puesto en una situación profesional.

*Para ir más allá*: Artículos D. 212-20 y D. 212-24 del Código del Deporte; Artículo 10 del decreto del 1 de diciembre de 2016 por el que se crea la designación de "buceo subacuático" de la especialidad "educador deportivo" BPJEPS.

**Prerrogativas comunes para opciones de "scuba" y "no scuba":**

- Diseñar un proyecto educativo para el público
- organizar y supervisar las prácticas de senderismo subacuático en materia de autonomía;
- Garantizar la seguridad de la práctica en todas las zonas de baño de acceso público utilizadas para actividades subacuáticas y la de los profesionales a su cargo;
- Gestionar equipos de emergencia y comunicación para dar la alarma
- participar en el funcionamiento de la estructura organizativa de las actividades, así como en el uso, mantenimiento y mantenimiento del equipo y las inmersiones de apoyo al buque.

*Para ir más allá*: Artículo 3 del decreto del 1 de diciembre de 2016 por el que se crea la designación de "buceo subacuático" de la especialidad "educador deportivo" BPJEPS.

**Prerrogativas especiales para la opción de "buceo":**

Bajo la autoridad de un titular mínimo de un certificado estatal de opción de educador deportivo "buceo subacuático", una especialidad DEJEPS "desarrollo deportivo" mención "buceo subacuático" o un SPECIALty DESJEPS "rendimiento deportivo" "buceo subacuático", el titular del BPJEPS tiene derecho a:

- llevar a cabo actividades para supervisar y facilitar las actividades de aprendizaje, descubrimiento y enseñanza de hasta 20 metros en buceo subacuático "en buceo";
- participar en la organización de la seguridad de las actividades de buceo subacuático.

*Para ir más allá*: Artículo 3 del decreto del 1 de diciembre de 2016 por el que se crea la designación de "buceo subacuático" de la especialidad "educador deportivo" BPJEPS.

**Prerrogativas específicas a la opción B "sin buceo":**

- llevar a cabo de forma autónoma las actividades de coaching y animación de actividades de aprendizaje, descubrimiento, enseñanza y formación en buceo subacuático "sin buceador";
- garantizar la seguridad de las actividades subacuáticas.

*Para ir más allá*: Artículo 3 del decreto del 1 de diciembre de 2016 por el que se crea la designación de "buceo subacuático" de la especialidad "educador deportivo" BPJEPS.

**Condiciones de acceso a la formación comunes a ambas opciones**

El interesado deberá:

- Ser mayores de edad
- Proporcione un formulario de registro con una fotografía
- Proporcione una fotocopia de un documento de identidad válido
- Si es necesario, presentar el certificado o certificados que justifiquen el aligeramiento de determinadas pruebas;
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Desarrollar pruebas de precertificación, si es necesario, de acuerdo con la certificación.
- Proporcionar copias del certificado censal y del certificado individual de participación en el Día de la Defensa y la Ciudadanía;
- Si es necesario, proporcione los documentos que justifiquen exenciones y equivalencias de la ley;
- para un certificado complementario, presentar una fotocopia del diploma que automese la inscripción en la formación o un certificado de inscripción para la formación que conduzca a dicho diploma;
- Presentar el certificado "Equipo de Nivel 1 de Primeros Auxilios" (PSE1) o su equivalente válido;
- Presentar la licencia para conducir embarcaciones de recreo en aguas marinas, opciones costeras o su equivalente;
- presentar un certificado médico de no contradictorio con la práctica y enseñanza del buceo subacuático con o sin buceo, dependiendo de la opción elegida a menos de tres meses de edad.

**Condiciones de acceso para la opción A "en un traje espacial"**

El interesado deberá:

- justifican un nivel técnico de aptitud PA-40 y al menos sesenta inmersiones en el medio natural, quince de las cuales son más de 30 metros en los últimos tres años, atestiguadas por el registro de buceo;
- para producir el certificado de éxito en la prueba técnica previa a la entrada, que consiste en:- snorkeling de hasta 10 metros de profundidad,
  - proporcionar una asistencia a menos de 20 metros de profundidad utilizando cualquier equipo apropiado,
  - inmersión en un polipasto a una profundidad de menos de 40 metros,
  - demuestra, en un intercambio en francés, su conocimiento de los síntomas, la prevención y la conducta que se llevaráa a cabo en caso de accidente relacionado con la práctica del buceo y su conocimiento de la gestión del aire , planificación de inmersiones, utilizando un ordenador de buceo y tablas de buceo existentes;
- cumplir con los siguientes requisitos previos al trabajo:- iniciar el uso de una máscara, aletas y snorkel (PMT) en piscinas y en el medio natural,
  - acompañar con seguridad a un grupo en una caminata submarina,
  - dominar las principales técnicas individuales en el buceo,
  - decirle a un grupo de buceadores las normas técnicas y de seguridad para la práctica de la actividad,
  - acompañar en seguridad, en un entorno natural en el espacio de 0 a 20 metros,
  - tomar decisiones inmediatas para proteger la seguridad del público en caso de un accidente o incidente que pueda conducir a problemas de seguridad.

**Condiciones de acceso para la opción B "sin trajes espaciales"**

El interesado deberá:

- producir una cualificación que acredite una habilidad para iniciar, enseñar o entrenar una actividad de buceo submarino "sin buceo" (excluyendo el senderismo submarino);
- para producir el certificado de éxito en la prueba técnica previa a la entrada, que consiste en:- snorkeling de hasta 15 metros de profundidad en peso constante moviéndose al menos 10 metros hasta el fondo,
  - nadar 800 metros con aletas, máscara y snorkel en menos de 13 minutos para los hombres, 14 minutos para las mujeres,
  - realizar una prueba de manejo de emergencia con un muñeco estandarizado de buceo libre,
  - demostrar, en el curso de un intercambio en francés, su conocimiento de los síntomas, la prevención y la conducta que se llevaráa a cabo en caso de accidente relacionado con actividades de buceo subacuático sin buceo, así como sobre las normas de seguridad,
- cumplir los siguientes requisitos previos a la situación:- iniciar el uso de una máscara, aletas y snorkel (PMT) en piscinas y en el medio natural,
  - acompañar con seguridad a un grupo en una caminata submarina,
  - dominar las principales técnicas de buceo individuales sin buceo,
  - proporcionar a un grupo de profesionales las normas técnicas y de seguridad para la práctica de la actividad sin traje de buceo,
  - crear una organización de buceo sin buceo, en entornos naturales y artificiales,
  - supervisar de forma segura a un grupo de practicantes de buceo, en entornos naturales y artificiales,
  - tomar decisiones inmediatas para proteger la seguridad del público en caso de un accidente o incidente que pueda conducir a problemas de seguridad.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35, A. 212-36 y Apéndice III-14b del Código del Deporte; Apéndices IV y V de la Orden BEJEPS de 1 de diciembre por la que se crea la palabra "buceo subacuático".

##### ESPECIALIDAD DEJEPS "desarrollo deportivo" mención "buceo subacuático"

DEJEPS es un diploma estatal registrado en el Nivel III[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/). Da fe de la adquisición de una cualificación en el ejercicio de una actividad profesional de coordinación y supervisión con fines educativos en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Este diploma se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr) y el Artículo D. 212-40 del Código del Deporte.

*Para ir más allá*: Artículo D. 212-35 del Código del Deporte.

**Es bueno saber**

El certificado complementario (CC) "buceo profundo y tutoría"**. Esta CC puede estar asociada con la designación deJEPS especialidad "desarrollo deportivo" de "buceo subacuático". Atestigua las siguientes habilidades:

- proporcionar dirección técnica para las actividades de buceo subacuático en el área de 40-60 metros de profundidad del sitio de buceo submarino;
- llevar a cabo la animación, iniciación y desarrollo en buceo submarino en la zona de profundidad de 40 a 60 metros;
- proporcionar tutoría en situación para los aprendices hasta el nivel III de formación profesional en buceo subacuático.

El candidato deberá justificar una experiencia de veinte inmersiones en el medio natural a una profundidad superior a 40 metros realizadas en un período de cinco años antes de la entrada en formación, mediante un certificado expedido por el Director Técnico Nacional de la Federación Francesa de Estudios Subacuáticos y Deportes.

*Para ir más allá* : Decreto de 1 de diciembre de 2016 por el que se establece el CC "buceo profundo y tutoría" asociado con la mención "buceo subacuático" del "desarrollo deportivo" DE SPECIALty DEJEPS.

**Prerrogativas DEJEPS:**

- Diseñar programas de organización, animación, iniciación y desarrollo en buceo subacuático;
- coordinar la ejecución de proyectos para organizar, facilitar, iniciar, desarrollar y desarrollar buceo subacuático;
- Proporcionar dirección técnica de las actividades en el sitio de buceo submarino;
- llevar a cabo la animación, la iniciación y el desarrollo en el buceo subacuático;
- proporcionar tutorías directas para los aprendices hasta el nivel III de formación profesional en buceo subacuático;
- gestionar las funciones logísticas de una estructura de buceo subacuático.

*Para ir más allá*: Artículo 2 del decreto del 6 de julio de 2011 por el que se crea la designación de "desarrollo deportivo" de la especialidad deJEPS "desarrollo deportivo".

**Condiciones de acceso a la formación que conduzca a DEJEPS**

El interesado deberá:

- Ser mayores de edad
- Proporcione un formulario de registro con una fotografía
- Proporcione una fotocopia de un documento de identidad válido
- Si es necesario, presentar el certificado o certificados que justifiquen el aligeramiento de determinadas pruebas;
- Proporcionar un certificado médico de no contradictorio para la práctica deportiva de menos de un año de edad;
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Desarrollar pruebas de precertificación, si es necesario, de acuerdo con la certificación.
- Proporcionar copias del certificado censal y del certificado individual de participación en el Día de la Defensa y la Ciudadanía;
- Si es necesario, proporcione los documentos que justifiquen exenciones y equivalencias de la ley;
- para un registro DE CC, presentar una fotocopia del diploma autorizando la inscripción en la formación o un certificado de inscripción para la formación que conduzca a ese diploma;
- Sostenga el Certificado de Primeros Auxilios de Nivel 1 (PSE1) o su equivalente;
- Tener licencia para conducir barcos de recreo en aguas marinas, opciones costeras o equivalentes;
- justifican la experiencia de cien inmersiones en el medio natural, treinta de las cuales a una profundidad de al menos 30 metros obtenidas en un período de cinco años antes de la entrada en formación. El número de inmersiones es atestiguado por el Director Técnico Nacional de Deportes Subacuáticos;
- justificar un nivel técnico de habilidades PA-40;
- elaborar certificados (expedidos por el Director Regional de Juventud, Deportes y Cohesión Social) de éxito en las pruebas de:- manejo de una emergencia con un muñeco de buceo estándar,
  - manejo de una emergencia de un buceador,
  - realizando una inmersión de exploración,
  - verificación de los conocimientos teóricos y prácticos en el buceo subacuático en lengua francesa;
- cumplir con los requisitos de la situación pedagógica. El candidato debe ser capaz de:- para garantizar el acompañamiento seguro de buceadores en ambientes submarinos, en trajes espaciales autónomos, hasta una profundidad de 40 metros,
  - para rescatar, en caso de incidente o accidente, a un buceador en un entorno submarino, hasta una profundidad de 40 metros,
  - para movilizar los procedimientos de alerta y primeros auxilios,
  - planificar la organización del buceo en el aire usando una computadora o una mesa de buceo,
  - detectar, prevenir y comportarse adecuadamente para evitar cualquier incidente o accidente que pueda ocurrir en el contexto del buceo subacuático recreativo,
  - para conducir con seguridad en acciones de entrenamiento de inmersión de buceadores en el espacio de 0 a 20 metros.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35 y A. 212-36 del Código del Deporte; de 6 de julio de 2011 creando la especialidad deJEPS "buceo subacuático" designación de "desarrollo deportivo".

**Curso de reciclaje**

Los titulares de DeJEPS están sujetos a un curso de readaptación cada cinco años. Está organizado por una de las instituciones responsables de la implementación del DEJEPS, que se conoce como "buceo subacuático".

*Para ir más allá*: Artículo 7 del decreto del 6 de julio de 2011 por el que se crea la designación de "desarrollo deportivo" de la especialidad deJEPS "desarrollo deportivo".

##### Especialidad DESJEPS "desarrollo deportivo" mención "buceo subacuático"

El DESJEPS es un diploma estatal superior inscrito en el Nivel II de la [RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/). Atestigua la adquisición de una cualificación en el ejercicio de una actividad profesional de especialización técnica y gestión con fines educativos en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Este diploma se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr) y el Artículo D. 212-56 del Código del Deporte.

*Para ir más allá*: Artículo D. 212-51 del Código del Deporte.

**Prerrogativas DESJEPS:**

- Desarrollar proyectos de exploración, desarrollo y gestión para sitios de buceo submarino;
- llevar a cabo conocimientos técnicos, educativos y medioambientales en buceo subacuático;
- Proporcionar dirección técnica para las actividades de buceo subacuático;
- asesoramiento y formación de buceadores de todos los niveles, en todos los ambientes y sitios de buceo subacuático;
- gestionar y tutorizar a los becarios en formación profesional en buceo subacuático;
- Gestionar la logística de una estructura de buceo subacuático
- organizar y coordinar acciones de formación para los formadores.

*Para ir más allá*: Artículo 2 de la Orden de 6 de julio de 2011 por el que se crea la designación de "buceo subacuático" del desJEPS de SPECIALty.

**Condiciones de acceso a la formación que conduzca a DESJEPS**

El interesado deberá:

- Ser mayores de edad
- Proporcione un formulario de registro con una fotografía
- Proporcione una fotocopia de un documento de identidad válido
- Si es necesario, presentar el certificado o certificados que justifiquen el aligeramiento de determinadas pruebas;
- Proporcionar un certificado médico de no contradictorio para la práctica deportiva de menos de un año de edad;
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Desarrollar pruebas de precertificación, si es necesario, de acuerdo con la certificación.
- Proporcionar copias del certificado censal y del certificado individual de participación en el Día de la Defensa y la Ciudadanía;
- Si es necesario, proporcione los documentos que justifiquen exenciones y equivalencias de la ley;
- para un certificado complementario, presentar una fotocopia del diploma que automese la inscripción en la formación o un certificado de inscripción para la formación que conduzca a dicho diploma;
- Poseer el certificado de 1a ayuda en el equipo de Nivel 1 (PSE1) o su equivalente;
- Tener licencia para conducir barcos de recreo en aguas marinas, opciones costeras o equivalentes;
- Mantenga la capacidad de un buceador nitrox confirmado (PN-C);
- justifican la experiencia de cien inmersiones en el medio natural, de las cuales treinta a una profundidad de al menos 30 metros, obtenidas en un período de cinco años antes de la entrada en formación y atestiguadas por el director técnico nacional de deportes subacuáticos;
- justifican la experiencia de 800 horas de instrucción de buceo subacuático, obtenidas en un período de cinco años antes de la entrada en formación y certificadas por el Director Técnico Nacional de Deportes Subacuáticos;
- justificar un nivel técnico de habilidades PA-40;
- producir los certificados de éxito, emitidos por el Director Regional de Juventud, Deportes y Cohesión Social, sobre las pruebas de:- gestión de una emergencia con un muñeco de buceo libre estándar,
  - manejo de una emergencia de un buceador,
  - organizar y llevar a cabo una inmersión docente en el espacio profundo,
  - verificación de conocimientos teóricos y prácticos;
- cumplir con los requisitos de la situación pedagógica. El candidato debe ser capaz de:- para garantizar el acompañamiento seguro de todo público en ambiente sin agua, en trajes espaciales autónomos en aire o nitrox, de 0 a 40 metros,
  - para rescatar, en caso de incidente o accidente, a un buceador en un entorno submarino, en el espacio de 0 a 40 metros,
  - para movilizar procedimientos de alerta y rescate,
  - planificar la organización del buceo en el aire usando una computadora o una mesa de buceo,
  - detectar, prevenir y comportarse adecuadamente ante cualquier accidente que pueda ocurrir en el contexto del buceo subacuático recreativo,
  - para conducir con seguridad en acciones de entrenamiento de inmersión de buceadores en un espacio de 0-40 metros.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35 y A. 212-36 del Código del Deporte; Artículos 3 y 4 del decreto del 6 de julio de 2011 por el que se crea la designación de "buceo subacuático" del desJEPS de SPECIALty.

**Curso de reciclaje**

Los titulares de DesJEPS están sujetos a un curso de readaptación cada cinco años. Está organizado por una de las instituciones públicas del Ministerio de Deportes encargada de impartir formación en buceo subacuático, bajo la autoridad del director regional de juventud, deportes y cohesión social.

*Para ir más allá*: Artículo 7 de la Orden de 6 de julio de 2011 por el que se crea la designación de "buceo subacuático" del desJEPS de SPECIALty.

#### Costos asociados con la calificación

Se paga la formación para BPJEPS, DEJEPS y DESJEPS. El costo varía dependiendo de las menciones y las organizaciones de capacitación.

Para más detalles, es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como instructor de buceo subacuático en Francia para personas que hayan sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Obligación de presentación de informes (con el fin de obtener la tarjeta de educador deportivo profesional)

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial](https://eaps.sports.gouv.fr).

**hora**

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

**Documentos de apoyo**

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial](https://eaps.sports.gouv.fr).

**hora**

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

**Documentos de apoyo**

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y siguientes, A. 212-209 y Apéndice II-12-3 del Código del Deporte.

### c. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial](https://eaps.sports.gouv.fr).

**hora**

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

**Documentos de apoyo para la primera declaración de actividad**

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

**Evidencia para una declaración de renovación de la actividad**

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182, A. 212-209 y Listas II-12-2-a y II-12-b del Código del Deporte.

### d. Medidas de compensación

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

