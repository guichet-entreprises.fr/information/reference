﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP082" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Servicios funerarios" -->
<!-- var(title)="Consejero funerario" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="servicios-funerarios" -->
<!-- var(title-short)="consejero-funerario" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/servicios-funerarios/consejero-funerario.html" -->
<!-- var(last-update)="2020-04-15 17:22:27" -->
<!-- var(url-name)="consejero-funerario" -->
<!-- var(translation)="Auto" -->


Consejero funerario
===================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:27<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El consejero funerario es un profesional cuya misión es asesorar y acompañar a las familias en la organización de funerales y en procedimientos administrativos (escribiendo el aviso de defunción, formulario a llenar para los ayuntamientos, etc.) pero también práctico.

Organiza la ceremonia, vende servicios funerarios, gestiona el movimiento del convoy y coordina a todas las partes interesadas.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de consejero funerario está reservada al titular de un diploma que justifica una aptitud profesional.

*Para ir más allá*: Artículo D.2223-55-2 del Código de Autoridades Locales.

#### Entrenamiento

El diploma que permite la profesión de funerario se emite tan pronto como el candidato ha sido admitido con éxito a un examen teórico y una evaluación de la formación práctica.

La formación teórica de 140 horas se centra en las siguientes asignaturas:

- higiene, seguridad y ergonomía;
- legislación y reglamentación funeraria;
- psicología y sociología del duelo;
- prácticas funerarias y ritos;
- Diseñar y organizar una ceremonia;
- Entrenar a un equipo
- productos, servicios y asesoramiento en venta;
- regulación comercial.

Se acompaña de un curso de formación práctica de 70 horas realizado en una empresa, un consejo o una asociación autorizada al final del cual se realizará una evaluación por escrito.

*Para ir más allá*: Artículos D. 2223-55-3 y siguientes del Código de Autoridades Locales;[decretado 30 de abril de 2012](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025789769&dateTexte=20180103) Decreto No 2012-608 de 30 de abril de 2012 sobre diplomas en el sector funerario.

#### Costos asociados con la calificación

Se paga la capacitación que conduce al diploma de consejero funerario. Para más información, es aconsejable acercarse a las agencias que lo dispensan.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o una parte en el Espacio Económico Europeo podrá trabajar en Francia como consejero funerario, de forma temporal y ocasional si cumple una de las siguientes condiciones:

- Estar legalmente establecidos en ese Estado para llevar a cabo la misma actividad;
- han estado en esta actividad durante un año en los últimos diez años antes de la entrega, cuando ni la formación ni la actividad están reguladas en ese estado;
- han recibido una[Despeje](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&idArticle=LEGIARTI000006390299&dateTexte=&categorieLien=cid) por la prefectura competente.

*Para ir más allá*: Artículos L. 2223-23 y L. 2223-47 del Código de Autoridades Locales.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

El nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si justifica:

- tres años seguidos;
- o un certificado de competencia expedido por una autoridad competente cuando la profesión esté regulada en ese Estado;
- ya sea a tiempo completo o a tiempo parcial, durante un año en los últimos diez años, cuando el estado no regula la profesión o la formación.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar al prefecto territorialmente competente el reconocimiento de sus cualificaciones profesionales (véase infra "4o. a. Solicitar el reconocimiento de las cualificaciones profesionales del nacional para un ejercicio permanente (LE)).

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones requeridas para el acceso a la profesión y a su práctica en Francia, el interesado podrá ser sometido a una prueba de aptitud o a una etapa de adaptación (véase infra "4.00). a. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos L. 2223-48 y L. 2223-50 del Código de Autoridades Locales.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Cualquier hecho que pueda corromper a un tercero para obtener de él que recomienda a la familia de un fallecido la empresa o asociación del consejero funerario es castigado con cinco años de prisión y 75.000 euros de multa.

El consejero funerario también puede verse afectado por la prohibición de sus derechos civiles, civiles o familiares, la prohibición de la actividad profesional y la publicación o difusión de la decisión.

*Para ir más allá*: Artículo L. 2223-35 del Código de Autoridades Locales.

4°. Procedimientos y formalidades de reconocimiento de cualificación
-----------------------------------------------------------------------------------------

### a. Solicitar el reconocimiento de las cualificaciones profesionales del nacional para un ejercicio permanente (LE)

**Autoridad competente**

El prefecto del lugar de instalación del nacional es competente para decidir sobre la solicitud de reconocimiento de cualificaciones profesionales.

**Documentos de apoyo**

La solicitud se realiza presentando un archivo con los siguientes documentos justificativos:

- Una identificación válida
- Un certificado de competencia o un certificado de formación expedido por una autoridad competente;
- cualquier prueba, en su caso, de que el nacional haya trabajado como consejero funerario durante un año a tiempo completo o a tiempo parcial en los últimos diez años en un Estado miembro que no regule la profesión.

**Procedimiento**

El prefecto confirmará la recepción del expediente completo en el plazo de un mes e informará al nacional en caso de que falten documentos. A partir de entonces, dispondrá de cuatro meses para decidir sobre la solicitud de reconocimiento o someter al nacional a una medida de compensación.

**Bueno saber: medidas de compensación**

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a una medida de compensación, que puede ser:

- Un curso de adaptación
- una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

*Para ir más allá*: Artículos R. 2223-133 y siguientes del Código de Autoridades Locales; orden de 25 de agosto de 2009 por la que se aplican medidas compensatorias y de verificación de conocimientos para el reconocimiento de cualificaciones profesionales en el sector funerario.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

