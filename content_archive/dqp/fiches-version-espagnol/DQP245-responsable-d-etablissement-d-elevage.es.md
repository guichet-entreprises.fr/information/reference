﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP245" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Responsable del establecimiento de animales de especies no domésticas, venta o alquiler, tránsito, presentación al público de especímenes vivos de fauna francesa y extranjera" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="responsable-del-establecimiento" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/responsable-del-establecimiento-de-animales-de-especies-no-domesticas.html" -->
<!-- var(last-update)="2020-04-15 17:20:56" -->
<!-- var(url-name)="responsable-del-establecimiento-de-animales-de-especies-no-domesticas" -->
<!-- var(translation)="Auto" -->


Responsable del establecimiento de animales de especies no domésticas, venta o alquiler, tránsito, presentación al público de especímenes vivos de fauna francesa y extranjera
==============================================================================================================================================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:56<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El jefe de un centro de cría de animales no doméstico es responsable de la venta, alquiler, tránsito y presentación al público de estos animales.

Se considera que los animales de especies no domésticas no figuran en la lista de especies, razas o variedades de animales domésticos[decretado a partir del 11 de agosto de 2006](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000789087).

Además, los establecimientos reproductores sólo podrán albergar animales que no hayan sido modificados por selección.

*Para ir más allá*: Artículo R. 413-8 del Código de Medio Ambiente; decreto de 11 de agosto de 2006 por el que se establece la lista de especies, razas o variedades de animales domésticos.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de gerente de dicha institución, el profesional debe cumplir con los siguientes requisitos:

- estar calificados profesionalmente (ver infra "Formación");
- Poseer un certificado de capacidad para el cuidado de animales de especies no domésticas;
- para obtener una autorización para abrir dicho establecimiento (véase infra "4o. b. Solicitud de permiso para abrir");
- identificar animales en cautiverio.

*Para ir más allá*: Artículo L. 413-2 del Código de Medio Ambiente.

#### Entrenamiento

Para obtener el Certificado de Capacidad, el profesional debe tener un diploma para realizar una actividad animal no doméstica y una experiencia laboral que varía en función de la naturaleza del título obtenido.

La lista de diplomas y la duración de la experiencia laboral requerida se establece en el[Apéndice I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=7A2E4C8838C4669AE6617350AFDEF4D5.tplgfr28s_3?idArticle=LEGIARTI000021122568&cidTexte=LEGITEXT000021122566&dateTexte=20180315) de la orden de 12 de diciembre de 2000 por la que se establecen los diplomas y condiciones de experiencia profesional exigidos por el artículo R. 413-5 del Código de Medio Ambiente para la expedición del Certificado de Capacidad para el Mantenimiento de Animales de Especies No Domésticas.

**Certificado de capacidad**

El administrador de un establecimiento de cría de animales no doméstico que los venda, arrende, transite o presente al público deberá obtener un certificado de capacidad para el mantenimiento de estos animales.

Para ello, el interesado debe presentar una solicitud al prefecto del departamento de su hogar (véase infra "4o. a. Solicitud de certificado de capacidad para animales no domésticos").

*Para ir más allá*: Artículo L. 413-2 del Código de Medio Ambiente.

**Cómo identificar animales**

Para llevar a cabo la actividad de cría de animales, venta o alquiler, tránsito o presentación de animales no domésticos al público, deben identificarse a través de:

- Marcado individual
- su inclusión en el índice nacional de identificación de animales de especies no domésticas en cautiverio.

**Marcar animales**

Todos los animales retenidos en el centro deben ser etiquetados individualmente tan pronto como el mes después del nacimiento.

Los términos de este marcado se establecen en el[Apéndice A](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=7A2E4C8838C4669AE6617350AFDEF4D5.tplgfr28s_3?idArticle=LEGIARTI000022808149&cidTexte=LEGITEXT000018810562&dateTexte=20180315) del auto de 10 de agosto de 2004 por el que se establecen las condiciones para la autorización de la detención de determinadas especies no nacionales en establecimientos de cría, venta, alquiler, tránsito o presentación de animales de especies no nacionales al público.

Este procedimiento, llevado a cabo por un veterinario, permite asignar a cada animal un número de identificación único.

El veterinario que marque al propietario del animal deberá facilitar al propietario del animal un documento que acredite el marcado y, a continuación, entregarlo al administrador nacional de la fichero.

**Tenga en cuenta que**

Se concede un período de tiempo más largo en caso de imposibilidad biológica de marcar al animal dentro del tiempo asignado.

**Inscripción en el expediente nacional**

Los datos e información del animal sobre su propietario, incluyendo su identidad y su casa, deben almacenarse en este archivo.

*Para ir más allá*: Artículo R. 413-23-1 del Código de Medio Ambiente; Artículos 6 a 11 de la orden de 10 de agosto de 2004 por el que se establecen las condiciones para la autorización de animales de determinadas especies no nacionales que se mantendrán en establecimientos de cría, venta, alquiler, tránsito o presentación al público de animales de especies no nacionales que se mantengan en establecimientos de cría, venta, alquiler, tránsito o presentación al público de animales de especies no nacionales Doméstica.

#### c. Costos asociados con la calificación

El coste de la formación del profesional varía según el curso previsto. Es aconsejable acercarse a las instituciones interesadas para obtener más información.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Todo nacional de un Estado miembro de la Unión Europea (UE) o de un Estado Parte en el Acuerdo del Espacio Económico Europeo (EEE), legalmente establecido y actuando como jefe de un establecimiento animal no nacional, podrá ejercer título temporal y casual, la misma actividad en Francia, sin poseer el certificado de capacidad.

Para ello, el interesado deberá hacer una declaración previa con el prefecto del departamento en el que desea realizar su primera actuación (véase infra "4o. c. Solicitud de preinformación para el nacional de la UE para un ejercicio temporal e informal (LPS)").

Cuando existan diferencias sustanciales entre la formación recibida por el profesional y la necesaria para llevar a cabo la actividad en Francia, el prefecto podrá decidir someterlo a una medida de compensación (véase infra "4o. c. Bueno saber: medida de compensación").

*Para ir más allá*: Artículo L. 413-2 del Código de Medio Ambiente.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

No existen disposiciones específicas para el nacional de la UE para un ejercicio permanente en Francia. Como tal, está sujeto a los mismos requisitos que el nacional francés (véase "2.0). Cualificaciones profesionales").

3°. Seguros y sanciones
--------------------------------

El profesional que proactúe a nivel liberal debe disponer de un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

**Sanciones**

Tan pronto como el profesional opera un establecimiento de cría de animales no doméstico sin tener un permiso de apertura, el prefecto le obliga a regularizar su situación y puede considerar suspender su actividad durante este período. Si el profesional no regulariza su situación, su establecimiento podrá ser cerrado dentro de los dos años siguientes a su notificación.

*Para ir más allá*: Artículo R. 413-45 a R. 413-47 del Código de Medio Ambiente

Además, el profesional se enfrenta a una multa de 1.500 euros, 3.000 euros en caso de reincidencia, si no identifica y marca a sus animales.

*Para ir más allá*: Artículos R. 415-4 a R. 415-5 del Código de Medio Ambiente.

4°. Procedimientos y formalidades de reconocimiento de cualificación
-----------------------------------------------------------------------------------------

### a. Solicitud del certificado de capacidad animal no nacional

**Autoridad competente**

El profesional debe solicitar al prefecto del departamento de su casa o al prefecto de policía en París si no está domiciliado en un departamento francés o en Saint-Pierre-et-Miquelon.

**Documentos de apoyo**

La solicitud del profesional debe contener la siguiente información:

- identidad, dirección y tipo de cualificación general o especial solicitada
- todos los diplomas, certificados o cualquier documento que justifique su experiencia profesional;
- cualquier documentación que justifique la competencia del solicitante para realizar una actividad profesional en relación con las mascotas y el desarrollo de un establecimiento que las albergaría.

**Retrasos y resultados del procedimiento**

Al recibir la solicitud completa del profesional, el prefecto le expide el certificado de capacidad. Este certificado podrá concederse por un período de tiempo limitado o ilimitado y menciona la especie o actividad para la que se concedió y, como opción opcional, el número de animales autorizados para su mantenimiento.

**Tenga en cuenta que**

Para la cría de ciertas categorías de animales, la consulta con la Comisión Departamental de Naturaleza, Paisajes y Sitios no es obligatoria siempre y cuando el profesional cumpla con los requisitos de calificación. Los términos de este procedimiento son establecidos por el[decretado a partir del 2 de julio de 2009](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020887078&dateTexte=20180315) citado a continuación.

*Para ir más allá* R. 413-3 a R. 413-7 del Código de Medio Ambiente; orden de 2 de julio de 2009 por la que se establecen las condiciones simplificadas en las que puede expedirse el certificado de capacidad para el mantenimiento de animales de especies no nacionales.

### b. Solicitar permiso para abrir

La apertura de un centro de cría de animales no domésticos es objeto de una autorización previa para abrir. Para ello, el interesado debe solicitarlo a la autoridad competente.

Las instituciones se clasifican en dos categorías:

- el primero incluye los establecimientos que plantean graves peligros o desventajas para la vida silvestre y los entornos naturales y la seguridad de las personas;
- el segundo incluye a aquellos que no presentan los peligros anteriores, pero que deben cumplir con las disposiciones para garantizar la protección de la vida silvestre y los entornos naturales y la seguridad de las personas.

**Autoridad competente**

El profesional deberá presentar una solicitud de siete copias al prefecto del departamento en el que se encuentre el establecimiento (o desde su domicilio, siempre que el establecimiento sea móvil).

**Documentos de apoyo**

La solicitud debe incluir la siguiente información:

- La identidad, la dirección y si el solicitante es una persona jurídica, su nombre y su nombre;
- La naturaleza de las actividades que el profesional desea llevar a cabo;
- La lista de las instalaciones de la instalación y el plan de la instalación;
- La lista de especies y el número de animales de cada especie en poder de la institución y su distribución en la instalación;
- Un aviso sobre cómo funciona el establecimiento
- El certificado de capacidad del administrador de la instalación.

**Tenga en cuenta que**

El nombre de la institución no debe incluir los siguientes términos, debido a las disposiciones específicas que los rigen:

- Parque Nacional;
- Reserva Natural;
- Conservatorio.

Además, cuando la instalación incluye instalaciones pre-aprobadas clasificadas para la protección del medio ambiente, la instalación está sujeta a permiso de apertura.

**Retrasos y resultados del procedimiento**

El prefecto recibe el dictamen de la Comisión Departamental sobre la Naturaleza de los Paisajes y Sitios, que puede cuestionar al solicitante. Si es necesario, será informado por el prefecto ocho días antes de su presentación.

El prefecto dispone de cinco meses a partir de la presentación de la solicitud para autorizar la apertura del establecimiento. En caso de dictamen favorable, decide la autorización de apertura que establece la lista de especies que el establecimiento puede tener y las actividades que pueden llevarse a cabo.

**Tenga en cuenta que**

Para los establecimientos de la primera categoría, el prefecto debe recabar la opinión de las autoridades locales, que tienen un período de decisión de 45 días. A falta de respuesta, el dictamen se considera favorable.

Por otra parte, en el caso de los establecimientos de la segunda categoría, el prefecto examina el cumplimiento de la aplicación con los requisitos de protección de las especies y la calidad de las instalaciones de cuidado animal no nacionales.

*Para ir más allá*: Artículos R. 413-10 a R. R. 413-14 del Código de Medio Ambiente; 10 de agosto de 2004 supra.

### c. Solicitud de preinformación para el nacional de la UE para el ejercicio temporal y casual (LPS)

**Autoridad competente**

El nacional envía una solicitud al prefecto del departamento en el que desea ejercer su primer beneficio.

**Documentos de apoyo**

Su solicitud debe incluir la siguiente información, si la hubiera, con su traducción al francés:

- identidad, nacionalidad y dirección
- El tipo de actividad para la que se realiza la solicitud
- un certificado que certifique que el nacional está legalmente establecido en un Estado de la UE para llevar a cabo esta actividad y que no incurre en ninguna prohibición temporal o permanente de la práctica;
- prueba de sus cualificaciones profesionales
- cuando el Estado miembro de establecimiento no regule el acceso a la profesión o a su ejercicio, la prueba de que el solicitante ha ejercido esta actividad en uno o varios Estados miembros durante al menos un año en los últimos diez años;
- un documento que justifica que el profesional ha contratado un seguro de responsabilidad civil profesional.

**Retrasos y resultados del procedimiento**

En el plazo de un mes a partir de la recepción de la solicitud, el prefecto le informa que estará o no sujeto a una revisión de sus cualificaciones profesionales y tendrá que someterse a una medida de compensación.

Al final de este período y si la solicitud del nacional no está sujeta a revisión y la solicitud está completa, el prefecto expide el certificado de capacidad.

**Bueno saber: medida de compensación**

Cuando el examen de las cualificaciones profesionales del nacional revela diferencias sustanciales entre su formación y la necesaria para llevar a cabo la actividad en Francia, lo que podría perjudicar la salud o la seguridad del público o de los animales , el prefecto remitió el asunto a una comisión asesora de vida silvestre cautiva, compuesta por profesionales y representantes de los ministerios pertinentes. En caso necesario, el nacional deberá someterse a una prueba de aptitud organizada por esta comisión y compuesta por preguntas escritas y orales.

Al final de la prueba de aptitud, el prefecto expide un certificado de cualificación profesional al nacional para llevar a cabo sus servicios en Francia.

**Tenga en cuenta que**

El silencio guardado por el prefecto más allá de un período de un mes vale la pena aceptar la solicitud.

*Para ir más allá*: Artículo R. 413-4 del Código de Medio Ambiente.

### d. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea.

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

