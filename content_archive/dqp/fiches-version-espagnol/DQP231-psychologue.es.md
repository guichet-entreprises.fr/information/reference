﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP231" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Psicólogo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="psicologo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/psicologo.html" -->
<!-- var(last-update)="2020-04-15 17:22:04" -->
<!-- var(url-name)="psicologo" -->
<!-- var(translation)="Auto" -->


Psicólogo
=========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:04<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El psicólogo es un profesional que escucha, acompaña y ayuda a personas con trastornos morales o psicológicos, puntuales o crónicos.

Realiza evaluaciones psicológicas, realiza diagnósticos e implementa terapias individuales o colectivas adaptadas a sus pacientes.

El psicólogo también trabaja con profesionales médicos, sociales y educativos, sensibilizando e informándoles sobre el aspecto psicológico.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

### Legislación nacional

De acuerdo con el artículo 44 de la Ley 85-772, de 25 de julio de 1985, "el uso profesional del título de psicólogo, acompañado o no por un calificador, está reservado a los titulares de un diploma, certificado o título sancionar una educación universitaria básica y aplicada de alto nivel en psicología preparándose para la vida profesional y enumerada por decreto en el Consejo de Estado o titulares de un diploma extranjero reconocido equivalente al diplomas nacionales requeridos."

En virtud del artículo 5 del Decreto 90-255, de 22 de marzo de 1990, por el que se establece la lista de diplomas de uso profesional del título de psicólogo, el reconocimiento del carácter equivalente de los títulos extranjeros es responsabilidad del Ministro responsable de educación superior tras el asesoramiento de una comisión cuya composición se fija por decreto de ese ministro.

Además, el artículo II del artículo 44 de la Ley de 25 de julio de 1985 dispone que los titulares de un título, certificado o título que permita el ejercicio de la profesión en otro estado podrán hacer uso profesional del título de psicólogo cuando permite el ejercicio de la profesión en ese estado.

*Para ir más allá*: Artículo 1 del Decreto de 22 de marzo de 1990 y artículo 44 de la Ley de 25 de julio de 1985.

### Entrenamiento

Se requiere una licenciatura en psicología y una maestría en psicología con una pasantía profesional de al menos 500 horas.

Durante la pasantía, completada en períodos continuos o fraccionarios, el estudiante es puesto bajo la responsabilidad de un psicólogo profesional-referente calificado que ha estado practicando durante al menos tres años y un profesor en prácticas que es uno de los maestros-investigadores de la formación que conduzca al máster. La pasantía debe completarse a más tardar un año después de la formación teórica proporcionada como parte del máster en psicología.

Al final de la pasantía, el interesado presenta un informe de prácticas y lo apoya ante un jurado compuesto por los directores del curso y un profesor-investigador de psicología designado por el jefe de la mención de psicología del máster.

La validación de las prácticas da lugar a la expedición de un certificado que debe ajustarse al modelo adjunto al pedido de 19 de mayo de 2006.

**Tenga en cuenta que**

La usurpación del título de psicólogo es un delito castigado con las penas previstas en el artículo 433-17 del Código Penal, es decir, un año de prisión y una multa de 15.000 euros.

*Para ir más allá*: Artículo 433 del Código Penal; 16 de mayo de 2006.

### Costos relacionados

Se paga la formación que conduce al grado de psicólogo. Su costo varía dependiendo de las instituciones que proporcionan las enseñanzas.

### b. Nacionales de la UE y de la UE: para el ejercicio temporal o casual (Servicio Gratuito)

No existe ningún reglamento para un nacional de un miembro de la Unión Europea (UE) o parte del Espacio Económico Europeo (EEE) que desee ejercer como psicólogo en Francia, ya sea de forma temporal o ocasional.

Por lo tanto, sólo las medidas adoptadas para el libre establecimiento de nacionales de la UE o del EEE (véase más adelante "5. "Proceso de Reconocimiento de Calificación y Formalidades") encontrará que se aplican.

### c. Nacionales de la UE y del EEE: para un ejercicio permanente (establecimiento libre)

Dado que la profesión de psicólogo no forma parte del sistema automático de reconocimiento de diplomas, cualquier persona que posea diplomas de otro Estado miembro de la UE o de una parte en el Acuerdo EEE que desee ejercer un contrato permanente profesión de psicólogo en Francia debe obtener una licencia para ejercer expedida por el Ministro responsable de la educación superior (cf. infra "5. Proceso de calificación y formalidades").

Cuando exista una diferencia sustancial entre la formación impartida por el solicitante y la exigida en Francia, podrá estar sujeto a medidas de compensación (véase infra "Buenas medidas de compensación") previstas en el Decreto No 2003-1073, de 14 de noviembre, de 14 de noviembre enmendado en 2003.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. La ética y la ética

Aunque no están regulados, hay obligaciones éticas y éticas para los psicólogos, incluyendo:

- Respetar el secreto profesional
- Asegurar que se mantenga la dignidad de los pacientes;
- actuar honestamente y con integridad.

El código de ética no regulado se remonta a 1996. Fue revisado en 2012.

### b. Actividades acumulativas

El psicólogo que trabaja en la función hospitalaria puede realizar otra actividad profesional. La combinación de las dos actividades debe ser coherente con la dignidad y la calidad requeridas por su práctica profesional y que se ajuste a la normativa vigente.

*Para ir más allá*: Artículos 5 a 12 del decreto del 27 de enero de 2017.

4°. Legislación social y seguro
----------------------------------------------------

En caso de ejercicio liberal, el psicólogo está obligado a conllevar un seguro de responsabilidad profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante esta actividad.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Obtener permiso para ejercer para los nacionales de la UE o del EEE para un ejercicio permanente (LE)

El titular de los títulos expedidos en otro Estado miembro de la UE o parte en el Acuerdo EEE deberá solicitar la autorización para ejercer.

**Autoridad competente**

El permiso para ejercer como psicólogo es expedido por el Ministro responsable de la educación superior, previa dirección de un comité de expertos compuesto por profesores-investigadores y representantes de organizaciones profesionales.

**Procedimiento**

En apoyo de su solicitud de autorización, el titular de los títulos expedidos en otro Estado miembro de la UE o parte en el Acuerdo EEE deberá enviar un expediente completo al Ministerio de Educación Superior, Investigación e Innovación (MESRI) transmitidos por medios desmaterializados. Los casos se oyen por orden de llegada y hasta 80 casos por comisión.

**Documentos de apoyo**

La solicitud de licencia para la práctica debe incluir:

- El formulario de solicitud de autorización de la profesión cumplimentado. Este formulario está disponible en línea en el[Sitio web de MESRI](http://www.enseignementsup-recherche.gouv.fr/cid66177/psychologue-une-profession-reglementee-en-france.html) ;
- Id
- Un CV
- Una copia de todos los títulos, certificados o títulos en psicología obtenidos;
- Una copia de los certificados de las autoridades que emiten el certificado de formación, especificando el nivel de formación y el detalle, año tras año, de las lecciones imitadas, su volumen por hora y el contenido y duración de las prácticas validadas;
- En caso necesario, un certificado de la autoridad competente del Estado miembro que justifique el ejercicio de la profesión de psicólogo con indicación de la duración;
- para aquellos que han trabajado en un Estado miembro de la UE o en una parte en el Acuerdo EEE que no regula el acceso o el ejercicio de la profesión: un certificado que justifique la duración del ejercicio en dicho Estado con las fechas correspondientes o un registro de prácticas pueden seguirse con una indicación de su duración.

Se dispone de información sobre el procedimiento para reconocer diplomas extranjeros para ejercer como psicólogo, así como preguntas frecuentes[en el sitio web de MESRI](http://www.enseignementsup-recherche.gouv.fr/pid24843-cid66177/psychologue-une-profession-reglementee-en-france.html).

**Qué saber**

Los documentos justificativos, si los hubiere, deben ser traducidos al francés por un traductor jurado.

**Resultado del procedimiento**

La Comisión de Expertos emite un dictamen, el Ministro responsable de la educación superior es el único competente para tomar una decisión motivada.

Por lo tanto, la decisión ministerial adopta la forma de:

- o una decisión favorable por la que se establezca el reconocimiento del título como equivalente a los exigidos en Francia para el ejercicio de la profesión de psicólogo;
- o una decisión desfavorable: si es necesario, la persona puede tener que elegir entre una prueba de aptitud y un curso de adaptación (véase a continuación "Bueno saber: medidas de compensación").
- o una decisión reservada para el envío de piezas adicionales

**Tenga en cuenta que**

El silencio mantenido por el Ministro responsable de la educación superior durante más de cuatro meses sobre la solicitud de autorización merece la decisión de rechazar.

*Para ir más lejos:* Decreto No 2003-1073 de 14 de noviembre de 2003.

**Remedio**

En caso de decisión desfavorable, el solicitante podrá impugnarla.

Tiene dos meses después de la notificación de la decisión de formar, a elección de:

- una apelación agraciada a MESRI;
- una impugnación legal ante el tribunal administrativo de París.

**Costo**

Gratis.

**Bueno saber: medidas de compensación**

Una vez recibida la decisión ministerial, el individuo deberá, si es necesario, indicar por correo electrónico su elección entre una prueba de aptitud y un curso de alojamiento.

Debe presentar ante el presidente de la universidad solicitada un expediente que incluya una solicitud de inscripción y una copia de la decisión ministerial.

**La prueba de aptitud**

Incluye preguntas escritas y orales y ejercicios prácticos, o sólo una de estas modalidades de control.

**El curso de adaptación**

Puede tener lugar en varios campos de formación, en instituciones de acuerdo con la universidad organizadora.

El aprendiz ha estado bajo la responsabilidad de un profesional calificado que ha estado practicando durante al menos tres años.

Al final de la pasantía, que puede ir acompañada de una formación teórica adicional, el aprendiz presenta un informe y lo apoyará ante un jurado cuyo presidente y miembros sean nombrados por el presidente de la universidad.

La universidad organizadora envía la notificación oficial de éxito al curso de adaptación o a la prueba de aptitud para la Dirección de Educación Superior. Después de revisar la solicitud, la decisión adoptada por el Ministro responsable de la educación superior se notifica al solicitante.

*Para ir más allá*: Artículos 2 a 7 de la orden de 18 de noviembre de 2003.

### b. Registro en la Agencia Regional de Salud (ARS) en el directorio de Adeli

**Autoridad competente**

El registro en el directorio de Adeli se realiza con la Agencia Regional de Salud (ARS) del lugar de ejercicio.

**hora**

La solicitud de registro debe presentarse antes de asumir el cargo, independientemente del modo de práctica (liberal, asalariado, mixto).

**Documentos de apoyo**

En apoyo de su solicitud de inscripción, el psicólogo debe:

- la decisión favorable emitida por el Ministro de Educación Superior por la que se establece el reconocimiento del título como equivalente a los exigidos en Francia para el ejercicio de la profesión de psicólogo;
- Id
- Formulario Cerfa 12269Completado, fechado y firmado.

**Resultado del procedimiento**

Se emite un número Adeli, se menciona en la recepción del archivo, emitido por el ARS.

**Costo**

Gratis.

### c. Remedios

#### Nacional

En caso de decisión desfavorable, el solicitante podrá impugnarla.

Dispone de dos meses después de la notificación de la decisión, para formar la elección de:

- un llamamiento agraciado al Ministro de Educación Superior;
- una impugnación legal ante el tribunal administrativo de París.

##### Costo

Gratis.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez presentado su expediente, SOLVIT se pone en contacto con él en un plazo indicativo de una semana para solicitar, si es necesario, información adicional y verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT trata de encontrar una solución individual y pragmática de manera amistosa en un plazo indicativo de diez semanas a partir del día en que el centro SOLVIT se haya hecho cargo del caso en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Después de que SOLVIT procesa el archivo, presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

