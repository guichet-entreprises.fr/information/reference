﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP064" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Cirugía plástica, reconstructiva y estética" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="cirugia-plastica-reconstructiva" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/cirugia-plastica-reconstructiva-y-estetica.html" -->
<!-- var(last-update)="2020-04-15 17:21:22" -->
<!-- var(url-name)="cirugia-plastica-reconstructiva-y-estetica" -->
<!-- var(translation)="Auto" -->


Cirugía plástica, reconstructiva y estética
===========================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:22<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La cirugía plástica, reconstructiva y estética apoya las afecciones de la piel y los tejidos blandos no viscerales que requieren cirugía. La cirugía reconstructiva está diseñada para reparar defectos congénitos o que ocurren después de un accidente o enfermedad. La cirugía estética tiene como objetivo embellecer las formas del cuerpo humano.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

En virtud del artículo L. 4111-1 del Código de Salud Pública, para ejercer legalmente como médico en Francia, los interesados deben cumplir acumulativamente las tres condiciones siguientes:

- poseer el diploma estatal francés de doctor en medicina o un título o diploma que confiere el título de médico;
- nacionalidad francesa, ciudadanía andorrana o nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE) o Marruecos, con sujeción a la aplicación de las normas derivadas del Código de Salud compromisos públicos o internacionales. Sin embargo, esta condición no se aplica a un médico con el diploma estatal francés de doctor en medicina;
- ser incluidos en la Tabla del Colegio de Médicos (ver infra "5.0). a. Solicitar inclusión en la lista del Colegio de Médicos").

Sin embargo, las personas que no cumplan con los títulos de diploma o nacionalidad pueden ser autorizadas a ejercer como médicos por orden individual del Ministro responsable de la salud (véase infra "5o. c. Si es necesario, solicitar un permiso de ejercicio individual").

*Para ir más allá*: Artículos L. 4111-1, L. 4112-6, L. 4112-7 y L. 4131-1 del Código de Salud Pública.

**Tenga en cuenta que**

En caso de no cumplir todas estas condiciones, el ejercicio de la profesión de médico es ilegal y se castiga con dos años de prisión y una multa de 30.000 euros.

**Bueno saber: reconocimiento automático del diploma**

Con arreglo al artículo L. 4131-1 del Código de Salud Pública, los nacionales de la UE o del EEE podrán ejercer como médico si poseen uno de los siguientes títulos:

- documentos de formación médica expedidos por un Estado de la UE o del EEE de conformidad con las obligaciones de la UE y enumerados en el anexo del Decreto de 13 de julio de 2009 por el que se establecen listas y condiciones para el reconocimiento de la UE formación de médicos y médicos especialistas expedidos por los Estados miembros de la UE o partes en el Acuerdo EEE contemplado en el artículo L. 4131-1 del Código de Salud Pública;
- certificados de formación médica expedidos por un Estado de la UE o del EEE de conformidad con las obligaciones de la UE, no en la lista anterior, si van acompañados de un certificado de ese Estado que certifica que están sancionando la formación cumplir con estas obligaciones y son asimilados por él a los títulos de formación en esta lista;
- certificados de formación médica expedidos por un Estado de la UE o del EEE sancionando la formación médica iniciada en ese Estado antes de las fechas del decreto antes mencionado y no de conformidad con las obligaciones de la UE, si se acompañan un certificado de uno de estos Estados que certifique que el titular de los títulos de formación se ha dedicado, en ese Estado, de manera efectiva y legal, al ejercicio de la profesión de doctor en la especialidad de que se trate durante al menos tres años consecutivos en el Cinco años antes de la expedición del certificado;
- certificados de formación de médicos expedidos por la antigua Checoslovaquia, la antigua Unión Soviética o la antigua Yugoslavia o que sancionan la formación iniciada antes de la fecha de independencia de la República Checa, Eslovaquia, Estonia, Letonia, Lituania o Eslovenia, si van acompañados de un certificado de las autoridades competentes de uno de estos Estados que certifique que tienen la misma validez jurídica que los documentos de formación expedidos por dicho Estado. Este certificado va acompañado de un certificado expedido por las mismas autoridades que indica que el titular ha ejercido en dicho Estado, de manera efectiva y lícita, la profesión de médico en la especialidad de que se trate durante al menos tres años consecutivos en el Cinco años antes de la expedición del certificado;
- certificados de formación médica expedidos por un Estado de la UE o del EEE que no figuran en la lista anterior si van acompañados de un certificado expedido por las autoridades competentes de ese Estado que certifica que el titular del certificado de formación fue establecido en su territorio en la fecha establecida en el decreto antes mencionado y que haya adquirido el derecho a llevar a cabo las actividades de un médico general en virtud de su régimen nacional de seguridad social;
- certificados de formación médica expedidos por un Estado de la UE o del EEE sancionando la formación médica iniciada en ese Estado antes de las fechas del decreto antes mencionado y no de acuerdo con las obligaciones de la UE, pero permitiendo ejercer legalmente la profesión de médico en el estado que los expidió, si el médico justifica haber realizado en Francia en los cinco años anteriores tres años consecutivos a tiempo completo de funciones hospitalarias en la especialidad correspondiente formación como agregado asociado, profesional asociado, asistente asociado o funciones académicas como jefe asociado de clínica de universidades o asistente asociado de universidades, siempre que haya sido nombrado funciones hospitalarias al mismo tiempo;
- Certificados de formación médica especializados de Italia en la lista antes mencionada sancionando la formación médica especializada iniciada en ese estado después del 31 de diciembre de 1983 y antes del 1 de enero de 1991, si va acompañada de un certificado expedido por las autoridades estatales que indica que su titular ha ejercido en ese Estado, de manera efectiva y legal, la profesión de médico en la especialidad de que se trate durante al menos siete años consecutivos en los diez años anteriores emisión del certificado.

*Para ir más allá*: Artículo L. 4131-1 del Código de Salud Pública; 13 de julio de 2009, decreto por el que se establecen las listas y condiciones para el reconocimiento de los documentos de formación de médicos y médicos especialistas expedidos por los Estados miembros de la Unión Europea o partes en el Acuerdo sobre el Espacio Económico Europeo a que se refiere el Sección L. 4131-1 del Código de Salud Pública.

#### Entrenamiento

Los estudios médicos consisten en tres ciclos con una duración total de entre nueve y once años, dependiendo del curso elegido.

La formación, que se lleva a cabo en la universidad, incluye muchas prácticas y está puntuada por dos competiciones:

- la primera ocurre al final del primer año. Este año de estudio, llamado el "primer año de estudios comunes de salud" (PACES) es común a los estudiantes de medicina, farmacia, odontología, fisioterapia y parteras. Al final de esta primera competición, los estudiantes se clasifican de acuerdo con sus resultados. Aquellos en un rango útil bajo el numerus clausus se les permite continuar sus estudios y elegir, si es necesario, continuar la formación que conduce a la práctica de la medicina;
- el segundo ocurre al final del segundo ciclo (es decir, al final del sexto año de estudio): este concurso se denomina pruebas de clasificación nacional (ECN) o anteriormente "escuelas de embarque". Al final de esta competición, los estudiantes eligen, en función de su clasificación, su especialidad y/o su ciudad de asignación. La duración de los estudios siguientes varía en función de la especialidad elegida.

Para obtener un título estatal (DE) como doctor en medicina, el estudiante debe validar todas sus prácticas, su diploma de estudios especializados (DES) y apoyar su tesis con éxito.

*Para ir más allá*: Artículo L. 632-1 del Código de Educación.

**Es bueno saber**

Los estudiantes de medicina deben llevar a cabo vacunas obligatorias. Para obtener más información, consulte la Sección R. 3112-1 del Código de Salud Pública.

**Diploma de educación general en ciencias médicas**

El primer ciclo está sancionado por el diploma de formación general en ciencias médicas. Consta de seis semestres y corresponde al nivel de licencia. Los dos primeros semestres corresponden a THE PACES.

El objetivo de la formación es:

- la adquisición de los conocimientos científicos básicos, esenciales para el posterior dominio de los conocimientos y conocimientos técnicos necesarios para el ejercicio de las profesiones médicas. Esta base científica es amplia y abarca la biología, ciertos aspectos de las ciencias exactas y varias disciplinas de las humanidades y las ciencias sociales;
- el enfoque fundamental del hombre sano y del hombre enfermo, incluyendo todos los aspectos de la semiología. Incluye enseñanzas teóricas, metodológicas, aplicadas y prácticas, así como la realización de pasantías, incluyendo un curso de atención introductoria de cuatro semanas en un hospital.

*Para ir más allá* : orden de 22 de marzo de 2011 relativo al esquema de educación para el diploma de formación general en ciencias médicas.

**Grado de formación en profundidad en ciencias médicas**

Este curso de tres años está disponible para el titular de un título general de ciencias médicas. Su objetivo es permitir al candidato adquirir los conocimientos necesarios para ejercer en un hospital o ambulatorio.

Las habilidades a adquirir son las de comunicador, clínico, cooperador, miembro de un equipo de atención médica multiprofesional, actor de salud pública, científico y líder ético y ético. El estudiante también debe aprender a ser reflexivo.

Las lecciones se centran en lo que es común o grave, lo que constituye un problema de salud pública y lo que es clínicamente ejemplar.

Los objetivos de la formación son:

- la adquisición de conocimientos sobre procesos fisiopatológicos, patología, bases terapéuticas y prevención que complementen y profundicen los adquiridos durante el ciclo anterior;
- formación en el proceso científico
- Aprendizaje del razonamiento clínico
- habilidades genéricas para prepararse para los estudios médicos de posgrado.

El título de ciencias médicas en profundidad se otorga al candidato que ha validado todas las unidades docentes y que ha obtenido el certificado de competencia clínica.

*Para ir más allá* : orden de 8 de abril de 2013 relativo al régimen educativo para el primer y segundo ciclo de estudios.

**Diploma de Estudios Especializados (DES) en Cirugía Plástica, Reconstructiva y Estética**

Para ejercer como cirujano especializado, el profesional debe obtener el DES de cirugía plástica, reconstructiva y estética.

**Tenga en cuenta que**

Los estudiantes solo pueden asistir a NCT dos veces.

Este diploma permite al futuro especialista adquirir los conocimientos y la práctica necesarios para llevar a cabo sus funciones.

Este diploma consta de doce semestres que incluyen:

- al menos ocho en la especialidad elegida;
- al menos cinco en una pasantía con supervisión universitaria;
- dos en un lugar de prácticas sin supervisión universitaria.

**Tenga en cuenta que**

Como parte de su proyecto profesional, el futuro cirujano puede solicitar capacitación transversal especializada (FST), especialmente en cirugía de manos.

La formación del DES se divide en tres fases: la fase base, la fase de profundización y la fase de consolidación.

**La fase base**

Con una duración de dos semestres, permite al candidato adquirir los conocimientos básicos de la especialidad elegida y realizar:

- una pasantía en un hospital con licencia en cirugía plástica, reconstructiva y estética;
- una pasantía en un hospital autorizado en cirugía plástica, reconstructiva y estética y principalmente en cirugía visceral y digestiva o cirugía pediátrica o cirugía vascular o cirugía o cirugía o cirugía ortopédico y traumatológico.

Una de estas pasantías debe realizarse en un entorno hospitalario con supervisión universitaria.

**La fase de profundización**

Con una duración de seis semestres, permite al candidato adquirir los conocimientos y habilidades necesarios para practicar la especialidad elegida y realizar:

- tres pasantías en un hospital autorizado en cirugía plástica, reconstructiva y estética;
- dos pasantías en un hospital autorizado como principal o complementario en cirugía plástica;
- una pasantía gratuita.

**La fase de consolidación**

Con una duración de dos años, permite al candidato consolidar sus conocimientos y completar dos prácticas de un año en un hospital con licencia en capacidad primaria en cirugía plástica, reconstructiva y estética. Una de estas prácticas debe realizarse en un lugar con supervisión universitaria.

El DES es elegible para la calificación de especialista en cirugía plástica.

*Para ir más allá* :[Apéndices I y II](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000036237037) del decreto de 27 de noviembre de 2017 por el que se modifica el decreto de 12 de abril de 2017 relativo a la organización del tercer ciclo de estudios médicos y al decreto de 21 de abril de 2017 relativo a los conocimientos, habilidades y modelos de formación de los diplomas de estudio y establecer la lista de estos diplomas y las opciones especializadas transversales y la formación de los estudios médicos de postgrado

#### Costos asociados con la calificación

La formación que conduce al título de médico se paga y el costo varía dependiendo de la institución elegida. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

El profesional que sea miembro de un Estado de la UE o un Estado parte en el acuerdo EEE que se establezca y realice legalmente su actividad de cirugía plástica en uno de estos Estados puede realizar la misma actividad en Francia de forma temporal y ocasional. , sin figurar en el Colegio de Médicos.

Para ello, el profesional debe hacer una declaración previa, así como una declaración que justifique que tiene las habilidades de lenguaje necesarias para ejercer en Francia (ver infra "5o. b. Predeclaración con el fin de ejercer un LPS").

**Qué saber**

La inscripción en la lista del Colegio de Médicos no es necesaria para el profesional de servicio gratuito (LPS). Por lo tanto, no está obligado a pagar cuotas ordinales. El médico está simplemente registrado en una lista específica mantenida por el Consejo Nacional del Colegio de Médicos.

La predeclaración debe ir acompañada de una declaración sobre las habilidades del idioma necesarias para llevar a cabo el servicio. En este caso, el control del dominio del idioma debe ser proporcional a la actividad que debe llevarse a cabo y llevarse a cabo una vez reconocida la cualificación profesional.

Cuando los títulos de formación no reciben reconocimiento automático (véase supra "2.0). a. Legislación nacional"), las cualificaciones profesionales del proveedor se comprueban antes de que se preste el primer servicio. En caso de diferencias sustanciales entre las cualificaciones del interesado y la formación requerida en Francia que pueda perjudicar a la salud pública, el demandante será sometido a una prueba.

El médico en situación LPS está obligado a respetar las normas profesionales aplicables en Francia, incluidas todas las normas éticas (véase infra "3." Condiciones de honorabilidad, reglas éticas, ética"). Está sujeto a la jurisdicción disciplinaria del Colegio de Médicos.

**Tenga en cuenta que**

El servicio se realiza bajo el título profesional francés de médico. Sin embargo, cuando no se reconocen las cualificaciones de formación y no se han verificado las cualificaciones, el rendimiento se lleva a cabo bajo el título profesional del Estado de establecimiento, con el fin de evitar confusiones Con el título profesional francés.

*Para ir más allá*: Artículo L. 4112-7 del Código de Salud Pública.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

#### El reconocimiento automático de los diplomas obtenidos en un Estado de la UE

El artículo L. 4131-1 del Código de Salud Pública establece un régimen de reconocimiento automático en Francia de determinados títulos o títulos, en su caso, acompañados de certificados, obtenidos en un Estado de la UE o del EEE (véase más arriba "2". a. Legislación Nacional").

Corresponde al consejo departamental del colegio de médicos responsable de verificar la regularidad de diplomas, títulos, certificados y certificados, conceder el reconocimiento automático y luego decidir sobre la solicitud de inscripción en la lista de la Orden. .

*Para ir más allá*: Artículo L. 4131-1 del Código de Salud Pública; 13 de julio de 2009, decreto por el que se establecen las listas y condiciones para el reconocimiento de los documentos de formación de médicos y médicos especialistas expedidos por los Estados miembros de la Unión Europea o partes en el Acuerdo sobre el Espacio Económico Europeo a que se refiere el Sección L. 4131-1 del Código de Salud Pública.

#### El régimen derogatorio: autorización previa

Si el nacional de la UE o del EEE no reúne los requisitos para el reconocimiento automático de sus credenciales, está comprendido en un régimen de autorización (véase más adelante "5o). c. Si es necesario, solicitar un permiso de ejercicio individual").

Las personas que no reciben reconocimiento automático pero que poseen una designación de capacitación para ejercer legalmente como médico pueden ser autorizadas individualmente a ejercer en la especialidad Ministro de Salud, previa asesoría de una comisión formada por profesionales.

Si el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia profesional muestra diferencias sustanciales con las cualificaciones requeridas para acceder a la profesión en la especialidad de que se trate y su ejercicio en Francia, el interesado debe someterse a una medida de indemnización.

En función del nivel de cualificación exigido en Francia y del que posea el interesado, la autoridad competente podrá:

- Ofrecer al solicitante la opción de elegir entre un curso de ajuste o una prueba de aptitud;
- imponer un curso de ajuste o una prueba de aptitud
- imponer un curso de ajuste y un alcierto.

*Para ir más allá*: Artículo L. 4131-1-1 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Cumplimiento del Código de ética de los médicos

Las disposiciones del Código de ética médica son obligatorias para todos los médicos que practican en Francia, ya sea que estén registrados en el consejo de la Orden o estén exentos de esta obligación (véase supra "5". a. Solicitar inclusión en la lista del Colegio de Médicos").

**Qué saber**

Todas las disposiciones del Código de ética están codificadas en las secciones R. 4127-1 a R. 4127-112 del Código de Salud Pública.

Como tal, el médico debe respetar los principios de moralidad, probidad y dedicación esenciales para la práctica de la medicina. También está sujeto al secreto médico y debe ejercer de forma independiente.

*Para ir más allá*: Artículos R. 4127-1 a R. 4127-112 del Código de Salud Pública.

### b. Actividades acumulativas

El médico sólo puede participar en cualquier otra actividad si tal combinación es compatible con los principios de independencia profesional y dignidad que se le imponen. La acumulación de actividades no debe permitirle aprovechar sus recetas o su consejo médico.

Por lo tanto, el médico no puede combinar el ejercicio médico con otra actividad cercana al campo de la salud. En particular, se le prohíbe ejercer como óptico, paramédico o gerente de una empresa de ambulancias, fabricante o vendedor de dispositivos médicos, propietario o gerente de un hotel para curadores, gimnasio, spa, práctica de masaje.

Del mismo modo, se prohíbe a un médico que cumpla un mandato electivo o administrativo usarlo para aumentar su clientela.

*Para ir más allá*: Artículos R. 4127-26 y R. 4127-27 del Código de Salud Pública.

### c. Condiciones de honorabilidad

Para ejercer, el médico debe certificar que ningún procedimiento que pueda dar lugar a una condena o una sanción que pueda afectar su inclusión en la junta está en su contra.

*Para ir más allá*: Artículo R. 4112-1 del Código de Salud Pública.

### d. Obligación para el desarrollo profesional continuo

Los médicos deben participar en un programa de desarrollo profesional continuo de varios años. El programa se centra en evaluar las prácticas profesionales, mejorar las habilidades, mejorar la calidad y la seguridad de la atención, mantener y actualizar los conocimientos y habilidades.

Todas las acciones llevadas a cabo por los médicos bajo su obligación de desarrollarse profesionalmente se remontan a un documento específico que acredite el cumplimiento de esta obligación.

*Para ir más allá*: Artículos L. 4021-1 y artículos subsiguientes R. 4021-4 y los siguientes del Código de Salud Pública.

### e. Aptitud física

Los médicos no deben presentar enfermedades o patologías incompatibles con el ejercicio de la profesión (ver infra "5.0). a. Solicitud de inclusión en la lista del Colegio de Médicos").

*Para ir más allá*: Artículo R. 4112-2 del Código de Salud Pública.

4°. Seguro
-------------------------------

### a. Obligación de constete de un seguro de responsabilidad civil profesional

Como profesional de la salud, un cirujano plástico, si practica a nivel liberal, debe tomar un seguro de responsabilidad civil profesional.

Si ejerce como empleado, corresponde al empleador obtener dicho seguro para sus empleados por los actos realizados durante esta actividad.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

### b. Obligación de adherirse al fondo de pensiones independiente de médicos de Francia (CARMF)

Cualquier médico inscrito en el consejo de la Orden y que actúe en forma liberal (incluso a tiempo parcial e incluso si también está empleado) tiene la obligación de adherirse a la CARMF.

#### hora

El interesado deberá registrarse en la CARMF en el plazo de un mes a partir del inicio de su actividad liberal.

#### Términos

El interesado deberá devolver el formulario de declaración, cumplimentado, fechado y contrafirmado por el consejo departamental del Colegio de Médicos. Este formulario se puede descargar desde el[CARMF](http://www.carmf.fr/).

**Qué saber**

En el caso de una práctica en una empresa de práctica liberal (SEL), la membresía en el CARMF también es obligatoria para todos los socios profesionales que ejercen allí.

### c. Obligación de Informes de Seguros Médicos

Una vez en la lista de la Orden, el médico que practice en forma liberal debe declarar su actividad con el Fondo de Seguro de Salud Primaria (CPAM).

#### Términos

El registro en el CPAM se puede hacer en línea en el sitio web oficial de Medicare.

#### Documentos de apoyo

El solicitante de registro debe proporcionar un archivo completo que incluya:

- Copiar una identificación válida
- un declaración de identidad bancaria profesional (RIB)
- si es necesario, el título (s) para permitir el acceso al Sector 2.

Para obtener más información, consulte la sección sobre la instalación de médicos en el sitio web del seguro médico.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de inclusión en la Mesa del Colegio de Médicos

La inscripción en el consejo de la Orden es obligatoria para llevar a cabo legalmente la actividad de un médico en Francia.

La inscripción en el tablero de la Orden no se aplica:

- Nacionales de la UE o del EEE establecidos y que estén legalmente practicando como médicos en un Estado miembro o parte, cuando realicen actos de su profesión de forma temporal y ocasional en Francia (véase supra "2o. b. Nacionales de la UE: para el ejercicio temporal y ocasional);
- médicos pertenecientes a los ejecutivos activos del Servicio de Salud de las Fuerzas Armadas;
- médicos que, con la condición de servidor público o agente titular de una autoridad local, no están llamados, en el ejercicio de sus funciones, a ejercer la medicina.

*Para ir más allá*: Artículos L. 4112-5 a L. 4112-7 del Código de Salud Pública.

**Tenga en cuenta que**

El registro en el consejo de la Orden permite la emisión automática y gratuita de la Tarjeta Profesional de La Salud (CPS). El CPS es un documento electrónico de identidad comercial. Está protegido por un código confidencial y contiene, entre otras cosas, los datos de identificación del médico (identidad, ocupación, especialidad). Para obtener más información, se recomienda consultar el sitio web del gobierno[Agencia Francesa de Salud Digital](http://esante.gouv.fr/).

#### Autoridad competente

La solicitud de inscripción se dirige al Presidente del Consejo del Colegio de Médicos del Departamento en el que el interesado desea establecer su residencia profesional.

La solicitud puede ser presentada directamente al consejo departamental de la Orden en cuestión o enviada a él por correo certificado con solicitud de notificación de recepción.

*Para ir más allá*: Artículo R. 4112-1 del Código de Salud Pública.

**Qué saber**

En el caso de un traslado de su residencia profesional fuera del departamento, el profesional está obligado a solicitar su expulsión de la orden del departamento donde estaba practicando y su inscripción por orden de su nueva residencia profesional.*Para ir más allá*: Artículo R. 4112-3 del Código de Salud Pública.

#### Procedimiento

Una vez recibida la solicitud, el consejo del condado nombra a un ponente que lleva a cabo la solicitud y hace un informe por escrito.

La junta verifica los títulos del candidato y solicita la divulgación del boletín 2 de los antecedentes penales del solicitante. En particular, verifica que el candidato:

- cumple las condiciones necesarias de moralidad e independencia (véase supra "3.3. c. Condiciones de honorabilidad");
- cumple con los requisitos de competencia necesarios;
- no presenta una discapacidad o condición patológica incompatible con el ejercicio de la profesión (véase supra "3." e. Aptitud física").

En caso de serias dudas sobre la competencia profesional del solicitante o la existencia de una discapacidad o condición patológica incompatible con el ejercicio de la profesión, el consejo de condado remite el asunto al consejo regional o interregional Experiencia. Si, en opinión del informe pericial, existe una insuficiencia profesional que hace peligrosa el ejercicio de la profesión, el consejo departamental deniega el registro y especifica las obligaciones de formación del profesional.

No se puede tomar ninguna decisión de rechazar el registro sin que la persona sea invitada con al menos una quincena de antelación por una carta recomendada solicitando que se presente un aviso de recepción para explicarlo ante la Junta.

La decisión del Consejo Universitario se notifica en el plazo de una semana al individuo, al Consejo Nacional del Colegio de Médicos y al Director General de la Agencia Regional de Salud (ARS). La notificación es por carta recomendada con solicitud de notificación de recepción.

La notificación menciona los recursos contra la decisión. La decisión de rechazar debe estar justificada.

*Para ir más allá*: Artículos R. 4112-2 y R. 4112-4 del Código de Salud Pública.

#### hora

El Presidente reconoce haber recibido el expediente completo en el plazo de un mes a partir de su registro.

El consejo departamental del Colegio debe decidir sobre la solicitud de inscripción en un plazo de tres meses a partir de la recepción del expediente completo de solicitud. Si no se responde a una respuesta dentro de este plazo, la solicitud de registro se considera rechazada.

Este período se incrementa a seis meses para los nacionales de terceros países cuando se llevará a cabo una investigación fuera de la Francia metropolitana. A continuación, se notifica al interesado.

También puede prorrogarse por un período de no más de dos meses por el consejo departamental cuando se haya ordenado un dictamen pericial.

*Para ir más allá*: Artículos L. 4112-3 y R. 4112-1 del Código de Salud Pública.

#### Documentos de apoyo

El solicitante debe presentar un expediente de solicitud completo que incluya:

- dos copias del cuestionario estandarizado cumplimentado, fechado y firmado, acompañado de un documento de identidad con fotografía. Este cuestionario está disponible en los consejos departamentales del Colegio o se puede descargar directamente desde el[sitio web oficial](https://www.conseil-national.medecin.fr/) Consejo Nacional del Colegio de Médicos;
- Una fotocopia de un documento de identidad válido o, en su caso, un certificado de nacionalidad expedido por una autoridad competente;
- En su caso, una fotocopia de la tarjeta de residencia familiar de un ciudadano de la UE válido, la tarjeta válida de residente-EC de larga duración o la tarjeta de residente con estatus de refugiado válido;
- Si es así, una fotocopia de la tarjeta de crédito europea válida;
- una copia, acompañada si es necesario por una traducción de un traductor certificado, de los cursos de formación a los que se adjuntan:- cuando el solicitante sea nacional de la UE o del EEE, el certificado o certificado sinvisado (véase más arriba "2. a. Requisitos nacionales");
  - solicitante recibe un permiso de ejercicio individual (véase supra "2. c. Nacionales de la UE: para un ejercicio permanente"), una copia de esta autorización;
  - Cuando el solicitante presente un diploma expedido en un Estado extranjero cuya validez se reconozca en territorio francés, la copia de los títulos a los que pueda subordinarse dicho reconocimiento;
- nacionales de un Estado extranjero, un extracto de antecedentes penales o un documento equivalente de menos de tres meses de edad, expedido por una autoridad competente del Estado de origen. Esta parte podrá sustituirse, para los nacionales de la UE o del EEE que requieran prueba de moralidad o honorabilidad para el acceso a la actividad médica, por un certificado de menos de tres meses de edad, establecido por la autoridad competente de El Estado de origen que certifica que se cumplen estas condiciones morales o de honor;
- una declaración sobre el honor del solicitante que certifique que ningún procedimiento que pudiera dar lugar a una condena o sanción que pudiera afectar a la lista en la junta está en su contra;
- un certificado de baja de la lista, registro o registro expedido por la autoridad con la que el solicitante fue previamente registrado o registrado o, en su defecto, una declaración de honor del solicitante que certifique que nunca fue registrado o registrado o, en su defecto, un certificado de registro o registro en un Estado de la UE o del EEE;
- todas las pruebas de que el solicitante tiene las habilidades de idiomas necesarias para ejercer la profesión;
- Un CV
- contratos y endosos para el ejercicio de la profesión, así como los relativos al uso del equipo y de los locales en los que ejerce el solicitante;
- Si la actividad se lleva a cabo en forma de SEL o de una sociedad civil profesional (SCP), los estatutos de esa sociedad y sus posibles avales;
- Si el solicitante es un servidor público o un funcionario público, el orden de nombramiento;
- si el solicitante es profesor de universidades - médico hospitalario (PU-PH), profesor universitario - médico hospitalario (MCU-PH) u practicante hospitalario (PH), el orden de nombramiento como practicante hospitalario y, si es necesario, el decreto u orden de nombramiento como profesor universitario o profesor en las universidades.

Para obtener más información, visite el sitio web oficial del Consejo Nacional del Colegio de Médicos.

*Para ir más allá*: Artículos L. 4113-9 y R. 4112-1 del Código de Salud Pública.

#### Remedios

El solicitante o el Consejo Nacional del Colegio de Médicos podrá impugnar la decisión de registrarse o denegar el registro dentro de los 30 días siguientes a la notificación de la decisión o la decisión implícita de rechazarla. El recurso se interpone ante el consejo regional territorialmente competente.

El consejo regional debe decidir en el plazo de dos meses a partir de la recepción de la solicitud. En ausencia de una decisión dentro de este plazo, el recurso se considera desestimado.

La decisión del consejo regional también está sujeta a apelación, en un plazo de 30 días, ante el Consejo Nacional del Colegio de Médicos. La propia decisión puede ser apelada ante el Consejo de Estado.

*Para ir más allá*: Artículos L. 4112-4 y R. 4112-5 del Código de Salud Pública.

#### Costo

La inscripción en el consejo de la Universidad es gratuita, pero crea la obligación de pagar las cuotas ordinales obligatorias, cuyo importe se establece anualmente y que debe pagarse en el primer trimestre del año calendario en curso. El pago se puede hacer en línea en el sitio web oficial del Consejo Nacional del Colegio de Médicos. Como indicación, en 2017, el importe de esta contribución asciende a 333 euros.

*Para ir más allá*: Artículo L. 4122-2 del Código de Salud Pública.

### b. Predeclaración para ejercicio temporal e informal (LPS)

#### Autoridad competente

Antes de cualquier prestación de servicios, el nacional de un Estado miembro de la UE o del EEE debe, cada año, enviar una declaración al Consejo Nacional de la Orden de la Profesión.

#### Condiciones de presentación de informes y recepción

La declaración puede enviarse por correo o directamente en línea en el sitio web oficial del Colegio de Médicos.

Cuando el Consejo Nacional del Colegio de Médicos recibe la declaración y todos los documentos justificativos necesarios, envía al demandante un recibo especificando su número de registro y disciplina.

**Tenga en cuenta que**

El prestador de servicios informa a la agencia nacional de seguros de salud pertinente de su prestación de servicios mediante el envío de una copia del recibo o por cualquier otro medio.

*Para ir más allá*: Artículos R. 4112-9-2 y R. 4112-11 del Código de Salud Pública.

#### Documentos de apoyo

La declaración debe incluir:

- el[formulario de declaración](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=83B6B69A79BA3CA1D089890DFF32CC1E.tplgfr34s_3?idArticle=LEGIARTI000036145868&cidTexte=LEGITEXT000036145857&dateTexte=20180115) Completado y firmado;
- Copia de una documento de identificación válido o cualquier documento que acredite la nacionalidad del solicitante;
- Copia del título de formación y la denominación de formación especializada que permite al solicitante llevar a cabo su actividad;
- un certificado de menos de tres meses que justifique que el profesional está legalmente establecido y no está sujeto a ninguna prohibición de ejercer, ni siquiera con carácter temporal, su profesión;
- cuando el profesional haya adquirido su formación en un tercer Estado y reconocido en un Estado de la UE o del EEE:- Reconocimiento de estas credenciales de formación básicas y especializadas,
  - cualquier documentación que justifique que ha estado practicando durante tres años a tiempo completo o a tiempo parcial;
- si es así, una copia de la declaración antes mencionada.

*Para ir más allá*: Artículo R. 4112-9 y lo siguiente del Código de Salud Pública; 4 de diciembre de 2017 sobre la declaración previa de prestación de servicios para las profesiones médicas y farmacéuticos.

#### hora

En el plazo de un mes a partir de la recepción de la declaración, el Consejo Nacional de la Orden informa al solicitante:

- Si puede o no comenzar a prestar servicios;
- cuando la verificación de las cualificaciones profesionales muestra una diferencia sustancial con la formación requerida en Francia, debe demostrar haber adquirido los conocimientos y habilidades que faltan Aptitud. Si cumple con este cheque, se le informa en el plazo de un mes que puede comenzar la prestación de servicios;
- cuando la revisión del archivo resalta una dificultad que requiere más información, las razones del retraso en la revisión del archivo. Luego tiene un mes para obtener la información adicional solicitada. En este caso, antes de que finalice el segundo mes a partir de la recepción de esta información, el Consejo Nacional informa al demandante, tras revisar su expediente:- si puede o no comenzar la prestación de servicios,
  - cuando la verificación de las cualificaciones profesionales del demandante muestre una diferencia sustancial con la formación requerida en Francia, debe demostrar que ha adquirido los conocimientos y habilidades que faltan, sujeto a una prueba de aptitud.

En este último caso, si cumple con este control, se le informa en el plazo de un mes que puede iniciar la prestación de servicios. De lo contrario, se le informa de que no puede comenzar la prestación de servicios.

En ausencia de una respuesta del Consejo Nacional de la Orden dentro de estos plazos, la prestación de servicios puede comenzar.

*Para ir más allá*: Artículo R. 4112-9-1 del Código de Salud Pública.

### c. Si es necesario, solicitar autorización individual para ejercer

#### Para nacionales de la UE o del EEE

Los nacionales de la UE o del EEE con licencia de formación podrán solicitar una autorización individual:

- emitido por uno de estos Estados que no se beneficia del reconocimiento automático (véase supra "2.0). c. Nacionales de la UE y del EEE: para un ejercicio permanente);00
- emitidos por un tercer Estado pero reconocidos por un Estado miembro de la UE o del EEE, siempre que justifiquen la práctica como médico en la especialidad durante un período equivalente a tres años a tiempo completo en ese Estado miembro.

Una Junta de Autorización (CAE) revisa la capacitación y la experiencia laboral del solicitante.

Puede proponer una medida de compensación:

- cuando la formación sea al menos un año inferior a la del ED francés, cuando abarque temas sustancialmente diferentes, o cuando uno o más componentes de la actividad profesional cuyo ejercicio esté sujeto al citado diploma no existen en la profesión correspondiente en el Estado miembro de origen o no se han enseñado en ese Estado;
- formación y experiencia del solicitante no es probable que cubra estas diferencias.

En función del nivel de cualificación exigido en Francia y del que posea el interesado, la autoridad competente podrá:

- Ofrecer al solicitante la opción de elegir entre un curso de ajuste o una prueba de aptitud;
- requieren un curso de ajuste o una prueba de aptitud.

el**prueba de aptitud** tiene por objeto verificar, mediante pruebas escritas u orales o ejercicios prácticos, la aptitud del solicitante para ejercer como médico en la especialidad pertinente. Se ocupa de temas que no están cubiertos por la formación o experiencia del solicitante.

el**curso de adaptación** tiene por objeto permitir a las partes interesadas adquirir las competencias necesarias para ejercer la profesión de médico. Se lleva a cabo bajo la responsabilidad de un médico y puede ir acompañado de formación teórica adicional opcional. La duración de la pasantía no exceda de tres años. Se puede hacer a tiempo parcial.

*Para ir más allá*: Artículos L. 4111-2 II, L. 4131-1-1, R. 4111-17 a R. 4111-20 y R. 4131-29 del Código de Salud Pública.

#### Para nacionales de un tercer estado

Podrá aplicarse una autorización individual para ejercer, siempre que justifiquen un nivel suficiente de competencia en el idioma francés, personas con un título de formación:

- por un Estado de la UE o del EEE cuya experiencia sea atestiguada por cualquier medio;
- emitido por un tercer estado que permite el ejercicio de la profesión de médico en el país de graduación:- si cumplen con pruebas anónimas para verificar los conocimientos básicos y prácticos. Para obtener más información sobre estos eventos, visite el sitio web oficial del Centro Nacional de Gestión (NMC),
  - y si justifican tres años de funciones en un departamento u organización acreditado para la formación de pasantes.

**Tenga en cuenta que**

Se considera que los médicos con un título especializado obtenido como parte de las prácticas extranjeras cumplen con las pruebas de verificación de conocimientos.

*Para ir más allá*: Artículos L. 4111-2 (I y I bis), D. 4111-1, D. 4111-6 y R. 4111-16-2 del Código de Salud Pública.

#### Autoridad competente

La solicitud se dirige en dos copias, por carta recomendada con solicitud de notificación de recepción a la unidad responsable de las comisiones de autorización de ejercicio (CAE) del NMC.

La autorización para ejercer es expedida por el Ministro responsable de la salud tras la notificación de la CEA.

*Para ir más allá*: Artículos R. 4111-14 y R. 4131-29 del Código de Salud Pública; decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las CAE competentes para el examen de las solicitudes presentadas para el ejercicio en Francia de las profesiones de médico, cirujano dental, partera y farmacéutico.

#### hora

El NMC confirma la recepción de la solicitud en el plazo de un mes a partir de la recepción.

El silencio guardado durante un cierto período de tiempo a partir de la recepción del expediente completo merece la decisión de desestimar la solicitud. Este plazo es:

- cuatro meses para las solicitudes de nacionales de la UE o del EEE con un título de uno de estos Estados;
- seis meses para las solicitudes de terceros nacionales con un diploma de un Estado de la UE o del EEE;
- un año para otras aplicaciones. Este plazo podrá prorrogarse por dos meses, mediante decisión de la autoridad ministerial notificada a más tardar un mes antes de la expiración de esta última, en caso de dificultad grave para evaluar la experiencia profesional del candidato.

*Para ir más allá*: Artículos R. 4111-2, R. 4111-14 y R. 4131-29 del Código de Salud Pública.

#### Documentos de apoyo

El archivo de solicitud debe contener:

- un formulario de solicitud de autorización para ejercer la profesión, basado en la Lista 1 de la Orden de 25 de febrero de 2010, cumplimentado, fechado y firmado, mostrando, en su caso, la especialidad en la que el solicitante presenta;
- Una fotocopia de un documento de identidad válido
- Una copia del título de formación que permita el ejercicio de la profesión en el estado de obtención, así como, en su caso, una copia del título de formación especializada;
- Si es necesario, una copia de los diplomas adicionales;
- cualquier prueba útil que justifique la formación continua, la experiencia y las habilidades adquiridas durante el ejercicio profesional en un Estado de la UE o del EEE, o en un tercer estado (certificados de funciones, informe de actividad, evaluación operativa, etc. ) ;
- en el contexto de funciones desempeñadas en un Estado distinto de Francia, una declaración de la autoridad competente de dicho Estado, de menos de un año de edad, que acredite la ausencia de sanciones contra el solicitante.

Dependiendo de la situación del solicitante, se requiere documentación adicional de apoyo. Para obtener más información, visite el sitio web oficial del NMC.

**Qué saber**

Los documentos de apoyo deben estar escritos en francés o traducidos por un traductor certificado.

*Para ir más allá* : decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización competentes para el examen de las solicitudes presentadas para la práctica en Francia de las profesiones de médico, cirujano dental, partera y farmacéutico; 17 de noviembre de 2014 no DGOS/RH1/RH2/RH4/2014/318.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

