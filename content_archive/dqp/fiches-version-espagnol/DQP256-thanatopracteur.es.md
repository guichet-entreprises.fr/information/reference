﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP256" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Servicios funerarios" -->
<!-- var(title)="Thanatopractor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="servicios-funerarios" -->
<!-- var(title-short)="thanatopractor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/servicios-funerarios/thanatopractor.html" -->
<!-- var(last-update)="2020-04-15 17:22:30" -->
<!-- var(url-name)="thanatopractor" -->
<!-- var(translation)="Auto" -->


Thanatopractor
==============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:30<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El thanatopracteur es un profesional de posgrado obligatorio que tiene la actividad de realizar cuidados de conservación también llamados cuidados de la thanatopraxia. Este cuidado son operaciones funerarias reguladas. Estos son actos invasivos*Mortem* que son drenados por fluidos y gases del cuerpo e inyectando un producto biocida como reemplazo. Su propósito es retrasar el proceso de descomposición del cuerpo y su degradación.

*Para ir más allá* :[Antecedentes familiares sobre cuidado sin conservación](https://solidarites-sante.gouv.fr/IMG/pdf/information_aux_familles_sur_les_soins_de_conservation_040118.pdf).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El acceso a la profesión está reservado al titular del diploma nacional de thanatopracteur.

*Para ir más allá*: Artículos D. 2223-123 y siguientes del Código General de Autoridades Locales; decreto de 18 de mayo de 2010 por el que se establecen las condiciones para organizar la formación y el examen del acceso al diploma nacional de thanatopracteur.

#### Entrenamiento

La formación que conduce a la profesión de thanatopracteur se imparte en un centro de formación declarado a la Direccte territorialmente competente.

La formación teórica en cuidados de la conservación debe ser de un mínimo de 195 horas. La formación práctica debe ser seguida por el candidato durante un mínimo de doce meses consecutivos y consiste en la finalización de cien cuidados de conservación.

Al final de la formación, el candidato tendrá que aprobar un examen para obtener el diploma nacional de thanatopracteur.

Este examen se divide en dos pruebas. El primero, teórico, consiste en un examen escrito. El segundo, práctico, toma la forma de una evaluación del candidato en el campo de su pasantía. El acceso a la formación práctica está sujeto a un numerus clausus establecido por decreto de los ministros responsables de la salud y el interior cada año.

**Tenga en cuenta que**

Es imperativo que la persona en formación sea vacunada contra la hepatitis B.

*Para ir más allá*: Artículo D. 2223-132 del Código de Autoridades Locales y Artículos L. 3111-4-1 del Código de Salud Pública.

#### Costos asociados con la calificación

Se paga la formación que conduzca al diploma thanatopracteur. También es obligatorio poder inscribirse en el examen nacional de ingreso al diploma thanatopracteur. Para más información, es aconsejable acercarse a las agencias que lo dispensan.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o parte en el Espacio Económico Europeo (EEE) podrá ejercer la actividad de thanatopracteur en Francia, de forma temporal y ocasional si cumple una de las siguientes condiciones:

- Estar legalmente establecidos en ese Estado para llevar a cabo la misma actividad;
- han estado en esta actividad durante un año en los últimos diez años antes de la entrega cuando ni la formación ni la actividad están reguladas en ese estado;
- han recibido el reconocimiento de sus cualificaciones profesionales por la prefectura competente.

*Para ir más allá*: Artículos L. 2223-23, L. 2223-47 y R. 2223-133 y siguientes del Código General de Gobierno Local.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Todo nacional de un Estado de la UE o del EEE establecido y que practique legalmente actividad de thanatopracteur en ese Estado podrá llevar a cabo la misma actividad en Francia de forma permanente si:

- posee un título expedido por una autoridad competente de un Estado miembro, que regula el acceso a la profesión o a su ejercicio;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Para ello, tendrá que solicitar el reconocimiento de sus cualificaciones profesionales desde el prefecto del lugar de instalación (véase infra "4o. a. Solicitar el reconocimiento de las cualificaciones profesionales de la UE o del EEE para un ejercicio permanente (LE) ").

Si, durante el examen de la solicitud, el prefecto constata que existen diferencias sustanciales entre la formación y la experiencia profesional del nacional y las requeridas en Francia, se pueden adoptar medidas de compensación (véase infra "4o. a. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos L. 2223-49 y R. 2223-134 del Código de Autoridades Locales.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Aunque las obligaciones no reguladas, éticas y éticas son responsabilidad de los thanatopractators, incluyendo:

- Respetar la dignidad y la integridad física del difunto;
- Respetar el secreto profesional
- ejercer su actividad con la misma conciencia sobre todos los difuntos.

*Para ir más allá*: Artículo R. 2223-132 del Código General de Autoridades Locales.

4°. Procedimientos y formalidades de reconocimiento de cualificación
-----------------------------------------------------------------------------------------

### a. Solicitar el reconocimiento de las cualificaciones profesionales de los nacionales de la UE o del EEE para el ejercicio permanente (LE)

**Autoridad competente**

El prefecto del lugar de instalación del nacional es competente para decidir sobre la solicitud de reconocimiento de cualificaciones profesionales.

**Documentos de apoyo**

Esta solicitud se realiza enviando un archivo al prefecto incluyendo todos los siguientes documentos:

- Una copia de los títulos, certificados y otros documentos de formación expedidos por la autoridad competente de dicho Estado cuando el Estado miembro regule el acceso a la profesión;
- un certificado o documento de formación que justifique que el nacional ha estado practicando durante un año en ese Estado, cuando el Estado miembro no regula la profesión o la actividad.

**Procedimiento**

El prefecto reconoce la recepción del expediente en el plazo de un mes e informa al nacional, si es necesario, de los documentos que falten. Las piezas son revisadas por tres personas cualificadas, incluyendo un thanatopracteur. Al final de esta auditoría, el prefecto y el ministro responsable de la salud podrán decidir someter al nacional a una medida de compensación en caso de diferencias sustanciales entre su cualificación profesional y la Francia.

El prefecto dispondrá de cuatro meses para decidir si concede o no la calificación al nacional. En caso de compensación, este período se suspenderá hasta la finalización del curso de ajuste o la prueba de aptitud.

**Bueno saber: medidas de compensación**

La elección de la medida de compensación queda a discreción del nacional que tiene un mes para decidirse.

El curso de adaptación, de hasta dos años de duración, debe realizarse en una asociación autorizada de gestión, empresa o funeraria. Será evaluado por el administrador de pasantías y luego validado por el Ministro responsable de la salud.

La verificación de los conocimientos del nacional, durante la prueba de aptitud, se refiere a las materias especificadas en el[Artículo 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=7F79CBC33451D825348CD8199AF82FBF.tplgfr38s_2?idArticle=LEGIARTI000020996952&cidTexte=LEGITEXT000020996948&dateTexte=20180116) del auto de 25 de agosto de 2009 por el que se aplican las medidas de verificación de conocimientos y compensatorias para el reconocimiento de cualificaciones profesionales en el sector funerario. Está validado por el Ministro responsable de la salud que dará su opinión al prefecto. Este último informará al nacional de su decisión de conceder el reconocimiento de la cualificación profesional.

*Para ir más allá*: Artículos L. 2223-49 y siguientes artículos R. 2223-134 y siguientes del Código General de Autoridades Locales.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

