﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP234" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Guardería" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="guarderia" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/guarderia.html" -->
<!-- var(last-update)="2020-04-15 17:22:06" -->
<!-- var(url-name)="guarderia" -->
<!-- var(translation)="Auto" -->


Guardería
=========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:06<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El cuidador de niños es una enfermera profesional que se especializa en el cuidado de la primera infancia.

Los deberes del niño acadaincluye:

- seguimiento y atención de recién nacidos, lactantes y niños;
- seguir las dietas de los recién nacidos y los niños en incubadoras o en fototerapia.

El cerebro de los niños puede trabajar en salas de maternidad, salas pediátricas de hospitales o en centros de protección maternoinfantil.

*Para ir más allá*: Artículo R. 4311-13 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ejercer la profesión de niñera, el profesional debe tener los siguientes títulos:

- un título de enfermería/partera
- un diploma estatal como niñera.

*Para ir más allá*: Artículo D. 4311-49 del Código de Salud Pública.

#### Entrenamiento

Para obtener más información sobre la formación para títulos de enfermería y partería, consulte las tarjetas "Enfermera/Era" y "Midwife".

**Diploma estatal de cuidado infantil**

El diploma estatal de childminder se otorga al candidato:

- Titular del diploma estatal de enfermera/partera;
- haber aprobado el examen de ingreso en una escuela que se prepara para el diploma;
- (ver Sección L. 3111-1 y siguientes del Código de Salud Pública).

Para participar en el concurso de admisión, el solicitante debe presentar un expediente que incluya:

- Una solicitud de registro manuscrita
- Una tarjeta de registro civil
- Un CV
- Copiadendo todos sus diplomas o títulos de formación;
- comprobante de pago de la cuota de entrada para la competición de admisión.

El concurso de admisión consta de dos pruebas de elegibilidad escritas y un examen de ingreso oral.

El entrenamiento de 12 meses consiste en:

- Lecciones teóricas de 650 horas;
- Enseñanza clínica de 710 horas;
- trabajo dirigido y evaluación de 140 horas;
- pasantías en hospitales e instituciones contratadas por la escuela.

El Diploma Estatal de Childminder se otorga a un candidato que ha aprobado las evaluaciones de conocimientos y habilidades profesionales durante su formación.

*Para ir más allá* : decreto de 12 de diciembre de 1990 relativo a la escolarización, el diploma estatal de cuidado de los niños y el funcionamiento de las escuelas.

#### Costos asociados con la calificación

La formación que conduce a la graduación de un niño de mente vale la pena. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

El nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), establecido y que actúe legalmente las actividades de un enfermero de cuidado infantil en uno de estos Estados, podrá llevar a cabo la misma actividad en Francia de vez en cuando con la condición de que se haya enviado una declaración previa de actividad al prefecto del departamento del lugar de entrega.

Si la cualificación de formación no se reconoce en Francia, las cualificaciones del nacional de un Estado de la UE o del EEE se comprueban antes de su primera actuación. En caso de diferencias sustanciales entre las cualificaciones del reclamante y la formación requerida en Francia, el nacional debe aportar pruebas de que ha adquirido los conocimientos y habilidades que faltan, compensación (véase infra "Bueno saber: medidas de compensación").

En todos los casos, el nacional europeo que desee ejercer en Francia de forma temporal u ocasional debe poseer las aptitudes linguísticas necesarias para llevar a cabo la actividad y dominar el peso y los sistemas de medición utilizados en Francia. Francia.

*Para ir más allá*: Artículo L. 4311-22 del Código de Salud Pública.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Los nacionales de un Estado de la UE o del EEE que deseen ejercer de forma permanente en Francia están comprendidos en dos regímenes distintos. Sin embargo, en ambos casos, el nacional debe disponer de las competencias linguísticas necesarias para llevar a cabo la actividad en Francia y dominar el peso y los sistemas de medición utilizados en Francia.

#### El sistema automático de reconocimiento de diplomas

Todos los titulares de un certificado de enfermero de cuidado infantil expedido por uno de los Estados de la UE o del EEE el 10 de junio de 2004 tienen derecho al reconocimiento automático de su título. Además, los nacionales que no posean uno de los títulos enumerados en la Lista I del decreto de 10 de junio de 2004 podrán recibir reconocimiento automático siempre que dispongan de credenciales de formación cuyo cumplimiento esté certificado por un certificado del Estado de obtención. Para conocer la lista completa de estos títulos, es aconsejable referirse al decreto de 10 de junio de 2004.

En todos los casos, las personas afectadas no tienen que solicitar permiso para ejercer. Por otro lado, deben inscribirse en el Consejo Departamental del Colegio de Enfermeras del lugar donde pretenden ejercer y solicitar su inscripción en el directorio "automatización de listas", Adeli (véase a continuación: "Solicitar el registro del diploma o el permiso para ejercer (No. Adeli)").

#### El régimen individual de autorización de ejercicio

Los nacionales que no se beneficien del régimen de reconocimiento automático deben obtener una autorización individual para ejercer en Francia para poder ejercerse permanentemente en Francia. Deben solicitarlo enviando un expediente completo a la Dirección Regional de Juventud, Deportes y Cohesión Social (DRJSCS) del lugar donde tengan previsto establecerse.

En caso de diferencia sustancial entre las cualificaciones profesionales adquiridas y las exigidas en Francia, los nacionales pueden estar sujetos a medidas de compensación (véase más adelante: "Buenas medidas de compensación") previstas por el decreto del 24 de marzo de 2010.

*Para ir más allá*: Artículos L. 4311-4 y siguientes y R. 4311-34 y siguientes artículos del Código de Salud Pública, decreto de 10 de junio de 2004 por el que se establece la lista de diplomas, certificados y otros títulos de enfermería/guardería expedidos por los Estados miembros de la UE u otros Estados partes en el Acuerdo EEE, artículo L. 4311-3 del Código de Salud Pública, orden del 20 de enero de 2010 sobre la declaración previa de la prestación de servicios para el ejercicio de las profesiones de enfermería (...), y el auto de 24 de marzo de 2010 en el que se establecen las condiciones de establecimiento de las condiciones organización de la prueba de aptitud y el curso de adaptación para la práctica de la enfermería en Francia por nacionales de los Estados miembros de la UE o parte en el Acuerdo EEE.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Todos los deberes generales impuestos a las enfermeras francesas de cuidado infantil se aplican a los nacionales que practican en Francia.

Como tal, las enfermeras de niñera de los niños deben respetar los principios de dignidad, no discriminación o independencia. Están sujetos a las condiciones de ejercicio de la profesión, a las normas profesionales aplicables en Francia y al tribunal disciplinario competente.

*Para ir más allá*: Artículos R. 4312-1 y siguientes del Código de Salud Pública.

### a. Actividades acumulativas

El enfermero/enfermera infantil puede realizar otra actividad profesional siempre que esta combinación sea coherente con la dignidad y calidad requeridas por su práctica profesional y que esté de acuerdo con la normativa vigente.

*Para ir más allá*: Artículo R. 4312-20 del Código de Salud Pública.

### b. Condiciones de honorabilidad

Para practicar, el enfermero/infantil no debe:

- estar sujetos a una prohibición temporal o permanente de la práctica en Francia o en el extranjero;
- ser suspendido debido al grave peligro para los pacientes por el ejercicio de la actividad.

*Para ir más allá*: Artículos L. 4311-16 y L. 4311-26 del Código de Salud Pública.

### c. Obligación para el desarrollo profesional continuo

Las enfermeras infantiles deben participar anualmente en un programa de desarrollo profesional en curso. Este programa tiene como objetivo mantener y actualizar sus conocimientos y habilidades, así como mejorar sus prácticas profesionales.

Como tal, el profesional de la salud (salario o liberal) debe justificar su compromiso con el desarrollo profesional. El programa está en forma de formación (presente, mixta o no presental) en análisis, evaluación y mejora de la práctica y gestión de riesgos. Toda la formación se registra en un documento personal que contiene certificados de formación.

*Para ir más allá*: Artículos L. 4021-1 y R. 4382-1 del Código de Salud Pública.

### d. Aptitud física

La enfermera no debe estar afectada por una discapacidad o una condición médica que haga que el ejercicio de la profesión sea peligroso.

*Para ir más allá*: Artículo L. 4311-18 del Código de Salud Pública.

4°. Seguro
-------------------------------

### a. Obligación de constete de un seguro de responsabilidad civil profesional

Como profesional de la salud, una enfermera liberal / enfermera de enfermería debe tomar un seguro de responsabilidad civil profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

### b. Obligación de adherirse al fondo autosostenible de pensiones y pensiones de los asistentes médicos (Carpimko)

La enfermera enfermera ejercer a su calidad liberal, incluso por cierto, debe unirse al fondo de pensiones y pensiones de enfermeras, masajistas, pedicuras-podólogos, logopedas (Carpimko).

**Documentos de apoyo**

El interesado deberá dirigirse a carpimko lo antes posible:

- El cuestionario de afiliados [descargable desde el sitio web de Carpimko](http://www.Carpimko.com/document/pdf/affiliation_declaration.pdf) o una carta en la que se mencione la fecha de inicio de la actividad liberal;
- Fotocopia del diploma estatal;
- una fotocopia del número de registro (Adeli) del diploma expedido por la Agencia Regional de Salud (ARS) o una fotocopia del dorsa (Adeli) del diploma.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

**Autoridad competente**

La declaración previa de actividad debe dirigirse al Consejo Nacional del Colegio de Enfermeras (CNOI), antes de la primera prestación de servicios.

**Renovación de la predeclaración**

La declaración anticipada debe renovarse una vez al año si el prestador desea volver a prestar servicios en Francia.

**Documentos de apoyo**

Esta solicitud va acompañada de los siguientes documentos justificativos:

- el[formulario de declaración](https://www.legifrance.gouv.fr/affichTexte.docidTexte=JORFTEXT000036171877&dateTexte=20180129) Completado y firmado;
- certificado de suscripción de seguro de responsabilidad profesional para actos realizados en territorio francés;
- Una fotocopia de una identificación que acredite la nacionalidad del solicitante;
- Fotocopia del título o títulos de formación
- el certificado de la autoridad competente del Estado de establecimiento, miembro de la UE o del EEE, que certifique que el interesado está legalmente establecido en él y que no incurre, cuando se expide el certificado, no se puede ejercer ninguna prohibición, ni siquiera temporal.

**Qué saber**

Los documentos justificativos, con excepción de la fotocopia del documento de identidad, deben ser traducidos al francés por un traductor autorizado ante los tribunales franceses o facultado para intervenir ante las autoridades judiciales o administrativas de un Estado MIEMBRO de la UE o del EEE.

**hora**

En el plazo de un mes a partir de la recepción de la declaración anticipada, el CNOI informa al demandante del resultado del examen de sus cualificaciones.

Dentro de este tiempo, el CNOI puede solicitar más información. En este caso, el período inicial de un mes se prorroga por un mes.

Al final de este procedimiento, el CNOI informa al demandante, según sea el caso:

- Que puede comenzar la prestación de servicios
- No puede comenzar la prestación de servicios;
- que debe demostrar que ha adquirido los conocimientos y habilidades que faltan si su formación muestra diferencias sustanciales con la formación requerida en Franco (incluso a través de una medida de compensación).

En ausencia de una respuesta del CNOI a tiempo, la prestación del servicio puede comenzar.

**Entrega de recibo**

El CNOI registra al proveedor de servicios en una lista determinada y, a continuación, envía al proveedor un recibo de registro. A continuación, el reclamante deberá informar a la agencia nacional de seguro médico correspondiente de su prestación y proporcionarle su número de registro.

**Qué saber**

El reclamante ocasional y temporal no está obligado a registrarse en el directorio de Adeli y no es responsable de la contribución ordinal.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 4311-22 y siguientes y R. 4311-38 y siguientes del Código de Salud Pública y el auto de 20 de enero de 2010 mencionado anteriormente.

### b. Formalidades para los nacionales de la UE para un ejercicio permanente (LE)

#### Si es necesario, solicite permiso para practicar

Si el nacional no está en virtud del régimen de reconocimiento automático, debe solicitar una licencia para ejercer.

**Autoridad competente**

La solicitud de permiso para ejercer se dirige al prefecto regional del lugar de asentamiento de la persona. Esta última pone en venta, en caso necesario, la autorización de práctica previa alegó el Comité de Enfermeras.

**Procedimiento**

La solicitud de autorización para ejercer deberá dirigirse a la Dirección Regional de Juventud, Deportes y Cohesión Social (DRJSCS) del lugar de práctica propuesto. El prefecto regional reconoce la recepción de la solicitud en el plazo de un mes a partir de la recepción del expediente.

**Documentos de apoyo**

La solicitud de licencia para la práctica debe figurar en dos copias y contener:

- El formulario de solicitud de autorización de la profesión cumplimentado. Este formulario está disponible en línea en el sitio web de DRJSCS en revisión;
- Una fotocopia de un documento de identidad válido
- Una copia del título de formación que permite ejercer la profesión en el país de obtención;
- Si es necesario, una copia de los diplomas adicionales;
- todos los documentos útiles que justifiquen la formación continua, la experiencia y las competencias adquiridas durante el año profesional en un Estado miembro de la UE o del EEE o en un tercer Estado;
- una declaración, de menos de un año, de la autoridad competente del Estado de la UE o del EEE que acredite la ausencia de sanciones;
- Una copia de los certificados de las autoridades que expidieron el título de formación especificando el nivel de formación y el detalle, año tras año, de las lecciones imitadas, su volumen por hora, su contenido y la duración de las prácticas validadas;
- para aquellos que han trabajado en un Estado de la UE o del EEE que no regula el acceso o el ejercicio de la profesión, todos los documentos que justifican que han ejercido el equivalente a dos años a tiempo completo en ese Estado en los últimos diez años;
- para aquellos que posean un certificado de formación expedido por un tercer Estado y reconocido en un Estado de la UE o del EEE, distinto de Francia, el reconocimiento del certificado de formación y, en su caso, la designación de especialista establecida por el estado de UE o EEE habiendo reconocido estos títulos.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tenga en cuenta que**

Para obtener esta autorización, el interesado puede estar obligado a llevar a cabo medidas de compensación (prueba de aptitud o adaptación si resulta que las cualificaciones y la experiencia profesional que utiliza son sustancialmente diferentes de los requeridos para el ejercicio de la profesión en Francia.

Si se consideran necesarias las medidas de compensación, el prefecto de la región responsable de expedir la autorización de ejercicio indica que dispone de dos meses para elegir entre la prueba de aptitud y el curso de adaptación (véase más adelante " Buen conocimiento: medidas de compensación").

**Resultado del procedimiento**

El silencio guardado por el prefecto regional al final de un período de cuatro meses a partir de la recepción del expediente completo merece la decisión de rechazar la solicitud.

**Remedios**

Si se deniega la solicitud de solicitud de licencia para ejercer (implícita o expresa), el solicitante podrá impugnar dicha decisión. Así pues, en el plazo de dos meses a partir de la notificación de la decisión de denegación, puede constituir la elección de:

- un llamamiento agraciado al prefecto regional;
- un llamamiento jerárquico al Ministro de Salud;
- acciones legales ante el tribunal administrativo territorialmente competente.

**Costo**

Gratis.

**Bueno saber: medidas de compensación**

**La prueba de aptitud**

El DRJSCS organizador de las pruebas de aptitud debe convocar a la persona por carta recomendada con notificación de recepción al menos un mes antes del inicio de las pruebas. Esta citación menciona el día y el lugar del juicio. La prueba de aptitud puede tomar la forma de preguntas escritas u orales señaladas en veinte, cada una de las asignaturas que no fueron inicialmente enseñadas o adquiridas durante la experiencia profesional.

La admisión se pronuncia con la condición de que la persona haya alcanzado un promedio mínimo de diez de veinte, sin puntuación inferior a ocho sobre veinte. Los resultados de la prueba se notifican al interesado por el prefecto regional.

En caso de éxito, el prefecto regional autoriza al interesado a ejercer la profesión.

**El curso de adaptación**

Se lleva a cabo en un centro de salud público o privado acreditado por el ARS. El becario es sometido a la responsabilidad pedagógica de un profesional cualificado que ha estado practicando la profesión durante al menos tres años y que establece un informe de evaluación.

La pasantía, que finalmente incluye formación teórica adicional, es validada por el jefe de la estructura de recepción a propuesta del profesional cualificado que evalúa al aprendiz.

Los resultados de la pasantía se notifican al interesado por el prefecto regional.

En caso afirmativo, se toma la decisión de autorizar el ejercicio, tras un nuevo dictamen de la comisión a que se refiere la sección L. 4311-4 del Código de Salud Pública.

*Para ir más allá*: Artículos L. 4311-4, R. 4311-34 y siguientes del Código de Salud Pública y el auto de 20 de enero de 2010 supra.

#### Pide inclusión en el Colegio de Enfermeras

Para ejercer la profesión de enfermera/cuidado infantil, es obligatorio inscribirse en la junta directiva del Colegio de Enfermeras. Esta inscripción hace que el ejercicio de la profesión sea lícito en territorio francés.

**Autoridad competente**

La solicitud de inscripción deberá presentarse ante el consejo departamental o interdepartamental del Colegio de Enfermeras (CDOI o CIOI) en el que el interesado desee ejercer, preferiblemente mediante carta recomendada con preaviso de recepción.

**Procedimiento**

Al recibir el expediente completo de la solicitud, el Presidente del CDOI o CIOI confirma la recepción de la solicitud en el plazo de un mes. El CDOI o CIOI tiene un máximo de tres meses para estudiar y decidir sobre la solicitud. La decisión se da a conocer a la persona interesada mediante carta recomendada con notificación de recepción hasta una semana después de la deliberación del CDOI o CIOI.

**Documentos de apoyo**

- El formulario de solicitud de inscripción en la junta de la Orden que se retirará del CDOI o CIOI considerado o descargado de la [sitio web nacional del Colegio de Enfermeras](http://www.ordre-infirmiers.fr/lordre-et-les-conseils-ordinaux/inscription-a-lordre.html) ;
- fotocopia de una identificación válida acompañada, en su caso, de un certificado de nacionalidad expedido por una autoridad competente;
- una copia del diploma de enfermería/infantil, traducido por un traductor certificado, si es necesario. Esta copia debe ir acompañada de:- o un certificado del Estado de expedición del diploma que certifique la formación de conformidad con las obligaciones europeas,
  - o un certificado que certifique que el individuo ha trabajado el equivalente a dos años a tiempo completo, en los diez años anteriores a la solicitud, la profesión de enfermera /profesión de cuidado infantil, incluida la programación completa, la organización y la administración de enfermería a los pacientes;
- evidencia de conocimiento de la lengua francesa y del sistema de pesos y medidas utilizados en el país;
- prueba de moralidad:- o un extracto de antecedentes penales, con menos de tres meses de edad, expedido por una autoridad competente del Estado de origen u origen,
  - un certificado de moralidad o de honor del Consejo de la Orden o de la autoridad competente del Estado miembro de la UE de menos de tres meses de edad;
- una declaración sobre el honor de la persona que certifica que no es objeto de un caso que podría dar lugar a una condena o sanción que podría tener un impacto en la inscripción en la pizarra;
- un certificado de baja, registro o registro, expedido por la autoridad con la que fue previamente registrado o registrado. En caso contrario, el interesado deberá presentar una declaración de honor que certifique que nunca ha sido registrado o, en su defecto, un certificado de registro o registro en un Estado miembro de la UE o del EEE;
- un currículum.

Es posible que se requieran otros documentos justificativos de acuerdo con CDOI o CIDI. Para más detalles, es aconsejable acercarse más a la pizarra en cuestión.

Si la persona desea ejercer en forma de empresa, deberá adjuntar, además de los documentos mencionados anteriormente:

- una copia de los estatutos y, en su caso, una copia del reglamento interno y una copia o envío de la constitución;
- Un certificado de registro por orden de cada uno de los socios o, si aún no están registrados, la prueba de la solicitud de registro;
- para un ejercicio liberal (SEL) en la sociedad:- un certificado del Tribunal de Comercio o del Registro del Tribunal Superior que declara que la solicitud de registro de la empresa se presenta al registro del Registro Mercantil y Corporativo,
  - un certificado de los socios que especifique la naturaleza y evaluación de las contribuciones, el importe del capital social, el número, el importe mínimo y la distribución de acciones o acciones, la afirmación de la liberación total o parcial de las contribuciones.

**Remedios**

Cualquier recurso debe presentarse ante el consejo regional o interregional en el que se encuentre el CDOI o el CIOI, que se ha pronunciado dentro de los treinta días siguientes a la notificación de la decisión.

**Costo**

La inscripción en el consejo de la Orden es gratuita, pero crea la obligación de someterse a las cuotas ordinales obligatorias, cuyo importe se fija anualmente por el Consejo Nacional de la Orden.

*Para ir más allá*: Artículos L. 4311-15, L. 4311-16, L. 4312-7, R. 4112- y lo siguiente es aplicable en los artículos R. 4311-52, R. 4113-4, R. 4113-28 del Código de Salud Pública.

#### En el caso de ejercicio sin derecho a la sociedad civil profesional (SCP) o a un SEL, solicitar la inclusión de la empresa en el consejo del Colegio de Enfermeras

Si la persona desea ejercer en forma de CPS o sEL, debe incluir la empresa en el orden del lugar de establecimiento de la sede central.

**Autoridad competente**

La solicitud de registro debe presentarse al CDOI o AL CIOI en el que la persona desee ejercer, preferiblemente por carta recomendada con previo aviso de recepción.

**Procedimiento**

Al recibir el expediente completo de la solicitud, el Presidente del CDOI o CIOI confirma la recepción de la solicitud en el plazo de un mes. El CDOI o CIOI tiene un máximo de tres meses para estudiar y decidir sobre la solicitud. La decisión se da a conocer a la persona interesada mediante carta recomendada con notificación de recepción hasta una semana después de la deliberación del CDOI o CIOI.

**Documentos de apoyo**

- una copia de los estatutos y, en su caso, una copia del reglamento interno y una copia o envío de la constitución;
- Un certificado de registro por orden de cada uno de los socios o, si aún no están registrados, la prueba de la solicitud de registro;
- para un EJERCICIO en SEL:- un certificado del Tribunal de Comercio o del Registro del Tribunal Superior que declara que la solicitud de registro de la empresa se presenta al registro del Registro Mercantil y Corporativo,
  - un certificado de los socios que especifique la naturaleza y evaluación de las contribuciones, el importe del capital social, el número, el importe mínimo y la distribución de acciones o acciones, la afirmación de la liberación total o parcial de las contribuciones.

**Remedios**

Cualquier recurso debe presentarse ante el consejo regional o interregional en el que se encuentre el CDOI o cioI, que ha dictaminado, en un plazo de 30 días a partir de la notificación de la decisión.

**Costo**

La inscripción en el consejo de la Orden es gratuita, pero crea la obligación de someterse a las cuotas ordinales obligatorias, cuyo importe se fija anualmente por el Consejo Nacional de la Orden.

#### Solicitar inscripción de diploma o autorización para ejercer (Adeli)

Los enfermeros de guardería están obligados a registrar su certificado de formación o la autorización requerida para el ejercicio de la profesión.

**Autoridad competente**

El registro del diploma o la autorización para ejercer debe registrarse en el directorio Adeli ("automatización de listas") con el ARS del lugar de práctica.

**hora**

La solicitud de inscripción debe presentarse en el plazo de un mes a partir de la toma de posesión, independientemente del modo de ejercicio (liberal, asalariado, mixto). El recibo emitido por el ARS menciona el número Adeli. A continuación, el LRA envía al solicitante un formulario de solicitud para la concesión de la tarjeta de salud profesional.

**Documentos de apoyo**

Los documentos justificativos que se proporcionarán son:

- El diploma original (traducido al francés por un traductor certificado, si corresponde);
- Id
- Comprobante de inscripción a la Orden del Departamento de Ejercicios;
- Forma Cerfa 10906Completado, fechado y firmado.

Esta lista puede variar de una región a otra. Para obtener más detalles, es aconsejable acercarse al ARS en cuestión.

**Costo**

Gratis.

*Para ir más allá*: Artículo L. 4311-15 del Código de Salud Pública.

#### Solicitar afiliación con seguro médico

Registrarse en el seguro de salud permite a la compañía de seguros de salud cuidar de la atención realizada. Además, este registro desencadena la adquisición de la Tarjeta Profesional de la Salud (CPS) y hojas de atención en nombre de la enfermera/ proveedor de cuidado infantil.

**Autoridad competente**

Esto se hace en el departamento de profesionales de la salud del fondo de seguro médico primario (CPAM) en el lugar de práctica.

**Documentos de apoyo**

Los documentos justificativos que se proporcionarán son:

- Copiar el diploma
- Tarjeta de Adeli;
- El formulario de solicitud de la Tarjeta de Salud Profesional (CPS)
- Un extracto de identidad bancaria
- certificación de tarjeta vital y tarjeta vital.

Se pueden solicitar otros documentos justificativos de acuerdo con los diferentes CPAM. Para más detalles, es aconsejable acercarse a la CPAM competente.

### c. Tarjeta Profesional Europea (CPE)

La tarjeta profesional europea es un procedimiento electrónico para reconocer las cualificaciones profesionales en otro Estado de la UE.

El procedimiento CPE puede utilizarse tanto cuando el nacional desea operar en otro Estado de la UE:

- temporales y ocasionales;
- de forma permanente.

El CPE es válido:

- indefinidamente en caso de un acuerdo a largo plazo;
- 18 meses para la prestación de servicios con carácter temporal.

**Solicitud de tarjeta de visita europea**

Para solicitar un CPE, el nacional debe:

- Cree una cuenta de usuario en el[Servicio de autenticación de la Comisión Europea](https://webgate.ec.europa.eu/cas) ;
- a continuación, complete su perfil de CPE (identidad, información de contacto, etc.).

**Tenga en cuenta que**

También es posible crear una solicitud de CPE descargando los documentos de apoyo escaneados.

**Costo**

Por cada solicitud de CPE, las autoridades del país de acogida y del país de origen pueden cobrar una tasa de revisión de los archivos, cuyo importe varía en función de la situación.

**hora**

Para una solicitud de CPE en el contexto de una actividad temporal y ocasional: en el plazo de una semana, la autoridad del país de origen reconoce la recepción de la solicitud de CPE, los informes si faltan documentos e informa de cualquier costo. A continuación, las autoridades del país anfitrión revisan el caso.

Si no se requiere verificación con el país anfitrión, la autoridad del país de origen revisa la solicitud y toma una decisión final en un plazo de tres semanas.

Si se requieren verificaciones dentro del país anfitrión, la autoridad del país de origen tiene un mes para revisar la solicitud y reenviarla al país anfitrión. A continuación, el país anfitrión toma una decisión final en un plazo de tres meses.

Para una solicitud de CPE como parte de una actividad permanente: en el plazo de una semana, la autoridad del país de origen reconoce la recepción de la solicitud de CPE, informa si faltan documentos e informa de cualquier costo. A continuación, el país de origen dispone de un mes para revisar la solicitud y reenviarla al país anfitrión. Este último toma la decisión final en un plazo de tres meses.

Si las autoridades del país anfitrión creen que el nivel de educación o formación o experiencia laboral está por debajo de los estándares requeridos en ese país, pueden pedir al solicitante que realice una prueba de aptitud o que complete una pasantía Adaptación.

**Emisión de la aplicación CPE**

Si se concede la solicitud para el CPE, después es posible obtener un certificado CPE en línea.

Si las autoridades del país anfitrión no tocan una decisión dentro del tiempo asignado, las cualificaciones se reconocen tácitamente y se emite un CPE. A continuación, es posible obtener un certificado CPE de su cuenta en línea.

Si se desestima la solicitud de CPE, la decisión de denegación debe estar justificada y presentar los recursos para impugnar dicha denegación.

*Para ir más allá*: Artículos R. 4311-41-4 y siguientes del Código de Salud Pública.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas del día del día en que el centro SOLVIT en el país donde se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París, ([Sitio web](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

