﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP085" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Construcción" -->
<!-- var(title)="Controlador técnico de la construcción" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="construccion" -->
<!-- var(title-short)="controlador-tecnico-de-la-construccion" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/construccion/controlador-tecnico-de-la-construccion.html" -->
<!-- var(last-update)="2020-04-15 17:20:46" -->
<!-- var(url-name)="controlador-tecnico-de-la-construccion" -->
<!-- var(translation)="Auto" -->


Controlador técnico de la construcción
======================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:46<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La misión del controlador técnico es prevenir cualquier peligro técnico que pueda ocurrir durante la construcción de una obra.

Garantizará la solidez de las estructuras, la seguridad de las personas que la ocuparán, el cumplimiento de la normativa sobre personas con movilidad reducida y rendimiento energético.

**Tenga en cuenta que**

El control técnico es obligatorio para las obras contempladas en la Sección R. 111-38 del Código de Edificación y Vivienda.

*Para ir más allá*: Artículo L. 111-23 del Código de La Construcción y Vivienda;[Decreto 99-443 de 28 de mayo de 1999](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000561661&dateTexte=20180416) cláusulas técnicas generales aplicables a los contratos públicos de control técnico.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de controlador técnico de construcción, el profesional debe ser:

- profesionalmente calificados;
- aprobación ministerial.

*Para ir más allá*: Artículos L. 111-25 y R. 111-32-2 del Código de Construcción y Vivienda.

#### Entrenamiento

Para ser reconocida como una persona profesionalmente calificada, la persona debe justificar:

- para el personal operativo y los ingenieros:- tienen un título postsecundario en construcción o ingeniería civil que justifica al menos cuatro años de estudio, y tienen al menos tres años de experiencia práctica en el diseño, implementación, control técnico o experiencia de Edificios
  - o tener seis años de experiencia práctica en el campo;
- para el personal de ejecución de la misión:- o bien poseer un certificado de escuela secundaria en el campo de actividad previsto, y una práctica de al menos tres años en el diseño, construcción, control técnico o experiencia de las construcciones,
  - o tener seis años de experiencia práctica en este campo.

Una vez que el profesional cumple estas condiciones, con el fin de ejercer como controlador técnico de la construcción, debe solicitar la aprobación ministerial (ver infra "5o. a. Solicitud de aprobación").

*Para ir más allá*: Artículo R. 111-32-2 del Código de Construcción y Vivienda.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Todo nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE), que actúe como controlador técnico de la construcción, podrá llevar a cabo la misma actividad de forma temporal e informal Francia.

Para ello, la persona debe hacer una declaración previa al Ministro responsable de la construcción antes de su primera actuación (véase infra "5o. b. Predeclaración para los nacionales de la UE o del EEE para el ejercicio temporal y casual").

Además, cuando ni el acceso a la actividad ni su ejercicio estén regulados en ese Estado, el nacional deberá justificar haber participado en esta actividad durante al menos un año en los diez años anteriores a su primer beneficio.

*Para ir más allá*: Artículo L. 111-25 del Código de La Construcción y Vivienda.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Todo nacional de un Estado miembro de la UE o del EEE, legalmente establecido y que actúe como responsable técnico de la construcción, podrá llevar a cabo la misma actividad de forma permanente en Francia.

Para ello, la persona debe disponer de un certificado de competencia o de un certificado de formación expedido por un Estado miembro de la UE o del EEE que regule la actividad de control técnico de la construcción y acredite un nivel de cualificación en el menos igual al nivel inmediatamente inferior al requerido para un nacional francés (véase más arriba "2 grados). a. Capacitación").

Una vez que el nacional cumpla estas condiciones, deberá solicitar la aprobación del Ministro responsable de la construcción, así como del nacional francés (véase infra "5o. a. Solicitud de aprobación").

En caso de diferencias sustanciales entre sus cualificaciones profesionales y las requeridas en Francia, el nacional será audicionado por el[Comisión de Acreditación](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=67FE5CB5D0712DFD0DD7565EC7F5A972.tplgfr36s_1?idArticle=LEGIARTI000020740035&cidTexte=LEGITEXT000006074096&dateTexte=20091007) y tendrá que demostrar sus habilidades y conocimientos en el campo de la construcción.

*Para ir más allá*: Artículo R. 111-32-2 del Código de Construcción y Vivienda.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

**Responsabilidad**

El controlador técnico de construcción está sujeto a una presunción de plena responsabilidad por todos los daños que comprometan la fuerza de la estructura o la hagan inadecuada para su propósito previsto.

*Para ir más allá*: Artículo L. 111-13 del Código de La Construcción y Vivienda.

**Incompatibilidades**

La actividad del controlador técnico es incompatible con las actividades de diseño, ejecución o experiencia laboral. Además, el responsable técnico debe actuar imparcialmente y no interferir con la independencia de quienes lleven a cabo las actividades anteriores.

*Para ir más allá*: Artículo R. 111-31 del Código de La Construcción y Vivienda.

4°. Seguro
-------------------------------

Como parte de su misión y debido a su plena responsabilidad, el controlador técnico tendrá que tomar un seguro de responsabilidad civil de 10 años.

*Para ir más allá*: Artículo L. 111-24 del Código de La Construcción y Vivienda.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de aprobación

**Autoridad competente**

El Ministro responsable de la construcción es responsable de expedir la licencia, que tiene una validez renovable de cinco años.

**Documentos de apoyo**

La solicitud de aprobación se realiza mediante la presentación de un archivo que incluye:

- El estado civil del solicitante y la dirección de su domicilio;
- la justificación de la competencia teórica y la experiencia práctica del personal directivo, la organización interna de la gestión técnica, las normas de asistencia a los servicios operativos efectivamente responsables del seguimiento y los criterios Contratación o asignación de oficiales
- El compromiso de que el Contralor actuará de manera imparcial e independiente;
- El compromiso del Contralor con la atención de la administración se realizará a cualquier cambio en la información que haya dado con el fin de obtener la acreditación;
- En caso afirmativo, todas las aprobaciones que ha obtenido previamente, en el sector de la construcción;
- el alcance de la aprobación requerida de acuerdo con la categoría o categorías de construcción de la estructura, y la naturaleza o extensión de los caprichos que puedan resultar.

**Procedimiento**

El expediente se presentará a la Comisión de Acreditación, que garantizará el cumplimiento de las pruebas documentales. Ella escuchará a la solicitante antes de deliberar sobre qué hacer con su solicitud. Cuando emita un dictamen favorable, notificará al Ministro que emitirá la aprobación en un plazo de seis meses a partir de la recepción del expediente completo. El silencio guardado dentro de este tiempo valdrá la pena rechazar la solicitud.

*Para ir más allá*: Artículos R. 111-29 y R. 111-32 del Código de Edificación y Vivienda; Apéndice III de la[decretado el 26 de noviembre de 2009](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021344640&dateTexte=20180416) establecer las formas prácticas en que se puede acceder a la actividad del controlador técnico;[enlace a la secretaría de la Comisión de Acreditación de Controladores Técnicos](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### b. Predeclaración para el nacional de la UE o del EEE para el ejercicio temporal y casual

**Autoridad competente**

El Ministro responsable de la construcción es responsable de decidir sobre la declaración previa de actividad.

**Documentos de apoyo**

Esta solicitud se puede enviar electrónicamente o por correo, y toma la forma de una carpeta con los siguientes documentos justificativos:

- El estado civil del nacional y la dirección de su casa;
- un certificado que justifique que está legalmente establecido en un Estado de la UE o del EEE y que no incurre en una prohibición de la práctica;
- cualquier documento que justifique sus cualificaciones profesionales;
- cualquier documento que justifique que ha participado en esta actividad durante al menos dos años en los últimos diez años, siempre que la UE o el Estado del EEE en el que esté establecida no regulen esta actividad;
- El compromiso de que el nacional actuará de manera imparcial e independiente;
- La naturaleza de la actuación que planea realizar y su fecha de inicio y finalización;
- un certificado de seguro de responsabilidad civil adaptado al beneficio que planea hacer.

**Procedimiento**

El Ministro responsable de la construcción dispondrá de un mes para verificar el expediente y autorizar la entrega, previa asesoración favorable de la Comisión de Acreditación. En caso de diferencias sustanciales entre las cualificaciones profesionales del nacional y las requeridas en Francia, el Ministro podrá pedir al nacional que comparezca ante la comisión y demuestre sus conocimientos y aptitudes Construcción.

El silencio de la autoridad competente después de la recepción del expediente, en el plazo de un mes, valdrá la pena aceptar la solicitud.

*Para ir más allá*: Artículos R. 111-29-1 y R. 111-32-1 del Código de Construcción y Vivienda;[enlace a la secretaría de la Comisión de Acreditación de Controladores Técnicos](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### c. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

#### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

Seis grados. Textos de referencia
---------------------------------

- Artículos L. 111-23 a L. 111-26 del Código de La Construcción y Vivienda;
- Artículos R. 111-29 a R. 111-42 del Código de La Construcción y Vivienda;
- Orden de 26 de noviembre de 2009 por la que se establecen las condiciones prácticas de acceso al ejercicio de la actividad del controlador técnico;
- Decreto 99-443, de 28 de mayo de 1999, relativo a las cláusulas técnicas generales aplicables a los contratos públicos de control técnico;
- Estándar NF P 03-100.

