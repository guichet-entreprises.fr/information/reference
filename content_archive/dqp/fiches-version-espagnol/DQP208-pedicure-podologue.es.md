﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP208" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Pedicura-podólogo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="pedicura-podologo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/pedicura-podologo.html" -->
<!-- var(last-update)="2020-04-15 17:21:56" -->
<!-- var(url-name)="pedicura-podologo" -->
<!-- var(translation)="Auto" -->


Pedicura-podólogo
=================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:56<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El pedicura-podólogo es un profesional de la salud especializado en el tratamiento de las condiciones epidérmicas y no gueales (uñas) del pie.

Por lo tanto, es competente para practicar el cuidado de la higiene (actividad pedicura) pero también para hacer y adaptar plantillas ortopédicas para aliviar estas dolencias (actividad podiatry).

*Para ir más allá*: Artículos L. 4322-1 y R. 4322-1 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La práctica del pedicura-podólogo está reservada a los titulares del diploma estatal de pedicura-podólogo.

*Para ir más allá*: Artículo L. 4322-2 del Código de Salud Pública.

#### Entrenamiento

El diploma estatal de pedicura-podólogo es una licenciatura que se prepara en tres años después de la graduación.

**Tenga en cuenta que**

La admisión a algunas de las instituciones de formación se puede realizar mediante competencia externa.

Una formación teórica y práctica de 2.028 horas y una formación clínica de 1.170 horas deben permitir al estudiante desarrollar sus conocimientos y habilidades para incluir:

- analizar y evaluar una situación y desarrollar un diagnóstico en el campo de la pedicuria-podología;
- Diseñar y llevar a cabo asesoramiento, educación, prevención en pedicuria-podología y salud pública;
- Diseñar, dirigir y evaluar un proyecto terapéutico en pedicuria-podología;
- implementar actividades terapéuticas en el campo de la pedicuria-podología;
- una estructura y sus recursos.

El candidato es declarado elegible para recibir el diploma estatal una vez que haya obtenido el promedio de las pruebas y validado las prácticas obligatorias.

#### Costos asociados con la calificación

La capacitación que conduce a un diploma estatal da sus frutos y el costo varía de una institución a otra. Para más información, es aconsejable acercarse a estos establecimientos.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Un nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo del Espacio Económico Europeo (EEE), legalmente establecido en uno de estos Estados, podrá llevar a cabo la misma actividad en Francia de forma temporal y ocasional.

Para ello, tendrá que hacer una declaración por escrito, antes de la primera actuación, al prefecto del departamento del lugar de ejecución de la entrega (véase infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE para un ejercicio temporal e informal (LPS)).

Si ni el acceso ni la formación están regulados en el Estado miembro de origen ni en el estado del lugar de establecimiento, el nacional deberá justificar haber llevado a cabo esta actividad allí durante al menos un año en los últimos diez años anteriores a la Ventaja.

En todos los casos, el nacional europeo que desee ejercer en Francia de forma temporal u ocasional debe poseer las aptitudes linguísticas necesarias para llevar a cabo la actividad y dominar el peso y los sistemas de medición utilizados en Francia. Francia.

*Para ir más allá*: Artículo L. 4322-15 del Código de Salud Pública.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de un Estado de la UE o del EEE puede llevar a cabo actividades pedicura-podólogos de forma permanente en Francia, siempre y cuando haya completado con éxito un ciclo de educación postsecundaria y posea:

- un certificado de formación expedido por un Estado de la UE o del EEE en el que se regulen el acceso a la profesión o a su práctica y que permita ejercer allí la práctica jurídica;
- un certificado de formación expedido por un Estado de la UE o del EEE en el que no se regula el acceso a la profesión ni su ejercicio, y justifican haber llevado a cabo esta actividad en ese Estado durante al menos el equivalente a dos años a tiempo completo en los diez En los últimos años
- un certificado de formación expedido por un tercer Estado reconocido en un Estado de la UE o del EEE, distinto de Francia, que permita ejercer allí la profesión.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar una autorización individual para ejercer desde el prefecto regional del lugar en el que desea ejercer su profesión (véase más adelante "5o). b. Solicitar un permiso de ejercicio para el nacional de la UE o del EEE para la actividad permanente (LE) ").

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y su ejercicio en Francia, el interesado podrá estar sujeto a una medida de compensación (cf. infra "Bueno saber: medidas de compensación").

*Para ir más allá*: Artículo L. 4322-4 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Las disposiciones del Código de ética de la profesión se imponen a todos los pedicuras-podólogos que practican en Francia.

**Qué saber**

Todas las disposiciones del Código de ética están codificadas en las secciones R. 4322-31 a R. 4322-99 del Código de Salud Pública.

Como tal, el profesional debe:

- Respetar el secreto profesional
- practicando su profesión con probidad;
- Proporcionar atención informada que sea consistente con la profesión
- para mejorar sus conocimientos durante su carrera.

*Para ir más allá*: Artículo L. 4322-14 del Código de Salud Pública.

4°. Seguro
-------------------------------

Como profesional de la salud, un pedicura-podólogo liberal debe obtener un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

La declaración previa de actividad debe dirigirse al Consejo Nacional de la Orden de Pedicuras-Podólogos, antes de la primera prestación de servicios.

**Renovación de la predeclaración**

La declaración anticipada debe renovarse una vez al año si el reclamante desea realizar una nueva prestación en Francia.

**Documentos de apoyo**

La solicitud se realiza mediante la presentación de un expediente que contiene todos los siguientes documentos justificativos:

- Una copia de un documento de identidad válido
- Una copia del título de formación que permite ejercer la profesión en el estado de obtención;
- un certificado, de menos de tres meses de edad, de la autoridad competente del Estado de la UNIÓN o del EEE, que certifique que el interesado está legalmente establecido en ese Estado y que, cuando se expide el certificado, no existe ninguna prohibición, ni siquiera temporal, Ejercicio
- cualquier prueba que justifique que el nacional haya ejercido la profesión en un Estado de la UE o del EEE durante un año en los últimos diez años, cuando dicho Estado no regule la formación, el acceso a la profesión solicitada o su ejercicio;
- cuando el certificado de formación haya sido expedido por un tercer Estado y reconocido en un Estado de la UE o del EEE distinto de Francia:- reconocimiento del título de formación establecido por las autoridades estatales que han reconocido este título,
  - cualquier prueba que justifique que el nacional ha ejercido la profesión en ese estado durante tres años;
- Si es así, una copia de la declaración anterior, así como la primera declaración hecha;
- un certificado de responsabilidad civil profesional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tiempo de respuesta**

Una vez recibido el expediente, el prefecto de la región tendrá un mes para decidir sobre la solicitud e informará al nacional;

- Puede empezar la actuación.
- que estará sujeto a una medida de compensación si existen diferencias sustanciales entre la formación o la experiencia profesional del nacional y las requeridas en Francia;
- no podrá iniciar la actuación;
- cualquier dificultad que pueda retrasar su decisión. En este último caso, el prefecto podrá tomar su decisión en el plazo de dos meses a partir de la resolución de esta dificultad, y a más tardar tres meses de notificación al nacional.

El silencio del prefecto dentro de estos plazos valdrá la pena aceptar la solicitud de declaración.

**Remedios**

Las decisiones del Consejo Nacional de la Orden relativas a la libre prestación de servicios pueden ser impugnadas en el plazo de dos meses a partir de su notificación mediante un recurso de casación interpuesto ante el órgano jurisdiccional administrativo territorialmente competente. El tiempo de impugnación se incrementa a cuatro meses si el solicitante no permanece en Francia.

**Costo**

Gratis.

*Para ir más allá*: Artículo R. 4322-17 del Código de Salud Pública; 8 de diciembre de 2017 orden sobre la declaración previa de prestación de servicios para asesores genéticos, médicos y preparadores de farmacias y farmacias hospitalarias, así como para ocupaciones en el Libro III de Parte IV del Código de Salud Pública.

### b. Solicitar un permiso de ejercicio para la UE o el EEE nacional para la actividad permanente (LE)

**Autoridad competente**

La autorización de ejercicio es expedida por el prefecto regional, previa asesoración del comité de pedicuras-podólogos.

**Documentos de apoyo**

La solicitud de autorización se realiza mediante la presentación de un expediente que contiene todos los siguientes documentos:

- El formulario de solicitud de autorización para ejercer
- Una copia de un documento de identidad válido
- Una copia del título de formación
- Si es necesario, una copia de los diplomas adicionales;
- cualquier prueba que justifique la formación, la experiencia y las aptitudes adquiridas en la UE o en el Estado del EEE;
- una declaración de la autoridad competente del Estado de la UE o del EEE que justifique la ausencia de sanciones contra el nacional;
- Una copia de los certificados de las autoridades en la que se especifica el nivel de formación, el detalle y el volumen por hora de los cursos seguidos, así como el contenido y la duración de las prácticas validadas;
- cualquier documento que justifique que el nacional ha sido pedicura-podólogo durante un año en los últimos diez años, en un Estado de la UE o del EEE, donde ni el acceso ni el ejercicio están regulados en ese Estado.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

El prefecto reconoce la recepción del expediente en el plazo de un mes y decidirá después de tener la opinión de la comisión. Este último es responsable de examinar los conocimientos y habilidades del nacional adquiridos durante su formación o durante su experiencia profesional. Puede someter al nacional a una medida de compensación.

El silencio guardado por el prefecto de la región en un plazo de cuatro meses es una decisión de rechazar la solicitud de autorización.

**Bueno saber: medidas de compensación**

Si el examen de cualificaciones profesionales, atestiguado por las credenciales de formación y la experiencia laboral, muestra diferencias sustanciales con las cualificaciones necesarias para el acceso a la profesión de pedicura-podólogo y en Francia, el interesado tendrá que someterse a una medida de indemnización.

En función del nivel de cualificación exigido en Francia y del que posea el interesado, la autoridad competente podrá:

- Ofrecer al solicitante la opción de elegir entre un curso de alojamiento y una prueba de aptitud;
- requieren un curso de ajuste y/o una prueba de aptitud.

*Para ir más allá*: Artículos R. 4322-14 a R. 4322-16 del Código de Salud Pública; decreto de 20 de enero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización pertinentes para el examen de las solicitudes presentadas para el ejercicio en Francia de las profesiones de asesor genético, enfermero, masajista-kinesiterapeuta, pedicura-podólogo, terapeuta ocupacional, manipulador en electroradiología médica y dietista.

### c. Solicitar el registro de la RPPS

Un nacional que desea ejercer como pedicura-podólogo en Francia está obligado a registrar su título en el directorio compartido de profesionales de la salud (RPPS) (un directorio que reemplazará gradualmente el número ADELI).

El Colegio Nacional de Pedicuras-podiatistas es responsable de registrar al profesional en el RPPS en el momento de la inscripción y darle su número RPPS.

**Costo**

Gratis.

*Para ir más allá* : Decreto de 18 de abril de 2017 por el que se modifica el decreto modificado del 6 de febrero de 2009 por el que se crea un tratamiento de datos personales denominado "Directorio Compartido de Profesionales del Sistema de Salud" (RPPS).

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la Administración Pública de un Estado de la UE no ha respetado los derechos conferidos por el Derecho de la Unión como ciudadano o empresa de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html))

