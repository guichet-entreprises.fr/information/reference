﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP003" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sector aéreo" -->
<!-- var(title)="Oficial de información de vuelo y alerta" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sector-aereo" -->
<!-- var(title-short)="oficial-de-informacion-de-vuelo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sector-aereo/oficial-de-informacion-de-vuelo-y-alerta.html" -->
<!-- var(last-update)="2020-04-15 17:22:14" -->
<!-- var(url-name)="oficial-de-informacion-de-vuelo-y-alerta" -->
<!-- var(translation)="Auto" -->


Oficial de información de vuelo y alerta
========================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:14<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El oficial de información de vuelo y alerta o "agente AFIS" (*Servicio de Información de Vuelos de Aerodrome*) es un profesional que opera dentro de un aeródromo.

Como tal, sus principales misiones son:

- Proporcionar a los pilotos información sobre los parámetros meteorológicos, el tráfico aéreo o la configuración del aeródromo y las pistas;
- para proporcionar una alerta de accidente e iniciar procesos de emergencia en caso de un incidente.

También puede desempeñar un papel en las relaciones con los funcionarios de aviación civil, y debe asegurarse de que transmite una buena imagen del aeródromo en el que se encuentra.

**Qué saber**

El agente de AFIS tiene que diferenciarse del controlador: no emite ninguna autorización para despegar o aterrizar, siendo su papel exclusivamente informativo.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de AFIS está sujeta a la emisión por la Dirección de Seguridad de la Aviación Civil (DSAC) de una cualificación conocida como "calificación AFIS" que acredite la adquisición de conocimientos teóricos y prácticos, y que "Inglés AFIS." Esta calificación permite al titular practicar en un aeródromo específico.

*Para ir más allá*: Artículo R. 135-8 del Código de Aviación Civil, y orden de 16 de julio de 2007 relativo a la cualificación y formación del personal de AFIS.

#### Entrenamiento

La calificación AFIS se obtiene si el candidato tiene:

- obtuvo una designación "AFIS" cuando sea necesario;
- seguido de la formación, que fue objeto de una evaluación teórica inicial y luego de la formación teórica y práctica local realizada directamente en un aeródromo y el tema de una evaluación.

**Formación teórica inicial**

Si justifica haber recibido formación cuyo programa se establece en el Apéndice 1 del decreto de 16 de julio de 2007, cualquier candidato, de al menos 18 años, puede realizar la prueba de evaluación teórica inicial.

Una vez registrado en el[Plataforma OCEANE](https://www.ecologique-solidaire.gouv.fr/sites/default/files/Guide_detaille_inscription_QCM_OCEANE.pdf), el candidato podrá ir a un centro de examen para realizar la prueba de evaluación teórica inicial. El candidato será sometido a un cuestionario de opción múltiple (MQ) y tendrá que responder correctamente un mínimo de 48 de 60 preguntas para validar la prueba.

El certificado de éxito emitido por la DSAC tiene una validez de un año y permite el acceso a la formación teórica y práctica local.

*Para ir más allá*: Artículos 6 y 8 de la[decretado a partir del 16 de julio de 2007](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000022270822) cualificación y capacitación del personal de AFIS.

**Formación teórica y práctica local**

Con una duración de dos a veinticuatro semanas dependiendo del candidato, la formación teórica local incluye un currículo idéntico al de la formación teórica inicial, pero adaptado al entorno local del aeródromo en el que el candidato quiere evolucionar. La formación teórica y práctica local abarca también un programa complementario de formación establecido en la Lista 2 del Decreto de 16 de julio de 2007.

Su evaluación, para la evaluación teórica local, se realiza por escrito y también puede ir acompañada de una prueba oral, a elección del evaluador. La evaluación práctica local se lleva a cabo in situ.

Después de realizar con éxito la evaluación teórica y práctica local, el candidato recibe un certificado de éxito. Este certificado le permite desempeñar sus funciones durante dos meses, a la espera de la emisión de la titulación AFIS por parte de la DSAC.

*Para ir más allá*: Artículos 7 y 9 de la orden de 16 de julio de 2007.

**Renovación**

La calificación AFIS tiene una validez de tres años. El solicitante podrá solicitar la renovación presentando una solicitud por escrito a la DSAC territorialmente competente.

Esta solicitud debe incluir:

- un certificado del proveedor de AFIS que demuestre que el interesado realizó al menos 24 horas o cuatro vacaciones en los tres meses anteriores a la solicitud;
- un certificado del proveedor de AFIS que demuestre que el interesado ha completado la formación profesional continua (véase más adelante. "4a Formación Profesional Continua").

**Mención en inglés AFIS**

Además del éxito de las diversas pruebas teóricas y prácticas mencionadas anteriormente, el oficial de AFIS que solicita la calificación AFIS debe estar marcado como "AFIS Inglés" cuando se presta el servicio de información de vuelo y alerta en Inglés en el aeródromo donde tiene la intención de ejercer.

Esto puede estar en la calificación cuando el oficial cumpla con las siguientes condiciones:

- tienen un certificado de dominio del idioma inglés[Nivel B1](https://www.coe.int/fr/web/common-european-framework-reference-languages/table-1-cefr-3.3-common-reference-levels-global-scale) El Marco Común Europeo para las Lenguas (CECRL);
- han recibido formación sobre temas aeronáuticos y fraseología (todos los giros típicos en un entorno determinado) de conformidad con los temas del Apéndice 7 al decreto de 16 de julio de 2007.

**Qué saber**

Esto sólo es válido por un período de tres años. En cada renovación, el oficial de AFIS deberá hacer una solicitud por escrito a la DSAC junto con los siguientes documentos:

- Un certificado de dominio del idioma inglés de nivel B1 emitido dentro de los seis meses de la renovación;
- un certificado de formación sobre temas aeronáuticos y fraseología expedido por un proveedor de AFIS.

*Para ir más allá*: Artículo 2-1 y Apéndices 5, 6 y 7 de la Orden del 16 de julio de 2007.

#### Costos asociados con la calificación

Los costos de capacitación para los oficiales de AFIS varían según la organización de capacitación. Para más información, es aconsejable acercarse a uno de ellos.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Todo nacional de un Estado miembro de la Unión Europea (UE) o parte en el Espacio Económico Europeo, establecido y ejercita legalmente la actividad de un agente de AFIS en ese Estado, podrá ejercer la misma actividad en Francia, de forma temporal y ocasional.

Primero debe solicitarlo mediante declaración escrita dirigida a la DSAC, 50 Henry Farman Street, 75720 Paris Cedex 15 (véase infra "5o. a. Obtener una licencia para ejercer para el nacional un ejercicio temporal o casual (LPS)").

En el caso de que la profesión no esté regulada, ni en el curso de la actividad ni en el marco de la formación, en el estado en el que el profesional esté legalmente establecido, deberá haber realizado esta actividad durante al menos un año, en el transcurso de los diez años antes del beneficio, en uno o más Estados de la UE.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación en Francia, la DSAC competente podrá exigir que el interesado se someta a una prueba de aptitud.

*Para ir más allá* párrafo IV del artículo 2 del artículo 2 de la Orden de 16 de julio de 2007.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Para ejercer como agente de AFIS en Francia de forma permanente, la UE o el nacional del EEE deben cumplir una de las siguientes condiciones:

- Poseer un certificado de competencia o certificado de formación requerido para la profesión de agente AFIS en un Estado de la UE o del EEE al regular la profesión en su territorio;
- cuando el Estado de la UE o del EEE no regule esta profesión:- han sido agentes de AFIS durante al menos un año, a tiempo completo o a tiempo parcial, durante los últimos diez años,
  - tener un certificado de competencia o prueba de título de capacitación que justifique que el nacional ha preparado el ejercicio del agente AFIS.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar el reconocimiento AFIS de la DSAC, 50 rue Henry Farman, 75720 Paris Cedex 15 (ver más abajo "5o. b. Obtener la calificación AFIS para el nacional para un ejercicio permanente (LE)).

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación en Francia, la DSAC competente podrá exigir que el interesado se someta a medidas de compensación (véase infra 5. a. Bueno saber: medidas de medidas de compensación").

*Para ir más allá* párrafoiI+I a III del artículo 2 de la Orden de 16 de julio de 2007.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

En caso de impacto grave o negligencia en el desempeño de sus funciones, el oficial de AFIS puede tener su calificación AFIS suspendida o retirada, respectivamente, con o sin formalidad, cuando la emergencia y la seguridad lo requieran.

*Para ir más allá*: Artículo R.135-8 del Código de Aviación Civil.

4°. Formación profesional continua
-------------------------------------------------------

La formación profesional continua de 6 horas al año debe ser seguida por el agente de AFIS. Permite al profesional actualizar regularmente sus conocimientos sobre las nuevas regulaciones y sus habilidades, así como mantener su nivel de habilidades de idioma B1 requeridas para la designación "Inglés AFIS".

Al final de cada formación continua, el oficial de AFIS recibe un certificado que justifica que ha completado con éxito la sesión de mantenimiento de habilidades.

En cualquier caso, cada oficial de AFIS tendrá que ser capaz de justificar, a lo largo de su carrera, el seguimiento de esta educación continua anual presentando un folleto de capacitación actualizado.

Además, cuando se requieran menciones en inglés, el solicitante debe mantener un mínimo de nivel B1 y someterse a una formación continua en su conocimiento de la fraseología y temas aeronáuticos en la Lista 7 del pedido 16. Julio de 2007.

*Para ir más allá*: Artículos 11 y 11-1 de la Orden de 16 de julio de 2017.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Obtener una licencia para ejercer para el nacional para un ejercicio temporal o casual (LPS)

**Autoridad competente**

La Dirección de Seguridad de la Aviación Civil tiene la autoridad para decidir sobre la solicitud de autorización para realizar el servicio.

**Documentos de apoyo**

En apoyo de su solicitud de licencia, el nacional presenta a la DSAC territorialmente competente un expediente que contiene los siguientes documentos justificativos:

- un certificado de reconocimiento de su competencia, cuando la profesión esté regulada en el Estado de la UE o en el EEE en el que esté establecida;
- un certificado que justifique su actividad durante al menos un año, en los últimos diez años, cuando ni la formación ni el ejercicio de la profesión están regulados en su estado;
- si es necesario, un certificado de dominio del idioma (véase más arriba "2. b. Mención en inglés AFIS").

**hora**

Una vez recibido el expediente, la autoridad competente dispondrá de un mes para tomar su decisión, que podrá ser:

- permitir que el nacional preste su primer servicio;
- solicitar más información. En este caso, tendrá dos meses después de enviar estos documentos para decidir;
- en caso de diferencias sustanciales entre la formación del nacional y la de Francia, y dadas las implicaciones para la seguridad pública, someterlo a una prueba de aptitud en el plazo de un mes a partir de su decisión. Si la persona tiene éxito, la autoridad competente le otorgará la calificación AFIS para llevar a cabo la ejecución;
- no autorizar la entrega.

*Para ir más allá* párrafo IV del artículo 2 del artículo 2 de la Orden de 16 de julio de 2007.

### b. Obtener la calificación AFIS para el nacional para un ejercicio permanente (LE)

**Autoridad competente**

La Dirección de Seguridad de la Aviación Civil tiene la autoridad para decidir sobre la solicitud de calificación AFIS.

**Documentos de apoyo**

En apoyo de su solicitud de calificación, el nacional presenta un expediente a la DSAC que contiene los siguientes documentos justificativos:

- Un certificado de reconocimiento de la competencia de uno cuando la profesión está regulada en la UE o en el Estado del EEE;
- un certificado que justifique su actividad durante al menos un año en los últimos diez años, cuando ni la formación ni el ejercicio de la profesión estén regulados en el Estado miembro;
- si es necesario, un certificado de dominio del idioma (véase más arriba "2. b. Mención en inglés AFIS").

**Resultado del procedimiento**

Una vez recibidos todos los elementos del expediente, la DSAC podrá decidir sobre la emisión del título AFIS o, en su caso, sobre la aplicación de medidas de compensación por parte del interesado.

Una vez otorgada la calificación AFIS, el nacional podrá ejercer como agente de AFIS en un aeródromo específico.

**Bueno saber: medidas de compensación**

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a la medida de compensación de su elección, que puede ser:

- un curso de adaptación, a veces con formación adicional, que se evaluará al final del proyecto;
- una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

*Para ir más allá* los párrafos II y III del artículo 2.3 y los artículos 4, 5 y 11 de la orden de 16 de julio de 2007.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

