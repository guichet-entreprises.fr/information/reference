﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP179" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Monitor de canoa-kayak" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="monitor-de-canoa-kayak" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/monitor-de-canoa-kayak.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="monitor-de-canoa-kayak" -->
<!-- var(translation)="Auto" -->


Monitor de canoa-kayak
======================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El instructor de canoa-kayak prepara, organiza y lleva a cabo sesiones introductorias y avanzadas de canoa en aguas tranquilas, en aguas blancas o en el mar, dependiendo de su entrenamiento. Supervisa de forma independiente las caminatas y caminatas basadas en las referencias educativas definidas por la Federación Francesa de Canoa-Kayak (FFCK) garantizando la seguridad de los profesionales y terceros.

El instructor de canoa-kayak organiza situaciones educativas adaptadas en tiempo y espacio a las características específicas y expectativas de los practicantes. Participa en la organización y evaluación de sesiones técnicas de cruce de nivel.

Dependiendo de sus cualificaciones profesionales, el instructor de canoa-kayak puede tener prerrogativas limitadas (dependiendo de su diploma, podrá supervisar a cualquier público dentro de ciertos límites geográficos y en ciertas condiciones climáticas).

*Para ir más allá* : orden del 12 de agosto de 2013 registrándose en el Directorio Nacional de Certificaciones Profesionales (RNCP) de la opción de instructor de canoa-kayak "canoe-kayak in calm water - white water" y "canoe-kayak in water calma - mar", decreto de 9 de julio de 2002 por el que se establece la especialidad "actividades náuticas" del certificado profesional de juventud, educación popular y deporte (BPJPS), decretada a partir del 1 de julio de 2008 con la creación de las palabras "canoe-kayak y disciplinas asociadas con el agua calmada" y "canoe-kayak y disciplinas afines en aguas blancas" del Diploma Estatal de Juventud, Educación Popular y Deporte (DEJEPS) y decretadas a partir del 1 de julio de 2008 con la creación de las palabras "canoe-kayak y disciplinas" asociados con el agua tranquila" y "canoeing y disciplinas asociadas en aguas blancas y en el mar" del Diploma Estatal Superior de Juventud, Educación Popular y Deporte (DESJEPS).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Como profesor de deportes, el instructor de canoa-kayak debe poseer un diploma, un título profesional o un certificado de calificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

La actividad de instructor de canoa-kayak se rige por los artículos L. 212-1 y los siguientes artículos del Código Deportivo que requieren certificaciones incluyendo el CQP, el certificado de aptitud profesional del técnico facilitador asistente del juventud y deportes (BAPAAT), las "actividades náuticas" de BPJEPS, el "desarrollo deportivo" SPECIALty DEJEPS mencionan "canoe-kayak y disciplinas asociadas (CKDA) en aguas tranquilas" o "CKDA en aguas bravas" y la mención DESJEPS "rendimiento deportivo especializado" CKDA en aguas tranquilas" o "CKDA en aguas blancas y mar".

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos A. 212-1 Apéndice II-1, L. 212-1 y R. 212-84 del Código del Deporte.

**Bueno saber: el entorno específico**

La práctica del piragua y las disciplinas asociadas en el río superior de tres clases es una actividad en un entorno específico que implica el cumplimiento de medidas de seguridad específicas. Por lo tanto, sólo las organizaciones bajo la tutela del Ministerio del Deporte pueden formar a futuros profesionales.

*Para ir más allá*: Artículos L. 212-2 y R. 212-7 del Código del Deporte.

#### Entrenamiento

##### Instructor de canoa-kayak de CQP

El instructor de canoa-kayak DE CQP es un diploma de coaching profesional e informal administrado por la FFCK.

Hay dos opciones:

- aguas tranquilas - aguas blancas;
- agua tranquila - mar.

Este diploma se puede obtener a través de la formación profesional continua, el aprendizaje o la validación de la experiencia (VAE). Para obtener más información, puede ver[sitio web oficial](http://www.vae.gouv.fr/) Vae.

**Tenga en cuenta que**

El CQP es también un trampolín para las profesiones docentes de los deportes de pádel. Este es un primer paso hacia los títulos estatales y, en particular, hacia el BPJEPS, que tiene prerrogativas más amplias (apoyos y límites de condiciones de práctica) y permite la supervisión a tiempo completo.

**Prerrogativas del CQP**

El titular del instructor de canoa-kayak CQP puede supervisar, animar y enseñar deportes de pádel dentro de las 360 horas al año:

- opciones de agua tranquila - agua blanca y aguas tranquilas - mar: en aguas tranquilas, el instructor supervisa todos los deportes de pádel;
- Opción de agua tranquila - aguas blancas: en aguas blancas, el instructor supervisa todos los deportes de pádel excepto la balsa de pádel en cursos de dificultad que llegan a clase II incluidos con pasajes no sucesivos de clase III;
- opción tranquila agua-mar: en el mar, el instructor supervisa todos los deportes de pádel hasta una milla de un refugio en condiciones de dificultad viento de fuerza máxima 3 Beaufort y oleaje de un metro en el sitio de la evolución.

**Condiciones de acceso a la formación que conduce al CQP:**

El interesado deberá:

- Tener 18 años el día de la entrada en formación;
- dar fe de un nivel de canotaje al menos igual al de la "paleta de agua azul y tranquila";
- pasar las pruebas de entrada
- Proporcione una copia del documento de identidad nacional
- comunicar un certificado médico de no contradictorio con la práctica y enseñanza de los deportes de piragua y pádel;
- Celebración de la Unidad de Educación de Socorro y Prevención Cívica de Nivel 1 (PSC1);
- certificación de un nivel de natación-rescate consistente con el formulario estándar que se adjuntará al archivo de registro.

Es posible que se requieran documentos adicionales al registrarse en una de las organizaciones de formación (por ejemplo, certificado censal). Para obtener más información, es aconsejable acercarse a la organización en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertas pruebas, especialmente para aquellos con un diploma federal. Para más información, es aconsejable consultar el [Sitio web de FFCK](http://www.ffck.org/federation/formation/encadrement-professionnel/#1454081500030-1e5f308b-6d61).

*Para ir más allá*: Artículos L. 212-1 y R. 212-84 del Código del Deporte.

##### Opción BAPAAT "recreación al aire libre", soporte técnico "senderismo acuático, canoa" o "senderismo acuático, balsa" o "senderismo acuático, natación en aguas blancas" o "senderismo acuático, kayak de mar"

Se trata de un diploma estatal aprobado en el nivel V (CAP, BEP, BEPC), común al sector sociocultural y deportivo. El BAPAAT representa el primer nivel de cualificación para la animación y supervisión de actividades deportivas y socioculturales.

Las opciones de acceso de apertura BAPAAT al marco de canoa-kayak son la opción de ocio al aire libre BAPAAT, soporte técnico:

- senderismo acuático, canoa;
- caminata acuática, balsa;
- senderismo acuático, natación en aguas bravas;
- o senderismo acuático, kayak de mar.

El entrenamiento se lleva a cabo alternativamente. El volumen de formación incluye de 1.500 a 2.000 horas de instrucción general, tecnológica y profesional.

**Condiciones de acceso a la formación que conduce a BAPAAT**

- Tener al menos 16 años de edad
- no hay requisito previo para un título, pero se recomienda un buen nivel de práctica personal.

*Para ir más allá*: Artículos D. 212-11 y siguientes del Código del Deporte, A. 212-2 y siguientes, orden de 4 de marzo de 1993 relativa a la creación y organización de las opciones profesionales de BAPAAT, orden de 10 de agosto de 1993 relativa a la lista de actividades socioculturales que pueden utilizarse como apoyo a la actividad profesional de los titulares de BAPAAT.

##### BPJEPS "actividades náuticas"

Las actividades náuticas de BPJEPS atestian la posesión de las competencias profesionales esenciales para el ejercicio del instructor de actividades náuticas. Es un grado de nivel 4 (como una licenciatura técnica, una licenciatura tradicional o un certificado de técnico).

La capacitación puede ser común a varias disciplinas, pero el candidato debe elegir las "menciones" que son apropiadas para su proyecto. Puede elegir una sola mención para ser un especialista en una disciplina (esa es la designación monovalente) o varias opciones para responder a un perfil más generalista (esa es la mención multivalente).

La referencia monovalente a los deportes de pádel se llama "Actividades náuticas BPJEPS mención CKDA". El candidato generalista puede elegir al menos 2 opciones multicandidatas en dos grupos diferentes:

- Grupo A: canoa "agua tranquila, mar y olas" o canoa "agua tranquila y río de aguas bravas";
- Grupo B: remo en el mar - inicio y descubrimiento de remo;
- Grupo C: tanque de vela;
- Grupo D: crucero costero - multicasco y bote - windsurf;
- Grupo E: iniciación y descubrimiento del esquí acuático;
- Grupo F: jet - lanchas de iniciación y descubrimiento - equipo remolcada;
- Grupo G: paracaidismo de ascenso náutico;
- Grupo H: Diapositiva prolongada.

El candidato está en formación profesional y trabaja bajo la autoridad de un tutor. La formación se lleva a cabo alternativamente entre el centro de formación y la estructura para la recepción de situaciones profesionales. Tiene una duración total de 600 horas en un centro de formación y 600 horas de estructura para la formación inicial, repartidas en 12 a 24 meses dependiendo de la organización.

**Tenga en cuenta que**

Los titulares de la "BEES 1 canoe kayak y disciplinas relacionadas" pueden obtener las actividades acuáticas de BPJEPS como "canoe kayak y disciplinas relacionadas" como "canoeing y disciplinas relacionadas". Es posible solicitarlo a la Dirección Regional de Deportes Juveniles y Cohesión Social (DRJSCS).

**Prerrogativas****

Las actividades náuticas de BPJEPS permiten supervisar la remuneración con plena autonomía. Los lugares donde se realizan las actividades son diversos y variados: asociación, club deportivo, negocios, autoridad local, institución para personas mayores, etc. Los públicos que pueden ser supervisados son diversos: niños, adultos, ancianos, deportistas, turistas, etc.

Límites de ejercicio:

- Mención monovalente de la CKDA: El instructor supervisa y lleva a cabo actividades de descubrimiento e iniciación, incluyendo los primeros niveles de competencia en cualquier soporte o barco impulsado por remo o natación. Se produce sólo en aguas tranquilas, en el mar con fuerza máxima 4 viento en el sitio de evolución, en aguas blancas hasta la clase III incluida, así como en aguas blancas hasta la clase III incluidas y en los cañones enumerados hasta incluyendo V1, A5 y E II;
- mención multivalente "agua tranquila de canoa-kayak, mar y olas": el monitor supervisa las actividades de la CKDA para todos los públicos, en aguas tranquilas y en el mar, dentro de los límites de la navegación en sexta categoría en rutas conocidas y reconocidas, a lo sumo por fuerza viento 4 en el evolución. También dirige sesiones introductorias en kayak de olas;
- mención multivalente "agua tranquila de canoa-kayak y río de aguas bravas": el instructor supervisa las actividades de la CKDA para todos los públicos, en aguas tranquilas y río, hasta clase III incluida.

**Condiciones de acceso a la formación que conducen al PJEPS**

El interesado deberá:

- Ser mayores de edad
- Tener un certificado de formación en primeros auxilios (PSC1, AFPS o equivalente);
- aprobar los exámenes de ingreso en el instituto de formación;
- presentar un certificado médico de no contradictorio con la práctica de las actividades náuticas, que se remonta a menos de tres meses a la fecha de entrada en la formación;
- producir un certificado emitido por un socorrista de 100 metros estilo libre, inicio de buceo y recuperación de un objeto sumergido a una profundidad de 2 metros;
- para cumplir con los eventos deportivos (llamados pruebas previas a los requisitos) detallados en la Lista III del orden del 9 de julio de 2002 y cuyo contenido depende de la mención o menciones elegidas.

Es posible que se requieran documentos adicionales al registrarse en una de las organizaciones de formación (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación correspondiente.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable referirse al decreto de 9 de julio de 2002 mencionado anteriormente.

*Para ir más allá*: Artículos L. 212-2 y D. 212-20 y siguientes del Código del Deporte, orden de 9 de julio de 2002 por la que se establece la especialidad "actividades náuticas" del BPJEPS.

**Es bueno saber**

Los titulares de BpJEPS especializados "actividades acuáticas" pueden ampliar sus prerrogativas pasando una unidad complementaria capitalizada (UCC) "canoe-kayak, aguas tranquilas y río de aguas bravas" o "canoe-kayak, aguas tranquilas, mar y olas". Para más información, es aconsejable consultar el [Sitio](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/BPJPES/Reglementation-11011/Les-unites-capitalisables-complementaires/) Ministerio del Deporte.

##### LA especialidad DEJEPS "desarrollo deportivo" menciona "CKDA en aguas tranquilas" o "CKDA en aguas bravas"

DEJEPS es un diploma aprobado por el Nivel III (Bac 2/3). Da fe de la adquisición de una cualificación en el ejercicio de una actividad profesional de coordinación y supervisión con fines educativos en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Este diploma se puede obtener en formación inicial realizando una formación de 700 horas en el centro y 500 horas en la estructura alterna y validando la experiencia.

Los diplomas DeJEPS que le permiten llevar a cabo la actividad de instructor de canoa-kayak son:

- DEJEPS menciona el agua tranquila de LA CKDA;
- DEJEPS menciona el agua blanca DE la CKDA;
- y el Certificado de Especialización del Mar "CKDA en el Mar".

El diploma se prepara a través de la formación inicial, el aprendizaje o la educación continua.

**Prerrogativas DEJEPS**

Las habilidades dirigidas son:

- entrenamiento deportivo en los clubes y estructuras del Curso de Excelencia Deportiva (PES) de la FFCK;
- los de los riberes y/o guías marítimas, principalmente en instalaciones de ocio deportivo.

**Las condiciones de acceso a la formación que conducen a la especialidad DEJEPS "desarrollo deportivo" mencionan "CKDA en aguas tranquilas" o "CKDA en aguas blancas":**

El interesado deberá:

- Rellene el formulario de registro estándar con un documento de identidad con fotografía
- Ser mayores de edad
- certificado de formación de primeros auxilios (tipo PSC1, AFPS);
- pasar las pruebas de acceso a la formación;
- comunicar un certificado médico de no contradictorio con la enseñanza y la práctica de la CKDA en aguas tranquilas/blancas (como se indica, con menos de tres meses de edad;
- presentar un certificado de coaching independiente de un mínimo de seiscientas horas o tres temporadas deportivas, expedido por el director técnico nacional de la CKDA (DTNCKDA);
- presentar un certificado, expedido por la DTNCKDA, de participación en una competición nacional de embarcaciones regional o de primer nivel en dos ambientes diferentes, uno de los cuales se encuentra en aguas blancas o en aguas tranquilas según la mención elegida;
- presentar un certificado de éxito de la prueba técnica y pedagógica organizada por la FFCK;
- producir un certificado de éxito de una prueba de natación libre de 100 metros con un inicio de inmersión y la recuperación de un objeto sumergido a una profundidad de 2 metros.

Se pueden solicitar documentos adicionales (copia del documento nacional de identidad, certificado de participación en el día de recurso ante la defensa, certificado de responsabilidad civil, etc.). Para obtener más información, es aconsejable consultar con el centro de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4, 6 y siguientes de los autos de 12 de julio de 2007 mencionados anteriormente.

*Para ir más allá*: Artículos D. 212-35 y siguientes del Código del Deporte, ordenado el 1 de julio de 2008 por el que se crea la designación "CKDA in calm water" del SPECIALty DEJEPS "desarrollo deportivo" y ordenó a partir del 1 de julio de 2008 la creación de la palabra "CKDA in white water" de DEJEPS.

**Es bueno saber**

Los titulares de DeJEPS mencionan "canoa-kayak y disciplinas asociadas en aguas tranquilas" o "canoe-kayak y disciplinas asociadas en aguas blancas" pueden ampliar sus prerrogativas aprobando el Certificado de Especialización (CS) "canoe-kayak y disciplinas asociados con ellos en el mar. Para más detalles sobre las condiciones en las que se obtiene esta CS, es aconsejable referirse a la [sitio web oficial de este certificado](http://www.sports.gouv.fr/).

##### Especialidad DESJEPS "rendimiento deportivo" mención "CKDA en aguas tranquilas" o "CKDA en aguas blancas y mar"

El DESJEPS es un diploma aprobado por el nivel II (B.A. 3/4).

Este diploma se puede obtener en formación inicial realizando una formación de 700 horas en el centro y 500 horas en la estructura alterna y validando la experiencia.

Los diplomas que le permiten llevar a cabo la actividad de instructor de canoa-kayak son:

- DESJEPS menciona el agua tranquila de la CKDA;
- DESJEPS menciona el agua blanca y el mar de la CKDA;
- Certificado de Especialización en El Mar de la CKDA (CS).

El curso de formación se basa en el principio de alternar entre el centro de formación y la empresa (donde las situaciones de aprendizaje práctico son responsabilidad de un tutor). El diploma también se puede obtener a través del VAE.

**Es bueno saber**

Los titulares de DesJEPS están marcados como "canoe-kayak y disciplinas asociadas en aguas tranquilas" o "canoeing y disciplinas asociadas en aguas blancas y mar" pueden ampliar sus prerrogativas aprobando el Certificado de Especialización (CS) "canoe-kayak and disciplinas asociadas en el mar." Para más detalles sobre las condiciones en las que se obtiene esta CS, es aconsejable referirse a la [sitio web oficial de este certificado](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/des-jeps/Reglementation-11081/La-specialite-performance-sportive-du-DES-JEPS-et-les-mentions-unites-capitalisables-complementaire-et-certificats-de-specialisation-s-y-rapportant/Les-certificats-de-specialisation-du-DES-JEPS-specialite-performance-sportive/article/Canoe-kayak-et-disciplines-associees-en-mer-12403).

**Prerrogativas DESJEPS**

Proporciona al titular las siguientes habilidades:

- Preparar el proyecto de desempeño estratégico en un campo disciplinario;
- pilotar un sistema de entrenamiento, dirigir un proyecto deportivo;
- Evaluación del sistema de formación
- organización de acciones formativas para formadores en el marco de las redes profesionales de la organización.

**Las condiciones de acceso a la formación que conducen al DESJEPS mencionan "CKDA en aguas tranquilas" y "CKDA en aguas blancas y marítimas":**

El interesado deberá:

- Rellene el formulario de registro estándar con un documento de identidad con fotografía
- Ser mayores de edad
- certificado de formación de primeros auxilios (tipo PSC1, AFPS);
- comunicar un certificado médico de no contradictorio con la enseñanza y la práctica de la CKDA en aguas tranquilas/aguas blancas y mar (según la mención elegida), de menos de tres meses de edad;
- producir un certificado de entrenamiento deportivo de seiscientas o tres temporadas emitido por el DTNCKDA;
- presentar un certificado de participación en un barco monoplaza en un concurso a nivel nacional, ya sea en aguas tranquilas o en aguas blancas y en el mar, según la mención elegida, emitida por el DTNCKDA;
- presentar un certificado de éxito de la prueba técnica organizada por la FFCK, consistente en el análisis de un documento de vídeo para evaluar la capacidad del candidato para observar, analizar y establecer un diagnóstico con el fin de proponer una formación de canotaje ya sea en aguas tranquilas, en aguas blancas o en el mar dependiendo de la mención elegida, para un atleta o un equipo de nivel nacional, emitida por el DTNCKDA;
- producir un certificado de éxito de una inmersión libre de 100 metros y la recuperación de un objeto sumergido a una profundidad de dos metros, emitido por una persona con una certificación de actividades acuáticas.

Se pueden solicitar documentos adicionales (copia del documento nacional de identidad, certificado de participación en el día de recurso ante la defensa, certificado de responsabilidad civil, etc.). Para obtener más información, es aconsejable consultar con el centro de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos D. 212-51 y siguientes del Código del Deporte, orden de 1 o 1 de julio de 2008 por la que se crea la "CKDA en aguas bravas y marítimas" del "rendimiento deportivo" de la ESPECIE y decretael 1o de julio de 2008 por la creación de la palabra "CKDA in calm water" Especialidad DESJEPS "rendimiento deportivo".

#### Costos asociados con la calificación

Se paga la capacitación para obtener el CQP "instructor de canoa-kayak" (independientemente de la mención elegida). Su coste es de unos 2.100 euros (coste indicativo). Para más detalles, es aconsejable acercarse al centro de formación en cuestión.

La capacitación para obtener BPJEPS, DEJEPS o DESJEPS también da sus frutos. Sus costos varían dependiendo de las menciones y de las organizaciones de capacitación. Para [más detalles](https://foromes.calendrier.sports.gouv.fr/#/formation), es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como instructor de canoa-kayak en Francia para personas que han sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Obligación de presentación de informes (con el fin de obtener la tarjeta de educador deportivo profesional)

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el [sitio web oficial](https://eaps.sports.gouv.fr).

#### hora

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

#### Documentos de apoyo

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

#### Autoridad competente

La declaración previa de actividad debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde el Departamento en el que declarante quiere realizar su actuación.

#### hora

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

#### Documentos de apoyo

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### c. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

#### hora

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

#### Documentos de apoyo para la primera declaración de actividad

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

#### Evidencia para una declaración de renovación de la actividad

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

### d. Medidas de compensación

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

