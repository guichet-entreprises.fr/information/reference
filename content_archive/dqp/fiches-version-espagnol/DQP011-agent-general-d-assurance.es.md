﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP011" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Agente de Seguros Generales" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="agente-de-seguros-generales" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/agente-de-seguros-generales.html" -->
<!-- var(last-update)="2020-04-15 17:20:59" -->
<!-- var(url-name)="agente-de-seguros-generales" -->
<!-- var(translation)="Auto" -->


Agente de Seguros Generales
===========================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:59<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Agente de Seguros Generales (AGM) es un profesional independiente cuya actividad es comercializar y gestionar productos y servicios de seguros bajo un mandato establecido por una o más compañías de seguros.

Por lo tanto, el agente interviene con las compañías de seguros, pero también con una clientela de profesionales o personas que se benefician de productos y servicios de seguros.

Para esta clientela, es su misión asesorar y vigilar sus necesidades en cuanto a productos de seguros y acompañarlos desde la firma de su contrato hasta su indemnización en caso de desastre.

Como profesional independiente, la AGM gestiona y gestiona su negocio de forma independiente y participa en la contratación y formación de su equipo.

*Para ir más allá* : decreto de 5 de marzo de 1949 y decreto de 28 de diciembre de 1950 que garantiza, respectivamente, la condición de agentes de seguros generales (accidentes, incendios, riesgos diversos) y el de los agentes generales de seguros de vida;[decreto del 15 de octubre de 1996](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000195590&categorieLien=id) aprobación de la condición de agentes de seguros generales.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

### Legislación nacional

Cualquier persona que desee ejercer la profesión de AGM debe justificar una capacidad profesional en el seguro de Nivel I, obligatorio para presentar productos de seguro.

Para obtener más información sobre los niveles de capacidad profesional en seguros, es aconsejable[Orias](https://www.orias.fr/web/guest/assurance2).

### Entrenamiento

Para ejercer como AGM, la persona debe justificar:

- o una pasantía profesional cuyo programa mínimo de formación se establece en el[decretado a partir del 11 de julio de 2008](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000019216662). No puede ser inferior a 150 horas y debe llevarse a cabo:- ya sea con una compañía de seguros, una entidad de crédito, una compañía financiera o un intermediario si está registrado como corredor o agente de seguros generales,
  - ya sea en un centro de formación elegido por él mismo o por el director;

**Tenga en cuenta que**

Al final de la pasantía, el profesional estará sujeto a un control de las habilidades adquiridas.

- dos años de experiencia ejecutiva en una función relacionada con la producción o gestión de contratos de seguros o capitalización dentro de una compañía de seguros o intermediario de seguros;
- cuatro años de experiencia en una función relacionada con la producción o gestión de contratos de seguros o capitalización dentro de estas mismas empresas o intermediarios;
- titular de un diploma o título correspondiente:- ya sea a nivel del maestro,
  - o simultáneamente a nivel de licenciatura y formación 313 en actividades financieras, bancarias, de seguros e inmobiliarias;
- es decir, un certificado de cualificación profesional inscrito en el directorio nacional de certificaciones profesionales y correspondiente a la especialidad de formación 313 relacionada con actividades financieras, bancarias, de seguros e inmobiliarias.

*Para ir más allá*: Artículos R. 512-9, R. 512-11 y A. 512-6 del Código de Seguros.

### Costo de la formación

El entrenamiento de AGM vale la pena. Para obtener más información, es aconsejable acercarse a un centro de formación o a las instituciones que emiten los diplomas o certificados mencionados anteriormente.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (restauración de servicios gratuitos)

Un nacional de un Estado de la Unión Europea (UE) o parte en el Espacio Económico Europeo (EEE) establecido y que opere legalmente AGM en uno de estos Estados puede llevar a cabo la misma actividad en Francia de forma temporal y ocasional.

Simplemente debe informar previamente al registro de su estado, que garantizará que la información se transmita al propio registro francés (véase infra "5o. a. Informar al registro único del Estado de la UE o del EEE").

*Para ir más allá* : Directiva europea de intermediación de seguros de 9 de diciembre de 2002 conocida como " [DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) » ; Orden de 2018-361 de 16 de mayo de 2018 y Decreto No 2018-431 de 1 de junio de 2018 que transponen la Directiva 2016/97 de 20 16 de enero de 2016 y que entrará en vigor el 1 de octubre de 2018.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

El nacional de un Estado de la UE o del EEE establecido y que opere legalmente AGM en uno de estos Estados puede llevar a cabo la misma actividad en Francia de forma permanente.

Simplemente debe informar previamente al registro de su estado, que garantizará que la información se transmita al propio registro francés (véase infra "5o. a. Informar al registro único del Estado de la UE o del EEE").

*Para ir más allá* : Directiva europea de intermediación de seguros de 9 de diciembre de 2002 conocida como " [DIA1](http://eur-lex.europa.eu/legal-content/FR/ALL/?uri=celex:32002L0092) » ; Orden de 2018-361 de 16 de mayo de 2018 y Decreto No 2018-431 de 1 de junio de 2018 que transponen la Directiva 2016/97 de 20 16 de enero de 2016 y que entrará en vigor el 1 de octubre de 2018.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

La AGM está obligada a cumplir con las condiciones de honor que son responsabilidad de su profesión, de acuerdo con la[Artículo L. 322-2 del Código de Seguros](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006073984&idArticle=LEGIARTI000006797493&dateTexte=&categorieLien=cid). En particular, no debe haber sido condenado por menos de diez años por delitos, blanqueo de dinero, corrupción o evasión fiscal, o haber sido removido de las funciones oficiales públicas o ministeriales.

**Tenga en cuenta que**

Los Orias solicitan directamente un extracto del boletín 2 del historial penal de la AGM para el ejercicio de la profesión.

*Para ir más allá*: Artículos L. 322-2, L. 512-4 y R. 514-1 del Código de Seguros.

4°. Requisito de inscripción y seguro
----------------------------------------------------------

### a. Solicitar inscripción en el registro de Orias

El interesado que desee ejercer en Francia como AGM debe estar inscrito en el registro único de intermediarios en seguros, banca y finanzas (Orias).

**Documentos de apoyo**

El registro está precedido por el[abrir una cuenta](https://www.Orias.fr/espace-professionnel) Orias y el envío de un archivo que contiene todos los siguientes documentos justificativos:

- Una copia del documento de identidad de la persona
- un documento que justifique su capacidad profesional o un extracto de KBis de menos de tres meses si la AGM está inscrita en el registro de comercio y empresas, tal como se especifica en el párrafo "2." a. Entrenamiento":- un folleto de pasantías,
  - Un certificado de formación,
  - Un certificado de oficina,
  - Un diploma o certificado
- cualquier documento que acredite la existencia de una o más órdenes;
- liquidación de las tasas de registro.

**Renovación del registro**

El procedimiento de inscripción en orias debe renovarse cada año y con cualquier cambio en la situación profesional de la persona. En este último caso, la AGM tendrá que mantener a los Orias informados un mes antes del cambio o dentro de un mes del evento de modificación.

La solicitud de renovación debe tener lugar antes del 31 de enero de cada año y debe incluir el pago de las tasas de inscripción.

**Qué saber**

Las plantillas de certificación están disponibles directamente en el[Orias](https://www.Orias.fr/web/guest/en-savoir-plus-ias).

**Costo**

La cuota de inscripción se fija en 30 euros pagaderos directamente en línea en el sitio web de Orias. En caso de impago de estos gastos, orias envía una carta informando al interesado de que dispone de un plazo de treinta días a partir de la fecha de recepción del correo para abonar la suma. No liquidar esta cantidad:

- No se tendrá en cuenta la solicitud de primer registro en orias;
- la AGM será eliminada del registro si se trata de una solicitud de renovación.

*Para ir más allá*: Artículo R. 512-5 del Código de Seguros.

### b. Obligación de contrato de seguro de responsabilidad civil profesional

En caso de ejercicio liberal, el individuo está obligado a conllevar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus trabajadores por los actos realizados en ocasiones.

*Para ir más allá*: Artículo 1242 del Código Civil; Secciones L. 512-6, R. 512-4 y A. 512-4 del Código de Seguros.

### c. Sanciones penales

El incumplimiento de las obligaciones contempladas en los apartados 3 y 4 se castiga con una pena de dos años de prisión y una multa de 6.000 euros, o sólo una de estas dos sentencias.

*Para ir más allá*: Artículo L. 514-1 del Código de Seguros.

5°. Procedimientos y formalidades de reconocimiento de cualificaciones
--------------------------------------------------------------------------------

### a. Informar al registro único del Estado de la UE o del EEE

**Autoridad competente**

El nacional de un Estado miembro de la UE o del EEE que haya estado activo en ese Estado y que desee ejercer el servicio libre o el establecimiento libre en Francia deberá informar en primer lugar al registro único de su Estado.

**Procedimiento**

El nacional deberá facilitar la siguiente información al registro único de su estado:

- Su nombre, dirección y, en su caso, número de registro;
- El Estado miembro en el que desea operar en caso de LPS o establecerse en caso de LE;
- La categoría de intermediario de seguros a la que pertenece, a saber, la de AGM y, en su caso, el nombre de cualquier compañía de seguros o reaseguros a la que represente;
- de seguros, si es necesario.

**hora**

El registro único del Estado de la UE o del EEE, del que el nacional ha notificado su intención de operar en Francia, dispone de un mes para facilitar a los Orias la información relativa a él.

*Para ir más allá*: Artículos L. 515-1 y L. 515-2 del Código de Seguros; Artículo 6 de la Directiva de la UE "DIA1", de 9 de diciembre de 2002;[Artículos 4 y 6 de la Directiva Europea sobre Intermediación de Seguros de 20 de enero de 2016](https://www.Orias.fr/documents/10227/26917/2016-01-20_Directive%20europeenne%20sur%20la%20distribution%20assurances.pdf).

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

