﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP151" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Identificación equina" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="identificacion-equina" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/identificacion-equina.html" -->
<!-- var(last-update)="2020-04-15 17:20:55" -->
<!-- var(url-name)="identificacion-equina" -->
<!-- var(translation)="Auto" -->


Identificación equina
=====================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:55<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La identificación de equinos por transpondedor es obligatoria y permite que el equino se registre en el archivo central del SIRE ("Sistema de Información Equina").

Como recordatorio, los equinos forman la familia de mamíferos que actualmente comprende tres grupos de especies: caballos, burros y cebras. Estas especies tienen la distinción de tener sólo un dedo en cada extremidad, terminada con una pezuña.

La identificación se realiza mediante un identificador declarado con los servicios del Instituto Francés de Equitación y Equitación (IFCE), encargado de implantar un transpondedor electrónico en el cuello del equino y llevar a cabo la lectura del informe de Equina. El responsable de la identificación completa así un formulario de identificación sobre el terreno (papel o desmaterializado) que transmite a la IFCE o a un organismo emisor para el registro y edición del documento de identificación.

*Para ir más allá*: Artículo L. 212-9 del Código Rural y Pesca Marina.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Puede llevar a cabo la actividad de identificación de equinos:

- veterinarios en ejercicio, es decir, en el Colegio de Veterinarios;
- veterinarios de ejércitos activos;
- veterinarios declarados que ejercen en Francia de forma temporal y casual;
- Investigadores de profesores veterinarios como parte de su misión docente en las escuelas veterinarias nacionales;
- Funcionarios de la IFCE u funcionarios contractuales que tengan acceso acumulativo a:- un certificado que certifique su capacidad para identificar a los equinos mediante la inspección de marcas naturales,
  - certificado expedido después de la formación específica para el marcado activo por implantación de un transpondedor, cuyos términos se establecen por[decretado a partir del 24 de febrero de 2003](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000005634024&dateTexte=20150811).

*Para ir más allá*: Artículo D. 212-58 del Código Rural y Pesca Marina.

#### Entrenamiento

Sólo aquellos que han sido certificados como veterinarios o oficiales de IFCE que han sido capacitados para hacerlo pueden solicitar ser colocados en la lista de identificación equina.

#### Formación veterinaria

Para conocer todo lo que se refiere a la formación y el acceso a la profesión veterinaria en Francia, es aconsejable consultar la ficha de cualificación prevista para este fin.

#### Capacitación de oficiales de la IFCE

La formación de los directivos de la IFCE consiste en cursos teóricos y trabajos prácticos cuyo contenido se define en consulta con la profesión veterinaria. Al final de la capacitación, el oficial de la IFCE recibirá un certificado de capacidad por parte de una Comisión Nacional de Identificación Electrónica de equinos, basado en los informes de los instructores y el informe resumido preparado por el gerente de capacitación. para juzgar la capacidad del oficial para hacer identificaciones.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal o ocasional (prestación gratuita de servicios)

Los nacionales veterinarios de un Estado miembro de la UE o del EEE que deseen, de forma temporal y ocasional, identificar los equinos en Francia deben declararse primero como veterinarios que practican de forma temporal y ocasional. Para conocer todas las modalidades a realizar, es aconsejable consultar la ficha de calificación prevista a tal efecto.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

El nacional veterinario de un Estado miembro de la UE o del EEE que desee tener derecho permanente a identificar los equinos en Francia debe tomar las medidas para:

- para ejercer la profesión veterinaria de forma permanente. Para conocer todas las modalidades a realizar, es aconsejable consultar la ficha de calificación prevista a tal efecto;
- ser un servidor público o agente contractual de la IFCE.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El veterinario está sujeto a las normas de ética aplicables a la profesión (Artículos R. 242-33 a R. 242-84 Código de Pesca Rural y Marina).

4°. Seguro
-------------------------------

Los veterinarios en la práctica o en la libre prestación de servicios deben tomar un seguro de responsabilidad civil profesional cuya validez se reconozca en territorio francés.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales veterinarios de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

#### Autoridad competente

El Director General de la IFCE es responsable de recibir la predeclaración de la práctica temporal u ocasional de la actividad identificadora. Antes de esta declaración, el profesional debe haber hecho su declaración previa de práctica para la profesión veterinaria al Consejo Nacional del Colegio de Veterinarios.

#### Documentos de apoyo

La pre-notificación de la actividad del identificador equino por parte de un veterinario se realiza electrónicamente. El profesional envía una solicitud de correo electrónico a la dirección de info@ifce.fr adjuntando el[formulario de solicitud cumplimentado y firmado descargable](https://www.ifce.fr/wp-content/uploads/2018/07/SIRE-DECLARATION-VETO-IDENTIFICATEURV2.pdf).

*Para ir más allá* : orden de 25 de junio de 2018 relativo a la identificación de equinos.

#### hora

El Director General de la IFCE dispone de un mes a partir de la recepción del expediente para tomar su decisión:

- permitir que el reclamante realice su beneficio. En caso de incumplimiento de este plazo de un mes o del silencio de la administración, podrá llevarse a cabo la prestación de servicios;
- informarles de una o más dificultades que puedan retrasar la toma de decisiones. En este caso, dispondrá de dos meses para decidir, a partir de la resolución de las dificultades y, en cualquier caso, en un plazo máximo de tres meses a partir de la información del interesado sobre la existencia de la dificultad o dificultades. En ausencia de una respuesta de la autoridad competente dentro de este plazo, podrá comenzar la prestación de servicios.

*Para ir más allá*: Artículo R. 204-1 del Código Rural y Pesca Marina.

### b. Registro en la lista de identificadores equinos para los nacionales de la UE que realizan actividades permanentes (LE)

#### Autoridad competente

El Director General de la IFCE es responsable de autorizar a quienes deseen identificar permanentemente la actividad de identificación de los equinos en Francia.

#### Procedimiento

El nacional que desee identificar los equinos debe solicitar primero el registro en el Colegio de Veterinarios y, a continuación, inscribirlo en la lista de identificadores ante el Director General de la IFCE.

La solicitud de inscripción en la lista de identificación se realiza electrónicamente. El profesional envía una solicitud de correo electrónico a la dirección de info@ifce.fr adjuntando el[formulario de solicitud cumplimentado y firmado descargable](https://www.ifce.fr/wp-content/uploads/2018/07/SIRE-DECLARATION-VETO-IDENTIFICATEURV2.pdf).

*Para ir más allá* : orden de 25 de junio de 2018 relativo a la identificación de equinos.

#### Costo

Gratis.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas, excepto para los veterinarios para los que sólo la Orden es competente.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

