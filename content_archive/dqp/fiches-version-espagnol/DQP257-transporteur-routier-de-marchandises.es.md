﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP257" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transporte" -->
<!-- var(title)="Transportista de mercancías por carretera" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transporte" -->
<!-- var(title-short)="transportista-de-mercancias-carretera" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/transporte/transportista-de-mercancias-por-carretera.html" -->
<!-- var(last-update)="Augusto de 2021" -->
<!-- var(url-name)="transportista-de-mercancias-por-carretera" -->
<!-- var(translation)="Auto" -->


Transportista de mercancías por carretera
=========================================

Actualización más reciente: <!-- begin-var(last-update) -->Augusto de 2021<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La empresa de camiones es un profesional cuya actividad es prestar un servicio para la entrega de mercancías a través de un vehículo de motor.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de un transportista de mercancías por carretera, el profesional debe:

- Estar registrado:- registro comercial y corporativo (RCS) o directorio de operaciones. Es aconsejable consultar la hoja "Registro RCS" para obtener más información,
  - El Registro Electrónico Nacional de Empresas de Transporte por Carretera;
- cumplir con los requisitos:- Asentamiento
  - profesionalmente honrado,
  - capacidad financiera y profesional.

Además, toda empresa que desee operar como transportista de mercancías por carretera debe designar un administrador de transporte para gestionar el mantenimiento del vehículo, la verificación de contratos y documentos de transporte, la contabilidad , comprobar los procedimientos de seguridad y asignar cargas o servicios a los conductores.

#### Condiciones de acceso a la profesión

**Capacidad profesional**

El profesional que desee llevar a cabo la actividad de un transportista de mercancías por carretera deberá:

- poseer un diploma, un título universitario, un certificado de estudio o una designación profesional expedido en Francia por una institución de educación superior o un organismo autorizado y, siempre y cuando haya recibido los conocimientos necesarios para realizar esta actividad durante su entrenamiento;
- justificar la gestión continua de una empresa pública de transporte de mercancías, de mudanzas o de arrendamiento de vehículos industriales con conductores destinados al transporte de mercancías en uno o más Estados de la Unión Europea durante el 10 años antes del 4 de diciembre de 2009.

Incumpliendo cualquiera de estas condiciones, deberá someterse a un examen escrito sobre todos los temas establecidos en la Lista I del Reglamento (CE) 1071/2009 del Parlamento Europeo y del Consejo, de 21 de octubre de 2009, por el que se establecen normas comunes sobre las condiciones que deben cumplirse para ejercer como transportista por carretera.

**Tenga en cuenta que**

El profesional que desee realizar una actividad de transporte con vehículos que no excedan un peso máximo autorizado de 3,5 toneladas cumple con este requisito de capacidad profesional siempre y cuando posea un certificado de capacidad en transporte ligero emitido por el prefecto regional.

*Para ir más allá*: Artículos R. 3211-36 a R. 3211-42 del Código de Transporte.

**Establecimiento**

El requisito de establecimiento se cumple siempre que:

- La empresa con sede en Francia posee los principales documentos de la empresa (documentos contables, gestión de personal, datos de tiempo de trabajo y descanso de los conductores, etc.) en sus instalaciones;
- El transportista tiene al menos un vehículo registrado.
- la empresa lleva a cabo las actividades de estos vehículos de forma continua gracias a los equipos y técnicas administrativas adecuados.

**Tenga en cuenta que**

Si la empresa no tiene su sede en Francia, estos documentos deben estar en su establecimiento principal, si es necesario, el profesional debe informar al prefecto regional.

*Para ir más allá*: Artículos R. 3211-19 a R. 3211-23 del Código de Transporte.

**Capacidad financiera**

Cada año, el profesional debe, a través de documentos certificados (por un contador, un auditor o un centro de gestión certificado), certificar su capacidad financiera.

Para ello, debe justificar tener capital y reservas:

- al menos 1.800 euros para el primer vehículo y 900 euros para el siguiente, para vehículos que no superen un peso máximo permitido de 3,5 toneladas;
- 9.000 euros para el primer vehículo y 5.000 euros para cada uno de los siguientes vehículos, para vehículos que superen este límite.

En caso contrario, el profesional podrá presentar garantías otorgadas por una o más entidades financieras que posean la libertad bajo fianza, siempre que esta garantía no supere la mitad de la capacidad financiera adeudada.

*Para ir más allá*: Artículos R. 3211-32 a R. 3211-35 del Código de Transporte.

#### Inscripción en el Registro Nacional de Empresas de Transporte Por Carretera

**Autoridad competente**

El profesional debe solicitar al prefecto de la región donde se encuentra la sede de su empresa. Las empresas que no tienen su sede en Francia deben solicitar al prefecto de la región donde se encuentra su principal establecimiento.

**Documentos de apoyo**

Su solicitud deberá incluir, en su caso, el formulario cerfa No. [16093](https://www.service-public.fr/professionnels-entreprises/vosdroits/R57874) o [16094](https://www.service-public.fr/professionnels-entreprises/vosdroits/R14156) solicitando autorización para el ejercicio de la profesión de operador de transporte por carretera e inscripción en el registro, así como los justificantes mencionados.

**Resultado del procedimiento**

El registro en este registro da lugar a la emisión por parte del prefecto:

- una licencia comunitaria cuando la empresa utilice uno o más vehículos con un peso máximo autorizado superior a 3,5 toneladas;
- licencia de transporte interno cuando no se supera este límite.

Estas licencias con una validez renovable de 10 años se expiden con tantas copias certificadas como vehículos registrados.

*Para ir más allá*: Artículo R. 3211-9 del Código de Transporte; Decreto de 28 de diciembre de 2011 relativo a la autorización para el ejercicio de la profesión de operador de transporte público por carretera y los términos y condiciones de la solicitud de autorización por parte de las empresas..

3°. Condiciones de honorabilidad
-----------------------------------------

Nadie puede llevar a cabo la actividad de un transportista de mercancías por carretera, siempre y cuando haya sido objeto de:

- varias condenas mencionadas en el boletín 2 de su historial delictivo y que llevaron a la prohibición de ejercer una profesión comercial o industrial;
- varias condenas enumeradas en el boletín 2 de su historial delictivo, incluyendo:- incumplimientos de las obligaciones de transporte de materiales peligrosos (véanse los artículos L. 1252-5 a L. 1252-7 del Código de Transporte),
  - incumplimiento de las obligaciones de fijación de precios contractuales, falsificación de documentos o datos electrónicos, o la emisión de información falsa, o la falta de instalación de dispositivos de control (véanse los artículos L. 3242-2 a L. 3242-5, L. 3315-4 a L. 3315-6, L. 3315-6, L. 3315-7, L. 3315-9 y L. 3315-10 del Código de Transporte),
  - delitos contemplados en los artículos 221-6-1, 222-19-1 y siguientes del Código Penal (como injerencia dolosa o involuntaria con la integridad física, el acoso moral, la agresión sexual, etc.) de una persona).
  - las infracciones contempladas en los artículos L. 654-4 a L. 654-15 del Código de Comercio (penas por quiebra),
  - Infracciones de Tráfico (ver Secciones L. 221-2 y lo siguiente de la Ley de Tráfico de Carreteras),
  - obligaciones de prevención y gestión de residuos (véanse los artículos L. 541-46-5- del Código de Medio Ambiente).

*Para ir más allá*: Artículo R. 3211-27 del Código de Transporte.

