﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP078" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transporte" -->
<!-- var(title)="Conductor de carretera profesional de vehículos HGV" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transporte" -->
<!-- var(title-short)="conductor-de-carretera-profesional" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/transporte/conductor-de-carretera-profesional-de-vehiculos-hgv.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="conductor-de-carretera-profesional-de-vehiculos-hgv" -->
<!-- var(translation)="Auto" -->


Conductor de carretera profesional de vehículos HGV
===================================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El conductor profesional de la carretera de los vehículos HGV es un profesional, cuya actividad consiste en conducir vehículos que transportan mercancías de más de 3,5 toneladas o transportar personas con más de nueve plazas (incluyendo asiento del conductor) en un entorno profesional.

*Para ir más allá*: Artículo L. 3314-2 del Código de Transporte.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de conductor de carretera profesional de vehículos pesados, el profesional debe tener una tarjeta de calificación de conductor. Para ello debe ser:

- Haber completado la formación profesional inicial
- poseer uno de los títulos o diplomas de conductor de carretera de Nivel IV y V, inscrito sin el Registro Nacional de Certificaciones Profesionales ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)).

*Para ir más allá*: Artículo R. 3314-28 del Código de Transporte.

#### Entrenamiento

Para obtener una tarjeta de calificación, el profesional debe justificar haber completado la formación inicial obligatoria (FMO) o poseer uno de los siguientes títulos o títulos profesionales:

- para el transporte de mercancías:- una licenciatura en "conductor de carga de carretera" (CTRM),
  - un Certificado de Aptitud Profesional (CAP) conductor de carga por carretera o conductor de carga de carretera (CLM),
  - un certificado de estudios profesionales (BEP) de conducción y servicios en el transporte por carretera,
  - designación profesional (TP) como conductor del transporte de mercancías por carretera en todos los vehículos (CTRMV) o en el transportista (CTRMP) expedido por el Ministro responsable del empleo y la formación profesional;
- para el transporte de pasajeros:- un oficial de recepción y conducción de la PAC, transporte de pasajeros,
  - un TP de conductor de tráfico interurbano (CTRIV) expedido por el Ministro de Empleo y Formación Profesional,
  - un TP Comercial y de Conductor para el Transporte Urbano de Pasajeros por Carretera (ACCTRUV) emitido por el Ministro de Empleo y Formación Profesional.

*Para ir más allá* :[Anexo](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=BF895445AB868383BB76802EBB11EA7A.tplgfr22s_2?idArticle=LEGIARTI000027654685&cidTexte=LEGITEXT000027654687&dateTexte=20180502) del auto de 26 de febrero de 2008 por el que se establecen las listas de los títulos de nivel IV y V admitidos en equivalencia en virtud de la calificación inicial de los conductores de determinados vehículos asignados al transporte de mercancías por carretera o de pasajeros.

**Formación Inicial Mínima Obligatoria (FIMO)**

Esta formación, que dura al menos 280 horas, permite al profesional, una vez finalizado el examen final, obtener el título profesional de conducción por carretera y, como tal, conducir:

- a partir de los 18 años, los vehículos para los que se requieren las licencias C1, C1E, C o CE, cuando esta formación se ha centrado en el transporte de mercancías;
- vehículos que requieran un permiso de conducir en las categorías D1, D1E, D o DE, cuando esta formación se haya llevado a cabo en el transporte de pasajeros.

La cualificación profesional inicial también puede ser obtenida por el candidato que ha completado una formación inicial mínima llamada acelerada. Este entrenamiento, que dura al menos 140 horas durante cuatro semanas consecutivas, permite al profesional conducir:

- vehículos para los que se requiere la licencia C1E, C1E (a partir de 18 años), C o CE (a partir de los 21 años) en el caso de formación en el transporte de mercancías;
- vehículos para los que se requiere la licencia D1 o D1E (a partir de los 21 años), D o DE (a partir de los 23 años) cuando la formación se ha centrado en el transporte de pasajeros. Sin embargo, esta edad se incrementa a 21 años para los vehículos de conducción cuya licencia D o DE se requiere cuando el profesional presta servicios regulares y no de 50 kilómetros de pasajeros.

Al final de esta formación inicial, el profesional recibe un certificado de formación de acuerdo con el modelo establecido en la Lista 2 de la[decretado a partir del 31 de diciembre de 2010](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFCONT000023452189) fijar las condiciones para la expedición de la tarjeta de cualificación del conductor y modificar el decreto de 4 de julio de 2008 por el que se define el modelo de certificados relativos a la formación profesional inicial y continua de los conductores de transporte por carretera mercancías y pasajeros.

*Para ir más allá*: Artículos R. 3314-1 y siguientes del Código de Transporte.

**Entrenamiento específico de "puente"**

El profesional con una calificación inicial como conductor de carga, puede obtener la calificación de conductor de pasajero, siempre y cuando posea una licencia válida de clase D1, D1E, D o DE y después de tener seguido de un curso de capacitación adicional de 35 horas.

Esta equivalencia también se prevé para el profesional que ha obtenido una cualificación inicial como conductor de pasajeros y que desea obtener la calificación de conductor de carga. Si procede, la persona debe tener una licencia C1, C1E, C o CE válida y haber completado un curso de formación de 35 horas.

Además, se considera que los conductores con licencia de conducir de clase han obtenido la calificación inicial:

- D o DE válido y emitido antes del 10 de septiembre de 2008 para el ejercicio de la actividad de transporte de pasajeros;
- C o CE válidas y emitidas antes del 10 de septiembre de 2009, para el ejercicio de la actividad de transporte de mercancías.

*Para ir más allá*: Artículos R. 3314-7 a R. 3314-9 del Código de Transporte.

**Educación Continua Obligatoria (FCO)**

El profesional está obligado a completar un curso de educación continua de 35 horas cada cinco años desde el momento de obtener su calificación inicial. Esta formación debe ser impartida por un organismo acreditado en las condiciones del Artículo R. 338-8 del Código de Educación.

El programa y las modalidades de estos cursos de formación iniciales y en curso se establecen en el[Detenido](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000018008166) 3 de enero de 2008 sobre el programa y cómo aplicar la formación profesional inicial y continua de los conductores de mercancías por carretera y pasajeros.

*Para ir más allá*: Artículos R. 3314-10 a R. 3314-14 del Código de Transporte.

**Tarjeta de calificación del conductor**

La tarjeta de calificación del conductor se expide, después de comprobar la validez de la licencia de conducir, al profesional titular:

- uno de los títulos o títulos de formación que figuran en el anexo del decreto de 26 de febrero de 2008 supra;
- certificado de formación.

Un modelo de este mapa se adjunta a la[Apéndice 1](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFCONT000023452189) 31 de diciembre de 2010. Esta tarjeta se renueva cada cinco años después de cada sesión de educación continua.

*Para ir más allá*: Artículo R. 3314-28 del Código de Transporte.

#### Costos asociados con la calificación

La capacitación calificada vale la pena, y el costo varía dependiendo de los centros de capacitación pertinentes. Es aconsejable acercarse a los centros de formación considerados para obtener más información.

### b. Nacionales de la UE: para la entrega temporal y ocasional (entrega gratuita de servicios (LPS)) o permanente (liquidación libre (LE))

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) se ajuste a un ejercicio temporal y ocasional (LPS) o permanente (LE) en Francia.

Como tal, el nacional está sujeto a las mismas disposiciones que el nacional francés (véase supra "2.2). a. Requisitos nacionales").

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El conductor de carretera profesional de los vehículos HGV está obligado a cumplir con las disposiciones específicas relativas a las horas de trabajo y el tiempo de descanso.

Como tal, el profesional debe:

- descansos cada seis horas como mínimo:- al menos treinta minutos cuando su tiempo total de trabajo es de entre seis y nueve horas,
  - al menos cuarenta y cinco minutos cuando su tiempo total de trabajo es de más de nueve horas;
- Tenga cuidado de no exceder las sesenta horas de trabajo por semana;
- Asegurar que sus horas de trabajo no excedan de diez horas, siempre y cuando parte de su trabajo se lleve a cabo entre la medianoche y cinco horas durante un período de veinticuatro horas;
- cuando opere de forma independiente, consuete los documentos necesarios para contar sus horas de trabajo.

Además, el profesional puede obtener de su empleador toda la información relativa al recuento de su tiempo de trabajo.

*Para ir más allá*: Artículos L. 3312-1 en L. 3312-9 y D. 3312-21 a D. 3312-22 del Código de Transporte.

4°. Sanciones
----------------------------------

El profesional se enfrenta a una pena de un año de prisión y una multa de 30.000 euros si falsifica documentos, proporciona información falsa, se deteriora o modifica los dispositivos para el control o no procede a su instalación.

Si es necesario, el vehículo en el que se cometió la infracción será retirado de la circulación hasta que se regulariza.

Además, el profesional se enfrenta a una pena de seis meses de prisión y 3.750 euros si realiza esta actividad sin tarjeta de conductor o con una tarjeta que no le pertenece.

*Para ir más allá*: Artículo L. 3315-4 del Código de Transporte.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

