﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP261" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transporte" -->
<!-- var(title)="Conductor de coche de transporte sin conductor (VTC)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transporte" -->
<!-- var(title-short)="conductor-de-coche-de-transporte" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/transporte/conductor-de-coche-de-transporte-sin-conductor-vtc.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="conductor-de-coche-de-transporte-sin-conductor-vtc" -->
<!-- var(translation)="Auto" -->



Conductor de coche de transporte sin conductor (VTC)
====================================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El conductor de un coche de transporte con chófer (VTC) es un profesional cuya actividad consiste en el transporte de personas y su equipaje con reservas anticipadas utilizando un vehículo con al menos cuatro y no más de nueve asientos, Conductor.

El conductor puede operar como un operador de automóviles de transporte con chófer (VTC), siempre que esté inscrito en el registro VTC.

#### Regulación de la actividad

A diferencia de los taxis, el conductor vTC no es elegible para la autorización de estacionamiento:

- Debe justificar una pre-reserva para cuidar de un cliente;
- está prohibido practicar merodeadores, es decir, parar, aparcar o conducir por el carril abierto al tráfico público en busca de clientes;
- no puede aparcar en el carril abierto al tráfico público en la parte delantera de las estaciones y terminales más de una hora antes de que el cliente que ha hecho una pre-reserva sea atendido;
- Al finalizar el servicio, está obligado a regresar a la ubicación del operador de ese coche o a un lugar, fuera de la carretera, donde se permite el estacionamiento, a menos que justifique otra reserva anticipada o un contrato con el cliente final.

*Para ir más lejos:* Secciones L. 3120-2 y L. 3122-9 del Código de Transporte.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

El controlador VTC debe cumplir ciertas condiciones de instalación y funcionamiento, entre ellas:

- Tener una licencia de conducir
- Cumplir con una condición de aptitud profesional
- cumplir con una condición de honor profesional.

Además, el conductor debe haber contratado una póliza de seguro de responsabilidad civil y debe solicitar a la autoridad administrativa una tarjeta de conductor profesional VTC.

*Para ir más lejos:* Artículos L. 3120-2, L.3120-4, R. 3120-1 a R.3120-9 del Código de Transporte.

#### Condiciones de licencia de conducir

El conductor del VTC debe tener una licencia de conducir para conducir el vehículo utilizado. El período de prueba de la licencia de conducir debe expirar. Para los nacionales de un Estado miembro de la Unión Europea o de un Estado parte en el Espacio Económico Europeo, el conductor del VTC debe poseer una licencia que le haya sido expedida durante más de tres años.

*Para ir más lejos:* Sección R. 3120-6 y R. 3120-8-1 del Código de Transporte.

#### Condición de aptitud profesional

El estado de la aptitud profesional se puede verificar aprobando un examen de ingreso del conductor de vtc. Este examen incluye pruebas de elegibilidad escritas y una prueba de ingreso práctica, cuyo programa y pruebas se definen por orden conjunta del Ministro de Transporte y el Ministro responsable de Economía.

Las pruebas escritas consisten en un núcleo común entre los candidatos para la profesión de conductor de taxi y VTC, y pruebas específicas para la profesión de conductor VTC. La prueba práctica de admisión consiste en una situación.

Las sesiones de examen están organizadas por las Cámaras de Comercio y Artesanía (CMA). El candidato debe pagar una cuota de inscripción (aproximadamente 202 euros en 2019 para todas las pruebas). El registro debe hacerse en línea[en la plataforma dedicada](https://examentaxivtc.fr). Se recomienda encarecidamente a los solicitantes que paguen sus cuotas de inscripción en línea con tarjeta de crédito.

Los documentos justificativos que deben facilitarse en el momento del registro consisten en:

- Una fotografía de identidad reciente
- Una copia de la prueba de identidad válida (CNI, pasaporte);
- prueba de residencia de menos de 3 meses
- una fotocopia a dos lados de la licencia de conducir B;
- para los extranjeros de un Estado no perteneciente a la UE, el permiso de trabajo mencionado en el artículo L.5221-2 del Código de Trabajo;
- un certificado médico, original y expedido por un oficial médico, tal como se define en el Artículo R. 221-11 de la Ley de Tráfico de Carreteras, menor de dos años;
- pago de las tasas de registro, mediante cheque o directamente en la plataforma de registro.

Un candidato no puede tomar el examen si ha sido sometido a:

- una retirada permanente de su tarjeta de visita del conductor VTC en los diez años anteriores a su solicitud;
- exclusión por fraude en el examen de ingreso en la profesión de conductor vTC durante los últimos cinco años.

*Para ir más lejos:* Artículos L. 3120-2-1 y R. 3120-7 del Código de Transporte; Artículos 24-24-2 y 26 del Código de Artesanía; 6 de abril de 2017 orden sobre los programas y la evaluación de los exámenes de acceso a las profesiones de taxista y conductor del coche de transporte con chófer; Orden de 6 de abril de 2017 por la que se fijan los importes de las tasas de matriculación para los exámenes de taxista y conductor del coche de transporte con chófer; regulación general del examen aprobado por CMA France, accesible en los emplazamientos de las cámaras regionales de artesanía y artesanía.

La condición de aptitud profesional también se puede determinar por equivalencia. En este caso, el conductor debe justificar un mínimo de un año de experiencia laboral, a tiempo completo o a tiempo parcial durante un período de tiempo equivalente, en las funciones de conductor profesional de transporte de personas, durante los diez años antes de la solicitud de una tarjeta de visita.

*Para ir más lejos:* Sección R. 3122-11 del Código de Transporte.

#### Acondicionamiento físico

Para llevar a cabo su actividad, el conductor de VTC debe haber llevado a cabo un control médico de aptitud para conducir con un médico aprobado por el prefecto. Después de esta visita médica, si se declara en forma, el conductor recibe un certificado emitido por el prefecto.

*Para ir más lejos:* Artículo R. 221-10 de la Ley de Tráfico de Carreteras; de 31 de julio de 2012 relativo a la organización del control médico de la aptitud para conducir.

#### Formación continua

El conductor de VTC está obligado a completar un curso de educación continua cada cinco años en un centro de formación aprobado. El propósito de esta formación de 14 horas es actualizar los conocimientos del profesional. Tiene varios módulos obligatorios, y un módulo de elección en una lista predefinida. Al final de su formación, el candidato recibe un certificado de formación de seguimiento.

*Para ir más lejos:* Artículo R. 3120-8-2 del Código de Transporte; orden de 11 de agosto de 2017 relativa a la formación continua de taxistas y conductores de coches de transporte con chófer y a la movilidad de los taxistas.

### b. Nacionales de la UE: para el ejercicio temporal y ocasional (prestación gratuita de servicios)

No existe ninguna disposición específica para el nacional de un Estado miembro de la Unión Europea o de un Estado parte en el Acuerdo sobre el Espacio Económico Europeo para el ejercicio de la actividad de conductores de VTC de forma temporal y ocasional. Como tal, el profesional que desea realizar esta actividad de forma temporal y casual en Francia está sujeto a los mismos requisitos que el nacional francés.

### c. Nacionales de la UE: para un ejercicio permanente (establecimiento libre)

Los nacionales de un Estado miembro de la Unión Europea o de un Estado parte en el Espacio Económico Europeo pueden ejercer la profesión de conductor de VTC en el territorio nacional de manera sostenible. Para ello, primero deben justificar su idoneidad profesional:

- ya sea mediante la producción de un certificado de competencia o un certificado de formación expedido por la autoridad competente de uno de estos Estados, cuando dicha certificación o título esté obligado a realizar los servicios de conducción de VTC (reconocimiento mutuo Cualificaciones);
- ya sea mediante la producción de piezas que establezcan una experiencia laboral de al menos un año a tiempo completo, o a tiempo parcial durante un período de tiempo equivalente, durante los últimos diez años (equivalencia);
- ya sea aprobando el examen de ingreso.

Las disposiciones específicas se aplican a los casos en que existe una diferencia entre la formación recibida por un solicitante en su estado de origen y las aptitudes que deben ser validadas por el examen VTC en virtud del Código de Transporte. En este caso, si las habilidades adquiridas por el solicitante durante su experiencia profesional o a través de la formación que recibió en su Estado de origen no cubren esta diferencia o discrepancia, con respecto a las asignaturas esencial para el ejercicio de la actividad del conductor de VTC, el prefecto del departamento, o el prefecto de la policía, según sea el caso, puede requerir que el solicitante apruebe una prueba de aptitud o que tome un curso de ajuste, con el fin de compensar esta diferencia.

Los nacionales de la Unión Europea o un Estado parte en el Espacio Económico Europeo deben tener un nivel en francés suficiente para ejercer la profesión. Se puede organizar un control del nivel de conocimiento de la lengua si existe una duda seria y concreta sobre el nivel suficiente de conocimiento de la lengua del profesional en relación con las actividades que pretende llevar a cabo.

*Para ir más lejos:* Sección R. 3120-8-1 del Código de Transporte.

3°. Condiciones de honorabilidad
-----------------------------------------

El profesional que desee llevar a cabo la actividad de conductor de VTC debe cumplir con una condición de honorabilidad profesional. Además, no debe haber sido objeto de:

- una reducción de la mitad del número máximo de puntos en una licencia de conducir;
- una condena definitiva por conducir un vehículo sin licencia o por negarse a devolver una licencia de conducir después de que la licencia de conducir haya sido invalidada o cancelada;
- una sentencia definitiva dictada por un tribunal francés o extranjero a una sentencia penal o una sentencia correccional de al menos seis meses de prisión por robo, fraude, violación de la confianza, violación dolosa de la integridad de la agresiones sexuales, tráfico de armas, extorsión o delitos de drogas.

*Para ir más lejos:* Sección R. 3120-8 del Código de Transporte.

(4) Sanciones administrativas y penales
---------------------------------------

Para los operadores de vehículos de transporte de chófer, la falta de registro en el registro VTC se castiga con un año de prisión y una multa de 15.000 euros. El operador natural también puede ser suspendido por hasta 5 años, la pérdida de su vehículo o inmovilizado por hasta un año.

El hecho de que un conductor de VTC se ocupe de un pasajero sin reserva previa o practique merodeadores lo expone a las mismas sanciones.

Además, el conductor incurre en la retirada temporal o permanente de su tarjeta de visita en caso de violación de la normativa aplicable al transporte público particular de personas.

*Para ir más lejos:* Secciones L. 3124-7, L. 3124-11 y L. 3124-12 del Código de Transporte.

5°. Solicitud para la tarjeta de visita del conductor VTC
-------------------------------------------------------------------

La celebración de una tarjeta de conductor VTC profesional es obligatoria para ejercer la profesión.

#### Autoridad competente

El profesional deberá presentar su solicitud al prefecto del departamento del lugar de residencia.

#### Documentos de apoyo

Es aconsejable acercarse a la prefectura en la que el profesional desea ejercer para conocer la lista de piezas que se proporcionarán en el momento de la solicitud.

#### hora

La prefectura emite la tarjeta de visita del conductor de VTC dentro de los tres meses siguientes a la presentación de la solicitud.

#### Bligaciones

Un conductor de VTC que utilice su vehículo profesionalmente debe colocar su tarjeta de visita del conductor VTC en el parabrisas o, en su defecto, en el vehículo de tal manera que la fotografía sea fácilmente visible desde el exterior.

*Para ir más lejos:* Secciones L. 3120-2-2 y R. 3120-6 del Código de Transporte.

