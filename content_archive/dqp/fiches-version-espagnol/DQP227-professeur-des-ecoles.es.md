﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP227" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Profesor de escuela" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="profesor-de-escuela" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/profesor-de-escuela.html" -->
<!-- var(last-update)="2020-04-15 17:21:10" -->
<!-- var(url-name)="profesor-de-escuela" -->
<!-- var(translation)="Auto" -->


Profesor de escuela
===================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:10<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El maestro de escuela es un profesional cuya actividad es proporcionar parte de la educación de los niños de tres a once años y proporcionarles instrucción en jardines de infantes y escuelas primarias.

Como tal, el profesional lleva a cabo una evaluación del trabajo de sus estudiantes y los ayuda con su trabajo personal.

**Tenga en cuenta que**

El maestro de escuela es un funcionario profesional del estado y practica en un oficio de maestros de escuela.

*Para ir más allá* Decreto 90-680, de 1 de agosto de 1990, sobre la condición especial de los profesores de escuela.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ejercer como maestro de escuela, el profesional debe estar profesionalmente calificado. Para ello debe:

- completó con éxito el Concurso de Reclutamiento de Maestros Escolares (CRPE) de la academia;
- han realizado prácticas como profesor en prácticas en un departamento de la academia en cuestión.

#### Entrenamiento

**Concurso de Reclutamiento de Maestros Escolares (CRPE)**

Hay varias formas en las que el profesional puede participar en este concurso, dependiendo de su curso profesional:

- competencia externa si el profesional tiene un máster (B.A. 5), preferentemente especializado en la enseñanza de oficios de educación y formación (MEEF) o cumple con los requisitos para matricularse en el segundo año grado de maestría. Este concurso también puede incluir dos eventos adicionales de idioma regional, si es necesario, será nombrado un concurso externo especial;
- el concurso interno reservado al profesional que justifica una experiencia profesional de tres años. El concurso también puede incluir dos eventos de idioma regional adicionales;
- el segundo concurso interno accesible al profesional con tres años de experiencia profesional en el servicio público y titular de una licenciatura (B.A. 3);
- el tercer concurso accesible a profesionales con experiencia profesional en el sector privado y sin requisito de grado.

El candidato deberá someterse a pruebas de admisibilidad por escrito y a una prueba de admisión oral establecida por el decreto de 19 de abril de 2013 por el que se establecen las modalidades de competencia externa, la competencia externa especial, la segunda competencia interna, la segunda competencia y el tercer concurso para la contratación de maestros de escuela.

**Curso de formación**

El candidato exitoso es nombrado maestro de escuela en prácticas y debe completar una pasantía de un año.

Durante esta pasantía, el profesional está capacitado para adquirir las habilidades necesarias para ejercer su profesión. Es emitido por una institución de educación superior y consiste en períodos de situación profesional en una escuela y períodos de formación dentro de una institución de educación superior.

Al final de la pasantía, el profesor de la escuela en prácticas:

- es nombrado por el Director Académico de los Servicios Nacionales de Educación;
- obtiene el certificado de aptitud para la cátedra escolar;
- y es asignado al departamento en el que fue asignado como pasante.

**Tenga en cuenta que**

Si no hay vacante en este departamento, puede ser asignado a otro departamento de la academia u otra academia.

*Para ir más allá*: Artículos 10 a 13 del Decreto de 1 de agosto de 1990 supra.

**Obligación de educación continua**

El profesional está obligado a formarse regularmente y a hacerlo, a tomar la formación ofrecida por las escuelas superiores de profesoría y educación.

*Para ir más allá*: Artículo L. 921-2 del Código de Educación.

#### Costos asociados con la calificación

El costo de la formación varía en función del curso previsto. Para obtener más información, es aconsejable acercarse a las instituciones interesadas.

3°. Condiciones de honorabilidad, reglas profesionales
---------------------------------------------------------------

**Requisito de edad**

Para enseñar en un jardín de infantes, el profesional debe tener al menos dieciocho años de edad. Tan pronto como desea dirigir una escuela, este requisito se incrementa a veintiún años.

*Para ir más allá*: Artículo L. 921-1 del Código de Educación.

**Discapacidades**

Nadie puede ejercer como maestro de escuela si ha sido condenado:

- por un delito o delito menor contrario a la probidad y la moral;
- privarlo de sus derechos civiles, civiles y familiares o despojado de su patria potestad;
- permanentemente lo priva de su derecho a enseñar.

Además, no puede ejercer al profesional que ha sido removido de sus funciones como profesor.

**Responsabilidad y deber de información**

El profesional es responsable de todas las actividades escolares de sus alumnos. También debe mantener a los padres informados de la evolución de la educación de sus estudiantes.

