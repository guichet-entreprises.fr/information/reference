﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP266" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Seguridad" -->
<!-- var(title)="Armero" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="seguridad" -->
<!-- var(title-short)="armero" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/seguridad/armero.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="armero" -->
<!-- var(translation)="None" -->

# Armero [FR]

Actualización más reciente : <!-- begin-var(last-update) -->2020-12<!-- end-var -->

## 1°. Définition de l’activité

L'activité d'armurier consiste, à titre principal ou accessoire, soit en la fabrication, le commerce, l'échange, la location, la location-vente, le prêt, la modification, la réparation ou la transformation, soit en la négociation ou l'organisation d'opérations en vue de l'achat, de la vente, de la fourniture ou du transfert d'armes, de munitions ou de leurs éléments.

*Pour aller plus loin* : article L. 313-2 du Code de la sécurité intérieure et article L. 2332-1 du Code de la défense.
 
## 2°. Qualifications professionnelles

### a. Exigences nationales 
 
#### Législation nationale

Les personnes souhaitant exercer l'activité d'armurier doivent disposer d'un document établissant leurs compétences professionnelles.

Ce document établissant les compétences professionnelles de l'intéressé consiste en la présentation de l’un des diplômes ou titres suivants :

* soit d'un diplôme délivré par la France ou d'un diplôme ou titre équivalent délivré par un autre État membre de l'Union européenne ou par un autre État partie à l'accord sur l'Espace économique européen, sanctionnant une compétence professionnelle dans les métiers de l'armurerie ou de l'armement ;
* soit du certificat de qualification professionnelle élaboré par la branche professionnelle de l'armurerie et agréé par arrêté du ministre de l'Intérieur ;
* soit, pour le dirigeant de l'entreprise, d'un diplôme de niveau IV délivré par la France, par un autre État membre de l'Union européenne ou par un autre État partie à l'accord sur l'Espace économique européen, ou tout document justifiant une expérience professionnelle d'au moins six ans dans les métiers de l'armurerie. Dans ce dernier cas, chacun des établissements de l'entreprise doit comporter dans son personnel au moins un salarié titulaire de l'un des diplômes, titres ou certificats de qualification précédents ;
* soit, pour le commerce autre que de détail, des armes, des munitions et de leurs éléments des catégories A1° et B, l'un des diplômes, titres ou certificats de qualification précédents ou tout document justifiant une expérience professionnelle d'au moins dix ans dans les métiers de l'armurerie.

Pour les ressortissants d'un État membre de l'Union européenne autre que la France ou d'un autre État partie à l'accord sur l'Espace économique européen, à défaut de produire l’un des diplômes ou titres précédents, les intéressés doivent produire un document établissant leur capacité professionnelle consistant en la copie de l'agrément ou du titre équivalent délivré par l'autorité administrative de cet État et justifiant la capacité à exercer la profession d'armurier. 

#### Formation

Le CAP (certificat d'aptitude professionnelle) est un diplôme professionnel national de niveau V (c’est-à-dire correspondant à une sortie de second cycle général et technologique avant l’année de terminale) délivré par le ministère de l'Education nationale. Il se prépare en 2 ans après la 3e, à temps plein ou en apprentissage. En fonction du profil et des besoins de l'élève, le diplôme peut aussi se préparer en 1, 2 ou 3 ans. Diplôme d'insertion professionnel, le CAP forme aux techniques et aux savoir-faire professionnels des métiers.

Le BMA (brevet des métiers d'art) est un diplôme national français d'études secondaires et d'enseignement professionnel de niveau IV délivré par le ministère de l'Education nationale qui a pour but de promouvoir l'innovation, de conserver et transmettre les techniques traditionnelles. Il est enregistré au Répertoire national des certifications professionnelles.

Le CQP (certificat de qualification professionnelle) est un certificat de qualification élaboré par la branche professionnelle de l'armurerie qui doit respecter un cahier des charges. Il est agréé par arrêté du ministre de l'Intérieur s'il respecte ce cahier des charges.

*Pour aller plus loin* : article R. 313-4 du Code de la sécurité intérieure.
 
#### Coûts associés

La formation est le plus souvent gratuite lorsque le diplôme est préparé au sein d'un établissement public local d'enseignement. Pour plus de précisions, il est conseillé de se reporter au centre de formation considéré.
 
La formation menant à l'obtention du certificat de qualification professionnelle est payante. Pour plus d'informations, il est conseillé de se rapprocher des organismes de formation la dispensant. 

*Pour aller plus loin* : articles R. 313-3 et R. 313-33 du Code de la sécurité intérieure.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (libre prestation de services)

Il n'existe pas de dispositions pour une activité ponctuelle ou occasionnelle dans la mesure où il n'est pas prévu de délivrer d'agréments préfectoraux ou d'autorisations ministérielles pour de courtes durées, sauf circonstances exceptionnelles.
 
### c. Ressortissants UE : en vue d’un exercice permanent (libre établissement)
 
#### Ressortissant UE déjà établi dans un autre État membre

À défaut de produire l’un des diplômes ou titres requis par la réglementation, les intéressés doivent produire un document établissant leur capacité professionnelle consistant en la copie de l'agrément ou du titre équivalent délivré par l'autorité administrative de cet État et justifiant la capacité à exercer la profession d'armurier. 
 
#### Ressortissant UE sans établissement dans un autre État membre : système général ou reconnaissance de l’expérience professionnelle ou reconnaissance automatique ou cas particuliers

Tout ressortissant d'un État de l'UE ou de l'EEE qui est établi et exerce légalement l'activité d'armurier dans cet État peut exercer la même activité en France de manière permanente. 

##### Pour les agréments d'armurier des armes, des munitions et de leurs éléments des catégories C et D

En vue d'obtenir la reconnaissance de leurs qualifications professionnelles, les ressortissants d'un État membre de l'Union européenne autre que la France ou d'un autre État partie à l'Espace Economique Européen adressent au préfet du département de leur domicile ou du lieu où ils envisagent d'exercer leur activité un dossier comprenant les documents attestant leur compétence ou qualification professionnelle.

Le préfet en accuse réception dans un délai d'un mois à compter de sa réception et informe le demandeur, le cas échéant, de tout document manquant. Il notifie sa décision dûment motivée trois mois au plus tard après la réception d'un dossier complet.

##### Pour les autorisations de fabrication, de commerce et d'intermédiation des armes, des munitions et de leurs éléments des catégories A1° et B

En vue d'obtenir la reconnaissance de leurs qualifications professionnelles, les ressortissants d'un État membre de l'Union européenne autre que la France ou d'un autre État partie à l'Espace Economique Européen adressent au ministre de l'Intérieur un dossier comprenant les documents attestant leur compétence ou qualification professionnelle.

Le ministre en accuse réception dans un délai d'un mois à compter de sa réception et informe le demandeur, le cas échéant, de tout document manquant. Il notifie sa décision dûment motivée trois mois au plus tard après la réception d'un dossier complet.

*Pour aller plus loin* : articles R. 313-3-1 et R. 313-33-1 du Code de la sécurité intérieure.

## 3°. Conditions d’honorabilité

### Pour les agréments d'armuriers des armes, des munitions et de leurs éléments des catégories C et D

Le demandeur doit produire un ou des documents établissant son honorabilité et consistant en :

* une déclaration sur l'honneur du demandeur selon laquelle il ne fait l'objet d'aucune interdiction d'exercer une profession commerciale ;
* pour les ressortissants étrangers, un document équivalent au bulletin n° 2 du casier judiciaire.

Tout document rédigé dans une langue étrangère est accompagné de sa traduction en français.

L'agrément d'armurier peut être refusé lorsque le demandeur a été condamné à une peine d'emprisonnement avec ou sans sursis supérieure à trois mois, inscrite à son casier judiciaire ou, pour les ressortissants étrangers, dans un document équivalent au bulletin n° 2 du casier judiciaire. 

L'agrément d'armurier est refusé lorsque le demandeur a fait ou fait l'objet :

* d'une décision d'interdiction d'acquisition et de détention d'armes devenue définitive ;
* d'une interdiction d'exercer une activité commerciale ;
* dans un État autre que la France, de mesures équivalentes à celles qui précdent. 

### Pour les autorisations de fabrication, de commerce et d'intermédiation des armes, des munitions et de leurs éléments des catégories A1° et B

L'autorisation peut être refusée lorsque le demandeur, personne physique, a fait ou l'objet :

* d'une décision d'interdiction d'acquisition et de détention d'armes devenue définitive ;
* d'une interdiction d'exercer une activité commerciale ;
* dans un État autre que la France de mesures équivalentes aux mesures précédentes.

L'autorisation peut être également refusée lorsque le demandeur ou une personne appartenant aux organes de surveillance dans la société ou le groupement d'intérêt économique demandeur ou y exerçant une fonction d'administrateur, de gérance ou de direction a été condamné à une peine d'emprisonnement avec ou sans sursis supérieure à trois mois, figurant sur le bulletin n° 2 de son casier judiciaire ou dans un document équivalent pour les ressortissants d'un État membre de l'Union européenne ou d'un autre État partie à l'accord sur l'Espace économique européen. 

## 4°. Incompatibilités

### Pour les agréments d'armurier des armes, des munitions et de leurs éléments des catégories C et D

L'agrément d'armurier est refusé lorsque le demandeur :

* fait l'objet d'une mesure de protection juridique ;
* a fait ou fait l'objet d'une admission en soins psychiatriques ; 
* a été ou est hospitalisé sans son consentement en raison de troubles mentaux ;
* a un état psychique manifestement incompatible avec la détention d'une arme.

### Pour les autorisations de fabrication, de commerce et d'intermédiation des armes, des munitions et de leurs éléments des catégories A1° et B

L'autorisation est refusée lorsque le demandeur :

* fait l'objet d'une mesure de protection juridique ;
* a fait ou fait l'objet d'une admission en soins psychiatriques ;
* a été ou est hospitalisé sans son consentement en raison de troubles mentaux ;
* a un état psychique manifestement incompatible avec la détention d'une arme.

*Pour aller plus loin* : articles R. 313-6 et R. 313-29 du Code de la sécurité intérieure.

## 5°. Démarches et formalités
 
### a. Pour les agréments d'armuriers des armes, des munitions et de leurs éléments des catégories C et D

La demande est adressée au préfet du lieu d'implantation de l'établissement ou, à défaut, du domicile du demandeur. Il en est délivré un récépissé.

**A noter** : La demande d'agrément est présentée par la personne qui exerce l'activité d'armurier. S'il s'agit d'une personne morale, elle est présentée par son représentant légal et l'agrément est délivré à celui-ci.

Les documents suivants sont joints à la demande d'agrément :

* un document établissant l'état civil de l'intéressé ainsi qu'un extrait d'acte de naissance avec mentions marginales datant de moins de trois mois ;
* un document établissant les compétences professionnelles de l'intéressé ;
* un ou des documents établissant l'honorabilité du demandeur et consistant en :
  * une déclaration sur l'honneur du demandeur selon laquelle il ne fait l'objet d'aucune interdiction d'exercer une profession commerciale,
  * pour les ressortissants étrangers, un document équivalent au bulletin n° 2 du casier judiciaire.

Tout document rédigé dans une langue étrangère est accompagné de sa traduction en français.

*Pour aller plus loin* : articles R. 313-1 et R. 313-3 du Code de la sécurité intérieure.

## b. Pour les autorisations de fabrication, de commerce et d'intermédiation des armes, des munitions et de leurs éléments des catégories A1° et B

Les demandes d'autorisation de fabrication ou de commerce ou d'intermédiation d'armes, de munitions et de leurs éléments de catégories A1° et B sont adressées à  M. le ministre de l’Intérieur – secrétariat général – service central des armes – Place Beauvau – 75800 Paris cedex 08.

### Lorsque le demandeur est une personne physique, celui-ci doit utiliser le [formulaire 15694*02](https://www.service-public.fr/professionnels-entreprises/vosdroits/R47730).

### Lorsque le demandeur est une personne morale, celui-ci doit utiliser le [formulaire 15693*02](https://www.service-public.fr/professionnels-entreprises/vosdroits/R47729).

À la demande d'autorisation sont joints les renseignements suivants :

* pour les entreprises individuelles :
  * justification de la nationalité du demandeur ;
* pour les sociétés de personnes :
  * noms de tous les associés en nom, commandités, commanditaires et gérants,
  * justification de la nationalité de ces personnes ;
* pour les sociétés par actions et les sociétés à responsabilité limitée :
  * noms des gérants, commandités, membres du conseil d'administration, du directoire ou du conseil de surveillance,
  * justification de la nationalité de ces personnes, renseignements concernant la nationalité des actionnaires ou des titulaires des parts sociales et la part du capital détenue par les citoyens français,
  * forme des titres des sociétés par actions ;
* pour les groupements d'intérêt économique :
  * nom du ou des administrateurs,
  * en cas de constitution avec capital, renseignements concernant la nationalité des titulaires des parts de capital et la part du capital détenue par les titulaires français ;
* un extrait d'acte de naissance avec mentions marginales datant de moins de trois mois pour le demandeur et pour chacune des personnes exerçant, dans la société ou le groupement d'intérêt économique demandeur, une fonction de direction ou de gérance ;
* le cas échéant, nature des fabrications exécutées pour les services de l'État et indication sommaire de leur importance ;
* nature de l'activité ou des activités exercées ;
* un document établissant les compétences professionnelles du demandeur ;
* la carte nationale d'identité et, pour les étrangers, le passeport ou le titre de séjour font foi de la nationalité du requérant.

## 6°. Voies de recours

### Nationales

#### Pour les agréments d'armuriers des armes, des munitions et de leurs éléments des catégories C et D

Le demandeur peut contester le refus de délivrance d'agrément d'armurier par le service de la préfecture qui traite le dossier dans les deux mois à compter de la notification de la décision de refus par :

* un recours gracieux, adressé au service de la préfecture qui traite le dossier ;
* un recours hiérarchique, adressé à  M. le ministre de l’Intérieur – secrétariat général – service central des armes – Place Beauvau – 75800 Paris cedex 08 ;
* un recours contentieux, adressé au tribunal administratif adresse du tribunal. Le tribunal administratif peut être saisi par l’application informatique « Télérecours citoyens » accessible par le site internet [telerecours.fr](www.telerecours.fr).

Ce recours juridictionnel doit être déposé au plus tard avant l’expiration du deuxième mois suivant la date de notification de la décision contestée (ou bien du deuxième mois suivant la date du rejet de votre recours gracieux ou hiérarchique).

*Pour aller plus loin* : articles R. 313.1 à R. 313-7-1 du Code de la sécurité intérieure ; articles R. 421-1 à R. 421-7 du Code de justice administrative.

#### Pour les autorisations de fabrication, de commerce et d'intermédiation des armes, des munitions et de leurs éléments des catégories A1° et B

Le demandeur peut contester le refus de délivrance de l'autorisation de fabrication, de commerce et d'intermédiation par le ministre de l’Intérieur dans les deux mois à compter de la notification de la décision de refus par :

* un recours gracieux, adressé à  M. le ministre de l’Intérieur – secrétariat général – service central des armes – Place Beauvau – 75800 Paris cedex 08 ;
* un recours contentieux, adressé au tribunal administratif [adresse du tribunal]. Le tribunal administratif peut être saisi par l’application informatique « Télérecours citoyens » accessible par le site internet [telerecours.fr](www.telerecours.fr).

Ce recours juridictionnel doit être déposé au plus tard avant l’expiration du deuxième mois suivant la date de notification de la décision contestée (ou bien du deuxième mois suivant la date du rejet de votre recours gracieux ou hiérarchique).

*Pour aller plus loin* : articles R. 313.28 à R. 313-38-2 du Code de la sécurité intérieure ; articles R. 421-1 à R. 421-7 du Code de justice administrative.

### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.
 
### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

**Conditions**

L’intéressé ne peut recourir à SOLVIT que s’il établit :

* que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
* qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

**Procédure**

Le ressortissant doit remplir un formulaire de plainte en ligne .

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

**Pièces justificatives**

Pour saisir SOLVIT, le ressortissant doit communiquer :

* ses coordonnées complètes ;
* la description détaillée de son problème ;
* l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

**Délai**

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

**Coûts :** gratuit.

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

* si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
* s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

**Informations supplémentaires**

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).