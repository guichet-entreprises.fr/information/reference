﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP057" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Quiropráctico" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="quiropractico" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/quiropractico.html" -->
<!-- var(last-update)="2020-04-15 17:21:17" -->
<!-- var(url-name)="quiropractico" -->
<!-- var(translation)="Auto" -->


Quiropráctico
=============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El quiropráctico es un profesional de la salud que practica la manipulación y movilización manual, instrumental o asistida, dedicándose a los sistemas nerviosos y musculoesqueléticos, incluida la columna vertebral.

Realiza un análisis identificando los puntos de bloqueo, detecta el movimiento de las estructuras óseas y realiza manipulaciones.

*Para ir más allá*: Artículo 1 del Decreto No 2011-32, de 7 de enero de 2011, sobre los actos y condiciones quiroprácticas.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La práctica del quiropráctico está reservada:

- titulares del diploma quiropráctico expedido por una institución de formación acreditada por el Ministro responsable de la salud, previa asesoría de la Comisión Nacional de Acreditación;
- nacionales de un Estado miembro de la Unión Europea (UE) o parte en el Espacio Económico Europeo (EEE) que posean una autorización para ejercer quiropráctica o utilizar el título de quiropráctico expedido por la autoridad administrativa competente de la Este estado;
- médicos, parteras, masajistas y enfermeras registradas, titulares de una universidad o título interuniversitario sancionando la formación en este campo dentro de una unidad de formación e investigación de práctica médica emitida por una universidad médica y reconocida por el Consejo Nacional del Colegio de Médicos.

*Para ir más allá*: Artículo 75 de la Ley 2002-303, de 4 de marzo de 2002, sobre los derechos de los enfermos y la calidad del sistema de salud; Artículo 4 del Decreto No 2011-32, de 7 de enero de 2011, relativo a los actos y condiciones de práctica de la quiropráctica.

#### Entrenamiento

La entrada a una institución de formación quiropráctica está abierta a los titulares de un bachillerato francés o a un título emitido por otro estado y reconocido como equivalente.

Se requiere una formación teórica de al menos 2.120 horas y una formación clínica práctica de al menos 1.400 horas para realizar los exámenes que conduzcan a la emisión del diploma de quiropráctico.

En Francia, el[Instituto Francoeuropeo de Quiropráctica](https://www.ifec.net/) (IFEC) imparte formación y otorga el diploma quiropráctico.

*Para ir más allá*: Artículos 1 a 7 de la orden del 24 de marzo de 2014 sobre la formación de quiroprácticos y la acreditación de instituciones de formación quiropráctica.

#### Costos asociados con la calificación

El costo de esta capacitación vale la pena. Para obtener más información, es aconsejable acercarse al IFEC.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (prestación gratuita de servicios)

Un nacional de un Estado de la UE o del EEE que actúe legalmente en uno de estos Estados puede utilizar su título profesional en Francia de forma temporal y ocasional.

Tendrá que solicitarlo, antes de su primera actuación, mediante declaración dirigida al director general de la agencia regional de salud de Ile-de-France (véase infra "5o. a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que esté legalmente establecida, el profesional deberá justificar haberla realizado en uno o varios Estados miembros durante al menos dos años en los diez años antes de la actuación.

*Para ir más allá*: Artículos 11 y 12 del Decreto No 2011-32, de 7 de enero de 2011, sobre los actos y condiciones quiroprácticas.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Cualquier nacional de un Estado de la UE o del EEE podrá establecerse en Francia y llevar a cabo la actividad quiropráctico de forma permanente si tiene:

- un certificado de formación expedido por un Estado de la UE o del EEE que regula el acceso a la actividad o a su ejercicio;
- un certificado de formación expedido por un tercer Estado pero reconocido por la autoridad competente de un Estado de la UE o del EEE;
- cualquier prueba que justifique que el nacional ha estado activo durante dos años en los últimos diez años en un Estado de la UE o del EEE que no regula el acceso o el ejercicio de esta actividad en su territorio.

Una vez que el nacional cumpla una de estas condiciones, puede solicitar una autorización individual para ejercer al Director General de la agencia regional de salud de Ile-de-France (véase infra "5o. b. Solicitar un permiso de ejercicio individual para el nacional de la UE o del EEE para un ejercicio permanente (LE)).

Si el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia profesional muestra diferencias sustanciales con las cualificaciones necesarias para el acceso a la profesión y su ejercicio en Francia, la persona debe someterse a una medida de compensación (véase infra "5o. b. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos 6 y 7 del Decreto No 2011-32, de 7 de enero de 2011, sobre los actos y condiciones quiroprácticas.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Reglas e incompatibilidades éticas

Aunque no está codificado,[normas éticas](http://chiropraxie.com/wp-content/uploads/2013/07/20130406-AFC-RI.pdf) aplicar al quiropráctico, incluyendo:

- Asegurar que se respete la confidencialidad profesional de sus pacientes;
- No utilizar su mandato electivo o función administrativa para aumentar su clientela;
- examinar, aconsejar o tratar a cada paciente de la misma manera, independientemente de su origen, modales o situación familiar;
- actos con el consentimiento libre e informado del paciente.

El quiropráctico no puede realizar ciertos actos, incluyendo:

- manipulación ginecológica-obstétrica;
- toques pélvicos.

*Para ir más allá*: Artículo 3 del Decreto No 2011-32 de 7 de enero de 2011 sobre los actos y condiciones quiroprácticas.

### b. Obligación de someterse a formación profesional continua

Los quiroprácticos deben participar en un programa de varios años de desarrollo profesional continuo. El programa se centra en evaluar las prácticas profesionales, mejorar las habilidades, mejorar la calidad y la seguridad de la atención, mantener y actualizar los conocimientos y habilidades. Todas las acciones llevadas a cabo por los quiroprácticos bajo su obligación de desarrollarse continuamente se trazan en un documento específico que acredite el cumplimiento de esta obligación.

*Para ir más allá*: Artículo 75 de la Ley 2002-303, de 4 de marzo de 2002, sobre los derechos de los enfermos y la calidad del sistema de salud; Artículos L. 4021-1 y artículos subsiguientes R. 4021-4 y los siguientes del Código de Salud Pública.

4°. Seguro
-------------------------------

Como profesional de la salud, un quiropráctico que proactúe de manera liberal debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

**Qué saber**

El hecho de realizar esta actividad sin estar cubierto por un seguro de responsabilidad profesional se castiga con una multa de 45.000 euros.

*Para ir más allá* Los artículos 1 y 2 de la Ley No 2014-201, de 24 de febrero de 2014, abarcan diversas disposiciones de adaptación al Derecho sanitario de la Unión.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración previa para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

#### Autoridad competente

El Director General de la Agencia Regional de Salud de Ile-de-France es responsable de expedir la declaración previa de actividad a un nacional que desee ejercer su profesión en Francia de forma temporal y ocasional.

#### Documentos de apoyo

La solicitud de un informe previo de la actividad va acompañada de un archivo completo que contiene los siguientes documentos justificativos:

- el[Forma](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=75C65DA9206264782345D43D208F57AC.tplgfr39s_3?idArticle=LEGIARTI000023449766&cidTexte=LEGITEXT000023449667&dateTexte=20180208) declaración completa, fechada y firmada;
- Una fotocopia del documento de identidad válido
- un certificado de seguro de responsabilidad civil profesional;
- Una fotocopia de los títulos de formación
- un certificado de la autoridad competente del Estado de la UE o del EEE que certifique que el nacional no está sujeto a ninguna prohibición de ejercer.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

#### Procedimiento

El Director General reconoce la recepción del expediente y tiene un mes para decidir e informar al nacional:

- Puede empezar la actuación.
- que no puede iniciarlo;
- que será sometido a una prueba de aptitud en caso de una diferencia sustancial entre la formación requerida en Francia y sus cualificaciones profesionales;
- que el archivo está siendo revisado y que se necesita más información.

**Tenga en cuenta que**

La declaración es renovable cada año y en caso de un cambio en la situación del nacional.

*Para ir más allá* : Apéndice 3 del decreto de 7 de enero de 2011 relativo a la composición del expediente y a las modalidades de organización de la prueba de aptitud y al curso de adaptación previsto para los quiroprácticos mediante el Decreto No 2011-32, de 7 de enero de 2011, relativo a actos y actos condiciones de ejercicio quiropráctico

### b. Solicitar un permiso de ejercicio individual para el nacional de la UE o del EEE para un ejercicio permanente (LE)

#### Autoridad competente

El Director General de la Agencia Regional de Salud de Ile-de-France es responsable de expedir la autorización individual para ejercer a un nacional que desee establecerse en Francia para llevar a cabo la actividad de quiropráctico.

#### Documentos de apoyo

Para obtener esta autorización, el nacional deberá enviar a la autoridad competente un expediente de dos copias por carta recomendada con acuse de recibo con los siguientes documentos justificativos:

- el[Forma](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=8E0C3323BA88B61EACBF39F205F5CEAF.tplgfr39s_3?idArticle=LEGIARTI000023449673&cidTexte=LEGITEXT000023449667&dateTexte=20180208) solicitud de permiso individual, completada, fechada y firmada;
- Una fotocopia del documento de identidad válido
- Una copia del título de formación para el ejercicio de la profesión;
- Si es necesario, cualquier diploma adicional;
- cualquier documentación que justifique el seguimiento de la educación continua, la experiencia y las habilidades adquiridas en ese estado;
- un certificado de la autoridad competente del Estado que justifique que el nacional no tiene una sanción en su contra;
- Una copia de los certificados que justifican el nivel de formación y, año tras año, los detalles y el volumen por hora de los cursos seguidos, así como el contenido y la duración de las prácticas validadas;
- En su caso, cualquier documento que justifique el ejercicio de la profesión en un Estado de la UE o del EEE durante dos años en los últimos diez años, cuando dicho Estado no regule el acceso o el ejercicio de la profesión de quiropráctico;
- si es necesario, el reconocimiento de un certificado de formación expedido por un Estado de la UE o del EEE cuando el título haya sido adquirido en un tercer Estado.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

#### Procedimiento

El Director General confirmará la recepción del expediente en el plazo de un mes. Si se consideran necesarias las medidas de compensación, el director dará al nacional dos meses para elegir entre una prueba de aptitud o un curso de ajuste.

El silencio guardado por la autoridad competente en un plazo de cuatro meses merece la pena rechazar la solicitud de licencia.

**Bueno saber: medidas de compensación**

La prueba de aptitud toma la forma de un examen escrito u oral puntuado de 20. Su validación se pronuncia cuando el nacional ha obtenido una puntuación media de 10 sobre 20 o más, sin una puntuación inferior a 8 de 20. Si tiene éxito en la prueba, se le permitirá al nacional utilizar el título de quiropráctico.

La pasantía se realiza en un centro de salud público o privado, o a nivel profesional, y no debe durar más de tres años. También incluye formación teórica que será validada por el gestor de prácticas. La decisión de permitir al nacional utilizar el título de quiropráctico se someterá al dictamen de una comisión compuesta por el Director General de la Agencia Regional de Salud de Ile-de-France y cuatro quiroprácticos conocidos por sus habilidades y Experiencia.

*Para ir más allá*: Artículos 6 a 10 del Decreto No 2011-32, de 7 de enero de 2011, sobre los actos y condiciones quiroprácticas; decreto de 7 de enero de 2011 relativo a la composición del expediente y a las modalidades de organización de la prueba de aptitud y al curso de adaptación previsto para los quiroprácticos mediante el Decreto No 2011-32, de 7 de enero de 2011, relativo a los actos y condiciones de práctica Quiropráctica.

### c. Registro en el directorio Adeli

Un nacional que desee ejercer como quiropráctico en Francia está obligado a registrar su autorización para ejercer en el directorio Adeli ("Automatización de las Listas").

#### Autoridad competente

El registro en el directorio de Adeli se realiza con la Agencia Regional de Salud (ARS) del lugar de práctica.

#### hora

La solicitud de inscripción se presenta en el plazo de un mes a partir de la toma del cargo del nacional, independientemente del modo de práctica (liberal, asalariado o mixto).

#### Documentos de apoyo

En apoyo de la solicitud de registro, el quiropráctico debe proporcionar un archivo que contenga:

- el título original que acredite la formación del epítisto expedido por el Estado de la UE o el EEE (traducido al francés por un traductor certificado, si procede);
- Id
- Formulario Cerfa 13777Completado, fechado y firmado.

#### Resultado del procedimiento

El número Adeli del nacional se mencionará directamente en la recepción del expediente, emitido por el ARS.

#### Costo

Gratis.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

