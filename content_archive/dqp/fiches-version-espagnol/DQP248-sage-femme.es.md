﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP248" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Partera" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="partera" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/partera.html" -->
<!-- var(last-update)="2020-04-15 17:22:10" -->
<!-- var(url-name)="partera" -->
<!-- var(translation)="Auto" -->


Partera
=======

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:10<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La partería se trata de ayudar a las mujeres a dar a luz. Este profesional de la salud acompaña a sus pacientes durante todo su embarazo, realiza ecografías, hace diagnósticos y prescribe pruebas médicas y exámenes.

Se les puede exigir que prescriban vacunas a madres y recién nacidos, que participen con el médico en actividades de asistencia médica reproductiva o que lleven a cabo consultas anticonceptivas y seguimiento ginecológico preventivo.

*Para ir más allá*: Artículos L. 4151-1 a L. 4151-4, y R. 4127-318 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

En virtud del artículo L. 4111-1 del Código de Salud Pública, para ejercer legalmente como partera en Francia, los interesados deben cumplir acumulativamente las tres condiciones siguientes:

- poseer el diploma estatal francés de partería o un diploma, certificado u otro título mencionado en el artículo L. 4151-5 del Código de Salud Pública (véase infra "Bien saber: reconocimiento automático del diploma");
- nacionalidad francesa, ciudadanía andorrana o nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE) o Marruecos, con sujeción a la aplicación de las normas derivadas del código sanitario compromisos públicos o internacionales;
- con excepciones, figurar en la Junta del Colegio de Parteras (véase infra "5.0). b. Solicitar inclusión en la lista del Colegio de Parteras").

*Para ir más allá*: Artículos L. 4111-1 y L. 4151-5 del Código de Salud Pública.

**Bueno saber: reconocimiento automático del diploma**

Con arreglo al artículo L. 4151-5 del Código de Salud Pública, los nacionales de la UE o del EEE podrán ejercer como partera si poseen uno de los siguientes títulos:

- un certificado de formación de partería expedido por uno de estos Estados de conformidad con las obligaciones de la UE y enumerado en el decreto de 13 de febrero de 2007;
- un certificado de formación de partería expedido por un Estado de la UE o del EEE, de conformidad con las obligaciones de la UE, no en la lista del decreto de 13 de febrero de 2007, si va acompañado de un certificado de ese Estado que certifique que está sancionando un formación de conformidad con estas obligaciones y un certificado que indique el tipo de formación realizada, complementado en su caso por una práctica profesional, y que se asimila, por él, a los diplomas, certificados y títulos enumerados en esta lista;
- un certificado de formación de partería expedido por uno de estos Estados de conformidad con las obligaciones comunitarias, no en la lista del decreto de 13 de febrero de 2007 y no acompañado del certificado de práctica profesional mencionado en el decreto de 13 Febrero de 2007, si un Estado de la UE o del EEE certifica que la persona se ha dedicado efectivamente y legalmente a las actividades de partería durante al menos dos años consecutivos en los cinco años anteriores a la expedición de este certificado;
- un certificado de formación de partería expedido por un Estado de la UE o del EEE sancionando la formación de partería iniciada en ese Estado antes de las fechas del decreto de 13 de febrero de 2007 y no de conformidad con las obligaciones de la UE, si es certificado de uno de estos estados que certifica que el titular del certificado de capacitación se ha dedicado efectivamente y legalmente a las actividades de partería en ese estado durante al menos tres años consecutivos en los cinco años anteriores la emisión de este certificado;
- un certificado de formación de partería expedido por la antigua Checoslovaquia, la antigua Unión Soviética o la ex Yugoslavia o que sancione la formación iniciada antes de la fecha de independencia de la República Checa, Eslovaquia, Estonia, Letonia, Lituania o Eslovenia, si van acompañados de un certificado de las autoridades competentes de la República Checa o Eslovaquia para los documentos de formación expedidos por la antigua Checoslovaquia, Estonia, Letonia o la República Checa Lituania para los documentos de formación expedidos por la antigua Unión Soviética, Eslovenia para los documentos de formación expedidos por la ex Yugoslavia, que certifican que tienen la misma validez jurídica que los documentos de formación emitidos por Estado. Este certificado va acompañado de un certificado expedido por las mismas autoridades que indica que el titular ha ejercido la profesión de partería en ese estado, de manera efectiva y lícita, durante el al menos tres años consecutivos en los cinco años anteriores a la expedición del certificado;
- una designación de formación en parte de las empresas sancionando la formación iniciada en Rumanía antes de las fechas establecidas en el orden de 13 de febrero de 2007 y no de conformidad con las obligaciones comunitarias, si dicho Estado certifica que la persona ha ejercido en ese Estado, de manera efectiva y legal, la profesión de partería durante los períodos fijados por el ministro a cargo de la salud.
- un certificado de formación de partería expedido en Polonia a profesionales que completaron su formación antes del 1 de mayo de 2004 y no de conformidad con las obligaciones comunitarias si dicho Estado certifica que la persona ha ejercido en ese Estado, de manera efectiva y legal, el partería para los períodos fijados por el decreto del Ministro de Salud o si el documento de capacitación incluye un programa especial de revalorización que permite equipararlo con un título en la lista del decreto de 13 de febrero de 2007;
- certificados de formación de partería expedidos por un Estado de la UE o del EEE sancionando la formación que comenzó antes del 18 de enero de 2016.

*Para ir más allá*: Artículo L. 4151-5; decreto de 13 de febrero de 2007 por el que se establece la lista de diplomas, certificados y otros títulos de partería expedidos por los Estados miembros de la Unión Europea, los Estados Partes en el Acuerdo sobre el Espacio Económico Europeo y la Confederación Suiza, a que se refiere el artículo L. 4151-5 (2 grados) del código de salud pública.

#### Entrenamiento

Los estudios de partería consisten en dos ciclos con una duración total de cinco
Años.

El entrenamiento está puntuado:

- un concurso al final del primer año en la universidad. Este año de estudio, llamado el "primer año de estudios comunes de salud" (PACES) es común a los estudiantes de medicina, farmacia, odontología, fisioterapia y parteras. Al final de esta primera competición, los estudiantes se clasifican de acuerdo con sus resultados. Aquellos en un rango útil bajo el numerus clausus se les permite continuar sus estudios y elegir, si es necesario, continuar la formación que conduce a la partería;
- la graduación de una formación general en ciencias maiéuticas al final del tercer año;
- al final del quinto año de capacitación en una de las 35 escuelas de partería.

**Es bueno saber**

Los estudiantes de partería deben llevar a cabo vacunas obligatorias. Para obtener más información, consulte la Sección R. 3112-1 del Código de Salud Pública.

**Diploma de Educación General en Ciencias Maieuse (DFGSMa)**

El primer ciclo es sancionado por la DFGSMa. Consta de seis semestres y corresponde al nivel de licencia. Los dos primeros semestres tienen lugar en la Universidad de Medicina y corresponden a los PACES.

El objetivo de la formación es:

- adquirir los conocimientos científicos esenciales para dominar los conocimientos y conocimientos técnicos necesarios para ejercer la profesión de partería complementando y profundizando los adquiridos durante el ciclo anterior;
- adquirir conocimientos y habilidades prácticas durante la formación clínica y las prácticas;
- El enfoque científico
- Aprender a trabajar en equipo multiprofesional y adquirir técnicas de comunicación esenciales para la práctica profesional;
- desarrollo profesional continuo, incluida la evaluación de las prácticas profesionales y la profundización continua del conocimiento.

Incluye un núcleo común y un curso personalizado durante el cual el estudiante puede profundizar o complementar sus conocimientos:

- en el campo de la maieutics y la salud perinatal;
- para una orientación de investigación, como parte de un curso de investigación, acompañado de una pasantía mínima de cuatro semanas;
- en un área en particular que no sea maieutics.

**Diploma estatal de partería**

El segundo ciclo de estudios de partería está sancionado por el diploma estatal e incluye cuatro semestres correspondientes al nivel de maestría.

Este ciclo incluye enseñanzas teóricas, metodológicas, aplicadas, prácticas y clínicas, así como la realización de prácticas.

Durante sus prácticas, los estudiantes deben escribir una disertación que resultará en una defensa pública ante un jurado.

Por último, se organiza un certificado de síntesis clínica y terapéutica durante el último semestre de formación y debe utilizarse para verificar las habilidades adquiridas durante el segundo ciclo.

El diploma estatal se expide al estudiante tan pronto como él o ella tiene:

- validado el certificado de síntesis clínica y terapéutica;
- apoyó sus memorias con éxito.

*Para ir más allá* : decreto de 11 de marzo de 2013 relativo al esquema de educación para el diploma estatal de partería.

#### Costos asociados con la calificación

La formación que conduce al diploma de partería se paga y el costo varía dependiendo de la institución elegida. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

El profesional que sea miembro de un Estado de la UE o del EEE establecido y que practique legalmente en uno de estos Estados podrá, de forma temporal y ocasional en Francia, realizar la misma actividad de forma temporal y ocasional, sin Parteras.

Para ello, el profesional debe hacer una declaración previa, así como una declaración que justifique que tiene las habilidades de lenguaje necesarias para ejercer en Francia (ver infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

**Qué saber**

No es necesario registrarse en el Colegio de Parteras para el profesional de servicio gratuito (LPS). Por lo tanto, no está obligado a pagar cuotas ordinales. La partera está simplemente inscrita en una lista específica mantenida por el Consejo Nacional del Colegio de Parteras.

La predeclaración debe ir acompañada de una declaración sobre las habilidades del idioma necesarias para llevar a cabo el servicio. En este caso, el control del dominio del idioma debe ser proporcional a la actividad que debe llevarse a cabo y llevarse a cabo una vez reconocida la cualificación profesional.

Cuando los títulos de formación no reciben reconocimiento automático (véase supra "2.0). a. Legislación nacional"), las cualificaciones profesionales del proveedor se comprueban antes de que se preste el primer servicio. En caso de diferencias sustanciales entre las cualificaciones del interesado y la formación requerida en Francia que pueda perjudicar a la salud pública, el solicitante se somete a una prueba de aptitud.

La partera en situación LPS está obligada a respetar las normas profesionales aplicables en Francia, incluidas todas las normas éticas (véase infra "3. Condiciones de honorabilidad, reglas éticas, ética"). Está sujeto a la jurisdicción disciplinaria del Colegio de Parteras.

**Tenga en cuenta que**

La actuación se realiza bajo el título profesional francés de partera. Sin embargo, cuando no se reconocen las cualificaciones de formación y no se han verificado las cualificaciones, el rendimiento se lleva a cabo bajo el título profesional del Estado de establecimiento, con el fin de evitar confusiones Con el título profesional francés.

*Para ir más allá*: Artículo L. 4112-7 del Código de Salud Pública.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

**El sistema automático de reconocimiento de diplomas**

El artículo L. 4151-5 del Código de Salud Pública establece un régimen de reconocimiento automático en Francia de determinados títulos o títulos, en su caso, acompañados de certificados, obtenidos en un Estado de la UE o del EEE (véase supra "2'a). Legislación Nacional").

Corresponde al Consejo Nacional del Colegio de Parteras verificar la regularidad de diplomas, títulos, certificados y certificados, conceder el reconocimiento automático y luego pronunciarse sobre la solicitud de inclusión en la lista de la Orden.

*Para ir más allá*: Artículo L. 4151-5 del Código de Salud Pública.

**El régimen de autorización individual para ejercer**

Si el nacional de la UE o del EEE no reúne los requisitos para el reconocimiento automático de sus credenciales, está comprendido en un régimen de autorización (véase más adelante "5o). b. Si es necesario, solicite una autorización de ejercicio individual).

Las personas que no reciben reconocimiento automático pero que poseen una designación de formación para ejercer legalmente como partera pueden estar autorizadas individualmente a ejercer en Francia, por el Ministro de salud, después del asesoramiento de una comisión compuesta por profesionales en particular.

Si el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia profesional muestra diferencias sustanciales con las cualificaciones necesarias para el acceso a la profesión y su ejercicio en Francia, la persona debe someterse a una medida de compensación.

En función del nivel de cualificación exigido en Francia y del que posea el interesado, la autoridad competente podrá:

- Ofrecer al solicitante la opción de elegir entre un curso de ajuste o una prueba de aptitud;
- imponer un curso de ajuste o una prueba de aptitud
- imponer un curso de ajuste y un alcierto.

*Para ir más allá*: Artículos L. 4151-5-1, D. 4111-8, R. 4111-17 a R. 4111-20, y R. 4151-19 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Cumplimiento del Código de ética de la parte de partería

Las disposiciones del Código de ética se imponen a todas las parteras que practican en Francia, ya sea en el consejo de la Orden o exentas de esta obligación (véase más arriba: "5. b. Solicitar inclusión en la lista del Colegio de Parteras").

**Qué saber**

Todas las disposiciones del Código de ética están codificadas en las secciones R. 4127-301 a R. 4127-367 del Código de Salud Pública.

Como tal, las parteras deben respetar los principios de dignidad, no discriminación, secreto profesional e independencia.

*Para ir más allá*: Artículos R. 4127-301 a R. 4127-367 del Código de Salud Pública.

### b. Actividades acumulativas

La partera sólo podrá realizar cualquier otra actividad si tal combinación es compatible con los principios de independencia profesional y dignidad que se le imponen. La acumulación de actividades no debe permitirle aprovechar sus recetas o su consejo médico.

Una partera que cumple un mandato electivo o administrativo también tiene prohibido usarlo para aumentar su clientela.

*Para ir más allá*: Artículos R. 4127-322 y R. 4127-323 del Código de Salud Pública.

### c. Condiciones de honorabilidad

Para ejercer, la partera debe certificar que ningún procedimiento que pueda dar lugar a una condena o sanción que pueda afectar su inclusión en la junta está en su contra.

*Para ir más allá*: Artículo R. 4112-1 del Código de Salud Pública.

### d. Obligación para el desarrollo profesional continuo

Las parteras deben participar anualmente en un programa de desarrollo profesional en curso. Este programa tiene como objetivo mantener y actualizar sus conocimientos y habilidades, así como mejorar sus prácticas profesionales.

Como tal, el profesional de la salud (salario o liberal) debe justificar su compromiso con el desarrollo profesional. El programa está en forma de formación (presente, mixta o no presental) en análisis, evaluación y mejora de la práctica y gestión de riesgos. Toda la formación se registra en un documento personal que contiene certificados de formación.

*Para ir más allá*: Artículos L. 4021-1 y R. 4382-1 del Código de Salud Pública.

### e. Aptitud física

Las parteras no deben presentar seablación de enfermedades o patologías incompatibles con el ejercicio de la profesión (ver infra "5 grados. a. Solicitar inclusión en la lista de la Facultad de Parteras").

*Para ir más allá*: Artículo R. 4112-2 del Código de Salud Pública.

4°. Seguro
-------------------------------

### a. Obligación de constete de un seguro de responsabilidad civil profesional

Como profesional de la salud, una partera liberal debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si se ejercita como empleada, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

### b. Obligación de unirse al Fondo Independiente de Jubilación de Cirujanos Dentales y Parteras (CARCDSF)

Cualquier partera que esté en el Colegio de Parteras y practique en la forma liberal (incluso a tiempo parcial e incluso si también está empleada) tiene la obligación de unirse a la CARCDSF.

El individuo debe reportarse a carCDSF en el plazo de un mes a partir del inicio de su actividad liberal.

*Para ir más allá*: Artículo R. 643-1 del Código de la Seguridad Social; el sitio de la[CARCDSF](http://www.carcdsf.fr/).

### c. Obligación de Informes de Seguros Médicos

Una vez en la lista de la Orden, la partera que practica en forma liberal debe declarar su actividad con el Fondo de Seguro de Salud Primaria (CPAM).

**Términos**

El registro en el CPAM se puede hacer en línea en el sitio web oficial de Medicare.

**Documentos de apoyo**

El solicitante de registro debe proporcionar un archivo completo que incluya:

- Copiar una identificación válida
- un declaración de identidad bancaria profesional (RIB)
- si es necesario, el título (s) para permitir el acceso al Sector 2.

Para obtener más información, consulte la sección sobre la instalación liberal de parteras en el sitio web de Medicare.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

Cualquier nacional de la UE o del EEE que esté establecido y practique legalmente actividades de partería en uno de estos Estados podrá ejercer en Francia de forma temporal u ocasional si hace la declaración previa (véase supra "2". b. Nacionales de la UE y del EEE: para un ejercicio temporal e informal (Entrega gratuita de servicios)").

La declaración anticipada debe renovarse cada año.

**Tenga en cuenta que**

Cualquier cambio en la situación del solicitante debe ser notificado en las mismas condiciones.

**Autoridad competente**

La declaración debe dirigirse, antes de la primera entrega de servicios, al Consejo Nacional del Colegio de Parteras.

**Condiciones de presentación de informes y recepción**

La declaración puede enviarse por correo o directamente en línea en el sitio web oficial del Colegio de Parteras.

Cuando el Consejo Nacional de la Orden recibe la declaración y todos los documentos justificativos necesarios, envía al demandante un recibo especificando su número de registro, así como la disciplina ejercitada.

**Tenga en cuenta que**

El prestador de servicios informa a la agencia nacional de seguros de salud pertinente de su prestación de servicios mediante el envío de una copia del recibo o por cualquier otro medio.

**hora**

En el plazo de un mes a partir de la recepción de la declaración, el Consejo Nacional de la Orden informa al solicitante:

- Si puede o no comenzar a prestar servicios;
- cuando la verificación de las cualificaciones profesionales muestra una diferencia sustancial con la formación requerida en Francia, debe demostrar haber adquirido los conocimientos y habilidades que faltan Aptitud. Si cumple con este cheque, se le informa en el plazo de un mes que puede comenzar la prestación de servicios;
- cuando la revisión del archivo resalta una dificultad que requiere más información, las razones del retraso en la revisión del archivo. Luego tiene un mes para obtener la información adicional solicitada. En este caso, antes de que finalice el segundo mes a partir de la recepción de esta información, el Consejo Nacional informa al demandante, tras revisar su expediente:- si puede o no comenzar la prestación de servicios,
  - cuando la verificación de las cualificaciones profesionales del demandante muestre una diferencia sustancial con la formación requerida en Francia, debe demostrar que ha adquirido los conocimientos y habilidades que faltan, sujeto a una prueba de aptitud.

En este último caso, si cumple con este control, se le informa en el plazo de un mes que puede iniciar la prestación de servicios. De lo contrario, se le informa de que no puede comenzar la prestación de servicios. En ausencia de una respuesta del Consejo Nacional de la Orden dentro de estos plazos, la prestación de servicios puede comenzar.

**Documentos de apoyo**

La predeclaración deberá ir acompañada de una declaración sobre las aptitudes linguísticas necesarias para llevar a cabo el servicio y los siguientes documentos justificativos:

- el[Formulario de Informes de Entrega de Servicios Por adelantado](http://www.ordre-sages-femmes.fr/wp-content/uploads/2015/10/Formulaire-de-d%C3%A9claration-LPS.pdf) ;
- Copia de una identificación válida o un documento que acredite la nacionalidad del solicitante;
- Copia del documento o títulos de formación, acompañados, si es necesario, de una traducción de un traductor certificado;
- un certificado de la autoridad competente del Estado de Conciliación de la UE o del EEE que certifique que la persona está legalmente establecida en ese Estado y que no está prohibido ejercer, acompañado, en su caso, de una traducción francesa establecido por un traductor certificado.

**Tenga en cuenta que**

El control del dominio del idioma debe ser proporcional a la actividad que debe llevarse a cabo y llevarse a cabo una vez reconocida la cualificación profesional.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 4112-7, R. 4112-9 y siguientes del Código de Salud Pública; 20 de enero de 2010 orden sobre la declaración previa de la prestación de servicios para la práctica de médico, cirujano dental y partera.

### b. Formalidades para los nacionales de la UE o del EEE para un ejercicio permanente (LE)

#### Si es necesario, solicite autorización individual para ejercer

Si el nacional no está en virtud del régimen de reconocimiento automático, debe solicitar una licencia para ejercer.

**Autoridad competente**

La solicitud se dirige en dos copias, por carta recomendada con solicitud de notificación de recepción a la unidad responsable de las comisiones de autorización de ejercicio (CAE) del Centro Nacional de Gestión (NMC).

**Documentos de apoyo**

El archivo de solicitud debe contener todos los siguientes documentos justificativos:

- el[formulario de solicitud de autorización para ejercer la profesión](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E876A52D33A89FE000860F290423BADF.tplgfr36s_1?idArticle=LEGIARTI000029734948&cidTexte=LEGITEXT000029734898&dateTexte=20180117) ;
- Una fotocopia de un documento de identidad válido
- Una copia del título de formación que permita el ejercicio de la profesión en el estado de obtención, así como, en su caso, una copia del título de formación especializada;
- Si es necesario, una copia de los diplomas adicionales;
- cualquier prueba útil que justifique la formación continua, la experiencia y las habilidades adquiridas durante el ejercicio profesional en un Estado de la UE o del EEE, o en un tercer estado (certificados de funciones, informe de actividad, evaluación operativa, etc. ) ;
- en el contexto de funciones desempeñadas en un Estado distinto de Francia, una declaración de la autoridad competente de dicho Estado, de menos de un año de edad, que acredite la ausencia de sanciones contra el solicitante.

Dependiendo de la situación del solicitante, se requiere documentación adicional de apoyo. Para obtener más información, consulte el sitio web oficial de la[Gnc](http://www.cng.sante.fr/).

**Qué saber**

Los documentos de apoyo deben estar escritos en francés o traducidos por un traductor certificado.

**hora**

El NMC confirma la recepción de la solicitud en el plazo de un mes a partir de la recepción.

El silencio guardado durante un cierto período de tiempo a partir de la recepción del expediente completo merece la decisión de desestimar la solicitud. Este retraso se incrementa a:

- cuatro meses para las solicitudes de nacionales de la UE o del EEE con un título de uno de estos Estados;
- seis meses para las solicitudes de terceros nacionales con un diploma de un Estado de la UE o del EEE;
- un año para otras aplicaciones.

Este plazo podrá prorrogarse por dos meses, mediante decisión de la autoridad ministerial notificada a más tardar un mes antes de la expiración de esta última, en caso de dificultad grave para evaluar la experiencia profesional del candidato.

*Para ir más allá* : decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización competentes para el examen de las solicitudes presentadas para el ejercicio en Francia de las profesiones de médico, cirujano dental, partera y Farmacéutico.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la formación y la experiencia laboral del nacional y las necesarias para ejercer en Francia, el NMC podrá decidir:

- Sugerir que el solicitante elija entre un curso de ajuste o una prueba de aptitud;
- para imponer un curso de ajuste y/o una prueba de aptitud.

**La prueba de aptitud** tiene por objeto verificar, mediante pruebas escritas u orales o ejercicios prácticos, la capacidad del solicitante para ejercer como partera en la especialidad de que se trate. Se ocupa de temas que no están cubiertos por las credenciales de formación o formación del solicitante o la experiencia profesional.

**El curso de adaptación** tiene por objeto permitir a las partes interesadas adquirir las competencias necesarias para ejercer la profesión de partería. Se lleva a cabo bajo la responsabilidad de una partera y puede ir acompañado de formación teórica adicional opcional. La duración de la pasantía no exceda de tres años. Se puede hacer a tiempo parcial.

*Para ir más allá*: Artículos R. 4111-17 a R. 4111-20 del Código de Salud Pública.

#### Pide inclusión en la mesa del Colegio de Parteras

El registro en el consejo de la Orden es obligatorio para llevar a cabo legalmente la actividad de partería en Francia.

El registro no se aplica:

- Nacionales de la UE o del EEE que están establecidos y que están legalmente comprometidos en actividades de partería en un Estado de la UE o del EEE, cuando realizan actos de su profesión en Francia de forma temporal y ocasional (véase supra "2". b. Nacionales de la UE y del EEE: para el ejercicio temporal y ocasional);
- Parteras pertenecientes a ejecutivos activos del servicio militar de salud;
- las parteras que, que tienen la condición de servidor público o un funcionario de una comunidad local, no están llamadas, en el ejercicio de sus funciones, a ejercer la medicina.

**Tenga en cuenta que**

La inscripción en el consejo de la Orden permite la emisión automática y gratuita de la tarjeta profesional de la salud. La tarjeta de profesional de la salud es un documento electrónico de identidad de la empresa. Está protegido por un código confidencial y contiene, entre otras cosas, los datos de identificación de la partera (identidad, ocupación, especialidad). Para obtener más información, se recomienda consultar el sitio web del gobierno de la Agencia Francesa de Salud Digital.

**Autoridad competente**

La solicitud de inscripción se dirige al Presidente de la Junta del Colegio de Parteras del departamento en el que el interesado desea establecer su residencia profesional.

La solicitud puede ser presentada directamente al consejo departamental de la Orden en cuestión o enviada a él por correo certificado con solicitud de notificación de recepción.

**Qué saber**

En el caso de un traslado de su residencia profesional fuera del departamento, el profesional está obligado a solicitar su expulsión de la orden del departamento donde estaba practicando y su inscripción por orden de su nueva residencia profesional.

**Procedimiento**

Una vez recibida la solicitud, el consejo del condado nombra a un ponente que lleva a cabo la solicitud y hace un informe por escrito. La junta verifica los títulos del candidato y solicita la divulgación del boletín 2 de los antecedentes penales del solicitante. En particular, verifica que el candidato:

- cumple las condiciones necesarias de moralidad e independencia (véase supra "3.3. c. Condiciones de honorabilidad");
- cumple con los requisitos de competencia necesarios;
- no presenta una discapacidad o condición patológica incompatible con el ejercicio de la profesión (véase supra "3." e. Aptitud física").

En caso de serias dudas sobre la competencia profesional del solicitante o la existencia de una discapacidad o condición patológica incompatible con el ejercicio de la profesión, el consejo de condado remite el asunto al consejo regional o interregional Experiencia. Si, en opinión del informe pericial, existe una insuficiencia profesional que hace peligrosa el ejercicio de la profesión, el consejo departamental deniega el registro y especifica las obligaciones de formación del profesional.

No se puede tomar ninguna decisión de rechazar el registro sin que la persona sea invitada con al menos una quincena de antelación por una carta recomendada solicitando que se presente un aviso de recepción para explicarlo ante la Junta.

La decisión del Consejo Universitario se notifica en el plazo de una semana al individuo, al Consejo Nacional del Colegio de Parteras y al Director General de la Agencia Regional de Salud (ARS). La notificación es por carta recomendada con solicitud de notificación de recepción.

La notificación menciona los recursos contra la decisión. La decisión de rechazar debe estar justificada.

**hora**

El Presidente reconoce haber recibido el expediente completo en el plazo de un mes a partir de su registro.

El consejo departamental del Colegio debe decidir sobre la solicitud de inscripción en un plazo de tres meses a partir de la recepción del expediente completo de solicitud. Si no se responde a una respuesta dentro de este plazo, la solicitud de registro se considera rechazada.

Este período se incrementa a seis meses para los nacionales de terceros países cuando se llevará a cabo una investigación fuera de la Francia metropolitana. A continuación, se notifica al interesado.

También podrá prorrogarse por un período no superior a dos meses por el Consejo Departamental cuando se haya ordenado un dictamen pericial.

**Documentos de apoyo**

El solicitante debe presentar un expediente de solicitud completo que incluya:

- dos copias del cuestionario estandarizado con una identificación con fotografía completa, fechada y firmada, disponible en los Consejos Departamentales del Colegio o directamente descargable desde el sitio web oficial del Consejo Nacional del Colegio de Parteras;
- Una fotocopia de un documento de identidad válido o, en su caso, un certificado de nacionalidad expedido por una autoridad competente;
- En su caso, una fotocopia de la tarjeta de residencia familiar de un ciudadano de la UE válido, la tarjeta válida de residente-EC de larga duración o la tarjeta de residente con estatus de refugiado válido;
- En caso afirmativo, una fotocopia de un certificado de nacionalidad válido;
- una copia, acompañada si es necesario por una traducción, realizada por un traductor certificado, de los títulos de formación a los que se adjuntan:- cuando el solicitante sea nacional de la UE o del EEE, el certificado o certificado sinvisado (véase más arriba "2. a. Requisitos nacionales"),
  - solicitante recibe un permiso de ejercicio individual (véase supra "2. c. Nacionales de la UE y del EEE: para un ejercicio permanente"), copiando esta autorización,
  - Cuando el solicitante presente un diploma expedido en un Estado extranjero cuya validez se reconozca en territorio francés, la copia de los títulos a los que pueda subordinarse dicho reconocimiento;
- nacionales de un Estado extranjero, un extracto de antecedentes penales o un documento equivalente de menos de tres meses de edad, expedido por una autoridad competente del Estado de origen. Esta parte puede sustituirse, para los nacionales de la UE o del EEE que requieran una prueba de moralidad o honorabilidad para el acceso a la actividad de partería, por un certificado, de menos de tres meses de edad, de la autoridad competente del Estado. certificando que se cumplen estas condiciones morales o de honor;
- una declaración sobre el honor del solicitante que certifique que ningún procedimiento que pudiera dar lugar a una condena o sanción que pudiera afectar a la lista en la junta está en su contra;
- un certificado de registro o registro expedido por la autoridad con la que el solicitante fue registrado o registrado previamente o, en su defecto, una declaración de honor del solicitante que certifique que nunca fue registrado o registrado o, en su defecto, un certificado de registro o registro en un Estado de la UE o del EEE;
- todas las pruebas de que el solicitante tiene las habilidades de idiomas necesarias para ejercer la profesión;
- un currículum.

**Remedios**

El solicitante o el Consejo Nacional del Colegio de Parteras podrán impugnar la decisión de registrar o denegar el registro dentro de los 30 días siguientes a la notificación de la decisión o de la decisión implícita de rechazarla. El recurso se interpone ante el consejo regional territorialmente competente.

El consejo regional debe decidir en el plazo de dos meses a partir de la recepción de la solicitud. En ausencia de una decisión dentro de este plazo, el recurso se considera desestimado.

La decisión del consejo regional también está sujeta a apelación, en un plazo de 30 días, ante el Consejo Nacional del Colegio de Parteras. La propia decisión puede ser apelada ante el Consejo de Estado.

**Costo**

La inscripción en el consejo de la Universidad es gratuita, pero crea la obligación de pagar las cuotas ordinales obligatorias, cuyo importe se establece anualmente y que debe pagarse en el primer trimestre del año calendario en curso. El pago se puede hacer en línea en el sitio web oficial del Consejo Nacional del Colegio de Parteras. Como indicación, en 2018, el importe de esta contribución asciende a 148 euros.

*Para ir más allá*Artículo R. 4112-1 y el siguiente del Código de Salud Pública.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

