﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP260" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Veterinaria" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="veterinaria" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/veterinaria.html" -->
<!-- var(last-update)="2020-04-15 17:20:57" -->
<!-- var(url-name)="veterinaria" -->
<!-- var(translation)="Auto" -->


Veterinaria
===========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:57<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El veterinario es un profesional especializado en medicina animal y cirugía.

Su misión incluye:

- Proteger y cuidar a los animales
- Diagnosticar enfermedades físicas y conductuales, lesiones, dolor y malformaciones de animales;
- Recetas seguras para medicamentos
- administrar medicamentos por medios parenterales;
- garantizar la inocuidad de los alimentos y la salud pública, incluso contribuyendo al control de la higiene en las industrias agroalimentarias;
- Para preservar el medio ambiente
- para desarrollar investigación y formación.

2°. Entrenamiento
-------------------------

Los estudios para entrar en la profesión veterinaria consisten en dos ciclos con una duración total de siete años, que pueden completarse con un tercer ciclo de especialización de hasta tres años dependiendo de la especialidad seguida.

El primer ciclo de estudios de al menos dos años se lleva a cabo después de obtener una licenciatura. El estudiante podrá participar en el concurso de ingreso en una de las cuatro instituciones que obtendrán el diploma estatal de veterinaria al final de este ciclo.

Seis concursos diferentes están abiertos a los estudiantes dependiendo de los cursos elegidos durante la formación inicial:

- El concurso A para estudiantes en la clase preparatoria de biología, química, física y ciencias de la tierra (BCPST);
- el concurso A-TB para estudiantes de la clase preparatoria de tecnología y biología, reservado a los titulares de licenciatura en tecnología y tecnologías de laboratorio (STL) y agronomía y ciencias y tecnologías de la vida (STAV);
- Concurso B para estudiantes matriculados en el tercer año de su licenciatura en un curso científico, en los campos relacionados con las ciencias de la vida;
- El concurso C para estudiantes que posean un DUT de ingeniería biológica/aplicada específica o de ciertas especialidades de BTS o BTSA;
- Concurso D para titulares del diploma estatal de doctor en medicina, doctor en farmacia, doctor en cirugía dental o máster predominantemente biológico;
- el concurso E para estudiantes de las escuelas superiores normales de Cachan y Lyon, admitido como la lista principal en la sesión anterior de la pista A de la competición.

En Francia, el diploma estatal de médico veterinario se emite después de un curso de formación en una de las siguientes cuatro instituciones y la defensa de una tesis universitaria:

- [Escuela Nacional de Veterinaria de Alfort (ENVA)](http://www.vet-alfort.fr/) ;
- [Instituto de Educación Superior e Investigación en Alimentación, Sanidad Animal, Ciencias Agrícolas y Ambientales (VetAgro Sup)](http://www.vetagro-sup.fr/) ;
- [Escuela Nacional de Veterinaria, Alimentación y Alimentación (Oniris)](http://www.oniris-nantes.fr/) ;
- [Escuela Nacional de Veterinaria de Toulouse (INP-ENVT)](http://www.envt.fr/).

La formación de la Escuela Veterinaria Nacional es un curso de cinco años que consiste en:

- cuatro años de núcleo común para la formación básica;
- el año pasado de profundización.

#### Diplomado en Estudios Veterinarios Básicos

La formación básica de cuatro años en la escuela está sancionada por el diploma de Estudios Veterinarios Básicos (DEFV). Consta de ocho semestres durante los cuales el alumno recibe formación teórica y práctica básica.

A partir del tercer año, el estudiante participa en la operación de consultas clínicas. A partir del cuarto año, está inmersión total en una clínica veterinaria.

**Qué saber**

El estudiante debe completar una pasantía internacional con una duración mínima de 4 semanas.

#### Diploma estatal

En el quinto año del estudio, el estudiante puede elegir:

- profundizar sus conocimientos profesionales y práctica sin tener que optar por uno de los dominantes específicos en cada escuela;
- continuar su formación en investigación.

La realización del diploma estatal de médico veterinario está sujeta a la redacción y defensa de una tesis cuya elección de sujeto queda a discreción del estudiante.

A lo largo del quinto año, el estudiante debe dedicarse a la preparación de una tesis doctoral de práctica veterinaria. El director de tesis es profesor en una de las escuelas nacionales. La defensa debe tener lugar al menos ocho días después de la colocación del manuscrito. La cátedra del jurado de tesis es profesora en una universidad médica.

El apoyo a la tesis permite al estudiante obtener el título de médico veterinario.

*Para ir más allá*: Artículos D. 241-1 y siguientes del Código Rural y Pesca Marina.

#### Formación adicional

Una vez que un graduado estatal, el veterinario puede elegir una educación opcional para adquirir una especialización en un campo en particular.

El internado es un curso de formación clínica general de un año en una de las cuatro escuelas veterinarias nacionales que se inscriben a través de competiciones nacionales. El veterinario podrá elegir entre uno de los tres canales:

- mascotas;
- Animales de producción;
- Équidos.

El paciente es una formación clínica específica de tres años con un médico veterinario con un diploma complementario como veterinario especialista en la especialidad cuya admisión está registrada. Se acompaña de la preparación para un concurso organizado a nivel europeo por colegios especializados europeos.

El Certificado de Estudios Veterinarios en Profundidad (CEAV) se obtiene después de un año de formación en un ENV cuya entrada está registrada.

El Diploma de Estudios Veterinarios Especializados (DESV) se obtiene después de tres años de formación después de la admisión en el archivo.

Para conocer más sobre las especialidades estudiadas en el internado, el internado, el CEAV o el DESV, es aconsejable acercarse a las escuelas veterinarias nacionales.

#### Costos asociados con la capacitación

La formación para la práctica como veterinario vale la pena. Para más información, es aconsejable acercarse a uno de los establecimientos de formación.

Tres de nosotros. Cualificaciones profesionales
-----------------------------------------------

### a. Requisitos nacionales

Para ejercer como veterinario en Francia, el interesado deberá:

- poseer el diploma estatal de médico veterinario u otro título de formación listado por el[decreto de 19 de julio de 2019](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038831087&categorieLien=id) (véase a continuación "Bueno saber: reconocimiento automático del diploma");
- Ser nacional francés o nacional de un Estado miembro de la Unión Europea (UE) u otro Estado parte en el Acuerdo del Espacio Económico Europeo (EEE) o Suiza;
- inscribir su diploma en uno de los consejos regionales del Colegio de Veterinarios antes de su inclusión en la junta directiva de la Orden.

*Para ir más allá*: Artículos L. 241-1 y L. 241-2 del Código Rural y Pesca Marina.

**Tenga en cuenta que**

La práctica ilegal de medicina animal o cirugía se castiga con dos años de prisión y una multa de 30.000 euros.

*Para ir más allá*: Artículos L. 243-1 y L. 243-4 del Código Rural y Pesca Marina.

**Bueno saber: reconocimiento automático de diplomas**

Con arreglo a la Directiva 2005/36, al artículo L. 241-2 del Código de Pesca Rural y Marítima y al artículo 1 del Decreto de 19 de julio de 2019, los ANationAL de la UE, otro Estado miembro del EEE o Suiza podrán ejercer como veterinario si poseen uno de los siguientes títulos:

- diplomas, certificados o títulos mencionados en el anexo del decreto y expedidos, para cada Estado, después de la fecha posiblemente establecida por el anexo del decreto de 19 de julio de 2019;
- títulos, certificados o títulos expedidos por un Estado miembro de la UE u otro Estado miembro del EEE que no cumplan todos los requisitos de formación resultantes de la legislación de la UE y que sancionen la formación que se inició antes de la fecha de referencia indicada, para cada Estado, al anexo del decreto si van acompañadas de un certificado expedido por la autoridad competente de ese Estado que certifica que sus titulares se han dedicado de manera efectiva y legal a las actividades de veterinario durante al menos tres años consecutivos en los cinco años anteriores a la expedición de este certificado;
- títulos, certificados o títulos expedidos por la antigua República Democrática Alemana que no cumplan todos los requisitos de formación resultantes de la legislación de la UE si van acompañados de un certificado emitido por el Alemania competente que certifica que sus titulares se han dedicado efectivamente y legalmente a actividades veterinarias durante al menos tres años consecutivos en los cinco años anteriores a la expedición de este certificado;
- certificados, certificados o títulos expedidos por la antigua Checoslovaquia o por la República Checa y Eslovaquia cuando sancionen la formación que comenzó en el territorio de la antigua Checoslovaquia, siempre que la autoridad competente de la República Checa o Eslovaquia atestigua que estos diplomas, certificados o títulos tienen, en su territorio, la misma validez jurídica que los diplomas, certificados o títulos que estos Estados emiten para sancionar la formación y que estos títulos, certificados o títulos vayan acompañados de un certificado expedido por la misma autoridad que certifique que sus titulares se han dedicado de manera efectiva y lícita a actividades veterinarias durante al menos tres años. consecutivos en los cinco años anteriores a la expedición de este certificado;
- diplomas, certificados o títulos expedidos por la antigua Unión Soviética o por Lituania y Letonia cuando sancionen la formación que comenzó en el territorio de la antigua Unión Soviética, siempre que la autoridad competente de Lituania o Letonia atestigua que estos diplomas, certificados o títulos tienen, en su territorio, la misma validez jurídica que los diplomas, certificados o títulos que estos Estados emiten para sancionar la formación iniciada en su territorio y que estos diplomas, certificados o títulos van acompañados de un certificado expedido por la misma autoridad que certifica que sus titulares se han dedicado de manera efectiva y lícita a actividades veterinarias durante al menos tres años consecutivos durante el cinco años antes de la expedición de este certificado;
- diplomas, certificados o títulos expedidos por la antigua Unión Soviética o Estonia al sancionar la formación que comenzó en el territorio de la antigua Unión Soviética, siempre que la autoridad competente de Estonia certifique que diplomas, certificados o títulos tienen la misma validez jurídica en su territorio que los diplomas, certificados o títulos que Estonia emite para sancionar la formación iniciada en su territorio y que estos diplomas, certificados o títulos también van acompañados de un certificado expedido por la misma autoridad que certifica que sus titulares se han dedicado efectivamente y legalmente a actividades veterinarias durante al menos cinco años consecutivos en los siete años que precedió a la emisión de este certificado;
- certificados de formación expedidos por Estonia o cuya formación comenzó en ese Estado antes del 1 de mayo de 2004 si van acompañados de un certificado que indique que los titulares han llevado a cabo de hecho y legalmente las actividades en Estonia causa durante al menos cinco años consecutivos en los siete años anteriores a la fecha de la certificación;
- diplomas, certificados o títulos expedidos por la ex Yugoslavia o Croacia y Eslovenia al sancionar la formación que comenzó en el territorio de la ex Yugoslavia, siempre que la autoridad competente de Croacia o Eslovenia certifica que estos diplomas, certificados o títulos tienen, en su territorio, la misma validez jurídica que los diplomas, certificados o títulos que estos Estados emiten para sancionar la formación iniciada en su territorio y que estos diplomas, los certificados o títulos también van acompañados de un certificado expedido por la misma autoridad que certifica que sus titulares se han dedicado de manera efectiva y lícita a actividades veterinarias durante al menos tres años consecutivos durante el cinco años antes de la expedición de este certificado;
- diplomas, Certificados o títulos expedidos por un Estado miembro de la UE u otro Estado parte en el EEE que no cumplan los nombres de la lista del anexo del decreto de 19 de julio de 2019, siempre que dichos diplomas, certificados o títulos vayan acompañados de un certificado expedido por la autoridad competente del Estado de que se trate que certifique que están a los que figuran en dicha lista y que sancionan de conformidad con lo dispuesto en la Directiva 2005/36, de 7 de septiembre de 2005;
- títulos, certificados o títulos que no hayan sido expedidos por un Estado miembro de la UE u otro Estado parte en el EEE, siempre que hayan sido reconocidos por un Estado miembro de la UE u otro Estado parte en el EEE y el titular haya adquirido experiencia profesional de al menos tres años en ese estado y atestiguado por ella.

*Para ir más allá* :[Artículo L. 241-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071367&idArticle=LEGIARTI000006582837&dateTexte=&categorieLien=cid) del Código de Pesca Rural y Marítima y el decreto de 19 de julio de 2019 por el que se establece la lista de diplomas, certificados o títulos veterinarios mencionados en el artículo L. 241-2 del Código Rural.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal o ocasional (prestación gratuita de servicios)

Individuos nacionales de uno de los Estados miembros de la Unión Europea u otro Estado parte en el Acuerdo sobre el Espacio Económico Europeo, así como las empresas constituidas de conformidad con la legislación de uno de estos Estados y con sede en ese país su oficina legal o institución principal, que lleve a cabo legalmente sus actividades veterinarias en uno de estos Estados, distinto de Francia, podrá nadar actos profesionales en Francia de forma temporal y ocasional. Sin embargo, la ejecución de estos actos está sujeta a una predeclaración renovada anualmente. Si la emergencia no permite que esta declaración se haga antes del acto, deberá hacerse en una fecha posterior en un plazo máximo de quince días.

Los interesados están obligados a respetar las normas de conducta profesional vigentes en Francia y están sujetos a la jurisdicción disciplinaria del Colegio de Veterinarios.

**Qué saber**

Para ejercer la profesión veterinaria de forma temporal o ocasional, el nacional debe tener las aptitudes linguísticas necesarias.

*Para ir más allá*: Artículo L. 241-3 del Código Rural y Pesca Marina.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de la UE o del EEE que desee trabajar permanentemente en Francia está comprendido en dos regímenes distintos. En ambos casos, debe tener las habilidades de idiomas necesarias para llevar a cabo la actividad en Francia.

#### El sistema de reconocimiento automático de diplomas

El artículo L. 241-1 del Código de Pesca Rural y Marítima establece un régimen de reconocimiento automático en Francia de los títulos y títulos obtenidos en un Estado de la UE u otro Estado del EEE o suizo (véase supra "2". a. Legislación Nacional").

Corresponde al consejo regional de la orden de los veterinarios comprobar la regularidad de los diplomas y otros títulos de formación, conceder el reconocimiento automático y luego decidir sobre la solicitud de inclusión en la lista de la Orden.

*Para ir más allá* : Artículo L241-2 del Código de Pesca Rural y Marina, y ordenado a partir del 19 de julio de 2019 por el que se establece la lista de títulos, certificados o títulos veterinarios a que se refiere el artículo L. 241-2 del Código Rural y de la Pesca Marina.

#### El régimen despectivo: la autorización para ejercer

Para ejercer en Francia, un nacional con un título veterinario no mencionado por el decreto de 19 de julio de 2019 y el artículo L. 241-2 del Código Rural y Pesca Marítima debe obtener una autorización individual expedida por el Ministro responsable de la agricultura.

Esta autorización está sujeta a una comprobación de conocimientos exitosa[Escuela Nacional de Veterinaria, Alimentación y Alimentación (Oniris)](http://www.oniris-nantes.fr/etudes/examen-de-controle-des-connaissances/examen-de-controle-des-connaissances-pour-lexercice-du-metier-de-veterinaire-en-france/) y cuyos términos se establecen mediante el decreto de 3 de mayo de 2010 (véase infra "5o. b. Si es necesario, solicitar una autorización individual para ejercer").
La solicitud de licencia para la práctica debe enviarse al Ministerio de Agricultura.

#### Procedimiento

La autorización de ejercicio está sujeta a la verificación de todos los conocimientos, cuyos términos se establecen en el auto de 3 de mayo de 2010.

el[Revisión](https://www.oniris-nantes.fr/etudier-a-oniris/les-formations-veterinaires/examen-de-controle-des-connaissances-ex-concours-pays-tiers/) consiste en una prueba de elegibilidad en forma de cuatro cuestionarios de opción múltiple (MQC) y pruebas orales y prácticas.

Para acceder a las pruebas orales y prácticas, el candidato debe obtener una puntuación media de 10 sobre 20 en todos los presupuestos, sin puntuación inferior a 5 de 20.

#### Documentos de apoyo

La admisión al examen sancionando la verificación de conocimientos está sujeta al envío de un expediente completo dirigido a los Oniris. El archivo debe incluir los siguientes documentos justificativos:

- Una hoja informativa completa, fechada y firmada del candidato;
- Una carta de solicitud de autorización para ejercer para el Ministro de Agricultura;
- Un CV
- Una fotocopia de un documento de identidad válido
- Una copia de un extracto del antecedente penal (boletín 3);
- Una copia certificada del título veterinario y su traducción al francés por un traductor certificado;
- una forma que menciona las disciplinas elegidas.

El archivo completo debe enviarse por correo a Oniris antes del 31 de diciembre del año anterior a la inspección.

#### Resultado del procedimiento

El éxito de las pruebas de control de conocimientos se hace público por orden del Ministro responsable de la agricultura, por la que vale la pena la autorización para ejercer la profesión de veterinaria en Francia con la condición de registrarse en la Orden (véase supra "5". b. Solicitar inscripción en el Colegio de Veterinarios").

*Para ir más allá*: Artículos R. 241-25 y R. 241-26 del Código de Pesca Rural y Marina.

#### Costo

La cuota de inscripción para el examen se fija en 250 euros y debe abonarse al Contable del Oniris antes del 31 de diciembre del año anterior a la inspección.

4°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------------------

El veterinario inscrito en la lista de la orden o declarado en LPS, debe respetar las normas de conducta profesional vigentes en Francia. También está sujeto a la jurisdicción disciplinaria del Colegio de Veterinarios.

### a. Cumplimiento del Código de Conducta Veterinario

Las disposiciones del Código de ética son necesarias para los veterinarios que practican en Francia, ya sea que estén en la junta directiva de la Orden o que estén practicando de forma temporal y ocasional.

Como tal, el veterinario debe respetar los principios de moralidad, probidad y dedicación esenciales para el ejercicio de la actividad. También está sujeto al secreto médico, debe ejercer de forma independiente, y no debe prescribir medicamentos para los seres humanos, incluso con la prescripción de un médico.

*Para ir más allá*: Artículo R. 242-32 y el siguiente del Código Rural y la pesca marina.

### b. Desarrollo profesional continuo

La formación profesional continua permite a los veterinarios actualizar sus conocimientos durante sus carreras. Es una obligación ética, cuyo defecto puede ser sancionado por las cámaras disciplinarias.

Para obtener más información sobre la formación profesional continua, es aconsejable acercarse a la Comisión de Práctica Profesional del Colegio de Veterinarios y Escuelas Veterinarias Nacionales.

### c. Actividad acumulada

El veterinario podrá realizar otra actividad profesional si es compatible con la normativa, por un lado, con los principios de independencia profesional y dignidad que se le imponen.

En caso afirmativo, no debe entrar en conflicto con los intereses de su actividad, aparte de los de un veterinario, con los deberes éticos de la profesión veterinaria.

Del mismo modo, se prohíbe a un veterinario que cumpla un mandato electivo o administrativo utilizarlo para aumentar su clientela.

*Para ir más allá*: Artículos R. 242-33 del Código Rural y Pesca Marina.

5°. Legislación social y seguro
-----------------------------------------

### a. Obligación de constete de un seguro de responsabilidad civil profesional

Como profesional de la salud, el médico veterinario debe tomar un seguro de responsabilidad civil profesional.

*Para ir más allá*: Artículo R. 242-48 VI del Código Rural y Pesca Marina.

### b. Inscripción en el fondo autosostenible de pensiones y pensiones de veterinarios

Los veterinarios que ejerzan sobre una base liberal, ya sea bajo la condición de colaborador liberal o gerente de una empresa de ejercicios, deben unirse al fondo veterinario independiente de pensiones y pensiones (CARPV) y completar el[formulario de suscripción](http://www.carpv.fr/wp-content/uploads/2017/03/formulaire-Retraite-RP-2017.pdf).

Seis grados. Procedimientos y formalidades
------------------------------------------

### a. Solicitar el registro en el Colegio de Veterinarios

La inscripción para el Colegio de Veterinarios es obligatoria para hacer legal el ejercicio de la profesión.

#### Autoridad competente

La solicitud de inscripción está dirigida al Presidente del Consejo Regional del Colegio de Veterinarios de la región donde la persona desea establecer su residencia profesional.

#### Procedimiento

Puede presentarse directamente ante el consejo regional de la Orden de que se trate o enviarse a él por correo certificado con notificación de recepción.

#### Documentos de apoyo

el[solicitud de registro](https://www.veterinaire.fr/la-profession/le-metier-veterinaire/linscription-a-lordre.html) en la Mesa del Colegio de Veterinarios se acompaña de un archivo de documentos justificativos que incluye:

# Presentar el original o producir o enviar una fotocopia legible de un pasaporte válido o un documento nacional de identidad;
# una copia del título estatal de médico veterinario o diploma, certificado o título veterinario mencionado en el artículo L. 241-2, así como, para los veterinarios mencionados en el artículo L. 241-2-1, el decreto ministerial que les autoriza a ejercer en Francia;
# un extracto de antecedentes penales que datan de menos de tres meses, sustituidos o completados, para veterinarios de la Unión Europea u otro Estado Parte en el Acuerdo sobre el Espacio Económico Europeo, por un certificado expedido por menos de tres mes por la autoridad competente del Estado miembro de origen u origen, certificando que se cumplen los requisitos morales y de honor exigidos en dicho Estado para el acceso a las actividades veterinarias;
# una declaración manuscrita escrita en lengua francesa en la que, bajo juramento, declara que tiene conocimiento del Código de ética veterinaria y se compromete a ejercer su profesión con conciencia, honor y probidad;
# Si el veterinario tiene la intención de ejercer su profesión en el reparto de actividades, una copia del contrato escrito relativo a este reparto de actividades;
# En caso afirmativo, una copia del contrato entre el veterinario y su empleador;
# Una prueba de residencia administrativa profesional;
# para el ejercicio como veterinario responsable, o como veterinario interino a cargo de una empresa a que se refiere el artículo L. 5142-1 del Código de Salud Pública, como veterinario delegado o como veterinario interino de un estas mismas empresas, la copia del contrato que vincula al veterinario con la empresa o establecimiento, a su vez acompañada de:

(a) la justificación para que la persona cumpla con las condiciones de práctica previstas, según proceda, para las secciones R. 5142-16 en R. 5142-18 o en la sección R. 5145-14 del Código de Salud Pública,
b. una copia del acto del órgano social competente de la sociedad nombrando al interesado y fijando su cometido;

# para el ejercicio como veterinario asistente en una empresa o establecimiento a que se refiere el artículo L. 5142-1 del Código de Salud Pública, cualquier prueba que indique la naturaleza, condiciones y procedimientos de esta actividad;
# para el ejercicio como veterinario vinculado por la convención en virtud de las secciones R. 5142-54 y R. 5142-60 del Código de Salud Pública a una empresa de la que depende una instalación de fabricación, importación o distribución de alimentos medicinales, la copia de la acuerdo entre el veterinario y la empresa.

Todos los documentos presentados en apoyo de la solicitud de registro van acompañados, si no están escritos en francés, de una traducción certificada por un traductor jurado o autorizada a intervenir ante las autoridades judiciales o administrativas de un otro Estado miembro de la Unión Europea, de un Estado Parte en el acuerdo del Espacio Económico Europeo o de la Confederación Suiza.

Los veterinarios que soliciten el registro pueden estar obligados a visitar a un miembro del consejo regional de la Orden especialmente designado por el Presidente o el Secretario General.

El veterinario también puede estar obligado a proporcionar cualquier prueba de que tiene las habilidades de idioma necesarias para ejercer la profesión veterinaria.

#### hora

El silencio mantenido por el Consejo Regional del Colegio de Veterinarios sobre una solicitud de inclusión en el consejo de la Orden, hecha por una persona física o una empresa, mencionada respectivamente en los artículos R. 242-85 y R. 242-86, se obtiene la decisión de desestimar.

#### Resultado del procedimiento

Una vez que la Orden haya validado el registro, el nacional tiene derecho a ejercer su profesión en Francia.

#### Remedio

En caso de denegación de registro, podrá interponerse un recurso ante el Consejo Nacional de la Orden en el plazo de dos meses a partir de la notificación de la denegación de registro.

*Para ir más allá*: Artículos R. 242-85 y siguientes del Código Rural y Pesca Marina.

### b. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

Cualquier nacional de la UE o del EEE que esté establecido y practique legalmente actividades veterinarias en uno de estos Estados podrá ejercer en Francia de forma temporal u ocasional si hace la declaración previa (véase supra 2. b. "Nacionales de la UE y EEE: para ejerciciotemporal y ocasional (entrega de servicios gratuitos)).

#### Autoridad competente

La declaración debe enviarse por correo o correo electrónico, antes de la primera prestación de servicios, a la[Consejo Nacional de la Orden Nacional de Veterinarios](https://www.veterinaire.fr/).

#### Renovación de la predeclaración

La declaración previa debe renovarse cada año y en caso de cambio de situación laboral.

#### Documentos de apoyo

La predeclaración debe ir acompañada de un archivo completo con los siguientes documentos justificativos:

- Una copia de una documento de identidad válido o un documento que acredite la nacionalidad del solicitante;
- el[Formulario de Informes de Entrega de Servicios Por adelantado](https://www.veterinaire.fr/la-profession/le-metier-veterinaire/les-conditions-dexercice-en-france/la-libre-prestation-de-service-lps.html), completado, fechado y firmado;
- un certificado de la autoridad competente del Estado de Conciliación de la UE o del EEE que certifique que la persona está legalmente establecida en ese Estado y que no está prohibido ejercer, acompañado, en su caso, de una traducción francesa Establecido por un traductor certificado
- prueba de cualificaciones profesionales.

El Consejo Nacional de la Orden reconoce haber recibido la declaración anual de ejercicio ocasional y temporal en territorio francés en el plazo de un mes.

#### Costo

Gratis

#### Listado en la lista de empresas de práctica veterinaria

Los veterinarios que ejerzan como parte de una sociedad del ejercicio (Art L241-17 CPMR) deben solicitar al Colegio de Veterinarios.

La solicitud de registro debe ser presentada colectivamente por los socios. Está dirigido al Consejo Regional de la Orden de la Sede De la Compañía mediante carta recomendada con solicitud de notificación de recepción, junto con los siguientes documentos:

- Una copia de los estatutos y, si es necesario, una expedición o copia de la constitución;
- Un certificado de inscripción en la junta de cada socio
- un certificado del secretario del tribunal que se pronunció sobre la sede de la empresa, señalando la presentación en el registro de la solicitud y los documentos utilizados para su inscripción en el registro de comercio y empresas.

**Tenga en cuenta que**

El Consejo Regional de la Orden decide en las mismas condiciones que las mencionadas en el párrafo "5o. b. Solicitar inscripción en el Colegio de Veterinarios."

*Para ir más allá*: Artículos L. 241-17, R. 241-29 a R. 241-32 del Código Rural y Pesca Marina.

### c. Remedios

#### Solvit

SOLVIT es un servicio prestado por la administración nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresa de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

