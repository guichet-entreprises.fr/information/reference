<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP089" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Corredor de vinos y licores" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="corredor-de-vinos-y-licores" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/corredor-de-vinos-y-licores.html" -->
<!-- var(last-update)="2021-05" -->
<!-- var(url-name)="corredor-de-vinos-y-licores" -->
<!-- var(translation)="None" -->

# Corredor de vinos y licores [FR]

Dernière mise à jour : <!-- begin-var(last-update) -->2021-05<!-- end-var -->

## 1°. Définition de l'activité

Le courtier en vins et spiritueux, dit courtier « de campagne », est un intermédiaire professionnel dont l'activité consiste à mettre en relation les différents acteurs de la production viticole et vinicole.

Il est notamment chargé de rechercher et mettre en rapport producteurs et négociants et veiller à la bonne exécution de la transaction.

Dès la conclusion de celle-ci, le professionnel perçoit une rémunération de courtage.

*Pour aller plus loin* : articles 1 et 5 de la loi n° 49-1652 du 31 décembre 1949 réglementant la profession de courtier en vins dite de « courtier de campagne ».

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de courtier en vins et spiritueux, le professionnel doit :

- jouir de ses droits civils ;
- être de nationalité française ou en situation régulière sur le territoire français ;
- justifier de connaissances et/ou d'une expérience professionnelle.

*Pour aller plus loin* : article 2 de la loi du 31 décembre 1949 susvisée.

**Bon à savoir**

L'ordonnance n° 2015-1682 du 17 décembre 2015 portant simplification de certains régimes d'autorisation préalable et de déclaration des entreprises et des professionnels, entrée en vigueur au 1er janvier 2016, a prévu de remplacer la carte professionnelle par une inscription au Registre national des courtiers en vins. L'inscription au registre est de droit dès lors que les conditions prévues à l'article 2 de la loi du 31 décembre 1949 sont satisfaites.

*Pour aller plus loin* : article 1 du décret n° 2020-1253 du 13 octobre 2020 relatif au Registre national des courtiers en vins et spiritueux ; article 2 et 3 de la loi du 31 décembre 1949 susvisée.

#### Formation

L'accès et l'exercice de la profession de courtier en vins et spiritueux ne requiert aucune formation spécifique, mais requiert des qualifications professionnelles déterminées et attestées par un examen. 

Le candidat peut se présenter à l'examen s'il justifie :

- soit avoir effectué un stage de six mois minimum dans la filière viti-vinicole ;
- soit avoir obtenu un diplôme sanctionnant une formation dans la filière viti-vinicole ;
- soit avoir une expérience professionnelle de six mois minimum en qualité de travailleur indépendant ou salarié dans la filière viti-vinicole sur le territoire d'un État membre de l'Union européenne ou d'un État partie à l'accord sur l'Espace économique européen.

L'examen est organisé par la chambre de commerce et d'industrie dans le ressort territorial de laquelle le candidat souhaite exercer, et porte notamment sur les matières suivantes :

- connaissances administratives et juridiques ;
- connaissances des pratiques de médiation et de négociation ;
- connaissance du métier de courtier en vins et spiritueux ;
- connaissances générales des grandes régions viticoles de l'Union européenne ;
- connaissances de la région viticole envisagée pour l'activité ;
- connaissances de la vinification ;
- connaissances de la dégustation.

Pour aller plus loin : article 2-7 de la loi du 31 décembre 1949 ; article 1 du décret 2020-1254 du 13 octobre 2020 relatif à l'accès et à l'exercice de la profession de courtier en vins et spiritueux ; annexe du décret précité pour un exposé complet des matières évaluées lors de l'examen.

**Registre national des courtiers en vins**

Pour s'inscrire au Registre national, le courtier en vins et spiritueux doit :

- en effectuer la demande au président de CCI France par lettre recommandée avec avis de réception, ou par voie électronique ;
- remplir le modèle de déclaration et joindre :
  - une photocopie recto-verso de sa carte nationale d'identité ou de son passeport en cours de validité,
  - une attestation de l'employeur, si le demandeur est salarié, datant de moins de trois mois,
  - le paiement de la redevance relative au registre ;
- se soumettre à un examen devant un jury présidé par un juge consulaire et composé :
  - d'un professeur d'œnologie,
  - d'un représentant local de la profession de courtier en vins et spiritueux à la retraite,
  - d'un membre de chambre de commerce et d'industrie (CCI) territoriale.

Lorsque le demandeur a réussi cet examen, le président de la CCI lui délivre sa carte professionnelle dans un délai d'un mois à compter de la décision du jury d'examen.

*Pour aller plus loin* : article 1 de l'arrêté du 13 octobre 2020 fixant les modalités d'inscription au Registre national des courtiers en vins et spiritueux ; article 1 du décret n° 2020-1253 du 13 octobre 2020 relatif au Registre national des courtiers en vins et spiritueux.

### b. Ressortissants UE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant légalement établi sur le territoire d'un État membre de l'Union européenne ou d'un État partie à l'accord sur l'Espace économique européen, et qui exerce l'activité de courtier en vins et spiritueux, peut exercer cette activité de façon temporaire et occasionnelle en France librement.

Il n'a pas à demander au préalable son inscription au registre.

Dans ce cas, les prestations sont délivrées sous le titre habituellement utilisé dans l'État où le professionnel est établi.

*Pour aller plus loin* : article 1 du décret n° 2020-1253 du 13 octobre 2020 relatif au Registre national des courtiers en vins et spiritueux.

### c. Ressortissants UE : en vue d'un exercice permanent (Libre Établissement)

Tout ressortissant de l'UE ou de l'EEE, légalement établi et exerçant l'activité de courtier en vins et spiritueux, peut exercer en France, à titre permanent, la même activité, sans être soumis à examen.

Pour cela, l'intéressé doit satisfaire aux exigences permettant d'exercer l'activité dans des conditions équivalentes à celles requises en France :

- ne faire l'objet d'aucune interdiction d'exercer une profession commerciale ou industrielle ;
- ne pas exercer d'activité incompatible avec l'exercice de la profession de courtier en vins et spiritueux ;
- être légalement établi au sein de l'État membre ;
- justifier soit :
  - avoir exercé l'activité dans les mêmes conditions sur le territoire d'un État membre ou d'un État partie à l'EEE pendant deux ans de manière consécutive, et justifier d'une formation dans le domaine viticole,
  - avoir exercé l'activité dans les mêmes conditions sur le territoire d'un État membre ou d'un État partie à l'EEE pendant trois ans de manière consécutive,
  - être titulaire d'une attestation de compétence ou d'un titre de formation délivré par l'État membre qui réglemente l'accès ou l'exercice à la profession,
  - lorsque l'État ne réglemente pas l'activité, être titulaire d'une attestation de compétence ou d'un titre de formation justifiant qu'il a suivi une préparation en vue de l'exercice de cette activité, et justifier d'une expérience professionnelle pendant au moins trois ans dans cet État,
  - être titulaire d'un diplôme, d'un titre ou d'un certificat permettant d'exercer l'activité de courtier en vins et spiritueux et délivré par un État tiers et reconnu par un État membre, et avoir exercé cette activité pendant au moins trois ans au sein de cet État membre.

Dès lors qu'il remplit ces conditions, le ressortissant doit solliciter son inscription au registre.

Dans le cas où le professionnel ne satisfait pas à ces conditions :

  - son expérience professionnelle sur le territoire d'un État tiers pourra être prise en compte ;
  - il pourra lui être proposé un stage d'adaptation ou une épreuve d'aptitude.

*Pour aller plus loin* : article 1 du décret n° 2020-1253 du 13 octobre 2020 relatif à l'accès et à l'exercice de la profession de courtiers en vins et spiritueux.

## 3°. Conditions d'honorabilité

Pour exercer, le professionnel ne doit pas :

* faire l'objet d'une interdiction d'exercer une profession commerciale ou industrielle ou de diriger, d'administrer, de gérer ou de contrôler une entreprise commerciale ou industrielle ;
* effectuer d'achat ou de vente de vins et spiritueux à son compte, excepté pour des besoins familiaux ou de vins provenant de sa propriété ;
* être titulaire d'une licence de marchand de vins et spiritueux en gros ou de détail.

*Pour aller plus loin* : article 2 de la loi n° 49-1652 du 31 décembre 1949 réglementant la profession de courtier en vins dite de « courtier de campagne ».

**Incompatibilités**

Ne peuvent exercer l'activité de courtier en vins et spiritueux, les professionnels qui exercent les activités suivantes :

- achat ou vente de vin ou spiritueux en gros ou en détail ;
- membres des conseils d'administration ou des directoires, dirigeants et employés des négociants en vins et spiritueux ;
- membres des conseils d'administration, dirigeants et employés des caves coopératives, unions ou groupements de ces caves ;
- vinificateur et œnologue prestataires de services ;
- transitaire, transporteur, manutentionnaire ;
- dirigeants et employés d'organismes privés ou parapublics, dont l'activité est principalement consacrée à l'examen des questions relatives à la viticulture et au commerce des vins et spiritueux.

*Pour aller plus loin* : article 2-4 de la loi du 31 décembre 1949 ; article 5 du décret n° 2020-1254 du 13 octobre 2020 relatif à l'accès et à l'exercice de la profession de courtiers en vins et spiritueux.

## 4°. Sanctions

Dès lors qu'il cesse de remplir les conditions requises pour exercer, le professionnel est tenu, sur demande du président de la CCI, de justifier dans un délai d'un mois, qu'il remplit toujours ces conditions.

En l'absence de justification passé ce délai, le président de la CCI peut radier le professionnel de la liste des courtiers en vins et spiritueux.

Le professionnel qui exerce sous le titre de courtier en vins et ne se déclare pas en vue de son inscription au Registre national, qui n'informe pas le président de CCI France des modifications survenues dans sa situation, ou ne demande pas la radiation de son immatriculation au registre en dépit de la cessation d'exerce, s'oppose à une contravention de cinquième classe.

*Pour aller plus loin* : articles 3 et 4 du décret n° 2020-1253 du 13 octobre 2020 relatif au Registre national des courtiers en vins et spiritueux.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir l'inscription au Registre national

**Autorité compétente**

Le professionnel doit adresser sa demande au président de la CCI de la zone au sein de laquelle il souhaite exercer.

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
<tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">CCI du Maine et Loire pour les régions d'exercice suivantes :</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">CCI de Bourgogne Franche Comté pour les régions d'exercice suivantes :</th>
</tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
<tr>
<td style="border: 1px solid #AAA;padding: 4px">Normandie<br>Bretagne<br>Pays de la Loire<br>Centre-Val de Loire<br>Nouvelle-Aquitaine<br>Occitanie</td>
<td style="border: 1px solid #AAA;padding: 4px">Hauts-de-France<br>Île-de-France<br>Grand Est<br>Bourgogne-Franche-Comté<br>Auvergne-Rhône-Alpes<br>Provence-Alpes-Côte d'Azur<br>Corse
</td>
</tr>
</thead>
</table>

**Pièces justificatives**

Sa demande doit comporter :

- le modèle de déclaration (voir annexe de l’arrêté du 13 octobre 2020 fixant les modalités d’inscription au Registre national des courtiers en vins et spiritueux) ;
- une photocopie recto-verso de sa carte nationale d’identité ou de son passeport en cours de validité, le cas échéant copie du titre de séjour pour un ressortissant d’État tiers ;
- une attestation de l’employeur si le demandeur est salarié, datant de moins de trois mois ;
- le paiement de la redevance relative au Registre national ;
- s'il est immatriculé au registre du commerce et des sociétés (RCS) ou s'il est mentionné en tant que dirigeant ou associé d'une société, un extrait d'inscription datant de moins de trois mois ou à défaut, un document attestant sur l'honneur qu'il ne fait l'objet d'aucune interdiction d'exercer une profession commerciale ;
- pour le ressortissant de l'UE, un document justifiant qu'il a exercé l'activité de courtier en vins et spiritueux pendant deux années consécutives ;
- à défaut, une attestation de compétence ou un titre de formation délivré par un État membre justifiant sa préparation à l'exercice de la profession ;
- une attestation sur l’honneur certifiant que le demandeur n’exerce aucune activité incompatible avec l’exercice de la profession de courtier en vins et spiritueux, en application de l’article 2 de la loi du 31 décembre 1949 ;
- le justificatif du règlement de la redevance de 300 € net par virement bancaire (RIB joint).

**Délai et procédure**

Une fois l'examen d'aptitude validé, le président de la CCI l'inscrit au Registre national.

**À noter**

Le registre tenu par CCI France mentionne :

- l'identité du professionnel, sa date et lieu de naissance, son adresse ;
- le statut de l'intéressé au sein de l'entreprise ;
- la date d'inscription ou de modification.

*Pour aller plus loin* : article 2 du décret n° 2020-1253 du 13 octobre 2020 relatif au Registre national des courtiers en vins et spiritueux

### b. Voies de recours

**Centre d'assistance français**

Le Centre ENIC-NARIC est le centre français d'information sur la reconnaissance académique et professionnelle des diplômes.

**SOLVIT**

SOLVIT est un service fourni par l'Administration nationale de chaque État membre de l'UE ou partie à l'accord sur l'EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l'UE à l'Administration d'un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

**Conditions**

L'intéressé ne peut recourir à SOLVIT que s'il établit :

* que l'Administration publique d'un État de l'UE n'a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d'un autre État de l'UE ;
* qu'il n'a pas déjà initié d'action judiciaire (le recours administratif n'est pas considéré comme tel).

**Procédure**

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d'une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

**Pièces justificatives**

Pour saisir SOLVIT, le ressortissant doit communiquer :

* ses coordonnées complètes ;
* la description détaillée de son problème ;
* l'ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l'autorité administrative concernée).

**Délai**

SOLVIT s'engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

**Coût**

Gratuit.

**Issue de la procédure**

À l'issue du délai de dix semaines, le SOLVIT présente une solution :

* si cette solution règle le différend portant sur l'application du droit européen, la solution est acceptée et le dossier est clos ;
* s'il n'y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

**Informations supplémentaires**

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).