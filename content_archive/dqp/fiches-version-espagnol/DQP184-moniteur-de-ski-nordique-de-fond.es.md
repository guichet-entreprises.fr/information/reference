﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP184" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Instructor de esquí nórdico de fondo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="instructor-de-esqui-nordico-fondo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/instructor-de-esqui-nordico-de-fondo.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="instructor-de-esqui-nordico-de-fondo" -->
<!-- var(translation)="Auto" -->


Instructor de esquí nórdico de fondo
====================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El instructor de esquí nórdico supervisa esta disciplina y sus actividades derivadas de la seguridad. Realiza un enfoque didáctico en todo el entorno de montaña nevada en las montañas medias, en terreno montañoso excluyendo cualquier accidente de terreno y a granaltitud en laderas preparadas para esta práctica, marcadas y acicaladas ubicadas en alivios idénticos.

El esquí de fondo se define como una actividad de deslizamiento de nieve o en movimiento, generada principalmente por la fuerza muscular del esquiador solo y practicada con cualquier tipo de equipo dejando el talón de la zapatilla libre y en el terreno habitual dedicado a esto Práctica.

*Para ir más allá* : Apéndice VIII del decreto de 26 de abril de 2013 relativo a la formación específica del Diploma Estatal de Esquí Nacional de Esquí Nórdico.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La actividad de instructordeo de esquí nórdico está sujeta a la aplicación del artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas.

Como profesor de deportes, el instructor de esquí alpino debe poseer un diploma, un título profesional o un certificado de cualificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- registrado en el Registro Nacional de Certificaciones Profesionales (RNCP). Para más información, es aconsejable consultar el[Sitio web de RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos A. 212-1 Apéndice II-1, L. 212-1 y R. 212-84 del Código del Deporte.

**Bueno saber: el entorno específico**

El esquí nórdico, independientemente del área de evolución, es una actividad en un entorno específico que implica el cumplimiento de medidas de seguridad específicas. Por lo tanto, sólo las organizaciones bajo la tutela del Ministerio del Deporte pueden formar a futuros profesionales.

*Para ir más allá*: Artículos L. 212-2 y R. 212-7 del Código del Deporte.

#### Entrenamiento

##### Diploma Estatal de Esquí - Instructor Nacional de Esquí Nórdico

El Diploma Estatal de Instructor Nacional de Esquí Nórdico de Fondo está registrado en el Nivel III del Registro Nacional de Certificaciones Profesionales (RNCP).

Este diploma se expide al final de un curso que comprende un ciclo general común a la enseñanza, el entrenamiento y la formación de deportes de montaña y la formación específica para el esquí nórdico de fondo. Los programas respetan el principio de alternancia basado en la articulación de períodos de aprendizaje en centros de formación y situaciones profesionales bajo tutoría pedagógica.

La formación es imparte por la Escuela Nacional de Deportes de Montaña (ENSM), sede del Centro Nacional de Esquí Nórdico y Medio de Montaña (CNSNMM). Para obtener más información, visite el sitio web[CNSNMM](http://cnsnmm.sports.gouv.fr/index.php?option=com_content&view=article&id=47&Itemid=27).

Los solicitantes que deseen solicitar la validación de la experiencia para la graduación deben haber cumplido con la prueba de capacidad técnica y haber obtenido el primer ciclo (ver más abajo "Curso de formación").

*Para ir más allá*: Artículos D. 212-68 y los siguientes artículos del Código del Deporte; Artículo 28 del decreto de 26 de abril de 2013 relativo a la formación específica del Diploma Estatal de Instructor Nacional de Esquí Nórdico de Fondo.

**Prerrogativas**

El Diploma Estatal de Instructor de Esquí Nórdico certifica las habilidades necesarias para la supervisión, animación, enseñanza y formación en seguridad del esquí nórdico y sus actividades spin-off.

Permite a su propietario practicar en las montañas medias, en terreno montañoso excluyendo cualquier accidente de terreno importante. El instructor de esquí nórdico de fondo supervisa, de forma independiente e independiente, con cualquier tipo de equipo de esquí nórdico de fondo y con cualquier tipo de equipo derivado de este equipo, las siguientes actividades:

- El paseo del norte dura hasta un día en las pistas de esquí de fondo;
- Senderismo nórdico que dura uno o más días, en las pistas o fuera de las pistas de esquí de fondo. En el caso de que la caminata dure varios días, el alojamiento nocturno se organiza en una estructura o refugio vigilado;
- La incursión nórdica que dura uno o más días consecutivos en plena autonomía o no completa;
- esquí de fondo, derivado de la forma competitiva de esquí nórdico de fondo, que se practica en las pistas de esquí de fondo y también se puede practicar a altitud en las pistas preparadas para esta práctica, marcadas y acicaladas, situadas en terreno montañoso, excluyendo todas accidente de terreno importante.

*Para ir más allá*: Artículo 1 del Decreto de 26 de abril de 2013 sobre la formación específica del Diploma Estatal de Instructor Nacional de Esquí Nórdico de Fondo.

**Es bueno saber**

Las actividades derivadas del esquí nórdico abarcan los diversos elementos que definen el esquí de fondo (véase supra "1 definición"). Además de las formas habituales de esquí nórdico de fondo (marcha nórdica, senderismo, incursiones), las actividades spin-off más practicadas son el biatlón, el salto de esquí y el combinado nórdico, la telemarcación, la raqueta de nieve, skiercross, marcha nórdica sobre la nieve.

*Para ir más allá* : Apéndice VIII del decreto de 26 de abril de 2013 relativo a la formación específica del Diploma Estatal de Esquí Nacional de Esquí Nórdico.

**Entrenamiento de sensano**

La formación va precedida de una prueba de acceso técnico que incluye una prueba de rendimiento y un evento de demostración técnica. Este curso tiene lugar en el siguiente orden cronológico:

- El ciclo preparatorio de un mínimo de 70 horas repartidas en dos semanas;
- El curso de formación de sensibilización tiene una duración mínima de 25 días;
- La prueba de capacidad técnica
- el primer ciclo de cuatro semanas que consta de tres unidades de entrenamiento (UF) que se seguirán en el siguiente orden:- En primer lugar, los "fundamentos de la enseñanza del esquí nórdico de fondo y sus actividades derivadas" tienen una duración mínima de 70 horas repartidas en dos semanas,
  - a continuación, la UF "seguridad 1 esquí nórdico de fondo y actividades derivadas en el entorno de montaña" y el "entorno profesional" de la UF, de una duración mínima de 35 horas repartidas a lo largo de una semana, se pueden seguir en el orden deseado,
  - El curso de solicitud, que dura un mínimo de 25 días;
- el segundo ciclo de cinco semanas, que consta de tres UFs que deben seguirse en el siguiente orden cronológico:- UF "prácticas competitivas de esquí nórdico de fondo y sus actividades spin-off" que duran un mínimo de 35 horas repartidas en una semana,
  - UF "dominio técnico y pedagógico de la enseñanza del esquí nórdico de fondo y sus actividades spin-off" de 70 horas repartidas en dos semanas,
  - UF "seguridad 2 esquí nórdico de fondo y actividades derivadas de la deriva en entornos de montaña nevada" con una duración mínima de 70 horas repartidas en dos semanas.

*Para ir más allá*: Artículo 2 del decreto de 26 de abril de 2013 relativo a la formación específica del Diploma Estatal de Instructor Nacional de Esquí Nórdico de Fondo.

**Condiciones de acceso a la prueba de acceso técnico**

El interesado deberá:

- Tener 17 años de edad el 31 de diciembre del año natural en el que se lleva a cabo esta prueba;
- Proporcionar una solicitud de registro basada en una impresión estandarizada con un documento de identidad con fotografía reciente y una fotocopia a dos lados del documento nacional de identidad o pasaporte;
- para los solicitantes de nacionalidad francesa, nacidos a partir de 1979 para hombres y a partir de 1983 para mujeres, proporcionan una fotocopia del certificado censal o el certificado individual de participación en el día de la defensa y la ciudadanía;
- para menores, permiso previo de los padres o el del tutor legal;
- presentar un certificado médico de no contradictorio con la práctica y enseñanza del esquí de menos de un año de edad en la fecha de cierre del primer registro;
- proporcionar tres sobres de 23 x 16 cm con la tarifa actual y etiquetados en el nombre y la dirección del candidato.

*Para ir más allá*: Artículo 2 y Apéndice I del decreto de 26 de abril de 2013 relativo a la formación específica del Diploma Estatal de Instructor Nacional de Esquí Nórdico de Fondo.

**Condiciones de acceso al ciclo preparatorio**

El interesado deberá:

- Tener 18 años al menos en el primer día de entrenamiento;
- Proporcionar una solicitud de registro basada en una impresión estandarizada con permiso de los padres para menores para salidas fuera del tiempo de entrenamiento;
- presentar el certificado de pase de prueba técnico con menos de tres años de edad en la fecha de registro;
- Proporcionar una fotocopia del "Nivel 1 de Prevención Y Alivio Cívico" (PSC 1) o su equivalente;
- proporcionar un certificado médico de no contradictorio con la práctica y enseñanza del esquí alpino de menos de un año de edad en la fecha de cierre del primer registro;
- proporcionar dos fotografías de identidad recientes, tres sobres de pegatinas de 23 x 16 cm, publicados al ritmo actual y denominados en el nombre y la dirección del candidato, y un sobre de pegatina de 21 x 29,7 cm sellado a la tarifa actual para enviar un recomendado con acuse de recibo.

*Para ir más allá*: Artículo 7 y Apéndice II del decreto del 26 de abril de 2013 sobre la formación específica del Diploma Estatal de Instructor Nacional de Esquí Nórdico de Fondo.

**Reciclaje**

Los titulares del Diploma Estatal de Instructor Nacional de Esquí Alpino son sometidos cada seis años a un curso de actualización organizado por el NAMSC, el SITIO de la CNSNMM. El reciclaje debe tener lugar antes del 31 de diciembre del 6o año siguiente a la graduación o la última readaptación.

*Para ir más allá*: Artículo 1 del Decreto de 26 de abril de 2013 sobre la formación específica del Diploma Estatal de Instructor Nacional de Esquí Nórdico de Fondo.

##### Diploma Estatal de Esquí - Instructor de Esquí nórdico de fondo especializado en capacitación

El diploma estatal de instructor nacional de esquí de esquí nórdico de fondo especializado en formación está registrado en el nivel II del RNCP.

Este diploma se expide al final de un curso que comprende un ciclo general común a la enseñanza, el entrenamiento y la formación de deportes de montaña y la formación específica para el esquí nórdico de fondo. Los programas respetan el principio de alternancia basado en la articulación de períodos de aprendizaje en centros de formación y situaciones profesionales bajo tutoría pedagógica.

La capacitación es proporcionada por el NMSB, el sitio CNSNMM.

Los solicitantes que deseen solicitar la validación de la experiencia (VAE) para el Diploma Estatal de Instructor Nacional de Esquí Nórdico especializado en formación deben haber cumplido con los requisitos de precertificación y han obtenido la unidad de entrenamiento "optimización del rendimiento en el esquí nórdico de fondo y spin-off".

*Para ir más allá*: Artículos D. 212-68 y los siguientes artículos del Código del Deporte; Artículos 2 y 15 de la Orden de 26 de febrero de 2016 relativo al Diploma Estatal de Monitor Nacional de Esquí de Nórdico especializado en Formación.

**Prerrogativas**

El diploma estatal de instructor nacional de esquí de esquí nórdico de fondo especializado en la formación atestigua las habilidades requeridas en el esquí nórdico de fondo y sus actividades derivadas para la formación, el entrenamiento y el desarrollo de las carreras de esquí. También ofrece formación a los entrenadores que compiten en el proceso de formación, así como la gestión y promoción de estructuras de formación.

*Para ir más allá*: Artículo 1 de la Orden de 26 de abril de 2013 relativa al Diploma Estatal de Esquí-Monitor Nacional de Esquí Nórdico de Fondo Especializado en Formación.

**Entrenamiento de sensano**

La duración total del curso de formación es de 490 horas. Se lleva a cabo en el siguiente orden cronológico:

- UF "optimización del rendimiento en actividades nórdicas de esquí multipaís y spin-off" con una duración de 245 horas;
- El campo de 140 horas en una situación;
- UF "gestiona un proyecto competitivo de capacitación para el desarrollo de autocares de 105 horas en actividades de esquí nórdico de fondo y spin-off".

*Para ir más allá*: Artículo 2 del Decreto de 26 de febrero de 2016 relativo al Diploma Estatal de Monitor Nacional de Esquí de Nórdico especializado en Formación.

**Condiciones de acceso**

El interesado deberá:

- realizar una prueba escrita de 2 horas para analizar un documento o vídeo relacionado con una secuencia o proyecto de formación en actividades de esquí nórdico a través y spin-off. Le permite comprobar el conocimiento y la capacidad del candidato para escribir un proyecto de capacitación;
- realizar una entrevista de hasta 30 minutos, que permite al jurado evaluar la capacidad del candidato para establecer relaciones con el entorno profesional e institucional del esquí nórdico y sus actividades spin-off y motivación profesional;
- completar los requisitos de registro previo a la entrada, que incluye los siguientes documentos:- una copia del certificado estatal de educador deportivo de 1er grado, opción "esquí nórdico de fondo", o el diploma estatal de esquí nacional de esquí nórdico de fondo o la exención otorgada por la sección permanente de esquí de fondo de la Formación y el uso del Consejo Superior de Deportes de Montaña en casos específicos, tales como personas que poseen el certificado estatal de educador deportivo de 1o grado, opción de "esquí alpino", o el diploma estatal de instructor nacional de esquí alpino,
  - un certificado de la Federación Francesa de Esquí (FFS) que certifique una clasificación de puntos del FFS de ciento veinte puntos,
  - un dossier de cinco a diez páginas sobre la experiencia del candidato durante su carrera deportiva y profesional, así como su motivación y proyecto profesional,
  - un formulario de registro estandarizado,
  - Una fotocopia de un documento de identidad,
  - un certificado médico de no contradictorio con la práctica, la enseñanza y la formación del esquí nórdico de fondo que data de menos de un año a la fecha de cierre de la inscripción,
  - Un documento de identidad con foto,
  - dos sobres estampados.

*Para ir más allá*: Artículo 4 y Apéndice I del Decreto de 26 de febrero de 2016 relativo al Diploma Estatal de Instructor Nacional de Esquí Nórdico para el Esquí Nórdico de Fondo Especializado en Formación.

#### Costos asociados con la calificación

Se paga la formación que conduzca al diploma estatal de ski-monitor nacional de esquí nórdico y al diploma estatal de monitor de esquí nacional de esquí nórdico especializado en formación. Para más detalles, es aconsejable acercarse a la[CNSNMM](http://cnsnmm.sports.gouv.fr/).

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecido en uno de estos Estados puede llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del Departamento de Isáre.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte; Artículo 29-1 del Decreto de 11 de abril de 2012 sobre la formación específica del Diploma Estatal de Instructor Nacional de Esquí de Esquí Alpino.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como instructor de esquí nórdico de fondo en Francia para las personas que han sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Obligación de presentación de informes (con el fin de obtener la tarjeta de educador deportivo profesional)

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial](https://eaps.sports.gouv.fr).

**hora**

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

**Documentos de apoyo**

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

#### Autoridad competente

La declaración previa de actividad debe dirigirse al prefecto del Departamento de Isáre.

#### hora

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

#### Documentos de apoyo

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### c. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

#### Autoridad competente

La declaración debe dirigirse al prefecto del Departamento de Isáre.

#### hora

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

#### Documentos de apoyo para la primera declaración de actividad

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

#### Evidencia para una declaración de renovación de la actividad

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

### d. Medidas de compensación

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-90-1 y R. 212-92 del Código del Deporte.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

