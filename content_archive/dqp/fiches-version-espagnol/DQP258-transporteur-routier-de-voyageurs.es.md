﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP258" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transporte" -->
<!-- var(title)="Transporte de pasajeros por carretera" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transporte" -->
<!-- var(title-short)="transporte-de-pasajeros-por-carretera" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/transporte/transporte-de-pasajeros-por-carretera.html" -->
<!-- var(last-update)="Augusto de 2021" -->
<!-- var(url-name)="transporte-de-pasajeros-por-carretera" -->
<!-- var(translation)="Auto" -->


Transporte de pasajeros por carretera
=====================================

Actualización más reciente: <!-- begin-var(last-update) -->Augusto de 2021<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El transportista público de pasajeros por carretera es un profesional cuya actividad consiste en el transporte de pasajeros a su retribución mediante vehículos de carretera y motorizados de al menos cuatro plazas (incluido el conductor).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de un transportista por carretera de los viajeros, el profesional debe:

- Estar registrado como una empresa de transporte por carretera;
- obtener una licencia para ejercer la profesión y, para ello, cumplir con los requisitos de honorabilidad, capacidad financiera y profesional.

Además, toda empresa que desee operar como transportista público por carretera de personas debe designar un administrador de transporte para gestionar el mantenimiento de vehículos, la verificación de contratos y documentos de transporte, auditoría de los procedimientos de seguridad y asignación de cargas o servicios a los conductores.

*Para ir más allá*: Artículos L. 3113-1 y R. 3113-43 del Código de Transporte; Orden de 28 de diciembre de 2011 relativa a la autorización para ejercer como transportista público por carretera y a los términos de la solicitud de autorización por parte de las empresas.

**Tenga en cuenta que**

Algunos profesionales están exentos del requisito de capacidad financiera y profesional:

- Individuos y asociaciones que ofrecen servicios de transporte a la carta y escolares (si hay falta de suministro de transporte) cuando utilizan un solo vehículo con hasta nueve plazas;
- Empresas que realizan esta actividad bajo demanda o de forma regular como cómplice de otra actividad y tienen un solo vehículo con hasta nueve plazas;
- autoridades locales y las autoridades locales que prestan servicios de transporte con fines no comerciales y poseen hasta dos vehículos.

*Para ir más allá*: Artículos R. 3113-10 y R. 3113-11 del Código de Transporte.

#### Condiciones de acceso a la profesión

**Establecimiento**

Para operar en Francia, el profesional debe:

- Tener su sede o establecimiento principal en Francia;
- en las instalaciones de su sede central, o en su establecimiento principal, en ausencia de una sede central en Francia:- documentos clave de la empresa (documentos contables, gestión de personal, datos de tiempo de trabajo y descanso de los conductores, etc.),
  - La licencia de transporte original de la Comunidad o nacional expedida por el prefecto regional,
  - Si es necesario, todos los contratos con las autoridades que organizan servicios de transporte público para personas,
  - Cualquier documento relacionado con la actividad de la empresa;
- Tener al menos un vehículo registrado
- dirigir de manera efectiva y continua las actividades de estos vehículos de pasajeros.

*Para ir más allá*: Artículo R. 3113-18 a R. 3131-22 del Código de Transporte.

**Capacidad financiera**

Cada año, el profesional debe, mediante documentos certificados (por un contador, un auditor o un centro de gestión certificado), certificar su capacidad financiera.

Para ello, debe justificar tener capital y reservas:

- al menos 1.500 euros por cada vehículo que no exceda de nueve plazas, incluido el conductor;
- 9.000 euros para el primer vehículo y 5.000 euros para cada uno de los siguientes vehículos, para vehículos que superen este límite.

En caso contrario, el profesional podrá presentar garantías otorgadas por una o más entidades financieras que posean la libertad bajo fianza, siempre que esta garantía no supere la mitad de la capacidad financiera adeudada.

*Para ir más allá*: Artículos R. 3113-31 a R. 3113-34 del Código de Transporte.

**Capacidad profesional**

El profesional que desee llevar a cabo la actividad de un transportista de mercancías por carretera deberá someterse a un examen escrito sobre todas las materias establecidas en la Lista I del Reglamento (CE) 1071/2009 del Parlamento Europeo y del Consejo, de 21 de octubre de 2009, por el que se establecen normas comunes sobre las condiciones que deben cumplirse para ejercer la profesión de transportista por carretera.

Sin embargo, también puede obtener el certificado de capacidad profesional, la persona interesada que:

- posee un diploma, un título universitario, un certificado de estudio o una designación profesional expedido en Francia por una institución de educación superior o un organismo autorizado y, siempre y cuando haya recibido, durante su formación, los conocimientos necesario para llevar a cabo esta actividad;
- justifica haber gestionado continuamente una empresa de transporte público por carretera para personas en uno o varios Estados de la UE en los últimos diez años antes del 4 de diciembre de 2009.

Además, también se puede expedir a los profesionales un certificado de capacidad profesional en el transporte por carretera de personas con vehículos que no excedan de nueve plazas:

- titulares de un diploma o designación profesional expedido en Francia que implique el conocimiento de las materias establecidas en la Lista I del Reglamento de 21 de octubre de 2009 y con sujeción a la abislación del examen escrito;
- justificando que había gestionado una empresa de transporte por carretera de forma continua y principal durante dos años y, siempre que no hubiera dejado de operar durante más de diez años.

**Tenga en cuenta que**

Mientras el profesional no haya estado en el negocio durante cinco años, el prefecto puede entrenarlo para actualizar sus conocimientos.

*Para ir más allá*: Artículos R. 3113-35 a R. 3113-42 del Código de Transporte.

#### Inscripción en el registro de empresas de transporte por carretera

**Autoridad competente**

El profesional debe solicitar al prefecto de la región donde se encuentra la sede de su empresa. Las empresas que no tienen su sede en Francia deben solicitar al prefecto de la región donde se encuentra su principal establecimiento.

**Documentos de apoyo**

Su solicitud deberá incluir, en su caso, el formulario cerfa No. [16093](https://www.service-public.fr/professionnels-entreprises/vosdroits/R57874) o [16094](https://www.service-public.fr/professionnels-entreprises/vosdroits/R14156) solicitando autorización para el ejercicio de la profesión de operador de transporte por carretera e inscripción en el registro, así como los justificantes mencionados.

**Resultado del procedimiento**

El prefecto tiene tres meses para decidir sobre la solicitud. Este plazo puede prorrogarse un mes en caso de que falten documentos en el archivo. El registro en este registro da lugar a la emisión por parte del prefecto:

- una licencia comunitaria cuando la empresa utiliza uno o más autobuses o autocares, siempre que no esté inscrito en el registro de transporte público;
- una licencia de transporte interno cuando la empresa utiliza uno o más vehículos que no sean autobuses o autocares, o cuando está inscrita en el registro de transporte público.

Estas licencias con una validez renovable de 10 años se expiden con tantas copias certificadas como vehículos registrados.

*Para ir más allá*: Artículo R. 3113-2 a R. 3113-9 del Código de Transporte; Decreto de 28 de diciembre de 2011 relativo a la autorización para el ejercicio de la profesión de operador de transporte público por carretera y los términos y condiciones de la solicitud de autorización por parte de las empresas..

3°. Condiciones de honorabilidad
-----------------------------------------

El profesional que se dedica a una actividad de transporte itinerante no debe haber estado sujeto a:

- varias condenas mencionadas en el boletín 2 de su historial delictivo y que llevaron a la prohibición de ejercer una profesión comercial o industrial;
- de varias condenas mencionadas en el boletín 2 de su historial penal tan pronto como él:- no cumplía con los requisitos para el transporte de materiales peligrosos, en términos de fijación de precios del contrato, falsificación de documentos o datos electrónicos, información falsa o falta de instalación dispositivos de control o en el ámbito de la prevención y gestión de residuos,
  - ha cometido un delito en virtud del Código Penal por lesiones personales o agresiones sexuales, código de comercio concursal o Ley de Tráfico de Carreteras.

*Para ir más allá*: Artículos R. 3113-23 a R. 3113-30, L. 1252-5 a L. 1252-7, L. 3242-2 a L. 3242-5 y L. 3315-4 a L. 3315-6 del Código de Transporte; Artículos L. 654-4 a L. 654-15 del Código de Comercio; Artículos 221-6-1, 222-19-1 y siguientes del Código Penal; Artículo L. 541-46-5 del Código de Medio Ambiente; Sección L. 221-2 y la siguiente de la Ley de Tráfico de Carreteras.