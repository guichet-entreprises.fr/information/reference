﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP213" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Farmacéutico" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="farmaceutico" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/farmaceutico.html" -->
<!-- var(last-update)="2020-04-15 17:21:58" -->
<!-- var(url-name)="farmaceutico" -->
<!-- var(translation)="Auto" -->


Farmacéutico
============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:58<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El farmacéutico es un profesional de la salud cuya misión principal es entregar medicamentos al público. Como parte de su actividad, se asegurará de que las dosis prescritas por el médico a una persona enferma sean conformes, o explicará el tratamiento. También puede ser necesario hacer preparaciones específicas con receta médica.

Además, se le exige que realice más tareas administrativas. Por lo tanto, debe gestionar la contabilidad de su dispensario, ordenar y recibir los productos, así como gestionar el inventario.

Cuando trabaje en el laboratorio, analizará las muestras, creará nuevos fármacos y supervisará los existentes.

*Para ir más allá*: Artículo L. 4211-1 del Código de Salud Pública

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

A tenor del artículo L. 4221-1 del Código de Salud Pública, para ejercer legalmente como farmacéutico en Francia, los interesados deberán cumplir acumulativamente los tres requisitos siguientes:

- poseer el diploma estatal francés de farmacéutico o un diploma, certificado u otro título mencionado en los artículos L. 4221-2 a L. 4221-5 del Código de Salud Pública (véase infra "Bueno saber: reconocimiento automático del diploma");
- ser nacional francés, ciudadano andorrano, nacional de un Estado miembro de la Unión Europea o parte en el acuerdo sobre el Espacio Económico Europeo, o nacional de un país en el que los franceses puedan ejercer la farmacia cuando titulares del diploma que abre el ejercicio a los nacionales de este país;
- con excepciones, figurar en la Junta Directiva del Colegio de Farmacéuticos (véase infra "5o. b. Solicitar inclusión en la lista del Colegio de Farmacéuticos").

**Bueno saber: graduación automática**

En virtud de los artículos L. 4221-2 a L. 4221-5 del Código de Salud Pública, los nacionales de la UE o del EEE podrán ejercer como farmacéuticos si poseen uno de los siguientes títulos:

- uno de los certificados farmacéuticos expedidos por un Estado de la UE o del EEE citado en[Anexo](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=D81969C59246FC9D4C6D48DB84015B93.tplgfr23s_3?idArticle=LEGIARTI000027964612&cidTexte=LEGITEXT000006055521&dateTexte=20180118) 13 de febrero de 2007;
- certificado de farmacéutico expedido por un Estado de la UE o del EEE que no figura en la lista del anexo anterior, acompañado de un certificado de dicho Estado;
- un título expedido por un Estado de la UE o por el Estado del EEE sancionando la formación farmacéutica iniciada antes de una de las fechas mencionadas en el decreto de 13 de febrero de 2007, y acompañada de un certificado que certifique que el nacional se ha dedicado legalmente a Actividades farmacéuticas durante al menos tres años en los cinco años anteriores a la expedición del certificado;
- un título expedido por un Estado de la UE o del EEE, sancionando la formación farmacéutica iniciada antes de una de las fechas mencionadas en el decreto de 13 de febrero de 2007, y permitiendo la práctica jurídica de farmacéutico en dicho Estado. El nacional debe justificar haber pasado tres años en la función hospitalaria en Francia como agregado asociado, profesional asociado, asistente asociado o en la función universitaria como jefe de clínica asociada de la universidades o asistentes asociados de universidades.

*Para ir más allá*: Artículo L. 4221-2 a L. 4221-5 del Código de Salud Pública; decreto de 13 de febrero de 2007 por el que se establece la lista de diplomas, certificados y otros títulos farmacéuticos expedidos por los Estados miembros de la Unión Europea, los Estados Partes en el Acuerdo sobre el Espacio Económico Europeo y la Confederación Suiza contemplados en el artículo L. 4221-4 (1) del Código de Salud Pública.

#### Entrenamiento

Los estudios de farmacia constan de tres ciclos con una duración total de entre seis y nueve años dependiendo del rango elegido.

**Grado de educación general en ciencias farmacéuticas**

El primer ciclo está sancionado por el diploma de formación general en ciencias farmacéuticas. Consta de seis semestres y corresponde al nivel de licencia. Los dos primeros semestres corresponden a THE PACES.

El objetivo de la formación es adquirir:

- Conocimientos básicos en el campo de las ciencias exactas y las ciencias biológicas;
- Conocimientos específicos de las disciplinas necesarias para estudiar el medicamento y otros productos de salud;
- Las habilidades necesarias para hacer el uso correcto de este conocimiento
- elementos útiles para la orientación del estudiante a las diversas profesiones de la farmacia, incluyendo las áreas de farmacias officiina y domésticas, biología médica, industria e investigación.

Incluye enseñanzas teóricas, metodológicas, aplicadas y prácticas, así como la realización de una pasantía obligatoria en officiine y dos pasantías opcionales.

*Para ir más allá* :[Detenido](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023850773&dateTexte=20180118) 22 de marzo de 2011 sobre el régimen educativo del Diploma general de educación en ciencias farmacéuticas.

**Grado de formación en profundidad en ciencias farmacéuticas**

El segundo ciclo está sancionado por el diploma de formación en profundidad en ciencias farmacéuticas. Los titulares de un título general de formación en ciencias farmacéuticas pueden inscribirse en la formación. Consta de cuatro semestres de formación y corresponde al nivel máster.

Su objetivo es:

- la adquisición de conocimientos científicos, médicos y farmacéuticos, complementando y profundizando los adquiridos durante el primer ciclo;
- formación en el proceso científico
- Conocimientos prácticos
- Aprender a trabajar en equipo y adquirir técnicas de comunicación
- introducción al desarrollo profesional continuo.

Además de las enseñanzas teóricas y prácticas, la formación incluye la realización:

- una pasantía en la oficina de una a dos semanas;
- 12 meses de prácticas hospitalarias a tiempo parcial o seis meses.

El segundo ciclo es validado por el éxito del conocimiento de las enseñanzas enseñadas durante la formación, así como la emisión de un certificado de síntesis farmacéutica.

*Para ir más allá*: Artículos 4 a 17 de la orden del 8 de abril de 2013 sobre el régimen educativo del Diploma de Doctor a Nivel Estatal de Farmacia.

**Diploma de Doctor estatal en Farmacia**

El tercer ciclo está sancionado por la emisión del diploma estatal de médico en farmacia. Incluye:

- Un breve ciclo de dos semestres de formación;
- un ciclo de ocho semestres de formación para los estudiantes que han sido galardonados con el concurso de internado;
- la defensa de una tesis.

El título de postgrado corto debe permitir al estudiante:

- profundizar los conocimientos y habilidades relacionados con la trayectoria profesional elegida y posiblemente participar en una especialización farmacéutica particular, específica de esta orientación profesional;
- para preparar su tesis para la realización del diploma estatal de doctor en farmacia.

Se acompaña de una pasantía práctica profesional a tiempo completo de seis meses, que se puede completar en un dispensario, una farmacia mutua, una farmacia de una empresa de ayuda minera, un establecimiento farmacéutico o en un un establecimiento industrial o comercial cuyas actividades puedan contribuir a la formación del farmacéutico.

Dentro de dos años de validar las enseñanzas impartidas durante el corto grado de postgrado, el estudiante debe apoyar una tesis ante un jurado.

*Para ir más allá*: Artículo L. 633-2 del Código de Educación; Artículo 18 y siguiente de la orden de 8 de abril de 2013 relativa al régimen educativo del Diploma Estatal de Doctor en Farmacia.

#### Costos asociados con la calificación

Se paga la formación que conduce a la obtención del GRADO DEL DOCTOR en farmacia. Su costo varía dependiendo de las universidades que proporcionan las enseñanzas. Para más información, es aconsejable acercarse a la universidad en cuestión.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

El profesional que sea miembro de un Estado de la UE o del EEE establecido y que practique legalmente en uno de estos Estados podrá llevar a cabo la misma actividad en Francia de forma temporal y ocasional sin estar incluido en la Orden de Farmacéuticos.

Para ello, el profesional debe hacer una declaración previa, así como una declaración que justifique que tiene las habilidades de lenguaje necesarias para ejercer en Francia (ver infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

**Qué saber**

No es necesario registrarse en la lista del Colegio de Farmacéuticos para el profesional de servicio gratuito (LPS). Por lo tanto, no está obligado a pagar cuotas ordinales. El farmacéutico está simplemente inscrito en una lista específica mantenida por el Consejo Nacional del Colegio de Farmacéuticos.

La predeclaración debe ir acompañada de una declaración sobre las habilidades del idioma necesarias para llevar a cabo el servicio. En este caso, el control del dominio del idioma debe ser proporcional a la actividad que debe llevarse a cabo y llevarse a cabo una vez reconocida la cualificación profesional.

Cuando los títulos de formación no reciben reconocimiento automático (véase supra "2.0). a. Legislación nacional"), las cualificaciones profesionales del proveedor se comprueban antes de que se preste el primer servicio. En caso de diferencias sustanciales entre las cualificaciones del interesado y la formación requerida en Francia que pueda perjudicar a la salud pública, el solicitante se somete a una prueba de aptitud.

Los farmacéuticos en situaciones LPS están obligados a respetar las normas profesionales aplicables en Francia, incluidas todas las normas éticas (véase infra "3o. Condiciones de honorabilidad, reglas éticas, ética"). Está sujeto a la jurisdicción disciplinaria del Colegio de Farmacéuticos.

**Tenga en cuenta que**

El servicio se realiza bajo el título profesional francés de farmacéutico. Sin embargo, cuando no se reconocen las cualificaciones de formación y no se han verificado las cualificaciones, el rendimiento se lleva a cabo bajo el título profesional del Estado de establecimiento, con el fin de evitar confusiones Con el título profesional francés.

*Para ir más allá*: Artículo L. 4222-9 del Código de Salud Pública.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

**El sistema automático de reconocimiento de diplomas**

El artículo L. 4221-4 del Código de Salud Pública establece un régimen de reconocimiento automático en Francia de determinados títulos o títulos, en su caso, acompañados de certificados, obtenidos en un Estado de la UE o del EEE (véase más arriba "2". a. Legislación Nacional").

Corresponde al Consejo Nacional del Colegio de Farmacéuticos verificar la regularidad de diplomas, títulos, certificados y certificados, conceder el reconocimiento automático y luego pronunciarse sobre la solicitud de inclusión en la lista de la Orden.

*Para ir más allá*: Artículo L. 4221-1 del Código de Salud Pública.

**El régimen de autorización individual para ejercer**

Si el nacional de la UE o del EEE no reúne los requisitos para el reconocimiento automático de sus credenciales, está comprendido en un régimen de autorización (véase más adelante "5o). c. Si es necesario, solicitar un permiso de ejercicio individual").

Las personas que no reciben reconocimiento automático pero que poseen una designación de formación para ejercer legalmente como farmacéutico pueden poder ejercer individualmente en la especialidad de que se trate. Ministro de Salud, previa asesoría de una comisión formada por profesionales.

Si el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia profesional muestra diferencias sustanciales con las cualificaciones requeridas para acceder a la profesión en la especialidad de que se trate y su ejercicio en Francia, el interesado debe someterse a una medida de indemnización.

En función del nivel de cualificación exigido en Francia y del que posea el interesado, la autoridad competente podrá:

- Ofrecer al solicitante la opción de elegir entre un curso de ajuste o una prueba de aptitud;
- imponer un curso de ajuste o una prueba de aptitud
- imponer un curso de ajuste y un alcierto.

*Para ir más allá*: Artículo L. 4221-1-1 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Cumplimiento del Código de ética de los farmacéuticos

Las disposiciones del Código de ética se imponen a todos los farmacéuticos que practican en Francia, tanto si están inscritos en el consejo de la Orden como si están exentos de esta obligación (véase supra "5". b. Solicitar inclusión en la lista del Colegio de Farmacéuticos").

**Qué saber**

Todas las disposiciones del Código de ética están codificadas en las secciones R. 4235-1 a R. 4235-77 del Código de Salud Pública.

Como tal, los farmacéuticos deben respetar los principios de dignidad, no discriminación, secreto profesional o independencia.

*Para ir más allá*: Artículos L. 4235-1, y R. 4235-1 a R. 4235-77 del Código de Salud Pública.

### b. Actividades acumulativas

El farmacéutico sólo puede realizar cualquier otra actividad si tal combinación es compatible con los principios de independencia profesional y dignidad que se le imponen.

Un farmacéutico que cumple una función electiva o administrativa también tiene prohibido utilizarlo para aumentar su clientela.

*Para ir más allá*: Artículos R. 4235-4 y R. 4235-23 del Código de Salud Pública.

### c. Obligación para el desarrollo profesional continuo

Los farmacéuticos deben participar anualmente en un programa de desarrollo profesional en curso. Este programa tiene como objetivo mantener y actualizar sus conocimientos y habilidades, así como mejorar sus prácticas profesionales.

Como tal, el profesional de la salud (salario o liberal) debe justificar su compromiso con el desarrollo profesional. El programa está en forma de formación (presente, mixta o no presental) en análisis, evaluación y mejora de la práctica y gestión de riesgos. Toda la formación se registra en un documento personal que contiene certificados de formación.

*Para ir más allá*: Artículo R. 4235-11 del Código de Salud Pública; Decreto No 2016-942, de 8 de julio de 2016, relativo a la organización del desarrollo profesional continuo de los profesionales sanitarios.

4°. Seguro
-------------------------------

### a. Obligación de constete de un seguro de responsabilidad civil profesional

Como profesional de la salud, un farmacéutico que proactúe a modo liberal debe disponer de un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

### b. Fondo de Seguro de Vejez de Farmacéuticos (CAVP)

Cualquier farmacéutico inscrito en el Consejo de Administración del Colegio de Farmacéuticos y que actúe en forma liberal (incluso a tiempo parcial e incluso si también está empleado) tiene la obligación de adherirse al CAVP.

El individuo debe informar a la OPC en el plazo de un mes a partir del inicio de su actividad liberal.

*Para ir más allá*: Artículo R. 643-1 del Código de la Seguridad Social; el sitio de la[Cavp](https://www.cavp.fr/).

### c. Obligación de Informes de Seguros Médicos

Una vez en la lista de la Orden, el farmacéutico que interactúe en forma liberal debe declarar su actividad en el Fondo de Seguro Médico Primario (CPAM).

**Términos**

El registro en el CPAM se puede hacer en línea en el sitio web oficial de Medicare.

**Documentos de apoyo**

El solicitante de registro debe proporcionar un archivo completo que incluya:

- Copiar una identificación válida
- un declaración de identidad bancaria profesional (RIB).

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

Cualquier nacional de la UE o del EEE que esté establecido y practique legalmente a los farmacéuticos en uno de estos Estados podrá ejercer en Francia de forma temporal u ocasional si hace la declaración previa (véase supra "2o. b. Nacionales de la UE y del EEE: para un ejercicio temporal e informal (Entrega gratuita de servicios)").

La declaración anticipada debe renovarse cada año.

**Tenga en cuenta que**

Cualquier cambio en la situación del solicitante debe ser notificado en las mismas condiciones.

**Autoridad competente**

La declaración debe dirigirse, antes de la primera prestación de servicios, al Consejo Nacional del Colegio de Farmacéuticos.

**Condiciones de presentación de informes y recepción**

La declaración puede enviarse por correo o directamente en línea en la página web oficial del Colegio de Farmacéuticos.

Cuando el Consejo Nacional de la Orden recibe la declaración y todos los documentos justificativos necesarios, envía al demandante un recibo especificando su número de registro, así como la disciplina ejercitada.

**Tenga en cuenta que**

El prestador de servicios informa a la agencia nacional de seguros de salud pertinente de su prestación de servicios mediante el envío de una copia del recibo o por cualquier otro medio.

**hora**

En el plazo de un mes a partir de la recepción de la declaración, el Consejo Nacional de la Orden informa al solicitante:

- Si puede o no comenzar a prestar servicios;
- cuando la verificación de las cualificaciones profesionales muestra una diferencia sustancial con la formación requerida en Francia, debe demostrar haber adquirido los conocimientos y habilidades que faltan Aptitud. Si cumple con este cheque, se le informa en el plazo de un mes que puede comenzar la prestación de servicios;
- cuando la revisión del archivo resalta una dificultad que requiere más información, las razones del retraso en la revisión del archivo. Luego tiene un mes para obtener la información adicional solicitada. En este caso, antes de que finalice el segundo mes a partir de la recepción de esta información, el Consejo Nacional informa al demandante, tras revisar su expediente:- si puede o no comenzar la prestación de servicios,
  - cuando la verificación de las cualificaciones profesionales del demandante muestre una diferencia sustancial con la formación requerida en Francia, debe demostrar que ha adquirido los conocimientos y habilidades que faltan, sujeto a una prueba de aptitud.

En este último caso, si cumple con este control, se le informa en el plazo de un mes que puede iniciar la prestación de servicios. De lo contrario, se le informa de que no puede comenzar la prestación de servicios. En ausencia de una respuesta del Consejo Nacional de la Orden dentro de estos plazos, la prestación de servicios puede comenzar.

**Documentos de apoyo**

La predeclaración deberá ir acompañada de una declaración sobre las aptitudes linguísticas necesarias para llevar a cabo el servicio y los siguientes documentos justificativos:

- el[Formulario de Informes de Entrega de Servicios Por adelantado](http://www.ordre.pharmacien.fr/content/download/376629/1813657/version/1/file/Formulaire+-+Arr%C3%AAt%C3%A9+du+8+d%C3%A9cembre+2017+relatif+%C3%A0+la+d%C3%A9claration+pr%C3%A9alable+de+prestation+de+services+.pdf) ;
- Copia de una identificación válida o un documento que acredite la nacionalidad del solicitante;
- Copia del documento o títulos de formación, acompañados, si es necesario, de una traducción de un traductor certificado;
- un certificado de la autoridad competente del Estado de Conciliación de la UE o del EEE que certifique que la persona está legalmente establecida en ese Estado y que no está prohibido ejercer, acompañado, en su caso, de una traducción francesa establecido por un traductor certificado.

**Tenga en cuenta que**

El control del dominio del idioma debe ser proporcional a la actividad que debe llevarse a cabo y llevarse a cabo una vez reconocida la cualificación profesional.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 4222-9 en L. 4222-10, y R.4112-9 en R. 4112-12 del Código de Salud Pública.

### b. Formalidades para los nacionales de la UE o del EEE para un ejercicio permanente (LE)

#### Si es necesario, solicite autorización individual para ejercer

Si el nacional no está en virtud del régimen de reconocimiento automático, debe solicitar una licencia para ejercer.

**Autoridad competente**

La solicitud se dirige en dos copias, por carta recomendada con solicitud de notificación de recepción a la unidad responsable de las comisiones de autorización de ejercicio (CAE) del Centro Nacional de Gestión (NMC).

Debe añadir a su petición:

- el[formulario de solicitud de autorización para ejercer la profesión](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=4349F01DDAE283C55604F0C28FE06D3D.tplgfr23s_3?idArticle=LEGIARTI000029734948&cidTexte=LEGITEXT000029734898&dateTexte=20180118) ;
- Una fotocopia de un documento de identidad válido
- Una copia del título de formación que permita el ejercicio de la profesión en el estado de obtención, así como, en su caso, una copia del título de formación especializada;
- Si es necesario, una copia de los diplomas adicionales;
- cualquier prueba útil que justifique la formación continua, la experiencia y las habilidades adquiridas durante el ejercicio profesional en un Estado de la UE o del EEE, o en un tercer estado (certificados de funciones, informe de actividad, evaluación operativa, etc. ) ;
- en el contexto de funciones desempeñadas en un Estado distinto de Francia, una declaración de la autoridad competente de dicho Estado, de menos de un año de edad, que acredite la ausencia de sanciones contra el solicitante.

Dependiendo de la situación del solicitante, se requiere documentación adicional de apoyo. Para obtener más información, consulte el sitio web oficial de la[Gnc](http://www.cng.sante.fr/).

**Qué saber**

Los documentos de apoyo deben estar escritos en francés o traducidos por un traductor certificado.

**hora**

El NMC confirma la recepción de la solicitud en el plazo de un mes a partir de la recepción.

El silencio guardado durante un cierto período de tiempo a partir de la recepción del expediente completo merece la decisión de desestimar la solicitud. Este retraso se incrementa a:

- cuatro meses para las solicitudes de nacionales de la UE o del EEE con un título de uno de estos Estados;
- seis meses para las solicitudes de terceros nacionales con un diploma de un Estado de la UE o del EEE;
- un año para otras aplicaciones.

Este plazo podrá prorrogarse por dos meses, mediante decisión de la autoridad ministerial notificada a más tardar un mes antes de la expiración de esta última, en caso de dificultad grave para evaluar la experiencia profesional del candidato.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la formación y la experiencia laboral del nacional y las necesarias para ejercer en Francia, el NMC podrá decidir:

- Sugerir que el solicitante elija entre un curso de ajuste o una prueba de aptitud;
- imponer un curso de ajuste o una prueba de aptitud;
- para imponer un curso de ajuste y una prueba de aptitud.

**La prueba de aptitud** tiene por objeto verificar, mediante pruebas escritas u orales o ejercicios prácticos, la capacidad del solicitante para ejercer como farmacéutico en la especialidad de que se trate. Se ocupa de temas que no están cubiertos por la formación o experiencia del solicitante.

**El curso de adaptación** tiene por objeto permitir a las partes interesadas adquirir las competencias necesarias para ejercer la profesión de farmacéutico. Se lleva a cabo bajo la responsabilidad de un farmacéutico y puede ir acompañado de formación teórica adicional opcional. La duración de la pasantía no exceda de tres años. Se puede hacer a tiempo parcial.

*Para ir más allá* : decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización competentes para el examen de las solicitudes presentadas para el ejercicio en Francia de las profesiones de médico, cirujano dental, partera y Farmacéutico.

#### Solicitar inclusión en el consejo de la Orden

La inscripción en el consejo de la Orden es obligatoria para ejercer legalmente la actividad de farmacéutico en Francia.

El registro no se aplica:

- inspectores de salud pública de farmacéuticos, inspectores de organismos regionales de salud, inspectores de la Agencia Nacional para la Seguridad de los Medicamentos y Productos Sanitarios;
- farmacéuticos que son funcionarios públicos o el ministerio de salud similar;
- farmacéuticos que son funcionarios o asimilados por el Ministerio de Educación Superior, que no se dedican de otro modo a actividades farmacéuticas;
- farmacéuticos pertenecientes al marco activo del ejército, el servicio de salud marítima y aérea.

**Tenga en cuenta que**

El registro en el consejo de la Orden permite la emisión automática y gratuita de la Tarjeta Profesional de La Salud (CPS). El CPS es un documento electrónico de identidad comercial. Está protegido por un código confidencial y contiene, entre otras cosas, datos de identificación farmacéutico (identidad, ocupación, especialidad). Para obtener más información, se recomienda consultar el sitio web del gobierno de la Agencia Francesa de Salud Digital.

**Autoridad competente**

La sección R. 4222-1 del Código de Salud Pública dispone que la solicitud de inscripción se dirige, según sea el caso, al presidente del consejo regional de la región en la que el farmacéutico desea ejercer, o al presidente del consejo central de la sección.

**Documentos de apoyo**

El solicitante debe presentar, por cualquier medio, una solicitud completa de registro que incluya:

- dos copias del cuestionario estandarizado con una identificación con foto completa, fechada y firmada, disponible en los consejos departamentales del Colegio o directamente descargable desde el sitio web oficial del Consejo Nacional del Colegio de Farmacéuticos;
- Una fotocopia de un documento de identidad válido o, en su caso, un certificado de nacionalidad expedido por una autoridad competente;
- Si es así, una fotocopia de la tarjeta de residencia familiar de un ciudadano de la UE válida, la tarjeta válida de residente-CE de larga duración o la tarjeta de residente con estatus de refugiado válido;
- En caso afirmativo, una fotocopia de la tarjeta de visita europea válida;
- una copia, acompañada si es necesario por una traducción de un traductor certificado, de los cursos de formación a los que se adjuntan:- cuando el solicitante sea nacional de la UE o del EEE, el certificado o certificado sinvisado (véase más arriba "2. a. Requisitos nacionales"),
  - solicitante recibe un permiso de ejercicio individual (véase supra "2. c. Nacionales de la UE y del EEE: para un ejercicio permanente"), copiando esta autorización,
  - Cuando el solicitante presente un diploma expedido en un Estado extranjero cuya validez se reconozca en territorio francés, la copia de los títulos a los que pueda subordinarse dicho reconocimiento;
- nacionales de un Estado extranjero, un extracto de antecedentes penales o un documento equivalente de menos de tres meses de edad, expedido por una autoridad competente del Estado de origen. Esta parte puede sustituirse, para los nacionales de la UE o del EEE que requieran prueba de moralidad o honorabilidad para el acceso a la actividad médica, por un certificado, de menos de tres meses de edad, de la autoridad competente del Estado. certificando que se cumplen estas condiciones morales o de honor;
- una declaración sobre el honor del solicitante que certifique que ningún procedimiento que pudiera dar lugar a una condena o sanción que pudiera afectar a la lista en la junta está en su contra;
- un certificado de baja de la lista, registro o registro expedido por la autoridad con la que el solicitante fue previamente registrado o registrado o, en su defecto, una declaración de honor del solicitante que certifique que nunca fue registrado o registrado o, en su defecto, un certificado de registro o registro en un Estado de la UE o del EEE;
- todas las pruebas de que el solicitante tiene las habilidades de idiomas necesarias para ejercer la profesión;
- un currículum.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**hora**

El Consejo Regional o Central decide sobre la solicitud de registro en un plazo de tres meses a partir de la recepción del expediente completo de solicitud. Si no se responde a una respuesta dentro de este plazo, la solicitud de registro se considera rechazada.

Este período se incrementa a seis meses para los nacionales de terceros países cuando se llevará a cabo una investigación fuera de la Francia metropolitana. A continuación, se notifica al interesado.

Si se concede el registro, la decisión adoptada por la junta se notifica al interesado por carta recomendada, acompañada de su certificado de registro.

La decisión del consejo regional también está sujeta a recurso, en un plazo de 30 días, ante el Consejo Nacional del Colegio de Farmacéuticos. La propia decisión puede ser apelada ante el Consejo de Estado.

**Costo**

La inscripción en el consejo de la Universidad es gratuita, pero crea la obligación de pagar las cuotas ordinales obligatorias, cuyo importe se establece anualmente y que debe pagarse en el primer trimestre del año calendario en curso.

*Para ir más allá*: Artículos L. 4222-1 en L. 4222-8, y R. 4222-1 en R. 4222-4-3 del Código de Salud Pública.

### c. Tarjeta Profesional Europea (CPE)

La tarjeta profesional europea es un procedimiento electrónico para las cualificaciones profesionales de 500 miembros en otro Estado de la UE.

El procedimiento CPE puede utilizarse cuando el nacional desee operar en otro Estado de la UE de forma temporal y ocasional, o de forma permanente.

**Procedimiento**

Un nacional que desea establecerse en Francia o prestar servicios, se pone en contacto con la autoridad competente de su estado y le envía un expediente electrónicamente. A continuación, la autoridad competente enviará la solicitud a la Dirección Regional de Juventud, Deportes y Cohesión Social de Ile-de-France, que la enviará al Consejo Nacional de la Orden de farmacéuticos.

**Documentos de apoyo**

La solicitud CPE debe ir acompañada de todos los siguientes documentos justificativos:

- Una fotocopia legible de una pieza de identificación válida
- una declaración del Consejo Nacional del Colegio de Farmacéuticos, que acredite la lista de la orden y la ausencia de suspensión o prohibición de la práctica;
- En el caso del reconocimiento automático, una copia de los títulos de formación y, en su caso, un certificado de conformidad, un certificado de cambio de nombre o un certificado de derechos adquiridos;
- en caso de reconocimiento de cualificaciones profesionales, copia de los títulos de formación, documentos útiles para proporcionar información sobre la formación realizada, así como cualquier documento que justifique las cualificaciones requeridas;
- en el caso del reconocimiento de un título expedido por un tercer Estado, un certificado que certifique tres años de experiencia en Francia;
- cualquier documento que justifique la experiencia laboral de al menos un año en los últimos diez años, cuando el Estado no regula la formación o el ejercicio de la profesión;
- un certificado de responsabilidad civil profesional.

Además, el solicitante podrá adjuntar cualquier prueba de su conocimiento de la lengua francesa.

**Resultado del procedimiento**

Después de recibir los documentos justificativos, el Consejo de la Orden podrá decidir:

- Para entregar el CPE;
- negarse a emitir el CPE por decisión razonada y propensa a apelar;
- someter al profesional a una medida de compensación en caso de diferencia sustancial con la formación necesaria para ejercer la profesión en Francia

*Para ir más allá*: Artículos R. 4222-9 a R. 4222-11 del Código de Salud Pública; 8 de diciembre de 2017 sobre la aplicación de la tarjeta profesional europea mencionada en el artículo L. 4002-2 del Código de Salud Pública.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

