﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP237" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Radiofísico" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="radiofisico" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/radiofisico.html" -->
<!-- var(last-update)="2020-04-15 17:22:08" -->
<!-- var(url-name)="radiofisico" -->
<!-- var(translation)="Auto" -->


Radiofísico
===========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:08<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El radiofísico o físico médico es un profesional de la salud que trabaja dentro de un equipo multidisciplinar iobizado cuya actividad es implementar sus conocimientos de radiación en el campo médico. En particular, es responsable de la calidad de las imágenes, la dosimetría (estudio cuantitativo de la radiación) y asegura la optimización del uso de equipos de radiación con fines diagnósticos o terapéuticos del paciente.

El radiofísico aporta su experiencia en:

- radioterapia;
- Medicina nuclear;
- imágenes médicas.

**Tenga en cuenta que**

La práctica ilegal de la profesión se castiga con dos años de prisión y una multa de 30.000 euros

*Para ir más allá*: Artículos L. 4251-1 y L. 4252-2 del Código de Salud Pública (Código de Salud Pública).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de radiofísico, el profesional debe:

- poseer uno de los siguientes diplomas o títulos de formación:- un grado en física radiológica y médica,
  - un título de físico médico,
  - acreditación como radiofísico emitida antes del 28 de noviembre de 2004;
- para registrarlo.

**Tenga en cuenta que**

Este procedimiento de registro es gratuito y debe llevarse a cabo antes del inicio de la profesión de radiofísico con el organismo pertinente.

*Para ir más allá*: ArtículoL. 4251-2 y L. 4251-3 del Código de Salud Pública.

#### Entrenamiento

**Títulos honoríficos previos a los requis**

Para ser reconocida como una persona profesionalmente calificada, la persona debe tener un título de maestría (B.A.5) en física radiológica o físico médico. Los arreglos de acceso y la formación imprevistas son específicos de las universidades que emiten estos diplomas.

Es aconsejable acercarse a las instituciones interesadas para obtener más información.

**Diplomado en Física Radiológica y Médica (DQPRM)**

Para especializarse, el profesional debe realizar formación en radioterapia, braquiterapia, radiología y medicina nuclear y protección contra la radiación de los pacientes.

Esta formación está disponible por competición. Pueden solicitarcandidatos con un máster (B.A. 5) o un nivel equivalente en los campos de la física radiológica y médica, admitidos como requisito previo para la inscripción en los exámenes de ingreso.

Para inscribirse en las pruebas de selección de formación especializada, el diploma del candidato debe cumplir con los requisitos de contenido establecidos en el Orden ii del 6 de diciembre de 2011 sobre la formación y las misiones de la persona radiofísica médica y el reconocimiento de las cualificaciones profesionales de los extranjeros para el desempeño de estas misiones en Francia.

La formación especializada es por un mínimo de un año e incluye:

- instrucción teórica de al menos 180 horas
- una pasantía en uno o más centros de salud o prácticas liberales bajo la responsabilidad de un profesional radiofísico. Esta pasantía se desglosa de la siguiente manera:- 36 semanas como mínimo en radioterapia;
  - 10 semanas en medicina nuclear;
  - Seis semanas en radiología.

Al final de la pasantía, el candidato debe someterse a pruebas de conocimiento ante un jurado profesional y, si tiene éxito, obtiene un DQPRM por parte de la organización que emitió la formación.

*Para ir más allá*: Artículos R. 1333-59 y siguientes del Código de Salud Pública; 6 de diciembre de 2011.

#### Costos asociados con la calificación

Se paga la formación para un título en física radiológica y médica o físico médico y el costo varía dependiendo de la institución que proporciona la capacitación. Para más información, es aconsejable acercarse al establecimiento en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Todo nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo sobre el Espacio Económico Europeo (EEE) legalmente establecido y que ejerza la actividad de los radiofísicos en ese Estado podrá ejercer en Francia de forma temporal e informal la misma actividad.

Cuando el Estado miembro no regule la formación o el ejercicio, el individuo deberá justificar haber ejercido como radiofísico durante al menos un año en los últimos diez años.

Una vez que el profesional cumple estas condiciones, debe hacer una declaración previa antes de su primera actuación en Francia (ver infra "4o. b. Predeclaración para un nacional de la UE para un ejercicio temporal e informal (LPS)").

**Tenga en cuenta que**

Cuando existan diferencias sustanciales entre la formación profesional y la experiencia del nacional y las necesarias para ejercer en Francia, el prefecto del departamento podrá decidir someterlo a una prueba de aptitud o a una pasantía. adaptación (véase infra "5.00). b. Bueno saber: medidas de compensación").

Además, el nacional debe justificar tener los conocimientos necesarios para ejercer la profesión de radiofísico en Francia.

*Para ir más allá*: Artículo L. 4251-6 del Código de Salud Pública.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Cualquier nacional de la UE legalmente establecido en un Estado miembro para trabajar como radiofólogo puede llevar a cabo la misma actividad en Francia, siempre y cuando posea:

- un certificado de formación expedido por uno o varios Estados miembros que regulan la actividad del radiofísico y le permiten llevar a cabo esta actividad legalmente;
- cuando el Estado no regula el acceso o el ejercicio de la profesión, un título que acredite que ha recibido formación en preparación para el ejercicio de dicha profesión y un documento que justifique que ha participado en esta actividad durante al menos un año en los últimos diez años Años
- un certificado de formación expedido por un tercer Estado y reconocido por un Estado miembro distinto de Francia que le permita ejercer legalmente su profesión. En caso afirmativo, el interesado debe haber justificado haber estado activo durante tres años en ese Estado miembro.

**Tenga en cuenta que**

En caso de diferencias sustanciales entre la formación del profesional y la necesaria para ejercer como radiofísico en Francia, el prefecto puede someterlo a una medida de compensación (véase infra "5o. b. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículo L. 4251-5 del Código de Salud Pública.

3°. Seguro
-------------------

Como profesional de la salud, el radiofísico debe, si ejerce a nivel liberal, tomar un seguro de responsabilidad civil profesional.

Si ejerce como empleado, corresponde al empleador obtener dicho seguro para sus empleados por los actos realizados durante esta actividad.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

4°. Procedimientos y formalidades de reconocimiento de cualificación
-----------------------------------------------------------------------------------------

### a. Predeclaración para un nacional de la UE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe solicitar antes de su primera entrega de servicio al prefecto del departamento de su elección.

**Documentos de apoyo**

La solicitud debe incluir:

- el[formulario de declaración](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=FDBFFCF534F0D849F090355DB5B55EE8.tplgfr25s_2?idArticle=LEGIARTI000024986947&cidTexte=LEGITEXT000024986922&dateTexte=20180313) Completado y firmado;
- toda la información relativa al estado civil, la nacionalidad y la legalidad del establecimiento del nacional en el Estado miembro de origen.

**hora**

En el plazo de un mes a partir de la recepción de la solicitud del nacional, el prefecto le informa:

- Que puede comenzar la prestación de servicios
- No puede comenzar la prestación de servicios;
- que debe ser indemnizado para demostrar que tiene los conocimientos necesarios para llevar a cabo su actividad en Francia.

Vale la pena rechazar la solicitud si el prefecto no responde más allá de un retraso de un mes.

**Resultado del procedimiento**

El prefecto regional registra al nacional en una lista en particular y le envía un recibo indicando su número de registro y especificando la agencia de seguro de salud correspondiente.

**Tenga en cuenta que**

La declaración previa permite al nacional ejercer en todo el territorio francés y debe renovarse cada año en las mismas condiciones.

*Para ir más allá*: Artículos 12 a 16 y Apéndice VII de la orden del 6 de diciembre de 2011 sobre la formación y misiones del especialista en radiofísica médica y el reconocimiento de las cualificaciones profesionales de los extranjeros para el ejercicio de estos misiones a Francia.

### b. Solicitud de autorización para ejercer para el nacional de la UE para un ejercicio permanente (LE)

**Autoridad competente**

El nacional deberá presentar una solicitud en dos copias por carta recomendada con notificación de recepción al prefecto del departamento en el que desee ejercer.

**Documentos de apoyo**

Su solicitud deberá contener los siguientes documentos, si los hubiere, con su traducción al francés:

- El formulario de autorización de ejercicio establecido en la Lista V de la orden del 6 de diciembre de 2011 cumplió y firmó;
- Una fotocopia de su dNI válido
- Una copia de su título de formación que permite el ejercicio de la actividad del radiofísico y, si es necesario, sus diplomas adicionales;
- cualquier documento que justifique su formación o experiencia profesional;
- una declaración de la autoridad competente de su Estado de establecimiento de menos de un año de edad, que certifique que no está sujeta a ninguna sanción;
- Una copia de los certificados en los que se detalle el nivel de formación y el contenido y la duración de las prácticas validadas por el solicitante;
- dependiendo del caso:- cuando el Estado no regule el acceso o el ejercicio de la profesión, cualquier prueba que justifique que ha ocupado el cargo de especialista en radiofísica médica durante al menos un año en los últimos diez años,
  - cuando el profesional ha adquirido su título de formación en un tercer Estado pero reconocido por un Estado miembro, el reconocimiento del título de formación por parte del Estado lo ha reconocido.

**Retrasos y resultados del procedimiento**

El prefecto reconoce la recepción de la solicitud en el plazo de un mes a partir de la recepción.

La falta de respuesta más allá de los cuatro meses es una razón para rechazar la solicitud. Una vez aceptada la solicitud, se le entrega una tarjeta sanitaria profesional y puede ejercer como radiofísico en las mismas condiciones que el nacional francés.

*Para ir más allá*: Artículos 6 a 11 del auto de 6 de diciembre de 2011 supra.

**Bueno saber: medidas de compensación**

Cuando el examen de las cualificaciones profesionales del nacional revela diferencias sustanciales entre su formación y la necesaria para llevar a cabo la actividad de radiofísico en Francia o siempre que la formación del profesional sea menos de un año menos que el del diploma francés, el prefecto de departamento podrá, previa notificación de una comisión, decidir someter al profesional a una prueba de aptitud o a un curso de adaptación.

La prueba de aptitud consiste en un examen escrito y oral para verificar el conocimiento del nacional. El curso de adaptación, por un máximo de tres años, debe llevarse a cabo con un radiofísico que haya estado practicando durante al menos tres años para prácticas en medicina nuclear y radiología y durante al menos cinco años para pasantías de radioterapia.

Las modalidades de organización de las medidas de compensación figuran en el anexo VI del citado decreto de 6 de diciembre de 2011.

*Para ir más allá*: Artículo L. 4251-5 del Código de Salud Pública.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París[sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html).

