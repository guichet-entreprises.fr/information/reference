﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP112" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Entrenador de artes marciales" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="entrenador-de-artes-marciales" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/entrenador-de-artes-marciales.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="entrenador-de-artes-marciales" -->
<!-- var(translation)="Auto" -->


Entrenador de artes marciales
=============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El entrenador de artes marciales es un profesional que proporciona enseñanza, entrenamiento, desarrollo y entrenamiento en una de las disciplinas de artes marciales para las que es graduado, a saber:

- aikido, aikibudo y disciplinas relacionadas;
- las artes energéticas de China;
- artes marciales chinas externas;
- artes marciales chinas internas;
- judo-jujitsu;
- Karate y disciplinas relacionadas;
- taekwondo y disciplinas relacionadas.

Acompaña tanto a principiantes como al público practicando la competición. El entrenador diseña programas de desarrollo deportivo y lleva a cabo actividades de entrenamiento. Garantiza la seguridad de los profesionales a los que acompaña, así como la de terceros.

Para más información: Artículos 2 de los decretos por los que se crean las diversas menciones del Diploma Estatal de Juventud, Educación Popular y Especialidad Deporte "Desarrollo Deportivo":

- ordenó a partir del 15 de abril de 2009 la expresión "aikido, aikibudo y disciplinas asociadas";
- detenido el 12 de julio de 2007 por referencias a las "artes energéticas chinas", las "artes marciales chinas externas" y las "artes marciales chinas internas";
- 18 de diciembre de 2008 para las referencias a "judo-jujitsu", "karate y disciplinas asociadas" y "taekwondo y disciplinas asociadas".

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La actividad de coaching está sujeta a la aplicación del Artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas, incluyendo el Diploma Estatal de Juventud, Educación Popular y Deporte (DEJEPS).

Como profesor de deportes, el entrenador deportivo debe poseer un diploma, un título profesional o un certificado de calificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos L. 212-1 y R. 212-84 del Código del Deporte.

#### Entrenamiento

El desarrollo deportivo de DeJEPS, independientemente de la mención considerada ("aikido, aikibudo y disciplinas conexas", "Artes energéticas chinas", "artes marciales chinas externas", etc.), se clasifica como Nivel III, es decir, nivel bac 2.

DEJEPS se prepara a través de la formación inicial, la educación continua o la validación de la experiencia (VAE). Para obtener más información, puede ver[sitio web oficial](http://www.vae.gouv.fr/) Vae.

###### Condiciones comunes de acceso a la formación que conducen a la obtención de una de las artes marciales DEJEPS

El interesado deberá:

- Rellene el formulario de registro estándar con un documento de identidad con fotografía
- Ser mayores de edad
- certificado de formación de primeros auxilios (tipo PSC1, AFPS);
- pasar las pruebas de acceso a la formación;
- comunicar un certificado médico de no contradictorio con la enseñanza y la práctica de la disciplina marcial en cuestión, con menos de tres meses de edad.

###### Condiciones adicionales específicas de JEPS que mencionan "aikido, aikibudo y disciplinas asociadas"

El interesado deberá:

- cumplir con los requisitos pedagógicos requeridos: ser capaz de evaluar los riesgos objetivos asociados con la práctica de la disciplina, anticipar los riesgos potenciales para el practicante, dominar el comportamiento y las acciones a realizar en caso de un incidente o e implementar una secuencia de mejora técnica. Estas habilidades se comprueban durante una sesión de actualización técnica de 30 minutos, seguida de una entrevista de 20 minutos;
- presentar un certificado, expedido por el Director Regional de Juventud, Deportes y Cohesión Social, que justifique una actividad docente de al menos 300 horas en las últimas cinco temporadas deportivas en aikido o aikibudo o disciplinas Asociados
- presentar un certificado, expedido por la Unión de Federaciones de Aikido, de aprobar una prueba técnica de 30 minutos de un segundo nivel dan, organizada por la Unión de Federaciones de Aikido;
- presentar un certificado, expedido por la Federación Francesa de Aikido, Aikibudo y Afinidades o por la Federación Francesa de Aikido y Budo, de éxito en una prueba pedagógica de 30 minutos consistente en realizar una sesión de aprendizaje, seguido de una entrevista de 20 minutos.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4, 6 y siguientes del auto de 15 de abril de 2009 mencionado anteriormente.

###### Condiciones adicionales específicas de jePS que mencionan las "artes energéticas chinas"

El interesado deberá:

- cumplir con los requisitos pedagógicos requeridos: poder garantizar la integridad física y moral del público, hacer cumplir las normas de seguridad en la animación de las sesiones y reaccionar adecuadamente en caso de incidente o accidente durante el Sesión. Estas habilidades se comprueban durante una sesión introductoria de 30 minutos seguida de una entrevista de 20 minutos;
- presentar un certificado, expedido por la Federación Francesa de Wushu, Energía China y Artes Marciales, de pasar una prueba de dominio técnico correspondiente al tercer nivel en artes marciales chinas externas;
- elaborar un certificado, expedido por el responsable de la estructura o estructuras en las que se llevó a cabo la actividad de animación y coaching, justificando una experiencia de animación grupal en un arte energético chino.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4, 6 y siguientes del auto de 12 de julio de 2007 mencionado anteriormente.

###### Otras condiciones específicas de JEPS mencionan "artes marciales chinas externas"

El interesado deberá:

- cumplir con los requisitos pedagógicos requeridos: poder garantizar la integridad física y moral del público, hacer cumplir las normas de seguridad en la animación de las sesiones y reaccionar adecuadamente en caso de incidente o accidente durante el Sesión. Estas habilidades se comprueban durante una sesión introductoria de 30 minutos, seguida de una entrevista de 20 minutos;
- presentar un certificado, expedido por la Federación Francesa de Wushu, Energía China y Artes Marciales, de pasar una prueba mínima de dominio técnico en artes marciales chinas externas correspondiente al tercer nivel técnico de la Disciplina;
- presentar un certificado, expedido por el responsable de la estructura o estructuras en las que se llevó a cabo la actividad de animación, justificando una experiencia de animación grupal en un arte marcial chino externo.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4, 6 y siguientes del auto de 12 de julio de 2007 mencionado anteriormente.

###### Condiciones adicionales específicas de deJEPS que mencionan las "artes marciales chinas internas"

El interesado deberá:

- cumplir con los requisitos pedagógicos requeridos: poder garantizar la integridad física y moral del público, hacer cumplir las normas de seguridad en la animación de las sesiones y reaccionar adecuadamente en caso de incidente o accidente durante el Sesión. Estas habilidades se comprueban durante una sesión introductoria de 30 minutos, seguida de una entrevista de 20 minutos;
- presentar un certificado, expedido por la Federación Francesa de Wushu, Energía China y Artes Marciales, de pasar una prueba mínima de dominio técnico en artes marciales chinas externas correspondiente al tercer nivel técnico de la Disciplina;
- presentar un certificado, expedido por el responsable de la estructura o estructuras en las que se llevó a cabo la actividad de animación, justificando una experiencia de animación grupal en un arte marcial chino interno.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4, 6 y siguientes del auto de 12 de julio de 2007 mencionado anteriormente.

###### Condiciones adicionales específicas de JEPS que mencionan "judo-jujitsu"

El interesado deberá:

- presentar un certificado, expedido por el director técnico nacional de judo-jujitsu, desficiando los requisitos pedagógicos requeridos: poder evaluar los riesgos objetivos asociados con la práctica de la disciplina, para anticipar los riesgos dominar el comportamiento y las acciones a realizar en caso de incidente o accidente e implementar una secuencia de desarrollo técnico. Estas habilidades se comprueban durante una sesión de entrenamiento técnico de 30 minutos, seguida de una entrevista de 20 minutos;
- presentar un certificado, expedido por el Director Técnico Nacional de Judo, Jujitsu, Kendo y Disciplinas Asociadas, de una actividad de coaching de al menos 300 horas en la disciplina durante los últimos tres años;
- certificados de producción, expedidos por el Director Técnico Nacional de Judo, Jujitsu, Kendo y Disciplinas Conexas, desde el éxito hasta:- una prueba técnica de 30 minutos de un segundo nivel dan,
  - una prueba pedagógica de 30 minutos consistente en realizar una sesión de actualización técnica, seguida de una entrevista de 20 minutos.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4, 6 y siguientes del auto de 18 de diciembre de 2008 mencionado anteriormente.

###### Otras condiciones específicas de JEPS mencionan "karate y disciplinas asociadas"

El interesado deberá:

- elaborar un certificado, expedido por el Director Técnico Nacional de Karate y disciplinas asociadas, desfidando los requisitos pedagógicos requeridos: poder evaluar los riesgos objetivos asociados con la práctica de la disciplina, anticipar riesgos potenciales para el practicante, dominar el comportamiento y las acciones a realizar en caso de un incidente o accidente e implementar una secuencia de desarrollo técnico. Estas habilidades se comprueban durante una sesión de actualización técnica de 30 minutos, seguida de una entrevista de 20 minutos;
- presentar un certificado, expedido por el Director Técnico Nacional de Karate y Disciplinas Conexas, de la práctica del karate (o en una disciplina asociada);
- presentar un certificado, expedido por el Director Técnico Nacional de Karate y disciplinas conexas, de éxito para:- una prueba técnica de una hora que incluya demostraciones técnicas de un segundo nivel dan en la disciplina de karate u otra disciplina asociada, la elección del candidato,
  - una prueba educativa que incluye la supervisión de una sesión introductoria en la disciplina de karate o en una disciplina asociada, la elección del candidato, seguida de una entrevista para verificar las habilidades del candidato para enseñar al público.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4 y 6 del auto de 18 de diciembre de 2008 antes mencionado.

###### Las condiciones adicionales específicas de JEPS mencionan "taekwondo y disciplinas asociadas"

El interesado deberá:

- cumplir con los requisitos pedagógicos requeridos: ser capaz de evaluar los riesgos objetivos asociados con la práctica de la disciplina, anticipar los riesgos potenciales para el practicante, dominar el comportamiento y las acciones a realizar en caso de un incidente o e implementar una sesión introductoria. Estas habilidades se comprueban durante la creación de una sesión introductoria de 30 minutos, seguida de una entrevista de 20 minutos;
- presentar un certificado, expedido por el Director Técnico Nacional de Taekwondo y disciplinas conexas, de pasar una prueba técnica de demostración técnica de 30 minutos de un dan de primer nivel;
- presentar un certificado, expedido por el Director Técnico Nacional de Taekwondo y disciplinas conexas, de mantenimiento de 30 minutos consistente en que el candidato presente una revisión de las actividades y un proyecto profesional en taekwondo o en disciplina asociada.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4, 6 y siguientes del auto de 18 de diciembre de 2008 mencionado anteriormente.

**Es bueno saber**

Las personas con el Diploma Estatal Senior de Educación Juvenil, Educación Popular y Deporte (DESJEPS) rendimiento deportivo especializado también pueden practicar como entrenador deportivo. Este diploma estatal de Nivel II es emitido por el Director Regional de Juventud y Deportes. Para practicar en el campo de las artes marciales, el entrenador optará por una de las siguientes menciones del DESJEPS: "aikido, aikibudo y disciplinas relacionadas", "judo-jujitsu", "karate y disciplinas asociadas" o "taekwondo y disciplinas asociadas".[Más información](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/des-jeps/Reglementation-11081/La-specialite-performance-sportive-du-DES-JEPS-et-les-mentions-unites-capitalisables-complementaire-et-certificats-de-specialisation-s-y-rapportant/) sobre los requisitos de admisión y la formación previa a la graduación.

*Para ir más allá*: Artículos D. 212-35 y siguientes del Código del Deporte, ordenado el 20 de noviembre de 2006 por el que se organiza el Diploma Estatal de Juventud, Educación Popular y Especialidad Deporte "Desarrollo Deportivo" emitido por el Ministerio de Juventud y Juventud deportes y todos los decretos antes mencionados creando las diversas referencias a las artes marciales.

#### Costos asociados con la calificación

La formación en el desarrollo deportivo dejePS, independientemente de la mención elegida, se paga (coste variable dependiendo de las menciones). Para [más detalles](https://foromes.calendrier.sports.gouv.fr/#/formation), es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como entrenador de artes marciales en Francia para personas que han sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Requisito de notificación (con el fin de obtener la tarjeta de educador deportivo profesional)
-----------------------------------------------------------------------------------------------------------------------

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el [sitio web oficial](https://eaps.sports.gouv.fr).

#### hora

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

#### Documentos de apoyo

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

5°. Proceso de cualificaciones y formalidades
-------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

#### Autoridad competente

La declaración previa de actividad debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde el Departamento en el que declarante quiere realizar su actuación.

#### hora

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

#### Documentos de apoyo

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

#### hora

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

#### Documentos de apoyo para la primera declaración de actividad

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia,
  - documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

#### Evidencia para una declaración de renovación de la actividad

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

**Bueno saber: medidas de compensación**

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

