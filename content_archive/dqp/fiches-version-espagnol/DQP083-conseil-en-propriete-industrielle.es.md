﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP083" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Asesoramiento en propiedad industrial" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="asesoramiento-en-propiedad-industrial" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/asesoramiento-en-propiedad-industrial.html" -->
<!-- var(last-update)="2020-04-15 17:21:05" -->
<!-- var(url-name)="asesoramiento-en-propiedad-industrial" -->
<!-- var(translation)="Auto" -->


Asesoramiento en propiedad industrial
=====================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:05<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La consultoría de propiedad industrial es un profesional que se encarga de asesorar, asistir o representar a los clientes en la obtención, mantenimiento, explotación o defensa de los derechos de propiedad industrial, derechos y derechos auxiliares. cuestiones relacionadas.

El Consejo de Propiedad Industrial también es responsable de proporcionar consultas jurídicas, en particular sobre propiedad industrial, y de redactar los proyectos de base bajo semilla privada.

También lleva a cabo la misión de representar a sus clientes en el procedimiento ante la[Instituto Nacional de propiedad industrial](https://www.inpi.fr/fr) (Inpi).

*Para ir más allá*: Artículos L. 422-1 y L. 422-4 del Código de Propiedad Intelectual.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El título de consultoría de propiedad industrial está reservado a los profesionales en una lista de consejos de propiedad industrial, en poder del Director General de Inpi.

**Tenga en cuenta que**

La usurpación del título de consejo de propiedad industrial o un título que pueda ser confuso, se castiga con un año de prisión y una multa de 15.000 euros.

*Para ir más allá*: Artículo L. 422-1 del Código de Propiedad Intelectual y artículo 433-17 del Código Penal.

#### Condiciones de acceso a la profesión

Para acceder a la profesión de consultora de propiedad industrial en Francia, el interesado debe justificar:

- un posgrado en derecho, científico o técnico, o un título reconocido como equivalente;
- un diploma emitido por el[Centro de Estudios Internacionales de Propiedad Industrial](http://www.ceipi.edu/) (Ceipi) de la Universidad de Estrasburgo o un título reconocido como equivalente;
- una práctica profesional de al menos 3 años
- aprobar un examen de aptitud o validar una experiencia.

**Tenga en cuenta que**

El Ceipi ofrece dos cursos diferentes: uno sobre patentes de invención y que conduce al diploma de estudios internacionales de propiedad industrial en "Patentes de Invención", y el otro sobre las marcas, diseños que conducen a la estudios internacionales de propiedad en "Marcas, Diseños y Modelos". Para obtener más información, infórmese directamente de la[Ceipi](http://www.ceipi.edu/).

#### Examen de aptitud

El examen de aptitud para el idioma francés organizado por Inpi incluye pruebas orales y de elegibilidad por escrito. La revisión debe determinar si tiene los conocimientos teóricos y prácticos para llevar a cabo las misiones de asistencia, asesoramiento y representación en propiedad industrial.

**Procedimiento**

El registro para la prueba de aptitud es una solicitud en forma de carta fechada y firmada por el solicitante, incluyendo su dirección personal y el área técnica elegida para la prueba oral.

**Documentos de apoyo**

La carta de solicitud de inscripción para el examen debe ir acompañada de:

- Una copia de la identificación válida del candidato
- Una copia del título de posgrado jurídico, científico o técnico o un título reconocido como equivalente;
- una copia del diploma expedido por el Centro de Estudios Internacionales de Propiedad Industrial (Ceipi) de la Universidad de Estrasburgo o un título reconocido como equivalente;
- uno o más certificados que acrediten un mínimo de tres años de práctica profesional, describiendo su duración y las obligaciones desempeñadas por el candidato.

La experiencia profesional debe haberse llevado a cabo bajo la responsabilidad de una persona cualificada en propiedad industrial.

**Costo**

El candidato tendrá que pagar una tasa fijada en 200 euros con el contable del Inpi.

**Resultado del procedimiento**

Para validar y obtener la prueba de aptitud, el individuo debe haber obtenido un promedio de 10 sobre 20 en todas las pruebas.

**Validar la experiencia**

Si este es el caso, el solicitante estará exento de una prueba de aptitud si tiene un mínimo de ocho años de experiencia profesional bajo la responsabilidad de una persona cualificada en propiedad industrial.

Tendrá que hacer una solicitud expresa al Director General de Inpi y deberá dar una entrevista oral a un jurado, después de considerar su solicitud.

**Tenga en cuenta que**

El jurado incluye a un magistrado de la orden judicial, un profesor universitario que enseña derecho privado, un abogado, dos personas competentes de propiedad industrial y cuatro personas en la lista de personas cualificadas en propiedad dos consejos de propiedad industrial.

*Para ir más allá*: Artículos R. 421-1 y R. 421-6, relativos a los miembros del jurado en la prueba de aptitud, R. 422-1-1, R. 422-1-2 y R. 421-2 del Código de Propiedad Intelectual.

**Costos asociados con la calificación******

Se paga la formación para prepararse para la graduación que conduce a la profesión de consultoría de propiedad industrial. Su costo varía dependiendo de las universidades que proporcionan las enseñanzas. Para más detalles, es aconsejable acercarse a las universidades consideradas.

### b. Nacionales de la UE: para el ejercicio temporal o casual (LPS)

Un nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), que se dedica a actividades de asesoramiento en materia de propiedad industrial en uno de estos Estados, puede utilizar su título profesional para representar a las personas Inpi, siempre y cuando su título sea acreditado por la autoridad competente del Estado en el que esté legalmente establecido.

Cuando el ejercicio de la profesión no esté sujeto a la posesión de un título regulado en el Estado del Establecimiento, el interesado sólo podrá representar a personas ante el Inpi siempre que justifique:

- o bien una práctica de la profesión de consultoría de propiedad industrial en uno o varios Estados miembros de la UE o parte en el EEE, durante al menos un año en los últimos diez años, a tiempo completo o a tiempo parcial. La justificación de esta práctica se puede hacer por cualquier medio;
- o certificar la formación regulada en su estado de establecimiento y dar acceso a la profesión.

En cualquier caso, el nacional que participe en la representación de personas ante el Inpi, estará obligado a respetar las normas profesionales aplicables a la profesión de consultora de propiedad industrial en Francia, y en particular a todos éticas (véase infra "3.3. Condiciones de honorabilidad y reglas éticas").

En caso de incumplimiento de sus obligaciones, se aplicarán las sanciones previstas en el artículo L. 422-10 (véase infra "3. a. Condiciones de honorabilidad").

*Para ir más allá*: Artículos R. 422-7-1 y R. 422-7-2 del Código de Propiedad Intelectual.

### c. Nacionales de la UE: para un ejercicio permanente (LP)

Un nacional de un Estado de la UE o del EEE que desee ejercer de forma permanente en la profesión de consultoría de propiedad industrial en Francia podrá solicitar el registro en la lista de personas habilitadas en propiedad industrial (véase infra "5o. Proceso de calificación y formalidades").

Como tal, el titular de una designación de asesoramiento en materia de propiedad industrial expedida en uno de estos Estados puede hacer valer sus cualificaciones profesionales y quedar exento de las condiciones de acceso a la profesión antes mencionadas (véase supra "2". b. Condiciones de acceso a la profesión").

En este caso, el interesado debe haber completado con éxito un ciclo de estudios y justificar:

- ya sea un diploma, certificado u otro título que permita el ejercicio de la profesión en un Estado de la UE o del EEE expedido:- por la autoridad competente de ese Estado,
  - o por una autoridad de un tercer país, acompañada de un certificado del Estado de la UE o del EEE que certifique que el titular ha adquirido al menos tres años de experiencia profesional en su territorio, a tiempo completo o a tiempo parcial;
- a tiempo completo o a tiempo parcial, durante un año en los diez años anteriores, en un Estado de la UE o del EEE que no regula el acceso o la práctica. El individuo deberá presentar todos los certificados de competencia o comprobante de las credenciales de formación que se preparen para el ejercicio de la profesión.

**Tenga en cuenta que**

Si el solicitante certifica la formación regulada, no se requiere experiencia laboral.

En caso de diferencias sustanciales entre las cualificaciones profesionales adquiridas y las exigidas en Francia, el nacional podrá estar sujeto a medidas de compensación (véase más adelante: "Bueno saber: medida de compensación") previstas en el artículo R. 421-8 del Código de Propiedad Intelectual.

*Para ir más allá*: Artículo R. 421-7 del Código de Propiedad Intelectual.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Todos los deberes impuestos a los consejos de propiedad industrial se aplican a los nacionales que deseen ejercer la profesión en Francia.

### a. Condiciones de honorabilidad

Para ejercer, el Consejo de la Propiedad Industrial no tiene que:

- Ofender las normas relativas a la condición de la práctica de los abogados de propiedad industrial;
- han llevado a cabo actos contrarios a la probidad, el honor o la delicadeza.

De conformidad con el artículo L. 422-10 del Código de Propiedad Intelectual, podrán adoptar medidas disciplinarias[Empresa Nacional de Consultoría en Propiedad Industrial](https://www.cncpi.fr/) (CNCPI), contra el profesional que no cumpla con las disposiciones establecidas, a saber:

- Una advertencia
- Culpar
- amortización temporal o permanente. Nota: esta medida disciplinaria se sustituye por una prohibición temporal o permanente de la práctica para la UE o el nacional del EEE que desee ejercer en Francia.

*Para ir más allá*: Artículos L. 422-10 y R. 422-7-2 del Código de Propiedad Intelectual.

### b. La ética

El consejo de propiedad industrial debe respetar, entre otras cosas, los principios de independencia, confidencialidad absoluta de la correspondencia, la preservación de los riesgos de conflicto de intereses y el secreto profesional.

*Para ir más allá*: Artículos L. 422-11, R. 422-7-2, R. 422-52 y R. 422-54 del Código de Propiedad Intelectual.

### c. Incompatibilidad del ejercicio

La profesión de consultor a la propiedad industrial es incompatible con:

- cualquier actividad comercial
- La condición de socio o gerente de la empresa, a menos que la empresa sea para el asesoramiento en propiedad industrial;
- miembro del consejo de supervisión o director de una corporación comercial, a menos que el consejo de propiedad industrial tenga más de siete años de práctica profesional y haya obtenido una exención por adelantado en las condiciones establecidas por decreto en el Consejo de Estado.

Por otro lado, la profesión de consultoría de propiedad industrial es compatible con las funciones docentes, así como con las de árbitro, mediador, conciliador o experto judicial.

*Para ir más allá*: Artículos L. 422-12 y L. 422-13 del Código de Propiedad Intelectual.

### d. Obligación continua de formación profesional

Los consejos de propiedad industrial deben participar en la formación profesional continua en virtud de la Sección L. 422-10-1 del Código de Propiedad Intelectual. El objetivo de esta formación es garantizar que los conocimientos necesarios para ejercer la profesión se actualicen y desarrollen.

El requisito de educación continua se cumple si el consejo de propiedad industrial justifica una de las siguientes acciones:

- participación en la formación, legal, económica o profesional;
- participación en la formación impartida por los consejos de propiedad industrial o por personas físicas o jurídicas establecidas en el territorio de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el Acuerdo sobre el Espacio Económico Europeo ( EEE) y facultado para representar a personas ante el Departamento Central de Propiedad Industrial de ese Estado;
- asistencia a seminarios o conferencias legales o económicas relacionadas con la actividad profesional de consultoría en propiedad industrial;
- impartiendo enseñanzas relacionadas con la actividad profesional de consultoría en propiedad industrial en un entorno universitario o profesional;
- la publicación de trabajos jurídicos o económicos relacionados con la actividad profesional de consultoría en propiedad industrial.

Cualquier educación continua seguida por un nacional de un Estado de la UE o del EEE en uno de estos estados se considera que cumple con esta obligación.

**Duración**

La duración de la educación continua es de veinte horas en un año natural o cuarenta horas en dos años consecutivos. Durante los dos primeros años de práctica profesional, esta formación incluye al menos diez horas de gestión de la empresa, ética y estatus profesional.

**Procedimiento**

Los consejos de propiedad industrial informan, a más del 31 de enero de cada año, a la CNCPI las medidas adoptadas para cumplir con sus obligaciones de educación continua en el último año natural o, en su caso, los últimos dos años.

La documentación justificativa para verificar el cumplimiento de esta obligación se adjunta a esta declaración.

El CNCPI supervisa el cumplimiento efectivo de la obligación de formación profesional continua de los consejos de propiedad industrial y verifica el cumplimiento de las formaciones seguidas y las acciones llevadas a cabo, en particular su relación con la actividad de asesoramiento en propiedad industrial.

*Para ir más allá* : Artículos L. 422-10-1 y R. 422-55-1 del Código de Propiedad Intelectual

4°. Seguros y garantía
-------------------------------------------

Cualquier abogado de propiedad industrial debe justificar un seguro de responsabilidad profesional que garantice la negligencia y las faltas que pueda cometer a sus clientes en el desempeño de sus funciones. Además de la compra de un seguro de responsabilidad profesional, el consejo de propiedad industrial debe tener una garantía específicamente destinada a la devolución de los fondos, efectos o valores recibidos.

Los documentos de seguro y garantía deben ser enviados al Inpi cada año.

*Para ir más allá*: Artículo L. 422-8 del Código de Propiedad Intelectual.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### Si es necesario, solicite la inclusión en la lista de personas cualificadas en la propiedad industrial

Los nacionales de la UE o del EEE que deseen ejercer como consultorías de propiedad industrial están obligados a estar inscritos en una lista de personas cualificadas en propiedad industrial para el desempeño de su actividad.

El registro está sujeto a una obligación de buen carácter y respeto a las condiciones del diploma y la práctica profesional. Especifica la especialización de la consultoría en propiedad industrial de acuerdo con los diplomas que obtuvo y la práctica profesional que adquirió.

**Autoridad competente**

El Director de Inpi, previa asesoración de la CNCPI, elabora una lista anual de personas cualificadas en propiedad industrial.

**Documentos de apoyo**

En apoyo de su solicitud de registro, el nacional debe presentar la prueba necesaria de los diplomas y prácticas profesionales especificados anteriormente en el párrafo "2." b. Nacionales de la UE: para un ejercicio permanente (LE)."

**Qué saber**

El nacional que desee estar en la lista no debe:

- han sido objeto de una condena penal;
- estar sujetos a una sanción disciplinaria o administrativa de despido, supresión, despido, retirada de la acreditación o autorización, por actos similares;
- estar sujeto a bancarrota personal o una sanción similar.

En caso afirmativo, el Director General de Inpi podrá decidir retirarlo de la lista, por decisión motivada, siempre que el interesado haya podido formular sus observaciones de antemano.

**Tenga en cuenta que**

Para obtener este registro en la lista, el nacional puede estar obligado a realizar una medida de compensación (véase a continuación "Bueno saber: medida de compensación) en forma de una prueba de aptitud en francés, si resulta que las calificaciones o la experiencia profesional que utiliza es sustancialmente diferente de las requeridas para el ejercicio de la profesión en Francia.

**Resultado del procedimiento**

El Director de Inpi decidirá sobre la solicitud de inscripción después de que el jurado decida dentro de los cuatro meses siguientes a su presentación.

Sin embargo, en caso de incumplimiento de la solicitud, se solicitará al solicitante que la regularice o impugne las objeciones formuladas por inpi, que tendrán por efecto suspender este período.

El silencio guardado por el Director General de Inpi al final del período de cuatro meses merece la pena aceptar la solicitud.

*Para ir más allá*: Artículos L. 422-5 y R. 421-7 y el siguiente código de propiedad intelectual.

**Bueno saber: medida de compensación**

Tras examinar los documentos justificativos, el Director General de Inpi, encargado de establecer el registro en la lista de p.i. calificado, indica al nacional el nivel de cualificación profesional requerido para ejercer en Francia y las diferencias sustanciales que justifican el uso de una medida de compensación.

Esta medida toma la forma de una prueba de aptitud ante el jurado mencionado anteriormente en el párrafo "2." a. Requisitos nacionales."

La revisión, cuyos términos y programas se establecen por decreto conjunto de la Guardia del Sello, el Ministro de Justicia y el Ministro de Propiedad Industrial, se organiza en un plazo de seis meses a partir de la notificación de la Decisión del Director de Inpi para el interesado.

**Costo**

Gratis.

*Para ir más allá*: Artículo R.421-8 del Código de Propiedad Intelectual.

### Solicitar inclusión en la lista de consejos de propiedad industrial

Una vez en la lista de personas habilitadas en propiedad industrial, el nacional podrá solicitar el registro en la lista de consejos de propiedad industrial a que se refiere el artículo R. 422-1 del Código de Propiedad Industrial, en el Condición:

- ofrecer o comprometerse a ofrecer sus servicios en un plazo de tres meses, ya sea como individuo o como grupo, o como empleado de otra consultoría de propiedad industrial o con una empresa de consultoría de propiedad industrial;
- Ser nacional francés o nacional de un Estado de la UE o del EEE;
- Tener una casa o establecimiento en Francia
- para cumplir con las condiciones de seguro y garantía establecidas en el apartado 4. Seguro y garantía."

**Autoridad competente**

La solicitud de inclusión en la lista de consejos de propiedad industrial se dirige al Director General de Inpi.

**Procedimiento**

En un plazo de cuatro meses a partir de la recepción de la solicitud, el Director General de Inpi tomará una decisión razonada tomada tras el asesoramiento de la CNCPI. Tenga en cuenta que el aviso se considera escrito en caso de silencio guardado por el CNCPI dentro de un mes de su remisión.

En caso de incumplimiento de la solicitud, el plazo se suspenderá hasta que el solicitante regule o impugne las objeciones formuladas por inpi.

Si la notificación se notifica dentro de este plazo, la solicitud de registro se considera aceptada.

**Resultado del procedimiento**

Una vez aceptada la solicitud, el Director General de Inpi se registra indicando:

- El nombre del Consejo de Propiedad Industrial;
- el nombre de la empresa o el nombre de la empresa en la que opera o desde la que opera.

**Costo**

Gratis.

*Para ir más allá*: Artículos R. 422-1 y siguientes del Código de Propiedad Intelectual.

### En el caso de un ejercicio en forma de sociedad civil profesional (SCP) o de una empresa de ejercicios liberales (SEL), solicite el registro de la empresa ante el Inpi

Si la persona desea ejercer en forma de CPS o de SEL, deberá incluirla en la lista elaborada por el Director General de Inpi, además del registro obligatorio de los ayuntamientos de propiedad personal que la componen.

*Para ir más allá*: Artículo L.422-7 del Código de Propiedad Intelectual.

### Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

