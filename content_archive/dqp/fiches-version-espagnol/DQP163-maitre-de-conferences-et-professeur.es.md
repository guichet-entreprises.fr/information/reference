﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP163" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arte y cultura" -->
<!-- var(title)="Profesor y profesor universitario" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arte-y-cultura" -->
<!-- var(title-short)="profesor-y-profesor-universitario" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/arte-y-cultura/profesor-y-profesor-universitario.html" -->
<!-- var(last-update)="2020-04-15 17:20:42" -->
<!-- var(url-name)="profesor-y-profesor-universitario" -->
<!-- var(translation)="Auto" -->


Profesor y profesor universitario
=================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:42<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

Los profesores y profesores universitarios son profesores-investigadores cuya actividad es transmitir a los estudiantes conocimientos en una o más asignaturas impartidas durante su formación inicial y continua.

Durante esta actividad, son responsables de desarrollar y organizar sus enseñanzas y proporcionar asesoramiento y orientación a los estudiantes.

*Para ir más allá*: Artículo 3 del Decreto 84-431, de 6 de junio de 1984, por el que se establecen las disposiciones legales comunes aplicables a los profesores-investigadores y que tienen un estatuto especial del cuerpo de profesores universitarios y del cuerpo de profesores.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Las funciones de profesor y profesor de universidades están disponibles para los candidatos:

- Profesionalmente calificado;
- en una lista de cualificaciones elaborada por el Consejo Nacional de Universidades (UNC).

**Tenga en cuenta que**

Profesionales que han estado enseñando y estudiando durante menos de 18 meses en una institución de educación superior en un Estado miembro de la Unión Europea (UE) o que son parte en el acuerdo del Espacio Económico Europeo (EEE) ) excepto Francia, están exentos de ser incluidos en la lista de cualificaciones para las funciones de profesor.

*Para ir más allá* decreto de 6 de junio de 1984.

#### Entrenamiento

**Inscripción en una lista que califique**

**Deberes de profesor**

Para ser incluido en la lista de cualificaciones de la CNU para las funciones de profesor, el profesional debe:

- tener un doctorado o una autorización para liderar la investigación. El Doctorado Estatal, el Doctorado de Posgrado y el Diploma de Doctor-Ingeniero están admitidos en equivalencia;
- justifican al menos tres años de experiencia profesional, adquirida en los últimos seis años;
- Ser un maestro asociado a tiempo completo
- Estar separado del cuerpo de los profesores;
- pertenecen a un cuerpo de investigadores.

Una vez que cumplan con estas condiciones, la persona debe hacer una declaración de solicitud en línea en el portal[Galaxia](https://www.galaxie.enseignementsup-recherche.gouv.fr/) personal de educación superior.

*Para ir más allá*: Artículo 23 del decreto de 6 de junio de 1984 mencionado anteriormente; decreto de 5 de julio de 2017 relativo al procedimiento de inscripción en las listas de cualificaciones para las funciones de profesor o profesor de universidades.

**Cátedras universitarias**

Para ser incluido en la lista de cualificaciones para los puestos de profesor de universidades, el profesional debe:

- estar facultados para llevar a cabo investigaciones. No obstante, el titular de un título universitario, una cualificación o un nivel equivalente de cualificación podrá nadarde de este requisito por la CNU;
- justificar haber llevado a cabo una actividad profesional efectiva de al menos cinco años el primero de enero de su año de inscripción, en los últimos ocho años. Sin embargo, no se tienen en cuenta las actividades docentes, de investigación y de interés incidentales en instituciones públicas de carácter científico y tecnológico;
- Ser un maestro asociado a tiempo completo
- Estar separado del cuerpo de los profesores universitarios;
- pertenecen a un cuerpo de investigadores asimilados a profesores universitarios.

En cuanto a los profesores, cuando el candidato cumple con estos requisitos, debe hacer una declaración de solicitud en línea en el portal Galaxy, personal de educación superior.

**Es bueno saber**

Las ofertas para puestos como profesor y profesor de universidades se publican en el portal Galaxy:

- en un calendario común, ofrecido cada año en el sitio;
- sobre el agua, por las instituciones que publican las ofertas por sí mismas, así como el momento del proceso de solicitud específico para cada puesto.

*Para ir más allá*: Artículos 43 y 44 del decreto de 6 de junio de 1984.

**Concurso de reclutamiento**

La contratación de profesores y profesores de universidades se lleva a cabo, a través de diversos concursos organizados por las instituciones, cuyo carácter depende de las cualificaciones del candidato.

Mientras el solicitante posea una de las cualificaciones requeridas para asistir al menos a uno de estos concursos, deberá solicitar al director de la institución de que se trate, para cubrir la vacante (véase infra "5o. a. Solicitud para el concurso de selección de puestos de profesor y profesor universitario").

*Para ir más allá*: Artículos 26 y 46 del Decreto de 6 de junio de 1984.

**Cita**

**Disposiciones de los profesores**

Los profesores admitidos son nombrados como pasantes durante un año y deben llevar a cabo la formación durante este período con el fin de profundizar sus habilidades docentes necesarias para llevar a cabo sus funciones.

Además, dentro de los cinco años de su permanencia, el profesional puede solicitar formación adicional para profundizar sus habilidades docentes. Si es necesario, se beneficiará de un desempeño de la actividad docente.

*Para ir más allá*: Artículos 32 y 32-1 del Decreto de 6 de junio de 1984 antes mencionado; decreto de 8 de febrero de 2018 por el que se establece el marco nacional de formación destinado a profundizar las competencias pedagógicas de los profesores en prácticas.

**Disposiciones de los profesores universitarios**

Los profesores universitarios son nombrados por decreto del Presidente de la República.

Además, los candidatos admitidos en concursos de selección (profesores y profesores universitarios) deben comprometerse al puesto correspondiente para ser nombrados.

*Para ir más allá*: Artículos 50 y 58-5 del Decreto de 6 de junio de 1984 supra.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

No se prevé que el nacional de un Estado de la UE o del EEE trabaje de forma temporal y casual como profesor y profesor de universidades en Francia.

Como tal, el nacional está sujeto a los mismos requisitos que el nacional francés (véase infra "4o. Procedimientos y formalidades de reconocimiento de cualificación").

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Cualquier nacional de un Estado de la UE o del EEE, legalmente establecido y que actúe como profesor o profesor universitario, podrá llevar a cabo la misma actividad en Francia.

Para ello, el nacional puede solicitar los distintos concursos de selección en las mismas condiciones que el nacional francés (véase más arriba "4o. a. Concurso de contratación").

*Para ir más allá*: Artículos 27 y 42 del Decreto de 6 de junio de 1984 antes mencionado.

3°. Condiciones de honorabilidad, reglas éticas
--------------------------------------------------------

**Informe de actividad**

Cada cinco años, el profesor o profesor universitario debe proporcionar al presidente o director de la institución un informe que describa todas sus actividades y sus posibles cambios. El director de la escuela debe presentar este informe a la CNU.

*Para ir más allá*: Artículo 7-1 del decreto de 6 de junio de 1984.

**Actividad acumulada**

El profesional está obligado por las disposiciones de los funcionarios estatales y, como tal, se le puede permitir combinar una actividad incidental con su actividad principal. Si es así, no debe interferir con el funcionamiento normal, la independencia o la neutralidad del servicio público.

*Para ir más allá* Decreto No 2017-105 de 27 de enero de 2017 relativo al ejercicio de actividades privadas por parte de funcionarios públicos y determinados funcionarios contractuales de derecho privado que han cesado sus funciones, la acumulación de actividades y la comisión de ética de la función pública.

**Adelanto**

Los profesores pueden beneficiarse del proceso de intensificación debido a su edad. Este avance se pronuncia por orden del presidente o director del establecimiento de acuerdo con lo dispuesto en los artículos 36 a 40-1-1 del decreto de 6 de junio de 1984 antes mencionado.

Además, los profesores universitarios también pueden beneficiarse de la adelanto o la promoción de clases en las condiciones establecidas en los artículos 52 a 57 del decreto.

4°. Procedimientos y formalidades de reconocimiento de cualificación
-----------------------------------------------------------------------------------------

### a. Solicitud de concursos de selección para el puesto de profesor o profesor universitario

**Autoridad competente**

El solicitante deberá presentar su solicitud en tres copias, destinadas al presidente o al director de la institución en la que el puesto esté vacante y a sus ponentes. Esta solicitud debe hacerse en papel o electrónicamente, dentro de al menos treinta días a partir de la fecha de apertura de los registros de solicitud.

**Documentos de apoyo**

Su expediente de solicitud debe incluir los siguientes documentos, si los hubiere, con su traducción aprobada al francés:

- una declaración de solicitud impresa del sitio web[Galaxia](https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/cand_doc_dossier_qualif.htm) Personal superior;
- Una fotocopia de su identificación con fotografía;
- un documento que acredite sus cualificaciones profesionales (véase supra "2. a. Legislación nacional");
- Un currículum de trabajo, libros, artículos y logros;
- una copia del informe de la defensa del diploma producido.

**Resultado del procedimiento**

Al final del procedimiento, la lista de candidatos contratados se publica en el portal[Galaxia](https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/cand_recrutement.htm) Ministerio de Educación Superior, Investigación e Innovación.

*Para ir más allá*: Artículos 9 a 16 de las órdenes del 13 de febrero de 2015 relativas a las condiciones generales de transferencia, contratación de servicios y de competencia de profesores y profesores universitarios.

### b. Remedios

**Centro de Asistencia Francesa****

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

