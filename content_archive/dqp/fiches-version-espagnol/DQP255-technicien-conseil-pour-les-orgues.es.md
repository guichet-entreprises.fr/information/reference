﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP255" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arte y cultura" -->
<!-- var(title)="Técnico consultor de órganos protegidos" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arte-y-cultura" -->
<!-- var(title-short)="tecnico-consultor-de-organos-protegidos" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/arte-y-cultura/tecnico-consultor-de-organos-protegidos.html" -->
<!-- var(last-update)="2020-04-15 17:20:43" -->
<!-- var(url-name)="tecnico-consultor-de-organos-protegidos" -->
<!-- var(translation)="Auto" -->


Técnico consultor de órganos protegidos
=======================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:43<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de actividad
--------------------------

El consultor de órganos protegidos es un profesional que asiste al Ministerio de Cultura en la realización de misiones de servicio público relacionadas con la protección de órganos bajo monumentos históricos (censo, supervisión estatal, propuestas de medidas de conservación, participación en la formación de proyectos distintos de aquellos para los que solicitó misiones de gestión de proyectos, seguido de la manutención de órganos pertenecientes a Estado). También se le puede exigir que participe en programas de investigación y enseñanza en el campo del patrimonio instrumental.

*Para ir más allá* :[Capítulo II del Decreto 2016-831](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032753839&categorieLien=id) 22 de junio de 2016 sobre técnicos de consultoría certificados para órganos protegidos bajo monumentos históricos.

El Código de Patrimonio estipula que el propietario o cesionario de un órgano clasificado o registrado como monumento histórico está obligado a confiar el proyecto de «reparación, reconocimiento y restauración»:

- ya sea a un técnico de consultoría con licencia;
- ya sea en una operación determinada, a un nacional francés o a un nacional de otro Estado miembro de la Unión Europea (UE) cuya formación, experiencia profesional adquirida en operaciones recientes en órganos patrimoniales atestiquen los conocimientos necesarios para llevar a cabo el trabajo.

*Para ir más allá* :[Capítulo II del Decreto 2016-831](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032753839&categorieLien=id) 22 de junio de 2016 sobre técnicos de consultoría certificados para órganos protegidos bajo monumentos históricos.

2°. Cualificaciones profesionales
--------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El título de técnico consultor de órganos protegidos está reservado a los profesionales que han recibido la acreditación del Ministro responsable de la cultura. La acreditación se emite a través de un proceso de solicitud de 5 años y renovable.

*Para ir más allá* :[23 de febrero de 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034133677&categorieLien=id) en relación con las condiciones requeridas para la acreditación de técnicos de consultoría para órganos protegidos bajo monumentos históricos y las condiciones relativas a la declaración para llevar a cabo la actividad de forma temporal y ocasional.

#### Entrenamiento

Para obtener la aprobación del Ministerio de Cultura, el profesional que desee obtener la condición de técnico consultor de órganos protegidos debe cumplir las siguientes condiciones:

- poseer un título de posgrado o equivalente en los campos de la musicología, la organología y la práctica instrumental;
- tener al menos un año de experiencia profesional en estas áreas.

Cuando la formación realizada sea diferente de la citada anteriormente, el solicitante deberá justificar haber adquirido al menos seis años de experiencia laboral a tiempo completo o a tiempo parcial en los últimos diez años a partir de la fecha de la facturas de órganos como organólogo o contratista principal.

#### Costos asociados con la calificación

Se paga la formación que conduzca al grado requerido para ejercer como técnico consultor de órganos protegidos. Su coste varía según las instituciones (universidades, conservatorios o escuelas privadas) que imparten enseñanzas en el campo de la musicología, la organología y la práctica instrumental.

### b. Nacionales de la UE: para ejercicios temporales o ocasionales (Entrega gratuita de servicios (LPS))

El nacional de un Estado miembro de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) que trabaje como técnico consultor de órganos protegidos podrá hacer uso de su título profesional en Francia, de forma temporal e informal, tan pronto como solicitándolo, antes de su primera actuación, mediante declaración dirigida al Ministro de Cultura (véase infra "5.a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y temporales y (LPS)).

En el caso de que la profesión no esté regulada, ni en el curso de la actividad ni en el marco de la formación, en el país en el que el profesional esté legalmente establecido, deberá haber realizado esta actividad durante al menos un año durante los diez años antes de la prestación, en uno o varios Estados miembros de la Ue-Ue.

Además, un nacional europeo que desee ejercer en Francia de forma temporal y ocasional debe contar con las aptitudes linguísticas necesarias para llevar a cabo la actividad.

*Para ir más allá* :[Artículo R. 622-59](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074236&idArticle=LEGIARTI000029691954&dateTexte=&categorieLien=cid) Código de Patrimonio.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Para ejercer un control permanente en Francia sobre las obras sobre órganos protegidos por monumentos históricos, el nacional de un Estado miembro de la UE o del EEE debe cumplir una de las siguientes condiciones:

- Poseer un certificado de competencia o certificado de formación expedido por la autoridad competente de un Estado miembro de la UE o del EEE que regule el acceso o el ejercicio de esta actividad;
- certificar que esta actividad se ha realizado durante al menos un año a tiempo completo o a tiempo parcial en los últimos diez años en un Estado miembro de la UE o del EEE que no regula el acceso a la profesión o el ejercicio de la actividad;
- poseer un certificado de formación expedido por un tercer país a la Unión Europea, reconocido por un Estado miembro de la UE o del EEE que le haya permitido trabajar como técnico consultor en ese Estado durante un mínimo de tres años.

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones antes mencionadas podrá solicitar la acreditación de los servicios de monumentos históricos dentro del Ministerio de Cultura. Para obtener más información, es aconsejable consultar el párrafo "5." b. Obtener la acreditación de los nacionales de la UE para un ejercicio permanente (LE).

En caso de diferencias sustanciales entre las cualificaciones profesionales adquiridas y las exigidas en Francia, el nacional puede estar sujeto a medidas de compensación (véase más adelante "Bueno saber: medida de compensación").

El nacional que desee obtener la acreditación para llevar a cabo su actividad en Francia debe tener las habilidades linguísticas necesarias para llevar a cabo la función.

3°. Condiciones de honorabilidad, reglas éticas, ética
-----------------------------------------------------

Aunque no están codificados, todas las funciones requeridas a los técnicos de consultoría para órganos protegidos se aplican a los nacionales que deseen ejercer la profesión en Francia.

4°. Seguros
----------

Como contratista principal, el interesado tiene la obligación de contratar un seguro de responsabilidad civil profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
-------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

**Autoridad competente**

La declaración previa de actividad debe dirigirse al Ministro responsable de la cultura antes de la primera prestación del servicio.

**Renovación de la predeclaración**

La declaración previa deberá renovarse una vez al año si el nacional desea llevar a cabo un nuevo servicio en Francia.

Esta declaración se dirige por carta recomendada con solicitud de acuse de recibo, contra la recepción, a la subdirección de monumentos históricos y áreas protegidas de la Dirección General de Patrimonio.

Incluye las siguientes piezas acompañadas, si es necesario, por su traducción al francés:

- Copiar una identificación válida
- certificado que certifica que el nacional está legalmente establecido en un Estado de la UE o del EEE para llevar a cabo la actividad de técnico consultor de órganos protegidos en virtud de monumentos históricos y que no incurre en ninguna prohibición, incluso temporal, ejercicio;
- Prueba de sus cualificaciones profesionales
- si el Estado no regula el acceso o el ejercicio de la actividad, las pruebas por cualquier medio de que el nacional ha participado en esta actividad durante al menos un año, a tiempo completo o a tiempo parcial, en los diez años anteriores a la presentación de la declaración.

*Para ir más allá*: Artículo 8 de la Orden de 23 de febrero de 2017.

### b. Obtención de la acreditación de los nacionales de la UE para un ejercicio permanente (LE)

Cualquier nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el decreto 2016-831 de 22 de junio de 2016 relativo a los técnicos consultivos certificados para órganos protegidos por monumentos históricos que deseen establecerse en Francia debe obtener primero la aprobación del Ministro de Cultura y Monumentos Históricos.

Esta acreditación debe renovarse cada cinco años.

Para obtener esta certificación, el nacional puede estar obligado a llevar a cabo una medida de compensación (prueba de aptitud o curso de adaptación) si resulta que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de los requeridos para el ejercicio de la profesión en Francia (véase a continuación: "Bueno saber: medidas de compensación").

**Autoridad competente**

El expediente de solicitud se remite a la Dirección de Monumentos Históricos y áreas protegidas de la Dirección General de Patrimonio. El archivo se somete a notificación a la Comisión Nacional de Patrimonio y Arquitectura, que audiciona al candidato.

**hora**

La concesión de la acreditación por parte del Ministro responsable de Cultura y Monumentos Históricos se lleva a cabo en el plazo de dos meses a partir del día en que el candidato recibe el expediente completo por decisión motivada.

**Documentos de apoyo**

En apoyo de su solicitud de acreditación, un nacional de un Estado de la UE o del EEE debe presentar un expediente completo enviado por carta recomendado con notificación de recepción o presentado contra la recepción. Debe incluir las siguientes pruebas:

- Una copia de un documento de identidad válido
- Un CV detallado
- referencias a estudios, investigaciones o publicaciones;
- La lista de asignaciones realizadas por el candidato como organólogo o contratista principal;
- Una carpeta equivalente al contenido de una carpeta de protección de órganos
- un archivo equivalente a un estudio previo a la restauración de un órgano;
- un archivo que contiene todos los documentos preparados por el candidato en la ejecución de una misión completa de gestión de proyectos, incluyendo un archivo equivalente al contenido de un expediente documental de las obras realizadas.

A todas estas exposiciones también habrá que añadir otros elementos de apoyo dependiendo de si:

- el nacional es legalmente un técnico consultor en un estado de la UE o del EEE que regula el acceso o el ejercicio en la actividad del técnico consultor. En este caso, tendrá que producir:- Copia del certificado de competencia o título de formación sancionando la formación en el ámbito de la musicología, la organología y la práctica instrumental expedido por las autoridades competentes de ese Estado,
  - Se siguió una copia de la descripción detallada del plan de estudios;
- cuando el acceso a la profesión o el ejercicio de la actividad no esté regulado en un Estado de la UE o del EEE, el nacional de uno de estos Estados justifica el ejercicio de una actividad durante al menos un año, a tiempo parcial o a tiempo completo, durante los diez en los últimos años en ese estado. En este caso, tendrá que producir:- una copia de cualquier certificado de competencia o documento de formación expedido por las autoridades competentes de un Estado de la UE o del EEE que acredite la preparación para el ejercicio de la actividad de técnico consultor de órganos protegidos,
  - Una descripción de la experiencia profesional adquirida
- el nacional de un Estado de la UE o del EEE ha obtenido una denominación de formación en un tercer Estado, reconocida en un Estado de la UE o del EEE, y ha ejercido legalmente la profesión durante al menos tres años. En este caso, tendrá que producir:- Una copia del título de capacitación emitido por el tercer estado,
  - una copia del documento expedido por el estado en el que se adquirió experiencia laboral, certificando el ejercicio de la actividad del técnico consultor,
  - una descripción de la experiencia profesional adquirida.

**Tenga en cuenta que**

En caso de expediente incompleto, la Dirección de Monumentos Históricos y Zonas Protegidas se pondrá en contacto con el solicitante por carta con acuse de recibo para invitarle a facilitar los documentos necesarios.

**Prueba de aprobación**

Sólo se requerirá un informe de actividad preparado por el candidato, traducido al francés, para el expediente de solicitud.

**Costo**

Gratis.

*Para ir más allá*: Artículos 3 y 4 de la orden del 23 de febrero de 2017.

**Bueno saber: medida de compensación**

Después de revisar los documentos justificativos, la Dirección a cargo de los monumentos históricos indica por decisión motivada al nacional, un candidato a la acreditación, el uso de medidas de compensación cuando existen diferencias sustanciales entre formación del nacional y la formación impartida en Francia.

La decisión motivada incluirá:

- El nivel requerido de cualificación profesional
- El nivel de cualificación profesional del candidato para la acreditación;
- Diferencias entre la formación del candidato y la formación impartida en Francia;
- razones por las que las diferencias no pueden ser superadas por los conocimientos, habilidades y habilidades adquiridas a través de la experiencia laboral o el aprendizaje.

Estas medidas pueden adoptar la forma de un curso de ajuste o una prueba de aptitud, ya que el nacional tiene dos meses para dar a conocer su elección. Las asignaturas de la prueba de aptitud y la duración de la pasantía se indicarán por decreto ministerial.

**La prueba de aptitud**

La prueba de aptitud tendrá lugar seis meses después de que el candidato haya notificado la elección de la medida de compensación elegida.

Durante el evento, el candidato comparecerá ante un jurado nombrado por el Ministro responsable de los monumentos históricos.

*Para ir más allá*: Artículo 5 de la Orden de 23 de febrero de 2017.

**El curso de adaptación**

La pasantía, que no podrá exceder de un año, deberá realizarse con un técnico consultor de órganos protegidos activos en el momento de la aplicación y haber realizado esta actividad durante al menos cinco años, en los últimos diez años anteriores al inicio de la pasantía.

Al final de la pasantía, el candidato en prácticas será sometido a una evaluación por parte del administrador de prácticas que lo informará y lo remitirá a la subdirección de monumentos históricos en el plazo de un mes a partir del final de la pasantía.

**Resultado del procedimiento**

Una vez recibido el informe de evaluación de la pasantía o los resultados obtenidos en la prueba de aptitud, el Ministro responsable de monumentos históricos tendrá dos meses para decidir sobre la decisión de acreditación.

*Para ir más allá*: Artículo 6 de la Orden de 23 de febrero de 2017.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

