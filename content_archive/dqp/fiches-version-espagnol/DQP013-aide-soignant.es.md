﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP013" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Cuidador" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="cuidador" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/cuidador.html" -->
<!-- var(last-update)="2020-04-15 17:21:12" -->
<!-- var(url-name)="cuidador" -->
<!-- var(translation)="Auto" -->


Cuidador
========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:12<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

Como parte de un equipo de atención, el cuidador es un profesional que ayuda a la enfermera en las actividades de cuidado diario. Contribuye al bienestar de los enfermos, los acompaña en todos los gestos de la vida cotidiana y ayuda a mantener su autonomía. En colaboración con la enfermera, y bajo su responsabilidad, el asistente de enfermería proporciona cuidados de higiene y comodidad.

*Para ir más allá*: Apéndice I del decreto del 22 de octubre de 2005 sobre la formación que conduce al Diploma Estatal de Enfermería.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Las personas con el Diploma estatal de Auxiliar de Enfermería (DEAS), el Certificado de Aptitud para el Cuidador o el Diploma de Enfermería Profesional pueden ejercer como cuidadores.

*Para ir más allá*: Artículo L. 4391-1 del Código de Salud Pública.

#### Entrenamiento

Más a menudo que no, el diploma estatal se prepara en diez meses en un instituto de capacitación.

La formación está abierta a todas las personas mayores de diecisiete años que hayan superado el concurso de ingreso en uno de los institutos de formación, sin necesidad de cualificación.

El diploma estatal también se puede obtener mediante la validación de la experiencia (VAE). A continuación, el candidato deberá justificar las competencias profesionales adquiridas en el curso de una actividad asalariada o voluntaria, directamente relacionada con el contenido del diploma. Para obtener más información, puede ver[sitio web oficial](http://www.vae.gouv.fr/) Vae.

*Para ir más allá* 22 de octubre de 2005.

#### Costos asociados con la calificación

Se paga la capacitación para el diploma de asistente de enfermería del estado. Su coste varía en función de los institutos de formación (de unos 500 a 4.000 euros, como indicación). Para más detalles, es aconsejable acercarse al instituto de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

El nacional de un Estado miembro de la Unión Europea (UE)*o parte en el Acuerdo del Espacio Económico Europeo (EEE)*, legalmente establecido en uno de estos Estados podrá nabrán la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si ni el acceso ni la formación están regulados en el Estado miembro de origen ni en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Las cualificaciones profesionales del proveedor se comprueban antes de que se proporcione el primer servicio. En caso de diferencia sustancial entre las cualificaciones del reclamante y la formación requerida en Francia, que puede perjudicar a la salud pública, la autoridad competente pide al reclamante que demuestre que ha adquirido los conocimientos y las aptitudes medidas de compensación (véase infra "Bueno saber: medidas de compensación").

En todos los casos, el nacional europeo que desee ejercer en Francia de forma temporal u ocasional debe poseer las aptitudes linguísticas necesarias para llevar a cabo la actividad y dominar el peso y los sistemas de medición utilizados en Francia. Francia.

*Para ir más allá*: Artículos L. 4391-4 y L. 4391-5 del Código de Salud Pública.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Los ciudadanos de la UE que deseen trabajar como cuidadores permanentes en Francia deben obtener una licencia para ejercer.

Los nacionales de la UE o del EEE pueden poder ejercer en Francia si han completado con éxito un ciclo de educación postsecundaria y son titulares, de su elección:

- un certificado de formación expedido por un Estado de la UE o del EEE en el que se regulen el acceso a la profesión o a su práctica y que permita ejercer allí la práctica jurídica;
- un certificado de formación expedido por un Estado de la UE o del EEE en el que no se regula el acceso a la profesión ni su ejercicio, acompañado de un certificado que justifique el ejercicio en ese estado de esta actividad durante al menos el equivalente de dos años a tiempo completo en los últimos diez años;
- un certificado de formación, expedido por un tercer Estado, reconocido en un Estado de la UE o del EEE, distinto de Francia, que permita ejercer allí la profesión.

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y a su práctica en Francia, el interesado podrá someterse, en función de su elección, a un examen (véase a continuación"Bien saber: medidas de compensación").

*Para ir más allá*: Artículo L. 4391-2 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### Obligación de formación profesional continua

Los cuidadores deben participar anualmente en un programa de desarrollo profesional en curso. Este programa tiene como objetivo mantener y actualizar sus conocimientos y habilidades, así como mejorar sus prácticas profesionales.

Como tal, el profesional de la salud (salario o liberal) debe justificar su compromiso con el desarrollo profesional. El programa está en forma de formación presentacional, mixta o no presential. Toda la formación se registra en un documento personal que contiene certificados de formación.

*Para ir más allá* : decreto de 30 de diciembre de 2011 sobre el continuo desarrollo profesional de los profesionales sanitarios aliados.

4°. Seguro
-------------------------------

En caso de ejercicio liberal, el cuidador está obligado a tomar un seguro de responsabilidad civil profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus trabajadores por los actos realizados en ocasiones.

5°. Proceso de reconocimiento de cualificación y trámites
-------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Todo nacional de la UE o del EEE que esté establecido y realice legalmente las actividades de las ayudas de enfermería en un Estado de la UE o del EEE podrá ejercer en Francia de forma temporal y ocasional si hace la declaración previa.

Si ni el acceso ni el ejercicio de esta actividad están regulados en el Estado de práctica de la UE o del EEE, el interesado deberá aportar pruebas de un ejercicio profesional de al menos dos años a tiempo completo en los últimos diez años.

Las cualificaciones profesionales de la persona se comprueban antes de proporcionar el primer servicio. En caso de diferencias sustanciales entre las cualificaciones de la persona y la formación requerida en Francia, el reclamante puede ser requerido para demostrar que ha adquirido los conocimientos y habilidades que faltan, compensación (véase a continuación: "Bueno saber: medidas de compensación").

#### Autoridad competente

La declaración previa de actividad debe dirigirse, antes de la primera actuación, a la Dirección Regional de Juventud, Deportes y Cohesión Social (DRJSCS) en Hauts-de-France.

#### Renovación de la predeclaración

La declaración anticipada debe renovarse una vez al año si el reclamante desea realizar una nueva prestación en Francia.

#### Procedimiento

El Ministro responsable de la salud decide, tras el dictamen de la comisión de asistentes de enfermería de la región de Hauts-de-France. En el plazo de un mes a partir de la recepción de la declaración, el Ministro informa a la persona que puede o no comenzar la prestación de servicios o que debe proporcionar pruebas de que ha adquirido los conocimientos y habilidades que faltan, incluso a través de medidas de compensación. El Ministro también podrá, en el plazo de un mes a partir de la recepción de la declaración, informar de la necesidad de más información. En este caso, el Ministro dispone de dos meses a partir de la recepción de información adicional para informar al interesado de que puede o no comenzar su prestación de servicios.

A falta de una respuesta del Ministro responsable de la salud dentro del plazo antes mencionado, puede comenzar la prestación de servicios.

#### Entrega de recibo

El Ministro responsable de la salud enumera al proveedor de atención médica en una lista en particular y le envía un recibo con su número de registro. A continuación, el demandante deberá informar a la agencia nacional de seguros pertinente de su prestación enviando una copia de dicho recibo.

#### Documentos de apoyo

- el formulario de declaración cumplimentado, cuyo modelo se presenta como apéndice de la orden del 24 de marzo de 2010 en el que se establece la composición del expediente que se facilitará a las comisiones de licencia pertinentes para la revisión de las solicitudes presentadas para el ejercicio fiscal Francia de las profesiones de auxiliar de enfermería, auxiliar de cuidado de niños y trabajador de ambulancias;
- Fotocopia de un DNI, que se cumplimentará con un certificado de nacionalidad en caso necesario;
- fotocopia del título de formación (traducido al francés por un traductor certificado);
- un certificado de la autoridad competente del Estado de Conciliación de la UE o del EEE que certifique que la persona está legalmente establecida en ese Estado y no incurre en ninguna prohibición de ejercer (traducida al francés por un traductor certificado).

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 4391-4, R. 4391-5, R. 4331-12 a R. 4331-15 del Código de Salud Pública, orden de 24 de marzo de 2010 supra y orden de 11 de agosto de 2010 por el que se nombran comisiones regionales para dar aviso de declaraciones de libre prestación de servicios para paramédicos, auxiliares de enfermería, auxiliares de cuidado infantil y paramédicos.

### b. Formalidades para los nacionales de la UE para un ejercicio permanente (LE)

#### Pedir permiso para practicar

Un nacional de un Estado de la UE o del EEE debe obtener una autorización para poder ejercer como cuidador permanente en territorio francés.

#### Autoridad competente

La solicitud debe dirigirse al prefecto regional del lugar de asentamiento de la persona. El prefecto expide la autorización para ejercer, previa asesoración del Comité de Cuidadores.

#### Procedimiento

El solicitante deberá presentar su solicitud por duplicado por carta recomendada con notificación de recepción, acompañada de un expediente completo a la Dirección Regional de Juventud, Deportes y Cohesión Social (DRJSCS) del lugar en el que desee ejercer.

#### hora

El prefecto regional deberá confirmar la recepción en el plazo de un mes a partir de la recepción de la solicitud acompañada del expediente completo. En caso de archivo incompleto, el prefecto deberá indicar los documentos que faltan y el plazo dentro del cual deben ser comunicados.

Si el prefecto permanece en silencio durante cuatro meses después de recibir la solicitud, la solicitud de licencia se considera rechazada.

#### Documentos de apoyo

Los documentos justificativos son:

- El formulario de solicitud de autorización para ejercer la profesión, cuyo modelo se proporciona como apéndice del auto de 24 de marzo de 2010;
- Una fotocopia de un documento de identidad válido
- Una copia del título de formación que permite ejercer la profesión en el país de obtención;
- Una copia de los diplomas adicionales si es necesario;
- todos los documentos útiles que justifiquen la formación continua, la experiencia y las competencias adquiridas durante la experiencia laboral en un Estado de la UE, eee o tercer Estado;
- una declaración de la autoridad competente del Estado de la UE o del EEE del Establecimiento, de menos de un año de edad, que acredite la ausencia de sanciones contra el solicitante;
- Una copia de los certificados de las autoridades que emiten el certificado de formación en el que se especifica el nivel de formación, los detalles de las lecciones año tras año y el contenido y la duración de las prácticas realizadas;
- para aquellos que han trabajado en un Estado de la UE o del EEE que no regula el acceso o el ejercicio de la profesión, todos los documentos que justifican un ejercicio en ese Estado por el equivalente a dos años a tiempo completo en los últimos diez años;
- para las personas titulares de un certificado de formación expedido por un tercer Estado y reconocido en un Estado de la UE o del EEE, distinto de Francia, el reconocimiento del certificado de formación establecido por las autoridades estatales que han reconocido dicho título.

**Es bueno saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

#### Remedios

Si se rechaza la solicitud de licencia, el solicitante podrá iniciar:

- una apelación agraciada al prefecto regional que tomó la decisión, en el plazo de dos meses a partir de la decisión (implícita o expresa) de rechazo;
- un llamamiento jerárquico al Ministro responsable de la salud, en el plazo de dos meses a partir de la decisión (implícita o expresa) de rechazo;
- un recurso judicial ante el tribunal administrativo territorialmente competente, en el plazo de dos meses a partir de la decisión (implícita o expresa) de denegación.

**Bueno saber: medidas de compensación**

Para obtener permiso para ejercer, el interesado puede estar obligado a someterse a una prueba de aptitud o a un curso de adaptación, si parece que las cualificaciones profesionales y la experiencia que utiliza son sustancialmente diferentes. los necesarios para el ejercicio de la profesión en Francia.

Si se consideran necesarias las medidas de compensación, el prefecto regional responsable de expedir la autorización de ejercicio indica al interesado que dispone de dos meses para elegir entre la prueba de aptitud y el curso de ajuste.

#### La prueba de aptitud

El DRJSCS, que organiza las pruebas de aptitud, debe convocar a la persona por carta recomendada con un aviso de recepción al menos un mes antes del inicio de las pruebas. Esta citación menciona el día, la hora y el lugar del juicio.

La prueba de aptitud puede tomar la forma de preguntas escritas u orales señaladas en 20 sobre cada una de las asignaturas que no fueron inicialmente enseñadas o no adquiridas durante la experiencia profesional.

La admisión se pronuncia con la condición de que el individuo haya alcanzado un promedio mínimo de 10 sobre 20, sin puntuación inferior a 8 de 20. Los resultados de la prueba se notifican al interesado por el prefecto regional.

En caso de éxito, el prefecto regional autoriza al interesado a ejercer la profesión.

#### El curso de adaptación

Se lleva a cabo en un centro de salud público o privado aprobado por la agencia regional de salud (ARS). El becario es sometido a la responsabilidad pedagógica de un profesional cualificado que ha estado practicando la profesión durante al menos tres años y que establece un informe de evaluación.

La pasantía, que finalmente incluye formación teórica adicional, es validada por el jefe de la estructura de recepción a propuesta del profesional cualificado que evalúa al aprendiz.

Los resultados de la pasantía se notifican al interesado por el prefecto regional.

La solicitud de licencia para la práctica se hace después de nuevo aviso del Comité de Enfermería.

*Para ir más allá*: Artículos L. 4391-2 y los siguientes y D. 4391-2 y siguientes del Código de Salud Pública y el auto de 24 de marzo de 2010, supra.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

#### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

#### Procedimiento

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

#### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

#### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

#### Costo

Gratis.

#### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París,[sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

