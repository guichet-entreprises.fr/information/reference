﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP199" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Creador de luna sin óptica" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="creador-de-luna-sin-optica" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/creador-de-luna-sin-optica.html" -->
<!-- var(last-update)="2020-04-15 17:21:51" -->
<!-- var(url-name)="creador-de-luna-sin-optica" -->
<!-- var(translation)="Auto" -->


Creador de luna sin óptica
==========================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:51<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El óptico es un profesional sanitario especializado en el diseño, fabricación y venta de gafas, lentes correctivas y lentes de contacto.

Basado en una receta de un oftalmólogo (médico especialista), sin embargo puede evaluar la vista de su cliente haciendo un examen optométrico.

Una vez que el cliente ha elegido el marco, el óptico realizará el corte, calibración y montaje de las lentes.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de óptico-lunetier está reservada al titular del certificado de técnico superior (BTS) de óptico-lunetier.

*Para ir más allá*: Artículo D. 4362-1 del Código de Salud Pública.

#### Entrenamiento

El BTS es un diploma de Nivel III disponible después de la graduación. El diploma se prepara en dos años en escuelas secundarias tecnológicas, escuelas privadas o como parte de los aprendizajes en centros de formación para aprendices.

#### Costos asociados con la calificación

La formación para convertirse en un óptico-lunetier vale la pena y dependerá del tipo de educación elegida. Para obtener más información, es aconsejable acercarse a las instituciones interesadas.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (Servicio Gratuito)

Un nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), que prosiga legalmente la actividad óptica-lunetier en uno de estos Estados, podrá utilizar su título profesional en Francia de forma temporal o ocasional.

Tendrá que solicitarlo, antes de su primera actuación, mediante declaración dirigida al prefecto de la región en la que desea realizar la entrega (véase infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que esté legalmente establecida, el profesional deberá justificar haber la llevado a cabo en uno o más Estados de la UE o del EEE durante al menos un año, durante el transcurso de diez años antes de la actuación.

*Para ir más allá*: Artículo L. 4362-7 del Código de Salud Pública.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Todo nacional de un Estado de la UE o del EEE, establecido y que practique legalmente la actividad óptica-lunetier en ese Estado, podrá llevar a cabo la misma actividad en Francia de forma permanente si:

- posee un certificado de formación expedido por una autoridad competente de otro Estado miembro, que regula el acceso o el ejercicio de la profesión;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación o el ejercicio de la profesión;
- posee un título expedido por una autoridad competente de un tercer Estado y reconocido por un Estado de la UE o del EEE, y que justifica haber ejercido la profesión durante al menos tres años en la UE o en el Estado del EEE.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar una autorización individual para ejercer desde el prefecto de la región en la que desea ejercer su profesión (véase infra "5o). b. Solicitar un permiso de ejercicio para el nacional de la UE o del EEE para la actividad permanente (LE) ").

*Para ir más allá*: Artículo L. 4362-3 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El óptico de gafas no está sujeto a ninguna regla ética. Sin embargo, este último deberá cumplir con la normativa relativa a la emisión de lentes correctivas y especificada en las secciones L. 4362-9 a L. 4362-12 del Código de Salud Pública.

4°. Seguro
-------------------------------

Como profesional de la salud, un óptico-lunetier liberal debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

**Autoridad competente**

El prefecto de la región es competente para decidir sobre la solicitud de una declaración previa de actividad.

**Documentos de apoyo**

La solicitud se realiza mediante la presentación de un archivo que incluye los siguientes documentos:

- Una copia de un documento de identidad válido
- Una copia del título de formación que permite ejercer la profesión en el estado de obtención;
- un certificado, de menos de tres meses de edad, de la autoridad competente del Estado de la UNIÓN o del EEE, que certifique que el interesado está legalmente establecido en ese Estado y que, cuando se expide el certificado, no existe ninguna prohibición, ni siquiera temporal, Ejercicio
- cualquier prueba que justifique que el nacional haya ejercido la profesión en un Estado de la UE o del EEE durante un año en los últimos diez años, cuando dicho Estado no regule la formación, el acceso a la profesión solicitada o su ejercicio;
- cuando el certificado de formación haya sido expedido por un tercer Estado y reconocido en un Estado de la UE o del EEE distinto de Francia:- reconocimiento del título de formación establecido por las autoridades estatales que han reconocido este título,
  - cualquier prueba que justifique que el nacional ha ejercido la profesión en ese estado durante tres años;
- Si es así, una copia de la declaración anterior, así como la primera declaración hecha;
- un certificado de responsabilidad civil profesional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**hora**

Una vez recibido el expediente, el prefecto de la región tendrá un mes para decidir sobre la solicitud e informará al nacional:

- que puede comenzar la actuación. A partir de entonces, el prefecto registrará al solicitante en el directorio Adeli;
- que estará sujeto a una medida de compensación si existen diferencias sustanciales entre la formación o la experiencia profesional del nacional y las requeridas en Francia;
- no podrá iniciar la actuación;
- cualquier dificultad que pueda retrasar su decisión. En este último caso, el prefecto podrá tomar su decisión en el plazo de dos meses a partir de la resolución de esta dificultad, y a más tardar tres meses de notificación al nacional.

El silencio del prefecto dentro de estos plazos valdrá la pena aceptar la solicitud de declaración.

**Tenga en cuenta que **

La rentabilidad es renovable cada año o en cada cambio en la situación del solicitante.

*Para ir más allá* : Orden de 8 de diciembre de 2017 relativa a la declaración previa de prestación de servicios para asesores genéticos, médicos y preparadores de farmacias y farmacias hospitalarias, así como para ocupaciones en el Libro III de Parte IV del Código de Salud Pública.

### b. Solicitar un permiso de ejercicio para la UE o el EEE nacional para la actividad permanente (LE)

**Autoridad competente**

La autorización de ejercicio es expedida por el prefecto de la región, previa asesoración del comité de ópticos-lunetiers.

**Documentos de apoyo**

La solicitud de autorización se realiza mediante la presentación de un expediente que contiene todos los siguientes documentos:

- El formulario de solicitud cumplimentado y firmado para la autorización de ejercicio;
- Una fotocopia de un documento de identidad válido
- Una copia de su título de formación que le permite llevar a cabo la actividad de óptico-lunetier y, si es necesario, una fotocopia de diplomas adicionales;
- todos los documentos para justificar su formación continua y su experiencia profesional adquirida en un Estado miembro;
- una declaración del Estado miembro en la que se indique que el nacional no está sujeto a ninguna sanción;
- Una copia de todos sus certificados mencionando el nivel de capacitación recibido, y los detalles de las horas y el volumen de las enseñanzas siguieron;
- cuando ni el acceso a la formación ni su ejercicio estén regulados en el Estado miembro, ninguna documentación que justifique que haya sido un óptico-lunetier durante un año en los últimos diez años;
- cuando el certificado de formación haya sido expedido por un tercer Estado pero reconocido en un Estado miembro, el reconocimiento del título de formación por parte del Estado miembro.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

El prefecto reconoce la recepción del expediente en el plazo de un mes y decidirá después de tener la opinión del comité de ópticos-lunetiers. Este último es responsable de examinar los conocimientos y habilidades del nacional adquiridos durante su formación o durante su experiencia profesional. Puede someter al nacional a una medida de compensación.

El silencio guardado por el prefecto de la región en un plazo de cuatro meses merece la pena rechazar la solicitud de autorización.

*Para ir más allá*: Artículos R. 4362-2 a R. 4362-4 del Código de Salud Pública; decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización pertinentes para el examen de las solicitudes presentadas para la práctica en Francia de las profesiones de psicomotricista, logopeda, ortopedista, audiotésico y óptico-lunetier.

**Bueno saber: medidas de compensación**

Si el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia profesional muestra diferencias sustanciales con las cualificaciones necesarias para el acceso a la profesión de óptico-lunetier y su en Francia, el interesado tendrá que someterse a una medida de indemnización.

En función del nivel de cualificación exigido en Francia y del que posea el interesado, la autoridad competente podrá:

- Ofrecer al solicitante la opción de elegir entre un curso de ajuste o una prueba de aptitud;
- imponer un curso de ajuste y/o una prueba de aptitud;

*Para ir más allá*: Artículo L. 4362-3 del Código de Salud Pública; decreto de 30 de marzo de 2010 por el que se establece la organización de la prueba de aptitud y el curso de adaptación para la práctica en Francia de las profesiones de psicomotricia, logopeda, ortopedista, audioprotésico, optician-lunetier por Estados miembros de la Unión Europea o partes en el Acuerdo sobre el Espacio Económico Europeo.

### c. Registro en el directorio Adeli

Un nacional que desee ejercer como óptico-lunetier en Francia está obligado a registrar su autorización para ejercer en el directorio Adeli ("Automatización de listas").

**Autoridad competente**

El registro en el directorio de Adeli se realiza con la Agencia Regional de Salud (ARS) del lugar de práctica.

**hora**

La solicitud de inscripción se presenta en el plazo de un mes a partir de la toma del cargo del nacional, independientemente del modo de práctica (liberal, asalariado, mixto).

**Documentos de apoyo**

En apoyo de su solicitud de registro, el óptico-lunetier debe proporcionar un expediente que contenga:

- el título original o título que acredite la formación de óptico-lunetier emitido por el Estado de la UE o el EEE (traducido al francés por un traductor certificado, si procede);
- Id
- Formulario de ciervo 10906-06 completado, fechado y firmado.

**Resultado del procedimiento**

El número Adeli del nacional se mencionará directamente en la recepción del expediente, emitido por el ARS.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 4362-1 y D. 4364-18 del Código de Salud Pública.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

