﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP181" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Monitor de paracaidismo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="monitor-de-paracaidismo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/monitor-de-paracaidismo.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="monitor-de-paracaidismo" -->
<!-- var(translation)="Auto" -->


Monitor de paracaidismo
=======================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El instructor de paracaidismo supervisa de forma independiente el tándem, la progresión acompañada en caída libre o la progresión tradicional según la mención elegida. El instructor diseña un proyecto de animación en el campo de paracaidismo. Lleva a cabo actividades de descubrimiento y aprendizaje en el campo del salto. Por último, organiza la seguridad del público y participa en el funcionamiento de la estructura.

Un salto consiste en dejar caer un avión (por ejemplo, un avión) con un paracaídas, y a veces otros accesorios, desde una altura que puede estar a 1.000 a 6.000 metros del suelo, dependiendo de la disciplina practicada. Después de la salida, el paracaidista está en caída libre durante un período más largo o más corto dependiendo de la disciplina practicada y la altura a la que fue dejado caer. Puede realizar figuras solo o con otras personas antes de abrir su paracaídas. El umbral reglamentario mínimo para la apertura es de 850 metros.

La actividad de paracaidismo también incluye las actividades de ascenso náutico y vuelo de túnel de viento.

*Para ir más allá* : Apéndice I del decreto de 11 de julio de 2011 por el que se establece la especialidad de "paracaidismo" del certificado profesional de los jóvenes de la educación popular y el deporte (BPJEPS); Apéndice I del decreto de 9 de julio de 2002 por el que se establece la especialidad "actividades náuticas" del BPJEPS; Apéndice II-1 (Artículo A. 212-1) del Código del Deporte.

2°. Cualificaciones profesionales
-----------------------------------------

### Requisitos nacionales

#### Legislación nacional

La actividad del instructor en paracaídas está sujeta a la aplicación del Artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas.

Como profesor de deportes, el instructor de paracaidismo debe poseer un diploma, un título profesional o un certificado de cualificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

Las cualificaciones para el paracaídas son la especialidad "paracaidismo" BPJEPS, el Diploma Estatal de Juventud, Educación Popular y Deporte (DEJEPS) especialidad "desarrollo deportivo" mención "paracaidismo" y el Diploma estatal de juventud, educación popular y deporte (DESJEPS) especialidad "rendimiento deportivo" mención "paracaidismo".

La calificación para ejercer como instructor de paracaídas de ascenso es la especialidad BPJEPS "actividades acuáticas".

La calificación para el túnel de viento es el Certificado de Cualificación Profesional (CQP) "monitor de túnel de viento plano" con, si es necesario, la calificación adicional "vuelo del túnel de viento 3D".

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos A. 212-1 Apéndice II-1, L. 212-1 y R. 212-84 del Código del Deporte.

**Es bueno saber**

El entorno específico. La práctica del paracaidismo es una actividad que se lleva a cabo en un entorno específico independientemente del área de evolución. Implica el cumplimiento de medidas especiales de seguridad. Por lo tanto, sólo las organizaciones bajo la tutela del Ministerio del Deporte pueden formar a futuros profesionales.

*Para ir más allá*: Artículos L. 212-2 y R. 212-7 del Código del Deporte.

#### Entrenamiento

##### CQP "monitor de vuelo plano en túnel de viento"

Este es un diploma gestionado por la Federación Francesa de Paracaidistas (FFP).

La formación se lleva a cabo alternativamente entre la organización de formación y la estructura anfitriona. El entrenamiento interno dura al menos 140 horas. La duración total del entrenamiento es de 320 horas.

Este diploma está abierto a la validación de la experiencia (VAE).

**Prerrogativas**

El "monitor de respirador de vuelo plano" de CQP permite la supervisión autónoma de las actividades de vuelo plano a viento para cualquier público.

*Para ir más allá*: Apéndice II-1 (Artículo A. 212-1) del Código del Deporte.

**Condiciones de acceso a la formación que conducen al CQP**

El interesado deberá:

- Tener dieciocho años el día de la inscripción
- presentar un certificado médico de no contradictorio para la práctica de menos de tres meses de edad;
- Poseer la Unidad de Educación de Prevención y Socorro Cívica de Nivel 1 (PSC1) o su equivalente;
- presentar un certificado de éxito a las seis pruebas técnicas y físicas de entrada en formación definidas por la Federación Francesa de Paracaidismo y expedidas por ella.

Para más información, es aconsejable consultar el[Sitio web de FFP](http://www.ffp.asso.fr/wp-content/uploads/2013/03/presentation-CQP-soufflerie.pdf).

**Es bueno saber**

El CQP "monitor de túnel de viento plano" puede ir acompañado de la calificación adicional "vuelo del túnel de viento 3D", que permite monitorear las actividades de vuelo 3D en el túnel de viento.

##### BPJEPS especialidad "paracaidismo"

El BPJEPS es un diploma estatal registrado en el[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) y clasificado en el nivel IV de la nomenclatura de nivel de certificación.

Da fe de la adquisición de una cualificación en el ejercicio de una actividad profesional encargada de fines educativos o sociales, en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Este diploma se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr). En el contexto de un VAE, el candidato también tendrá que ser puesto en una situación profesional.

*Para ir más allá*: Artículos D. 212-20 y D. 212-24 del Código del Deporte; Apéndice VIII del decreto del 11 de julio de 2011 por el que se crea la especialidad "paracaidismo" del BPJEPS.

Tres menciones son posibles:

- el llamado entrenamiento de método sin método tradicional (TRAD) que comienza con saltos de apertura automáticos a una altura mínima de 1.000 metros, seguido sin "manijas de control" (saltos en los que el alumno hace el gesto de tirar del asa de apertura de su paracaídas, pero donde en realidad se abre por la correa de apertura automática que lo conecta con el avión, permitiendo que el ala se abra incluso si el gesto del estudiante es incorrecto). Después de dos saltos consecutivos exitosos en un mango de control, se permite al estudiante saltar en la apertura manual y más alto y más alto, hasta alcanzar gradualmente la altura de 4.000 metros;
- la progresión de caída sin salida (PAC) que permite al estudiante saltar en caída libre desde una altura mínima de 3.000 metros. Para el primer salto, el estudiante es acompañado por dos instructores, que monitorean y corrigen su posición durante el otoño. Los siguientes cinco saltos pueden ir acompañados por un solo instructor, el objetivo es poder saltar solo en el séptimo salto;
- El salto en tándem implica saltar sobre un instructor ya experimentado. Esto le permite descubrir las sensaciones de caída libre para cualquier persona mayores de quince años. El salto tiene lugar a una altitud de al menos 3.000 metros para una caída libre de unos cincuenta segundos, antes de que el paracaídas se abra a una altitud de 1.500 metros. Te permite descubrir el deporte en un solo salto.

**Prerrogativas de BPJEPS**

- Diseñar e implementar acciones de animación, iniciación, enseñanza, supervisión y progresión a la autonomía, garantizando la seguridad de los profesionales y de terceros;
- acompañar a los profesionales en el descubrimiento y el respeto del marco de práctica de paracaidismo;
- participar en el funcionamiento de la estructura.

*Para ir más allá* : orden de 11 de julio de 2011 por la que se establece la especialidad de "paracaídas" del BPJEPS.

**Condiciones de acceso a la formación que conduce al PJEPS:**

El interesado deberá:

- Ser mayores de edad
- Proporcione un formulario de registro con una fotografía
- Proporcione una fotocopia de un documento de identidad válido
- Si es necesario, presentar el certificado o certificados que justifiquen el aligeramiento de determinadas pruebas;
- Proporcionar un certificado médico de no contradictorio para la práctica deportiva de menos de un año de edad;
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Desarrollar pruebas de precertificación, si es necesario, de acuerdo con la certificación.
- Proporcionar copias del certificado censal y del certificado individual de participación en el Día de la Defensa y la Ciudadanía;
- Si es necesario, proporcione los documentos que justifiquen exenciones y equivalencias de la ley;
- para un certificado complementario, presentar una fotocopia del diploma que automese la inscripción en la formación o un certificado de inscripción para la formación que conduzca a dicho diploma;
- producir un certificado de tres años de experiencia continua en la práctica del paracaidismo, entregado por el Director Técnico Nacional de Paracaidismo;
- para la designación "tradicional", proporcionar el certificado de quinientos saltos de caída libre, incluyendo cien saltos en los últimos doce meses antes de la fecha del expediente de registro, emitido por el Director Técnico Nacional de Paracaidismo;
- para la mención de "avances acompañados en una caída", proporcionar el certificado de ochocientos saltos de caída libre, incluyendo cien saltos en los últimos doce meses antes de la fecha de presentación del expediente de registro, emitido por el Director Técnico Nacional de la paracaidismo;
- para la designación "tándem", proporcionar el certificado de mil saltos de caída libre, incluyendo cien saltos en los últimos doce meses antes de la fecha del archivo de registro, y un salto en tándem en una posición de estudiante que data de menos de seis meses bajo la guía de un instructor en tándem autorizado por el Director Técnico Nacional de Paracaidismo;
- proporcionar el certificado de éxito en las pruebas técnicas y de seguridad de dos partes, emitido por el Director Técnico Nacional de Paracaidismo;
- cumplir con los requisitos preeducativos atestiguados por el jefe de la organización de capacitación durante una sesión de salto basada en la referencia. El candidato debe ser capaz de:- para movilizar en la práctica los conocimientos relacionados con la seguridad,
  - movilizar los conocimientos técnicos para verificar la aeronavegabilidad del equipo,
  - para preparar los materiales y herramientas de enseñanza,
  - para demostrar dominio técnico,
  - para explicar las diferentes técnicas,
  - dominar las técnicas profesionales,
  - para enmarcar la práctica del descubrimiento de la actividad hasta la autonomía,
  - para introducir el marco de práctica,
  - para evaluar su acción.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35 y A. 212-36 del Código del Deporte; Apéndices III, IV y V del decreto del 11 de julio de 2011 por el que se crea la especialidad "paracaidismo" del BPJEPS.

##### BPJEPS especialidad "actividades náuticas" mención multivalente "paracaídas de ascenso náutico"

Las "actividades náuticas" del BPJEPS atestiatan la posesión de las competencias profesionales esenciales para el ejercicio del instructor de actividades náuticas. Es un grado de nivel IV (como una licenciatura técnica, una licenciatura tradicional o un certificado de técnico).

Este diploma se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr).

La formación puede ser común a varias disciplinas de actividades náuticas, pero el candidato debe elegir la mención adecuada para su proyecto. Con respecto al paracaidismo de ascenso náutico, esta es la referencia multivalente al "paracaidismo de ascenso náutico" (Grupo G).

*Para ir más allá*: Artículo D. 212-20 del Código del Deporte; Artículos 1 y 2 de la orden del 9 de julio de 2002 por el que se crea la especialidad "actividades náuticas" del PJEPS.

**Es bueno saber**

La unidad de capitalización complementaria (UCC) "paracaidismo de ascenso náutico" permite enmarcar esta actividad y se puede asociar con cada una de las menciones de las "actividades náuticas" del BPJEPS.

**Prerrogativas bpJEPS:**

- mentores y actividades de descubrimiento e iniciación, incluidos los primeros niveles de competencia;
- Participar en la organización y gestión de su negocio
- Participar en el funcionamiento de la estructura organizadora de las actividades;
- participar en el mantenimiento y mantenimiento de los equipos.

*Para ir más allá*: Artículo 3 de la orden del 9 de julio de 2002 por el que se crea la especialidad de BPJEPS de "actividades náuticas".

**Condiciones de acceso a la formación que conduce al PJEPS:**

El interesado deberá:

- Ser mayores de edad
- Proporcione un formulario de registro con una fotografía
- Proporcione una fotocopia de un documento de identidad válido
- presentar el certificado o certificados que justifiquen el aligeramiento de determinadas pruebas;
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Desarrollar pruebas de precertificación, si es necesario, de acuerdo con la certificación.
- Proporcionar copias del certificado censal y del certificado individual de participación en el Día de la Defensa y la Ciudadanía;
- Si es necesario, proporcione los documentos que justifiquen exenciones y equivalencias de la ley;
- para un certificado complementario, presentar una fotocopia del diploma que automese la inscripción en la formación o un certificado de inscripción para la formación que conduzca a dicho diploma;
- proporcionar un certificado médico de no contradictorio con la práctica de actividades náuticas de menos de tres meses de edad, previa entrada en formación;
- Poseer la unidad docente PSC1 o su equivalente;
- presentar un certificado de éxito con los requisitos previos relacionados con la práctica personal del candidato y expedido por el director técnico nacional de la Federación Francesa de Paracaidistas de que se trate o por un experto designado por el director regional de la juventud, deportes y ocio;
- Ser titular de una licencia "costera" de mar o de aguas interiores;
- Ser capaz de gestionar el vuelo del jugador, en un mínimo de cinco vuelos, en el modo de práctica elegido (playa o barco de plataforma);
- requisitos preeducativos. El candidato debe ser capaz de:- gestionar las diferentes fases del vuelo de paracaidismo de ascenso náutico, en un modo de práctica de su elección (playa o barco de plataforma),
  - conocer el material específico del modo de práctica elegido,
  - recordar las regulaciones y normas de seguridad vigentes,
  - para hacerse cargo de un grupo en una sesión introductoria,
  - gestionar el área de práctica y la evolución del tractor teniendo en cuenta las limitaciones de seguridad,
  - para realizar una intervención con un practicante en dificultad.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35 y A. 212-36 del Código del Deporte; Artículo 5, Apéndice III y IV de la orden de 9 de julio de 2002 por el que se crea la especialidad "actividades náuticas" del PJEPS.

##### ESPECIALIDAD DEJEPS "desarrollo deportivo" mención "paracaidismo"

DeJEPS un diploma estatal registrado en el Nivel III de la[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Da fe de la adquisición de una cualificación en el ejercicio de una actividad profesional de coordinación y supervisión con fines educativos en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Este diploma se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr) y el Artículo D. 212-40 del Código del Deporte.

*Para ir más allá*: Artículo D. 212-35 del Código del Deporte.

**Prerrogativas DEJEPS:**

- Diseñar un proyecto escolar de paracaidismo
- Coordinar la ejecución del proyecto de la escuela de paracaidismo;
- Mentoring profesionales de todos los niveles en un proceso de desarrollo deportivo;
- Organizar una sesión de salto seguro
- gestionar el equipo utilizado por los profesionales para garantizar su seguridad.

**Condiciones de acceso a la formación que conduce a DEJEPS:**

El interesado deberá:

- Ser mayores de edad
- Proporcione un formulario de registro con una fotografía
- Proporcione una fotocopia de un documento de identidad válido
- Si es necesario, presentar el certificado o certificados que justifiquen el aligeramiento de determinadas pruebas;
- Proporcionar un certificado médico de no contradictorio para la práctica deportiva de menos de un año de edad;
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Desarrollar pruebas de precertificación, si es necesario, de acuerdo con la certificación.
- Proporcionar copias del certificado censal y del certificado individual de participación en el Día de la Defensa y la Ciudadanía;
- Si es necesario, proporcione los documentos que justifiquen exenciones y equivalencias de la ley;
- para un certificado complementario, presentar una fotocopia del diploma que automese la inscripción en la formación o un certificado de inscripción para la formación que conduzca a dicho diploma;
- proporcionar la certificación de una práctica continua de paracaidismo durante 4 años, incluyendo al menos ochocientos saltos emitidos por el Director Técnico Nacional de Paracaidismo;
- proporcionar la certificación de la experiencia de coaching en paracaidismo con una duración mínima de 600 horas en los últimos cuatro años antes de la entrada en formación, entregado por el Director Técnico Nacional de Paracaidismo;
- cumplir con los requisitos preeducativos verificados al establecer una sesión pedagógica de medio día seguida de una entrevista de 30 minutos. El candidato debe ser capaz de:- evaluar los riesgos objetivos asociados con la práctica,
  - para anticipar los riesgos potenciales asociados con la actividad para el practicante,
  - controlar el comportamiento y las acciones a realizar en caso de accidente o incidente,
  - para llevar a cabo una sesión segura de "saltos escolares".

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35 y A. 212-36 del Código del Deporte; Artículos 3, 4 y 6 del decreto del 11 de julio de 2011 por el que se crea la designación de "desarrollo deportivo" de la especialidad deJEPS "paracaidismo".

##### DeSJEPS especialidad "rendimiento deportivo" mención "paracaidismo"

El DESJEPS es un diploma estatal superior inscrito en el Nivel II de la[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Atestigua la adquisición de una cualificación en el ejercicio de una actividad profesional de especialización técnica y gestión con fines educativos en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Este diploma se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr) y el Artículo D. 212-56 del Código del Deporte.

*Para ir más allá*: Artículo D. 212-51 del Código del Deporte.

**Prerrogativas DESJEPS:**

- Preparar un proyecto de rendimiento estratégico
- Pilotar un sistema de entrenamiento
- Ejecución de un proyecto deportivo
- Evaluar un sistema de formación
- organizar, facilitar y evaluar las acciones de formación de los formadores.

*Para ir más allá* : decreto de 11 de julio de 2011 por el que se crea la palabra "paracaídas" del DESJEPS.

**Condiciones de acceso a la formación que conduzca a DESJEPS:**

El interesado deberá:

- Ser mayores de edad
- Proporcione un formulario de registro con una fotografía
- Proporcione una fotocopia de un documento de identidad válido
- Si es necesario, presentar el certificado o certificados que justifiquen el aligeramiento de determinadas pruebas;
- Proporcionar un certificado médico de no contradictorio para la práctica deportiva de menos de un año de edad;
- para las personas con discapacidad, proporcionar la opinión de un médico aprobado por la Federación Francesa de Handisport o por la Federación Francesa de Deporte Adaptado o nombrado por el Comité de Los Derechos y Autonomía de las Personas con Discapacidad sobre la necesidad Desarrollar pruebas de precertificación, si es necesario, de acuerdo con la certificación.
- Proporcionar copias del certificado censal y del certificado individual de participación en el Día de la Defensa y la Ciudadanía;
- Si es necesario, proporcione los documentos que justifiquen exenciones y equivalencias de la ley;
- para un certificado complementario, presentar una fotocopia del diploma que automese la inscripción en la formación o un certificado de inscripción para la formación que conduzca a dicho diploma;
- producir la certificación de una práctica continua de paracaidismo durante cinco años, incluyendo al menos mil saltos emitidos por el Director Técnico Nacional de Paracaidismo;
- producir una certificación de prueba consistente en el análisis técnico de un documento de vídeo sobre una disciplina de competencia en paracaídas emitida por el Director Técnico Nacional de Paracaidismo;
- producir un certificado de experiencia formativa en el campo del paracaidismo con una duración mínima de 150 horas durante los últimos cinco años emitido por el Director Técnico Nacional de Paracaidismo;
- cumplir con los requisitos preeducativos verificados al establecer una sesión de capacitación o una acción de entrenamiento de medio día seguida de una entrevista de 30 minutos. El candidato debe ser capaz de:- evaluar los riesgos objetivos asociados con la práctica de la disciplina,
  - para evaluar los riesgos objetivos asociados con la actividad del practicante,
  - para controlar el comportamiento y las acciones a tomar en caso de incidente o accidente,
  - implementar una sesión de capacitación o una acción de capacitación.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá*: Artículos A. 212-35 y A. 212-36 del Código del Deporte; Artículos 3, 4 y 5 del decreto del 11 de julio de 2011 por el que se crea la designación de "paracaídas" de la especialidad DESJEPS "desarrollo deportivo".

#### Costos asociados con la calificación

Se paga la formación para CQP, BPJEPS, DEJEPS y DESJEPS. El costo varía dependiendo de las menciones y las organizaciones de capacitación.

Para más detalles, es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como instructor de paracaidismo en Francia para personas que hayan sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Obligación de presentación de informes (con el fin de obtener la tarjeta de educador deportivo profesional)

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial](https://eaps.sports.gouv.fr).

**hora**

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

**Documentos de apoyo**

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

**Autoridad competente**

El prefecto de la región de Provenza-Alpes-Costa Azul.

**hora**

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

**Documentos de apoyo**

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y siguientes, A. 212-209 y Apéndice II-12-3 del Código del Deporte.

### c. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

**Autoridad competente**

El prefecto de la región de Provenza-Alpes-Costa Azul.

**hora**

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

**Documentos de apoyo para la primera declaración de actividad**

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

**Evidencia para una declaración de renovación de la actividad**

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182, , A. 212-209 y Listas II-12-2-a y II-12-b del Código del Deporte.

### d. Medidas de compensación

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

