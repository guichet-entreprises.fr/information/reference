﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| Copyright © Guichet Entreprises - All Rights Reserved
| 	All Rights Reserved.
| 	Unauthorized copying of this file, via any medium is strictly prohibited
| 	Dissemination of this information or reproduction of this material
| 	is strictly forbidden unless prior written permission is obtained
| 	from Guichet Entreprises.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->

<!-- var(key)="DQP262" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Paisajista de diseño" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="paisajista-de-diseno" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/paisajista-de-diseno.html" -->
<!-- var(last-update)="2020-04-15 17:21:12" -->
<!-- var(url-name)="paisajista-de-diseno" -->
<!-- var(translation)="Auto" -->



Paisajista de diseño
====================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:12<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El diseñador paisajista, conocido internacionalmente como*arquitecto paisajista*, es un gestor de vida y especialista en proyectos territoriales. Especialista de alto nivel en el corazón de las cuestiones ambientales y de desarrollo, las tareas que se le han encomendado se encuentran principalmente en dos niveles:

- **misiones de estudio, consultoría, acompañamiento y programación como asistente del propietario:** el paisajista de diseño lleva a cabo, como experto o mediador, más a menudo en nombre de las autoridades locales, la planificación, la programación, la planificación y los estudios ambientales (esquemas de coherencia territorial, planes locales urbanismo, planes de guía, planes para la protección y mejora de espacios naturales, charters y planes paisajísticos, atlas paisajísticos). También dirige talleres (alrededor de un proyecto territorial, un proyecto urbano o un espacio público). También puede acompañar al propietario en el proceso de búsqueda de un contratista principal (ayuda en la redacción de una especificación, la organización de la competencia);
- **misiones operativas de diagnóstico, diseño y gestión de proyectos como contratista principal, agente o cocontratista:** Sobre la base de un diagnóstico inicial, el paisajista de diseño diseña y gestiona desarrollos completos, en todo tipo de espacios y a todas las escalas, desde la parcela hasta el gran territorio: proyectos urbanos, eco-barrios, subdivisiones, zonas espacios comerciales e industriales, espacios públicos urbanos, suburbanos y rurales (lugares, carriles, parques, jardines, paseos marítimos, caminos, infraestructuras de transporte y energía (inserción y entorno), patrimonio y sitios turísticos, espacios rurales, agrícolas y forestales, también proporciona misiones de gestión y vigilancia.

Al contribuir de manera significativa a mejorar el medio ambiente con vistas al desarrollo sostenible, participan activamente en la aplicación del Convenio Europeo del Paisaje.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La Ley No 2016-1087, de 8 de agosto de 2016, para la reconquista de la biodiversidad, la naturaleza y los paisajes (Artículo 174) creó el título de diseñador paisajístico. Convierte la profesión paisajística en una profesión regulada en el ámbito de aplicación de la Directiva 2005/36/CE relativa al reconocimiento de cualificaciones profesionales. Es el uso del título de paisajista de diseño que está regulado y no el acceso a la profesión. Esta regulación del título de diseñador de paisajes no crea un monopolio de actividad.

La aplicación de la normativa se enmarca en los siguientes textos:

- [Decreto No 2017-673 de 28 de abril de 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034517176&categorieLien=id)  relacionados con el uso del título de diseñador de paisajes;
- [orden del 28 de agosto de 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000035589400&categorieLien=id) establecer las condiciones para solicitar y expedir la autorización para utilizar el título de paisajista de las personas mencionadas en el Decreto No 2017-673, de 28 de abril de 2017, relativo a la utilización del título de paisajista de diseño.

#### Entrenamiento

El paisajista de diseño cuenta con una formación multidisciplinar centrada en el diseño de entornos de vida y proyectos territoriales. Su formación le permite movilizar, a través del enfoque del proyecto paisajístico, desde el orden hasta su implementación, el conocimiento en los campos de las ciencias y tecnologías de la vida, la economía, las ciencias ecológicas, humanas y sociales y las artes y la creación.

La formación de diseño en el paisaje se imparte en cinco escuelas superiores nacionales. Su acceso se realiza en un concurso nacional común, después de revisar el expediente del candidato, completado por una entrevista con posibles pruebas.

Cuatro escuelas entregan el[Diploma estatal como paisajista](https://www.francecompetences.fr/recherche_certificationprofessionnelle/), obteniendo un máster, después de un curso de 3 años accesible para bac 2. Creado en 2014, sucede al diploma de paisajista graduado por el gobierno (DPLG landscaper). Estas escuelas son:

- [Escuela Nacional de Paisaje Superior de Versalles-Marsella](http://www.ecole-paysage.fr/) ;
- [INSA Centre-Val-de-Loire School of Nature and Landscape](http://www.insa-centrevaldeloire.fr/) ;
- [Escuela Nacional de Posgrado de Arquitectura y Paisaje de Burdeos](http://www.bordeaux.archi.fr/) ;
- [Escuela Nacional de Graduados de Arquitectura y Paisaje de Lille](http://www.lille.archi.fr/).

Una escuela emite el diploma de ingeniero paisajista: diploma que vale la pena un título de maestría después de una formación en 3, 4 o 5 años, accesible en post-bac o más.

- [Instituto Superior de Ciencias Agrícolas, Agroalimentarias, Hortícolas y Paisajísticas (AGROCAMPUS WEST)](https://www.agrocampus-ouest.fr/).

#### Costos relacionados

Las tasas de formación oscilan entre 601 y 1.987 euros al año para los no estudiantes. Para más información, es aconsejable acercarse a las diferentes escuelas que proporcionan la formación.

### b. Nacionales europeos: para el ejercicio temporal y ocasional (prestación gratuita de servicios (LPS))

Los nacionales de un Estado miembro de la Unión Europea u otro Estado parte en el Acuerdo sobre el Espacio Económico Europeo podrán prestar libremente servicios de diseño paisajístico en Francia de forma ocasional y temporal.

En este caso, el servicio se lleva a cabo bajo el título profesional del Estado miembro del establecimiento, que se menciona en la lengua oficial o en una de las lenguas oficiales del Estado miembro del establecimiento con el fin de evitar confusiones con el título de paisajista. En caso de que el título profesional no exista en el Estado miembro del establecimiento, el nacional menciona su título de formación en la lengua oficial o en una de las lenguas oficiales del Estado miembro del establecimiento.

No se requiere informeprevio para un ejercicio temporal y ocasional (LPS).

### c. Nacionales de la UE: para un ejercicio permanente (establecimiento libre (LE))

Los pasos necesarios para obtener permiso para usar el título del paisajista del diseñador se describen en la sección 5.b. a continuación.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

A " [repositorio de buenas prácticas](https://norminfo.afnor.org/norme/BP%20X50-787/paysagiste-concepteur-definition-de-la-profession-et-de-ses-modalites-dexercice/84824) Afnor (BP X50 787) fue creado por la Federación Francesa del Paisaje en 2009.

4°. Legislación social y seguro
----------------------------------------------------

Para ejercer de forma permanente en Francia, el paisajista de diseño no está sujeto a la obligación legal de sacar un seguro.

Sin embargo, puede tener que hacerlo dependiendo de la finalidad de su misión o de la naturaleza de su contrato con el propietario (por ejemplo, un seguro de responsabilidad profesional que opera y/o decadal).

5°. Procedimientos y formalidades
-------------------------------------------

Las personas con un título o experiencia profesional adquirida en otro Estado miembro de la Unión Europea o en un Estado parte en el acuerdo sobre el Espacio Económico Europeo, ya establecido en Francia o que deseen establecerse allí permanentemente, deberá presentar su solicitud de autorización ante el Ministerio de Transición Ecológica y Solidaria, que se encarga de recibir y escuchar las solicitudes de autorización relativas al uso de la denominación paisajista de diseño. Toda la información, así como un[instrucciones descargables](https://www.ecologique-solidaire.gouv.fr/politique-des-paysages#e7), están disponibles en el sitio web del ministerio.

### a. Criterios de evaluación

Para obtener permiso para utilizar la designación Design Landscaper, los solicitantes deben tener las siguientes habilidades:

- **capacidad de diseñar el paisaje a través de un enfoque de proyecto paisajístico:**- Ser capaz de interpretar espacialmente una planificación y un problema de la tierra cuestionando y priorizando los elementos de un diagnóstico,
  - ser capaz de diseñar el mantenimiento, mejora, evolución, adaptación o transformación de paisajes,
  - Saber definir una estrategia eligiendo o proponiendo de manera argumentativa un enfoque y modus operandi apropiados; identificar indicadores a corto, mediano y largo plazo para medir los efectos esperados,
  - Ser capaz de inventar un enfoque y crear sus propias herramientas, ser creativos y movilizar la intuición para avanzar propuestas relevantes y justas,
  - ser capaces de proponer desarrollos sostenibles y sostenibles, imaginar espacios y métodos de gestión a lo largo del tiempo y a lo largo del tiempo, teniendo en cuenta en particular el impacto cíclico y aleatorio de los usos, estaciones y climas,
  - demostrar capacidades proyectivas a todas las escalas,
  - ser capaz de calificar, definir, representar configuraciones espaciales y establecer requisitos sobre la relación entre los espacios construidos y externos, para especificar los componentes materiales del proyecto de acuerdo con las intenciones compatible con las condiciones ecológicas encontradas,
  - Ser capaz de tener en cuenta las aspiraciones y representaciones de las poblaciones a lo largo del proyecto, desde el diagnóstico hasta la transcripción técnica;
- **capacidad de movilizar y articular el conocimiento general del paisaje:**- ser capaz de movilizar el conocimiento general relacionado con el paisaje y sus características históricas y actuales (agricultura, parques y jardines, artes visuales, arquitectura, arte urbano, planificación urbana, planificación) así como conocimientos científicas y técnicas relacionadas con los paisajes (geomorfología, hidrografía, agronomía, horticultura, ecología, geografía natural y humana, etc.) y ciertos principios de ingeniería de interés para el paisaje (saneamiento alimentado por lluvias, tratamiento del suelo, apoyo, movimientos de tierra, plantaciones),
  - Ser capaz de articular los conocimientos, conocimientos y prácticas artísticas, científicas y técnicas adquiridas en formación y/o a lo largo de la experiencia profesional,
  - Tener conocimiento de las partes interesadas, cómo intervenir, marcos y herramientas institucionales y regulatorias,
  - Conocer las políticas públicas en el panorama, la planificación urbana y la planificación;
- **capacidad para desarrollar un diagnóstico de territorios y entender las cuestiones territoriales:**- ser capaz de desarrollar un diagnóstico sensible de los territorios: identificar, describir, analizar y caracterizar un paisaje o territorio a través de sus diversos componentes (enfoque multidisciplinar y multiescalary a un sitio), su características (geomorfológica, hidrológica, agrícola, humana, patrimonial), su dinámica en el trabajo, y esto a todas las escalas,
  - saber describir los elementos permanentes, invariables y mutables y analizar la evolución dinámica de un paisaje a lo largo del tiempo,
  - saber cómo llevar a cabo investigaciones e investigaciones documentales,
  - saber cómo llevar a cabo investigaciones de campo y encuestas de diferentes tipos,
  - saber cómo hacer un buen uso de los estudios y diagnósticos existentes,
  - saber analizar los principales problemas y problemas relacionados con los territorios y poder priorizarlos,
  - las cuestiones del paisaje en el marco más amplio de las cuestiones sociales, ambientales, económicas, políticas y patrimoniales,
  - saber identificar a todas las partes interesadas e informar sobre sus puntos de vista, intenciones, equilibrios de poder, medios de acción respectivos, estrategias y modos de intervención,
  - Ser capaz de capturar las percepciones espaciales y culturales de las partes interesadas, representaciones y proyecciones de su entorno y entorno de vida;
- **capacidad de comunicar, expresar y mediar en situaciones paisajísticas:**- ser capaz de expresar claramente el diagnóstico de una situación paisajística articulando personajes sensibles, organización espacial física y humana, elementos vivos, evolución morfológica e histórica y describiendo los factores y agentes El trabajo,
  - podrá describir una situación y sus cuestiones desde varios puntos de vista complementarios o contradictorios, distinguiendo las intenciones individuales y las cuestiones colectivas y expresando una situación de acuerdo con un análisis multicriterio,
  - ser capaz de traducir las percepciones y representaciones de las personas en una forma apropiada, permitiendo la acción y la toma de decisiones,
  - Ser capaz de comunicarse, ser pedagógico, transmitir conocimientos e información adaptando formas de comunicación a audiencias y socios,
  - saber representar y expresar con precisión una situación a través del dominio y uso adecuado de diferentes herramientas de comunicación (escritas, gráficas, plásticas, etc.),
  - Ser capaz de negociar y discutir
- **capacidad de anticipar la evolución de un paisaje:**- ser capaz de predecir e integrar elementos dinámicos, evolutivos y variables (flujos y usos, peligros naturales) en el diseño
  - Ser capaz de anticipar y simular la evolución de un paisaje bajo el efecto acumulativo de las intervenciones de diferentes actores a lo largo del tiempo, a corto, medio y largo plazo,
  - Ser capaz de expresar una visión prospectiva mediante el desarrollo de escenarios evolutivos e imaginando diferentes modos de acción en el paisaje,
  - Ser capaz de comprender y anticipar los cambios sociales, culturales y ecológicos;
- **capacidad de asumir el control operativo y trabajar en un equipo profesional multidisciplinario:**- comprender las modalidades y condiciones de construcción en el contexto de una operación de gestión de proyectos (escritura de documentos contractuales, precisión de documentos y dibujos, elección de materiales y materiales (vegetales responsabilidades y seguros),
  - conocer los vínculos y complementariedades entre paisajistas de diseño y otros profesionales (relaciones entre gestión de proyectos y gestión de proyectos, relaciones con cocontratistas en gestión de proyectos, conocimiento de responsabilidades respectivamente en obras),
  - Ser capaz de trabajar en equipo,
  - Ser capaz de discernir los límites de las habilidades de uno y recurrir a uno o más conocimientos especializados complementarios;
- **capacidad para manejar múltiples situaciones profesionales:**- ser capaz de asumir varias situaciones profesionales específicas de la profesión paisajística en el sector privado o público o parapúblico (contratista, contratista principal, asistente de contratista, consultor, mediador),
  - ser capaz de dirigir muchas de las siguientes misiones (diseño, ingeniería, pilotaje, planificación, consultoría, estudio, enseñanza, investigación, conciencia).

Estas capacidades se enumeran en el artículo 2 de la[orden del 28 de agosto de 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000035589400&categorieLien=id) establecer las condiciones para solicitar y expedir la autorización para utilizar el título de paisajista de las personas mencionadas en el Decreto No 2017-673, de 28 de abril de 2017, relativo a la utilización del título de paisajista de diseño y su anexo.

### b. Documentos de apoyo que se facilitarán

Los documentos justificativos que se proporcionarán son:

- Una copia legible de un documento de identidad válido
- Una copia legible de diplomas, certificados de diplomas, certificados, autorizaciones o títulos obtenidos;
- Un CV
- Una carta en la que se esbozan las motivaciones del solicitante
- Una lista de referencias al trabajo y estudios realizados que muestren su duración, el orden de magnitud de los presupuestos y la situación y responsabilidad del solicitante;
- un certificado de la autoridad competente del Estado miembro, cuando exista, que certifique la duración del ejercicio profesional con las fechas correspondientes.

Los documentos antes mencionados están escritos en francés o acompañados por su traducción al francés por un traductor jurado

Si la autoridad competente no está en condiciones de pronunciarse únicamente sobre la base de estas pruebas documentales, podrá requerir elementos adicionales. Por ejemplo, puede solicitar la descripción ilustrada y bien argumentada de las capacidades esperadas de algunos proyectos emblemáticos y/o estudios realizados por el solicitante, donde su función personal y método de trabajo son claros.

Si lo desean, los solicitantes podrán, desde la primera presentación, enviar información adicional que permita a la autoridad competente evaluar su nivel de competencia de la mejor manera posible.

### c. Cómo presentar la solicitud de autorización

Los documentos de apoyo deben enviarse por correo electrónico como un único documento pdf para:[paysagiste-concepteur@developpement-durable.gouv.fr](mailto:paysagiste-concepteur@developpement-durable.gouv.fr).

Si el solicitante no tiene una dirección de correo electrónico, puede ser enviado por carta recomendada con acuse de recibo a:

<p style="margin-left: 2em">Ministère de la Transition écologique et solidaire<br>
Direction de l’Habitat, de l’Urbanisme et des Paysages<br>
Sous-direction de la qualité du cadre de vie<br>
Bureau des paysages et de la publicité<br>
92055 Paris La Défense Cedex</p>### d. Tiempo de respuesta

Respuesta dentro de los cuatro meses siguientes a la recepción de una solicitud que incluye todos los documentos de apoyo y cumple con las expectativas.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

#### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

#### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

#### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

#### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

#### Costo

Gratis.

#### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

#### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

Seis grados. Textos de referencia
---------------------------------

[Decreto No 2017-673 de 28 de abril de 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034517176&categorieLien=id) con respecto al uso del título de diseñador de paisajes.

[Arrestado 28 de agosto de 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000035589400&categorieLien=id) establecer las condiciones para solicitar y expedir la autorización para utilizar el título de paisajista de las personas mencionadas en el Decreto No 2017-673, de 28 de abril de 2017, relativo a la utilización del título de paisajista de diseño.

