﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP087" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Transportador de animales para equinos y animales domésticos de especies de ganado bovino, ovino, porcino, caprino y avícola" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="transportador-de-animales-para" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/transportador-de-animales-para-equinos-y-animales-domesticos.html" -->
<!-- var(last-update)="2020-04-15 17:20:52" -->
<!-- var(url-name)="transportador-de-animales-para-equinos-y-animales-domesticos" -->
<!-- var(translation)="Auto" -->




Transportador de animales para equinos y animales domésticos de especies de ganado bovino, ovino, porcino, caprino y avícola
============================================================================================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:52<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El transportador de animales vivos (definido en el artículo 2 del Reglamento (CE) 1/2005) para los equinos domésticos y los animales domésticos de las especies bovina, caprina, ovina, porcina y avícola (los llamados "animales de alquiler") interviene en el transporte de animales durante el viaje:

- no está sujeto a una emergencia veterinaria;
- es más de 65 km
- se lleva a cabo como parte de una actividad económica.

Sus principales tareas son:

- mantener y garantizar el bienestar de los animales transportados;
- garantizar su riego y alimentación;
- proporcionar primeros auxilios, si es necesario, a los animales que están heridos o enferman mientras son transportados.

Durante el transporte, el transportador puede dedicarse exclusivamente a estas misiones o, en su defecto, ser:

- El cliente en el lugar de salida hasta que se incluya la carga;
- El destinatario en el destino de la descarga incluido;
- Responsable del punto de parada, incluida la carga y descarga;
- en cualquier otro momento del viaje.

**Tenga en cuenta que**

El transporte deberá efectuarse de conformidad con las condiciones de los anexos I y II de la[Reglamento (CE) No 1/2005 del Consejo, de 22 de diciembre de 2004](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32005R0001).

*Para ir más allá*: Artículo R. 214-55 del Código Rural y Pesca Marina.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de cinta transportadora está reservada al profesional titular de un certificado de cualificaciones expedido por la Dirección Departamental encargada de la protección de la población (DDPP).

La expedición de este certificado de competencia puede obtenerse siempre que la persona esté justificada:

- o han sido sometidos a una formación específica impartida por el[organismo autorizado](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032743454&categorieLien=id) Ministro de Agricultura y habiendo aprobado la evaluación asociada;
- o poseer uno de los diplomas, títulos o certificados en la Parte 1 del calendario de la orden del 12 de noviembre de 2005.

En caso necesario, cuando el transportador también sea responsable del transporte de los animales, deberá disponer de un permiso adaptado al tipo de vehículo conducido.

**Tenga en cuenta que**

El transportista que emplee un transportador de animales vivos debe tener una autorización de transporte válida. Por otro lado, cuando el transportador actúe en su propio nombre, tendrá que solicitar esta autorización de transporte rellenando el formulario[Cerfa 15714*01](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15714.do) que se referirá al DDPP territorialmente competente.

*Para ir más allá*: Artículos 1 y 2 de la[decreto de 12 de noviembre de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031521436&dateTexte=20171114) en relación con los aclaramientos o registros de las organizaciones de formación que ejecutan la formación necesaria para las personas que desempeñan una función de transporte directo de animales y el artículo 6, párrafo 5, de la[1/2005 Reglamento del Consejo de 22 de diciembre de 2004](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:32005R0001) protección de los animales durante el transporte y las operaciones conexas.

#### Entrenamiento

La formación debe centrarse en los aspectos técnicos y reglamentarios de la legislación sobre la protección de los animales en el transporte, incluidos:

- Aptitud, condiciones generales y documentos para el transporte de animales vivos;
- la fisiología de los animales, sus necesidades alimentarias, el riego, su comportamiento y el concepto de estrés;
- Los aspectos prácticos del manejo de animales
- El impacto de la conducción en el bienestar de los animales que se transportan y en la calidad de la carne;
- Cuidado animal de emergencia
- Aspectos de seguridad para el personal que manipula animales;
- viajes a largo plazo y el cuaderno de bitácora.

La duración mínima del entrenamiento es de 14 horas para una categoría de animales. Esta duración mínima se añade a un mínimo de tres horas por categoría adicional de animales.

El Certificado de Habilidades se emite como resultado de la capacitación y el éxito en una prueba de evaluación.

*Para ir más allá*: Artículo 1 de la Orden de 12 de noviembre de 2015 y anexo IV del Reglamento de 22 de diciembre de 2004.

#### Costos asociados con la calificación

Se paga la formación para entrar en la profesión de transportador. Para obtener más información, es aconsejable acercarse a los organismos de formación autorizados por el Ministro responsable de la agricultura.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (prestación gratuita de servicios)

El nacional de un Estado miembro de la UE o del EEE, que ejerza la actividad de los animales transportadores para los equinos domésticos y los animales domésticos de las especies de ganado bovino, caprino, ovino, porcino y avícola en uno de estos Estados, podrá hacer uso de su título profesional en Francia, ya sea de forma temporal o casual. Debe hacer una declaración al prefecto de la región en la que desea practicar (véase más adelante "5o). a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año, durante los diez años que preceder el rendimiento.

Cuando el examen de las cualificaciones profesionales muestre diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y a su práctica en Francia, el interesado podrá someterse a una prueba de aptitud.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de un Estado miembro de la Unión Europea (UE) o parte en el Espacio Económico Europeo (EEE) que lleve a cabo legalmente la actividad de los animales transportadores en ese Estado podrá establecerse en Francia para llevar a cabo la misma actividad de forma permanente.

Para ello, deberá obtener un certificado de competencia del DDPP territorialmente competente (véase infra "5o. a. Obtener un certificado de competencia para la UE o el EEE nacional").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que está legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año en los diez años que preceder el rendimiento.

Si el examen de las cualificaciones profesionales revela diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y a su práctica en Francia, el interesado podrá estar sujeto a medidas de compensación (véase más adelante "5. a. Bueno saber: medidas de compensación").

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Las obligaciones éticas son responsabilidad del transportador animal, incluyendo garantizar la salud y la dignidad de los animales transportados.

4°. Seguro
-------------------------------

Como parte de sus funciones, el transportador de animales está obligado a realizar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus trabajadores por los actos realizados en ocasiones.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

#### a. Obtener un certificado de competencia para la UE o el EEE

**Autoridad competente**

El DDPP del lugar de residencia del nacional es competente para decidir sobre la expedición del certificado de competencia a la UE o al nacional del EEE.

**Documentos de apoyo**

El nacional enviará un archivo por correo a la autoridad competente que contenga los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- Una copia de un comprobante de residencia
- El formulario[Cerfa No. 15715*01](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15715.do), debidamente cumplimentado y firmado;
- prueba de calificación.

**Procedimiento**

Una vez que el DDPP haya llevado a cabo las auditorías del expediente y se haya asegurado de que la persona no haya sido objeto de una retirada o supresión de un certificado en su Estado de origen, podrá expedir el certificado de competencia.

**Tenga en cuenta que**

El certificado debe establecerse en el idioma del estado emisor, así como en inglés si es probable que el transportador de animales se traslade a otro estado.

*Para ir más allá* Sección 17 y Capítulo III de la Lista III del acuerdo del 22 de diciembre de 2004.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

