﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP126" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Epitesis" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="epitesis" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/epitesis.html" -->
<!-- var(last-update)="2020-04-15 17:21:28" -->
<!-- var(url-name)="epitesis" -->
<!-- var(translation)="Auto" -->


Epitesis
========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El epítista es un profesional de la salud que se especializa en el diseño, fabricación y adaptación de equipos faciales por prótesis externas (o epítesis).

Trabajando en colaboración con cirujanos cosméticos, plásticos, esmatólogos o maxilofaciales, realiza las prótesis una vez que el paciente ha sido operado, teniendo en cuenta los imperativos fisiológicos y anatómicos.

Tomará las huellas, esculpirá y moldeará la prótesis, y luego será capaz de encargarse de su pose final sobre el paciente.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de epitetista está reservada al titular del diploma estatal de epitetista.

No obstante, también podrán ejercer la profesión, las personas que justifiquen:

- poseer una licenciatura universitaria en prótesis facial aplicada que tenga un certificado que valide una experiencia profesional de tres años con un epitetista o un servicio de salud especializado en la fabricación de prótesis;
- una competencia profesional reconocida por el Ministerio de Veteranos y Víctimas de la Guerra, o agencias de seguro de salud;
- una competencia profesional reconocida por una comisión nacional de profesionales de equipos para discapacitados.

*Para ir más allá*: Artículos L. 4364-1, D. 4363-7 y los siguientes del Código de Salud Pública; Artículos 6 y 8 de la orden del 1 de febrero de 2011 sobre las profesiones de protésico y ortopedista para el dispositivo de las personas con discapacidad.

#### Entrenamiento

Hasta la fecha, sólo la Escuela de Medicina Pierre y Marie Curie, ubicada en París, ha emitido el diploma de prótesis facial aplicada que conduce a la profesión de epitetista. Su acceso está reservado:

- titulares del título médico estatal, especialistas en estomatología o cirugía maxilofacial;
- Titulares del grado estatal en cirugía dental;
- titulares de un título para ejercer la estomatología u odontología, expedido por un Estado miembro de la Unión Europea (UE) o parte en el Espacio Económico Europeo (EEE);
- titulares de un Certificado de Cualificación Profesional (CAP) o un Certificado de Estudios Profesionales (BEP) como protésico dental;
- titulares del certificado profesional (BP), el título profesional de grado o la patente técnica de los oficios (BTM) de las prótesis;
- personas que justifiquen cinco años de actividad profesional en un laboratorio de depilación, un laboratorio de epítismos o un equipo grande;
- titulares de la licencia del ocularista.

El estudiante tendrá que presentar un expediente a la universidad en cuestión y será sometido a una prueba práctica.

El estudiante seleccionado, habiendo superado con éxito esta prueba, se someterá a una formación teórica de 72 horas y 45 horas de trabajo práctico. Al final de su formación, será sometido a pruebas teóricas y prácticas cuyo éxito le permitirá adquirir el diploma de prótesis facial aplicada.

#### Costos asociados con la calificación

Se paga la formación que conduce al diploma de prótesis facial aplicada. Para obtener más información, es aconsejable acercarse a la Universidad Pierre y Marie Curie.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Un nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), que actúe legalmente como epítisto en uno de estos Estados, podrá utilizar su título profesional en Francia de forma temporal o ocasional.

Tendrá que solicitar, antes de su primera actuación, mediante declaración dirigida al prefecto de la región en la que desea realizar la entrega (véase infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá justificar haberla realizado en uno o varios Estados miembros durante al menos un año, en los diez años antes de la actuación.

*Para ir más allá*: Artículos L. 4364-6 y R. 4364-11-3 del Código de Salud Pública.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Todo nacional de un Estado de la UE o del EEE, establecido y que practique legalmente la actividad epiteista en ese Estado, podrá llevar a cabo la misma actividad en Francia de forma permanente si:

- posee un certificado de formación expedido por una autoridad competente de otro Estado miembro, que regula el acceso o el ejercicio de la profesión;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar una autorización individual para ejercer desde el prefecto regional en el que desee ejercer su profesión (véase infra "5o). b. Solicitar un permiso de ejercicio para el nacional de la UE o del EEE para la actividad permanente (LE) ").

*Para ir más allá*: Artículo L. 4364-5 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Un nacional que desee ejercer como epíteto en Francia está obligado a respetar las normas de buenas prácticas, entre ellas:

- el relativo al secreto profesional;
- practicar en locales accesibles para personas con discapacidad, preservar la privacidad del paciente y cumplir con las normas de higiene y seguridad;
- realizar la prescripción médica sin hacer cambios a menos que sean médica o técnicamente necesarios;
- para inscribir su diploma en el repertorio de Adeli (véase infra "5 grados). v. Registro en el directorio Adeli").

*Para ir más allá*: Artículos D. 4364-12 a D. 4364-18 del Código de Salud Pública; Artículos 13 a 26 de la orden de 1 de febrero de 2011.

4°. Seguro
-------------------------------

Como profesional de la salud, un epitetista liberal debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

#### Autoridad competente

El prefecto regional es responsable de decidir sobre la solicitud de una declaración previa de actividad.

#### Documentos de apoyo

La solicitud se realiza mediante la presentación de un archivo que incluye los siguientes documentos:

- Una copia de un documento de identidad válido
- Una copia del título de formación que permite ejercer la profesión en el estado de obtención;
- un certificado, de menos de tres meses de edad, de la autoridad competente del Estado de la UNIÓN o del EEE, que certifique que el interesado está legalmente establecido en ese Estado y que, cuando se expide el certificado, no existe ninguna prohibición, ni siquiera temporal, Ejercicio
- cualquier prueba que justifique que el nacional haya ejercido la profesión en un Estado de la UE o del EEE durante un año en los últimos diez años, cuando dicho Estado no regule la formación, el acceso a la profesión solicitada o su ejercicio;
- cuando el certificado de formación haya sido expedido por un tercer Estado y reconocido en un Estado de la UE o del EEE distinto de Francia:- reconocimiento del título de formación establecido por las autoridades estatales que han reconocido este título,
  - cualquier prueba que justifique que el nacional ha ejercido la profesión en ese estado durante tres años;
- Si es así, una copia de la declaración anterior, así como la primera declaración hecha;
- un certificado de responsabilidad civil profesional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

#### hora

Una vez recibido el expediente, el prefecto regional dispondrá de un mes para decidir sobre la solicitud e informará al nacional:

- que puede comenzar la actuación. A partir de entonces, el prefecto registrará al solicitante en el directorio Adeli;
- que estará sujeto a una medida de compensación si existen diferencias sustanciales entre la formación o la experiencia profesional del nacional y las requeridas en Francia;
- no podrá iniciar la actuación;
- cualquier dificultad que pueda retrasar su decisión. En este último caso, el prefecto podrá tomar su decisión en el plazo de dos meses a partir de la resolución de esta dificultad, y a más tardar tres meses de notificación al nacional.

El silencio del prefecto dentro de estos plazos valdrá la pena aceptar la solicitud de declaración.

**Tenga en cuenta que**

La rentabilidad es renovable cada año o en cada cambio en la situación del solicitante.

*Para ir más allá* : Orden de 8 de diciembre de 2017 relativa a la declaración previa de prestación de servicios para asesores genéticos, médicos y preparadores de farmacias y farmacias hospitalarias, así como para ocupaciones en el Libro III de Parte IV del Código de Salud Pública.

### b. Solicitar un permiso de ejercicio para la UE o el EEE nacional para la actividad permanente (LE)

#### Autoridad competente

La autorización de ejercicio es expedida por el prefecto regional, previa asesorada de la Comisión de Prostetistas y Ortopedistas para la adaptación de personas con discapacidad.

#### Documentos de apoyo

La solicitud de autorización se realiza mediante la presentación de un expediente que contiene todos los siguientes documentos:

- Una copia de una identificación válida y un documento en el que se mencione la información de contacto del nacional;
- Una carta de solicitud de autorización para ejercer;
- Una descripción detallada de la actividad profesional del nacional;
- referencias de los médicos con los que está acostumbrado a trabajar y sus evaluaciones de los beneficios proporcionados por el candidato;
- cualquier documentación que atestigue la experiencia laboral del empleador;
- en la medida de lo posible, una carta de su empleador describiendo su actividad con sus evaluaciones.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

#### Procedimiento

El prefecto reconoce la recepción del expediente en el plazo de un mes y decidirá tras tener el dictamen de la Comisión de Prótesis y Ortopedistas para la adaptación de personas con discapacidad. Este último es responsable de examinar los conocimientos y habilidades del nacional adquiridos durante su formación o durante su experiencia profesional. Puede someter al nacional a una medida de compensación.

El silencio guardado por el prefecto de la región en un plazo de cuatro meses es una decisión de rechazar la solicitud de autorización.

*Para ir más allá*: Artículos R. 4364-11 a R. 4364-11-2 del Código de Salud Pública Artículos 10-12 de la Orden de 1 de febrero de 2011.

**Bueno saber: medidas de compensación**

Si el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia profesional muestra diferencias sustanciales con las cualificaciones necesarias para acceder a la profesión de epitetista y su ejercicio en Francia, el interesado tendrá que someterse a una medida de indemnización.

En función del nivel de cualificación exigido en Francia y del que posea el interesado, la autoridad competente podrá:

- Ofrecer al solicitante la opción de elegir entre un curso de ajuste o una prueba de aptitud;
- imponer un curso de ajuste o una prueba de aptitud
- imponer un curso de ajuste y un alcierto.

*Para ir más allá*: Artículo L. 4364-5 del Código de Salud Pública.

### c. Registro en el directorio Adeli

Un nacional que desee ejercer como epítisto en Francia está obligado a registrar su autorización para ejercer en el directorio Adeli ("Automatización de las Listas").

#### Autoridad competente

El registro en el directorio de Adeli se realiza con la Agencia Regional de Salud (ARS) del lugar de práctica.

#### hora

La solicitud de inscripción se presenta en el plazo de un mes a partir de la toma del cargo del nacional, independientemente del modo de práctica (liberal, asalariado, mixto).

#### Documentos de apoyo

En apoyo de su solicitud de registro, el epíteto debe presentar un expediente que contenga:

- el título original o título que acredite la formación del epitetista expedido por el Estado de la UE o el EEE (traducido al francés por un traductor certificado, si procede);
- Id
- Formulario de ciervo 10906-06 completado, fechado y firmado.

#### Resultado del procedimiento

El número Adeli del nacional se mencionará directamente en la recepción del expediente, emitido por el ARS.

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 4364-2 y D. 4364-18 del Código de Salud Pública.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

