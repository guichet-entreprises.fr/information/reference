﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP252" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sector marítimo" -->
<!-- var(title)="Segundo mecánico de buques pesqueros" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sector-maritimo" -->
<!-- var(title-short)="segundo-mecanico-de-buques-pesqueros" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sector-maritimo/segundo-mecanico-de-buques-pesqueros.html" -->
<!-- var(last-update)="2020-04-15 17:22:19" -->
<!-- var(url-name)="segundo-mecanico-de-buques-pesqueros" -->
<!-- var(translation)="Auto" -->


Segundo mecánico de buques pesqueros
====================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:19<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El segundo ingeniero de buques pesqueros es el asistente directo del ingeniero jefe. Sus principales tareas son garantizar la propulsión del buque y dirigir a los oficiales y al personal de actuación del servicio de máquinas.

Como parte de sus funciones, el segundo ingeniero también planificará y llevará a cabo operaciones de mantenimiento, identificará los fallos del motor y realizará un seguimiento del trabajo realizado durante la navegación.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de segundo ingeniero de buques pesqueros está reservada al titular del certificado del segundo ingeniero, cumpliendo todas las condiciones siguientes:

- Tener al menos 20 años de edad el día en que se presente la solicitud de patente
- Poseer un certificado de aptitud médica
- Tener las cualificaciones profesionales necesarias
- han completado al menos 12 meses de servicio en el mar como oficial a cargo de la relojaina.

**Tenga en cuenta que**

El segundo certificado de mecánica tiene una validez de cinco años y debe renovarse con arreglo a las normas del auto de 24 de julio de 2013 relativas a la revalidación de los certificados de formación profesional marítima.

*Para ir más allá*: Artículo 10 de la[decreto de 19 de abril de 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032569477&dateTexte=20171208) patente de segundo mecánico y certificado de ingeniero jefe.

#### Entrenamiento

Para cumplir con los requisitos de cualificación profesional, el candidato para el segundo certificado de mecánico debe ser titular de:

- una de las siguientes patentes válidas:- certificado de oficial jefe de la máquina de vigilancia,
  - certificado como oficial a cargo de la vigilancia de buques marítimos,
  - patente enumerada en el cuadro 3 de la Orden de 19 de abril de 2016;
- uno de los siguientes grados válidos:- diploma como ingeniero jefe,
  - posgrado de la Marina Mercante,
  - diploma o certificado mencionado en el Cuadro 4 del decreto;
- certificado que certifica la validación de la educación médica de Nivel II (EM II).

*Para ir más allá*: Artículo 10 de la[decreto de 19 de abril de 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032569477&dateTexte=20171208) patente de segundo mecánico y certificado de ingeniero jefe.

#### Acondicionamiento físico

El acceso a la profesión de segundo ingeniero en buques pesqueros está sujeto a requisitos mínimos de aptitud que se evalúan durante las visitas médicas requeridas:

- antes del acceso a la profesión de segundo mecánico;
- Antes del primer embarque;
- antes de entrar en la formación marítima;
- antes de la expiración del certificado de aptitud médica, la duración de la cual varía según la edad del oficial.

Al final de la visita médica, el médico puede decidir que el segundo mecánico está en forma y se le dará un certificado de aptitud.

Por otra parte, cuando la decisión del médico indique una idoneidad parcial, incapacidad temporal o incapacidad total, podrá impugnarla ante la Comisión Médica Regional para la Aptitud para la Navegación (CMRA).

*Para ir más allá*: Artículo L. 5521-1 del Código de Transporte y[decreto del 3 de diciembre de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031560450) salud y aptitud médica para navegar.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (Servicio Gratuito)

Un nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), actuando legalmente como un segundo mecánico en buques pesqueros en uno de estos Estados, puede utilizar su título profesional en Francia como temporal o casual.

Tendrá que solicitarlo, antes de su primera actuación, mediante declaración dirigida al director interregional del mar competente de la región administrativa en la que se identifica (véase infra "4o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá justificar haberla realizado en uno o varios Estados miembros durante al menos un año, en los diez años antes de la actuación.

**Qué saber**

El ejercicio de la segunda mecánica en Francia, de forma temporal o casual, exige que el nacional posea todas las habilidades linguísticas necesarias.

Cuando existan diferencias sustanciales entre la formación del nacional y las exigidas en Francia, o cuando el interesado no haya adquirido todas las competencias necesarias para ejercer en un buque pesquero de bandera francesa, compensación (ver infra "4o. a. Bueno saber: medida de compensación").

*Para ir más allá*: Artículos 20 del año[decreto de 24 de junio de 2015](https://www.legifrance.gouv.fr/eli/decret/2015/6/24/DEVT1502017D/jo/texte) y 8 de los[decretado a partir del 8 de febrero de 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021844162&dateTexte=20171207).

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si:

- posee un título expedido por una autoridad competente de otro Estado miembro, que regula el acceso a la profesión o a su ejercicio;
- El título presentado tiene su equivalente exacto en Francia;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Una vez que cumpla una de las tres condiciones anteriores, tendrá que solicitar un certificado de reconocimiento del Director Interregional competente del Mar (véase infra "4o. b. Obtener un certificado de reconocimiento para los nacionales de la UE o del EEE para un ejercicio permanente (LE)).

Si, al revisar el expediente, el Director Interregional del Mar constata que existen diferencias sustanciales entre la formación profesional y la experiencia del nacional y las necesarias para operar en un buque con pabellón medidas de compensación (ver infra "4o. b. Bueno saber: medida de compensación"

*Para ir más allá*: Artículos 4 a 5 de la orden de 8 de febrero de 2010.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Una persona que desee ejercer como segundo mecánico debe respetar las condiciones morales y no haber sido condenada por una sentencia correccional o penal.

Para ello, deberá presentar un extracto de su historial delictivo de menos de tres meses o un certificado de menos de tres meses del Estado de la UE o del EEE, que certifique estas condiciones.

*Para ir más allá*: Artículos 8 y 9 del Decreto de 2 de junio de 2015.

4°. Procedimientos y formalidades de reconocimiento de cualificación
-----------------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

**Autoridad competente**

El Director Interregional del Mar en la región administrativa en la que el segundo ingeniero desea llevar a cabo la entrega o en la que se encuentra el puerto de armamento del buque pesquero, es competente para decidir sobre la declaración. Confirmará la recepción de la solicitud en el plazo de un mes a partir de la recepción del expediente.

**Renovación de la predeclaración**

La declaración debe renovarse una vez al año y en caso de que se produzca un cambio en la situación del nacional.

**Documentos de apoyo**

Para ejercer como segundo ingeniero en un buque pesquero, el nacional envía un expediente a la autoridad competente que contiene los siguientes documentos justificativos:

- Una declaración escrita firmada por el nacional;
- Una pieza de identificación válida para el nacional;
- un certificado del Estado de la eu o de la autoridad competente del EEE que certifique que el nacional está legalmente establecido en ese Estado y no incurre en ninguna prohibición de ejercer;
- Un certificado que justifique las cualificaciones profesionales del nacional;
- un certificado que justifique su actividad durante al menos dos años en los últimos diez años, cuando ni la formación ni el ejercicio de la profesión están regulados en el Estado miembro;
- Un certificado que justifica que se cumplen las condiciones morales;
- Un certificado de aptitud física para navegar
- un certificado de dominio de las habilidades del lenguaje.

**hora**

El servicio puede comenzar siempre y cuando no haya oposición de la Dirección Interregional del Mar:

- La expiración de un plazo de un mes a partir de la solicitud de declaración;
- en caso de una solicitud de información adicional o verificación de cualificaciones profesionales, al final de un período de dos meses a partir de la recepción de la solicitud completa.

**Tenga en cuenta que**

En caso de solicitud de acceso parcial a la profesión, el nacional deberá adoptar las mismas medidas que para el ejercicio de la actividad de forma temporal o ocasional en territorio francés.

**Bueno saber: medida de compensación**

Para obtener permiso para ejercer, el interesado puede estar obligado a someterse a una prueba de aptitud si parece que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de las ejercer la profesión en Francia.

La prueba de aptitud debe establecer que se dominan los conocimientos y cualificaciones pertinentes.

**Costo**

Gratis.

*Para ir más allá*: Artículos 7-2 a 10 de la orden del 8 de febrero de 2010.

### b. Obtener un certificado de reconocimiento para los nacionales de la UE o del EEE para un ejercicio permanente (LE)

**Autoridad competente**

El Director Interregional del Mar, situado en la región administrativa del puerto de armamento del buque pesquero, es competente para expedir el certificado de reconocimiento que autoriza el ejercicio permanente del segundo ingeniero en Francia.

**Procedimiento**

La solicitud de certificado de reconocimiento se dirige por cualquier medio a la autoridad competente de la región administrativa en la que desea resolver. En caso de falta de documento, la autoridad competente dispone de un mes a partir de la recepción del expediente para informar al nacional.

**Documentos de apoyo**

Para ejercer como segundo mecánico de forma permanente en Francia, el interesado debe presentar un expediente completo que contenga:

- El formulario[Cerfa No. 14750](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14750.do) Debidamente cumplimentado y firmado;
- Una identificación válida
- un certificado de experiencia profesional expedido por la autoridad competente del Estado miembro, cuando la profesión no esté regulada en dicho Estado;
- cuando así lo solicite la autoridad competente, si fuera necesario, el programa de formación que conduzca a la expedición del título;
- Un certificado que justifica que se cumplen las condiciones morales;
- Un certificado de aptitud física para navegar
- un certificado de dominio de las habilidades del lenguaje.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Resultado del procedimiento**

La autoridad competente dispone de un mes para decidir sobre la solicitud de certificación, tan pronto como se reciba el expediente completo.

Cualquier decisión, ya sea aceptación, denegación o indemnización de res, debe estar justificada.

El silencio guardado al final de un período de dos meses valdrá la pena la decisión de rechazar la solicitud de reconocimiento.

Si se acepta la decisión, la autoridad competente emite el certificado de reconocimiento, que tiene un plazo de validación de cinco años.

**Bueno saber: medidas de compensación**

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a la medida de compensación de su elección, ya sea un curso de ajuste o una prueba de aptitud, realizada en el plazo de seis meses a partir de la decisión de autoridad competente.

**Costo**

Gratis.

*Para ir más allá*: Artículos 2 a 5-2 de la orden del 8 de febrero de 2010.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

