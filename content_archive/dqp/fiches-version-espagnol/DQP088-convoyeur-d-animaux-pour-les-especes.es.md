﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP088" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Transportador de animales para especies no equinas y domésticas de ganado bovino, ovino, porcino, caprino y avícola" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="transportador-de-animales-para" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/transportador-de-animales-para-especies-no-equinas-y-domesticas.html" -->
<!-- var(last-update)="2020-04-15 17:20:52" -->
<!-- var(url-name)="transportador-de-animales-para-especies-no-equinas-y-domesticas" -->
<!-- var(translation)="Auto" -->




Transportador de animales para especies no equinas y domésticas de ganado bovino, ovino, porcino, caprino y avícola
===================================================================================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:52<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El transportador de animales vivos para especies distintas de los equinos domésticos y los animales domésticos del ganado bovino, caprino, ovino, porcino y avícola (los llamados "animales de alquiler") interviene en el transporte de animales cuando el viaje:

- no está sujeto a una emergencia veterinaria;
- es más de 65 km
- se lleva a cabo como parte de una actividad económica.

Sus principales tareas son:

- mantener y garantizar el bienestar de los animales transportados;
- garantizar su riego y alimentación;
- proporcionar primeros auxilios, si es necesario, a los animales que están heridos o enferman mientras son transportados.

Durante el transporte, el transportador puede dedicarse exclusivamente a estas misiones o, en su defecto, ser:

- El cliente en el lugar de salida hasta que se incluya la carga;
- El destinatario en el destino de la descarga incluido;
- Responsable del punto de parada, incluida la carga y descarga;
- en cualquier otro momento del viaje.

El transporte deberá efectuarse de conformidad con las condiciones de los anexos I y II de la[Reglamento (CE) No 1/2005 del Consejo, de 22 de diciembre de 2004](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32005R0001).

*Para ir más allá* Reglamento 1/2005 y Sección R. 214-55 del Código Rural y Pesca Marina.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de transportador de animales vivos está reservada al profesional titular de un certificado de finalización de la formación en el transporte por carretera de animales vivos, expedido por una organización de formación inscrita por el Ministerio de Agricultura.

La expedición de este certificado de finalización de la formación se puede obtener siempre y cuando la persona esté justificada:

- o han sido sometidos a una formación específica impartida por el[organización de formación registrada](https://info.agriculture.gouv.fr/gedei/site/bo-agri/document_administratif-957fabc5-1f76-4cab-9c6c-df6cac6eba6b) ;
- o celebrar uno de los diplomas de la Parte 2 de la Lista de la orden del 12 de noviembre de 2005.

En caso necesario, cuando el transportador también sea responsable del transporte de los animales, deberá justificar la tenencia de un permiso adaptado al tipo de vehículo conducido.

**Tenga en cuenta que**

El transportista que emplee un transportador de animales debe tener una autorización de transporte válida. Por otro lado, cuando el transportador actúe en su propio nombre, tendrá que solicitar esta autorización de transporte rellenando el formulario[Cerfa 15714*01](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15714.do) que se referirá a la Dirección Departamental encargada de la protección de la población (DDPP) territorialmente competente.

*Para ir más allá*: Artículos 1 y 2 de la[decreto de 12 de noviembre de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031521436&dateTexte=20171114) en relación con las autorizaciones o registros de las organizaciones de formación que implementan la formación necesaria para las personas que actúan como transportadores de animales vivos; 6 apartado 4 del Reglamento 1/2005.

#### Entrenamiento

La formación debe centrarse en los aspectos técnicos y reglamentarios de la legislación en materia de transporte de animales, incluidos:

- Aptitud, términos y condiciones y documentos para el transporte de animales vivos;
- la fisiología de los animales, sus necesidades alimentarias, el riego, su comportamiento y el concepto de estrés;
- Los aspectos prácticos del manejo de animales
- el impacto de la conducción en el bienestar de los animales que se transportan y en la calidad de la carne;
- Cuidado animal de emergencia
- aspectos de seguridad para el personal que manipula animales.

La duración mínima del entrenamiento es de 7 horas para una o incluso dos categorías comparables de animales. A este mínimo se añade un mínimo de una hora por categoría adicional de animales comparables y 3 horas por categoría adicional de animales.

*Para ir más allá*: Artículo R. 214-57 del Código Rural y Pesca Marina; Artículo 1 del Decreto de 12 de noviembre de 2015; Lista IV del Reglamento del 22 de diciembre de 2004.

#### Costos asociados con la calificación

Se paga la formación para entrar en la profesión de transportador. Para obtener más información, es aconsejable acercarse a las organizaciones de formación registradas por el Ministro responsable de la agricultura.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

El nacional de un Estado miembro de la UE o del EEE, que lleve actividad de transporte animal para especies distintas de los equinos domésticos y los animales domésticos de las especies de ganado bovino, caprino, ovino, porcino y avícola en uno de estos Estados, podrá para hacer uso de su título profesional en Francia, de forma temporal o casual. Debe hacer una declaración al prefecto de la región en la que desea practicar (véase más adelante "5o). a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año, durante los diez años que preceder el rendimiento.

Cuando el examen de las cualificaciones profesionales muestre diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y a su práctica en Francia, el interesado podrá someterse a una prueba de aptitud.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Todo nacional de la UE o del EEE que esté establecido y lleve a cabo legalmente la actividad de los animales de transporte en ese Estado podrá llevar a cabo la misma actividad en Francia de forma permanente.

Para ello, tendrá que solicitar el reconocimiento de sus cualificaciones profesionales a partir de la DDPP territorialmente competente (véase infra "5o. a. Solicitar el reconocimiento de las cualificaciones profesionales del nacional para un ejercicio permanente (LE)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que está legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año en los diez años que preceder el rendimiento.

Si el examen de las cualificaciones profesionales revela diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y a su práctica en Francia, el interesado podrá estar sujeto a medidas de compensación (véase más adelante "5. a. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos R. 214-57 y R. 204-2 y siguientes del Código Rural y la pesca marina.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Las obligaciones éticas son responsabilidad del transportador animal, incluyendo garantizar la salud y la dignidad de los animales transportados.

4°. Seguro
-------------------------------

Como parte de sus funciones, el transportador de animales está obligado a realizar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus trabajadores por los actos realizados en ocasiones.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar el reconocimiento de las cualificaciones profesionales del nacional para un ejercicio permanente (LE)

**Autoridad competente**

El DDPP del lugar de residencia es competente para decidir sobre la solicitud de reconocimiento de la cualificación del nacional.

**Documentos de apoyo**

El nacional acompañará su solicitud de reconocimiento de los siguientes documentos justificativos:

- Una identificación válida
- Un certificado de competencia o un certificado de formación adquirido en el Estado de origen;
- cuando el Estado no regula la profesión, un documento para justificar que el nacional ha sido un transportador de animales durante al menos un año en los últimos diez años.

**Procedimiento**

Una vez que el DDPP haya llevado a cabo los controles del expediente y se haya asegurado de que el interesado no haya sido objeto de una retirada o supresión del certificado en su Estado de origen, podrá expedirle una prueba de reconocimiento de Calificaciones.

**Bueno saber: medidas de compensación**
Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a la medida de compensación de su elección que pueda ser:

- un curso de adaptación de hasta tres años
- una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

*Para ir más allá*: Artículos R. 204-2 a R. 204-6 del Código Rural y Pesca Marina.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

