﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP145" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Guía de alta montaña" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="guia-de-alta-montana" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/guia-de-alta-montana.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="guia-de-alta-montana" -->
<!-- var(translation)="Auto" -->


Guía de alta montaña
====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El guía de montaña es un profesional que dirige y educa y educa todo tipo de montañismo y actividades relacionadas en seguridad y autonomía. Puede trabajar como acompañante, profesor y formador con fines educativos durante una práctica de ocio o actuación deportiva, pero también puede actuar como asesor técnico/consultor en actividades de montaña o realizar funciones de rescate de montaña.

El guía de montaña realiza las siguientes actividades:

- conducir y acompañar a personas en excursiones o ascensos de montaña a roca, nieve, hielo y terreno mixto;
- conducir y acompañar a la gente en las excursiones de esquí, montañeses de esquí y excursiones de esquí fuera de pista;
- enseñar montañismo, escalada y esquí de turismo, montañismo de esquí y esquí fuera de pista;
- formación en prácticas competitivas en las disciplinas antes mencionadas;
- supervisión y enseñanza profesional de la práctica de cañones verticales y acuáticos que requieran el uso de agrés para los titulares de diplomas enumerados en el artículo 3 del decreto del 10 de mayo de 1993 sobre la Patente Estatal de Montañismo.

El trabajo de guía de montaña se puede practicar a tiempo completo o como una ocupación estacional.

**Bueno saber: la obligación de reciclaje**

El permiso para ejercer como guía de montaña está limitado a un período de seis años, renovable. Al final de este período, la renovación de la licencia para ejercer se concede a los titulares del diploma de guía de montaña que ha completado un curso de actualización organizado por la Escuela Nacional de Deportes de Montaña (véase más adelante: "Curso de Reciclaje").

*Para ir más allá*: Artículo 3 y Apéndice II del decreto de 10 de mayo de 1993 relativo a la Patente Estatal de Montañería, decreto de 16 de junio de 2014 relativo a la formación específica del Diploma Estatal de Montaña de Montañería.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La actividad de guía de alta montaña está sujeta a la aplicación del artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas.

Como profesor de deportes, el guía de montaña debe poseer un diploma, un título profesional o un certificado de titulación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

Para practicar como guía de montaña, el interesado debe tener el diploma estatal de montañero - un guía de montaña, que le da la calidad de un educador deportivo.

**Bueno saber: el entorno específico**

El montañismo y las actividades asociadas se consideran actividades en un "entorno específico" (ES), independientemente del área de evolución. Como resultado, su práctica requiere el cumplimiento de las medidas de seguridad específicas a que se refiere la sección L. 212-2 del Código del Deporte.

*Para ir más allá*: Artículos L. 212-1, R. 212-84 y D. 212-67 del Código del Deporte.

#### Entrenamiento

El Diploma Estatal de Montañismo - Guía de Alta Montaña está clasificado como Nivel II (Bac-3/4). Se prepara después de un curso de formación bajo estatus de estudiante o estudiante, a través de un contrato de aprendizaje, después de un curso de educación continua, en un contrato de profesionalización, por solicitud individual o por validación de experiencia (VAE). Para obtener más información, puede ver[sitio web oficial](http://www.vae.gouv.fr/) Artículo 26 del decreto de 16 de junio de 2014 sobre la formación específica del Diploma Estatal de Montañería - un guía de montaña.

La formación que conduce al Diploma Estatal de Montañismo - Guía de Montaña es proporcionada por la Escuela Nacional de Deportes de Montaña (ENSM), en el[Escuela Nacional de Esquí y Montañismo](http://www.ensa.sports.gouv.fr/)(ENSA) en Chamonix.

El trabajo de un guía de montaña requiere serias habilidades físicas, técnicas y morales. La graduación del Diploma Estatal de Montañismo - un guía de montaña sanciona la formación respetando el principio de alternancia de períodos de formación en centros de formación y situaciones laborales bajo tutoría pedagógica, que se lleva a cabo se desglosa de la siguiente manera:

- enseñanzas comunes que conduzcan indiscriminadamente a diplomas de aspirantes a guía y guía de montaña, sancionando los conocimientos teóricos generales. Esta primera formación es por un mínimo de tres años;
- luego la formación específica de la guía de montaña, se centró en la preparación para el trabajo de guía.

El titular del Diploma Estatal de Montañismo - Guía de Montaña debe actualizar sus habilidades profesionales cada seis años a través de un curso de reentrenamiento que se lleva a cabo a más tardar el 31 de diciembre del sexto año después de la graduación o Reciclaje. El reciclaje determina el ejercicio de la profesión (véase más adelante: "Curso de reciclaje").

###### Condiciones para el acceso a la formación que conduce al Diploma Estatal de Montañesa - Guía de Montaña: Libertad Condicional

Para ser admitido en la capacitación que conduce al Diploma Estatal de Montañería - Guía de Montaña, el candidato debe pasar un examen de prueba que dura diez días que consiste en pruebas de eliminación.

###### Requisitos de libertad condicional

El interesado deberá:

- Ser una especialización el 1 de enero del año del examen;
- enviar un archivo dos meses antes del inicio del examen al Director General de la Escuela Nacional de Deportes de Montaña, sede de la Escuela Nacional de Esquí y Montañismo, que contiene:- Una solicitud de registro basada en una impresión estandarizada con una fotografía,
  - una copia a dos lados del documento nacional de identidad o pasaporte del candidato,
  - una fotocopia del certificado censal y el certificado individual de participación en el recurso de preparación de la defensa, si el candidato es menor de veinticinco años de edad,
  - un certificado de participación en la unidad de educación "Nivel 1 de Primeros Auxilios Cívicos" (PSC1) o su equivalente,
  - un certificado médico de no contradictorio con la práctica y la enseñanza del montañero, que se remonta a menos de tres meses a la fecha de apertura de los acontecimientos,
  - dos sobres sellados de acuerdo con las tarifas detalladas en la Lista I del orden de 16 de junio de 2014 mencionado anteriormente,
  - una lista (basada en el modelo definido por la Escuela Nacional de Deportes de Montaña) de las carreras y subidas realizadas. Algunas carreras son necesarias para registrarse. La lista de estos es determinada y comunicada por la Escuela Nacional de Deportes de Montaña, sede de la Escuela Nacional de Esquí y Montañismo, previa asesoración de la sección permanente de montañismo del Comité de Formación y Empleo del Consejo Superior de deportes de montaña.

###### Contenido y conducta de la revisión probatoria

El examen probatorio incluye una serie de eventos de eliminación organizados y evaluados en dos partes sucesivas.

- La primera parte consta de seis eventos:- una entrevista previa a la descripción de las carreras en la lista adjunta al expediente de inscripción del examen de prueba. Esta entrevista dura un máximo de 30 minutos y evalúa la capacidad del candidato para comunicar información precisa sobre estas razas,
  - una prueba de orientación con sólo un mapa, una brújula de aguja magnetizada y un alijo como instrumentos,
  - un evento de escalada en zapatillas de escalada,
  - una prueba de nieve y hielo,
  - una prueba de evolución en varios terrenos, incluyendo una evolución de roca en zapatos de montaña,
  - un evento de esquí de nieve, esquí todo terreno;
- la segunda parte implica cambios en un entorno de alta montaña, durante un período de 5 días. Su objetivo es evaluar la capacidad del candidato para movilizar sus habilidades técnicas de forma independiente.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más detalles, es aconsejable consultar los artículos 6 y siguientes del auto de 16 de junio de 2014 mencionado anteriormente.

*Para ir más allá* :[Descripción](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) de la certificación "Diploma Estatal de Montañismo - Guía de Alta Montaña" del Directorio Nacional de Certificaciones Profesionales (RNCP), decreto de 16 de junio de 2014 relativo a la formación específica del Diploma Estatal de Montañismo - Guía de Alta Montaña.

###### Curso de reciclaje

La sesión de reciclaje, con una duración mínima de 24 horas, tiene como objetivo actualizar las competencias profesionales de los guías de montaña, en particular en las áreas de gestión de la seguridad, significa obligación y regulación, desde análisis preliminar de las prácticas profesionales y su evolución: análisis de riesgos, sinspicalogía, cambios en el marco jurídico o social, actualización del conocimiento y know-how.

Al final de cada sesión, los certificados de reciclaje son emitidos por el Director General de la Escuela Nacional de Deportes de Montaña, sede de la Escuela Nacional de Esquí y Montañismo.

*Para ir más allá* : decreto de 11 de marzo de 2015 relativo al contenido y organización del reciclaje de los titulares de diplomas de guía de montaña.

#### Costos asociados con la calificación

La formación que conduce al diploma estatal de montañismo - guía de montaña se paga: alrededor de 11.000 euros (desde el examen de prueba hasta la graduación del diploma estatal). Este costo se indica como una indicación. Para más detalles, es aconsejable acercarse a la[Escuela Nacional de Deportes de Montaña](http://www.ensm.sports.gouv.fr/).

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar el ejercicio seguro de la actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7, R. 212-92 a R. 212-94 del Código del Deporte.

**Bueno saber: la diferencia sustancial**

Para la supervisión del montañismo por parte de un guía de montaña, la diferencia sustancial, que probablemente existirá entre la cualificación profesional del solicitante de registro y la cualificación profesional requerida en el territorio nacional, se aprecia en referencia a la formación del diploma de guía de montaña del Certificado Estatal de Montañesa, ya que incorpora:

- Conocimientos teóricos y prácticos sobre seguridad
- habilidades técnicas de seguridad.

En el contexto de la libre prestación de servicios, cuando el prefecto considere, tras el asesoramiento de la sección permanente de montañismo de la Comisión de Formación y Empleo del Consejo Superior de Deportes de Montaña, se remitió al Centro Nacional de Comercios de esquí y montañismo, ya sea que haya una diferencia sustancial, puede decidir someter al declarante a una prueba de aptitud o a un curso de adaptación.

*Para ir más allá*: Artículos A. 212-222 y A. 212-224 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si cumple una de las cuatro condiciones:

**Si el Estado de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE y que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si la cualificación profesional del nacional difiere sustancialmente de la cualificación requerida en Francia que no garantizaría la seguridad de los profesionales y de terceros, puede estar obligado a someterse a un aptitud o realizar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para ejercer su actividad en Francia, en particular para garantizar el ejercicio seguro de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

**Bueno saber: la diferencia sustancial**

Para la supervisión del montañismo por parte de un guía de montaña, la diferencia sustancial, que probablemente existirá entre la cualificación profesional del solicitante de registro y la cualificación profesional requerida en el territorio nacional, se aprecia en referencia a la formación del diploma de guía de montaña del Certificado Estatal de Montañesa, ya que incorpora:

- Conocimientos teóricos y prácticos sobre seguridad
- habilidades técnicas de seguridad.

En el contexto de la libertad de establecimiento, cuando el prefecto considere, tras el asesoramiento de la sección permanente de montañismo del Comité de Formación y Empleo del Consejo Superior de Deportes de Montaña, se remitió al Centro Nacional de Comercios de la gestión del esquí y el montañismo, que hay una diferencia sustancial, remitió el asunto a la Comisión de Reconocimiento de Cualificación, adjuntando el dictamen de la sección permanente al expediente.

Después de pronunciarse sobre la existencia de una diferencia sustancial, la Comisión de Reconocimiento de Cualificaciones propone, si es necesario, al prefecto someter al declarante a una prueba de aptitud o a un curso de adaptación.

Por otra parte, si el prefecto cree que no hay diferencia sustancial o cuando se ha identificado una diferencia sustancial y el solicitante de registro ha cumplido con la prueba de aptitud, el prefecto expide un certificado de libertad al declarante. establecimiento y una tarjeta de educador deportivo profesional.

*Para ir más allá*: Artículos A. 212-222, A. 212-223 y A. 212-228, R. 212-88 a R. 212-91 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como guía de montaña en Francia para personas que hayan sido condenadas por cualquier delito o por uno de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Requisito de notificación (con el fin de obtener la tarjeta de educador deportivo profesional)
-----------------------------------------------------------------------------------------------------------------------

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el [sitio web oficial](https://eaps.sports.gouv.fr).

#### hora

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

#### Documentos de apoyo

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

5°. Procedimientos y formalidades de reconocimiento de cualificaciones
--------------------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

#### Autoridad competente

La declaración previa de actividad debe dirigirse a la Dirección Regional de Juventud, Deportes y Cohesión Social (DDCS) de Isére.

#### hora

En el mes siguiente a la recepción del expediente de declaración, el prefecto notifica al proveedor:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

#### Documentos de apoyo

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

#### hora

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

#### Documentos de apoyo para la primera declaración de actividad

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

#### Evidencia para una declaración de renovación de la actividad

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

**Bueno saber: medidas de compensación (para una actividad en un entorno específico)**

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de las cualificaciones colocadas al Ministro responsable del deporte. Esta comisión, antes de emitir su dictamen, remite a los órganos de consulta especializados para obtener asesoramiento. Después de pronunciarse sobre la existencia de una diferencia sustancial, la comisión propone, si es necesario, al prefecto someter al declarante a una prueba de aptitud. Al leer el dictamen de la comisión, el prefecto puede exigir al solicitante de registro que se someta a una prueba de aptitud.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud, la naturaleza que define (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía. , tipos de estructuras que pueden acomodar al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- si el solicitante de registro requiere compensación, debe someterse a una prueba de aptitud. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R212-84, R212-90-1 y D212-84-1 del Código del Deporte.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

