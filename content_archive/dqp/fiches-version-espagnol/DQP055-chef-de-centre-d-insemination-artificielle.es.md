﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP055" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Jefe del centro de inseminación artificial equino" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="jefe-del-centro-de-inseminacion" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/jefe-del-centro-de-inseminacion-artificial-equino.html" -->
<!-- var(last-update)="2020-04-15 17:20:51" -->
<!-- var(url-name)="jefe-del-centro-de-inseminacion-artificial-equino" -->
<!-- var(translation)="Auto" -->


Jefe del centro de inseminación artificial equino
=================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:51<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El jefe del centro de inseminación artificial equino ("líder central") es el jefe de un centro de producción de semillas equinos y/o un equipo de trasplante embrionario.

Como parte de sus misiones, lleva a cabo, organiza y gestiona las actividades de inseminación, congelación o refrigeración de espermatozoides, almacenamiento de dosis y envío de semillas, y, si es necesario, actividades de transferencia de embriones.

En colaboración con los equipos de inseminadores y obreros que trabajan en su centro, mantiene relaciones comerciales con los propietarios de las yeguas y sementales con los que trabaja, al tiempo que promueve su Estructura.

Su misión también es mantener los términos publicitarios por delante de las nuevas técnicas y procesos de inseminación, así como participar en la investigación en el campo.

Para obtener más información sobre las misiones de la cabeza central, consulte el[sitio del Instituto Francés de Equitación y Caballo](http://www.haras-nationaux.fr/information/accueil-equipaedia/formations-et-metiers/les-metiers-de-lelevage/chef-de-centre-d-ia.html).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Cualquier persona que desee convertirse en jefe del centro debe haberse declarado a la autoridad administrativa (el prefecto de la región en la que desea ejercer), que lo registrará en vista de la presentación de la licencia de un jefe de centro Inseminación.

Esta licencia sólo podrá expedirse a los titulares del Certificado de Aptitud para el Gerente del Centro expedido por una institución de formación especializada.

Una solicitud de validación de los logros académicos, evaluada por la junta de examen de fin de sesión, es posible:

- veterinarios especializados en reproducción equina, con un diploma francés o un colegio europeo;
- nacionales de la Unión Europea o de los países del Espacio Económico Europeo, titulares de acreditación, nivel equivalente de formación, de uno de los países de la Unión Europea, para llevar a cabo las funciones de jefe del centro.

La validación de los logros académicos permite al receptor quedar exento de parte de la formación o parte de las pruebas para el certificado de aptitud.

*Para ir más allá*: Artículo L. 653-13 y Artículo R. 653-96 del Código de Pesca Rural y Marina; Artículos 9 y 13 de la[decreto de 21 de enero de 2014](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028535400&dateTexte=20171106) certificados de idoneidad para el centro de inseminación y de inseminación artificial en especies equinas y asinas.

#### Entrenamiento

La licencia del jefe del centro está sujeta a un certificado de aptitud expedido después de la formación en una de las siguientes instituciones:

- el centro de formación de yeguas del Instituto Francés de Equitación;
- El Centro de Educación Zootécnica de Rambouillet;
- una de las cuatro escuelas veterinarias nacionales.

El acceso a la formación puede ser limitado dependiendo del número de plazas disponibles y viene después de la decisión del director de una de estas instituciones.

La admisión se puede hacer:

- o después de revisar el expediente de la persona, siempre que justifique:- o diploma estatal como médico veterinario,
  - ya sea un título de ingeniería de una institución de educación superior o un colegio nacional de agricultura;
- o, tras un control de los conocimientos adquiridos, el titular de un certificado de idoneidad para las funciones de inseminante que justifique:- grado de Nivel III en agricultura o biología y justificando tres años de actividad en inseminación equina,
  - cinco años de experiencia profesional en la cría equina, incluyendo al menos cuatro años en inseminación equina,
  - o un grado de Nivel I en biología reproductiva equina.

**Qué saber**

La verificación de conocimientos abarca las asignaturas definidas en la Lista III del decreto de 21 de enero de 2014 y puede implicar una entrevista motivacional.

La duración de la formación es de cinco semanas durante las cuales el candidato tendrá que alcanzar objetivos en fisiología reproductiva y biotecnología, dieta, genética, higiene y profilaxis, regulación, relaciones promoción y marketing.

Al final de esta formación, el candidato tendrá que aprobar un examen consistente en pruebas teóricas, prácticas, orales y legislativas.

La admisión se pronuncia si la persona ha obtenido una puntuación media de 12 sobre 20, sin puntuación inferior a 10 de 20.

**Tenga en cuenta que**

Para conocer la formación que conduce a la profesión veterinaria en Francia, es aconsejable consultar la ficha de cualificación dedicada a la misma.

*Para ir más allá*: Artículos 9 y siguientes, y los Apéndices III y IV de la[decreto de 21 de enero de 2014](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028535400&dateTexte=20171106) certificados de idoneidad para el centro de inseminación y de inseminación artificial en especies equinas y asinas.

#### Costos asociados con la calificación

La formación para convertirse en directora del centro da sus frutos. Para obtener más información sobre los costes de formación, es aconsejable acercarse a instituciones especializadas.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (Servicio Gratuito)

Un nacional de un Estado miembro de la UE o del EEE, que actúe como director de centro en uno de estos Estados, podrá utilizar su título profesional en Francia de forma temporal o ocasional. Debe hacer una declaración al prefecto de la región en la que desea ejercer (véase más adelante "5.a. Hacer una declaración previa de actividad para los nacionales de la UE que ejercen actividades) antes de su primera actuación. (LPS)) ).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que está legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año en los diez años que preceder el rendimiento.

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones requeridas para el acceso a la profesión y su ejercicio en Francia, el interesado podrá ser sometido a una prueba de aptitud en un un mes después de la recepción por parte del prefecto de la solicitud de declaración.

*Para ir más allá*: Artículos L. 653-13, R. 653-96, L. 204-1 y R. 204-1 del Código Rural y Pesca Marina.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si:

- posee un certificado de formación o un certificado de competencia expedido por una autoridad competente de otro Estado miembro que regula el acceso a la profesión o a su ejercicio;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Una vez que cumpla una de las dos condiciones anteriores, tendrá que obtener la licencia de jefe de centro requerida para el ejercicio de la profesión, del prefecto regional competente. Para obtener más información, es aconsejable hacer referencia al apartado 5.b. Obtener una licencia para los nacionales de la UE o del EEE para un ejercicio permanente (LE)".

Si, durante el examen del expediente, el prefecto constata que existen diferencias sustanciales entre la formación y la experiencia profesional del nacional y las necesarias para ejercer en Francia, podrán adoptarse medidas de compensación ("5 grados). b. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos R. 204-5 del Código Rural y Pesca Marina.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Las obligaciones éticas son responsabilidad del jefe del centro, incluido el respeto de la confidencialidad profesional asociada a la recolección de semillas, la inseminación y la transferencia de embriones, garantizar la salud y el mantenimiento de la dignidad del animal inseminado o trazabilidad y cumplimiento de las regulaciones sanitarias.

4°. Seguro
-------------------------------

Como parte de sus funciones, el jefe del centro está obligado a realizar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus trabajadores por los actos realizados en ocasiones.

5°. Proceso de reconocimiento de cualificación y trámites
-------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

#### Autoridad competente

El prefecto regional es responsable de recibir la predeclaración de práctica de forma temporal o ocasional.

#### Documentos de apoyo

La declaración previa del nacional deberá transmitirse por cualquier medio a la autoridad competente e incluir los siguientes documentos justificativos:

- Prueba de la nacionalidad del profesional
- un certificado que lo certifique:- está legalmente establecido en un Estado de la UE o del EEE,
  - ejerce una o más profesiones cuya práctica en Francia requiere la celebración de un certificado de capacidad,
  - y no incurre en una prohibición de ejercer, ni siquiera temporalmente, al expedir el certificado;
- prueba de sus cualificaciones profesionales
- cuando ni la actividad profesional ni la formación estén reguladas en la UE o en el Estado del EEE, prueba por cualquier medio de que el nacional ha estado involucrado en esta actividad durante un año, a tiempo completo o a tiempo parcial, en los últimos diez años;
- una declaración de compromiso de actividad temporal o ocasional de menos de un año.

Esta declaración anticipada incluye información relativa al seguro u otros medios de protección personal o colectiva suscritos por el solicitante de registro para cubrir su responsabilidad profesional.

Estos documentos se adjuntan, según sea necesario, a su traducción al francés.

#### hora

El prefecto regional tiene un mes desde el momento en que se recibe el archivo para tomar su decisión:

- permitir que el reclamante realice su beneficio.  En caso de incumplimiento de este plazo de un mes o del silencio de la administración, podrá llevarse a cabo la prestación de servicios;
- someter a la persona a una medida de compensación en forma de prueba de aptitud, si resulta que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de las requeridas para el ejercicio de la profesión en Francia (véase más adelante: "Bueno saber: medida de compensación");
- informarles de una o más dificultades que puedan retrasar la toma de decisiones. En este caso, dispondrá de dos meses para decidir, a partir de la resolución de las dificultades y, en cualquier caso, en un plazo máximo de tres meses a partir de la información del interesado sobre la existencia de la dificultad o dificultades. En ausencia de una respuesta de la autoridad competente dentro de este plazo, podrá comenzar la prestación de servicios.

*Para ir más allá*: Artículos R. 204-1 y R. 204-6 del Código Rural y pesca marina.

### b. Obtener una licencia para los nacionales de la UE que realizan actividades permanentes (LE)

#### Autoridad competente

El prefecto regional del lugar de práctica es responsable de expedir la licencia que permite al nacional ejercer permanentemente la actividad de jefe de centro en Francia.

#### Procedimiento

El nacional debe transmitir al prefecto todos los documentos necesarios para respaldar su solicitud de licencia, incluidos:

- prueba de nacionalidad
- Un certificado de formación o certificado de competencia adquirido en un Estado miembro de la UE o del EEE;
- cualquier prueba que justifique, en su caso, que el nacional haya sido director del centro durante un año, a tiempo completo o a tiempo parcial, en un Estado miembro que no regula la profesión.

El prefecto tiene un mes desde la recepción de las pruebas justificativas para reconocerlo o solicitar el envío de documentos faltantes.

La decisión de conceder la licencia tendrá lugar en un plazo de tres meses, prorrogado por un mes adicional en caso de que falten piezas.

El silencio guardado en estos plazos valdrá la pena la decisión de aceptación.

#### Bueno saber: medida de compensación

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a la medida de compensación de su elección, que puede ser:

- un curso de adaptación de hasta tres años
- una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

Sin embargo, la elección de la medida será responsabilidad del prefecto si el nacional justifica:

- un certificado de competencia adquirido como resultado de la formación general, el examen o el ejercicio a tiempo completo o a tiempo parcial durante tres años en los últimos diez años;
- un ciclo de enseñanza secundaria general, técnico o vocacional completado con una pasantía, una práctica profesional o un ciclo de estudio o formación.

*Para ir más allá*: Artículos R. 204-2 a R. 204-6 del Código Rural y Pesca Marina.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

