﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP272" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Asistente dental" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="assistante-dental" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/assistante-dental.html" -->
<!-- var(last-update)="2021-06" -->
<!-- var(url-name)="assistante-dental" -->
<!-- var(translation)="None" -->

# Asistente dental [FR]

## 1°. Définition de l’activité

La profession d’assistant dentaire consiste à assister le chirurgien-dentiste ou le médecin exerçant dans le champ de la chirurgie dentaire dans son activité professionnelle, sous sa responsabilité et son contrôle effectif. Dans ce cadre, l’assistant dentaire contribue aux activités de prévention et d’éducation à la santé dans le domaine bucco-dentaire.

L'assistant dentaire exerce en qualité de salarié, notamment au sein de cabinets de soins dentaires, en établissements de santé et en centres de santé.

Activités :

- assistance du praticien dans la réalisation des gestes avant, pendant et après les soins ;
- accueil et communication auprès des patients ;
- informations et éducation des patients dans le champ de la santé bucco- dentaire ;
- entretien de l'environnement de soins, des matériels liés aux activités et gestion du risque infectieux ;
- gestion et suivi du dossier du patient ;
- recueil, transmission des informations par écrit et par oral et mise en œuvre de la traçabilité dans le cadre de la structure de soins ;
- accueil, accompagnement des assistants dentaires en formation ou nouveaux arrivants dans la structure, et amélioration des pratiques professionnelles.

Ces activités ne sont pas des activités réservées.

## 2°. Qualifications professionnelles

### a. Exigences nationales 

#### Législation nationale

Pour exercer la profession d’assistant dentaire, le professionnel doit être titulaire du titre de formation français permettant l’exercice de cette profession, délivré par la Commission paritaire nationale de l’emploi et de la formation professionnelle. 

Peuvent également exercer cette profession, les personnes titulaires d’un certificat ou d’un titre délivré par la Commission paritaire nationale de l’emploi et de la formation professionnelle, dès lors que la formation correspondante a commencé avant le 1er janvier 2019. 

Par dérogation, les étudiants en chirurgie dentaire qui ont obtenu un niveau de connaissance suffisant peuvent être autorisés à exercer la profession d’assistant dentaire dans les cabinets dentaires pendant la durée de leurs études.

Le titre d'assistant dentaire est obtenu par les voies suivantes :

- la formation en contrat de professionnalisation ;
- la formation par apprentissage ;
- la formation professionnelle continue ;
- la validation des acquis de l'expérience professionnelle ;
- la formation initiale.

À l'exception de la voie par l'apprentissage, aucune limite d'âge n'est prévue.

Pour être admis à effectuer les études conduisant au titre d'assistant dentaire, les candidats doivent être âgés de dix-huit ans révolus pour l'entrée en formation et justifier d'un titre ou diplôme de niveau V, ce qui correspond à dix-huit mois de formation.

Le titre d'assistant dentaire s'obtient par la validation de l'ensemble des unités d'enseignement de la formation théorique et des compétences acquises en formation clinique.

#### Formation

L’article R. 4393-8 définit dans ses annexes le référentiel de formation des assistants dentaires.

La période de formation conduisant au titre d'assistant dentaire est d'une durée de dix-huit mois. 343 heures de formation théorique et 1 535 heures de formation pratique sont prévues.

#### Coûts associés

La prise en charge des droits annuels d'inscription et des frais de scolarité est fixée dans la convention de formation initiale ou professionnelle, en application de l’arrêté du 8 juin 2018 relatif à la formation conduisant au titre d’assistant dentaire.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (LPS)

Tout ressortissant d’un État membre de l’UE ou de l’EEE qui est établi et exerce légalement l’activité d’assistant dentaire dans cet État peut exercer en France, de manière temporaire et occasionnelle, la même activité.

Il doit au préalable en faire la demande par déclaration à la préfecture de région (la direction régionale de l’économie, de l’emploi, du travail et des solidarités (DREETS)) du lieu dans lequel il souhaite réaliser la prestation.

Dans le cas où la profession n’est pas réglementée dans le pays dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité pendant au moins un an, au cours des dix dernières années précédant la prestation, dans un ou plusieurs États membres ou parties de l’UE.

Lorsqu’il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, le préfet de région (la DREETS) peut exiger que l’intéressé se soumette à une épreuve d’aptitude.

*Pour aller plus loin :* article L. 4393-14 du Code de la santé publique.

### c. Ressortissants UE : en vue d’un exercice permanent (LE)

Les ressortissants de l’UE ou de l’EEE souhaitant exercer à titre permanent en France doivent obtenir une autorisation d’exercer.

Les ressortissants de l’UE ou de l’EEE peuvent être autorisés à exercer en France s’ils sont titulaires au choix :

- d’un titre de formation délivré par un État de l’UE ou de l’EEE dans lequel l’accès à la profession ou son exercice est réglementé et qui permet d’y exercer légalement ;
- d’un titre de formation délivré par un État de l’UE ou de l’EEE dans lequel ni l’accès à la profession ni son exercice n’est réglementé, et justifier avoir exercé, dans cet État, cette activité pendant au moins l’équivalent d’un an à temps complet ou à temps partiel au cours des dix dernières années ;
- d’un titre de formation délivré par un État tiers reconnu dans un État de l’UE ou de l’EEE, autre que la France, permettant d’y exercer légalement la profession. L’intéressé justifie avoir exercé la profession pendant trois ans à temps plein ou à temps partiel pendant une durée totale équivalente dans cet État, membre de l’UE ou de l’EEE.

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis, selon son choix, à une épreuve d’aptitude ou un stage d’adaptation, ou à l’une des deux mesures de compensation, ou aux deux (cf. infra « Bon à savoir : mesures de compensation »).

*Pour aller plus loin :* articles L. 4393-9 et suivants et L. 4393-12 du Code de la santé publique.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

Nul ne peut exercer la profession d’assistant dentaire s’il fait l’objet d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévus au 11° de l’article 131-6 du Code pénal.

## 4°. Assurances

En tant que salarié, l’assistant dentaire est tenu de vérifier l’étendue de la garantie souscrite par son employeur en ce qui concerne l’assurance de responsabilité civile professionnelle, en application de l’article 3 de l’arrêté du 8 décembre 2017. En effet, l’assistant dentaire étant un salarié, c’est à l’employeur de souscrire pour ses salariés une assurance de responsabilité civile professionnelle pour les actes effectués à l’occasion de leur activité professionnelle.

*Pour aller plus loin :* article L. 1142-2 du Code de la santé publique.

## 5°. Démarche et formalités

### a. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

La déclaration préalable d’activité est adressée avant la première prestation de services au préfet de la région du lieu dans lequel le ressortissant souhaite réaliser la prestation. La DREETS est compétente pour instruire la déclaration préalable d’activité de l’assistant dentaire.

#### Pièces justificatives

La demande de déclaration préalable d’activité est remplie sur le modèle de formulaire prévu à cet effet et est accompagnée d’un dossier complet comprenant les pièces justificatives suivantes :

- une copie d'une pièce d'identité en cours de validité à la date de dépôt de la déclaration ;
- si cette pièce ne le prévoit pas, un document attestant la nationalité du demandeur ;
- une copie du titre de formation permettant l'exercice de la profession dans le pays d'obtention ;
- attestation datant de moins de trois mois de l'autorité compétente de l'État d'établissement, membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, certifiant que l'intéressé est légalement établi dans cet État et qu'il n'encourt, lorsque l'attestation est délivrée, aucune interdiction, même temporaire, d'exercer ;
- lorsque le candidat a exercé dans un État, membre ou partie à l'accord sur l'Espace économique européen, qui ne réglemente ni la formation, ni l'accès à la profession demandée ou son exercice, toutes pièces utiles justifiant qu'il a exercé la profession dans cet État à temps plein pendant un an au cours des dix dernières années ou à temps partiel pendant une durée totale équivalente au cours de la même période ;
- lorsque le titre de formation a été délivré par un État tiers et reconnu dans un État, membre ou partie, autre que la France :
  - la reconnaissance du titre de formation établie par les autorités de l'État ayant reconnu ce titre,
  - toutes pièces utiles justifiant qu'il a exercé la profession dans cet État pendant trois ans à temps plein ou à temps partiel pendant une durée totale équivalente ;
- le cas échéant, une copie de la déclaration précédente ainsi que de la première déclaration effectuée.

**A savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Issue de la procédure

Dans un délai d'un mois à compter de la réception de la déclaration, le préfet de la région informe le prestataire, au vu de l'examen de son dossier :

- ou bien qu'il peut débuter la prestation de services sans vérification préalable de ses qualifications professionnelles ;
- ou bien, lorsque la vérification de ses qualifications professionnelles met en évidence une différence substantielle avec la formation exigée en France de nature à nuire à la santé publique et qu'elle ne peut pas être compensée par l'expérience professionnelle ou par la formation tout au long de la vie ayant fait l'objet d'une validation par un organisme compétent, qu'il doit se soumettre à une épreuve d'aptitude afin de démontrer qu'il a acquis les connaissances et compétences manquantes. S'il satisfait à ce contrôle, la prestation de services débute dans le mois qui suit la décision relative à l'épreuve d'aptitude. Dans le cas contraire, il est informé qu'il ne peut pas débuter la prestation de services ;
- ou bien qu'il ne peut pas débuter la prestation de services.

Dans le même délai d'un mois à compter de la réception de la déclaration, lorsque l'examen du dossier met en évidence une difficulté susceptible de provoquer un retard de sa décision, le préfet de région informe le prestataire des raisons de ce retard. La décision est prise dans les deux mois suivant la résolution de la difficulté et, au plus tard, dans un délai de trois mois à compter de la date à laquelle le prestataire a été informé de l'existence de la difficulté.

En l'absence de réponse du préfet de région dans les délais fixés susmentionnés, la prestation de services peut débuter.

### b. Demander une autorisation d’exercice pour le ressortissant de l'UE ou de l'EEE en cas d'exercice permanent (LE)

#### Autorité compétente

Le candidat à l'autorisation d'exercice dépose un dossier dans la préfecture de la région dans laquelle il a l'intention de s'établir. La demande est transmise au préfet de la région Auvergne-Rhône-Alpes qui centralise les demandes d’autorisation d’exercice.

Le préfet de la région Auvergne-Rhône-Alpes délivre, après avis de la commission des assistants dentaires, l'autorisation d'exercice.

#### Procédure

Le préfet de la région Auvergne-Rhône-Alpes accuse réception de la demande dans le délai d'un mois à compter de sa réception et le cas échéant, indique au demandeur dans ce même délai les pièces et informations manquantes.

Le silence gardé par le préfet de la région Auvergne-Rhône-Alpes à l'expiration d'un délai de quatre mois à compter de la réception du dossier complet vaut décision de rejet de la demande.

#### Pièces justificatives

La demande de déclaration préalable d’activité est remplie sur le modèle de formulaire prévu à cet effet et est accompagnée d’un dossier complet comprenant les pièces justificatives suivantes :

- une photocopie lisible d'une pièce d'identité en cours de validité à la date de dépôt du dossier ;
- une copie du titre de formation permettant l'exercice de la profession dans le pays d'obtention ;
- le cas échéant, une copie des diplômes complémentaires ;
- toutes pièces utiles justifiant des formations continues, de l'expérience et des compétences acquises au cours de l'exercice professionnel dans un État, membre ou partie, ou dans un État tiers ;
- une déclaration de l'autorité compétente de l'État, membre ou partie, d'établissement, datant de moins d'un an, attestant de l'absence de sanctions ;
- une copie des attestations des autorités ayant délivré le titre de formation, spécifiant le niveau de la formation et, année par année, le détail et le volume horaire des enseignements suivis ainsi que le contenu et la durée des stages validés ;

Pour les candidats qui ont exercé dans un État, membre ou partie, qui ne réglemente pas l'accès à la profession demandée ou son exercice, il convient de fournir toutes pièces utiles justifiant qu'ils ont exercé dans cet État, à temps plein pendant un an au cours des dix dernières années ou à temps partiel pendant une durée correspondante au cours de la même période, la profession pour laquelle ils demandent l'autorisation. Ces pièces ne sont pas à fournir lorsque la formation conduisant à cette profession est réglementée.

Pour les candidats titulaires d'un titre de formation délivré par un État tiers et reconnu dans un État, membre ou partie, autre que la France, il convient de fournir la reconnaissance du titre de formation. Cette reconnaissance doit permettre au bénéficiaire d'y exercer sa profession.

**A savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Issue de la procédure

La commission d’autorisation d’exercice peut :

- délivrer une autorisation d’exercice directe ;
- demander des éléments d’information complémentaires et sursoir à statuer jusqu’à réception de ces documents ;
- refuser l’autorisation d’exercice. Dans ce cas, la décision est motivée et comporte les délais et voies de recours ;
- demander de réaliser des mesures compensatoires visant à compléter la formation du demandeur (cf. infra « Bon à savoir : mesures de compensation »).

En l’absence de réponse du préfet de région dans le délai de quatre mois à compter de la réception du dossier complet, la demande d’autorisation est réputée refusée.

#### Voies de recours

Le demandeur peut contester toute décision (implicite ou exprès) de refus de sa demande d’autorisation d’exercer. Pour cela, il peut, dans les deux mois suivant la notification de la décision de refus, former au choix :

- un recours gracieux auprès du préfet de région ;
- un recours hiérarchique auprès du ministre chargé de la santé ;
- un recours contentieux auprès du tribunal administratif territorialement compétent.

**Bon à savoir : mesures de compensation**

Pour obtenir l’autorisation d’exercer, l’intéressé peut être amené à se soumettre à une épreuve d’aptitude et/ou un stage d’adaptation s’il s’avère que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France. On parle de mesures de compensation.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème  ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).