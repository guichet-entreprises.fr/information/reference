﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP146" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arte y cultura" -->
<!-- var(title)="Guía-altavoz" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arte-y-cultura" -->
<!-- var(title-short)="guia-altavoz" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/arte-y-cultura/guia-altavoz.html" -->
<!-- var(last-update)="2020-04-15 17:20:41" -->
<!-- var(url-name)="guia-altavoz" -->
<!-- var(translation)="Auto" -->


Guía-altavoz
============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:41<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El guía-profesor es un profesional cuya misión es proporcionar visitas guiadas, en francés o en lengua extranjera, en museos de Francia y monumentos históricos. Su función es potenciar el patrimonio mediante el diseño de acciones de mediación cultural dirigidas al público en los territorios y lugares patrimoniales.

*Para ir más allá*: Artículo L. 221-1 del Código de Turismo.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de profesor titular, el interesado debe justificar una tarjeta profesional expedida a los titulares de una certificación inscrita en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

#### Entrenamiento

La tarjeta de guía-altavoz profesional se proporciona al titular:

- Licencia profesional como guía-profesor;
- máster con las siguientes unidades docentes:- habilidades de los profesores guía,
  - situación y práctica profesional,
  - una lengua viva distinta del francés;

**Tenga en cuenta que**

Estas unidades docentes deben haber sido validadas por el interesado. Prueba de su validación puede adoptar la forma de un certificado expedido por la institución educativa o un apéndice descriptivo adjunto al diploma.

- máster y justificación:- al menos un año de experiencia profesional acumulada en mediación de herencia oral en los últimos cinco años,
  - nivel C1 al marco común europeo de referencia para las lenguas ([CECRL](https://www.coe.int/T/DG4/Linguistic/Source/Framework_FR.pdf)) en una lengua viva extranjera, una lengua regional en Francia o lengua de signos francesa.

*Para ir más allá*: Artículo 1 y apéndice de la[pedido a partir del 9 de noviembre de 2011](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024813610) en relación con las habilidades necesarias para expedir la tarjeta de guía-altavoz profesional a los titulares de una licencia profesional o diploma que confiere el título de máster.

#### Costos asociados con la calificación

Se paga la formación que conduce a la profesión de profesor-guía. Para más información, es aconsejable acercarse a los establecimientos que lo emiten.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Todo nacional de un Estado miembro de la UE o del EEE establecido y que practique legalmente la actividad de portavoz guía en ese Estado podrá ejercer la misma actividad en Francia, de forma temporal y ocasional.

El nacional simplemente tendrá que indicar la mención del título profesional o el título de formación que ostente en ese estado en los documentos que presentará en Francia a las personas relacionadas con la recepción turística, así como al museo o gerentes monumento histórico visitado.

En el caso de que la profesión no esté regulada, ni en el curso de la actividad ni en el marco de la formación, en el estado en el que el profesional esté legalmente establecido, deberá haber realizado esta actividad durante al menos un año durante los diez años antes del beneficio, en uno o más Estados de la UE o del EEE.

*Para ir más allá*: Artículos L. 211-1, L. 221-3, L. 221-4 y R. 221-14 del Código de Turismo.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de un Estado de la UE o del EEE establecido y que actúe legalmente como portaguías en uno de estos Estados podrá llevar a cabo la misma actividad en Francia de forma permanente.

Podrá solicitar la tarjeta profesional del prefecto (ver infra "5 grados. a. Solicitar una tarjeta de visita para el nacional de la UE o del EEE para un ejercicio permanente (LE) ), siempre que justifique:

- poseer un diploma, certificado u otro título que permita el ejercicio de la actividad a título profesional en un Estado de la UE o del EEE y expedido por:- la autoridad competente de ese Estado,
  - un tercer Estado, con la certificación de un Estado de la UE o del EEE que reconozca el título y certifique que ha estado en el negocio durante un mínimo de tres años;
- Poseer un título de formación adquirido en su estado de origen, específicamente dirigido al ejercicio de la profesión;
- durante al menos un año a tiempo completo o a tiempo parcial en los últimos diez años en un Estado de la UE o del EEE que no regula el acceso o el ejercicio de esta actividad.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación en Francia, el prefecto competente podrá exigir que el interesado se someta a una medida de compensación (véase infra "5o. a. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos L.221-2, R. 221-12 y R. 221-13 del Código de Turismo.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Cualquier persona que trabaje como guía-profesor sin tener una tarjeta profesional es multada con una multa de hasta 450 euros.

*Para ir más allá*: Artículo R. 221-3 del Código de Turismo y artículo 131-13 del Código Penal.

4°. Seguro
-------------------------------

El profesor-profesor liberal debe comprar un seguro de responsabilidad civil profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar una tarjeta de visita para la UE o el nacional del EEE para un ejercicio permanente (LE)

**Autoridad competente**

El prefecto del departamento en el que el nacional desea estableceres es competente para expedir la tarjeta de guía-altavoz profesional.

**Documentos de apoyo**

En apoyo de su solicitud, el nacional deberá presentar al prefecto competente un expediente que contenga los siguientes documentos justificativos:

- Un formulario de solicitud de tarjeta, completado y firmado;
- Un documento en el que se exponen las menciones específicas, ya sean linguísticas o científicas o culturales, que se incluirán en el mapa;
- Una fotocopia de un documento de identidad válido
- Uno o más documentos fotográficos
- Una copia del diploma, certificado o título obtenido en un Estado de la UE o del EEE;
- un certificado que justifique su actividad durante al menos un año en los últimos diez años, cuando ni la formación ni el ejercicio de la profesión estén regulados en la UE o en el Estado del EEE.

**Resultado del procedimiento**

El prefecto dispondrá de un mes a partir de la recepción de las pruebas justificativas para reconocerla o solicitar el envío de los documentos que faltan. La decisión de emitir la tarjeta de visita tendrá lugar en un plazo de dos meses.

El silencio guardado al final de este período valdrá la pena la decisión de otorgar la tarjeta profesional.

**Costo**

Gratis.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales en la formación imprevista por el nacional y la exigida en Francia, el prefecto competente podrá someter al interesado a la medida de compensación de su elección, a saber:

- un curso de adaptación que no puede durar más de tres años;
- una prueba de aptitud.

El individuo tendrá entonces dos meses para dar a conocer la medida de su elección.

En el caso de que el nacional posea un certificado de competencia o un título sancionando un diploma de escuela secundaria, la decisión sobre la elección de la medida de compensación será hasta el prefecto en un plazo de dos meses.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

