﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP131" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios agrícolas" -->
<!-- var(title)="Experto en tierra y agricultura" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-agricolas" -->
<!-- var(title-short)="experto-en-tierra-y-agricultura" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-agricolas/experto-en-tierra-y-agricultura.html" -->
<!-- var(last-update)="2020-04-15 17:20:48" -->
<!-- var(url-name)="experto-en-tierra-y-agricultura" -->
<!-- var(translation)="Auto" -->


Experto en tierra y agricultura
===============================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:48<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El experto en tierra y agricultura es un profesional que lleva a cabo misiones de especialización en tierras y agricultura que se ocupan de la propiedad de otros, muebles y edificios, así como los derechos de propiedad y propiedad relacionados con estas propiedades.

Es particularmente activo en las siguientes áreas:

- estimación de edificios y derechos de propiedad e inmuebles, tanto urbanos como rurales;
- estimaciones de los valores de alquiler y de la propiedad urbana y rural;
- proyectos de redacción y asesoramiento jurídico;
- uso de la tierra y gestión ambiental sostenible (asesoramiento, apoyo);
- producción agropecua-animal o vegetal;
- Estimación de la granja y sus componentes;
- Estimación del daño
- evaluación de los daños ecológicos y la aplicación de las medidas de compensación (ERC).

*Para ir más allá*: Artículo L. 171-1 del Código Rural y Pesca Marina.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El profesional debe ser incluido en la lista nacional de expertos en tierra y agrícolas elaborada por el comité del Consejo Nacional de Experiencia en Tierras Agrícolas y Forestales (Cnefaf).

El registro está abierto a la persona que cumpla con las siguientes condiciones:

- una práctica profesional en los campos de la tierra y la experiencia agrícola, ya sea personalmente o bajo la responsabilidad de un aprendiz:- al menos siete años,
  - al menos tres años si el profesional posee un título o diploma sancionando al menos cuatro años de educación postsecundaria en las disciplinas agrícola, agronómica, ambiental, forestal, jurídica o económica o en el áreas de uso de la tierra, paisajes o planificación urbana;
- no haber sido condenado por hechos contrarios al honor, la probidad o la buena moral en los últimos cinco años;
- no una sanción disciplinaria o administrativa por despido, despido, despido, retirada de la acreditación o autorización;
- no han sido golpeados con bancarrota personal.

*Para ir más allá*: Artículos L. 171-1 y R. 171-10 del Código Rural y Pesca Marina.

#### Entrenamiento

Varios cursos que ofrecen títulos y diplomas como el grado de ingeniería o máster especializado, permiten seguir un curso para convertirse en un experto en tierra y agricultura.

Por ejemplo, la persona puede ser entrenada para obtener (lista no exhaustiva):

- un grado en ingeniería agrícola o ingeniero agrícola;
- un máster en "experiencia en tierra" ([Escuela IHEDREA](http://www.ihedrea.org/)).

#### Costos asociados con la calificación

La formación que conduce a la profesión de experto en tierra y agricultura se paga y el costo varía dependiendo de la institución elegida. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

El nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) puede llevar a cabo misiones de expertos en Francia, ya sea de forma temporal o ocasional, con sujeción a:

- estar legalmente establecido en uno de estos Estados para ejercer como experto en tierra y agrícola;
- cuando ni la profesión ni la formación estén reguladas en ese Estado para haber ejercido como experto en tierra y agricultura en ese Estado durante al menos un año o a tiempo parcial durante un período de tiempo equivalente en los últimos diez años;
- estar asegurado contra las consecuencias pecuniarias de su responsabilidad civil profesional.

Para ello, el nacional tendrá que enviar una declaración al Cnefaf antes de su primera actuación (véase infra "5o. a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)").

*Para ir más allá*: Artículo L. 171-2 del Código Rural y Pesca Marina.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Cualquier nacional de un Estado miembro de la Unión Europea u otro Estado parte en el acuerdo sobre el Espacio Económico Europeo que desee establecerse en Francia para ejercer como experto en tierra y agricultura tendrá que solicitar su inclusión en la lista expertos nacionales en tierra y agricultura del comité de Cnefaf (véase infra "5o. b. Solicitar la inclusión en la lista de expertos en tierra y agrícolas para la UE o el EEE nacionales para un ejercicio permanente (LE)).

El comité de Cnefaf hará una comparación entre, por un lado, la formación necesaria en Francia para ser incluida en la lista y, por otro, la recibida por el solicitante, así como los conocimientos, habilidades y habilidades que adquirió durante su experiencia laboral o aprendizaje permanente que ha sido validado por un organismo competente.

Cuando este examen muestra una diferencia sustancial en la formación en cuanto a las cualificaciones necesarias para el acceso a la profesión y su práctica en Francia, que los conocimientos adquiridos por el solicitante durante su experiencia profesional no es probable que llenen, total o parcialmente, el comité someterá al solicitante a una medida de compensación (curso de ajuste o prueba de aptitud).

*Para ir más allá*: Artículos L. 171-3, R. 171-12-1, R. 2045 y R. 171-12-2 del Código Rural y Pesca Marina.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Reglas y deberes profesionales

El experto en tierra y agricultura está obligado por las normas y deberes de su profesión durante el ejercicio de su actividad.

Como tal, el experto en tierra y agrícola se compromete a:

- Respetar la independencia necesaria para llevar a cabo la profesión;
- hacer una declaración imparcial;
- Respetar el secreto profesional
- abstenerse de cualquier práctica desleal hacia sus colegas.

*Para ir más allá*: Artículos L. 171-1, R. 172-2 y siguientes del Código Rural y Pesca Marina.

### b. Incompatibilidades

La profesión de experto en tierra y agricultura es incompatible:

- con las oficinas de funcionario público y ministerial;
- con cualquier función que pudiera menoscabar su independencia, en particular la de adquirir bienes personales o inmobiliarios de manera habitual para su reventa.

*Para ir más allá*: Artículo L. 171-1 del Código Rural y Pesca Marina.

4°. Seguros y sanciones
--------------------------------------------

### Seguro

El experto liberal en tierra y agrícola debe conseguir un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

*Para ir más allá*: Artículo L. 171-1 párrafo 8 del Código Rural y Pesca Marina.

### Sanciones disciplinarias

Cualquier incumplimiento de las normas profesionales por parte de la tierra y experto agrícola puede estar sujeto a procedimientos disciplinarios ante el comité de Cnefaf y una sanción disciplinaria que puede ser:

- Culpar
- Una advertencia
- una suspensión temporal de tres meses a tres años;
- una inhabilitación en caso de falta profesional grave o condena por actos contrarios al honor, la probidad y la buena moral.

*Para ir más allá*: Artículo L. 171-1 párrafo 7 del Código Rural y Pesca Marina.

##### Sanciones penales

Cualquier persona se enfrenta a una pena de un año de prisión y una multa de 15.000 euros por usurpación del título si utiliza:

- el título de experto en tierra y agricultura sin ser incluido en la lista establecida por el comité de Cnefaf;
- una denominación que puede ser confusa con el título de tierra y experto agrícola.

*Para ir más allá*: Artículo L. 171-1 párrafo 9 del Código Rural y Pesca Marina; Artículo 433-17 del Código Penal.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)

**Autoridad competente**

El profesional debe hacer una declaración previa con el Cnefaf.

**Documentos de apoyo**

Su declaración debe incluir los siguientes documentos:

- prueba de nacionalidad
- Un certificado que certifique que está legalmente establecido en un Estado de la UE o del EEE;
- prueba de que el nacional ha participado en actividades de experiencia en tierras y productos agrícolas durante al menos un año o a tiempo parcial durante un período de tiempo equivalente en los últimos diez años cuando la actividad o formación no está regulada en su estado Miembro original;
- información sobre la suscripción de pólizas de seguro detallando el nombre y dirección de la compañía de seguros, las referencias y el período de validez del contrato, el alcance y el importe de las garantías.

Estos documentos se adjuntan, según sea necesario, a su traducción al francés.

*Para ir más allá*: Artículos R. 171-17- a R. 171-17-3 del Código Rural y Pesca Marina.

### b. Solicitar la inclusión en la lista de expertos en tierra y agrícolas para la UE o el EEE nacionalpara un ejercicio permanente (LE)

**Autoridad competente**

El comité de Cnefaf es responsable de decidir sobre la solicitud de inclusión en la lista de expertos en tierra y agrícolas.

**Documentos de apoyo**

La solicitud deberá contener lo siguiente, en su caso, acompañado de su traducción al francés:

- Todas las pruebas que justifican el estado civil del solicitante;
- Una copia de sus títulos o diplomas
- prueba de experiencia laboral
- Un cv que detalla las actividades profesionales anteriores del profesional (fecha y lugar de práctica);
- una prueba o, en su defecto, un compromiso de seguro de responsabilidad profesional subestituto;
- un extracto de antecedentes penales no 3 menos de tres meses de edad o cualquier documento equivalente expedido por la autoridad competente del Estado de la UE o el EEE de menos de tres meses;
- una declaración de honor o cualquier otra evidencia de que la persona está cumpliendo con las condiciones de honor;
- en caso necesario, una declaración de la actividad prevista en forma de empresa.

**Procedimiento**

Una vez recibido el expediente, el comité dispone de tres meses para informar al nacional de su decisión de incluirlo en la lista.

No obstante, en caso de diferencias sustanciales entre la formación profesional y la experiencia del nacional y las requeridas en Francia, el comité podrá someter al nacional a la medida de compensación de su elección, es decir, a un curso de ajuste, o a una prueba de acondicionamiento físico.

El silencio guardado por un período de tres meses vale la pena aceptar la decisión.

La renovación de la solicitud está sujeta a la producción del certificado de seguro de responsabilidad profesional.

*Para ir más allá*: Artículos R. 171-10 a R. 171-13 del Código Rural y Pesca Marina.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

