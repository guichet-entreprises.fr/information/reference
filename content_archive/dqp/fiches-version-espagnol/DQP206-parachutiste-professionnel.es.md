﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP206" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sector aéreo" -->
<!-- var(title)="Paracaidista profesional" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sector-aereo" -->
<!-- var(title-short)="paracaidista-profesional" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sector-aereo/paracaidista-profesional.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="paracaidista-profesional" -->
<!-- var(translation)="Auto" -->


Paracaidista profesional
========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El paracaidista profesional es un aircrew profesional en aviación civil. Su actividad consiste en caer de una altura de cien metros a varios miles, generalmente saliendo de un avión, y luego regresar a la tierra con la ayuda de un paracaídas.

Después de la salida, el paracaidista está en caída libre durante un período más o menos largo dependiendo de la disciplina practicada, la altura a la que fue abandonado y la altitud relativa de apertura. Puede actuar solo, por ejemplo en acrobacias, o con otros paracaidistas de las figuras durante la caída antes de llegar al suelo volando su paracaídas para aterrizar en el lugar previsto.

El profesional puede realizar saltos de demostración pagados o completar una misión de trabajo aéreo.

*Para ir más allá*: Artículo L. 6521-1 del Código de Transporte; orden de 25 de agosto de 1954 relativa a la clasificación de la aeronave profesional en la aviación civil; Artículo 1 de la orden sobre la creación de una patente y una licencia de paracaidista profesional y una titulación de instructor.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Como aviador profesional en aviación civil, el paracaidista profesional está sujeto a la aplicación de los artículos L. 6521-2 del Código de Transporte, que establecen que deben ser:

- titular de un título aeronáutico válido;
- registrados y en una de tres categorías:- ensayos y recepciones,
  - Transporte aéreo,
  - trabajo aéreo.

Para llevar a cabo su actividad, el paracaidista debe poseer la patente y la licencia correspondiente a la naturaleza del salto previsto requerido.

**Tenga en cuenta que**

Aircrew que presta servicios de transporte aéreo o de trabajo establecidos en un Estado miembro de la Unión Europea (UE) distinto de Francia o en un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) o acuerdos bilaterales la UE con la Confederación Suiza, así como el asalariado de un proveedor de servicios de transporte aéreo o de trabajo con sede en uno de los Estados antes mencionados, que están temporalmente activos en Francia, no están comprendidos en el ámbito de aplicación la aplicación de esta obligación. Este personal se refiere a las disposiciones de los "b. nacionales de la UE: para un ejercicio temporal y ocasional (entrega gratuita de servicios)" y "c. nacionales de la UE: para un ejercicio permanente (establecimiento libre)" a continuación.

*Para ir más allá*: Artículos L. 6521-1 y siguientes del Código de Transporte; Artículo 2 del Decreto del 3 de diciembre de 1956 sobre la creación de una patente y licencia de paracaidista profesional y una titulación de instructor.

#### Entrenamiento

Nadie puede practicar paracaidismo si no puede justificar que posee la licencia correspondiente a la naturaleza del salto planificado requerido con todas las cualificaciones necesarias.

La licencia y las cualificaciones sólo pueden expedirse a los titulares de patentes.

*Para ir más allá*: Artículo 2 del Decreto del 3 de diciembre de 1956 sobre la creación de una patente y licencia de paracaidista profesional y una titulación de instructor.

##### Patente y licencia de paracaidista profesional

La patente es un título que sanciona un conjunto de conocimientos teóricos y prácticos generales. Se expide después del examen y se adquiere definitivamente de su titular.

La licencia es un título de 12 meses, sancionando la idoneidad y el derecho de los titulares de patentes a desempeñar las funciones correspondientes.

Se renovará por un período de la misma duración, siempre que la demandante haya completado veinte saltos en los doce meses anteriores a la solicitud de renovación o cinco saltos en los seis meses anteriores a dicha solicitud. Sólo se tendrán en cuenta los saltos durante los cuales se utilizaron los paracaídas.

El incumplimiento de estas condiciones debe cumplir con la comprobación de un instructor de las pruebas prácticas necesarias para emitir la licencia.

*Para ir más allá*: Artículos 1, 7 y 9 de la orden del 3 de diciembre de 1956 sobre la creación de una patente y licencia de paracaidista profesional y una calificación de instructor.

###### Prerrogativas 

La licencia de paracaidista profesional permite al titular realizar todo tipo de saltos con equipos de acuerdo con la normativa vigente para la remuneración.

*Para ir más allá*: Artículo 9 del auto de 3 de diciembre de 1956 supra.

###### Condiciones para la expedición de la patente y la licencia

El interesado deberá:

- poseer un certificado médico de Clase 1. Para obtener más información sobre este certificado, se recomienda hacer referencia al auto de 27 de enero de 2005 sobre la aptitud física y mental de la aeronave técnica de la aviación civil;
- Tener dieciocho años;
- o un total de doscientos cincuenta saltos, incluyendo al menos doscientos saltos durante los cuales utilizó sólo el dispositivo de apertura controlada y que comprende un mínimo de veinticinco caídas libres de una duración de 30 segundos o más;
- es decir, un total de doscientos saltos, incluidos al menos ciento cincuenta saltos durante los cuales utilizó sólo el dispositivo de apertura controlada y que comprende un mínimo de veinticinco caídas libres de una duración de 30 segundos o más, si justifica tener siguió de manera completa y satisfactoria una enseñanza certificada;
- cumplir con las pruebas teóricas sobre aerodinámica, construcción y equipamiento de paracaídas, técnica y uso de la implementación del paracaídas, meteorología y regulaciones de aviación;
- cumplir con las pruebas prácticas: estos son eventos en vuelo que incluyen una prueba de saltos y una prueba de caída de hardware.

*Para ir más allá*: Artículos 4 y 9 del auto de 3 de diciembre de 1956 antes mencionado; Artículo 3 y anexo a la orden del 25 de abril de 1962 sobre el programa y el régimen de examen de la patente y licencia del paracaidista profesional.

**Es bueno saber**

Para obtener la patente y la licencia del paracaidista profesional por equivalencia, el candidato debe:

- Tener 18 años de edad y poseer un certificado médico de clase 1;
- Ser titular:- que, durante al menos doce meses, un diploma estatal de juventud, educación popular y deporte (DEJEPS) especialidad "desarrollo deportivo" mención "paracaidismo", un certificado profesional de juventud, educación popular y deporte ( BPJEPS) especialidad "paracaídas" o certificado estatal de educador deportivo (BEES) 1o grado, opción "paracaidismo", y habiendo cumplido las pruebas teóricas del certificado de paracaidista profesional,
  - o, desde un certificado de instructor hasta el salto de apertura controlado retrasado emitido por el Ministro de Defensa y justificar haber llevado a cabo una formación completa y satisfactoria sobre las normas de aviación civil proporcionadas por la autoridad militar y certificado por una organización de las fuerzas armadas,
  - o bien, el certificado de aviador o paracaidista experimentado de pruebas y recepciones emitidos por el Ministro de Defensa y justifican haber llevado a cabo una formación completa y satisfactoria sobre las normas de aviación civil proporcionado por la autoridad militar y atestiguado por un organismo de las fuerzas armadas;
- 20 saltos en los 12 meses anteriores a la solicitud.

*Para ir más allá*: Artículo 9-2 del auto de 3 de diciembre de 1956.

##### Calificación de Instructor

Se requiere una cualificación de instructor para facultar al titular de la licencia de paracaidista profesional para dar o dirigir la formación de vuelo necesaria para obtener esta licencia.

###### Validez

La calificación de instructor tiene una validez de treinta y seis meses, prorrogable por período de la misma duración, siempre que el interesado justifique haber realizado la formación de al menos cincuenta saltos durante los veinticuatro meses anteriores a la solicitud.

Si la persona no ha llevado a cabo este número de saltos, debe cumplir con la prueba de aptitud de un instructor de paracaidista profesional para las tareas de un instructor de paracaidista profesional.

###### Condiciones

El candidato debe cumplir las siguientes condiciones:

- total de trescientos cincuenta saltos, incluyendo al menos trescientos saltos durante los cuales utilizó sólo el dispositivo de apertura controlada y que comprende un mínimo de cuarenta caídas libres que duran entre 30 y 60 segundos y un mínimo de diez caídas libres que duran más de 60 segundos;
- han completado un curso satisfactorio y completo como instructor de paracaidistas.

*Para ir más allá*: Artículo 10 de la orden del 3 de diciembre de 1956 sobre la creación de una patente y licencia de paracaidista profesional y una calificación de instructor.

**Es bueno saber**

Para calificar como instructor de paracaidista profesional por equivalencia, se deben cumplir los siguientes requisitos:

- Ser titular válido de una licencia de paracaidista profesional o cumplir los requisitos para obtener la patente y la licencia paracaidista profesional por equivalencia;
- Ser titular:- ya sea, durante al menos doce meses, una especialidad DEJEPS "desarrollo deportivo" mención "paracaidismo", o un BEES de 2o grado, opción "paracaidismo",
  - ya sea, desde una cualificación de entrenador hasta el progreso acompañado en una caída o una calificación de un entrenador de piloto de paracaídas biplaza con transporte de pasajeros expedido por el Ministro de Defensa,
  - ya sea el certificado de paracaidista que navega por el experimentador o paracaidista de pruebas y recepciones, subjefe de misión mínima, expedido por el Ministro de Defensa;
- han realizado al menos cincuenta saltos en los últimos 24 meses antes de la solicitud.

*Para ir más allá*: Artículo 10.2 de la orden del 3 de diciembre de 1956 sobre la creación de una patente y licencia de paracaidista profesional y una calificación de instructor.

##### Clasificación de saltos de paracaídas biplaza

###### Prerrogativas

Esta calificación es necesaria para capacitar al titular de una licencia de paracaidista profesional para realizar saltos en paracaídas biplaza.

*Para ir más allá*: Artículo 9-1 del Decreto del 3 de diciembre de 1956 sobre la creación de una patente y licencia de paracaidista profesional y una calificación de instructor; orden del 30 de mayo de 2011 sobre la formación, calificación y práctica de saltos de paracaídas biplaza por paracaidistas profesionales.

**Qué saber**

Cada piloto de paracaídas de dos plazas debe, antes de llevar un pasajero, asegurarse de:

- que la información informativa de los pasajeros fue realizada por un paracaidista que posee la calificación de salto en paracaídas biplaza;
- Tiene al menos un alticata y un cortador de correa;
- en condiciones normales, que el dispositivo principal de apertura de la vela se implementa a una altura mínima de 1.500 metros.*Para ir más allá*: Artículo 9 de la orden de 30 de mayo de 2011.

###### Validez

La calificación tiene una validez de doce meses. Se amplía si el paracaidista profesional justifica, en los doce meses anteriores al final de la fecha de validez, la realización de cien saltos, incluyendo treinta en un paracaídas biplaza o un salto de prueba con un instructor de salto de paracaídas biplaza, este último tomando Asiento del pasajero.

Además, el paracaidista debe realizar un control a bordo con un instructor de salto en paracaídas de dos plazas antes de una verificación de conocimientos teóricos cada cinco años a partir de la fecha de calificación. La calificación se renueva si justifica haber completado el salto de prueba en los treinta días anteriores.

###### Condiciones de emisión

El interesado deberá:

- Poseer una licencia de paracaidista profesional
- Haber completado y completado satisfactoriamente la formación teórica y práctica;
- saltos supervisados realizados satisfactoriamente.

###### Condiciones de admisión a la formación

El interesado deberá:

- Poseer un certificado de aptitud para las pruebas prácticas de vuelo del examen para la licencia de paracaidista profesional válida o una licencia de paracaidista profesional;
- justifican un total mínimo de mil saltos de caída libre, incluyendo al menos doscientos saltos en los últimos veinticuatro meses y al menos cien saltos en los últimos doce meses. Al menos 200 saltos deben haberse realizado utilizando un dispositivo de apertura de extractor flexible;
- cumplir con dos saltos prácticos de selección.

*Para ir más allá*: Artículo 2 de la orden de 30 de mayo de 2011.

**Es bueno saber**

Para calificar para un salto en paracaídas biplaza por equivalencia, el titular de una licencia de paracaidista profesional válida debe cumplir los siguientes requisitos:

- justifican un total mínimo de mil saltos de caída libre, incluyendo al menos doscientos saltos en los últimos veinticuatro meses y al menos cien saltos en los últimos doce meses. Al menos 200 saltos deben haberse realizado utilizando un dispositivo de apertura de extractor flexible;
- justificar la realización de al menos cien saltos de paracaídas de dos plazas;
- Ser titular:- ya sea, durante al menos doce meses, una especialidad "paracaidismo" BPJEPS, o un BEES de primer grado, una opción de "paracaídas", una especialidad de enseñanza en tándem,
  - ya sea una cualificación de piloto de paracaídas operacional de dos plazas en el transporte de pasajeros o una cualificación de entrenador de piloto de paracaídas biplaza con transporte de pasajeros;
- tienen una experiencia reciente de 100 saltos, incluyendo treinta saltos de paracaídas de dos plazas, en los doce meses anteriores a la demanda;
- han renovado los conocimientos teóricos específicos de la práctica del salto en paracaídas biplaza.

*Para ir más allá*: Artículo 8 del auto de 30 de mayo de 2011 supra.

#### Costos asociados con la calificación

La formación que conduce a la patente y la licencia se paga. El costo varía según la organización de la capacitación.

Para más detalles, es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Todo nacional de un Estado miembro de la UE o del EEE que tenga previsto prestar servicios de forma temporal y ocasional deberá:

- Estar legalmente establecido en un Estado miembro para trabajar como paracaidista profesional;
- han sido paracaidistas en uno o más Estados miembros a tiempo completo durante al menos un año o a tiempo parcial durante un período total equivalente a los diez años anteriores a la prestación cuando la profesión o la formación en ese país la conducción no está regulada en el Estado miembro del establecimiento.

*Para ir más allá*: Artículo 2 bis del auto de 3 de diciembre de 1956 supra.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Cualquier nacional de un Estado miembro de la UE o del EEE puede obtener una licencia de paracaidista profesional y, en su caso, las cualificaciones asociadas:

- si tiene el certificado de competencia o la formación requerida para realizar actividad de paracaidistas en uno de estos estados cuando regula la profesión;
- o si tiene uno o más certificados de competencia o documentos de prueba de formación expedidos por una autoridad competente de uno de estos Estados miembros que no regula nadea esta profesión, y acredite que ha sido preparado para el ejercicio de esta actividad, y si ha trabajado como paracaidista profesional a tiempo completo o a tiempo parcial durante un período total equivalente a los diez años anteriores en uno o más de los estados que no regulan esta ocupación.

*Para ir más allá*: Artículo 2 bis del auto de 3 de diciembre de 1956 supra.

3°. Legislación social
-------------------------------

El Código de Trabajo es aplicable a los asistentes de vuelo de aviación civil y a sus empleadores, a menos que exista una disposición específica.

### a. Contrato de trabajo

El compromiso de un miembro profesional de la tripulación de vuelo da lugar necesariamente al establecimiento de un contrato de trabajo escrito que especifica diferentes menciones.

Si el contrato se celebra para una misión específica, indica el destino final de la misión y el tiempo desde el que se considera cumplido.

Cada ave de vuelo asalariada tiene derecho a un salario garantizado mensualmente.

### b. Tiempo de trabajo

Las disposiciones del Código del Trabajo relativas al descanso obligatorio, el trabajo nocturno y el descanso diario no se aplican a los aircrew de la aviación civil.

El tiempo de servicio anual de los paracaidistas no puede exceder las 2.000 horas, en las que el tiempo de vuelo está limitado a 900 horas.

Además de los períodos de licencia legal definidos por el Código de Trabajo, estos empleados se benefician de al menos 7 días al mes y al menos 96 días por año natural sin ningún servicio y obligación. Estos días, notificados con antelación, pueden incluir períodos de descanso y todo o parte del tiempo de inactividad determinado por la ley o la regulación.

### c. Ejecución del contrato de trabajo

A menos que se trata de proporcionar un servicio público, los empleados aéreos y las azafatas adicionales sólo pueden ser asignados al trabajo aéreo en áreas de hostilidad civil y militar si son voluntarios. A continuación, un contrato especial establece las condiciones especiales de trabajo y cubre expresamente, aparte de los riesgos habituales, los riesgos particulares debidos a las condiciones de empleo.

Con excepción, la actividad de paracaidistas no podrá llevarse a cabo en el transporte aéreo público más allá de los sesenta años.

*Para ir más allá*: Artículos L. 6521-6 y siguientes, L. 6523-1 y siguientes del Código de Transporte.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Pre-registro de los registros de la tripulación aérea

Como aviador profesional en aviación civil, un paracaidista profesional que practice de manera habitual y principal, en su propio nombre o en nombre de otros, con fines de lucro o por honorarios, debe:

- Tener una designación de aviación válida
- inscribirse en el registro de sus funciones y en una de las tres categorías:- ensayos y recepciones,
  - trabajo aéreo.

Para ser inscrito en el registro correspondiente a sus funciones, el candidato no debe tener en el número 2 de su expediente penal ninguna referencia incompatible con el desempeño de las funciones a las que está solicitando.

#### Autoridad competente 

La solicitud debe dirigirse a la Dirección General de Aviación Civil, 50 Henry Farman Street, 75720 Paris Cedex 15.

#### Documentos de apoyo

Los documentos justificativos que se proporcionarán son:

- Forma Cerfa 47-0049;
- Una copia de un documento de identidad
- Un permiso de residencia
- un extracto no.3 del historial delictivo de menos de tres meses;
- Un certificado médico
- una declaración de honor (incluyendo la certificación de que usted es un marino regular y principal);
- una declaración de no pertenencia al servicio público;
- para la gente de mar titularde una licencia profesional establecida por un Estado miembro de la UE y validada por Francia, una copia de esta validación;
- para los empleados, una copia certificada del contrato de trabajo mencionando explícitamente su fecha de empleo y las funciones realizadas;
- para los trabajadores por cuenta propia, el certificado de registro en el registro mercantil o el certificado de declaración a los servicios fiscales que muestren claramente la actividad de la gente de mar por cuenta propia;
- para el personal militar de carrera, el certificado de la autoridad militar que certifica que se colocan en una posición cuya duración no se tiene en cuenta en el cálculo de la pensión militar y que no les prohíbe una actividad remunerada primaria fuera Ejército.

*Para ir más allá*: Artículos L. 6521-2 y L. 6521-3 del Código de Transporte; decreto de 21 de enero de 1998 sobre los procedimientos de registro de aeronaves profesionales en la aviación civil.

### b. Hacer una predeclaración de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Antes de la primera prestación de servicios, el nacional debe presentar una solicitud por escrito a la autoridad competente que pueda ordenar una auditoría de sus cualificaciones profesionales.

La inspección realizada debe permitir al Ministro encargado de la aviación civil, previa asesoría del Consejo del Personal Profesional de Vela de Aeronáutica Civil o de su grupo de expertos, garantizar que el interesado, para el ejercicio de la actividad paracaidista no presenta ninguna insuficiencia profesional que pueda afectar a la seguridad.

Después de todos los controles, si el candidato se considera apto, se le expide la licencia de paracaidista profesional y, si es necesario, las calificaciones asociadas. De lo contrario, se objeta la solicitud y el solicitante no está autorizado a realizar actividades profesionales de paracaidistas. El solicitante puede representarse a sí mismo en la prueba de aptitud.

#### Autoridad competente

La solicitud debe dirigirse al Ministro de Aviación Civil (Dirección General de Aviación Civil, 50 Henry Farman Street, 75720 Paris Cedex 15).

#### Condiciones de licencia

El solicitante debe:

- cumplir con las normas médicas (véase supra "2 a. Capacitación");
- justificar el conocimiento de las reglamentaciones nacionales de aviación ya sea a través de un curso de adaptación con un operador o mediante la aprobación de la prueba de derecho aéreo 010 del examen teórico de la licencia de piloto comercial (CPL);
- cumplir con los requisitos mínimos de experiencia.

#### hora 

En el plazo de un mes a partir de la recepción de la solicitud y de los documentos adjuntos, el Ministro responsable de la aviación civil:

- informa al interesado del resultado de la auditoría;
- o solicitudes de más información especificando la información que se debe proporcionar. En este caso, toma la decisión en el plazo de dos meses a partir de la recepción de la información adicional.

#### Medidas de compensación

El Ministro de Aviación Civil podrá exigir al solicitante que demuestre que tiene los conocimientos, habilidades o habilidades que faltan a través de una prueba de aptitud:

- en caso de diferencia sustancial entre las competencias profesionales del solicitante y las adquiridas por la formación teórica y práctica necesaria para llevar a cabo la actividad de paracaidista profesional en territorio francés;
- o cuando estos son más bajos, ya que esta diferencia puede afectar a la seguridad de la aviación y no puede ser compensada por la experiencia laboral de la persona o por los conocimientos, habilidades y habilidades adquiridas durante el aprendizaje permanente que ha sido validado por un organismo competente para este propósito.

*Para ir más allá*: Artículo 2 bis del Decreto del 3 de diciembre de 1956 sobre la creación de una patente y licencia de paracaidista profesional y una cualificación de instructor.

### c. Obtención de una licencia para los nacionales de la UE para un ejercicio permanente (LE)

Para obtener la licencia y, en su caso, las cualificaciones asociadas, el profesional debe solicitara a la autoridad competente.

Después de todos los controles, si el candidato se considera apto, se le expide la licencia de paracaidista profesional y, si es necesario, las calificaciones asociadas.

De lo contrario, se objeta la solicitud y el solicitante no está autorizado a realizar actividades profesionales de paracaidistas.

#### Autoridad competente 

El solicitante debe presentar su solicitud al Ministro de Aviación Civil (Dirección de Aviación Civil, 50 Henry Farman Street, 75720 Paris Cedex 15).

#### Condiciones de licencia

El solicitante debe:

- Cumplir con los estándares médicos
- justificar el conocimiento de las reglamentaciones nacionales de aviación ya sea a través de un curso de adaptación con un operador o mediante la aprobación de la prueba de derecho aéreo 010 del examen teórico de la licencia de piloto comercial (CPL);
- cumplir con los requisitos mínimos de experiencia.

Si existen serias y concretas dudas sobre el nivel suficiente de conocimientos de idiomas en francés de la persona que se beneficia del reconocimiento de sus cualificaciones profesionales en relación con las actividades de paracaidista profesional, el Ministro puede imponer un control de las habilidades del lenguaje.

#### Medidas de compensación

Cuando los conocimientos, habilidades y habilidades que el solicitante ha adquirido a través de la formación y la experiencia laboral y el aprendizaje permanente son sustancialmente diferentes o menos en términos de contenido que los adquiridos por formación teórica y práctica para llevar a cabo la actividad de paracaidista profesional en Francia, el Ministro encargado de la aviación civil puede, previa asesoría del Consejo de la tripulación aérea profesional de la aeronáutica civil o de su grupo experto, tomar la decisión debidamente justificada de imponer a la persona:

- un curso de adaptación que está siendo evaluado y posiblemente acompañado de una mayor formación;
- o una prueba de aptitud para evaluar la capacidad del solicitante para realizar actividades de paracaidistas profesionales.

La elección queda en el solicitante. El Ministro de Aviación Civil se asegura de que el solicitante tenga la oportunidad de presentar la prueba de aptitud en un plazo máximo de seis meses a partir de la decisión inicial que requiere la prueba de aptitud del solicitante.

*Para ir más allá*: Artículo 2 bis del Decreto del 3 de diciembre de 1956 sobre la creación de una patente y licencia de paracaidista profesional y una cualificación de instructor.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

