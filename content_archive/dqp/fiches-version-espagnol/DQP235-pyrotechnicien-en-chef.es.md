﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP235" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artesanía" -->
<!-- var(title)="Jefe de Pirotécnico" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artesania" -->
<!-- var(title-short)="jefe-de-pirotecnico" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/artesania/jefe-de-pirotecnico.html" -->
<!-- var(last-update)="2020-04-15 17:20:37" -->
<!-- var(url-name)="jefe-de-pirotecnico" -->
<!-- var(translation)="Auto" -->


Jefe de Pirotécnico
===================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:37<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El pirotécnico o artífice es un profesional cuya actividad consiste en la puesta en escena y realización de espectáculos pirotécnicos (fuegos artificiales) compuestos por diferentes explosivos.

Los fuegos artificiales se clasifican según su tipo de uso, el nivel de riesgo y el nivel de ruido producido durante su uso:

- Las categorías 1 a 4 representan elementos de entretenimiento clasificados según el tamaño del peligro representado y el nivel de ruido producido cuando se utilizan;
- Las categorías T1 y T2 se refieren a las pirotecnias destinadas a la escenificación, clasificadas según la naturaleza del peligro presentado;
- las categorías P1 y P2 se refieren a la pirotecnia distintades de los artículos de entretenimiento y teatro y también clasificadas según su peligrosidad.

*Para ir más allá*: Artículo 13 del Decreto No 2010-455, de 4 de mayo de 2010, sobre la comercialización y el control de productos explosivos.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de pirotécnico, el profesional debe:

- Estar certificado cuando utilice dispositivos de Categoría 4, T2 y P2;
- cuando desee realizar un espectáculo pirotécnico, primero debe hacer una declaración al ayuntamiento donde desea realizarlo (ver infra "Pre-declaración para un espectáculo pirotécnico").

*Para ir más allá*: Artículo 28 del Decreto de 4 de mayo de 2010 supra.

#### Entrenamiento

**Certificación de cualificación**

Para obtener el Certificado de Calificación, el profesional debe completar la formación con un organismo acreditado, con una duración mínima de dos días para el Nivel 1 y tres días para el Nivel 2. El contenido de esta formación es determinado por el organismo aprobado dentro de sus especificaciones.

El primer nivel permite al profesional llevar a cabo las operaciones de montaje, tiro y limpieza de la zona con pirotecnia de categoría 4 o T2 excepto los artificios:

- agua náutica;
- con un ingrediente activo de no más de 500 g por producto;
- cuyo diámetro del mortero de lanzamiento es inferior a 50 mm (en caso de castañas de aire) o inferior a 150 mm para otras pirotecnias disparadas por un mortero;
- con ángulos de apertura de menos de 30 grados.

El certificado de Nivel 2 permite al profesional llevar a cabo las operaciones de montaje, tiro y limpieza de la zona de tiro con todas las categorías de pirotecnia.

Al final de su formación y tras obtener un certificado de realización de la pasantía, el profesional es objeto de una evaluación de sus conocimientos y aptitudes, y en caso necesario, recibe un certificado de éxito en la pasantía.

**Autoridad competente**

El profesional debe solicitar un certificado de calificación a la prefectura del departamento de origen.

**Documentos de apoyo**

Esta solicitud debe incluir:

- un certificado de finalización de la pasantía y un certificado de éxito en la evaluación de los conocimientos, ambos de hace menos de cinco años y correspondientes al nivel solicitado;
- cuando el profesional desea obtener el Nivel 1, prueba de que ha participado en la edición o toma de tres espectáculos en los últimos cinco años que involucran la Categoría 4, K4 (acreditación) o pirotecnia T2 y supervisada por un artificante titular del certificado de cualificación;
- cuando el profesional desea obtener el Nivel 2, su certificado de nivel 1 de calificación que data de al menos un año, y la prueba de que ha participado en la asamblea o el rodaje de tres espectáculos pirotécnicos en los últimos dos años. Estos espectáculos deben cumplir las mismas condiciones que las mencionadas anteriormente.

**Procedimiento**

Una vez recibida la solicitud completa, el prefecto expide el certificado de cualificación que incluye información relativa a la identidad del profesional y a su nivel de cualificación. El certificado de nivel 1 tiene una validez de cinco años y el certificado de nivel 2 tiene una validez de dos años.

**Tenga en cuenta que**

La solicitud de renovación del certificado debe ser realizada por el profesional antes de su fecha de caducidad.

*Para ir más allá*: Artículos 28 a 39 de la orden 31 de mayo de 2010 adoptada en virtud de los artículos 3, 4 y 6 del Decreto No 2010-580, de 31 de mayo de 2010, relativo a la adquisición, posesión y utilización de dispositivos de entretenimiento y pirotecnia para el teatro.

#### Pre-declaración para un espectáculo pirotécnico

**Autoridad competente**

El profesional debe solicitar, al menos un mes antes de la fecha programada del tiroteo, al ayuntamiento de la comuna o a la prefectura del departamento donde desee realizar el espectáculo.

**Documentos de apoyo**

Su solicitud debe incluir el[Formulario Cerfa 14098*01](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14098.do) completado y firmado y los siguientes documentos:

- Fotocopia del certificado de calificación válido en el caso del uso de fuegos artificiales de categoría 4;
- un esquema de implementación que incluye un plan del campo de tiro, el perímetro de seguridad, puntos de agua que se pueden utilizar en caso de incendio y puntos de recepción de emergencia;
- Todas las medidas destinadas a limitar los riesgos para el público y el barrio;
- En el caso del uso de fuegos artificiales para ser lanzados con un mortero (categoría 1 y 2), la copia de la acreditación válida de la prefectura;
- La lista de productos implementados durante la feria;
- El certificado de responsabilidad civil que lo cubre frente a los riesgos asociados a la realización de esta actividad;
- cuando almacena temporalmente su equipo antes de la feria, la presentación de las condiciones de almacenamiento de los productos (masa total del ingrediente activo, la descripción de la instalación y su entorno, así como las distancias de aislamiento).

**Procedimiento**

Al recibir la solicitud, el alcalde y el prefecto emiten cada uno un recibo del archivo.

**Tenga en cuenta que**

El día del espectáculo, el organizador del espectáculo debe enviarles una lista de las personas que tendrán que manejar la pirotecnia, esta lista debe mencionar:

- Su identidad y fecha de nacimiento
- su nivel de certificación, así como el número de recibo del formulario de declaración de la feria.

Además, al final del espectáculo, su organizador debe dar esta lista a la prefectura del departamento del lugar de rodaje.

**Costo**

Gratis.

*Para ir más allá*: Artículos 20 a 22 del auto de 31 de mayo de 2010 supra.

#### Costos asociados con la calificación

Es aconsejable acercarse a las agencias pertinentes para obtener más información.

3°. Reglas profesionales
---------------------------------

**Condiciones para almacenar fuegos artificiales**

Los dispositivos pirotécnicos de almacenamiento profesional deben cumplir con todas las disposiciones relativas al equipo almacenado, así como el sitio y la sala de almacenamiento.

La posesión temporal de dispositivos de entretenimiento o pirotecnia está permitida hasta quince días antes de la fecha del espectáculo.

Por ejemplo, los dispositivos deben almacenarse en su embalaje original, en una sala cerrada, no accesible al público y bajo la supervisión de un guardia o un sistema de monitoreo electrónico. La presencia de dichos productos debe indicarse en este local. Además, ninguna vivienda o establecimiento público debe estar a menos de 50 metros de este local o incluso un edificio de gran altura dentro de 100m.

Este almacenamiento debe estar bajo el control de una persona designada por el organizador. El ayuntamiento del lugar donde se llevará a cabo el espectáculo también verifica el cumplimiento de este almacén con las normas de seguridad.

*Para ir más allá*: Artículos 3 a 18 del auto de 31 de mayo de 2010 supra.

**Obligaciones de seguridad**

El profesional debe garantizar el cumplimiento de las normas de seguridad del espectáculo pirotécnico e incluir:

- Asegurar que el campo de tiro esté delimitado, no accesible al público y que tenga recursos de extinción de incendios;
- proporcionar un punto de recepción de emergencia dentro del campo de tiro materializado por un letrero marcado como "punto de recepción de emergencia";
- Limpiar el área de tiro al final del espectáculo y recoger todos los residuos.

El ayuntamiento de la ciudad del lugar de tiro debe asegurarse de que se cumplen todos estos requisitos.

*Para ir más allá*: Artículos 23 a 27 del auto de 31 de mayo de 2010 como consecuencia de su resultado.

4°. Seguros y sanciones penales
----------------------------------------------------

**Seguro**

El pirotécnico, como profesional, debe obtener un certificado de seguro de responsabilidad civil por los riesgos incurridos durante su actividad profesional.

Por otro lado, cuando es un empleado, le depende al empleador tomar este seguro para sus empleados.

**Sanciones penales**

En caso de incumplimiento de las obligaciones de seguridad, seguridad y almacenamiento, el profesional incurre en una multa de 750 euros, así como una posible sanción adicional de confiscación de los artificios.

Además, un profesional que:

- organiza un espectáculo pirotécnico sin antes hacer una declaración al ayuntamiento;
- no posee un certificado de calificación.

*Para ir más allá*: Artículos 8 a 10 del decreto del 31 de mayo de 2010.

