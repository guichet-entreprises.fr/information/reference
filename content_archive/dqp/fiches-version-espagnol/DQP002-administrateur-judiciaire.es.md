﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP002" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Administrador Judicial" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="administrador-judicial" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/administrador-judicial.html" -->
<!-- var(last-update)="2020-04-15 17:20:58" -->
<!-- var(url-name)="administrador-judicial" -->
<!-- var(translation)="Auto" -->


Administrador Judicial
======================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:58<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Administrador Judicial es un profesional al que se le confía por orden judicial administrar la propiedad de otros o ejercer funciones de asistencia o supervisión en la gestión de estos activos. Como parte de la prevención de las dificultades empresariales, se puede confiar al administrador judicial tareas de mandato*ad hoc* Conciliación.

Como parte del tratamiento judicial de las dificultades a las que se enfrentan las sociedades, el administrador judicial interviene en los procedimientos de salvaguardia, reparación judicial e incluso, de manera excepcional, en la liquidación judicial en los casos continuación de la actividad. En estos tres procedimientos colectivos, su designación es obligatoria con empresas con defectos con al menos 20 empleados y una facturación de 3 millones de euros.

La misión del administrador judicial es elaborar la evaluación económica, social y ambiental de la empresa y elaborar un proyecto de plan para salvaguardar o dar un giro a la empresa.

La misión del administrador es también:

- salvaguardar, vigilar o ayudar al deudor en la gestión de la empresa;
- para ayudar al deudor en su gestión o para asegurar la totalidad o parte de él en el lugar del deudor.

El administrador judicial puede intervenir como administrador interino, en la decisión del tribunal de administrar una corporación y aplicar los medios necesarios para ayudarle a superar su estado de crisis. Esta misión, al igual que otras que el administrador judicial también puede realizar, no es específica de esta profesión.

*Para ir más allá*: Artículo L. 811-1 del Código de Comercio;[sitio web oficial del Consejo Nacional de Directores Judiciales y Agentes](https://www.cnajmj.fr/fr/).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Los administradores judiciales figuran en una lista nacional elaborada por el[Comisión Nacional de Registro y Disciplina para Administradores Judiciales y Agentes Judiciales](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006242221&dateTexte=&categorieLien=cid).

Para ser registrada, la persona debe cumplir las siguientes condiciones:

- Ser francés o nacional de un Estado miembro de la Unión Europea (UE) o parte en el Espacio Económico Europeo (EEE);
- no haber sido condenado penalmente por actos contrarios al honor o a la probidad;
- no han sido objeto de sanciones disciplinarias o administrativas por despido, despido, revocación, revocación de la acreditación o autorización;
- no han sido objeto de quiebra personal ni de ninguna de las medidas de decomiso o prohibición previstas en la legislación sobre procedimientos colectivos.

*Para ir más allá*: Artículos L. 811-2 y L. 811-5 del Código de Comercio.

#### Entrenamiento

Hay varias formas de acceder a la profesión de administrador judicial:

- poseer los diplomas en virtud de los artículos R. 811-7 y R. 811-8 del Código de Comercio y aprobar el Examen de Acceso a Pasantías Profesionales, completar una colocación laboral remunerada de tres a seis años, y luego aprobar el Examen de Aptitud para trabajar Administrador Judicial;
- tener un máster en administración y liquidación de empresas con problemas y justificar:- al menos cinco años de experiencia profesional como empleado de un administrador judicial,
  - o al menos ocho años de práctica profesional contable, jurídica o financiera en el ámbito de la administración, la financiación, la reestructuración, incluidas las fusiones y adquisiciones, o la adquisición de sociedades, en particular en crisis,
  - o haber realizado una pasantía profesional de al menos treinta meses en un estudio del administrador judicial en la lista de la Comisión Nacional de Registro y Disciplina;

**Bueno saber: exenciones de exámenes y pasantías**

Algunas exenciones pueden concederse a la persona que desee convertirse en administrador judicial, dependiendo de sus cualificaciones, experiencia profesional y/o las funciones que pueda haber desempeñado.

Las personas que justifiquen haber adquirido en un Estado miembro de la Unión Europea distinto de Francia u otro Estado parte en el acuerdo sobre el Espacio Económico Europeo, una cualificación suficiente para la profesión de administrador judicial condiciones de diploma, pasantía sesión y examen profesional están exentos, sujeto a un chequeo de conocimientos en algunos casos (formación en temas sustancialmente diferentes o actividad no regulada Estado miembro de origen).

Esta calificación puede ser el resultado de:

- la justificación para completar con éxito un ciclo de educación postsecundaria de un mínimo de tres años o de un período equivalente de educación a tiempo parcial en una universidad o institución de educación superior o en otra institución de un nivel equivalente de formación y, en su caso, la formación profesional necesaria además de este ciclo de estudio;
- justificación de un ejercicio a tiempo completo de la profesión durante al menos dos años en los diez años anteriores en un Estado miembro o parte que no regule el acceso o el ejercicio de la profesión de administrador judicial, siempre que ser certificado por la autoridad competente de ese Estado. Esta condición de experiencia profesional de dos años no es necesaria cuando la participación o los títulos celebrados sancionan la formación regulada directamente orientada hacia el ejercicio de la profesión.

*Para ir más allá*: Artículos L. 811-5 último párrafo y R. 811-27 del Código de Comercio.

Además:

- examen del acceso a las prácticas:- agentes judiciales que han ejercido su profesión durante al menos tres años,
  - abogados, notarios, subastadores judiciales, funcionarios judiciales, secretarios de tribunales comerciales, contadores y auditores, que hayan ejercido su profesión durante al menos cinco años,
  - personas titulares de uno de los títulos o diplomas mencionados en el artículo R. 811-7 del Código de Comercio, que justifique al menos cinco años de práctica profesional contable, jurídica o financiera en el ámbito de la administración, la financiación, la reestructuración, incluidas las fusiones y adquisiciones, o la adquisición de empresas, en particular en crisis,
  - Personas que han servido como colaboradores de un administrador judicial por un período de cinco años;
- prácticas profesionales:- abogados, notarios, subastadores judiciales, funcionarios judiciales, secretarios de tribunales mercantiles, ex abogados, contables, auditores, habiendo ejercido su profesión durante diez años en el Menos
  - empleados de un administrador judicial que ha trabajado durante al menos diez años,
  - aquellos con los diplomas necesarios para el camino a través del examen del acceso a la pasantía profesional ([Artículo R. 811-7 del Código de Comercio](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid)) que justifique una práctica profesional de al menos quince años en los ámbitos contable, jurídico, financiero o administrativo, de financiación, reestructuración o adquisición;
- todas las pruebas del examen de aptitud:- Agentes judiciales que han ejercido su profesión durante al menos cinco años y han completado la pasantía profesional;
- prueba civil y penal de los casos de examen de aptitud:- abogados, notarios, subastadores judiciales, funcionarios judiciales, secretarios de tribunales comerciales,
  - aquellos con los diplomas necesarios para el acceso a la profesión mediante el examen del acceso a la práctica profesional ([Artículo R. 811-7 del Código de Comercio](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid)) que justifiquen al menos cinco años de práctica jurídica profesional en el ámbito del administrador, la financiación de la reestructuración o la adquisición de sociedades;
- La prueba de aptitud para la gestión de la oficina de un administrador judicial:- personas que han servido durante al menos cinco años como empleado de un administrador judicial,
  - contadores y auditores,
  - aquellos con los diplomas necesarios para el acceso a la profesión mediante el examen del acceso a la práctica profesional ([Artículo R. 811-7 del Código de Comercio](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid)) que justifiquen al menos cinco años de práctica profesional contable o financiera, en el ámbito de la administración, la financiación, la reestructuración, incluidas las fusiones y adquisiciones, o la adquisición de sociedades, especialmente en crisis.

Por último, la duración de la pasantía profesional se reduce a un año para:

- abogados, notarios, subastadores judiciales, funcionarios judiciales, secretarios de tribunales comerciales, ex abogados, contables, auditores que han ejercido su profesión durante al menos cinco años;
- personas que poseen uno de los títulos o diplomas mencionados en el[Artículo R. 811-7](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006270565&dateTexte=&categorieLien=cid) código de comercio y justificando al menos diez años de práctica profesional contable, jurídica o financiera, en el ámbito de la administración, la financiación, la reestructuración, incluidas las fusiones y adquisiciones, o la adquisición de sociedades, particularmente en problemas

*Para ir más allá* : Artículos R. 811-7, R. 811-8, R. 811-24, R. 811-25, R. 811-26, R. 811-27, R. 811-28, R. 811-28-1 y R. 811-28-2 del Código de Comercio.

#### Costos relacionados

Los exámenes de entrada y la aptitud son gratuitos.

*Para ir más allá*: Artículo R. 811-16 del Código de Comercio.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Una persona de la UE o del EEE que desee ejercer temporal y ocasionalmente la profesión de administrador judicial debe cumplir las condiciones especificadas anteriormente y llevar a cabo los procedimientos de registro en la lista nacional nacional y la comisión disciplinaria.

*Para ir más allá*: Artículo L. 811-5 del Código de Comercio.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Cualquier ciudadano de la UE o del EEE puede entrar en la profesión a través de los canales tradicionales.

Además, un nacional de un país de la UE o del EEE con un mínimo de tres años de título de posgrado podrá ingresar a la profesión para ejercer permanentemente si:

- posee títulos, certificados u otros títulos adquiridos en un Estado miembro de la UE o del EEE que justifican una cualificación suficiente para la profesión de administrador judicial (ciclo de estudio de al menos tres años o equivalente) (Artículo R. 811-27 1) del Código de Comercio);
- o si ha trabajado a tiempo completo durante dos años en los últimos diez años en un Estado miembro que no regula el acceso a la profesión o a su ejercicio, siempre que este ejercicio esté certificado por la autoridad competente de dicho Estado (artículo R. 811-27 2o código de comercio).

Una vez que cumpla una de estas condiciones, podrá solicitar el registro en la lista de administradores judiciales en la Comisión Nacional para el registro y la disciplina de los administradores judiciales y agentes judiciales (cf. infra "5 grados. a. Solicitar el registro en la lista de administradores judiciales del nacional para un ejercicio permanente (LE)).

Si el examen de las cualificaciones profesionales muestra diferencias sustanciales en relación con las necesarias para el acceso y el ejercicio de la profesión en Francia, el interesado podrá someterse a un control de conocimientos.

*Para ir más allá*: Artículos L. 811-5, R. 811-27 y R. 811-28 del Código de Comercio.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Incompatibilidad del ejercicio

Los administradores judiciales pueden operar individualmente o en cualquier forma de empresa. Sin embargo, la forma jurídica elegida no debe conferir a sus socios la condición de comerciante.

Con la excepción de un abogado, la práctica del administrador judicial es incompatible con:

- todas las actividades comerciales, ya sean realizadas directamente o por la persona interpuesta;
- la condición de socio o gerente de determinadas empresas (artículo L. 811-10 del Código de Comercio)

*Para ir más allá*: Artículos L. 811-7 y L. 811-10 del Código de Comercio.

4°. Educación continua y seguros
-----------------------------------------------------

### a. Obligación de someterse a formación profesional continua

El administrador judicial está obligado a someterse a una formación profesional continua. Esta formación es definida por el Consejo Nacional de Administradores Judiciales durante 20 horas en un año calendario o 40 horas en dos años consecutivos y tiene como objetivo mantener y actualizar los conocimientos del administrador. contable, fiscal y legal.

*Para ir más allá*: Artículos L. 814-9, L. 814-10, R. 814-28-1 y el siguiente Código de Comercio.

### b. Obligación de registrarse en el Fondo de Garantía de Administradores Judiciales

El administrador judicial está obligado a suscribirse a un fondo de garantía gestionado por los propios contribuyentes. Este fondo debe garantizar el reembolso de fondos, efectos o valores recibidos por cada director como parte de sus misiones.

Por otro lado, cuando el director está empleado, sólo es opcional y depende de su empleador suscribir dicha garantía.

*Para ir más allá*: Artículo L. 814-3 del Código de Comercio.

### c. Obligación de constete de un seguro de responsabilidad civil profesional

El administrador judicial debe pagar un seguro de responsabilidad civil profesional que cubra las consecuencias financieras resultantes de su actividad.

Sin embargo, si él es un empleado, le contestar al empleador para tomar dicho seguro.

*Para ir más allá*: Artículo L. 814-4 del Código de Comercio.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar inscripción en la lista de administradores judiciales del nacional para un ejercicio permanente (LE)

**Autoridad competente**

La Comisión Nacional para el Registro y disciplina de Administradores Judiciales y Agentes Judiciales está facultada para decidir sobre la solicitud de registro de la Comisión Nacional de Registro y Disciplina de administradores judiciales y agentes judiciales, 13 Place Vendéme, 75042 PARIS Cedex 01 (FRANCE)).

**Documentos de apoyo**

El nacional debe presentar la solicitud de registro a la secretaría de la Comisión mediante una carta recomendada con notificación de recepción. Esto es en forma de un archivo con los siguientes documentos justificativos:

- Solicitud de registro
- documentos que establezcan el estado civil y la nacionalidad del solicitante;
- Una copia de los títulos y diplomas de los que tiene la intención de hacer uso o, en su defecto, un certificado de las autoridades autorizadas para expedirlos;
- Si es así, el certificado de éxito en la prueba de aptitud;
- En caso afirmativo, las exenciones solicitadas y los documentos justificativos correspondientes;
- Si es así, el certificado de éxito en el examen de comprobación de conocimientos;
- El solicitante también indica sus actividades profesionales anteriores y el lugar donde planea establecer su residencia profesional;
- cualquier documento útil que indique que se cumplen las condiciones del artículo L. 811-5 del Código de Comercio (sin condena/sanción o prohibición).

Los documentos deben ser reenviados a la siguiente dirección postal:

> Ministerio de Justicia<br>
> Comisión Nacional de Registro y Disciplina para Administradores Judiciales y Funcionarios Judiciales<br>
> 13 Colocar Vendéme<br>
> 75042 PARIS Cedex 01 (FRANCIA)

**Es bueno saber**

Se requieren algunos documentos específicos cuando el ejercicio de la profesión está previsto en forma social (véase en particular el[Artículos R. 814-60 y el siguiente del Código de Comercio](https://www.legifrance.gouv.fr/affichCode.do;jsessionid=235FB1B419B02405DFBE8202B5B7EE09.tplgfr24s_2?idSectionTA=LEGISCTA000006191119&cidTexte=LEGITEXT000005634379&dateTexte=20190723).

**Procedimiento**

Una vez que la Comisión Nacional ha recibido el expediente completo, debe recabar la opinión del Consejo Nacional de Administradores Judiciales y Agentes Judiciales (CNAJMJ), que tiene un mes para decidir. Si la Comisión Nacional considera que existen diferencias sustanciales entre la formación o la experiencia profesional del nacional y las exigidas en Francia, puede someterla a una comprobación de conocimientos.

En todos los casos, la Comisión no podrá tomar su decisión sobre si incluye o no al nacional en la lista o si solicita más información hasta que haya recibido la notificación del CNAJMJ, en el plazo de cuatro meses a partir de la recepción del expediente.

*Para ir más allá*: Artículos R. 811-27, R. 811-29 y los siguientes del Código de Comercio.

### b. Remedios

Las apelaciones contra la decisión de la Comisión Nacional de Registro y Disciplina de Directores y Agentes Judiciales podrán ser ejercitadas ante el Tribunal de Apelación de París en el plazo de un mes a partir de la fecha de recepción de la carta de notificación de la decisión. La apelación se realiza mediante declaración ante la Secretaría del Tribunal de Apelación o por carta recomendada con solicitud de notificación de recepción dirigida al Secretario Jefe de dicha Corte.

La representación de un abogado no es obligatoria.

*Para ir más allá* Artículos R. 811-33, R. 811-35, R. 814-2 del Código de Comercio

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresa de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

