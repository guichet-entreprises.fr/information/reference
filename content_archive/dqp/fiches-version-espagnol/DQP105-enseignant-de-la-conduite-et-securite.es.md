﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP105" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Seguridad vial" -->
<!-- var(title)="Profesor de conducción y seguridad vial" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="seguridad-vial" -->
<!-- var(title-short)="profesor-de-conduccion-y-seguridad" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/seguridad-vial/profesor-de-conduccion-y-seguridad-vial.html" -->
<!-- var(last-update)="2020-04-15 17:22:24" -->
<!-- var(url-name)="profesor-de-conduccion-y-seguridad-vial" -->
<!-- var(translation)="Auto" -->


Profesor de conducción y seguridad vial
=======================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:24<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El profesor de conducción y seguridad vial es un profesional cuya misión es impartir formación teórica y práctica a los conductores en diferentes categorías de vehículos (motor, ciclismo, motocicleta o HGV).

Permite a los estudiantes desarrollar habilidades técnicas y de comportamiento con el fin de hacerlos conductores responsables.

Por lo general, las lecciones teóricas se imparten en clases grupales en las que el maestro se centrará en aprender las reglas de conducta y las señales de tráfico.

De conformidad con lo dispuesto en la sección D. 114-12 del Código de Relaciones Públicas y de la Administración, cualquier usuario podrá obtener un certificado informativo sobre las normas aplicables a las profesiones relacionadas con la enseñanza de la conducción costosa y la concienciación sobre la seguridad vial. Para ello, debe enviar la solicitud a la siguiente dirección: bfper-dsr@interieur.gouv.fr.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La práctica de la enseñanza de la conducción y la seguridad vial está reservada a aquellos con una autorización administrativa para enseñar, que cumplan las siguientes condiciones:

- Poseer una licencia de conducir válida con un período de prueba expirado, válido para las categorías de vehículos que desea enseñar a conducir;
- poseer un título o diploma como profesor de conducción y seguridad vial o, dependiendo de las condiciones, estar en formación para su preparación. En este último caso, la autorización se expedirá de forma temporal y restrictiva;
- En caso necesario, el reconocimiento de cualificaciones profesionales para el nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE);
- Tener al menos 20 años de edad
- cumplir con los requisitos de aptitud física, cognitiva y sensorial requeridos para las licencias de conducir en las categorías C1, C, D1, D, C1E, CE, D1E y DE. Para ello, la persona tendrá que obtener el consejo de un oficial médico.

*Para ir más allá*: Artículo L. 212-1, 212-2 y R. 212-2 de la Ley de Tráfico de Carreteras.

#### Entrenamiento

Para ser profesor de conducción y seguridad vial, la persona debe poseer una de las siguientes cualificaciones o diplomas para enseñar la conducción de la Ley de Vehículos B, B1 y BE de la Ley de Tráfico por Carretera:

- El título profesional de profesor de conducción y seguridad vial;
- El certificado para la práctica de la conducción y la seguridad vial (BEPECASER) se impartió antes del 31 de diciembre de 2016;
- Certificado de aptitud profesional para enseñar vehículos de motor terrestres (CAPEC);
- La tarjeta profesional y el Certificado de Aptitud Profesional y Educativa (CAPP);
- títulos o diplomas militares definidos en el orden de 13 de septiembre de 1996 por el que se establece la lista de diplomas militares reconocidos como equivalentes al certificado para la práctica de la enseñanza de la conducción y la seguridad vial;
- la conducción de diplomas de educación emitidos por las comunidades extranjeras y Nueva Caledonia.
- para enseñar la conducción de vehículos en los vehículos de licencia de conducir AM, A1, A2, C1, C, D1, D1E, CE, D1E y DE.

  - el título profesional de profesor de conducción y seguridad vial y los certificados de especialización de este título expedidos por el Ministro responsable del empleo en los artículos R. 338-1 y siguientes del Código de Educación,
  - el certificado para la profesión docente de conducción y seguridad vial (BEPECASER) obtenido antes del 31 de diciembre de 2016 y las menciones de "dos ruedas" y "grupo pesado" del mismo diploma obtenido antes del 31 de diciembre de 2019, en condiciones establecidas por una orden del Ministro de Seguridad Vial,
  - El Certificado de Aptitud Profesional para Enseñar Vehículos Motorizados Terrestres (CAPEC), para personas que han superado con éxito las pruebas o pruebas correspondientes a estas menciones,
  - los títulos o diplomas mencionados en las casillas b, c y 1 a la segunda siempre que los titulares estuvieran en posesión, el 1 de enero de 1982, de las categorías correspondientes de licencia de conducir,
  - una cualificación profesional que cumpla con las condiciones del Artículo R. 212-3-1,
  - un diploma de educación para conducir expedido por un Estado que no es miembro de la Unión Europea ni parte en el Acuerdo sobre el Espacio Económico Europeo y reconocido por la práctica de la enseñanza de la conducción y la seguridad vial por decisión del Ministro responsable de la seguridad vial.

*Para ir más allá* : orden de 20 de abril de 2016 relativo al título profesional de profesor de conducción y seguridad vial; Sección R. 212-3 de la Ley de Tráfico de Carreteras.

#### Costos asociados con la calificación

La formación que conduce a uno de estos títulos y en particular el título profesional vale la pena. Para obtener más información, es aconsejable acercarse a las organizaciones que lo emiten.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Todo nacional de un Estado de la UE o del EEE, legalmente establecido y que actúe la actividad de conductor y profesor de seguridad vial, podrá llevar a cabo la misma actividad de forma temporal e informal en Francia.

Tendrá que solicitarlo, antes de su primera actuación, mediante declaración dirigida al prefecto del departamento en el que desea realizar la entrega (véase infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

Cuando la profesión no esté regulada en el curso de actividad o formación en el país en el que el profesional esté legalmente establecido, deberá haber llevado a cabo esta actividad durante al menos un año, en los últimos diez años anteriores a la uno o más Estados de la UE o del EEE.

*Para ir más allá*: Artículo L. 212-1 II y R.212-3-1 de la Ley de Tráfico de Carreteras.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

El nacional de un Estado de la UE o del EEE, establecido y ejercita legalmente la actividad de conductor y profesor de seguridad vial en ese Estado, podrá llevar a cabo la misma actividad en Francia de forma permanente si:

- posee un certificado expedido por la autoridad competente de un Estado de la UE o del EEE que regula el acceso o el ejercicio de la profesión;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro estado que no regula la formación o el ejercicio de la profesión.

Una vez que el nacional cumple una de estas condiciones, primero debe solicitar el reconocimiento de sus cualificaciones profesionales por parte del prefecto del departamento donde tiene previsto ejercer (véase infra "5o). b. Solicitar el reconocimiento de las cualificaciones profesionales de la UE o del EEE nacionales para la práctica permanente (LE) ").

El reconocimiento de sus cualificaciones profesionales le permitirá solicitar permiso para enseñar con la misma autoridad competente (véase infra "5o). c. Obtener permiso administrativo para enseñar").

Sin embargo, si el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia laboral muestra diferencias sustanciales con las cualificaciones necesarias para el acceso a la profesión y su ejercicio en Francia, el interesado tendrá que someterse a una medida de indemnización.

*Para ir más allá*: Artículo R. 212-3-1 de la Ley de Tráfico de Carreteras.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El profesor de conducción y seguridad vial debe cumplir con las condiciones de honor, incluyendo no ser sentenciado a una sentencia penal o correccional en virtud de la sección R. 212-4 de la Ley de Tráfico por Carretera.

La falta de licencia de enseñanza se castiga con un año de prisión y una multa de 15.000 euros. El profesor de conducción y seguridad vial también puede tener prohibido practicar.

*Para ir más allá*: Artículos L. 212-2, L. 212-4 y R. 212-4 de la Ley de Tráfico de Carreteras.

4°. Seguro
-------------------------------

El profesor liberal de conducción y seguridad vial debe llevar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. Depende del empresario la apropiación de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

**Autoridad competente**

El prefecto del departamento en el que el nacional desea enseñar a conducir es competente para decidir sobre la solicitud de declaración previa de actividad.

**Documentos de apoyo**

La solicitud se realiza mediante la presentación de un archivo que incluye los siguientes documentos:

- Una identificación válida
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE para llevar a cabo esta actividad, y que no incurre en ninguna prohibición temporal o permanente de la práctica;
- prueba de sus cualificaciones profesionales
- cualquier documento que justifique que el nacional haya ejercido en un Estado de la UE o del EEE durante un año en los últimos diez años, cuando dicho Estado no regule la formación, el acceso a la profesión solicitada o su ejercicio.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

Una vez recibidos los documentos justificativos, el prefecto dispone de un mes para informar al nacional de su decisión, que puede ser:

- Autorizar la entrega;
- someterlo a una prueba de aptitud si existen diferencias sustanciales entre la formación o la experiencia profesional del nacional y las requeridas en Francia. En este caso, la decisión sobre la autorización de la prestación tendrá lugar en el plazo de un mes a partir del examen;
- para informarle de cualquier dificultad que pueda retrasar su decisión. En este caso, el prefecto podrá tomar su decisión en el plazo de un mes a partir de la resolución de esta dificultad, y a más tardar dos meses después de la notificación al nacional.

El silencio del prefecto dentro de estos plazos vale el permiso para realizar el servicio.

*Para ir más allá*: Artículo R. 212-2 de la Ley de Tráfico de Carreteras.

### b. Solicitar el reconocimiento de las cualificaciones profesionales de los nacionales de la UE o del EEE para el ejercicio permanente (LE)

**Autoridad competente**

El prefecto del departamento en el que el nacional desea enseñar es competente para decidir sobre la solicitud de reconocimiento de cualificaciones profesionales.

**Documentos de apoyo**

La solicitud se realiza mediante la presentación de un archivo que incluye los siguientes documentos justificativos:

- La solicitud de reconocimiento que debe solicitarse a los servicios del prefecto del departamento, fechada y firmada;
- Una fotocopia de un documento de identidad
- Una fotografía de identidad de menos de seis meses
- prueba de residencia de menos de seis meses
- Fotocopia de la licencia de conducir de dos lados
- Fotocopia de certificados de competencia o documentos de formación obtenidos para el ejercicio de la actividad docente de conducción;
- Una declaración sobre su conocimiento de la lengua francesa para el ejercicio de la profesión de que se trate;
- en su caso, un certificado de competencia o un documento de formación que justifique que el nacional ha estado involucrado en la actividad durante al menos un año en los últimos diez años, en un estado que no regula la formación o su ejercicio.

**Procedimiento**

El prefecto reconoce la recepción del expediente en el plazo de un mes y dispone de dos meses a partir de esa misma fecha para decidir sobre la solicitud.

En caso de diferencias sustanciales entre la formación que ha recibido y la requerida en Francia, el prefecto podrá someterlo a una prueba de aptitud o a un curso de adaptación.

La prueba de aptitud, realizada en un plazo máximo de seis meses, es en forma de entrevista ante un jurado. El curso de aptitud se lleva a cabo durante un mínimo de dos meses en una escuela de conducción acreditada.

Después de comprobar los documentos en el expediente y, si es necesario, después de recibir los resultados de la medida de compensación elegida, el prefecto emitirá el reconocimiento de cualificaciones cuyo modelo se especifique en el[Anexo](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=D3225762F0A0C9E9EA272A874A63EF2F.tplgfr42s_2?cidTexte=JORFTEXT000035589366&dateTexte=20170920) 13 de septiembre de 2017.

*Para ir más allá* : decreto de 13 de septiembre de 2017 relativo al reconocimiento de cualificaciones adquiridas en un Estado miembro de la Unión Europea o en otro Estado parte en el acuerdo sobre el Espacio Económico Europeo por personas que deseen ejercer las profesiones educación vial regulada.

### c. Obtener permiso administrativo para enseñar

#### Permiso para enseñar

**Autoridad competente**

El prefecto del departamento en el que el nacional reside o desea enseñar (para los solicitantes domiciliados en el extranjero) es competente para expedir la autorización de enseñanza.

**Documentos de apoyo**

El nacional deberá adjuntar a la solicitud los siguientes documentos justificativos:

- La solicitud de autorización docente cumplida para ser solicitada a los servicios del prefecto del departamento, fechada y firmada;
- Dos fotos de identidad idénticas y recientes;
- Una fotocopia de un documento de identidad
- la justificación es de buena fiar con respecto a la legislación y los reglamentos relativos a los extranjeros en Francia;
- Comprobante de residencia
- Fotocopia de certificados de competencia o documentos de formación obtenidos para el ejercicio de la actividad docente de conducción;
- Reconocimiento de cualificaciones profesionales;
- un certificado médico emitido por un médico con licencia.

La autorización se expide por un período de cinco años, renovable. Dos meses antes de su expiración, el nacional debe solicitar la renovación presentando los mismos documentos justificativos al prefecto del departamento.

*Para ir más allá*: Artículos R. 212-1 y siguientes de la Ley de Tráfico de Carreteras; orden de 8 de enero de 2001 relativa a la autorización para enseñar, por una tasa, la conducción de vehículos de motor y la seguridad vial.

#### Autorización temporal y restrictiva para enseñar

Se concede una autorización temporal y restrictiva a la persona que se dice que está en proceso de formación en la profesión de conducción.

El prefecto del departamento donde se encuentra la escuela de conducción con la que el solicitante tiene previsto ejercer, es competente para expedir la autorización.

Además de cumplir las condiciones establecidas en el "2o. a. Requisitos nacionales" también tendrá que:

- Haber firmado un contrato de trabajo con una institución de educación para conducir con licencia;
- Poseer los certificados de competencia profesional que conforman el título profesional de profesor de conducción expedidos por el Ministro responsable del empleo;
- estar inscritos en una sesión de examen para completar la validación de las habilidades necesarias para obtener el título de profesor de conducción y seguridad vial.

**Qué saber**

Esta autorización sólo se concede por doce meses.

*Para ir más allá* : Orden de 13 de abril de 2016 relativa a la autorización temporal y restrictiva a la práctica contemplada en el artículo R. 212-1 de la Ley de Tráfico por Carretera; Artículo R. 212-2 I bis de la Ley de Tráfico de Carreteras.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Información adicional**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

