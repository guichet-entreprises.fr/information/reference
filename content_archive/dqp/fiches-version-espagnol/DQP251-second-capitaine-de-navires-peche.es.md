﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP251" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sector marítimo" -->
<!-- var(title)="Segundo capitán de buques pesqueros" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sector-maritimo" -->
<!-- var(title-short)="segundo-capitan-de-buques-pesqueros" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sector-maritimo/segundo-capitan-de-buques-pesqueros.html" -->
<!-- var(last-update)="2020-04-15 17:22:19" -->
<!-- var(url-name)="segundo-capitan-de-buques-pesqueros" -->
<!-- var(translation)="Auto" -->


Segundo capitán de buques pesqueros
===================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:19<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El segundo maestro de buques pesqueros es un oficial encargado de asistir y asistir al maestro para el reloj que se le ha asignado. Es responsable del servicio de puente, que incluye oficiales, el maestro de la tripulación y los marineros, así como el servicio general.

Entre estas muchas misiones, el segundo capitán es responsable de:

- supervisar u operar el buque durante el enfoque piloto, la salida o la asistencia marítima;
- Organizar el trabajo en el servicio de puente;
- Planificar operaciones de mantenimiento
- comprobar el equipo de seguridad.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de segundo máster de buques pesqueros está reservada al titular del segundo certificado de máster, cumpliendo todas las condiciones siguientes:

- Tener al menos 20 años de edad el día en que se presente la solicitud de patente
- Poseer un certificado de aptitud médica
- Han adquirido un nivel suficiente de conocimientos de idiomas y conocimientos jurídicos;
- Tener las cualificaciones profesionales necesarias
- han completado al menos 12 meses de servicio en el mar como oficial de vigilancia de puentes.

**Tenga en cuenta que**

El segundo certificado de capitán tiene una validez de cinco años y debe renovarse bajo las reglas de la[decreto de 24 de julio de 2013](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027788297&categorieLien=id) revalidación de los títulos de formación profesional marítima.

*Para ir más allá*: Artículo L. 5521-3 del Código de Transporte.

#### Entrenamiento

Para cumplir con el requisito de cualificaciones profesionales, el candidato para el segundo certificado de capitán debe ser el titular:

- una de las siguientes patentes válidas:- certificado como oficial de vigilancia de puentes,
  - certificado como oficial a cargo de la vigilancia de buques marítimos,
  - patente enumerada en el cuadro 3 de la Orden de 18 de abril de 2016;
- uno de los siguientes grados válidos:- Diploma del capitán,
  - graduado de la marina mercante,
  - diploma o certificado mencionado en el cuadro 4 del auto de 18 de abril de 2016;
- Certificado Básico de Capacitación en Seguridad (CFBS);
- Certificado de Calificación avanzada de lucha contra incendios (CQALI);
- Certificado de aptitud para operar botes salvavidas y balsas salvavidas (CAEERS);
- Certificado que certifica la validación de la educación médica de Nivel III (EM III);
- Certificado de Operador General (CGO).

*Para ir más allá*: Artículos 10 y siguientes de la[decreto del 18 de abril de 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032459503&dateTexte=&categorieLien=id) patente de segundo capitán y el certificado de capitán.

#### Acondicionamiento físico

El acceso a la profesión de segundo máster en buques pesqueros está sujeto a requisitos mínimos de aptitud que se evalúan durante las visitas médicas requeridas:

- antes del acceso a la profesión de segundo capitán;
- Antes del primer embarque;
- antes de entrar en la formación marítima;
- antes de la expiración del certificado de aptitud médica, la duración de la cual varía según la edad del oficial.

Al final del examen médico, el médico puede decidir que el segundo capitán está en forma y se le dará un certificado de aptitud.

Por otra parte, cuando la decisión del médico indique una idoneidad parcial, incapacidad temporal o incapacidad total, podrá impugnarla ante la Comisión Médica Regional para la Aptitud para la Navegación (CMRA).

*Para ir más allá* :[decreto del 3 de diciembre de 2015](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031560450) salud y aptitud médica para navegar.

#### Control del lenguaje y el conocimiento jurídico

Para ejercer como segundo capitán, la persona debe tener conocimientos de asuntos franceses y jurídicos. Para justificarlos, tendrá que proporcionar:

- un diploma de educación secundaria o superior francés o un certificado de menos de un año que acredite un máster B2. Para obtener más información, consulte el[Marco Común Europeo para las Lenguas](http://eduscol.education.fr/cid45678/cadre-europeen-commun-de-reference-cecrl.html) ;
- cualquier diploma de educación superior francesa que sancione la formación o la enseñanza específicas relacionadas con las facultades y prerrogativas del poder público conferidas al capitán de un buque de bandera francesa.

Si el segundo capitán no tiene ninguno de estos documentos, tendrá que someterse a una prueba escrita en francés y una entrevista ante una junta nacional de evaluación. La prueba escrita y la entrevista probarán si la persona tiene los conocimientos legales necesarios para el puesto, y para evaluar su capacidad para comunicarse en un contexto profesional, así como para escribir en el idioma francés.

*Para ir más allá*: Artículos 3 y siguientes de la[decreto de 2 de junio de 2015](https://www.legifrance.gouv.fr/eli/decret/2015/6/2/DEVT1422283D/jo/texte)

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (Servicio Gratuito)

Un nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), que actúa legalmente como segundo capitán de buques pesqueros en uno de estos Estados, puede hacer uso de su título profesional en Francia con carácter temporal. o casual.

Tendrá que solicitarlo, antes de su primera actuación, mediante declaración dirigida al director interregional del mar competente de la región administrativa en la que se identifica (véase infra "4o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá justificar haberla realizado en uno o varios Estados miembros durante al menos un año, en los diez años antes de la actuación.

A saber: el ejercicio del segundo capitán en Francia, de forma temporal o casual, exige que el nacional posea todas las habilidades linguísticas necesarias.

Cuando existan diferencias sustanciales entre la formación del nacional y las exigidas en Francia, o cuando el interesado no haya adquirido todas las competencias necesarias para ejercer en un buque pesquero de bandera francesa, compensación (ver infra "4o. a. Bueno saber: medida de compensación").

*Para ir más allá*: Artículos 20 del año[decreto de 24 de junio de 2015](https://www.legifrance.gouv.fr/eli/decret/2015/6/24/DEVT1502017D/jo/texte) y 8 de los[decretado a partir del 8 de febrero de 2010](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021844162&dateTexte=20171207).

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si:

- posee un título expedido por una autoridad competente de otro Estado miembro, que regula el acceso a la profesión o a su ejercicio;
- El título presentado tiene su equivalente exacto en Francia;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Una vez que cumpla una de las tres condiciones anteriores, tendrá que solicitar un certificado de reconocimiento del Director Interregional competente del Mar (véase infra "4o. b. Obtener un certificado de reconocimiento para los nacionales de la UE o del EEE para un ejercicio permanente (LE)).

Si, al revisar el expediente, el Director Interregional del Mar constata que existen diferencias sustanciales entre la formación profesional y la experiencia del nacional y las necesarias para operar en un buque con pabellón medidas de compensación (ver infra "4o. b. Bueno saber: medida de compensación"

*Para ir más allá*: Artículos 4 a 5 de la orden de 8 de febrero de 2010.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El interesado debe respetar las condiciones morales y no haber sido condenado por una sentencia correccional o penal.

Para ello, deberá presentar un extracto de su historial delictivo de menos de tres meses o un certificado de menos de tres meses, del Estado de la UE o del EEE, que certifique estas condiciones.

*Para ir más allá*: Artículo L. 5521-4 del Código de Transporte.

4°. Procedimientos y formalidades de reconocimiento de cualificación
-----------------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

**Autoridad competente**

El Director Interregional del Mar en la región administrativa en la que el segundo maestro desea llevar a cabo la realización o en la que se encuentra el puerto de armamento del buque pesquero, es competente para decidir sobre la declaración. Confirmará la recepción de la solicitud en el plazo de un mes a partir de la recepción del expediente.

**Renovación de la predeclaración**

La declaración debe renovarse una vez al año y en caso de que se produzca un cambio en la situación del nacional.

**Documentos de apoyo**

Para ejercer como segundo capitán en un buque pesquero, el nacional envía un expediente a la autoridad competente que contiene los siguientes documentos justificativos:

- Una declaración escrita firmada por el nacional;
- Una pieza de identificación válida para el nacional;
- un certificado del Estado de la eu o de la autoridad competente del EEE que certifique que el nacional está legalmente establecido en ese Estado y no incurre en ninguna prohibición de ejercer;
- Un certificado que justifique las cualificaciones profesionales del nacional;
- un certificado que justifique su actividad durante al menos dos años en los últimos diez años, cuando ni la formación ni el ejercicio de la profesión están regulados en el Estado miembro;
- Un certificado que justifica que se cumplen las condiciones morales;
- Un certificado de aptitud física para navegar
- un certificado de dominio de las habilidades del lenguaje.

**hora**

El servicio puede comenzar siempre y cuando no haya oposición de la Dirección Interregional del Mar:

- La expiración de un plazo de un mes a partir de la solicitud de declaración;
- en caso de una solicitud de información adicional o verificación de cualificaciones profesionales, al final de un período de dos meses a partir de la recepción de la solicitud completa.

**Tenga en cuenta que**

En caso de solicitud de acceso parcial a la profesión, el nacional deberá adoptar las mismas medidas que para el ejercicio de la actividad de forma temporal o ocasional en territorio francés.

**Bueno saber: medida de compensación**

Para obtener permiso para ejercer, el interesado puede estar obligado a someterse a una prueba de aptitud si parece que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de las ejercer la profesión en Francia.

La prueba de aptitud debe establecer que se dominan los conocimientos y cualificaciones pertinentes.

**Costo**

Gratis.

*Para ir más allá*: Artículos 7-2 a 10 de la orden del 8 de febrero de 2010.

### b. Obtener un certificado de reconocimiento para los nacionales de la UE o del EEE para un ejercicio permanente (LE)

**Autoridad competente**

El Director Interregional del Mar, situado en la región administrativa del puerto de armamento del buque pesquero, es competente para expedir el certificado de reconocimiento que autoriza el ejercicio permanente del segundo capitán en Francia.

**Procedimiento**

La solicitud de certificado de reconocimiento se dirige por cualquier medio a la autoridad competente de la región administrativa en la que desea resolver. En caso de falta de documento, la autoridad competente dispone de un mes a partir de la recepción del expediente para informar al nacional.

**Documentos de apoyo**

Para ejercer como segundo capitán en Francia de forma permanente, el interesado debe presentar un expediente completo que contenga:

- El formulario[Cerfa No. 14750](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14750.do) Debidamente cumplimentado y firmado;
- Una identificación válida
- un certificado de experiencia profesional expedido por la autoridad competente del Estado miembro, cuando la profesión no esté regulada en dicho Estado;
- cuando así lo solicite la autoridad competente, si fuera necesario, el programa de formación que conduzca a la expedición del título;
- Un certificado que justifica que se cumplen las condiciones morales;
- Un certificado de aptitud física para navegar
- un certificado de dominio de las habilidades del lenguaje.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Resultado del procedimiento**

La autoridad competente dispone de un mes para decidir sobre la solicitud de certificación, tan pronto como se reciba el expediente completo.

Cualquier decisión, ya sea aceptación, denegación o indemnización de res, debe estar justificada.

El silencio guardado al final de un período de dos meses valdrá la pena la decisión de rechazar la solicitud de reconocimiento.

Si se acepta la decisión, la autoridad competente emite el certificado de reconocimiento, que tiene un plazo de validación de cinco años.

**Bueno saber: medidas de compensación**

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a la medida de compensación de su elección, ya sea un curso de ajuste o una prueba de aptitud, realizada en el plazo de seis meses a partir de la decisión de autoridad competente.

**Costo**

Gratis.

*Para ir más allá*: Artículos 2 a 5-2 de la orden del 8 de febrero de 2010.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

