﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP157" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios agrícolas" -->
<!-- var(title)="Inspector de equipos de pulverización fitofarmacéuticos" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-agricolas" -->
<!-- var(title-short)="inspector-de-equipos-de-pulverizacion" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-agricolas/inspector-de-equipos-de-pulverizacion-fitofarmaceuticos.html" -->
<!-- var(last-update)="2020-04-15 17:20:49" -->
<!-- var(url-name)="inspector-de-equipos-de-pulverizacion-fitofarmaceuticos" -->
<!-- var(translation)="Auto" -->


Inspector de equipos de pulverización fitofarmacéuticos
=======================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:49<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El inspector de pulverización fitofarmacéutica es un profesional, cuya actividad consiste en comprobar, a intervalos regulares, el equipo necesario para la aplicación de fitofarmacéuticos (pesticidas).

Esta inspección es llevada a cabo por un organismo de inspección, a petición y a expensas del propietario del equipo.

*Para ir más allá*: Artículo L. 256-2 del Código Rural y Pesca Marina.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de inspector de equipos fitofarmacéuticos de pulverización, el profesional debe poseer un certificado para llevar a cabo los controles de una o más categorías de pulverizadores sancionadores aprobación de un examen después de la formación en un centro de formación acreditado.

*Para ir más allá*: Artículos D. 256-16 y D. 256-23 del Código de Pesca Rural y Marina

#### Entrenamiento

**Obtener un certificado de práctica**

Para ser reconocido como profesionalmente cualificado y obtener un certificado, el profesional debe recibir formación de uno de los centros de formación, aprobados por el Ministerio de Agricultura.

Esta formación consiste en:

- un primer módulo, con una duración mínima de 23 horas, que cubre:- todas las generalidades relacionadas con pulverizadores como mantenimiento, ajustes, etc. (duración mínima de 3 horas),
  - introducción al protocolo de control teórico y práctico (duración mínima de 14 horas),
  - la salud y seguridad del inspector (duración mínima de 2 horas),
  - La relación entre el inspector y su cliente (duración de 2 horas);
- un segundo módulo, con una duración mínima de 19 horas, que cubra:- y el marco para el ejercicio de la profesión de inspector (duración mínima de 4 horas),
  - dominar el protocolo de control del equipo y ponerlo en práctica (duración mínima de 8 horas),
  - equipo de control (duración mínima de 4 horas).

Para obtener el certificado, el profesional debe pasar dos pruebas:

- al final del primer módulo, que dura dos horas en forma de un cuestionario de opción múltiple de 30 preguntas (MQC). Con el fin de continuar el curso de formación y obtener el certificado, el umbral de éxito se establece en 20 respuestas correctas de 30. En caso de fallo, se debe ofrecer al candidato lo antes posible un nuevo cuestionario de opción múltiple;
- Al final del segundo módulo, la prueba consiste en:- una prueba práctica en la que debe llevar a cabo una comprobación de hardware y elaborar un informe de control,
  - una prueba oral sobre el marco del ejercicio de la profesión, la salud y la seguridad del inspector y el buen funcionamiento del equipo de control, así como la realización de un control sobre una categoría de equipos distintos de aquel para el que llevó a cabo el control durante la prueba práctica.

Una vez que el candidato ha alcanzado 2/3 de los objetivos, obtiene un certificado de cualificación de acuerdo con el modelo establecido en la Lista II de la Orden del 18 de diciembre de 2008 sobre los centros de formación de inspectores de pulverización en virtud del artículo D. 256-24 del Código Rural y de la Pesca Marina.

Este certificado tiene una validez de cinco años renovables (véase infra "Bueno saber: renovación del certificado").

**Posible exención de formación**

Algunos profesionales pueden estar exentos de la formación del primer módulo, siempre y cuando tengan al menos tres meses de experiencia profesional que implique el ajuste adecuado de los pulverizadores y Lista III del auto de 18 de diciembre de 2008.

Una vez que cumplen con estos requisitos, tienen la oportunidad de pasar la prueba de entrenamiento del primer módulo, y si tienen éxito, el acceso al segundo módulo de capacitación.

Además, el primer módulo (prueba de formación y acceso al segundo módulo) está completamente exento, los nacionales de un Estado miembro de la Unión Europea, un Estado Parte en el Espacio Económico Europeo, mencionados en los artículos D. 256-27 y D. 256-28 de la El Código de Pesca Rural y Marina que proporcionó al Grupo de Interés Público Pulvés los documentos previstos en las mismas secciones, y cuyos conocimientos y habilidades se consideraron insuficientes para ejercer la profesión de inspector de aerosoles.

**Bueno saber: renovación de certificados**

Al renovar el certificado, el profesional está obligado a seguir un módulo de renovación que tiene como objetivo actualizar los conocimientos y prácticas profesionales movilizados durante la comprobación periódica obligatoria de pulverizadores. Este módulo incluye un curso de formación específico que dura al menos 21 horas seguido de una prueba de 2 horas. Esta prueba consta de treinta preguntas de opción múltiple. Para obtener la renovación del certificado, el umbral de éxito se establece en 20 respuestas correctas de 30.

En caso de fallo, se debe ofrecer al candidato lo antes posible un nuevo cuestionario de opción múltiple.

El modelo de certificado de renovación se establece en la Lista II de la Orden de 18 de diciembre de 2008.

*Para ir más allá* :[web oficial del Grupo de Interés Público Pulvés](http://www.gippulves.fr) ; Orden de 18 de diciembre de 2008 relativa a los centros de formación de inspectores de pulverización en virtud del artículo D. 256-24 del Código Rural y de la Pesca Marina.

**Acreditación del organismo de inspección**

El inspector titular de certificados debe ejercer en un organismo de inspección autorizado por el prefecto de la región en la que se encuentre su sede.

El organismo de inspección se considerará acreditado siempre que esté acreditado por el Comité de Acreditación de Francia ([Cofrac](https://www.cofrac.fr/)), o por cualquier otro organismo de acreditación signatario del acuerdo multilateral de reconocimiento establecido por la coordinación del organismo europeo de acreditación, de conformidad con las Listas A, B o C de la norma NF EN ISO/CIS 17020 para actividades de "control periódico del pulverizador".

Para obtener la aprobación, el organismo de inspección debe justificar:

- operar independientemente de otras actividades de control, fabricación o distribución de fitofarmacéuticos;
- que todos los inspectores que emplea poseen el certificado de práctica;
- Que todo el equipo de control que posee cumple con los requisitos de cumplimiento y calibración;
- tienen un sistema para archivar documentos y que se mantengan actualizados y accesibles.

**Tenga en cuenta que**

A partir del 1 de enero de 2018, cualquier organismo de inspección que realice al menos trescientas cincuenta inspecciones al año debe estar acreditado para su certificación o renovación.

Una vez que cumpla con estas condiciones, la organización debe presentar una solicitud al Pulvés GIP (véase más adelante. "5. a. Solicitud de aprobación para un organismo de inspección").

*Para ir más allá*: Artículos D. 256-15 a D. 256-20-1 del Código de Pesca Rural y Marina y orden de 18 de diciembre de 2008 relativo a los organismos de inspección por aspersión en las secciones D. 256-20 y D. 256-26 del Código Rural y Pesca Marina.

#### Costos asociados con la calificación

Se paga la formación que conduce a la calificación de inspector de equipos de pulverización fitofarmacéutico y el costo varía en función del centro de formación en cuestión. Es aconsejable acercarse a estos establecimientos para obtener más información.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Un nacional de un Estado miembro de la UE o del EEE, que trabaje como inspector de equipos de pulverización fitofarmacéuticos en uno de estos Estados, podrá utilizar su título profesional en Francia de forma temporal o ocasional. Debe hacer una declaración al prefecto de la región en la que desea practicar (véase más adelante "5o). a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año, durante los diez años que preceder el rendimiento.

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones requeridas para el acceso a la profesión y su ejercicio en Francia, el interesado podrá ser sometido a una prueba de aptitud en un un mes después de la recepción de la declaración por parte del prefecto.

*Para ir más allá*: Artículos L. 2041 y R. 2041 del Código Rural y Pesca Marina.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Todo nacional de un Estado de la UE o del EEE, legalmente establecido y que actúe como inspector de equipos de pulverización fitofarmacéuticos, podrá llevar a cabo la misma actividad de forma permanente en Francia.

Para ello, el nacional debe justificar la celebración de un certificado de formación o un certificado de competencia expedido por un Estado miembro que regule el acceso y el ejercicio de la profesión.

Cuando el Estado no regule el ejercicio de la profesión, el nacional deberá justificar haber ejercido como inspector de equipos fitofarmacéuticos de pulverización durante un año, en los últimos diez años.

Cuando la formación que conduzca al ejercicio de esta profesión, de la que tiene el nacional, esté regulada en ese Estado miembro, no se requiere experiencia laboral.

Una vez que el nacional cumple estas condiciones, se considera que posee el certificado de ejercicio. Este último se emite durante cinco años. Se renueva tras una formación específica en un centro de formación acreditado.

Cuando existan diferencias sustanciales entre la formación recibida por el nacional, que posee un certificado, y la necesaria para ejercer en Francia, este último será sometido, a su elección, a una prueba de aptitud o a un curso de adaptación.

*Para ir más allá*: Artículos R. 204-2, R. 204-3 y D. 256-27 del Código de Pesca Rural y Marina.

3°. Protocolo y modalidades de control
-----------------------------------------------

El inspector está obligado a cumplir con los procedimientos del protocolo de control de equipos de pulverización para fitofarmacéuticos.

Como tal, debe:

- Asegurarse de que el pulverizador tiene un identificador (una placa o pegatina) de manera legible e indeleble y de acuerdo con las características establecidas en la Lista I del orden del 18 de diciembre de 2008 sobre el control de los pulverizadores 1o del artículo D. 256-14 del Código Rural y de la Pesca Marítima;
- para comprobar e identificar los defectos de todos los puntos de inspección enumerados en la Lista II de la orden de 18 de diciembre de 2008 mencionada anteriormente;
- hacer una contra-visita tan pronto como se encuentra un defecto y requiere reparación;
- preparar un informe de inspección, cuyo modelo se adjunta a la Lista IV de la orden publicada anteriormente.

Si no se encuentran defectos, durante el primer control o al final de la contravisita, el inspector debe colocar una pegatina en el pulverizador, de manera visible, cuyo modelo se adjunta a la[Apéndice III](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=B52089429CC279EA134E0A00ABA104FF.tplgfr34s_1?idArticle=LEGIARTI000032750781&cidTexte=LEGITEXT000032750734&dateTexte=20180418) del decreto antes mencionado.

*Para ir más allá* : orden de 18 de diciembre de 2008 relativa a los procedimientos de control de los pulverizadores adoptados en virtud del artículo D. 256-14 del Código Rural y de la Pesca Marítima.

4°. Sanciones penales
------------------------------------------

Un organismo de inspección que realice controles sin licencia o que tenga dichos controles realizados por inspectores que no dispongan de certificado de práctica está sujeto a una multa de 1.500 euros.

*Para ir más allá*: Artículo R. 256-31 del Código Rural y Pesca Marina.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de aprobación para un organismo de inspección

**Autoridad competente**

El organismo de inspección debe presentar una solicitud al Pulvés GIP.

**Documentos de apoyo**

Para ello, debe proporcionar un archivo que contenga:

- un documento que justifique que posee la acreditación del Cofrac u otro organismo de acreditación que sea signatario del acuerdo multilateral de reconocimiento. En caso contrario, deberá adjuntar el Reglamento previsto en el artículo L. 256-2-1 del Código Rural y de la Pesca Marítima correspondiente a la primera visita del GIP;
- La organización de sus diversas actividades;
- Información sobre los inspectores que emplea
- todos los procedimientos y equipos de control que tiene y sus certificados de calibración;
- Todos los procedimientos internos para llevar a cabo controles periódicos de los dispositivos de medición;
- La descripción de todas sus instalaciones;
- diagnósticos voluntarios realizados antes del 1 de enero de 2009.

Además, la organización debe comprometerse con el IIP para:

- transmitirles los resultados de las comprobaciones de pulverizadores.
- Implementar los medios técnicos para facilitar su misión y la de los oficiales de control de cumplimiento de la organización;
- utilizando únicamente inspectores titulares de certificados;
- pagar el gravamen en virtud de la Sección L. 256-2-1 del Código Rural y de la Pesca Marina;
- comunicar la decisión del organismo de acreditación después de cada una de sus auditorías.

**Retrasos y procedimientos**

El IPI debe asegurarse de que la organización está cumpliendo con las obligaciones para las que ha comprometido. Cuando la organización no está acreditada, el IPI debe realizar visitas de inspección.

La acreditación se otorga por un período de cinco años.

**Tenga en cuenta que**

Cuando la organización justifique que los inspectores que emplea hayan completado al menos cincuenta controles o hayan completado un curso de formación de al menos una semana en un organismo de inspección autorizado, el recibo de acreditación se adeuda a la aprobación provisional. Esta aprobación provisional es válida hasta que se procese la solicitud y a más tardar seis meses después de la emisión del recibo.

Es aconsejable ponerse en contacto con el[GIP Pulvés](http://www.gippulves.fr/) para obtener más información.

*Para ir más allá*: Artículos D. 256-17 a D. 256-20-1; Orden del 18 de diciembre de 2008 sobre las agencias de inspección de pulverizadores en virtud de las secciones D. 256-20 y D. 256-26 del Código Rural y Pesca Marina.

### b. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea.

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

