﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP037" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Seguridad vial" -->
<!-- var(title)="Facilitador de cursos de sensibilización sobre seguridad vial" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="seguridad-vial" -->
<!-- var(title-short)="facilitador-de-cursos-de-sensibilizacion" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/seguridad-vial/facilitador-de-cursos-de-sensibilizacion-sobre-seguridad-vial.html" -->
<!-- var(last-update)="2020-04-15 17:22:22" -->
<!-- var(url-name)="facilitador-de-cursos-de-sensibilizacion-sobre-seguridad-vial" -->
<!-- var(translation)="Auto" -->


Facilitador de cursos de sensibilización sobre seguridad vial
=============================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:22<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El facilitador de los cursos de sensibilización en seguridad vial es un profesional cuya actividad incluye proporcionar a los conductores que han perdido puntos en su licencia de conducir puntos en puntos y puntos de conciencia seguridad vial.

Estos cursos están compuestos por módulos destinados a cambiar las actitudes de estos conductores para que no repitan su comportamiento peligroso.

Estos cursos son facilitados por un equipo formado por un profesor de conducción y seguridad vial y un psicólogo, que poseen el certificado de aptitud para capacitar a instructores de conducción de vehículos terrestres en (BAFM) y la autorización de animación.

De conformidad con lo dispuesto en la sección D. 114-12 del Código de Relaciones Públicas y de la Administración, cualquier usuario podrá obtener un certificado informativo sobre las normas aplicables a las profesiones relacionadas con la enseñanza de la conducción costosa y la concienciación sobre la seguridad vial. Para ello, debe enviar la solicitud a la siguiente dirección: bfper-dsr@interieur.gouv.fr.

*Para ir más allá*: Artículos L. 212-1 a L. 212-5 y R. 223-5 a R. 223-13 de la Ley de Tráfico de Carreteras.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de facilitador de cursos de sensibilización en seguridad vial, el profesional debe tener al menos 25 años de edad y celebrar:

- licencia de conducir válida y cuyo período de prueba ha expirado;
- un título de profesor de conducción y seguridad vial para expertos en seguridad vial;
- una autorización de ejercicio (véase a continuación "Autorización para el Ejercicio");
- para los pyschólogos, una prueba de su inscripción en el Registro Nacional de Psicólogos.

*Para ir más allá*: Artículo L. 212-1 de la Ley de Tráfico de Carreteras.

#### Entrenamiento

Para ser reconocido como experto en seguridad vial, el profesional debe cumplir con todos los requisitos para llevar a cabo las funciones docentes de conducción y seguridad vial. Puede obtener más información sobre el listado[Enseñanza de la conducción y la seguridad vial](https://www.guichet-qualifications.fr/fr/professions-reglementees/enseignant-de-la-conduite-et-de-la-securite-routiere/).

*Para ir más allá*: Artículo R. 212-2 de la Ley de Tráfico de Carreteras; de 26 de junio de 2012 sobre la autorización para dirigir cursos de sensibilización en seguridad vial.

#### Formación inicial y continua obligatoria

**Formación inicial**

Antes de practicar, el profesional debe completar la formación inicial. Para poder acceder, debe tener uno de los siguientes títulos adicionales además de su cualificación profesional (véase arriba "Formación"):

- para el maestro, un certificado de aptitud para capacitar a los instructores que enseñan vehículos de motor terrestres (BAFM) expedido por el Ministerio de Seguridad Vial;
- para el profesor, un certificado de facilitador para la formación de conductores responsables de delitos (BAFCRI) expedido por el Ministerio de Seguridad Vial;
- para el facilitador psicólogo, diploma que le permite hacer uso profesional del título de psicólogo (la lista de los cuales se fija en el artículo 1 del Decreto No 90-255, de 22 de marzo de 1990, por el que se establece la lista de diplomas para hacer uso del título de psicólogo).

Esta formación inicial, que tiene como objetivo permitirle dirigir cursos de sensibilización en seguridad vial, es impartida por el Instituto Nacional de Seguridad Vial e Investigación (INSERR).

Consta de cinco semanas de formación teórica y formación práctica de forma alterna para observar y animar secuencias de prácticas.

El contenido de esta formación se fija en el Anexo 5 del orden de 26 de junio de 2012, como se ha señalado.

**Formación continua**

El profesional debe realizar una formación continua durante su actividad para poder actualizar sus conocimientos. Esta capacitación de dos a cinco días también es proporcionada por inSERR. Debe llevarse a cabo cada cinco años antes de la renovación de la autorización de animación.

Los términos y contenidos de esta formación se establecen en el Anexo 6 del auto de 26 de junio de 2012, como se ha señalado.

*Para ir más allá*: Artículo 4 del auto de 26 de junio de 2012 supra.

#### Autorización para ejercer

Para obtener esta autorización el profesional debe:

- Tener al menos 25 años de edad
- Disponer de un certificado de formación inicial en el desarrollo de cursos de sensibilización en seguridad vial;
- ser el titular:- autorización docente válida (las condiciones establecidas en el artículo I. 212-2 del Código de Carreteras) y un diploma adicional en el ámbito de la formación en materia de seguridad vial,
  - un diploma para utilizar el título de psicólogo y la licencia de conducir válida y cuyo período de prueba ha expirado;
  - no haber sido condenado a una sentencia penal o correccional en virtud de la sección R. 212-4 de la Ley de Tráfico de Carreteras.

Una vez que cumpla con estas condiciones, debe solicitar una autorización de ejercicio (ver infra "5o. b. Solicitud de permiso para ejercer").

**Tenga en cuenta que**

La autorización de ejercicio podrá ser retirada del profesional, siempre que ya no cumpla los requisitos exigidos para su emisión;

El permiso puede suspenderse por hasta seis meses si incurre en una de las condenas o sanciones bajo la sección R.212-4 de la Ley de Tráfico de Carreteras.

*Para ir más allá*: Artículos L. 212-3 a L. 212-5 y II de la Sección R. 212-2 de la Ley de Tráfico de Carreteras.

#### Costos asociados con la calificación

Se paga la formación para llevar a cabo cursos de formación en concienciación sobre seguridad vial y el coste varía en función de la ruta elegida.

Para obtener más información, es aconsejable acercarse a las instituciones interesadas.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE), que esté legalmente establecido y participe en la labor de un curso de sensibilización sobre la seguridad vial, puede trabajar como miembro del Espacio Económico Europeo (EEE) temporal y casual, la misma actividad en Francia.

Cuando ni el acceso a la profesión ni su ejercicio estén regulados en ese Estado miembro, el profesional deberá justificar haber realizado esta actividad durante un mes a un año en los últimos diez años.

Una vez que cumpla esta condición, el interesado debe, antes de su primera actuación en Francia, hacer una declaración previa para llevar a cabo esta actividad (cf. "5o. b. Predeclaración para el ejercicio temporal y casual (LPS)").

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Todo nacional de un Estado de la UE o del EEE legalmente establecido en ese Estado miembro que sea facilitador de cursos de sensibilización en la seguridad vial podrá, de forma permanente, llevar a cabo la misma actividad en Francia.

Para ello, debe:

- Disponer de un certificado de competencia o certificado de formación expedido por un Estado miembro que regule la actividad o por un tercer Estado y reconocido por un Estado miembro;
- cuando el Estado no regula la profesión, se han dedicado a esta actividad durante al menos un año en los últimos diez años y poseen al menos un certificado de competencia o documento de formación que justifique que ha sido preparado para el ejercicio de esta profesión.

Una vez que el profesional cumple con estas condiciones, debe solicitar un permiso de ejercicio (ver "5.00. (a.) solicitud de autorización para ejercer) en las mismas condiciones que el nacional francés.

*Para ir más allá*: Artículo R. 212-3-1 de la Ley de Tráfico de Carreteras.

3°. Condiciones de honorabilidad
-----------------------------------------

Para ejercer, el profesional no debe haber sido condenado por un delito o una sentencia correccional por:

- daño a la persona humana (por ejemplo, daño involuntario a la vida, poner en peligro a otros, tráfico de narcóticos, etc.);
- daños a la propiedad (robo, fraude, recepción, etc.);
- violación de la autoridad estatal y la confianza pública (indignación y rebelión con una persona que es custodio de la autoridad pública, falsificación y el uso de la falsificación por escrito, falso testimonio, etc.);
- fraude en un examen público o competencia
- incumplimientos de las obligaciones de la legislación laboral (trabajo oculto, violación de la igualdad entre hombres y mujeres, empleo de extranjeros ilegales, etc.);
- una infracción de tráfico (impedir el tráfico, conducir sin licencia de conducir, atropellar y ejecutar, falta de seguro, etc.);
- uso de narcóticos.

*Para ir más allá*: Artículos L. 212-2 y R. 212-4 de la Ley de Tráfico de Carreteras.

4°. Sanciones
----------------------------------

El profesional se enfrenta a una pena de un año de prisión y una multa de 15.000 euros si trabaja como facilitador del curso de sensibilización en carretera sin tener la autorización para dirigir.

*Para ir más allá*: Artículo L. 212-4 de la Ley de Tráfico de Carreteras.

5°. Proceso de reconocimiento de cualificación y trámites
-------------------------------------------------------------------

### a. Solicitud de permiso para ejercer

**Autoridad competente**

El profesional deberá presentar su solicitud al prefecto del departamento de su lugar de residencia en papel gratuito.

**Documentos de apoyo**

Su solicitud deberá ir acompañada de los siguientes documentos:

- Prueba de identidad
- una prueba de residencia o, si no un empleado, una declaración de establecimiento en el territorio nacional;
- Una fotocopia a dos manos de su licencia de conducir válida
- Una fotocopia de sus diplomas o títulos
- para el extranjero, un permiso de residencia válido;
- Una fotocopia de su autorización docente válida siempre y cuando el profesional sea un facilitador experto en seguridad vial;
- Si el profesional está practicando como psicólogo, una fotocopia de su comprobante de inscripción en el Registro Nacional de Psicólogos (Archivo de Directorio Compartido de profesionales de la salud (RPPS) o anteriormente ADELI);
- fotocopia de su certificado de seguimiento del curso de formación inicial en concienciación sobre la seguridad vial, cuyo modelo se establece en la Lista 2 del auto de 26 de junio de 2012 mencionado anteriormente.

**Procedimiento y plazos**

El prefecto reconoce la recepción de la solicitud en el plazo de un mes. La autorización de ejercicio, que se basa en el Anexo 3 de la orden del 26 de junio de 2012, se expide por un período de cinco años.

**Tenga en cuenta que**

La autorización podrá estar sujeta a una solicitud de renovación que deberá presentar el profesional al menos dos meses antes de su expiración.

*Para ir más allá* 26 de junio de 2012.

### b. Predeclaración para ejercicio temporal e informal (LPS)

**Autoridad competente**

El profesional debe solicitarpor cualquier medio al prefecto del departamento en el que desee ejercer.

**Documentos de apoyo**

Su solicitud debe incluir los siguientes documentos, si los hubiere, con su traducción al francés:

- Una pieza de identificación o prueba de nacionalidad para el nacional de la UE;
- para el nacional de la UE, un certificado que certifica que está legalmente establecido en un país miembro para llevar a cabo la actividad de facilitador de cursos de sensibilización en seguridad vial y que no incurre en prohibiciones temporales o permanentes practicar o condenar penalmente por una infracción de tráfico;
- prueba de sus cualificaciones profesionales
- en caso afirmativo, prueba de que ha participado en esta actividad durante al menos un año en los últimos diez años en un Estado miembro.

**Procedimiento**

En el plazo de un mes a partir de la recepción de la solicitud, el prefecto podrá autorizar al solicitante a realizar la actividad o para el examen de sus cualificaciones, decidir someterlo a una prueba de aptitud (véase a continuación "Bueno saber: medidas de compensación").

El silencio guardado por el prefecto más allá de un mes vale permiso para practicar. Esta autorización se expide por un período de cinco años.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la formación recibida por el nacional y la requerida en Francia para llevar a cabo la actividad de un curso de concienciación sobre la seguridad vial, el prefecto podrá decidir someterlo a un evento Aptitud. Esta prueba de aptitud toma la forma de un examen escrito, cuyos resultados se le comunicarán dentro de los treinta días.

*Para ir más allá*: Artículos R. 212-1 y R. 212-2 de la Ley de Tráfico de Carreteras.

### c. Remedios

#### Centro de Asistencia Francesa

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

