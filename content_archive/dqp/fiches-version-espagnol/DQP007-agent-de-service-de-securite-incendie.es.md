﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP007" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Seguridad" -->
<!-- var(title)="Oficial de seguridad contra incendios y asistencia personal" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="seguridad" -->
<!-- var(title-short)="oficial-de-seguridad-contra-incendios" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/seguridad/oficial-de-seguridad-contra-incendios-y-asistencia-personal.html" -->
<!-- var(last-update)="2020-04-15 17:22:21" -->
<!-- var(url-name)="oficial-de-seguridad-contra-incendios-y-asistencia-personal" -->
<!-- var(translation)="Auto" -->


Oficial de seguridad contra incendios y asistencia personal
===========================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:21<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Oficial de Seguridad contra Incendios y Asistencia Personal ("Oficial SSIAP") es un profesional cuya misión es garantizar la seguridad de las personas y la seguridad contra incendios de la propiedad, prevención de incendios, seguridad contra incendios y asistencia a las personas, mantenimiento básico de los activos de seguridad contra incendios, alerta y recepción de servicios de emergencia, evacuación del público, respuesta temprana a los incendios, asistencia al fuego personas en los establecimientos donde operan y el funcionamiento del PC de seguridad contra incendios.

Se encarga de las rondas en los sitios monitoreados y se asegura de que el personal sea consciente de los problemas de incendio.

En caso de incendio, coordinará la evacuación y seguridad de los presentes en el sitio.

*Para ir más allá*: Artículo 2 de la[decretado a partir del 2 de mayo de 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000448223) misiones, empleo y cualificación de personal permanente de seguridad contra incendios en edificios públicos y de gran altura.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de agente SSIAP está reservada al titular:

- Un certificado de aptitud médica de menos de tres meses
- Certificado de Capacitación de Primeros Auxilios (AFPS) o Certificado de Prevención de Alivio Cívico de Nivel 1 (PSC1) por menos de 2 años;
- un certificado de primeros auxilios (SST) válido o un certificado de equipo de primeros auxilios (PSE1);
- uno de los siguientes títulos:- El Diploma de Oficial de Seguridad contra Incendios y Asistencia Personal (SSIAP 1),
  - la licenciatura profesional especialidad de prevención de la seguridad,
  - El agente técnico profesional especializado en patentes de prevención y seguridad,
  - El certificado de aptitud profesional de prevención especializada y oficial de seguridad,
  - La mención adicional de especialidad de seguridad civil y corporativa,
  - certificado nacional para los jóvenes bomberos durante menos de tres años con seguimiento por un[Añadir](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=87A6A1A93A66E34BCFFFFD96518CC4E2.tplgfr27s_1?idArticle=LEGIARTI000029044217&cidTexte=LEGITEXT000006051723&dateTexte=20171221),
  - o han sido bomberos profesionales o voluntarios, bomberos militares del ejército, bomberos militares de la fuerza aérea o bomberos de la marina nacional y han seguido el módulo complementario mencionado anteriormente,
  - o han sido suboficiales de bomberos profesionales o voluntarios, bomberos militares del ejército, bomberos militares de la fuerza aérea o bomberos de la Marina Nacional y poseedor de la unidad de valor de entrenamiento PRV 1 o AP 1 o el certificado de prevención expedido por el Ministro del Interior.

*Para ir más allá*: Artículos 3 y 4 de la orden de 2 de mayo de 2005.

#### Entrenamiento

El diploma de oficial SSIAP de nivel 1 anterior es emitido por un centro de capacitación acreditado por la prefectura.

Después de un entrenamiento de al menos 67 horas, el candidato realizará un examen consistente en una prueba escrita con un cuestionario de opción múltiple, y una prueba práctica durante la cual realizará una ronda con detección de anomalías y desastres Potencial.

*Para ir más allá*: Artículo 8 de la orden de 2 de mayo de 2005.

#### Costos asociados con la calificación

El precio de formación que lleva a la emisión del diploma SSIAP1 oscila entre 850 y 1.300 euros. Para obtener más información, es aconsejable acercarse a los centros autorizados dispensarlo.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Todo nacional de un Estado miembro de la Unión Europea (UE) o parte en el Espacio Económico Europeo que esté establecido y practique legalmente la actividad de agente SSIAP en ese Estado podrá ejercer la misma actividad en Francia, de forma temporal y ocasional.

En primer lugar, debe solicitarlo mediante declaración escrita al Ministro del Interior (véase infra "4o. a. Obtener una licencia para ejercer para el nacional un ejercicio temporal o casual (LPS)").

En el caso de que la profesión no esté regulada, ni en el curso de la actividad ni en el marco de la formación, en el estado en el que el profesional esté legalmente establecido, deberá haber realizado esta actividad durante al menos un año, en el transcurso de los diez años antes del beneficio, en uno o más Estados de la UE.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación en Francia, el Ministro del Interior podrá exigir que la persona se someta a una prueba de aptitud.

*Para ir más allá*: Artículo 3-1 de la orden de 2 de mayo de 2005.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

El nacional de un Estado de la UE o del EEE, que sea legalmente agente de SSIAP en ese Estado, podrá establecerse en Francia para llevar a cabo la misma actividad de forma permanente.

Por lo tanto, tendrá que solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante: "4. b. Solicitar un certificado de cualificación profesional para el ejercicio permanente de la UE o el EEE para un ejercicio permanente (LE)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá justificar haberla realizado en uno o varios Estados miembros durante al menos un año, a tiempo completo o en a tiempo parcial, a lo largo de diez años.

Cuando existan diferencias sustanciales entre su cualificación profesional y la formación requerida en Francia, el Ministro del Interior podrá exigirle que se someta a una medida de compensación (véase infra 4. b. Bueno saber: medida de compensación »).

*Para ir más allá*: Artículo 3-2 de la orden de 2 de mayo de 2005.

3°. Formación profesional continua
-------------------------------------------

La formación profesional continua debe ser completada por el oficial del SSIAP en la práctica cada tres años, a más tardar el aniversario de la emisión del diploma SSIAP1.

Debe permitir al profesional actualizar su conocimiento de las nuevas regulaciones y mantener sus habilidades. Al final de las prácticas, el centro de formación acreditado dará a cada participante un certificado de formación para mantener los conocimientos.

**Tenga en cuenta que**

El oficial de SSIAP deberá someterse a un curso de primeros auxilios cada dos años.

*Para ir más allá*: Artículo 7 y Apéndice XII de la orden de 2 de mayo de 2005.

4°. Procedimientos y formalidades de reconocimiento de cualificación
-----------------------------------------------------------------------------------------

### a. Obtener una licencia para ejercer para el nacional para un ejercicio temporal o casual (LPS)

**Autoridad competente**

El Ministro del Interior está facultado para decidir sobre la solicitud de autorización para prestar el servicio.

**Documentos de apoyo**

En apoyo de la solicitud de licencia, el nacional envía un expediente a la autoridad competente que contiene los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE;
- un certificado que justifique su actividad durante al menos un año en los últimos diez años, cuando ni la formación ni el ejercicio de la profesión estén regulados en el Estado miembro;
- prueba de sus cualificaciones profesionales, particularmente en el área de rescate personal, conocimiento de las normas francesas de seguridad contra incendios, y su capacidad para escribir informes en francés;
- un certificado que confirme la ausencia de prohibiciones temporales o permanentes en la práctica de la profesión o condenas penales.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Resultado del procedimiento**

Una vez recibidos todos los documentos del expediente, el Ministro del Interior dispone de un mes para decidir:

- ya sea para autorizar la entrega;
- o imponer una prueba de aptitud al nacional cuando existan diferencias sustanciales entre sus cualificaciones profesionales y las requeridas en Francia. En caso de denegación de esta medida de compensación o en caso de incumplimiento en su ejecución, el nacional no podrá llevar a cabo la prestación de servicios en Francia;
- informarle de una o más dificultades que pueden retrasar la toma de decisiones. En este caso, tendrá dos meses para decidir, a partir de la resolución de las dificultades.

El silencio guardado por la autoridad competente en estos tiempos merece autorización para iniciar la prestación de servicios.

*Para ir más allá*: Artículo 3-1 de la orden de 2 de mayo de 2015.

### b. Solicitar un certificado de cualificación profesional para el ejercicio permanente de la UE o del EEE para un ejercicio permanente (LE)

**Autoridad competente**

La solicitud de reconocimiento de la cualificación profesional debe dirigirse al Ministro del Interior.

**Documentos de apoyo**

La solicitud de certificación de cualificación profesional se realiza por correo e incluye un fichero que consta de los siguientes documentos justificativos:

- Una fotocopia del documento de identidad válido del solicitante
- una prueba de cualificación profesional en forma de certificado de competencia o un diploma o un certificado de formación profesional;
- Una declaración para justificar su conocimiento de la lengua francesa;
- una declaración para justificar su conocimiento de las normas francesas de seguridad contra incendios.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Bueno saber: medida de compensación**

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a una prueba de aptitud en un plazo máximo de seis meses a partir de la decisión que se le imponga.

*Para ir más allá*: Artículo 3-2 de la orden de 2 de mayo de 2015.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

