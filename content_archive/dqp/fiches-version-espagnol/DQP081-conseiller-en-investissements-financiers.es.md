﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP081" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Asesor de Inversiones Financieras" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="asesor-de-inversiones-financieras" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/asesor-de-inversiones-financieras.html" -->
<!-- var(last-update)="2020-04-15 17:21:04" -->
<!-- var(url-name)="asesor-de-inversiones-financieras" -->
<!-- var(translation)="Auto" -->


Asesor de Inversiones Financieras
=================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:04<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Asesor de Inversiones Financieras (CIF) es un profesional que trabaja como profesional en:

- asesoramiento en inversiones en instrumentos financieros (acciones, bonos, etc.);
- Asesoramiento en la prestación de servicios de inversión;
- asesoramiento sobre la realización de transacciones en diversos activos.

*Para ir más allá*: Artículo L. 541-1 del Código Monetario y Financiero.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de CIF, el interesado debe cumplir todas las condiciones siguientes:

- Tener al menos 18 años de edad
- habitualmente residen o están legalmente establecidos en Francia;
- estar inscritos en el registro único de la[Orias](https://www.orias.fr/) (registro único de intermediarios de seguros, banca y finanzas);
- no han sido objeto de una condena definitiva en los últimos diez años:- por el crimen,
  - una pena de prisión firme o al menos seis meses de prisión suspendida,
  - desimismo de las funciones de funcionario público o ministerial;
- no está sujeto a una prohibición de la práctica, una suspensión temporal o permanente, o una retirada total o parcial de la acreditación;
Además, también debe estar al día con la contribución anual (450 euros) de la Autoridad de Mercados Financieros (AMF).

*Para ir más allá*: Artículo L.500-1 del Código Monetario y Financiero.

#### Entrenamiento

Para realizar las funciones de CIF, el profesional debe:

- Tener un título de tres años en estudios de posgrado legales, económicos o de gestión;
- Han completado 150 horas de formación profesional en relación con las misiones del CIF;
- tienen dos años de experiencia profesional en los cinco años anteriores a la ascensión a operaciones relacionadas con la inversión, la banca o la prestación de servicios de inversión.

*Para ir más allá*: Artículo 325-1 del Reglamento General de la[Amf](http://www.amf-france.org/).

#### Costos asociados con la calificación

La formación que conduce a la cualificación profesional del CIF se paga y el costo varía en función del curso previsto. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Con el fin de reforzar la protección de los inversores y armonizar las condiciones de los proveedores de servicios de inversión en toda la Unión Europea (UE), se han adoptado directivas sobre los mercados de instrumentos financieros 21 de abril de 2004 y 15 de mayo de 2014 por el Parlamento Europeo y el Consejo.

No obstante lo que se[FOMIN I](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex:32004L0039) Y[FOMIN II](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32014L0065), el ejercicio de la actividad de CIF en Francia, de forma temporal y ocasional, no está regulado para un nacional de un Estado miembro de la UE o el Espacio Económico Europeo (EEE) en Francia de forma temporal y ocasional.

Por lo tanto, el nacional que desee ejercer en Francia estará sujeto a la normativa aplicable a los nacionales franceses (véase más arriba "2". a. Cualificaciones profesionales").

*Para ir más allá* : Directiva 2014/65/UE del Parlamento Europeo y del Consejo (MIF II), de 15 de mayo de 2014; Reglamento 600/2014, de 15 de mayo de 2014, del Parlamento Europeo y del Consejo.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre (LE))

La misma exención se aplica a los nacionales de la UE que deseen llevar a cabo el CIF de forma permanente.

A continuación, estará sujeto a la normativa aplicable a los nacionales franceses (véase más arriba "2). b. Nacionales de la UE o del EEE: para un ejercicio temporal e informal (Entrega gratuita de servicios)").

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Reglas de buena conducta

El CIF está sujeto al cumplimiento de las normas de buena conducta y debe:

- Actuar honestamente, de manera justa y profesional;
- Respetar la equidad
- informar a los clientes y satisfacer sus intereses
- Empoderarse a sí mismos para prevenir y manejar cualquier conflicto de intereses;
- llevar a cabo sus misiones de forma independiente.

*Para ir más allá*: Artículo L. 541-8-1 del Código Monetario y Financiero y artículos 325-3 y siguientes del Reglamento General de la AMF.

### b. Solicitud para unirse a una asociación profesional

El CIF debe unirse a una de las siguientes asociaciones aprobadas por AMF:

- Analistas y asesores de inversiones, finanzas y transferencias corporativas (Acifte);
- Asociación Nacional de Asesoramiento Financiero - Cif ([Anacofi-Cif](http://www.anacofi.asso.fr/)) ;
- Cámara Nacional de Asesores de Gestión de Patrimonio ([CNCGP](http://www.cncgp.fr/)) ;
- Cámara Nacional de Asesores de Inversiones Financieras ([CNCIF](http://www.cncif.org/)) ;
- Empresa independiente de consultoría de gestión de patrimonios ([Empresa CGPI](http://www.lacompagniedescgpi.fr/)).

**Tenga en cuenta que**

Estas asociaciones pueden inscribirse en el registro único de sus miembros.

**Admisión**

La asociación determina los procedimientos escritos de admisión y sanción del profesional.

Esta asociación es responsable de garantizar:

- seguimiento de la actividad profesional de sus miembros;
- su representación colectiva;
- para defender sus derechos e intereses.

La asociación verifica que el CIF tiene un programa de actividad que indica:

- Los tipos de actividades realizadas por el profesional;
- La estructura de su organización
- la identidad de sus accionistas y el importe de su participación.

La asociación determina los procedimientos de afiliación, retiro de la membresía, control y sanción de sus miembros a través de procedimientos escritos.

*Para ir más allá*: Artículo L. 541-4 del Código Monetario y Financiero; Artículo 325-2 del Reglamento General AMF.

### Sanciones

El acto de un CIF que prohívelo ilegalmente se castiga con cinco años de prisión y una multa de 375.000 euros, en las mismas condiciones que la estafa (véase el artículo 313-2 del Código Penal).

En caso de violación de las normas de registro, el CIF se enfrenta a una pena de dos años de prisión y/o a una multa de 6.000 euros.

*Para ir más allá*: Artículo L. 546-4 del Código Monetario y Financiero.

4°. Seguro
-------------------------------

### a. Obligación de inscribirse en el registro único de orias

**Autoridad competente**

El CIF debe estar inscrito en el registro único de intermediarios de seguros del organismo para el mantenimiento del registro único de intermediarios de seguros, bancarios y financieros ([Orias](https://www.orias.fr/)).

**Documentos de apoyo**

Para cualquier solicitud de inscripción, el profesional debe registrarse en el[Orias](https://www.orias.fr/) y proporcionar en línea:

- un certificado de registro en el Registro de Comercio y Sociedades (Kbis) de menos de tres meses o, en su defecto, un documento de identidad si no está inscrito en el Registro de Comercio y Sociedades;
- un certificado de pertenencia a una asociación profesional (cf.supra "3.3. b. Solicitud para unirse a una asociación profesional";
- prueba de capacidad profesional, es decir:- es decir, un título de posgrado de tres años,
  - un mínimo de 150 horas de capacitación adquirida a un profesional u organización dedicada a actividades de asesoramiento de inversión financiera,
  - dos años de experiencia profesional en los últimos cinco años;
- un certificado de responsabilidad civil profesional (véase infra "4.0). b. Seguros");
- tasas de inscripción (30 euros).

**Tenga en cuenta que**

Si no se realiza ningún pago dentro de los 30 días posteriores a la recepción de la solicitud, se rechazará.

**hora**

Orias registra al solicitante en un plazo máximo de dos meses a partir de la recepción de su expediente completo.

*Para ir más allá*: Artículos L. 546-1 y L. 541-2 del Código Monetario y Financiero; Sección L. 512-1 del Código de Seguros.

### b. Seguros

Como profesional, el CIF debe tomar un seguro de responsabilidad civil profesional.

Esta garantía entra en vigor el 1 de marzo por un período de doce meses y se renueva tácitamente el 1 de enero de cada año.

Cuando el profesional inicia su negocio cIF, su contrato de seguro se extiende por el período de la fecha de registro al 1 de marzo del año siguiente.

El umbral de esta garantía es de 150.000 euros por reclamación y 150.000 euros al año de seguro para entidades jurídicas con menos de dos empleados.

*Para ir más allá*: Artículos L. 541-3 y D. 541-9 del Código Monetario y Financiero.

5°. Remedios
----------------------

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

