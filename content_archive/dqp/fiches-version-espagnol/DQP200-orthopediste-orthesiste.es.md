﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP200" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Ortopedista-ortopedista" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="ortopedista-ortopedista" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/ortopedista-ortopedista.html" -->
<!-- var(last-update)="2020-04-15 17:21:52" -->
<!-- var(url-name)="ortopedista-ortopedista" -->
<!-- var(translation)="Auto" -->


Ortopedista-ortopedista
=======================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:52<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El ortopedista es un asistente médico profesional, cuya actividad consiste en realizar, con receta médica, un dispositivo con el objetivo de tratar todas las afecciones relacionadas con el sistema musculoesquelético (musculoesquelético) de personas con discapacidad.

Como tal, el profesional mide, diseña y fabrica la herramienta del equipo y entrega, adapta y repara el producto.

Los dispositivos médicos de los dispositivos involucrados son:

- cinturones de apoyo o apoyo;
- aparatos ortopédicos de la inmovilización de la columna vertebral;
- hernias de vendaje;
- ortesis de restricción elástica;
- ropa compresiva a medida.

*Para ir más allá*: Artículos L. 4364-1 y D. 4364-6 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de ortopedista - ortopedista, el profesional debe:

- Estar calificado profesionalmente
- para registrar su diploma (ver infra "5 grados). a. Solicitud de registro de su diploma").

#### Entrenamiento

Para ser reconocido como profesionalmente calificado, el profesional debe ser titular de:

- un título profesional de ortopedista - ortopedista;
- el Certificado de Técnico Superior (BTS) del Prótesis - Ortopedista (Bac 2);
- para farmacéuticos, una universidad o un título interuniversitario en ortopedia;
- para profesionales no farmacéuticos y no ortoprotésicos a partir del 25 de febrero de 2007, un título o certificado reconocido por decisión del Ministro responsable de la salud y la validación de la formación en "Ortopedia-Ortopedia" inscrita en el Directorio Certificaciones Profesionales Nacionales ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)) ;
- para profesionales no farmacéuticos y no ortoprotéticos antes del 25 de febrero de 2007, uno de los siguientes grados:- un certificado de técnico de vendaje ortopédico-ortático expedido por la escuela de Marsella, las cámaras de comercio de Alsacia y Mosela, o el centro de formación Ecotev en Viena,
  - un certificado como técnico ortológico senior expedido por la Cámara de Comercio de París y la Cámara Nacional Sindical de Podólogos,
  - un título didáctico en ortopedia publicado por la Escuela Ortopédica de Poissy entre 1996 y 2000;
- reconocimiento de sus cualificaciones profesionales de las agencias de seguros de salud y el Ministerio de Veteranos y Víctimas de la Guerra.

*Para ir más allá* : Artículo D. 4364-7 del Código de Salud Pública y Artículo 7 de la Orden del 1 de febrero de 2011 sobre las Profesiones de Protista, Ortopedista para el Dispositivo de Las Personas con Discapacidad.

#### Costos asociados con la calificación

El costo de la formación que conduce a la profesión de ortopedia -ortopedista se paga y su costo varía de acuerdo con el curso previsto. Es aconsejable acercarse a las instituciones interesadas para obtener más información.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Todo nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE), legalmente establecido y practicando la actividad de ortopedia - ortopedista, podrá, de forma temporal e informal, ejercer el mismo ejercicio actividad en Francia.

Cuando ni el acceso a la actividad ni su ejercicio estén regulados en ese Estado miembro, el nacional deberá justificar haber llevado a cabo esta actividad durante al menos un año en los últimos diez años en uno o varios Estados miembros;

Antes de la prestación de servicios, el profesional está obligado a hacer una declaración al prefecto regional (véase infra "5o. b. Predeclaración del nacional para un ejercicio temporal e informal (LPS)").

Además, el profesional debe justificar la necesidad de contar con las habilidades del idioma necesarias para llevar a cabo su actividad.

*Para ir más allá*: Artículos L. 4364-6 y L. 4364-7 del Código de Salud Pública.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Cualquier nacional de la UE o del EEE, legalmente establecido y que actúe la actividad de la ortopedia - ortopedista puede llevar a cabo, de forma permanente, la misma actividad, en Francia.

Para ello, el profesional debe ser el titular:

- un certificado de formación expedido por un Estado miembro que regula el ejercicio y el acceso a dicha profesión;
- cuando el Estado miembro no regule el acceso o la práctica a la profesión, un documento de formación que acredite que ha estado preparado para llevar a cabo la actividad de anestesista ortopédico y un certificado que certifique que ha llevado a cabo esta actividad durante al menos un año en los últimos diez años;
- un certificado de formación expedido por un tercer Estado pero reconocido por un Estado miembro distinto de Francia, que permita llevar a cabo esta actividad, así como una prueba de ejercicio de esta actividad durante al menos tres años.

En caso de diferencias sustanciales entre la formación recibida por el profesional y la necesaria para llevar a cabo la actividad de ortopedia - ortopedista en Francia, el prefecto puede decidir someter al nacional a una medida de compensación (véase infra "5o. c. Bueno saber: medidas de compensación").

Además, el profesional debe justificar la necesidad de contar con las habilidades del idioma necesarias para llevar a cabo su actividad.

*Para ir más allá*: Artículo L. 4364-5 del Código de Salud Pública.

3°. Reglas profesionales
---------------------------------

**Respeto a la confidencialidad profesional**

El ortopedista, como profesional de la salud, está obligado por la confidencialidad profesional.

*Para ir más allá*: Artículo D. 4364-12 del Código de Salud Pública.

**Reglas de buenas prácticas**

Además, el profesional está obligado por las reglas de buenas prácticas que son responsabilidad de su profesión.

Como tal, debe incluir:

- trabajar en locales equipados y adaptados a las necesidades del ejercicio de su actividad, y permitirle recibir a sus pacientes en buenas condiciones de aislamiento (fonética y visual);
- mantener y actualizar un archivo para cada uno de sus pacientes incluyendo toda la información sobre ellos y una descripción de los dispositivos necesarios y posibles intervenciones.

*Para ir más allá*: Artículo D. 4364-13 del Código de Salud Pública; Artículos 13 a 26 del auto de 1 de febrero de 2011 antes mencionado.

4°. Seguro
-------------------------------

Un ortopedista - ortopedista como profesional de la salud está obligado a tomar un seguro de responsabilidad civil profesional por los riesgos incurridos durante el curso de su actividad.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Proceder a registrar su diploma

**Autoridad competente**

El solicitante debe solicitar el registro a la Agencia Regional de Salud (ARS).

**Documentos de apoyo**

Para ello, deberá transmitir la siguiente información, considerada validada y certificada por la organización que emitió el diploma o el título de formación:

- El estado civil del titular del diploma y todos los datos para identificar al solicitante;
- El nombre y la dirección de la institución que impartió la capacitación;
- el título de la formación.

**Resultado del procedimiento**

Después de comprobar las exposiciones, el LRA registra el diploma.

**Tenga en cuenta que**

La inscripción sólo es posible para un departamento, sin embargo, si el profesional desea ejercer en varios departamentos, se le colocará en la lista del departamento en el que se encuentra el lugar principal de su actividad.

**Costo**

Gratis.

*Para ir más allá*: Artículo D. 4364-18 del Código de Salud Pública; Artículos D. 4333-1 a D. 4333-6-1 del Código de Salud Pública.

### b. Predeclaración del nacional para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe solicitar al prefecto de la región donde desee llevar a cabo sus servicios.

**Documentos de apoyo**

Su solicitud debe incluir:

- formulario de declaración adjunto a la lista de la[Detenido](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000021863886&dateTexte=20171113) 19 de febrero de 2010 completado y firmado;
- todos los documentos justificativos mencionados en este formulario.

**Tiempo y procedimiento**

El prefecto regional informa al solicitante en el plazo de un mes a partir de la recepción de su expediente:

- Que puede comenzar su prestación de servicio;
- No puede comenzar su prestación de servicio;
- que debe estar sujeto a un chequeo previo de sus cualificaciones profesionales. Si es así, cuando exista una diferencia sustancial entre la formación recibida por el nacional y la formación requerida

**Tenga en cuenta que**

Para llevar a cabo esta actividad en Francia, el prefecto puede decidir presentarla como compensación (véase infra "5o. c. Bueno saber: medidas de compensación").

En ausencia de una respuesta del prefecto en el plazo de un mes, el nacional puede comenzar su prestación de servicio.

**Tenga en cuenta que**

El nacional será incluido en una lista especial por el prefecto, que le facilitará un recibo de su declaración mencionando su número de registro.

Esta declaración debe renovarse anualmente en las mismas condiciones.

*Para ir más allá*: Artículos R. 4364-11-3, R. 4331-12 a R. 4331-15 y D. 4364-11-9 del Código de Salud Pública; orden de 19 de febrero de 2010 relativo a la declaración previa de la prestación de servicios para la práctica de ortoprésteis, podólogo, ocularista, epitetista, ortopedista-ortopedista.

### c. Solicitud de autorización de ejercicio para el nacional para un ejercicio permanente (LE)

**Autoridad competente**

El nacional debe presentar su solicitud por duplicado, por carta recomendada con notificación de recepción, al Comité de Ortopedia.

**Documentos de apoyo**

La solicitud debe incluir el[Formulario de solicitud de autorización](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DB4E58672EA257FEDF6B78008E6B179E.tplgfr24s_1?idArticle=LEGIARTI000021777548&cidTexte=LEGITEXT000021777545&dateTexte=20180412) completado y firmado.

Además, deberá proporcionar los siguientes documentos justificativos, si los hubiere, con su traducción certificada al francés:

- Una fotocopia de su dNI válido
- una copia de su documento de formación que permita el ejercicio de la actividad de ortopedista - ortopedista en ese Estado miembro y, en su caso, de sus diplomas adicionales;
- cualquier documentación que justifique la educación continua o la experiencia laboral del solicitante;
- Un certificado del Estado miembro que certifique que el solicitante no está sujeto a ninguna sanción;
- Una copia de los certificados expedidos por las autoridades en los que se menciona el nivel de formación del solicitante, así como los detalles de la formación (volumen por horas, instrucciones seguidas);
- cuando el Estado no regule el acceso a la profesión o a su ejercicio, prueba de que ha estado involucrado en esta actividad durante al menos dos años en los últimos diez años;
- cuando el nacional ha adquirido su título de formación en un tercer Estado pero reconocido por un Estado miembro, el reconocimiento de su título de formación.

**Tiempo y procedimiento**

El prefecto regional reconoce la recepción de la solicitud en el plazo de un mes. Si la solicitud no es contestada dentro de los cuatro meses siguientes a la recepción de la solicitud, se considerará denegada.

*Para ir más allá*: Artículos R. 4364-11 a R. 4364-11-2 del Código de Salud Pública; decreto de 20 de enero de 2010 por el que se establece la composición del expediente que se facilitará a la comisión de autorización competente para el examen de las solicitudes presentadas para el ejercicio en Francia de las profesiones de ortoprotésico, podólogo, ocularista, epítisto, ortopedista-ortostista.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la formación recibida por el profesional y la necesaria para llevar a cabo la actividad de otopedista -orthetist en Francia, la autoridad competente podrá decidir someterla a la elección, o a una pasantía de adaptación , o a una prueba de aptitud.

La prueba de aptitud debe completarse dentro de los seis meses siguientes a la notificación al solicitante. Su propósito es verificar los conocimientos y habilidades del nacional para poder ejercer en Francia. El objetivo del curso de adaptación con un profesional cualificado es permitir al nacional adquirir los conocimientos necesarios para llevar a cabo esta actividad.

*Para ir más allá*: Artículos R. 4311-35 y R. 4311-36 del Código de Salud Pública; decreto de 24 de marzo de 2010 por el que se establecen las modalidades de organización de la prueba de aptitud y el curso de adaptación para la práctica en Francia de las profesiones de ortoprotista, podólogo, ocularista, epitetista, ortopedista-ortesor nacionales de los Estados miembros de la UE o parte en el acuerdo del Espacio Económico Europeo.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

