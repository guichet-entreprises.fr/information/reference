﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP233" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Psicoterapeuta" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="psicoterapeuta" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/psicoterapeuta.html" -->
<!-- var(last-update)="2020-04-15 17:22:05" -->
<!-- var(url-name)="psicoterapeuta" -->
<!-- var(translation)="Auto" -->


Psicoterapeuta
==============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:05<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El psicoterapeuta es un profesional cuya actividad consiste en analizar todos los trastornos sociales, psicológicos y psicosomáticos de sus pacientes con el fin de tratarlos y mejorar su bienestar.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para poder practicar, el psicoterapeuta debe ser:

- Titular del título de psicoterapeuta;
- registrado en el Registro Nacional de Psicoterapeutas.

#### Entrenamiento

**Título de psicoterapeuta**

Para ejercer y estar inscrito en el Registro Nacional de Psicoterapeutas, el profesional debe:

- poseer uno de los siguientes grados:- un doctorado de última generación,
  - un máster especializado en psicología o psicoanálisis;
- formación en psicopatología clínica de al menos 400 horas para conocer:- desarrollo, funcionamiento mental y procesos,
  - criterios para el discernimiento de las principales patologías psiquiátricas,
  - principales enfoques utilizados dentro de la profesión.

Además de esta formación, el futuro profesional está obligado a realizar una pasantía práctica de al menos cinco meses a tiempo completo, a tiempo parcial o dividido, en una institución pública o privada acreditada.

**Tenga en cuenta que**

El profesional que justifique haber ejercido psicoterapia durante al menos cinco años a partir de la publicación del decreto de 20 de mayo de 2010 de mayo de 20 con carátula también puede ser reconocido como profesional cualificado. Para ello, debe[formulario de solicitud de autorización de registro](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DD83A8F3F417C19CE560EA85D606ADF6.tplgfr33s_1?idArticle=LEGIARTI000022338740&cidTexte=LEGITEXT000022338729&dateTexte=20180413) completado y firmado con el director del LRA.

Una vez que el profesional tiene su título, puede hacer que su inscripción se haga en el registro nacional de psicoterapeutas (ver infra "5o. a. Inscripción en el Registro Nacional de Psicoterapeutas").

*Para ir más allá* Decreto No 2010-534 de 20 de mayo de 2010 sobre el uso del título de psicoterapeuta; Orden de 9 de junio de 2010 relativa a las solicitudes de inscripción en el Registro Nacional de Psicoterapeutas.

#### Costos asociados con la calificación

La formación para el título de psicoterapeuta se paga y el costo varía de acuerdo con el curso previsto. Es aconsejable consultar con las instituciones interesadas para obtener más información.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido podrá realizar la misma actividad en Francia de forma temporal y ocasional. .

Cuando el Estado miembro no regule el acceso a la formación o su ejercicio, el nacional deberá justificar la realización de esta actividad durante al menos un año en los últimos diez años.

Una vez que cumpla estas condiciones, se requiere hacer una declaración previa con la agencia regional de salud (ARS) antes de comenzar su prestación de servicios.

Además, el profesional debe justificar la necesidad de tener las habilidades de idiomas necesarias para ejercer su profesión en Francia.

*Para ir más allá*: Artículo 52 de la Ley de Política de Salud Pública 2004-806, de 9 de agosto de 2004.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Cualquier nacional de un Estado de la UE o del EEE, legalmente establecido y que actúe como psicoterapeuta, podrá llevar a cabo la misma actividad en Francia de forma permanente.

Para ello, debe ser el titular:

- un certificado de formación expedido por un Estado miembro que regula el acceso y el ejercicio de dicha profesión;
- cuando el Estado miembro no regule el acceso o el ejercicio de un documento de formación expedido por un Estado miembro que acredite su preparación para el ejercicio de la profesión, así como un certificado que justifique que ha llevado a cabo esta actividad durante el menos de un año en los últimos diez años;
- un certificado de formación expedido por un tercer Estado pero reconocido por un Estado miembro de la UE para llevar a cabo esta actividad. Además, debe justificar haber participado en esta actividad durante al menos tres años.

Una vez que el nacional cumple con estas condiciones, debe solicitar permiso para utilizar su título profesional (ver infra "5o. c. Solicitud de autorización de práctica para el nacional para un ejercicio permanente").

Además, el profesional debe justificar la distadez necesaria para llevar a cabo su actividad en Francia.

*Para ir más allá*: Artículo 52 de la Ley de Política de Salud Pública 2004-806, de 9 de agosto de 2004.

3°. Reglas éticas
--------------------------

**Código de ética**

Las normas éticas aplicables a la profesión de psicoterapeuta no están reguladas. Sin embargo, los profesionales deben seguir la carta global de ética de los miembros de esta profesión.

Como tal, el profesional se compromete a:

- el derecho a la información, la privacidad y la dignidad de sus pacientes,
- secreto profesional.

Puede ver la sección de código de ética, en el[sitio web de la Federación Francesa de Psicoterapia y Psicoanálisis](http://www.ff2p.fr/fichiers_site/la_ff2p/la_ff2p.html#code) (FF2P).

4°. Seguro
-------------------------------

El psicoterapeuta como profesional está obligado a tomar un seguro para cubrirlo contra los riesgos incurridos durante su actividad.

**Tenga en cuenta que**

Si el ejercicio profesional como empleado, corresponde al empleador tomar dicho seguro para sus empleados, por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Inscripción en el Registro Nacional de Psicoterapeutas

**Autoridad competente**

El profesional debe solicitar a la agencia regional de salud (ARS) en la región donde se encuentra su residencia profesional principal para el registro.

**Documentos de apoyo**

Su solicitud debe incluir:

- Una copia de su identificación
- un certificado que justifica la obtención de un diploma estatal como médico o máster;
- un certificado de formación de seguimiento en psicopatología clínica;
- Si es necesario, el certificado de registro para profesionales médicos;
- el profesional con un doctorado en medicina, un título de psicólogo o una designación de psicoanalista también debe proporcionar:- un certificado de graduación,
  - un certificado de su registro en un directorio de asociación de psicoanalistas.

**Tiempo y procedimiento**

El Director del LRA reconoce haber recibido su solicitud tan pronto como se reciba su expediente. La falta de respuesta más allá de dos meses es un caso para rechazar su solicitud.

**Costo**

Gratis

**Tenga en cuenta que**

Si el profesional opera dentro de varios sitios, debe declarar todas las direcciones en cuestión.

*Para ir más allá*: Artículos 7 a 9 del Decreto del 20 de mayo de 2010.

### b. Predeclaración del nacional para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional envía su solicitud al Director General del LRA en la región en la que desea llevar a cabo su servicio.

**Documentos de apoyo**

Su solicitud deberá incluir los siguientes documentos, si los hubiere, con su traducción, por un traductor certificado en francés:

- El formulario de presentación previa a la presentación de informes que figura en la Lista 6 de la circular de 24 de diciembre de 2012;
- Una fotocopia de su DNI y un documento que acredite su nacionalidad, si su documento de identidad no lo menciona;
- Una fotocopia de todas sus credenciales de formación;
- un certificado de la autoridad competente del Estado miembro que certifique que el nacional está legalmente establecido para ejercer la actividad de psicoterapeuta y que no está prohibido ejercer.

**Tiempo y procedimiento**

El Director General reconoce haber recibido la solicitud del nacional y le informa en el plazo de un mes:

- Que puede comenzar su prestación de servicio;
- Que no puede comenzar su prestación de servicio;
- que debe estar sujeto a un control de competencia y, en su caso, someterse a una prueba de aptitud si el examen de sus cualificaciones muestra una diferencia sustancial entre su formación y la requerida para el ejercicio de profesión en Francia.

**Tenga en cuenta que**

Esta declaración debe renovarse cada año, en las mismas condiciones.

*Para ir más allá*: Apéndices 2 y 3 de la Circular DGOS/RH2/2012/431, de 24 de diciembre de 2012, sobre las condiciones de utilización de la designación de psicoterapeuta por los titulares de diplomas expedidos por los Estados miembros de la Unión Europea, el Espacio Económico Europeo y la Confederación Suiza.

### c. Solicitud de autorización de ejercicio para el nacional para un ejercicio permanente (LE)

**Autoridad competente**

El nacional envía su solicitud al director general del LRA en la región en la que el profesional desea establecer su residencia profesional.

**Documentos de apoyo**

Su solicitud debe incluir:

- El formulario de solicitud de autorización para utilizar el título de psicoterapeuta, cuyo modelo figura en la Lista 3 de la circular de 24 de diciembre de 2012;
- Una fotocopia de su dNI válido
- Una copia de todos sus diplomas, certificados o títulos obtenidos;
- cualquier prueba que justifique el seguimiento de la educación continua, la experiencia o las aptitudes adquiridas en un Estado miembro;
- una declaración de menos de un año de la autoridad competente del Estado miembro en la que se certifique que no está sujeta a ninguna sanción;
- Una copia de los certificados expedidos por los Estados miembros en la que se especifique el nivel de formación y el volumen por hora de las lecciones dictadas;
- cuando el Estado no regule el acceso a la profesión o a su ejercicio, cualquier prueba que justifique que ha sido psicoterapeuta durante al menos un año en los últimos diez años;
- cuando el profesional haya adquirido su cualificación en un tercer Estado pero reconocida por un Estado miembro de la UE, el certificado de reconocimiento del certificado de formación, así como cualquier prueba que justifique que ha realizado esta actividad durante al menos tres años en ese Estado Miembro.

**Tiempo y procedimiento**

El Gerente General confirma la recepción de la solicitud en el plazo de un mes a partir de la recepción. Si la solicitud no es respondida más allá de los cuatro meses posteriores a la recepción, la solicitud se considerará denegada.

*Para ir más allá*: Artículo 14 del Decreto No 2017-1520, de 2 de noviembre de 2017, sobre el reconocimiento de cualificaciones profesionales en el ámbito de la salud.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la formación recibida por el profesional y la necesaria para ejercer como psicoterapeuta, el Director General podrá decidir someterla a una prueba de aptitud en forma de examen completado dentro de los seis meses de la decisión, es decir, en un curso de alojamiento.

Este último debe llevarse a cabo en un centro de salud, una institución social o una institución médico-social bajo la responsabilidad de un profesional.

*Para ir más allá* Circular DGOS/RH2/2012/431, de 24 de diciembre de 2012, sobre las condiciones de utilización de la denominación de psicoterapeuta por los titulares de diplomas expedidos por los Estados miembros de la Unión Europea, el Espacio Económico Europeo y la Confederación Suiza.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

