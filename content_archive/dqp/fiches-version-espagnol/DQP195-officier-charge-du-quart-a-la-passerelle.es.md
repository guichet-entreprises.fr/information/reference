﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP195" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sector marítimo" -->
<!-- var(title)="Oficial a cargo de la vigilancia de puentes en los buques pesqueros" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sector-maritimo" -->
<!-- var(title-short)="oficial-a-cargo-de-la-vigilancia" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sector-maritimo/oficial-a-cargo-de-la-vigilancia-de-puentes-en-los-buques-pesqueros.html" -->
<!-- var(last-update)="2020-04-15 17:22:18" -->
<!-- var(url-name)="oficial-a-cargo-de-la-vigilancia-de-puentes-en-los-buques-pesqueros" -->
<!-- var(translation)="Auto" -->


Oficial a cargo de la vigilancia de puentes en los buques pesqueros
===================================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:18<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Oficial Jefe de Vigilancia del Buque Pesquero ("Oficial") es un marinero cuyas misiones son:

- ayudar al capitán o segundo capitán;
- para maniobrar el buque mientras está en navegación;
- coordinar y mantener el turno, día y noche;
- ayudar a mantener la seguridad del buque y la tripulación;
- para guiar a su equipo en el puente planificando y organizando su trabajo.

También contribuye a:

- Antidesastre
- control de la gestión y actualización de los documentos náuticos por parte de la tripulación;
- para garantizar que los dispositivos de navegación funcionen por las agencias pertinentes y para tomar medidas para mantenerlos.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

### Legislación nacional

La ocupación del Director de Vigilancia en el Puente del Buque pesquero está reservada para el titular del certificado del oficial de vigilancia del puente.

La emisión de esta patente requiere que la persona:

- 18 años el día en que se presentó la solicitud de patente;
- o bien poseer el diploma de oficial a cargo del puente especificado a continuación o uno de los diplomas en el Cuadro 2 de la Lista I de la orden de 22 de diciembre de 2015;
- ser certificado como validar la educación médica de nivel II o III;
- Poseer el certificado general del operador;
- completó un servicio en el mar en la cubierta durante al menos doce meses con entrenamiento a bordo. Para más información, es aconsejable consultar el[Orden de 13 de agosto de 2015 relativa a los registros de formación a bordo de buques](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031070040&categorieLien=id).

**Tenga en cuenta que**

Los certificados y certificados requeridos deben ser válidos en el momento de la solicitud de patente.

*Para ir más allá*: Artículos 2, 14 y siguientes de la orden de 22 de diciembre de 2015.

### Diploma de oficial en el puente

El diploma de Master Watch Officer en el puente es un certificado que justifica la adquisición de módulos o el éxito en la formación inicial internacional de oficiales de puente.

#### Formación profesional de los oficiales

Este curso incluye formación que conduce a la adquisición de módulos y formación que conduce a la emisión o revalidación de los siguientes certificados o certificados:

- Certificado Básico de Formación en Seguridad (CFBS),
- Certificado de Calificación Avanzada de Lucha contra Incendios (CQALI),
- Certificado de Aptitud para Operar Botes Salvavidas y Balsas salvavidas (CAEERS),
- certificado que certifica la validación de la educación médica de nivel II (EM II),
- Certificado general de operador (CGO),
- certificado de idoneidad para las funciones del oficial de seguridad del buque,
- certificado básico de capacitación para operaciones relacionadas con la carga de buques cisterna de gas licuado,
- certificado básico de capacitación para operaciones de carga de buques cisterna y buques cisterna para productos químicos,
- Certificación de formación en el Sistema de Tarjeta Electrónica y Visualización de la Información (ECDIS);
- Certificación de formación en gestión y trabajo en equipo, así como gestión de recursos en el puente y la máquina;
- certificado de formación para el personal que presta servicios en buques de pasajeros exigido en los artículos 3 a 6 del auto de 6 de mayo de 2014 como consecuencia de ello.

**Requisitos de admisión**

La persona que desee integrar la formación profesional de los oficiales debe ser el titular:

- Un certificado de aptitud médica para la navegación;
- Patente del Capitán 500 o una patente reconocida en la Lista I de la orden del 22 de diciembre de 2015;
- certificado que justifica la adquisición de la[Módulo de libertad condicional OCQP](http://www.ucem-nantes.fr/referentiels/cer-pont).

También están disponibles para esta capacitación aquellos que poseen:

- Un certificado como técnico marino superior en la especialidad "pesca y gestión del medio marino";
- Desde el certificado de guardia de oficial a la máquina;
- el certificado de una mano de mazo en el puente o el puente calificado certificado de mar.

*Para ir más allá*: Artículos 6 y siguientes, y apéndices I, II y III de la orden del 22 de diciembre de 2015.

**El resultado de la formación que conduce a la adquisición de módulos**

Cada módulo del artículo 5 de la orden del 23 de diciembre de 2015 se adquiere cuando el candidato ha completado la[Entrenamiento](http://www.ucem-nantes.fr/images/stories/documents/new_ref/reforme_filiere_B/OCQP/Annexe-IV.pdf) y promedió 10 de 20 en todos los eventos.

Al obtener cada módulo, se expide un certificado al candidato de formación, válido por cinco años.

#### Formación inicial internacional

La formación internacional inicial se lleva a cabo durante tres años en la Escuela Nacional de Posgrado Marítimo (ENSM). Su registro se realiza en la selección de los expedientes cuyas condiciones son establecidas por el Director del NSMS.

Para obtener más información sobre la formación inicial internacional, consulte[el sitio web de ENSM](https://www.supmaritime.fr/officier-chef-de-quart-passerelle-international-capitaine-3000.html).

*Para ir más allá* : orden de 5 de julio de 2017 relativa al curso internacional de formación inicial para los diplomas de jefe de guardia y capitán 3000.

#### Graduación

Para obtener el diploma de director de vigilancia en el puente, una persona que haya completado la formación profesional de los oficiales o la formación internacional inicial debe ser titular de:

- Un certificado válido de aptitud médica para la navegación;
- El certificado que justifica el seguimiento exitoso de la formación internacional inicial impartida por el Director de la ENSM;
- certificados y certificados validados durante la formación internacional inicial, adjuntos, si fuera necesario, con el certificado que justifica la formación del servicio en el mar necesario para su emisión;
- certificado certificado de dominio del idioma inglés al menos B1 del Marco Común Europeo de Referencia para las Lenguas (CECR).

### Acondicionamiento físico

El acceso a la profesión de oficial de vigilancia de puentes en los buques pesqueros está sujeto a requisitos mínimos de aptitud física.

Estas condiciones de acondicionamiento físico se evalúan durante las visitas médicas requeridas:

- Antes del acceso a la profesión de oficial;
- Antes del primer embarque;
- antes de entrar en la formación marítima;
- antes de la expiración del certificado de aptitud médica, la duración de la cual varía según la edad del oficial.

Al final de la visita médica, el médico puede decidir que el oficial está en forma. A partir de entonces, se le otorgará un certificado de aptitud.

Cuando la decisión del médico indique aptitud parcial, incapacidad temporal o incapacidad total, el oficial puede impugnarla ante la Comisión Médica Regional para la Aptitud para la Navegación (CMRA).

*Para ir más allá* : decreto de 3 de diciembre de 2015 sobre la salud y la aptitud médica para navegar.

### Control del lenguaje y el conocimiento jurídico

Para ejercer como oficial de vigilancia de puentes, la persona debe tener conocimiento según la francés y jurídica.

Para justificarlos, el oficial debe proporcionar:

- un diploma de educación secundaria o superior francés o un certificado de menos de un año que acredite un máster B2. Para obtener más información, consulte el sitio web del Ministerio de Educación Nacional con el[Marco Común Europeo para las Lenguas](http://eduscol.education.fr/cid45678/cadre-europeen-commun-de-reference-cecrl.html) ;
- cualquier diploma de educación superior francesa que sancione la formación o la enseñanza específicas relacionadas con las facultades y prerrogativas del poder público conferidas al capitán de un buque de bandera francesa.

Si el oficial no tiene ninguno de estos documentos, tendrá que someterse a una prueba escrita en francés y una entrevista ante un jurado cuya composición se menciona en el artículo 5 del decreto de 2 de junio de 2015. La prueba escrita y la entrevista probarán si el oficial tiene los conocimientos legales necesarios para el puesto, y evaluará su capacidad para comunicarse en un contexto profesional y escribir en el idioma francés.

*Para ir más allá*: Artículos 3 y siguientes de la[decreto de 2 de junio de 2015](https://www.legifrance.gouv.fr/eli/decret/2015/6/2/DEVT1422283D/jo/texte),[Sección L. 5521-3 del Código de Transporte](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000023086525&idArticle=LEGIARTI000023073934&dateTexte=&categorieLien=cid).

### Costos asociados con la capacitación

Se paga la formación que conduce al diploma de oficial a cargo de la vigilancia del puente. Para más información, es aconsejable consultar el[Unidad de Concursos y Reseñas Marítimas](http://www.ucem-nantes.fr/) y el ENSM.

### b. Nacionales de la UE o del EEE: para ejercicios temporales o ocasionales (Servicio Gratuito)

Un nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), que actúa legalmente como oficial de vigilancia en buques pesqueros en uno de estos Estados, puede hacer uso de su título profesional en Francia, de forma temporal o casual. Debe solicitarlo, antes de su primera actuación, mediante declaración dirigida al director interregional del mar competente de la región administrativa en la que se identifica (cf. infra "5.a. Hacer una declaración previa Nacionales de la UE/EEE que realizan actividades temporales y ocasionales (LPS)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá justificar haberla realizado en uno o varios Estados miembros durante al menos un año, en los diez años antes de la actuación.

**Qué saber**

Para ser un oficial temporal o ocasional a cargo de la vigilancia del puente, el nacional debe tener las habilidades de idioma necesarias.

Cuando existan diferencias sustanciales entre la formación del nacional y las exigidas en Francia, o cuando el interesado no haya adquirido todas las competencias necesarias para ejercer en un buque pesquero de bandera francesa, compensación (véase "5.a. Bueno saber: medida de compensación").

*Para ir más allá*: Artículos 19 y 20 del Decreto de 24 de junio de 2015.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si:

- posee un título expedido por una autoridad competente de otro Estado miembro, que regula el acceso a la profesión o a su ejercicio;
- El título presentado tiene su equivalente exacto en Francia;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Una vez que cumpla una de las tres condiciones anteriores, deberá solicitar un certificado de reconocimiento del Director Interregional competente del Mar. Para obtener más información, es aconsejable consultar el apartado 5.b. Obtener un certificado de reconocimiento para los nacionales de la UE o del EEE para un ejercicio permanente (LE)."

Si, al revisar el expediente, el Director Interregional del Mar constata que existen diferencias sustanciales entre la formación profesional y la experiencia del nacional y las necesarias para operar en un buque con pabellón medidas de compensación ("5.b. Bueno saber: medida de compensación")

*Para ir más allá*: Artículos 4 a 5 de la orden de 8 de febrero de 2010.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Para servir como el oficial a cargo de la vigilancia del puente, la persona debe:

- Respetar las condiciones morales
- justifican que no se registra ninguna referencia al desempeño de sus funciones en el segundo boletín de su historial penal, a saber, una sentencia penal o una sentencia correccional.

Estas condiciones morales se cumplen si la UE o el EEE nacional produce:

- un extracto de menos de tres meses de sus antecedentes penales del Estado de la UE o del EEE de conformidad con las disposiciones de los convenios internacionales en vigor,
- o un certificado de menos de tres meses del Estado miembro que certifique el cumplimiento de estas condiciones. Un modelo de certificación se establece mediante un decreto conjunto del Ministro del Mar y del Ministro de Justicia.

*Para ir más allá*: Artículo L. 5521-4 del Código de Transportes y Artículo 8 y siguientes del Decreto 24 de junio de 2015.

4°. Seguro
-------------------------------

El funcionario, que ejerce su profesión como empleado, está cubierto por el seguro de responsabilidad profesional contratado por su empleador, para los actos realizados en ocasiones.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

**Autoridad competente**

El Director Interregional del Mar en la región administrativa en la que el oficial desea realizar la realización o en la que se encuentra el puerto de armamento del buque pesquero, es competente para decidir sobre la declaración. Confirmará la recepción de la solicitud en el plazo de un mes a partir de la recepción del expediente.

**Renovación de la predeclaración**

La declaración debe renovarse una vez al año y en caso de que se produzca un cambio en la situación del nacional.

**Documentos de apoyo**

La solicitud para ejercer en Francia, de forma temporal y casual, es un archivo que implica:

- Una declaración escrita firmada por el nacional;
- Una pieza de identificación válida para el nacional;
- un certificado del Estado de la eu o de la autoridad competente del EEE que certifique que el nacional está legalmente establecido en ese Estado y no incurre en ninguna prohibición de ejercer;
- Un certificado que justifique las cualificaciones profesionales del nacional;
- un certificado que justifique su actividad durante al menos dos años en los últimos diez años, cuando ni la formación ni el ejercicio de la profesión están regulados en el Estado miembro;
- Un certificado que justifica que se cumplen las condiciones morales;
- Un certificado de aptitud física para navegar
- un certificado de dominio de las habilidades del lenguaje.

**hora**

El servicio puede comenzar siempre y cuando no haya oposición de la Dirección Interregional del Mar:

- La expiración de un plazo de un mes a partir de la solicitud de declaración;
- en caso de una solicitud de información adicional o verificación de cualificaciones profesionales, al final de un período de dos meses a partir de la recepción de la solicitud completa.

**Tenga en cuenta que**

En caso de solicitud de acceso parcial a la profesión, el nacional deberá adoptar las mismas medidas que para el ejercicio de la actividad de forma temporal o ocasional en territorio francés.

**Costo**

Gratis.

*Para ir más allá*: Artículos 7-2 a 9 de la orden del 8 de febrero de 2010.

**Bueno saber: medida de compensación**

Para obtener permiso para ejercer, el interesado puede estar obligado a someterse a una prueba de aptitud si parece que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de las ejercer la profesión en Francia.

La prueba de aptitud debe establecer que se dominan los conocimientos y cualificaciones pertinentes.

*Para ir más allá*: Artículo 10 de la orden de 8 de febrero de 2010.

### b. Obtener un certificado de reconocimiento para los nacionales de la UE o del EEE para un ejercicio permanente (LE)

**Autoridad competente**

El Director Interregional del Mar, situado en la región administrativa del puerto de armamento del buque pesquero, es competente para expedir el certificado de reconocimiento que autoriza el ejercicio permanente del oficial en Francia.

**Procedimiento**

La solicitud de certificado de reconocimiento se dirige por cualquier medio a la autoridad competente de la región administrativa en la que desea resolver. En caso de falta de documento, la autoridad competente dispone de un mes a partir de la recepción del expediente para informar al nacional.

**Documentos de apoyo**

Para llevar a cabo la profesión de funcionario en Francia de forma permanente, el interesado deberá presentar un expediente completo que contenga:

- El formulario[Cerfa No.14750](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14750.do) Debidamente cumplimentado y firmado;
- Una identificación válida
- un certificado de experiencia profesional expedido por la autoridad competente del Estado miembro, cuando la profesión no esté regulada en dicho Estado;
- cuando así lo solicite la autoridad competente, si fuera necesario, el programa de formación que conduzca a la expedición del título;
- Un certificado que justifica que se cumplen las condiciones morales;
- Un certificado de aptitud física para navegar
- un certificado de dominio de las habilidades del lenguaje.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Resultado del procedimiento**

La autoridad competente dispone de un mes para decidir sobre la solicitud de certificación, tan pronto como se reciba el expediente completo.

Cualquier decisión, ya sea aceptación, denegación o indemnización de res, debe estar justificada.

El silencio guardado al final de un período de dos meses valdrá la pena la decisión de rechazar la solicitud de reconocimiento.

Si se acepta la decisión, la autoridad competente emite el certificado de reconocimiento, que tiene un plazo de validación de cinco años.

**Costo**

Gratis.

*Para ir más allá*: Artículos 2 a 4-1, y artículo 7 de la orden de 8 de febrero de 2010.

**Bueno saber: medidas de compensación**

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a la medida de compensación de su elección, ya sea un curso de ajuste o una prueba de aptitud, realizada en el plazo de seis meses a partir de la decisión de autoridad competente.

*Para ir más allá*: Artículos 5 a 5-2 de la orden del 8 de febrero de 2010.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

