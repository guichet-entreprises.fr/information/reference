﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP046" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Biólogo médico" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="biologo-medico" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/biologo-medico.html" -->
<!-- var(last-update)="2020-04-15 17:21:16" -->
<!-- var(url-name)="biologo-medico" -->
<!-- var(translation)="Auto" -->


Biólogo médico
==============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:16<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La biología médica es una disciplina médica que consiste, a través de un examen médico específico, de diagnosticar y prevenir las patologías de los pacientes y asegurar su seguimiento terapéutico.

El especialista médico (llamado biólogo médico) realiza este examen en un laboratorio de biología médica.

*Para ir más allá*: Artículo L. 6211-1 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ser un biólogo médico, el profesional debe:

- estar profesionalmente cualificados para ejercer como médico o farmacéutico y celebrar:- especialidad en biología médica (ver más abajo "Diploma Especializado en Biología Médica (DESBM)"),
  - un título en biología médica expedido por el Colegio Nacional de Médicos, el Colegio Nacional de Farmacéuticos o el Ministro de Defensa para Profesionales Militares,
  - una autorización de ejercicio individual expedida por el Ministro responsable de la salud en las condiciones del artículo L. 4111-2 del Código de Salud Pública;
- estar en el Colegio de Médicos o Farmacéuticos.

Para saber cómo formarse para convertirse en farmacéutico o médico, y cómo inscribirse en el Colegio de Estas Profesiones, es aconsejable consultar las tarjetas "Farmacéutico" y "Médico Calificado en Medicina General".

Además, algunos profesionales pueden tener su área de especialización reconocida (ver infra "Reconocimiento de un área de especialización").

*Para ir más allá*: Artículos L. 6213-1 a L. 6213-6-1 del Código de Salud Pública.

#### Entrenamiento

**Diplomado en Biología Médica (DESBM)**

Para ejercer como biólogo médico, el profesional debe tener el DES "Biología Médica".

Esta capacitación está disponible para los estudiantes de medicina que han aprobado con éxito las Pruebas Dentadas Nacionales (NCA) y para los estudiantes de farmacia que han pasado el concurso de pasantías.

Este diploma permite al futuro especialista adquirir los conocimientos y la práctica necesarios para llevar a cabo sus funciones.

Esta formación consta de ocho semestres, de los cuales al menos tres están en prácticas con supervisión universitaria y al menos uno en un lugar de prácticas sin supervisión universitaria.

**Tenga en cuenta que**

Como parte de su proyecto profesional, el futuro biólogo médico puede solicitar una formación transversal especializada (TSF) entre las siguientes:

- bioinformática médica;
- genética y medicina molecular bioclínica;
- hematología bioclínica;
- higiene - prevención de infecciones, resistencia;
- medicina y biología reproductiva - andrología;
- Nutrición aplicada
- Farmacología médica/terapéutica;
- terapia celular/transfusión.

La formación del DES se divide en tres fases: la fase base, la fase de profundización y la fase de consolidación.

**La fase base**

Con una duración de cuatro semestres, permite al candidato adquirir los conocimientos básicos de la especialidad elegida y realizar:

- tres pasantías en un lugar acreditado senior en biología médica y deben abarcar los campos de la bioquímica molecular, la hematología y la bacteriología-virología;
- una pasantía en un lugar con licencia senior en biología médica y en un campo diferente de la pasantía anterior.

**La fase de profundización**

Con una duración de dos semestres, permite al candidato adquirir los conocimientos y habilidades necesarios para practicar la especialidad elegida y completar dos pasantías en un lugar aprobado como director de biología médica y beneficiarse de la acreditación. para esta área.

**La fase de consolidación**

Con una duración de dos semestres, permite al candidato consolidar sus conocimientos y realizar:

- una pasantía de un semestre en un lugar con licencia de alto nivel en biología médica;
- una pasantía de un semestre en un lugar con acreditación funcional en biología médica o una principalmente acreditada en otra especialidad y secundaria en biología médica.

*Para ir más allá* : Decreto de 27 de noviembre de 2017 por el que se modifica el Decreto de 12 de abril de 2017 relativo a la organización del tercer ciclo de estudios médicos y al decreto de 21 de abril de 2017 relativo a los conocimientos, habilidades y modelos de formación de los diplomas de estudio y estableciendo la lista de estos diplomas y las opciones especializadas transversales y la formación de los estudios médicos de postgrado.

**Reconocimiento de un área especializada**

También puede ejercer como biólogo médico, el profesional que, a partir del 30 de mayo de 2013:

- practicó biología médica en un centro de salud durante al menos dos años en los últimos 10 años, o que comenzó a ejercer entre el 13 de enero de 2008 y el 13 de enero de 2010 y practicó esta actividad durante dos años antes del 13 de enero de 2012. Sin embargo, el profesional que ha practicado la biología médica en un campo de especialización sólo puede practicar en este campo;
- como veterinario y que, habiendo comenzado una especialización en biología médica antes del 13 de enero de 2010, se adjudicó su especialización antes del 13 de enero de 2016;
- Se desempeñó como Director o Director Adjunto de un Centro Nacional de Enfermedades Transmisibles y fue autorizado a ejercer biología médica por el Ministro responsable de la salud;
- ha sido autorizado por el Ministro responsable de la salud en un ámbito de especialización de su laboratorio;
- ha servido durante al menos tres años como biólogo médico en una estructura o laboratorio de biología médica.

Si es necesario, el profesional tendrá que solicitar el reconocimiento de su área de especialización (ver infra "5 grados. a. Solicitud de reconocimiento de especialización").

Además, el director o subdirector de un centro nacional para el control de las enfermedades transmisibles también tendrá que solicitar una autorización de ejercicio (véase infra "5o. b. Solicitud de autorización para ejercer para el director o subdirector de un centro nacional de enfermedades transmisibles").

*Para ir más allá*: Artículos L. 6213-2 y L. 6213-2-1 del Código de Salud Pública;[Ordenanza No 2010-49](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021683301&dateTexte=20180409) 13 de enero de 2010 en Biología Médica.

#### Costos asociados con la calificación

La formación que conduce al título de médico se paga y el costo varía dependiendo de la institución elegida. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Un nacional de un Estado de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido puede ejercer la misma práctica temporal y ocasional en Francia. sin figurar en el Colegio de Médicos o Farmacéuticos.

Para ello, tendrá que hacer una declaración previa, cuyo carácter depende de su cualificación profesional y de una declaración que justifique que tiene las habilidades linguísticas necesarias para ejercer en Francia.

Para conocer los términos de estas declaraciones anticipadas, es aconsejable referirse a los párrafos "5." Procedimientos y formalidades de reconocimiento de la cualificación" de las tarjetas "Farmacéutico" y "Médico Calificado en Medicina General".

### c. Nacionales de la UE: para un ejercicio permanente (LE)

Un nacional de un Estado de la UE o EEE legalmente establecido que practique como médico o farmacéutico puede llevar a cabo la misma actividad de forma permanente en Francia.

Como tal, el nacional puede beneficiarse, dependiendo de la naturaleza de su cualificación profesional, es decir:

- reconocimiento automático de su diploma médico o farmacéutico. Si es necesario, le será al Colegio verificar la regularidad de su diploma, título o certificación y otorgar el reconocimiento automático para inscribirlo en la Orden de la Profesión en cuestión;
- autorización individual para ejercer. Si es necesario, el profesional tendrá que solicitar permiso. Es aconsejable consultar las tarjetas "Farmacéutico" y "Médico Calificado en Medicina General" para obtener más información.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Cumplimiento del código ético de los médicos o farmacéuticos

El biólogo médico está sujeto al respeto de las normas profesionales aplicables a las profesiones de médico y farmacéutico.

Todas las disposiciones de los códigos de ética de los médicos y farmacéuticos están codificadas en las secciones R. 4127-1 en R. 4127-112 y R. 4235-1 en R. 4235-77 del Código de Salud Pública.

Como tal, el profesional debe respetar los principios de dignidad, no discriminación, secreto profesional o independencia.

*Para ir más allá*: Artículo L. 6213-7 del Código de Salud Pública.

### b. Actividades acumulativas

El profesional sólo puede participar en cualquier otra actividad si tal combinación es compatible con los principios de independencia profesional y dignidad que se le imponen.

Como tal, no puede combinar sus deberes con ninguna otra actividad cercana al campo de la salud.

También está prohibido realizar una función administrativa y utilizarla para aumentar su clientela.

*Para ir más allá*: Artículos R. 4127-26 a R. 4127-27 y R. 4235-4 a R. 4235-23 del Código de Salud Pública.

### c. Obligación para el desarrollo profesional continuo

El biólogo médico como profesional de la salud debe seguir un programa de desarrollo profesional continuo. Este programa mejora la calidad y seguridad de la atención, y mantiene y actualiza el conocimiento.

Toda formación o acciones deben transcribirse en un documento personal que contenga los certificados de formación.

*Para ir más allá* Decreto No 2016-942 de 8 de julio de 2016 sobre la organización del desarrollo profesional continuo de los profesionales de la salud.

4°. Seguros y sanciones
--------------------------------------------

### a. Obligación de seguro

**Obligación de contrato de seguro de responsabilidad civil** Profesional**

Como profesional de la salud, un biólogo médico que practice de manera liberal debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

**Obligación de unirse a un fondo de seguro de vejez o pensiones**

El profesional está obligado a:

- para unirse al Fondo Autónomo de Pensiones de Médicos de Francia (CARMF), si ejerce como médico y está incluido en la junta del Colegio de Médicos;
- para unirse al Fondo de Seguros de Vejez de Farmacéuticos (CAVP, por sus aires), si aparece en la mesa del Colegio de Farmacéuticos.

Para saber cómo son estos procedimientos, puede consultar el[Sitio web de CAVP](https://www.cavp.fr/) Y[Sitio web de CARMF](http://www.carmf.fr/).

**Impuestos después del seguro de salud**

Una vez inscrito en la Orden de su profesión, el profesional que ejercerá en forma liberal debe declarar su actividad en el Fondo de Seguro de Salud Primaria (CPAM).

**Términos**

El registro en el CPAM se puede hacer en línea en el sitio web oficial de Medicare.

**Documentos de apoyo**

El solicitante de registro debe proporcionar un archivo completo que incluya:

- Copiar una identificación válida
- un declaración de identidad bancaria profesional (RIB)
- si es necesario, el título (s) para permitir el acceso al Sector 2.

Para obtener más información, consulte la sección sobre la instalación de médicos en el sitio web del seguro médico.

### b. Sanciones

**Sanciones disciplinarias**

El profesional está obligado, para su inclusión en la lista del orden de su profesión, a comunicar al consejo departamental la orden en virtud de la cual pertenece, todos los contratos y avales relativos a su profesión, así como los relativos al uso del material y las instalaciones en las que opera, si no es el propietario.

En caso de incumplimiento de esta obligación, el profesional incurre en una de las siguientes sanciones disciplinarias:

- Una advertencia
- Culpar
- una prohibición temporal o permanente de la práctica;
- deslistar del gráfico de óseos.

*Para ir más allá*: Artículos L. 6241-1; L. 4124-6 y L. 4234-6 del Código de Salud Pública.

**Sanciones penales**

Un profesional que trabaja como biólogo médico sin estar profesionalmente cualificado se enfrenta a una pena de un año de prisión y una multa de 15.000 euros por usurpación de título.

Además, la práctica ilegal de la profesión de biólogo médico se castiga con una pena de dos años de prisión y una multa de 30.000 euros.

*Para ir más allá*: Artículos L. 6242-1 y L. 6242-2 del Código de Salud Pública.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de reconocimiento de la especialización

**Autoridad competente**

El profesional deberá presentar su solicitud al Centro Nacional de Gestión mediante carta recomendada con notificación de recepción en:

Centro Nacional de Gestión, Departamento de Competencia, Autorizaciones de Ejercicio, Movilidad y Desarrollo Profesional
21 B, rue Leblanc, 75737 Paris Cedex 15

**Documentos de apoyo**

Su solicitud debe incluir los siguientes documentos:

- Se prevé una carta de solicitud motivada que se adenda al área de especialización;
- Una fotocopia de un documento de identidad válido
- Un CV detallado y las publicaciones de cualquier solicitante
- Una copia de todos sus diplomas
- un certificado que justifique que el nacional ha ejercido biología médica durante al menos dos años en los últimos diez años y antes del 13 de enero de 2012;
- para el subdirector de un centro nacional de enfermedades transmisibles, su doctorado en la práctica o universidad, o su título de ingeniería en relación con la biología médica;
- para el médico o farmacéutico que interactúe en un hospital o universidad, un documento que justifique que ha estado practicando bien en una institución de este tipo durante al menos tres años y mencionando su situación.

**Tiempo y procedimiento**

El Centro Nacional de Gestión confirma la recepción de la solicitud en el plazo de un mes a partir de la recepción y la envía a la Comisión Nacional de Biología Médica. Si el solicitante no es respondido en el plazo de cuatro meses a partir de la recepción del expediente completo, se le concede el reconocimiento.

*Para ir más allá*: Artículo R. 6213-2 del Código de Salud Pública; orden de 14 de febrero de 2017 por la que se establece la composición del expediente que se facilitará a la Comisión Nacional de Biología Médica en virtud del artículo L. 6213-12 del Código de Salud Pública y se definen las áreas de especialización mencionadas en el artículo R. 6213-1 del mismo Código.

### b. Solicitud de autorización para ejercer para el director o subdirector de un centro nacional de enfermedades transmisibles

**Autoridad competente**

El profesional debe presentar su solicitud al Centro Nacional de Gestión mediante carta recomendada con notificación de recepción.

**Documentos de apoyo**

Su solicitud debe incluir:

- Una carta de solicitud motivada
- Una fotocopia de un documento de identidad válido
- Un currículum que describe su formación profesional y, si es necesario, todas sus publicaciones;
- Una copia de su solicitud de permiso para desempeñar las funciones de director o subdirector de laboratorio;
- una fotocopia de todos sus diplomas, títulos o certificaciones en el campo de la biología médica.

**Tiempo y procedimiento**

El Centro Nacional de Gestión confirma la recepción de la solicitud en el plazo de un mes a partir de la recepción. La falta de respuesta al Ministro de Salud más allá de cuatro meses es la razón del rechazo de la solicitud de licencia.

*Para ir más allá* R. 6213-5 a R. 6213-7 del Código de Salud Pública; pedido a partir del 14 de febrero de 2017 supra; V de la Sección 9 de la Ordenanza No 2010-49 de 13 de enero de 2010 relativa a la biología médica.

### c. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

