﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP167" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Agente no exclusivo en servicios bancarios y de pago" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="agente-no-exclusivo-en-servicios" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/agente-no-exclusivo-en-servicios-bancarios-y-de-pago.html" -->
<!-- var(last-update)="2020-04-15 17:21:10" -->
<!-- var(url-name)="agente-no-exclusivo-en-servicios-bancarios-y-de-pago" -->
<!-- var(translation)="Auto" -->


Agente no exclusivo en servicios bancarios y de pago
====================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:10<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El agente no exclusivo en servicios bancarios y de pago ("MNEOBSP") es un intermediario cuya actividad consiste en presentar, proponer o asistir en la celebración de transacciones bancarias o servicios de pago. Su misión es llevar a cabo el trabajo y proporcionar asesoramiento preparatorio para ayudar en la implementación de estas operaciones y servicios.

MNEOBSP opera bajo uno o más mandatos emitidos por una o más instituciones financieras como entidades de crédito, empresas financieras, instituciones de pago o instituciones monetarias. servicios de pago.

Como parte de sus funciones, MNEOBSP también puede estar obligado a proporcionar recomendaciones personalizadas sobre las transacciones relacionadas con contratos de crédito inmobiliario[Artículo L. 313-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000032222205&dateTexte=&categorieLien=cid) Código Monetario y Financiero.

*Para ir más allá*: Artículos L. 519-1 a L. 519-2 del Código Monetario y Financiero (CMF).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Cualquier persona que desee ejercer la profesión de MNEOBSP debe justificar una capacidad profesional en operaciones bancarias y servicios de pago cuyo nivel está determinado por el Registro único de intermediarios en Seguros, Banca y Finanzas (Orias ).

Así, el profesional que desee llevar a cabo la actividad de MNEOBSP como principal, tendrá que justificar un nivel II-IOBSP.

Cuando la actividad de MNEOBSP se lleve a cabo sólo como un imprevisto, un nivel III-IOBSP cumplirá las condiciones.

Por último, si se le exige que ofrezca contratos de crédito inmobiliario, debe haber completado una formación de nivel III-CI.

Para obtener más información sobre los niveles de capacidad profesional en operaciones bancarias y servicios de pago, es aconsejable visitar el[Orias](https://www.orias.fr/documents/10227/28915/2017-01-25%20Liste%20des%20pieces%20a%20joindre%20MOBSP.pdf).

#### Entrenamiento

El nivel I-IOBSP se alcanza cuando la persona está justificada:

- o un grado sancionando la educación superior de un nivel de educación III según la nomenclatura de los niveles de formación en una de las siguientes áreas: Economía, Derecho y Ciencias Políticas, Finanzas, Banca, Seguros e Inmobiliaria, Contabilidad y Administración
- o un diploma emitido por una de las escuelas de negocios y gestión que figuran en un[lista determinada por el Ministro de Educación Superior](https://www.orias.fr/documents/13705/16521/BO%20Special%20du%2012%20mars%2020015_Master.pdf) ;
- es decir, un año de experiencia ejecutiva en servicios bancarios o de pago en los tres años anteriores al registro en Orias;
- dos años de experiencia en puestos relacionados con transacciones bancarias o servicios de pago, en los cinco años anteriores al registro en Orias;
- una pasantía profesional de 80 horas adaptada a los productos realizados:- con una entidad de crédito, una entidad de pago, una compañía de seguros, una sociedad financiera o una institución de dinero electrónico que preste servicios de pago,
  - o a una organización de formación elegida por la persona, su empleador o su director.

Sin embargo, si el interesado deseara llevar a cabo la actividad de MNEOBSP únicamente como medio incidental, tendría que justificar:

- o un título sancionando la educación superior de un nivel de educación III en uno de estos campos: Economía, Derecho y Ciencias Políticas, Finanzas, Banca, Seguros e Inmobiliaria, Contabilidad y Gestión;
- es decir, un diploma expedido por una de las escuelas de negocios y de gestión en la misma lista del ministro encargado de la educación superior mencionada anteriormente;
- seis meses de experiencia profesional en puestos relacionados con operaciones bancarias o servicios de pago, en los dos años anteriores al registro en Orias;
- una pasantía de duración suficiente:- ya sea con una entidad de crédito, una entidad de pago o una compañía de seguros,
  - o a una organización de formación elegida por la persona, su empleador o su director.

En el caso de que el individuo proponga contratos de préstamos inmobiliarios, deberá poder justificar el seguimiento de una formación de nivel III-CI:

- o un título sancionando la educación superior de un nivel de educación III en uno de estos campos: Economía, Derecho y Ciencias Políticas, Finanzas, Banca, Seguros e Inmobiliaria, Contabilidad y Gestión;
- es decir, un diploma expedido por una de las escuelas de negocios y de gestión en la misma lista del ministro encargado de la educación superior mencionada anteriormente;
- o un año de experiencia como ejecutivo en funciones relacionadas con el desarrollo, propuesta o adjudicación de contratos de crédito inmobiliario en los tres años anteriores al registro en Orias;
- tres años de experiencia en funciones relacionadas con el desarrollo, propuesta o adjudicación de contratos de crédito inmobiliario en los diez años anteriores al registro en Orias;
- o una pasantía profesional mínima de 40 horas, cuyo programa está programado en el[anexo del decreto de 9 de junio de 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032675834&dateTexte=20171102). Al final de esta pasantía, se presentará a la persona una verificación de competencia en forma de cuestionarios de opción múltiple o preguntas cortas. El examen tendrá éxito si la persona ha respondido poco más del 70% de las preguntas.

**Qué saber**

La formación o la experiencia laboral en un Estado de la UE o del EEE deben completarse con un curso de ajuste de tres meses con un banco y un intermediario de servicios de pago, una entidad de crédito o una entidad de crédito. financiación en Francia. Durante el curso, la formación profesional tendrá que ser seguida durante 28 horas.

*Para ir más allá*: Artículos R. 519-9 y R. 519-10 del Código Monetario y Financiero.

#### Costos asociados con la calificación

La formación para obtener uno de los diplomas que conducen a la profesión de MNEOBSP da sus frutos. Para más información, es aconsejable acercarse a los establecimientos que lo emiten.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (Servicio Gratuito)

Todo nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo del Espacio Económico Europeo (EEE), establecido y que lleve a cabo legalmente las actividades de MNEOBSP en uno de estos Estados, podrá llevar a cabo la misma actividad de forma temporal o Francia.

Sin embargo, esta facultad sólo está abierta a los nacionales que ofrecen contratos de crédito inmobiliario como parte de su actividad MNEOBSP.

El interesado simplemente tendrá que informar previamente al Registro de su Estado, que velará por que la información se transmita al propio Registro Francés (véase infra "5o). a. Informar al Registro único del Estado de la UE o del EEE"). Este último procederá a su registro en Francia, a la espera de haber asegurado que los principales para los que trabaja la persona puedan operar en Francia.

**Tenga en cuenta que**

Además de las medidas para comunicar su información al Registro único francés a través del Registro de su Estado de origen, el nacional también tendrá que someterse a una formación de 14 horas que conduzca a la obtención de un módulo especializado en materia de crédito. Inmobiliaria.

*Para ir más allá*: Artículos L. 519-9, R. 519-4 y R. 519-11-2 del Código Monetario y Financiero;[decreto de 9 de junio de 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032675834&categorieLien=id).

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Cualquier nacional de un Estado de la UE o del EEE, establecido y jurídicamente operativo MNEOBSP en uno de estos Estados, podrá llevar a cabo la misma actividad de forma permanente en Francia.

Sin embargo, esta facultad sólo está abierta a los nacionales que ofrecen contratos de crédito inmobiliario como parte de su actividad MNEOBSP.

El interesado simplemente tendrá que informar previamente al Registro de su Estado, que velará por que la información se transmita al propio Registro Francés (véase infra "5o). a. Informar al Registro único del Estado de la UE o del EEE"). Este último procederá a su registro en Francia, a la espera de haber asegurado que los principales para los que trabaja la persona puedan operar en Francia.

**Tenga en cuenta que**

Además de las medidas para comunicar su información al Registro único francés a través del Registro de su Estado de origen, el nacional también tendrá que someterse a una formación de 14 horas que conduzca a la obtención de un módulo especializado en materia de crédito. Inmobiliaria.

*Para ir más allá*: Artículos L. 519-9, R. 519-4 y R. 519-11-2 del Código Monetario y Financiero;[decreto de 9 de junio de 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032675834&categorieLien=id).

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

MNEOBSP debe comportarse con lealtad y actuar en el mejor interés de sus clientes. También está obligado a cumplir con las condiciones de honor que consten a la profesión y, en particular, a no haber sido condenado por menos de diez años por delitos, blanqueo de dinero, corrupción o evasión fiscal, o de las funciones de funcionario público o ministerial.

Además, el profesional tiene la obligación de informar y asesorar a sus clientes; deberá comunicar de forma clara y precisa cualquier información relacionada con las transacciones bancarias y los servicios de pago.

También existe una obligación de lealtad al profesional en el contexto de los mandatos que lo vinculan a entidades de crédito, sociedades financieras o entidades de pago.

*Para ir más allá*: Artículos L. 500-1, L. 519-3-3 y R. 519-19 del Código Monetario y Financiero.

4°. Requisito de inscripción y seguro
----------------------------------------------------------

### a. Solicitar inscripción en el registro de Orias

Aquellos que deseen ejercer en Francia en la profesión de MNEOBSP que ofrezcan contratos de crédito inmobiliario deben estar inscritos en el Registro Único de Intermediarios de Seguros, Bancarios y Financieros (Orias).

**Documentos de apoyo**

El registro está precedido por el[abrir una cuenta](https://www.orias.fr/espace-professionnel) Orias y el envío de un archivo que contiene todos los siguientes documentos justificativos:

- Una copia del documento de identidad de la persona o un extracto de KBis de menos de tres meses si la persona está inscrita en el registro de comercio y empresas;
- un documento que justifique su capacidad profesional como se especifica en el párrafo "2." a. Entrenamiento":- o una copia completa del folleto de prácticas,
  - ya sea un certificado de funciones relacionadas con el desarrollo, la propuesta o la adjudicación de contratos de crédito inmobiliario, cuando el interesado sea nacional de la UE o del EEE,
  - ya sea un certificado de funciones relacionadas con la realización de transacciones bancarias o servicios de pago,
  - es decir, un certificado del curso de ajuste de tres meses, complementado por un curso de formación de 28 horas cuando la experiencia se adquiere en un estado de la UE o del EEE,
  - ya sea una copia del diploma, certificado o título expedido por un Estado de la UE o del EEE y reconocido como equivalente a diplomas y títulos en una de las clasificaciones de la Nomenclatura de Formación especializada 122, 128, 313 o 314;
- cualquier documento que acredite la existencia de uno o más mandatos con una o más entidades de crédito;
- En caso de cobro de fondos, un certificado de garantía financiera;
- Liquidación de las tasas de registro
- transacciones bancarias propuestas.

**Qué saber**

Los documentos justificativos deben estar escritos en francés o traducidos por un traductor certificado, si es necesario.

**Renovación del registro**

El procedimiento de inscripción en orias debe renovarse cada año y con cualquier cambio en la situación profesional de la persona. En este último caso, el MNEOBSP deberá mantener informados a los Orias un mes antes del cambio o en el plazo de un mes a partir del evento de modificación.

La solicitud de renovación debe tener lugar antes del 31 de enero de cada año y debe incluir:

- Liquidación de las tasas de registro
- en caso de cobro, un certificado de garantía financiera que cubra el período comprendido entre el 1 de marzo del año N y el 28 de febrero del año No. 1.

**Qué saber**

Las plantillas de certificado están disponibles directamente en el[Orias](https://www.orias.fr/banque1).

**Costo**

La cuota de inscripción se fija en 30 euros pagaderos directamente en línea en el sitio web de Orias. En caso de impago de estos gastos, orias envía una carta informando al interesado de que dispone de un plazo de treinta días a partir de la fecha de recepción del correo para abonar la suma.

No liquidar esta cantidad:

- No se tendrá en cuenta la solicitud de primer registro en orias;
- MNEOBSP se eliminará del registro cuando se trata de una solicitud de renovación.

**Excepción**

El MNEOBSP, que participa en esta actividad como un hecho incidental y no supera los siguientes umbrales de actividad por año, está exento de inscripción en Orias:

- para transacciones bancarias o servicios de pago, el número total de 20 transacciones;
- 200.000 euros de créditos concedidos o servicios de pago prestados o realizados por el MNEOBSP.

*Para ir más allá*: Artículos L. 546-1, L. 546-2 y R. 519-2 del Código Monetario y Financiero; 1 de marzo de 2012.

### b. Obligación de contrato de seguro de responsabilidad civil profesional

El interesado, que presta servicio a uno o más términos, se beneficia del seguro de responsabilidad profesional de sus electores que cubre los riesgos financieros asociados con su actividad habitual de MNEOBSP.

*Para ir más allá*: Artículos L. 519-3-4 y R. 519-16 del Código Monetario y Financiero.

### c. Obligación de sacar un seguro de garantía financiera

Cualquier persona que practice la profesión de MNEOBSP, siempre y cuando cobre fondos, incluso de forma casual, está obligada a sacar un seguro de garantía financiera, asignado al reembolso de estos fondos a sus asegurados.

El importe mínimo de la garantía financiera debe ser de al menos 115.000 euros, y no puede ser inferior al doble del importe medio mensual de los fondos recaudados por el MNEOBSP, calculado sobre la base de los fondos recaudados en los últimos doce meses anteriores al mes fecha de suscripción o renovación del compromiso de fianza.

*Para ir más allá*: Artículo R. 519-17 del Código Monetario y Financiero.

### d. Obligación de someterse a formación profesional continua

Los MNEOBSP que desempeñan funciones relacionadas con los contratos de crédito inmobiliario deben recibir formación profesional continua para actualizar sus conocimientos y aptitudes, teniendo en cuenta los cambios en la legislación o Reglamentos.

Esta formación, que tiene una duración mínima de 7 horas, debe completarse cada año.

### e. Sanciones penales

Toda persona interesada en cumplir una de las obligaciones previstas en los apartados 3 y 4 será sancionada con una pena de dos años de prisión o una multa de 6.000 euros, o dos penas acumulativas.

El hecho de que el interesado cree confusión o haga parecer que está inscrito en el registro de Orias en otra categoría se castiga con una pena de tres años de prisión y una multa de 375.000 euros, o una de las dos penas.

*Para ir más allá*: Artículos L. 546-3 y L. 546-4 del Código Monetario y Financiero.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

**Qué saber**

Los procedimientos y formalidades de reconocimiento de cualificación se aplican únicamente a los nacionales de la UE o del EEE que ofrecen contratos de crédito inmobiliario como parte de su negocio MNEOBSP.

### a. Informar al Registro único del Estado de la UE o del EEE

**Autoridad competente**

Un nacional de un Estado miembro de la UE o del EEE que haya estado activo en ese Estado y que desee ejercer el autoservicio o el autoestablecimiento en Francia debe informar en primer lugar al registro único de su Estado.

**Procedimiento**

El nacional deberá facilitar la siguiente información al Registro único de su estado:

- Su nombre, dirección y, en su caso, número de registro;
- El Estado miembro en el que desee llevar a cabo su actividad en caso de servicio gratuito o de establecerse en caso de establecimiento libre;
- La categoría de intermediario bancario al que pertenece, a saber, la de MNEOBSP;
- ramas pertinentes, si es necesario.

**hora**

El Registro único del Estado de la UE o del EEE, al que el nacional ha notificado su intención de operar en Francia, dispone de un mes para facilitar a los Orias la información que le conceda.

El nacional podrá iniciar su actividad MNEOBSP en Francia en el plazo de un mes después de que el Registro único de su Estado le informara de la comunicación realizada a orias.

*Para ir más allá*: Artículos L. 519-9 del Código Monetario y Financiero y[32 de la Directiva 2014/17/UE de 4 de febrero de 2014](http://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014L0017).

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

