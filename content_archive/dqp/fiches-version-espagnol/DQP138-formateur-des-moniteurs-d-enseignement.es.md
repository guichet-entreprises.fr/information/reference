﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP138" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Seguridad vial" -->
<!-- var(title)="Entrenador de instructores de conducción de vehículos de motor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="seguridad-vial" -->
<!-- var(title-short)="entrenador-de-instructores-de-conduccion" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/seguridad-vial/entrenador-de-instructores-de-conduccion-de-vehiculos-de-motor.html" -->
<!-- var(last-update)="2020-04-15 17:22:26" -->
<!-- var(url-name)="entrenador-de-instructores-de-conduccion-de-vehiculos-de-motor" -->
<!-- var(translation)="Auto" -->


Entrenador de instructores de conducción de vehículos de motor
==============================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:26<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El instructor de conducción de vehículos de motor es un profesional responsable de la formación de profesores de conducción en diferentes categorías de vehículos (vehículos de motor, bicicletas, motocicletas o vehículos pesados).

De conformidad con lo dispuesto en la sección D. 114-12 del Código de Relaciones Públicas y de la Administración, cualquier usuario podrá obtener un certificado informativo sobre las normas aplicables a las profesiones relacionadas con la enseñanza de la conducción costosa y la concienciación sobre la seguridad vial. Para ello, debe enviar la solicitud a la siguiente dirección: bfper-dsr@interieur.gouv.fr.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El profesor de conducción y seguridad vial, que desea convertirse en formador de instructores, debe tener una licencia de enseñanza y obtener un diploma estatal específico para la formación de instructores.

Para obtener una licencia de enseñanza, el profesional debe cumplir las siguientes condiciones:

- Ser una licencia de conducir válida con un período de prueba expirado, válido para las categorías de vehículos que desea enseñar a conducir;
- poseer un título o diploma como profesor de conducción y seguridad vial (véase infra "2". Diploma de profesor en conducción y seguridad vial");
- Tener al menos 20 años de edad
- cumplir con los requisitos de aptitud física, cognitiva y sensorial requeridos para las licencias de conducir en las categorías C1, C, D1, D, C1E, CE, D1E y DE. Para ello, la persona tendrá que obtener el consejo de un oficial médico.

Una vez que cumplan con estas condiciones, tendrán que solicitar permiso administrativo para enseñar (véase más abajo "5. a. Solicitar permiso para enseñar").

Con el fin de convertirse en un instructor instructor, el profesional también debe tener un certificado de aptitud para capacitar a los instructores en la instrucción de conducción de vehículos de motor terrestres (BAFM).

*Para ir más allá*: Artículo L. 212-1 de la Ley de Tráfico de Carreteras.

#### Entrenamiento

**Diploma de profesor en conducción y seguridad vial**

Para ser profesor de conducción y seguridad vial, el profesional debe poseer uno de los siguientes diplomas o títulos:

- El título profesional de profesor de conducción y seguridad vial;
- El certificado para la práctica de la conducción y la seguridad vial (BEPECASER) se impartió antes del 31 de diciembre de 2016.

Para la enseñanza de la conducción de vehículos de motor terrestres en la categoría de licencia de conducir, B, B1 y BE:

- Certificado de aptitud profesional para enseñar vehículos de motor terrestres (CAPEC),
- tarjeta profesional y certificado de aptitud profesional y pedagógica (CAPP),
- títulos o diplomas militares definidos en el orden de 13 de septiembre de 1996 por el que se establece la lista de diplomas militares reconocidos como equivalentes al certificado para la práctica de la enseñanza de la conducción y la seguridad vial,
- la conducción de diplomas de educación emitidos por las comunidades extranjeras y Nueva Caledonia.

*Para ir más allá*: Artículo R. 212-3 de la Ley de Tráfico de Carreteras.

**Certificado de aptitud para entrenar instructores de conducción (BAFM)**

Para obtener este diploma, el profesional debe aplicar un[Forma](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E1519F08621A0D1B2597D65D131F5A77.tplgfr41s_1?idArticle=LEGIARTI000036496244&cidTexte=LEGITEXT000006075067&dateTexte=20180424) registro para el examen, completado y firmado, así como los documentos justificativos mencionados, al prefecto encargado del centro de examen desde el que desea presentarse.

La lista de departamentos adscritos a cada uno de los centros de examen se establece en el[Apéndice III](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E1519F08621A0D1B2597D65D131F5A77.tplgfr41s_1?idArticle=LEGIARTI000036496276&cidTexte=LEGITEXT000006075067&dateTexte=20180424) del auto de 23 de agosto de 1971 sobre el certificado de aptitud para formar instructores en la conducción de vehículos de motor a tierra.

El examen de este diploma consiste en pruebas de elegibilidad escritas y prácticas orales y de admisión para el[Anexo](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E1519F08621A0D1B2597D65D131F5A77.tplgfr41s_1?idArticle=LEGIARTI000028907015&cidTexte=LEGITEXT000006075067&dateTexte=20180424) I de la orden del 23 de agosto de 1971 antes mencionada.

**Tenga en cuenta que**

El profesional puede ser excusado de tomar pruebas de elegibilidad por escrito si cumple con los siguientes requisitos:

- un diploma de profesor de conducción de menos de un año de edad en la fecha de las pruebas de elegibilidad;
- un título nacional de posgrado o que justifique haber sido profesor durante al menos cinco años en una institución educativa general, técnica o agrícola (secundaria o superior).

*Para ir más allá* : orden de 23 de agosto de 1971 relativo al certificado de aptitud para formar instructores que impartan la conducción de vehículos de motor terrestres.

#### Costos asociados con la calificación

Se paga la capacitación que conduce al BAFM, el costo varía dependiendo de los centros de examen apropiados. Es aconsejable acercarse a los centros de examen pertinentes para obtener más información.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o de un Estado Parte en el acuerdo del Espacio Económico Europeo (EEE), establecido legalmente y que proactúe la actividad de conducción docente y seguridad vial a los futuros conductores o monitores, pueden realizar la misma actividad de forma temporal e informal en Francia.

Para ello, tendrá que enviar una declaración antes de su primera actuación, al prefecto del departamento en el que desea ejercer (véase infra "5o. b. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE para un ejercicio temporal e informal (LPS)).

Cuando el Estado miembro no regule el acceso a la actividad o a su ejercicio, el profesional deberá justificar haber realizado esta actividad durante al menos un año, en los últimos diez años.

*Para ir más allá*: Artículo L. 212-1 II de la Ley de Tráfico de Carreteras.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Todo nacional de la UE o del EEE, legalmente establecido y que actúe la actividad de conductor y profesor o formador de seguridad vial, podrá llevar a cabo esta actividad de forma permanente, en Francia si lo justifica:

- Poseer un certificado de competencia expedido por un Estado miembro que regule la profesión;
- han estado haciendo esto durante un año en los últimos diez años en un Estado miembro que no regula la formación o el ejercicio de la profesión.

Una vez que el nacional cumple una de estas condiciones, debe solicitar al prefecto del departamento donde planea ejercer (véase infra "5). c. Solicitud de reconocimiento de cualificación para el nacional de la UE para un ejercicio permanente (LE)).

*Para ir más allá*: Artículo R. 212-3-1 de la Ley de Tráfico de Carreteras.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El profesor de conducción y seguridad vial debe cumplir con las condiciones de honor, incluyendo no ser condenado a una sentencia penal o correccional.

El hecho de que un profesional sea un entrenador de instructores de conducción de vehículos de motor sin tener la autorización para enseñar se castiga con un año de prisión y una multa de 15.000 euros. También se le puede impedir la práctica.

*Para ir más allá*: Artículos L. 212-2 y L. 212-4 de la Ley de Tráfico de Carreteras.

4°. Seguro
-------------------------------

El profesional que proactúe a nivel liberal debe disponer de un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. Depende del empresario la apropiación de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar permiso para enseñar

**Autoridad competente**

El prefecto del departamento en el que el nacional desea enseñar es competente para expedir la autorización de enseñanza.

**Documentos de apoyo**

El nacional deberá adjuntar a la solicitud los siguientes documentos justificativos:

- La solicitud de permiso de enseñanza cumplimentada, fechada y firmada;
- Dos identificaciones fotográficas
- Una fotocopia de un documento de identidad
- prueba de residencia de menos de seis meses
- Fotocopia de certificados de competencia o documentos de formación obtenidos para el ejercicio de la actividad docente de conducción;
- Reconocimiento de cualificaciones profesionales;
- Un certificado médico emitido por un médico con licencia
- extracto del historial delictivo o cualquier documento equivalente de menos de tres meses.

Permiso para enseñar, cuyo modelo se establece en el[Anexo](https://www.legifrance.gouv.fr/jo_pdf.do?numJO=0&dateJO=20130117&numTexte=16&pageDebut=01099&pageFin=01100) del Decreto de 10 de enero de 2013 por el que se modifica el decreto de 8 de enero de 2001 relativo a la autorización de enseñanza de un vehículo de motor de tasa y de seguridad vial, se expide por un período de cinco años renovables en las mismas condiciones.

*Para ir más allá*: Artículo R. 212-1 del Código de Carreteras; orden de 8 de enero de 2001 relativa a la autorización para enseñar, por una tasa, la conducción de vehículos de motor y la seguridad vial.

### b. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe presentar su solicitud al prefecto del departamento en el que desea ejercer.

**Documentos de apoyo**

Su solicitud deberá incluir los siguientes documentos, si los hubiere, con su traducción aprobada al francés:

- Una identificación válida
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE para llevar a cabo esta actividad, y que no incurre en ninguna prohibición temporal o permanente de la práctica;
- prueba de sus cualificaciones profesionales
- cualquier documento que justifique que el nacional haya ejercido en un Estado de la UE o del EEE durante un año en los últimos diez años, cuando dicho Estado no regule la formación, el acceso a la profesión solicitada o su ejercicio.

**Tiempo y procedimiento**

Al recibir los documentos justificativos, el prefecto tiene un mes para informarle, es decir:

- Que puede comenzar su prestación de servicio;
- someterlo a una prueba de aptitud si existen diferencias sustanciales entre la formación o la experiencia profesional del nacional y las requeridas en Francia. En este caso, la decisión sobre la autorización de la prestación tendrá lugar en el plazo de un mes a partir del examen;
- para informarle de cualquier dificultad que pueda retrasar su decisión. En este caso, el prefecto podrá tomar su decisión en el plazo de un mes a partir de la resolución de esta dificultad, y a más tardar dos meses después de la notificación al nacional.

**Tenga en cuenta que**

El silencio del prefecto dentro de estos plazos vale el permiso para realizar el servicio.

*Para ir más allá* II y III de la Sección R. 212-2 de la Ley de Tráfico de Carreteras.

### c. Solicitud de reconocimiento de cualificación para el nacional de la UE para un ejercicio permanente (LE)

**Autoridad competente**

El nacional debe presentar su solicitud al prefecto del departamento en el que desea ejercer.

**Documentos de apoyo**

Su solicitud debe incluir:

- Una solicitud de reconocimiento, fechada y firmada;
- Una fotocopia de un documento de identidad
- Una fotografía de identidad de menos de seis meses
- prueba de residencia de menos de seis meses
- Fotocopia de la licencia de conducir de dos lados
- Fotocopia de certificados de competencia o documentos de formación obtenidos para el ejercicio de la actividad docente de conducción;
- Una declaración sobre su conocimiento de la lengua francesa para el ejercicio de la profesión de que se trate;
- en su caso, un certificado de competencia o un documento de formación que justifique que el nacional ha estado ocupado en la actividad durante al menos un año en los últimos diez años, en un estado que no regula el acceso a la profesión o su ejercicio.

**Procedimiento**

El prefecto reconoce la recepción del archivo y tiene un mes para decidir sobre la solicitud.

En caso de diferencias sustanciales entre la formación que ha recibido y la requerida en Francia para ejercer, el prefecto podrá someterlo a una prueba de aptitud o a un curso de adaptación.

La prueba de aptitud, realizada en un plazo máximo de seis meses, es en forma de entrevista ante un jurado. El curso de aptitud de dos meses debe llevarse a cabo en una escuela de conducción.

Después de comprobar los documentos en el expediente y, si es necesario, después de recibir los resultados de la medida de compensación elegida, el prefecto expedirá el certificado de reconocimiento de cualificaciones de acuerdo con el modelo establecido en el[Anexo](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=D3225762F0A0C9E9EA272A874A63EF2F.tplgfr42s_2?cidTexte=JORFTEXT000035589366&dateTexte=20170920) 13 de septiembre de 2017.

*Para ir más allá* : decreto de 13 de septiembre de 2017 relativo al reconocimiento de cualificaciones adquiridas en un Estado miembro de la Unión Europea o en otro Estado parte en el acuerdo sobre el Espacio Económico Europeo por personas que deseen ejercer las profesiones educación vial regulada.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea.

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

