﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP029" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Facilitador de habilidades motoras para educación para la salud deportiva" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="facilitador-de-habilidades-motoras" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/facilitador-de-habilidades-motoras-para-educacion-para-la-salud.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="facilitador-de-habilidades-motoras-para-educacion-para-la-salud" -->
<!-- var(translation)="Auto" -->


Facilitador de habilidades motoras para educación para la salud deportiva
=========================================================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El facilitador deportivo de salud y educación motora es un profesional de la animación deportiva cuya actividad es supervisar y facilitar las actividades físicas y deportivas con el fin de mejorar las habilidades motoras (movimientos voluntarios y automático), la salud y el bienestar de los profesionales. Esta actividad se puede realizar como de costumbre, estacional u ocasional.

*Para ir más allá*: Artículo L. 212-1 del Código del Deporte.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de salud deportiva y facilitador de la educación motora, el profesional debe ser:

- profesionalmente calificados. Para ello, deberá poseer un diploma, un título profesional o un certificado de titulación:- para justificar sus habilidades de seguridad de profesionales y terceros,
  - registrado en el directorio nacional de certificaciones profesionales ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)) ;
- para hacer una declaración de su actividad con el fin de obtener una tarjeta de educador deportivo profesional (ver infra "5o. a. Declaración de actividad para obtener la tarjeta de educador deportivo profesional").

**Tenga en cuenta que**

El profesional que está siendo capacitado para obtener uno de los diplomas calificados también puede realizar esta actividad.

*Para ir más allá*: Artículo L. 212-1 del Código del Deporte.

#### Entrenamiento

Para ser reconocido como un profesionalmente calificado, el profesional debe tener una licenciatura en ciencia y tecnología en actividades físicas y deportivas (STAPS) marcada como "Educación y Habilidades Motoras".

Este curso de tres años es accesible:

- después de un curso de formación como estudiante;
- Aprendizaje
- después de un curso de formación continua;
- profesionalizado;
- individual o a través del proceso de validación de la experiencia (VAE). Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr/).

**Es bueno saber**

La lista de instituciones que imparten esta formación está disponible en el[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

*Para ir más allá*: Artículo A. 212-1 y Apéndice II-1 del Código del Deporte.

#### Costos asociados con la calificación

La formación que conduzca a la profesión de facilitador de deportes de salud y educación motora se paga y su costo varía según el curso previsto. Para obtener más información, es aconsejable acercarse a las instituciones interesadas.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado de la Unión Europea (UE) o un miembro del Acuerdo del Espacio Económico Europeo (EEE) que participe en las actividades de salud deportiva y educación motora podrá realizar la misma actividad de forma temporal e informal. Francia.

Cuando ni el acceso a la actividad ni su ejercicio estén regulados en el Estado miembro, el profesional deberá justificar su ejercicio durante al menos un año en los últimos diez años.

El profesional debe, antes de su primera prestación de servicio, hacer una declaración previa con el prefecto del departamento en el que desea ejercer (ver infra "5o. c. Predeclaración para el nacional de la UE para un ejercicio temporal e informal (LPS)").

*Para ir más allá*: Artículo L. 212-7 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Cualquier nacional de la UE o del EEE libremente establecido que sea un facilitador de la salud y la educación motora podrá llevar a cabo esta actividad en Francia de forma permanente.

Para ello, el nacional deberá:

- Disponer de un certificado de competencia o un certificado de formación que le permita llevar a cabo esta actividad en un Estado miembro que regula su acceso y ejercicio;
- Cuando el Estado miembro no regule el acceso a la actividad o a su formación, justifique haber realizado esta actividad durante al menos un año en los últimos diez años;
- ser titular de un título de formación expedido por un Estado miembro que no regule el acceso a la profesión o su ejercicio que le permita llevar a cabo la totalidad o parte de esta actividad y sancionar la formación reglada consistente en un ciclo de estudio y un experiencia laboral (formación, prácticas o práctica profesional)
- poseer un certificado de formación expedido por un tercer Estado pero admitido en equivalencia en un Estado miembro que regule el acceso a la actividad o a su ejercicio, y justificar la realización de esta actividad durante al menos dos años en dicho Estado miembro.

Una vez que el nacional cumpla una de estas condiciones, tendrá que hacer una declaración previa de su actividad con el prefecto del departamento en el que desea ejercer como principal (véase infra "5o). b. Predeclaración del nacional de la UE para el ejercicio permanente (LE)").

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Nadie puede actuar como facilitador si es o ha sido condenado por un delito criminal o delito menor por:

- un ataque a la persona (integridad física, vida, libertades, peligro);
- extorsión o malversación;
- contra el Estado, la nación y la paz pública;
- Consumo de drogas
- posesión de armas o municiones en violación del Título I del Libro III del Código de Seguridad Interior;
- poner en peligro la salud y la seguridad física o moral de los profesionales;
- El uso de sustancias dopantes
- introducción ilegal de bebidas alcohólicas en un recinto deportivo durante un evento deportivo.

**Tenga en cuenta que**

Si el título de facilitador deportivo o educador se utiliza en la ignorancia de estas disposiciones, el profesional se enfrenta a una pena de un año de prisión y una multa de 15.000 euros.

*Para ir más allá*: Artículos L. 212-9 y L. 212-10 del Código del Deporte.

4°. Sanciones
----------------------------------

El profesional se enfrenta a una pena de un año de prisión y una multa de 15.000 euros si:

- hace negocios sin ser calificado profesionalmente;
- emplea a una persona no cualificada o a un nacional de la UE que desconozca las disposiciones relativas al ejercicio temporal u ocasional y permanente de esta actividad en Francia (véase supra "2o. Cualificaciones profesionales").

*Para ir más allá*: Artículo L. 212-8 del Código del Deporte.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Declaración de actividad para obtener tarjeta de educador deportivo profesional

**Autoridad competente**

El profesional deberá presentar su solicitud al prefecto del departamento en el que desee llevar a cabo su actividad.

**Documentos de apoyo**

Su solicitud debe incluir:

- el[Formulario de informes de ejercicios](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=29D52BEE6C97415E0338E1B83C12C433.tplgfr21s_1?idArticle=LEGIARTI000032674633&cidTexte=LEGITEXT000006071318&dateTexte=20180409) Lista II-12 del Código del Deporte;
- Una copia de su identificación
- Una fotografía de identidad
- Una declaración sobre el honor que certifique la exactitud de la información proporcionada;
- Una copia de todos sus diplomas, títulos o certificados de cualificación;
- si es necesario, una autorización para ejercer, la equivalencia de su diploma o un certificado que justifique que tiene los conocimientos mínimos para ejercer;
- un certificado médico de no contradictorio con la práctica y supervisión de una actividad física o deportiva que data de menos de un año.

**Retrasos y procedimientos**

El prefecto reconoce haber recibido su solicitud en el plazo de un mes. Luego le da una tarjeta de educador deportivo profesional al profesional.

**Tenga en cuenta que**

Esta declaración debe renovarse cada cinco años. Para ello, deberá enviar un certificado médico de no contradictorio presentado durante la declaración de menos de un año de edad.

*Para ir más allá*: Artículos L. 212-11 y R. 212-85, y artículos A. 212-176 y siguientes del Código del Deporte.

### b. Predeclaración del nacional de la UE para el ejercicio permanente (LE)

**Autoridad competente**

El profesional deberá presentar su solicitud al prefecto del departamento en el que desee llevar a cabo su actividad en el cargo principal.

**Documentos de apoyo**

Su solicitud debe incluir:

- el[Formulario de informes de actividad](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=29D52BEE6C97415E0338E1B83C12C433.tplgfr21s_1?idArticle=LEGIARTI000036153612&cidTexte=LEGITEXT000006071318&dateTexte=20180409) modelo en la Lista II-12-2 a. del Código del Deporte y en los documentos justificativos mencionados.

**Procedimiento y plazos**

El prefecto reconoce la recepción de la solicitud en el plazo de un mes a partir de su recepción. Después de reconocer la recepción de su solicitud, el prefecto le emite una tarjeta de educador deportivo profesional indicando las condiciones de su actividad.

**Tenga en cuenta que**

Esta declaración debe ser renovada por el nacional cada cinco años. Para ello, el profesional debe[formulario de solicitud de renovación](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=29D52BEE6C97415E0338E1B83C12C433.tplgfr21s_1?idArticle=LEGIARTI000036153622&cidTexte=LEGITEXT000006071318&dateTexte=20180409) y los documentos justificativos mencionados en este documento.

*Para ir más allá*: Artículos R. 212-88 y A. 212-182 del Código del Deporte.

**Bueno saber: medidas de compensación**

Cuando exista una diferencia sustancial entre la formación recibida por el profesional y la necesaria para llevar a cabo esta actividad en Francia, el prefecto podrá, previa comisión de reconocimiento de cualificación, decidir someterla a una aptitud o un curso de ajuste de hasta tres años.

*Para ir más allá*: Artículo R. 212-90-1 del Código del Deporte.

### c. Predeclaración del nacional de la UE para el ejercicio temporal y casual (LPS)

**Autoridad competente**

El nacional debe solicitar al prefecto del departamento donde desee llevar a cabo la mayor parte de su actividad.

**Documentos de apoyo**

Su solicitud debe incluir:

- el[Formulario de informes de actividad](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=29D52BEE6C97415E0338E1B83C12C433.tplgfr21s_1?idArticle=LEGIARTI000036153628&cidTexte=LEGITEXT000006071318&dateTexte=20180409), rellenado y firmado;
- todos los documentos justificativos mencionados en este formulario.

**Retrasos y procedimientos**

El prefecto reconoce haber recibido su solicitud en el plazo de un mes. A falta de una respuesta del prefecto más allá de dos meses, puede comenzar la libre prestación de servicios.

**Es bueno saber**

En caso de serias dudas sobre el nivel de conocimiento del francés del nacional, el prefecto podrá decidir que se someta a un control para permitir un ejercicio seguro de las actividades físicas y deportivas y verificar su capacidad de alerta Rescate.

**Tenga en cuenta que**

Su declaración debe renovarse anualmente.

*Para ir más allá*: Artículos R. 212-92 a R. 212-94 y A.212-182-2 del Código del Deporte.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

