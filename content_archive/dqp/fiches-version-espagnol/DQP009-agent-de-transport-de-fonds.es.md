﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP009" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Seguridad" -->
<!-- var(title)="Agente de transporte de dinero" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="seguridad" -->
<!-- var(title-short)="agente-de-transporte-de-dinero" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/seguridad/agente-de-transporte-de-dinero.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agente-de-transporte-de-dinero" -->
<!-- var(translation)="Auto" -->


Agente de transporte de dinero
==============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El transportista de fondos o transportador de fondos proporciona transporte seguro para bienes valiosos como billetes, monedas, cheques en blanco, tarjetas bancarias o joyas. Esta ocupación requiere un permiso para portar un arma; acceso y el ejercicio de sus misiones están muy supervisados.

*Para ir más allá*: Artículos L. 611-1 párrafo 2 y R. 613-24 del Código de Seguridad Nacional.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Cualquier persona que desee trabajar como agente de transporte debe demostrar una aptitud profesional y tener una tarjeta profesional emitida por la Junta Local de Acreditación y Control (CLAC) establecida en el Consejo Actividades de Seguridad Nacional (Cnaps). La tarjeta de visita se emite con la condición de que:

- no haber sido condenado a una sentencia correccional o penal registrada en el expediente penal número 2 o para los extranjeros en un documento equivalente, incompatible con el ejercicio de una actividad delictiva Seguridad privada;
- justifican las cualificaciones necesarias para llevar a cabo la actividad de protección física de las personas: seguida de la formación en una organización de formación autorizada por los Cnaps y poseer una certificación profesional registrada en el RNCP, justificar una equivalencia bajo ciertas condiciones (policía, gendarmes, policía municipal, militar);
- para los extranjeros, para tener un permiso de residencia para llevar a cabo una actividad en el territorio nacional;
- no estar sujeto a una orden de expulsión o prohibición del territorio francés actual.

*Para ir más allá* Sección L. 612-20 del Código de Seguridad Nacional.

#### Entrenamiento

La formación profesional para justificar la capacidad profesional de un persona portadora de fondos está sujeta a una autorización expedida por los Cnaps. Esta decisión se produce:

- antes de contratar a una empresa que lleva a cabo fondos. En este caso, el interesado recibirá una**autorización previa para el acceso a la formación** 6 meses, para ser entregados a un centro de formación (autorizado por los Cnaps). Las certificaciones profesionales que proporcionan la capacidad de llevar a cabo la actividad de un transportista de fondos se actualizan regularmente. Están registrados en el RNCP y aparecen en el sitio web de Cnaps;
- pensando en contratar. Una persona que haya celebrado un contrato de trabajo con una empresa de protección privada debe solicitar una**autorización provisional** ser empleado durante 6 meses. Esta autorización no le permite ocupar un puesto como transportista de fondos, pero compromete a la compañía a proporcionarle entrenamiento inmediato para justificar su aptitud para practicar.

**Tenga en cuenta que**

La solicitud de autorización se realiza directamente[en línea](https://depot-teleservices-cnaps.interieur.gouv.fr/autorisation-prealable/identification) en el sitio web de Cnaps.

*Para ir más allá*: Artículos L. 612-22 y L. 612-23 del Código de Seguridad Nacional.

#### Costos asociados con la calificación

Se puede pagar la formación que conduzca a la profesión de transporte de fondos. Para obtener más información, es aconsejable acercarse a las organizaciones de formación que lo dispensan.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

No hay disposiciones para una actividad única u ocasional, ya que no hay planes de emitir tarjetas de visita por períodos cortos de tiempo.

*Para ir más allá*: Artículo R. 612-25 del Código de Seguridad Nacional.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Todo nacional de un Estado de la UE o del EEE establecido y actúe legalmente como agente de transporte de dinero en ese Estado podrá llevar a cabo la misma actividad en Francia de forma permanente.

Tendrá que solicitar una tarjeta profesional, que esté siendo estudiada por la delegación territorial competente y presentada al CLAC territorialmente competente (véase infra "5o). b. Obtener una tarjeta de visita para el nacional de la UE o del EEE para un ejercicio permanente (LE) ").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que está legalmente establecida, la UE o el Nacional del EEE tendrán que justificar la formación en ese estado y experiencia laboral. uno o más Estados de la UE o del EEE al menos un año en los últimos diez años.

Si el examen de las cualificaciones profesionales revela diferencias sustanciales en relación con las necesarias para el acceso a la profesión y su práctica en Francia, el interesado puede estar sujeto a una medida de compensación (véase infra "5o. b. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos L. 612-20 y R. 612-24-1 del Código de Seguridad Nacional.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Tan pronto como la persona actúa como agente de transporte de dinero, no puede:

- Proteger a los buques franceses de amenazas de actos terroristas o adquisiciones;
- buscar información o información destinada a terceros como parte de la actividad de un agente de búsqueda privado;
- monitorear los puntos de vista políticos, filosóficos o religiosos de las personas o las afiliaciones sindicales.

*Para ir más allá*: Artículos L. 611-1, L. 612-2 y L. 612-4 del Código de Seguridad Nacional.

4°. Educación continua y seguros
-----------------------------------------------------

### a. Obligación de someterse a formación profesional continua

La renovación de la tarjeta profesional está sujeta al seguimiento de la formación continua destinada a mantener y actualizar las competencias (MAC) del interesado. Esta formación se imparte en forma de[pasantía](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=B41F0D4C24B7400661708873FBFECD42.tplgfr22s_3?idArticle=JORFARTI000034104608&cidTexte=JORFTEXT000034104578&dateTexte=29990101&categorieLien=id) 14 horas y debe tener lugar dentro de los treinta y seis meses antes de la fecha de caducidad de la tarjeta.

*Para ir más allá*: Artículo L. 622-20-1 del Código de Seguridad Interior y Artículo 10 de la Orden 27 de Febrero de 2017 sobre la Capacitación Continua de Los Oficiales de Seguridad Privada.

### b. Obligación de contrato de seguro de responsabilidad civil profesional

Como profesional independiente, el transportista de fondos debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

*Para ir más allá* Sección L. 612-5 del Código de Seguridad Nacional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

#### Autoridad competente

La delegación territorial de la CLAC en la que se encuentra París es competente para decidir sobre la solicitud de declaración.

#### Documentos de apoyo

No hay disposiciones para la actividad temporal o temporal.

### b. Obtener una tarjeta de visita para el nacional de la UE o del EEE para un ejercicio permanente (LE)

#### Autoridad competente

La CLAC, territorialmente competente, decide sobre la expedición de la tarjeta de visita siempre que el nacional cumpla las condiciones de atribución.

#### Documentos de apoyo

Para obtener la tarjeta de visita, el nacional envía un archivo completo por correo a la CLAC territorialmente competente. Este archivo debe incluir los siguientes documentos auxiliares:

- Un[Forma](https://www.cnaps-securite.fr/sites/default/files/inline-files/FormCP-MG2_0.pdf) Debidamente cumplimentado y firmado;
- Una fotocopia del documento de identidad de un nacional
- prueba de aptitud profesional que puede ser:- un certificado profesional registrado en el directorio nacional de certificaciones profesionales,
  - un certificado de cualificación profesional desarrollado por su sucursal profesional,
  - un certificado de competencia o certificado de formación expedido por un Estado de la UE o del EEE que regula la actividad de un agente de transporte de dinero en su territorio, e incluye los detalles y la duración de los módulos de la formación realizada,
  - prueba por cualquier medio de que el nacional ha participado en esta actividad, a tiempo completo o a tiempo parcial, en los últimos diez años, cuando ni la actividad profesional ni la formación están reguladas en la UE o en el Estado del EEE.

**Qué saber**

Los documentos justificativos deben estar escritos en francés o traducidos por un traductor certificado, si es necesario.

#### Duración y renovación

La tarjeta de visita se emite en forma desmaterializada de un número de registro y sigue siendo válida durante cinco años. Cualquier cambio en el estado de empleo tendrá que ser notificado a la CLAC, pero no dará lugar a la renovación obligatoria de la tarjeta. Al final de estos cinco años, el profesional podrá solicitar la renovación tres meses antes de la fecha de caducidad, siempre que presente un certificado de educación continua (véase supra "4o). (a) Obligación de someterse a formación profesional continua").

#### Resultado del procedimiento

Una vez que el nacional haya obtenido el número de registro de la CLAC territorialmente competente, tendrá que transmitirlo a su empleador que le expedirá la tarjeta profesional final.

#### Bueno saber: medidas de compensación

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a una medida de compensación, que puede ser:

- un curso de adaptación de hasta tres años
- una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

*Para ir más allá*: Artículos L. 612-20, L. 612-24, R. 612-12 y siguientes del Código de Seguridad Nacional.

#### Formas y plazos para la apelación

El solicitante podrá impugnar la negativa a expedir la tarjeta de visita en el plazo de dos meses a partir de la notificación de la decisión de denegación mediante la formación de un recurso preadministrativo obligatorio con la Acreditación Nacional y control del Consejo Nacional de Actividades de Seguridad Privada (Cnaps), ubicado 2-4-6, Boulevard Poissonniére, 75009 París.

La Comisión Nacional se pronunciará sobre la base del estatuto fáctico y jurídico vigente en la fecha de su decisión.

Este recurso es obligatorio antes de cualquier litigio. Es gratis.

Los litigios podrán ser ejercitados ante el tribunal administrativo del lugar de residencia del solicitante o ante el tribunal administrativo de París para los solicitantes que tengan su lugar de residencia en el extranjero en el plazo de dos meses a partir de la notificación. decisión expresa de la Comisión Nacional de Acreditación y Control, es decir, la adquisición de la decisión implícita de rechazar el silencio mantenido por la Comisión Nacional de Acreditación y Control durante dos meses a partir de la fecha de recepción del recurso administrativo previo obligatorio.

*Para ir más allá*: Artículo L. 633-3 del Código de Seguridad Nacional; Artículos L. 412-1 a L. 412-8 del Código de Relaciones Público-Gubernamentales; Artículos R. 421-1 a R. 421-7 del Código de Justicia Administrativa.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

