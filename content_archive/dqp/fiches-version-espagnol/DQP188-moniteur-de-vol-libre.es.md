﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP188" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Monitor de vuelo gratuito" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="monitor-de-vuelo-gratuito" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/monitor-de-vuelo-gratuito.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="monitor-de-vuelo-gratuito" -->
<!-- var(translation)="Auto" -->


Monitor de vuelo gratuito
=========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La actividad de vuelo libre incluye ala delta, parapente, kitesurf y kitesurf.

En el ala delta y parapente, el instructor diseña e implementa, de forma independiente, acciones de animación, iniciación y progresión (hang glider o parapente dependiendo de la mención del diploma elegido) hasta la autonomía, garantizando la seguridad de la práctica y terceros. Practica vuelos pedagógicos biplazas, acompaña a los practicantes en el descubrimiento y respeto del marco de práctica de la libre huida y participa en el funcionamiento de la estructura.

El instructor aeronáutico supervisa y anima las actividades de descubrimiento e iniciación, incluidos los primeros niveles de competición en esquí aeronáutico, para cualquier público y en los lugares náuticos o terrestres de práctica de actividad. El deslizador aeronáutico cubre las actividades de cometa, cometa terrestre, cometa extraída de agua y tablero náutico remolcado llamado kitesurf.

*Para ir más allá* : decreto de 27 de diciembre de 2007 por el que se establece la especialidad "vuelo libre" del certificado profesional de juventud, educación popular y deporte (BPJEPS), decreto de 9 de julio de 2002 por el que se establece la especialidad "actividades náuticas" del BPJEPS.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La actividad de monitor de vuelo gratuito está sujeta a la aplicación del artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas. Como profesor de deportes, el instructor de vuelo libre debe poseer un diploma, un título profesional o un certificado de calificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el [directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Cualificaciones para ejercer como instructor de:

- ala delta: la especialidad "vuelo libre" especialidad "ala delta", el diploma estatal de juventud, educación popular y deporte (DEJEPS, especialidad "desarrollo deportivo" mención "ala delta", el diploma estatal superior de la juventud, Educación Popular y Deporte (DESJEPS) especialidad "rendimiento deportivo" mención "ala delta";
- parapente: la especialidad BPJEPS "vuelo libre" mención "parapente", la specIALty DEJEPS "mención de desarrollo deportivo "parapente" y el SPECIALty DESJEPS "rendimiento deportivo" mención "parapente";
- kitesurf: BPJEPS especialidad "actividades náuticas" mencionan "aerotracté glide", el SPECIALty DEJEPS "desarrollo deportivo" mención "diapositivas aeronáuticas prolongadas" y el SPECIALty DESJEPS "rendimiento deportivo" mención "diapositivas aeroprolongadas" coches náuticos de riego";
- cometa: BPJEPS especialidad "actividades náuticas" mencionan "aerotracté glide", el SPECIALty DEJEPS "desarrollo deportivo" mención "diapositivas aeronáuticas prolongadas" y el SPECIALty DESJEPS "rendimiento deportivo" mención "diapositivas aeroprolongadas" Certificado de Especialización (CS) "kite" asociado con BPJEPS, DEJEPS y DESJEPS.

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos L. 212-1 y R. 212-84 del Código del Deporte.

**Bueno saber: el entorno específico**

La práctica del vuelo libre, independientemente del área de evolución, con excepción de la cometa acrobática y de combate, constituye una actividad realizada en un entorno específico, que implica el respeto de medidas de seguridad específicas. Por lo tanto, sólo las organizaciones bajo la tutela del Ministerio del Deporte pueden formar a futuros profesionales.

*Para ir más allá*: Artículos L. 212-2 y R. 212-7 del Código del Deporte.

#### Entrenamiento

##### La especialidad BPJEPS "vuelo libre" menciona "parapente" y "ala delta"

El vuelo libre BPJEPS es un diploma de Nivel IV. Esta especialidad se emite bajo dos menciones:

- parapente;
- Deslizamiento.

Se puede preparar a través de la formación inicial, el aprendizaje o la educación continua

En el caso de un título profesional, el titular puede ejercer como trabajador por cuenta propia o como empleado de una empresa o asociación.

Organizada sobre el principio de alternancia, la formación se lleva a cabo de forma continua, con una duración de entre 6 y 10 meses (dependiendo de las habilidades ya adquiridas por el candidato antes de entrar en la formación).

**Prerrogativas**

El titular del vuelo libre especializado BPJEPS puede enmarcar, en su mención (deltaplano o parapente), pilotos mayores de 14 años (excepto público con movilidad reducida) desde el descubrimiento de la actividad hasta la autonomía del vuelo.

También puede realizar vuelos de dos plazas a todo el público (excepto al público con movilidad reducida). Su intervención está sujeta al cumplimiento de las normas establecidas por las autoridades en el espacio aéreo en el que se encuentra.

###### Condiciones de acceso a la formación que conducen a la especialidad "vuelo libre" PJEPS

El interesado deberá:

- Ser mayores el día de la entrada en la formación;
- presentar un certificado médico de no contradictorio con la práctica deportiva de menos de tres meses de edad;
- Celebrar la unidad de educación y socorro cívico de nivel 1 (PSC1) sobre reciclaje anual y producir el certificado anual de reciclaje;
- presentar el certificado expedido por el Director Técnico Nacional de Vuelo Libre que certifica la participación, durante los últimos tres años y en la actividad que constituye la mención, en tres rondas de competencia en un calendario nacional, con un resultado, para cada uno, en la primera mitad del ranking;
- requisitos preeducativos:- identificar en el medio ambiente los elementos pertinentes para una práctica de seguridad (meteorología, erología, especificidad del sitio y reglamentos específicos),
  - Ser capaz de presentar las características del material,
  - ser capaz de volar en dos plazas,
  - Ser capaz de dirigir una sesión para descubrir la actividad,
  - Ser capaz de utilizar las herramientas de orientación didáctica,
  - ser capaz de reaccionar cuando se pone en juego la seguridad de los profesionales.

Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá* : orden de 27 de diciembre de 2007 por el que se establece la especialidad de "vuelo libre" del BPJEPS.

##### BPJEPS especialidad "actividades náuticas" mención monovalente "aerotractés"

Las "actividades náuticas" del BPJEPS atestiatan la posesión de las competencias profesionales esenciales para el ejercicio del instructor de actividades náuticas. Es un grado de nivel 4 (como una licenciatura técnica, una licenciatura tradicional o un certificado de técnico).

La capacitación puede ser común a varias disciplinas, pero el candidato debe elegir la mención apropiada para su proyecto. Para el kitesurf y el vuelo de cometas, esta es la referencia monovalente a los "toboganes aerodinámicos".

**Prerrogativas**

La especialidad "actividades acuáticas" mención especial "aerotractés" permite al instructor supervisar y animar las actividades de descubrimiento e iniciación, incluyendo los primeros niveles de competencia en cometa, kite tracción, kite tirado por agua y kitesurf.

**Las condiciones de acceso a la formación que conducen a la obtención de las actividades náuticas de BPJEPS mencionan los toboganes aerodinámicos**:

- Ser mayores el día de la entrada en la formación;
- presentar un certificado médico de no contradictorio con la práctica deportiva de menos de tres meses de edad;
- Celebrar la unidad anual de reciclaje PSC1 y producir el certificado anual de reciclaje;
- Tener la licencia de conducir para barcos de recreo a motor, una opción "costera";
- producir un certificado de natación de estilo libre de 100 metros, inicio buceado, con la recuperación de un objeto submarino a una profundidad de 2 metros;
- aprobar la evaluación de competencias:- insertarse en el espacio de práctica, navegar en grupos sobre un espacio pequeño,
  - se equipan con todos los elementos de protección personal, dependiendo de las condiciones climáticas del sitio,
  - despegar, aterrizar e inmovilizar su cometa de tracción sin ayuda de seguridad,
  - volver al área de preparación después de activar el sistema de seguridad de la cometa para tracción y plegado de emergencia en la zona de evolución,
  - hacer una parada de emergencia, operar con facilidad en diferentes tipos de bastidores de deslizamiento y tirar de la cometa,
  - navegar a todas las velocidades con facilidad, fluidez y eficiencia,
  - hacer giros de 180 grados a la máquina de laminación en un borde sin pérdida de deslizamiento,
  - evolucionar en las olas, realizar varias figuras de expresión de un nivel regional de competencia.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá* : decreto de 9 de julio de 2002 por el que se establecen las actividades náuticas especializadas del BPJEPS.

##### LA especialidad DEJEPS "desarrollo deportivo" menciona "hang glider", "parapente" y menciona "diapositivas aeroprolongadas náuticas"

DEJEPS es un diploma certificado de Nivel 3 (Bac 2/3). Da fe de la adquisición de una cualificación en el ejercicio de una actividad profesional de coordinación y supervisión con fines educativos en los ámbitos de las actividades físicas, deportivas, socioeducativas o culturales.

Este diploma se prepara alternativamente mediante la formación inicial, el aprendizaje o la educación continua. Se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr/).

**Prerrogativas**

La posesión del DEJEPS, que está marcado como "hang glider", "paraglider" o "aerotractés náutico" atestigua las siguientes habilidades en el área elegida:

- Diseñar programas de desarrollo deportivo y competición
- Coordinar la ejecución de un proyecto de desarrollo deportivo y acceso a la competencia;
- llevar a cabo un enfoque de desarrollo deportivo y el acceso a la competencia;
- llevar a cabo actividades de capacitación
- para la mención de "diapositivas aerotractés", desarrollar actividades deportivas y educativas, llevar a cabo acciones hacia audiencias específicas, desarrollar actividades para audiencias específicas, coordinar un equipo de profesionales y voluntarios en escala de un proyecto.

###### Las condiciones de acceso a la formación que conducen a DEJEPS mencionan "hang glider" y "paraglider"

El interesado deberá:

- Ser mayores el día de la entrada en la formación;
- presentar un certificado médico de no contradictorio con la práctica deportiva de menos de tres meses de edad;
- producir certificados de participación en cinco rondas de competición de parapente o parapente, dependiendo de la mención elegida, en un calendario nacional durante los últimos tres años, con un resultado para cada uno en el primer tercio de la clasificación, emitido por el Director Técnico Nacional de Vuelo Libre;
- producir certificados para justificar una experiencia de 400 horas en el campo de la supervisión docente en los últimos tres años;
- llevar a cabo con éxito la entrevista basada en una experiencia de parapente o ala delta de la enseñanza de entrenamiento;
- ser capaz de enmarcar el parapente o el planeador colgante, dependiendo de la mención elegida, en seguridad (requisito prepedagógico).

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá* : Ordenes del 27 de diciembre de 2007 por las que se crea el "parapente" del "desarrollo deportivo" SPECIALty DEJEPS y se crea la designación "hang glider" del "desarrollo deportivo" SPECIALty DEJEPS.

###### Las condiciones de acceso a la formación que conducen a deJEPS mencionan "deslizamientos aerodinámicos náuticos"

El interesado deberá:

- Ser mayores el día de la entrada en la formación;
- presentar un certificado médico de no contradictorio con la práctica deportiva de menos de tres meses de edad;
- presentar un certificado de participación efectiva en tres concursos a nivel nacional en el ámbito de los cielos aeronáuticos o un certificado de éxito en una prueba técnica consistente en un rendimiento evaluado por el director técnico vuelo libre nacional (la prueba está organizada por la Federación Francesa de Vuelo Libre y el certificado expedido por el director técnico nacional de vuelo libre);
- presentar un certificado que justifique en el campo de las diapositivas prolongadas en los últimos cinco años de un total de 1.000 horas de experiencia en enseñanza, formación, tutoría, formación, competencia o coordinación de proyectos . El certificado es expedido por el director técnico nacional de vuelo libre;
- pasar una entrevista basada en un dossier preparado por el candidato en relación con su experiencia de coaching, enseñanza, formación, tutoría, formación o coordinación de proyectos, en el campo de las diapositivas aerolargadas náuticas;
- pasar una sesión de coaching con tres personas para la progresión y seguida de una entrevista para verificar los requisitos para la siguiente situación pedagógica: evaluar los riesgos objetivos asociados con la práctica de la disciplina en áreas aerológicas y marítimas e intervenir técnicamente en incidentes, organizar la zona de práctica y supervisar la actividad, organizar el disparador de rescate de acuerdo con la normativa vigente, comportamientos de riesgo, incluido el dopaje y la aplicación de una situación formativa.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá* : decreto del 5 de enero de 2010 por el que se crea la designación "aerotractés náutico" del "desarrollo deportivo" SPECIALty DEJEPS.

##### La especialidad DESJEPS "rendimiento deportivo" menciona "hang glider", "parapente" y menciona "diapositivas aerodinámicas náuticas"

El DESJEPS es un título certificado en el nivel 2 (bac 3/4). Se prepara como una alternancia en la formación inicial, el aprendizaje o la educación continua. Se puede obtener en su totalidad a través del VAE. Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr/).

**Prerrogativas**

La posesión de la especialidad DESJEPS "rendimiento deportivo" mencionan "hang glider", "paraglider" o mencionan "deslizamientos aeronáuticos" atestigua, en el campo elegido, las siguientes habilidades:

- Preparar un proyecto de rendimiento estratégico
- Pilotar un sistema de entrenamiento
- Ejecución de un proyecto deportivo
- Evaluar un sistema de formación
- Organizar actividades de formación para entrenadores de vuelo libre;
- para las palabras "diapositivas acropolitradas náuticas", llevar a cabo actividades de entrenamiento deportivo.

###### Condiciones de acceso a la formación que conduzcan a la designación DESJEPS "ala delta" y "parapente"

El interesado deberá:

- Ser mayores el día de la entrada en la formación;
- presentar un certificado médico de no contradictorio con la práctica deportiva de menos de tres meses de edad;
- producir certificados de participación en cinco rondas de competencia de ala delta o parapente, dependiendo de la mención elegida, en un calendario nacional durante los últimos cinco años, con un resultado, para cada uno, en el primer tercio de la clasificación, emitido por el Director Técnico Nacional de Vuelo Libre;
- producir certificados para justificar 800 horas de experiencia en los últimos cinco años en las áreas de enseñanza, formación, tutoría, formación o coordinación de proyectos de vuelo libre;
- entrevista exitosa basada en un registro de enseñanza, formación, tutoría, formación o coordinación de un proyecto de vuelo libre;
- ser capaz de enmarcar el parapente o el planeador colgante, dependiendo de la mención elegida, en seguridad (requisito prepedagógico).

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá* : Ordenes de 27 de diciembre de 2007 por las que se crea el "parapente" del DESJEPS de "rendimiento deportivo" de SPECIALty y se crea la designación "hang glider" del DESJEPS DESJEPS.

###### Condiciones de acceso a la formación que conducen a la designación DESJEPS "deslizamientos náuticos atraquedos"

El interesado deberá:

- Ser mayores el día de la entrada en la formación;
- presentar un certificado médico de no contradictorio con la práctica deportiva de menos de tres meses de edad;
- presentar un certificado de participación efectiva en tres concursos a nivel nacional en el ámbito de los cielos náuticos aerotratados y un certificado de éxito en una prueba técnica consistente en un rendimiento evaluado por el director técnico Vuelo Libre Nacional;
- presentar un certificado que justifique en el campo de las diapositivas prolongadas en los últimos cinco años de un total de mil horas de experiencia en enseñanza, formación, tutoría, formación, competencia o coordinación de Proyecto;
- realizar con éxito una entrevista basada en un dossier preparado por el candidato en relación con su experiencia en coaching, enseñanza, formación, tutoría, formación o coordinación de proyectos, en el campo de las diapositivas aeromonáticas;
- pasar una sesión de coaching de tres personas, con el objetivo de progresión y seguida de una entrevista para verificar los siguientes requisitos prepedagógicos: evaluar los riesgos objetivos asociados con la práctica de la disciplina en las zonas aerológicas y marítimas e intervenir técnicamente en incidentes, organizar el área de práctica y monitorear la actividad, organizar el disparador de rescate de acuerdo con la normativa vigente, comportamientos de riesgo, incluido el dopaje, implementan una situación formativa.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos.

*Para ir más allá* : decreto del 5 de enero de 2010 por el que se crea la etiqueta "aerotractés náutico" de la especialidad DESJEPS "rendimiento deportivo".

##### CS "kite" asociado con BPJEPS, DEJEPS y DESJEPS

La cometa CS atestigua las habilidades del titular a garantizar en el campo de la cometa, en la autonomía educativa, la realización de servicios de descubrimiento, animación e iniciación, así como los ciclos de aprendizaje hasta el primer nivel de competencia en actividades de cometas.

La cometa CS se puede asociar con:

- la especialidad BPJEPS "actividad física para todos", especialidad "actividades acuáticas" y especialidad "vuelo libre";
- especialidad DEJEPS "desarrollo deportivo" mención "vela", "aerotractés náutico", "gplaneando colgante" y "parapente";
- desJEPS especialidad "rendimiento deportivo" mención "vela" y mención "aerotractés náutico.

*Para ir más allá* : decreto del 5 de enero de 2010 por el que se establece la "kite" CS asociada con BPJEPS, la especialidad DEJEPS "desarrollo deportivo" y la especialidad DESJEPS "rendimiento deportivo".

#### Costos asociados con la calificación

Se paga la capacitación para obtener BPJEPS, DEJEPS y DESJEPS. Sus costos varían dependiendo de la organización de capacitación. Para [más detalles](http://foromes.calendrier.sports.gouv.fr/#/formation), es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

#### Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

#### Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como instructor de vuelo libre en Francia para personas que han sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Proceso de cualificaciones y formalidades
------------------------------------------------------------------

### a. Declaración Anticipada/Tarjeta Profesional

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial](https://eaps.sports.gouv.fr).

#### hora

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

#### Documentos de apoyo

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

### b. Predeclaración de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

#### Autoridad competente

La declaración previa de actividad debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde el Departamento en el que declarante quiere realizar su actuación.

#### hora

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

#### Documentos de apoyo

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### c. Predeclaración de actividad de los nacionales de la UE para el ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

#### hora

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

#### Documentos de apoyo para la primera declaración de actividad

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

#### Evidencia para una declaración de renovación de la actividad

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

### d. Medidas de compensación

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

