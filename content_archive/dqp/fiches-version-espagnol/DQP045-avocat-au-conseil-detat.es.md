﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP045" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Abogado del Consejo de Estado y del Tribunal de Casación" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="abogado-del-consejo-de-estado-tribunal" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/abogado-del-consejo-de-estado-y-del-tribunal-de-casacion.html" -->
<!-- var(last-update)="Augusto de 2021" -->
<!-- var(url-name)="abogado-del-consejo-de-estado-y-del-tribunal-de-casacion" -->
<!-- var(translation)="Auto" -->


Abogado del Consejo de Estado y del Tribunal de Casación
========================================================

Actualización más reciente: : <!-- begin-var(last-update) -->Augusto de 2021<!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

## 1°. Definición de la actividad

El abogado del Consejo de Estado y del Tribunal de Casación es un profesional judicial responsable de asistir y/o representar a las partes en caso de litigio ante el Consejo de Estado y el Tribunal de Casación.

Estos abogados a las juntas son los únicos con derecho a declarar ante estos tribunales cuando la representación es obligatoria.

## 2°. Cualificaciones profesionales

### a. Requisitos nacionales

#### Legislación nacional

Para ejercer como abogado en el Consejo de Estado y en el Tribunal de Casación, el profesional debe cumplir los siguientes requisitos de aptitud:

- Ser nacional francés o nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo del Espacio Económico Europeo (EEE);
- poseer un máster en derecho (B.A. 4) o un título o formación reconocido como equivalente para el acceso a la profesión de abogado;
- han estado en la junta directiva de un abogado durante al menos un año;
- han recibido formación específica (véase infra "Formación en la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación");
- han superado con éxito la prueba de aptitud para la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación;
- no ser objeto de una condena penal por actos contrarios al honor, la probidad o la buena moral;
- no estar sujeto a ninguna sanción disciplinaria o administrativa de despido, retirada de la lista, despido, retirada de la acreditación o autorización;
- no estar sujeto a quiebra personal o a la prohibición de dirigir, administrar, administrar o controlar un negocio comercial, artesanal o agrícola.

Tan pronto como cumpla con estas condiciones, se designa al profesional (véase infra "2." a. Nominación" por la Guardia del Sello:

- En una oficina existente, sobre la presentación del interesado;
- En una oficina creada;
- en una oficina vacante;
- además de estos métodos de nombramiento para ejercer como abogado liberal, un abogado con abogado puede ser nombrado como empleado en una oficina.

**Tenga en cuenta que**

Un profesional que tenga un cargo legal en el Consejo de Estado y el Tribunal de Casación no puede emplear a más de un abogado asalariado en el mismo consejo.

*Para ir más allá*: Artículo 1 del Decreto 91-1125, de 28 de octubre de 1991, sobre las condiciones de acceso a la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación.

**Bueno saber: exenciones**

Exento de la obtención de un máster en Derecho, inscripción en un bar y formación específica:

- miembros y antiguos miembros del Consejo de Estado, excepto los maestros de peticiones y auditores;
- magistrados y ex magistrados del Tribunal de Casación y del Tribunal de Cuentas, con excepción de consejeros, fiscales generales de referéndum y auditores;
- justifican cuatro años de ejercicio:- profesores universitarios a cargo de la educación jurídica,
  - maestros y antiguos maestros de peticiones al Consejo de Estado, concejales, fiscales generales del referéndum y ex asesores generales del referéndum ante el Tribunal de Casación y el Tribunal de Cuentas;
- siempre y cuando justifiquen al menos un año de práctica profesional con un abogado del Consejo de Estado y del Tribunal de Casación:- magistrados y ex magistrados de la orden judicial, distintos de los mencionados anteriormente y que justifiquen haber ejercido durante al menos ocho años,
  - miembros y antiguos miembros del Consejo de Estado y del Tribunal de Cuentas distintos de los mencionados anteriormente, miembros y ex miembros del órgano de los tribunales administrativos y tribunales administrativos de apelación y miembros y ex miembros de las salas cuentas con al menos ocho años de experiencia,
  - profesores de derecho y ex profesores asistentes con un doctorado en derecho y que justifican una práctica de al menos diez años de educación jurídica,
  - abogados y ex abogados registrados durante al menos diez años en el consejo de un colegio de abogados francés o un Estado miembro de la UE,
  - asesoramiento legal anterior que puede incluirse durante al menos 10 años en una lista de asesoramiento jurídico,
  - notarios con al menos diez años de experiencia.

*Para ir más allá*: Artículos 2 a 4 del Decreto 91-1125 de 28 de octubre de 1991.

#### Entrenamiento

**Formación en la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación**

Esta formación, que tiene una duración de tres años, está bajo la autoridad del Consejo de la Orden de los Abogados en el Consejo de Estado y el Tribunal de Casación, que establece sus términos. El profesional admitido en la formación está inscrito en un registro específico mantenido por el Consejo de la Orden.

Esta formación consiste en:

- educación teórica en forma de participación en el trabajo de la conferencia de prácticas de abogados en el Consejo de Estado y el Tribunal de Casación y práctica profesional;
- práctica profesional bajo el control del Consejo de la Orden y bajo la responsabilidad de un maestro en prácticas, abogado en el Consejo de Estado y el Tribunal de Casación.

En caso de condena penal por violaciones de honor, probidad y moral, o en caso de ausencia injustificada durante más de tres meses, el profesional en prácticas puede estar sujeto a una bajada y formación.

Al final de su formación, el profesional recibe un certificado de finalización de la formación indicando la duración del servicio prestado, la naturaleza de las funciones realizadas y las observaciones del alumno.

*Para ir más allá*: Artículos 6 a 16 del Decreto de 28 de octubre de 1991 supra.

**Prueba de aptitud para el certificado de aptitud para abogar (Capac)**

Para ejercer como abogado y por el Tribunal de Casación, el profesional debe haber completado con éxito una prueba de aptitud consistente en pruebas de elegibilidad por escrito y pruebas de admisión oral.

Este examen se presenta ante un jurado compuesto por:

- consejero de Estado;
- asesor del Tribunal de Casación;
- un profesional universitario, a cargo de la educación jurídica;
- tres abogados en el Consejo de Estado y el Tribunal de Casación.

Las modalidades y el programa de las pruebas figuran en el anexo del decreto de 22 de agosto de 2016 por el que se establecen el programa y las modalidades de examen de aptitud para la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación.

Una vez que el profesional cumpla estas condiciones, deberá solicitar al Presidente del Consejo de la Orden de los Abogados este examen al Consejo de Estado y al Tribunal de Casación (véase infra "5o). a. Solicitud de inscripción para la prueba de aptitud").

**Tenga en cuenta que**

Algunos profesionales pueden beneficiarse de las exenciones establecidas en el artículo 17 del decreto de 28 de octubre de 1991.

Una vez completado con éxito el examen, se expide al profesional un Certificado de Aptitud para la Profesión de Consejero (Capac).

*Para ir más allá*: Artículos 17 y 18 del Decreto 91-1125, de 28 de octubre de 1991.

**Formación profesional continua**

El abogado del Tribunal de Casación está obligado a llevar a cabo una formación continua.

Esta formación obligatoria, que dura veinte horas (durante un período de un año) o cuarenta horas (durante un período de dos años), puede adoptar la forma de:

- participación en actividades de formación jurídica o profesional en las universidades;
- participación en la formación de abogados en el Consejo de Estado y el Tribunal de Casación o instituciones educativas;
- asistencia en conferencias legales o conferencias relacionadas con la actividad del profesional;
- una exención de la educación jurídica en un entorno académico o profesional;
- publicación de trabajo jurídico.

**Tenga en cuenta que**

Durante los dos primeros años de práctica profesional, esta educación continua debe incluir diez horas de gestión de agencias, ética y estatus profesional. Sin embargo, la formación de los profesionales que se benefician de las exenciones debe centrarse exclusivamente en estos puntos.

Además, cada año, a más tardar el 31 de enero, el profesional está obligado a informar el número de horas de educación continua seguidas a la secretaría del Consejo de Abogados al Consejo de Estado y al Tribunal de Casación.

*Para ir más allá*: Artículos 18-1 y 18-2 del Decreto de 28 de octubre de 1991 supra y artículo 13-2 de la ordenanza de 10 de septiembre de 1817 que reúne, bajo el nombre de la Orden de los Abogados en el Consejo de Estado y el Tribunal de Casación, la Orden del Consejero al Abogado y el Colegio de Abogados para el Tribunal de Casación fija irrevocablemente el número de titulares y contiene disposiciones para la disciplina interna de la Orden.

**Cita**

El profesional que cumpla con las cualificaciones generales de aptitud y posea la Capac deberá, para prestar juramento y asumir el cargo, ser nombrado por el Ministro de Justicia.

El profesional que cumpla los requisitos generales de idoneidad para la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación podrá ser objeto de un nombramiento en un cargo creado en las condiciones previstas en los artículos 24 y 25 del Decreto 91-1125.

El candidato que desee suceder a un abogado en el consejo debe solicitar la aprobación del Ministro de Justicia (ver infra "5o. b. Solicitud de acreditación para una nominación previa a la presentación").

El profesional que desee ejercer como empleado, en una oficina creada debe presentar su solicitud al Ministro de Justicia (cf. infra "5. c. Solicitud de nombramiento para un ejercicio asalariado").

Además, en caso de que se declare vacante una oficina, el Ministro de Justicia hace una convocatoria de manifestaciones de interés, mediante decreto publicado en el Diario Oficial de la República Francesa y fija el importe de la indemnización adeudada. El profesional que desee ocupar esta oficina vacante debe solicitar al guardaste, acompañado de un compromiso de pago de esta compensación.

**Tenga en cuenta que**

El profesional también podrá ejercer como abogado en una sociedad, en las condiciones previstas en el Decreto No 2016-881, de 29 de junio de 2016, relativo al ejercicio de la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación en forma de sociedad que no sea una sociedad civil profesional, como una sociedad polizónada multiprofesional.

Una vez nombrado, el profesional toma posesión desde el momento de su juramento. Esto se hará en el plazo de un mes a partir de su nombramiento por el Ministro de Justicia.

*Para ir más allá*: Artículos 19 a 31 del Decreto 91-1125, de 28 de octubre de 1991, sobre las condiciones de acceso a la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación.

#### Costos asociados con la calificación

La formación que conduce a Capac es gratuita. Puede echar un vistazo a la[de abogados en el Consejo de Estado y el Tribunal de Casación](http://www.ordre-avocats-cassation.fr/), para obtener más información.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Cualquier ciudadano europeo de la UE o del EEE puede ejercer en Francia la actividad de asesoramiento / representación ante el Consejo de Estado y el Tribunal de Casación, bajo su título profesional original, de forma temporal y ocasional, si reúne las siguientes condiciones:

- ejercicio bajo uno de los títulos que figuran en la lista del artículo 31-2 II del decreto n ° 91-1125 de 28 de octubre de 1991 relativo a las condiciones de acceso a la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación;
- estar autorizado en el Estado miembro en el que esté establecido para representar a las partes ante el tribunal o tribunales supremos y dedicar una parte sustancial de su actividad a ello de forma periódica.

En este caso, el profesional europeo interesado deberá presentar una solicitud al Guardián de los Sellos, Ministro de Justicia, por vía electrónica. Adjunta prueba de su identidad y nacionalidad, así como documentos para verificar que cumple con las condiciones antes mencionadas. La prueba de estas condiciones es gratuita.

El ministro toma una decisión en quince días. La posible ausencia de una decisión del Ministro al final de este período constituye una autorización. La autorización emitida no tiene límite de tiempo.

Una vez expedida la autorización, el profesional solo podrá asistir / representar a un cliente después de haber establecido su residencia con un abogado en el Consejo de Estado y en el Tribunal de Casación para que los documentos procesales le sean notificados periódicamente.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Cualquier ciudadano europeo de la UE o del EEE puede ejercer en Francia la actividad de asesoramiento / representación ante el Consejo de Estado y el Tribunal de Casación, bajo su título profesional original, de forma permanente, si cumple las siguientes condiciones:

- ejercicio bajo uno de los títulos que figuran en la lista del artículo 31-2 II del decreto n ° 91-1125 de 28 de octubre de 1991 relativo a las condiciones de acceso a la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación;
- estar autorizado en el Estado miembro en el que esté establecido para representar a las partes ante el tribunal o tribunales supremos y dedicar una parte sustancial de su actividad a ello de forma periódica.

En este caso, el profesional europeo interesado deberá presentar una solicitud al Guardián de los Sellos, Ministro de Justicia, por vía electrónica. Adjunta prueba de su identidad y nacionalidad, así como documentos para verificar que cumple con las condiciones antes mencionadas. La prueba de estas condiciones es gratuita.

El ministro da su opinión en dos meses. La posible ausencia de una decisión del Ministro al final de este período constituye una autorización. La autorización emitida no tiene límite de tiempo.

Una vez expedida la autorización, el profesional solo podrá asistir / representar a un cliente después de haber establecido su residencia con un abogado en el Consejo de Estado y en el Tribunal de Casación para que los documentos procesales le sean notificados periódicamente.

El profesional europeo autorizado para ejercer bajo su título original de forma permanente está inscrito en una lista especial del registro del Colegio de Abogados en el Consejo de Estado y el Tribunal de Casación.

Al término de una actividad regular y efectiva en el territorio nacional por un período mínimo igual a tres años y si cumple las condiciones generales de aptitud para la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación, puede solicitar ser nombrado para la oficina de un abogado en el Consejo de Estado y el Tribunal de Casación en las mismas condiciones que los abogados en el Consejo de Estado nacional y el Tribunal de Casación.

El titular así designado prestará juramento.

## 3°. Condiciones de honorabilidad, reglas éticas, ética

**Ética**

El profesional que ejercerá la actividad de un abogado y cerca del Tribunal de Casación está obligado a respetar las normas de ética establecidas por la profesión.

Como tal, se requiere para:

- ejercer de forma independiente;
- respeto a la confidencialidad profesional
- Actuar para evitar cualquier conflicto de intereses de sus clientes;
- el respeto de los requisitos de cortesía, delicadeza, moderación y lealtad a las diversas jurisdicciones;
- el respeto del deber de delicadeza y solidaridad hacia sus colegas;
- para ejercer moderación y delicadeza en el establecimiento de sus honorarios.

**Es bueno saber**

Las tarifas del profesional se establecen de acuerdo con el cliente dentro de un acuerdo de tarifa.

Las reglas generales de la ética están disponibles en el[de abogados en el Consejo de Estado y el Tribunal de Casación](http://www.ordre-avocats-cassation.fr/les-avocats-aux-conseils/deontologie-et-honoraires).

*Para ir más allá*: Artículo 15 de la ordenanza de 10 de septiembre de 1817.

**Disciplina**

En caso de incumplimiento de las normas éticas y profesionales, el abogado puede estar sujeto a procedimientos disciplinarios. Como tal, se enfrenta a las siguientes sanciones disciplinarias:

- La advertencia
- Culparlo;
- una prohibición temporal de la práctica, que no podrá exceder de tres años;
- retirar la lista de la tabla de aguacate o retirar el honorario;
- privación del derecho a ser miembro del Colegio de Abogados del Consejo de Estado y del Tribunal de Casación.

*Para ir más allá* Decreto No 2002-76, de 11 de enero de 2002, relativo a la disciplina de los abogados en el Consejo de Estado y en el Tribunal de Casación.

## 4°. Seguro financiero y garantía

**Contrato de grupo de seguros**

La responsabilidad civil profesional de los abogados en el Consejo de Estado y en el Tribunal de Casación se rige por el artículo 13 de la ordenanza de 10 de septiembre de 1817. El Consejo Universitario ha firmado un contrato de grupo de seguros para garantizar las actividades profesionales de abogados y consejos.

**Obligación de justificar una garantía financiera**

El abogado debe justificar una garantía financiera que se asignará al reembolso de los fondos recibidos durante su actividad. Este es un compromiso de fianza con un banco o institución de crédito, una compañía de seguros o una compañía de seguros mutuos. Su importe debe ser al menos igual al de los fondos que planea tener.

*Para ir más allá*: Artículos 210 a 225 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

## 5°. Procedimientos y formalidades de reconocimiento de cualificación

### a. Solicitud de inscripción para la prueba de aptitud

Cualquier ciudadano de la UE o del EEE puede ser nombrado abogado en el Consejo de Estado y en el Tribunal de Casación si puede justificar:

- diplomas, certificados u otros títulos o formación similar que permitan el ejercicio de la misma profesión en un Estado miembro de la UE o del EEE expedidos por la autoridad competente de ese Estado y que sancionen una formación adquirida de forma predominante en el EEE, o por un tercer país, siempre que un certificado de la autoridad competente del Estado miembro o parte que reconozca los diplomas, certificados, otros títulos o formación similar que acredite que su titular tiene al menos tres años de experiencia profesional en ese Estado;
- o el ejercicio a tiempo completo de la misma profesión durante al menos un año o, en el caso del ejercicio a tiempo parcial, durante un período total equivalente durante los diez años anteriores en un Estado miembro o parte que no regule el acceso o el ejercicio de esta profesión siempre que este ejercicio sea certificado por la autoridad competente de ese Estado. Sin embargo, no se exige la condición de experiencia profesional de un año cuando el certificado o certificados de formación que posea el solicitante acredite una formación reglada directamente orientada al ejercicio de la profesión.

En cuanto cumpla alguna de estas condiciones, el nacional deberá someterse a una prueba de aptitud (artículo 5 del citado decreto del 28 de octubre de 1991 y para ir más allá ver el decreto del 22 de noviembre de 1991 tomado en aplicación del artículo 5 del decreto n. ° 91-1125 de 28 de octubre de 1991 relativo a las condiciones de acceso a la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación).

El candidato podrá ser eximido de una prueba cuando los conocimientos, habilidades y competencias que haya adquirido durante su formación, su experiencia profesional previa o aprendizaje permanente y habiendo sido objeto, a tal efecto, de la debida validación por un organismo competente, en un Estado miembro o en un tercer país sea tal que no sea necesario realizar esta prueba. Sin embargo, no puede estar exento de comprobar sus conocimientos sobre la regulación profesional y la gestión de una oficina.

El interesado envía su expediente al Guardián de los Sellos, Ministro de Justicia. Al recibir el expediente completo, el Guardián de los Sellos, Ministro de Justicia, le emite un recibo.

La lista de candidatos admitidos para presentarse al examen es, tras el asesoramiento del Consejo del Colegio de Abogados, el Consejo de Estado y el Tribunal de Casación, adoptada por el Guardián de los Sellos, Ministro de Justicia.

La decisión del Guardián de los Sellos, Ministro de Justicia, debe tomarse dentro de los cuatro meses siguientes a la fecha de emisión del recibo. Está motivada y es precisa:

a) el nivel de cualificaciones profesionales exigidas en Francia y el nivel de cualificaciones profesionales que posee el candidato de acuerdo con la clasificación que figura en el artículo 11 de la Directiva 2005/36 / CE del Parlamento Europeo y del Consejo, de 7 de septiembre de 2005, modificada relacionados con el reconocimiento de cualificaciones profesionales;
b) así como, en su caso, las materias sobre las que deba ser interrogado el candidato, teniendo en cuenta las diferencias sustanciales entre, por un lado, los conocimientos, habilidades y competencias adquiridos durante su formación, su experiencia profesional o el aprendizaje permanente y, por otro lado, las materias cuyo dominio es imprescindible para el ejercicio de la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación.

Nadie puede realizar la prueba de aptitud más de tres veces.

### b. Solicitud de acreditación para una cita de presentación

**Autoridad competente**

El solicitante debe presentar una solicitud al Ministro de Justicia.

**Documentos de apoyo**

Su solicitud debe incluir todos los acuerdos entre el titular de la junta o su titular de los derechos y el candidato y, en su caso, todos los elementos para justificar su capacidad financiera.

**Tiempo y procedimiento**

El Ministro de Justicia podrá solicitar el asesoramiento del Consejo de la Orden sobre la honorabilidad y las capacidades financieras del candidato. A falta de una notificación emitida por el Consejo más allá de un plazo de cuarenta y cinco días, se considera que ha emitido un dictamen favorable. Además, el Ministro recibe el dictamen motivado del Vicepresidente del Consejo de Estado, el primer Presidente y el Fiscal del Tribunal de Casación. En ausencia de notificación emitida más allá de cuarenta y cinco días a partir de la remisión, la notificación se considera dictada.

*Para ir más allá*: Artículos 20 a 23 del Decreto 91-1125, de 28 de octubre de 1991, sobre las condiciones de acceso a la profesión de abogado en el Consejo de Estado y en el Tribunal de Casación.

### c. Solicitud de nombramiento para un ejercicio asalariado

**Autoridad competente**

La solicitud deberá ser formulada por el titular de la oficina y el candidato, al Ministro de Justicia, en el plazo de dos meses a partir de la publicación de las recomendaciones de la autoridad de competencia para la creación de oficinas jurídicas en el Consejo Tribunal de Justicia y el Tribunal de Casación.

**Documentos de apoyo**

Su solicitud debe incluir una copia del contrato de trabajo y toda la documentación necesaria.

**Tiempo y procedimiento**

El Guardián del Sello recibe el dictamen motivado del Vicepresidente del Consejo de Estado, el Primer Presidente y el Fiscal General del Tribunal de Casación y el Consejo de la Orden sobre honorabilidad, capacidad profesional y cumplimiento del contrato de trabajo candidato de las normas profesionales. En ausencia de una respuesta más allá de cuarenta y cinco días desde el momento en que se trasladan, se considera que los avisos se han prestado.

**Tenga en cuenta que**

El abogado está obligado a tomar el juramento dentro de un mes de su nombramiento, de lo contrario se considerará que renuncia a su nombramiento.

*Para ir más allá* : Decreto No 2016-651 de 20 de mayo de 2016 relativo a los abogados del Consejo de Estado y del Tribunal de Casación.

### d. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

