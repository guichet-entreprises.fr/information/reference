﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP077" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transporte" -->
<!-- var(title)="Taxista" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transporte" -->
<!-- var(title-short)="taxista" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/transporte/taxista.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="taxista" -->
<!-- var(translation)="Auto" -->


Taxista
=======

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El taxista es un profesional cuya actividad consiste en el transporte bajo demanda, las personas y su equipaje a través de un vehículo de hasta ocho plazas y equipado con equipo especial (placa, medios de pago electrónico, etc.).

Como tal, el interesado, que tiene un permiso de estacionamiento, puede esperar a los clientes en la vía pública en los lugares reservados para él o ella.

*Para ir más allá*: Artículos L. 3120-5 y L. 3121-1 del Código de Transporte.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de taxista el profesional debe ser:

- profesionalmente calificados. Para ello debe:- han aprobado un examen profesional (ver infra "2.2). Capacitación"),
  - justificar una experiencia profesional como taxista de al menos un año en los últimos diez años;
- Titular de un permiso de estacionamiento (ADS) y una tarjeta de visita;
- han recibido capacitación en prevención de nivel 1 y alivio cívico ([PSC 1](http://www.croix-rouge.fr/Je-me-forme/Particuliers/Prevention-et-secours-civiques-de-niveau-1-IRR)) al menos dos meses antes del ejercicio de su actividad;
- físicamente en forma.

*Para ir más allá*: Artículo R. 3121-17 del Código de Transporte; Decreto 91-834 sobre formación en primeros auxilios.

**Autorización de aparcamiento**

El profesional que desee llevar a cabo la actividad de taxista debe tener un permiso de estacionamiento que le permita aparcar o conducir en los sitios reservados para él con el fin de esperar a los clientes. También debe justificar su explotación real y continua.

El interesado debe solicitar permiso (véase infra" 5 grados. a. Solicitud de permisos de estacionamiento").

**Tenga en cuenta que**

Se prevén disposiciones específicas para los titulares de un permiso de estacionamiento expedido antes de la promulgación de la ley de 1 de octubre de 2014 relativa a los taxis y los coches de transporte con chófer. (ver Sección R. 3121-8 del Código de Transporte).

Además, la autoridad competente podrá retirar el permiso de estacionamiento siempre que el profesional:

- ya no está en posesión de su tarjeta de visita;
- Realiza la solicitud;
- se declara permanentemente no apto (en caso de retirada de su licencia de conducir);
- Muere.

*Para ir más allá*: Artículos L. 3121-1-2 y siguientes y L. 3121-11 del Código de Transporte.

**Tarjeta de visita**

Para llevar a cabo su actividad el taxista debe:

- Haber tenido una licencia de conducir durante al menos tres años utilizada en los términos y condiciones de las Secciones R. 221-1 y siguientes de la Ley de Tráfico de Carreteras);
- profesionalmente aptos (ver infra"Formación");
- cumplir con las condiciones de honorabilidad (véase infra"3. Condiciones de honorabilidad").

Una vez que el profesional cumple con estas condiciones, puede solicitar la emisión de la tarjeta profesional (ver infra" 5. b. Solicitud de la tarjeta de visita").

**Tenga en cuenta que**

El profesional está obligado, una vez en posesión de su tarjeta de visita, a fijarlo al parabrisas de su taxi.

*Para ir más allá*: Artículos L. 3120-2-2 y R. 3120-6. Código de Transporte.

**Acondicionamiento físico**

Para llevar a cabo su actividad, el taxista debe haber llevado a cabo un control médico de aptitud para conducir con un médico aprobado por el prefecto.

Para ello, el profesional debe proporcionar al médico el[Forma](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14880.do) Cerfa 14880 y el[Forma](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14948.do) 14948 en relación con las licencias de conducir francesas y europeas, así como las partes mencionadas.

Después de esta visita médica, se entregará un certificado médico al prefecto que, si es necesario, expedirá al profesional un certificado de aptitud física.

*Para ir más allá*: Artículo R. 226-1 y siguiente de la Ley de Tráfico de Carreteras; de 31 de julio de 2012 relativo a la organización del control médico de la aptitud para conducir.

#### Entrenamiento

El profesional debe realizar un examen que incluya pruebas de elegibilidad escritas y una prueba de ingreso práctica.

Nadie puede registrarse para esta revisión si han sido sometidos a:

- una retirada permanente de su tarjeta de visita dentro de los diez años siguientes a su solicitud de registro;
- por fraude de dicha revisión en los últimos cinco años.

**Autoridad competente**

Las sesiones de examen están organizadas por la Cámara de Comercio y Artesanía (CMA) de la región en la que el profesional desea ejercer.

**Documentos de apoyo**

El candidato que ha aprobado las pruebas de elegibilidad debe aplicar un archivo firmado, incluyendo:

- Una solicitud para registrarse para la sesión deseada
- Una fotocopia de un documento de identidad válido
- Para los nacionales de la UE un permiso de trabajo;
- prueba de residencia de menos de tres meses
- Una fotocopia de la licencia de conducir
- un certificado de aptitud física (véase supra" 2. Aptitud física");
- Un documento de identidad con foto
- liquidación de las tasas de registro (cf.[Detenido](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034379062&fastPos=10&fastReqId=673563316&categorieLien=cid&oldAction=rechTexte) 6 de abril de 2017 por el que se establecen los importes de las tasas de registro para el taxista y el conductor de los exámenes de transporte con chófer).

*Para ir más allá* : orden de 6 de abril de 2017 relativa a los programas y evaluación de los exámenes de acceso a las profesiones de taxista y conductor de coche de transporte con chófer; Artículo 23 del Código de Artesanía.

#### Otras capacitaciones

**Formación continua**

El taxista debe realizar un curso de educación continua cada cinco años en un centro de formación aprobado.

El curso de 14 horas tiene como objetivo actualizar los conocimientos del profesional e incluye:

- Los siguientes módulos obligatorios:- el derecho de transporte público especial de personas,
  - regulaciones específicas para la actividad del taxi,
  - Seguridad vial
- un módulo para elegir:- Inglés
  - desarrollo de negocios (incluido el uso de nuevas tecnologías de la información y la comunicación),
  - prevención cívica y alivio.

Al final de su formación, el candidato recibe un certificado de formación de seguimiento.

*Para ir más allá*: Artículo R. 3120-8-2 del Código de Transporte; orden de 11 de agosto de 2017 relativa a la formación continua de taxistas y conductores de coches de transporte con chófer y a la movilidad de los taxistas.

**Formación en movilidad**

Un profesional que desee trabajar como taxista en un departamento distinto del que obtuvo su cualificación profesional debe someterse a una formación de movilidad de 14 horas, incluidos los siguientes módulos:

- Conocimiento del territorio
- las regulaciones locales.

Al final de esta formación, el centro de formación otorga un certificado de seguimiento y una autorización para ejercer al candidato y al prefecto del departamento en el que obtuvo su examen.

*Para ir más allá* : orden de 11 de agosto de 2017 antes mencionado.

#### Costos asociados con la calificación

El examen que conduce a la profesión de taxista se paga y su costo varía en función de la naturaleza de las pruebas previstas.

*Para ir más allá* : decreto de 6 de abril de 2017 por el que se fijan las tasas de matriculación para los exámenes de los taxistas y conductores de coches de transporte con chófer.

### b. Nacionales de la UE: para el ejercicio temporal e informal (prestación gratuita de servicios)

No se prevé el ejercicio de la actividad de taxista al nacional de un Estado miembro de la Unión Europea (UE) ni a un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE).

Como tal, el profesional que desea llevar a cabo esta actividad de forma temporal y casual en Francia está sujeto a los mismos requisitos que el nacional francés (véase supra" 2. a. Requisitos nacionales").

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Cualquier nacional de la UE legalmente establecido que practique la actividad de taxista puede, de forma permanente, llevar a cabo la misma actividad en Francia.

Para ello, el profesional debe:

- estar calificados profesionalmente, es decir:- Ser un certificado de competencia o un certificado de prueba expedido por la autoridad competente del estado del que es nacional,
  - justificar haber sido taxista durante al menos un año en los últimos diez años;
- como el nacional francés (véase infra" 5 grados. a. Pasos y formalidades para el reconocimiento de cualificaciones");
- justificar tener las habilidades de idiomas necesarias para ejercer su profesión en Francia.

La aptitud profesional será reconocida por el prefecto del departamento domiciliado del profesional o, en su caso, por el prefecto de policía.

**Bueno saber: medida de compensación**

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia para llevar a cabo la actividad de taxista, el prefecto podrá exigir que el interesado se someta a un alojamiento o una prueba de aptitud.

*Para ir más allá*: Artículo R. 3120-8-1 del Código de Transporte.

3°. Condiciones de honorabilidad
-----------------------------------------

El profesional que desee realizar la actividad de taxista no debe haber estado sujeto a:

- una retirada de la mitad del número máximo de puntos en la licencia de conducir;
- una condena definitiva por conducir sin licencia de conducir o negarse a devolver una licencia después de que haya sido invalidada o cancelada;
- una sentencia definitiva de seis meses de prisión por uno de los siguientes delitos:- Vuelo
  - Estafa
  - violación de la confianza,
  - Injerencia deliberada con la integridad de la persona,
  - agresión sexual,
  - tráfico de armas,
  - Extorsión
  - Narcotráfico.

*Para ir más allá*: Artículo R. 3120-8 del Código de Transporte.

4°. Seguros y sanciones
--------------------------------------------

### Seguro

El taxista, como profesional, debe tomar un seguro de responsabilidad civil profesional.

Además, debe sacar un seguro para el vehículo, indicando:

- El nombre y la dirección de la compañía de seguros
- La identidad del asegurado
- El número de la póliza de seguro;
- El período de seguro
- Las características del vehículo.

*Para ir más allá*: Artículos L. 3120-4 y R. 3120-4 del Código de Transporte y Sección R. 211-15 del Código de Seguros.

### Sanciones

**Sanciones administrativas**

El profesional incurre en sanciones administrativas si:

- No explota el permiso de estacionamiento de forma efectiva o continua;
- grave o repetidamente infringe su autorización o normativa aplicable a los taxistas.

Si es necesario, la autoridad que ha emitido su permiso de estacionamiento puede proceder con su retiro final o temporal y emitir una advertencia.

*Para ir más allá*: Artículos L. 3124-1 a L. 3124-5 del Código de Transporte.

**Sanciones penales**

El vehículo utilizado por el profesional para el ejercicio de su actividad deberá estar equipado con los siguientes equipos:

- un medidor horolométrico (taxímetro);
- un dispositivo de luz exterior marcado como "taxi"
- Una placa anexa al vehículo y visible desde el exterior indicando el número del permiso de estacionamiento y la jurisdicción geográfica de dicho permiso;
- Una impresora para proporcionar facturación al cliente
- un terminal de pago electrónico.

Si el vehículo no tiene todo lo anterior, el profesional incurre en una multa de 450 euros.

El profesional también incurre en una multa, cuyo importe varía en función de la naturaleza del delito cometido, siempre y cuando:

- no cumple con las tarifas de las tarifas de taxi (cf.[Decreto](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000031285847&categorieLien=cid) No. 2015-1252 de 7 de octubre de 2015 relativo a las tarifas de taxi;
- informa al cliente de su disponibilidad y ubicación sin tener el permiso de estacionamiento;
- investiga a sus clientes;
- no cumple con todas las disposiciones aplicables a los vehículos de taxi o conductores de dichos vehículos;
- no está en condiciones de proporcionar un seguro a las fuerzas del orden en caso de un chequeo;
- no puso su tarjeta de visita en su vehículo o presentarlo durante un control policial.

*Para ir más allá*: Artículos R. 3124-2 y R. 3124-3 y artículos R. 3124-11 a R. 3124-13 del Código de Transporte.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de permiso de estacionamiento

**Autoridad competente**

Para obtener un permiso de estacionamiento, el profesional debe registrarse en una lista de espera. La solicitud de autorización deberá dirigirse al alcalde del lugar en el que el profesional desee ejercer o al prefecto de la policía para los profesionales que deseen ejercer en París.

Para estar en una lista de espera, el profesional:

- Debe tener una tarjeta de visita.
- No debe estar en posesión de un permiso de estacionamiento;
- no debe colocarse en otra lista de espera.

**Documentos de apoyo**

Para obtener más información sobre los términos de la solicitud, es aconsejable acercarse al ayuntamiento de que se trate.

**Tenga en cuenta que**

La autoridad responsable de la expedición de las autorizaciones también podrá imponer signos distintivos comunes a todos los taxis (color).

**Resultado del procedimiento**

El profesional sólo podrá llevar a cabo su actividad en la jurisdicción mencionada en su autorización. La validez de esta autorización es de cinco años renovables.

*Para ir más allá*: Artículo L. 3121-1- a L. 3121-8 y R. 3121-4, R. 3121-12 a R. 3121-13 del Código de Transporte.

### b. Solicitud de tarjeta de visita

**Autoridad competente**

El profesional debe dirigir su solicitud al prefecto de departamento o al prefecto de policía en su área de competencia.

**Documentos de apoyo**

Es aconsejable acercarse a la prefectura en la que el profesional desea ejercer para conocer la lista de piezas que se proporcionarán en el momento de la solicitud.

**hora**

La prefectura emite la tarjeta de visita en un plazo de tres meses a partir de la presentación de la solicitud.

*Para ir más allá*: Artículos L. 3120-2-2 y R. 3121-16 del Código de Transporte

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

