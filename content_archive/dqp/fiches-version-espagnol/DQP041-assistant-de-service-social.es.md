﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP041" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Social" -->
<!-- var(title)="Asistente de servicios sociales" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="social" -->
<!-- var(title-short)="asistente-de-servicios-sociales" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/social/asistente-de-servicios-sociales.html" -->
<!-- var(last-update)="2020-04-15 17:22:30" -->
<!-- var(url-name)="asistente-de-servicios-sociales" -->
<!-- var(translation)="Auto" -->


Asistente de servicios sociales
===============================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:30<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Asistente de Servicios Sociales (SSA) apoya a individuos, familias o grupos en dificultad ayudándoles a integrarse social o profesionalmente.

Trabajando en colaboración con médicos, educadores especializados y servicios de ayuntamientos, los acompaña y les informa sobre sus derechos, les dice qué hacer con la vivienda, la educación infantil o la salud.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El acceso a la profesión de SSA está reservado al titular del Diploma de Asistente de Servicios Sociales (DEASS). Una vez en posesión de este título, el profesional tendrá que registrarlo en el[Directorio de Adeli](https://www.service-public.fr/professionnels-entreprises/vosdroits/R33005) ("Automatización de Listas").

*Para ir más allá*: Artículos L. 411-1 y L. 411-2 del Código de Acción Social y Familias.

#### Entrenamiento

Los candidatos a THE DEASS deben ser precalificados con un título al menos de nivel IV (grado de grado, certificado de técnico o certificado profesional).

Tan pronto como justifiquen uno de estos títulos, estarán sujetos a un concurso que les permitirá integrar la formación en una institución de formación. El candidato tendrá que aprobar una prueba de elegibilidad por escrito y luego dos exámenes de ingreso.

Una vez que haya integrado la formación, seguirá una enseñanza teórica de 1.740 horas y doce meses de prácticas, de las cuales 110 horas se dedicarán a las relaciones entre instituciones formativas y sitios de clasificación.

**Qué saber**

El DEASS también se puede obtener a través del sistema de validación de experiencia ([Vae](https://www.pole-emploi.fr/file/mmlelement/pj/bb/7f/de/a3/vae_ministere_affaires_sociales54618.pdf)).

*Para ir más allá*: Artículos D. 451-29 y los siguientes artículos del Código de Acción Social y Familias; de 29 de junio de 2004 relativo al Diploma Estatal de Asistente de Servicios Sociales.

#### Costos asociados con la calificación

Se paga la formación que conduzca a la emisión de DEASS. Para más información, es aconsejable acercarse a los establecimientos dispensadores.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (Servicio Gratuito)

Un nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), que opera legalmente como SSA en uno de estos Estados, puede utilizar su título profesional en Francia de forma temporal o casual.

Tendrá que hacer una solicitud previa de actividad con el Ministro responsable de Asuntos Sociales (véase infra "5o. a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)").

**Qué saber**

El ejercicio de la SSA, de forma temporal o ocasional, exige que el nacional posea todas las habilidades linguísticas necesarias.

*Para ir más allá*: Artículo L. 411-1-1 del Código de Acción Social y Familias.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

El nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente:

- si posee un certificado de formación expedido por una autoridad competente de otro Estado miembro, que regula el acceso a la profesión o su ejercicio;
- Si posee un certificado de formación expedido por un tercer Estado pero reconocido por un Estado de la UE o del EEE;
- si ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en un Estado de la UE o del EEE que no regula la formación o la práctica.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar un certificado de capacidad del Ministro responsable de Asuntos Sociales (véase más adelante "5 grados). b. Obtener un certificado de capacidad para el nacional de la UE o del EEE para un ejercicio permanente (LE)).

Si, durante el examen de la solicitud, el Ministro responsable de Asuntos Sociales constata que existen diferencias sustanciales entre la formación y la experiencia profesional del nacional y de las exigidas en Francia, las medidas de compensación se puede tomar (ver infra "5 grados. b. Bueno saber: medidas de compensación").

**Qué saber**

El ejercicio de la SSA, de forma permanente, exige que el nacional posea todos los conocimientos de idiomas necesarios.

*Para ir más allá*: Artículo L. 411-1 del Código de Acción Social y Familias.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

La SSA está obligada por el secreto profesional como parte de sus misiones. Si incumple esta obligación, podrá ser procesado y castigado con una pena de un año de prisión y una multa de 15.000 euros.

*Para ir más allá*: Artículos L. 411-3 del Código de Acción Social y Familias y L 226-13 del Código Penal.

4°. Registro y seguro
------------------------------------------

### a. Registro en el directorio adeli

La SSA está obligada a registrar su diploma en el directorio Adeli ("Automatización de Listas").

**Autoridad competente**

El registro en el directorio de Adeli se realiza con la Agencia Regional de Salud (ARS) del lugar de práctica.

**hora**

La solicitud de inscripción se presenta en el plazo de un mes a partir de la toma del cargo de la SSA, independientemente del modo de práctica (liberal, asalariado, mixto).

**Documentos de apoyo**

En apoyo de su solicitud de registro, la SSA debe proporcionar un expediente que contenga:

- el diploma o título original que acredite la formación en psicología expedida por el Estado de la UE o el EEE (traducido al francés por un traductor certificado, si procede);
- Id
- El formulario[Cerfa 12269*02](https://www.service-public.fr/professionnels-entreprises/vosdroits/R33005) completado, fechado y firmado.

**Resultado del procedimiento**

El número Adeli del nacional se mencionará directamente en la recepción del expediente, emitido por el ARS.

**Costo**

Gratis.

### b. Obligación de contrato de seguro de responsabilidad civil profesional

En caso de ejercicio liberal, la SSA está obligada a conllevar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante esta actividad.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)

**Autoridad competente**

El Ministro responsable de los asuntos sociales es responsable de la emisión de la declaración previa de la actividad.

**Documentos de apoyo**

El nacional deberá presentar por cualquier medio un expediente que contenga los siguientes documentos justificativos:

- Apéndice V de la[decreto del 31 de marzo de 2009](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000020506757) ;
- Una identificación válida
- cualquier documento que justifique sus cualificaciones profesionales;
- cualquier documento que justifique el seguro de responsabilidad civil profesional.

**Procedimiento**

Una vez que el Ministro responsable de Asuntos Sociales haya recuperado los documentos justificativos, decidirá sobre la solicitud de declaración y registrará al nacional en la lista de la SSA en el directorio Adeli.

*Para ir más allá*: Artículos L. 411-1-1, R. 411-7 y R. 411-8 del Código de Acción Social y Familias.

### b. Obtener un certificado de capacidad para el nacional de la UE o del EEE para un ejercicio permanente (LE)

**Autoridad competente**

El Ministro responsable de asuntos sociales es responsable de expedir el certificado de capacidad para ejercer para el nacional que desea establecerse en Francia.

**Documentos de apoyo**

El nacional enviará, por correo postal, un expediente a la dirección regional de un centro de examen interregional que entregue DEASS, incluidos los siguientes documentos justificativos:

- Una copia del documento de identidad válido
- Una copia del título de formación obtenido y su traducción al francés;
- en caso necesario, un certificado que justifique que ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en un Estado de la UE o del EEE que no regula la formación o el ejercicio de la profesión;
- Un documento en el que se esboza la formación y el contenido de los estudios y prácticas realizados;
- Una descripción de las principales características del título de capacitación de la Lista I del decreto del 31 de marzo de 2009;
- Un CV escrito a mano
- un correo del nacional en el que designa una institución de formación que emitirá un dictamen técnico sobre la comparación de la formación y las aptitudes atestiguadas por el DEASS y el contenido de la formación seguida por el interesado.

El Director Regional confirmará la recepción del expediente en el plazo de un mes y decidirá si utiliza o no una medida de compensación en un plazo de cuatro meses a partir de la recepción del expediente.

Si el dictamen técnico es favorable, el director regional enviará al nacional una recepción de la exhaustividad de su solicitud de certificación de la capacidad de práctica.

**Bueno saber: medidas de compensación**

En caso de diferencias sustanciales entre la formación del nacional y el ejercicio de la profesión y lo que se exige en Francia, la autoridad competente podrá someterlo a una medida de compensación que pueda ser una prueba de aptitud o una pasantía. Adaptación.

**Prueba de aptitud**

Incluye preguntas escritas y orales, así como ejercicios prácticos, que tratan sobre el conocimiento esencial del ejercicio de la profesión, incluida la política social, y reglamentos sobre el acceso a la ley.

**Curso de adaptación**

Se llevará a cabo bajo la responsabilidad de un profesional cualificado.

*Para ir más allá*: Artículos R. 411-3 y los siguientes artículos del Código de Acción Social y Familias;[decreto del 31 de marzo de 2009](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020506757&dateTexte=20180112) condiciones de acceso a la profesión de asistente de servicios sociales para los titulares de títulos extranjeros.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

