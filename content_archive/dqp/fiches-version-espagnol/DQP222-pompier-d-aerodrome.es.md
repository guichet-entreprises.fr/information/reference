﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP222" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sector aéreo" -->
<!-- var(title)="Bombero del aeródromo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sector-aereo" -->
<!-- var(title-short)="bombero-del-aerodromo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sector-aereo/bombero-del-aerodromo.html" -->
<!-- var(last-update)="2020-04-15 17:22:14" -->
<!-- var(url-name)="bombero-del-aerodromo" -->
<!-- var(translation)="Auto" -->


Bombero del aeródromo
=====================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:14<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El bombero del aeródromo es un profesional que trabaja en el servicio de rescate y extinción de incendios de aeronaves en los aeródromos (SSLIA).

Como tal, su misión es:

- proporcionar socorro, protección y prevención contra incendios y accidentes que amenacen la seguridad de las personas y los bienes (aviones y su medio ambiente) en un aeródromo especificado;
- Evacuación y primeros auxilios a los heridos;
- cuando no se supervisa un vuelo:- Vehículos SSLIA,
  - para llevar a cabo entrenamiento deportivo para mantener buenas habilidades físicas,
  - para capacitar y asesorar a nuevos reclutas.

*Para ir más allá*: Artículo D. 213-1-5 del Código de Aviación Civil; Sección L. 6332-3 del Código de Transporte.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La persona que desee ejercer la profesión de bombero en una SSLIA debe ser autorizada por el prefecto del lugar en el que se encuentra el aeródromo.

*Para ir más allá*: Artículo D. 213-1-6 del Código de Aviación Civil; Artículo 10, párrafo I, de la[decretado a partir del 18 de enero de 2007](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=F2DA70ACD6746F7232B96B0021A84504.tplgfr37s_3?cidTexte=JORFTEXT000000464146&dateTexte=20170511) sobre las normas técnicas aplicables al servicio de rescate y extinción de incendios de aeronaves en los aeródromos, modificada por la Orden de 22 de diciembre de 2015 sobre el reconocimiento de las cualificaciones profesionales para la profesión de bombero del aeródromo.

#### Aprobación

Para operar en un aeródromo específico, el bombero debe solicitar la aprobación del prefecto del sitio del aeródromo, siempre y cuando cumpla con todas las siguientes condiciones:

- Tener materiales básicos validados y módulos de rescate personal y de fuego;
- un certificado médico expedido por uno de los siguientes médicos:- el médico del departamento médico de la Dirección General de Aviación Civil,
  - el médico del departamento de salud y rescate médico del Departamento de Bomberos y Rescate,
  - El médico aprobado por uno de los dos servicios anteriores o por el prefecto;
- Tener una licencia o permiso válido para conducir vehículos OSLIA o barcos en el aeródromo;
- un certificado de formación expedido por el operador del aeródromo que certifica el seguimiento de la formación local, cuyo programa está previsto en el párrafo 2 del Anexo II de la orden del 18 de enero de 2007.

Sin embargo, a propuesta del operador de este aeródromo, el interesado también podrá obtener la validación de su formación previa a partir de entonces:

- Posee la referencia adicional a la "seguridad civil y corporativa";
- ha tenido por menos de dos años:- servido en un servicio de bomberos y rescate como bombero,
  - una unidad militar de extinción de incendios y garantiza un entrenamiento específico,
  - formado como voluntario en la función pública del bombero,
  - obtuvo el Certificado Nacional de Jóvenes Bomberos.

*Para ir más allá*: Artículo 10, párrafo I, del Decreto de 18 de enero de 2007 y apéndice II, párrafo I, del Decreto de 18 de enero de 2007.

#### Entrenamiento

La formación que conduce a la profesión de bombero del aeródromo es proporcionada por el Centro de Entrenamiento de Brigadas de Bomberos del Aeropuerto Francés (C2FPA).

Está abierto a los candidatos:

- titulares de licencias de conducir válidas para las categorías de vehículos SSLIA;
- justificando un certificado médico de menos de un año que certifique sus capacidades físicas;
- validó el entrenamiento inicial de un bombero voluntario.

Sólo una docena de candidatos por sesión pueden unirse al C2FPA y continuar la capacitación que incluye un núcleo común y capacitación local.

La formación básica común se centra en los siguientes objetivos:

- Conocer las reglas generales de aviación y aeronaves
- conocer las regulaciones de SSLIA, incluidas las relativas a los extintores de incendios utilizados, los vehículos SSLIA, el reabastecimiento de combustible de aeronaves, la protección del personal, las tácticas de extinción de incendios de aeronaves y las técnicas de respuesta riesgos especiales.

El entrenamiento básico de 105 horas se validará después de un examen escrito y una prueba física.

A continuación, se impartirá formación local dentro del aeródromo, donde el candidato desea ejercer y se centrará plenamente en el conocimiento del aeródromo.

*Para ir más allá*: Apéndice II de la orden del 18 de enero de 2007.

#### Costos asociados con la calificación

La formación que conduce a la profesión de bombero del aeródromo vale la pena. Para obtener más información, es aconsejable ponerse en contacto con el[C2FPA](http://www.c2fpa.fr/pages/formations-sauvetage-incendie-daeronefs-2/).

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Un nacional de un Estado o parte de la Unión Europea (UE) en el Espacio Económico Europeo (EEE), establecido y que lleve a cabo legalmente las actividades de bomberos de aeródromos en uno de estos Estados, puede llevar a cabo la misma actividad en Francia con carácter temporal y Ocasional.

Tendrá que hacer una declaración al prefecto del lugar donde se encuentra el aeródromo en el que desea realizar el servicio (ver infra "5o. a. Obtener una licencia para ejercer para el nacional un ejercicio temporal o casual (LPS)").

Cuando el examen de las cualificaciones profesionales muestre diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y a su práctica en Francia, el interesado podrá someterse a una prueba de aptitud.

**Tenga en cuenta que**

El prefecto podrá autorizar al nacional a realizar únicamente determinadas tareas de su actividad en Francia, en el marco del acceso parcial, siempre que cumpla las siguientes condiciones:

- justifican las cualificaciones necesarias para el ejercicio de su actividad en el Estado de origen;
- cuando existan diferencias entre la actividad realizada en su Estado de origen y la que en Francia, que las medidas de compensación adoptadas contra él equivaldrían a una formación completa;
- la actividad del nacional puede separarse de la de la profesión de bombero del aeródromo en Francia;
- la actividad nacional puede llevarse a cabo de forma independiente en Francia.

**Qué saber**
El prefecto podrá imponer un control del conocimiento de la lengua al nacional cuando lo considere necesario.

*Para ir más allá*: Artículo 10 ter de la orden del 18 de enero de 2007.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

El nacional de un Estado miembro de la UE o parte en el EEE, establecido y que lleve a cabo legalmente las actividades de bomberos de aeródromos en uno de estos Estados, podrá llevar a cabo la misma actividad en Francia de forma permanente.

Tendrá que solicitar la aprobación del prefecto del lugar donde se encuentra el aeródromo en el que desea establecerse (ver infra "5o. b. Obtener la acreditación para el nacional para un ejercicio permanente (LE)).

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y su ejercicio en Francia, el interesado podrá estar sujeto a una medida de compensación (cf. infra "5 grados. b. Bueno saber: medidas de compensación").

**Tenga en cuenta que**

El prefecto podrá autorizar al nacional a realizar únicamente determinadas tareas de su actividad en Francia, en el marco del acceso parcial, siempre que cumpla las siguientes condiciones:

- justifican las cualificaciones necesarias para el ejercicio de su actividad en el Estado de origen;
- cuando hay diferencias entre su actividad en su estado de origen y la de Francia, que las medidas de compensación adoptadas en su contra equivaldrían a una formación completa;
- la actividad del nacional puede separarse de la de la profesión de bombero del aeródromo en Francia;
- la actividad nacional puede llevarse a cabo de forma independiente en Francia.

**Qué saber**

El prefecto podrá imponer un control del conocimiento de la lengua al nacional cuando lo considere necesario.

*Para ir más allá*: Artículo 10 II de la orden de 18 de enero de 2007.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Aunque no están reguladas, las reglas éticas pueden ser responsabilidad del bombero del aeródromo, incluyendo el respeto, el buen carácter o la solidaridad.

4°. Formación continua
-------------------------------------------

El mantenimiento de la certificación otorgada al bombero del aeródromo está sujeto a la obligación de someterse a una formación profesional continua por parte del titular.

Como parte de esta formación, el bombero del aeródromo deberá llevar a cabo cursos de formación teórica y práctica, así como cursos de educación continua, cuyos modalidades se especifican en los párrafos III-A y III-B de la[Apéndice II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=8792DE5D64EE31B4A7CF060E1B45EBB4.tplgfr24s_3?idArticle=LEGIARTI000034828830&cidTexte=LEGITEXT000021506419&dateTexte=20171127) 18 de enero de 2007. En el caso de que el bombero no cumpla con estas dos condiciones, tendría su acreditación suspendida hasta que se lleven a cabo estos entrenamientos.

Además, la licencia podrá ser retirada del bombero del aeródromo tan pronto como:

- no está en posesión de un certificado médico que lo haga físicamente apto para ejercer la profesión;
- completaron 144 horas de servicio o 24 vacaciones (para aeródromos con un período de tres meses)[nivel de protección](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=8792DE5D64EE31B4A7CF060E1B45EBB4.tplgfr24s_3?idArticle=LEGIARTI000021506448&cidTexte=LEGITEXT000021506419&dateTexte=20171127) menos de 6), excepto en el caso de una actualización certificada por el gerente de SSLIA, después de una ausencia de seis meses.

*Para ir más allá*: Artículo 12 y Apéndice II de la orden de 18 de enero de 2007.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Obtener una licencia para ejercer para el nacional para un ejercicio temporal o casual (LPS)

**Autoridad competente**

El prefecto del departamento en el que el aeródromo en el que el nacional desea prestar el servicio es responsable de decidir sobre la declaración de autorización para ejercer.

**Documentos de apoyo**

Para llevar a cabo el servicio en Francia, el nacional debe transmitir al prefecto, por cualquier medio, un expediente que contenga los siguientes documentos justificativos:

- una declaración escrita en la que se informa a la persona que ejerce un servicio en Francia;
- un certificado médico expedido por uno de los siguientes médicos:- el médico del departamento médico de la Dirección General de Aviación Civil,
  - el médico del departamento de salud y rescate médico del Departamento de Bomberos y Rescate,
  - El médico aprobado por uno de los dos servicios anteriores o por el prefecto;
- una o más fotocopias de las licencias válidas que permiten el funcionamiento de vehículos o barcos SSLIA ubicados en el aeródromo donde el nacional realizará el servicio;
- Un certificado de reconocimiento de la competencia de uno cuando la profesión está regulada en la UE o en el Estado del EEE;
- un certificado que justifique su actividad durante al menos un año en los últimos diez años, cuando ni la formación ni el ejercicio de la profesión estén regulados en el Estado miembro;
- un certificado de formación expedido por el operador del aeródromo cuando el nacional haya completado el programa de formación local previsto en el párrafo A-2 de la Lista II de la orden del 18 de enero de 2007.

**hora**

El prefecto regional tiene un mes desde el momento en que se recibe el archivo para tomar su decisión, que puede ser:

- permitir que el nacional preste su primer servicio;
- someter a la persona a una medida de compensación en forma de prueba de aptitud, si resulta que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de las requeridas para el ejercicio de la profesión en Francia. La decisión sobre si autorizar el beneficio debe tener lugar dentro de un mes de la prueba;
- informarles de una o más dificultades que puedan retrasar la toma de decisiones. En este caso, tendrá dos meses para decidir a partir de la resolución de las dificultades.

*Para ir más allá*: Artículos 10 ter y 13 de la orden de 18 de enero de 2007.

### b. Obtención de una licencia para el nacional para un ejercicio permanente (LE)

**Autoridad competente**

El prefecto del departamento donde se encuentra el aeródromo, en el que el nacional desea ejercer como bombero, es competente para decidir sobre la solicitud de acreditación.

**Documentos de apoyo**

Para establecerse en Francia, el nacional debe presentar al prefecto, por cualquier medio, un expediente que contenga los siguientes documentos justificativos:

- un certificado médico expedido por uno de los siguientes médicos:- médico de la Dirección General de Aviación Civil,
  - médico del Departamento de Bomberos y Rescate,
  - un médico autorizado por uno de los dos servicios anteriores o por el prefecto;
- una o más fotocopias de las licencias válidas que permiten el funcionamiento de vehículos o barcos SSLIA ubicados en el aeródromo donde el nacional realizará el servicio;
- Un certificado de reconocimiento de la competencia de uno cuando la profesión está regulada en la UE o en el Estado del EEE;
- un certificado de formación expedido por el operador del aeródromo cuando el nacional haya completado el programa de formación local previsto en el párrafo A-2 de la Lista II de la orden del 18 de enero de 2007;
- un certificado que justifique su actividad durante al menos un año en los últimos diez años, cuando ni la formación ni el ejercicio de la profesión están regulados en el Estado miembro.

**Bueno saber: medidas de compensación**

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a la medida de compensación de su elección, que puede ser:

- un curso de adaptación, a veces con formación adicional, que se evaluará al final del proyecto;
- una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

*Para ir más allá*: Artículo 10, apartado II, de la orden de 18 de enero de 2007.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

