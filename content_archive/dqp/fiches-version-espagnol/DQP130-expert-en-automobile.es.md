﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP130" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Experto en automoción" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="experto-en-automocion" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/experto-en-automocion.html" -->
<!-- var(last-update)="2020-04-15 17:21:07" -->
<!-- var(url-name)="experto-en-automocion" -->
<!-- var(translation)="Auto" -->


Experto en automoción
=====================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:07<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El experto en automoción es un profesional cuyo trabajo es preparar informes de expertos. Así pues, interviene principalmente en el contexto de los procedimientos de "vehículos dañados" regulados por las disposiciones de los artículos L. 327-1 a L. 327-6 y R.327-1 a R.327-6 de la Ley de Tráfico de Carreteras. Estos procedimientos consisten en los procedimientos de "vehículos gravemente dañados" (VGE) y "Vehículos económicamente irreparables" (EVR).

También es responsable de aplicar el procedimiento de reentrada de estos vehículos como parte del procedimiento VGE.

Como resultado, el experto desempeña un papel importante en la seguridad vial. Tiene la obligación de informar al propietario del vehículo si es probable que ponga en peligro la vida del conductor u otros (artículo R. 326-2 de la Ley de Tráfico por Carretera). Interviene principalmente cuando un vehículo está involucrado en un accidente y muy a menudo al ser ordenado por la aseguradora del propietario del vehículo. Por lo general, es la aseguradora del vehículo afectado la que designa al experto en automóviles en el contexto de una garantía de daños (todos los accidentes, colisiones, robos, etc.) como en la de una garantía de responsabilidad. El experto entonces en nombre de la compañía de seguros de los actos materiales de especialización (identificación del vehículo, registro de daños e imputación de estos, definición de una metodología de reparación, encriptación del costo de la rehabilitación y el valor de la vehículo, si es necesario) sin el poder de representación. Relaciones entre aseguradoras y por lo tanto, los expertos son a menudo fundamentales para la práctica de la profesión.

Este profesional puede estar obligado a ejercer en una firma de expertos, con una compañía de seguros, con expertos forenses, con bufetes de abogados, la administración estatal o individuos.

De conformidad con lo dispuesto en la Sección D. 114-12 del Código de Relaciones Públicas y la Administración, cualquier usuario podrá obtener un certificado de información sobre las normas aplicables a la profesión automotriz. Para ello, debe enviar la solicitud a la siguiente dirección: devenir-expert-automobile@interieur.gouv.fr.

*Para ir más allá*: Artículo L. 326-4 de la Ley de Tráfico de Carreteras.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

Para ejercer como experto en automoción y obtener la cualificación para el control de vehículos dañados, el profesional debe:

- Tener un título en expertos en automoción
- ser incluidos en una lista nacional de expertos en automoción establecida por el Ministro de Transportes (véase más adelante "5 grados). a. Solicitud de inclusión en la lista de expertos en automoción");
- han sido capacitados para actualizar los conocimientos jurídicos y técnicos necesarios para llevar a cabo procedimientos para vehículos dañados. Sin esta formación y obteniendo esta titulación EV, el experto no podrá intervenir en los procedimientos ve.

*Para ir más allá*: Artículo R. 326-11 de la Ley de Tráfico de Carreteras.

**Tenga en cuenta que**

Se considera que los profesionales con un grado de experto en automoción de menos de un año de edad han completado la formación específica.

*Para ir más allá* : orden de 26 de julio de 2011 relativa a la obtención y mantenimiento de la cualificación para el control de vehículos dañados para expertos en automoción.

#### Legislación nacional

#### Entrenamiento

**Diploma de experto en automóviles**

Para ser reconocido como profesionalmente calificado, el profesional debe tener un grado de experto en automoción de Nivel III (B.A. 2). Este diploma confiere la calidad de experto en automoción, pero no es suficiente por sí solo para permitir el ejercicio de esta actividad (ver infra "5 grados. a. Solicitud de inclusión en la lista de expertos en automoción").

Este diploma está disponible para ambos candidatos calificados:

- un grado de Nivel IV (bac) o un grado certificado de este nivel;
- experiencia de al menos tres años en la práctica de la reparación de automóviles;
- por lo menos dos años de experiencia de una actividad experta como pasante con alguien con el estatus de experto en automoción.

**Tenga en cuenta que**

La duración de esta experiencia laboral se reduce a un año para aquellos con los siguientes diplomas:

- certificado de técnico superior (BTS) en el campo de la automoción o maquinaria agrícola;
- un título universitario en tecnología
- un título de ingeniería.

La lista de estos diplomas figura en el Apéndice I de la Orden de 31 de julio de 2012 que define el diploma de experto en automoción disponible en el[Boletín oficial](http://cache.media.education.gouv.fr//file/34/83/4/BO_MEN_20-09-12_226834.pdf) Departamento de Educación al 20 de septiembre de 2012.

Este diploma se otorga a los candidatos que han completado con éxito un examen que consiste en pruebas escritas y trabajo práctico y oral en unidades docentes. El programa de estas unidades se establece en el[Apéndice IIa](http://www.enseignementsup-recherche.gouv.fr/pid20536/bulletin-officiel.html?cid_bo=61279&cbo=1) 31 de julio de 2012, disponible en el Boletín Oficial anterior.

*Para ir más allá* Decreto 95-493, de 25 de abril de 1995, por el que se establece y regula el diploma de experto en automoción; de 31 de julio de 2012 por el que se establece la definición de diploma de experto en automoción.

**Actualización del conocimiento**

Los profesionales calificados profesionalmente deben capacitarse para actualizar sus conocimientos legales y técnicos necesarios para llevar a cabo procedimientos para vehículos dañados.

Esta formación de un día es imparte por organizaciones de formación acreditadas por el Ministro responsable del transporte. Incluye una parte administrativa y una parte técnica cuyo programa se establece en el[Apéndice 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0893E48E42291FCC14A543F7FC009173.tplgfr25s_1?idArticle=LEGIARTI000024423600&cidTexte=LEGITEXT000024423570&dateTexte=20180522) 26 de julio de 2011.

Al final de esta formación, un certificado de seguimiento de la formación, cuyo modelo se establece en el[Apéndice 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0893E48E42291FCC14A543F7FC009173.tplgfr25s_1?idArticle=LEGIARTI000024423600&cidTexte=LEGITEXT000024423570&dateTexte=20180522) se le da al profesional.

Una vez que el profesional cumple con estos requisitos de calificación, debe solicitar el registro en la lista nacional de expertos en automoción (ver infra "5o. a. Solicitud de inclusión en la lista de expertos en automoción").

*Para ir más allá*: Artículo R. 326-11 de la Ley de Tráfico de Carreteras; de 26 de julio de 2011 relativo a la obtención y mantenimiento de la cualificación para el control de vehículos dañados para expertos en automoción.

#### Costos asociados con la calificación

Por lo general, se paga la formación que conduce a un diploma de experto en automoción y a la formación en actualización de conocimientos. Es aconsejable acercarse a los establecimientos aprobados para obtener más información.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido podrá realizar la misma actividad temporal y ocasional en Francia.

Cuando el Estado miembro no regule el acceso a la actividad o a su ejercicio, el nacional deberá justificar la realización de esta actividad durante un año en los últimos diez años.

Una vez que la persona cumpla con estas condiciones, la persona deberá hacer una declaración al Ministro responsable del transporte antes de la primera prestación de servicio.

*Para ir más allá*: Artículo L. 326-4 de la Ley de Tráfico de Carreteras; Secciones R. 326-5, R. 326-6, R. 326-8, R. 326-8-1 y R. 326-9 de la Ley de Tráfico de Carreteras.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Cualquier nacional de un Estado de la UE o eee legalmente establecido que sea un experto en automóviles puede llevar a cabo la misma actividad en Francia de forma permanente.

Para ello, el interesado debe inscribirse en la lista nacional de expertos en automoción, en las mismas condiciones que el nacional francés (véase infra "5o. a. Solicitud de inclusión en la lista de expertos en automoción").

*Para ir más allá*: Artículos L. 326-1 y L. 326-4 de la Ley de Tráfico por Carretera; Secciones R. 326-5, R. 326-6, R. 326-8, R. 326-8-1 y R. 326-9 de la Ley de Tráfico de Carreteras.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

**Condiciones de honorabilidad**

Para ser un experto en automoción, el profesional no debe haber sido objeto de ninguna condena por:

- Robo;
- Estafa;
- violación de la confianza
- Recepción;
- Agresiones sexuales;
- resta cometida por un custodio de la autoridad pública;
- falso testimonio;
- corrupción o tráfico de influencias;
- un delito menor castigado con robo, fraude o violación de la confianza.

*Para ir más allá*: Artículo L. 326-2 de la Ley de Tráfico de Carreteras.

**Incompatibilidades**

La actividad del experto en automoción es incompatible con:

- Ocupar una oficina de funcionarios públicos o ministeriales;
- actividades relacionadas con la producción, venta, alquiler, reparación y representación de vehículos de motor y piezas accesorias;
- Seguro.

*Para ir más allá*: Artículo L. 326-6 de la Ley de Tráfico de Carreteras.

**Reglas profesionales**

El experto en automoción está obligado a:

- indicar el precio del beneficio a la persona que busca sus servicios;
- dar sus conclusiones dentro de su misión, pero debe informar al propietario del vehículo de cualquier incumplimiento del vehículo que pueda poner en peligro la vida de otros;
- para preparar un informe de experto con lo siguiente, y enviar una copia al propietario del vehículo:- El nombre del experto que llevó a cabo la experiencia,
  - Todas las transacciones realizadas,
  - El nombre y la calidad de las personas presentes en la reunión de expertos,
  - Documentos proporcionados por el propietario,
  - Conclusiones del experto
- informar al propietario y custodio del vehículo tan pronto como se enteren de una disputa sobre las conclusiones técnicas del vehículo o el costo de daños o reparaciones.

Dado el alto nivel de cuestiones de seguridad vial relacionadas con el ejercicio de la profesión y las misiones de los expertos en automoción, el profesional incurre en una sanción disciplinaria en caso de mala conducta o incumplimiento de estas normas y sus obligaciones profesionales.

*Para ir más allá*: Artículos R. 326-1 a R. 326-4, Secciones R. 326-14 y D. 326-15 de la Ley de Tráfico de Carreteras.

4°. Seguros y sanciones
--------------------------------------------

**Seguro**

El experto en automoción debe tomar un seguro de responsabilidad civil.

*Para ir más allá*: Artículo L. 326-7 de la Ley de Tráfico de Carreteras.

**Sanciones**

Un profesional que trabaja como experto en automoción sin estar en la lista o profesionalmente cualificado se enfrenta a una pena de un año de prisión y una multa de 15.000 euros.

Además, en caso de condena por incumplimientos de honor o probidad, el profesional incurre en una prohibición temporal o permanente de ejercer su actividad.

*Para ir más allá*: Artículos L. 326-8 y L. 326-9 de la Ley de Tráfico por Carretera; Decreto de 5 de febrero de 2002 por el que se modifica el auto de 13 de agosto de 1974 sobre las condiciones mínimas del contrato de seguro de responsabilidad civil profesional que deben ser suscritos por expertos en automóviles; orden de 13 de agosto de 1974 relativa a las condiciones mínimas del contrato de seguro de responsabilidad civil profesional que deben ser suscritos por expertos en automóviles.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de inclusión en la lista de expertos en automoción

La inscripción en la lista nacional de expertos en automoción es obligatoria para poder llevar a cabo esta actividad profesional. It's que vale la pena comodidades.

**Autoridad competente**

El profesional debe solicitar al Ministro responsable del transporte.

**Documentos de apoyo**

Su solicitud debe incluir la siguiente información, en su caso, con su traducción al francés:

- su estado civil;
- una copia es:- un certificado de experto profesional en automoción (obtenido entre 1977 y 1994) o el reconocimiento de la condición de experto, previsto en el Decreto No 74-472, de 17 de mayo de 1974, relativo a los expertos en automoción, o el diploma del experto en automoción creado en 1995 (o su transcripciones),
  - un título expedido por un Estado miembro de la UE equivalente a los títulos anteriores,
  - cualquier documento que justifique su experiencia profesional en la experiencia automotriz;
- una declaración de honor que acredite que no tiene ningún cargo de funcionario público o ministerial y no ejerce ninguna profesión incompatible con el desarrollo de esa actividad;
- Una prueba de seguro de menos de tres meses
- un extracto del boletín 3 de su historial penal o un documento equivalente para los extranjeros, así como una declaración sobre el honor que justifique que no ha sido objeto de condenas penales (véase supra "3". Condiciones de honorabilidad") de menos de tres meses;
- prueba de que cumple con los requisitos de formación requeridos para esta actividad.

**Tiempo y procedimiento**

El Ministro responsable del transporte confirma la recepción de la solicitud en el plazo de un mes y decide sobre la solicitud en el plazo de tres meses a partir de la recepción del expediente completo.

Esta lista de expertos está disponible en el[Sitio web de seguridad vial](http://www.securite-routiere.gouv.fr/).

*Para ir más allá*: Artículos R. 326-5 y R. 326-13 de la Ley de Tráfico por Carretera; Decreto No 95-493, de 25 de abril de 1995, creación de portán y regulación general del diploma de experto en automoción; Decreto 74-472, de 17 de mayo de 1974, relativo a los expertos en automoción.

**Bueno saber: medida de compensación**

En caso de diferencias sustanciales entre la formación recibida por el profesional en el extranjero y la necesaria en Francia para llevar a cabo la actividad de experto en automoción, que pueda perjudicar la seguridad de las personas, el Ministro encargado del transporte podrá decidir someterse a una prueba de aptitud en un plazo máximo de seis meses o a un curso de ajuste de hasta dos años y realizado bajo la responsabilidad de un experto en automoción en la lista nacional.

*Para ir más allá* : decreto de 15 de junio de 2017 relativo al reconocimiento de las cualificaciones profesionales de los expertos en automoción; Artículos R. 326, R. 326-8, R. 326-8-16-8 y R. 326-8-1 de la Ley de Tráfico de Carreteras).

### b. Predeclaración del nacional de la UE para el ejercicio temporal y casual (LPS)

**Autoridad competente**

El profesional deberá presentar su solicitud por cualquier medio al Ministro responsable del transporte.

**Documentos de apoyo**

Su solicitud debe incluir los siguientes documentos, si los hubiere, con su traducción al francés:

- Un documento que justifica su identidad
- un certificado que certifique que está legalmente establecido en un Estado miembro de la UE para llevar a cabo la actividad de experto en automoción y no está prohibido ejercer;
- prueba de sus cualificaciones profesionales
- cuando el Estado miembro de la UE no regule el acceso a la profesión o a su ejercicio, demuestre que la persona ha dedicado a la práctica de supervisar las reparaciones y volver a entrar en vehículos durante al menos un año en los próximos diez años En los últimos años
- un documento que justifica que ha contratado un seguro de responsabilidad civil.

**Tiempo y procedimiento**

El Ministro responsable del transporte verifica los conocimientos del nacional en el plazo de un mes a partir de la recepción de la solicitud. Registra al nacional en la lista de expertos en automoción por un período de un año. Después de reconocer su cualificación profesional, el Ministro encargado del transporte podrá pedir al profesional que justifique su nivel de práctica en francés (artículo R. 326-7 del Código de Carreteras).

A falta de una respuesta del Ministro en el plazo de un mes a partir de la recepción de su solicitud, se considerará que el solicitante está en esa lista.

**Tenga en cuenta que**

Para renovar su registro, el nacional debe solicitar al Ministro responsable del transporte para la renovación y debe recibir la formación para actualizar sus conocimientos (véase más arriba "2. a. Actualización de conocimientos").

*Para ir más allá*: Artículo R. 326-6 de la Ley de Tráfico de Carreteras; Artículo 2 del Auto de 26 de julio de 2011.

### c. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

