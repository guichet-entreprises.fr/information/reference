﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP155" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Inseminator equino" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="inseminator-equino" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/inseminator-equino.html" -->
<!-- var(last-update)="2020-04-15 17:20:55" -->
<!-- var(url-name)="inseminator-equino" -->
<!-- var(translation)="Auto" -->


Inseminator equino
==================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:55<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El inseminator equino está a cargo de la inseminación artificial de yeguas.

Trabajando en un centro de inseminación artificial equina o directamente en criadores, su misión es recoger la semilla de los machos, tratarla en el laboratorio y luego proceder a la inseminación de las hembras.

Para obtener más información sobre las misiones del inseminator equino, es aconsejable[sitio del Instituto Francés de Equitación y Caballo](http://www.haras-nationaux.fr/information/accueil-equipaedia/formations-et-metiers/les-metiers-de-lelevage/inseminateur-equin.html) (IFCE).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Toda persona que desee convertirse en un inseminante equino debe haberse registrado ante la autoridad administrativa (el prefecto de la región en la que desea ejercer), que lo registrará en vista de la presentación de una licencia. inseminante.

Esta licencia sólo se puede emitir a los licenciatarios:

- Certificado de idoneidad para las funciones de inseminador equino expedido por una institución de formación especializada;
- uno de los títulos veterinarios mencionados en el artículo L. 241-2 del Código Rural y de la Pesca Marina;
- licencia de gerente del centro de inseminación artificial para especies equinas o asinas.

*Para ir más allá*: Artículos L. 653-13 y R. 653-87 del Código y Orden de Pesca Rural y Marina, de 21 de enero de 2014, relativo a los certificados de aptitud para el inseminador y el centro de inseminación artificial en especies equinas y asinas.

#### Entrenamiento

La licencia del inseminador está sujeta al certificado de aptitud expedido después de la formación en una de las siguientes instituciones:

- el centro de formación de yeguas del Instituto Francés de Equitación;
- El Centro de Educación Zootécnica de Rambouillet;
- una de las cuatro escuelas veterinarias nacionales.

El acceso a la formación puede ser limitado dependiendo del número de plazas disponibles y viene después de la decisión del director de una de estas instituciones.

La admisión se puede hacer:

- ya sea después de revisar el expediente de la persona, siempre que justifique un diploma de nivel IV o más del Ministerio de Agricultura, en el campo de la producción animal;
- o después de un control de los conocimientos adquiridos para:- Titulares de un certificado de aptitud para ser inseminador de otras especies animales,
  - personas que han trabajado en la agricultura equina durante al menos tres años.

**Qué saber**

El control del conocimiento cubre las asignaturas definidas en el Decreto del Programa I del 21 de enero de 2014 e incluye una entrevista motivacional.

La duración de la formación es de cinco semanas durante las cuales el candidato tendrá que alcanzar objetivos en fisiología reproductiva y biotecnología, zootecnia, regulación y relaciones de consultoría.

Al final de esta formación, el candidato tendrá que aprobar un examen consistente en pruebas teóricas, prácticas y orales.

La admisión se pronuncia si la persona ha obtenido una puntuación media de 12 sobre 20, sin puntuación inferior a 7 de 20 en una prueba teórica o 10 de 20 en la prueba práctica.

**Tenga en cuenta que**

Para conocer los cursos de formación que conducen a las profesiones de veterinario y jefe de centro de inseminación artificial equino o asine en Francia, es aconsejable consultar las fichas de calificación dedicadas a ellos.

*Para ir más allá* : ordenado a partir del 21 de enero de 2014.

#### Costos asociados con la calificación

El entrenamiento para convertirse en un inseminador equino vale la pena. Para obtener más información sobre los costes de formación, es aconsejable acercarse a instituciones especializadas.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (Servicio Gratuito)

El nacional de un Estado miembro de la UE o del EEE, que se dedica a la actividad de inseminator equino en uno de estos Estados, podrá utilizar su título profesional en Francia de forma temporal o ocasional. Debe solicitarlo, antes de su primera actuación, mediante declaración dirigida al prefecto de la región en la que desea ejercer (véase infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año, durante los diez años que preceder el rendimiento.

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones requeridas para el acceso a la profesión y su ejercicio en Francia, el interesado podrá ser sometido a una prueba de aptitud en un un mes después de la recepción por parte del prefecto de la solicitud de declaración.

*Para ir más allá*: Artículos L. 653-13, L. 204-1 y R. 204-1 del Código de Pesca Rural y Marina.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si:

- posee un certificado de formación o un certificado de competencia expedido por una autoridad competente de otro Estado miembro que regula el acceso a la profesión o a su ejercicio;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Una vez que cumpla una de las dos condiciones anteriores, tendrá que obtener la licencia de inseminator equino requerida para el ejercicio de la profesión, del prefecto regional competente. Para obtener más información, es aconsejable consultar el párrafo "5o" a continuación. b. Obtener una licencia para los nacionales de la UE o del EEE para un ejercicio permanente (LE).

Si, durante el examen del expediente, el prefecto constata que existen diferencias sustanciales entre la formación y la experiencia profesional del nacional y las necesarias para ejercer en Francia, podrán adoptarse medidas de compensación ("5 grados). b. Bueno saber: medidas de compensación").

*Para ir más allá*: ArtículoR. 204-2 y R. 653-96 del Código Rural y pesca marina.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Los veterinarios que llevan a cabo una misión de inseminación equina están sujetos a las reglas de ética y ética aplicables a la profesión.

Para los inseminadores distintos de los veterinarios, tienen obligaciones éticas, incluido el respeto del secreto profesional asociado con los datos de inseminación y para garantizar la salud y la dignidad del animal inseminado.

4°. Seguro
-------------------------------

En caso de ejercicio liberal, el inseminator equino tiene la obligación de conllevar un seguro de responsabilidad profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante esta actividad.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

#### Autoridad competente

El prefecto regional es responsable de decidir sobre la declaración previa.

#### Documentos de apoyo

La declaración previa del nacional deberá transmitirse por cualquier medio a la autoridad competente e incluir los siguientes documentos justificativos:

- Prueba de la nacionalidad del profesional
- un certificado que lo certifique:- está legalmente establecido en un Estado de la UE o del EEE,
  - ejerce una o más profesiones cuya práctica en Francia requiere la celebración de un certificado de capacidad,
  - no incurre en una prohibición de ejercer, aunque sea temporal, al expedir el certificado;
- prueba de sus cualificaciones profesionales
- cuando ni la actividad profesional ni la formación estén reguladas en la UE o en el Estado del EEE, se demuestre por ningún medio que el nacional haya participado en esta actividad durante un año, a tiempo completo o a tiempo parcial, en los últimos diez años.

Esta declaración anticipada incluye información relativa al seguro u otros medios de protección personal o colectiva suscritos por el solicitante de registro para cubrir su responsabilidad profesional.

Estos documentos se adjuntan, según sea necesario, a su traducción al francés.

#### hora

El prefecto regional tiene un mes desde el momento en que se recibe el archivo para tomar su decisión:

- permitir que el reclamante realice su beneficio.  En caso de incumplimiento de este plazo de un mes o del silencio de la administración, podrá llevarse a cabo la prestación de servicios;
- someter a la persona a una medida de compensación en forma de prueba de aptitud, si resulta que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de las requeridas para el ejercicio de la profesión en Francia;
- informarles de una o más dificultades que puedan retrasar la toma de decisiones. En este caso, dispondrá de dos meses para decidir, a partir de la resolución de las dificultades y, en cualquier caso, en un plazo máximo de tres meses a partir de la información del interesado sobre la existencia de la dificultad o dificultades. En ausencia de una respuesta de la autoridad competente dentro de estos plazos, puede comenzar la prestación de servicios.

*Para ir más allá*: Artículo R. 204-1 del Código Rural y Pesca Marina

### b. Obtener una licencia para los nacionales de la UE que realizan actividades permanentes (LE)

#### Autoridad competente

El prefecto regional del lugar de práctica es responsable de expedir la licencia que permite al nacional llevar a cabo permanentemente la actividad de inseminador equino en Francia.

#### Procedimiento

El nacional debe transmitir al prefecto todos los documentos necesarios para respaldar su solicitud de licencia, incluidos:

- prueba de nacionalidad
- Un certificado de formación o certificado de competencia adquirido en un Estado miembro de la UE o del EEE;
- cualquier prueba, en su caso, de que el nacional ha sido inseminante durante un año, a tiempo completo o a tiempo parcial, en un Estado miembro que no regula la profesión.

Por lo tanto, el prefecto tendrá un mes a partir de la recepción de las pruebas justificativas para confirmar la recepción o solicitar el envío de los documentos que faltan. La decisión de conceder la licencia tendrá lugar en un plazo de tres meses, prorrogado por un mes adicional en caso de que falten piezas. El silencio guardado en estos plazos valdrá la pena ser aceptado.

#### Bueno saber: medidas de compensación

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a la medida de compensación de su elección, que puede ser:

- un curso de adaptación de hasta tres años
- una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

*Para ir más allá*: Artículos R. 204-2 a R. 204-6 del Código Rural y Pesca Marina.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

