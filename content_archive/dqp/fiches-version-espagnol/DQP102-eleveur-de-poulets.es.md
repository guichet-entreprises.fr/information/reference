﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP102" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios agrícolas" -->
<!-- var(title)="Criador de pollos" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-agricolas" -->
<!-- var(title-short)="criador-de-pollos" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-agricolas/criador-de-pollos.html" -->
<!-- var(last-update)="2020-04-15 17:20:48" -->
<!-- var(url-name)="criador-de-pollos" -->
<!-- var(translation)="Auto" -->


Criador de pollos
=================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:48<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El agricultor de pollos de engorde es un profesional de una granja que se encarga de cuidar de los pollos de engorde (Gallus gallus) para la producción y venta de su carne.

**Tenga en cuenta que**

Esta hoja está relacionada únicamente con la producción de carne, por lo que no se discutirán las granjas de gallinas ponedoras. Además, sólo ciertas granjas están cubiertas por normas mínimas para proteger a los pollos.

No se ve afectado por estas disposiciones:

- granjas con menos de 500 pollos;
- Granjas con sólo pollos reproductores;
- criaderos;
- pollos orgánicos, pollos criados en el interior en un sistema extenso o pollos criados al aire libre, al aire libre o en la naturaleza.

Por otro lado, las granjas compuestas tanto por rebaños como por rebaños reproductores están sujetas a estos requisitos.

*Para ir más allá* : un decreto de 28 de junio de 2010 por el que se establecen normas mínimas para la protección de los pollos para la producción de carne.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ser un agricultor de pollos, el profesional debe:

- Estar calificado profesionalmente
- Mantener un registro de cría
- declarar la densidad de su funcionamiento, es decir, el peso total de los pollos en ella (ver infra "5 grados). d. Declaración de densidad ganadera").

Además, deben cumplirse determinadas obligaciones de funcionamiento, tales como:

- actividad del operador debe ser reportada (ver más abajo "5 grados). c. Declaración de actividad operativa");
- estándares mínimos para la protección de los engordes.

*Para ir más allá* : decreto de 28 de junio de 2010 supra; Artículo 4 de la Orden de 26 de febrero de 2008 sobre el control de las infecciones por Salmonella en los rebaños reproductores de las especies de Gallus gallus en la corriente de carne y se establecen los procedimientos de notificación de salmonelosis aviar contemplados en el artículo D. 223-1 Código Rural y Pesca Marítima en estos mismos rebaños.

#### Entrenamiento

**Certificado profesional individual de pollo de engorde agricultor**

Para ser reconocido como una justificación para un nivel de conocimientos de bienestar animal adquiridos durante el entrenamiento, el individuo debe tener un certificado individual como criador de pollos de engorde.

Para ello, deberá recibir un mínimo de 7 horas de formación sobre la legislación nacional y de la UE sobre la protección de los pollos y, en particular, sobre los siguientes puntos:

(a) Los apéndices I y II de la Directiva 2007/43/CE supra y de esta orden;

b) la fisiología de los animales, incluidas sus necesidades de alimentos y agua, su comportamiento y el concepto de estrés;

(c) los aspectos prácticos del manejo cuidadoso de los pollos, su captura, carga y transporte;

(d) atención de emergencia para pollos, matanzas y procedimientos de sacrificio de emergencia;

(e) medidas preventivas de bioseguridad.

Esta formación es impartida por una organización de formación aprobada por el Ministro responsable de la agricultura, cuya lista se establece en la Lista VII del decreto de 28 de junio de 2010.

Al final de su formación, la organización le emite un certificado de formación de acuerdo con el modelo establecido en el Apéndice VI de dicha orden.

El profesional debe entonces solicitar su certificado profesional (ver infra "5o. a. Solicitar un certificado profesional individual como criador de pollos de engorde").

*Para ir más allá*: Artículo 4 del auto de 28 de junio de 2010 supra.

#### Bueno saber: exención de formación

Cualquier criador que se haya establecido durante más de un año antes del 30 de junio de 2010 puede estar exento de la formación si así lo solicita el prefecto del departamento de su casa (ver infra "5o. b. Solicitud de expedición del certificado individual por renuncia").

Para ello, aporta pruebas de que lleva un mínimo de un año criando aves de corral de carne de proa. Esta evidencia puede ser cualquier documento escrito que mencione el nombre del criador y el nombre de la granja en la que practica o ha practicado.

*Para ir más allá*: Artículo 4 de la orden de 28 de junio de 2010.

#### Obligación de mantener un registro de cría

El propietario profesional de animales cuya carne o productos están destinados al consumo está obligado a declarar su actividad (véase infra "5.0. c. Declaración de Actividad Operativa") y debe mantener un registro para cada uno de los gallineros que indique:

- El número de pollos introducidos;
- La superficie utilizable
- raza híbrida o de pollo, si los conocen;
- durante cada cheque, el número de pollos encontrados muertos y las causas de la muerte, así como el número de pollos puestos a la muerte;
- el número de pollos que quedan en el rebaño después de la retirada de las aves para su venta o sacrificio.

Este registro debe ser mantenido durante al menos tres años por el criador u operador y debe ponerse a disposición de los servicios de control durante las inspecciones o cuando se solicite.

*Para ir más allá*: Artículo L. 234-1 del Código Rural y Pesca Marina; Apéndice I del auto de 28 de junio de 2010 supra.

#### Costos asociados con la calificación

El costo de la capacitación que conduce al certificado de práctica individual varía dependiendo de la duración y las organizaciones de capacitación. Es aconsejable acercarse a las instituciones interesadas para obtener más información.

### b. Nacionales de la UE: para un ejercicio temporal e informal (Beneficios gratuitos de los servicios (LPS)) o un ejercicio permanente (establecimiento libre (LE))

Un nacional de un Estado miembro de la UE o del EEE que sea agricultor de pollos en uno de estos Estados podrá utilizar su título profesional en Francia de forma temporal o ocasional. Debe hacer una declaración al prefecto de la región en la que desea practicar (véase más adelante "5o). a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que está legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año en los diez años que preceder el rendimiento.

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones requeridas para el acceso a la profesión y su ejercicio en Francia, el interesado podrá ser sometido a una prueba de aptitud en un un mes después de la recepción de la declaración por parte del prefecto. El nacional que desee establecerse en Francia está sujeto a las mismas disposiciones que el nacional francés (véase infra "5o. Pasos y formalidad del reconocimiento de cualificación").

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### Disposiciones ganaderas

Los agricultores de pollo están obligados a cumplir con todas las disposiciones de cría. Como tal, deben asegurarse de que estos animales:

- no ser privados de los alimentos o agua necesarios para satisfacer las necesidades fisiológicas de su especie y su grado de desarrollo, adaptación o domesticación;
- no quedar desatendido en caso de enfermedad o lesión;
- ambiente adecuado y no están sujetos a ningún maltrato.

Además, está prohibido para un criador administrar ciertas sustancias a pollos como anabólicos, anti-catálisis o beta-agonistas o para comercializar pollos que han sido probados. Medicado.

*Para ir más allá*: Artículos R. 214-17 y L. 234-2 del Código Rural y pesca marina.

### Disposiciones comunes a todas las granjas de pollos

Los criadores deben asegurarse de que sus gallineros cumplan con todos los requisitos aplicables a todas las granjas, incluyendo:

- Sistemas de alimentación animal
- Sistemas de ventilación y calefacción
- Iluminación de las instalaciones y limpieza del equipo utilizado;
- procedimientos quirúrgicos realizados en pollos.

Estas disposiciones figuran en la Lista I del auto de 28 de junio de 2010 supra.

### Reglas profesionales

Siempre y cuando las guías de buenas prácticas sean desarrolladas por organizaciones avícolas profesionales, el profesional está obligado a aplicarlas.

*Para ir más allá*: Artículo 5 del auto de 28 de junio de 2010 supra.

### Requisito de información de la cadena alimentaria

El agricultor de pollos de engorde deberá facilitar al administrador del matadero toda la información relativa a la cadena alimentaria (ICA) de los animales, al menos 24 horas antes de su expulsión.

Este documento debe ser coherente con un[Modelo](http://mesdemarches.agriculture.gouv.fr/spip.php?action=acceder_document&arg=426&cle=d7aab69f5c9bd2a15e38a6b3cc847bbb9cc92082&file=pdf%2FICA_Gallus_chair.pdf) y sigue siendo válido durante cinco días a partir de la fecha en que fue escrito. No obstante, su validez podrá prorrogarse si los animales del lote siguiente pertenecen a la misma banda y están destinados al mismo matadero.

Se debe mantener una copia de este documento dentro de la operación.

Si se incumplen estas obligaciones, el profesional es multado con 1.500 euros.

*Para ir más allá* : un decreto de 20 de marzo de 2009 sobre la forma de aplicar información sobre la cadena alimentaria para lotes de aves de corral y lagomorfos para su sacrificio para consumo humano.

4°. Sanciones
----------------------------------

### Responsabilidad

El criador es totalmente responsable del bienestar de los animales en su granja.

*Para ir más allá*: Artículo 1 del auto de 28 de junio de 2010 antes mencionado.

### Medidas de la policía administrativa

En caso de incumplimiento de las disposiciones sobre sustancias ilegales o si los productos representan un peligro para la salud pública, los veterinarios cualificados podrán ordenar la aplicación de medidas de policía administrativa, :

- Secuestro, censado o etiquetado de animales de granja;
- Controles sanitarios de los productos antes de su comercialización;
- el sacrificio y la destrucción de animales o sus productos;
- La destrucción de las sustancias administradas y de los productos en cuestión;
- Seguimiento de la granja durante el año siguiente al sacrificio de los animales;
- control del ganado de la granja y de todos los establecimientos que han estado en contacto con ella.

*Para ir más allá*: Artículos L. 234-3 y L. 234-4 del Código Rural y Pesca Marina.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de un certificado profesional individual como criador de pollos de engorde

#### Autoridad competente

El profesional debe presentar su solicitud al prefecto del departamento de su casa, a la dirección departamental encargada de la protección de la población (DDPP).

#### Documentos de apoyo

Su solicitud debe incluir:

- El formulario de declaración[Cerfa No. 14138*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14138.do) Completado y firmado;
- Una copia del certificado de formación.

#### Tiempo y procedimiento

Después de recibir la solicitud por completo, el prefecto del departamento expide el certificado profesional al solicitante, así como la documentación sobre la normativa relativa a la protección de los engordes. El profesional está obligado a leer y mantener esta documentación dentro de su registro de cría.

*Para ir más allá*: Artículo 4 del Auto de 28 de junio de 2010.

### b. Solicitud de expedición del certificado individual admitido en equivalencia

#### Autoridad competente

El profesional debe presentar su solicitud a su prefecto de departamento, al DDPP del departamento de su casa.

#### Documentos de apoyo

Su solicitud debe incluir:

- El formulario de declaración[Cerfa No. 14144*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14144.do) Completado y firmado;
- un documento que menciona su identidad y el nombre de la finca en la que ejercía, justificando que había participado en esta actividad durante más de un año antes del 30 de junio de 2010.

*Para ir más allá*: Artículo 4 del Auto de 28 de junio de 2010.

### c. Declaración de actividad operativa

#### Autoridad competente

Siempre y cuando su funcionamiento consista en más de 250 aves de corral de la especie Gallus gallus, el operador profesional, o en su defecto, el criador, está obligado a declarar su actividad con el DDPP del departamento donde se encuentran los talleres.

#### Documentos de apoyo

Su solicitud debe incluir:

- El formulario[Cerfa No. 13989*04](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13989.do) actividad de un operador de rebaño de aves de corral de la especie Gallus gallus o meleagris gallapavo completado y firmado;
- El formulario de declaración[Cerfa No. 13990*05](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13990.do) establecer o salir de una bandada de aves de corral.

*Para ir más allá*: Artículo 4 del auto de 26 de febrero de 2008 supra.

### d. Declaración de densidad ganadera

#### Autoridad competente

El profesional debe solicitar al DDPP a más tardar una quincena antes de la instalación de su rebaño.

#### Documentos de apoyo

Su solicitud debe incluir el formulario de declaración[Cerfa No. 14148*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14148.do) completado y firmado.

Siempre que el DDPP lo exija, el profesional también puede estar obligado a proporcionar detalles de las instalaciones operativas que cumplan los requisitos establecidos en la Lista I de la Orden de 28 de junio de 2010 (véase supra "3o. Disposiciones comunes a todas las granjas de pollos").

**Tenga en cuenta que**

Cuando el profesional desea declarar una densidad superior a 33 kg/m2 de peso vivo, también debe enviar la misma forma al DDPP.

#### Resultado del procedimiento

El profesional deberá conservar y poner a disposición documentación relativa a las instalaciones de su explotación de conformidad con lo dispuesto en el auto de 28 de junio de 2010.

### e. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

