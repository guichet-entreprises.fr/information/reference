﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP117" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Entrenador deportivo grupal" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="entrenador-deportivo-grupal" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/entrenador-deportivo-grupal.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="entrenador-deportivo-grupal" -->
<!-- var(translation)="Auto" -->


Entrenador deportivo grupal
===========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El entrenador deportivo del equipo es un profesional que proporciona enseñanza, coaching, desarrollo y entrenamiento en uno de los deportes de grupo para los que es un graduado: béisbol, softball, cricket, baloncesto, fútbol americano, balonmano, hockey, rugby (xiii o XV), voleibol y voley playa, hockey sobre hielo, polo, waterpolo, remo y fútbol.

Acompaña tanto a principiantes como al público practicando la competición. El entrenador diseña programas de desarrollo deportivo y lleva a cabo actividades de entrenamiento. Garantiza la seguridad de los profesionales a los que acompaña, así como la de terceros.

Para más información: Artículos 2 de los decretos por los que se crean las diversas menciones del Diploma Estatal de Juventud, Educación Popular y Especialidad Deporte "Desarrollo Deportivo":

- detenido el 15 de abril de 2009 por "béisbol, softball y cricket";
- decretado el 8 de mayo de 2010 para la palabra "baloncesto";
- decreto de 29 de junio de 2009 para la palabra "fútbol americano";
- decretado el 4 de enero de 2008 para la palabra "balonmano";
- ordenó a partir del 1 de julio de 2008 las menciones "hockey", "liga de rugby", "voleibol (y voleibol de playa), "remo y disciplinas asociadas" y "polo";
- decretado el 15 de diciembre de 2006 para la palabra "rugby union";
- 25 de enero de 2011 para "hockey sobre hielo";
- decreto de 15 de marzo de 2010 para la palabra "waterpolo";
- decreto de 26 de mayo de 2016 por el que se registra el directorio nacional de certificaciones profesionales del diploma de "entrenador de fútbol".

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La actividad de coaching está sujeta a la aplicación del Artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas, incluyendo el Diploma Estatal de Juventud, Educación Popular y Deporte (DEJEPS) y la Patente entrenador de fútbol.

Como profesor de deportes, el entrenador debe poseer un diploma, un título profesional o un certificado de calificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

*Para ir más allá*: Artículos L. 212-1 y R. 212-84 del Código del Deporte.

#### Entrenamiento

El desarrollo deportivo DEJEPS, independientemente de la mención considerada (baloncesto, hockey, voleibol, etc.) así como el certificado del entrenador de fútbol se clasifican como Nivel III, es decir, nivel bac 2.

El DEJEPS y el certificado del entrenador de fútbol se preparan a través del entrenamiento inicial, la educación continua o la validación de la experiencia (VAE). Para obtener más información, puede ver[sitio web oficial](http://www.vae.gouv.fr/) Vae.

##### Condiciones comunes de acceso a la formación que conducen a la obtención de uno de los DEJEPS de deportes de grupo, independientemente de la mención elegida

El interesado deberá:

- Rellene el formulario de registro estándar con un documento de identidad con fotografía
- Ser mayores de edad
- certificar que se está siguiendo la formación en primeros auxilios (tipo PSC1, AFPS);
- pasar las pruebas de acceso a la formación;
- comunicar un certificado médico de no contradictorio con la práctica docente y deportiva en cuestión, de menos de tres meses de edad;
- cumplir con los requisitos pedagógicos requeridos: ser capaz de evaluar los riesgos objetivos asociados con la práctica de la disciplina, anticipar los riesgos potenciales para el practicante y dominar el comportamiento y las acciones a realizar en caso de un incidente o Accidente. Estas habilidades se comprueban en una sesión de 30 minutos, seguida de una entrevista de 20 minutos.

Algunas organizaciones de capacitación pueden requerir la publicación de otros documentos en el momento del registro (como un certificado censal). Para obtener más información, es aconsejable acercarse a la organización de formación en cuestión.

##### Condiciones adicionales específicas de DEJEPS que mencionan "béisbol, solftball y cricket"

El interesado deberá:

- Ser capaz de realizar una demostración técnica y analizarla (durante la implementación de una sesión introductoria de 30 minutos, seguida de una entrevista de 20 minutos);
- producir un certificado, emitido por el Director Técnico Nacional de Béisbol, Softbol y Cricket, de entrenamiento en una de estas disciplinas de al menos 200 horas durante una temporada deportiva;
- producir un certificado, emitido por el Director Técnico Nacional de Béisbol, Softbol y Cricket, para practicar una de estas disciplinas durante al menos tres temporadas deportivas.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4 y 6 del auto de 15 de abril de 2009 antes mencionado.

##### Condiciones adicionales específicas de jePS que mencionan el "baloncesto"

El interesado deberá:

- Ser capaz de implementar una secuencia de oposición en el baloncesto (durante la implementación de una sesión de entrenamiento, seguida de una entrevista);
- presentar un certificado, expedido por el director técnico nacional de baloncesto, de experiencia como entrenador durante al menos tres temporadas en uno o más equipos que jueguen en el campeonato francés, jóvenes o mayores, o en equipos de nivel similar en la liga extranjera;
- producir un certificado, expedido por el Director Técnico Nacional de Baloncesto, para practicar esta disciplina durante tres temporadas deportivas;
- producir un certificado de éxito en la prueba oral organizada por la Federación Francesa de Baloncesto y emitida por el Director Técnico Nacional de Baloncesto. Este evento consiste en analizar un documento de vídeo que traza una secuencia de partidos que dura hasta 2 minutos para una competición elegida de los siguientes campeonatos: "Nacional Masculino o Femenino 2", "Nacional Masculino o Femenino 3" o Campeonato Juvenil francés. El candidato debe demostrar su capacidad para observar, analizar y diagnosticar para desarrollar capacitación para un jugador de nivel nacional o para un grupo que juegue en una competición de nivel nacional.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4 y 6 del auto de 8 de mayo de 2010 mencionado anteriormente.

##### Condiciones adicionales específicas de JEPS que mencionan el "fútbol americano"

El interesado deberá:

- ser capaz de realizar una demostración técnica, analizarla e implementar una sesión introductoria (estos requisitos son monitoreados durante la implementación de una sesión introductoria de 30 minutos en el fútbol americano, seguida de una entrevista con 20 minutos);
- producir un certificado, emitido por el Director Técnico Nacional de Fútbol Americano, de 150 horas de experiencia introductoria en el fútbol americano durante una temporada deportiva;
- presentar un certificado, emitido por el Director Técnico Nacional de Fútbol Americano, de practicar durante al menos una temporada de deportes en el fútbol americano.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, conviene remitirse a los artículos 4 y 6 del auto de 29 de junio de 2009 antes mencionado.

##### Condiciones adicionales específicas de JEPS que mencionan el "balonmano"

El interesado deberá:

- Ser capaz de implementar una secuencia de oposición en el balonmano;
- presentar un certificado, expedido por el Director Técnico Nacional de Balonmano, de entrenamiento responsable de balonmano durante 450 horas, o al menos tres temporadas deportivas en los últimos cinco años;
- presentar un certificado, expedido por el Director Técnico Nacional de Balonmano, de práctica de la disciplina que abarca tres temporadas en los últimos cinco años.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4 y 6 del decreto de 4 de enero de 2008 antes mencionado.

##### Condiciones adicionales específicas de deJEPS que mencionan "hockey"

El interesado deberá:

- ser capaz de evaluar la propia capacidad de realizar una demostración técnica en hockey, así como llevar a cabo una sesión de aprendizaje seguro (estos requisitos se supervisan al configurar una sesión de aprendizaje de hockey seguro 30 minutos como máximo, seguido de una entrevista de 15 minutos);
- producir un certificado, expedido por el Director Técnico Nacional de Hockey, de entrenamiento de hockey durante una temporada deportiva y de una duración mínima de 100 horas en los últimos cinco años.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4 y 6 del decreto de 1 de julio de 2008 antes mencionado.

##### Condiciones adicionales específicas de JEPS que mencionan la "liga de rugby"

El interesado deberá:

- Ser capaz de implementar una situación de formación (este requisito se supervisa durante la creación de una sesión educativa, seguida de una entrevista);
- producir un certificado, emitido por el director técnico nacional de la liga de rugby, con licencia para la Federación de la Liga francesa de Rugby o un miembro nacional de la Junta de la Liga Internacional de Rugby, durante al menos tres temporadas deportivas ;
- producir un certificado, emitido por el Director Técnico Nacional de la Liga de Rugby, de supervisión técnica de un equipo de la liga de rugby (escuela de rugby juvenil, personas mayores) durante al menos una temporada deportiva.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4 y 6 del decreto de 1 de julio de 2008 antes mencionado.

##### Condiciones adicionales específicas de JEPS que mencionan la "unión de rugby"

El interesado deberá:

- Ser capaz de implementar una situación formativa
- producir uno o más certificados de participación en competiciones de rugby durante al menos tres temporadas deportivas;
- producir uno o más certificados de participación en el entrenamiento de un equipo (escuela de rugby, joven o senior) en la unión de rugby durante al menos una temporada deportiva.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 3, 4 y 6 del auto de 15 de diciembre de 2006 antes mencionado.

##### Condiciones adicionales específicas de jePS que mencionan "voleibol"

El interesado deberá:

- Ser capaz de implementar una secuencia de animación en voleibol o voleibol de playa (este requisito se controla al establecer una sesión educativa, seguida de una entrevista);
- presentar un certificado, expedido por el Director Técnico Nacional de Entrenamiento de Voleibol, Voleibol o Voleibol de Playa;
- presentar un certificado, expedido por el Director Técnico Nacional de Voleibol, de la práctica de voleibol o voleibol de playa durante al menos dos temporadas deportivas.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4 y 6 del decreto de 1 de julio de 2008 antes mencionado.

##### Condiciones adicionales específicas de jePS que mencionan "polo"

El interesado deberá:

- presentar un certificado, expedido por el administrador jurídico de la estructura o estructuras en las que la actividad o en la que se realizó la actividad o por el director técnico nacional de polo, justificando una experiencia profesional o de voluntariado de dos años, justificando una experiencia profesional o de voluntariado de dos años Entrenamiento grupal;
- presentar un certificado, expedido por el director técnico nacional de polo, dedándose la capacidad del candidato para dominar la técnica del polo, realizar el análisis técnico de una pareja de jugadores-caballos desconocidos a partir de un vídeo y proponer situaciones sesiones de entrenamiento. Estas habilidades se verifican mediante una prueba técnica organizada por la Federación Francesa de Polo.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más información, es aconsejable remitirse a los artículos 4 y 6 del decreto de 1 de julio de 2008 antes mencionado.

##### Condiciones adicionales específicas de deJEPS referidas a "hockey sobre hielo"

El interesado deberá:

- Producir un certificado de experiencia de entrenamiento para un equipo mínimo de hockey sobre hielo de 450 horas adquirido en las últimas tres temporadas deportivas en los últimos cinco años;
- la experiencia como participante competitivo en hockey sobre hielo durante tres temporadas deportivas.

Para verificar estos dos requisitos previos, el candidato debe proporcionar certificaciones preparadas por el Director Técnico Nacional de Hockey sobre Hielo.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más detalles, es aconsejable remitirse a los artículos 4 y 6 del auto de 25 de enero de 2011 mencionado anteriormente.

##### Condiciones adicionales específicas de jePS que mencionan el "polo de agua"

El interesado deberá:

- Poseer el Certificado de Primeros Auxilios en un equipo de Nivel 1 (o su equivalente) el día de la educación continua;
- ser capaz de rescatar al jugador de waterpolo, presentar el programa deportivo federal y los conceptos del plan de carrera del jugador de polo. Estas habilidades se comprueban durante la realización de una sesión de desarrollo de deportes de waterpolo de 20 minutos, seguida de un máximo de 30 minutos de entrevista;
- presentar un certificado de éxito, expedido por el Director Técnico de Natación, a una prueba de seguridad realizada sin gafas de natación, clips nasales o ayuda sescalera. Esta prueba se lleva a cabo a una distancia de 50 metros y se descompone de la siguiente manera:- salida libre desde el borde de la cuenca,
  - Curso de estilo libre de 25 metros,
  - llamada inmersión de pato y búsqueda de un maniquí regulador sumergido a 25 metros del punto de partida a una profundidad entre 1,80 y 3 metros,
  - el ascenso del muñeco a la superficie,
  - remolcar a una persona durante una distancia de 25 metros hasta el borde de la cuenca,
  - El agua de la víctima
- para llevar a cabo un curso de tiro que justifique un nivel técnico correspondiente a las habilidades dirigidas en el pase de competición de "polo de agua" emitido por la Federación Francesa de Natación. El éxito de esta prueba es objeto de un certificado expedido por el Director Técnico Nacional de Natación;
- producir un certificado de experiencia de una práctica mínima de waterpolo durante tres temporadas deportivas en un equipo equivalente al "Nacional 3", "Nacional 2" o "Nacional 1" y justificando la participación efectiva en el campo de los diez partidos partidos al menos por temporada deportiva. Este certificado es expedido por el Director Técnico Nacional de Natación;
- presentar un certificado de experiencia educativa, ya sea voluntaria o profesional, en waterpolo de 800 horas, ya sea dentro de un club de una federación deportiva acreditada o dentro de un polo en la lista elaborada por el Ministro responsable de los deportes en Artículo R. 221-26 del Código del Deporte, durante un mínimo de tres años en los últimos cinco años. Esta certificación es establecida por el Director Técnico Nacional de Natación para las estructuras afiliadas a la Federación Francesa de Natación, o por el Director Técnico Nacional o, en su defecto, por el Presidente de una de las Federaciones Miembros del Consejo. actividades acuáticas interfederales de acuerdo con la Federación Francesa de Natación para sus estructuras afiliadas.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más detalles, es aconsejable consultar los artículos 4, 6 y siguientes del auto de 15 de marzo de 2010 mencionado anteriormente.

###### Las condiciones adicionales específicas de jePS mencionan "el remo y las disciplinas asociadas"

El interesado deberá:

- Tener licencia para conducir embarcaciones de recreo con una opción "costera" y la opción "aguas interiores";
- ser capaz de poner un dispositivo de seguridad en el agua y en el suelo y saber cómo intervenir con un practicante en dificultad. Estos requisitos se verifican al establecer una sesión de instrucción de seguridad de 45 minutos para una tripulación que opera a nivel regional, seguida de una entrevista de 15 minutos;
- presentar un certificado de éxito, expedido por una persona titular de una certificación de coaching de actividades acuáticas de conformidad con el artículo L. 212-1 del Código del Deporte, a una prueba de 100 metros de buceo a estilo libre con la recuperación de un objeto sumergido a una profundidad de 2 metros;
- presentar un certificado de éxito, expedido por el Director Técnico Nacional de Remo, a una primera prueba técnica organizada por la Federación Francesa de Sociedades de Remo, incluido un evento práctico para verificar la autonomía de la candidato en skiff;
- presentar un certificado de éxito, expedido por el Director Técnico Nacional de Remo, a una segunda prueba técnica organizada por la Federación Francesa de Sociedades de Remo, incluyendo la supervisión de una sesión educativa en remo seguida de una para verificar las habilidades del candidato en la formación de una tripulación que opera a nivel regional.

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más detalles, es aconsejable remitirse a los artículos 4 y 6 del auto de 1 de julio de 2008 antes mencionado.

###### Condiciones de acceso a la formación que conduzcan a la certificación de un entrenador de fútbol

El interesado deberá:

- Ser mayores el día de la entrada en la formación;
- ser licenciado a la Federación Francesa de Fútbol para la temporada en día (y proporcionar una copia de su licencia);
- poseer el Certificado de Capacitación de Primeros Auxilios (AFPS) o el Certificado de Prevención y Socorro Cívico de Nivel 1 (PSC1);
- completar el formulario de solicitud, adjuntar una fotocopia de su documento de identidad válido, un documento de identidad con fotografía y un certificado de honorabilidad;
- comunicar un certificado médico de no contradictorio con la práctica y el entrenamiento de fútbol, de menos de tres meses de edad;
- para producir el certificado de éxito en las pruebas de selección, emitido por la organización de formación. Esta prueba incluye:- un examen escrito de hasta 45 minutos sobre la animación y la experiencia de práctica del candidato,
  - Un máximo de 20 minutos de entrevista para evaluar las motivaciones del candidato, las habilidades de entrenamiento y la capacidad de expresarlo,
  - Poseer el certificado regional de nivel de juego emitido por la Federación Francesa de Fútbol;
- cumplir una de las siguientes condiciones y proporcionar una copia del diploma correspondiente o del certificado correspondiente, en su caso:
- Ser una licencia de instructor de fútbol obtenida después del 2 de abril de 2008;
- ser o haber sido deportista de alto nivel en la lista ministerial mencionada en el artículo L. 221-2 del Código del Deporte;
- ser o haber sido un jugador nacional en la Ligue 1 o Liga 2 o Nacional o CFA o CFA2 para 100 partidos senior;
- ser o haber jugado a nivel nacional en D1 femenino o D2 durante 100 partidos senior;
- Poseer el certificado estatal de opción de fútbol para educadores deportivos de primer grado;
- poseer el certificado profesional de mención juvenil y deportiva "fútbol";
- tener al menos una unidad capitalizada del certificado de entrenador de fútbol obtenido como parte de una solicitud de validación de la experiencia (VAE).

Bajo ciertas condiciones, se pueden obtener exenciones para ciertos eventos. Para más detalles, es aconsejable consultar el pedido de 26 de mayo de 2016 registrándose en el directorio nacional de certificaciones profesionales bajo el título de "entrenador de fútbol".

**Es bueno saber**

Las personas con el Diploma Estatal Senior de Educación Juvenil, Educación Popular y Deporte (DESJEPS) rendimiento deportivo especializado también pueden practicar como entrenador deportivo. Este diploma estatal de Nivel II es emitido por el Director Regional de Juventud y Deportes. Para practicar en el campo de los deportes de grupo, el entrenador optará por una de las siguientes menciones desJEPS: "fila", "béisbol, softball", "baloncesto", "cricket", "fútbol", "fútbol americano", "balonmano", "hockey", "hockey sobre hielo", rugby, rugby, voleibol, remo o waterpolo. Por último, el titular de la mención DESJEPS "voleibol" puede especializarse en la disciplina del voleibol de playa optando por el certificado de especialización "voleibol de playa". Para obtener más información sobre los requisitos de admisión y la capacitación que conduce a la graduación, consulte el [Sitio web del Ministerio de Deportes](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/des-jeps/Reglementation-11081/La-specialite-performance-sportive-du-DES-JEPS-et-les-mentions-unites-capitalisables-complementaire-et-certificats-de-specialisation-s-y-rapportant/).

*Para ir más allá*: Artículos D. 212-35 y siguientes del Código del Deporte, ordenado el 20 de noviembre de 2006 por el que se organiza el Diploma Estatal de Juventud, Educación Popular y Especialidad Deporte "Desarrollo Deportivo" emitido por el Ministerio de Juventud y Juventud deportes y todos los decretos antes mencionados creando las diversas menciones relacionadas con el deporte colectivo.

#### Costos asociados con la calificación

La formación del diploma deJEPS en desarrollo deportivo menciona el deporte colectivo (coste variable dependiendo de las menciones). La formación que lleva al certificado del entrenador de fútbol cuesta unos 2.800 euros. Para [más detalles](https://foromes.calendrier.sports.gouv.fr/#/formation), es aconsejable acercarse a la organización de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:**

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:**

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como entrenador de deportes colectivos en Francia para personas que hayan sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Requisito de notificación (con el fin de obtener la tarjeta de educador deportivo profesional)
-----------------------------------------------------------------------------------------------------------------------

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el [sitio web oficial](https://eaps.sports.gouv.fr).

#### hora

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

#### Documentos de apoyo

Los documentos justificativos que se proporcionarán son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

#### Costo

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

5°. Proceso de cualificaciones y formalidades
-------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

#### Autoridad competente

La declaración previa de actividad debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde el Departamento en el que declarante quiere realizar su actuación.

#### hora

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

#### Documentos de apoyo

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

#### Autoridad competente

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

#### hora

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

#### Documentos de apoyo para la primera declaración de actividad

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia,
  - documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

#### Evidencia para una declaración de renovación de la actividad

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

#### Costo

Gratis.

#### Remedios

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

**Bueno saber: medidas de compensación**

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

