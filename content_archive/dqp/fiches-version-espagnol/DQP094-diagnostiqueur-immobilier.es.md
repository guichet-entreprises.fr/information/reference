﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP094" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Construcción" -->
<!-- var(title)="Diagnóstico Inmobiliario" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="construccion" -->
<!-- var(title-short)="diagnostico-inmobiliario" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/construccion/diagnostico-inmobiliario.html" -->
<!-- var(last-update)="2020-04-15 17:20:46" -->
<!-- var(url-name)="diagnostico-inmobiliario" -->
<!-- var(translation)="Auto" -->


Diagnóstico Inmobiliario
========================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:46<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El diagnóstico inmobiliario es un profesional cuya actividad consiste, con motivo de una transacción inmobiliaria, de establecer un balance técnico del edificio.

El archivo de diagnóstico técnico (DDT) puede incluir:

- El rendimiento energético del edificio para la liberación de la etiqueta energética del edificio;
- La seguridad de sus instalaciones de gas;
- Instalaciones eléctricas
- presencia de amianto, plomo o cualquier otra causa de condiciones insalubres.

**Tenga en cuenta que**

Los documentos establecidos en relación con su actividad incluyen la mención de la certificación del profesional.

*Para ir más allá*: Artículos L. 271-4 y R. 271-3 del Código de Construcción y Vivienda.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de diagnóstico inmobiliario el profesional debe:

- profesionalmente calificados (ver infra "2.2). Entrenamiento inicial");
- estar en posesión de un certificado de competencia (véase infra "2." Certificación").

*Para ir más allá*: Artículo R. 271-1 del Código de La Construcción y Vivienda.

#### Entrenamiento

**Formación inicial**

Para ser reconocido como profesionalmente calificado, el profesional debe poseer uno de los siguientes títulos:

- licencia profesional mencionar "BTP trades: energía y desempeño ambiental de los edificios" o "seguridad de bienes y personas";
- Diploma de proyector de investigación de edificios y obras públicas emitido por el Conservatorio Nacional de Artes y Oficios (Cnam);
- Diploma de Técnico en Diagnóstico Inmobiliario por el Instituto Técnico de Gas y Aire (ITGA);
- diploma de diagnóstico inmobiliario emitido por una institución de educación superior.

Estos diplomas están disponibles para los candidatos de un curso de formación en condición de estudiante o estudiante, contrato de aprendizaje, después de la educación continua, contrato de profesionalización, solicitud individual o a través del proceso de validación de la experiencia (VAE). Para más información, es aconsejable consultar el[Sitio web oficial de VAE](http://www.vae.gouv.fr/).

En ausencia de cualquiera de los títulos anteriores, el profesional debe tener tres años de experiencia profesional como técnico o oficial de máster en el campo de la construcción, o cualquier otra función equivalente.

*Para ir más allá* Directorio nacional de certificaciones profesionales ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)) ;[Apéndice 2](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=2402E91AE29265EE7B4A9C101AD212D6.tplgfr22s_3idArticle=LEGIARTI000025027138&cidTexte=LEGITEXT000025027132&dateTexte=20180227) del auto de 16 de octubre de 2006 por el que se establecen los criterios para certificar las competencias de las personas que realizan el diagnóstico del rendimiento energético o el certificado de consideración de las normas térmicas, y los criterios acreditación de organismos de certificación.

**Certificación**

Para realizar un diagnóstico inmobiliario, el profesional titular de uno de los citados títulos debe justificar garantías de competencia.

Para ello, la persona física o jurídica debe obtener una certificación de competencia expedida por un organismo certificador acreditado por el Comité de Acreditación francés ([Cofrac](https://www.cofrac.fr/)) en el sector de la construcción (véase infra "5o. a. Procedimiento de certificación").

*Para ir más allá*: Artículos L. 271-6 y R. 271-1 del Código de Construcción y Vivienda.

#### Costos asociados con la calificación

El coste de la cualificación varía en función del curso previsto. Para más información, es aconsejable acercarse al establecimiento en cuestión.

### b. Nacionales de la UE: para la entrega temporal y ocasional (entrega gratuita de servicios (LPS)) o permanente (establecimiento libre(LE))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido podrá ejercer de forma temporal y casual o la misma actividad en Francia.

Por esta razón, el nacional está sujeto a los mismos requisitos que el nacional francés (véase más arriba "2". a. Requisitos nacionales") y, por lo tanto, deben justificar un título de un nivel equivalente al exigido a un nacional francés.

Además, debe obtener la certificación de un organismo europeo signatario del acuerdo multilateral europeo adoptado como parte de la coordinación europea de los organismos de acreditación.

*Para ir más allá*: Apéndice 2 del auto de 16 de octubre de 2006 supra.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El profesional debe llevar a cabo su actividad de forma imparcial e independiente y, como tal, no debe tener ninguna conexión para interferir con estas obligaciones.

*Para ir más allá*: Artículo L. 271-6 del Código de La Construcción y Vivienda.

**Tenga en cuenta que**

El profesional puede estar obligado a cumplir con las reglas éticas específicas de cada organismo certificador. Para obtener más información, es aconsejable visitar el sitio web de estas organizaciones.

**Sanciones penales**

El diagnóstico inmobiliario se enfrenta a una multa de hasta 1.500 euros (o 3.000 euros en caso de reincidencia) si:

- hace un diagnóstico sin estar calificado profesionalmente, sin haber contratado un seguro (ver infra "4o. Seguros" o desconocimiento de las obligaciones de imparcialidad e independencia;
- está certificado por un organismo no acreditado para emitir la certificación.

*Para ir más allá*: Artículo R. 271-4 del Código de La Construcción y Vivienda.

4°. Seguro
-------------------------------

El diagnóstico de bienes raíces está obligado a tomar un seguro para cubrir los riesgos incurridos durante su actividad.

El importe de esta garantía no puede ser inferior a 300.000 euros por siniestralidad y de 500.000 euros por año de seguro.

**Tenga en cuenta que**

Si el ejercicio profesional como empleado, corresponde al empleador tomar dicho seguro para sus empleados, por los actos realizados durante su actividad profesional.

*Para ir más allá*: Artículos L. 271-6 y R. 271-2 del Código de Construcción y Vivienda.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Procedimiento de certificación

El candidato a la certificación debe presentar un expediente de solicitud al organismo certificador que juzgará su admisibilidad. Una vez elegible, el candidato debe ser objeto de:

- examen teórico para verificar que la persona tiene el conocimiento de:- generalidades sobre el edificio,
  - La térmica del edificio,
  - El sobre del edificio,
  - (tecnologías de calefacción, introducción de energías renovables, etc.),
  - Textos reglamentarios
- un examen práctico del análisis situacional de un candidato para verificar que el candidato es capaz de evaluar el consumo de energía de un edificio y desarrollar un diagnóstico de rendimiento agresivo teniendo en cuenta los detalles del caso Tratado.

La certificación otorgada al profesional tiene una validez de cinco años. Al final de este período debe proceder a su renovación.

**Tenga en cuenta que**

Un profesional sólo puede poseer una certificación y debe proporcionar una declaración de honor que acredite que no tiene otra certificación.

*Para ir más allá*: Artículo R. 134-4 del Código de La Construcción y Vivienda; 16 de octubre de 2006.

### b. Procedimiento de seguimiento

El profesional certificado es supervisado por el organismo certificador y debe proporcionar:

- La situación de las reclamaciones y reclamaciones en su contra durante su actividad;
- todos los informes preparados, así como la información sobre la misión llevada a cabo (fecha, local, tipo de misión, e información relacionada con la clase energética de los locales).

**Tenga en cuenta que**

Esta información puede solicitarse durante el proceso de renovación de la certificación.

*Para ir más allá* : decreto de 16 de octubre de 2006 supra; Sección R. 271-1 del Código de Vivienda y Construcción.

### c. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea.

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

