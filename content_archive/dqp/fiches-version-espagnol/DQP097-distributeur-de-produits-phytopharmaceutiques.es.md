﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP097" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios agrícolas" -->
<!-- var(title)="Distribuidor de fitofármacos" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-agricolas" -->
<!-- var(title-short)="distribuidor-de-fitofarmacos" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-agricolas/distribuidor-de-fitofarmacos.html" -->
<!-- var(last-update)="2020-04-15 17:20:47" -->
<!-- var(url-name)="distribuidor-de-fitofarmacos" -->
<!-- var(translation)="Auto" -->




Distribuidor de fitofármacos
============================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:47<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El distribuidor de fitofarmacéuticos es un profesional cuya actividad consiste en garantizar la venta, venta o distribución de fitofármacos de forma gratuita a los usuarios de estos productos o a particulares. moral es que actúe en su nombre, incluidos los grupos de compra.

*Para ir más allá*: Artículos L. 254-1 y R. 254-1 del Código Rural y Pesca Marina.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ser distribuidor de productos fitosanitarios, el profesional debe:

- Poseer un certificado individual de ejercicio llamado "certiphyto";
- Obtener aprobación
- una vez al año proporcionar asesoramiento individualizado a cada uno de sus clientes que utilizan fitofarmacéuticos;
- mantener un registro de sus ventas (véase infra "3.3. Mantener un registro de ventas").

*Para ir más allá*: Artículos L. 254-1, L. 254-3, L. 254-6 y L. 254-7 del Código Rural y Pesca Marina.

#### Entrenamiento

**Certificado individual para la actividad "venta, venta de fitofarmacéuticos"**

El certificado individual se expide al profesional que:

- se sometió a capacitación que incluyó una prueba escrita de una hora de verificación de conocimientos;
- superó con éxito una prueba de una hora y media que consta de treinta preguntas sobre el programa de formación establecido en la Lista II de la orden del 29 de agosto de 2016;
- posee uno de los diplomas en el calendario I lista de la orden del 29 de agosto de 2016, obtenida en los cinco años anteriores a la fecha de solicitud.

Una vez que el profesional cumple una de estas condiciones, debe solicitar el certificado (ver más abajo "5 grados). a. Solicitar un certificado de práctica individual").

*Para ir más allá* : decreto de 29 de agosto de 2016 por el que se establecen y establecen las condiciones de obtención del certificado individual para la actividad "venta, venta de fitofarmacéuticos".

### b. Nacionales de la UE: para el ejercicio temporal y ocasional (prestación gratuita de servicios (LPS))

Todo nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) titular de un certificado expedido por un Estado miembro, legalmente establecido y que ejerza una actividad de distribución de productos fitofarmacéuticos, pueden llevar a cabo la misma actividad de forma temporal y casual en Francia.

Una vez que cumpla estas condiciones, deberá, antes de su primera prestación de servicios, hacer una declaración al Director Regional de Alimentación, Agricultura y Silvicultura (DRAAF) del lugar donde se presta el primer servicio. servicios (ver infra "5 grados. c. Predeclaración del nacional para un ejercicio temporal e informal (LPS)."

*Para ir más allá*: Artículos L. 204-1 y R. 254-9 del Código Rural y Pesca Marina.

### c. Nacionales de la UE: para un ejercicio permanente (establecimiento libre (LE))

Todo nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) titular de un certificado expedido por un Estado miembro, legalmente establecido y que actúe como distribuidor de productos fitofarmacéuticos, puede llevar a cabo la misma actividad de forma permanente en Francia.

Mientras el nacional posea un certificado individual de ejercicio expedido por un Estado miembro, se considerará que posee el certificado necesario para ejercer en Francia.

Además, el profesional está sujeto a las mismas condiciones de instalación que el nacional francés (véase infra "5 grados). Procedimientos y formalidades de reconocimiento de cualificación").

*Para ir más allá* II del artículo R. 254-9 del Código Rural y de la Pesca Marina.

3°. Condiciones de honorabilidad, reglas profesionales
---------------------------------------------------------------

**Obligaciones para la venta de fitofarmacéuticos a usuarios no profesionales**

Sólo los productos con la palabra "uso autorizado en jardines" pueden ser listados para la venta, vendidos o distribuidos a usuarios no profesionales, excepto si actúan en nombre de profesionales. Si es así, estos usuarios deben justificar estar en posesión:

- para los propietarios de tierras para uso no agrícola y no forestal, gestionadas por un tercero:- un contrato o certificado de delegación a un tercio de todas las decisiones de protección fitosanitaria,
  - prueba de la calidad del usuario profesional de este tercero;
- para los agricultores o silvicultores, que no participan en la definición de la estrategia de tratamiento o la elección de los productos que se utilizarán y que utilizan un organismo para todo su trabajo de aplicación fitofarmacéutica. aprobado para la solicitud de prestación de servicios:- pruebas escritas de la finalización del servicio por una persona jurídica autorizada para la consultoría o distribución independiente de fitofarmacéuticos a usuarios profesionales,
  - un documento contractual con una organización aprobada para la solicitud de prestación de servicios con el nombre de una persona certificada.

Además, en el caso de una venta de productos que no llevan esta mención, el profesional está obligado a garantizar de antemano la calidad profesional del futuro comprador. Para ello, deberá comprobar que éste dispone de un certificado de práctica individual en una de las siguientes categorías:

- "Responsable de la toma de decisiones en el trabajo y los servicios";
- "Toma de decisiones en el trabajo agrícola";
- "Aplicador Comunitario Territorial."

En cada venta, el profesional está obligado a:

- productos separados etiquetados como "uso autorizado en jardines" y aquellos que no lo hacen, utilizando un proceso de señalización explícito;
- proporcionar toda la información sobre el uso de los productos y los riesgos que plantean para la salud y el medio ambiente, así como directrices de seguridad para prevenir riesgos.

*Para ir más allá*: Artículos R. 254-20 a -22 del Código Rural y Pesca Marina; Pedido del 6 de enero de 2016 sobre la documentación requerida para la compra de fitofármacos de la gama de usos "profesionales".

**Obligación de informar a los clientes que utilizan fitofarmacéuticos**

El profesional que vende o distribuye fitofarmacéuticos debe proporcionar un asesoramiento escrito individualizado a sus clientes al menos una vez al año indicando:

- El ingrediente activo y la especialidad recomendada
- El objetivo de este producto, así como las parcelas y el área afectada por su uso;
- La dosis que se utilizará y las condiciones para su aplicación;
- métodos alternativos (métodos no químicos y el uso de productos de biocontrol).

Además, cuando el profesional vende estos productos, debe proporcionar toda la información relacionada con el uso de fitofarmacéuticos, posibles riesgos para la salud y el medio ambiente, y directrices de seguridad.

Además, en caso de transferencia de productos a usuarios no profesionales, el profesional deberá facilitar toda la información relativa a los riesgos potenciales para la salud humana y el medio ambiente (peligros, exposición, condiciones de almacenamiento y para la manipulación, aplicación y eliminación seguras de estos productos y alternativas de bajo riesgo).

*Para ir más allá*: Artículo L. 254-7 del Código Rural y Pesca Marina.

**Mantener un registro de ventas**

El profesional debe mantener un registro de las ventas realizadas mencionando la información del producto (nombre comercial, número de autorización, cantidad vendida, cantidad de regalías).

Además, para la distribución de fitofarmacéuticos a usuarios profesionales, el distribuidor también debe incluir en el registro:

- El número y la fecha de facturación, si corresponde.
- El código postal del usuario final del producto
- Documentos que justifican el estatus profesional del usuario
- toda la información sobre la semilla tratada (las especies vegetales en cuestión, el número y la fecha de la facturación).

Cada año, el profesional debe hacer balance del año calendario anterior con toda la información mencionada anteriormente durante la venta de cada producto fitofarmacéutico.

Además, cada año antes del 1 de abril, debe transmitir esta información a la agencia de agua y a la junta de agua electrónicamente, en apoyo del gravamen por la contaminación difusa.

*Para ir más allá*: Artículos R. 254-23 y siguientes del Código Rural y Pesca Marina.

4°. Seguros y sanciones
--------------------------------------------

**Seguro**

El profesional está obligado, para obtener su aprobación, a obtener un seguro que cubra su responsabilidad civil profesional.

*Para ir más allá*: Artículo L. 254-2 del Código Rural y Pesca Marina.

**Sanciones penales**

Un profesional que trabaja como distribuidor de fitofarmacéuticos sin licencia está sujeto a una pena de seis meses de prisión y una multa de 15.000 euros.

Además, el profesional es multado con 1.500 euros por la venta de fitofarmacéuticos a no profesionales en violación de lo dispuesto en el artículo L. 254-20 del Código Rural y de la Pesca Marítima.

*Para ir más allá*: Artículos L. 254-12 y R. 254-30 del Código Rural y pesca marina.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de certificado individual de ejercicio

**Autoridad competente**

El profesional deberá presentar su solicitud a la Dirección Regional de Alimentación, Agricultura y Silvicultura (DRAAF) del lugar de su residencia, o, en su caso, el lugar de la sede de la organización donde se llevó a cabo su formación.

**Documentos de apoyo**

Los solicitantes deben crear una cuenta en línea en el sitio web[service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) para acceder al servicio de aplicación de certificados.

Su solicitud debe incluir, dependiendo del caso:

- Una prueba de entrenamiento de seguimiento y, si es necesario, un pase a la prueba;
- copia de su diploma o título de formación.

**Tiempo y procedimiento**

El certificado se expide dentro de los dos meses siguientes a la presentación de su solicitud. En ausencia de la emisión del certificado después de este período, y a menos que la notificación de denegación, los documentos justificativos anteriores valen la pena un certificado individual de hasta dos meses.

Este certificado tiene una validez de cinco años y es renovable en las mismas condiciones.

*Para ir más allá*: Artículos R. 254-11 y R. 254-12 del Código de Pesca Rural y Marina.

### b. Solicitud de certificación para la práctica de la actividad del distribuidor

**Autoridad competente**

El profesional debe presentar su solicitud al prefecto de la región en la que se encuentra la sede central de la empresa. Si el profesional tiene su sede en un Estado miembro distinto de Francia, tendrá que presentar su solicitud al prefecto de la región en la que llevará a cabo su primera prestación de servicios.

**Documentos de apoyo**

Su solicitud debe incluir el[Formulario de solicitud](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14581.do) completado y firmado, así como los siguientes documentos:

- Un certificado de póliza de seguro de responsabilidad civil profesional;
- Un certificado expedido por un organismo tercero que justifica que realiza esta actividad de acuerdo con las condiciones que garantizan la protección de la salud pública y el medio ambiente, y la buena información de los usuarios;
- copia de un contrato con una agencia externa que prevé el seguimiento necesario para mantener la certificación.

**Tenga en cuenta que**

Los microdistribuidores deben presentar los siguientes documentos a DRAAF:

- un certificado de seguro de responsabilidad civil profesional;
- prueba de la detención de Certiphyto por todo el personal que desempeña funciones de supervisión, ventas o asesoramiento;
- Un documento que justifique que está sujeto al sistema fiscal de microempresas;

**Tiempo y procedimiento**

El profesional que inicie su actividad deberá solicitar una aprobación provisional que se concederá por un período de seis meses no renovable. Después de esto, podrá solicitar la aprobación final.

El silencio guardado por el prefecto de la región más allá de un período de dos meses merece una decisión de rechazo.

*Para ir más allá*: Artículos R. 254-15-1 a R. 254-17 del Código Rural y Pesca Marina; orden de 27 de abril de 2017 por la que se modifica el auto de 25 de noviembre de 2011 relativo al marco de certificación previsto en el artículo R. 254-3 del Código de Pesca Rural y Marítima para la actividad "distribución de fitofarmacéuticos a los no usuarios profesionales."

**Tenga en cuenta que**

Incluso si solicita varias actividades, la acreditación obtenida será única.

### c. Predeclaración del nacional para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe solicitar por cualquier medio al DRAAF el lugar donde reciba los servicios por primera vez.

**Documentos de apoyo**

Su solicitud debe incluir el certificado individual de ejercicio expedido por el Estado miembro y, en su caso, con su traducción al francés por un traductor certificado.

**Tenga en cuenta que**

Esta declaración debe renovarse cada año y en caso de cambio de situación laboral.

*Para ir más allá* III del artículo R. 254-9 del Código Rural y de la Pesca Marina.

### d. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

