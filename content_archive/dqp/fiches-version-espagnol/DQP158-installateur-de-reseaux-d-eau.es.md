﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP158" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artesanía" -->
<!-- var(title)="Instalador del sistema de agua" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artesania" -->
<!-- var(title-short)="instalador-del-sistema-de-agua" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/artesania/instalador-del-sistema-de-agua.html" -->
<!-- var(last-update)="2020-04-15 17:20:31" -->
<!-- var(url-name)="instalador-del-sistema-de-agua" -->
<!-- var(translation)="Auto" -->


Instalador del sistema de agua
==============================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:31<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El instalador del sistema de agua es un profesional cuya actividad consiste en la instalación y reparación de tuberías de agua, instalaciones sanitarias (baño, fregadero, piscina, etc.) y sus equipos.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ser un instalador de sistemas de agua, el profesional debe estar profesionalmente calificado o puesto bajo el control efectivo y permanente de una persona calificada.

La persona que practique o controle el ejercicio deberá poseer uno de los siguientes diplomas o títulos de formación:

- Certificado de Maestría (BM) "Instalador Sanitario";
- Patente Técnica de Comercio (BTM) "Instalador de Salud";
- Certificado de Cualificación Profesional (CAP) "Instalación de Salud."

A falta de uno de estos títulos, el interesado deberá justificar una experiencia profesional efectiva de tres años, en un Estado de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE), adquirido como líder autónomos o asalariados en el trabajo de instalador del sistema de agua. En este caso, el interesado podrá solicitar un certificado de reconocimiento de la cualificación profesional de la Cámara de Comercio y Artesanía (CMA) pertinente.

*Para ir más allá*: Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 1 del Decreto 98-246, de 2 de abril de 1998; Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996.

#### Entrenamiento

El "instalador sanitario" del BM es un diploma de Nivel III (bac 2) emitido por la Asamblea Permanente de Cámaras de Artesanía y Artesanía (APCMA). Este diploma está disponible para el candidato en un contrato de aprendizaje, después de un curso de educación continua o por validación de la experiencia (VAE). Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr/).

El BTM "Health Installor" es un diploma (bac) de Nivel IV emitido por la APCMA, accesible en un contrato de aprendizaje o profesionalización, después de un curso de formación continua o a través del procedimiento VAE.

El CAP "Health Installator" es un diploma de nivel V, accesible después de un curso de formación en condición de estudiante, aprendizaje o contrato de profesionalización, después de un curso de educación continua, por solicitud individual o VAE.

*Para ir más allá* Directorio nacional de certificaciones profesionales ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)).

#### Costos asociados con la calificación

La formación que conduce al diploma del instalador del sistema de agua suele ser gratuita. Para más detalles, es aconsejable acercarse al centro de formación en cuestión.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Todo nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo sobre el Espacio Económico Europeo (EEE), legalmente establecido y que opere como instalador del sistema de agua en ese Estado, podrá ejercer en Francia ocasional, la misma actividad.

Cuando el Estado miembro de la UE o del EEE no regule el acceso o el ejercicio de la profesión, el profesional deberá justificar haber realizado esta actividad durante al menos un año, en los últimos diez años anteriores a la prestación, en uno o varios años Estados miembros de la UE.

En caso de diferencias sustanciales entre la cualificación profesional del nacional y la exigida en Francia, para llevar a cabo la actividad de instalación de los sistemas de agua, la CMA competente podrá exigir que el interesado se someta a una prueba de aptitud.

Si cumple estas condiciones, primero debe hacer una declaración de actividad con la CMA del lugar donde desea ejercer (véase a continuación "Hacer una declaración previa para el nacional de la UE o del EEE para un ejercicio temporal (LPS)).

*Para ir más allá*: Artículo 17-1 de la Ley de 5 de julio de 1996; Artículo 2 de la[decreto del 2 de abril de 1998](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000388449&categorieLien=cid) modificado por el[decreto del 4 de mayo de 2017](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7D50D037AAEEDE8367FE375890CB8510.tplgfr39s_2?cidTexte=JORFTEXT000034598573&dateTexte=20170506).

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Para funcionar como instalador del sistema de agua en Francia, de forma permanente, el nacional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba: "2. a. Cualificaciones profesionales");
- poseer un certificado de competencia o certificado de formación requerido para el ejercicio de la actividad del instalador del sistema de agua en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su disposición a llevar a cabo la actividad del instalador del sistema de agua cuando este certificado o título se haya obtenido en un estado de la UE o del EEE que no regule ni regule el acceso ni el ejercicio de esta actividad;
- ser un título, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona haya llevado a cabo esta actividad durante tres años en el Estado que haya admitido la equivalencia.

Una vez que el nacional de un Estado de la UE o del EEE cumpla una de las condiciones anteriores, podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante "5o). b. Solicitar un certificado de cualificación profesional para el ejercicio permanente de la UE o del EEE para un ejercicio permanente (LE))

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la CMA competente podrá exigir que el interesado se someta a medidas de compensación (véase infra "5o. a. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 a 3-2 del Decreto de 2 de abril de 1998 modificado por el decreto de 4 de mayo de 2017.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Nadie puede trabajar como instalador del sistema de agua si:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá*: Artículo 19 III de la Ley 96-603, de 5 de julio de 1996.

4°. Seguro
-------------------------------

El instalador del sistema liberal de agua debe obtener un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración previa para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe solicitar a la CMA cuando desee prestar servicios.

**Documentos de apoyo**

Su solicitud deberá incluir los siguientes documentos, si los hubiere, con su traducción aprobada al francés:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que está legalmente establecido en un Estado de la UE o del EEE;
- un documento que justifique su cualificación profesional, es decir:- Una copia de un diploma, título o certificado,
  - Un certificado de competencia,
  - cualquier documento que acredite su experiencia profesional.

**Tenga en cuenta que**

Cuando el expediente está incompleto, la CMA tiene un plazo de quince días para informar al nacional y solicitar todos los documentos que faltan.

**Resultado del procedimiento**

Al recibir todos los documentos en el archivo, el CMA tiene un mes para decidir:

- autorizar la prestación cuando el nacional justifique tres años de experiencia laboral en un Estado de la UE o del EEE, y adjuntar a dicha Decisión un certificado de cualificación profesional;
- o autorizar la disposición cuando las cualificaciones profesionales del nacional se consideren suficientes;
- ya sea para imponer una prueba de aptitud o un curso de ajuste cuando hay diferencias sustanciales entre las cualificaciones profesionales del nacional y las requeridas en Francia. En caso de negativa a realizar esta medida de compensación o si no cumple, el nacional no podrá prestar el servicio en Francia.

En ausencia de una respuesta de la CMA dentro del plazo antes mencionado, el nacional puede comenzar su prestación de servicios.

*Para ir más allá*: Artículo 2 del Decreto de 2 de abril de 1998; Artículo 2 de la[17 de octubre de 2017](https://www.legifrance.gouv.fr/eli/arrete/2017/10/17/ECOI1719273A/jo) respecto a la presentación de la declaración y las solicitudes previstas en el Decreto 98-246, de 2 de abril de 1998, y el título I del Decreto 98-247, de 2 de abril de 1998.

### b. Solicitar un certificado de reconocimiento de la cualificación profesional para el ejercicio permanente de la UE o del EEE para un ejercicio permanente (LE)

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

Su solicitud debe dirigirse a la CMA competente del lugar en el que desea llegar a un acuerdo.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA pide al interesado que lo complete dentro de una quincena de la presentación del expediente. Se emite un recibo tan pronto como se completa.

**Documentos de apoyo**

La solicitud de certificación de cualificación profesional es un fichero con los siguientes documentos justificativos:

- Una solicitud de certificado de cualificación profesional
- una prueba de cualificación profesional en forma de certificado de competencia o un diploma o un certificado de formación profesional;
- Una fotocopia del documento de identidad válido del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

**Qué saber**

Si es necesario, todos los documentos justificativos deben ser traducidos al francés por un traductor certificado.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (Ciep) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**hora**

Dentro de los tres meses siguientes a la recepción, la CMA podrá decidir:

- Reconocer la cualificación profesional y emitir la certificación de cualificación profesional;
- someter al nacional a una medida de indemnización y notificarle dicha decisión;
- negarse a expedir el certificado de cualificación profesional.

**Qué saber**

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Remedios**

Si la CMA rechaza la solicitud de cualificación profesional de la CMA, el solicitante puede impugnar la decisión. Por lo tanto, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, puede formar:

- una apelación agraciada al prefecto del departamento de CMA pertinente;
- una impugnación legal ante el tribunal administrativo pertinente.

**Costo**

Gratis.

**Bueno saber: medidas de compensación**

La CMA notifica al solicitante su decisión de que realice una de las medidas de compensación. Esta Decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son imprescindibles para ejercer en Francia.

A continuación, el solicitante debe elegir entre un curso de ajuste de hasta tres años y una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final del curso de ajuste, el solicitante envía al CMA un certificado que certifica que ha completado válidamente esta pasantía, acompañado de una evaluación de la organización que lo supervisó. La CMA emite, sobre la base de este certificado, un certificado de cualificación profesional en el plazo de un mes.

La decisión de utilizar una medida de indemnización podrá ser impugnada por el interesado, que deberá presentar un recurso administrativo ante el prefecto en el plazo de dos meses a partir de la notificación de la decisión. Si su apelación es desestimada, puede iniciar una impugnación legal.

**Costo**

Se puede cobrar una tarifa fija que cubra la investigación del caso. Para obtener más información, es aconsejable acercarse a la CMA correspondiente.

*Para ir más allá*: Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### c. Remedios

**Centro de Asistencia Francesa****

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

