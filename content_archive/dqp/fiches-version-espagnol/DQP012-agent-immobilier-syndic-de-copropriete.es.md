﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP012" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Agente inmobiliario - fideicomisario de condominios - Administrador de la propiedad" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="agente-inmobiliario-fideicomisario" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/agente-inmobiliario-fideicomisario-de-condominios-administrador.html" -->
<!-- var(last-update)="2020-04-15 17:20:59" -->
<!-- var(url-name)="agente-inmobiliario-fideicomisario-de-condominios-administrador" -->
<!-- var(translation)="Auto" -->


Agente inmobiliario - fideicomisario de condominios - Administrador de la propiedad
===================================================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:59<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El agente inmobiliario, el administrador de la propiedad y el fideicomisario del condominio (conocido como "profesionales inmobiliarios") son profesionales cuyas misiones son participar en las operaciones en propiedad de otros y relacionadas con:

- comprar, vender, investigar, intercambiar, alquilar o subarrendarte, estacionalmente o no, desnudos o amueblados, de edificios construidos o no dedos;
- Compra, venta o arrendamiento de fondos comerciales;
- la venta de un rebaño (fondo de ganado) vivo o muerto;
- suscribir, comprar, vender acciones o acciones en empresas inmobiliarias o empresas de vivienda;
- La compra, venta de acciones no negociables cuando el activo social incluye un edificio o un fondo comercial;
- Gestión de la propiedad
- la venta de documentos relacionados con la compra, venta, alquiler o subarrendamiento, desnudos o amueblados, de edificios construidos o no, o la venta de fondos comerciales (excluidas las publicaciones por prensa);
- la celebración de cualquier contrato de disfrute de la construcción de tiempo compartido;
- desempeño de las funciones de fideicomisario de condominios.

*Para ir más allá*: Artículos 1 y siguientes de la Ley 70-9, de 2 de enero de 1970, por la que se regulan las condiciones para la realización de actividades relativas a determinadas operaciones que impliquen fondos inmobiliarios y comerciales.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo estas actividades, la persona debe tener una tarjeta de visita con la siguiente información:

- actividades profesionales, a saber, las siguientes especialidades:- "transacciones en fondos inmobiliarios y comerciales"
  - "Gestión inmobiliaria"
  - "copropietario fideicomisario"
  - "lista de comerciantes",
  - «servicios turísticos»,
  - "prestación de servicios" para personas en situación de libre suministro;
- La identidad del titular y la dirección de su institución principal;
- La identidad del presidente y el nombre de la cámara de comercio e industria territorial o departamental que emitió la tarjeta;
- El número y las fechas del inicio y el final de la tarjeta;
- El número de identificación único de la empresa.

Para obtener una tarjeta de visita, la persona debe justificar:

- aptitud profesional, que puede ser:- un diploma o un título de formación (véase infra "2". a. Capacitación"),
  - al menos tres años de experiencia profesional en una actividad correspondiente a la mención solicitada en la tarjeta de visita,
  - nacional de otro Estado miembro de la Unión Europea (UE) o de una parte en el Espacio Económico Europeo (EEE), un certificado de competencia o un certificado de formación expedido por la autoridad competente de dicho Estado;
- Una garantía financiera
- seguro de responsabilidad civil
- no estar incapacitado o prohibido de practicar.

**Tenga en cuenta que**

Un profesional de bienes raíces puede ser elegible para ejercer en nombre de un agente con una tarjeta de visita. Si es necesario, estará sujeto a las condiciones de discapacidad y seguro de la misma manera que las que tienen la tarjeta profesional.

*Para ir más allá*: Artículos 3 y 4 de la Ley de 2 de enero de 1970.

#### Entrenamiento

Para calificar como profesional de bienes raíces, el individuo debe tener uno de los siguientes valores:

- una licenciatura (B.A. 3) y una licenciatura en derecho;
- El diploma o título en el directorio nacional de certificaciones profesionales de un nivel equivalente (B.A. 3);
- Patente del Técnico Superior (BTS) "Profesiones Inmobiliarias";
- Instituto de Estudios Económicos y Jurídicos aplicados a la construcción y la vivienda.

**Costos asociados con la calificación******

La formación que conduce a la cualificación profesional se paga y el costo varía de acuerdo con el curso previsto. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Todo nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) que esté establecido y lleve a cabo legalmente actividades inmobiliarias en ese Estado podrá llevar a cabo las mismas actividades en Francia con carácter temporal y Ocasional.

Para ello, tendrá que hacer una declaración previa con el presidente de la Cámara de Comercio e Industria Territorial o la cámara departamental de Ile-de-France (véase infra "5o). a. Solicitar una predeclaración, para el nacional, para un ejercicio temporal o casual (LPS) ").

No obstante, cuando ni la actividad profesional ni la formación estén reguladas en la UE o en el Estado del EEE, el interesado podrá ejercer en Francia de forma temporal y ocasional si justifica haber realizado dicha actividad durante el menos de un año en los diez años anteriores a la prestación.

Además, el nacional podrá solicitar una tarjeta de visita europea (véase infra "5o. c. Tarjeta Profesional Europea (CPE) para nacionales que trabajen como agente inmobiliario").

*Para ir más allá*: Artículo 8-1 de la Ley de 2 de enero de 1970.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre (LE))

Para llevar a cabo una de las actividades de un profesional inmobiliario de forma permanente en Francia, la UE o el nacional del EEE tendrán que cumplir una de las siguientes condiciones:

- Poseer un certificado de competencia o un certificado de formación expedido por una autoridad competente del Estado miembro de origen;
- han estado en el negocio durante al menos un año en los diez años anteriores, cuando la UE o el Estado del EEE no regulan esta ocupación.

Tan pronto como el nacional cumpla una de estas condiciones, podrá solicitar la entrega de una tarjeta profesional del presidente de la Cámara de Comercio e Industria Territorial o de la Cámara Departamental de Ile-de-France en la jurisdicción que es su principal establecimiento (véase infra "5o. b. Solicitar una tarjeta de visita para el nacional para un ejercicio permanente (LE) ").

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Reglas éticas

El profesional de bienes raíces está sujeto, a lo largo de su práctica, al cumplimiento de las reglas del Código de ética de agentes inmobiliarios, administradores de propiedades, fideicomisarios de condominios y distribuidores de listas.

Como tal, debe cumplir con una serie de reglas, incluyendo:

- no discriminar;
- Actuar con los clientes de forma transparente
- Respetar la confidencialidad del negocio que maneja;
- para evitar cualquier conflicto de intereses.

*Para ir más allá*: Apéndice 1 del Decreto No 2015-1090, de 28 de agosto de 2015, por el que se establecen las normas del Código de ética aplicables a determinadas personas dedicadas a la transacción y gestión de edificios y fondos comerciales.

### c. Sanciones disciplinarias

En caso de sanción, el profesional inmobiliario puede incurrir en una sanción disciplinaria, a saber:

- Una advertencia
- Culpar
- una prohibición temporal de la práctica durante un máximo de tres años;
- una prohibición permanente de la práctica.

*Para ir más allá*: Artículos 13-4 y siguientes de la Ley de 2 de enero de 1970.

### d. Sanciones penales y administrativas

El hecho de que un profesional inmobiliario practique ilegalmente se castiga con seis meses de prisión y una multa de 7.500 euros. También se enfrenta a una pena de dos años de prisión y una multa de 30.000 euros si mantiene ilegalmente fondos o bienes durante su ejercicio.

El profesional inmobiliario también incurre en una multa administrativa por violar las normas publicitarias[Artículo L111-1 del Código del Consumidor](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000020625532&cidTexte=LEGITEXT000006069565).

*Para ir más allá*: Artículo 14 y siguientes de la Ley de 2 de enero de 1970.

4°. Educación continua y seguros
-----------------------------------------------------

### a. Educación continua

La formación continua de 14 horas al año o 42 horas durante tres años consecutivos de práctica es obligatoria para cualquier profesional inmobiliario. La renovación de su tarjeta de visita está condicionada al cumplimiento de este requisito.

*Para ir más allá*: Artículos 1 y siguientes del Decreto No 2016-173 de 18 de febrero de 2016 relativo a la formación continua de los profesionales inmobiliarios.

### b. Garantía financiera

El profesional de bienes raíces, que posee o está solicitando la tarjeta de visita, debe solicitar una garantía financiera de al menos la cantidad máxima de fondos que planea tener. Esta garantía está destinada a cubrir los créditos que han sido diseñados durante su actividad. Además, el titular de la tarjeta solo podrá recibir pagos o descuentos dentro del importe de dicha garantía. Una vez que haya contratado esta garantía, el fondo de depósito y depósito le emite un certificado de garantía.

*Para ir más allá*: Artículos 27 y siguientes del decreto de 20 de julio de 1972.

### c. Seguro de responsabilidad civil profesional

Como profesional de bienes raíces, el nacional debe estar en posesión de un contrato de seguro con su información de contacto, así como los de la agencia de seguros para cubrir sus riesgos financieros en el curso de su actividad. El límite de la garantía no puede ser inferior a 76.224,51 euros al año para el mismo asegurado.

*Para ir más allá*: Artículo 49 del Decreto de 20 de julio de 1972; decreto de 1 de julio de 2005 por el que se establecen las condiciones de seguro y la forma del documento justificante previsto por el decreto de 20 de julio de 1972.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar un informe previo para el nacional para un ejercicio temporal o casual (LPS)

**Autoridad competente**

El presidente de la Cámara de Comercio e Industria Territorial o la Cámara Departamental de Ile-de-France en la que el nacional tiene previsto obtener su primer beneficio es competente para decidir sobre la solicitud de declaración.

**Documentos de apoyo**

La solicitud podrá realizarse por carta recomendada o electrónicamente e ir acompañada de un fichero que contenga los siguientes documentos justificativos:

- un certificado que justifique que el profesional inmobiliario está legalmente establecido en un Estado miembro de la UE o del EEE y que no está prohibido ejercer;
- Cuando el Estado miembro no regule la actividad, cualquier documento que justifique la experiencia laboral durante al menos un año en los diez años anteriores a la prestación;
- Un certificado de nacionalidad
- Un certificado de garantía financiera
- un certificado de seguro de responsabilidad civil profesional;
- en caso afirmativo, una declaración sobre el honor de que no posee fondos, efectos o valores distintos de los representativos de su remuneración.

*Para ir más allá*: Artículo 16-6 del Decreto de 20 de julio de 1972.

### b. Solicitar una tarjeta de visita para el nacional para un ejercicio permanente (LE)

**Autoridad competente**

El presidente de la Cámara de Comercio e Industria Territorial o la Cámara Departamental de Ile-de-France es responsable de expedir la tarjeta profesional a un nacional que desee ejercer en Francia.

**Documentos de apoyo**

La solicitud, enviada por carta recomendada, debe ir acompañada de un archivo que contenga los siguientes documentos justificativos:

- El formulario[Cerfa 15312*01](https://www.formulaires.modernisation.gouv.fr/gf/Cerfa_15312.do) completado y firmado;
- Prueba de la aptitud profesional del solicitante
- Un certificado de garantía financiera
- un certificado de seguro de responsabilidad civil profesional;
- un extracto del Registro de Comercio y Sociedades (RCS) de menos de un mes de edad si la persona está registrada, o el doble de la demanda;
- dependiendo del caso:- certificado con el número de cuenta emitido por la entidad de crédito que abrió la cuenta,
  - o un certificado de apertura en nombre de cada principal de cuentas bancarias;
- en su caso, la declaración de honor de que el solicitante no posee, directa o indirectamente, fondos distintos de los que representan su remuneración;
- boletín 2 del historial penal nacional o equivalente para los nacionales de otro Estado miembro de la UE.

**Costo**

La solicitud debe ir acompañada de[Tasas de archivo de investigación](https://www.entreprises.cci-paris-idf.fr/web/formalites/demande-carte-professionnelle-immobilier) por un importe de 120 euros.

**hora**

Si el expediente está incompleto, la autoridad competente envía al profesional una lista de los documentos que faltan dentro de una quincena de horas de su recepción. Si su expediente no se completa en un plazo de dos meses, su solicitud de tarjeta será rechazada.

*Para ir más allá*: Artículos 2 y siguientes del decreto de 20 de julio de 1972.

### c. Tarjeta Profesional Europea (CPE) para nacionales que trabajan como agentes inmobiliarios

La tarjeta profesional europea es un procedimiento electrónico para reconocer las cualificaciones profesionales en otro Estado de la UE.

El procedimiento CPE puede utilizarse tanto cuando el nacional desea operar en otro Estado de la UE:

- temporales y ocasionales;
- de forma permanente.

El CPE es válido:

- indefinidamente en caso de un acuerdo a largo plazo;
- 18 meses para la prestación de servicios con carácter temporal.

**Solicitud de tarjeta de visita europea**

Para solicitar un CPE, el nacional debe:

- Cree una cuenta de usuario en el[Servicio de autenticación de la Comisión Europea](https://webgate.ec.europa.eu/cas) ;
- a continuación, rellene su perfil de CPE (identidad, información de contacto...).

**Tenga en cuenta que**

También es posible crear una aplicación CPE descargando los documentos de apoyo escaneados.

**Costo**

Por cada solicitud de CPE, las autoridades del país de acogida y del país de origen pueden cobrar una tasa de revisión de los archivos, cuyo importe varía en función de la situación.

**hora**

En el caso de una solicitud de CPE para una actividad temporal y ocasional, la autoridad del país de origen reconoce la recepción de la solicitud de CPE en el plazo de una semana, informa si faltan documentos e informa de los costos. A continuación, las autoridades del país anfitrión revisan el caso.

Si no se requiere verificación con el país anfitrión, la autoridad del país de origen revisa la solicitud y toma una decisión final en un plazo de tres semanas.

Si se requieren verificaciones dentro del país anfitrión, la autoridad del país de origen tiene un mes para revisar la solicitud y reenviarla al país anfitrión. A continuación, el país anfitrión toma una decisión final en un plazo de tres meses.

En el caso de una solicitud de cpE para una actividad permanente, la autoridad del país de origen reconoce la recepción de la solicitud de CPE en el plazo de una semana, informa si faltan documentos e informa de los costos. A continuación, el país de origen dispone de un mes para revisar la solicitud y reenviarla al país anfitrión. Este último toma la decisión final en un plazo de tres meses.

Si las autoridades del país anfitrión creen que el nivel de educación o formación o experiencia laboral está por debajo de los estándares requeridos en ese país, pueden pedir al solicitante que realice una prueba de aptitud o que complete una pasantía Adaptación.

**Emisión de la aplicación CPE**

Si se concede la solicitud para el CPE, después es posible obtener un certificado CPE en línea.

Si las autoridades del país anfitrión no tocan una decisión dentro del tiempo asignado, las cualificaciones se reconocen tácitamente y se emite un CPE. A continuación, es posible obtener un certificado CPE de su cuenta en línea.

Si se desestima la solicitud de CPE, la decisión de denegación debe estar justificada y presentar los recursos para impugnar dicha denegación.

*Para ir más allá*: Artículos 16-8 y siguientes del decreto de 20 de julio de 1972.

### d. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

