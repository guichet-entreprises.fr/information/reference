﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP216" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sector marítimo" -->
<!-- var(title)="Piloto marino" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sector-maritimo" -->
<!-- var(title-short)="piloto-marino" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sector-maritimo/piloto-marino.html" -->
<!-- var(last-update)="2020-04-15 17:22:18" -->
<!-- var(url-name)="piloto-marino" -->
<!-- var(translation)="Auto" -->


Piloto marino
=============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:18<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El piloto de marina es un profesional cuya misión es ayudar al capitán de un barco en sus maniobras de salida y entrada en puertos, estuarios, arroyos y canales. Les aconseja sobre las operaciones de aproximación, atraque, salida o remolque del barco.

Adjunto a una estación piloto, domina todos los detalles y proporciona las instrucciones y consejos necesarios al comandante.

*Para ir más allá*: Artículos L. 5341-1 y L. 5341-2 del Código de Transporte.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ejercer como piloto de marina, el candidato debe poseer una licencia expedida por el prefecto regional, interviniendo tras el éxito en una competición organizada por la estación de vuelo en la que la persona desea razonara.

Las estaciones de vuelo determinan cada una sus propias condiciones para la admisión a la competición. Sin embargo, existen disposiciones comunes, entre ellas:

- Estar entre los 24 y 35 años para competir en una estación piloto;
- haber navegado durante al menos 72 meses en la marina mercante o en buques estatales, incluidos 48 meses en el servicio de navegación y vigilando los buques armados de larga distancia, costas, aguas profundas o en alta mar;
- Tener las habilidades físicas
- para tener un título de entrenamiento.

*Para ir más allá*: Artículos R. 5341-24 y R. 5341-28 del Código de Transporte.

#### Entrenamiento

Para participar en la competición, el candidato debe tener primero los documentos de formación requeridos por la estación de vuelo, que pueden ser:

- El certificado de navegación marítima del capitán de primera clase;
- El título de posgrado de la marina mercante;
- La patente del capitán 3000.

Una vez que el candidato tiene uno de estos diplomas, puede participar en el concurso remitiendo un expediente a la Dirección Departamental de Territorios y al Mar de que se trate, incluyendo los siguientes documentos justificativos:

- Una declaración razonada y manuscrita
- Una encuesta de navegación
- un extracto no.3 de los antecedentes penales de menos de tres meses;
- El certificado TOEIC de menos de dos años con el número de puntos necesarios para obtener el título de formación;
- En caso necesario, la lista de vueltas realizadas bajo el suplente con los conductores de la estación de que se trate antes de la competición;
- cualquier certificado que indique las funciones tomadas a bordo de un buque de la marina estatal o mercante;
- un certificado de aptitud física.

El jurado del concurso revisa los documentos en el expediente y decide presentar al candidato para las pruebas escritas y orales, acompañado de una entrevista individual.

La admisión se pronuncia si la persona ha obtenido una puntuación media de 12 sobre 20, sin puntuación inferior a 5 de 20

*Para ir más allá*: Artículo R. 5341-24;[decreto de 26 de septiembre de 1990](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000026771294) relacionados con la organización y el programa de los concursos piloto.

#### Acondicionamiento físico

El ejercicio de la profesión está sujeto a un examen médico para justificar la aptitud física del candidato. Este último se determina durante las visitas médicas que tienen lugar anualmente, a lo largo de la carrera del piloto marítimo.

Si durante una visita se descubrió que el piloto no era físicamente apto, podría ser referido a una comisión de visita local, así como a un comité de contra-visita al solicitarlo.

En caso de una incapacidad física probada, el piloto se verá obligado a dejar de realizar la función, por decisión del prefecto.

*Para ir más allá*: Artículo R. 5341-26 del Código de Transporte;[decretado a partir del 8 de abril de 1991](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006077516) aptitud para las tareas de piloto y capitán piloto.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (Servicio Gratuito)

No existe ningún reglamento para un nacional de un Estado miembro de la Unión Europea (UE) o parte del Espacio Económico Europeo (EEE) que desee ejercer como piloto marítimo en Francia, ya sea de forma temporal o ocasional.

Por lo tanto, sólo las medidas adoptadas para el libre establecimiento de nacionales de la UE o del EEE (véase más adelante "5. "Proceso de Reconocimiento de Calificación y Formalidades") encontrará que se aplican.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

El nacional de un Estado de la UE o del EEE, que lleve a cabo legalmente la actividad de piloto marítimo en ese Estado, podrá establecerse en Francia para llevar a cabo la misma actividad de forma permanente.

En primer lugar, debe solicitar un visado para obtener un visado de reconocimiento de designación profesional de la Dirección Interregional del Mar, con el fin de solicitar la competencia de pilotos marítimos en una estación piloto (véase infra "5o). a. Obtener un visado para la UE o el nacional del EEE para un ejercicio permanente (LE) ").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá justificar su realización en uno o varios Estados miembros durante al menos dos años, en un plazo de diez años. años antes de la actuación.

*Para ir más allá*: Artículo 10 del año[decreto de 24 de junio de 2015](https://www.legifrance.gouv.fr/eli/decret/2015/6/24/DEVT1502017D/jo/texte) en relación con la emisión de documentos de formación profesional marítima y las condiciones en que los buques armados operan a bordo de cultivos comerciales, recreativos, pesqueros y marinos.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El interesado debe respetar las condiciones morales y no haber sido condenado por una sentencia correccional o penal.

Para ello, deberá presentar un extracto de su historial delictivo de menos de tres meses o un certificado de menos de tres meses, del Estado de la UE o del EEE, que certifique estas condiciones.

*Para ir más allá*: Artículo L. 5521-4 del Código de Transporte.

4°. Seguro
-------------------------------

El propietario del buque pilotado por el interesado es civilmente responsable de los daños causados por el buque durante las maniobras de pilotaje. En este caso, el piloto proporcionará una garantía que será abandonada cuando su responsabilidad se ponga en la línea por mala conducta comprobada.

Por otro lado, cuando el piloto es el propietario de la nave, él o ella tendrá que tomar un seguro de responsabilidad civil profesional él mismo.

*Para ir más allá*: Artículos L. 5341-11 a L. 5341-14 del Código de Transporte.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Obtener un visado para la UE o el Nacional del EEE para un ejercicio permanente (LE)

**Autoridad competente**

El Director Interregional del Mar, situado en la región administrativa del puerto de armamentos del buque, es responsable de expedir el certificado de reconocimiento que autoriza el ejercicio permanente del piloto marítimo en Francia.

**Documentos de apoyo**

Para ejercer la profesión de piloto marítimo de forma permanente en Francia, el interesado debe presentar un expediente completo que contenga:

- Una identificación válida
- Una copia de los títulos, títulos o certificados que justifican la cualificación profesional del nacional;
- un certificado que justifique su actividad durante al menos dos años en los últimos diez años, cuando ni la formación ni el ejercicio de la profesión están regulados en el Estado miembro, así como los detalles de las lecciones seguidas y el contenido y la duración Prácticas validadas
- Un certificado de aptitud física para navegar
- una copia de un extracto de los antecedentes penales del nacional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Resultado del procedimiento**

La autoridad competente dispone de dos meses para decidir sobre la solicitud de visado, tan pronto como se reciba el expediente completo.

El silencio guardado al final de este período valdrá la pena la decisión de rechazar la solicitud de visado.

Si se acepta la solicitud, la autoridad competente podrá expedir un certificado temporal de tres meses con antelación que le permita solicitar una vacante en una estación de vuelo. El visado final se le dará por un período de cinco años, renovable a petición.

*Para ir más allá*: Artículos 10 y siguientes del decreto de 24 de junio de 2015;[decreto de 25 de septiembre de 2007](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000794396) en relación con el reconocimiento de documentos de formación profesional marítima expedidos por otros Estados miembros de la UE o terceros países para el servicio a bordo de armas de comercio y buques de recreo con pabellón francés.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

