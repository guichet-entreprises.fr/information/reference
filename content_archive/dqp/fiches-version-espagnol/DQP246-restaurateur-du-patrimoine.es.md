﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP246" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Arte y cultura" -->
<!-- var(title)="Restaurador de patrimonio" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="arte-y-cultura" -->
<!-- var(title-short)="restaurador-de-patrimonio" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/arte-y-cultura/restaurador-de-patrimonio.html" -->
<!-- var(last-update)="2020-04-15 17:20:43" -->
<!-- var(url-name)="restaurador-de-patrimonio" -->
<!-- var(translation)="Auto" -->


Restaurador de patrimonio
=========================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:43<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El conservador puede actuar en conservación preventiva (acción sobre el medio ambiente de la obra con el fin de reducir el riesgo de degradación), conservación curativa (intervención directa sobre el objeto para estabilizar su estado), restauración ( intervención directa sobre el objeto para mejorar su condición, conocimiento, comprensión y uso). Antes de cualquier intervención, el profesional establece una declaración del estado de la propiedad sobre la base de la observación en profundidad, documentación y, en su caso, análisis o estudios adicionales. Basándose en estos elementos, formula un diagnóstico, un pronóstico y propuestas de intervención.

El restaurador puede ejercer bajo diversos estatus profesionales, como artesano, artesano o empleado de empresa liberal. Puede trabajar para particulares, operadores del mercado del arte, instituciones privadas o públicas.

Para intervenir en la restauración de los bienes que forman parte de las colecciones de museos en Francia, se requieren condiciones especiales para la cualificación profesional.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de restaurador patrimonial en Francia se puede llevar a cabo tras el seguimiento de la formación especializada en grados y/o la adquisición de experiencia profesional en este campo.

Por otro lado, las intervenciones de restauración en la propiedad de las colecciones de museos en Francia están reservadas exclusivamente para cualquier persona:

- Titular de un diploma francés con fines profesionales en el ámbito de la restauración patrimonial, expedido después de cinco años de formación en el mismo campo;
- haberse beneficiado de la validación de la experiencia (VAE) en la restauración del patrimonio;
- titular de un diploma francés con fines profesionales en el ámbito de la restauración patrimonial, otorgado después de cuatro años de formación en el mismo campo, obtenido antes del 29 de abril de 2002;
- que, entre el 28 de abril de 1997 y el 29 de abril de 2002, restauró la propiedad de los museos que habían recibido o probablemente recibirel con el nombre de "Museo de Francia" y que estaba facultado por el Ministro responsable de la cultura para llevar a cabo operaciones de restauración propiedad del museo en Francia;
- tener la condición de funcionario y tener una vocación legal para asegurar los trabajos de restauración.

*Para ir más lejos:* Artículo R. 452-10 del Código de Patrimonio.

#### Entrenamiento

La expedición del título francés que permita la restauración de bienes de las colecciones de los museos de Francia debe tener lugar al final de un curso de formación de cinco años durante el cual el interesado habrá seguido enseñanzas teóricas y prácticas, complementadas por pasantías en Francia y en el extranjero.

La formación concluye con la realización de una disertación de fin de ciclo sobre una obra de restauración de un bien cultural, apoyada ante un jurado de profesionales de la restauración.

Varias organizaciones ofrecen diplomas que conducen a la profesión de restaurador que puede intervenir en los activos de las colecciones de museos en Francia, incluyendo:

- Instituto del Patrimonio Nacional (INP);
- Universidad Panteón-Sorbonne de París con el máster en conservación y restauración de bienes culturales;
- Escuela Superior de Arte de Aviñón;
- TALM Escuela Superior de Bellas Artes - Tours

*Para ir más lejos:* Artículos 5 y siguientes del capítulo II del auto de 3 de mayo de 2016 relativo a las cualificaciones necesarias para proceder a la restauración de un inmueble que forme parte de las colecciones de los museos de Francia.

#### Costos asociados con la calificación

La formación que conduce a la actividad de restauradores patrimoniales que pueden intervenir en la propiedad de las colecciones de los museos de Francia se paga. Para obtener más información, es aconsejable acercarse a las organizaciones que lo dispensan.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (prestación gratuita de servicios)

Un estado nacional de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) que emplee legalmente la actividad de restauración de bienes en colecciones de museos de interés general en uno de estos Estados puede hacer uso de su título Francia, de forma temporal e informal.

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que está legalmente establecida, deberán haberse llevado a cabo en ese estado durante al menos un año, a tiempo completo o a tiempo parcial, durante los diez años que preceder el rendimiento.

El interesado tendrá que hacer una declaración, antes de su primera actuación, al Ministro responsable de la cultura (véase infra "5o. a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)").

*Para ir más lejos:* Artículo R. 452-12 del Código de Patrimonio.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Todo nacional de un Estado de la UE o del EEE que esté establecido y practique legalmente la actividad de restauración de bienes en colecciones de museos de interés general en ese Estado podrá llevar a cabo la misma actividad en Francia sobre los bienes pertenecientes a la colecciones de museos en Francia de forma permanente si:

- posee un título expedido por una autoridad competente de otro Estado miembro o de un Estado del EEE que regula el acceso a la profesión o la ejerce y permite ejercerla legalmente en dicha profesión;
- posee un certificado de formación expedido por un tercer Estado pero certificado por un Estado miembro u otro Estado del EEE y que este título le ha permitido ejercer esta profesión en ese Estado durante tres años;
- ha estado trabajando para restaurar las colecciones de museos de interés general a tiempo completo o a tiempo parcial durante un año durante los últimos diez años en otro Estado miembro que no regula la formación o el ejercicio de la profesión. , y que la persona posea un título sancionando la formación en enseñanza superior, expedido por las autoridades competentes de un Estado miembro u otro Estado parte en el Espacio Económico Europeo y que acredite su preparación para el ejercicio de este Actividad. No se requiere la condición de ejercicio a tiempo completo durante un año de la actividad de restauración de la propiedad de colecciones de interés general cuando el diploma en poder de las sanciones del solicitante regula la formación en el Estado de origen.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar al Ministro de Cultura el reconocimiento de sus cualificaciones profesionales (véase infra "5o. b. Solicitar el reconocimiento de las cualificaciones profesionales de la UE o del EEE para un ejercicio permanente (LE) ").

Si, durante el examen de la solicitud, el Ministro responsable de la cultura constata que existen diferencias sustanciales entre la formación y la experiencia profesional del nacional y las exigidas en Francia, las medidas de compensación pueden Tomado.

Si el titular posee un certificado de competencia en el sentido del artículo 11 de la Directiva 2005/36/CE, de 7 de septiembre de 2005, relativo al reconocimiento de cualificaciones profesionales, el Ministro podrá denegar el acceso a la profesión y su ejercicio al titular

*Para ir más lejos:* Artículo R. 452-11 del Código de Patrimonio.

El acceso parcial a una actividad profesional bajo la profesión de conservador de un inmueble perteneciente a las colecciones de museos en Francia, podrá concederse caso por caso a los nacionales de un Estado miembro de la UE o del EEE cuando los tres se cumplen las condiciones:

- el profesional está plenamente cualificado para llevar a cabo, en el Estado de origen, la actividad profesional para la que se solicita acceso parcial;
- las diferencias entre la actividad profesional legalmente realizada en el Estado de origen y la profesión regulada en Francia de la restauración de un inmueble perteneciente a las colecciones de museos en Francia son tan importantes que la aplicación de medidas de compensación equivaldría a obligar al solicitante a seguir el programa integral de educación y formación requerido en Francia para tener pleno acceso a esta profesión regulada;
- la actividad profesional es independiente de las actividades de la profesión regulada, especialmente en la medida en que se lleva a cabo de forma independiente en el Estado de origen.

El acceso parcial puede denegarse por razones imperiosas de interés público, si esta denegación es proporcionada a la protección de ese interés.

Las solicitudes de acceso parcial se consideran, según proceda, como solicitudes con el fin de establecer o prestar servicios temporales y ocasionales a la profesión de que se trate.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Aunque las obligaciones no reguladas, éticas y éticas son responsabilidad de los conservadores del patrimonio, incluyendo:

- respetar la integridad física, estética e histórica de los bienes culturales que restauran;
- mantener la discreción profesional sobre la propiedad cultural restaurada;
- tener en cuenta todos los aspectos de la conservación preventiva antes de intervenir directamente en los bienes culturales.

Para obtener más información, es aconsejable consultar el Código de ética redactado por la Confederación Europea de Colegios de Conservadores y Organizaciones de Conservadores (ECCO).

4°. Seguro
-------------------------------

En caso de ejercicio liberal, el conservador patrimonial está obligado a consumir un seguro de responsabilidad civil profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante esta actividad.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)

#### Autoridad competente

El Ministro de Cultura es responsable de decidir sobre la solicitud de declaración.

#### Documentos de apoyo

La solicitud de declaración se realiza mediante la presentación de un expediente enviado por carta recomendado al departamento de los museos de Francia de la Dirección General de Patrimonio, incluidos todos los documentos siguientes:

- Una copia del documento de identidad válido
- Un certificado que certifica que la persona está practicando y está legalmente establecido en un Estado de la UE o del EEE;
- Prueba de las cualificaciones profesionales del nacional;
- prueba por cualquier medio de que ha estado en esta actividad durante un año a tiempo completo o a tiempo parcial en los últimos diez años, cuando ni la actividad profesional ni la formación están reguladas en la UE o en el Estado del EEE.

**Qué saber**

Los documentos justificativos deben estar escritos en francés o traducidos por un traductor certificado, si es necesario.

#### Renovación

La declaración deberá renovarse una vez al año si el nacional desea realizar una nueva prestación en el año en curso, así como en caso de cambio en la situación del nacional.

*Para ir más lejos:* Artículo R. 452-12 y artículo 11 de la Orden de 3 de mayo de 2016 relativo a las cualificaciones necesarias para proceder a la restauración de un inmueble que forme parte de las colecciones de los museos de Francia.

### b. Solicitar el reconocimiento de las cualificaciones profesionales de los nacionales de la UE o del EEE para el ejercicio permanente (LE)

#### Autoridad competente

El Ministro de Cultura es competente para decidir sobre la solicitud de reconocimiento.

#### Documentos de apoyo

La solicitud de reconocimiento de cualificaciones profesionales se realiza presentando un expediente enviado por carta recomendada al departamento de museos francés de la Dirección General de Patrimonio, incluyendo todos los siguientes documentos:

- Una copia del documento de identidad válido
- estado miembro regula el acceso a la profesión:- Una copia de los diplomas, certificados y otros documentos de formación expedidos por la autoridad competente de ese Estado para llevar a cabo legalmente esta actividad,
  - Una copia del currículo detallado de los estudios seguidos;
- cuando el certificado de formación fue emitido por un estado tercero:- Una copia del reconocimiento de este título por parte de un Estado miembro,
  - cualquier documento expedido por ese Estado que justifique que el nacional ha dedicado a la restauración de bienes en colecciones museísticas de interés general durante al menos tres años,
  - Una copia del currículo detallado de los estudios seguidos,
  - Una descripción de la experiencia profesional adquirida
- El Estado miembro no regula la profesión ni la actividad:- prueba por cualquier medio de que el nacional se ha dedicado a la restauración de bienes en colecciones museísticas de interés general en ese estado durante un año en los últimos diez años,
  - Una copia del currículo detallado de los estudios seguidos,
  - una descripción de la experiencia profesional adquirida.

#### Procedimiento

El Departamento de Museos de Francia confirmará la recepción del expediente y podrá solicitar, en su caso, el envío de piezas adicionales. Una vez finalizado el expediente, el Ministro responsable de Cultura dispondrá de dos meses para decidir sobre la solicitud de reconocimiento o informar al nacional de que tendrá que someterse a una de las siguientes medidas de compensación:

- un curso de adaptación que dura hasta un año
- una prueba de aptitud.

Una vez que la persona haya completado el curso de ajuste o la prueba de aptitud, el Ministro responsable de la cultura tendrá dos meses más para decidir y tomar una decisión razonada sobre si reconocer o no sus calificaciones. Profesional.

*Para ir más lejos:* Artículos 5 y siguientes del auto de 3 de mayo de 2016 relativo a las cualificaciones necesarias para proceder a la restauración de un inmueble perteneciente a las colecciones de los museos de Francia.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un formulario de queja en línea.

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

