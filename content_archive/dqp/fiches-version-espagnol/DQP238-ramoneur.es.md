﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP238" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artesanía" -->
<!-- var(title)="Deshollinador" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artesania" -->
<!-- var(title-short)="deshollinador" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/artesania/deshollinador.html" -->
<!-- var(last-update)="2020-04-15 17:20:38" -->
<!-- var(url-name)="deshollinador" -->
<!-- var(translation)="Auto" -->


Deshollinador
=============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:38<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La barredora de chimeneas es un profesional responsable del mantenimiento de los conductos de chimenea, estufas de leña e instalaciones de combustible, petróleo y gas, de acuerdo con la normativa vigente. También limpia chimeneas, estufas, incineradores, calderas, conductos de ventilación y dispositivos de eliminación de humo.

Diagnostica a sus clientes y monitorea el correcto funcionamiento de las instalaciones para evitar cualquier riesgo de incendio o intoxicación por monóxido de carbono.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El interesado que desee trabajar como barredora de chimeneas debe tener una cualificación profesional o un ejercicio bajo el control efectivo y permanente de una persona con esta cualificación.

Para ser considerado profesionalmente calificado, la persona debe poseer uno de los siguientes diplomas o títulos:

- Certificado de Cualificación Profesional (CAP) o Patente de Estudios Profesionales (BEP) en una de estas áreas:- Mampostería
  - Yeserías
  - Mantenimiento
- certificado técnico del comercio (CTM) "barredora de chimeneas";
- patente técnica del comercio (BTM) "barredora de chimeneas";

En ausencia de uno de estos títulos o títulos, el interesado debe justificar una experiencia profesional efectiva de tres años en el territorio de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) adquirida como líder empresarial, trabajadores por cuenta propia o asalariados en la alineación. En este caso, se recomienda al interesado que se ponga en contacto con la Cámara de Comercio y Artesanía (CMA) para solicitar un certificado de cualificación profesional.

*Para ir más allá* :[Artículo 16](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006513344&cidTexte=JORFTEXT000000193678) Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía;[Artículo 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1AC41CCDC69D007EC85FD499D4D6CEA2.tplgfr29s_3?cidTexte=JORFTEXT000000388449&idArticle=LEGIARTI000034624936&dateTexte=20170506&categorieLien=id#LEGIARTI000034624936) Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996.

#### Entrenamiento

Cap y BEP son diplomas de Nivel V (es decir, un segundo ciclo general y tecnológico antes del último año). La BTM es un diploma de Nivel IV (es decir, licenciatura general, tecnológica o profesional).

El CAP es accesible después de una tercera clase. La selección se realiza generalmente en el archivo y en las pruebas. Este diploma es accesible después de un curso de formación en condición de estudiante, contrato de aprendizaje, después de un curso de educación continua, contrato de profesionalización, solicitud individual o por[validación de la experiencia](http://www.vae.gouv.fr/) (VAE). La formación normalmente dura dos años y tiene lugar en una escuela secundaria vocacional.

El BEP está disponible en un contrato de aprendizaje, después de un curso de formación continua, en un contrato de profesionalización, por aplicación individual o por validación de la experiencia. Muy a menudo, el entrenamiento que conduce a la obtención es por dos años.

La CTM es un título específico de la artesanía que se está preparando a partir del tercero. Este diploma es accesible después de un curso de formación bajo estatus de estudiante o estudiante, en un contrato de aprendizaje, después de un curso de educación continua, en un contrato de profesionalización o por validación de la experiencia. Muy a menudo, el entrenamiento que conduce a la obtención es por dos años.

El BTM es un título específico de la artesanía abierto al titular de un diploma de Nivel IV o V. El diploma es accesible después de un curso de formación continua, en un contrato de aprendizaje, en un contrato de profesionalización, por aplicación individual o por validación de la experiencia. El entrenamiento normalmente dura dos años.

*Para ir más allá*: Artículos D. 337-1 y los siguientes artículos del Código de Educación;[Portal de artesanía](http://www.artisanat.fr/Espacejeunes/Devenirapprenti/Lesparcoursetlesdipl%C3%B4mes/tabid/99/Default.aspx).

#### Costos asociados con la calificación

El entrenamiento suele ser gratuito. Para más detalles, es aconsejable consultar el centro de formación en cuestión.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (Servicio Gratuito)

Cualquier nacional de un Estado miembro de la UE o del EEE establecido y que actúe legalmente la actividad de barrido de chimeneas en ese Estado podrá realizar la misma actividad en Francia de forma temporal y ocasional.

En primer lugar, deberá presentar la solicitud mediante declaración a la CMA del lugar en el que desea llevar a cabo el servicio.

En el caso de que la profesión no esté regulada, ni en el curso de la actividad ni en el marco de la formación, en el país en el que el profesional esté legalmente establecido, deberá haber realizado esta actividad durante al menos un año durante los diez años antes de la prestación, en uno o varios Estados miembros de la UE.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la CMA competente podrá exigir que el interesado se someta a una prueba de aptitud.

*Para ir más allá*: Artículo 17-1 de la Ley de 5 de julio de 1996; Artículo 2 de la[decreto del 2 de abril de 1998](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000388449&categorieLien=cid) modificado por el[decreto del 4 de mayo de 2017](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7D50D037AAEEDE8367FE375890CB8510.tplgfr39s_2?cidTexte=JORFTEXT000034598573&dateTexte=20170506).

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Para llevar a cabo una actividad de barrido de chimeneas en Francia de forma permanente, la UE o el nacional del EEE deben cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba "2 grados). a. Cualificaciones profesionales");
- poseer un certificado de competencia o certificado de formación requerido para el ejercicio de la actividad de barrido de chimeneas en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su preparación para el ejercicio de la actividad de barrido de chimeneas cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de este Actividad
- ser un diploma, título o certificado adquirido en un tercer país eu-11 y admitido en equivalencia por un Estado de la UE o parte en el EEE con la condición adicional de que la persona ha sido barrendero durante tres años en el estado que ha admitido Equivalencia.

Una vez que el nacional de un Estado de la UE o del EEE cumpla una de las condiciones anteriores, podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante "5o). b. Solicitar un certificado de cualificación profesional para el ejercicio permanente de la UE o del EEE para un ejercicio permanente (LE))

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la CMA competente podrá exigir que el interesado se someta a medidas de compensación (véase infra "5o. a. Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 a 3-2 del Decreto de 2 de abril de 1998 modificado por el decreto de 4 de mayo de 2017.

3°. Condiciones de honorabilidad
-----------------------------------------

Nadie puede ejercer la profesión si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá*: Artículo 19 III de la Ley 96-603, de 5 de julio de 1996.

4°. Seguro
-------------------------------

El barrido liberal de chimeneas debe llevar un seguro de responsabilidad civil profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

La CMA del lugar en el que el nacional desea llevar a cabo el beneficio es competente para emitir la declaración previa de actividad.

**Documentos de apoyo**

La solicitud de un informe previo de la actividad va acompañada de un archivo completo que contiene los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE;
- un documento que justifique la cualificación profesional del nacional que puede ser, a su elección:- Una copia de un diploma, título o certificado,
  - Un certificado de competencia,
  - cualquier documentación que acredite la experiencia profesional del nacional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tenga en cuenta que**

Cuando el expediente está incompleto, la CMA tiene un plazo de quince días para informar al nacional y solicitar todos los documentos que faltan.

**Resultado del procedimiento**

Al recibir todos los documentos en el archivo, el CMA tiene un mes para decidir:

- autorizar la prestación cuando el nacional justifique tres años de experiencia laboral en un Estado de la UE o del EEE, y adjuntar a dicha Decisión un certificado de cualificación profesional;
- o autorizar la disposición cuando las cualificaciones profesionales del nacional se consideren suficientes;
- o imponerle una prueba de aptitud cuando existan diferencias sustanciales entre las cualificaciones profesionales del nacional y las exigidas en Francia. En caso de denegación de esta medida de compensación o en caso de incumplimiento en su ejecución, el nacional no podrá llevar a cabo la prestación de servicios en Francia.

El silencio guardado por la autoridad competente en estos tiempos merece autorización para iniciar la prestación de servicios.

*Para ir más allá*: Artículo 2 del Decreto de 2 de abril de 1998; Artículo 2 de la[17 de octubre de 2017](https://www.legifrance.gouv.fr/eli/arrete/2017/10/17/ECOI1719273A/jo) respecto a la presentación de la declaración y las solicitudes previstas en el Decreto 98-246, de 2 de abril de 1998, y el título I del Decreto 98-247, de 2 de abril de 1998.

### b. Solicitar un certificado de reconocimiento de la cualificación profesional para el nacional de la UE o del EEE en caso de ejercicio permanente (LE)

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la CMA correspondiente del lugar en el que la persona desee resolver.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA pide al interesado que lo complete dentro de una quincena de la presentación del expediente. Se emite un recibo tan pronto como se completa.

**Documentos de apoyo**

La solicitud de certificación de cualificación profesional es un fichero con los siguientes documentos justificativos:

- Una solicitud de certificado de cualificación profesional
- una prueba de cualificación profesional en forma de certificado de competencia o un diploma o un certificado de formación profesional;
- Una fotocopia del documento de identidad válido del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

**Qué saber**

Si es necesario, todos los documentos justificativos deben ser traducidos al francés por un traductor certificado.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (Ciep) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**hora**

Dentro de los tres meses siguientes a la recepción, la CMA podrá decidir:

- Reconocer la cualificación profesional y emitir la certificación de cualificación profesional;
- someter al nacional a una medida de indemnización y notificarle esa decisión;
- negarse a expedir el certificado de cualificación profesional.

**Qué saber**

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Remedios**

Si la CMA rechaza la solicitud de cualificación profesional de la CMA, el solicitante puede impugnar la decisión. Por lo tanto, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, puede formar:

- una impugnación legal ante el tribunal administrativo pertinente;
- una apelación agraciada al prefecto del departamento de CMA pertinente;
- una impugnación legal ante el tribunal administrativo pertinente.

**Costo**

Gratis.

**Bueno saber: medidas de compensación**

La CMA notifica al solicitante su decisión de que realice una de las medidas de compensación. Esta Decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son imprescindibles para ejercer en Francia.

A continuación, el solicitante debe elegir entre un curso de ajuste de hasta tres años o una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final del curso de ajuste, el solicitante envía al CMA un certificado que certifica que ha completado válidamente esta pasantía, acompañado de una evaluación de la organización que lo supervisó. La CMA emite, sobre la base de este certificado, un certificado de cualificación profesional en el plazo de un mes.

La decisión de utilizar una medida de indemnización podrá ser impugnada por el interesado, que deberá presentar un recurso administrativo ante el prefecto en el plazo de dos meses a partir de la notificación de la decisión. Si su apelación es desestimada, puede iniciar una impugnación legal.

**Costo**

Se puede cobrar una tarifa fija que cubra la investigación del caso. Para obtener más información, es aconsejable acercarse a la CMA correspondiente.

*Para ir más allá*: Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

