﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP006" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Seguridad" -->
<!-- var(title)="Oficial de investigación privado" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="seguridad" -->
<!-- var(title-short)="oficial-de-investigacion-privado" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/seguridad/oficial-de-investigacion-privado.html" -->
<!-- var(last-update)="2020-04-15 17:22:20" -->
<!-- var(url-name)="oficial-de-investigacion-privado" -->
<!-- var(translation)="Auto" -->


Oficial de investigación privado
================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:20<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El oficial de investigación privada ("ARP"), también conocido como "investigador privado", tiene la tarea de recopilar toda la información o información comercial, privada o industrial para construir un archivo que será admisible en caso de un procedimiento judicial. . Interviene en la materialización de las pruebas de las que depende la resolución de una controversia. Su condición de agente privado le permite viajar en territorio francés y en el extranjero para llevar a cabo su misión.

*Para ir más allá* Sección L. 621-1 del Código de Seguridad Nacional.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Cualquier persona que desee llevar a cabo la actividad de ARP debe acreditar una aptitud profesional y tener una tarjeta profesional emitida por la Junta Local de Acreditación y Control (CNAC) establecida dentro del Consejo Nacional de Actividades (Cnaps).

La tarjeta de visita se puede emitir en las siguientes condiciones:

- no haber sido condenado a una sentencia correccional o penal registrada en el expediente penal número 2 o para los extranjeros en un documento equivalente, incompatible con el ejercicio de una actividad delictiva Seguridad privada;
- justifican las cualificaciones necesarias para llevar a cabo la actividad de protección física de las personas: seguida de la formación en una organización de formación autorizada por los Cnaps y poseer una certificación profesional registrada en el RNCP, justificar una equivalencia bajo ciertas condiciones (policía, gendarmes, policía municipal, militar);
- para los extranjeros, para tener un permiso de residencia para llevar a cabo una actividad en el territorio nacional;
- no estar sujeto a una orden de expulsión o prohibición del territorio francés actual.

*Para ir más allá*: Artículo L. 622-19 del Código de Seguridad Nacional.

#### Entrenamiento

La formación profesional para justificar la capacidad profesional de ARP está sujeta a una autorización emitida por los Cnaps. Esta decisión puede venir:

- antes de reclutar a una empresa que protege a las personas. En este caso, el interesado recibirá una**autorización previa para el acceso a la formación** 6 meses, para ser entregados a un centro de formación (autorizado por los Cnaps). Las certificaciones profesionales que proporcionan la capacidad de llevar a cabo la actividad A3P se actualizan regularmente. Están registrados en el RNCP y aparecen en el sitio web de Cnaps;
- durante la contratación. Una persona que haya celebrado un contrato de trabajo con una empresa de protección privada debe solicitar una**autorización provisional para ser empleada** válido durante 6 meses. Esta autorización no le permite ocupar un puesto de A3P, pero compromete a la compañía a proporcionarle entrenamiento inmediato para justificar su aptitud para practicar. La solicitud de autorización se realiza directamente[en línea](http://www.cnaps.interieur.gouv.fr/) en el sitio web de Cnaps.

**Tenga en cuenta que**

La solicitud de permiso se realiza directamente en línea en el sitio web de Cnaps.

*Para ir más allá*: Artículos L. 622-1, L. 622-2 y R. 622-17 y siguientes del Código de Seguridad Nacional.

#### Costos asociados con la calificación

La formación que conduce a la profesión de ARP puede dar sus frutos. Para obtener más información, es aconsejable acercarse a las organizaciones de formación que lo dispensan.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Un nacional de un Estado de la UE o del EEE que opere como PRA en uno de estos Estados puede utilizar su designación profesional en Francia de forma temporal y casual.

Tendrá que solicitarlo antes de su primera entrega mediante declaración dirigida a la Comisión Local de Acreditación y Control (CLAC) responsable territorialmente de París (véase infra "5o. a. Hacer una declaración para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que está legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año en los diez años que preceder el rendimiento.

Si el examen de las cualificaciones profesionales muestra diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y su ejercicio en Francia, el interesado podrá ser sometido a una prueba de aptitud en un plazo de tiempo. un mes a partir de la recepción de la solicitud de declaración de la CLAC.

*Para ir más allá*: Artículo R. 622-23 del Código de Seguridad Nacional.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Cualquier nacional de un Estado de la UE o del EEE que esté establecido y lleve a cabo legalmente una actividad de ARP en ese Estado podrá llevar a cabo la misma actividad en Francia de forma permanente.

Tendrá que solicitar una tarjeta profesional, que esté siendo estudiada por la delegación territorial competente y presentada al CLAC territorialmente competente (véase infra "5o). b. Obtener una tarjeta de visita para el nacional de la UE o del EEE para un ejercicio permanente (LE) ").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que está legalmente establecida, la UE o el Nacional del EEE tendrán que justificar la formación en ese estado y experiencia laboral. uno o más Estados de la UE o del EEE al menos un año en los últimos diez años.

Si el examen de las cualificaciones profesionales revela diferencias sustanciales en relación con las necesarias para el acceso a la profesión y su práctica en Francia, el interesado puede estar sujeto a una medida de compensación (véase infra "5o. b. Bueno saber: medidas de compensación").

*Para ir más allá* Sección L. 622-19 del Código de Seguridad Nacional.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Cumplimiento de las normas éticas

Las disposiciones del Código de ética relativas a las actividades de seguridad privada se imponen a todas las PRA que practican en Francia. Están codificados en las secciones R. 631-1 a R. 631-31 del Código de Seguridad Nacional.

En particular, el PRA debe evitar conflictos de intereses, respetar el secreto empresarial, adoptar una actitud profesional en todo momento o garantizar la confidencialidad de los intercambios.

*Para ir más allá*: Artículos R. 631-1 a R. 631-31 del Código de Seguridad Nacional.

### b. Incompatibilidad del ejercicio

Una vez que la persona actúa como ARP, ya no puede:

- monitorear o mantener a las personas o a la propiedad o propiedad
- supervisar y transportar joyas, fondos o metales preciosos;
- Proteger la integridad física de las personas
- proteger a los buques franceses de amenazas de actos terroristas o tomas de control.

*Para ir más allá*: Artículos L. 611-1 y L. 622-2 del Código de Seguridad Nacional.

4°. Educación continua y seguros
-----------------------------------------------------

### a. Obligación de recibir educación continua

La renovación de la tarjeta profesional está sujeta al seguimiento de la formación continua destinada a mantener y actualizar las competencias (MAC) del interesado. Esta formación, impartida en forma de[pasantía](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=426183422AEAB5864A868A3038C21D37.tplgfr22s_2?idArticle=JORFARTI000034104623&cidTexte=JORFTEXT000034104616&dateTexte=29990101&categorieLien=id) 35 horas deben tener lugar dentro de los 24 meses de la fecha de vencimiento de la tarjeta.

*Para ir más allá* :[27 de febrero de 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034104616&categorieLien=id) formación continua de los oficiales privados de investigación.

### b. Seguro de responsabilidad civil profesional

Como profesional independiente, el PRA debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

*Para ir más allá* Sección L. 622-5 del Código de Seguridad Nacional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

#### Autoridad competente

La delegación territorial de la CLAC en la que se encuentra París es competente para decidir sobre la solicitud de declaración.

#### Documentos de apoyo

La solicitud de devolución es un archivo transmitido por cualquier medio, incluidos todos los siguientes documentos:

- Una declaración que contiene información sobre el estado civil del nacional;
- Una fotocopia de su dNI
- Un certificado que certifica que la persona está practicando y está legalmente establecido en un Estado de la UE o del EEE;
- la ausencia de una condena penal en sus antecedentes penales;
- prueba por cualquier medio de que el nacional ha participado en esta actividad, a tiempo completo o a tiempo parcial, en los últimos diez años, cuando ni la actividad profesional ni la formación están reguladas en la UE o en el Estado del EEE.

#### hora

El CLAC tiene un mes para tomar su decisión:

- Permitir que el nacional preste su primer servicio;
- someter a la persona a una medida de compensación en forma de prueba de aptitud, si resulta que las cualificaciones profesionales y la experiencia que utiliza son sustancialmente diferentes de las requeridas para el ejercicio de la profesión en Francia;
- informarles de una o más dificultades que puedan retrasar la toma de decisiones. En este caso, tendrá dos meses para decidir, a partir de la resolución de la dificultad o dificultades. En ausencia de una respuesta de la autoridad competente dentro de estos plazos, puede comenzar la prestación de servicios.

*Para ir más allá*: Artículo R. 622-23 del Código de Seguridad Nacional.

### b. Obtener una tarjeta de visita para el nacional de la UE o del EEE para un ejercicio permanente (LE)

#### Autoridad competente

La CLAC, territorialmente competente, decide sobre la expedición de la tarjeta de visita siempre que el nacional cumpla las condiciones de atribución.

#### Documentos de apoyo

Para obtener la tarjeta de visita, el nacional envía un archivo completo por correo a la CLAC territorialmente competente. Este archivo debe incluir los siguientes documentos auxiliares:

- El formulario completado y firmado;
- Una fotocopia del documento de identidad de un nacional
- Un certificado de empleo expedido por el empleador o futuro empleador del nacional;
- prueba de aptitud profesional que puede ser:- un certificado profesional registrado en el directorio nacional de certificaciones profesionales,
  - un certificado de cualificación profesional desarrollado por su sucursal profesional,
  - un certificado de competencia o certificado de formación expedido por un Estado de la UE o del EEE que regula la actividad de ARP en su territorio, incluidos los detalles y la duración de los módulos de la formación,
  - prueba por cualquier medio de que el nacional ha estado en esta actividad durante un año a tiempo completo o a tiempo parcial en los últimos diez años, cuando ni la actividad profesional ni la formación están reguladas en la UE o en el Estado del EEE.

**Qué saber**

Los documentos justificativos deben estar escritos en francés o traducidos por un traductor certificado, si es necesario.

#### Duración y renovación

La tarjeta de visita se emite en forma desmaterializada de un número de registro y sigue siendo válida durante cinco años. Cualquier cambio en el estado de empleo tendrá que ser notificado a la CLAC, pero no dará lugar a la renovación obligatoria de la tarjeta. Al final de estos cinco años, el profesional podrá solicitar la renovación tres meses antes de la fecha de caducidad, siempre que presente un certificado de educación continua (véase supra "4o). (a) Obligación de someterse a formación profesional continua").

#### Resultado del procedimiento

Una vez que el nacional haya obtenido el número de registro de la CLAC territorialmente competente, tendrá que transmitirlo a su empleador que le expedirá la tarjeta profesional final.

#### Bueno saber: medidas de compensación

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a una medida de compensación, que puede ser:

- un curso de adaptación de hasta tres años
- una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

*Para ir más allá*: Artículos L. 622-19 y R. 622-22 del Código de Seguridad Nacional.

#### Formas y plazos para la apelación

El solicitante podrá impugnar la negativa a expedir la tarjeta de visita en el plazo de dos meses a partir de la notificación de la decisión de denegación mediante la formación de un recurso preadministrativo obligatorio con la Acreditación Nacional y control del Consejo Nacional de Actividades de Seguridad Privada (Cnaps), ubicado 2-4-6, Boulevard Poissonniére, 75009 París.

La Comisión Nacional se pronunciará sobre la base del estatuto fáctico y jurídico vigente en la fecha de su decisión.

Este recurso es obligatorio antes de cualquier litigio. Es gratis.

Los litigios podrán ser ejercitados ante el tribunal administrativo del lugar de residencia del solicitante o ante el tribunal administrativo de París para los solicitantes que tengan su lugar de residencia en el extranjero en el plazo de dos meses a partir de la notificación. decisión expresa de la Comisión Nacional de Acreditación y Control, es decir, la adquisición de la decisión implícita de rechazar el silencio mantenido por la Comisión Nacional de Acreditación y Control durante dos meses a partir de la fecha de recepción del recurso administrativo previo obligatorio.

*Para ir más allá*: Artículo L. 633-3 del Código de Seguridad Nacional; Artículos L. 412-1 a L. 412-8 del Código de Relaciones Público-Gubernamentales; Artículos R. 421-1 a R. 421-7 del Código de Justicia Administrativa.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

