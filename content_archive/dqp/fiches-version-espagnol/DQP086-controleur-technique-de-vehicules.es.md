﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP086" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Seguridad vial" -->
<!-- var(title)="Controlador técnico del vehículo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="seguridad-vial" -->
<!-- var(title-short)="controlador-tecnico-del-vehiculo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/seguridad-vial/controlador-tecnico-del-vehiculo.html" -->
<!-- var(last-update)="2020-04-15 17:22:23" -->
<!-- var(url-name)="controlador-tecnico-del-vehiculo" -->
<!-- var(translation)="Auto" -->


Controlador técnico del vehículo
================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:23<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El controlador técnico del vehículo es un profesional automotriz certificado por el estado cuya actividad es garantizar el correcto funcionamiento y mantenimiento de vehículos de motor ligeros (que no pesen más de 3,5 toneladas) y vehículos pesados.

Como tal, debe llevar a cabo todos los siguientes controles:

- para los vehículos ligeros, los previstos para la[Apéndice I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0CDFD1D0ECCF66C346F803B51C8737D0.tplgfr41s_2?idArticle=LEGIARTI000034159662&cidTexte=LEGITEXT000020559004&dateTexte=20180330) el decreto de 18 de junio de 1991 sobre el establecimiento y la organización del control técnico de vehículos que no pesen más de 3,5 toneladas;
- para los vehículos pesados, los previstos para la[Apéndice I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=A8F5D581C452730D90D4F300AAFBCEDC.tplgfr22s_2?idArticle=LEGIARTI000034200988&cidTexte=LEGITEXT000005838877&dateTexte=20180403) 27 de julio de 2004 sobre el control técnico de los vehículos pesados.

**Tenga en cuenta que**

El profesional puede operar de forma independiente o dentro de la red nacional de control aprobada por el Ministerio de Transporte.

*Para ir más allá*: Artículo L. 323-1 de la Ley de Tráfico de Carreteras.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de control técnico del vehículo, el profesional debe:

- Estar calificado profesionalmente
- acreditar (ver infra "5 grados). a. Solicitud de acreditación");
- práctica en instalaciones con medios físicos (técnicos y informáticos) para llevar a cabo controles técnicos. Estas instalaciones deben ser aprobadas por el prefecto del departamento en las condiciones establecidas en los artículos 16 a 17-2 de la[decretado el 18 de junio de 1991](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000020559004&dateTexte=20180330) para vehículos ligeros y en las condiciones de los artículos 21 a 25-1 de la[pedido a partir del 27 de julio de 2004](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=A8F5D581C452730D90D4F300AAFBCEDC.tplgfr22s_2?cidTexte=JORFTEXT000000626890&dateTexte=20180403) vehículos pesados.

*Para ir más allá*: Artículos L. 323-1 y R. 323-13 a R. 323-15 de la Ley de Tráfico por Carretera.

#### Entrenamiento

**Llevar a cabo la actividad de control técnico de vehículos ligeros**

El profesional debe tener uno de los siguientes títulos:

- un diploma de Nivel V (CAP, BEP, certificado universitario) o un diploma equivalente registrado en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP) en una de las siguientes disciplinas automotrices:- mecánica automotriz,
  - La carrocería,
  - Chapa
  - electricidad automotriz,
  - mantenimiento del coche. Además, el profesional debe haber sometido al menos 900 horas de formación especializada en control técnico;
- un grado de Nivel IV (nivel bac) en una de las disciplinas automotrices mencionadas anteriormente o en uno de los sectores de la industria automotriz (mecánica, producción, automatización electrónica, electromecánica o mantenimiento aeronáutico). También debe haber completado la formación técnica adicional durante un mínimo de 175 horas;
- un diploma de Nivel V o un equivalente registrado en el RNCP en el campo de la automoción, así como una experiencia laboral de al menos dos años en reparación o mantenimiento automotriz. Además, debe haber realizado una formación técnica especializada durante un mínimo de 175 horas;
- un Certificado de Cualificación Profesional (CQP) o un título profesional de controlador técnico automotriz emitido bajo las condiciones de la[Detenido](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000036468664&categorieLien=id) 6 de diciembre de 2017 relativo al título profesional de controlador técnico automotriz de vehículos ligeros.

**Tenga en cuenta que**

También puede practicar al profesional que justifica al menos cinco años de experiencia en reparación automotriz y formación especializada adicional en control técnico durante un mínimo de 175 horas.

*Para ir más allá*: Artículo 12 y[Apéndice IV](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0CDFD1D0ECCF66C346F803B51C8737D0.tplgfr41s_2?idArticle=LEGIARTI000034159670&cidTexte=LEGITEXT000020559004&dateTexte=20180330) 18 de junio de 1991.

**Llevar a cabo la actividad de controlador técnico de vehículos pesados**

El profesional debe tener uno de los siguientes títulos:

- un diploma de nivel V en una disciplina automotriz o, en su defecto, un diploma equivalente al significado del RNCP. Así como formación especializada en el control técnico de vehículos pesados con una duración mínima de 280 horas;
- un diploma de nivel V o un equivalente al RNCP en el campo de la automoción, así como al menos tres años de experiencia en esta disciplina o en el control técnico de vehículos ligeros y la formación especializada en el control técnico de la vehículos pesados de al menos 280 horas (incluidas 175 horas de formación teórica y 105 horas de formación práctica);
- un diploma de Nivel IV o un equivalente al RNCP en el campo de la automoción y han recibido formación especializada en el control técnico de vehículos pesados con una duración mínima de 280 horas (incluyendo una formación teórica de 175 horas y parte de la 105 horas de práctica);
- CQP o un título profesional de controlador técnico automotriz.

*Para ir más allá*: Apéndice IV de la orden de 27 de julio de 2004 sobre el control técnico de los vehículos pesados.

**Formación continua**

Cada año, el profesional está obligado a someterse a una formación continua con el fin de mantener sus habilidades profesionales al día. Como tal, debe justificar que se lleve a cabo:

- Capacitación adicional de al menos 20 horas para vehículos ligeros y al menos 24 horas para vehículos pesados, en una organización reconocida y designada por la red;
- al menos 300 visitas técnicas periódicas para vehículos ligeros o 500 controles técnicos para vehículos pesados;
- al menos una auditoría cada dos años, al término de una visita técnica periódica.

*Para ir más allá* 4. Apéndice IV del auto de 18 de junio de 1991; 2.1 de la Lista IV del Auto de 27 de julio de 2004.

#### Costos asociados con la calificación

El costo de la formación para ejercer como controlador técnico del vehículo varía de acuerdo con el curso previsto. Es aconsejable acercarse a las instituciones interesadas para obtener más información.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido podrá ejercer un control temporal y ocasional de vehículos actividad en Francia.

Cuando el Estado miembro no regule el ejercicio de la actividad o su formación, el nacional deberá haber llevado a cabo esta actividad en uno o varios Estados miembros durante al menos un año en los últimos diez años.

Una vez que cumpla estas condiciones, deberá, antes de cualquier prestación de servicios, hacer una declaración previa a la autoridad competente (véase infra "5o. b. Predeclaración para el nacional de la UE para un ejercicio temporal e informal (LPS)").

*Para ir más allá* Ii de la Sección L. 323-1 de la Ley de Tráfico de Carreteras.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

No se prevén medidas para que la UE o el nacional del EEE estén legalmente establecidos en un Estado miembro para llevar a cabo la actividad de control técnico de los vehículos con carácter temporal en Francia.

Como tal, el nacional está sujeto a los mismos requisitos que el nacional francés (véase infra "5o. a. Solicitud de acreditación").

3°. Condiciones de honorabilidad e incompatibilidad
------------------------------------------------------------

**Incompatibilidades**

El controlador técnico del vehículo no puede participar en ninguna otra actividad relacionada con la reparación o el comercio de automóviles, ya sea de forma independiente o como empleado.

*Para ir más allá*: Artículo L. 323-1 y R. 323-17 de la Ley de Tráfico de Carreteras.

**Integridad**

Para ejercer, el profesional no debe haber sido objeto de ninguna condena en la segunda votación de su historial delictivo.

Además, el nacional de la UE que participa en esta actividad no debe tener ninguna conexión que pueda interferir con su independencia con personas, organizaciones o empresas que se dedican a actividades de reparación o comercio en el sector de la automoción.

*Para ir más allá*: Artículos L. 323-1 y R. 323-17 de la Ley de Tráfico de Carreteras.

4°. Sanciones
----------------------------------

El profesional se enfrenta a una multa de 750 euros si realiza un control técnico sin cumplir con la normativa.

Además, está prohibida la utilización de los resultados de la inspección técnica realizada para fines distintos de los previstos por la normativa.

*Para ir más allá*: Artículos R. 323-19 y R. 323-20 de la Ley de Tráfico de Carreteras.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitud de acreditación

**Autoridad competente**

El profesional deberá enviar su solicitud en dos copias al prefecto del departamento donde se encuentre el centro de control en el que bebe.

**Documentos de apoyo**

Su solicitud toma la forma de un archivo que incluye:

- su solicitud de acreditación como responsable del tratamiento indicando el centro de control en el que desea ejercer (si procede, la red de control aprobada);
- Boletín 2 de su historial delictivo;
- Una identificación válida
- prueba de sus cualificaciones profesionales
- una hoja de resumen con el modelo adjunto al Apéndice 1 del Apéndice VII. Cuando el solicitante sea nacional de la UE o del EEE, deberá presentar un documento equivalente de menos de tres meses de edad y escrito en francés o, en su defecto, una traducción aprobada;
- si, el dictamen de la red de control aprobada de la que depende, o, si no está unido a una red, la opinión del organismo técnico central siguiendo el modelo establecido en el Apéndice 2 de la Lista VII del decreto de 18 de junio de 1991;
- Si el solicitante es un empleado, una copia de su contrato de trabajo o una carta de compromiso de su empleador;
- una declaración sobre el honor:- certificando la exactitud de la información proporcionada,
  - certificando que no está sujeto a ninguna suspensión o retiro de la acreditación,
  - certificando que se compromete a no realizar ninguna otra actividad en la reparación o el comercio de automóviles y a no utilizar la información obtenida durante su actividad para fines distintos de los previstos por la reglamentación.

**Procedimiento y plazos**

La autorización de acreditación se notifica al solicitante y al centro de control al que está adscrita.

**Tenga en cuenta que**

El profesional puede solicitar ambas aprobaciones, una para vehículos ligeros y otra para vehículos pesados.

*Para ir más allá*: Artículo R. 323-18 de la Ley de Tráfico de Carreteras; Artículo 13 y Apéndice VII del Auto de 18 de junio de 1991 mencionado anteriormente; Apéndice VII del Auto de 27 de julio de 2004 supra.

### b. Predeclaración del nacional de la UE para el ejercicio temporal y casual (LPS)

**Autoridad competente**

El nacional debe enviar su solicitud por escrito al prefecto del departamento en el que tiene previsto llevar a cabo sus servicios.

**Documentos de apoyo**

Su solicitud debe incluir los siguientes documentos, con su traducción al francés:

- prueba de nacionalidad
- un certificado que certifique que está legalmente establecido en un Estado miembro de la UE o del EEE para llevar a cabo la actividad de controlador técnico del vehículo y que no está prohibido ejercer;
- prueba de sus cualificaciones profesionales
- cuando ni el acceso a la actividad ni su ejercicio estén regulados en ese Estado miembro, prueba de que ha participado en esta actividad durante al menos un año en los últimos diez años;
- una copia de su contrato de trabajo o, en su defecto, una carta de compromiso del centro de control que lo emplea.

**hora**

En el plazo de un mes, el prefecto envía un recibo de una declaración al nacional.

*Para ir más allá*: Artículo R. 323-18-1 de la Ley de Tráfico de Carreteras; Artículo 26-6 de la orden del 18 de junio de 1991.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

