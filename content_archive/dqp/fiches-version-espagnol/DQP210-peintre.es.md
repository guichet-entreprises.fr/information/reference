﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP210" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Artesanía" -->
<!-- var(title)="Pintor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="artesania" -->
<!-- var(title-short)="pintor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/artesania/pintor.html" -->
<!-- var(last-update)="2020-04-15 17:20:34" -->
<!-- var(url-name)="pintor" -->
<!-- var(translation)="Auto" -->


Pintor
======

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:34<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El pintor es un profesional que interviene en obras o obras de renovación. Debe preparar las superficies que se van a pintar y elegir los materiales y herramientas utilizados para aplicar la pintura final. Este es un trabajo de decoración, pero también tiene experiencia técnica para elegir los materiales adecuados contra los factores de degradación (aire, humedad).

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El interesado que desee trabajar como pintor debe tener una cualificación profesional o un ejercicio bajo el control efectivo y permanente de una persona con esta cualificación.

De conformidad con el Decreto 98-246, de 2 de abril de 1998, una persona es considerada una persona cualificada profesionalmente si posee uno de los siguientes títulos o títulos:

- Certificado de Cualificación Profesional (CAP)
- Una Patente de Estudios Profesionales (BEP);
- un diploma o un título, igual o superior, aprobado o registrado en el momento de su emisión en el [directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) y se emitió para la práctica de la pintura.

En el caso de la actividad de un pintor, por ejemplo, la persona debe ser el titular:

- CAP "pintor aplicador de recubrimiento";
- Patente profesional "recubrimientos de pintura";
- de la disposición pro bin y acabado del edificio dominante "pintura, vidrio, revestimiento" o dominante "pintura de yeso".

A falta de uno de estos títulos o títulos, el interesado debe justificar una experiencia profesional efectiva de tres años en el territorio de la Unión Europea (UE)*Espacio Económico Europeo (EEE)*, adquirida como gerente de empresa, autónomo o asalariado en el ejercicio de la profesión de pintor. En este caso, se recomienda al interesado que se ponga en contacto con la Cámara de Comercio y Artesanía (CMA) para solicitar un certificado de cualificación profesional.

*Para ir más allá*: Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía, Decreto No 98-246, de 2 de abril de 1998, relativo a la cualificación profesional necesaria para las actividades del artículo 16 de la Ley no. 5 de julio de 1996.

#### Entrenamiento

La PAC es un diploma de Nivel V (es decir, correspondiente a un graduado general y tecnológico antes del último año). Los certificados profesionales y profesionales son diplomas de nivel IV (es decir, licenciatura general, tecnológica o profesional).

El CAP "pintor aplicador de revestimiento" es accesible después de una tercera clase. La selección se realiza generalmente en el archivo y en las pruebas. Este diploma es accesible después de un curso de formación en condición de estudiante, contrato de aprendizaje, después de un curso de educación continua, contrato de profesionalización, solicitud individual o validación de los logros de (VAE). Para obtener más información, puede ver[sitio web oficial](http://www.vae.gouv.fr/) Vae. La formación normalmente dura dos años y tiene lugar en una escuela secundaria vocacional.

El certificado profesional de "recubrimientos de pintura" está disponible en un contrato de aprendizaje, después de un curso de formación continua, en un contrato de profesionalización, por aplicación individual o por validación de experiencia (VAE). Para obtener más información, puede ver[sitio web oficial](http://www.vae.gouv.fr/) Vae. La formación, que lleva a la obtención de este certificado profesional, normalmente dura dos años.

El bac pro "desarrollo y acabado del edificio" es accesible después de un curso de formación bajo la condición de estudiante o estudiante, contrato de aprendizaje, después de un curso de educación continua, contrato de profesionalización, por aplicación validación de la experiencia (VAE). Para obtener más información, puede ver[sitio web oficial](http://www.vae.gouv.fr/) Vae.

*Para ir más allá*: Artículos D. 337-1 y los siguientes del Código de Educación.

#### Costos asociados con la calificación

El entrenamiento es, más a menudo que no, libre. Para más detalles, es aconsejable consultar el centro de formación en cuestión.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

El profesional nacional de la UE o del EEE podrá ejercer un control efectivo y permanente de la actividad del pintor en Francia de forma temporal y ocasional, siempre que esté legalmente establecido en uno de estos Estados para llevar a cabo la misma actividad.

Si ni la actividad ni la formación que la conducen están reguladas en el Estado del Establecimiento, el interesado también deberá demostrar que ha sido pintor en ese Estado durante al menos el equivalente a dos años a tiempo completo, en los últimos diez años. años antes de la actuación que quiere actuar en Francia.

*Para ir más allá*: Artículo 17-1 de la Ley 96-603, de 5 de julio de 1996.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Para llevar a cabo el control efectivo y permanente de la actividad del pintor en Francia de forma permanente, el profesional nacional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tener las mismas cualificaciones profesionales que un nacional francés (véase más arriba: "2.a. Cualificaciones profesionales");
- poseer un certificado de competencia o certificado de formación requerido para la práctica de la pintura en un Estado de la UE o del EEE al regular el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su preparación para el ejercicio de la actividad de pintor, cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de este Actividad
- obtener un diploma, título o certificado adquirido en un tercer estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona ha sido pintora durante tres años en el estado que ha admitido equivalencia .

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones anteriores podrá solicitar un certificado de cualificación profesional para ejercer un control efectivo y permanente sobre la actividad de pintor. Para obtener más información, es aconsejable consultar el párrafo "5o" a continuación. b. Si es necesario, solicite un certificado de reconocimiento de la cualificación profesional."

Si la persona no cumple con cualquiera de las condiciones anteriores, la CMA puede pedirle que realice una medida de compensación en los siguientes casos:

- si la duración de la formación certificada es al menos un año inferior a la requerida para obtener una de las cualificaciones profesionales requeridas en Francia para llevar a cabo la actividad de pintor;
- Si la formación recibida abarca temas sustancialmente diferentes de los cubiertos por uno de los títulos o diplomas requeridos para ejercer en Francia como pintor;
- Si el control efectivo y permanente de la actividad del pintor requiere, para el ejercicio de algunas de sus competencias, una formación específica que no esté prevista en el Estado miembro de origen y que se refiera a temas sustancialmente diferentes de los cubiertos por el certificado de competencia o designación de formación mencionado por el solicitante.

*Para ir más allá*: Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996, artículos 3 a 3-2 del Decreto No 98-246, de 2 de abril de 1998.

**Bueno saber: medidas de compensación**

La CMA, que solicita un certificado de cualificación profesional, notifica al solicitante su decisión de que realice una de las medidas de compensación. Esta Decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son imprescindibles para ejercer en Francia.

A continuación, el solicitante debe elegir entre un curso de ajuste de hasta tres años y una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final del curso de ajuste, el solicitante envía al CMA un certificado que certifica que ha completado válidamente esta pasantía, acompañado de una evaluación de la organización que lo supervisó. La CMA emite, sobre la base de este certificado, un certificado de cualificación profesional en el plazo de un mes.

La decisión de utilizar una medida de indemnización podrá ser impugnada por el interesado, que deberá presentar un recurso administrativo ante el prefecto en el plazo de dos meses a partir de la notificación de la decisión. Si su apelación es desestimada, puede iniciar una impugnación legal.

*Para ir más allá*: Artículos 3 y 3-2 del Decreto No 98-246, de 2 de abril de 1998, artículo 6-1 del Decreto No 83-517, de 24 de junio de 1983, por el que se establecen las condiciones de aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Nadie puede ejercer como pintor si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá*: Artículo 19 III de la Ley 96-603, de 5 de julio de 1996.

4°. Seguro
-------------------------------

El profesional no está obligado a tomar un seguro de responsabilidad civil profesional. Sin embargo, es aconsejable contratar este seguro ya que permite estar cubierto por los daños causados a otros, ya sea que el profesional sea directamente responsable o el trabajo de sus empleados, locales o equipos.

Además, la ley exige que los artesanos involucrados en trabajos a gran escala y de construcción adquieran una asistencia de responsabilidad civil de 10 años. Esta garantía es aplicable cuando el trabajo realizado puede perjudicar la solidez de una obra o hacerla inadecuada para su destino.

Sin embargo, el [jurisprudencia nacional](https://www.legifrance.gouv.fr/affichJuriJudi.do?oldAction=rechJuriJudi&idTexte=JURITEXT000007045775&fastPos=1) considera que la persona que actúa como pintor no está sujeta a la obligación de sacar un seguro de 10 años. Según el análisis del Tribunal de Casación, las obras de pintura no son comparables a una obra, un elemento de equipamiento o un elemento de construcción en el sentido del artículo 1792-3 del Código Civil.

5°. Proceso de cualificaciones y formalidades
-------------------------------------------------------

### a. Si es necesario, solicite un certificado de reconocimiento de la cualificación profesional

El interesado que desee que se reconozca su diploma, distinto del exigido en Francia, o su experiencia profesional podrá solicitar un certificado de cualificación profesional.

#### Autoridad competente

La solicitud debe dirigirse a la ACM territorialmente competente.

#### Procedimiento

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA le pide al solicitante que lo complete dentro de los 15 días posteriores a la presentación del expediente. Se emite un recibo tan pronto como se completa el archivo.

#### Documentos de apoyo

La solicitud de certificado de cualificación profesional debe incluir:

- Solicitar un certificado de cualificación profesional
- Prueba de cualificación profesional: certificado de competencia o diploma o certificado de formación profesional;
- Prueba de la nacionalidad del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (CIEP) para obtener información adicional sobre el nivel de formación de un diploma, certificado o designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

Todos los documentos justificativos deben traducirse al francés, si es necesario.

#### Tiempo de respuesta

Dentro de los tres meses siguientes a la recepción, la CMA podrá, a su elección:

- Reconocer la cualificación profesional y emitir la certificación de cualificación profesional;
- decidir someter al solicitante a una medida de compensación y notificarle dicha decisión;
- negarse a expedir el certificado de cualificación profesional.

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

#### Remedios

Si la CMA se niega a emitir el reconocimiento de la cualificación profesional, el solicitante puede iniciar una acción judicial ante el tribunal administrativo pertinente en el plazo de dos meses a partir de la notificación de la denegación de la CMA. Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de compensación, primero debe iniciar una apelación agraciada ante el prefecto del departamento en el que se encuentra la CMA, en el plazo de dos meses a partir de la notificación de la decisión de la CMA. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo competente.

*Para ir más allá*: Artículos 3 a 3-2 del Decreto No 98-246, de 2 de abril de 1998, auto de 28 de octubre de 2009 adoptado en los decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un un Estado miembro de la Comunidad Europea u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### b. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades únicas (Entrega gratuita de servicios)

Los nacionales de la UE o del EEE que deseen ejercer en Francia de forma ocasional y ad hoc están sujetos a una obligación de preinformación en su primer servicio en suelo francés.

Esta declaración previa de actividad debe renovarse cada año si el interesado tiene la intención de volver a ejercer en Francia.

#### Autoridad competente

La declaración previa de actividad se dirige a la CMA en la jurisdicción desde la que el solicitante de registro tiene previsto llevar a cabo su desempeño.

#### Recibo

La CMA emite un recibo al solicitante de registro indicando la fecha en la que recibió el informe previo completo del expediente de actividad. Si el archivo está incompleto, el CMA notifica la lista de documentos que faltan al solicitante de registro en un plazo de 15 días. Emite el recibo tan pronto como se completa el archivo.

#### Documentos de apoyo

La notificación previa de las actividades debe incluir:

- La declaración fechada y firmada, en la que se menciona la información sobre el seguro civil profesional obligatorio;
- Prueba de la nacionalidad del solicitante de registro;
- prueba de las cualificaciones profesionales del solicitante de registro: título de formación, certificado de competencia expedido por la autoridad competente del Estado de establecimiento, cualquier documento que acredite una experiencia profesional que indique su naturaleza y duración , etc.;
- si procede, pruebas de que el solicitante de registro ha estado practicando la actividad de un pintor durante al menos dos años a tiempo completo durante los últimos diez años;
- un certificado de establecimiento en un Estado de la UE o del EEE para llevar a cabo la actividad de un pintor;
- un certificado de no condena a la prohibición, incluso temporal, de la práctica.

Todos los documentos deben ser traducidos al francés (por un traductor certificado) si no están redactados en francés.

#### hora

En el plazo de un mes a partir de la recepción del informe completo del expediente de actividad, la CMA emite un certificado de cualificación profesional al declarante o aconseja al declarante la necesidad de un examen posterior. En este último caso, la CMA notifica su decisión final en el plazo de dos meses a partir de la recepción del informe previo completo del expediente de actividad. En ausencia de notificación dentro de este plazo, se considera que la predeclaración ha sido adquirida y, por lo tanto, la prestación del servicio puede comenzar.

Al tomar su decisión, la CMA podrá ponerse en contacto con la autoridad competente del Estado de Liquidación del solicitante de registro para cualquier información relativa a la legalidad de la institución y su ausencia de sanción disciplinaria o penal de naturaleza. Profesional.

**Es bueno saber**

Si la CMA encuentra una diferencia sustancial entre la cualificación profesional requerida para ejercer en Francia y la declarada por el reclamante y esta diferencia puede afectar negativamente a la salud o la seguridad del beneficiario del servicio, el solicitante de registro debe someterse a una prueba de aptitud. Si se niega a hacerlo, la prestación de servicios no podrá llevarse a cabo. La prueba de aptitud debe realizarse dentro de los tres meses siguientes a la presentación del expediente completo de actividad previa al informe. Si no se cumple este plazo, se considera que el reconocimiento de la cualificación profesional ha sido adquirido y la prestación de servicios puede comenzar. Para más información sobre la prueba de aptitud, es aconsejable referirse al párrafo "Bueno saber: medidas de compensación".

#### Uso

Cualquier decisión de la CMA de someter al solicitante de registro a una prueba de aptitud puede ser objeto de un recurso administrativo ante el prefecto del departamento en el que se encuentra la CMA, dentro de los tres meses siguientes a la notificación de la decisión de la CMA. Si la apelación no tiene éxito, el solicitante de registro puede iniciar un litigio.

#### Costo

La pre-informe es gratuita. Sin embargo, si el solicitante de registro participa en una prueba de aptitud, se le puede cobrar una cuota de organización.

*Para ir más allá*: Artículo 17-1 de la Ley No 96-603, de 5 de julio de 1996, artículos 3 y siguientes del Decreto No 98-246, de 2 de abril de 1998, de los artículos 1 y 6 de la Orden de 28 de octubre de 2009 supra.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- y que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información** : SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París,[sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

