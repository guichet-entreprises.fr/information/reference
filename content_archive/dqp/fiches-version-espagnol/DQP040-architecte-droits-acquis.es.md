﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP040" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Construcción" -->
<!-- var(title)="Arquitecto (derechos adquiridos)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="construccion" -->
<!-- var(title-short)="arquitecto-derechos-adquiridos" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/construccion/arquitecto-derechos-adquiridos.html" -->
<!-- var(last-update)="2020-04-15 17:20:44" -->
<!-- var(url-name)="arquitecto-derechos-adquiridos" -->
<!-- var(translation)="Auto" -->


Arquitecto (derechos adquiridos)
================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:44<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El arquitecto es un profesional encargado de las distintas fases del diseño y construcción de una obra.

En primer lugar, se requiere realizar un estudio de viabilidad del terreno antes de trazar los primeros planos del futuro edificio. Obtendrá el permiso de construcción y negociará precios con los diversos contratistas que trabajarán en el sitio. A lo largo del proyecto, tendrá que tener en cuenta las normativas de planificación, las limitaciones legales y técnicas, así como el presupuesto del cliente y los requisitos de plazos.

Una vez diseñados los planos, el arquitecto coordina los equipos responsables de la construcción de la obra hasta la entrega de la obra.

2°. Cualificaciones profesionales
-----------------------------------------

#### Derechos adquiridos

El ejercicio de la profesión de arquitecto en Francia está reservado a los inscritos en la tabla regional de arquitectos.

No obstante, los profesionales con un título de diploma o formación expedido por otro Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE) podrán ejercer como arquitecto según francia De nacionalidad francesa.

#### Diplomas o documentos de formación expedidos por otro Estado de la UE o del EEE

Los títulos o documentos de formación expedidos por otro Estado miembro, enumerados en el Apéndice VI del Parlamento Europeo y del Consejo, de 7 de septiembre de 2005 sobre el reconocimiento de cualificaciones profesionales, están reconocidos en Francia y tienen los mismos efectos que los títulos nacionales de arquitectura.

*Para ir más allá*: Artículo 49 de la[Directiva 2005/36/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32005L0036&from=FR) Parlamento Europeo y del Consejo, de 7 de septiembre de 2005, sobre el reconocimiento de las cualificaciones profesionales.

Para saber cómo ejercer la profesión arquitectónica en Francia, es aconsejable consultar la hoja "Arquitecto".

