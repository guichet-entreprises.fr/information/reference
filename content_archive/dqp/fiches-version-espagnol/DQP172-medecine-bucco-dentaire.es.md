﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP172" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Medicina oral" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="medicina-oral" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/medicina-oral.html" -->
<!-- var(last-update)="2020-04-15 17:21:41" -->
<!-- var(url-name)="medicina-oral" -->
<!-- var(translation)="Auto" -->


Medicina oral
=============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:41<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La medicina oral es una disciplina médica derivada de la odontología que estudia y trata todas las enfermedades de la boca.

El médico oral es un especialista multidisciplinarque que cuida a sus pacientes con patologías graves y/o específicas.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

De conformidad con la sección L. 4111-1 del Código de Salud Pública, para realizar la actividad de un médico especializado en medicina oral, el profesional debe cumplir acumulativamente las siguientes condiciones:

- tienen los siguientes grados:- un diploma estatal francés como médico de cirugía dental, o un diploma, certificado u otro título mencionado en el artículo L. 4141-3 del Código de Salud Pública (véase a continuación "Bueno saber: Reconocimiento Automático del Diploma"),
  - un diploma en estudios especializados (DES) en medicina oral;
- nacionalidad francesa, ciudadanía andorrana o nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE) o Marruecos, con sujeción a la aplicación de las normas derivadas del Código de Salud compromisos públicos o internacionales;
- con excepciones, ser colocado en la junta del Colegio de Cirujanos Dentales (ver infra "5 grados). b. Solicitar inclusión en la lista de los cirujanos dentales);
- ser colocado en la lista de especialistas calificados en el Colegio de Cirujanos Dentales (ver "5 b. Solicitud de inclusión en la lista de especialistas calificados").

*Para ir más allá*: Artículos L. 4111-1 y L. 4141-3 del Código de Salud Pública.

**Bueno saber: reconocimiento automático del diploma**

De conformidad con el artículo L. 4141-3 del Código de Salud Pública, los nacionales de la UE o del EEE podrán ejercer como cirujano sortólogo si poseen uno de los siguientes títulos:

- Certificados de formación de odontólogos expedidos por uno de estos Estados de conformidad con las obligaciones comunitarias y enumerados por la Orden de 13 de julio de 2009 por la que se establecela la lista y las condiciones para el reconocimiento de formación como odontólogos especialistas expedidos por los Estados miembros de la Comunidad Europea o partes en el acuerdo del Espacio Económico Europeo contemplado en el artículo L. 4141-3 del Código de Salud Pública;
- certificados de formación de odontólogos expedidos por un Estado de la UE o del EEE de conformidad con las obligaciones de la UE, no en la lista del decreto de 13 de julio de 2009, si van acompañados de un certificado de ese Estado que certifica que sancionan la formación de conformidad con estas obligaciones y que sean asimiladas, por él, a los diplomas, certificados y títulos de esa lista;
- certificados de formación de odontólogos expedidos por un Estado de la UE o del EEE sancionando la formación como odontólogo iniciado en ese estado antes de las fechas en el orden mencionado en el orden de 13 de julio de 2009 y obligaciones comunitarias, si va acompañada de un certificado de uno de estos Estados que certifica que el titular de los títulos de formación se ha dedicado, en ese estado, de manera efectiva y legal a las actividades de un odontólogo o, si es necesario, un odontólogo especialista durante al menos tres años consecutivos en los cinco años anteriores a la expedición del certificado;
- certificados de formación de odontólogos expedidos por la antigua Unión Soviética o la ex Yugoslavia o que sancionan la formación iniciada antes de la fecha de independencia de Estonia, Letonia, Lituania o Eslovenia, si acompañados de un certificado de las autoridades competentes de Estonia, Letonia o Lituania para los documentos de formación expedidos por la antigua Unión Soviética, Eslovenia, para los documentos de formación expedidos por la ex Yugoslavia, certificando que tienen la misma validez legal que los certificados de formación emitidos por ese estado. Este certificado va acompañado de un certificado expedido por las mismas autoridades que indica que el titular ha ejercido en ese estado, de manera efectiva y lícita, la profesión de profesional del arte de la odontología o practicante del especialista en artes dentales durante el al menos tres años consecutivos en los cinco años anteriores a la expedición del certificado;
- Certificados de formación de odontólogos expedidos por un Estado, Miembro o Parte, sancionando la formación como odontólogo iniciado en ese estado antes de las fechas en la orden mencionada en el orden de 13 de julio de 2009 y no de acuerdo con las obligaciones comunitarias, sino permitiendo ejercer legalmente la profesión de profesional de la odontología en el estado que las expidió, si el profesional del arte de la odontología justifica haber realizado en Francia durante los cinco años tres años consecutivos a tiempo completo de funciones hospitalarias, si las hubiera en la especialidad correspondiente a los títulos de formación, como agregado asociado, profesional asociado, asistente asociado o funciones académicos como jefe asociado de clínicas de universidades o asistente asociado de universidades, siempre que hayan estado a cargo de las funciones hospitalarias al mismo tiempo;
- un certificado de formación médica expedido en Italia, España, Austria, la República Checa, Eslovaquia y Rumanía sancionando la formación que ha comenzado a más tardar en las fechas fijadas por decreto de los Ministros responsables de la educación superior y salud, si va acompañada de un certificado de las autoridades competentes de ese Estado que certifica que tiene derecho en ese Estado al ejercicio de la profesión de profesional del arte de la odontología y que su titular se ha dedicado, en ese estado, de manera efectiva y legal , las actividades de un odontólogo durante al menos tres años consecutivos en los cinco años anteriores a la expedición del certificado;
- Certificados de formación de odontólogos expedidos por un Miembro o Estado Parte, sancionando la capacitación que comenzó antes del 18 de enero de 2016;
- Certificados de formación médica expedidos en España que sancionan la formación médica iniciada en ese Estado entre el 1 de enero de 1986 y el 31 de diciembre de 1997, si van acompañados de un certificado expedido por las autoridades competentes de ese Estado indicando que el titular ha completado con éxito al menos tres años de estudio de acuerdo con las obligaciones comunitarias de formación básica de la profesión de odontólogo, que ha ejercido, de manera efectiva, legal y principal, la odontodental durante al menos tres años consecutivos en los cinco años anteriores a la expedición del certificado y que está autorizado a ejercer o ejercer, de manera efectiva, lícita y principalmente, esta profesión en las mismas condiciones que los titulares de títulos de formación en la lista mencionada en el orden de 13 de julio de 2009.

#### Entrenamiento

Los estudios de odontología, con una duración total de seis años, constan de tres ciclos:

- el primer ciclo, con una duración de seis semestres, da como resultado la emisión de dos diplomas:- el primer año de estudios conjuntos de salud[Pasos](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021276755&dateTexte=20180119) al final de los dos primeros semestres,
  - El Diploma General de Formación en Odontología (DFGO) a nivel de Licenciatura (B.A. 3);
- el segundo ciclo, que dura dos años, da lugar a la emisión del máster en formación en profundidad en ciencias odontológicas (DFASO);
- el tercer ciclo de un período de tres años, permite al futuro médico profesional adquirir, además de la ED en cirugía dental, un diploma de estudios especializados (DES) en medicina oral.

**Diploma de educación general en ciencias odontológicas**

La formación de pregrado consta de seis semestres y tiene como objetivo:

- la adquisición de una base de conocimiento científico esencial para el posterior dominio de los conocimientos y conocimientos necesarios para el ejercicio de la profesión de cirujano dental. Esta base científica abarca la biología, ciertos aspectos de las ciencias exactas y varias disciplinas de las humanidades y las ciencias sociales;
- la adquisición de conocimientos en los campos de la semiología médica, la farmacología y las disciplinas odontológicas;
- aprendizaje en equipo y técnicas de comunicación, necesarias para la práctica profesional.

También permite a los estudiantes aprender a comunicarse, diagnosticar, diseñar una propuesta terapéutica, entender un enfoque coordinado de la atención y asegurar las acciones de emergencia.

La formación incluye enseñanzas teóricas, metodológicas, aplicadas y prácticas, así como la realización de un curso introductorio a tiempo completo de cuatro semanas.

*Para ir más allá* :[decreto del 22 de marzo de 2011](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023850748&dateTexte=20180119) currículo para el grado general de formación en ciencias odontológicas.

**Grado de formación en profundidad en ciencias odontológicas**

El segundo ciclo consta de cuatro semestres y tiene como objetivo:

- adquirir los conocimientos científicos, médicos y odontológicos que complementen y profundicen los adquiridos en el ciclo anterior y necesarios para adquirir las habilidades para todas las actividades de prevención y diagnóstico y el tratamiento de enfermedades congénitas o adquiridas, reales o asumidas, de la boca, los dientes, los maxilares y los tejidos adyacentes;
- adquirir conocimientos prácticos y habilidades clínicas a través de pasantías y formación práctica y clínica;
- formación en el proceso científico
- Aprendizaje del razonamiento clínico
- aprender a trabajar en equipo multiprofesional, especialmente con otros odontólogos;
- Adquirir técnicas de comunicación esenciales para la práctica profesional;
- desarrollo profesional continuo, incluida la evaluación de las prácticas profesionales y la profundización continua del conocimiento.

Además de las enseñanzas teóricas y prácticas, la formación incluye la realización de prácticas hospitalarias.

El segundo ciclo está validado por el éxito del conocimiento de las enseñanzas impartidas durante la formación, así como la emisión de un certificado de síntesis clínica y terapéutica.

*Para ir más allá*: Artículos 4 a 15 de la[decretado el 8 de abril de 2013](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027343802&dateTexte=20180119) educación para el diploma estatal de doctor en cirugía dental.

**Diploma Estatal en Cirugía Dental**

El tercer ciclo está sancionado por la emisión del diploma estatal de doctor en cirugía dental. Incluye:

- Un breve ciclo de dos semestres de formación;
- un ciclo de seis a ocho semestres de formación para los estudiantes que han recibido la pasantía de odontología;
- la defensa de una tesis.

El tercer ciclo de dos semestres está dedicado al enfoque general del paciente y la preparación para el ejercicio independiente de la profesión.

Se acompaña de un curso profesional de iniciación de vida de 250 horas con un cirujano dental.

El estudiante tendrá que apoyar una tesis ante un jurado desde el segundo semestre del tercer ciclo y hasta su validación. El ED del DOCTOR en cirugía dental se le dará al estudiante que validó las enseñanzas de postgrado y validó su tesis.

*Para ir más allá*: Artículos 16 y siguientes de la orden del 8 de abril de 2013 sobre el régimen educativo del Diploma de Doctor del Estado en Cirugía Dental.

**Diploma de Estudios Especializados (DES) en medicina oral**

El acceso al posgrado (también conocido como pasantía en odontología) es a través de un concurso disponible para los estudiantes que han completado el segundo ciclo.

Este diploma permite al futuro especialista en medicina oral adquirir los conocimientos necesarios para llevar a cabo sus funciones.

Este curso consta de dos partes que duran seis semestres cada una:

- formación teórica que incluye:- formación teórica en medicina para complementar los conocimientos médicos del candidato,
  - formación teórica en medicina oral;
- formación práctica que conduce a pasantías en lugares con licencia. Estas pasantías están diseñadas para permitir al candidato cuidar a los pacientes con patologías específicas de una manera multidisciplinaria.

Para obtener el DES en medicina oral, el candidato debe:

- validar todas las lecciones que se enseñan durante el curso;
- promedio de todas las pruebas escritas y orales;
- apoyar con éxito su breve.

*Para ir más allá*: Artículo L. 634-1 del Código de Educación y[decreto del 31 de marzo de 2011](http://www.enseignementsup-recherche.gouv.fr/pid20536/bulletin-officiel.html?cid_bo=56026) establecer la lista de cursos de calificación y la regulación de diplomas de estudios especializados en odontología.

#### Costos asociados con la calificación

Se paga la formación que conduce al DES en medicina oral. Su costo varía según las universidades que ofrecen los diferentes cursos. Para más información, es aconsejable acercarse a estos establecimientos.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

El profesional que sea miembro de un Estado de la UE o del EEE establecido y que practique legalmente en uno de estos Estados podrá llevar a cabo la misma actividad en Francia de forma temporal y ocasional, sin ser incluido en el cirujanos dentales.

Para ello, el profesional debe hacer una declaración previa, así como una declaración que justifique que tiene las habilidades de lenguaje necesarias para ejercer en Francia (ver infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

**Qué saber**

La inscripción en el Colegio de Cirujanos Dentales no es necesaria para el profesional de servicio gratuito (LPS). Por lo tanto, no está obligado a pagar cuotas ordinales. El cirujano dental está simplemente registrado en una lista específica mantenida por el Consejo Nacional de la Orden.

La predeclaración debe ir acompañada de una declaración sobre las habilidades del idioma necesarias para llevar a cabo el servicio. En este caso, el control del dominio del idioma debe ser proporcional a la actividad que debe llevarse a cabo y llevarse a cabo una vez reconocida la cualificación profesional.

Cuando los títulos de formación no reciben reconocimiento automático (véase supra "2.0). a. Legislación nacional"), las cualificaciones profesionales del proveedor se comprueban antes de que se preste el primer servicio. En caso de diferencias sustanciales entre las cualificaciones del interesado y la formación requerida en Francia, que podría perjudicar a la salud pública, el solicitante se somete a una prueba de aptitud.

El cirujano dental en la situación de LPS está obligado a respetar las reglas profesionales aplicables en Francia, incluyendo todas las reglas éticas (ver infra "3." Condiciones de honorabilidad, reglas éticas, ética"). Está sujeto a la jurisdicción disciplinaria del Colegio de Cirujanos Dentales.

**Tenga en cuenta que**

El rendimiento se realiza bajo el título profesional francés de cirujano dental. Sin embargo, cuando no se reconocen las cualificaciones de formación y no se han verificado las cualificaciones, el rendimiento se lleva a cabo bajo el título profesional del Estado de establecimiento, con el fin de evitar confusiones Con el título profesional francés.

*Para ir más allá*: Artículo L. 4112-7 del Código de Salud Pública.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

**El sistema automático de reconocimiento de diplomas**

El artículo L. 4141-3 del Código de Salud Pública establece un régimen de reconocimiento automático en Francia de determinados títulos o títulos, en su caso, acompañados de certificados, obtenidos en un Estado de la UE o del EEE (véase más arriba "2". a. Legislación Nacional").

Es responsabilidad del Consejo Nacional del Colegio de Cirujanos Dentales verificar la regularidad de diplomas, títulos, certificados y certificados, otorgar el reconocimiento automático y luego pronunciarse sobre la solicitud de inclusión en la lista de la Orden.

*Para ir más allá*: Artículo L. 4151-5 del Código de Salud Pública.

**El régimen de autorización individual para ejercer**

Si el nacional de la UE o del EEE no reúne los requisitos para el reconocimiento automático de sus credenciales, está comprendido en un régimen de autorización (véase más adelante "5o). b. Si es necesario, solicitar una autorización individual para ejercer").

Las personas que no reciben reconocimiento automático pero que poseen un título de formación para ejercer legalmente como cirujano dental pueden estar autorizadas individualmente a ejercer en Francia, por el Ministro de Salud, previa asesoría de una comisión formada por profesionales.

Si el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia profesional muestra diferencias sustanciales con las cualificaciones necesarias para el acceso a la profesión y su ejercicio en Francia, la persona debe someterse a una medida de compensación.

En función del nivel de cualificación exigido en Francia y del que posea el interesado, la autoridad competente podrá:

- Ofrecer al solicitante la opción de elegir entre un curso de ajuste o una prueba de aptitud;
- requieren un curso de ajuste y/o una prueba de aptitud.

*Para ir más allá*: Artículos L. 4141-3-1 y R. 4111-14 y siguientes del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Las disposiciones del Código de ética son obligatorias para todos los profesionales especializados en medicina oral que practican en Francia, ya sea que estén inscritos en el consejo de la Orden o estén exentos de esta obligación (véase infra "5o. b. Solicitar inclusión en la lista de los cirujanos dentales").

**Qué saber**

Todas las disposiciones del Código de ética están codificadas en las secciones R. 4127-201 a R. 4127-284 del Código de Salud Pública.

Como tal, los cirujanos dentales deben respetar los principios de dignidad, no discriminación, secreto profesional o independencia.

4°. Seguro
-------------------------------

### a. Obligación de constete de un seguro de responsabilidad civil profesional

Como profesional de la salud, un cirujano dental liberal debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

### b. Obligación de afiliación al fondo de jubilación por cuenta propia para cirujanos dentales y parteras (CARCDSF)

Cualquier cirujano dental registrado en la junta del Colegio de Cirujanos Dentales y practicando en la forma liberal (incluso a tiempo parcial e incluso si también está empleado) tiene la obligación de unirse al CARCDSF.

El individuo debe reportarse a carCDSF en el plazo de un mes a partir del inicio de su actividad liberal.

*Para ir más allá*: Artículo R. 643-1 del Código de la Seguridad Social; el sitio de la[CARCDSF](http://www.carcdsf.fr/).

### c. Obligación de Informes de Seguros Médicos

Una vez en la lista de la Orden, el cirujano dental que practice en forma liberal debe declarar su actividad con el Fondo de Seguro de Salud Primaria (CPAM).

**Términos**

El registro en el CPAM se puede hacer en línea en el sitio web oficial de Medicare.

**Documentos de apoyo**

El solicitante de registro debe proporcionar un archivo completo que incluya:

- Copiar una identificación válida
- El certificado de inscripción en el consejo de la Orden;
- un declaración de identidad bancaria profesional (RIB)
- si es necesario, la notificación de la instalación radiológica.

Para obtener más información, consulte la sección sobre la instalación de cirujanos dentales en el sitio web del seguro médico.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

Cualquier nacional de la UE o del EEE que esté establecido y practique legalmente las actividades de cirujano dental en uno de estos estados puede ejercer en Francia de forma temporal u ocasional si hace la declaración previa (véase supra "2 b. b). Nacionales de la UE y del EEE: para un ejercicio temporal e informal (Servicio de Servicio Gratuito) ").

La declaración anticipada debe renovarse cada año.

**Tenga en cuenta que**

Cualquier cambio en la situación del solicitante debe ser notificado en las mismas condiciones.

**Autoridad competente**

La declaración debe ser dirigida antes de la primera entrega del servicio al Consejo Nacional del Colegio de Cirujanos Dentales.

**Condiciones de presentación de informes y recepción**

La declaración puede enviarse por correo o directamente en línea en el sitio web oficial del Colegio de Cirujanos Dentales.

Cuando el Consejo Nacional de la Orden recibe la declaración y todos los documentos justificativos necesarios, envía al demandante un recibo especificando su número de registro, así como la disciplina ejercitada.

**Tenga en cuenta que**

El prestador de servicios informa a la agencia nacional de seguros de salud pertinente de su prestación de servicios mediante el envío de una copia del recibo o por cualquier otro medio.

**hora**

En el plazo de un mes a partir de la recepción de la declaración, el Consejo Nacional de la Orden informa al solicitante:

- Si puede o no comenzar a prestar servicios;
- cuando la verificación de las cualificaciones profesionales muestra una diferencia sustancial con la formación requerida en Francia, debe demostrar haber adquirido los conocimientos y habilidades que faltan Aptitud. Si cumple con este cheque, se le informa en el plazo de un mes que puede comenzar la prestación de servicios;
- cuando la revisión del archivo resalta una dificultad que requiere más información, las razones del retraso en la revisión del archivo. Luego tiene un mes para obtener la información adicional solicitada. En este caso, antes de que finalice el segundo mes a partir de la recepción de esta información, el Consejo Nacional informa al demandante, tras revisar su expediente:- si puede o no comenzar la prestación de servicios,
  - cuando la verificación de las cualificaciones profesionales del demandante muestre una diferencia sustancial con la formación requerida en Francia, debe demostrar que ha adquirido los conocimientos y habilidades que faltan, sujeto a una prueba de aptitud.

En este último caso, si cumple con este control, se le informa en el plazo de un mes que puede iniciar la prestación de servicios. De lo contrario, se le informa de que no puede comenzar la prestación de servicios. En ausencia de una respuesta del Consejo Nacional de la Orden dentro de estos plazos, la prestación de servicios puede comenzar.

**Documentos de apoyo**

La predeclaración deberá ir acompañada de una declaración sobre las aptitudes linguísticas necesarias para llevar a cabo el servicio y los siguientes documentos justificativos:

- El formulario de entrega anticipada de servicios
- Copia de una identificación válida o un documento que acredite la nacionalidad del solicitante;
- Copia del documento o títulos de formación, acompañados, si es necesario, de una traducción de un traductor certificado;
- un certificado de la autoridad competente del Estado de Conciliación de la UE o del EEE que certifique que la persona está legalmente establecida en ese Estado y que no está prohibido ejercer, acompañado, en su caso, de una traducción francesa establecido por un traductor certificado.

**Tenga en cuenta que**

El control del dominio del idioma debe ser proporcional a la actividad que debe llevarse a cabo y llevarse a cabo una vez reconocida la cualificación profesional.

**Costo**

Gratis.

### b. Formalidades para los nacionales de la UE o del EEE para un ejercicio permanente (LE)

#### Si es necesario, solicite autorización individual para ejercer

Si el nacional no está en virtud del régimen de reconocimiento automático, debe solicitar una licencia para ejercer.

**Autoridad competente**

La solicitud se dirige en dos copias, por carta recomendada con solicitud de notificación de recepción a la unidad responsable de las comisiones de autorización de ejercicio (CAE) del Centro Nacional de Gestión (NMC).

**Documentos de apoyo**

El archivo de solicitud debe contener todos los siguientes documentos justificativos:

- el[formulario de solicitud de autorización para ejercer la profesión](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=BD37E468C18494C178C5EB7899AB7B76.tplgfr36s_1?idArticle=LEGIARTI000029734948&cidTexte=LEGITEXT000029734898&dateTexte=20180129) ;
- Una fotocopia de un documento de identidad válido
- Una copia del título de formación que permita el ejercicio de la profesión en el estado de obtención, así como, en su caso, una copia del título de formación especializada;
- Si es necesario, una copia de los diplomas adicionales;
- cualquier prueba útil que justifique la formación continua, la experiencia y las habilidades adquiridas durante el ejercicio profesional en un Estado de la UE o del EEE, o en un tercer estado (certificados de funciones, informe de actividad, evaluación operativa, etc. ) ;
- en el contexto de funciones desempeñadas en un Estado distinto de Francia, una declaración de la autoridad competente de dicho Estado, de menos de un año de edad, que acredite la ausencia de sanciones contra el solicitante.

Dependiendo de la situación del solicitante, se requiere documentación adicional de apoyo. Para obtener más información, visite el sitio web oficial del NMC.

**Qué saber**

Los documentos de apoyo deben estar escritos en francés o traducidos por un traductor certificado.

**hora**

El NMC confirma la recepción de la solicitud en el plazo de un mes a partir de la recepción.

El silencio guardado durante un cierto período de tiempo a partir de la recepción del expediente completo merece la decisión de desestimar la solicitud. Este retraso se incrementa a:

- cuatro meses para las solicitudes de nacionales de la UE o del EEE con un título de uno de estos Estados;
- seis meses para las solicitudes de terceros nacionales con un diploma de un Estado de la UE o del EEE;
- un año para otras aplicaciones.

Este plazo podrá prorrogarse por dos meses, mediante decisión de la autoridad ministerial notificada a más tardar un mes antes de la expiración de esta última, en caso de dificultad grave para evaluar la experiencia profesional del candidato.

*Para ir más allá* : decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización competentes para el examen de las solicitudes presentadas para el ejercicio en Francia de las profesiones de médico, cirujano dental, partera y Farmacéutico.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la formación y la experiencia laboral del nacional y las necesarias para ejercer en Francia, el NMC podrá decidir:

- Sugerir que el solicitante elija entre un curso de ajuste o una prueba de aptitud;
- para imponer un curso de ajuste y/o una prueba de aptitud.

El propósito de la prueba de aptitud es verificar, a través de pruebas escritas u orales o ejercicios prácticos, la aptitud del solicitante para ejercer como cirujano dental. Se ocupa de temas que no están cubiertos por las credenciales de formación o formación del solicitante o la experiencia profesional.

El objetivo del curso de adaptación es permitir a los interesados adquirir las habilidades necesarias para ejercer la profesión de cirujano dental. Se realiza bajo la responsabilidad de un cirujano dental y puede ir acompañado de formación teórica adicional opcional. La duración de la pasantía no exceda de tres años. Se puede hacer a tiempo parcial.

*Para ir más allá*: Artículos R. 4111-14 y R. 4111-17 a R. 4111-20 del Código de Salud Pública.

**Solicitar registro en el orden de la lista de cirujanos dentales**

El registro en el consejo de la Orden es obligatorio para ejercer legalmente la actividad de cirujano dental en Francia.

El registro no se aplica:

- Nacionales de la UE o del EEE establecidos y que sean legalmente cirujanos dentales en un Estado miembro o parte, cuando realicen actos de su profesión de forma temporal y ocasional en Francia (véase supra "2o. b. Nacionales de la UE y del EEE: para el ejercicio temporal y ocasional);
- cirujanos dentales pertenecientes a los ejecutivos activos del Servicio Militar de Salud;
- los cirujanos dentales que, que tienen la condición de un servidor público o un agente de la comunidad local, no están llamados, en el ejercicio de sus deberes, a realizar una cirugía dental.

**Tenga en cuenta que**

El registro en el consejo de la Orden permite la emisión automática y gratuita de la Tarjeta Profesional de La Salud (CPS). El CPS es un documento electrónico de identidad comercial. Está protegido por un código confidencial y contiene, entre otras cosas, la identificación de un cirujano dental (identidad, profesión, especialidad). Para obtener más información, se recomienda consultar el sitio web del gobierno de la Agencia Francesa de Salud Digital.

**Autoridad competente**

La solicitud de inscripción está dirigida al Presidente de la Junta de la Orden de Cirujanos Dentales del Departamento en la que la persona deseaba establecer su residencia profesional.

La solicitud puede ser presentada directamente al consejo departamental de la Orden en cuestión o enviada a él por correo certificado con solicitud de notificación de recepción.

**Qué saber**

En el caso de un traslado de su residencia profesional fuera del departamento, el profesional está obligado a solicitar su expulsión de la orden del departamento donde estaba practicando y su inscripción por orden de su nueva residencia profesional.

**Procedimiento**

Una vez recibida la solicitud, el consejo del condado nombra a un ponente que lleva a cabo la solicitud y hace un informe por escrito. La junta verifica los títulos del candidato y solicita la divulgación del boletín 2 de los antecedentes penales del solicitante. En particular, verifica que el candidato:

- cumple las condiciones necesarias de moralidad e independencia;
- cumple con los requisitos de competencia necesarios;
- no presenta una discapacidad o condición patológica incompatible con el ejercicio de la profesión (véase supra "3." e. Aptitud física").

En caso de serias dudas sobre la competencia profesional del solicitante o la existencia de una discapacidad o condición patológica incompatible con el ejercicio de la profesión, el consejo de condado remite el asunto al consejo regional o interregional Experiencia. Si, en opinión del informe pericial, existe una insuficiencia profesional que hace peligrosa el ejercicio de la profesión, el consejo departamental deniega el registro y especifica las obligaciones de formación del profesional.

No se puede tomar ninguna decisión de rechazar el registro sin que la persona sea invitada con al menos una quincena de antelación por una carta recomendada solicitando que se presente un aviso de recepción para explicarlo ante la Junta.

La decisión del Consejo Universitario se notifica, en el plazo de una semana, al interesado al Consejo Nacional de la Orden de Cirujanos Dentales y al Director General de la Agencia Regional de Salud (ARS). La notificación es por carta recomendada con solicitud de notificación de recepción.

La notificación menciona los recursos contra la decisión. La decisión de rechazar debe estar justificada.

**hora**

El Presidente reconoce haber recibido el expediente completo en el plazo de un mes a partir de su registro.

El consejo departamental del Colegio debe decidir sobre la solicitud de inscripción en un plazo de tres meses a partir de la recepción del expediente completo de solicitud. Si no se responde a una respuesta dentro de este plazo, la solicitud de registro se considera rechazada.

Este período se incrementa a seis meses para los nacionales de terceros países cuando se llevará a cabo una investigación fuera de la Francia metropolitana. A continuación, se notifica al interesado.

También puede prorrogarse por un período de no más de dos meses por el consejo departamental cuando se haya ordenado un dictamen pericial.

**Documentos de apoyo**

El solicitante debe presentar un expediente de solicitud completo que incluya:

- dos copias del cuestionario estandarizado completado, fechado y firmado, acompañado de una identificación con foto disponible en los consejos departamentales del Colegio;
- Una fotocopia de un documento de identidad válido o, en su caso, un certificado de nacionalidad expedido por una autoridad competente;
- En su caso, una fotocopia de la tarjeta de residencia familiar de un ciudadano de la UE válido, la tarjeta válida de residente-EC de larga duración o la tarjeta de residente con estatus de refugiado válido;
- En caso afirmativo, una fotocopia de un certificado de nacionalidad válido;
- una copia, acompañada si es necesario por una traducción de un traductor certificado, de los cursos de formación a los que se adjuntan:- cuando el solicitante sea nacional de la UE o del EEE, el certificado o certificado sinvisado (véase más arriba "2. a. Requisitos nacionales"),
  - solicitante recibe un permiso de ejercicio individual (véase supra "2. c. Nacionales de la UE y del EEE: para un ejercicio permanente"), una copia de la presente autorización,
  - Cuando el solicitante presente un diploma expedido en un Estado extranjero cuya validez se reconozca en territorio francés, la copia de los títulos a los que pueda subordinarse dicho reconocimiento;
- nacionales de un Estado extranjero, un extracto de antecedentes penales o un documento equivalente de menos de tres meses de edad, expedido por una autoridad competente del Estado de origen. Esta parte puede sustituirse, para los nacionales de la UE o del EEE que requieran prueba de moralidad o honorabilidad para el acceso a la actividad médica, por un certificado, de menos de tres meses de edad, de la autoridad competente del Estado. certificando que se cumplen estas condiciones morales o de honor;
- una declaración sobre el honor del solicitante que certifique que ningún procedimiento que pudiera dar lugar a una condena o sanción que pudiera afectar a la lista en la junta está en su contra;
- un certificado de registro o registro expedido por la autoridad con la que el solicitante fue registrado o registrado previamente o, en su defecto, una declaración de honor del solicitante que certifique que nunca fue registrado o registrado o, en su defecto, un certificado de registro o registro en un Estado de la UE o del EEE;
- todas las pruebas de que el solicitante tiene las habilidades de idiomas necesarias para ejercer la profesión;
- un currículum.

**Remedios**

El solicitante o el Consejo Nacional del Colegio de Cirujanos Dentales puede impugnar la decisión de registrarse o rechazar el registro dentro de los 30 días siguientes a la notificación de la decisión o la decisión implícita de rechazarla. El recurso se interpone ante el consejo regional territorialmente competente.

El consejo regional debe decidir en el plazo de dos meses a partir de la recepción de la solicitud. En ausencia de una decisión dentro de este plazo, el recurso se considera desestimado.

La decisión del consejo regional también está sujeta a apelación, en un plazo de 30 días, ante el Consejo Nacional de la Orden de Cirujanos Dentales. La propia decisión puede ser apelada ante el Consejo de Estado.

**Costo**

La inscripción en el consejo de la Universidad es gratuita, pero crea la obligación de pagar las cuotas ordinales obligatorias, cuyo importe se establece anualmente y que debe pagarse en el primer trimestre del año calendario en curso. El pago se puede hacer en línea en el sitio web oficial del Consejo Nacional de la Orden de Cirujanos Dentales. Como indicación, el importe de esta contribución fue de 422 euros en 2017.

*Para ir más allá*: Artículos L. 4112-1 en L. 4112-6, y R. 4112-1 en R. 4112-20 del Código de Salud Pública.

**Solicitud de inclusión en la lista de especialistas cualificados**

El profesional de medicina oral puede solicitar una lista de especialistas calificados en el Colegio Nacional de Cirujanos Dentales.

**Autoridad competente**

El profesional debe solicitar el registro al consejo departamental:

- La Orden de Inscripción para el profesional en el tablero de pedidos;
- Orden de París para el profesional francés residente en el extranjero.

**Documentos de apoyo**

El profesional debe dirigirse a:

- el[solicitud de calificación de medicina oral](http://www.ordre-chirurgiens-dentistes.fr/uploads/media/formulaire_demandeMBD_avril2016.pdf), en doble copia, completada y firmada;
- documentos de apoyo en el formulario relativos a las cualificaciones y cualificaciones de formación del profesional.

**Resultado del procedimiento**

El consejo departamental del Colegio remite la solicitud a la Junta Nacional de Calificación de Primera Instancia, que da un dictamen motivado. Después de recibir esta opinión, el consejo del condado de la orden hace que una decisión de calificación sea favorable o no.

**Tenga en cuenta que**

El profesional sólo puede ser registrado en una lista de especialistas.

*Para ir más allá*: Artículo R. 4127-283 del Código de Salud Pública, sitio web oficial del Consejo Nacional del Colegio de Cirujanos Dentales ([ONCD](http://www.ordre-chirurgiens-dentistes.fr/)).

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

