﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP156" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Inseminator para especies de ovejas, bovinos y caprinos" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="inseminator-para-especies-de-ovejas" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/inseminator-para-especies-de-ovejas-bovinos-y-caprinos.html" -->
<!-- var(last-update)="2020-04-15 17:20:56" -->
<!-- var(url-name)="inseminator-para-especies-de-ovejas-bovinos-y-caprinos" -->
<!-- var(translation)="Auto" -->


Inseminator para especies de ovejas, bovinos y caprinos
=======================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:56<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El inseminator es un profesional cuya misión principal es llevar a cabo el acto de inseminación artificial en las especies bovina, ovina o caprina.

Debe gestionar y rastrear las dosis del depósito de semillas declarado, que luego transmite datos al sistema nacional de información genética específico de las especies inseminadas.

*Para ir más allá*: Apéndice III de la[decretado a partir del 18 de enero de 2007](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000271038&dateTexte=20171204) en relación con la creación de la comisión encargada de la concesión del Certificado de Aptitud a las Funciones del Técnico de Inseminación en especies de ganado bovino, caprino y ovino.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Cualquier persona que desee convertirse en un inseminante debe estar en posesión de un certificado de aptitud para los deberes de técnico de inseminación (Cafti) adquirido de un[centro de evaluación](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1718413F499D82003CA70C796608A682.tplgfr38s_2?idArticle=LEGIARTI000006610101&cidTexte=LEGITEXT000006055588&dateTexte=20171204) autorizado por el Ministerio de Agricultura.

*Para ir más allá*: Artículo R. 653-87 del Código Rural y Pesca Marina.

#### Entrenamiento

El Cafti se emite en tres términos:

- para las especies bovinas, ovinos y caprinas, siempre y cuando la persona afirme ser el titular, es decir:- el Diploma de Médico Veterinario del Estado o uno de los diplomas mencionados en el artículo L. 241-2 del Código Rural y de la Pesca Marítima, expedidos por un Estado miembro de la Unión Europea (UE) o parte en el Espacio Económico Europeo (EEE),
  - Certificado de aptitud para las funciones de inseminador o licencia de inseminator para la especie de que se trate,
  - Certificado de aptitud para las funciones de jefe de centro o licencia de un director del centro de la especie de que se trate;
- validando la experiencia profesional si el individuo justifica al menos tres años de experiencia laboral a tiempo completo y después de la revisión del expediente por el centro de evaluación que validará o no esta experiencia después de entrevistar con el candidato ;
- éxito en una evaluación precedida o no por la formación.

**Tenga en cuenta que**

La evaluación consta de tres pruebas:

- La parte escrita consta de dos pruebas de cuestionario de opción múltiple (MQ): una sobre regulación y otra sobre conocimientos científicos y técnicos; El candidato ha tenido éxito si recibe al menos 12/20 en cada una de las pruebas;
- La parte práctica, que implica una situación profesional de 30 minutos, da como resultado una evaluación de la cuadrícula; el candidato tuvo éxito si al menos el 75% de los puntos objetivo se lograron correctamente. Algunos puntos son la eliminación en caso de fallo.

*Para ir más allá*: Artículo 9 y Apéndice II de la orden de 18 de enero de 2007.

#### Costos asociados con la calificación

Para obtener más información, es aconsejable acercarse a las organizaciones de formación.

### b. Nacionales de la UE o del EEE: para el ejercicio temporal y ocasional (Servicio Gratuito)

Un nacional de un Estado de la UE o del EEE que prosiga la actividad de los inseminadores en uno de estos Estados podrá utilizar su título profesional en Francia, ya sea temporal u ocasionalmente.

Antes de su primera actuación, tendrá que hacer una declaración de actividad con el centro de evaluación (ver infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que está legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año en los diez años que preceder el rendimiento.

Si el examen de las cualificaciones profesionales muestra diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y su ejercicio en Francia, el interesado podrá ser sometido a una prueba de aptitud en un plazo de tiempo. un mes después de la recepción por parte del prefecto de la solicitud de declaración.

*Para ir más allá*: Artículos R. 653-87 y R. 204-1 del Código de Pesca Rural y Marina.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si:

- posee un certificado de formación o certificado de competencia expedido por una autoridad competente de otro Estado miembro que regula el acceso o el ejercicio de la profesión;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Una vez que cumpla una de las dos condiciones anteriores, tendrá que obtener el Cafti requerido para el ejercicio de la profesión, del centro de evaluación competente (ver infra "5o. b. Obtener Cafti para los nacionales de la UE o del EEE para un ejercicio permanente (LE)). Si, durante el examen del expediente, el prefecto constata que existen diferencias sustanciales entre la formación y la experiencia profesional del nacional y las necesarias para ejercer en Francia, podrán adoptarse medidas de compensación ("5 grados). b. Bueno saber: medidas de compensación").

*Para ir más allá* ( párrafo II del artículo R. 653-87 del Código de Pesca Rural y Marina y artículo 11 de la Orden del 18 de enero de 2007.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Los veterinarios que llevan a cabo una misión de inseminación para especies bovinas, ovinos o caprinas están sujetos a normas éticas y éticas aplicables a la profesión.

Para los inseminadores distintos de los veterinarios, tienen obligaciones éticas. En particular, deben respetar el secreto profesional asociado a los datos de inseminación y a los reglamentos relativos al bienestar, la higiene, la seguridad y el mantenimiento de la dignidad del animal inseminado.

4°. Seguro
-------------------------------

En caso de ejercicio liberal, el inseminator tiene la obligación de consumir un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante esta actividad.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)

#### Autoridad competente

El centro de evaluación autorizado por el Ministro responsable de la agricultura es competente para decidir sobre la declaración de solicitud.

#### Renovación de la predeclaración

Debe renovarse una vez al año y con cada cambio en la situación laboral.

#### Documentos de apoyo

La declaración previa del nacional deberá transmitirse por cualquier medio a la autoridad competente e incluir los siguientes documentos justificativos:

- Prueba de la nacionalidad del profesional
- un certificado que lo certifique:- está legalmente establecido en un Estado de la UE o del EEE,
  - ejerce una o más profesiones cuya práctica en Francia requiere la celebración de un certificado de capacidad,
  - no incurre en una prohibición de ejercer, aunque sea temporal, al expedir el certificado;
- prueba de sus cualificaciones profesionales
- cuando ni la actividad profesional ni la formación estén reguladas en la UE o en el Estado del EEE, se demuestre por ningún medio que el nacional haya participado en esta actividad durante un año, a tiempo completo o a tiempo parcial, en los últimos diez años.

Esta declaración anticipada incluye información relativa al seguro u otros medios de protección personal o colectiva suscritos por el solicitante de registro para cubrir su responsabilidad profesional.

Estos documentos se adjuntan, según sea necesario, a su traducción al francés.

#### hora

El centro de evaluación dispone de un mes a partir de la recepción del expediente para tomar su decisión:

- Permitir que el reclamante realice su beneficio;
- someter a la persona a una medida de compensación en forma de prueba de aptitud, si resulta que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de las requeridas para el ejercicio de la profesión en Francia;
- informarles de una o más dificultades que puedan retrasar la toma de decisiones. En este caso, tendrá dos meses para decidir, a partir de la resolución de las dificultades.

En ausencia de una respuesta de la autoridad competente dentro de estos plazos, puede comenzar la prestación de servicios.

*Para ir más allá*: Artículos R. 204-1 y R. 653-87, párrafo III del Código Rural y de la pesca marina.

### b. Obtener Cafti para nacionales de la UE o del EEE con actividad permanente (LE)

#### Autoridad competente

El centro de evaluación autorizado por el Ministro de Agricultura es responsable de la emisión del Cafti a la UE o al nacional del EEE que desee establecerse en Francia y ejercer como inseminante.

#### Procedimiento

El nacional deberá proporcionar al centro de evaluación todos los documentos necesarios para respaldar su solicitud de certificado, incluidos:

- prueba de nacionalidad
- Un certificado de formación o certificado de competencia adquirido en un Estado miembro de la UE o del EEE;
- cualquier prueba, en su caso, de que el nacional ha sido inseminator durante un año a tiempo completo o a tiempo parcial en los últimos diez años en un Estado miembro que no regula la profesión.

Después de revisar el expediente, el centro decidirá sobre la admisibilidad de la solicitud. Puede decidir:

- o la concesión del Cafti al nacional;
- o la necesidad de recurrir a una medida de compensación en caso de diferencias sustanciales entre la formación del interesado y la exigida en Francia.

#### Bueno saber: medidas de compensación

Para llevar a cabo su actividad en Francia o para acceder a la profesión, el nacional puede estar obligado a someterse a la medida de compensación, que puede ser:

- un curso de adaptación de hasta tres años
- una prueba de aptitud realizada dentro de los seis meses siguientes a la notificación al interesado.

El centro de evaluación podrá decidir o dejar la decisión de la medida de compensación al nacional en virtud de los términos de los artículos[R. 204-4](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A0EB9EE48CC8DFB61D663334A035FF06.tplgfr40s_1?idArticle=LEGIARTI000031699999&cidTexte=LEGITEXT000006071367&dateTexte=20171205&categorieLien=id&oldAction=&nbResultRech=) Y[R. 204-5](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=A0EB9EE48CC8DFB61D663334A035FF06.tplgfr40s_1?idArticle=LEGIARTI000034398078&cidTexte=LEGITEXT000006071367&dateTexte=20171205&categorieLien=id&oldAction=&nbResultRech=) Código del Código Rural y Pesca Marítima.

*Para ir más allá*: Artículos R. 204-2 y R. 653-87, párrafo II del Código de Pesca Rural y Marina.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

##### Condiciones

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

##### Procedimiento

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

##### Documentos de apoyo

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

##### hora

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

##### Costo

Gratis.

##### Resultado del procedimiento

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

##### Más información

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

