﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP203" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Ortopedista" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="ortopedista" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/ortopedista.html" -->
<!-- var(last-update)="2020-04-15 17:21:53" -->
<!-- var(url-name)="ortopedista" -->
<!-- var(translation)="Auto" -->


Ortopedista
===========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:53<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El ortopedista es un profesional de la salud especializado en rehabilitación visual.

Como tal, se dedica a detectar discapacidades visuales y a asegurar la rehabilitación y rehabilitación de la vista de sus pacientes, a todas las edades, con receta médica.

Trabajando en estrecha colaboración con oftalmólogos, el ortopedista pone en marcha protocolos organizativos para garantizar la atención al paciente. Esto puede incluir la preparación del examen médico por el oftalmólogo o el seguimiento por parte del ortopediste de un paciente cuya patología visual es diagnosticada.

En ausencia de un médico, también puede ser capaz de realizar los primeros actos necesarios de atención en ortogas.

*Para ir más allá*: Artículo L. 4342-1 del Código de Salud Pública; Decreto No 2016-1670 de 5 de diciembre de 2016 sobre la definición de ortoptoy el ejercicio de la profesión de ortóptista.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La práctica del ortopedista está reservada a los titulares del certificado de capacidad ortopedista.

*Para ir más allá*: Artículo L. 4342-3 del Código de Salud Pública.

#### Entrenamiento

La formación que conduce al Certificado de Capacidad de Ortopedia está disponible para los licenciatarios:

- Bachillerato;
- Licenciado en acceso a la educación universitaria
- cualificaciones o experiencia que se considere suficiente.

El acceso a la formación es después de pasar una competición de admisión (con numerus clausus) en uno de los[catorce escuelas](https://supexam.fr/prepa-paramedical/orthoptiste/ecoles/) formación en ortología en Francia, que dura tres años.

**Qué saber**

El estudiante tendrá que realizar varias prácticas durante su formación que grabará en un cuaderno.

Al final de cada año, el estudiante debe aprobar exámenes escritos y orales, obteniendo el promedio general de puntos en todas las pruebas. En el último año, tendrán que pasar pruebas escritas y orales, una demostración de un examen ortóptico o dispositivo de tratamiento, y hacer una disertación final.

*Para ir más allá* : Orden de 20 de octubre de 2014 relativa a los estudios para el certificado de capacidad ortopedista.

#### Costos asociados con la calificación

El entrenamiento vale la pena. Para más información, es aconsejable acercarse a los establecimientos dispensadores.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Un nacional de un Estado de la UE o del EEE que actúe legalmente en uno de estos Estados puede utilizar su título profesional en Francia de forma temporal y casual.

Tendrá que solicitarlo, antes de su primera actuación, mediante declaración dirigida al prefecto de la región en la que desea realizar la entrega (véase infra "5o. a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)").

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el estado en el que esté legalmente establecida, el profesional deberá justificar haberla realizado en uno o varios Estados miembros durante al menos un año durante los diez años durante los diez años antes de la actuación.

*Para ir más allá*: Artículo L. 4342-5 del Código de Salud Pública.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Todo nacional de un Estado de la UE o del EEE establecido y ejerce legalmente la actividad ortopedista en ese Estado podrá llevar a cabo la misma actividad en Francia de forma permanente si:

- posee un certificado de formación expedido por una autoridad competente de otro Estado miembro, que regula el acceso o el ejercicio de la profesión;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación o el ejercicio de la profesión;
- posee un diploma, título o certificado adquirido en un tercer Estado pero reconocido y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona ha sido ortopedista durante tres años en el Estado que ha equivalencia admitida.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar una autorización individual para ejercer desde el prefecto de la región en la que desea ejercer su profesión (véase infra "5o). b. Obtener autorización individual para la UE o el EEE nacional para la actividad permanente (LE) ").

*Para ir más allá*: Artículo L. 4342-4 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Aunque no están codificados, las reglas de ética son responsabilidad de aquellos que desean practicar como ortopedista, incluyendo:

- Ofrecer la mejor atención posible a sus pacientes;
- mantener y desarrollar sus conocimientos científicos del campo;
- comportarse con lealtad y respeto hacia las personas con las que contactan en su práctica profesional.

Además, el ortopedista está obligado a respetar la confidencialidad profesional asociada con la práctica de su actividad. El incumplimiento de esta obligación se castiga con un año de prisión y una multa de 15.000 euros.

*Para ir más allá*: Artículo L. 4344-2 del Código de Salud Pública.

4°. Legislación social y seguro
----------------------------------------------------

### a. Obligación de constete de un seguro de responsabilidad civil profesional

Como profesional de la salud, un ortopedista liberal debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

### b. Obligación de adherirse al fondo autosostenible de pensiones y pensiones de los asistentes médicos (CARPIMKO)

Cualquier ortopedista que actúe en la forma liberal, incluso como sustituto, debe unirse a CARPIMKO dentro de un mes de comenzar su actividad.

El interesado tendrá que transmitir el[Forma](https://www.carpimko.com/document/pdf/affiliation_declaration.pdf) completado, fechado y firmado, y adjuntar a su solicitud una fotocopia de su título de formación, así como su número de registro Adeli (ver infra "5." v. Registro en el directorio Adeli").

### c. Obligación de Informes de Seguros Médicos

El ortopedista que proactúe en la forma liberal debe declarar su actividad con el Fondo de Seguro de Salud Primaria (CPAM).

El registro se puede hacer en línea en el[sitio web oficial](https://www.ameli.fr/orthoptiste) Medicare.

El solicitante tendrá que adjuntar a su solicitud:

- Una copia de un documento de identidad válido
- Un declaración de identidad bancaria profesional
- si es necesario, la documentación justificativa o los títulos que permiten el acceso al Sector 2.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)

**Autoridad competente**

El prefecto regional es responsable de decidir sobre la solicitud de una declaración previa de actividad.

**Documentos de apoyo**

La solicitud se realiza mediante la presentación de un archivo que incluye los siguientes documentos:

- Una copia de un documento de identidad válido
- Una copia del título de formación que permite ejercer la profesión en el estado de obtención;
- un certificado de menos de tres meses de edad de la autoridad competente del Estado de la UE o del EEE que certifique que la persona está legalmente establecida en ese Estado y que, cuando se expide el certificado, no existe prohibición, ni siquiera temporal, Ejercicio
- cualquier prueba que justifique que el nacional haya ejercido la profesión en un Estado de la UE o del EEE durante un año en los últimos diez años, cuando dicho Estado no regule la formación o el acceso a la profesión solicitada o a su ejercicio;
- cuando el certificado de formación haya sido expedido por un tercer Estado y reconocido en un Estado de la UE o del EEE distinto de Francia:- reconocimiento del título de formación establecido por las autoridades estatales que han reconocido este título,
  - cualquier prueba que justifique que el nacional ha ejercido la profesión en ese estado durante tres años;
- Si es así, una copia de la declaración anterior, así como la primera declaración hecha;
- un certificado de responsabilidad civil profesional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**hora**

Una vez recibido el expediente, el prefecto regional dispondrá de un mes para decidir sobre la solicitud e informará al nacional:

- que puede comenzar la actuación. A partir de entonces, el prefecto registrará al solicitante en el directorio Adeli;
- que estará sujeto a una medida de compensación si existen diferencias sustanciales entre la formación o la experiencia profesional del nacional y las requeridas en Francia;
- no podrá iniciar la actuación;
- cualquier dificultad que pueda retrasar su decisión. En este último caso, el prefecto podrá tomar su decisión en el plazo de dos meses a partir de la resolución de esta dificultad, y a más tardar tres meses de notificación al nacional.

El silencio del prefecto dentro de estos plazos valdrá la pena aceptar la solicitud de declaración.

**Tenga en cuenta que**

La rentabilidad es renovable cada año o en cada cambio en la situación del solicitante.

*Para ir más allá*: Artículos R. 4342-13, y R. 4331-12 a R. 4331-15 del Código de Salud Pública; 8 de diciembre de 2017 orden sobre la declaración previa de prestación de servicios para asesores genéticos, médicos y preparadores de farmacias y farmacias hospitalarias, así como para ocupaciones en el Libro III de Parte IV del Código de Salud Pública.

### b. Obtener autorización individual para la UE o el EEE nacional para la actividad permanente (LE)

**Autoridad competente**

La autorización de ejercicio es expedida por el prefecto de la región, previa aleteación del comité de ortopedistas.

**Documentos de apoyo**

La solicitud de autorización se realiza mediante la presentación de un expediente que contiene todos los siguientes documentos:

- el[Forma](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021906912&fastPos=2&fastReqId=1698185741&categorieLien=cid&oldAction=rechTexte) Solicitud de permiso de ejercicio;
- Una copia de un documento de identidad válido
- Una copia del título de formación
- Si es necesario, una copia de los diplomas adicionales;
- cualquier prueba que justifique la formación, la experiencia y las aptitudes adquiridas en la UE o en el Estado del EEE;
- una declaración de la autoridad competente del Estado de la UE o del EEE que justifique la ausencia de sanciones contra el nacional;
- Una copia de los certificados de las autoridades en la que se especifica el nivel de formación, el detalle y el volumen por hora de los cursos seguidos, así como el contenido y la duración de las prácticas validadas;
- cualquier documento que justifique que el nacional ha sido ortopedista durante un año en los últimos diez años en un Estado de la UE o del EEE en el que ni el acceso ni el ejercicio están regulados en ese Estado.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

El prefecto reconoce la recepción del expediente en el plazo de un mes y decidirá después de tener la opinión del comité de ortopedistas. Este último es responsable de examinar los conocimientos y habilidades del nacional adquiridos durante su formación o durante su experiencia profesional. Puede someter al nacional a una medida de compensación.

El silencio guardado por el prefecto de la región en un plazo de cuatro meses es una decisión de rechazar la solicitud de autorización.

*Para ir más allá*: Artículos R. 4342-10 a R. 4342-12 del Código de Salud Pública; decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización pertinentes para el examen de las solicitudes presentadas para la práctica en Francia de las profesiones de psicomotricista, logopeda, ortopedista, audiotésico y óptico-lunetier.

**Bueno saber: medidas de compensación**

Si el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia profesional muestra diferencias sustanciales con las cualificaciones requeridas para el acceso a la profesión de ortopedista y su ejercicio En Francia, el interesado tendrá que someterse a una medida de compensación que puede ser un curso de adaptación o una prueba de aptitud.

La prueba de aptitud toma la forma de un examen escrito u oral puntuado de 20. Su validación se pronuncia cuando el nacional ha obtenido una puntuación media de diez de veinte o más, sin una puntuación inferior a ocho de veinte. Si tiene éxito en la prueba, el nacional podrá utilizar el título de ortopedista.

La pasantía se realiza en un centro de salud público o privado, o a nivel profesional, y no debe durar más de tres años. También incluye formación teórica que será validada por el gestor de prácticas.

*Para ir más allá* : decreto de 30 de marzo de 2010 por el que se establece la organización de la prueba de aptitud y el curso de adaptación para la práctica en Francia de las profesiones de psicomotricismo, logopeda, ortopedista, audioprotésico, óptico-lunetier por Estados miembros de la Unión Europea o partes en el Acuerdo sobre el Espacio Económico Europeo.

### c. Registro en el directorio Adeli

Un nacional que desee ejercer como ortopedista en Francia está obligado a registrar su autorización para ejercer en el directorio Adeli ("Automatización de Listas").

**Autoridad competente**

El registro en el directorio de Adeli se realiza con la Agencia Regional de Salud (ARS) del lugar de práctica.

**hora**

La solicitud de inscripción se presenta en el plazo de un mes a partir de la toma del cargo del nacional, independientemente del modo de práctica (liberal, asalariado, mixto).

**Documentos de apoyo**

En apoyo de su solicitud de registro, el ortopedista debe proporcionar un archivo que contenga:

- el diploma o título original que acredite la formación del ortopedista expedido por el Estado de la UE o el EEE (traducido al francés por un traductor certificado, si procede);
- Id
- Formulario de ciervo 10906-06 completado, fechado y firmado.

**Resultado del procedimiento**

El número Adeli del nacional se mencionará directamente en la recepción del expediente, emitido por el ARS.

**Costo**

Gratis.

*Para ir más allá*: Artículo L. 4342-2 del Código de Salud Pública.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

