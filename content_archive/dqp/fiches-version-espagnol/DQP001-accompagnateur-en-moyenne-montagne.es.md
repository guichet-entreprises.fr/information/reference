﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP001" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Deporte" -->
<!-- var(title)="Guía de montaña media" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="deporte" -->
<!-- var(title-short)="guia-de-montana-media" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/deporte/guia-de-montana-media.html" -->
<!-- var(last-update)="2020-04-15 17:22:31" -->
<!-- var(url-name)="guia-de-montana-media" -->
<!-- var(translation)="Auto" -->


Guía de montaña media
=====================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:31<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El acompañante de montaña media es un profesional que dirige y supervisa a personas o grupos en zonas rurales de montaña, excluyendo las zonas glaciares, rocas, cañones y tierras que requieren el uso de equipos o técnicas de montañesa para la progresión.

Además, el guía de montaña media tiene habilidades específicas dependiendo de la opción que elija:

- la opción de "montaña de nieve media" le permite hacer ejercicio en terreno montañoso, excluyendo cualquier accidente de terreno importante. Queda excluida la práctica de todas las disciplinas de esquí y actividades spin-off, con la excepción de la raqueta de nieve;
- la opción de "montaña tropical y ecuatorial media" le permite ejercer fuertes lluvias en regiones climáticas tropicales y ecuatoriales en terrenos escarpados y empapados durante períodos, fijados por la autoridad pública competente.

Sus habilidades deben permitirle:

- gestionar el riesgo, en una lógica de seguridad, asociado con el senderismo en las montañas medias;
- Diseñar, coordinar y llevar a cabo un proyecto de acción en iniciación, desarrollo y formación en la media montaña para todo el público;
- Diseñar e implementar programas educativos y de formación en los ámbitos de la educación en el medio natural y humano y en el ámbito de la formación profesional;
- desarrollar prácticas sostenibles de senderismo de media montaña de acuerdo con el medio ambiente de montaña y las regulaciones;
- para realizar funciones de gestión, comunicación y diseño de proyectos relacionadas con las actividades de montaña y, en particular, con la organización de su profesión.

La ocupación del acompañante de montaña media puede practicarse a tiempo completo o como una ocupación estacional.

**Bueno saber: la obligación de reciclaje**

El permiso para trabajar como escolta en las montañas medias está limitado a un período de seis años, renovable. Al final de este período, la renovación de la licencia para ejercer se concede a los titulares del diploma de escolta de montaña media que hayan completado un curso de actualización organizado por la Escuela Nacional de Deportes de Montaña (véase más adelante: "Entrenamiento reciclaje").

*Para ir más allá* : Artículo 1 y siguientes del decreto de 25 de septiembre de 2014 relativo a la formación específica del Diploma Estatal de Montañero-acompañamiento en la mitad de la montaña, decreto de 11 de marzo de 2015 relativo al contenido y organización del reciclaje de los titulares de diplomas de escolta de montaña media.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La actividad de escolta de montaña media está sujeta a la aplicación del artículo L. 212-1 del Código del Deporte, que requiere certificaciones específicas.

Como profesor de deportes, el acompañante de la montaña media debe poseer un diploma, un título profesional o un certificado de calificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- y grabado en el[directorio nacional de certificaciones profesionales](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Los títulos extranjeros pueden ser admitidos en equivalencia a los títulos franceses por el Ministro responsable de la deporte, tras el dictamen de la Comisión para el Reconocimiento de Cualificaciones colocado con el Ministro.

**Bueno saber: el entorno específico**

El montañismo y sus actividades asociadas se consideran actividades en un "entorno específico" (ES), independientemente del área de evolución. En consecuencia, la práctica de estas actividades requiere el cumplimiento de las medidas de seguridad específicas a que se refiere la sección L. 212-2 del Código del Deporte.

*Para ir más allá*: Artículos L. 212-1, R. 212-84 y D. 212-67 del Código del Deporte.

#### Entrenamiento

Para trabajar como asistente en las montañas medias, el interesado debe tener el diploma estatal de montañero - un asistente en las montañas medias, que le da la calidad de un educador deportivo.

El diploma estatal de montañismo - guía de montaña media se clasifica como nivel III (bac 2). Se prepara después de un curso de formación en condición de estudiante o estudiante, a través de un contrato de aprendizaje, después de un curso de educación continua, en un contrato de profesionalización, por solicitud individual o por medio de validación de (VAE). Para obtener más información, puede ver[Sitio web oficial de VAE](http://www.vae.gouv.fr/) así como el artículo 23 del decreto de 25 de septiembre de 2014 relativo a la formación específica del Diploma Estatal de Montañería - guía de montaña media.

El individuo debe elegir una de las dos opciones:

- montaña mediana cubierta de nieve;
- tropical y ecuatorial.

La formación que conduce al Diploma Estatal de Montañismo - guía de montaña media es proporcionada por la Escuela Nacional de Deportes de Montaña (ENSM), en el sitio de la[Centro de esquí nórdico y de media montaña](http://cnsnmm.sports.gouv.fr/) (CNSNMM) en Premanon. La formación específica del Diploma Estatal de Montañismo - guía de montaña media que integra la unidad de formación "montaña tropical mediana y ecuatorial" se organiza en departamentos y regiones de ultramar.

La formación que conduce al diploma estatal de montañismo - un ayudante en las montañas medias está precedida por un examen de prueba. Una vez completado este examen, la formación alterna entre períodos de formación, una pasantía situacional, un período de observación y concluye con un examen final.

**Condiciones para el acceso a la formación que conduce al diploma estatal de montañero - guía de montaña media: el examen probatorio**

Para ser admitido en la formación previa al Diploma Estatal de Montañaria - guía de montaña media, el candidato debe pasar un examen de prueba que consiste en diferentes pruebas, algunas de las cuales son la eliminación.

**Requisitos de libertad condicional**

- Tener diecisiete años antes del 1 de enero del año del examen;
- Envíe un archivo de registro que contenga:- Una solicitud de registro en un formulario estandarizado,
  - una identificación con foto reciente,
  - una fotocopia a dos lados del dNI o pasaporte nacional,
  - Una fotocopia del certificado censal o el certificado individual de participación en el día de la defensa y la ciudadanía, si lo hubiere,
  - permiso de los padres (o tutor legal) si el solicitante es menor de edad,
  - un certificado de no contradictorio para el ejercicio de la profesión de escolta de montaña media, de menos de un año de edad en la fecha de cierre del primer registro,
  - fotocopia de la unidad de enseñanza "Nivel 1 de Prevención Cívica y Alivio" (PSC1), o su equivalente,
  - tres sobres sellados de acuerdo con las tarifas detalladas en la Lista I del orden de 25 de septiembre de 2014 mencionado anteriormente,
  - una lista de cuarenta caminatas llevadas a cabo por el candidato, elaborada sobre una forma estandarizada desarrollada por la Escuela Nacional de Deportes de Montaña, sitio de la National Nordic y Mid-Mountain Ski Centre. El contenido mínimo de esta lista se especifica en la Lista I del orden de 25 de septiembre de 2014 mencionado anteriormente.

**Contenido y conducta de la revisión probatoria**

El examen probatorio consta de tres pruebas que tienen lugar en el siguiente orden cronológico:

- un evento a pie, orientación y curso en terreno sin salida (este evento es eliminación). La prueba individual se lleva a cabo en autonomía de navegación y, para todo o en parte, fuera del camino. El recorrido dura entre 6 y 8 horas e incluye un gradiente positivo acumulado de unos 1.400 metros. Durante el curso, el candidato lleva una mochila de 10 kilogramos para los hombres y 7 kilogramos para las mujeres;
- un cuestionario sobre el entorno natural y humano de la montaña, que dura hasta una hora. Esta prueba escrita tiene como objetivo asegurar que el candidato tenga los conocimientos básicos del entorno natural de montaña (ecología general, fauna, flora, geología, geografía, meteorología) y humano (economía, hábitat y vida social, protección del medio ambiente);
- una entrevista de 20 minutos, realizada a partir de la lista de caminatas proporcionadas en el expediente de registro del examen de prueba. Esta prueba tiende a asegurar la realidad de las experiencias del candidato y a apreciar su experiencia de vida en las montañas, así como su capacidad de comunicación.

Para ser admitido en el examen de prueba, debe haber aprobado la primera prueba y validado los dos siguientes. Si es necesario, el Director Regional de Juventud, Deportes y Cohesión Social, que organiza el examen, dirige al candidato, un certificado de éxito en el examen de prueba.

*Para ir más allá* : Descripción de la certificación "DE: diploma estatal de montañismo - acpanista de montaña media" del directorio nacional de certificaciones profesionales (RNCP), decreto de 25 de septiembre de 2014 relativo a la formación específica del título estatal montañería - guía de montaña media.

###### Curso de reciclaje

El curso de reentrenamiento, que dura un mínimo de 24 horas, tiene por objeto actualizar las competencias profesionales de los chaperones de media montaña, en particular en los ámbitos de la gestión de la seguridad, significa obligación y regulación, basado en el análisis preliminar de las prácticas profesionales y su evolución: análisis de riesgos, sinspicato, cambios en el marco jurídico o social, actualización del conocimiento y know-how.

Al final de cada sesión, los certificados de reciclaje son expedidos por el Director General de la Escuela Nacional de Deportes de Montaña, sede de la Central Nacional de Esquí Nórdico y Mid-Mountain.

El curso de reentrenamiento debe tener lugar a más tardar el 31 de diciembre del sexto año después de la graduación o el reentrenamiento previo.

*Para ir más allá* : decreto de 11 de marzo de 2015 relativo al contenido y organización de la readaptación de los titulares de diplomas de escolta de mitad de montaña.

#### Costos asociados con la calificación

Se paga la formación que conduce al diploma estatal de montañismo - un acompañamiento en las montañas medias - se paga. Como indicación, cuesta unos 4.500 euros. Para más detalles, es aconsejable acercarse a la[Escuela Nacional de Deportes de Montaña](http://www.ensm.sports.gouv.fr/).

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Nacionales de la Unión Europea (UE)*Espacio Económico Europeo (EEE)* legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional a condición de que haya enviado una declaración previa de actividad al prefecto del departamento de entrega.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

**Bueno saber: la diferencia sustancial**

Para la supervisión del montañismo como acompañante en las montañas medias, la diferencia sustancial, que probablemente existirá entre la cualificación profesional del solicitante de registro y la cualificación profesional requerida en el territorio se aprecia en referencia a la formación que conduce al diploma estatal de montañismo - un acompañamiento en las montañas medias, ya que se integra:

- Conocimientos teóricos y prácticos sobre seguridad
- habilidades técnicas de seguridad.

En el contexto de la libre prestación de servicios, cuando el prefecto considere, tras el asesoramiento de la sección permanente de montañismo de la Comisión de Formación y Empleo del Consejo Superior de Deportes de Montaña, se remitió al Centro Nacional de Comercios de esquí y montañismo, ya sea que haya una diferencia sustancial, puede decidir someter al declarante a la totalidad o parte de la prueba de aptitud o a un curso de adaptación.

*Para ir más allá*: Artículos A. 212-222 y A. 212-224 del Código del Deporte.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

**Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad**

- Poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia.
- Poseer un título adquirido en un tercer Estado y admitido en equivalencia en un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

**Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad**

- Para justificar la realización de la actividad en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso del ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia.
- Tener un titular de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada dirigida a específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá*: Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

**Bueno saber: la diferencia sustancial**

Para la supervisión del montañismo como acompañante en las montañas medias, la diferencia sustancial que puede existir entre la cualificación profesional del solicitante de registro y la cualificación profesional requerida en el territorio se aprecia en referencia a la formación que conduce al diploma estatal de acpanista de montaña media, ya que integra:

- Conocimientos teóricos y prácticos sobre seguridad
- habilidades técnicas de seguridad.

En el contexto de la libertad de establecimiento, cuando el prefecto considere, tras el asesoramiento de la sección permanente de montañismo del Comité de Formación y Empleo del Consejo Superior de Deportes de Montaña, se remitió al Centro Nacional de Comercios de la gestión del esquí y el montañismo, que hay una diferencia sustancial, remitió el asunto a la comisión de reconocimiento, adjuntando la opinión de la sección permanente al expediente.

Después de pronunciarse sobre la existencia de una diferencia sustancial, la Comisión de Reconocimiento de Cualificaciones propone, si es necesario, al prefecto someter al declarante a la totalidad o parte de la prueba de aptitud o a un curso de ajuste.

Por otra parte, si el prefecto cree que no hay diferencia sustancial o cuando se ha identificado una diferencia sustancial y el solicitante de registro ha cumplido con la prueba de aptitud, el prefecto expide un certificado de libertad al declarante. establecimiento y una tarjeta de educador deportivo profesional.

*Para ir más allá*: Artículos A. 212-222, A. 212-223 y A. 212-228, R. 212-88 a R. 212-91 del Código del Deporte.

3°. Condiciones de honorabilidad
-----------------------------------------

Está prohibido ejercer como escolta media de montaña en Francia para personas que hayan sido condenadas por cualquier delito o por uno de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como narcóticos o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá*: Artículo L. 212-9 del Código del Deporte.

4°. Requisito de notificación (con el fin de obtener la tarjeta de educador deportivo profesional)
-----------------------------------------------------------------------------------------------------------------------

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el [sitio web oficial](https://eaps.sports.gouv.fr).

**hora**

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

**Documentos de apoyo**

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 212-11, R. 212-85 y A. 212-176 a A. 212-178 del Código del Deporte.

5°. Procedimientos y formalidades de reconocimiento de cualificaciones
--------------------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales y ocasionales (LPS)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

**Autoridad competente**

La declaración previa de actividad debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde el Departamento en el que declarante quiere realizar su actuación.

**hora**

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

**Documentos de apoyo**

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-92 y siguientes, A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### b. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

**hora**

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

**Documentos de apoyo para la primera declaración de actividad**

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

**Evidencia para una declaración de renovación de la actividad**

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá*: Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

**Bueno saber: medidas de compensación (actividad en un entorno específico)**

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de las cualificaciones colocadas al Ministro responsable del deporte. Esta comisión, antes de emitir su dictamen, remite a los órganos de consulta especializados para obtener asesoramiento.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud, la naturaleza que define (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía. , tipos de estructuras que pueden acomodar al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- si el solicitante de registro requiere compensación, debe someterse a una prueba de aptitud. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá*: Artículos R. 212-84, R. 212-90-1 y D. 212-84-1 del Código del Deporte.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

