﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP204" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Osteópata" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="osteopata" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/osteopata.html" -->
<!-- var(last-update)="2020-04-15 17:21:54" -->
<!-- var(url-name)="osteopata" -->
<!-- var(translation)="Auto" -->


Osteópata
=========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:54<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

Un osteópata es un profesional de la salud cuya actividad consiste en analizar, evaluar los trastornos funcionales del cuerpo de sus pacientes y tratarlos mediante la realización de manipulaciones manuales y externas. Estas manipulaciones pueden ser musculoesqueléticas o miofasciales (dolor local de los músculos).

**Tenga en cuenta que**

El profesional del osteópata está obligado, si no es médico, a derivar a su paciente a un profesional siempre y cuando los síntomas requieran un diagnóstico o tratamiento médico.

*Para ir más allá* Decreto No 2007-435, de 25 de marzo de 2007, sobre los actos y condiciones de osteopatía.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Puede ejercitar la actividad del osteópata:

- médicos, parteras, masajistas con una universidad o título interuniversitario de una universidad médica y reconocido por el Consejo Nacional de la Orden de los Médicos. Es aconsejable consultar los registros relativos a estas profesiones para obtener más información;
- titulares de un diploma sancionando la formación en osteopatía expedido por una institución acreditada en las condiciones del Decreto No 2014-1043, de 12 de septiembre de 2014, relativo a la acreditación de instituciones de formación en osteopatía (véase infra "Formación").

Una vez que cumpla una de estas condiciones, el interesado debe registrar su documento de formación en la Agencia Regional de Salud (véase infra "5o). a. Proceder a registrar su título como osteópata").

#### Entrenamiento de osteopatía

Para practicar, el profesional debe haber sido entrenado en osteopatía.

Este curso de cinco años está disponible para los solicitantes que tienen al menos diecisiete años de edad el 31 de diciembre del año en que ingresan a la formación y tienen una licenciatura.

El candidato debe presentar un expediente de solicitud a la institución en la que desee llevar a cabo su formación. Este archivo incluye:

- Un CV con una carta de presentación
- un registro escolar con todos sus resultados y evaluaciones;
- Si es necesario, un certificado de trabajo
- Una copia de su licenciatura o título equivalente
- un certificado de escolarización para estudiantes de último año.

Una vez seleccionado, el candidato debe ser entrevistado para evaluar sus habilidades y motivaciones.

Cada año de formación se divide en unidades didácticas teóricas y prácticas, cuyos contenidos serán objeto de un anexo publicado en breve en el boletín oficial de salud, protección social y solidaridad. Además, el candidato debe realizar varias prácticas prácticas durante su formación clínica práctica y al final de su formación apoyar una tesis.

Al final del curso, el diploma se otorga a los solicitantes que tienen:

- validaron todas las unidades docentes;
- apoyó con éxito su tesis de graduación;
- 150 consultas completas y habiendo adquirido todas las habilidades en la formación clínica práctica.

*Para ir más allá* : decreto de 12 de diciembre de 2014 relativo a la formación en osteopatía.

#### Costos asociados con la calificación

El costo de la formación varía en función del curso previsto. Para más información es aconsejable acercarse a las instituciones interesadas.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Cualquier nacional de un Estado miembro de la Unión Europea (UE) de un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) que actúe como osteópata podrá llevar a cabo la misma actividad de forma temporal e informal en Francia.

Para ello, el interesado debe hacer una declaración previa antes de su primer servicio en Francia (véase infra "5o. b. Predeclaración del nacional para un ejercicio temporal e informal (LPS)").

*Para ir más allá*: Artículo 10 y siguientes del Decreto No 2007-435 de 25 de marzo de 2007 relativo a la práctica y condiciones de la osteopatía.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Cualquier nacional de un Estado miembro de la UE o del EEE puede, si está legalmente establecido en ese Estado, hacer uso de su título de osteópata en Francia.

Para ello, el nacional debe:

- obtener un título de formación expedido por un Estado miembro que regule la actividad de los osteópatas y que le permita llevar a cabo esta actividad;
- Cuando ni el acceso a la formación ni al ejercicio estén regulados en ese Estado miembro, ostiquena de osteopatía y certificado que justifique que ha participado en esta actividad durante al menos dos años en los últimos diez años;
- un certificado de formación expedido por un tercer Estado y reconocido en un Estado miembro distinto de Francia.

Una vez que el nacional cumpla estas condiciones, puede solicitar permiso para utilizar su título profesional con el Director General de la Agencia Regional de Salud (ARS) (véase infra "5o. c. Solicitud de autorización para ejercer un ejercicio permanente (LE)).

*Para ir más allá*: Artículos 6 a 9 del Decreto No 2007-435 de 25 de marzo de 2007 sobre las prácticas y condiciones de osteopatía.

3°. Reglas éticas
--------------------------

El osteópata está obligado por las reglas de ética aplicables a su profesión.

Como tal, debe:

- Mantenga los términos publicitarios del progreso científico y asegúrese de mantener sus habilidades para el ejercicio de su negocio;
- proporcionar toda su atención a sus pacientes sin discriminación;
- Ejercer de forma independiente y garantizar el respeto de la confidencialidad profesional del paciente y el derecho a la información;
- mantener buenas relaciones con los colegas y no actuar con el propósito de la competencia desleal.

*Para ir más allá* El código de ética está disponible en el[Sitio](http://www.afosteo.org/) Asociación de Osteopatía (AFO).

4°. Seguro
-------------------------------

El profesional del osteópata liberal está obligado a tomar un seguro de responsabilidad civil por cualquier daño sufrido por terceros durante el transcurso de su actividad.

Los límites mínimos de la garantía no pueden ser inferiores a:

- ocho millones de euros por reclamación por profesional;
- 15 millones de euros al año de seguro por profesional.

*Para ir más allá*: Artículo 1 del Decreto No 2014-1347, de 10 de noviembre de 2014, sobre los límites de garantía de los contratos de seguro suscritos por osteópatas y quiroprácticos.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Proceder a registrar su título de osteópata

**Autoridad competente**

El profesional deberá inscribir su diploma, título, certificado o autorización ante el director de la agencia regional de salud de su residencia profesional.

**Documentos de apoyo**

Su solicitud debe incluir:

- el[formulario Cerfa No. 13777*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13777.do) Interactivo completado y firmado;
- Id
- para el ciudadano francés: el título, título, certificado o autorización originales;
- para el nacional de la UE:- el original de su grado,
  - traducción, certificada por un traductor certificado de su diploma en francés,
  - autorización para llevar a cabo su actividad en Francia.

**Procedimiento**

Una vez registrado, el profesional está inscrito en el registro de osteópatas llamado Adeli (Automatización de Listas) y recibe un número de identificación.

**Costo**

Gratis.

*Para ir más allá*: Artículo 5 del Decreto de 25 de marzo de 2007 supra.

### b. Predeclaración del nacional para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe enviar su declaración por carta recomendada con solicitud de notificación de recepción, al director general de la agencia regional de salud de su elección.

**Documentos de apoyo**

Su solicitud debe incluir los siguientes documentos, si los hubiere, con su traducción al francés:

- Una tarjeta de registro civil y la nacionalidad;
- Copia de sus diplomas, certificados o títulos de formación;
- una prueba de la autoridad que ha expedido su diploma o título que certifica que la formación se llevó a cabo en una institución de educación superior mencionando su duración;
- La duración y el contenido de los estudios y prácticas realizados durante la formación;
- cuando el nacional haya adquirido su diploma, título o certificado en un tercer país y reconocido por un Estado miembro:- Un certificado del Estado miembro que certifique la duración y las fechas del ejercicio de su actividad,
  - si es necesario, se llevó a cabo una encuesta de cursos de educación continua.

**Procedimiento**

El Director General de la ARS decide tras la opinión del comité de osteópatas compuesto por el director del LRA, un médico, un masajista, dos osteópatas incluyendo un profesor. En el plazo de un mes a partir de la recepción de la solicitud, el Director General informará al solicitante:

- Que puede comenzar la prestación de servicios
- No puede comenzar la prestación de servicios;
- cuando hay diferencias sustanciales entre su formación y la requerida en Francia, debe someterse a una prueba de aptitud.

En caso de expediente incompleto, el Director General del LRA informa al solicitante que dispone de un mes para facilitar la información que falte.

Además, a falta de una respuesta del Director General del LRA, el nacional puede comenzar a prestar servicios.

**Tenga en cuenta que**

El nacional está en una lista especial y recibe un recibo y número de registro. La declaración es renovable cada año en las mismas condiciones.

*Para ir más allá*: Artículos 10 a 10-5 del Decreto de 25 de marzo de 2007 mencionados anteriormente; Decreto de 25 de marzo de 2007 relativo a la composición del expediente y a la organización de la prueba de aptitud y al curso de adaptación previsto para los osteópatas mediante el Decreto No 2007-435, de 25 de marzo de 2007, relativo a los actos y condiciones de práctica de Osteopatía.

### c. Solicitud de autorización para ejercer para el nacional de la UE para un ejercicio permanente (LE)

**Autoridad competente**

El nacional deberá presentar su solicitud al Director General del LRA, quien decidirá, tras el dictamen del comité de osteópatas mencionado anteriormente.

**Documentos de apoyo**

Su solicitud debe incluir los siguientes documentos, si los hubiere, con su traducción al francés:

- Un certificado de nacimiento y un documento de identidad válidos
- Un cv y carta de presentación
- Una copia de todos los diplomas, certificados o títulos obtenidos;
- Un documento que justifica que el nacional ha completado su formación en una institución de educación superior;
- El contenido de los estudios y prácticas realizados durante su formación, así como el volumen por hora de enseñanzas y pasantías;
- cuando el nacional haya obtenido su título o título de formación en un tercer país pero reconocido por un Estado miembro de la UE o cuando el Estado miembro del país de obtención no regule el ejercicio de la actividad o su acceso, un certificado del Estado miembro certificando la duración de la práctica profesional del solicitante y las fechas correspondientes.

**Retrasos y procedimientos**

El Director General del LRA confirma la recepción de la solicitud en el plazo de un mes y somete la solicitud al dictamen de la comisión regional de osteópatas, que se encargará de verificar toda la información.

**Tenga en cuenta que**

La falta de respuesta en un plazo de cuatro meses es una decisión de desestimar la solicitud.

**Bueno saber: medidas de compensación**

Cuando la formación recibida por el nacional sea al menos un año inferior a la requerida para el nacional francés o cuando se refiera a sujetos sustancialmente diferentes de los exigidos en Francia, la comisión encargada de evaluar su las cualificaciones profesionales podrán decidir someterla a una prueba de aptitud o a un curso de adaptación de no más de tres años y llevada a cabo bajo la responsabilidad de un profesional cualificado.

*Para ir más allá*: Artículos 6 a 9 del Decreto de 25 de marzo de 2007 antes mencionado;[sitio web oficial](https://www.ars.sante.fr/) Ars.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

