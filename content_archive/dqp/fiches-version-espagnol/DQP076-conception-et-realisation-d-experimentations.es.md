﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP076" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Diseñar y llevar a cabo experimentos con animales" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="disenar-y-llevar-a-cabo-experimentos" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/disenar-y-llevar-a-cabo-experimentos-con-animales.html" -->
<!-- var(last-update)="2020-04-15 17:20:51" -->
<!-- var(url-name)="disenar-y-llevar-a-cabo-experimentos-con-animales" -->
<!-- var(translation)="Auto" -->


Diseñar y llevar a cabo experimentos con animales
=================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:51<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La misión de la persona que diseña y lleva a cabo experimentos con animales es establecer protocolos experimentales para:

- verificar hipótesis científicas
- evaluar los posibles efectos secundarios de nuevos fármacos, tratamientos o nuevos productos en seres humanos en particular;
- recoger o examinar células, órganos o fluidos corporales para verificar el estado de salud de las poblaciones de animales.

La persona también está obligada a vigilar el bienestar de los animales en los centros experimentales y a garantizar que el cuidado que se les da se practique adecuadamente.

En algunos casos, también puede tener que realizar procedimientos quirúrgicos en animales.

**Tenga en cuenta que**

Ser diseñador y director de experimentos con animales no es una profesión en el sentido estricto de la palabra, sino una función adicional que forma parte de una profesión regulada.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La función de diseñador y director de experimentos con animales está reservada a los titulares:

- una maestría o un título de ingeniería que requiera al menos cinco años de escuela de posgrado;
- un diploma o una designación que requiera al menos dos años de estudios de posgrado en un campo científico y que justifiquen cinco años de experiencia profesional bajo la responsabilidad directa de una persona con un máster o un título mínimo de ingeniería;
- una sanción a nivel C a la Federación Europea de Ciencias Animales de Laboratorio (FELASA) para los nacionales de un Estado miembro de la Unión Europea (UE) o parte en el Espacio Económico Europeo (EEE).

*Para ir más allá*: Artículo 2 de la[1 de febrero de 2013](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027037960&categorieLien=id) en relación con la adquisición y validación de las habilidades del personal de usuario, criador y proveedores de animales utilizados con fines científicos.

#### Entrenamiento

Mientras la persona esté justificada por uno de los títulos anteriores, debe someterse a una formación adicional para desempeñar el papel de diseñador y director de experimentos con animales. Esta formación se lleva a cabo dentro de un año de la toma del trabajo y consiste en un módulo general que cubre todas las especies y módulos específicos que cubren uno de los grupos de especies que se determinan a continuación:

- roedores;
- alquilar mamíferos;
- carnívoros;
- Aves;
- animales de sangre fría;
- primates;
- Vida silvestre.

Cuando el proyecto en el que está trabajando implica procedimientos quirúrgicos, el diseñador y director de experimentos con animales tendrá que ser entrenado en aspectos de la propedéutica quirúrgica (cuidados pre y postoperatorios, aseptia, anestesia y analgesia) durante un entrenamiento adaptado de un mínimo de 24 horas.

**Tenga en cuenta que**

El diseñador y director de experimentos con animales con el título de cirujano, cirujano dental o veterinario está exento de este módulo complementario.

*Para ir más allá*: Artículo 3 y anexo a la[1 de febrero de 2013](https://www.legifrance.gouv.fr/jo_pdf.do?numJO=0&dateJO=20130207&numTexte=29&pageDebut=02210&pageFin=02212) en relación con la adquisición y validación de las habilidades del personal de usuario, criador y proveedores de animales utilizados con fines científicos.

#### Costos asociados con la calificación

La formación que conduce a la actividad de diseñador y director de experimentos con animales da sus frutos. Para más información, es aconsejable acercarse a los establecimientos dispensadores.

### b. Nacionales de la UE o del EEE: para ejercicios temporales o ocasionales (Servicio Gratuito)

Para desempeñar el papel de diseñador y director de experimentos con animales en Francia de forma temporal o ocasional, el nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo del Espacio Económico Europeo (EEE) debe aplazar la normativa aplicable a la profesión ejercida como directora.

Si el interesado ha sido entrenado en el diseño y la realización de experimentos con animales en su estado de origen, podrá, en caso necesario, solicitar el reconocimiento en Francia si cumple las dos condiciones siguientes:

- justificar por cualquier medio de seguir esta formación;
- seguir un módulo sobre la normativa francesa y otro sobre ética en uno de los establecimientos aprobados por el Ministerio de Agricultura.

En caso de que el nacional no justifique el seguimiento de la formación específica en su Estado de origen, tendrá que seguir la dada en Francia (véase más arriba "2. a. Capacitación").

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Para actuar como diseñador y director de experimentos con animales en Francia para un ejercicio permanente, el nacional de un Estado miembro de la UE o del EEE debe referirse a la normativa aplicable a la profesión ejercitada como director.

Si el interesado ha sido entrenado en el diseño y la realización de experimentos con animales en su estado de origen, podrá, en caso necesario, solicitar el reconocimiento en Francia si cumple las dos condiciones siguientes:

- justificar por cualquier medio de seguir esta formación;
- seguir un módulo sobre la normativa francesa y otro sobre ética en uno de los establecimientos aprobados por el Ministerio de Agricultura.

En caso de que el nacional no justifique el seguimiento de la formación específica en su Estado de origen, tendrá que seguir la dada en Francia (véase más arriba "2. a. Capacitación").

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. La ética

Cualquier persona que diseñe y realice experimentos con animales tiene la obligación de cumplir con las reglas éticas, incluida la regla 3R, que incluye:

- Reducción del número de animales utilizados con fines experimentales
- perfeccionar la metodología aplicada y encontrar soluciones para reducir el sufrimiento de los animales mediante la aplicación de puntos límite;
- reemplazar los modelos animales.

### b. Formación profesional continua

El diseñador y director de experimentos con animales debe someterse a una educación continua obligatoria de tres días cada seis años.

Esta formación debe permitirle actualizar sus conocimientos y puede adquirirse durante la formación práctica o la participación en seminarios en los campos relacionados con la experimentación con animales.

El seguimiento de la educación continua está justificado por la posesión de certificados de formación o, como mínimo, certificados de asistencia a seminarios registrados en un folleto de competencias que incluya:

- El título de la formación;
- El método de adquisición
- La fecha y duración de la formación
- fecha de validación de la formación.

*Para ir más allá*: Artículos 5 y 6 de la orden de 1 de febrero de 2013.

### v. Sanciones

Cualquier usuario, criador o proveedor de animales con fines experimentales debe ser capaz de justificar que su personal ha seguido la formación regulatoria y continua requerida.

En caso de incumplimiento de esta obligación durante la visita de acreditación del Departamento de Agricultura acompañado por un inspector veterinario del departamento, el jefe del establecimiento y su personal no capacitado podrán ser vistos castigar por un billete de 4a clase de hasta 750 euros. También puede poner en peligro la certificación o renovación de la institución.

4°. Seguro
-------------------------------

En caso de ejercicio liberal, la persona que diseña y lleva a cabo experimentos con animales está obligada a realizar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus trabajadores por los actos realizados en ocasiones.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Obligación de recibir formación para nacionales de la UE o del EEE

Un nacional de un Estado de la UE o del EEE que haya recibido formación que conduzca al papel de diseñador y desarrollador de experimentos con animales en ese estado podrá solicitar el reconocimiento en Francia siempre que recibaformación adicional incluyendo un módulo sobre regulaciones y ética francesa en uno de los establecimientos aprobados por el Ministerio de Agricultura.

### b. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

