﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP232" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Psicomomotrician" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="psicomomotrician" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/psicomomotrician.html" -->
<!-- var(last-update)="2020-04-15 17:22:05" -->
<!-- var(url-name)="psicomomotrician" -->
<!-- var(translation)="Auto" -->


Psicomomotrician
================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:05<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El psicomotricista es un profesional de la salud paramédica cuya actividad consiste en realizar la rehabilitación motora de sus pacientes. Como tal, puede que tenga que darse cuenta en sus pacientes:

- un chequeo psicomotor
- rehabilitación de todos los trastornos psicomotores;
- tratamientos para compensar:- discapacidades intelectuales,
  - a las características, personalidad o trastornos emocionales de sus pacientes.

Este profesional sólo puede hacer ejercicio con receta médica.

*Para ir más allá*: Artículos L. 4332-1 y L. 4334-1 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de psicomotricista el profesional debe:

- Estar calificado profesionalmente
- han registrado su diploma.

#### Entrenamiento

Para ser reconocido como un profesionalmente calificado, debe:

- el grado estatal de psicomotricismo (bac-3);
- han sido psicomotriz durante al menos tres años en los diez años anteriores al 8 de mayo de 1988 y han completado una verificación de conocimientos dentro de los tres años posteriores a esa fecha.

**Diploma Estatal (DE) como psicomotricista**

La ED psitúreficacia está disponible por concurso a los candidatos que han sido capacitados para adquirir los conocimientos necesarios para ejercer la profesión, en el campo médico, en las humanidades y en las ciencias específicas y en el dominio de las técnicas. psicomotores.

Este curso de tres años se ofrece en una institución de educación superior y consta de:

- un primer año consistente en un curso de formación de 712 horas dividido en seis módulos de enseñanza teórica y un módulo de psicomotricidad práctica;
- un segundo año que consiste en 645 horas de formación divididas en cinco módulos teóricos, un módulo teórico-clínico y un módulo práctico;
- un tercer año que consiste en un curso de formación de 485 horas dividido en un módulo teórico, un módulo teórico-clínico y un módulo práctico de psicomotricidad.

El solicitante también debe realizar prácticas con un volumen total de 600 horas y una tesis final.

**Tenga en cuenta que**

El plan de estudios está incluido en el Programa I del decreto del 7 de abril de 1998 sobre estudios preparatorios para el diploma estatal de psicomotricismo.

El ED psicomotriz es emitido por el prefecto regional a los candidatos que han aprobado con éxito el examen programado al final de la capacitación. Este examen incluye una libertad condicional laboral que dura entre 45 minutos y una hora y la defensa de su informe durante 45 minutos. Una vez obtenido el diploma, el profesional está obligado a registrarse en la Agencia Regional de Salud (ARS) (ver infra "5 grados). a. Obligación de registrar el diploma").

**Es bueno saber**

Los solicitantes que posean uno de los diplomas enumerados en el[Artículo 25](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C3A25728E791E25F8A93F957B0465010.tplgfr33s_1?idArticle=LEGIARTI000027763599&cidTexte=LEGITEXT000005625897&dateTexte=20180316) y habiendo alcanzado un promedio general de diez sin una puntuación inferior a ocho en un examen escrito de un módulo teórico puede ser excusado de completar el primer año de formación. Si es necesario, los solicitantes deberán proporcionar una fotocopia de su diploma admitido como una renuncia desde el primer año de formación.

*Para ir más allá*: Artículos D. 4332-2 a R. 4332-8 del Código de Salud Pública; 7 de abril de 1998.

#### Costos asociados con la calificación

La formación para adquirir ED psicomotricista se paga y su costo varía dependiendo del curso previsto. Es aconsejable acercarse a las instituciones interesadas para obtener más información.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE), que esté legalmente establecido y participado en actividades psicomotriz, podrá realizar la misma actividad temporal y ocasional en Francia sin tener que registrarlo.

Cuando ni el acceso ni el ejercicio de la profesión estén regulados en el Estado miembro, el profesional deberá haber sido un psicomotricista durante al menos un año en los últimos diez años en uno o varios Estados miembros.

Una vez que el nacional cumpla con estas condiciones, tendrá que hacer una declaración antes de que se proporcione el primer servicio (ver más abajo "5 grados). b. Predeclaración para el nacional de la UE para un ejercicio temporal e informal (LPS)").

**Tenga en cuenta que**

Cuando existan diferencias sustanciales entre la formación recibida por el profesional y la necesaria para llevar a cabo la actividad de psicomotricente en Francia, el prefecto podrá decidir someterla a una prueba de aptitud.

Además, el nacional debe justificar la capacidad linguística necesaria para ejercer la profesión de psicomotricente en Francia.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Todo nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) legalmente establecido y que prosiga la actividad de psicomotricista podrá llevar a cabo la misma actividad de forma permanente en Francia.

Para ello, el interesado deberá ser titular de:

- un certificado de formación expedido por un Estado miembro que le permita llevar a cabo la actividad de psicomotricidad siempre que dicho Estado regule la actividad;
- cuando el Estado no regula el acceso o el ejercicio de la profesión de un documento de formación que haya sido preparado para el ejercicio de la profesión. También debe justificar una experiencia profesional de un año en los últimos diez años;
- un certificado de formación expedido por un tercer Estado y reconocido en un Estado miembro de la UE que le permita ejercer esa profesión y tres años de experiencia profesional en dicho Estado miembro.

Una vez que el nacional cumpla con estas condiciones, debe solicitar al prefecto regional permiso para ejercer (ver más abajo "5 grados). c. Solicitud de autorización para ejercer para el nacional de la UE para un ejercicio permanente (LE)).

**Tenga en cuenta que**

Cuando el examen de sus cualificaciones profesionales revele diferencias sustanciales entre la formación recibida por el nacional y la formación necesaria para llevar a cabo la actividad en Francia, el prefecto podrá decidir someterlo a una medida de compensación (véase infra "5.00. c. Bueno saber: medidas de compensación").

Además, al igual que en el contexto de la libre prestación de servicios, el nacional debe justificar las competencias linguísticas necesarias para el ejercicio de su profesión en Francia.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El psicomomotriciano, como profesional de la salud, está obligado a respetar la privacidad del paciente y el secreto profesional. Además, debe informar a sus pacientes de su estado de salud y, en particular, de:

- Los diferentes tratamientos que se ofrecen
- Los riesgos
- posibles alternativas en caso de denegación.

*Para ir más allá*: Artículos L. 1110-4 y L. 1111-2 del Código de Salud Pública.

4°. Seguros y sanciones
--------------------------------------------

**Seguro**

El psicomotriz como profesional de la salud está obligado a obtener un seguro de responsabilidad civil profesional por los riesgos incurridos durante el transcurso de su actividad.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

**Sanciones penales**

El acto de actuar como psicomomotriz sin ser calificado profesionalmente se considera una usurpación de valores y se castiga con un año de prisión y una multa de 15.000 euros.

*Para ir más allá*: Artículo L. 4334-2 del Código de Salud Pública; Artículo 433-17 del Código Penal.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Obligación de registrar el diploma

**Autoridad competente**

El solicitante debe solicitar el registro al LRA.

**Documentos de apoyo**

El solicitante debe proporcionar la siguiente información, considerada validada y certificada por la organización que emitió el diploma o título de formación:

- El estado civil del titular del diploma y todos los datos para identificar al solicitante;
- El nombre y la dirección de la institución que impartió la capacitación;
- el título de la formación.

**Resultado del procedimiento**

Después de comprobar las exposiciones, el LRA registra el diploma.

**Tenga en cuenta que**

El registro solo es posible para un departamento. Sin embargo, si el profesional desea ejercer en varios departamentos, el solicitante será incluido en la lista del departamento en el que se encuentra el lugar principal de su actividad.

**Costo**

Gratis.

*Para ir más allá*: Artículos L. 4333-1 y D. 4333-1 del Código de Salud Pública.

### b. Predeclaración del nacional de la UE para el ejercicio temporal y casual (LPS)

**Autoridad competente**

El nacional debe solicitar al prefecto de la región en la que desee ejercer antes de su primer servicio de entrega.

**Documentos de apoyo**

La solicitud debe incluir los siguientes documentos, si los hubiere, con su traducción al francés:

- el formulario de notificación de prestación de servicios adjunto al calendario de la orden del 8 de diciembre de 2017 sobre la declaración previa de prestación de servicios para asesores genéticos, físicos médicos y preparadores de farmacias y farmacias farmacia hospitalaria, así como para las ocupaciones del Libro III de la Parte IV del Código de Salud Pública;
- Una copia de su identificación válida
- Una copia de su título de formación que le permite llevar a cabo la actividad de psicomotricismo en su estado de establecimiento;
- un certificado de menos de tres meses que certifique que está legalmente establecido en un Estado miembro y no incurre en ninguna prohibición de ejercer;
- cuando ni el acceso a la actividad ni su ejercicio estén regulados en el Estado miembro, ninguna documentación que justifique que el profesional haya sido un psicomotricista durante un año en los últimos diez años;
- cuando la designación de formación haya sido establecida por un tercer Estado y reconocida por un Estado miembro:- Reconocimiento del título de formación establecido por el Estado miembro,
  - cualquier evidencia de que el profesional ha estado trabajando como psicomomotriz durante al menos tres años,
  - en caso de renovación, una copia de la declaración anterior y la primera declaración.

**Retrasos y procedimientos**

El prefecto informa al solicitante en el plazo de un mes:

- que puede empezar a prestar servicios sin comprobar sus cualificaciones profesionales;
- cuando existe una diferencia sustancial entre la formación recibida por el solicitante y la requerida en Francia para llevar a cabo la actividad de psicomotricismo, que somete a una prueba de aptitud para demostrar que es el propietario de todos los Conocimientos necesarios para la práctica;
- que no puede comenzar la prestación de servicios.

En ausencia de una respuesta del prefecto regional más allá de un período de un mes, el profesional puede comenzar su prestación de servicio. El profesional está en una lista en particular y recibe un recibo y número de registro.

**Tenga en cuenta que**

La declaración debe renovarse cada año en las mismas condiciones.

*Para ir más allá*: Artículos R. 4332-12 y R. 4331-12 a R. 4331-15 del Código de Salud Pública; 8 de diciembre de 2017 antes mencionado.

### c. Solicitud de autorización para ejercer para el nacional de la UE para un ejercicio permanente (LE)

**Autoridad competente**

El nacional debe enviar su solicitud en dos copias por carta recomendada con notificación de recepción al prefecto de la región en la que desea ejercer.

**Documentos de apoyo**

Su solicitud debe incluir:

- el[Forma](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C441904E50DA962D4EC05BCFE7BC0E2B.tplgfr31s_1?idArticle=LEGIARTI000021936246&cidTexte=LEGITEXT000021936243&dateTexte=20180319) Solicitud de autorización de ejercicio cumplimentada y firmada;
- Una fotocopia de un documento de identidad válido
- Una copia de su título de formación que le permite llevar a cabo la actividad de psicomotricismo y, si es necesario, una fotocopia de diplomas adicionales;
- todos los documentos para justificar su formación continua y su experiencia profesional adquirida en un Estado miembro;
- una declaración del Estado miembro en la que se indique que el nacional no está sujeto a ninguna sanción;
- Una copia de todos sus certificados mencionando el nivel de formación recibido y los detalles de las horas y el volumen de las enseñanzas seguidas;
- cuando ni el acceso a la formación ni su ejercicio estén regulados en el Estado miembro, ninguna documentación que justifique que ha sido un psicomotricista durante un año en los últimos diez años;
- cuando el certificado de formación haya sido expedido por un tercer Estado pero reconocido en un Estado miembro, el reconocimiento del título de formación por parte del Estado miembro.

**Procedimiento**

El prefecto regional reconoce haber recibido la solicitud en el plazo de un mes a partir de su recepción. Si no se hace respuesta en un plazo de cuatro meses, la solicitud se considera rechazada.

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la formación recibida por el profesional y la necesaria para llevar a cabo la actividad de psicomotricente en Francia, el prefecto podrá exigir que se someta a una prueba de aptitud o a un curso de adaptación. La prueba de aptitud toma la forma de uno de cada veinte exámenes calificados. El curso de alojamiento debe llevarse a cabo en un centro de salud público o privado bajo la responsabilidad de un profesional cualificado que haya estado trabajando como psicomotricista durante al menos tres años.

*Para ir más allá*: Artículo L. 4332-4 del Código de Salud Pública; decreto de 30 de marzo de 2010 por el que se establece la organización de la prueba de aptitud y el curso de adaptación para la práctica en Francia de las profesiones de psicomotricia, logopeda, ortopedista, audioprotésico, optician-lunetier por Estados miembros de la Unión Europea o partes en el Acuerdo sobre el Espacio Económico Europeo.

*Para ir más allá*: Artículos R. 4332-9 a R. 4332-11 del Código de Salud Pública; decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización pertinentes para el examen de las solicitudes presentadas para la práctica en Francia de las profesiones de psicomotricista, logopeda, ortopedista, audiotésico y óptico-lunetier.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700, París, ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

