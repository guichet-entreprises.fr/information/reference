﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP259" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios agrícolas" -->
<!-- var(title)="Usuario de fitofarmacéuticos" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-agricolas" -->
<!-- var(title-short)="usuario-de-fitofarmaceuticos" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-agricolas/usuario-de-fitofarmaceuticos.html" -->
<!-- var(last-update)="2020-04-15 17:20:50" -->
<!-- var(url-name)="usuario-de-fitofarmaceuticos" -->
<!-- var(translation)="Auto" -->




Usuario de fitofarmacéuticos
============================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:50<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El usuario de fitofarmacéuticos es un profesional que utiliza fitofarmacéuticos durante su actividad profesional, incluidos operadores, técnicos, empleadores y autónomos, tanto en el la agricultura que en otros sectores.

*Para ir más allá*: Artículo R. 254-1 del Código Rural y Pesca Marina

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

Para utilizar o aplicar fitofarmacéuticos, el profesional deberá:

- Poseer un certificado individual "uso profesional de fitofarmacéuticos";
- obtener la aprobación si esta actividad es ejercitada por una persona jurídica (ver infra "5o. b. Solicitud de aprobación para la aplicación de fitofarmacéuticos");
- mantener un registro de los productos utilizados. Este registro debe mencionar:- La fecha de adquisición de los productos o la entrega del tratamiento,
  - Las cantidades de producto utilizadas,
  - Números de lote.

**Tenga en cuenta que**

Este registro debe conservarse durante cinco años.

*Para ir más allá*: Artículos L. 254-1 a L. 254-6 del Código Rural y Pesca Marina; Reglamento (CE) 1107/2009 del Parlamento Europeo y del Consejo, de 21 de octubre de 2009, relativo a la comercialización de fitofármacos.

#### Legislación nacional

#### Entrenamiento

Este certificado (llamado Certiphyto) para el "uso profesional de fitofarmacéuticos" se divide en tres categorías correspondientes a los siguientes sectores de actividad:

- El responsable de la toma de decisiones no subsificado en la empresa;
- El responsable de la toma de decisiones aprobado en la empresa;
- operador en el negocio.

**Certificado individual para la actividad del "operador"**

El certificado de práctica individual se expide al profesional que:

- se sometió a capacitación que incluyó una prueba escrita de una hora de verificación de conocimientos;
- superó con éxito una prueba de una hora y media, que consta de veinte preguntas sobre el programa de formación establecido en la Lista II de la Orden de 29 de agosto de 2016;
- posee uno de los diplomas en el calendario I lista de la orden del 29 de agosto de 2016, obtenida en los cinco años anteriores a la fecha de solicitud.

*Para ir más allá* : decreto de 29 de agosto de 2016 por el que se establecen y establecen las condiciones de obtención del certificado individual para la actividad "uso profesional de los fitofarmacéuticos" en la categoría "operador".

**Certificado individual para las actividades de "decisión involuntaria en la empresa" y "responsable de la toma de decisiones empresarial acreditada"**

Estos certificados se expiden al profesional que:

- (a) un curso de formación que incluyera una prueba escrita de una hora de verificación de conocimientos;- pasó con éxito una prueba de una hora y media, que consta de treinta preguntas sobre el programa de formación establecido en la Lista II de la orden del 29 de agosto de 2016;
- posee uno de los diplomas en la lista de la Lista I del mismo orden en los cinco años anteriores a la solicitud.

*Para ir más allá* : un auto de 29 de agosto de 2016 por el que se establecen y establecen los términos de la obtención del certificado individual para la actividad "uso profesional de los fitofarmacéuticos" en las categorías "acreditado interno decisor" y " toma de decisiones en una empresa sin sucitar."

**Tenga en cuenta que**

Estos certificados tienen una validez de cinco años, renovables en las mismas condiciones.

Una vez que el profesional cumple con estas condiciones, debe solicitar la emisión de este certificado (ver más abajo "5 grados). a. Solicitar un certificado de práctica individual").

### b. Nacionales de la UE: para el ejercicio temporal y ocasional (prestación gratuita de servicios (LPS))

Todo nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) titular de un certificado expedido por un Estado miembro y establecido legalmente y que participe en una actividad relacionada con el uso de productos fitofarmacéuticos pueden llevar a cabo la misma actividad de forma temporal y ocasional en Francia.

Para ello, debe, antes de su primera prestación de servicios, hacer una declaración al Director Regional de Alimentación, Agricultura y Silvicultura del lugar donde se presta el primer servicio (véase más adelante "5o). c. Predeclaración del nacional para un ejercicio temporal e informal (LPS)."

*Para ir más allá*: Artículo R. 254-9 del Código Rural y Pesca Marina.

### c. Nacionales de la UE: para un ejercicio permanente (establecimiento libre (LE))

Cualquier nacional de un Estado miembro de la UE o del EEE legalmente establecido que participe en una actividad relacionada con el uso de fitoproductos podrá llevar a cabo la misma actividad de forma permanente en Francia.

Mientras el nacional posea un certificado individual de ejercicio expedido por un Estado miembro, se considerará que posee el certificado requerido para el ejercicio en Francia de esta actividad.

*Para ir más allá* II del artículo R. 254-9 del Código Rural y de la Pesca Marina.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

**Obligaciones sobre el uso de fitofarmacéuticos**

El profesional que utiliza fitofarmacéuticos está sujeto al cumplimiento de la normativa relativa al uso de estos productos.

Como tal, debe respetar:

- el plazo entre la aplicación de los productos y la cosecha;
- La velocidad máxima del viento más allá de la cual no puede utilizar estos productos;
- para la aplicación o vaciado de efluentes fitofarmacéuticos.

Además, todo profesional que utilice fitofarmacéuticos está obligado a seguir el plan nacional en el que se establecen los objetivos y medidas adoptadas para reducir los efectos del uso de estos productos en la salud humana y el medio ambiente.

*Para ir más allá*: Artículo L. 253-6 del Código Rural y Pesca Marina; Orden de 4 de mayo de 2017 relativa a la comercialización y utilización de fitofarmacéuticos y sus adyuvantes contemplados en el artículo L. 253-1 del Código Rural y de la Pesca Marina.

4°. Seguros y sanciones
--------------------------------------------

**Seguro**

El profesional está obligado, para obtener su aprobación, a obtener un seguro que cubra su responsabilidad civil profesional.

*Para ir más allá*: Artículo L. 254-2 del Código Rural y Pesca Marina.

**Sanciones**

El profesional se enfrenta a una pena de seis meses de prisión y una multa de 150.000 euros si hace uso de fitofarmacéuticos sin poseer la certificación o el certificado individual de ejercicio.

Además, la Agencia Nacional de Alimentación, Medio Ambiente y Seguridad Laboral puede prohibir o regular el uso de fitofármacos en áreas protegidas, incluidas las zonas abiertas al público.

**Tenga en cuenta que**

Las personas públicas no pueden utilizar los productos para eliminar las plagas para el mantenimiento de los espacios públicos (espacios verdes, escuelas), excepto las que son necesarias para combatir un grave peligro para la salud. y aquellos que no representan ninguna amenaza para el medio ambiente o la salud humana.

*Para ir más allá*: Artículo L. 253-7 del Código Rural y Pesca Marina.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar un certificado de práctica individual

**Autoridad competente**

La Dirección Regional de Alimentación, Agricultura y Silvicultura (DRAAF) del lugar de residencia del solicitante o, en su caso, del lugar de la sede de la organización donde se impartió la formación, es competente para expedir el certificado.

**Documentos de apoyo**

El profesional debe crear una cuenta en línea en el sitio[service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) para acceder al servicio de aplicación de certificados.

Su solicitud debe incluir, dependiendo del caso:

- Una prueba de entrenamiento de seguimiento y, si es necesario, un pase a la prueba;
- copia de su diploma o título.

**Tiempo y procedimiento**

El certificado se expide al profesional en un plazo de dos meses a partir de la presentación de su solicitud. En ausencia de la emisión del certificado después de este período, y a menos que la notificación de denegación, los documentos justificativos anteriores valen la pena un certificado individual de hasta dos meses.

*Para ir más allá*: Artículos R. 254-11 y R. 254-12 del Código de Pesca Rural y Marina.

### b. Solicitud de aprobación para fitofarmacéuticos

**Autoridad competente**

El prefecto de la región en la que se encuentra la sede central de la empresa es competente para expedir la acreditación.

**Documentos de apoyo**

Su solicitud debe incluir el[Formulario de solicitud](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14581.do) completado y firmado, así como los siguientes documentos:

- Un certificado de póliza de seguro de responsabilidad civil profesional;
- un certificado expedido por una organización externa que justifica que realiza esta actividad de acuerdo con las condiciones que garantizan la protección de la salud pública y del medio ambiente. Esta certificación de la empresa es realizada por un organismo certificador tras una auditoría sobre la base de un repositorio que figura en el anexo del decreto de 25 de noviembre de 2011 relativo al repositorio de certificación;
- copia de un contrato con una organización externa con un contrato que proporciona el seguimiento necesario para mantener la certificación.

**Tenga en cuenta que**

Los microdistribuidores deben presentar los siguientes documentos a THE DRAAF:

- un certificado de seguro de responsabilidad civil profesional;
- prueba de la celebración del certificado Certiphyto por parte de todo el personal que desempeña funciones de supervisión, ventas o consultoría;
- un documento que justifique que está sujeto al sistema fiscal de microempresas.

**Tiempo y procedimiento**

El profesional que inicie su actividad deberá solicitar una aprobación provisional que se concederá por un período de seis meses no renovable. A continuación, podrá solicitar la aprobación final.

El silencio guardado por el prefecto de la región más allá de un período de dos meses, vale la pena la decisión de rechazo.

Además, el profesional está obligado, cada año, a proporcionar al prefecto una copia del certificado de suscripción a una póliza de seguro.

*Para ir más allá*: Artículos R. 254-15-1 a R. 254-17 del Código Rural y Pesca Marina.

### c. Predeclaración del nacional para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe solicitar por cualquier medio al DRAAF el lugar donde reciba los servicios por primera vez.

**Documentos de apoyo**

Su solicitud deberá incluir el certificado individual de ejercicio expedido por el Estado miembro y, en su caso, acompañado de su traducción acreditada al francés.

**Tenga en cuenta que**

Esta declaración debe renovarse cada año y en caso de cambio de situación laboral.

*Para ir más allá* III del artículo R. 254-9 del Código Rural y de la Pesca Marina.

### d. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

