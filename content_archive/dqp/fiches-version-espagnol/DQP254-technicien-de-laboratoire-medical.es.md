﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP254" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Técnico de laboratorio médico" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="tecnico-de-laboratorio-medico" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/tecnico-de-laboratorio-medico.html" -->
<!-- var(last-update)="2020-04-15 17:22:13" -->
<!-- var(url-name)="tecnico-de-laboratorio-medico" -->
<!-- var(translation)="Auto" -->


Técnico de laboratorio médico
=============================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:22:13<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El técnico de laboratorio médico es un asistente médico cuya actividad consiste en llevar a cabo, con la ayuda de un biólogo médico y/o un médico especialista, un examen de biología médica patológica, anatomía y citología (análisis de células biológicas y fluidos).

Además, el técnico de laboratorio médico puede ser requerido para participar en programas de enseñanza, investigación y educación terapéutica para el paciente.

*Para ir más allá*: Artículo L. 4352-1 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para llevar a cabo la actividad de técnico de laboratorio médico, el profesional debe ser el titular de:

- un diploma estatal (DE) como técnico de laboratorio médico;
- un título de formación cuyo programa de formación es equivalente al del ED y cuya lista se establece en el[Artículo 1](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=68603435EE703FB09BF93D2EF66DB326.tplgfr41s_1?idArticle=LEGIARTI000036674386&cidTexte=LEGITEXT000006072818&dateTexte=20180524) del orden de 4 de noviembre de 1976 por el que se establece la lista de títulos o diplomas exigidos a las personas empleadas como técnicos en un laboratorio de biología médica.

Profesionales que:

- justificar el ejercicio de tareas técnicas en un laboratorio médico durante al menos seis meses antes del 8 de noviembre de 1976;
- se desempeñaban como técnico de laboratorio médico en la fecha de promulgación de la Ley No 2013-442, de 30 de mayo de 2013, sin poseer un diploma o designación de formación para ejercer esta profesión.

El profesional está obligado a registrarse en la agencia regional de salud (ARS) en el lugar de su práctica profesional.

*Para ir más allá*: Artículos L. 4352-2 y los siguientes del Código de Salud Pública.

#### Entrenamiento

**Diploma de Técnico Estatal de Laboratorio Médico**

Para obtener este diploma estatal, el profesional debe haber completado un curso de tres años con enseñanza teórica, práctica y pasantías.

Este diploma es emitido por el prefecto regional a los candidatos que tienen:

- completaron los tres años de formación y llevaron a cabo las prácticas obligatorias;
- obtuvo un certificado de formación en gestos de emergencia y atención en el nivel 2;
- superó con éxito las pruebas de grado estatal:- una prueba de síntesis escrita de cuatro horas que cubre todo el programa de capacitación,
  - dos pruebas prácticas que duran tres horas cada una.

*Para ir más allá*: Artículos D. 4352-1 a D. 4352-6 del Código de Salud Pública; orden de 21 de agosto de 1996 relativa a los estudios preparatorios para el Diploma Estatal de Técnico de Laboratorio Médico.

**Certificado de capacidad específica para muestras de sangre**

El profesional que proactúe como técnico de laboratorio médico puede obtener un certificado de capacidad para tomar muestras de sangre, siempre y cuando justifique haber se someta a formación cuyo programa se fija en el[Anexo](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=190B672931637BDEAB273D9AACC931B4.tplgfr27s_2?idArticle=LEGIARTI000026318006&cidTexte=LEGITEXT000026318008&dateTexte=20180528) del siguiente orden, y aprobado con éxito:

- Una prueba teórica
- un curso de capacitación en gestión y atención de emergencias, y la realización de cuarenta muestras venosas o capilares durante un período de hasta tres meses;
- una prueba práctica de muestras en presencia de un jurado.

Para obtener este certificado, el solicitante presenta una solicitud ante el LRA de su lugar de residencia, que incluye:

- Una solicitud para inscribirse en el examen
- Una fotocopia de su dNI
- una copia de todos sus títulos o diplomas.

*Para ir más allá* : orden de 13 de marzo de 2006 por el que se establecen las condiciones para expedir el certificado de capacidad para tomar muestras de sangre para exámenes de biología médica.

#### Costos asociados con la calificación

La capacitación que conduce al Diploma de Técnico del Laboratorio Médico Estatal se paga y el costo varía dependiendo de la institución que proporciona las enseñanzas. Para más información, es aconsejable acercarse al establecimiento en cuestión.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido podrá realizar el trabajo de un técnico de laboratorio médico de forma temporal e informal misma actividad en Francia.

Cuando el Estado miembro no regule el acceso a la actividad o a su ejercicio, el nacional deberá justificar haber realizado esta actividad durante al menos un año en los últimos diez años.

Si cumplen estas condiciones, el interesado deberá hacer una declaración previa al prefecto regional pertinente antes de su primera prestación de servicios (véase más adelante " Hacer una declaración previa para los foros nacionales de la UE ejercicio temporal y casual (LPS)").

El nacional temporal y ocasional no está sujeto al requisito de registro del nacional francés.

Además, debe justificar la necesidad de contar con las habilidades de idiomas necesarias para ejercer la profesión de técnico de laboratorio médico en Francia.

*Para ir más allá*: Artículo L. 4352-7 del Código de Salud Pública.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento Libre (LE))

Cualquier nacional de un Estado miembro de la UE o del EEE legalmente establecido que practique como técnico de laboratorio médico podrá realizar la misma actividad en Francia con carácter temporal.

Para ello, el interesado deberá ser titular de:

- un certificado de formación expedido por un Estado miembro que regula el acceso a esta profesión y le permite llevar a cabo esta actividad;
- cuando el Estado no regule el acceso a la actividad o a su ejercicio, un documento de formación que certifique que se ha estado preparando para la práctica de esta ocupación, así como un certificado que certifique que ha estado involucrado en esta actividad durante al menos un año en el En la última década;
- un documento de formación que permita el ejercicio de esta actividad expedido por un tercer Estado y reconocido en un Estado miembro de la UE o del EEE, así como un certificado que certifique que ha llevado a cabo esta actividad durante tres años en dicho Estado miembro.

Cuando el examen de las cualificaciones profesionales del nacional muestre una diferencia sustancial entre su formación y la cualificación necesaria para llevar a cabo la actividad de técnico de laboratorio en Francia, el prefecto regional podrá decidir someterlo a una prueba de aptitud o a un curso de ajuste.

Además, al igual que en el contexto de la ZABE, el nacional debe justificar la necesidad de tener las competencias linguísticas necesarias para ejercer la profesión en Francia.

Tan pronto como el nacional cumpla estas condiciones, deberá solicitar una autorización para ejercer del prefecto regional pertinente (véase a continuación "Solicitud de autorización para ejercer para el nacional de la UE para un ejercicio permanente").

*Para ir más allá*: Artículos L. 4352-6 y L. 4352-8 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El técnico de laboratorio médico, como profesional de la salud, está obligado a cumplir con las disposiciones del Código de ética médica.

Como tal, se requiere para:

- Secreto profesional;
- Respeto a la privacidad del paciente
- ejercer de forma independiente;
- respetar los principios de moralidad, probidad y devoción.

*Para ir más allá*: Artículos R. 4127-1 y siguientes del Código de Salud Pública.

4°. Seguros y sanciones penales
----------------------------------------------------

**Seguro**

El técnico de laboratorio médico como profesional de la salud está obligado a tomar un seguro de responsabilidad civil profesional por los riesgos incurridos durante el transcurso de su actividad.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

**Sanciones**

El profesional se enfrenta a una pena de un año de prisión y una multa de 15.000 euros si ejerce:

- ilegalmente la profesión de técnico de laboratorio médico;
- sin derecho, la calidad de técnico de laboratorio de biología médica o un título o diploma que permita el ejercicio de esta actividad se castiga con las penas previstas para el delito de usurpación de título.

*Para ir más allá*: Artículos L. 4353-1 y L. 4353-2 del Código de Salud Pública.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Proceder a registrarse

**Autoridad competente**

El profesional debe presentar su solicitud a la agencia regional de salud en el lugar de su práctica profesional.

**Documentos de apoyo**

Su expediente debe incluir los siguientes documentos justificativos:

- Una copia de su identificación
- una copia de su título de formación o autorización para ejercer, así como información certificada proporcionada por la organización que emitió este título o certificación:- los datos del estado civil del titular del título de formación y toda la información para identificar al solicitante,
  - La redacción y dirección de la organización que impartió la capacitación,
  - el título de la formación recibida.

**Tiempo y procedimiento**

El ARS registra al profesional después de comprobar todos los documentos de apoyo.

**Costo**

Gratis.

**Tenga en cuenta que**

Los profesionales que se han graduado o calificado para la formación de menos de tres años pero no trabajan como técnicos de laboratorio deben registrarse en el LRA en su casa o en cualquier otra LSA.

*Para ir más allá*: Artículos L. 4352-4 y D. 4354-1 a R. 4354-11 del Código de Salud Pública.

### b. Hacer una declaración previa para el nacional de la UE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe presentar una solicitud al prefecto de la región en la que desee ejercerla antes de su primera prestación de servicios.

**Documentos de apoyo**

La solicitud debe incluir los siguientes documentos, si los hubiere, con su traducción al francés:

- el formulario de notificación de prestación de servicios adjunto al calendario de la orden del 8 de diciembre de 2017 sobre la declaración previa de prestación de servicios para asesores genéticos, físicos médicos y preparadores de farmacias y farmacias farmacia hospitalaria, así como para las ocupaciones del Libro III de la Parte IV del Código de Salud Pública;
- Una copia de su identificación válida
- Una copia de su certificado de formación que le permite llevar a cabo la actividad de técnico de laboratorio médico en su estado de establecimiento;
- un certificado de menos de tres meses que certifique que está legalmente establecido en un Estado miembro y no incurre en ninguna prohibición de ejercer;
- Cuando ni el acceso a la actividad ni su ejercicio estén regulados en el Estado miembro, ninguna documentación que justifique el trabajo del profesional como técnico de laboratorio médico durante un año en los últimos diez años;
- cuando la designación de formación haya sido establecida por un tercer Estado y reconocida por un Estado miembro:- Reconocimiento del título de formación establecido por el Estado miembro;
  - cualquier evidencia que justifique el trabajo del profesional como técnico de laboratorio médico durante al menos tres años;
  - en caso de renovación, una copia de la declaración anterior y la primera declaración.

**Retrasos y procedimientos**

El prefecto informa al solicitante en el plazo de un mes:

- que puede empezar a prestar servicios sin comprobar sus cualificaciones profesionales;
- cuando exista una diferencia sustancial entre la formación recibida por el solicitante y la requerida en Francia para llevar a cabo la actividad de técnico de laboratorio médico, que someta a una prueba de aptitud para demostrar que es el propietario de la totalidad Los conocimientos necesarios para practicar
- que no puede comenzar la prestación de servicios.

La falta de respuesta por parte del prefecto de la región más allá de un período de un mes, el profesional puede comenzar su prestación de servicio. El profesional está en una lista en particular y recibe un recibo y número de registro.

**Tenga en cuenta que**

La declaración debe renovarse cada año en las mismas condiciones.

*Para ir más allá*: Artículos R. 4361-16; R. 4332-12 y R. 4331-12 a R. 4331-15 del Código de Salud Pública; 8 de diciembre de 2017 antes mencionado.

### c. Solicitud de autorización para ejercer para el nacional de la UE un ejercicio permanente

**Autoridad competente**

El nacional deberá presentar su solicitud en dos copias por carta recomendada con notificación de recepción a la secretaría de la comisión de autorización para ejercer.

**Documentos de apoyo**

Su solicitud deberá incluir los siguientes documentos, si los hubiere, con su traducción al francés, certificados:

- El formulario de solicitud de autorización de ejercicio, tal como se establece en el orden de 24 de marzo de 2010, citado a continuación;
- Una fotocopia de un documento de identidad válido
- Una copia del título de formación que permita el ejercicio de la actividad de técnico de laboratorio médico y, en su caso, de sus diplomas adicionales;
- cualquier prueba que justifique la formación continua y la experiencia profesional del nacional;
- Una declaración de menos de un año de edad y que certifique que no está sujeta a ninguna sanción;
- una copia de los certificados de las organizaciones que le expidieron su título de formación y que le declaran:- El nivel de formación
  - El detalle y el volumen por hora de las enseñanzas seguidas,
  - La duración y el contenido de las prácticas realizadas;
- cuando el Estado miembro no regule el acceso a la actividad o a su ejercicio, cualquier documento que justifique que ha participado en esta actividad durante al menos dos años en los últimos diez años.

**Tiempo y procedimiento**

El prefecto regional reconoce haber recibido la solicitud en el plazo de un mes a partir de la recepción y emite la autorización para ejercer previa asesoría del Comité de Técnicos de Laboratorio Médico.

A falta de una respuesta del prefecto en el plazo de cuatro meses a partir de la recepción del expediente, vale la pena aceptar su solicitud de autorización para ejercer.

*Para ir más allá*: Artículos R. 4352-7 a R. 4352-9 del Código de Salud Pública; decreto de 24 de marzo de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones competentes de autorización de ejercicio para el examen de las solicitudes presentadas para el ejercicio en Francia de la profesión de técnico de laboratorio médico.

**Bueno saber: medidas de compensación**

En caso de diferencias sustanciales entre la formación recibida por el profesional y la necesaria para el ejercicio en Francia de la actividad de técnico de laboratorio médico, el prefecto regional podrá decidir someterla a una medida de compensación.

La prueba de aptitud toma la forma de preguntas escritas u orales sobre temas no enseñados durante su formación o no adquiridos durante su experiencia profesional.

El curso de adaptación se lleva a cabo en un centro de salud público o privado aprobado por la Agencia Regional de Salud (ARS) y bajo la responsabilidad pedagógica de un profesional que ha estado trabajando como técnico de laboratorio médico durante al menos tres Años.

*Para ir más allá* : decreto de 24 de marzo de 2010 por el que se establece la organización de la prueba de aptitud y el curso de adaptación para la práctica de técnicos de laboratorio médico en Francia por nacionales de los Estados miembros de la Unión Europea o parte en Acuerdo sobre el Espacio Económico Europeo.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

