﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP042" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Audiólogo" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="audiologo" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/audiologo.html" -->
<!-- var(last-update)="2020-04-15 17:21:15" -->
<!-- var(url-name)="audiologo" -->
<!-- var(translation)="Auto" -->


Audiólogo
=========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:15<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El audiopréstista es un profesional de la salud cuya actividad es proporcionar un sistema de audífonos a personas con fallos auditivos. Este servicio sólo se puede realizar después de un examen otológico y audiométrico tonal y vocal y con receta médica de un otorrinolaringólogo.

*Para ir más allá*: Artículo L. 4361-1 del Código de Salud Pública.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El profesional que desee trabajar como audiopréstista en Francia debe:

- profesionalmente calificados (ver infra "2.2). a. Formación");
- han sido registrados en la agencia regional de salud (véase infra "5o. a. Procedimiento de registro");
- operan en un espacio construido específicamente.

*Para ir más allá*: Artículos L. 4361-2 y L. 4361-6 del Código de Salud Pública.

#### Entrenamiento

Para ser reconocida como una persona profesionalmente calificada, la persona debe ser titular de:

- un título estatal como audioprotésico;
- cualquier otro diploma, certificado o título que permita la práctica de la medicina en Francia.

**Diploma Estatal de Audioprostista**

Este diploma está disponible por concurso, para candidatos con licenciatura o equivalencia, y haber aprobado todos los exámenes de ingreso.

La capacitación de tres años incluye:

- enseñanzas teóricas;
- enseñanzas dirigidas;
- Lecciones prácticas
- realización de prácticas de audiología en clústeres hospitalarios de otoringología o en instituciones acreditadas por el consejo de formación e investigación;
- para una tesis de investigación en el último año de formación.

Varias instituciones están acreditadas para impartir esta formación:

- el[Universidad](http://www.univ-tlse3.fr/) Toulouse;
- la unidad de formación en ciencias médicas de la[université de Bordeaux](http://www.univ-bordeauxsegalen.fr/) ;
- La Unidad de Formación e Investigación en Ciencias Farmacéuticas y Biológicas (UFR) de la[université de Montpellier](http://www.umontpellier.fr/) ;
- UFR de Ciencias Farmacéuticas y Biológicas[université de Lorraine](http://pharma.univ-lorraine.fr/) ;
- Instituto de Ciencia y Tecnología de Rehabilitación ([ISTR](https://istr.univ-lyon1.fr/)) en la Universidad de Lyon;
- el Centro Estatal de Preparación del Diploma para el Audioprotista ([Cpda](http://cpda.cnam.fr/)) en el Conservatorio Nacional de Artes y Oficios de París.

**Tenga en cuenta que**

El acceso a esta formación es objeto de un numerus clausus.

*Para ir más allá*: Artículos D. 636-1 a D. 636-17 del Código de Educación; Secciones L. 4361-1 y D. 4361-1 del Código de Salud Pública.

**Otros diplomas**

Los profesionales también pueden llevar a cabo la actividad de audiopréstista, sin grabarlos:

- titulares de un certificado de estudios técnicos en acústica en el dispositivo de audífonos, certificado expedido por escuelas médicas, farmacéuticas o mixtas;
- justificando la experiencia profesional como audioprésista al menos cinco años antes del 4 de enero de 1967 tras haber sido autorizado por una comisión nacional de cualificación;
- haber aprobado un examen profesional probatorio si no han recibido la autorización anterior o tienen experiencia laboral de menos de cinco años antes del 4 de enero de 1967.

*Para ir más allá*: Artículos L. 4361-2 y L. 4361-5 del Código de Salud Pública.

#### Costos asociados con la calificación

El coste de la formación que conduce a la profesión de audiopréstista varía según el curso previsto. Para obtener más información, es importante acercarse a las instituciones involucradas.

### b. Nacionales de la UE: para el ejercicio temporal e informal (Entrega gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido podrá realizar la misma actividad temporal y ocasional en Francia, sin llevar a cabo el procedimiento de registro.

Para ello, debe hacer una declaración (véase infra "5 grados. b. Solicitud de declaración de prestación de servicios").

Cuando la profesión no esté regulada en el contexto de la actividad o formación en el país en el que el profesional está legalmente establecido, debe haber llevado a cabo esta actividad durante al menos un año en los diez años anteriores a uno o más Estados miembros de la UE.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la autoridad competente podrá exigir que el interesado se someta a una prueba de aptitud.

*Para ir más allá*: Artículo L. 4361-9 del Código de Salud Pública.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Para llevar a cabo la actividad de audiopréstetista en Francia de forma permanente, la UE o el nacional del EEE deben cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba: "2. Cualificaciones profesionales");
- licencia para ejercer (ver infra "5 grados). c. Solicitud de autorización para ejercer una LE").

**Bueno saber: medidas de compensación**

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la autoridad competente podrá exigir que la persona se someta a un curso de adaptación o a una prueba de aptitud.

*Para ir más allá*: Artículo L. 4361-4 del Código de Salud Pública;[Detenido](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000022050408&dateTexte=20180221) 30 de marzo de 2010 en el que se establece la organización de la prueba de aptitud y el curso de adaptación para la práctica en Francia de las profesiones de psicomotricismo, logopeda, ortopedista, audioprotésico, optician-lunetier por parte de nacionales de la Estados miembros de la UE o partes en el Acuerdo EEE.

3°. Reglas éticas, ética
---------------------------------

El audiopréstista está sujeto al respeto durante el ejercicio de su actividad:

- Secreto profesional
- Código de ética europeo para la profesión de los audioprotistas, incluidas las siguientes normas éticas:- ayudar al paciente y actuar sólo en su interés,
  - Cumplir con los requisitos de publicidad,
  - actuar con respeto a su profesión.

*Para ir más allá* En:[Código de ética](http://www.unsaf.org/site/audioprothesiste/deontologie.html) está disponible en el sitio web de unsaf (Unión Nacional de Audioprostistas).

4°. Sanciones penales y seguros
----------------------------------------------------

**Sanciones penales**

Un profesional que practica ilegalmente como audiopréstista se enfrenta a una pena de un año de prisión y una multa de 15.000 euros, así como a una de las siguientes sanciones adicionales:

- Publicar o difundir la condena
- confiscación de lo que condujo a la comisión del delito;
- una prohibición de cinco años sobre la práctica de la profesión o permanentemente.

*Para ir más allá*: Artículo L. 4363-1 a L. 4363-3 del Código de Salud Pública.

**Seguro**

Como profesional de la salud dedicado a la prevención, diagnóstico o atención, el audioprotésico debe, si ejerce a nivel liberal, contratar un seguro de responsabilidad profesional.

Si ejerce como empleado, corresponde al empleador obtener dicho seguro para sus empleados por los actos realizados durante esta actividad.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Procedimiento de registro

Antes de llevar a cabo su actividad, el profesional debe proceder a su inscripción.

**Autoridad competente**

La agencia regional de salud del lugar de práctica del profesional es competente para verificar toda la información proporcionada por el profesional para el registro.

**Documentos de apoyo**

Las organizaciones que hayan emitido la designación de formación profesional deberán proporcionar a la autoridad pertinente la siguiente información:

- Estado civil y toda la información sobre la identidad del profesional;
- La dirección y redacción de la institución que proporcionó la formación;
- el título de la formación.

**Costo**

Gratis

*Para ir más allá*: Artículos L. 4361-2-2, D. 4365-1 y D. 4333-1 del Código de Salud Pública.

### b. Solicitud de presentación de informes para la prestación de servicios

**Autoridad competente**

La solicitud de declaración debe enviarse antes de que se proporcione el primer servicio al prefecto de la zona de establecimiento profesional.

**Documentos de apoyo**

La solicitud de declaración debe incluir lo siguiente, si lo hubiere, traducido al francés:

- el[Forma](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7F200D263B3CF713E481471475EE0CFD.tplgfr26s_3?cidTexte=JORFTEXT000036171877) Declaración completada y firmada;
- identificación válida o cualquier documento que acredite la nacionalidad del solicitante;
- Copia del título de formación para llevar a cabo la actividad de audioprotésico en el Estado miembro;
- un certificado de menos de tres meses que certifique que el solicitante está legalmente establecido en el Estado de la UE o en el EEE y que no está prohibido ejercer temporal o permanentemente;
- cuando el título de formación haya sido adquirido en un tercer Estado y reconocido en un Estado miembro:- Reconocimiento del título de formación por parte del Estado miembro,
  - prueba de que la demandante ha ejercido como audiopréstista en ese Estado durante al menos tres años;
- Si es así, una copia de su declaración anterior y la primera hecha;
- una declaración que justifique que el solicitante tiene las habilidades de idiomas necesarias para ejercer su profesión en Francia.

**Resultado del procedimiento**

En el plazo de un mes a partir de la recepción del expediente del solicitante, el prefecto le informa:

- que puede iniciar la prestación de servicios sin verificación previa de sus cualificaciones profesionales;
- que tras la verificación de sus cualificaciones profesionales, el profesional debe someterse a una prueba de aptitud;
- que no puede comenzar la prestación de servicios.

El prefecto registra al profesional en una lista en particular y le envía un recibo indicando un número de registro. En ausencia de una respuesta del prefecto en el plazo de un mes, la prestación del servicio puede comenzar.

**Tenga en cuenta que**

La declaración es renovable cada año.

*Para ir más allá*: Artículos L. 4361-9, R. 4361-16 y R. 4331-12 a R. 4331-15 del Código de Salud Pública; 8 de diciembre de 2017 orden sobre la declaración previa de prestación de servicios para asesores genéticos, médicos y preparadores de farmacias y farmacias hospitalarias, así como para ocupaciones en el Libro III de Parte IV del Código de Salud Pública.

### c. Solicitud de autorización para ejercer un LE

**Autoridad competente**

La solicitud deberá enviarse en doble copia por carta recomendada con preaviso de recepción a la secretaría del comité de audioproprotistas compuesto por profesionales.

**Documentos de apoyo**

La solicitud del solicitante debe incluir:

- el[Forma](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=DDB2C307150C1EBD51D563B436CD57D5.tplgfr40s_3?idArticle=LEGIARTI000021936246&cidTexte=LEGITEXT000021936243&dateTexte=20180221)Solicitud completada y firmada;
- Una copia de su identificación válida
- Una copia de su título de formación que permita el ejercicio de la profesión en su país de obtención y, en su caso, sus diplomas adicionales, así como un certificado que mencione el nivel y el detalle de la formación;
- cualquier prueba de experiencia profesional, formación continua en un Estado miembro de la UE o en el EEE;
- cualquier documento que justifique que no está sujeto a ninguna sanción;
- cuando el Estado miembro no regule el acceso a la profesión o a su ejercicio, una prueba de que el profesional ha sido un profesional de audio durante un año en los últimos diez años;
- cuando el solicitante posea un certificado de formación expedido por un tercer Estado y reconocido por un Estado miembro, el reconocimiento del título de formación por el Estado miembro.

**Resultado del procedimiento**

El prefecto de la región del lugar de establecimiento del profesional reconoce la recepción del expediente en el plazo de un mes a partir de su recepción y emite la autorización de ejercicio previa notificación de la citada comisión.

**Tenga en cuenta que**

El silencio guardado por el prefecto después de cuatro meses desde la recepción del expediente completo vale la pena rechazar la solicitud.

*Para ir más allá*: Artículos R. 4361-13 a R. 4361-15 del Código de Salud Pública; decreto de 25 de febrero de 2010 por el que se establece la composición del expediente que se facilitará a las comisiones de autorización pertinentes para el examen de las solicitudes presentadas para la práctica en Francia de las profesiones de psicomotricista, logopeda, ortopedista, audiotésico y óptico-lunetier.

### d. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

