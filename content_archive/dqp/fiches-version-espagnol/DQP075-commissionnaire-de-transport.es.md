﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP075" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Transporte" -->
<!-- var(title)="Comisario de Transportes" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="transporte" -->
<!-- var(title-short)="comisario-de-transportes" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/transporte/comisario-de-transportes.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="comisario-de-transportes" -->
<!-- var(translation)="Auto" -->


Comisario de Transportes
========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El transportista es un profesional cuya misión es organizar el movimiento de mercancías de un lugar a otro, de acuerdo con los modos y medios de su elección.

Actúa como intermediario ya que celebra un contrato de comisión de transporte con su cliente, y uno o más contratos de transporte con transportistas que fleta en su nombre.

La Sección R. 1411-1 del Código de Transporte establece que las diversas actividades del Comisionado de Transporte son:

- Operaciones de agrupación: envío de mercancías desde varios remitentes o a múltiples destinatarios, ensamblados y formados en un único lote para el transporte;
- Operaciones chárter: los envíos se confían sin agrupación previa a transportistas públicos;
- Operaciones de la oficina de la ciudad: el comisionado se encarga de los paquetes o envíos al por menor y los entrega por separado a los transportistas públicos u otros comisarios de transporte;
- Operaciones de organización de transporte: el comisionado se encarga de las mercancías hacia y desde el territorio nacional, y proporciona el transporte por uno o más transportistas públicos a través de cualquier ruta.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

La profesión de comisario de transporte está reservada a cualquier persona con certificado de inscripción en el registro de transporte por carretera.

Para ser registrada, la persona debe estar en posesión de un certificado de capacidad profesional y debe cumplir con las condiciones de honor.

*Para ir más allá* : Artículo R. 1422-1 del Código de Transporte.

#### Entrenamiento

El titular de la región expide el certificado de capacidad profesional, necesario para la solicitud de registro, al interesado que justifique:

- tener un diploma en educación superior, habiendo recibido formación legal, económica, contable, comercial o técnica, o un diploma de educación técnica, habiendo recibido formación en actividades de transporte;
- haber pasado las pruebas de un[revisión escrita](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=72888CDED0E6534F1C718717EE40F003.tplgfr27s_3?cidTexte=JORFTEXT000031679969&dateTexte=20151224) en un centro de formación acreditado
- reconocimiento de cualificaciones profesionales adquiridas en un Estado miembro de la Unión Europea (UE) o parte en el acuerdo del Espacio Económico Europeo (EEE).

**Tenga en cuenta que**

La capacidad profesional necesaria para ejercer como comisario de transporte se adquiere siempre y cuando la persona haya recibido formación en gestión empresarial de al menos 200 horas de formación o cuando haya completado una pasantía de 80 horas. garantizar un nivel suficiente de ley aplicado al transporte, la economía del transporte y la comisión de transporte.

*Para ir más allá* : Artículo R. 1422-4 del Código de Transporte; Artículos 7 y 8 de la Orden de 21 de diciembre de 2015 relativa a la expedición del certificado de capacidad profesional que permite el ejercicio de la profesión de transportista.

#### Costos asociados con la calificación

La formación que conduce a esta ocupación se paga y el costo varía dependiendo de la organización elegida. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

No existe ningún reglamento para un nacional de la UE o del EEE que desee ejercer como comisionado de transporte en Francia, ya sea de forma temporal o ocasional.

Por lo tanto, sólo las medidas adoptadas para el libre establecimiento de nacionales de la UE o del EEE (véase más adelante "5. Los pasos y procedimientos para el reconocimiento de la cualificación") encontrarán que se aplicarán.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Para llevar a cabo la actividad de transporte en Francia de forma permanente, la UE o el nacional del EEE deben cumplir una de las siguientes condiciones:

- poseer un certificado de competencia o certificado de formación requerido para el ejercicio de la actividad de transportistaense en un Estado de la UE o del EEE cuando dicho Estado regule el acceso o el ejercicio de esta actividad en su territorio;
- han trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro estado que no regula la formación o el ejercicio de la profesión
- tener un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona ha sido comisionaria de transporte en el Estado durante tres años, equivalencia admitida.

Una vez que cumpla una de estas condiciones, el nacional podrá solicitar el reconocimiento de sus cualificaciones profesionales por parte del prefecto de la región en la que desea ejercer su profesión (véase infra "5o. a. Solicitar el reconocimiento de sus cualificaciones profesionales para la UE o el EEE nacional para la actividad permanente (LE)).

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, el prefecto territorialmente competente podrá exigir que el interesado se someta a medidas de indemnización (véase infra "5" . a. Bueno saber: medidas de compensación").

*Para ir más allá* : Artículos R. 1422-11 y R. 1422-15 del Código de Transporte.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El comisionado de transportes debe cumplir las condiciones de honor y no debe, entre otras cosas:

- han sido condenados por un tribunal francés, registrado en el boletín 2 del historial penal, o por un tribunal extranjero y registrado en un documento equivalente, y pronunciando una prohibición de ejercer una profesión comercial o Industrial;
- conducir sin licencia
- Resort al trabajo oculto
- para transportar las llamadas mercancías peligrosas.

*Para ir más allá* : Artículo R. 1422-7 del Código de Transporte.

4°. Seguro
-------------------------------

El Comisario de Transportes Liberal debe conllevar un seguro de responsabilidad civil profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar el reconocimiento de cualificaciones profesionales para los nacionales de la UE o del EEE para la actividad permanente (LE)

**Autoridad competente**

El prefecto regional es competente para expedir el certificado de capacidad profesional que justifica el reconocimiento de las cualificaciones profesionales del nacional.

**Documentos de apoyo**

La solicitud de certificación se realiza enviando un archivo al prefecto, incluyendo los siguientes documentos:

- El formulario [Cerfa 11414*05](https://www.formulaires.modernisation.gouv.fr/gf/Cerfa_11414.do) ;
- Una identificación válida
- una prueba de residencia, para la persona que tiene su residencia habitual en Francia;
- Una copia del certificado de competencia o certificado de formación expedido por un Estado de la UE o del EEE que regula la profesión de comisario de transporte;
- En caso afirmativo, cualquier documento que justifique el ejercicio jurídico del solicitante como operador de mercancías durante un año en los últimos diez años en un Estado de la UE o del EEE que no regule el acceso a la profesión o Entrenamiento
- en su caso, cualquier documento que justifique el ejercicio efectivo del solicitante durante al menos tres años, en un estado que haya concedido a la equivalencia un certificado de formación o certificado adquirido en un tercer estado y que permita el ejercicio de ese Profesión;
- programas de formación o el contenido de la experiencia adquirida.

**Tenga en cuenta que**

Si es necesario, todos los documentos justificativos deben ser traducidos al francés por un traductor certificado.

**Procedimiento**

Cuando se envíe el archivo, el prefecto confirmará la recepción en el plazo de un mes e informará al nacional de los documentos que falten. A menos que se adopten medidas de indemnización contra el nacional, retrasando el procedimiento, el prefecto podrá expedir el certificado de capacidad profesional en el plazo de un mes.

Una vez que el nacional haya obtenido su certificado de aforo profesional, podrá inscribirse en el registro de transporte por carretera.

*Para ir más allá* : Artículos 9 a 15 de la orden del 21 de diciembre de 2015 sobre la emisión del certificado de capacidad profesional que permita el ejercicio de la profesión de comisionaria de transporte

**Bueno saber: medidas de compensación**

En caso de diferencias entre la formación o la experiencia del nacional y los requisitos exigidos en Francia, el prefecto podrá someterlo a una medida de compensación que pueda ser un curso de adaptación o una prueba de aptitud.

El curso de adaptación debe durar al menos 80 horas y permitirle adquirir un nivel suficiente de ley aplicado al transporte, la economía del transporte y la comisión de transporte.

La prueba de aptitud es en forma de un cuestionario de opción múltiple para el cual el nacional debe obtener una puntuación de al menos 60 de 100.

*Para ir más allá* : Artículo R. 1422-18 del Código de Transporte.

### b. Solicitar inscripción en el registro de los comisarios de transportes

El registro se realiza enviando el formulario [Cerfa No. 16092*01](https://www.service-public.fr/professionnels-entreprises/vosdroits/R57872) a la autoridad local de transporte.

*Para ir más allá* : Orden de 4 de octubre de 2007 relativa a la composición del expediente de solicitud de inscripción en el registro de los comisarios de transportes.

### c. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

