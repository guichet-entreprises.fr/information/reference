﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP170" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sanidad" -->
<!-- var(title)="Masajista" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sanidad" -->
<!-- var(title-short)="masajista" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/sanidad/masajista.html" -->
<!-- var(last-update)="2020-04-15 17:21:39" -->
<!-- var(url-name)="masajista" -->
<!-- var(translation)="Auto" -->


Masajista
=========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:39<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El masajista es un profesional responsable del diagnóstico de fisioterapia y el tratamiento de trastornos del movimiento, habilidades motoras y deterioros y deterioros de las habilidades funcionales humanas.

Realiza, manual o instrumentalmente, masajes y gimnasia médica, en particular con fines de rehabilitación con receta médica, con el fin de prevenir el deterioro de las capacidades funcionales, para contribuir a su mantenimiento y, si son alterados, restaurarlos o reemplazarlos.

*Para ir más allá*: Artículo L. 4321-1 del Código de Salud Pública.

2°. Cualificaciones profesionales
=========================================

### a. Requisitos nacionales

#### Legislación nacional

En Francia, la profesión de masajista está reservada a los titulares del diploma estatal de masajista.

*Para ir más allá*: Artículo L. 4321-2 del Código de Salud Pública.

#### Entrenamiento

El diploma estatal se prepara en cuatro años en institutos de formación en fisioterapia de masaje (IFMK) aprobados por el prefecto regional, después de un año de estudios universitarios validados.

Para ser admitido en el primer año de estudios preparatorios al diploma estatal de masajista, dentro del límite de plazas autorizadas, el interesado debe haber validado la elección:

- El primer año de estudios de salud conjunta (PACES);
- el primer año de una licenciatura en ciencias, mencionando "ciencia y tecnología de las actividades físicas y deportivas" (STAPS);
- un primer año de licenciatura en ciencia, tecnología, salud.

La formación en una de las IFMKs se divide en dos ciclos de dos años: la primera está dedicada a los fundamentos, la ingeniería de la fisioterapia y el conocimiento transversal; el segundo es más profesional.

Al final de la formación necesaria para obtener el diploma estatal, los profesionales pueden aprovechar 240 créditos de su formación en institutos y 60 créditos de su año académico anterior.

*Para ir más allá* : decreto de 2 de septiembre de 2015 relativo al diploma estatal de masajista y al decreto de 2 de septiembre de 2015 relativo al diploma estatal de masajista.

#### Costos relacionados

Los costos asociados a la formación varían dependiendo de la situación de cada universidad (para el año académico anterior) y de cada instituto de formación en fisioterapia de masaje (los consejos regionales cubren el costo de la formación en variable).

Por ejemplo, la tasa media nacional de matrícula en IFMK es de 3.775 euros al año, según un estudio de 2013 de la Federación Nacional de Estudiantes de Fisioterapia.

### b. Nacionales de la UE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Los nacionales de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional, siempre que se hayan referido al prefecto de departamento cuando la entrega se lleve a cabo una declaración previa de actividad.

Si ni el acceso ni la formación están regulados en el Estado miembro de origen ni en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Las cualificaciones profesionales del proveedor se comprueban antes de que se proporcione el primer servicio. En caso de diferencia sustancial entre las cualificaciones del reclamante y la formación requerida en Francia, que puede perjudicar a la salud pública, la autoridad competente pide al reclamante que demuestre que ha adquirido los conocimientos y las aptitudes medidas de compensación (véase infra "Bueno saber: medidas de compensación").

En todos los casos, el nacional europeo que desee ejercer en Francia de forma temporal u ocasional debe poseer las aptitudes linguísticas necesarias para llevar a cabo la actividad y dominar el peso y los sistemas de medición utilizados en Francia. Francia.

*Para ir más allá*: Artículo L. 4321-11 del Código de Salud Pública.

### c. Nacionales de la UE: para un ejercicio permanente (Establecimiento libre)

Los ciudadanos de la UE que deseen trabajar permanentemente en Francia deben obtener una licencia para ejercer.

Los nacionales de la UE o del EEE pueden poder ejercer en Francia si han completado con éxito un ciclo de educación postsecundaria y son titulares de su elección:

- un certificado de formación expedido por un Estado de la UE o del EEE en el que se regulen el acceso a la profesión o a su práctica y que permita ejercer allí la práctica jurídica;
- un certificado de formación expedido por un Estado de la UE o del EEE en el que no se regula el acceso a la profesión ni su ejercicio, y justifican haber llevado a cabo esta actividad en ese Estado durante al menos el equivalente a dos años a tiempo completo en los diez En los últimos años
- un certificado de formación expedido por un tercer Estado reconocido en un Estado de la UE o del EEE, distinto de Francia, que permita ejercer allí la profesión.

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones necesarias para el acceso a la profesión y a su práctica en Francia, el interesado podrá someterse, en función de su elección, a un examen (véase a continuación"Bien saber: medidas de compensación").

*Para ir más allá*: Artículos L. 4321-2 y los siguientes y L. 4321-4 del Código de Salud Pública.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

### a. Cumplimiento del Código de ética de los terapeutas de masaje

El código de ética es necesario para todos los terapeutas de masaje en el tablero de pedidos.

Como tal, los masajistas deben respetar los principios de moralidad y probidad que se les imponen. Además, están sujetos al secreto médico.

*Para ir más allá*: Artículos R. 4321-51 y R. 4321-54 del Código de Salud Pública.

### b. Actividades acumulativas

Los masajistas pueden realizar otra actividad con la afección que:

- la acumulación de actividad no pone en tela de juicio su independencia, moralidad y dignidad profesional;
- la combinación de actividad no les permite aprovechar sus recetas.

*Para ir más allá*: Artículo R. 4321-68 del Código de Salud Pública.

### c. Condiciones de honorabilidad

Para hacer ejercicio, el fisioterapeuta no debe:

- han sido objeto de una prohibición temporal o permanente de la práctica en Francia o en el extranjero;
- han sido suspendidos debido al grave peligro para los pacientes en el ejercicio de la actividad.

*Para ir más allá*: Artículo L. 4321-10 y L. 4311-16 del Código de Salud Pública.

### d. Obligación para el desarrollo profesional continuo

Los masajistas deben participar anualmente en un programa de desarrollo profesional en curso. Este programa tiene como objetivo mantener y actualizar sus conocimientos y habilidades, así como mejorar sus prácticas profesionales.

Como tal, el profesional de la salud (salario o liberal) debe justificar su compromiso con el desarrollo profesional. El programa está en forma de formación (presente, mixta o no presental) en análisis, evaluación y mejora de la práctica y gestión de riesgos. Toda la formación se registra en un documento personal que contiene certificados de formación.

*Para ir más allá*: Artículos L. 4021-1 y R. 4382-1 del Código de Salud Pública.

### e. Aptitud física

Para ejercer como fisioterapeuta, el interesado no debe estar en una discapacidad ni estar en una condición patológica que haga peligroso el ejercicio de su profesión.

*Para ir más allá*: Artículo L. 4311-18 aplicable por la Sección L. 4321-10 del Código de Salud Pública.

4°. Legislación social y seguro
----------------------------------------------------

### a. Obligación de constete de un seguro de responsabilidad civil profesional

Como profesional de la salud, un masajista liberal debe tomar un seguro de responsabilidad civil profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

*Para ir más allá*: Artículo L. 1142-2 del Código de Salud Pública.

### b. Obligación de adherirse al Fondo Autónomo de Pensiones y Previsión de Asistentes Médicos (CARPIMKO)

Los masajistas que practican de manera liberal, incluso incidental, deben unirse al CARPIMKO (Fondo de Jubilación y Pensiones de enfermeras, masajistas, pedicuras-podólogos, logopedas).

**Documentos de apoyo**

El interesado deberá ponerse en contacto con CARPIMKO lo antes posible:

- El cuestionario de afiliados [Descargables](https://www.carpimko.com/document/pdf/affiliation_declaration.pdf) en el sitio web de CARPIMKO o en una carta en la que se mencione la fecha de inicio de la actividad liberal;
- Fotocopia del diploma estatal;
- una fotocopia del número de registro en el directorio de "automatización de listas" (Adeli) del diploma expedido por la Agencia Regional de Salud (Adeli) o una fotocopia del dorsa (automatización del diploma).

5°. Proceso de cualificaciones y formalidades
-------------------------------------------------------

### a. Hacer una predeclaración de actividad para los nacionales de la UE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

La declaración previa de actividad debe dirigirse al Consejo Nacional de la Orden de Los Terapeutas de Masaje, antes de la primera prestación de servicio.

**Renovación de la predeclaración**

La declaración anticipada debe renovarse una vez al año si el reclamante desea realizar una nueva prestación en Francia.

#### Documentos de apoyo

- Complete el formulario de declaración anticipada de actividad, cuyo modelo se define como apéndice del pedido del 20 de enero de 2010;
- presentar un certificado de suscripción al seguro de responsabilidad civil profesional por actos realizados en territorio francés;
- proporcionar una fotocopia de una declaración que acredite la nacionalidad del solicitante;
- proporcionar una fotocopia del título o títulos de formación
- certificando que la autoridad competente del Estado de establecimiento, miembro de la UE o del EEE, certifica que el interesado está legalmente establecido en dicho Estado y que no existe ninguna prohibición, ni siquiera temporal, sobre la base de la práctica.

**Qué saber**

Los documentos justificativos, con excepción de la fotocopia del documento de identidad, deben ser traducidos al francés por un traductor autorizado ante los tribunales franceses o facultado para intervenir ante las autoridades judiciales o administrativas de un Estado miembro de la UE o del EEE o Suiza.

**Tiempo de respuesta**

Dentro de un mes de recibir la devolución:

- el Consejo Nacional de la Orden informa al demandante de si puede o no iniciar la prestación de servicios. La junta de la orden también podrá, si la verificación de las cualificaciones profesionales del demandante muestra una diferencia sustancial con la formación requerida en Francia, puede solicitar una prueba de conocimientos y aptitudes desaparecidos, incluyendo someterse a una prueba de aptitud;
- el Consejo Nacional de la Orden podrá solicitar información adicional al proveedor. En este caso, el período inicial de un mes para decidir sobre la solicitud previa de autorización se prorroga por un mes adicional. Así, en el plazo de dos meses a partir de la recepción de la solicitud, el Consejo de Orden Nacional informa al demandante de si puede o no comenzar a prestar servicios. La junta de la orden también podrá, si la verificación de las cualificaciones profesionales del demandante muestra una diferencia sustancial con la formación requerida en Francia, puede solicitar una prueba de conocimientos y aptitudes sujeto a una prueba de aptitud o una pasantía.

A falta de cualquier respuesta del Consejo Nacional de la Orden dentro de los plazos antes mencionados, la entrega podrá comenzar.

**Entrega de recibo**

El Consejo Nacional de la Orden registra al demandante en una lista específica (este registro está exento de contribución) y le envía un recibo con su número de registro. El reclamante debe enviar una copia de este recibo a la agencia nacional de seguro de salud para informarle de su beneficio con antelación.

**Remedios**

Las decisiones del Consejo Nacional de la Orden sobre la libre prestación de servicios pueden ser impugnadas en el plazo de dos meses a partir de su notificación mediante un recurso de casación interpuesto ante el órgano jurisdiccional administrativo territorialmente competente. El tiempo de impugnación se incrementa a cuatro meses si el solicitante no permanece en Francia.

**Costo**

Gratis.

*Para ir más allá*: Artículo R. 4311-38, a la disposición del artículo R. 4321-30 del Código de Salud Pública y en la orden del 20 de enero de 2010 sobre la declaración previa de prestación de servicios para el ejercicio de las profesiones de (...) masajista.

### b. Formalidades para los nacionales de la UE para un ejercicio permanente (LE)

#### Pedir permiso para practicar

**Autoridad competente**

La solicitud de autorización permanente deberá enviarse en una carta recomendada con notificación de recepción a la Dirección Regional y Departamental de Juventud, Deportes y Cohesión Social (DRDJSCS) en la región en la que solicitante desea ejercer.

**hora**

THE DRDJSCS tiene un mes a partir de la fecha de recepción de la solicitud para verificar que el archivo de solicitud está completo. Cualquier expediente completo es revisado por la Comisión Regional de Autorización en un plazo de cuatro meses a partir de la fecha de confirmación de la integridad del expediente.

**Documentos de apoyo**

La solicitud de licencia para ejercer deberá enviarse en dos copias y contener:

- El formulario de solicitud de autorización para ejercer en Francia está disponible en el sitio web de DRDJSCS de que se trate;
- Una declaración sobre el honor de no presentar una solicitud de licencia para ejercer en otra región;
- Una fotocopia de una identificación válida
- Una copia del título de formación que permite ejercer la profesión en el país de obtención;
- cualquier evidencia útil que justifique la formación continua, la experiencia y las habilidades adquiridas durante la práctica profesional;
- una declaración de la autoridad competente del Estado de Conciliación de la UE o del EEE, de menos de un año de edad, que acredite la ausencia de sanciones;
- Una copia de los certificados de las autoridades que emiten el certificado de formación en el que se especifican los detalles de la formación;
- para los candidatos que han ejercido en un Estado de la UE o del EEE, que no regula el acceso o el ejercicio de la profesión, ninguna prueba que justifique que haya ejercido en ese estado como masajista fisioterapeuta durante el equivalente a dos años en el tiempo durante la última década.

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

Algunos DRDJSCS pueden requerir la liberación de otros documentos. Para obtener más detalles, es aconsejable acercarse al DRDJSCS correspondiente.

**Resultado del procedimiento**

La Comisión Regional de Licencias podrá:

- Emitir un permiso de ejercicio directo
- solicitar información adicional y sursoir que se decidan hasta que se reciban estos documentos;
- denegar el permiso para ejercer. En este caso, la decisión es razonada e incluye tiempo y recurso;
- solicitar medidas compensatorias para completar la formación del solicitante (véase a continuación "Bueno saber: Medidas de compensación").

A falta de una respuesta del prefecto regional en un plazo de cuatro meses a partir de la recepción del expediente completo, la solicitud de licencia se considerará denegada.

**Remedios**

El solicitante podrá impugnar cualquier decisión (implícita o expresa) de denegar su solicitud de licencia para ejercer. Para ello, puede, en el plazo de dos meses a partir de la notificación de la decisión de denegación, constituir la elección de:

- un llamamiento agraciado al prefecto regional;
- un llamamiento jerárquico al Ministro de Salud;
- acciones legales ante el tribunal administrativo territorialmente competente.

**Costo**

Variable según el DRDJSCS.

**Bueno saber: medidas de compensación**

Para obtener permiso para ejercer, el interesado puede estar obligado a someterse a una prueba de aptitud o a un curso de adaptación si resulta que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de los necesarios para el ejercicio de la profesión en Francia. Estamos hablando de medidas de compensación.

**Autoridad competente**

Si se consideran necesarias las medidas de compensación, el prefecto regional responsable de expedir la autorización de ejercicio indica al interesado que dispone de dos meses para elegir entre la prueba de aptitud y el curso de ajuste.

**La prueba de aptitud**

El DRJSCS, que organiza las pruebas de aptitud, debe convocar a la persona por carta recomendada con un aviso de recepción al menos un mes antes del inicio de las pruebas. Esta citación menciona el día, la hora y el lugar del juicio.

La prueba de aptitud puede adoptar la forma de preguntas escritas u orales indicadas en veinte sobre cada una de las asignaturas que no fueron inicialmente enseñadas o no adquiridas durante la experiencia profesional.

La admisión se pronuncia con la condición de que el individuo haya alcanzado un promedio mínimo de 10 sobre 20, sin puntuación inferior a 8 de 20. Los resultados de la prueba se notifican al interesado por el prefecto regional.

En caso de éxito, el prefecto regional autoriza al interesado a ejercer la profesión.

**El curso de adaptación**

Se lleva a cabo en un centro de salud público o privado aprobado por la agencia regional de salud (ARS). El becario es sometido a la responsabilidad pedagógica de un profesional cualificado que ha estado practicando la profesión durante al menos tres años y que establece un informe de evaluación.

La pasantía, que finalmente incluye formación teórica adicional, es validada por el jefe de la estructura de recepción a propuesta del profesional cualificado que evalúa al aprendiz.

Los resultados de la pasantía se notifican al interesado por el prefecto regional.

La solicitud de licencia a la práctica se realiza después de un nuevo aviso de la comisión a que se refiere la sección L. 4311-4 del Código de Salud Pública.

*Para ir más allá* : decreto de 24 de marzo de 2010 por el que se establece la organización de la prueba de aptitud y el curso de adaptación para la práctica de la enfermería en Francia por nacionales de los Estados miembros de la UE o parte en el Acuerdo EEE.

#### Pide inclusión en el panel de masajistas

Todas las personas con un diploma estatal o una licencia para ejercer deben registrarse con el orden de los masajistas para ser incluidos en la mesa del pedido. De hecho, esta inscripción hace que la práctica de la fisioterapia de masaje en territorio francés sea legal.

*Para ir más allá*: Artículos L. 4321-10 y L. 4112-5 del Código de Salud Pública.

**Autoridad competente**

La solicitud de registro deberá presentarse mediante carta recomendada con notificación de recepción al presidente del consejo departamental del orden del departamento en el que el interesado desee ejercer.

**Procedimiento**

El presidente del consejo de condado de la orden tiene tres meses desde la recepción de la solicitud acompañado por el expediente completo para decidir sobre la solicitud de inscripción en la junta. La junta informa al individuo de su decisión por carta recomendada. Cualquier negativa a registrarse está justificada.

###### Documentos de apoyo

Los documentos justificativos que se proporcionarán son:

- Una fotocopia de un documento de identidad válido
- para los profesionales de nacionalidad francesa nacidos en el extranjero, un certificado de nacionalidad expedido por la autoridad competente;
- Un documento de identidad con foto
- Una fotocopia de la prueba de residencia, de menos de tres meses de edad;
- Una copia del diploma estatal o autorización de ejercicio;
- Una fotocopia de los otros diplomas
- Una fotocopia de la inscripción en el archivo Adeli o una fotocopia de la tarjeta sanitaria profesional;
- Un certificado de conocimiento del Código de ética y una empresa escrita bajo juramento para respetarlo;
- Si el solicitante presenta un diploma expedido en un Estado extranjero cuya validez se reconoce en Francia, la copia de los títulos a los que puede subordinarse dicho reconocimiento;
- nacionales de un Estado extranjero, un extracto de antecedentes penales o documentos equivalentes, de menos de tres meses, expedidos por una autoridad competente del Estado de origen u origen. Los nacionales de la UE o del EEE que exijan una prueba de moralidad o honorabilidad para el acceso a la profesión podrán presentar un certificado de menos de tres meses a partir de la autoridad competente del Estado de origen u origen, certificando que se cumplen estas condiciones morales o de honor;
- una declaración sobre el honor del solicitante que certifique que ningún procedimiento que pudiera dar lugar a una condena o sanción que pudiera afectar a la lista en la junta está en su contra;
- un certificado de registro o registro expedido por la autoridad con la que el solicitante fue registrado y registrado previamente (o, en su defecto, una declaración de honor de que el solicitante nunca fue registrado o registrado);
- todas las pruebas de que el solicitante tiene las habilidades de idiomas necesarias para ejercer la profesión;
- un currículum.

Se pueden solicitar otros documentos justificativos en función de la forma en que se practique la profesión (salario, liberal, mixto). Para más detalles, es aconsejable acercarse al consejo departamental de la orden en cuestión.

**Remedios**

El interesado podrá impugnar, en un plazo de 30 días, la decisión de denegar su inclusión en el consejo de pedidos ante el consejo regional o interregional en el que se encuentre el consejo del condado en el que se haya denegado el registro.

**Costo**

El procedimiento de inscripción es gratuito, pero el registro implica la presentación de una evaluación obligatoria, cuyo importe es fijado anualmente por el Consejo Nacional de la Orden.

*Para ir más allá*: Artículos L. 4112-3, L. 4112-4, L. 4321-10, L. 4321-16, L. 4321-19, R. 4112-1, R. 4112-4 aplicables por la Sección R. 4323-1 del Código de Salud Pública.

#### En caso de creación de un SEL o un SCP, solicitar la inclusión de la empresa en el panel de masajistas

En el caso de la creación de una empresa de ejercicioliberal (SEL) o una sociedad civil profesional (SCP), esta empresa también debe ser puesta por orden del lugar de su sede.

**Autoridad competente**

La solicitud de inclusión en el tablero de pedidos debe dirigirse al consejo departamental del pedido de la sede de la empresa, por carta recomendada con solicitud de notificación de recepción.

**Procedimiento**

El consejo del condado decide sobre la solicitud de registro dentro de los tres meses siguientes a la recepción de la solicitud con el expediente completo. Cualquier negativa a registrarse está motivada y notificada a cada persona interesada por carta recomendada con solicitud de notificación de recepción.

##### Documentos de apoyo

- para SEL: los documentos justificativos requeridos se enumeran en el artículo R. 4113-4 del Código de Salud Pública;
- para CPS, los documentos justificativos requeridos se enumeran en la sección R. 4113-28 del Código de Salud Pública.

**Remedios**

El interesado podrá impugnar, en un plazo de 30 días, la decisión de negarse a registrarse en la junta de pedidos ante el consejo regional o interregional en la jurisdicción del consejo del condado que denegó el registro.

*Para ir más allá*: Artículos L. 4321-19, L. 4112-4 y R. 4112-4 del Código de Salud Pública.

**Costo**

El procedimiento de inscripción es gratuito, pero el registro implica la presentación de una evaluación obligatoria, cuyo importe es fijado anualmente por el Consejo Nacional de la Orden.

*Para ir más allá*: Artículos R. 4113-4 y R. 4113-28 aplicables en la Sección R. 4323-2 del Código de Salud Pública.

#### Solicitar inscripción de diploma o autorización de ejercicio (No.Adeli)

Las personas con un título de Estado o, para los nacionales europeos y extranjeros, la autorización para ejercer en Francia deben registrar su diploma o autorización en el directorio de Adeli.

Los fisioterapeutas que practican de forma temporal y ocasional están exentos de solicitar el registro de su diploma.

**Autoridad competente**

La Agencia Regional de Salud (ARS) del lugar donde se lleva a cabo la actividad es responsable del registro del diploma o de la autorización para ejercer.

**hora**

La solicitud de inscripción debe presentarse en el plazo de un mes a partir de la toma de posesión, independientemente del modo de ejercicio (liberal, asalariado, mixto). El recibo, que es emitido por el ARS, menciona el número Adeli. A continuación, el LRA envía al solicitante un formulario de solicitud para la concesión de la tarjeta de salud profesional.

##### Piezas para proporcionar

- para los diplomas franceses: el diploma original;
- para diplomas de la UE o del EEE: el diploma, su traducción acreditada al francés y la autorización para ejercer en Francia;
- Una copia de un documento de identidad válido
- CERFA 10906*06 ;
- prueba de registro al orden del departamento de práctica.

Algunos LCA pueden requerir la publicación de otros documentos. Para más detalles, es aconsejable acercarse al LRA competente.

**Costo**

Gratis.

*Para ir más allá*: Artículo L. 4321-10 del Código de Salud Pública.

### c. Tarjeta Profesional Europea (EPC)

La tarjeta profesional europea es un procedimiento electrónico para las cualificaciones profesionales de 500 miembros en otro Estado de la UE.

El procedimiento EPC puede utilizarse tanto cuando el nacional desea trabajar como fisioterapeuta en otro país de la UE:

- de forma temporal y casual
- de forma permanente.

El EPC es válido:

- indefinidamente en caso de un acuerdo a largo plazo
- 18 meses en principio para la prestación temporal de servicios (o 12 meses para ocupaciones que puedan tener un impacto en la salud o la seguridad públicas)

Para solicitar un EPC:

- es necesario crear una cuenta de usuario en el servicio de autenticación ECAS de la Comisión Europea
- a continuación, tiene que rellenar su perfil de EPC (identidad, información de contacto...)
- Por último, es posible crear una aplicación EPC descargando los documentos de apoyo escaneados.

**Costo**

Por cada solicitud de RCP, las autoridades del país de acogida y del país de origen pueden cobrar una tasa de revisión de archivos que varía en función de la situación.

**hora**

*Para una solicitud de EPC para actividades temporales y ocasionales*

En el plazo de una semana, la autoridad del país de origen reconoce la recepción de la solicitud de CPE, informa si faltan documentos e informa de los costos. A continuación, las autoridades del país anfitrión revisan el caso.

- Si no se requiere verificación con el país anfitrión, la autoridad del país de origen revisa la solicitud y toma una decisión final en un plazo de tres semanas.
- Si se requieren verificaciones dentro del país anfitrión, la autoridad del país de origen tiene un mes para revisar la solicitud y reenviarla al país anfitrión. A continuación, el país anfitrión toma una decisión final en un plazo de tres meses.

*Para una solicitud de EPC para una actividad permanente*

En el plazo de una semana, la autoridad del país de origen reconoce la recepción de la solicitud de CPE, informa si faltan documentos e informa de los costos. A continuación, el país de origen dispone de un mes para revisar la solicitud y reenviarla al país anfitrión. Este último toma la decisión final en un plazo de tres meses.

Si las autoridades del país anfitrión creen que el nivel de educación, formación o experiencia laboral está por debajo de los estándares requeridos en ese país, pueden solicitar una prueba de aptitud o realizar un curso de ajuste. .

*Problemas de la aplicación EPC*

- Si se concede la solicitud de RCP, se puede obtener un certificado de RCP en línea.
- Si las autoridades del país anfitrión no tocan una decisión dentro del tiempo asignado, las cualificaciones se reconocen tácitamente y se emite un CPE. A continuación, es posible obtener un certificado de RCP de su cuenta en línea.
- Si se desestima la solicitud de RDC, la decisión de denegar debe ser motivada y solicitar medidas para impugnar la denegación.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un [formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París, ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

