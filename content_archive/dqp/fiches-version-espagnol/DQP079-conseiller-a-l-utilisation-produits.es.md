﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP079" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Asesor sobre el uso de fitofarmacéuticos" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="asesor-sobre-el-uso-de-fitofarmaceuticos" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/asesor-sobre-el-uso-de-fitofarmaceuticos.html" -->
<!-- var(last-update)="2020-04-15 17:21:03" -->
<!-- var(url-name)="asesor-sobre-el-uso-de-fitofarmaceuticos" -->
<!-- var(translation)="Auto" -->




Asesor sobre el uso de fitofarmacéuticos
========================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:03<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El Asesor Fitofarmacéutico es un profesional cuyo trabajo es proporcionar asesoramiento sobre el control de plagas de cultivos y el uso seguro de fitofármacos.

*Para ir más allá*: Artículo R. 254-1 del Código Rural y Pesca Marina.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ser consultor fitofarmacéutico, el profesional debe:

- Poseer un certificado individual llamado "Certiphyto";
- aprobación (ver infra "5 grados. b. Solicitud de acreditación para la actividad de consultoría").

*Para ir más allá*: Artículos L. 254-1 a L. 254-6 del Código Rural y Pesca Marina.

#### Entrenamiento

**Certificado individual para la actividad "asesoramiento sobre el uso de fitofarmacéuticos"**

El certificado individual se expide al profesional que:

- se sometió a capacitación que incluyó una prueba escrita de una hora de verificación de conocimientos;
- superó con éxito una prueba de una hora y media, que consta de treinta preguntas sobre el programa de formación establecido en la Lista II de la Orden de 29 de agosto de 2016;
- posee uno de los diplomas en la Lista I de la orden del 29 de agosto de 2016, obtenido en los cinco años anteriores a la solicitud.

Una vez que el profesional cumple una de estas condiciones, debe solicitar el certificado (ver más abajo "5o. a. Solicitar un certificado de práctica individual").

*Para ir más allá* : decreto de 29 de agosto de 2016 por el que se establecen y establecen las condiciones de obtención del certificado individual para la actividad "asesoramiento sobre el uso de fitofarmacéuticos".

### b. Nacionales de la UE: para el ejercicio temporal y ocasional (prestación gratuita de servicios (LPS))

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) titular de un certificado expedido por un Estado miembro, legalmente establecido y dedicado a actividades de asesoramiento sobre productos fitofarmacéuticos, pueden llevar a cabo la misma actividad de forma temporal y casual en Francia.

Para ello, debe, antes de su primera prestación de servicios, hacer una declaración al Director Regional de Alimentación, Agricultura y Silvicultura (DRAAF) del lugar donde se presta el primer servicio (véase infra "5o). c. Predeclaración del nacional para un ejercicio temporal e informal (LPS)").

*Para ir más allá*: Artículos L. 204-1 y R. 254-9 del Código Rural y Pesca Marina.

### c. Nacionales de la UE: para un ejercicio permanente (establecimiento libre (LE))

Cualquier nacional de un Estado miembro de la UE o del EEE legalmente establecido que participe en una actividad relacionada con el uso de fitoproductos podrá llevar a cabo la misma actividad de forma permanente en Francia.

Mientras el nacional posea un certificado individual de ejercicio expedido por un Estado miembro, se considerará que posee el certificado necesario para ejercer en Francia.

*Para ir más allá* II del artículo R. 254-9 del Código Rural y de la Pesca Marina.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

El profesional está obligado a informar a sus clientes sobre todas las disposiciones relativas al uso de estos productos, incluyendo:

- el plazo entre la aplicación de los productos y la cosecha;
- La velocidad máxima del viento más allá de la cual no pueden utilizar estos productos;
- para la aplicación o vaciado de efluentes fitofarmacéuticos.

Además, todo profesional que utilice fitofarmacéuticos está obligado a seguir el plan nacional en el que se establecen los objetivos y medidas adoptadas para reducir los efectos del uso de estos productos en la salud humana y el medio ambiente.

*Para ir más allá*: Artículo L. 253-6 del Código Rural y Pesca Marina; Orden de 4 de mayo de 2017 relativa a la comercialización y utilización de fitofarmacéuticos y sus adyuvantes contemplados en el artículo L. 253-1 del Código Rural y de la Pesca Marina.

4°. Seguros y sanciones
--------------------------------------------

**Seguro**

El profesional está obligado, para obtener su aprobación, a obtener un seguro que cubra su responsabilidad civil profesional.

*Para ir más allá*: Artículo L. 254-2 del Código Rural y Pesca Marina.

**Sanciones penales**

Un profesional que trabaja como consultor fitofarmacéutico sin la certificación en virtud del artículo L. 254-1 del Código Rural y de Pesca Marina está sujeto a una pena de seis meses de prisión y una multa de 15.000 euros. La misma sanción se aplica si el titular de la licencia está practicando sin cumplir los requisitos de la sección L. 254-2 o la sección L. 254-5.

*Para ir más allá*: Artículo L. 254-12 del Código Rural y Pesca Marina.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Solicitar un certificado de práctica individual

**Autoridad competente**

El profesional deberá presentar su solicitud a la Dirección Regional de Alimentación, Agricultura y Silvicultura (DRAAF) del lugar de su residencia, o, en su caso, el lugar de la sede de la organización donde se realizaron las formaciones.

**Documentos de apoyo**

Los solicitantes deben crear una cuenta en línea en el sitio web[service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) para acceder al servicio de aplicación de certificados.

Su solicitud debe incluir, dependiendo del caso:

- Una prueba de seguimiento de la formación y, si es necesario, pasar la prueba;
- copia de su diploma o título.

**Tiempo y procedimiento**

El certificado se expide dentro de los dos meses siguientes a la presentación de su solicitud. En ausencia de la emisión del certificado después de este período, y a menos que la notificación de denegación, los documentos justificativos anteriores valen la pena un certificado individual de hasta dos meses.

Este certificado tiene una validez de cinco años y es renovable en las mismas condiciones.

*Para ir más allá*: Artículos R. 254-11 y R. 254-12 del Código de Pesca Rural y Marina.

### b. Solicitud de acreditación para actividades de consultoría

**Autoridad competente**

El profesional debe presentar su solicitud al prefecto de la región en la que se encuentra la sede central de la empresa. Si el profesional tiene su sede en un Estado miembro distinto de Francia, tendrá que presentar su solicitud al prefecto de la región en la que llevará a cabo su primera prestación de servicios.

**Documentos de apoyo**

Su solicitud debe incluir el[Formulario de solicitud](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14581.do) completado y firmado, así como los siguientes documentos:

- Un certificado de póliza de seguro de responsabilidad civil profesional;
- Un certificado expedido por un organismo tercero que justifica que realiza esta actividad de conformidad con las condiciones que garantizan la protección de la salud pública y del medio ambiente;
- copia de un contrato con una agencia externa que prevé el seguimiento necesario para mantener la certificación.

**Tenga en cuenta que**

Los microdistribuidores deben presentar los siguientes documentos a DRAAF:

- un certificado de seguro de responsabilidad civil profesional;
- prueba de la posesión de certiphyto por parte de todo el personal que desempeña funciones de supervisión, ventas o asesoramiento;
- un documento que justifique que está sujeto al sistema fiscal de microempresas.

**Tiempo y procedimiento**

El profesional que inicie su actividad deberá solicitar una aprobación provisional que se concederá por un período de seis meses no renovable. A continuación, podrá solicitar la aprobación final.

El silencio guardado por el prefecto de la región más allá de un período de dos meses merece una decisión de rechazo.

Además, el profesional está obligado a proporcionar al prefecto una copia de su certificado de seguro cada año.

*Para ir más allá*: Artículos R. 254-15-1 a R. 254-17 del Código Rural y Pesca Marina.

### c. Predeclaración del nacional para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe solicitar por cualquier medio al DRAAF el lugar donde reciba los servicios por primera vez.

**Documentos de apoyo**

Su solicitud debe incluir el certificado individual de ejercicio expedido por el Estado miembro y, en su caso, con su traducción al francés.

**Tenga en cuenta que**

Esta declaración debe renovarse cada año y en caso de cambio de situación laboral.

*Para ir más allá* III del artículo R. 254-9 del Código Rural y de la Pesca Marina.

### d. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea. Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

