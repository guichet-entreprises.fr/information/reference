﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP074" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Profesiones de asesoramiento y experiencia" -->
<!-- var(title)="Subastador voluntario" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="profesiones-de-asesoramiento-experiencia" -->
<!-- var(title-short)="subastador-voluntario" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/profesiones-de-asesoramiento-experiencia/subastador-voluntario.html" -->
<!-- var(last-update)="2020-04-15 17:21:02" -->
<!-- var(url-name)="subastador-voluntario" -->
<!-- var(translation)="Auto" -->


Subastador voluntario
=====================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:21:02<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El subastador de ventas voluntarias es un profesional responsable de evaluar y sedring (detallando todas las características) de un objeto o mueble, dándole un valor y realizando su promoción y subasta pública.

**Tenga en cuenta que**

El profesional que realiza ventas voluntarias de muebles en subasta pública toma el título de subastador de ventas voluntarias con exclusión de cualquier otro.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

Para ser un subastador de ventas voluntario, el profesional debe:

- Ser francés o nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE);
- no han sido objeto de una condena penal por hechos contrarios al honor o a la probidad o, en su profesión anterior, una sanción disciplinaria o administrativa de despido, desempleo, despido, retirada aprobación o autorización para hechos similares;
- Estar profesionalmente calificado y haber completado con éxito un curso de formación teórica y práctica de dos años;
- han hecho una declaración previa de actividad con el Consejo de Ventas Voluntarias de Muebles en subastas públicas (Consejo de Ventas Voluntarias) (véase infra "5o. a. Predeclaración de actividad").

*Para ir más allá*: Artículos L. 321-4 y R. 321-18 del Código de Comercio.

**Tenga en cuenta que**

Las ventas voluntarias de notarios y funcionarios judiciales en municipios donde no hay una oficina judicial subastadora pueden llevarse a cabo, sujeto a formación. A continuación, llevan a cabo su actividad como un imprevisto. (véase el artículo L. 321-2 del Código de Comercio).

#### Entrenamiento

Para ser reconocido como profesionalmente calificado, el profesional debe haber completado un curso de formación de dos años y realizar:

- licenciado en Derecho Nacional y licenciatura nacional en una de las siguientes áreas:- historia del arte,
  - Artes Aplicadas,
  - Arqueología
  - artes visuales;
- un grado reconocido como equivalente a los grados anteriores, a saber:- cualquier diploma nacional que sancione un nivel de formación de al menos tres años después de una licenciatura en las disciplinas jurídica, económica, comercial o de gestión;
  - cualquier grado que confiere el título o máster, sancionando estudios en las disciplinas jurídica, económica, comercial o de gestión;
  - cualquier diploma destinado por el Ministro de Educación Superior, sancionando un nivel de formación de al menos tres años de estudio después de la licenciatura, en las disciplinas jurídica, económica, comercial o de gestión;
  - cualquier título sancionando un nivel de formación correspondiente a al menos tres años de estudio después de la licenciatura en las disciplinas jurídica, económica, comercial o de gestión expedida por la facultad autónoma y cogestionada de La ley de París hasta 2018 incluyó;
  - cualquier diploma nacional que sancione un nivel de educación de al menos tres años después de la licenciatura en historia del arte, artes aplicadas, arqueología o artes visuales;
  - cualquier título que confiere una licenciatura o maestría, estudios sancionadores en historia del arte, artes aplicadas, arqueología o artes visuales;
  - el grado de la escuela louvre;
  - el diploma de paleografista archivero expedido por la Escuela Nacional de Cartas;
  - el grado en bi-licencia de derecho-historia del arte y la arqueología de la Universidad de París-I;
  - El grado bidisciplinario en derecho e historia del arte de la Universidad de Lyon-II;
  - el grado de historia y derecho de la Universidad de Brest;
  - El diploma de "especialista en consultoría de bienes y servicios culturales" del Instituto de Estudios de Posgrado de las Artes (IESA), emitido hasta 2018 incluido.

**Tenga en cuenta que**

El ejercicio de determinadas profesiones también permite quedar exenta del título nacional de Derecho (artículos R. 321-18 y R. 321-21 del Código de Comercio)

*Para ir más allá*: Artículos R. 321-18, A. 321-3 y A. 321-4 del Código de Comercio.

**Solicitud de acceso a prácticas y formación**

El futuro profesional que posea uno de los diplomas anteriores debe completar un curso de formación accesible después de un examen de acceso.

El examen de acceso a prácticas se lleva a cabo al menos una vez al año e incluye pruebas escritas y orales sobre temas artísticos, legales, económicos y contables, así como inglés y, como opción, otro idioma de vida extranjera.

**Autoridad competente**

La solicitud debe dirigirse al Consejo de Ventas Voluntarias por carta recomendada con notificación de recepción o por cualquier otro medio equivalente a más tardar un mes antes de la fecha de la primera prueba.

**Documentos de apoyo**

Su solicitud debe incluir:

- solicitar el examen de acceso a pasantías;
- prueba de identidad y nacionalidad
- Una copia de sus diplomas o títulos de formación o la justificación de su dispensa;
- el idioma extranjero elegido para la prueba de ingreso a prácticas.

**Tiempo y resultado del procedimiento**

La lista de candidatos elegibles para realizar el examen de acceso a la pasantía es decidida por el Consejo de Ventas Voluntarias tres semanas antes de la fecha de la primera prueba.

El Consejo de Ventas Voluntarias envía un certificado de éxito al examen de acceso a prácticas al candidato que ha obtenido el promedio para las pruebas de admisión.

*Para ir más allá*: Artículos A. 321-10 a A. 321-20 del Código de Comercio.

**Curso de formación**

Con una duración de dos años, de los cuales un mínimo de un año en Francia, el curso de formación consta de:

- educación teórica con el fin de profundizar el conocimiento del candidato en las artes, la tecnología, la economía, la contabilidad y los asuntos legales;
- educación práctica y práctica profesional.

Al final de la pasantía, el Consejo de Ventas Voluntarios otorga al pasante un certificado de buena realización de la pasantía que le permite dirigir las ventas voluntarias de muebles en subastas públicas.

**Tenga en cuenta que**

Las exenciones de pasantías de formación son posibles para algunos profesionales que justifican una práctica profesional de al menos siete años en una o más oficinas de subastador judicial, con uno o más operadores de ventas. subastadores públicos o corredores de materias primas jurados. Estas exenciones sólo son posibles si estos profesionales han pasado con éxito una prueba de aptitud ante un jurado que incluye:

- profesor de historia del arte;
- un curador patrimonial;
- un subastador judicial;
- un corredor de materias primas jurada;
- dos personas autorizadas para llevar a cabo ventas voluntarias de muebles en subastas públicas.

*Para ir más allá*: Artículo R. 321-19 y artículos R. 321-26 a R. 321-31 del Código de Comercio.

#### Costos asociados con la calificación

La formación que conduce al título de subastador voluntario de ventas se paga y su costo varía según el curso profesional previsto. Para más información, es aconsejable consultar con las instituciones interesadas.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Todo nacional de un Estado miembro de la UE o del EEE que esté legalmente establecido y actúe como comisario de ventas voluntarias en ese Estado podrá llevar a cabo la misma actividad en Francia de forma temporal y ocasional.

Para ello, debe hacer una declaración previa al Consejo de Ventas Voluntarias (véase infra "5o. b. Predeclaración para los nacionales de la UE o del EEE para el ejercicio temporal y casual (LPS)).

Cuando ni la actividad ni la formación estén reguladas en el Estado miembro en el que el profesional esté legalmente establecido, deberá haber llevado a cabo esta actividad en uno o varios Estados miembros de la UE durante al menos un año en los últimos diez años anteriores la entrega.

*Para ir más allá*: Artículos L. 321-24 y el siguiente del Código de Comercio.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

Para llevar a cabo la actividad de subastador voluntario de ventas en Francia de forma permanente, el profesional debe:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba: "2. a. Cualificaciones profesionales");
- poseer un certificado de competencia o certificado de formación requerido para el ejercicio de la actividad de subastador en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- distensión de formación que acredite su preparación para el ejercicio de la actividad del subastador cuando se haya obtenido este certificado o título en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de esta actividad;
- tener un certificado de competencia o un documento de formación que certifique la preparación para el ejercicio de la actividad de subastador voluntario de ventas y que justifique también, en un Estado miembro o en un Estado parte en el Acuerdo sobre el Espacio Económico que no regule el acceso o el ejercicio de esta profesión, un ejercicio a tiempo completo de la profesión durante al menos un año en los diez años anteriores o por un período equivalente en el caso de ejercicio a tiempo parcial, siempre que este ejercicio es atestiguado por la autoridad competente de ese Estado;
- reconocimiento de cualificación (véase infra "5 grados). c. Solicitud de reconocimiento de cualificación para el nacional para un ejercicio permanente (LE)).

Cuando la formación recibida por el profesional se refiera a materias sustancialmente diferentes de las exigidas para el ejercicio de la profesión en Francia, el Consejo de Ventas Voluntarias podrá exigir que se someta a una medida de compensación (cf. infra "5 grados. (c) Bueno saber: medidas de compensación").

*Para ir más allá*: Artículos R. 321-65 y el siguiente del Código de Comercio.

3°. Normas profesionales y sanciones penales
-----------------------------------------------------

**Reglas profesionales**

El subastador voluntario debe seguir las siguientes reglas profesionales:

- Probidad
- buenos modales;
- Honor.

Cualquier incumplimiento o acción contraria a estas normas puede ser objeto de una condena o de una prohibición de practicar.

*Para ir más allá*: Artículo L. 321-4 del Código de Comercio.

**Sanciones penales**

Un profesional es castigado con dos años de prisión y una multa de 375.000 euros para que un profesional proceda o tenga una venta voluntaria realizada:

- sin haber hecho la declaración previa de actividad (ver infra "5 grados. a. Predeclaración de actividad");
- sin solicitar la cualificación, cuando el profesional sea nacional de la UE (véase infra "5o c. Solicitud de reconocimiento de cualificación para el nacional de la UE o del EEE para un ejercicio permanente (LE);
- si bien no tiene las cualificaciones necesarias para organizar o llevar a cabo ventas voluntarias en subastas públicas o está prohibido celebrar dicha venta.

El profesional también incurre en sanciones adicionales que incluyen:

- una prohibición de la actividad durante cinco años o más;
- La publicación o difusión de la sentencia;
- la pérdida de dinero u objetos indebidamente recibidos por el profesional.

*Para ir más allá*: Artículo L. 321-15 del Código de Comercio.

4°. Medidas de publicidad y seguros
--------------------------------------------------------

### Medida publicitaria

**Autoridad competente**

Antes de cada venta voluntaria de muebles en subasta pública, el profesional debe tomar medidas de publicidad con el Consejo de Ventas Voluntarias ocho días antes de la venta.

**Documentos de apoyo**

El profesional deberá mencionar toda la información relativa a la organización de la venta, sus medios técnicos y financieros, así como la siguiente información:

- La fecha, ubicación e identidad del profesional que procederá a la venta, así como la fecha de su declaración al Consejo de Ventas Voluntarias;
- La calidad del vendedor como comerciante o artesano cuando el vendedor vende nuevos productos;
- El nuevo carácter de la propiedad;
- Si es así, el estado del propietario de la propiedad puesta a la venta en el momento del propietario es un operador de ventas voluntaria organizadoro o su empleado, gerente o socio, o un experto involucrado en la venta;
- La intervención de un experto en la organización de la venta;
- mención del estatuto de limitaciones para las acciones de responsabilidad civil incurridas durante una venta voluntaria, es decir, un período de cinco años a partir de la subasta o premio.

**Tenga en cuenta que**

El profesional debe mantener un registro electrónico de ventas y un directorio que contenga los minutos de las ventas realizadas todos los días.

*Para ir más allá*: Artículos L. 321-7 y R. 321-32 a R. 321-35 del Código de Comercio.

### Seguro

El profesional que es un subastador de ventas voluntario debe:

- Abrir una cuenta en una institución de crédito para recibir fondos mantenidos en nombre de otros durante su actividad;
- Contrate un seguro de responsabilidad civil profesional
- seguro de compra o un bono para garantizar los fondos recibidos durante su actividad. Estas garantías sólo pueden ser otorgadas por una entidad de crédito, una compañía de financiación autorizada, una compañía de seguros o una compañía de seguros mutuos.

**Tenga en cuenta que**

Los nacionales de la UE o del EEE deben cumplir estas obligaciones para un ejercicio permanente (LE) o temporal y ocasional (LPS).

*Para ir más allá*: Artículos L. 321-6, R. 321-10 a R. 321-17 y R. 321-56 del Código de Comercio.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Pre-declaración de actividad

**Autoridad competente**

El profesional debe enviar una declaración por carta recomendada con notificación de recibo o por desmaterializado al Consejo de Ventas Voluntarias de Muebles en subastas públicas.

**Documentos de apoyo**

Su declaración debe incluir:

- Prueba de identidad
- un certificado que certifique que no es objeto de ninguna condena penal, sanción disciplinaria o prohibición de ejercer;
- Un documento que justifica que las personas responsables de dirigir las ventas estén cualificadas profesionalmente;
- Una copia del arrendamiento o título de los locales donde se lleva a cabo la actividad, así como el último balance o balance de previsión;
- Un documento que justifica la apertura de una cuenta en una entidad de crédito para recibir fondos en nombre de otros;
- Un documento que justifica que el profesional ha contratado un seguro de responsabilidad civil profesional;
- un documento que justifique que el profesional ha contratado un seguro o una fianza que garantice la representación de los fondos mantenidos en nombre de otros.

*Para ir más allá*: Artículos R. 321-1 a R. 321-4 del Código de Comercio.

### b. Predeclaración para el nacional de la UE o del EEE para el ejercicio temporal y casual (LPS)

**Autoridad competente**

El profesional debe presentar, por carta recomendada con previo aviso de recepción o cualquier otro medio escrito, una solicitud de declaración previa al Consejo de Ventas Voluntarias.

**Documentos de apoyo**

La solicitud debe incluir lo siguiente en francés o, si es necesario, con una traducción:

- Prueba de la identidad del solicitante
- cualquier documento que justifique la legalidad del ejercicio del solicitante, la venta voluntaria de muebles en subastas públicas en el Estado miembro, su condición profesional y, en su caso, el nombre del organismo profesional bajo su jurisdicción;
- si ni la formación ni la actividad están reguladas en el Estado miembro, prueba por ningún medio de que haya ejercido durante al menos un año en los últimos diez años la actividad de un subastador voluntario;
- un certificado de menos de tres meses que certifique que no es temporal o temporal para trabajar;
- La fecha y ubicación de la venta prevista y la identidad y calificación del responsable de la venta;
- prueba hace menos de tres meses de que el profesional ha contratado un seguro de responsabilidad profesional o una fianza que garantiza la representación de los fondos mantenidos en nombre de otros.

**hora**

La declaración previa debe hacerse al menos un mes antes de la primera venta. El Consejo de Ventas Voluntarias informa al solicitante, si es necesario, dentro de los quince días siguientes a la recepción de la solicitud de cualquier documento que falte.

**Tenga en cuenta que**

Esta declaración deberá renovarse una vez al año si el profesional tiene previsto ejercer de forma ocasional durante el año o en caso de un cambio en su situación profesional.

*Para ir más allá*: Artículos L. 321-24 y artículos subsiguientes R. 321-56 y el siguiente del Código de Comercio.

### c. Solicitud de reconocimiento de cualificación para los nacionales de la UE o del EEE para el ejercicio permanente (LE)

**Autoridad competente**

El profesional que desee que se le reconozca su cualificación profesional para llevar a cabo la actividad de subastador en Francia de forma permanente deberá solicitar el reconocimiento de la cualificación mediante carta recomendada con notificación de o por cualquier otro medio al Consejo de Ventas Voluntarias.

**Documentos de apoyo**

La solicitud deberá ir acompañada de los siguientes documentos, si los hubiere traducidos al francés:

- prueba de la identidad, nacionalidad y residencia del solicitante;
- Una copia del certificado de competencia o designación de formación del solicitante que le da acceso a la actividad de un subastador voluntario de ventas;
- Si el nacional posee un diploma o documento de formación expedido por un tercer país y reconocido por un país miembro de la UE o el EEE, un certificado expedido por la autoridad competente de ese Estado miembro que certifique la duración del ejercicio profesional de la fechas de solicitante y ejercicio;
- una prueba del ejercicio de la actividad de subastador voluntario en los últimos diez años, ya que ni la formación ni la actividad están reguladas en el Estado miembro;
- prueba de su cualificación profesional
- un documento que justifique que no es objeto de ninguna condena penal, sanción disciplinaria o prohibición del ejercicio de la actividad de un comisionado de ventas voluntario.

**Tiempo y remedios**

La Comisión reconoce la recepción de la solicitud en el plazo de un mes e informa al solicitante si falta una pieza. La decisión de la Junta se envía por carta recomendada al solicitante en un plazo de tres meses a partir de la recepción del expediente completo. La decisión del Consejo está sujeta a recurso por carta recomendada con notificación de recepción en el registro del Tribunal de Apelación de París.

*Para ir más allá*: Artículos A. 321-66 y A. 321-27 del Código de Comercio.

**Bueno saber: medidas de compensación**

El Consejo de Ventas Voluntarias podrá exigir al profesional que se someta a una prueba de selección ante el jurado profesional encargado del examen del acceso al curso de formación, o un curso de adaptación de hasta tres años.

*Para ir más allá*: Artículo R. 321-67 del Código de Comercio.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la Unión Europea o parte en el Acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

