﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP039" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Construcción" -->
<!-- var(title)="Arquitecto" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="construccion" -->
<!-- var(title-short)="arquitecto" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/construccion/arquitecto.html" -->
<!-- var(last-update)="2020-04-15 17:20:44" -->
<!-- var(url-name)="arquitecto" -->
<!-- var(translation)="Auto" -->


Arquitecto
==========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:44<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

El arquitecto es responsable de las diversas fases del diseño y construcción de la obra.

En primer lugar, se requiere realizar un estudio de viabilidad del terreno antes de trazar los primeros planos del futuro edificio. Obtendrá el permiso de construcción y negociará precios con los diversos contratistas que trabajarán en el sitio. A lo largo del proyecto, tendrá que tener en cuenta las normativas de planificación, las limitaciones legales y técnicas, así como el presupuesto del cliente y los requisitos de plazos.

Una vez diseñados los planos, el arquitecto coordina los equipos responsables de la construcción de la obra hasta la entrega de la obra.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El ejercicio de la profesión de arquitecto está reservado a los inscritos en la tabla regional de arquitectos. La inscripción está abierta a los titulares del Diploma de Arquitecto Estatal que hayan completado un año adicional de formación que conduzca a la certificación para practicar el máster en su propio nombre (HMONP).

*Para ir más allá*: Artículo 9 de la Ley 77-2, de 3 de enero de 1977, de arquitectura.

#### Entrenamiento

La formación en arquitectura se imparte en 20 escuelas superiores nacionales de arquitectura de toda Francia, en el Instituto Superior Nacional de Ciencias Aplicadas (INSA) de Estrasburgo y en la Escuela Especial de Arquitectura (ESA) de París.

Es accesible después de la graduación y dura cinco años. El acceso se realiza después de revisar el archivo del candidato, con una entrevista con posibles pruebas.

Los estudios consisten en dos ciclos:

- un título de pregrado de tres años, que conduce al Diploma de Estudios de Arquitectura (DEEA);
- un segundo ciclo, en dos años (nivel maestro), que conduce al Diploma Estatal de Arquitecto (DEA).

#### Costos asociados con la calificación

Los costes de formación oscilan entre 300 y 500 euros al año para los colegios públicos. Los costes de la AEE son de unos 9.000 euros al año. Para más información, es aconsejable acercarse a las diferentes escuelas que proporcionan la formación.

### b. Nacionales de la UE o del EEE: para ejercicios temporales y ocasionales (Entrega gratuita de servicios)

Un ciudadano de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) que actúe legalmente como arquitecto en uno de estos Estados puede utilizar su título profesional en Francia, ya sea temporal u ocasionalmente.

Tendrá que solicitarlo, antes de su actuación, mediante declaración escrita al Consejo Regional de la Orden de Arquitectos (véase infra "5o. a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)").

En caso de diferencias sustanciales entre sus cualificaciones profesionales y la formación requerida en Francia, el consejo podrá someterlo a una prueba de aptitud que consiste en una prueba oral de treinta minutos. Esto cubrirá la totalidad o parte de los materiales cubiertos por el[Artículo 9](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=FBD3A253B7755AFB8CE594DDFEF3610E.tplgfr34s_3?idArticle=LEGIARTI000021673054&cidTexte=LEGITEXT000021673028&dateTexte=20180214) del decreto de 17 de diciembre de 2009 sobre las modalidades de reconocimiento de cualificaciones profesionales para el ejercicio de la profesión de arquitecto. Dependiendo del tema, se le puede pedir al nacional un proyecto arquitectónico para evaluar sus habilidades.

*Para ir más allá*: Artículo 10-1 de la Ley 77-2, de 3 de enero de 1977, de arquitectura; Artículos 10 a 14 del Decreto No 2009-1490 de 2 de diciembre de 2009 sobre el reconocimiento de cualificaciones profesionales para el ejercicio de la profesión de arquitecto.

### c. Nacionales de la UE o del EEE: para un ejercicio permanente (establecimiento libre)

El nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si tiene:

- el diploma estatal de arquitecto u otro diploma francés de arquitecto reconocido por ese estado, y el HMONP;
- un diploma, certificado u otro título extranjero reconocido por ese Estado que permita el ejercicio de la profesión de arquitecto y reconocida por el Estado.

En ambos casos, podrá solicitar directamente su inscripción en la lista regional de arquitectos ante el Consejo Regional de la Orden de Arquitectos (véase infra "5o. c. Solicitar la inscripción en la lista de arquitectos regionales para el ejercicio permanente (LE) de la UE o del EEE).

También puede establecerse en Francia de forma permanente, la UE o el nacional del EEE que:

- posee un certificado de formación expedido por un tercer Estado pero reconocido por un Estado de la UE o del EEE y ha permitido ejercer la profesión de arquitecto durante al menos tres años;
- fue reconocido como calificado por el Ministro de Cultura, después de una revisión de todos sus conocimientos, cualificaciones y experiencia profesional;
- fue reconocido como calificado por el Ministro de Cultura después de presentar referencias profesionales que atestian que se distinguió por la calidad de sus logros arquitectónicos, tras el asesoramiento de una comisión nacional.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar el reconocimiento de sus cualificaciones profesionales antes de solicitar el registro en la junta (véase más adelante "5o). b. Si es necesario, solicite el reconocimiento previo de sus cualificaciones profesionales para la UE o el Nacional del EEE para su inclusión en la lista de arquitectos regionales").

Si existen diferencias sustanciales entre su formación y las cualificaciones profesionales requeridas en Francia, el nacional podrá ser sometido a una prueba de aptitud organizada en un plazo de seis meses a partir de su decisión.

*Para ir más allá*: Artículo 10 de la Ley 77-2, de 3 de enero de 1977, de arquitectura; Artículos 1 a 9 del Decreto No 2009-1490 de 2 de diciembre de 2009 sobre el reconocimiento de cualificaciones profesionales para el ejercicio de la profesión arquitectónica.

3°. Condiciones de honorabilidad, reglas éticas, ética
---------------------------------------------------------------

Las disposiciones del Código de ética se imponen a todos los arquitectos que practican en Francia.

Como tal, el arquitecto debe respetar los principios de respeto a la confidencialidad profesional, la competencia y los conflictos de intereses.

Para más información, es aconsejable consultar el[orden del sitio web de arquitectos](https://www.architectes.org/code-de-deontologie-des-architectes).

*Para ir más allá*: Artículos 18 y 19 de la Ley 77-2 del 3 de enero de 1977 sobre arquitectura.

4°. Seguro
-------------------------------

El arquitecto que proactúe sobre una base liberal debe tomar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

*Para ir más allá*: Artículo 16 de la Ley 77-2, de 3 de enero de 1977, de arquitectura.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)

**Autoridad competente**

El Consejo Regional de la Orden de Arquitectos del lugar de ejecución del servicio es competente para decidir sobre la solicitud de declaración previa.

**Documentos de apoyo**

La solicitud de declaración previa de actividad se realiza mediante la presentación de un archivo que incluye:

- una declaración en la que se mencione la intención del nacional de prestar servicios en Francia;
- Un certificado de seguro civil profesional del nacional de menos de tres meses;
- Una copia de los títulos de formación
- Una copia del certificado que certifique que el nacional está establecido en un Estado de la UE o del EEE y no está comprendido en la prohibición de ejercer;
- una copia de un documento de identidad válido.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

La autoridad competente dispone de un mes a partir de la recepción de la declaración para decidir sobre la solicitud de declaración. Cuando el solicitante esté obligado a pasar una prueba de aptitud, el consejo regional tendrá un mes adicional para decidir.

*Para ir más allá*: Artículos 14 a 17 del Decreto del 17 de diciembre de 2009 sobre el reconocimiento de cualificaciones profesionales para el ejercicio de la profesión arquitectónica.

### b. Si es necesario, solicitar el reconocimiento previo de sus cualificaciones profesionales para la UE o el NACIONAL del EEE en vista de su inscripción en la lista de arquitectos regionales

**Autoridad competente**

El Ministro de Cultura es responsable de decidir sobre las solicitudes de reconocimiento de las cualificaciones profesionales del nacional.

**Documentos de apoyo**

La solicitud de reconocimiento deberá dirigirse a la autoridad competente y presentarse por expediente, en dos copias, que contenga los siguientes documentos:

- Una copia de un documento de identidad válido
- Una copia de los títulos de formación
- En su caso, una copia de la descripción detallada del plan de estudios seguido y su volumen por hora, así como una copia de la autorización para llevar el título de arquitecto;
- En su caso, una copia del título de formación expedido por un tercer Estado pero reconocido por un Estado de la UE o del EEE, así como una copia:- ya sea la certificación expedida por el Estado de la UE o el EEE que acredite el ejercicio de la profesión de arquitectura durante al menos tres años en ese Estado,
  - cualquier documento expedido por el Estado de la UE o el EEE que acredite el ejercicio de la profesión arquitectónica;
- una descripción de la formación y la experiencia laboral relevante para el ejercicio de la actividad.

En el caso del nacional que se habría distinguido por la calidad de sus logros en el campo de la arquitectura, el expediente incluirá:

- Un CV
- Una carta que describe sus motivaciones
- una colección que presenta una variada selección de estudios y proyectos realizados que muestran su aspecto exterior, diseño interior e inserción en el sitio, ilustrados con fotografías, planos y cortes, así como bocetos y dibujos;
- Una lista de referencias al trabajo y estudios realizados;
- Una copia de artículos y publicaciones dedicados a los logros presentados o a su autor;
- Certificados y recomendaciones de empleadores y clientes
- un certificado expedido por la UE o el Estado del EEE que certifique que las actividades del solicitante son arquitectónicas.

**Procedimiento**

La autoridad competente confirmará la recepción del expediente en el plazo de un mes. Una vez recibido el expediente completo, el Ministro responsable de la cultura dispondrá de cuatro meses para tomar su decisión, previa asesoría del Consejo Nacional de la Orden de Arquitectos.

En caso de diferencias sustanciales entre la formación y las cualificaciones requeridas en Francia, podrá decidir someter al nacional a la misma prueba de aptitud que la requerida en el caso de la prestación gratuita de servicios (véase más arriba "2. b. Nacionales de la UE o del EEE: para un ejercicio temporal e informal (Entrega gratuita de servicios)").

*Para ir más allá*: Artículos 3 a 7 del Decreto No 2009-1490, de 2 de diciembre de 2009, sobre el reconocimiento de cualificaciones profesionales para el ejercicio de la profesión de arquitectura; Artículos 2 a 13 del Decreto de 17 de diciembre de 2009 sobre las modalidades de reconocimiento de cualificaciones profesionales para el ejercicio de la profesión de arquitecto.

### c. Solicitar la inscripción en la lista de arquitectos regionales para la UE o el EEE nacional con vistas a un ejercicio permanente (LE)

**Autoridad competente**

El Consejo Regional del Colegio de Arquitectos es responsable de decidir sobre las solicitudes para inscribirse en el consejo de la Orden.

**Documentos de apoyo**

La solicitud se realiza enviando un archivo en dos copias con los siguientes documentos justificativos:

- Una copia de un documento de identidad válido
- Una copia del título de formación
- un certificado del seguro civil profesional del nacional, de menos de tres meses de edad;
- un extracto de los antecedentes penales.

**Procedimiento**

El consejo regional confirmará la recepción del expediente en el plazo de un mes. El silencio del consejo en un plazo de dos meses valdrá la pena la decisión de rechazar la solicitud.

*Para ir más allá*: Artículo 1 del Decreto de 17 de diciembre de 2009 sobre el reconocimiento de las cualificaciones profesionales para el ejercicio de la profesión arquitectónica.

### d. Remedios

#### Centro de asistencia francés

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

#### Solvit

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un[formulario de queja en línea](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

