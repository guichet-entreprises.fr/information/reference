﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP125" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Oficios animales" -->
<!-- var(title)="Mantenimiento de mascotas de especies domésticas" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="oficios-animales" -->
<!-- var(title-short)="mantenimiento-de-mascotas-de-especies" -->
<!-- var(url)="https://www.guichet-qualifications.fr/es/dqp/oficios-animales/mantenimiento-de-mascotas-de-especies-domesticas.html" -->
<!-- var(last-update)="2020-04-15 17:20:53" -->
<!-- var(url-name)="mantenimiento-de-mascotas-de-especies-domesticas" -->
<!-- var(translation)="Auto" -->


Mantenimiento de mascotas de especies domésticas
================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:20:53<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

La actividad de mantenimiento de mascotas de especies domésticas consiste en que el profesional se asegure, entre otras cosas:

- Tránsito de mascotas
- custodia de ellos;
- su educación
- su formación;
- presentación de perros y gatos al público.

**Tenga en cuenta que**

Las mascotas son las que están destinadas a ser propiedad de los seres humanos para su disfrute.

*Para ir más allá*: Artículo L. 214-6 del Código Rural y Pesca Marina.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Requisitos nacionales

#### Legislación nacional

El mantenimiento de las mascotas de especies domésticas sólo puede llevarse a cabo si se cumplen las siguientes condiciones:

- la actividad fue declarada por el profesional al prefecto (véase infra "5 grados. a. Declaración de actividad en relación con las mascotas");
- las instalaciones cumplen con las normas sanitarias y de protección animal (véase el decreto de 3 de abril de 2014 por el que se establecen las normas sanitarias y de protección animal a las que deben cumplirse las actividades relacionadas con las mascotas domésticas Sección L.214-6 del Código Rural y Pesca Marítima);
- al menos uno de los profesionales en contacto directo con los animales es el propietario de:- certificación profesional
  - un certificado de conocimientos relativos a las necesidades biológicas, fisiológicas, de comportamiento y de mantenimiento de mascotas expedido por una institución aprobada por el Ministro de Agricultura,
  - un certificado de capacidad para animales domésticos expedido antes del 1 de enero de 2016.

*Para ir más allá*: Artículo L. 214-6 y siguiente del Código Rural y Pesca Marina; Orden de 4 de febrero de 2016 relativa a la acción de formación y actualización de los conocimientos necesarios para las personas que realizan actividades relacionadas con las mascotas de especies domésticas y la autorización de organizaciones de formación.

#### Entrenamiento

Varias vías permiten al profesional adquirir los conocimientos necesarios para llevar a cabo la actividad de mantenimiento de animales domésticos.

**Certificación profesional**

La certificación profesional del interesado se define como uno de los títulos o diplomas[Apéndice II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E4347A807573D4D41A0A98B2B4ED58E7.tplgfr30s_1?idArticle=LEGIARTI000032095076&cidTexte=LEGITEXT000032095054&dateTexte=20180301) 4 de febrero de 2016.

**Certificado de conocimiento**

El profesional debe registrarse en un[organismo autorizado](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=BFA3E3D05AE04A529A4B56731FEACAA5.tplgfr26s_3?cidTexte=JORFTEXT000029966097&dateTexte=29990101) y realizar el llamado entrenamiento adaptativo para estar al tanto de las necesidades biológicas, fisiológicas, de comportamiento y de mantenimiento de las mascotas.

No se requiere ningún nivel de conocimiento para acceder a esta formación, que tiene una duración mínima de:

- catorce horas para una categoría de animales;
- dieciocho horas para dos categorías;
- veintidós horas para tres categorías.

El programa de evaluación se establece en el[Apéndice I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=AC0CC0F524B8E7C665AC199F37F21542.tplgfr26s_3?idArticle=LEGIARTI000032095074&cidTexte=LEGITEXT000032095054&dateTexte=20180302) 4 de febrero de 2016.

Al final de la formación, el profesional se sometió a un examen en forma de un cuestionario de opción múltiple (MQC) y, si tiene éxito, es expedido por el organismo pertinente un certificado de formación que menciona las categorías de animales cubiertos por la acción de Entrenamiento. A continuación, la organización de formación deberá presentar las actas de la sesión de evaluación a la Dirección Regional de Alimentación, Agricultura y Silvicultura (DRAAF) para que proporcione el certificado de conocimientos al profesional.

*Para ir más allá*: Artículo R. 214-27-1 del Código Rural y de la pesca marina; Artículo 3 del Decreto de 4 de febrero de 2016 supra; decreto de 19 de diciembre de 2014 por el que se modifica el decreto de 25 de noviembre de 2014 por el que se publica la lista de organismos de formación con derecho a aplicar la acción de formación profesional continua para las personas que se dedican a actividades relacionadas con animales de empresa de especies domésticas.

**Certificado de capacidad para mascotas (CCAD)**

También puede justificar el conocimiento del profesional titular:

- Una CAD emitida antes del 1 de enero de 2016;
- un título o certificado mencionado en el[Apéndice III](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1151FD9E2F570AE3B106AEB6DE70C0AF.tplgfr26s_3?idArticle=LEGIARTI000032095078&cidTexte=LEGITEXT000032095054&dateTexte=20180302) del auto de 4 de febrero de 2016 relativo a la acción de formación y a la actualización de los conocimientos necesarios para las personas que realizan actividades relacionadas con las mascotas de especies domésticas y la autorización de organizaciones de formación.

**Formación continua**

*Para ir más allá* :D, de 4 de febrero de 2016, relativa a la acción de formación y a la actualización de los conocimientos necesarios para las personas que se dedican a actividades relacionadas con las mascotas de especies domésticas y el empoderamiento de las organizaciones de formación.

### b. Nacionales de la UE: para el ejercicio temporal e informal (LPS)

Todo nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido y lleve a cabo una actividad nacional de mantenimiento de animales podrá, de forma temporal y ocasional, llevar a cabo Francia, la misma actividad.

Para ello, tendrá que enviar una declaración antes de la ejecución de su primera actuación (véase infra "5o. a. Solicitud de declaración previa de actividad para el nacional de la UE para un ejercicio temporal e informal (LPS)).

En el caso de que la profesión no esté regulada, ya sea en el curso de la actividad o en su formación, en el país en el que el profesional esté legalmente establecido, deberá haber llevado a cabo esta actividad en uno o más Estados miembros durante al menos un año durante la última década.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, el profesional tendrá que justificar la pérdida de conocimientos, habilidades y habilidades.

*Para ir más allá* :Artículo L. 204-1 del Código Rural y Pesca Marina.

### c. Nacionales de la UE: para un ejercicio permanente (LE)

No existen disposiciones especiales para el nacional de la UE que deseen llevar a cabo el mantenimiento permanente de las mascotas domésticas en Francia.

Como tal, el profesional está sujeto a los mismos requisitos que el nacional francés (véase más arriba "2." a. Requisitos nacionales").

3°. Prohibiciones
--------------------------

El profesional debe tener cuidado durante su actividad profesional de no:

- privar o maltratar a los animales que mantiene;
- Utilizar un sistema de apego o privación de libertad que no sea adecuado para sus necesidades;
- vender una mascota a un menor de dieciséis años sin el consentimiento de la persona que tiene la patria potestad*Para ir más allá*: Artículo R. 214-19-1 y siguientes del Código Rural y Pesca Marina.

4°. Seguro
-------------------------------

El profesional que proactúe a nivel liberal debe disponer de un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

5°. Procedimientos y formalidades de reconocimiento de cualificación
------------------------------------------------------------------------------

### a. Declaración de actividad en relación con las mascotas

**Autoridad competente**

El profesional debe enviar su declaración a la Dirección Departamental de Protección de la Población (DDPP) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde desea establecerse.

**Documentos de apoyo**

Debe enviar al organismo correspondiente al formulario[Cerfa 15045*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15045.do) completado y firmado. Este procedimiento también se puede hacer en línea en el[Sitio](https://agriculture-portail.6tzen.fr/default/requests/Cerfa15045/) Departamento de Agricultura.

**Procedimiento**

Una vez recibida la declaración, el prefecto envía al solicitante un recibo de una declaración.

*Para ir más allá*: Artículo L. 214-6-1 del Código Rural y Pesca Marina .

### b. Solicitud de declaración previa de actividad para los nacionales de la UE para el ejercicio temporal y casual (LPS)

**Autoridad competente**

El profesional debe solicitar al Director Regional de Alimentación, Agricultura y Silvicultura.

**Documentos de apoyo**

El profesional debe transmitir por cualquier medio lo siguiente:

- Prueba de la identidad del profesional
- un certificado que certifique que está legalmente establecido en un Estado miembro de la UE, que está llevando a cabo el mantenimiento de animales domésticos y que no está sujeto a ninguna prohibición de ejercer;
- prueba de sus cualificaciones profesionales
- evidencia por cualquier medio de que ha estado involucrado en esta actividad durante al menos un año en los últimos diez años en el caso de que ni la actividad ni la formación estén reguladas en ese estado.

**Procedimiento**

La autoridad competente podrá someter al profesional a una prueba de aptitud en caso de diferencias sustanciales entre su formación y la formación necesaria para el ejercicio en Francia de esta actividad.

Merece la pena aceptar el silencio por parte de la autoridad competente en el plazo de un mes.

**Tenga en cuenta que**

Esta declaración debe renovarse cada año y, en su caso, con cada cambio en la situación del profesional. En caso de incumplimiento de esta obligación declarativa, el nacional está sujeto a una multa de 1500 euros (o 3000 euros en caso de reincidencia).

*Para ir más allá*: Artículos L. 214-6-1 y R. 204-1 del Código de Pesca Rural y Marina.

### c. Remedios

**Centro de asistencia francés**

El Centro ENIC-NARIC es el centro francés de información sobre el reconocimiento académico y profesional de diplomas.

**Solvit**

SOLVIT es un servicio prestado por la Administración Nacional de cada Estado miembro de la UE o parte en el acuerdo EEE. Su objetivo es encontrar una solución a una disputa entre un nacional de la UE y la administración de otro de estos Estados. SOLVIT interviene en particular en el reconocimiento de cualificaciones profesionales.

**Condiciones**

El interesado sólo puede utilizar SOLVIT si establece:

- que la administración pública de un Estado de la UE no ha respetado sus derechos en virtud del Derecho de la UE como ciudadano o empresarial de otro Estado de la UE;
- que aún no ha iniciado acciones legales (la acción administrativa no se considera como tal).

**Procedimiento**

El nacional debe completar un formulario de queja en línea.

Una vez que su expediente ha sido enviado, SOLVIT se pone en contacto con él dentro de una semana para solicitar, si es necesario, información adicional y para verificar que el problema es de su competencia.

**Documentos de apoyo**

Para entrar en SOLVIT, el nacional debe comunicar:

- Datos de contacto completos
- Descripción detallada de su problema
- todas las pruebas del expediente (por ejemplo, correspondencia y decisiones recibidas de la autoridad administrativa pertinente).

**hora**

SOLVIT se compromete a encontrar una solución dentro de las diez semanas siguientes al día en que el caso fue asumido por el centro SOLVIT en el país en el que se produjo el problema.

**Costo**

Gratis.

**Resultado del procedimiento**

Al final del período de 10 semanas, SOLVIT presenta una solución:

- Si esta solución resuelve el litigio sobre la aplicación del Derecho europeo, se acepta la solución y se cierra el caso;
- si no hay solución, el caso se cierra como no resuelto y se remite a la Comisión Europea.

**Más información**

SOLVIT en Francia: Secretaría General de Asuntos Europeos, 68 rue de Bellechasse, 75700 París ([sitio web oficial](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

