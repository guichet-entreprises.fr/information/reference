﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP125" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Animal occupations" -->
<!-- var(title)="Care of pets of domestic species" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="animal-occupations" -->
<!-- var(title-short)="care-of-pets-of-domestic-species" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/animal-occupations/care-of-pets-of-domestic-species.html" -->
<!-- var(last-update)="2020-04-15 17:20:53" -->
<!-- var(url-name)="care-of-pets-of-domestic-species" -->
<!-- var(translation)="Auto" -->


Care of pets of domestic species
=======================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:20:53<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The maintenance activity of pets of domestic species consists for the professional to ensure among other things:

- Transit of pets
- custody of them;
- their education
- their training;
- presentation of dogs and cats to the public.

**Please note**

Pets are those intended to be owned by humans for their enjoyment.

*For further information*: Article L. 214-6 of the Rural Code and Marine Fisheries.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The maintenance of pets of domestic species can only be carried out if the following conditions are met:

- the activity was declared by the professional to the prefect (see infra "5°. a. Declaration of activity in relation to pets");
- facilities comply with sanitary and animal protection rules (see decree of 3 April 2014 setting out the sanitary and animal protection rules to which activities related to domestic pets must be satisfied Section L.214-6 of the Rural Code and Maritime Fisheries);
- at least one of the professionals in direct contact with the animals is the owner of:- professional certification
  - a certificate of knowledge relating to biological, physiological, behavioural and pet maintenance needs issued by an institution approved by the Minister for Agriculture,
  - a certificate of capacity for domestic animals issued before January 1, 2016.

*For further information*: Article L. 214-6 and the following of the Rural Code and Marine Fisheries; Order of 4 February 2016 relating to the training action and updating of the necessary knowledge for persons engaged in activities related to pets of domestic species and the authorisation of training organizations.

#### Training

Several pathways allow the professional to acquire the knowledge necessary to carry out the maintenance activity of domestic animals.

**Professional certification**

The professional certification of the person concerned is defined as one of the titles or diplomas listed at the[Appendix II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E4347A807573D4D41A0A98B2B4ED58E7.tplgfr30s_1?idArticle=LEGIARTI000032095076&cidTexte=LEGITEXT000032095054&dateTexte=20180301) February 4, 2016.

**Certificate of knowledge**

The professional must register with a[authorized body](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=BFA3E3D05AE04A529A4B56731FEACAA5.tplgfr26s_3?cidTexte=JORFTEXT000029966097&dateTexte=29990101) and perform so-called adaptive training to be aware of the biological, physiological, behavioural and maintenance needs of pets.

No level of knowledge is required to access this training, which has a minimum duration of:

- fourteen hours for a category of animals;
- eighteen hours for two categories;
- twenty-two hours for three categories.

The evaluation programme is set at the[Appendix I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=AC0CC0F524B8E7C665AC199F37F21542.tplgfr26s_3?idArticle=LEGIARTI000032095074&cidTexte=LEGITEXT000032095054&dateTexte=20180302) February 4, 2016.

At the end of the training, the professional undergone an examination in the form of a multiple choice questionnaire (MQC) and, if successful, is issued by the relevant body a certificate of training mentioning the categories of animals covered by the action of Training. The training organization must then submit the minutes of the evaluation session to the Regional Directorate of Food, Agriculture and Forestry (DRAAF) to provide the certificate of knowledge to the professional.

*For further information*: Article R. 214-27-1 of the Rural code and marine fisheries; Article 3 of the decree of 4 February 2016 above; decree of 19 December 2014 amending the decree of 25 November 2014 publishing the list of training bodies entitled to implement the action of continuing vocational training for persons engaged in activities related to animals of domestic species company.

**Pet Capacity Certificate (CCAD)**

Can also justify the knowledge of the titular professional:

- A CCAD issued before January 1, 2016;
- a title or certificate mentioned in the[Appendix III](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1151FD9E2F570AE3B106AEB6DE70C0AF.tplgfr26s_3?idArticle=LEGIARTI000032095078&cidTexte=LEGITEXT000032095054&dateTexte=20180302) of the order of 4 February 2016 relating to the training action and the updating of the necessary knowledge for persons engaged in activities related to pets of domestic species and the authorisation of training organizations.

**Continuous training**

*For further information*: :Decree of 4 February 2016 relating to the training action and the updating of the necessary knowledge for people engaged in activities related to pets of domestic species and the empowerment of training organizations.

### b. EU nationals: for temporary and casual exercise (LPS)

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement and carrying out a domestic animal maintenance activity may, on a temporary and casual basis, carry out France, the same activity.

In order to do so, he will have to send a statement before the execution of his first performance (see infra "5°. a. Request for prior declaration of activity for the EU national for a temporary and casual exercise (LPS)).

In the event that the profession is not regulated, either in the course of the activity or its training, in the country in which the professional is legally established, he must have carried out this activity in one or more member states for at least one year in the over the past decade.

Where there are substantial differences between the professional qualification of the national and the training required in France, the professional will have to justify having acquired the missing knowledge, skills and skills.

*For further information*: :Article L. 204-1 of the Rural Code and Marine Fisheries.

### c. EU nationals: for a permanent exercise (LE)

There are no special provisions for the EU national wishing to carry out the maintenance of domestic pets on a permanent basis in France.

As such, the professional is subject to the same requirements as the French national (see above "2." a. National requirements").

3°.Bans
-------------------

The professional must be careful during his professional activity not to:

- deprive or mistreat the animals he maintains;
- Use a system of attachment or deprivation of liberty that is unsuitable for their needs;
- sell a pet to a minor under the age of sixteen without the consent of the person holding parental authority*For further information*: Article R. 214-19-1 and following of the Rural Code and Marine Fisheries.

4°. Insurance
---------------------------------

The professional practising in a liberal capacity must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Statement of activity in relation to pets

**Competent authority**

The professional must send his statement to the Departmental Directorate of Population Protection (DDPP) or the Departmental Directorate of Social Cohesion and Population Protection (DDCSPP) of the department where he wishes to settle.

**Supporting documents**

It must send the relevant body to the form[Cerfa 15045*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15045.do) completed and signed. This procedure can also be done online on the[Site](https://agriculture-portail.6tzen.fr/default/requests/Cerfa15045/) Department of Agriculture.

**Procedure**

Once the declaration has been received, the prefect sends the applicant a receipt of a declaration.

*For further information*: Article L. 214-6-1 of the Rural Code and Marine Fisheries .

### b. Request for prior declaration of activity for EU nationals for temporary and casual exercise (LPS)

**Competent authority**

The professional must apply to the Regional Director of Food, Agriculture and Forestry.

**Supporting documents**

The professional must pass on by any means the following:

- Proof of the professional's identity
- a certificate certifying that it is legally established in an EU Member State, that it is carrying out the maintenance of domestic animals and that it is not subject to any prohibition on practising;
- proof of his professional qualifications
- evidence by any means that he has been engaged in this activity for at least one year in the last ten years in the event that neither activity nor training is regulated in that state.

**Procedure**

The competent authority may subject the professional to an aptitude test in case of substantial differences between his training and the training required for the exercise in France of this activity.

Silence on the part of the competent authority within one month is worth accepting the application.

**Please note**

This declaration must be renewed every year and, if necessary, with each change in the professional's situation. In the event of a breach of this declaratory obligation, the national is liable to a fine of 1500 euros (or 3000 euros in the event of a repeat offence).

*For further information*: Articles L. 214-6-1 and R. 204-1 of the Rural and Marine Fisheries Code.

### c. Remedies

**French assistance centre**

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

**Solvit**

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form.

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

