﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP125" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Métiers animaliers" -->
<!-- var(title)="Entretien des animaux de compagnie d'espèces domestiques" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="metiers-animaliers" -->
<!-- var(title-short)="entretien-des-animaux-de-compagnie" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/entretien-des-animaux-de-compagnie-d-especes-domestiques.html" -->
<!-- var(last-update)="2020-04-15 17:20:53" -->
<!-- var(url-name)="entretien-des-animaux-de-compagnie-d-especes-domestiques" -->
<!-- var(translation)="None" -->

# Entretien des animaux de compagnie d'espèces domestiques

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:20:53<!-- end-var -->

## 1°. Définition de l’activité

L'activité d'entretien des animaux de compagnie d'espèces domestiques consiste pour le professionnel à assurer notamment :

- le transit d'animaux domestiques ;
- leur garde ;
- leur éducation ;
- leur dressage ;
- la présentation au public de chiens et chats.

**À noter**

Les animaux domestiques sont ceux destinés à être détenus par l'homme pour son agrément.

*Pour aller plus loin* : article L. 214-6 du Code rural et de la pêche maritime.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'activité d'entretien des animaux de compagnie d'espèces domestiques ne peut être exercée que si les conditions suivantes sont remplies :

- l'activité a été déclarée par le professionnel auprès du préfet (cf. infra « 5°. a. Déclaration d'activité en lien avec les animaux domestiques ») ;
- les installations sont conformes aux règles sanitaires et de protection des animaux (cf. arrêté du 3 avril 2014 fixant les règles sanitaires et de protection animale auxquelles doivent satisfaire les activités liées aux animaux de compagnie d'espèces domestiques relevant du IV de l'article L.214-6 du Code rural et de la pêche maritime) ;
- au moins un des professionnels en contact direct avec les animaux est titulaire soit :
  - d'une certification professionnelle,
  - d'une attestation de connaissance relative aux besoins biologiques, physiologiques, comportementaux et à l'entretien des animaux de compagnie délivrée par un établissement agréé par le ministre chargé de l'agriculture,
  - d'un certificat de capacité des animaux d'espèces domestiques délivré avant le 1er janvier 2016.

*Pour aller plus loin* :article L. 214-6 et suivants du Code rural et de la pêche maritime ; arrêté du 4 février 2016 relatif à l'action de formation et à l'actualisation des connaissances nécessaires aux personnes exerçant des activités liées aux animaux de compagnie d'espèces domestiques et à l'habilitation des organismes de formation.

#### Formation

Plusieurs voies permettent au professionnel d'acquérir les connaissances nécessaires à l'exercice de l'activité d'entretien des animaux d'espèces domestiques.

##### Certification professionnelle

La certification professionnelle de l'intéressé s'entend de l'un des titres ou diplômes dont la liste est fixée à l'annexe II de l'arrêté du 4 février 2016 précité.

##### Attestation de connaissance

Le professionnel doit s'inscrire auprès d'un organisme habilité et effectuer une formation dite d'adaptation en vue d'être sensibilisé aux besoins biologiques, physiologiques, comportementaux et à l'entretien des animaux de compagnie.

Aucun niveau de connaissances n'est requis pour accéder à cette formation dont la durée minimale est de :

- quatorze heures pour une catégorie d'animaux ;
- dix huit heures pour deux catégories ;
- vingt deux heures pour trois catégories.

Le programme d'évaluation est fixé à l'annexe I de l'arrêté du 4 février 2016 précité.

À l'issue de la formation, le professionnel subi un examen sous forme de questionnaire à choix multiples (QCM) et en cas de réussite, se voit délivrer par l'organisme compétent une attestation de formation mentionnant les catégories d'animaux visées par l'action de formation. L'organisme de formation doit ensuite transmettre le procès-verbal de la session d'évaluation à la direction régionale de l'alimentation, de l'agriculture et de la forêt (DRAAF) pour qu'elle remette l'attestation de connaissances au professionnel.

*Pour aller plus loin* :article R. 214-27-1 du Code rural et de la pêche maritime ; article 3 de l'arrêté du 4 février 2016 susvisé ; arrêté du 19 décembre 2014 modifiant l'arrêté du 25 novembre 2014 portant publication de la liste des organismes de formation habilités à mettre en œuvre l'action de formation professionnelle continue pour les personnes exerçant des activités liées aux animaux de compagnie d'espèces domestiques.

##### Certificat de capacité des animaux d'espèces domestiques (CCAD)

Peut également justifier de ses connaissances le professionnel titulaire :

- d'un CCAD délivré avant le 1er janvier 2016 ;
- d'un titre ou certificat mentionné à l'annexe III de l'arrêté du 4 février 2016 relatif à l'action de formation et à l'actualisation des connaissances nécessaires aux personnes exerçant des activités liées aux animaux de compagnie d'espèces domestiques et à l'habilitation des organismes de formation.

*Pour aller plus loin* :arrêté du 4 février 2016 relatif à l'action de formation et à l'actualisation des connaissances nécessaires aux personnes exerçant des activités liées aux animaux de compagnie d'espèces domestiques et à l'habilitation des organismes de formation.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (LPS)

Tout ressortissant d'un État membre de l'Union Européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant une activité d'entretien d'animaux domestiques peut exercer, à titre temporaire et occasionnel, en France, la même activité.

Pour cela, il devra adresser une déclaration préalablement à l'exécution de sa première prestation (cf. infra « 5°. a. Demande de déclaration préalable d'activité pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS) »).

Dans le cas où la profession n'est pas réglementée, ni dans le cadre de l'activité, ni de sa formation, dans le pays dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité dans un ou plusieurs État(s) membre(s) pendant au moins un an au cours des dix dernières années.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, le professionnel devra justifier avoir acquis les connaissances, aptitudes et compétences manquantes.

*Pour aller plus loin* :article L. 204-1 du Code rural et de la pêche maritime.

### c. Ressortissants UE : en vue d’un exercice permanent (LE)

Aucune disposition particulière n'est prévue pour le ressortissant UE souhaitant exercer l'activité d'entretien d'animaux de compagnie d'espèces domestiques à titre permanent en France.

À ce titre, le professionnel est soumis aux mêmes exigences que le ressortissant français (cf. supra « 2°. a. Exigences nationales »).

## 3°. Interdictions

Le professionnel doit veiller au cours de son activité professionnelle à ne pas :

- priver de soin ni maltraiter les animaux dont il assure l'entretien ;
- utiliser de système d'attache ou de privation de liberté inadaptés à leurs besoins ;
- vendre un animal de compagnie à un mineur de moins de seize ans sans accord de la personne titulaire de l'autorité parentale.

*Pour aller plus loin* :article R. 214-19-1 et suivants du Code rural et de la pêche maritime.

## 4°. Assurances

Le professionnel exerçant à titre libéral doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Déclaration d'activité en lien avec des animaux domestiques

#### Autorité compétente

Le professionnel doit adresser sa déclaration à la Direction départementale de la protection des populations (DDPP) ou de la Direction départementale de la cohésion sociale et de la protection des populations (DDCSPP) du département où il souhaite s'établir.

#### Pièces justificatives

Il doit adresser à l'autorité compétente le formulaire [Cerfa 15045*02](https://www.formulaires.service-public.fr/gf/cerfa_15045.do) complété et signé. Cette procédure peut également se faire en ligne sur le [site](https://agriculture-portail.6tzen.fr/default/requests/Cerfa15045/) des services du ministère de l'agriculture.

#### Procédure

Une fois la déclaration reçue complétée, le préfet adresse au demandeur un récépissé de déclaration.

*Pour aller plus loin* :article L. 214-6-1 du Code rural et de la pêche maritime .

### b. Demande de déclaration préalable d'activité pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le professionnel doit adresser une demande au directeur régional de l'alimentation, de l'agriculture et de la forêt.

#### Pièces justificatives

Le professionnel doit transmettre par tout moyen les éléments suivants :

- un justificatif d'identité du professionnel ;
- une attestation certifiant qu'il est légalement établi dans un État membre de l'UE, qu'il exerce l'activité d'entretien d'animaux domestiques et qu'il ne fait l'objet d'aucune interdiction d'exercer ;
- un justificatif de ses qualifications professionnelles ;
- la preuve par tout moyen qu'il a exercé cette activité pendant au moins un an au cours des dix dernières années dans le cas où ni l'activité ni la formation ne sont réglementées dans cet État.

#### Procédure

L'autorité compétente peut soumettre le professionnel à une épreuve d'aptitude en cas de différences substantielles entre sa formation et la formation requise pour l'exercice en France de cette activité.

Le silence de la part de l'autorité compétente dans un délai d'un mois vaut acceptation de la demande.

**À noter**

Cette déclaration doit être renouvelée tous les ans et le cas échéant, lors de chaque changement de la situation du professionnel. En cas de manquement à cette obligation déclarative, le ressortissant encourt une amende de 1500 euros (ou 3000 euros en cas de récidive).

*Pour aller plus loin* :articles L. 214-6-1 et R. 204-1 du Code rural et de la pêche maritime.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne.

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).