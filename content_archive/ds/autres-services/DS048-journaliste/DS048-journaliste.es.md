﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS048" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Periodista" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="periodista" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/periodista.html" -->
<!-- var(last-update)="2020-04-15 17:23:28" -->
<!-- var(url-name)="periodista" -->
<!-- var(translation)="Auto" -->


Periodista
==========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:23:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

### a. Definición

El periodista es un profesional cuya misión es recopilar y verificar información, luego escribirla y transmitirla al público en diferentes medios.

Como parte de su actividad, también se le pedirá que testifique, filme, entreviste e iimagen esta información.

El periodista puede trabajar en una agencia de noticias, en la radio, en la televisión o en Internet.

*Para ir más allá* Artículo L. 7111-3 del Código de Trabajo; Artículo 2 de la[Ley de 29 de julio de 1881](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006070722) libertad de prensa.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Para el periodista, es la Cámara de Comercio e Industria (CCI).

Dos grados. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

La práctica del periodismo está reservada a los titulares de una tarjeta de prensa.

Para obtenerlo, el individuo debe cumplir las siguientes condiciones:

- llevar a cabo su negocio principalmente y de forma regular;
- que esta actividad representa al menos el 50% de sus ingresos;
- ser empleado por una agencia o una empresa de medios de comunicación.

No se requiere diploma o certificado para ejercer la profesión. Sin embargo, se recomienda que el individuo se lleve a cabo la educación postsecundaria en los campos de la comunicación, el periodismo o la publicación.

*Para ir más allá* Artículo L. 7111-6 del Código de Trabajo.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

No existe ningún reglamento para que el nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE) trabaje como periodista en Francia de forma permanente o permanente. temporal y casual.

Por lo tanto, el nacional está sujeto a los mismos requisitos que el nacional francés (véase infra "3. Procedimientos y trámites de instalación").

### c. Honorabilidad

Las empresas editoriales, diarias o periódicas, las agencias de noticias, las empresas de comunicación al público electrónica o la comunicación audiovisual deben proporcionar al periodista una carta de información Ética.

Como resultado, el periodista tendrá que cumplir con una serie de condiciones en el curso de su profesión, incluyendo:

- respetar la dignidad de las personas y la presunción de inocencia;
- Esté atento antes de difundir información
- mantener el secreto profesional y proteger sus fuentes.

*Para ir más allá* Artículo L. 7111-5-2 del Código de Trabajo.

Tres grados. Procedimientos y trámites de instalación
-----------------------------------------------------

### Pide una tarjeta de prensa

**Autoridad competente**

La Comisión de Tarjetas de Identidad de Periodistas Profesionales (CCIJP) es responsable de expedir la tarjeta al solicitante.

**Documentos de apoyo**

En apoyo de la solicitud, el solicitante deberá presentar todos los siguientes documentos justificativos:

- el[Forma](http://www.ccijp.net/upload/pdf/formulaire%201D%202018.pdf) completado, fechado y firmado;
- Id
- Un CV completo
- un extracto de su historial delictivo (boletín 3) de menos de 3 meses de edad;
- la afirmación de que ejerce la profesión de periodista en calidad de superior y regular, acompañada de copias de sus publicaciones;
- la indicación de cualquier otra ocupación regular;
- compromiso que compartirá con la comisión cualquier cambio que se produciría en su situación.

**Entrega de la tarjeta**

La tarjeta de prensa incluye la fotografía del titular, su nombre, nombres, nacionalidad y residencia, así como la mención de los medios de comunicación para los que practica. Tiene una validez de un año y será renovable para el mismo año, siempre que el periodista haya facilitado la documentación solicitada por el CCIJP.

Si se deniega la tarjeta, el CCIJP tendrá que notificar al solicitante su decisión por carta recomendada con notificación de recepción.

**Qué saber**

Un periodista con menos de dos años de experiencia recibirá una tarjeta de pasante.

*Para ir más allá* Artículos R. 7111-1 a R. 7111-10 del Código de Trabajo.

