﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS048" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Altri servizi" -->
<!-- var(title)="Giornalista" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="altri-servizi" -->
<!-- var(title-short)="giornalista" -->
<!-- var(url)="https://www.guichet-entreprises.fr/it/ds/altri-servizi/giornalista.html" -->
<!-- var(last-update)="2020-04-15 17:23:28" -->
<!-- var(url-name)="giornalista" -->
<!-- var(translation)="Auto" -->


Giornalista
===========

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:23:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

### a. Definizione

Il giornalista è un professionista la cui missione è raccogliere e verificare le informazioni, poi scriverle e trasmetterle al pubblico su diversi media.

Come parte della sua attività, sarà anche chiamato a testimoniare, filmare, intervistare e immaginare queste informazioni.

Il giornalista può lavorare in un'agenzia di stampa, in radio, in televisione o su Internet.

*Per andare oltre* articolo L. 7111-3 del codice del lavoro; L'articolo 2 del[Legge del 29 luglio 1881](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006070722) libertà di stampa.

### b. CFE competente

Il CFE in questione dipende dalla natura della struttura in cui viene svolta l'attività. Per il giornalista, è la Camera di Commercio e Industria (CCI).

Due gradi. Condizioni di installazione
--------------------------------------

### a. Qualifiche professionali

La pratica del giornalismo è riservata ai titolari di una carta stampa.

Per ottenerlo, l'individuo deve soddisfare le seguenti condizioni:

- svolgere la sua attività principalmente e regolarmente;
- che questa attività rappresenta almeno il 50% dei suoi ricavi;
- essere impiegato da un'agenzia o da una società di informazione.

Non è richiesto alcun diploma o certificato per esercitare la professione. Tuttavia, si raccomanda che l'individuo persegua l'istruzione post-secondaria nei settori della comunicazione, del giornalismo o dell'editoria.

*Per andare oltre* Articolo L. 7111-6 del codice del lavoro.

### b. Qualifiche professionali - Cittadini dell'UE o del CEE (Servizio gratuito o Stabilimento gratuito)

Non esistono regolamenti per i cittadini di uno Stato membro dell'Unione europea (UE) o parte dell'accordo sullo Spazio economico europeo (AEA) per lavorare come giornalista in Francia su base permanente o permanente. temporaneo e casuale.

In quanto tale, il cittadino è quindi soggetto agli stessi requisiti del cittadino francese (cfr. infra "3. Procedure di installazione e formalità").

### c. L'attenità

Le case editrici, quotidiane o periodiche, le agenzie di stampa, le società di comunicazione al pubblico devono fornire al giornalista una carta d'informazione Etica.

Di conseguenza, il giornalista dovrà rispettare una serie di condizioni nel corso della sua professione, tra cui:

- rispettare la dignità degli individui e la presunzione di innocenza;
- Prestare attenzione prima di diffondere le informazioni
- mantenere il segreto professionale e proteggere le sue fonti.

*Per andare oltre* Articolo L. 7111-5-2 del codice del lavoro.

Tre gradi. Procedure di installazione e formalità
-------------------------------------------------

### Richiedi un biglietto da stampa

**Autorità competente**

La Commissione professionale per le carte d'identità dei giornalisti (CCIJP) è responsabile del rimessa della carta al richiedente.

**Documenti di supporto**

A sostegno della domanda, il richiedente sarà tenuto a fornire tutti i seguenti documenti giustificativi:

- Le[modulo](http://www.ccijp.net/upload/pdf/formulaire%201D%202018.pdf) completato, datato e firmato;
- Id
- Un curriculum completo
- un estratto della sua fedina penale (bulletin 3) di meno di 3 mesi;
- l'affermazione che egli esercita la professione di giornalista in qualità di alto e regolare, accompagnato da copie delle sue pubblicazioni;
- l'indicazione di qualsiasi altra occupazione regolare;
- impegno che condividerà con la commissione eventuali modifiche che si verificherebbero nella sua situazione.

**Consegna della carta**

La carta stampa comprende la fotografia del titolare, il suo nome, i nomi, la nazionalità e la residenza, nonché la menzione dei media per i quali pratica. È valido per un anno e sarà rinnovabile per lo stesso anno, a condizione che il giornalista abbia fornito la documentazione richiesta dal CCIJP.

In caso di rifiuto della carta, il CCIJP dovrà notificare al richiedente la sua decisione per lettera raccomandata con notifica di ricevimento.

**Cosa sapere**

Un giornalista con meno di due anni di esperienza riceverà una carta di stagista.

*Per andare oltre* Articoli da R. da 7111-1 a R. 7111-10 del Codice del Lavoro.

