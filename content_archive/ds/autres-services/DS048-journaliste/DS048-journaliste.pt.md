﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS048" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Outros serviços" -->
<!-- var(title)="Jornalista" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="outros-servicos" -->
<!-- var(title-short)="jornalista" -->
<!-- var(url)="https://www.guichet-entreprises.fr/pt/ds/outros-servicos/jornalista.html" -->
<!-- var(last-update)="2020-04-15 17:23:28" -->
<!-- var(url-name)="jornalista" -->
<!-- var(translation)="Auto" -->


Jornalista
==========

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:23:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

### a. Definição

O jornalista é um profissional cuja missão é coletar e verificar informações, depois escrevê-la e repassá-la ao público em diferentes mídias.

Como parte de sua atividade, ele também será chamado para testemunhar, filmar, entrevistar e imagem dessas informações.

O jornalista pode trabalhar em uma agência de notícias, no rádio, na televisão ou na internet.

*Para ir mais longe* Artigo 7111-3 do Código do Trabalho; Artigo 2º do[Lei de 29 de julho de 1881](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006070722) liberdade de imprensa.

### b. CFE competente

A CFE relevante depende da natureza da estrutura em que a atividade é realizada. Para o jornalista, é a Câmara de Comércio e Indústria (CCI).

Dois graus. Condições de instalação
-----------------------------------

### a. Qualificações profissionais

A prática do jornalismo é reservada para detentores de um cartão de imprensa.

Para obtê-lo, o indivíduo deve atender às seguintes condições:

- realizar seus negócios principalmente e regularmente;
- que essa atividade é responsável por pelo menos 50% de suas receitas;
- ser empregado por uma agência ou uma empresa de mídia de notícias.

Nenhum diploma ou certificado é necessário para exercer a profissão. No entanto, recomenda-se que o indivíduo prossiga no ensino pós-secundário nas áreas de comunicação, jornalismo ou publicação.

*Para ir mais longe* Artigo 7111-6 do Código do Trabalho.

### b. Qualificações Profissionais - Cidadãos da UE ou EEE (Serviço Livre ou Estabelecimento Livre)

Não há regulamentos para o nacional de um Estado-Membro da União Europeia (UE) ou parte do acordo sobre a Área Econômica Europeia (EEE) para trabalhar como jornalista na França de forma permanente ou permanente. temporário e casual.

Como tal, o nacional está, portanto, sujeito aos mesmos requisitos do nacional francês (ver infra "3. Procedimentos de instalação e formalidades").

### c. Honorabilidade

Jornais, editoras diárias ou periódicas, agências de notícias, empresas de comunicação pública eletrônica ou audiovisual devem fornecer ao jornalista uma carta de informações Ética.

Como resultado, o jornalista terá que cumprir uma série de condições no curso de sua profissão, incluindo:

- respeitar a dignidade dos indivíduos e a presunção de inocência;
- Fique atento antes de divulgar informações
- manter o sigilo profissional e proteger suas fontes.

*Para ir mais longe* Artigo 7111-5-2 do Código do Trabalho.

Três graus. Procedimentos de instalação e formalidades
------------------------------------------------------

### Peça um cartão de imprensa

**Autoridade competente**

A Comissão de Carteira de Identidade de Jornalistas Profissionais (CCIJP) é responsável pela emissão do cartão ao requerente.

**Documentos de suporte**

Em apoio à solicitação, o requerente será obrigado a fornecer todos os seguintes documentos de suporte:

- O[Forma](http://www.ccijp.net/upload/pdf/formulaire%201D%202018.pdf) concluído, datado e assinado;
- Id
- Um currículo completo
- um extrato de sua ficha criminal (boletim 3) com menos de 3 meses de idade;
- a afirmação de que exerce a profissão de jornalista em função sênior e regular, acompanhada de cópias de suas publicações;
- a indicação de qualquer outra ocupação regular;
- compromisso que ele vai compartilhar com a comissão quaisquer mudanças que ocorreriam em sua situação.

**Entrega do cartão**

O cartão de imprensa inclui a fotografia do titular, seu nome, primeiros nomes, nacionalidade e residência, bem como a menção da mídia para a qual ele pratica. É válido por um ano e será renovável pelo mesmo ano, desde que o jornalista tenha fornecido a documentação solicitada pelo CCIJP.

Se o cartão for recusado, o CCIJP terá que notificar o requerente de sua decisão por carta recomendada com aviso de recebimento.

**O que saber**

Um jornalista com menos de dois anos de experiência receberá um cartão de estagiário.

*Para ir mais longe* Artigos R. 7111-1 a R. 7111-10 do Código do Trabalho.

