﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS048" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sonstige Dienstleistungen" -->
<!-- var(title)="Journalist" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sonstige-dienstleistungen" -->
<!-- var(title-short)="journalist" -->
<!-- var(url)="https://www.guichet-entreprises.fr/de/ds/sonstige-dienstleistungen/journalist.html" -->
<!-- var(last-update)="2020-04-15 17:23:28" -->
<!-- var(url-name)="journalist" -->
<!-- var(translation)="Auto" -->


Journalist
==========

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:23:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

### a. Definition

Der Journalist ist ein Profi, dessen Aufgabe es ist, Informationen zu sammeln und zu verifizieren, sie dann zu schreiben und an die Öffentlichkeit auf verschiedenen Medien weiterzugeben.

Im Rahmen seiner Tätigkeit wird er auch dazu aufgerufen sein, diese Informationen auszusagen, zu filmen, zu interviewen und zu bebildern.

Der Journalist kann in einer Nachrichtenagentur, im Radio, im Fernsehen oder im Internet arbeiten.

*Um weiter zu gehen* Artikel L. 7111-3 des Arbeitsgesetzbuches; Artikel 2 der[Gesetz vom 29. Juli 1881](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006070722) Pressefreiheit.

### b. zuständigeCFE

Die betreffende CFE hängt von der Art der Struktur ab, in der die Tätigkeit ausgeübt wird. Für den Journalisten ist es die Industrie- und Handelskammer (IHK).

Zwei Grad. Installationsbedingungen
-----------------------------------

### a. Berufliche Qualifikationen

Die Praxis des Journalismus ist den Inhabern eines Presseausweises vorbehalten.

Um sie zu erhalten, muss die Person die folgenden Bedingungen erfüllen:

- ihre Geschäfte hauptsächlich und regelmäßig ausüben;
- dass diese Tätigkeit mindestens 50 % ihrer Einnahmen ausmacht;
- bei einer Agentur oder einem Medienunternehmen angestellt werden.

Für die Ausübung des Berufs ist kein Diplom oder Zertifikat erforderlich. Es wird jedoch empfohlen, dass der Einzelne eine postsekundäre Ausbildung in den Bereichen Kommunikation, Journalismus oder Verlagswesen abhält.

*Um weiter zu gehen* Artikel L. 7111-6 des Arbeitsgesetzbuches.

### b. Berufsqualifikationen - EU- oder EWR-Bürger (Freier Dienst oder freie Niederlassung)

Es gibt keine Vorschriften für Staatsangehörige eines Mitgliedstaats der Europäischen Union (EU) oder Partei des Abkommens über den Europäischen Wirtschaftsraum (EWR), um als Journalist in Frankreich auf Dauer oder dauerhaft zu arbeiten. vorübergehend und lässig.

Als solcher unterliegt der Staatsangehörige daher den gleichen Anforderungen wie der französische Staatsangehörige (siehe infra "3. Installationsverfahren und Formalitäten").

### c. Ehrenfähigkeit

Zeitungs-, Tages- oder Zeitschriftenverlage, Nachrichtenagenturen, Kommunikationsunternehmen für die Öffentlichkeit, die elektronisch oder audiovisuelle Kommunikation sind, müssen dem Journalisten eine Informationscharta zur Verfügung stellen. Ethik.

Infolgedessen muss der Journalist im Laufe seines Berufs eine Reihe von Bedingungen erfüllen, darunter:

- die Würde des Einzelnen und die Unschuldsvermutung zu achten;
- Seien Sie wachsam, bevor Sie Informationen verbreiten
- das Berufsgeheimnis zu wahren und seine Quellen zu schützen.

*Um weiter zu gehen* Artikel L. 7111-5-2 des Arbeitsgesetzbuches.

Drei Grad. Installationsverfahren und Formalitäten
--------------------------------------------------

### Fragen Sie nach einem Presseausweis

**Zuständige Behörde**

Die Professional Journalists Identity Card Commission (CCIJP) ist für die Ausstellung der Karte an den Antragsteller zuständig.

**Belege**

Zur Unterstützung des Antrags muss der Antragsteller alle folgenden Belege vorlegen:

- das[Form](http://www.ccijp.net/upload/pdf/formulaire%201D%202018.pdf) ausgefüllt, datiert und unterzeichnet;
- Id
- Ein vollständiger Lebenslauf
- einen Auszug aus seinem Vorstrafenregister (Bulletin 3) im Alter von weniger als 3 Monaten;
- die Behauptung, dass er den Beruf des Journalisten in leitender und regelmäßiger Funktion ausübt, begleitet von Kopien seiner Veröffentlichungen;
- Angabe einer anderen regulären Beschäftigung;
- Verpflichtung, dass er mit der Kommission alle Änderungen teilen wird, die in seiner Situation auftreten würden.

**Lieferung der Karte**

Der Presseausweis enthält das Foto des Inhabers, seinen Namen, Vornamen, Nationalität und Wohnsitz sowie die Erwähnung der Medien, für die er praktiziert. Sie ist ein Jahr gültig und kann für das gleiche Jahr verlängerbar sein, sofern der Journalist die von der CCIJP angeforderten Unterlagen zur Verfügung gestellt hat.

Wird die Karte abgelehnt, muss die CCIJP den Antragsteller mit dem mit Empfangsschreiben empfohlenen Schreiben über ihre Entscheidung unterrichten.

**Was Sie wissen sollten**

Ein Journalist mit weniger als zwei Jahren Erfahrung erhält eine Internierungskarte.

*Um weiter zu gehen* Artikel R. 7111-1 bis R. 7111-10 des Arbeitsgesetzbuches.

