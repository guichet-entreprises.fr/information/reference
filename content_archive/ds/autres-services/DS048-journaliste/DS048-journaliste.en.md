﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS048" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Journalist" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="journalist" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/journalist.html" -->
<!-- var(last-update)="2020-04-15 17:23:28" -->
<!-- var(url-name)="journalist" -->
<!-- var(translation)="Auto" -->


Journalist
==========

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:23:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Defining the activity
------------------------

### a. Definition

The journalist is a professional whose mission is to collect and verify information, then to write it and pass it on to the public on different media.

As part of his activity, he will also be called upon to testify, film, interview and image this information.

The journalist can work in a news agency, on radio, on television or on the internet.

*To go further* Article L. 7111-3 of the Labour Code; Article 2 of the[Law of July 29, 1881](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006070722) freedom of the press.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out. For the journalist, it is the Chamber of Commerce and Industry (CCI).

Two degrees. Installation conditions
------------------------------------

### a. Professional qualifications

The practice of journalism is reserved for holders of a press card.

To obtain it, the individual must meet the following conditions:

- carry out its business mainly and on a regular basis;
- that this activity accounts for at least 50% of its revenues;
- be employed by an agency or a news media company.

No diploma or certificate is required to practice the profession. However, it is recommended that the individual pursue post-secondary education in the fields of communication, journalism or publishing.

*To go further* Article L. 7111-6 of the Labour Code.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Free Establishment)

There are no regulations for the national of a Member State of the European Union (EU) or party to the agreement on the European Economic Area (EEA) to work as a journalist in France on a permanent or permanent basis. temporary and casual.

As such, the national is therefore subject to the same requirements as the French national (see infra "3. Installation procedures and formalities").

### c. Honorability

Newspaper, daily or periodic publishing companies, news agencies, communication companies to the public electronically or audiovisual communication must provide the journalist with a charter of information Ethics.

As a result, the journalist will have to comply with a number of conditions in the course of his profession, including:

- respect the dignity of individuals and the presumption of innocence;
- Be vigilant before disseminating information
- keep professional secrecy and protect its sources.

*To go further* Article L. 7111-5-2 of the Labour Code.

Three degrees. Installation procedures and formalities
------------------------------------------------------

### Ask for a press card

**Competent authority**

The Professional Journalists Identity Card Commission (CCIJP) is responsible for issuing the card to the applicant.

**Supporting documents**

In support of the application, the applicant will be required to provide all of the following supporting documents:

- The[Form](http://www.ccijp.net/upload/pdf/formulaire%201D%202018.pdf) completed, dated and signed;
- ID
- A full resume
- an extract from his criminal record (bulletin 3) less than 3 months old;
- the assertion that he practises the profession of journalist in a senior and regular capacity, accompanied by copies of his publications;
- the indication of any other regular occupation;
- commitment that he will share with the commission any changes that would occur in his situation.

**Delivery of the card**

The press card includes the photograph of the holder, his name, first names, nationality and residence, as well as the mention of the media for which he practices. It is valid for one year and will be renewable for the same year, provided the journalist has provided the documentation requested by the CCIJP.

If the card is refused, the CCIJP will have to notify the applicant of its decision by letter recommended with notice of receipt.

**What to know**

A journalist with less than two years of experience will receive an intern card.

*To go further* Articles R. 7111-1 to R. 7111-10 of the Labour Code.

