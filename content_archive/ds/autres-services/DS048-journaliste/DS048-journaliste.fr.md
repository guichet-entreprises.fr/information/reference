﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS048" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Journaliste" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="journaliste" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/journaliste.html" -->
<!-- var(last-update)="2020-04-15 17:23:28" -->
<!-- var(url-name)="journaliste" -->
<!-- var(translation)="None" -->


# Journaliste

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:23:28<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le journaliste est un professionnel dont la mission est de recueillir et vérifier des informations, puis de les rédiger et les transmettre au public sur différents supports.

Dans le cadre de son activité, il sera également amené à témoigner, filmer, interviewer et monter en image ces informations.

Le journaliste peut travailler en agence de presse, à la radio, à la télévision ou sur internet.

*Pour aller plus loin* : article L. 7111-3 du Code du travail ; article 2 de la [loi du 29 juillet 1881](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006070722) sur la liberté de la presse.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée. Pour le journaliste, il s’agit de la chambre de commerce et d'industrie (CCI).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

L'exercice de la profession de journaliste est réservé aux titulaires d'une carte de presse.

Pour l'obtenir, l'intéressé doit remplir les conditions suivantes :

* exercer son activité de façon principale et de manière régulière ;
* que cette activité représente au moins 50 % de ses revenus ;
* être employé par une agence ou une entreprise de presse.

Aucun diplôme ou certificat n'est obligatoire pour exercer la profession. Toutefois, il est recommandé à l'intéressé de suivre des études postsecondaire dans les domaines de la communication, du journalisme ou encore de l'édition.

*Pour aller plus loin* : article L. 7111-6 du Code du travail.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer en France l'activité de journaliste à titre permanent ou à titre temporaire et occasionnel.

À ce titre, le ressortissant est donc soumis aux mêmes exigences que le ressortissant français (cf. infra « 3. Démarches et formalités d'installation »).

### c. Honorabilité

Les entreprises de presse, de publication quotidienne ou périodique, les agences de presse, les entreprises de communication au public par voie électronique ou de communication audiovisuelle doivent impérativement remettre au journaliste une charte de déontologie.

De ce fait, le journaliste devra respecter un certain nombre de conditions dans l'exercice de sa profession, et notamment :

* respecter la dignité des personnes et la présomption d'innocence ;
* faire preuve de vigilance avant de diffuser des informations ;
* garder le secret professionnel et protéger ses sources.

*Pour aller plus loin* : article L. 7111-5-2 du Code du travail.

## 3°. Démarches et formalités d’installation

### Demander une carte de presse

**Autorité compétente**

La Commission de la carte d'identité des journalistes professionnels (CCIJP) est compétente pour délivrer la carte au demandeur.

**Pièces justificatives**

À l'appui de sa demande, l'intéressé devra fournir l'ensemble des pièces justificatives suivantes :

* le [formulaire](http://www.ccijp.net/upload/pdf/formulaire%201D%202018.pdf) rempli, daté et signé ;
* une pièce d'identité ;
* un curriculum vitae complet ;
* un extrait de son casier judiciaire (bulletin n° 3) datant de moins de 3 mois ;
* l'affirmation sur l'honneur qu'il exerce bien la profession de journaliste à titre principal et régulier, accompagnée des exemplaires de ses publications ;
* l'indication de toute autre occupation régulière ;
* l'engagement qu'il fera part à la commission de tout changement qui surviendrait dans sa situation.

**Délivrance de la carte**

La carte de presse comporte la photographie du titulaire, ses nom, prénoms, nationalité et domicile, ainsi que la mention des organes de presse pour qui il exerce. Elle est valable pour un an et sera renouvelable pour la même année, sous réserve que le journaliste ait fourni les justificatifs demandés par la CCIJP.

En cas de refus de délivrer la carte, la CCIJP devra notifier sa décision au demandeur par lettre recommandée avec avis de réception.

**À savoir**

Le journaliste ayant moins de deux années d'expérience recevra une carte de stagiaire.

*Pour aller plus loin* : articles R. 7111-1 à R. 7111-10 du Code du travail.