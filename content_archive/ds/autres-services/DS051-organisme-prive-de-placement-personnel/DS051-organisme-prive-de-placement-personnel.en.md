﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS051" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Private employment agency" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="private-employment-agency" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/private-employment-agency.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="private-employment-agency" -->
<!-- var(translation)="Auto" -->


Private employment agency
=============================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The activity of a private staffing organization is to routinely connect job seekers and companies looking for staff.

The organization offering these placement services must not, under any circumstances, require compensation from the jobseeker (subject to the provisions relating to sports agents and intermittents of the show), nor should it take part in the future relationship of the Work.

**Please note**

These organizations can be specialized and thus offer only jobs in specific sectors.

*For further information*: Articles L. 5321-1 and L. 5321-2 of the Labour Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out.

As the investment activity is commercial in nature, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To create a private staffing organization, no professional qualifications are required.

However, the person concerned must proceed with the declaration of his company (see infra "3°. Installation procedures and formalities").

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There are no special provisions for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement to carry out this activity on a temporary and casual or permanent basis in France.

Thus, the EU national is subject to the same requirements as the French national (see infra "3°. Installation procedures and formalities").

### c. Conditions of honorability

#### Administrative sanctions

As long as the jobseeker is discriminated against during the placement procedure, the agency may be required by the competent administrative authority to close for up to three months.

*For further information*: Articles L. 5323-1 and L. 1132-1 of the Labour Code.

#### Criminal sanctions

The fact that a professional in an employment agency demands compensation from a jobseeker is punishable by six months' imprisonment and a fine of 3,750 euros.

**Procedure**

Once the breaches are found, the private body must submit its comments within a fortnight. If the prefect does not comply beyond this period, he can then send him a formal notice by letter recommended with notice of receipt and order its closure for a maximum of three months.

*For further information*: Article L. 5324-1 of the Labour Code.

### d. Some peculiarities of the regulation of the activity

**Transmission of information**

The private staffing agency must provide the prefect with information on:

- Turnover
- number of job seekers received, placed and registered by the organization and divided by age and gender.

*For further information*: Article R. 5323-8 of the Labour Code.

**Service contract**

A private staffing organization may enter into a service delivery contract for the care of job seekers with a public employment agency, including:

- State employment and professional equality services;
- Job centre;
- a state-of-the-art industrial and commercial institution (Epic) responsible for adult vocational training.

If necessary, the private placement agency must provide the public body and the Employment Centre with all the information relating to the jobseeker. For example, the private organization must provide the following information, thanks to the job seeker's unique file:

- how the applicant's personalized access to employment (PPAE) project will be adapted over time. This project brings together all the characteristics of the job sought;
- Updating the list of job seekers
- compensation
- follow-up and job search operations.

*For further information*: Articles L. 5311-1, R. 5323-12 to R. 5323-14 of the Labour Code.

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

It is advisable to refer to the listing " [Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

*For further information*: order of June 25, 1980 approving the general provisions of the fire and panic safety regulations in public institutions.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

**Competent authority**

The professional must proceed with the declaration of his company. To do so, it must file a declaration with the Chamber of Commerce and Industry (CCI).

**Supporting documents**

The person concerned must provide the[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Timeframe**

The CCI's Business Formalities Centre sends a receipt to the professional for the missing documents on the same day. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

### b. If necessary, register the company's statutes

The professional must, once the company's statutes have been dated and signed, register them with the Corporate Tax Office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

