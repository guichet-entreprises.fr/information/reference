﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS051" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Organisme privé de placement de personnel" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="organisme-prive-de-placement-personnel" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/organisme-prive-de-placement-de-personnel.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="organisme-prive-de-placement-de-personnel" -->
<!-- var(translation)="None" -->

# Organisme privé de placement de personnel

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

##1°. Définition de l’activité

### a. Définition

L'activité d'un organisme privé de placement de personnel consiste à mettre en relation, de manière habituelle, les demandeurs d'emploi et les entreprises en recherche de personnel.

L'organisme proposant ces services de placement ne doit en aucun cas exiger une rétribution de la part du demandeur d'emploi (sous réserve des dispositions relatives aux agents sportifs et aux intermittents du spectacle), ni prendre part à la future relation de travail.

**À noter**

Ces organismes peuvent être spécialisés et ainsi proposer uniquement des emplois dans des secteurs donnés.

*Pour aller plus loin* : articles L. 5321-1 et L. 5321-2 du Code du travail.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée.

L'activité de placement étant de nature commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour créer un organisme privé de placement de personnel, aucune qualification professionnelle n'est requise.

Toutefois, l'intéressé doit procéder à la déclaration de son entreprise (cf. infra « 3°. Démarches et formalités d'installation »).

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services ou Libre établissement)

Aucune disposition particulière n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer cette activité à titre temporaire et occasionnel ou permanent en France.

Ainsi, le ressortissant UE est soumis aux mêmes exigences que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation »).

### c. Conditions d’honorabilité

#### Sanctions administratives

Dès lors que le demandeur d'emploi fait l'objet d'une quelconque discrimination au cours de la procédure de placement, l'organisme pourra être tenu par l'autorité administrative compétente de fermer pendant une durée maximale de trois mois.

*Pour aller plus loin* : articles L. 5323-1 et L. 1132-1 du Code du travail.

#### Sanctions pénales

Le fait pour un professionnel au sein d'un organisme de placement d'exiger une rétribution à un demandeur d'emploi est puni d'une peine de six mois d'emprisonnement et de 3 750 euros d'amende.

#### Procédure

Dès lors que les manquements sont constatés, l'organisme privé doit présenter dans un délai de quinze jours ses observations. S'il ne procède pas à une mise en conformité au delà de ce délai, le préfet peut alors lui adresser une mise en demeure par lettre recommandée avec avis de réception et ordonner sa fermeture pour une durée maximale de trois mois.

*Pour aller plus loin* : article L. 5324-1 du Code du travail.

### d. Quelques particularités de la réglementation de l’activité

#### Transmission d'informations

L'organisme privé de placement de personnel doit adresser chaque année au préfet, avant le 31 mars de l'année suivante, les informations relatives au :

- chiffre d'affaires réalisé ;
- nombre de personnes à la recherche d'un emploi reçues, placées et inscrites par l'organisme et réparties selon leur âge et leur sexe.

*Pour aller plus loin* : article R. 5323-8 du Code du travail.

#### Contrat de prestations de services

Un organisme privé de placement de personnel peut conclure un contrat de prestation de services en vue de la prise en charge de demandeurs d'emploi avec un organisme public d'emploi notamment :

- les services de l’État chargés de l'emploi et de l'égalité professionnelle ;
- Pôle emploi ;
- un établissement public de l’État à caractère industriel et commercial (Epic) chargé de la formation professionnelle des adultes.

Le cas échéant, l'organisme privé de placement doit transmettre à l'organisme public et à Pôle emploi l'ensemble des informations relatives au demandeur d'emploi. Ainsi, l'organisme privé doit remettre, grâce au dossier unique du demandeur d'emploi, les informations suivantes :

- la manière dont sera adapté dans le temps le projet personnalisé d'accès à l'emploi du demandeur (PPAE). Ce projet réunit l'ensemble des caractéristiques de l'emploi recherché ;
- l'actualisation de la liste des demandeurs d'emploi ;
- leur indemnisation ;
- les opérations de suivi et de recherche d'emploi.

*Pour aller plus loin* : articles L. 5311-1, R. 5323-12 à R. 5323-14 du Code du travail.

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

#### Autorité compétente

Le professionnel doit procéder à la déclaration de son entreprise. Pour cela, il doit effectuer une déclaration auprès de la chambre de commerce et de l'industrie (CCI).

#### Pièces justificatives

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI adresse le jour même un récépissé au professionnel des pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### b. Le cas échéant, enregistrer les statuts de la société

Le professionnel doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.