﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS032" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Crèche – Accueil d’enfants de moins de 6 ans" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="creche-accueil-d-enfants-de-moins" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/creche-accueil-d-enfants-de-moins-de-6-ans.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="creche-accueil-d-enfants-de-moins-de-6-ans" -->
<!-- var(translation)="None" -->

# Crèche – Accueil d’enfants de moins de 6 ans

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Les établissements et services d'accueil collectif ont pour fonction de garder, de manière non permanente, des enfants de moins de six ans et de veiller à leur santé, leur sécurité, leur bien-être et leur développement.

Ces établissements d'accueil, à titre occasionnel ou saisonnier, peuvent prendre les formes suivantes soit :

- être gérés par une personne privée (physique ou morale) ;
- être de nature publique et/ou à caractère éducatif (loisirs, vacances scolaires, congés professionnels), en dehors du domicile parental.

Sont ainsi concernés :

- les établissements d'accueil collectifs dont :
  - les crèches collectives,
  - les haltes-garderies,
  - les services d'accueil familial ou crèches familiales qui assurent la garde non permanente d'enfants au domicile d'assistants maternels (un même établissement ou srvice dit « multi-accueil » peut associer l'accueil collectif et l'accueil familial ou l'accueil régulier et l'accueil occasionnel ;
- les crèches parentales gérées par une association de parents ;
- les jardins d'enfants (comprenant exclusivement des enfants de plus de deux ans non scolarisés ou scolarisés à temps partiel) ;
- les micro-crèches dont la capacité est limitée à dix places.

**À noter**

La capacité d'accueil de ces établissements est limitée selon la nature de l'établissement.

*Pour aller plus loin* : articles R. 2324-17 et suivants du Code de la santé publique.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la structure dans laquelle l'activité est exercée :

- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour une profession libérale, il s'agit de l'Urssaf ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ou du greffe du tribunal d'instance (pour les départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le professionnel qui souhaite diriger un établissement d'accueil d'enfants de moins de six ans doit justifier de qualifications professionnelles qui varient selon la nature de l'établissement et la fonction du professionnel.

**À noter**

Dès lors qu'il est titulaire des qualifications requises pour diriger l'un des établissements visés ci-après, le professionnel peut exercer les mêmes fonctions, au sein d'un établissement d'une capacité inférieure.

#### Directeur d'établissement

**Établissement de plus de quarante et une places**

Le professionnel qui souhaite assurer la direction d'un établissement doit être titulaire de l'un des diplômes suivants :

- un diplôme d’État de docteur en médecine ;
- un diplôme d’État de puéricultrice et justifiant d'une expérience professionnelle d'au moins trois ans ;
- un diplôme d’État d'éducateur de jeunes enfants à condition :
  - qu'il justifie d'une certification attestant de ses compétences dans le domaine de l'encadrement ou de la direction,
  - qu'il justifie de trois ans d'expérience professionnelle,
  - que l'établissement ou le service comprenne une personne titulaire soit :
    - d'un diplôme d’État de puéricultrice,
    - d'un diplôme d’État d'infirmier ou infirmière justifiant d'au moins un an d'expérience auprès de jeunes enfants.

**À noter**

Le directeur d'un établissement d'une capacité supérieure à soixante places doit être assisté d'un adjoint titulaire de l'un des diplômes susvisés ou mentionnés aux articles R. 2324-35 et R. 2324-46.

*Pour aller plus loin* : articles R.2324-34 et suivants du Code de la santé publique.

**Établissement d'une capacité inférieure ou égale à quarante places**

Peuvent assurer la direction d'un établissement d'accueil d'enfants de moins de six ans ayant une capacité inférieure ou égale à quarante places ou la responsabilité technique d'un établissement à gestion parentale, les professionnels suivants :

- une puéricultrice diplômée justifiant de trois ans d'expérience professionnelle ;
- un éducateur de jeunes enfants justifiant de trois ans d'expérience professionnelle dès lors qu'il exerce avec le concours d'un professionnel justifiant d'une expérience d'au moins un an auprès de jeunes enfants et diplômé d’État en tant que :
  - puéricultrice,
  - infirmier ou infirmière.

*Pour aller plus loin* : article R. 2324-35 du Code de la santé publique.

**Établissements d'une capacité inférieure ou égale à vingt places**

Peuvent assurer la direction d'un établissement d'accueil d'enfants de moins de six ans, ayant une capacité inférieure ou égale à vingt places, les professionnels suivants :

- une puéricultrice diplômée justifiant de trois ans d'expérience professionnelle ;
- un éducateur de jeunes enfants justifiant de trois ans d'expérience professionnelle.

*Pour aller plus loin* : article R. 2324-35 du Code de la santé publique.

**À savoir**

Les micro-crèches dont la capacité d'accueil est limitée à dix places, n'ont pas obligation de disposer d'un directeur mais d'un référent technique. Cependant, lorsque plusieurs établissements sont gérés par une même personne et que la capacité totale de ces établissements est supérieure à 20 places, celle-ci est tenue de désigner un directeur.

*Pour aller plus loin* : article R. 2324-36 et R. 2324-36-1 du Code de la santé publique.

**Mesures dérogatoires**

Si aucun professionnel ne remplissant les conditions de qualifications susvisées ne peut assurer la direction d'un établissement d'accueil d'enfants de moins de six ans, des dérogations sont prévues en fonction de la taille de l'établissement concerné.

Ainsi, la direction d'un établissement ou service d'une capacité supérieure à quarante places peut être confiée à un professionnel titulaire :

- d'un diplôme d’État d'éducateur de jeunes enfants s'il justifie de trois ans d'expérience professionnelle dont au moins deux en tant que :
  - directeur,
  - directeur adjoint,
  - responsable technique d'un tel établissement.
- d'un diplôme d’État de sage-femme ou d'infirmier et justifiant :
  - d'une expérience professionnelle de trois ans en tant que directeur ou directeur adjoint d'un tel établissement ;
  - ou d'une certification de niveau II minimum, enregistrée au Répertoire national des certifications professionnelles attestant de ses compétences d'encadrement ou de direction et d'une expérience de trois ans auprès d'enfants de moins de trois ans.

Le professionnel peut assurer la direction des établissements d'une capacité entre vingt et une et quarante places dès lors qu'il justifie d'un diplôme d'État d'assistant de service social, d'éducateur spécialisé, de conseillère en économie sociale et familiale, de psychomotricien, ou d'un DESS ou d'un master 2 justifiant :

- de trois ans d'expérience en tant que directeur, directeur adjoint ou responsable d'un tel établissement ;
- d'un certification de niveau II minimum enregistrée au Répertoire national des certifications professionnelles justifiant de compétences dans le domaine de l'encadrement ou de la direction et d'une expérience de trois ans auprès d'enfants de moins de trois ans.

Le professionnel peut assurer la direction d'un établissement de vingt places ou moins dès lors qu'il est titulaire de l'un des diplômes suivants :

- un diplôme d’État :
  - de sage-femme,
  - d'infirmier,
  - d'assistant de service social,
  - d'éducateur spécialisé,
  - de conseiller en économie sociale et familiale,
  - de psychomotricien ;
- d'un diplôme d'études supérieures spécialisées (DESS) ou d'un master II (bac +5) de psychologie et justifiant de trois ans d'expérience en tant que directeur, directeur adjoint ou responsable technique d'un tel établissement ou de trois ans d'expérience auprès de jeunes enfants.

Le professionnel ayant assuré pendant trois ans la direction d'un établissement à gestion parentale peut assurer la direction d'un établissement géré par une personne de droit privé.

*Pour aller plus loin* : article R. 2324-46 du Code de la santé publique.

#### Personnel de l'établissement

L'établissement d'accueil d'enfants de moins de six ans doit être composé de professionnels titulaires des diplômes suivants pour au moins 40 % de l'effectif :

- un diplôme d’État de puéricultrice ;
- un diplôme d’État d'éducateur de jeunes enfants ;
- un diplôme d'auxiliaire de puériculture ;
- un diplôme d’État d'infirmier/infirmière ;
- un diplôme d’État de psychomotricien.

En plus de ces professionnels, l'établissement doit être composé de professionnels titulaires pour au plus 60 % de l'effectif :

- des titres de formation suivants :
  - un certificat d'aptitude professionnelle petite enfance,
  - un certificat de travailleuse familiale ou un diplôme d’État de technicien de l'intervention sociale et familiale,
  - un brevet d’État d'animateur technicien de l'éducation populaire et de la jeunesse, option petite enfance,
  - un brevet d'études professionnelles, option sanitaire et sociale,
  - un certificat d'aptitude aux fonctions d'aide à domicile ;
- d'une expérience professionnelle de cinq ans en tant qu'assistante maternelle ;
- d'une expérience professionnelle de trois ans auprès d'enfants dans un établissement public d'accueil d'enfant de moins de six ans.

*Pour aller plus loin* : article R. 2323-42 du Code de la santé publique et arrêté du 26 décembre 2000 relatif aux personnels des établissements et services d'accueil des enfants de moins de six ans.

**À noter**

Dès lors qu'un établissement ou service a une capacité d'accueil supérieure à dix places, il doit, en fonction du nombre d'enfants et de leur âge, être composé des professionnels qualifiés dans les domaines suivants :

- psychologie ;
- social ;
- sanitaire ;
- éducatif ;
- culturel.

Dès lors que cette capacité dépasse les dix places, le personnel doit être composé d'un médecin spécialiste ou pédiatre, ou, à défaut, d'un médecin généraliste possédant une expérience professionnelle dans le domaine de la pédiatrie.

*Pour aller plus loin* : articles R. 2324-38 et R. 2324-39 du Code de la santé publique.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services (LPS) ou Libre Exercice (LE))

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un membre de l’Union Européenne (UE) ou partie de l’Espace économique européen (EEE) souhaitant exercer au sein d'un établissement d'accueil d'enfants de moins de six ans en France, à titre temporaire et occasionnel ou à titre permanent.

Dès lors, le ressortissant de l'UE ou de l'EEE sera soumis aux mêmes conditions d'installation que le professionnel français (cf. supra « 2°. Conditions d'installation »).

### c. Conditions d’honorabilité

Pour exercer en tant que professionnel au sein d'un établissement d'accueil d'enfants de moins de six ans, le professionnel ne doit avoir fait l'objet d'une incapacité d'exercice et/ou d'une condamnation définitive pour :

- crime ou d'une peine d'au moins deux mois d'emprisonnement pour atteinte aux personnes ;
- agression sexuelle ;
- atteinte ou mise en péril des mineurs ;
- recel ;
- usage illicite de stupéfiants.

De plus, le gestionnaire d'un tel établissement doit s'assurer que l'ensemble du personnel ne fait pas l'objet des condamnations susvisées.

*Pour aller plus loin* : articles L. 2324-1 alinéa 5 et R. 2324-33 du Code de la santé publique ; article L. 133-6 du Code de l'action sociale et des familles.

### d. Quelques particularités de la réglementation de l’activité

**Capacité d'accueil maximale**

La capacité d'accueil des enfants de moins de six ans au sein des établissements susvisés (cf. supra « 1°. Définition de l’activité ») est limitée.

Dès lors que le nombre hebdomadaire d'enfants ne dépasse pas cent pour cent de la capacité prévue par l'autorisation de création, un surnombre peut être envisagé s'il n'excède pas :

- 10 % de la capacité d'accueil pour les établissements de vingt places ou moins ;
- 15 % de la capacité d’accueil des établissements entre vingt et une et quarante places ;
- 20 % de la capacité d'accueil des établissements de quarante et une place ou plus.

*Pour aller plus loin* : article R. 2324-27 du Code de la santé publique.

**Délégation**

Le gestionnaire d'un établissement d'accueil d'enfants de moins de six ans peut, par écrit, en déléguer la direction à un professionnel. Il doit pour cela en adresser une copie au président du conseil départemental du département qui a délivré l'autorisation de création de l'établissement.

Ce document contient les informations suivantes, relatives à l'étendue de la délégation :

- la définition et la mise en œuvre du projet d'établissement ou de service ;
- l'animation et la gestion des ressources humaines ;
- la gestion budgétaire, financière et comptable ;
- la coordination avec les institutions et les intervenants extérieurs.

*Pour aller plus loin* : article R. 2324-37-2 du Code de la santé publique.

**Assurance de responsabilité civile**

Le gestionnaire de l'établissement doit souscrire une assurance de responsabilité civile pour ses salariés et pour les bénévoles et intervenants extérieurs et non salariés qui participent à l'accueil des enfants.

En outre, dès lors que survient un accident ayant entraîné une hospitalisation ou le décès de l'un des enfants, le gestionnaire doit le déclarer sans délai, au président du conseil départemental.

*Pour aller plus loin* : article R. 2324-44-1 du Code de la santé publique.

## 3°. Démarches et formalités d’installation

### a. Demande d'autorisation ou avis de création, extension et transformation

La création, l'extension ou la transformation d'un établissement d'accueil d'enfants de moins de six ans est soumis à la délivrance d'une autorisation dont les modalités dépendent de la nature de l'établissement.

#### Délivrance d'une autorisation en vue de la création d'un établissement ou service géré par une personne privée

**Autorité compétente**

La demande d'autorisation doit être adressée au président du conseil départemental du département dans lequel se trouve l'établissement.

**Pièces justificatives**

Le professionnel doit adresser une demande comportant les éléments suivants :

- une étude des besoins ;
- l'adresse de l'établissement ou service d'accueil ;
- les statuts de l'établissement ou du service d'accueil ou de l'organisme gestionnaire pour les établissements et services agréés par une personne de droit privé ;
- les objectifs, modalités d'accueil et les moyens mis en œuvre en fonction du public accueilli et du contexte local (capacité d'accueil, effectifs, qualification du personnel etc.) ;
- le projet d'établissement ou de service comprenant :
  - un projet éducatif (comprenant les dispositions prises pour assurer l'accueil, le soin, le développement, l'éveil et le bien-être des enfants),
  - un projet social, (précisant les modalités d'intégration de l'établissement ou du service dans son environnement social ),
  - les prestations d'accueil proposées, (durées, les rythmes d'accueil etc.),
  - le cas échéant, les dispositions particulières prises pour accueillir les enfants présentant un handicap ou une maladie chronique,
  - la présentation des compétences professionnelles du personnel,
  - pour les services d'accueil familial, les modalités de formation continue des assistantes maternelles, leur soutien professionnel et le suivi des enfants à leur domicile,
  - la définition de la place des familles et de leur participation à la vie de l'établissement ou du service,
  - les modalités des relations avec les organismes extérieurs ;
- le plan des locaux ;
- la copie de la décision d'autorisation d'ouverture au public et les pièces justifiant l'autorisation (cf. article R. 11-19-29 du Code de la construction et de l'habitat).

*Pour aller plus loin* : articles L. 2324-1 et R. 2324-18 du Code de la santé publique.

**Délais**

Le président du conseil départemental, après réception du dossier complet, sollicite l'avis du maire de la commune d'implantation de l'établissement et dispose d'un délai de trois mois, à compter de la date de réception du dossier complet, pour prendre sa décision. L'absence de réponse durant le délai susvisé vaut autorisation d'ouverture de l'établissement.

**À noter**

L'autorisation du président du conseil départemental mentionne les informations relatives à l'établissement (capacité d'accueil, prestations, âge des enfants accueillis, conditions de fonctionnement) et les informations relatives au personnel.

*Pour aller plus loin* : articles R. 2324-19 et suivants du Code de la santé publique.

#### Délivrance d'un avis en vue de la création d'un établissement de service public

L'autorisation prévue pour la création, l'extension et la transformation des établissements et services publics est délivrée par la collectivité publique après avis du président du conseil départemental.

**Autorité compétente et pièces justificatives**

Le professionnel adresse un dossier comportant les mêmes pièces justificatives que celles nécessaires pour l'autorisation délivrée aux établissements gérés par une personne privée (cf. supra « 3°. a. Délivrance d'une autorisation en vue de la création d'un établissement ou service géré par une personne privée ») à la collectivité publique qui sollicite l'avis du président du conseil départemental.

Ce dernier dispose d'un délai de trois mois à compter de la réception du dossier complet, pour rendre son avis portant notamment sur :

- les prestations ;
- les capacités d'accueil ;
- le fonctionnement ;
- le personnel de l'établissement.

*Pour aller plus loin* : articles R. 2324-21 et suivants du Code de la santé publique.

#### Établissements d'accueil collectif à caractère éducatif

L'organisation de ces établissements d'accueil des enfants de moins de six ans, hors période scolaire, est soumise à la délivrance d'une autorisation du représentant de l’État dans le département après un avis du médecin responsable du service départemental de protection maternelle des enfants.

L'autorité compétente ainsi que les pièces justificatives sont les mêmes que celles nécessaires à la délivrance d'une autorisation en vue d'ouvrir un établissement ou service de nature privée ou publique (cf. supra « 3°. a. Délivrance d'une autorisation en vue de la création d'un établissement ou service géré par une personne privée »).

*Pour aller plus loin* : article L. 2324-1 alinéa 3 du Code de la santé publique.

### b. Formalités de déclaration de l’entreprise

**Autorité compétente**

Le professionnel doit procéder à la déclaration de son entreprise, et pour cela, doit, effectuer une déclaration auprès de la chambre de commerce et de l'industrie (CCI).

**Pièces justificatives**

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

**Délais**

Le centre des formalités des entreprises de la CCI adresse le jour même, un récépissé au professionnel les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

**Voies de recours**

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

**Coût**

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### c. Le cas échéant, enregistrer les statuts de la société

Le professionnel, une fois les statuts de la société datés et signés, doit procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

**Autorité compétente**

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

**Pièces justificatives**

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.