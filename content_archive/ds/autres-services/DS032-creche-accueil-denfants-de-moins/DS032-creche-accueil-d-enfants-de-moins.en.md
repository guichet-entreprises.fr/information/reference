﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS032" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Nursery – Care of children aged under 6 years" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="nursery-care-of-children-aged-under-six-years" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/nursery-care-of-children-aged-under-six-years.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="nursery-care-of-children-aged-under-six-years" -->
<!-- var(translation)="Auto" -->


Nursery – Care of children aged under 6 years
==================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The function of institutions and community care services is to keep children under the age of six on a non-permanent basis and to ensure their health, safety, well-being and development.

These accommodation establishments, on an occasional or seasonal basis, may take the following forms:

- Be managed by a private person (physical or moral);
- public and/or educational (leisure, school holidays, work leave) outside the parental home.

This includes:

- collective host facilities including:- collective nurseries,
  - daycare centres,
  - family care services or family nurseries that provide non-permanent care of children in the homes of maternal assistants (the same institution or so-called "multi-host" can associate collective and foster care or foster care regular and occasional hospitality;
- Parental nurseries run by a parent's association;
- kindergartens (including exclusively children over two years of school or part-time schooling);
- micro-crèches with a capacity limited to ten places.

**Please note**

The capacity of these establishments is limited depending on the nature of the establishment.

*For further information*: Articles R. 2324-17 and the following sections of the Public Health Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the structure in which the activity is carried out:

- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for a liberal profession, it is the Urssaf;
- for civil societies, it is the registry of the commercial court or the registry of the district court (for the departments of the Lower Rhine, Upper Rhine and Moselle, it is the registry of the district court).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional who wishes to run a foster care facility for children under the age of six must justify professional qualifications that vary depending on the nature of the institution and the function of the professional.

**Please note**

Once the professional is qualified to lead one of the following institutions, he or she may perform the same duties in a lower-capacity institution.

#### Headmaster

**Establishment of more than forty-one places**

A professional who wishes to lead an institution must hold one of the following degrees:

- a state medical degree;
- a state diploma as a childminder with at least three years of professional experience;
- a state diploma as a young children's educator provided:- that it justifies certification attesting to its competence in the field of management or management,
  - he justifies three years of professional experience,
  - that the establishment or service includes a full-time person:- a state diploma as a childminder,
    * a state nursing degree that warrants at least one year of experience with young children.

**Please note**

The director of an institution with a capacity of more than sixty places must be assisted by an assistant holding one of the above diplomas or referred to in articles R. 2324-35 and R. 2324-46.

*For further information*: Articles R.2324-34 and the following sections of the Public Health Code.

**Establishing capacity of forty or less**

The following professionals may be in charge of a foster care facility for children under the age of six with capacity of less than forty places or the technical responsibility of a parenting facility:

- A certified childminder with three years of professional experience
- a young children's educator with three years of professional experience if he practises with the help of a professional with at least one year's experience with young children and a state graduate as:- childminders,
  - nurse.

*For further information*: Article R. 2324-35 of the Public Health Code.

**Facilities with a capacity of 20 or less**

The following professionals can manage a childcare facility for children under the age of six, with a capacity of less than 20 places or less:

- A certified childminder with three years of professional experience
- a young children's educator with three years of work experience.

*For further information*: Article R. 2324-35 of the Public Health Code.

**What to know**

Micro-crèches, whose capacity is limited to ten places, are not required to have a director but a technical reference. However, when several establishments are managed by the same person and the total capacity of these establishments is greater than 20 places, the latter is required to appoint a director.

*For further information*: Article R. 2324-36 and R. 2324-36-1 of the Public Health Code.

**Derogatory measures**

If no professional who meets the above qualification requirements can manage a childcare facility for children under the age of six, exemptions are provided depending on the size of the institution concerned.

Thus, the management of an establishment or service with a capacity of more than forty places can be entrusted to a professional holder:

- a state diploma as a young children's educator if it justifies three years of work experience, including at least two years as:- Director
  - Deputy Director,
  - technical manager of such an establishment.
- state degree as a midwife or nurse and justifying:- Three years of professional experience as a director or deputy director of such an institution;
  - or a minimum Level II certification, registered in the national directory of professional certifications attesting to his coaching or management skills and three years' experience with children under three years of age.

The professional can manage institutions with a capacity of between twenty and one and forty places as long as he justifies a state diploma as a social service assistant, a specialist educator, a social and family economics advisor. , psychomotrician, or a DESS or a master 2 justifying:

- Three years of experience as a director, deputy director or head of such an institution;
- minimum Level II certification in the national directory of professional certifications that warrants skills in coaching or management and three years of experience with children under three years of age.

The professional can manage an institution of twenty places or less if he holds one of the following diplomas:

- a state diploma:- midwife,
  - nursing,
  - social service assistant,
  - specialist educator,
  - social and family economics advisor,
  - psychomotrician;
- a specialized graduate degree (DESS) or a master's degree II (B.A. 5) in psychology and warranting three years of experience as a director, assistant director or technical manager of such an institution or three years of experience with young children.

The professional who has been in charge of a parenting institution for three years can lead an institution run by a private person.

*For further information*: Article R. 2324-46 of the Public Health Code.

#### Staff

The institution for children under the age of six must be made up of professionals with the following degrees for at least 40% of the workforce:

- a state diploma as a childminder;
- a state diploma as a young children's educator;
- a diploma as a childcare assistant;
- a state nursing degree;
- a state degree as a psychomotrician.

In addition to these professionals, the institution must be made up of professional incumbents for up to 60% of the workforce:

- training titles:- a certificate of early childhood professional aptitude,
  - a family worker's certificate or a state diploma as a social and family intervention technician,
  - a state certificate as a technician facilitator of popular education and youth, early childhood option,
  - a certificate of professional studies, health and social option,
  - A certificate of fitness for home help functions
- five years of professional experience as a nursery assistant
- three years of professional experience with children in a public institution for children under the age of six.

*For further information*: Article R. 2323-42 of the Public Health Code and order of 26 December 2000 relating to staff of institutions and services for children under the age of six.

**Please note**

If an institution or service has a capacity of more than ten places, depending on the number of children and their age, it must be composed of qualified professionals in the following fields:

- Psychology
- Social
- Health;
- Educational;
- Cultural.

If this capacity exceeds ten places, the staff must be composed of a specialist doctor or paediatrician, or, failing that, a general practitioner with professional experience in the field of paediatrics.

*For further information*: Articles R. 2324-38 and R. 2324-39 of the Public Health Code.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Free Exercise (LE))

There are no regulations for a national of a member of the European Union (EU) or part of the European Economic Area (EEA) wishing to practise in a childcare facility under the age of six in France, as a member of the European Economic Area (EEA) temporary and casual or permanent.

Therefore, the EU or EEA national will be subject to the same installation conditions as the French professional (see above "2o. Conditions of installation").

### c. Conditions of honorability

To practise as a professional in a foster care facility for children under the age of six, the professional must not have been incapacitated and/or definitively convicted for:

- or a sentence of at least two months' imprisonment for harming persons;
- Sexual assault
- harming or endangering minors;
- Receiving;
- illicit use of narcotics.

In addition, the manager of such an institution must ensure that all staff are not subject to the above convictions.

*For further information*: Articles L. 2324-1 paragraph 5 and R. 2324-33 of the Public Health Code; Article L. 133-6 of the Code of Social Action and Families.

### d. Some peculiarities of the regulation of the activity

**Maximum capacity**

The capacity to accommodate children under the age of six in the above institutions (see above "1.0th). Definition of activity") is limited.

As long as the weekly number of children does not exceed one hundred per cent of the capacity provided by the creative authorisation, an overstrip may be considered if it does not exceed:

- 10% of the capacity for establishments with 20 or fewer places;
- 15% of the capacity of establishments between twenty-one and forty places;
- 20% of the capacity of establishments of forty-one or more places.

*For further information*: Article R. 2324-27 of the Public Health Code.

**Delegation**

The manager of a foster care facility for children under the age of six may, in writing, delegate management to a professional. To do so, he must send a copy to the chairman of the departmental council of the department that has issued the authorisation for the creation of the establishment.

This document contains the following information on the extent of the delegation:

- The definition and implementation of the settlement or service project;
- Animation and human resources management;
- Budget, financial and accounting management
- coordination with external institutions and stakeholders.

*For further information*: Article R. 2324-37-2 of the Public Health Code.

**Liability insurance**

The manager of the establishment must take out liability insurance for his employees and for the volunteers and external and non-salary workers who participate in the reception of the children.

In addition, if an accident has occurred resulting in the hospitalization or death of one of the children, the manager must report it immediately to the president of the county council.

*For further information*: Article R. 2324-44-1 of the Public Health Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Request for authorisation or notice of creation, extension and transformation

The creation, extension or transformation of a foster care facility for children under the age of six is subject to the issuance of an authorization, the terms of which depend on the nature of the institution.

#### Issuing an authorization for the creation of a privately run institution or service

**Competent authority**

The request for authorisation must be addressed to the chairman of the departmental council of the department in which the establishment is located.

**Supporting documents**

The professional must submit an application with:

- A needs study
- The address of the establishment or reception service;
- The statutes of the institution or host service or the governing body for institutions and services approved by a private person;
- objectives, reception arrangements and the means implemented according to the welcomed public and the local context (capacity, staffing, staff qualification, etc.);
- The settlement or service project including:- an educational project (including arrangements for the reception, care, development, awakening and well-being of children)
  - a social project, (specifying how the institution or service will be integrated into its social environment),
  - the services offered, (durations, paces of reception, etc.),
  - If necessary, the special arrangements made to accommodate children with disabilities or chronic illnesses,
  - Presentation of the professional skills of staff,
  - for family care services, the continuing training arrangements for childminders, their professional support and the follow-up of children at home,
  - Defining the place of families and their participation in the life of the establishment or service,
  - How relations with outside agencies are married;
- The map of the premises
- copy of the decision to authorize the public and the documents justifying the authorization (see Section R. 11-19-29 of the Building and Habitat Code).

*For further information*: Articles L. 2324-1 and R. 2324-18 of the Public Health Code.

**Timeframe**

The president of the county council, after receiving the full file, seeks the opinion of the mayor of the municipality of the establishment and has a period of three months, from the date of receipt of the complete file, to make his decision. The lack of a response during the aforementioned period is worth the institution's opening permission.

**Please note**

The authorisation of the president of the county council mentions information about the establishment (capacity, benefits, age of children, operating conditions) and information relating to staff.

*For further information*: Articles R. 2324-19 and the following sections of the Public Health Code.

#### Issuing a notice for the creation of a public institution and service

The authorization for the creation, extension and transformation of public institutions and services is issued by the public community after advice from the chairman of the departmental council.

**Competent authority and supporting documents**

The professional sends a file with the same supporting documents as those required for the authorization issued to institutions run by a private person (see supra "3.3. a. Issuing an authorization for the creation of a privately run institution or service") to the public community seeking the advice of the chair of the departmental council.

The latter has three months from the time the full file is received, in order to give its opinion, including:

- Benefits
- Reception capacity
- How it works
- staff at the facility.

*For further information*: Articles R. 2324-21 and the following of the Public Health Code.

#### Educational collective reception institutions

The organization of these institutions for children under the age of six, outside the school period, is subject to the issuance of an authorisation from the State representative in the department after a notice from the doctor in charge of the departmental maternal protection of children.

The competent authority and the supporting documents are the same as those necessary to issue an authorization to open a private or public establishment or service (see above "3." (a) Issuing an authorization for the creation of a privately run institution or service").

*For further information*: Article L. 2324-1 paragraph 3 of the Public Health Code.

### b. Company reporting formalities

**Competent authority**

The professional must report his company, and for this, must make a declaration with the Chamber of Commerce and Industry (CCI).

**Supporting documents**

The person concerned must provide the[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Timeframe**

The CCI's business formalities centre sends the missing documents to the professional on the same day. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

### c. If necessary, register the company's statutes

The professional, once the company's statutes have been dated and signed, must register them with the Corporate Tax Office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

