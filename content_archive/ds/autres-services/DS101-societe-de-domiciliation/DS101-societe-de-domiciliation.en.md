﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS101" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Company address provider" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="company-address-provider" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/company-address-provider.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="company-address-provider" -->
<!-- var(translation)="Auto" -->


Company address provider
================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The activity of a domicile company consists of providing a professional legal address (postal, commercial, administrative, head office) to individuals and corporations registered in the Register of Trade and Companies (RCS) or in the (RM) to carry out their professional activity. The exercise of this activity requires prefectural approval.

*For further information*: Articles L. 123-11-2 and the following of the Code of Commerce.

### b. Competent Business Formalities Centre (CFE)

Commercially active, commercial companies are the responsibility of the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

In order to carry out the domicile activity, the company must:

- obtain prefectural approval
- rcS (see "3.0). b. Company registration").

To be approved, the domicile company must:

- provide residents with premises to ensure:- confidentiality necessary to carry out their activities and to the meeting of the various bodies (management, administration, supervision),
  - Keeping all mandatory documents and records;
- justify that it has legally available premises to legal or physical persons (as owner or holder of a commercial lease);
- If so, when the professional operates several secondary schools, they must comply with the above conditions;
- respect the conditions of honour specific to his profession (see "2.0). c. Conditions of honorability").

**Please note**

The fact that a professional engages in the activity of domicile without having a licence is punishable by six months' imprisonment and a fine of 7,500 euros.

*For further information*: Articles L. 123-11-3 and L. 123-11-4 of the Code of Commerce.

### b. Professional Qualifications - European Nationals (Free Service Or Freedom of establishment)

There are no regulations for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement to carry out the temporary and temporary domicile activity in France. casual or permanent.

As such, the eu-married professional is subject to the same requirements as the French national (see infra "3°. Installation procedures and formalities").

### c. Conditions of honorability

**Place of exercise and installation**

A domicile company may not operate in a residential or mixed-use professional premises.

*For further information*: Article L. 123-11-2 of the Code of Commerce.

**Incompatibilities**

The professional who wishes to carry out the activity of domicile must not, in order to obtain a license, have been subject to:

- a final conviction for a crime or a sentence of at least three months' imprisonment without a conditional sentence for the offences under the II. (3) of Article L. 123-11-3 Code of Commerce;
- Disciplinary or administrative sanction that resulted in the withdrawal of the accreditation;
- personal bankruptcy, a prohibition or forfeiture of exercise in the context of safeguarding, liquidation or judicial redress proceedings.

**Please note**

When the activity is carried out by a corporation, shareholders or associates holding at least 25% of the votes, shares or voting rights as well as the executives must respect the same conditions of honorability.

*For further information*: Articles L. 123-11-2 and L. 123-11-4 of the Code of Commerce.

### d. Some peculiarities of the regulation of the activity

**Fighting tax evasion**

The professional carrying out the activity of domicile must implement all the obligations relating to the fight against money laundering and the financing of terrorism provided by the Monetary and financial code.

*For further information*: Article L. 123-11-5 of the Code of Commerce; Articles L. 561-1 and the following of the Monetary and Financial Code.

**Home contract**

The contract must be concluded between the domiciled legal or physical person and the domicile company. This contract must be written down and mention references to the approval granted to the home.

**Please note**

This contract is for a minimum of three months and renewable by tacit renewal.

*For further information*: Article R. 123-168 of the Code of Commerce.

**Provisions for public institutions (ERP)**

As the premises are open to the public, the professional must comply with the rules relating to public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Application for accreditation

**Competent authority**

The professional must apply for accreditation with the prefect of the department where the head office of the domicile company is located, or, for Paris, with the police prefecture.

**Supporting documents**

The application must include:

- A declaration including its name, activity, address and, if applicable, that of secondary schools;
- The civil status, address, occupation and quality of the operator;
- A valid piece of identification
- whether the professional is a legal person, its legal form and the identity of legal or statutory representatives, executives and shareholders holding at least 25% of the votes, shares or voting rights;
- proof of compliance with the obligations necessary to obtain accreditation (see above "2. a. Professional qualifications");
- a certificate of honour that he is not the subject of a conviction or prohibition measure to practice his profession (see above "2." c. Condition of honorability").

**Timeframe**

The prefect makes his decision within two months of receiving the application for accreditation. The lack of a response at the end of this period is the result of the rejection of the application.

**Please note**

Any changes must be reported within two months to the prefect who issued the accreditation. In addition, if the professional no longer meets the requirements for obtaining accreditation, the latter may be withdrawn or suspended by the prefect for a period of six months.

If the company creates a secondary school, it must justify within two months to the prefect who has approved it that the necessary conditions are fulfilled for each of the secondary schools.

*For further information*: Article R. 123-166-1 and below, and Section R. 123-166-2 of the Code of Commerce.

### b. Company reporting and registration formalities

**Competent authority**

The professional engaged in a directing activity must proceed with the declaration of his company. To do so, it must file a declaration with the ICC.

**Supporting documents**

The person concerned must provide the supporting documents depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sends a receipt to the professional on the same day mentioning the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the CFE refuses to receive the file, the applicant has an appeal before the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company to be registered.

**Company registration**

The professional must register with the RCS. It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

**Please note**

The corporation wishing to carry out the activity of a domicile company must justify the enjoyment of the premises where it establishes its head office, or where it is located abroad, the establishment of representation, the agency or the branch established on French territory.

*For further information*: Article L. 123-11 of the Code of Commerce.

*For further information*: Section 635 of the General Tax Code.

### c. If necessary, register the company's statutes

The professional carrying out the directing activity must, once the company's statutes are dated and signed, register them with the corporate tax office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it (for example, status in the notarial form).

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Sections 635 and 862 of the General Tax Code.

