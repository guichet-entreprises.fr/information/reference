﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS101" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Société de domiciliation" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="societe-de-domiciliation" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/societe-de-domiciliation.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="societe-de-domiciliation" -->
<!-- var(translation)="None" -->

# Société de domiciliation

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité de société de domiciliation consiste à fournir à titre professionnel une adresse juridique (postale, commerciale, administrative, siège social) aux personnes physiques et morales immatriculées au registre du commerce et des sociétés (RCS) ou au répertoire des métiers (RM) en vue d'exercer leur activité professionnelle. L'exercic de cette activité requiert un agrément préfectoral.

*Pour aller plus loin* : articles L. 123-11-2 et suivants du Code de commerce.

### b. Centre de formalités des entreprises (CFE) compétent

Exerçant une activité commerciale, les sociétés commerciales relèvent de la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité de domiciliation, la société doit :

- obtenir un agrément préfectoral ;
- être immatriculée au RCS (cf. « 3°. b. Immatriculation de la société »).

Pour être agréée, la société de domiciliation doit :

- mettre à disposition des personnes domiciliées des locaux permettant d'assurer :
  - la confidentialité nécessaire à l'exercice de leur activité et à la réunion des différents organes (direction, administration, surveillance),
  - la tenue de l'ensemble des documents et registres obligatoires ;
- justifier qu'elle dispose juridiquement des locaux mis à disposition des personnes morales ou physiques (en qualité de propriétaire ou de titulaire d'un bail commercial) ;
- le cas échéant, lorsque le professionnel exploite plusieurs établissements secondaires ces derniers doivent respecter les conditions ci-dessus ;
- respecter les conditions d'honorabilité propres à sa profession (cf. « 2°. c. Conditions d'honorabilité »).

**À noter**

Le fait pour un professionnel d'exercer l'activité de domiciliation sans être titulaire d'un agrément est puni de six mois d'emprisonnement et de 7 500 euros d'amende.

*Pour aller plus loin* : articles L. 123-11-3 et L. 123-11-4 du Code de commerce.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement)

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer en France l'activité de domiciliation à titre temporaire et occasionnel ou permanent.

À ce titre, le professionnel ressortissant de l'UE est soumis aux mêmes exigences que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation »).

### c. Conditions d’honorabilité

#### Lieu d'exercice et installation

Une société de domiciliation ne peut exercer son activité dans un local d'habitation ou à usage mixte professionnel.

*Pour aller plus loin* : article L. 123-11-2 du Code de commerce.

#### Incompatibilités

Le professionnel qui souhaite exercer l'activité de domiciliation ne doit pas, en vue d'obtenir un agrément, avoir fait l'objet :

- d'une condamnation définitive pour crime ou d'une peine d'au moins trois mois d'emprisonnement sans sursis pour les infractions prévues au II. 3° de l'article L. 123-11-3 du Code de commerce ;
- d'une sanction disciplinaire ou administrative ayant entraîné le retrait de l'agrément ;
- de faillite personnelle, d'une mesure d'interdiction ou de déchéance d'exercice prononcée dans le cadre de procédures de sauvegarde, liquidation ou redressement judiciaire.

**À noter**

Lorsque l'activité est exercée par une personne morale, les actionnaires ou associés détenant au moins 25 % des voix, parts ou droits de vote ainsi que les dirigeants doivent respecter les mêmes conditions d'honorabilité.

*Pour aller plus loin* : articles L. 123-11-2 et L. 123-11-4 du Code de commerce.

### d. Quelques particularités de la réglementation de l’activité

#### Lutte contre les fraudes fiscales

Le professionnel exerçant l'activité de domiciliation doit mettre en œuvre l'ensemble des obligations relatives à la lutte contre le blanchiment des capitaux et le financement du terrorisme prévues par le Code monétaire et financier.

*Pour aller plus loin* : article L. 123-11-5 du Code de commerce ; articles L. 561-1 et suivants du Code monétaire et financier.

#### Contrat de domiciliation

Le contrat doit être conclu entre la personne morale ou physique domiciliée et la société de domiciliation. Ce contrat doit être rédigé par écrit et mentionner les références de l'agrément délivré au domiciliataire.

**À noter**

Ce contrat est d'une durée minimale de trois mois et renouvelable par tacite reconduction.

*Pour aller plus loin* : article R. 123-168 du Code de commerce.

#### Dispositions applicables aux établissements recevant du public (ERP)

Les locaux étant ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

## 3°. Démarches et formalités d’installation

### a. Demande d'agrément

#### Autorité compétente

Le professionnel doit adresser une demande d'agrément auprès du préfet du département où se situe le siège social de la société de domiciliation, ou, pour Paris, auprès de la préfecture de police.

#### Pièces justificatives

La demande doit comporter :

- une déclaration comprenant sa dénomination, son activité, son adresse et, le cas échéant, celle des établissements secondaires ;
- l'état civil, l'adresse, la profession et la qualité de l'exploitant ;
- une pièce d'identité en cours de validité ;
- si le professionnel est une personne morale, sa forme juridique et l'identité des représentants légaux ou statutaires, des dirigeants et des actionnaires détenant au moins 25 % des voix, parts ou droits de vote ;
- un justificatif du respect des obligations nécessaires à l'obtention de l'agrément (cf. supra « 2°. a. Qualifications professionnelles ») ;
- une attestation sur l'honneur qu'il ne fait pas l'objet d'une condamnation ou mesure d'interdiction pour exercer sa profession (cf. supra « 2°. c. Condition d'honorabilité »).

#### Délai

Le préfet rend sa décision dans un délai de deux mois à compter de la réception de la demande d'agrément. L'absence de réponse à l'issue de ce délai vaut rejet de la demande.

**À noter**

Tout changement doit être déclaré dans un délai de deux mois au préfet qui a délivré l'agrément. En outre, dès lors que le professionnel ne remplit plus les conditions requises pour l'obtention de l'agrément, ce dernier peut être retiré ou suspendu par le préfet pour une durée de six mois.

Si la société crée un établissement secondaire, elle doit justifier dans les deux mois auprès du préfet qui l'a agréée que les condition nécessaires sont réalisées pour chacun des établissements secondaires.

*Pour aller plus loin* : article R. 123-166-1 et suivants, et article R. 123-166-2 du Code de commerce.

### b. Formalités de déclaration et d'immatriculation de l’entreprise

#### Autorité compétente

Le professionnel exerçant une activité de domiciliation doit procéder à la déclaration de son entreprise. Pour cela, il doit effectuer une déclaration auprès de la CCI.

#### Pièces justificatives

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI adresse le jour même un récépissé au professionnel mentionnant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le CFE refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société à immatriculer.

#### Immatriculation de la société

Le professionnel doit s’immatriculer au RCS.

**À noter**

La personne morale qui souhaite exercer l'activité de société de domiciliation doit justifier de la jouissance des locaux où elle établit son siège social, ou lorsque celui-ci est situé à l'étranger, l'établissement de représentation, l'agence ou la succursale établi sur le territoire français.

*Pour aller plus loin* : article L. 123-11 du Code de commerce.

*Pour aller plus loin* : article 635 du Code général des impôts.

### c. Le cas échéant, enregistrer les statuts de la société

Le professionnel exerçant l'activité de domiciliation doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige (par exemple, statuts sous la forme notariée).

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : articles 635 et 862 du Code général des impôts.