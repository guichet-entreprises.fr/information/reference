﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS049" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Maisons de retraite – Accueil collectif de personnes âgées" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="maisons-de-retraite-accueil-collectif" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/maisons-de-retraite-accueil-collectif-de-personnes-agees.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="maisons-de-retraite-accueil-collectif-de-personnes-agees" -->
<!-- var(translation)="None" -->

# Maisons de retraite – Accueil collectif de personnes âgées

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Les établissements d'hébergement pour personnes âgées (EHPA), plus communément appelés maisons de retraite, sont des établissements d'accueil et d'hébergement collectif assurant la prise en charge des personnes âgées, à titre temporaire ou permanent de jour comme de nuit.

**Bon à savoir**

Les maisons de retraite ne sont pas médicalisées. Toutefois, si elles accueillent des personnes âgées dépendantes, elles doivent obtenir une autorisation de l'agence régionale de santé (ARS) et signer une convention avec le conseil général en vue d'acquérir le statut d’établissement d'hébergement pour personnes âgées dépendantes (EHPAD) et assurer des soins médicalisés. Ces EHPAD ne seront pas traités dans cette fiche.

*Pour aller plus loin* : article L. 312-1 du Code de l'action sociale et des familles.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les activités commerciales, il s'agit de la chambre de commerce et d'industrie (CCI) ;
- en cas de création d'une association :
  - si l'association à une activité lucrative, le CFE compétent sera le service des associations de la préfecture du département et le greffe du tribunal de commerce,
  - si l'association emploie des salariés, le CFE compétent sera l'Urssaf.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le professionnel qui souhaite ouvrir ou diriger un EHPA doit :

- être titulaire d'un niveau de qualification de niveau bac +3 ou bac +4 ;
- effectuer un appel à projet social ou médico-social lorsqu'il souhaite bénéficier de financements publics (totalement ou partiellement) (cf. infra « 3°. a. Effectuer un dossier d'appel à projet ») ;
- effectuer une demande d'autorisation de création d'un EHPA (cf. infra « 3°. b. Demande d'autorisation de création d'un EHPA »).

#### Directeur de l'établissement

Pour exercer la fonction de directeur d'un ou plusieurs EHPA, le professionnel doit justifier d'une certification de niveau II (bac +3 ou bac +4) enregistrée au Répertoire national des certifications professionnelles [RNCP](http://www.cncp.gouv.fr).

Peut également diriger un établissement de moins de dix salariés ou un établissement d'une capacité inférieure à vingt cinq places, le professionnel dès lors qu'il :

- est titulaire :
  - d'un diplôme sanitaire ou social de niveau III (bac +2),
  - d'une expérience professionnelle d'au moins trois ans dans le secteur sanitaire ou médico-social ;
- a suivi une formation à l'encadrement ou s'engage à la suivre dans un délai de cinq ans.

**À noter**

Lorsque le gestionnaire d'un EHPA (personne physique ou morale) en confie la direction à un professionnel, il doit transmettre à l'autorité compétente pour délivrer l'autorisation d'ouvrir l'établissement, un document écrit précisant :

- les compétences et les missions confiées au dirigeant ;
- la nature et l'étendue de la délégation notamment sur :
  - la mise en œuvre du projet d'établissement ou de service,
  - la gestion et l'animation des ressources humaines,
  - la gestion budgétaire, financière et comptable,
  - la coordination avec les institutions et intervenants extérieurs.

Ce professionnel doit, pour diriger cet établissement, être titulaire d'une certification de niveau I (bac +5).

*Pour aller plus loin* : articles D. 312-176-5 et D. 312-176-10 du Code de l'action sociale et des familles.

#### Personnel de l'établissement

Les prestations proposées au sein d'un EHPA doivent être effectuées par des équipes pluridisciplinaires qualifiées en vue d'assurer le confort et la qualité de séjour des personnes âgées.

*Pour aller plus loin* : article L. 312-1 du Code de l'action sociale et des familles.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement)

#### En vue d'une Libre Prestation de Services (LPS)

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) en vue de diriger un EHPA à titre temporaire et occasionnel en France. À ce titre, le ressortissant est soumis aux mêmes exigences que le ressortissant français (cf. supra « 2°. a. Qualifications professionnelles »).

#### En vue d'un Libre Établissement (LE)

Le ressortissant de l'UE légalement établi et qui exerce l'activité de dirigeant d'un EHPA peut exercer à titre temporaire la même activité en France.

Pour cela, l'intéressé doit être titulaire d'un titre, d'un diplôme ou d'une certification professionnelle d'un niveau équivalent à celui requis pour le ressortissant français (cf. supra « 2°. a. Qualifications professionnelles »).

*Pour aller plus loin* : articles D. 312-176-11 à D. 312-176-13 du Code de l'action sociale et des familles.

### c. Quelques particularités de la réglementation de l’activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP). Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

#### Obligation d'avoir un système de rafraîchissement de l'air

L'EHPA doit être aménagé d'une pièce équipée d'un système de rafraîchissement de l'air dont les conditions sont fixées au sein de l'arrêté ci-dessous.

*Pour aller plus loin* : article D. 312-161 du Code de l'action sociale et des familles ; arrêté du 7 juillet 2005 fixant le cahier des charges du plan d'organisation à mettre en œuvre en cas de crise sanitaire ou climatique et les conditions d'installation d'un système fixe de rafraîchissement de l'air ou de mise à disposition d'un local ou d'une pièce rafraîchi(e) dans les établissements mentionnés à l'article L. 313-12 du Code de l'action sociale et des familles.

#### Contrat de séjour

Dès lors que la durée du séjour est supérieure à deux mois (de manière continue ou discontinue), un contrat de séjour doit être conclu entre le représentant de l'établissement et la personne accueillie.

Ce contrat doit comporter les informations suivantes :

- les objectifs de la prise en charge de la personne accueillie ;
- les prestations nécessaires ;
- la description du séjour et de l'accueil ainsi que les conditions de participation financière du bénéficiaire ;
- le tarif d'hébergement.

**À noter**

Un avenant doit préciser dans un délai maximum de six mois les objectifs et les prestations adaptées à la personne accueillie.

*Pour aller plus loin* : articles L. 313-12 et D. 311-1 du Code de l'action sociale et des familles.

#### Bénéficiaires des prestations

Le niveau de perte d'autonomie des personnes âgées détermine leur appartenance à l'un des six groupes dits « groupes iso-resources » (GIR) en tenant compte de l'état de la personne selon le barème fixé à l'annexe 3-6 du Code de l'action sociale et des familles.

Un EHPA ne peut accueillir que des personnes classées au sein des groupes GIR 6 ou 5. En outre, il ne peut garder plus de 10 % de résidents devenus dépendants appartenant aux groupes GIR 1 ou 2. Le cas échéant, l'EHPA devra effectuer un partenariat avec un EHPAD environnant.

*Pour aller plus loin* : article R. 314-170-6 du Code de l'action sociale et des familles.

#### Obligations diverses

##### Documents obligatoires

Chaque EHPA doit posséder :

- un règlement définissant l'ensemble des règles à respecter en matière de vie collective au sein de l'établissement et les droits de chaque personne accueillie ;
- un registre des entrées et sorties de chaque résidents comprenant l'ensemble des informations relatives à leur identité ;
- un livret d'accueil et une charte des droits et libertés de la personne accueillie. Ces documents doivent être remis à chaque résident lors de son arrivée.

*Pour aller plus loin* : articles L. 311-4, L. 311-7 et L. 331-2 du Code de l'action sociale et des familles.

##### Procédure d'évaluation

Les directeurs d'établissement doivent procéder à des évaluations de leurs activités par un organisme agréé par l'Agence nationale de l'évaluation et de la qualité des établissements et services sociaux et médico-sociaux. Ils doivent ensuite les transmettre à l'autorité leur ayant délivré l'autorisation d'ouverture.

*Pour aller plus loin* : article L. 312-8 du Code de l'action sociale et des familles.

## 3°. Démarches et formalités d’installation

### a. Effectuer un dossier d'appel à projet

Lorsque le projet de création, de transformation ou d'extension d'un EHPA fait appel à des financements publics, il doit faire l'objet d'une sélection suite à un appel d'offre émis par l'autorité compétente en respectant le cahier des charges fixé.

#### Procédure de l'appel d'offre

L'autorité compétente fixe un calendrier mentionnant les besoins nécessaires par catégorie d'établissement pour couvrir l'ensemble de l'appel d'offre. Le professionnel bénéficie alors d'un délai de deux mois pour présenter ses observations sur le calendrier.

#### Autorité compétente

Le président du conseil départemental est compétent pour délivrer l'autorisation, si l'établissement fournit des prestations susceptibles d'être prises en charge par l'aide sociale départementale ou lorsqu'elles relèvent d'une compétence dévolue par la loi au département.

#### Pièces justificatives

La demande du candidat doit être adressée par lettre recommandée avec avis de réception ou tout autre moyen permettant d'attester de sa date de réception et comporter :

- l'ensemble des éléments permettant de l'identifier ;
- une déclaration sur l'honneur certifiant qu'il ne fait pas l'objet d'une condamnation définitive pour l'une des infractions mentionnées aux articles L. 313-22 et L. 313-22-1 du Code de l'action sociale et des familles ;
- une déclaration sur l'honneur qu'il ne fait pas l'objet d'une procédure de suspension ou d'interdiction d'exercice de l'activité de dirigeant d'un EHPA ;
- une copie de la dernière certification aux comptes de l'établissement ;
- les éléments descriptifs de son activité dans le domaine social et médico-social et de la situation financière de son activité ;
- tout document permettant de décrire le projet répondant aux exigences du cahier des charges de l'appel d'offre ;
- un état descriptif des principales caractéristiques auxquelles doit satisfaire le projet dont le contenu minimal est fixé à l'arrêté du 30 août 2010 relatif au contenu minimal de l'état descriptif des principales caractéristiques du projet déposé dans le cadre de la procédure de l'appel à projets mentionnée à l'article L. 313-1-1 du Code de l'action sociale et des familles ;
- le cas échéant, les variantes proposées ;
- si plusieurs personnes gestionnaires de l'établissement s'associent pour proposer le projet, un état descriptif de la coopération envisagée.

#### Issue de la procédure

Une fois la demande instruite, les candidats sont entendus par une commission d'information et de sélection dont la composition est fixée à l'article R. 313-1 du Code de l'action sociale et des familles. Cette commission classe ensuite leurs projets.

#### Coût

Gratuit.

*Pour aller plus loin* : articles R. 313-4 et suivants du Code de l'action sociale et des familles.

### b. Demande d'autorisation de création d'un EHPA

#### Autorité compétente

Le demandeur doit adresser sa demande au président du conseil départemental du département au sein duquel il souhaite exercer.

#### Pièces justificatives

La demande doit comporter les éléments suivants :

- pour les projets ne requérant aucun financement public :
  - l'identité du demandeur,
  - un état descriptif des principales caractéristiques du projet comportant notamment :
    - la nature des prestations fournies et leurs bénéficiaires,
    - la capacité prévue et les besoins de l'établissement,
    - une répartition prévisionnelle des effectifs du personnel par type de qualifications,
    - un dossier financier ;
- pour les projets requérant un financement public, les pièces justificatives sont celles requises pour la candidat à l'appel à projet (cf. supra « 3°. a. Effectuer un dossier d'appel à projet »).

#### Délais

En l'absence de réponse dans un délai de six mois, la demande est réputée rejetée. Le demandeur peut, pendant un délai de deux mois, interroger l'autorité sur les motifs de son refus qui devront le cas échéant, lui être adressés dans un délai d'un mois.

#### Voies de recours

La décision du préfet peut faire l'objet, dans un délai de deux mois, d'un recours gracieux, hiérarchique ou contentieux (devant le tribunal administratif).

#### Issue de la procédure

L'autorisation est délivrée dès lors que le projet satisfait aux règles d'organisation et de fonctionnement, répond aux objectifs fixés et aux besoins médico-sociaux et, en cas d'appel à projet, répond au cahier des charges fixé.

**À noter**

Cette autorisation est donnée pour une durée limitée de quinze ans. Son renouvellement est tacite, sauf avis contraire de l'autorité.

*Pour aller plus loin* : articles L. 313-2, R. 313-1 et suivants du Code de l'action sociale et des familles.

### c. Formalités de déclaration de l’entreprise

Cette exigence a pour objet de donner une base légale à l'entreprise s'il s'agit d'une société commerciale.

#### Autorité compétente

Le propriétaire de l'EHPA doit procéder à la déclaration de son entreprise, et pour cela doit effectuer une déclaration auprès de la CCI.

#### Pièces justificatives

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI adresse le jour même, un récépissé au professionnel mentionnant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le centre de formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### d. Le cas échéant, enregistrer les statuts de la société

Le dirigeant d'un EHPA doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.

### e. Autorisation post-immatriculation

#### Visite de conformité

Une fois l'autorisation obtenue, et dans les deux mois qui précèdent l'ouverture de l'établissement, une visite de conformité doit être effectuée au sein de l'établissement en vue de s'assurer qu'il respecte les exigences en matière de qualité et de règles d'organisation et de fonctionnement.

##### Autorité compétente

Le professionnel doit adresser une demande auprès de l'autorité ayant délivré l'autorisation.

##### Pièces justificatives

La demande de visite doit contenir :

- un projet d'établissement tel que défini à l'article L. 311-8 du Code de l'action sociale et des familles ;
- le règlement de fonctionnement de l'établissement ainsi qu'un livret d'accueil prévu à l'article L. 311-4 du Code de l'action sociale et des familles ;
- les éléments relatifs à l'établissement (le budget, le plan des locaux, le tableau des effectifs du personnel, le modèle du contrat de séjour des résidents etc.).

##### Procédure

La visite doit être effectuée au plus tard trois semaines avant la date d'ouverture de l'établissement. À l'issue de celle-ci, un procès-verbal est établi et adressé au professionnel. L'établissement peut commencer à fonctionner dès que le résultat de la visite est positif. En revanche si l'établissement n'est pas conforme, l'autorité compétente adresse au professionnel, dans un délai de quinze jours, l'ensemble des modifications auxquelles il doit procéder ainsi que le délai dont il dispose pour le faire. Le cas échéant, l'ouverture de l'établissement sera subordonnée à une contre-visite de conformité des équipements de l'établissement dans les mêmes conditions que la précédente.

*Pour aller plus loin* : articles L. 311-8, D. 313-11 et suivants, et D. 312-160 du Code de l'action sociale et des familles.