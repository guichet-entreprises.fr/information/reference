﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS049" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Retirement home - Collective accomodation of the elderly" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="retirement-home-collective-accomodation-of-the-elderly" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/retirement-home-collective-accomodation-of-the-elderly.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="retirement-home-collective-accomodation-of-the-elderly" -->
<!-- var(translation)="Auto" -->

Retirement home – Collective accomodation of the elderly
===========================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

Accommodation facilities for the elderly (EHPA), more commonly known as retirement homes, are reception and collective accommodation facilities that provide care for the elderly, either temporarily or permanently during the day. like at night.

**Good to know**

Retirement homes are not medicalized. However, if they are home to dependent elderly people, they must obtain an authorisation from the regional health agency (ARS) and sign an agreement with the General Council to acquire the status of a shelter for (EHPAD) and provide medical care. These EHPADs will not be covered in this card.

*For further information*: Article L. 312-1 of the Code of Social Action and Families.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For commercial activities, it is the Chamber of Commerce and Industry (CCI);
- if an association is created:
  - If the association to a lucrative activity, the competent CFE will be the service of the associations of the prefecture of the department and the registry of the commercial court,
  - if the association employs employees, the competent CFE will be the Urssaf.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional who wishes to open or run an EHPA must:

- Hold a qualification level at the bac level of 3 or bac 4;
- make a call for social or medico-social projects when it wishes to benefit from public funding (totally or partially) (see infra "3.3). a. Make a call-for-project file");
- apply for permission to create an EHPA (see infra "3 degrees). b. Application for permission to create an EHPA").

**Director of the school**

In order to serve as director of one or more EHPAs, the professional must justify a Level II certification (B.A. 3 or bac 4) registered in the national directory of professional certifications.[RNCP](http://www.cncp.gouv.fr).

Can also run an establishment with less than ten employees or an establishment with a capacity of less than twenty-five places, the professional if he:

- holds:- a level III health or social degree (Bac 2),
  - at least three years of professional experience in the health or medical-social sector;
- has completed coaching training or is committed to taking it within five years.

**Please note**

When the manager of an EHPA (natural or legal person) entrusts the management to a professional, he must pass on to the competent authority to issue the authorization to open the institution, a written document stating:

- The skills and missions entrusted to the leader;
- the nature and extent of the delegation, including:- Implementation of the settlement or service project,
  - Human resources management and animation,
  - budget, financial and accounting management,
  - coordination with external institutions and stakeholders.

This professional must, in order to run this institution, hold a Level I certification (Bac 5).

*For further information*: Articles D. 312-176-5 and D. 312-176-10 of the Code of Social Action and Families.

**Staff**

The services offered within an EHPA must be carried out by qualified multidisciplinary teams in order to ensure the comfort and quality of residence of the elderly.

*For further information*: Article L. 312-1 of the Code of Social Action and Families.

### b. Professional Qualifications - European Nationals (Free Service Or Freedom of establishment)

**For a Freedom to provide services**

There is no provision for the national of a Member State of the European Union (EU) or party to the agreement on the European Economic Area (EEA) to run an EHPA on a temporary and casual basis in France. As such, the national is subject to the same requirements as the French national (see above "2." a. Professional qualifications").

**In View to a Freedom of establishment**

A legally established EU national who is the leader of an EHPA may perform the same activity on a temporary basis in France.

In order to do so, the person must hold a title, diploma or professional certification of a level equivalent to that required for the French national (see above "2o. a. Professional qualifications").

*For further information*: Articles D. 312-176-11 to D. 312-176-13 of the Code of Social Action and Families.

### c. Some peculiarities of the regulation of the activity

#### Compliance with safety and accessibility standards

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP). It is advisable to refer to the "Establishment receiving the public" sheet for more information.

#### Requirement to have an air freshener system

The EHPA must be equipped with a room equipped with an air cooling system whose conditions are set within the order below.

*For further information*: Article D. 312-161 of the Code of Social Action and Families; decree of 7 July 2005 setting out the specifications of the organisational plan to be implemented in the event of a health or climate crisis and the conditions for the installation of a fixed air cooling system or the provision of a room or room refreshed in the institutions mentioned in Section L. 313-12 of the Code of Social Action and Families.

#### Residence contract

As long as the length of the stay is longer than two months (continuously or discontinuously), a residence contract must be concluded between the representative of the establishment and the person being admitted.

This contract must include the following information:

- The objectives of caring for the host;
- The necessary benefits
- The description of the stay and reception and the conditions of financial participation of the beneficiary;
- Accommodation rate.

**Please note**

An endorsement must specify within a maximum of six months the objectives and benefits suitable for the person being received.

*For further information*: Articles L. 313-12 and D. 311-1 of the Code of Social Action and Families.

#### Benefit recipients

The level of loss of autonomy of older adults determines their membership in one of the six so-called "iso-resource groups" (GIR) taking into account the condition of the person according to the scale set at the Appendix 3-6 Code of Social Action and Families.

An EHPA can only accommodate people classified within the GIR 6 or 5 groups. In addition, it may not keep more than 10% of residents who have become dependent on THE GIR 1 or 2 groups. If necessary, EHPA will have to partner with a surrounding EHPAD.

*For further information*: Article R. 314-170-6 of the Code of Social Action and Families.

#### Miscellaneous obligations

**Mandatory documents**

Each EHPA must have:

- A regulation defining all the rules to be respected in terms of collective life within the institution and the rights of each person hosted;
- A register of each resident's entrances and exits including all identity information;
- a welcome booklet and a charter of rights and freedoms of the person welcomed. These documents must be given to each resident upon arrival.

*For further information*: Articles L. 311-4, L. 311-7 and L. 331-2 of the Code of Social Action and Families.

**Evaluation procedure**

Principals are required to conduct assessments of their activities by an agency accredited by the National Agency for the Evaluation and Quality of Social and Medical-Social Institutions and Services. They must then pass them on to the authority that has issued them the authorization to open.

*For further information*: Article L. 312-8 of the Code of Social Action and Families.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Make a call-for-project file

When the project to create, transform or extend an EHPA uses public funding, it must be selected following a call for tender issued by the competent authority in accordance with the specifications set.

**Tender procedure**

The competent authority sets a timetable for the needs required by establishment category to cover the entire tender. The professional is then provided with two months to comment on the timetable.

**Competent authority**

The chairman of the county council is competent to issue the authorisation, if the institution provides benefits that can be taken care of by the departmental social assistance or when they fall under a jurisdiction devolved by law. department.

**Supporting documents**

The applicant's request must be addressed by a recommended letter with notice of receipt or any other means to certify the date of receipt and include:

- All the elements that can identify him
- a declaration of honour certifying that he is not the subject of a definitive conviction for any of the offences referred to in Articles L. 313-22 and L. 313-22-1 of the Code of Social Action and Families;
- a statement on the honour that he is not subject to a procedure for suspending or prohibiting the exercise of the activity of an EHPA leader;
- A copy of the last certification to the institution's accounts
- descriptive elements of his social and medico-social activity and the financial situation of his business;
- any documents to describe the project that meets the specifications of the tender;
- a descriptive statement of the main characteristics to which the project, whose minimum content is fixed at the Stopped 30 August 2010 on the minimum content of the description of the main features of the project filed as part of the call for projects procedure referred to in Article L. 313-1-1 of the Code of Social Action and Families;
- If so, the proposed variants;
- If several of the institution's managers join forces to propose the project, a descriptive statement of the proposed cooperation.

**Outcome of the procedure**

Once the application is heard, candidates are heard by an information and selection commission whose composition is set out in Article R. 313-1 of the Code of Social Action and Families. This commission then ranks their projects.

**Cost**

Free.

*For further information*: Articles R. 313-4 and the following articles of the Code of Social Action and Families.

### b. Application for authorisation to create an EHPA

**Competent authority**

The applicant must submit his application to the chairman of the departmental council of the department in which he wishes to practice.

**Supporting documents**

The application must include:

- for projects that do not require public funding:- The identity of the applicant
  - a descriptive statement of the main features of the project including:
    - The nature of the benefits provided and their beneficiaries,
    - The planned capacity and needs of the facility,
    - A projected distribution of staff by type of qualifications,
    - A financial record
- for projects requiring public funding, the supporting documents are those required for the candidate for the call for projects (see supra "3°. a. Make a call-for-project file").

**Timeframe**

If no response is made within six months, the application is deemed to be rejected. The applicant may, for a period of two months, question the authority on the reasons for its refusal, which, if any, must be addressed to him within one month.

**Remedies**

The prefect's decision may be subject to a graceful, hierarchical or contentious appeal within two months (before the administrative court).

**Outcome of the procedure**

The authorisation is issued if the project meets the rules of organisation and operation, meets the objectives set and the medical-social needs and, in the event of a call for projects, meets the specifications set.

**Please note**

This authorization is given for a limited period of fifteen years. Its renewal is tacit, unless the authority advises otherwise.

*For further information*: Articles L. 313-2, R. 313-1 and following of the Code of Social Action and Families.

### c. Company reporting formalities

The purpose of this requirement is to provide a legal basis for the company if it is a commercial company.

**Competent authority**

The owner of the EHPA must file the declaration of his business, and for this must make a declaration with the ICC.

**Supporting documents**

The person concerned must provide the supporting documents depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sends a receipt to the professional on the same day mentioning the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the business formality centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

### d. If necessary, register the company's statutes

The head of an EHPA must, once the company's statutes have been dated and signed, register them with the Corporate Tax Office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

### e. Post-registration authorisation

#### Compliance visit

Once the authorization has been obtained, and within two months of the establishment's opening, a compliance visit must be conducted within the institution to ensure that it meets the quality and rules requirements. organisation and operation.

**Competent authority**

The professional must apply to the authority that issued the authorization.

**Supporting documents**

The request for a visit must include:

- a settlement project as defined in Article L. 311-8 of the Code of Social Action and Families;
- the school's operating regulations and a welcome booklet in Article L. 311-4 of the Code of Social Action and Families;
- settlement elements (budget, space plan, staffing table, residents' contract model, etc.).

**Procedure**

The visit must be made no later than three weeks before the opening date of the establishment. At the end of the meeting, a report is prepared and sent to the professional. The facility can start operating as soon as the result of the visit is positive. On the other hand, if the institution is not compliant, the competent authority sends the professional, within a fortnight, all the changes to which he must make and the time available to him to do so. If necessary, the opening of the establishment will be subject to a counter-visit of compliance of the facilities of the establishment under the same conditions as the previous one.

*For further information*: Articles L. 311-8, D. 313-11 and beyond, and D. 312-160 of the Code of Social Action and Families.

