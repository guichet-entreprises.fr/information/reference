﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS084" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Laverie libre-service" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="laverie-libre-service" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/laverie-libre-service.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="laverie-libre-service" -->
<!-- var(translation)="None" -->

# Laverie libre-service

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l'activité

### a. Définition

L'exploitant d'une laverie en libre-service met à disposition du public des machines à laver le linge, des essoreuses centrifuges et des sèche-linge sans qu'il n'ait besoin d'être présent sur les lieux.

En parallèle de cette activité principale, l'exploitant peut également proposer des services de repassage ou de retouche.

### b. Centre de formalités des entreprises compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Qualifications professionnelles

### a. Qualifications professionnelles

Aucun diplôme spécifique n'est requis pour l'exploitation d'une laverie en libre-service. Toutefois, il est conseillé à l'intéressé d'avoir des connaissances en matière comptable et en gestion.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) n'est soumis à aucune condition de diplôme ou de certification, au même titre que les Français.

### c. Quelques particularités de la réglementation de l'activité

#### Respecter la réglementation relative à la sécurité des machines à laver

L'exploitant d'une laverie en libre-service doit respecter la réglementation relative à la sécurité de ses machines, notamment en :

- vérifiant au moins une fois par semaine le bon fonctionnement de leurs dispositifs de sécurité ;
- consignant ses observations sur un registre spécial qui sera mis à disposition des clients ;
- affichant un document visible et inaltérable comportant le numéro de toute personne à joindre en cas de signalement des anomalies de fonctionnement des machines ;
- détenant soit :
  - une déclaration de conformité,
  - une déclaration CE de conformité,
  - une déclaration du fabricant attestant que la machine possède les dispositifs de sécurité en cas de défaillance ou de dysfonctionnement.

L'exploitant qui ne respecterait pas cette réglementation sera puni d'une amende de 1 500 euros, portée à 3 000 euros en cas de récidive.

*Pour aller plus loin* : décret n° 2012-412 du 23 mars 2012 relatif à la sécurité des machines à laver et essoreuses mises à disposition du public.

#### Affichage des prix

L'exploitant d'une laverie doit informer le public des prix pratiqués en utilisant la voie du marquage, de l'étiquetage, de l'affichage ou par tout autre procédé approprié.

*Pour aller plus loin* : article L. 112-1 du Code de la consommation.

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

#### Vidéosurveillance

Les laveries en libre-service peuvent être équipées de systèmes permettant la vidéosurveillance des clients. L'exploitant devra dès lors indiquer leur présence afin de ne pas porter atteinte à la vie privée de ces derniers.

L'exploitant devra également déclarer son installation auprès de la préfecture du lieu de l'établissement et remplir le formulaire Cerfa n° 13806*03, accompagné de tout document détaillant le système utilisé.

## 3°. Démarches et formalités d'installation

### a. Formalités de déclaration de l’entreprise

L’entrepreneur doit s’immatriculer au registre du commerce et des sociétés (RCS).

### b. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE)

L'exploitation d'une laverie en libre-service peut avoir un caractère dangereux ou polluant susceptible de relever de la réglementation sur les installations classées.

Les laveries de linge entrent dans la [rubrique 2340](https://aida.ineris.fr/consultation_document/10599) et précisent les [formalités](https://www.service-public.fr/professionnels-entreprises/vosdroits/F33414) que l'exploitant doit réaliser :

- un enregistrement pour les machines ayant une capacité de lavage supérieure à 5 tonnes par jour ;
- une déclaration pour les machines ayant une capacité de lavage inférieure à 5 tonnes mais supérieures à 500 kilos par jour.

Lorsque l'exploitant reprend une ICPE, il ne sera pas soumis aux formalités d'enregistrement et de déclaration. En revanche, il devra en informer le préfet, dans le mois qui suit la prise en charge de l'exploitation.

**À noter**

En cas de déclaration, la procédure est gratuite. Par contre, en cas d'enregistrement et de demande d'autorisation, le coût de l'enquête publique et/ou des formalités d'affichage sont à prévoir.