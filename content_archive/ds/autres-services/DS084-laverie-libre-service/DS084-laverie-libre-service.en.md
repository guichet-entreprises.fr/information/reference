﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS084" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Launderette" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="launderette" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/launderette.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="launderette" -->
<!-- var(translation)="Auto" -->

Launderette
====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

The operator of a self-service laundromat provides the public with washing machines, centrifugal spinners and tumble dryers without the need to be present on the premises.

In parallel to this main activity, the operator can also offer ironing or retouching services.

### b. Competent Business Formality Centre

The relevant Business Formalities Centre (CFE) depends on the nature of the structure in which the activity is carried out:

- for civil societies, this is the registry of the Commercial Court;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Professional qualifications
----------------------------------------

### a. Professional qualifications

No specific diploma is required for the operation of a self-service laundromat. However, the individual is advised to have knowledge of accounting and management.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

A national of a Member State of the European Union (EU) or party to the Agreement on the European Economic Area (EEA) is not subject to any qualification or certification requirements, as are the French.

### c. Some peculiarities of the regulation of the activity

#### Comply with washing machine safety regulations

The operator of a self-service laundromat must comply with the safety regulations of its machines, including:

- Checking their safety devices at least once a week
- recording its observations on a special register that will be made available to clients;
- Displaying a visible and unalterable document with the number of anyone to be reached in case of a report of machine malfunctions;
- holding either:- A compliance declaration,
  - A CE Compliance Statement,
  - a statement from the manufacturer that the machine has the safety features in the event of a failure or malfunction.

The operator who does not comply with these regulations will be punished with a fine of 1,500 euros, increased to 3,000 euros in case of reoffending.

*For further information*: Decree No. 2012-412 of March 23, 2012 on the safety of washing machines and spinners made available to the public.

#### Price display

The operator of a laundromat must inform the public of the prices charged using the route of marking, labelling, display or any other appropriate process.

*For further information*: Article L. 112-1 of the Consumer Code.

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, it is advisable to refer to the listing [Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

#### Video surveillance

Self-service laundromats can be equipped with systems for video surveillance of customers. The operator will therefore have to indicate their presence in order not to infringe on their privacy.

The operator will also have to declare his installation with the prefecture of the establishment and complete the form Cerfa 13806*03, accompanied by any document detailing the system used.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The contractor must register with the Trade and Corporate Register (SCN). It is advisable to refer to the "Artisanal Company Reporting Formalities" for more information.

### b. If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)

The operation of a self-service laundromat may be dangerous or polluting that may fall under the regulations on classified facilities.

Laundry laundromats enter the[2340](https://aida.ineris.fr/consultation_document/10599) and specify the [Formalities](https://www.service-public.fr/professionnels-entreprises/vosdroits/F33414) operator must perform:

- A registration for machines with a washing capacity of more than 5 tons per day;
- a declaration for machines with a washing capacity of less than 5 tons but more than 500 kilos per day.

When the operator resumes an ICPE, it will not be subject to registration and reporting procedures. On the other hand, he will have to inform the prefect, within a month of taking over the operation.

**Please note**

In the event of a declaration, the procedure is free. On the other hand, in the event of registration and application for authorization, the cost of the public inquiry and/or posting procedures are to be expected.