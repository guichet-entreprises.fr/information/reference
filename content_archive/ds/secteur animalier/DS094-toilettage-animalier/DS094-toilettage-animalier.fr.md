﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS094" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Secteur animalier" -->
<!-- var(title)="Toilettage animalier" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="secteur-animalier" -->
<!-- var(title-short)="toilettage-animalier" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/secteur-animalier/toilettage-animalier.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="toilettage-animalier" -->
<!-- var(translation)="None" -->

# Toilettage animalier

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le toilettage animalier est une activité qui consiste à réaliser des soins de nature esthétique (hygiène, entretien, remise en forme) sur des animaux d'espèces domestiques.

Cette activité s'exerce aussi bien au sein d'une clinique, d'un cabinet vétérinaire que d'un salon de toilettage.

**À noter**

Le professionnel du toilettage peut également proposer des accessoires à la vente (produits de soins, nourriture, colliers, etc.).

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité
est exercée :

- pour les activités artisanales, le CFE compétent est la chambre de métiers et de l'artisanat (CMA) ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d'industrie (CCI) ;

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exploitant a une activité de restauration rapide associée à la fabrication de produits artisanaux, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité de toilettage animalier, le professionnel doit :

- être titulaire soit :
  - d'un certificat de toiletteur canin et félin délivré par l'Assemblée permanente des chambres de métiers et de l'artisanat (APCMA),
  - d'un certificat de toiletteur canin délivré par la Fédération française des artisans du toilettage animalier (FFATA) ;
- suivant la nature de son activité, s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

#### Formation

Le professionnel qui souhaite exercer l'activité de toiletteur animalier doit obtenir un certificat en toilettage canin de niveau V (de type certificat d'aptitude professionnelle (CAP), Brevet d'études professionnelles (BEP), ou brevet des collèges).

##### Certificat de toiletteur canin et félin

Le professionnel doit valider l'ensemble des capacités des différents domaines de formation, à savoir :

- le domaine professionnel (accueil du client, toilettage, agencement du magasin etc.) ;
- et le domaine transversal (établir un devis, effectuer une vente additionnelle d'accessoires ou produits cosmétiques etc.).

Cette formation est accessible au candidat en contrat d'apprentissage, en formation continue, en contrat de professionnalisation ou par la procédure de validation des acquis de l'expérience (VAE). Pour plus d'informations, consulter le [site officiel de la VAE](http://www.vae.gouv.fr/).

##### Certificat de toiletteur canin

Le professionnel doit suivre une formation de deux ans au sein d'un établissement agréé par l'Union nationale des centres de formation en toilettage ([UNCFT](http://www.uncft.fr/les-centres-de-formation/)).

À l'issue de sa formation, le professionnel doit avoir acquis les capacités nécessaires à l'exercice de son activité, notamment :

- l'accueil du client et de son animal ;
- la mise en place de l'espace de travail et la gestion de la fiche client ;
- la maîtrise des soins adaptés à l'animal ;
- la gestion administrative, commerciale du salon de toilettage.

Cette formation est accessible au candidat en contrat d'apprentissage, après un parcours de formation continue, en contrat de professionnalisation, par candidature individuelle ou par la procédure de VAE.

*Pour aller plus loin* : Répertoire national des certifications professionnelles ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)).

### b. Qualifications professionnelles - Ressortissants européens (LPS ou LE)

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'UE ou de l'EEE en vue d'exercer l'activité de toilettage animalier à titre temporaire et occasionnel ou permanent en France.

A ce titre, le professionnel ressortissant UE est soumis aux mêmes exigences professionnelles que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation »).

### c. Conditions d’honorabilité

Nul ne peut exercer l'activité de toilettage animalier s'il fait l'objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévus au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 III de la loi n° 96-603 du 5 juillet 1996 précitée.

### d. Quelques particularités de la réglementation de l’activité

#### Réglementation relative à la qualité d’artisan et aux titres de maître artisan et de meilleur ouvrier de France

Le professionnel du toilettage animalier peut se prévaloir de la qualité d'artisan, du titre de maître artisan ou du titre de meilleur ouvrier de France (MOF).

##### La qualité d’artisan

Pour se prévaloir de la qualité d’artisan, la personne doit justifier soit :

- d’un CAP, d’un BEP ou d’un titre homologué ou enregistré lors de sa délivrance au Répertoire national des certifications professionnelles (RNCP) d’un niveau au moins équivalent (cf. supra « 2°. a. Qualifications professionnelles ») ;
- d’une expérience professionnelle dans ce métier d'au moins trois.

*Pour aller plus loin* : article 1 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

##### Le titre de maître artisan

Ce titre est attribué aux personnes physiques, y compris les dirigeants sociaux des personnes morales :

- immatriculées au répertoire des métiers ;
- titulaires du brevet de maîtrise dans le métier exercé ;
- justifiant d’au moins deux ans de pratique professionnelle.

**À noter**

Les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan à la commission régionale des qualifications dans deux hypothèses :

- lorsqu’elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de niveau de formation au moins équivalent au brevet de maîtrise, qu’elles justifient de connaissances en gestion et en psychopédagogie équivalentes à celles des unités de valeur correspondantes du brevet de maîtrise et qu’elles ont deux ans de pratique professionnelle ;
- lorsqu’elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles disposent d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

*Pour aller plus loin* : article 3 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

##### Le titre de meilleur ouvrier de France (MOF)

Le diplôme professionnel « un des meilleurs ouvriers de France » est un diplôme d’État qui atteste l’acquisition d’une haute qualification dans l’exercice d’une activité professionnelle dans le domaine artisanal, commercial, industriel ou agricole.

Le diplôme est classé au niveau III de la nomenclature interministérielle des niveaux de formation. Il est délivré à l’issue d’un examen dénommé « concours un des meilleurs ouvriers de France » au titre d’une profession dénommée « classe », rattachée à un groupe de métiers.

Pour plus d’informations, il est recommandé de consulter le [site officiel](http://www.meilleursouvriersdefrance.info/) du concours « un des meilleurs ouvriers de France ».

*Pour aller plus loin* : article D. 338-9 du Code de l’éducation.

#### Affichage des prix

Le professionnel doit informer le consommateur, par marquage ou étiquetage, des prix des prestations pratiquées au sein de son salon de toilettage.

*Pour aller plus loin* : article L. 112-1 du Code de la consommation.

#### Facturation

Le professionnel est soumis au respect des obligations en matière de facturation et doit délivrer une note au client en cas de prestation d'un montant supérieur ou égal à 25 euros. Cette note doit comporter les informations suivantes :

- sa date de rédaction ;
- le nom et l'adresse du prestataire ;
- le nom du client (si celui-ci ne s'y oppose pas) ;
- la date et le lieu d'exécution de la prestation ;
- le détail de chaque prestation ainsi que le montant total à payer (toutes taxes comprises et hors taxes).

Le professionnel doit conserver un double de cette note, les classer chronologiquement et les conserver pendant deux ans.

*Pour aller plus loin* : arrêté n° 83-50/A du 3 octobre 1983 relatif à la publicité des prix de tous les services.

#### Réglementation en matière de protection d'animaux

L'activité de toilettage animalier doit être exercée au sein d'un salon conforme aux règles sanitaires en matière de protection animale.

Pour plus d'information, consulter le [site du bulletin officiel du ministère de l'Agriculture et de l'Alimentation](https://info.agriculture.gouv.fr/gedei/site/bo-agri/supima/f179d130-4504-4889-8059-1012c4d89c25).

*Pour aller plus loin* : arrêté du 3 avril 2014 fixant les règles sanitaires et de protection animale auxquelles doivent satisfaire les activités liées aux animaux de compagnie d'espèces domestiques relevant du IV de l'article L. 214-6 du Code rural et de la pêche maritime.

#### Respect des normes de sécurité et d'accessibilité

Les locaux étant ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, le professionnel du toilettage animalier doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Le cas échéant, enregistrer les statuts de la société

Le professionnel du toilettage animalier doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.