﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS094" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sector animal" -->
<!-- var(title)="Aseo de animales" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sector-animal" -->
<!-- var(title-short)="aseo-de-animales" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/sector-animal/aseo-de-animales.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="aseo-de-animales" -->
<!-- var(translation)="Auto" -->


Aseo de animales
================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El aseo animal es una actividad que implica la realización de cuidados cosméticos (higiene, mantenimiento, fitness) en animales de especies domésticas.

Esta actividad se lleva a cabo en una clínica, una práctica veterinaria, así como un salón de aseo.

**Tenga en cuenta que**

El profesional de aseo también puede ofrecer accesorios para la venta (productos de cuidado, alimentos, collares, etc.).

### b. Centro competente de formalidades comerciales (CFE)

La CFE correspondiente depende de la naturaleza de la estructura en la que la actividad
se ejerce:

- Para las actividades artesanales, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el operador profesional tiene una actividad de comida rápida asociada a la fabricación de artesanías, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad de aseo de animales, el profesional debe:

- ser el titular:- un certificado de peroomno canino y felino expedido por la Asamblea Permanente de Cámaras de Comercio y Artesanía (APCMA),
  - un certificado de peroomno canino expedido por la Federación Francesa de Artesanos del Aseo Animal (FFATA);
- Completar un curso de preparación de la instalación (SPI);
- en función de la naturaleza de su actividad, regístrese en el Registro mercantil y de artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

#### Preparación para la instalación

Antes de registrarse, el profesional debe completar un curso de preparación de la instalación (SPI). Este último es obligatorio para cualquier persona que solicite el registro en el repertorio de oficios y artesanías.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para la creación de un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo una educación en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado de un Estado miembro de la Unión Europea (UE) u otro Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) está exento del SPI si justifica con la CMA una cualificación en la gestión le confiere un nivel de conocimiento equivalente al proporcionado por la pasantía.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que:

- han trabajado durante al menos tres años exigiendo un nivel de conocimientos equivalente al proporcionado por las prácticas;
- tener conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que pueda cubrir total o parcialmente la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con las Francia para la gestión de una empresa de artesanía.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Una copia del diploma certificado de Nivel III;
- Una copia del máster
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982; Artículo 6-1 del Decreto 83-517 de 24 de junio de 1983.

#### Entrenamiento

El profesional que desee trabajar como peroomista de animales debe obtener un certificado en el nivel de aseo de perros V (certificado tipo de aptitud profesional (CAP), Certificado de Estudios Profesionales (BEP) o certificado universitario).

**Certificado de pegado canino y felina**

El profesional debe validar todas las capacidades de las diferentes áreas de formación, a saber:

- el dominio profesional (recepción del cliente, aseo, diseño de la tienda, etc.);
- y el dominio transversal (establecer una cotización, realizar una venta adicional de accesorios o cosméticos, etc.).

Esta formación está disponible para el candidato en un contrato de aprendizaje, educación continua, contrato de profesionalización o a través del procedimiento de validación de la experiencia (VAE). Para obtener más información, consulte[Sitio web oficial de VAE](http://www.vae.gouv.fr/).

**Certificado canino groomer**

El profesional debe recibir dos años de formación en una institución acreditada por la Unión Nacional de Centros de Formación de Aseo ([UNCFT](http://www.uncft.fr/les-centres-de-formation/)).

Al final de su formación, el profesional debe haber adquirido las habilidades necesarias para llevar a cabo su actividad, incluyendo:

- Acogiendo al cliente y a su mascota
- Configuración del espacio de trabajo y gestión de la tarjeta de cliente
- Control del cuidado apropiado para los animales
- gestión administrativa y comercial del salón de aseo.

Esta formación está disponible para el candidato en un contrato de aprendizaje, después de un curso de educación continua, en un contrato de profesionalización, por solicitud individual o por el procedimiento VAE.

*Para ir más allá* Directorio nacional de certificaciones profesionales ([RNCP](http://www.rncp.cncp.gouv.fr)).

### b. Cualificaciones profesionales - Nacionales europeos (LPS o LE)

No se prevé que el nacional de un Estado miembro de la UE o del EEE lleve a cabo el aseo de animales de forma temporal, casual o permanente en Francia.

Como tal, el profesional nacional eu está sujeto a los mismos requisitos profesionales que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

### c. Condiciones de honorabilidad

Nadie puede realizar el aseo de animales si está sujeto a:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 III de la Ley 96-603, de 5 de julio de 1996.

### d. Algunas peculiaridades de la regulación de la actividad

#### Reglamento sobre la calidad del artesano y los títulos de maestro artesano y mejor trabajador en Francia

El profesional de la acicalación de animales puede reclamar la calidad del artesano, el título de maestro artesano o el título de mejor trabajador en Francia (MOF).

**Artesanía**

Para reclamar la condición de artesano, la persona debe justificar:

- una PAC, un BEP o un título certificado o registrado cuando fue emitido al Directorio Nacional de Certificaciones Profesionales (RNCP) de al menos un nivel equivalente (véase arriba: "2. a. Cualificaciones profesionales");
- experiencia profesional en este comercio de al menos tres.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de maestro artesano**

Este título se otorga a las personas, incluidos los líderes sociales de las personas jurídicas:

- Registrado en el directorio trades;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

**Tenga en cuenta que**

Las personas que no poseen el título de máster pueden solicitar a la Comisión Regional de Cualificaciones el título de Maestro Artesano bajo dos supuestos:

- cuando están inscritos en el directorio de oficios, tienen un título al menos equivalente al máster, y justifican la gestión y los conocimientos psicopedagógicos equivalentes a los de las correspondientes unidades de valor del máster y que tienen dos años de práctica profesional;
- cuando se han inscrito en el repertorio de comercio durante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de mejor trabajador de Francia (MOF)**

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en el ámbito artesanal, comercial, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación. Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

Para obtener más información, se recomienda que consulte[sitio web oficial](http://www.meilleursouvriersdefrance.info/) "uno de los mejores trabajadores de Francia."

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

#### Visualización de precios

El profesional deberá informar al consumidor, mediante marcado o etiquetado, de los precios de los servicios prestados en su salón de aseo.

*Para ir más allá* Artículo L. 112-1 del Código del Consumidor.

#### Facturación

El profesional está sujeto al cumplimiento de las obligaciones de facturación y debe emitir una nota al cliente en caso de un beneficio de un importe superior o igual a 25 euros. Esta nota debe incluir la siguiente información:

- Su fecha de escritura;
- El nombre y la dirección del proveedor
- El nombre del cliente (si el cliente no se opone a él);
- La fecha y el lugar de ejecución del servicio:
- los detalles de cada beneficio, así como el importe total a pagar (todos los impuestos incluidos y excluidos los impuestos).

El profesional debe mantener un doble de este grado, clasificarlos cronológicamente y mantenerlos durante dos años.

*Para ir más allá* ( Orden 83-50/A, de 3 de octubre de 1983, relativa a la publicidad de precios de todos los servicios.

#### Normas de protección animal

El aseo de animales debe llevarse a cabo en un salón de conformidad con las normas de protección animal.

Para obtener más información, consulte[sitio web del boletín oficial del Ministerio de Agricultura y Alimentación](https://info.agriculture.gouv.fr/gedei/site/bo-agri/supima/f179d130-4504-4889-8059-1012c4d89c25).

*Para ir más allá* : un auto de 3 de abril de 2014 por el que se establecen las normas sanitarias y de protección animal a las que deben cumplirse las actividades relacionadas con las mascotas domésticas en virtud del artículo L. 214-6 del Código de Pesca Rural y Marítima.

#### Cumplimiento de las normas de seguridad y accesibilidad

Dado que las instalaciones están abiertas al público, el profesional debe cumplir con las normas relativas a las instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de su actividad, el profesional de la acicalación animal debe inscribirse en el Registro de Comercios y Oficios (RMA) o en el Registro de Comercios y Sociedades (RCS). Es aconsejable referirse a las "Formalidades de Reporte de Empresas Artesanales", "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Individual Comercial en el RCS" para obtener más información.

### b. Si es necesario, registre los estatutos de la empresa

El profesional de aseo de animales deberá, una vez que los estatutos de la empresa hayan sido fechados y firmados, registrarlos en la oficina del impuesto de sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio cuando los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

