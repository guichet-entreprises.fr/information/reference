﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS094" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Animal sector" -->
<!-- var(title)="Animal grooming" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="animal-sector" -->
<!-- var(title-short)="animal-grooming" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/animal-sector/animal-grooming.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="animal-grooming" -->
<!-- var(translation)="Auto" -->


Animal grooming
===============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Animal grooming is an activity that involves performing cosmetic care (hygiene, maintenance, fitness) on animals of domestic species.

This activity takes place in a clinic, a veterinary practice as well as a grooming salon.

**Please note**

The grooming professional can also offer accessories for sale (care products, food, necklaces, etc.).

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity
is exercised:

- For craft activities, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional operator has a fast food activity associated with the manufacture of handicrafts, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To carry out the animal grooming activity, the professional must:

- be the holder:- a canine and feline groomer certificate issued by the Permanent Assembly of Chambers of Trades and Crafts (APCMA),
  - a canine groomer certificate issued by the French Federation of Animal Grooming Craftsmen (FFATA);
- Complete an installation preparation course (SPI);
- depending on the nature of its activity, register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

#### Preparation for installation

Before registration, the professional must complete an installation preparation course (SPI). The latter is mandatory for anyone applying for registration in the trades and crafts repertoire.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary for the creation of a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including an education in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional from a Member State of the European Union (EU) or another State party to the Agreement on the European Economic Area (EEA) is exempt from the SPI if he justifies with the CMA a qualification in management confers on him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who:

- have either worked for at least three years requiring a level of knowledge equivalent to that provided by the internship;
- either have knowledge acquired in an EU or EEA state or a third country during a professional experience that can fully or partially cover the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France for the management of a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- A copy of the Level III certified diploma;
- A copy of the master's degree
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree 83-517 of June 24, 1983.

#### Training

The professional who wishes to work as an animal groomer must obtain a certificate in dog grooming level V (type certificate of professional aptitude (CAP), Certificate of Professional Studies (BEP), or college certificate).

**Canine and feline groomer certificate**

The professional must validate all the capabilities of the different training areas, namely:

- the professional domain (customer reception, grooming, store layout, etc.);
- and the cross-cutting domain (establishing a quote, making an additional sale of accessories or cosmetics, etc.).

This training is available to the candidate in an apprenticeship contract, continuing education, professionalization contract or through the experience validation procedure (VAE). For more information, see[VAE's official website](http://www.vae.gouv.fr/).

**Canine groomer certificate**

The professional must undergo two years of training at an institution accredited by the National Union of Grooming Training Centres ([UNCFT](http://www.uncft.fr/les-centres-de-formation/)).

At the end of his training, the professional must have acquired the necessary skills to carry out his activity, including:

- Welcoming the customer and his pet
- Setting up the workspace and managing the customer card
- Control of animal-appropriate care
- administrative, commercial management of the grooming salon.

This training is available to the candidate in an apprenticeship contract, after a continuing education course, in a professionalization contract, by individual application or by the VAE procedure.

*For further information*: National directory of professional certifications ([RNCP](http://www.rncp.cncp.gouv.fr)).

### b. Professional qualifications - European nationals (LPS or LE)

There is no provision for the national of an EU or EEA Member State to carry out animal grooming on a temporary and casual or permanent basis in France.

As such, the eu national professional is subject to the same professional requirements as the French national (see infra "3°. Installation procedures and formalities").

### c. Conditions of honorability

No one may perform animal grooming if it is subject to:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 III of Act 96-603 of July 5, 1996.

### d. Some peculiarities of the regulation of the activity

#### Regulations on the quality of craftsman and the titles of master craftsman and best worker in France

The animal grooming professional can claim the quality of craftsman, the title of master craftsman or the title of best worker in France (MOF).

**Craftsmanship**

To claim the status of craftsman, the person must justify either:

- a CAP, a BEP or a certified or registered title when it was issued to the National Directory of Professional Certifications (RNCP) of at least an equivalent level (see above: "2. a. Professional qualifications");
- professional experience in this trade of at least three.

*For further information*: Article 1 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of master craftsman**

This title is given to individuals, including the social leaders of legal entities:

- Registered in the trades directory;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

**Please note**

Individuals who do not hold the master's degree can apply to the Regional Qualifications Commission for the title of Master Craftsman under two assumptions:

- when they are registered in the trades directory, have a degree at least equivalent to the master's degree, and justify management and psycho-pedagogical knowledge equivalent to those of the corresponding value units of the master's degree and that they have two years of professional practice;
- when they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of best worker in France (MOF)**

The professional diploma "one of the best workers in France" is a state diploma that attests to the acquisition of a high qualification in the exercise of a professional activity in the artisanal, commercial, industrial or agricultural field.

The diploma is classified at level III of the interdepartmental nomenclature of training levels. It is issued after an examination called "one of the best workers in France" under a profession called "class" attached to a group of trades.

For more information, it is recommended that you consult[official website](http://www.meilleursouvriersdefrance.info/) "one of the best workers in France."

*For further information*: Article D. 338-9 of the Education Code.

#### Price display

The professional must inform the consumer, by marking or labelling, of the prices of the services performed in his grooming salon.

*For further information*: Article L. 112-1 of the Consumer Code.

#### Billing

The professional is subject to compliance with the billing obligations and must issue a note to the customer in case of a benefit of an amount greater than or equal to 25 euros. This note should include the following information:

- Its date of writing;
- The provider's name and address
- The customer's name (if the customer does not object to it);
- The date and place of execution of the service:
- the details of each benefit as well as the total amount payable (all taxes included and excluding taxes).

The professional must keep a double of this grade, rank them chronologically and keep them for two years.

*For further information*: ( Order 83-50/A of 3 October 1983 relating to the price advertising of all services.

#### Animal protection regulations

Animal grooming must be carried out in a salon in accordance with animal protection rules.

For more information, see[website of the official bulletin of the Ministry of Agriculture and Food](https://info.agriculture.gouv.fr/gedei/site/bo-agri/supima/f179d130-4504-4889-8059-1012c4d89c25).

*For further information*: an order of 3 April 2014 setting out the sanitary and animal protection rules to which activities related to domestic pets under article L. 214-6 of the Rural and Maritime Fisheries Code must be met.

#### Compliance with safety and accessibility standards

As the premises are open to the public, the professional must comply with the rules relating to public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of his activity, the animal grooming professional must register with the Trades and Crafts Register (RMA) or the Register of Trade and Societies (RCS). It is advisable to refer to the "Artisanal Company Reporting Formalities," "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. If necessary, register the company's statutes

The animal grooming professional must, once the company's statutes have been dated and signed, register them with the corporate tax office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building when the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

