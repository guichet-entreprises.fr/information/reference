﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS059" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Agencia de modelado" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="agencia-de-modelado" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/agencia-de-modelado.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agencia-de-modelado" -->
<!-- var(translation)="Auto" -->

Agencia de modelado
===================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El operador de una agencia de modelos representa a modelos que prestan su aspecto para promocionar productos, servicios o mensajes publicitarios y los pone a disposición de los clientes (revistas de moda, casas de moda, publicidad, etc.).

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

La actividad de colocación de maniquíes está reservada al titular de una licencia de agencia de modelos.

No se requiere ninguna formación específica para el operador de una agencia, sin embargo, un buen conocimiento del mundo de la moda en particular, y la competencia en asuntos sociales y contables son necesarias.

*Para ir más allá* Artículo L. 7123-11 del Código de Trabajo.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Para un ejercicio temporal e informal (LPS)

Todo nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo del Espacio Económico Europeo (EEE) que esté establecido y lleve a cabo legalmente la actividad de colocación de maniquíes en ese Estado podrá ejercer en Francia, temporal y ocasional, la misma actividad.

Debe, antes de la primera prestación, presentar una solicitud de declaración a la dirección regional de empresas, competencia, consumo, trabajo y empleo (Direccte) (véase infra "3o). b. Hacer una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio único y ocasional (LPS)).

*Para ir más allá* Artículo L. 7123-11 del Código de Trabajo.

#### Para un ejercicio permanente (LE)

El Reglamento profesional francés se aplica de la misma manera a la UE o al nacional del EEE que desee establecerse en Francia de forma permanente. Por lo tanto, el nacional tendrá que obtener la licencia de una agencia de modelos del prefecto de París para abrir su agencia (véase infra "3. c. Obtener una licencia de agencia de modelos").

*Para ir más allá* Artículos L. 7123-11 y R. 7123-10-2 del Código de Trabajo.

### c. Condiciones de honorabilidad e incompatibilidad

#### Disposiciones penales

El operador de una agencia de modelos debe celebrar un contrato de trabajo con cada modelo que contrate. De lo no hacerlo, se aplicará una pena de seis meses de prisión y una multa de 75.000 euros.

Las mismas sanciones pueden aplicarse al operador que no posea la licencia como se menciona a continuación.

*Para ir más allá* Artículos L. 7123-25 a L. 7123-32 del Código de Trabajo.

#### Prevención de conflictos de intereses

La actividad de la agencia de modelado puede dar lugar a conflictos de intereses con las siguientes actividades:

- producción o producción de obras cinematográficas o audiovisuales;
- distribución o selección para la adaptación de una producción;
- la organización de cursos remunerados o cursos de formación para modelos o actores;
- Agencia de publicidad;
- organizar desfiles de moda;
- Fotografía.

Para evitar tales conflictos, los modelos empleados por la agencia, sus clientes y el Direccte territorialmente competente deben ser informados:

- Cómo se facturan los maniquíes por los servicios realizados por los modelos;
- los detalles de los mandatos sociales ejercidos por los gerentes, asociados y empleados.

*Para ir más allá* Artículos R. 7123-15 a R. 7123-17-1 del Código de Trabajo.

### d. Algunas peculiaridades de la regulación de la actividad

#### Obligación de justificar una garantía financiera

Cualquier agencia de modelos debe justificar una garantía financiera a sus usuarios. El objetivo de esta garantía es garantizar el pago de maniquíes y agencias de seguridad social.

Su importe no debe ser inferior al 6% de la factura salarial, ni a un mínimo de 15.200 euros.

El incumplimiento de esta obligación se castiga con seis meses de prisión y una multa de 75.000 euros.

*Para ir más allá* Artículos L. 7123-19, L. 7123-22, y R. 7123-20, R. 7123-22 del Código de Trabajo.

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Hacer una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio único y casual (LPS)

**Autoridad competente**

El Direccte del lugar de ejecución de la prestación es competente para emitir la declaración previa de actividad.

**Documentos de apoyo**

La solicitud de un informe previo de la actividad va acompañada de un archivo que contiene los siguientes documentos justificativos:

- Referencias al registro del organismo a un registro profesional en su país de origen;
- El nombre o el nombre y la dirección del establecimiento de la agencia de modelos;
- Nombres, nombres y direcciones de la casa de los líderes de la agencia;
- La designación de la agencia o agencias a las que el organismo de modelización paga las cotizaciones a la seguridad social;
- Prueba de obtención de una garantía financiera
- Nombre o nombre y dirección del usuario
- Ubicaciones, fechas, duración y, en su caso, las horas de entrega;
- para el empleo de menores de 16 años.

*Para ir más allá* Artículos L. 7123-11 y R. 7123-12 del Código de Trabajo.

### c. Obtener una licencia de agencia de modelos

**Autoridad competente**

El prefecto de París es competente para expedir la licencia, tras la instrucción del expediente por el Direccte d'Ile-de-France y la notificación del director regional de asuntos culturales de Ile-de-France.

**Documentos de apoyo**

La solicitud de licencia se realiza mediante el envío de un expediente dirigido al prefecto mediante carta recomendada con un aviso de recibo que contenga todos los documentos siguientes, acompañado de su traducción al francés, si es necesario:

- un extracto de kbis de la agencia acompañado de sus estatutos;
- Un currículum que muestre, entre otras cosas, la experiencia laboral del solicitante en la fecha de solicitud;
- la lista de empleados permanentes, delegados del organismo y personas con derecho a representar al organismo durante la totalidad o parte de sus actividades en la sede del organismo o en las sucursales, con la indicación, para cada uno de ellos, de los nombres, nombres, nacionalidad, fecha y lugar de nacimiento, dirección personal, experiencia profesional (curriculum vitae), así como funciones realizadas dentro de la agencia;
- Una copia del certificado de garantía financiera
- un extracto del registro penal resguardo 2 o cualquier documento equivalente del solicitante de la licencia, líderes sociales y gerentes de agencias;
- Una nota sobre las condiciones en las que operará el organismo, en particular geográficamente, e incluyendo la identificación de sucursales y sectores profesionales pertinentes;
- una declaración que indique, en su caso, las demás actividades o profesiones realizadas y los mandatos sociales de cada oficial, agente social, socio, delegado y empleado.

**Procedimiento**

En caso de falta de documentos justificativos, el prefecto notificará al solicitante en el plazo de un mes.

El silencio del prefecto en un plazo de dos meses vale la pena aceptar la solicitud de licencia.

**Qué saber**

La licencia se concede por un período renovable de tres años. Cualquier cambio de ubicación de la sede de la agencia o de sus sucursales, cambios en sus estatutos o cambio de situación deberá notificarse al prefecto mediante declaración.

*Para ir más allá* Artículos L. 7123-11, R. 7123-10-1 y R. 7123-10-2 del Código de Trabajo.

### d. Autorización posterior al registro

#### Si es necesario, obtenga una licencia para el trabajo de modelo menor de 16 años

Si una agencia de modelos desea emplear a menores de 16 años, tendrá que solicitar el Direccte y obtener la acreditación.

**Documentos de apoyo**

El archivo de acreditación debe incluir:

- un extracto de los certificados de nacimiento de los oficiales, asociados y gerentes de la agencia;
- un certificado de pago de las cotizaciones a las agencias de seguridad social para las agencias que operan en el momento de presentar la solicitud de acreditación;
- un certificado por el cual la agencia se compromete a entregar al menor un examen médico a expensas de la agencia;
- Una copia de un aviso que detalla el funcionamiento de la agencia, el chequeo médico; El procedimiento de selección de usuarios, las condiciones para poner el usuario a disposición con la duración de sus viajes, las duraciones máximas de empleo y las condiciones de remuneración;
- todas las pruebas para apreciar:- la moral, competencia y experiencia profesional de emplear a los hijos modelo de los directores, asociados y gerentes de la agencia de modelización,
  - La situación financiera de la agencia
  - Las condiciones de funcionamiento de la agencia,
  - condiciones en las que trabajará con niños.

**hora**

La acreditación se otorga por un período renovable de un año, después de la notificación de un Comité Asesor. El silencio guardado dentro de los dos meses de recibir el expediente vale la pena aceptar la solicitud de aprobación.

La agencia de modelos tendrá que registrar, entre otras informaciones, la identidad de los menores empleados, las horas de trabajo, la disponibilidad del cliente para cada menor, en un registro especial.

*Para ir más allá* Artículos L. 7124-4, L. 7124-5 y R. 7124-8 a R. 7124-14 del Código de Trabajo.