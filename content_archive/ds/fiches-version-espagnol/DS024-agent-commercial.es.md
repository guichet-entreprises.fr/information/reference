﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS024" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Agente de ventas" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="agente-de-ventas" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/agente-de-ventas.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agente-de-ventas" -->
<!-- var(translation)="Auto" -->


Agente de ventas
================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El agente de ventas es un profesional obligatorio e independiente cuyo negocio consiste en negociar y celebrar contratos en nombre de otros (empresas, comerciantes, otros agentes comerciales, etc.).

Estos contratos pueden incluir:

- Prestación de servicios
- Ventas
- compras o alquileres.

*Para ir más allá* Artículo L. 134-1 del Código de Comercio.

### b. Centro competente de formalidades comerciales (CFE)

La CFE correspondiente depende de la naturaleza de la actividad.

Cualquiera que sea la naturaleza jurídica de la actividad, la CFE competente es el registro del tribunal comercial en el que el profesional está domiciliado o el registro del alto tribunal para los departamentos del Bajo Rin, del Alto Rin y del Mosela.

Sin embargo, si el profesional se dedica a la actividad comercial, la CFE correspondiente será la Cámara de Comercio e Industria (CCI).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El profesional que desee trabajar como agente de ventas no está sujeto a ningún requisito de diploma o título de formación.

Sin embargo, antes de ejercer, debe solicitar el registro en el Registro Especial de Agentes Comerciales (RSAC) (véase infra "3 grados). b. Inscripción en el Registro Especial de Agentes Comerciales").

*Para ir más allá* Artículo L. 134-6 del Código de Comercio.

### b. Cualificaciones profesionales - Nacionales Europeos (Servicio Gratuito o Establecimiento Libre)

**Para una entrega de servicio gratuita (LPS)**

Un nacional de un miembro de la Unión Europea (UE) o una parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido que actúe como agente comercial puede realizar la misma actividad temporal y ocasionalmente en Francia.

El nacional está sujeto a los mismos requisitos que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación"). Sin embargo, el interesado está exento del requisito de registro en la RSAC.

*Para ir más allá* párrafo 8 del artículo R. 134-6 del Código de Comercio.

**A la vista de un establecimiento gratuito (LE)**

No existe ninguna normativa para que el nacional de la UE lleve a cabo la actividad de un agente de ventas permanente en Francia.

Como tal, el nacional está sujeto a los mismos requisitos que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

### c. Condiciones de honor y sanciones penales

**Condiciones de honorabilidad**

El profesional que desee trabajar como agente de ventas no debe haber estado sujeto a:

- ni una condena penal o prohibición de dirigir, administrar o administrar un negocio comercial o artesanal o una persona jurídica;
- o una sanción suplementaria por prohibir el ejercicio de cualquier actividad profesional.

*Para ir más allá* Artículo 131-6 del Código Penal.

**Obligaciones**

El profesional está sujeto a un deber de lealtad y un deber de información al director.

Como tal, debe proporcionarle toda la información necesaria para llevar a cabo el contrato. Además, el principal está vinculado por las mismas obligaciones y debe proporcionar al agente de ventas toda la documentación relativa a los elementos del contrato.

*Para ir más allá* Artículos L. 134-4, R. 134-1 y los siguientes del Código de Comercio.

**Sanciones penales**

El profesional se enfrenta a una multa de hasta 3.000 euros si trabaja como agente de ventas sin tener:

- registrado en la RSAC;
- informar a la autoridad de cualquier cambio en la información sobre la declaración de registro;
- solicitado que se le retire si ya no ejerce su actividad o cumple las condiciones necesarias para el ejercicio de la profesión.

También se enfrenta a una multa si la información proporcionada para su registro es inexacta o incompleta.

*Para ir más allá* Artículos R. 134-14 y siguientes del Código de Comercio.

### d. Algunas peculiaridades de la regulación de la actividad

**Estado del agente de ventas**

El agente de ventas opera de forma independiente y puede aceptar representar a otros componentes. Sin embargo, está sujeto a una obligación de no competencia y debe obtener la autorización del principal antes de representar a una empresa competidora.

*Para ir más allá* Artículo L. 134-3 del Código de Comercio.

**Contrato de agente comercial**

El contrato entre el agente de ventas y el principal puede ser de duración determinada o indeterminado. Si el contrato continúa ejecutándose después del final de su mandato, el contrato de duración determinada se considerará un contrato indeterminado.

Además, cualquier contrato indeterminado podrá ser rescindido por las partes si cumplen con la notificación:

- un mes para el primer año del contrato
- dos meses para el segundo año del contrato;
- tres meses a partir del tercer año del contrato.

**Tenga en cuenta que**

Las Partes pueden proporcionar períodos de aviso más largos solo si el plazo para el principal no es más corto que el del agente de ventas.

Además, el principal debe proporcionar al agente de ventas una declaración que mencione todas las comisiones adeudadas y la base para calcular su importe.

*Para ir más allá* Artículos L. 134-11 y R. 134-3 del Código de Comercio.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Registro de Comercio y Sociedades (RCS)

El profesional que desee constituir una sociedad mercantil deberá registrarse en el RCS, cuyos trámites dependen de la CFE pertinente:

- cuando la CFE pertinente sea la Cámara de Comercio e Industria, una vez completado el registro en el RCS, el profesional deberá solicitar el registro en el Registro Especial de Agentes Comerciales (véase infra "3o. b. Inscripción en el Registro Especial de Agentes Comerciales (RSAC);
- cuando la CFE pertinente sea el registro del Tribunal de Comercio, el tribunal mercantil se registrará ante la RSAC y el RCS.

Para obtener más información, consulte la hoja "Formalidades de informes de empresas".

### b. Inscripción en el Registro Especial de Agentes Comerciales (RSAC)

**Autoridad competente**

Para el registro en la RSAC, el oficial de ventas debe presentar una declaración duplicada ante el Secretario del Tribunal Comercial.

**Documentos de apoyo**

La declaración debe incluir:

- Una copia del contrato firmado con el principal mencionando el contenido del contrato de agencia o, en su defecto, prueba de la existencia de dicho contrato traducido al francés;
- una prueba válida de identidad de la persona física o, en su caso, una prueba de identificación, así como un extracto del RCS de la corporación registrada;
- un boletín informativo o una solicitud de membresía en un fondo de seguro de vejez no salarial y un boletín informativo o solicitud de afiliación con un fondo de subsidio familiar;
- si el solicitante es una persona física:- una declaración de inseibilidad (véase el artículo L. 526-1 del Código de Comercio),
  - cualquier documento que justifique que el cónyuge es informado de las consecuencias de cualquier deuda sobre su propiedad común,
  - si es necesario, la identidad del cónyuge siempre y cuando sea un empleado, socio o empleado.

**Resultado del procedimiento**

Al presentar la solicitud, el Secretario informa al juez a cargo del RCS para solicitar el boletín 2 del antecedente penal. Al recibir esta boleta, se le asignará al solicitante un número de registro y una copia de su declaración.

**Tenga en cuenta que**

Esto se puede hacer electrónicamente.

*Para ir más allá* Artículos A. 134-1 y siguientes del Código de Comercio; Apéndice 1.1 del Código de Comercio.

### c. Si es necesario, registre los estatutos de la empresa

Una vez fechados y firmados los estatutos de la sociedad, el profesional deberá registrarlos en la Oficina del Impuesto sobre Sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

