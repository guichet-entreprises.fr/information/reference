﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS073" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Demolición" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="demolicion" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/demolicion.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="demolicion" -->
<!-- var(translation)="Auto" -->

Demolición
==========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La demolición es una actividad especializada en la deconstrucción y destrucción de edificios.

Se utilizan varias técnicas, como la voladura, la bola de demolición, la lanza térmica y la fuerza humana a través de la masa, entre otras.

A menudo se prefiere la deconstrucción a la destrucción porque permite desmontar los elementos de construcción del edificio, reciclar materiales más fácilmente y se pueden reducir las molestias (polvo, ruido, desorden, etc.).

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Sólo una persona cualificada que esté cualificada profesionalmente o puesta bajo el control efectivo y permanente de una persona cualificada pueda llevar a cabo la actividad de demolición.

La persona que practique o controle el ejercicio debe ser el titular del ejercicio:

- un Certificado de Cualificación Profesional (CAP) o un Grado profesional en los sectores de la construcción, obras públicas o ambientales;
- Una Patente de Estudios Profesionales (BEP);
- un diploma o un grado de igual o nivel superior aprobado o registrado cuando fue emitido al Directorio Nacional de Certificaciones Profesionales (RNCP).

En ausencia de uno de estos diplomas, el interesado deberá justificar una experiencia profesional efectiva de tres años en un Estado de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE) adquirido como líder autónomos o empleados en demolición. En este caso, el interesado podrá solicitar a la CMA pertinente un certificado de cualificación profesional.

En el caso del uso de artefactos explosivos, el responsable de la demolición (también conocido como extintor de incendios) tendrá que justificar:

- Tener un certificado de asistente de disparo
- haber adquirido suficiente práctica en la implementación de explosivos.

*Para ir más allá* Decreto 87-231, de 27 de marzo de 1987, relativo a los requisitos especiales de protección relativos a la utilización de explosivos en la construcción, las obras públicas y las obras agrícolas; orden de 10 de julio de 1987 relativa a las condiciones de expedición de la licencia de armas de fuego prevista por el Decreto 87-231, de 27 de marzo de 1987, relativo a los requisitos especiales de protección relativos al uso de explosivos en la construcción, obras públicas y el trabajo agrícola; Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

**Para un ejercicio temporal e informal (LPS)**

Cualquier nacional de un Estado miembro de la UE o del EEE establecido y que realice legalmente actividades de demolición en ese Estado podrá llevar a cabo la misma actividad en Francia de forma temporal y ocasional.

En primer lugar, deberá presentar la solicitud mediante declaración a la CMA del lugar en el que desea llevar a cabo el servicio.

En el caso de que la profesión no esté regulada, ni en el curso de la actividad ni en el marco de la formación, en el país en el que el profesional esté legalmente establecido, deberá haber realizado esta actividad durante al menos un año, en el transcurso de los diez años antes de la prestación, en uno o varios Estados miembros de la UE.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la CMA competente podrá exigir que el interesado se someta a una prueba de aptitud.

*Para ir más allá* Artículo 17-1 de la Ley de 5 de julio de 1996; Artículo 2 del Decreto de 2 de abril de 1998 modificado por el decreto de 4 de mayo de 2017.

**Para un ejercicio permanente**

Para llevar a cabo la actividad de demolición en Francia de forma permanente, la UE o el nacional del EEE deben cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que un francés;
- poseer un certificado de competencia o documento de formación necesario para el ejercicio de la actividad de demolición en un Estado de la UE o del EEE cuando dicho Estado regule el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su disposición a llevar a cabo la actividad de demolición cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de Esta actividad
- obtener un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona haya llevado a cabo la actividad de demolición en el estado que haya admitido Equivalencia.

Si se cumplen las cualificaciones de eu, un nacional de un Estado de la UE o del EEE podrá solicitar un certificado de reconocimiento de la cualificación profesional.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la CMA competente podrá exigir que el interesado se someta a medidas de indemnización.

*Para ir más allá* Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 a 3-2 del Decreto de 2 de abril de 1998 modificado por el decreto de 4 de mayo de 2017.

### c. Condiciones de honorabilidad e incompatibilidad

Nadie podrá llevar a cabo la actividad de demolición si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 III de la Ley 96-603, de 5 de julio de 1996.

### d. Obligación de constete de un seguro de responsabilidad civil profesional

El profesional que desee llevar a cabo una actividad de demolición debe realizar un seguro de responsabilidad civil profesional. Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

**Tenga en cuenta que**

El uso de vehículos motorizados terrestres (equipos de construcción o camiones) está sujeto a la obligación de sacar un seguro de responsabilidad civil para automóviles.

*Para ir más allá* Artículo L. 211-1 del Código de Seguros.

### e. Algunas peculiaridades de la regulación de la actividad

#### Reglamento sobre la calidad del artesano y maestro artesano

**Artesanía**

Para reclamar la condición de artesano, la persona debe justificar:

- una PAC, una BEP o una designación certificada o registrada cuando se emitió al RNCP al menos un nivel equivalente (véase supra "2." a. Cualificaciones profesionales");
- experiencia profesional en esta ocupación durante al menos tres años.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de maestro artesano**

Este título se otorga a las personas, incluidos los líderes sociales de las personas jurídicas:

- Registrado en el directorio trades;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

**Tenga en cuenta que**

Las personas que no poseen el título de máster pueden solicitar a la Comisión Regional de Cualificaciones el título de Maestro Artesano bajo dos supuestos:

- cuando están inscritos en el directorio de oficios, tienen un título al menos equivalente al máster, y justifican la gestión y los conocimientos psicopedagógicos equivalentes a los de las correspondientes unidades de valor del máster y que tienen dos años de práctica profesional;
- cuando se han inscrito en el repertorio de comercio durante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

#### Permiso para demoler

Cualquier operación para demoler un edificio está sujeta a una solicitud de permiso de construcción.

La solicitud es un fichero dirigido al ayuntamiento del municipio en el que se encuentra el edificio, en cuatro ejemplares.

*Para ir más allá* Artículos R. 423-1 y R. 423-2 del Código de Planificación.

#### Tarjeta de identificación profesional

Los empleados de edificios y obras públicas deben tener una tarjeta de identificación profesional para presentar a los oficiales de detección.

La solicitud de tarjeta es hecha por el empleador o por las empresas de trabajo temporal.

Es válido durante la duración del contrato de trabajo y por un período de cinco años para los trabajadores temporales.

**Costo**

La demanda de una tarjeta, que es[en línea](https://www.cartebtp.fr/), se acompaña del pago de una tasa fijada en 10,80 euros.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable referirse a las "Formalidades de Reporte de Empresas Artesanales", "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Individual Comercial en el RCS" para obtener más información.

### b. Siga el curso de preparación de la instalación (SPI)

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que son:

- han ejercido una actividad profesional que requiere un nivel de conocimientos equivalente al proporcionado por las prácticas durante al menos tres años;
- adquirir conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que pueda cubrir total o parcialmente la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con en Francia para dirigir una empresa de artesanías.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982; Artículo 6-1 del Decreto No 83-517 de 24 de junio de 1983.

### c. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

La CMA del lugar en el que el nacional desea llevar a cabo el beneficio es competente para emitir la declaración previa de actividad.

**Documentos de apoyo**

La solicitud de un informe previo de la actividad va acompañada de un archivo completo que contiene los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE;
- un documento que justifique la cualificación profesional del nacional que puede ser, a su elección:- Una copia de un diploma, título o certificado,
  - Un certificado de competencia,
  - cualquier documentación que acredite la experiencia profesional del nacional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tenga en cuenta que**

Cuando el expediente está incompleto, la CMA tiene un plazo de quince días para informar al nacional y solicitar todos los documentos que faltan.

**Resultado del procedimiento**

Al recibir todos los documentos en el archivo, el CMA tiene un mes para decidir:

- autorizar la prestación cuando el nacional justifique tres años de experiencia laboral en un Estado de la UE o del EEE, y adjuntar a dicha Decisión un certificado de cualificación profesional;
- o autorizar la disposición cuando las cualificaciones profesionales del nacional se consideren suficientes;
- o imponerle una prueba de aptitud cuando existan diferencias sustanciales entre las cualificaciones profesionales del nacional y las exigidas en Francia. En caso de denegación de esta medida de compensación o en caso de incumplimiento en su ejecución, el nacional no podrá llevar a cabo la prestación de servicios en Francia.

El silencio guardado por la autoridad competente en este plazo merece autorización para iniciar la prestación del servicio.

*Para ir más allá* Artículo 2 del Decreto de 2 de abril de 1998; Artículo 2 del Decreto de 17 de octubre de 2017 relativo a la presentación de la declaración y solicitudes previstas en el Decreto 98-246, de 2 de abril de 1998, y en el título I del Decreto 98-247, de 2 de abril de 1998.

### d. Si es necesario, solicite un certificado de reconocimiento de la cualificación profesional

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la ACM territorialmente competente.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA pide al interesado que lo complete dentro de una quincena de la presentación del expediente. Se emite un recibo tan pronto como se completa el archivo.

**Documentos de apoyo**

La carpeta debe contener las siguientes partes:

- Solicitar un certificado de cualificación profesional
- Un certificado de competencia o título de diploma o designación de formación profesional;
- Prueba de la nacionalidad del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (CIEP) para obtener información adicional sobre el nivel de formación de un diploma, certificado o designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**Qué saber**

Si es necesario, todos los documentos justificativos deben traducirse al francés.

**Tiempo de respuesta**

Dentro de los tres meses siguientes a la recepción, la CMA podrá:

- Reconocer la cualificación profesional y emitir la certificación de cualificación profesional;
- decidir someter al solicitante a una medida de compensación y notificarle dicha decisión;
- negarse a expedir el certificado de cualificación profesional.

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Remedios**

Si la CMA se niega a dictar el reconocimiento de la cualificación profesional, el solicitante podrá iniciar, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, una impugnación legal ante el tribunal administrativo competente. Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de indemnización, primero debe iniciar un recurso agraciado ante el prefecto del departamento en el que se basa la CMA, en el plazo de dos meses a partir de la notificación de la decisión. Cma. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### e. Autorización posterior al registro

#### Obtención de autorización para la adquisición de artefactos explosivos

Cualquier profesional de la demolición que utilice explosivos deberá obtener una autorización en forma de:

- un certificado de adquisición renovable de un año solicitado al prefecto del departamento, siempre y cuando el interesado desee adquirir más de 500 detonadores;
- una orden de tres meses solicitada a la unidad de gendarmería o al departamento de policía del lugar de uso de explosivos, para la adquisición de 500 productos explosivos de hasta 25 kg.

*Para ir más allá* Artículo R. 2352-74 del Código de Defensa; orden de 3 de marzo de 1982 relativo a la adquisición de productos explosivos.

#### Formalidades relacionadas con el almacenamiento y transporte de artefactos explosivos

Muchas formalidades recaen en el profesional que utiliza artefactos explosivos.

En particular, tendrá que obtener:

- **aprobación técnica para el almacenamiento.** La solicitud está dirigida al prefecto del lugar de instalación, que solicitará un dictamen de la inspección de armas para los polvos y explosivos en el Direccte, así como de los servicios de policía y gendarmería. Debe incluir:- un estudio de seguridad con una lista de las medidas de seguridad recomendadas,
  - Un aviso de cumplimiento de instalaciones

**Qué saber**

Vale la pena rechazar el silencio de la administración durante dos meses.

*Para ir más allá* Artículos R. 2352-97 a R. 2352-102 del Código de Defensa.

- **un permiso de funcionamiento individual para el almacenamiento.** La autorización se expide al propio operador que la solicita al prefecto del departamento del lugar de instalación.

La solicitud se presenta en forma de un archivo que incluye:

- Si se trata de una persona física, una copia de un documento de identificación oficial completo y válido, y la indicación de la ocupación, residencia y nacionalidad del solicitante, así como un envío del boletín número dos de antecedentes penales;
- Si se trata de un extranjero, un documento judicial equivalente, traducido al francés por un traductor certificado;
- Si se trata de una sociedad, un extracto de los estatutos que incluye la indicación de la forma de la empresa y la finalidad social, así como la dirección de la sede central y la misma información que se ha relacionado anteriormente con los agentes de la empresa que dirección para el funcionamiento del depósito, débito o instalación móvil de productos explosivos;
- la justificación de la inclusión de la empresa en el registro mercantil y su número de identificación asignado por el Instituto Nacional de Estadística y Estudios Económicos;

**Qué saber**

Vale la pena rechazar el silencio de la administración durante dos meses.

*Para ir más allá* Artículos R. 2352-110 a R. 2352-117 del Código de Defensa; orden de 12 de marzo de 1993 para la aplicación de los artículos R. 2352-110 a R.2352-121 del Código de Defensa.

- **autorización para el transporte de explosivos.** Cualquier profesional que lleve explosivos debe aplicarlo al prefecto. La autorización tiene una validez de cinco años renovables.

#### En caso necesario, proceder a las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)

Siempre que sea necesario el almacenamiento de productos explosivos, se aplicarán las normas sobre ICPE.

Dependiendo de la cantidad de explosivos, los trámites pueden ser:

- autorización de entre 500 kg y 10 toneladas;
- un registro de entre 100 kg y 500 kg;
- declaración previa con control periódico de entre 30 kg y 100 kg.

*Para ir más allá* Artículos R. 51-1, R. 512-46-1 a R. 512-46-7, y R. 512-46-19 a R. 512-46-23 del Código de Medio Ambiente.

