﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS075" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Disco" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="disco" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/disco.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="disco" -->
<!-- var(translation)="Auto" -->


Disco
=====

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

Un club nocturno, más comúnmente conocido como un club nocturno, es un establecimiento abierto a un gran público, con una pista de baile y un taxi que alberga un disc jockey.

El operador de un club nocturno debe incluir:

- Equipos de coordinación
- Controlar el mantenimiento de equipos e instalaciones
- Llevar a cabo la gestión contable y administrativa de la estructura
- para hacer frente a la comunicación de los próximos eventos en su institución.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere un diploma específico para la profesión de operador de discoteca. Sin embargo, se aconseja al individuo tener un conocimiento sólido del funcionamiento de las estructuras de ocio, contabilidad y comunicación.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o una parte en el Espacio Económico Europeo (EEE) no está sujeto a ningún requisito de calificación o certificación, al igual que los franceses.

### c. Honorabilidad e incompatibilidad

El profesional, que desea abrir una discoteca, está obligado a respetar una serie de reglas, incluyendo:

- prohibición de la venta de alcohol a personas menores de 18 años. También está prohibido recibir menores de 16 años si no están acompañados por un padre, su tutor legal o una persona mayor de 18 años con su cuidado o supervisión;
- el[prohibición de fumar](http://solidarites-sante.gouv.fr/prevention-en-sante/addictions/article/l-interdiction-de-fumer-dans-les-lieux-publics) en áreas cerradas y cubiertas.

*Para ir más allá* Artículo L. 3342-3 del Código de Salud Pública.

### d. Algunas peculiaridades de la regulación de la actividad

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Dado que las instalaciones están abiertas al público, el profesional debe cumplir con las normas relativas a las instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

#### Respeto a los horarios de funcionamiento

El operador de una discoteca debe cerrar las instalaciones a las 7 a.m.

**Tenga en cuenta que**

El municipio podrá conceder exenciones.

*Para ir más allá* Artículo D. 314-1 del Código de Turismo.

#### Disponibilidad de etilitas

El operador debe poner a disposición del público los respiradores que se han comprobado periódicamente. Deben estar marcados con carteles y acompañados de una nota explicativa.

*Para ir más allá* Artículo L. 3341-4 del Código de Salud Pública.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Requisito para poseer una licencia para el flujo de bebidas

El operador de un club nocturno que vende bebidas alcohólicas tiene licencia.

**Qué saber**

La licencia sólo es posible si el operador:

- es un menor emancipado (o menor);
- no está bajo tutela;
- no fue condenado por delitos penales, proxenetismo, robo, fraude o violación de la confianza.

Antes de obtener la licencia, el operador debe[permisos de explotación](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14407.do) después de recibir formación específica sobre la prevención y el control del alcoholismo, la protección de los menores, la legislación sobre drogas y el control del ruido.

Paralelamente a la solicitud del operador de una licencia de explotación, el[informes previos](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_11542.do) en el ayuntamiento del lugar de instalación, que más tarde emitirá un recibo justificando la posesión de la licencia.

### c. Autorización posterior a la instalación

#### Obligación de celebrar un contrato de interpretación con la Sociedad de Compositores y Editores de Música (SACEM)

Tan pronto como una discoteca emite música, su líder está obligado a solicitar permiso de SACEM, mediante la firma de un contrato de actuación.

Antes de su conclusión, una[Declaración](https://clients.sacem.fr/docs/autorisations/Diffusions_musicales_attractives.pdf), completado, fechado y firmado, debe ser remitido a SACEM que lo reconocerá mediante la devolución del contrato de representación que se firmará.

Una vez que SACEM haya concedido permiso para transmitir, el operador de la discoteca tendrá que pagarle un derecho de autor basado en el volumen de negocios que realice.

*Para ir más allá* Artículos L. 132-18 a L. 132-20-2 del Código de Propiedad Intelectual.

#### Obligación de informar a la Corporación de su actividad para la recaudación de una compensación equitativa por la comunicación de fonogramas comerciales (SPRE) al público

A diferencia de SACEM, que paga a autores, compositores y editores de música, el SPRE paga los derechos de[Compensación justa](https://www.spre.fr/index.php?page_id=47)) a los artistas intérpretes o ejecutantes y a los productores de fonogramas.

El importe de la compensación equitativa tendrá en cuenta el volumen de negocios alcanzado.

El operador deberá informar anualmente de su actividad al SPRE y enviar los siguientes documentos justificativos:

- el[desliz de declaración](https://www.spre.fr/document/bordereau_declaration_annuelle.pdf) completado, fechado y firmado;
- Una copia certificada de la cuenta de resultados o de la lista de cuentas de clase 7
- para microempresas, una copia certificada del extracto del Formulario 2042, bajo BIC.

**Tenga en cuenta que**

El operador que no acompañe su declaración de la prueba requerida activaría automáticamente una factura de 580 euros al mes como mínimo.

*Para ir más allá* Artículos L. 214-1 y siguientes del Código de Propiedad Intelectual.

#### Obligación de realizar un estudio del impacto de la contaminación acústica

El operador de una discoteca debe establecer un estudio del impacto de las perturbaciones acústicas que podrían afectar a la tranquilidad o la salud del barrio.

Debe incluir:

- diagnóstico acústico para estimar los niveles de presión acústica dentro y fuera de las instalaciones;
- una descripción de las disposiciones adoptadas para limitar el nivel de ruido (trabajo de aislamiento acústico, instalación de un limitador de presión acústica, etc.).

En caso de perturbaciones significativas relacionadas con la radiodifusión de música, podrán aplicarse sanciones penales (multa de 5a clase) al operador.

**Tenga en cuenta que**

Este estudio debe actualizarse con cada cambio en el diseño o la actividad del lugar.

*Para ir más allá* Artículos R. 571-25 a R. 571-28 del Código de Medio Ambiente.

