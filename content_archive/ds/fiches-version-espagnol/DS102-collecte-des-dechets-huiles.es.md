﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS102" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Recogida de residuos: aceites" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="recogida-de-residuos-aceites" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/recogida-de-residuos-aceites.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="recogida-de-residuos-aceites" -->
<!-- var(translation)="Auto" -->

Recogida de residuos: aceites
=============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad de recogida de aceites consiste, para el profesional (también llamado colector), para asegurar la recogida y el transporte, hasta el punto de tratamiento, de aceites que se han vuelto inadecuados para el uso para el que fueron destinados.

Los aceites usados se consideran:

- aceites de los motores de combustión y los sistemas de transmisión;
- aceites lubricantes
- aceites de turbina;
- aceites para sistemas hidráulicos.

*Para ir más allá* Artículo R. 543-3 del Código de Medio Ambiente.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Dado que la actividad es de carácter comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional realiza una actividad de comprayé su actividad será tanto comercial como artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad de recogida de aceites usados, el profesional debe poseer una acreditación válida por hasta cinco años dentro de una zona geográfica específica (un departamento).

Para ello, debe solicitar esta aprobación (véase infra "3o. a. Solicitud de aprobación").

*Para ir más allá* Artículos R. 543-6 y siguientes del Código de Medio Ambiente.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

**En caso de ejercicio temporal y ocasional (LPS)**

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) lleve a cabo la actividad de recogida de petróleo para un ejercicio temporal y ocasional en Francia. Como tal, el nacional está sujeto a los mismos requisitos que el nacional francés (véase infra "Procedimientos y formalidades de instalación").

**En caso de ejercicio permanente (LE)**

Todo nacional de la UE o del EEE legalmente establecido en un Estado miembro y que se dedique a la recogida de aceites usados podrá llevar a cabo la misma actividad en Francia de forma permanente.

Para ello debe:

- Ser titular de una autorización expedida por dicho Estado miembro con garantías equivalentes a las exigidas para ejercer en Francia;
- para hacer una declaración previa al prefecto del departamento en el que desea ejercer. Es aconsejable acercarse a la autoridad pertinente para obtener más información.

*Para ir más allá* Artículo R. 543-6 del Código de Medio Ambiente.

### c. Obligaciones profesionales

El profesional, durante cualquier colección, está obligado a respetar las obligaciones reunidas dentro de una especificación. Como tal, debe:

- recolección dentro del departamento para el que ha recibido la acreditación;
- mantener un registro de las tasas de recuperación de residuos. Estos aranceles deben tener en cuenta las diferentes cualidades del petróleo;
- recoger una gran cantidad de aceites usados de más de 600 litros en una quincena. No obstante, este plazo podrá prorrogarse por decisión del prefecto del departamento;
- emitir un vale de recogida al titular de los lotes de aceites recogidos indicando:- La cantidad y calidad de los aceites,
  - El precio de recuperación;
- doble muestreo de aceites antes de mezclar con otro lote para detectar la presencia de policlorobifenilos (PCB), que se clasifican como dañinos para el medio ambiente. El profesional está obligado a conservar una de estas muestras hasta que se traten los residuos y a entregar la segunda al titular que tendrá que firmar el comprobante de extracción e indicar que se le ha entregado una muestra;
- capacidad mínima de almacenamiento y conforme al Reglamento de Instalaciones Clasificadas para el Medio Ambiente (ICPE), a saber:- al menos 1/12 del tonelaje recaudado anualmente,
  - cincuenta metros cúbicos que permiten la separación entre los aceites y otros residuos recogidos, así como entre las diferentes cualidades de los aceites recogidos;
- Entregar aceites recogidos a eliminadores aprobados;
- se refieren mensualmente a la Agencia de Gestión del Medio Ambiente y la Energía ([Ademe](http://www.ademe.fr/)) toda la información sobre su actividad:- Los tonelajes recogidos y los titulares involucrados,
  - precios de recuperación,
  - tonelajes entregados a removedores o compradores de aceites claros para su reutilización.

**Tenga en cuenta que**

La reanudación de los aceites "motores" (que no contienen más del 5 % de agua) se lleva a cabo de forma gratuita por los recolectores de los departamentos y comunidades de ultramar, siempre que se beneficien de un régimen de ayudas.

*Para ir más allá* Artículo R. 543-11 del Código de Medio Ambiente; Apéndice II de la orden del 28 de enero de 1999 sobre las condiciones de recogida de los aceites usados.

### d. Algunas peculiaridades de la regulación de la actividad

**Garantías financieras**

Mientras su actividad esté comprendida en las instalaciones clasificadas de protección del medio ambiente sujetas a autorización (véase la infra "3." b. Si es necesario, para proceder a las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)"), el profesional está obligado a obtener garantías financieras.

Estas garantías financieras pueden dar lugar a:

- un compromiso escrito de una institución de crédito, seguro, financiamiento o fianza mutua;
- Un fondo de garantía gestionado por Ademe;
- un depósito en manos de la Caisse des dépéts et consignments;
- un compromiso escrito con una garantía independiente de una persona que posea más de la mitad del capital del operador.

Estas garantías financieras deben garantizarse durante un mínimo de dos años y deben renovarse al menos tres meses antes de su vencimiento.

Estas garantías financieras están destinadas a cubrir los riesgos asociados con los gastos para:

- La seguridad del lugar de funcionamiento del profesional;
- Intervenciones en caso de accidente o contaminación;
- reacondicionado después de su cierre.

*Para ir más allá* Artículos R. 516-1 y siguientes del Código de Medio Ambiente.

**Jerarquía de métodos de tratamiento de residuos**

La recogida y el tratamiento de los aceites usados deben llevarse a cabo de conformidad con la política nacional de prevención y gestión de residuos.

Como tal, el tratamiento de los aceites usados debe centrarse en:

- Preparación para la reutilización
- Reciclaje
- Recuperación de energía
- Eliminación.

*Para ir más allá* Artículos L. 541-1 y siguientes del Código de Medio Ambiente.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitud de aprobación

**Autoridad competente**

El profesional debe enviar su solicitud en tres copias al prefecto del departamento.

**Documentos de apoyo**

Su solicitud debe incluir:

- su compromiso de cumplir con todas las obligaciones de las especificaciones;
- una hoja informativa de la empresa que establece:- su estructura jurídica y financiera,
  - sus actividades anteriores,
  - Tonnages recogidos y entregados a eliminadores aprobados,
  - el área o áreas de recogida
  - volumen de negocios en los últimos dos años,
  - Otras actividades de residuos;
- una hoja informativa sobre los medios utilizados para recoger y almacenar aceites usados, incluida la información sobre:- personal de recogida,
  - vehículos de recogida,
  - instalaciones de almacenamiento,
  - archivo de su clientela (existente o prevista), así como los medios de prospección;
- una hoja cuantitativa y económica de previsiones operativas para los próximos cinco años.

**Retrasos y procedimientos**

El prefecto revisa la solicitud del profesional y emite la acreditación previa consulta con la Agencia de Medio Ambiente y Gestión Energética. El decreto de emisión se publica en la recogida de actos administrativos dentro de la prefectura y debe mencionarse en al menos dos periódicos de la prensa local o regional (cuyos gastos de publicación son responsabilidad del profesional).

*Para ir más allá* Título I del anexo de la orden del 28 de enero de 1999 sobre las condiciones de recogida de los aceites usados.

### b. Si es necesario, proceda con las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)

La recogida de aceites usados puede estar sujeta a reglamentos relativos a instalaciones clasificadas para la protección del medio ambiente en función del volumen de residuos recogidos.

La actividad del profesional está incluida en el artículo 2718: instalaciones de tránsito, recogida o clasificación de residuos peligrosos o residuos que contengan sustancias o preparados peligrosos a que se refiere el artículo R. 511-10 del Código del Código de excluyendo las instalaciones menores de 1313, 2710, 2711, 2712, 2717 y 2719.

Dependiendo del volumen de residuos recogidos, el profesional deberá realizar:

- una solicitud de permiso, en caso de volumen de cobro mayor o igual que una tonelada;
- control periódico si este volumen es inferior a una tonelada.

Es aconsejable consultar la nomenclatura del ICPE disponible en el sitio web de la[Aida](https://aida.ineris.fr/) para obtener más información.

### c. Formalidades de notificación de la empresa

En el caso de la creación de una empresa mercantil, el profesional está obligado a inscribirse en el Registro de Comercio y Sociedades (RCS).

**Autoridad competente**

El profesional debe hacer una declaración a la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

Para ello, debe proporcionar[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI le envió un recibo el mismo día indicando los documentos que faltaban en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la devolución de su expediente siempre que no se haya presentado durante los plazos mencionados anteriormente.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

