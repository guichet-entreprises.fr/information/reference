﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS016" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Comerciante de mercancías" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="comerciante-de-mercancias" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/comerciante-de-mercancias.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="comerciante-de-mercancias" -->
<!-- var(translation)="Auto" -->


Comerciante de mercancías
=========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El comerciante inmobiliario es un profesional especializado en la compra y reventa de bienes inmuebles, construcción de terrenos, fondos comerciales o acciones o acciones de empresas inmobiliarias.

Trabajando en colaboración con arquitectos y técnicos maestros, rehabilita la propiedad y la revende tras añadirle valor.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Cualificaciones profesionales

No se requiere un diploma específico para ejercer como distribuidor de mercancías. Sin embargo, es esencial conocer la normativa inmobiliaria.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) no está sujeto a ningún requisito de cualificación o certificación, al igual que los franceses.

### c. Algunas peculiaridades de la regulación de la actividad

#### Obligación de contrato de seguro de propiedad y accidentes y seguro de responsabilidad civil a 10 años

Tan pronto como el distribuidor de la propiedad tiene trabajo en la propiedad que tiene la intención de revender, se le requiere que contrate el llamado seguro de "daños por daños" para reparar los daños que podrían resultar.

Además, en la apertura de su sitio, tendrá que ser capaz de justificar que ha contratado un seguro de responsabilidad civil de diez años.

*Para ir más allá* Artículos L. 242-1 y L. 241-1 del Código de Seguros.

#### Propiedades alquiladas

El distribuidor de la propiedad que desea comprar un edificio en el que viven los inquilinos debe continuar su contrato de arrendamiento. Del mismo modo, el vendedor del edificio no podrá desalojarlos cuando venda su propiedad.

*Para ir más allá* Artículo 1743 del Código Civil.

#### Renovación de un edificio para reventa

Cuando el comerciante de mercancías se compromete a realizar trabajos de gran importancia en el edificio, deberá obtener autorizaciones previas en el ayuntamiento antes de llevarlas a cabo. Sin embargo, cuando se trata de obras más pequeñas como terraza de una sola planta, piscina de menos de diez metros cuadrados o paredes de menos de dos metros de altura, está exenta.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de la empresa, el contratista debe o no registrarse en el Registro de Comercio y Sociedades (RCS). Es aconsejable consultar las "Formalidades de Reporte de una Empresa Comercial" para obtener más información.

### b. Si es necesario, registre los estatutos de la empresa

El comerciante de bienes deberá, una vez que los estatutos de la empresa estén fechados y firmados, registrarlos en la Oficina del Impuesto sobre Sociedades (IES) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio cuando los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

