﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS093" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Tierra" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="tierra" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/tierra.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="tierra" -->
<!-- var(translation)="Auto" -->


Tierra
======

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad de movimientos de tierra consiste en que el profesional (el terrassier) modifique el terreno natural de un terreno y prepare el suelo para la futura construcción o modificación de la estructura.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad del terrasser, el profesional debe:

- dependiendo del caso, estar profesionalmente calificado;
- realizar un curso de preparación de la instalación (SPI) (ver infra "3. a. Hacer una pasantía previa a la instalación");
- en función de la naturaleza de su actividad, regístrese en el Registro mercantil y de artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS).

Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

#### Entrenamiento

Es posible llevar a cabo esta actividad sin cualificaciones profesionales siempre y cuando no se realice ningún trabajo durante el trabajo.

Por otro lado, siempre y cuando se planifique dicha construcción, el profesional deberá estar cualificado o puesto bajo el control efectivo y permanente de una persona cualificada.

La persona que practique o controle el ejercicio debe ser el titular, de la elección:

- Un Certificado de Cualificación Profesional (CAP) "Controladores de Motor: Obras Públicas y Carreras";
- una Patente de Estudios Profesionales (BEP) "Obras Públicas";
- una patente profesional (BP) "Conductor de equipos de obras públicas";
- un Grado Universitario en Tecnología (DUT) "Opción de Ingeniería Civil Obras Públicas y Planificación (TPA)";
- un título profesional (TP) "Conductor de obras públicas viales, oleoductos, movimientos de tierra";
- una licencia profesional en una de las siguientes especialidades:- "Obras públicas",
  - "Comercios de construcción: obras públicas",
  - "Actividades industriales especializadas en materiales de construcción en el sector del hormigón"
  - "Ingeniería civil y construcción";
- ingeniero especializado en obras públicas, ingeniería civil, geología o mecánica eléctrica;
- un diploma o un grado de igual o nivel superior aprobado o registrado cuando fue emitido al Directorio Nacional de Certificaciones Profesionales (RNCP).

A falta de uno de estos títulos, el interesado deberá justificar una experiencia profesional efectiva de tres años, en un Estado de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE), adquirido como líder trabajador por cuenta propia o asalariado en el trabajo de un terrassier. En este caso, el interesado podrá solicitar a la CMA pertinente un certificado de cualificación profesional.

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 1 del Decreto 98-246, de 2 de abril de 1998; Decreto No 98-246, de 2 de abril de 1998, relativo a la cualificación profesional necesaria para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996; El sitio web oficial de la[RNCP](http://www.rncp.cncp.gouv.fr).

### b. Cualificaciones profesionales - Nacionales Europeos (Servicio Gratuito o Establecimiento Libre)

#### Para entrega gratuita de servicios (LPS)

El profesional que sea miembro de la UE o del EEE podrá llevar a cabo la actividad de movimiento de tierras en Francia de forma temporal y ocasional si está legalmente establecido en uno de estos Estados y lleva a cabo la misma actividad allí.

Primero tendrá que solicitarlo mediante declaración escrita a la CMA pertinente (véase infra "3o. b. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)).

Si ni la actividad ni la formación que la lleva a ella están reguladas en el estado en el que el profesional está legalmente establecido, la persona también debe demostrar que ha estado activa en ese estado durante al menos un año a tiempo completo o a tiempo parcial durante el diez años antes de la actuación que quiere actuar en Francia.

**Tenga en cuenta que**

El profesional que cumpla estas condiciones está exento de los requisitos de registro del directorio de operaciones (RM) o del registro de empresas.

*Para ir más allá* Artículo 17-1 de la Ley 96-603 de 5 de julio de 1996.

#### Para un establecimiento gratuito (LE)

Para llevar a cabo la actividad de movimiento de tierras de forma permanente en Francia, el profesional nacional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba: "2. a. Cualificaciones profesionales");
- Poseer un certificado de competencia o título de formación requerido para el ejercicio de la actividad en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un documento de formación que certifique su preparación para el ejercicio de la actividad cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de esta actividad;
- ser un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona ha estado activa durante tres años en el estado que ha admitido la equivalencia.

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones anteriores podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante: "3. c. Si es necesario, solicite un certificado de reconocimiento de la cualificación profesional.")

Si la persona no cumple con cualquiera de las condiciones anteriores, la CMA puede pedirle que realice una medida de compensación en los siguientes casos:

- si la duración de la formación certificada es al menos un año inferior a la requerida para obtener una de las cualificaciones profesionales requeridas en Francia para llevar a cabo la actividad;
- Si la formación recibida abarca temas sustancialmente diferentes de los cubiertos por una de las cualificaciones requeridas para llevar a cabo la actividad en Francia;
- Si el control efectivo y permanente de la actividad requiere, para el ejercicio de algunas de sus competencias, una formación específica que no se imparte en el Estado miembro de origen y que cubra temas sustancialmente diferentes de los cubierto por el certificado de competencia o designación de formación a que se refiere el solicitante.

*Para ir más allá* Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998.

**Bueno saber: medidas de compensación**

La CMA, que solicita un certificado de reconocimiento de la cualificación profesional, notifica al solicitante su decisión de que realice una de las medidas de compensación. Esta Decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son imprescindibles para ejercer en Francia.

A continuación, el solicitante debe elegir entre un curso de ajuste de hasta tres años o una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final del curso de adaptación, el solicitante envía al CMA un certificado que certifica que ha completado válidamente esta pasantía, acompañado de una evaluación de la organización que lo supervisó. La CMA emite, sobre la base de este certificado, un certificado de cualificación profesional en el plazo de un mes.

La decisión de utilizar una medida de indemnización podrá ser impugnada por el interesado, que deberá presentar un recurso administrativo ante el prefecto en el plazo de dos meses a partir de la notificación de la decisión. Si su apelación es desestimada, puede iniciar una impugnación legal.

*Para ir más allá* Artículos 3 y 3-2 del Decreto 98-246, de 2 de abril de 1998; Artículo 6-1 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos para la aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

### c. Condiciones de honorabilidad

Nadie puede practicar como terrassier si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 de la Ley 96-603, de 5 de julio de 1996.

### d. Algunas peculiaridades de la regulación de la actividad

#### Reglamento sobre la calidad del artesano y los títulos de maestro artesano y mejor trabajador en Francia

**Artesanía**

Para reclamar la condición de artesano, la persona debe justificar:

- una PAC, un BEP o un título certificado o registrado cuando se emitió al RNCP al menos equivalente (véase arriba: "2. a. Cualificaciones profesionales");
- experiencia profesional en este comercio durante al menos tres años.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de maestro artesano**

Este título se otorga a las personas, incluidos los líderes sociales de las personas jurídicas:

- Registrado en el directorio trades;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

###### Tenga en cuenta que

Las personas que no poseen el título de máster pueden solicitar a la Comisión Regional de Cualificaciones el título de Maestro Artesano bajo dos supuestos:

- cuando están inscritos en el directorio de oficios, tienen un título al menos equivalente al máster, y justifican la gestión y los conocimientos psicopedagógicos equivalentes a los de las correspondientes unidades de valor del máster, y que justifican dos años de práctica profesional;
- cuando se han inscrito en el repertorio de comercio durante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de mejor trabajador de Francia (MOF)**

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en los ámbitos artesanal, comercial, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación. Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

Para obtener más información, se recomienda que consulte[sitio web oficial](http://www.meilleursouvriersdefrance.info/) "uno de los mejores trabajadores de Francia."

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

#### Cumplimiento de las normas de seguridad y accesibilidad

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales de las normas de seguridad contra incendios y pánico en los ERP.

Es aconsejable consultar la hoja "Establecimiento que recibe al público" para obtener más información.

#### Cumplimiento de las disposiciones para los trabajadores jóvenes

Algunos trabajos no pueden ser realizados por mineros, incluyendo:

- que puedan exponerlos a peligros (radiación, metales, agentes químicos o biológicos peligrosos, etc.);
- trabajo temporal de gran altura;
- exponiéndolos a temperaturas extremas.

*Para ir más allá* Artículos D. 4153-15 y los siguientes artículos del Código de Trabajo.

#### Seguridad de los trabajadores

El profesional está obligado a garantizar la seguridad y la salud de las personas que trabajan en el sitio. Para ello, debe:

- Evaluar los riesgos y evitarlos tanto como sea posible
- adaptar los equipos y métodos de trabajo a sus empleados;
- medidas de protección colectiva y dar instrucciones apropiadas a los trabajadores.

*Para ir más allá* Artículos L. 4121-1 a L. 4121-5 del Código de Trabajo.

#### Seguro de trabajo

Cualquier constructor de una obra puede ser considerado responsable tan pronto como se produzcan daños y afecte a su uso y solidez. Se dice que esta responsabilidad tiene diez años y dura un período de diez años desde el momento en que se recibe la obra.

Como tal, para estar cubierto contra este daño, el profesional debe contratar un seguro antes del inicio de la construcción o, en el contexto de un contrato público, en el momento de presentar la solicitud.

Además, cuando el profesional celebra un subcontrato, está obligado por las mismas obligaciones de seguro que el contratista principal que tiene el trabajo realizado en nombre de otros.

**Tenga en cuenta que**

El seguro de responsabilidad profesional es opcional, pero sin embargo se recomienda, especialmente para actividades de riesgo.

*Para ir más allá* Artículos L. 241-1 y L. 241-2 del Código de Seguros.

**Autorización para conducir vehículos autopropulsados y dispositivos de elevación**

Cualquier profesional que utilice equipos de elevación de carga (grues, equipos de construcción, plataformas de elevación, etc.) durante su actividad debe tener una autorización de conducción emitida por el jefe del equipo. Establecimiento. Para ello, el interesado debe haber sido capacitado.

La duración y el contenido de esta formación varían en función de la naturaleza del equipo implicado. Para más información, es aconsejable acercarse a las organizaciones que proporcionan la formación o a la institución en la que trabaja el profesional.

*Para ir más allá* Artículos R. 4323-55 a R. 4353-57 del Código de Trabajo; orden de 2 de diciembre de 1998 relativa a la formación en el funcionamiento de equipos móviles de trabajo autopropulsados y dispositivos de carga o elevación.

**Eliminación de amianto**

La empresa que emplee a trabajadores que puedan estar expuestos al amianto debe establecer medidas de protección adaptadas a la naturaleza del trabajo realizado, entre ellas:

- Garantizar la protección de los profesionales expuestos;
- para limitar la propagación de las fibras de amianto.

Además, si el nivel de desempolvado es demasiado alto, el profesional debe tomar las medidas necesarias para reducir el nivel del nivel y debe informar al contratista, al inspector de trabajo y a la agencia de servicios de prevención seguridad social.

*Para ir más allá* Artículos R. 4412-107 a R. 4412-115 del Código de Trabajo.

**Normas técnicas para nuevos equipos**

Cuando el profesional utiliza equipos nuevos o nuevos, debe cumplir con las normas técnicas establecidas en el Apéndice 1 Artículo R. 4312-1 del Código de Trabajo.

**Normas técnicas de protección**

El profesional que lleva a cabo movimientos de tierra deberá tomar todas las medidas generales de seguridad, incluso para informarse antes del inicio de la obra con el servicio de carretera:

- la existencia de tierras reportadas;
- la presencia de cables o tuberías en el área de trabajo;
- riesgos de impregnar el sótano con humos o productos nocivos.

*Para ir más allá* Artículos R. 4534-1 y los siguientes artículos del Código de Trabajo.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Completar un curso de preparación de la instalación (SPI)

Antes de registrarse, el profesional debe completar un curso de preparación de la instalación (SPI). Este último es un requisito obligatorio para cualquier persona que solicite el registro en el repertorio de oficios y artesanías.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para la creación de un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo una educación en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado de un Estado miembro de la Unión Europea (UE) u otro Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) está exento del SPI si justifica con la CMA una cualificación en la gestión le confiere un nivel de conocimiento equivalente al proporcionado por la pasantía.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que:

- han trabajado durante al menos tres años exigiendo un nivel de conocimientos equivalente al proporcionado por las prácticas;
- tener conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que pueda cubrir total o parcialmente la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con las requeridas. Francia para la gestión de una empresa de artesanía.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, artículo 6-1 del Decreto 83-517, de 24 de junio de 1983.

### b. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

La CMA del lugar en el que el nacional desea llevar a cabo el beneficio es competente para emitir la declaración previa de actividad.

**Documentos de apoyo**

La solicitud de un informe previo de la actividad va acompañada de un archivo completo que contiene los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE;
- un documento que justifique la cualificación profesional del nacional que puede ser, a su elección:- Una copia de un diploma, título o certificado,
  - Un certificado de competencia,
  - cualquier documentación que acredite la experiencia profesional del nacional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tenga en cuenta que**

Cuando el expediente está incompleto, la CMA tiene un plazo de quince días para informar al nacional y solicitar todos los documentos que faltan.

**Resultado del procedimiento**

Al recibir todos los documentos en el archivo, el CMA tiene un mes para decidir:

- autorizar la prestación cuando el nacional justifique tres años de experiencia laboral en un Estado de la UE o del EEE, y adjuntar a dicha Decisión un certificado de cualificación profesional;
- o autorizar la disposición cuando las cualificaciones profesionales del nacional se consideren suficientes;
- o imponerle una prueba de aptitud cuando existan diferencias sustanciales entre las cualificaciones profesionales del nacional y las exigidas en Francia. En caso de denegación de esta medida de compensación o en caso de incumplimiento en su ejecución, el nacional no podrá llevar a cabo la prestación de servicios en Francia.

El silencio guardado por la autoridad competente en este plazo merece autorización para iniciar la prestación de servicios.

*Para ir más allá* Artículo 2 del Decreto de 2 de abril de 1998; Artículo 2 del Decreto de 17 de octubre de 2017 relativo a la presentación de la declaración y solicitudes previstas en el Decreto 98-246, de 2 de abril de 1998, y en el título 1 del Decreto 98-247, de 2 de abril de 1998.

### c. Si es necesario, solicite un certificado de reconocimiento de la cualificación profesional

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la ACM territorialmente competente.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA pide al interesado que lo complete dentro de una quincena de la presentación del expediente. Se emite un recibo tan pronto como se completa el archivo.

**Documentos de apoyo**

La carpeta debe contener las siguientes partes:

- Solicitar un certificado de cualificación profesional
- Un certificado de competencia o título de diploma o designación de formación profesional;
- Prueba de la nacionalidad del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

La CMA puede solicitar más información sobre su formación o experiencia profesional a la persona para determinar la posible existencia de diferencias sustanciales con cualificación profesional. requerido en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (CIEP) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**Qué saber**

Si es necesario, todos los documentos justificativos deben traducirse al francés.

**Tiempo de respuesta**

Dentro de los tres meses siguientes a la recepción, la CMA podrá:

- Reconocer la cualificación profesional y emitir la certificación de cualificación profesional;
- decidir someter al solicitante a una medida de compensación y notificarle dicha decisión;
- negarse a expedir el certificado de cualificación profesional.

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Remedios**

Si la CMA se niega a emitir el reconocimiento de la cualificación profesional, el solicitante podrá iniciar, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, una impugnación legal ante el tribunal administrativo pertinente. Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de indemnización, primero debe iniciar un recurso agraciado ante el prefecto del departamento en el que se basa la CMA en el plazo de dos meses a partir de la notificación de la decisión. Cma. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### d. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar los "Formalidades de Reporte de Empresas Artesanales" para obtener más información.

