﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS101" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Empresa domiciliada" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="empresa-domiciliada" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/empresa-domiciliada.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="empresa-domiciliada" -->
<!-- var(translation)="Auto" -->


Empresa domiciliada
===================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad de una sociedad domiciliada consiste en proporcionar una dirección jurídica profesional (postal, comercial, administrativa, sede central) a personas y corporaciones inscritas en el Registro de Comercios y Sociedades (RCS) o en el (RM) para llevar a cabo su actividad profesional. El ejercicio de esta actividad requiere la aprobación de la prefectura.

*Para ir más allá* Artículos L. 123-11-2 y el siguiente del Código de Comercio.

### b. Centro competente de formalidades comerciales (CFE)

Comercialmente activas, las empresas comerciales son responsabilidad de la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad de domicilio, la empresa debe:

- obtener la aprobación de la prefectura
- rcS (véase "3.0). b. Registro de la empresa").

Para ser aprobada, la empresa domiciliada debe:

- proporcionar a los residentes locales para garantizar:- confidencialidad necesaria para llevar a cabo sus actividades y para la reunión de los distintos órganos (gestión, administración, supervisión),
  - Mantener todos los documentos y registros obligatorios;
- justificar que dispone de locales legalmente disponibles para personas físicas o jurídicas (como titular o titular de un contrato de arrendamiento comercial);
- Si es así, cuando el profesional opera varias escuelas secundarias, deben cumplir con las condiciones anteriores;
- respetar las condiciones de honor específicas de su profesión (véase "2.0). c. Condiciones de honor").

**Tenga en cuenta que**

El hecho de que un profesional se dedique a la actividad de domicilio sin tener licencia se castiga con seis meses de prisión y una multa de 7.500 euros.

*Para ir más allá* Artículos L. 123-11-3 y L. 123-11-4 del Código de Comercio.

### b. Cualificaciones profesionales - Nacionales Europeos (Servicio Gratuito o Establecimiento Libre)

No existe ningún reglamento para el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE) para llevar a cabo la actividad domiciliaria temporal y temporal en Francia. casual o permanente.

Como tal, el profesional casado está sujeto a los mismos requisitos que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

### c. Condiciones de honorabilidad

**Lugar de ejercicio e instalación**

Una empresa domiciliada no puede operar en locales residenciales o profesionales de uso mixto.

*Para ir más allá* Artículo L. 123-11-2 del Código de Comercio.

**Incompatibilidades**

Para obtener una licencia, el profesional que desee realizar la actividad de domicilio no debe haber estado sujeto a:

- una condena definitiva por un delito o una pena de al menos tres meses de prisión sin una sentencia condicional por los delitos previstos en la II. (3) del artículo L. 123-11-3 Código de Comercio;
- Sanción disciplinaria o administrativa que dio lugar a la retirada de la acreditación;
- quiebra personal, prohibición o decomiso del ejercicio en el contexto de procedimientos de salvaguardia, liquidación o reparación judicial.

**Tenga en cuenta que**

Cuando la actividad es llevada a cabo por una corporación, accionistas o asociados que posean al menos el 25% de los votos, acciones o derechos de voto, así como los ejecutivos deben respetar las mismas condiciones de honorabilidad.

*Para ir más allá* Artículos L. 123-11-2 y L. 123-11-4 del Código de Comercio.

### d. Algunas peculiaridades de la regulación de la actividad

**Lucha contra la evasión fiscal**

El profesional que ejerza la actividad de domicilio deberá cumplir todas las obligaciones relativas a la lucha contra el blanqueo de dinero y la financiación del terrorismo Código monetario y financiero.

*Para ir más allá* Artículo L. 123-11-5 del Código de Comercio; Artículos L. 561-1 y siguientes del Código Monetario y Financiero.

**Contrato de vivienda**

El contrato debe celebrarse entre la persona física o jurídica domiciliada y la sociedad domiciliada. Este contrato debe ser escrito y mencionar las referencias a la aprobación otorgada a la vivienda.

**Tenga en cuenta que**

Este contrato es por un mínimo de tres meses y renovable por renovación tácita.

*Para ir más allá* Artículo R. 123-168 del Código de Comercio.

**Disposiciones para las instituciones públicas (ERP)**

Dado que las instalaciones están abiertas al público, el profesional debe cumplir con las normas relativas a las instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitud de acreditación

**Autoridad competente**

El profesional debe solicitar la acreditación ante el prefecto del departamento donde se encuentra la sede de la empresa domiciliada, o, para París, con la prefectura policial.

**Documentos de apoyo**

La solicitud debe incluir:

- Una declaración que incluya su nombre, actividad, dirección y, en su caso, la de las escuelas secundarias;
- El estado civil, dirección, ocupación y calidad del operador;
- Una identificación válida
- si el profesional es una persona jurídica, su forma jurídica y la identidad de representantes legales o legales, ejecutivos y accionistas que posean al menos el 25% de los votos, acciones o derechos de voto;
- prueba del cumplimiento de las obligaciones necesarias para obtener la acreditación (véase más arriba "2. a. Cualificaciones profesionales");
- un certificado de honor de que no es objeto de una medida de condena o prohibición para ejercer su profesión (véase más arriba "2". c. Condición de honorabilidad").

**hora**

El prefecto toma su decisión dentro de los dos meses siguientes a la recepción de la solicitud de acreditación. La falta de una respuesta al final de este período es el resultado del rechazo de la solicitud.

**Tenga en cuenta que**

Cualquier cambio debe ser reportado dentro de los dos meses al prefecto que emitió la acreditación. Además, si el profesional ya no cumple los requisitos para obtener la acreditación, este último podrá ser retirado o suspendido por el prefecto por un período de seis meses.

Si la empresa crea una escuela secundaria, debe justificar en un plazo de dos meses al prefecto que la ha aprobado que se cumplen las condiciones necesarias para cada una de las escuelas secundarias.

*Para ir más allá* Artículo R. 123-166-1 y siguientes, y Sección R. 123-166-2 del Código de Comercio.

### b. Informes de la empresa y trámites de registro

**Autoridad competente**

El profesional que se dedica a una actividad de dirección debe proceder a la declaración de su empresa. Para ello, debe presentar una declaración ante la CPI.

**Documentos de apoyo**

El interesado debe proporcionar la documentos de apoyo dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI envía un recibo al profesional el mismo día mencionando los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si la CFE se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa a registrar.

**Registro de la empresa**

El profesional debe registrarse en el RCS. Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

**Tenga en cuenta que**

La sociedad que desee llevar a cabo la actividad de una sociedad domiciliada deberá justificar el disfrute de los locales en los que establezca su sede social, o cuando se encuentre en el extranjero, el establecimiento de la representación, la agencia o la sucursal establecido en territorio francés.

*Para ir más allá* Artículo L. 123-11 del Código de Comercio.

*Para ir más allá* Artículo 635 del Código Tributario General.

### c. Si es necesario, registre los estatutos de la empresa

El profesional que ejerza la actividad de dirección deberá, una vez fechados y firmados los estatutos de la sociedad, registrarlos en la Oficina del Impuesto sobre Sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere (por ejemplo, el estado en la forma notarial).

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículos 635 y 862 del Código Tributario General.

