﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS011" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Controlador técnico de la construcción" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="controlador-tecnico-de-la-construccion" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/controlador-tecnico-de-la-construccion.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="controlador-tecnico-de-la-construccion" -->
<!-- var(translation)="Auto" -->


Controlador técnico de la construcción
======================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La misión del controlador técnico es prevenir cualquier peligro técnico que pueda ocurrir durante la construcción de una obra.

Garantizará la solidez de las estructuras, la seguridad de las personas que la ocuparán, el cumplimiento de la normativa relativa a las personas con movilidad reducida o rendimiento energético.

*Para ir más allá* Artículo L. 111-23 del Código de La Construcción y Vivienda;[Decreto 99-443 de 28 de mayo de 1999](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000561661&dateTexte=20180416) cláusulas técnicas generales aplicables a los contratos públicos de control técnico.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad.

- Para una actividad comercial, la CFE correspondiente será la Cámara de Comercio e Industria (CCI);
- Si se configura una empresa individual, es el Urssaf;
- para las sociedades civiles, la CFE competente es el registro del Tribunal de Comercio o el registro del tribunal de distrito en los departamentos del Bajo Rin, el Alto Rin y el Mosela.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal, independientemente del número de empleados de la empresa con la condición de que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

La profesión de controlador técnico está reservada a los titulares con acreditación ministerial (véase infra "3 grados). a. Buscar acreditación") y las siguientes cualificaciones profesionales:

- para el personal operativo y los ingenieros:- tienen un título postsecundario en construcción o ingeniería civil que justifica al menos cuatro años de estudio, y tienen al menos tres años de experiencia práctica en el diseño, implementación, control técnico o experiencia de Edificios
  - o tener seis años de experiencia práctica en el campo;
- para el personal de ejecución de la misión:- o bien poseer un certificado de escuela secundaria en el campo de actividad previsto, y una práctica de al menos tres años en diseño, construcción, control técnico o experiencia en construcción,
  - o tener seis años de experiencia práctica en el campo.

*Para ir más allá* Artículo R. 111-32-2 del Código de Construcción y Vivienda.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento de establecimiento libre (LE))

**Para ejercicios temporales e informales (LPS)**

Todo nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) que trabaje como responsable técnico podrá llevar a cabo la misma actividad en Francia de forma temporal e informal.

Para ello, la persona debe hacer una declaración previa al Ministro responsable de la construcción antes de su primera actuación (véase infra "3o). b. Predeclaración para los nacionales de la UE o del EEE para el ejercicio temporal y casual").

Además, cuando ni el acceso a la actividad ni su ejercicio estén regulados en ese Estado, el nacional debe justificar haber participado en esta actividad durante al menos dos años en los diez años anteriores a su primer beneficio.

*Para ir más allá* Artículo L. 111-25 del Código de La Construcción y Vivienda.

**Para un ejercicio permanente (LE)**

Toda actividad nacional legalmente establecida como responsable del tratamiento técnico en un Estado de la UE o del EEE podrá llevar a cabo la misma actividad de forma permanente en Francia. Para ello, el profesional debe solicitar la aprobación del Ministro responsable de la construcción, así como del nacional francés (véase infra "3o. a. Buscar aprobación").

En caso de diferencias sustanciales entre sus cualificaciones profesionales y las requeridas en Francia, el nacional será audicionado por el[Comisión de Acreditación](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=67FE5CB5D0712DFD0DD7565EC7F5A972.tplgfr36s_1?idArticle=LEGIARTI000020740035&cidTexte=LEGITEXT000006074096&dateTexte=20091007) y tendrá que demostrar sus habilidades y conocimientos en el campo de la construcción.

*Para ir más allá* Artículo L. 111-25 del Código de Construcción y Hábitat.

### c. Condiciones de honorabilidad

La actividad del controlador técnico es incompatible con las actividades de diseño, ejecución o experiencia laboral. Además, el responsable técnico debe actuar imparcialmente y no interferir con la independencia de quienes lleven a cabo las actividades anteriores.

*Para ir más allá* Artículo R. 111-31 del Código de Construcción y Hábitat.

### d. Algunas peculiaridades de la regulación de la actividad

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales de las normas de seguridad contra incendios y pánico en las instituciones públicas.

Es aconsejable referirse a la lista[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

**Suscríbete al seguro de responsabilidad civil a 10 años**

Como parte de su misión, el controlador técnico tendrá que sacar un seguro de responsabilidad civil de 10 años. Será responsable de los daños que comprometan la fuerza de la estructura o la hagan inadecuada para su destino.

*Para ir más allá* Artículo L. 111-24 del Código de La Construcción y Vivienda.

**Informe de actividad**

El controlador técnico debe presentar un informe de actividad al ministro responsable de la construcción cada 31 de marzo de cada año. El informe debe incluir:

- La actividad anual general del controlador
- Su fuerza de trabajo;
- la indicación de las mejoras que el controlador cree que ha realizado durante el ejercicio de su actividad;
- una descripción de las afirmaciones y malversación que encontró;
- transacciones para las que utilizó un subcontratista.

*Para ir más allá* :[Artículo 3](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0DC55679BA8BBCEF1289B99A1B3777B3.tplgfr24s_1?idArticle=LEGIARTI000021355456&cidTexte=LEGITEXT000021355429&dateTexte=20180416) del auto de 26 de noviembre de 2009 por el que se establecen las condiciones prácticas de acceso al ejercicio de la actividad del controlador técnico.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Buscar aprobación

**Autoridad competente**

El Ministro responsable de la construcción es responsable de expedir la licencia, que tiene una validez renovable de cinco años.

**Documentos de apoyo**

La solicitud de aprobación se realiza mediante la presentación de un archivo que incluye:

- El estado civil del solicitante y la dirección de su domicilio;
- la justificación de la competencia teórica y la experiencia práctica del personal directivo, la organización interna de la gestión técnica, las normas de asistencia a los servicios operativos efectivamente responsables del seguimiento y los criterios Contratación o asignación de oficiales
- El compromiso de que el Contralor actuará de manera imparcial e independiente;
- El compromiso del Contralor con la atención de la administración se realizará a cualquier cambio en la información que haya dado con el fin de obtener la acreditación;
- En caso afirmativo, todas las aprobaciones que ha obtenido previamente en el sector de la construcción;
- el alcance de la aprobación requerida de acuerdo con la categoría o categorías de construcción de la estructura, y la naturaleza o extensión de los caprichos que puedan resultar.

**Procedimiento**

El expediente se presentará a la Comisión de Acreditación, que garantizará el cumplimiento de las pruebas documentales. Ella escuchará a la solicitante antes de deliberar sobre qué hacer con su solicitud. Cuando emita un dictamen favorable, notificará al Ministro que emitirá la aprobación en un plazo de seis meses a partir de la recepción del expediente completo. El silencio guardado dentro de este tiempo valdrá la pena rechazar la solicitud.

*Para ir más allá* Artículos R.111-29 y R.111-32 del Código de Edificación y Vivienda; Apéndice III de la[decretado el 26 de noviembre de 2009](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021344640&dateTexte=20180416) establecer las formas prácticas en que se puede acceder a la actividad del controlador técnico;[enlace a la secretaría de la comisión de acreditación](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### b. Predeclaración para el nacional de la UE o del EEE para el ejercicio temporal y casual (LPS)

**Autoridad competente**

El Ministro responsable de la construcción es responsable de decidir sobre la declaración previa de actividad.

**Documentos de apoyo**

Esta solicitud se puede enviar electrónicamente o por correo y toma la forma de una carpeta con los siguientes documentos justificativos:

- El estado civil del nacional y la dirección de su casa;
- un certificado que justifique que está legalmente establecido en un Estado de la UE o del EEE y que no incurre en una prohibición de la práctica;
- cualquier documento que justifique sus cualificaciones profesionales;
- cualquier documento que justifique que ha participado en esta actividad durante al menos dos años en los últimos diez años, siempre que la UE o el Estado del EEE en el que esté establecida no regulen esta actividad;
- El compromiso de que el nacional actuará de manera imparcial e independiente;
- La naturaleza de la actuación que planea realizar y su fecha de inicio y finalización;
- un certificado de seguro de responsabilidad civil adaptado al beneficio que planea hacer.

**Procedimiento**

El Ministro responsable de la construcción dispondrá de un mes para verificar el expediente y autorizar la entrega, previa asesoración favorable de la Comisión de Acreditación. En caso de diferencias sustanciales entre las cualificaciones profesionales del nacional y las requeridas en Francia, el Ministro podrá pedir al nacional que comparezca ante la comisión y demuestre sus conocimientos y aptitudes Construcción.

El silencio de la autoridad competente después de la recepción del expediente, en el plazo de un mes, valdrá la pena aceptar la solicitud.

*Para ir más allá* Artículos R. 111-29-1 y R. 111-32-1 del Código de Construcción y Vivienda;[enlace a la secretaría de la comisión de acreditación](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### c. Formalidades de notificación de la empresa

El profesional está obligado a registrarse en el Registro Mercantil (RCS) ante la CPI en caso de creación de una empresa mercantil o a hacer una declaración de actividad ante el Urssaf en caso de creación de una empresa individual.

Es aconsejable consultar el "Registro de una empresa comercial individual en el RCS" para obtener más información.

4°. Textos de referencia
---------------------------------------------

- Artículos L. 111-23 a L. 111-26 del Código de Construcción y Vivienda (Código de Construcción y Vivienda);
- Artículos R. 111-29 a R. 111-42 del Código de La Construcción y Vivienda;
- Orden de 26 de noviembre de 2009 por la que se establecen las condiciones prácticas de acceso al ejercicio de la actividad del controlador técnico;
- Decreto 99-443, de 28 de mayo de 1999, relativo a las cláusulas técnicas generales aplicables a los contratos públicos de control técnico;
- NF 03-100.

