﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS030" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Comisario de Transportes" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="comisario-de-transportes" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/comisario-de-transportes.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="comisario-de-transportes" -->
<!-- var(translation)="Auto" -->

Comisario de Transportes
========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

El transportista es un profesional cuya misión es organizar el movimiento de mercancías de un lugar a otro, de acuerdo con los modos y medios de su elección.

Actúa como intermediario ya que celebra un contrato de comisión de transporte con su cliente, y uno o más contratos de transporte con transportistas que fleta en su nombre.

La Sección R. 1411-1 del Código de Transporte establece que las diversas actividades del Comisionado de Transporte son:

- Operaciones de agrupación: envío de mercancías desde varios remitentes o a múltiples destinatarios, ensamblados y formados en un único lote para el transporte;
- Operaciones chárter: los envíos se confían sin agrupación previa a transportistas públicos;
- Operaciones de la oficina de la ciudad: el comisionado se encarga de los paquetes o envíos al por menor y los entrega por separado a los transportistas públicos u otros comisarios de transporte;
- Operaciones de organización de transporte: el comisionado se encarga de las mercancías hacia y desde el territorio nacional, y proporciona el transporte por uno o más transportistas públicos a través de cualquier ruta.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para una profesión liberal, la CFE competente es el Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Cualificaciones profesionales

La profesión de comisario de transporte está reservada a cualquier persona con certificado de inscripción en el registro de transporte por carretera.

Para ser registrada, la persona debe estar en posesión de un certificado de capacidad profesional expedido por el prefecto de la región, siempre que justifique:

- tener un diploma en educación superior, habiendo recibido formación legal, económica, contable, comercial o técnica, o un diploma de educación técnica, habiendo recibido formación en actividades de transporte;
- haber pasado las pruebas de un revisión escrita en un centro de formación acreditado
- reconocimiento de cualificaciones profesionales adquiridas en un Estado miembro de la Unión Europea (UE) o parte en el acuerdo del Espacio Económico Europeo (EEE).

**Tenga en cuenta que**

La capacidad profesional necesaria para ejercer como comisario de transporte se adquiere siempre y cuando la persona haya recibido formación en gestión empresarial de al menos 200 horas de formación o cuando haya completado una pasantía de 80 horas. garantizar un nivel suficiente de ley aplicado al transporte, la economía del transporte y la comisión de transporte.

*Para ir más allá* Artículo R. 1422-4 del Código de Transporte; Artículos 7 y 8 de la Orden de 21 de diciembre de 2015 relativa a la expedición del certificado de capacidad profesional que permite el ejercicio de la profesión de transportista.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Para un ejercicio temporal e informal (Entrega de servicio gratuito)

No existe ningún reglamento para un nacional de la UE o del EEE que desee ejercer como comisionado de transporte en Francia, ya sea de forma temporal o ocasional.

Por lo tanto, sólo se aplicarán las medidas adoptadas para el libre establecimiento de nacionales de la UE o del EEE.

### Para un ejercicio permanente (Establecimiento Libre)

Para llevar a cabo la actividad de transporte en Francia de forma permanente, la UE o el nacional del EEE deben cumplir una de las siguientes condiciones:

- poseer un certificado de competencia o certificado de formación requerido para el ejercicio de la actividad de transportistaense en un Estado de la UE o del EEE cuando dicho Estado regule el acceso o el ejercicio de esta actividad en su territorio;
- han trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro estado que no regula la formación o el ejercicio de la profesión;
- tener un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona ha sido comisionaria de transporte en el Estado durante tres años, equivalencia admitida.

Una vez que cumpla una de estas condiciones, el nacional podrá solicitar el reconocimiento de sus cualificaciones profesionales por parte del prefecto de la región en la que desea ejercer su profesión (véase infra "3o. c. Solicitar el reconocimiento de sus cualificaciones profesionales para la UE o el EEE nacional para la actividad permanente (LE)).

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, el prefecto territorialmente competente podrá exigir que el interesado se someta a medidas de indemnización.

*Para ir más allá* Artículos R. 1422-11 y R. 1422-15 del Código de Transporte.

### c. Condiciones de honorabilidad, reglas éticas, ética

El comisionado de transportes debe cumplir las condiciones de honor y no debe, entre otras cosas:

- han sido condenados por un tribunal francés, registrado en el boletín 2 del historial penal, o por un tribunal extranjero y registrado en un documento equivalente, y pronunciando una prohibición de ejercer una profesión comercial o Industrial;
- conducir sin licencia
- Resort al trabajo oculto
- para transportar las llamadas mercancías peligrosas.

*Para ir más allá* Artículo R. 1422-7 del Código de Transporte.

### d. Algunas peculiaridades de la regulación de la actividad

#### Obligaciones de la Comisión

El comisionado debe llevar en un registro en forma de libro-periódico, información relativa a la naturaleza y cantidad de los bienes que lleva.

Además, se garantiza su llegada a tiempo, así como en caso de daños o pérdidas.

*Para ir más allá* Artículos L. 132-3 a L. 132-9 del Código de Comercio.

#### Menciones obligatorias del contrato de la comisión de transporte

Una vez celebrado el contrato de transporte, el comisario deberá asegurarse de que se menciona la siguiente información:

- naturaleza y finalidad del transporte;
- cómo se lleva a cabo el servicio tanto en lo que respecta al transporte real como a las condiciones de retirada y entrega de los objetos transportados;
- las obligaciones respectivas del remitente, el comisionado, el transportista y el destinatario;
- precios de transporte, así como de servicios auxiliares.

*Para ir más allá* Artículo L. 1432-2 del Código de Transporte.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de la empresa, el contratista debe registrarse en el Registro de Comercio y Sociedades (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Solicitar un certificado de capacidad profesional

El profesional que desee realizar la actividad de transporte comisionado debe tener un certificado de capacidad profesional.

**Autoridad competente**

El prefecto regional es responsable de entregar el certificado de capacidad profesional.

**Documentos de apoyo**

La solicitud de certificación se envía a la Dirección Regional de Medio Ambiente, Planificación y Vivienda e incluye los siguientes documentos justificativos:

- El formulario Cerfa 11414*05 ;
- Comprobante de residencia
- El documento que justifica su situación en relación con las obligaciones del servicio nacional;
- Una fotocopia del diploma o título de graduación
- Cuando el solicitante está empleado, una fotocopia de su contrato de trabajo y recibos de pago;
- un certificado de membresía en un fondo de pensiones.

*Para ir más allá* Artículo R. 1422-4 del Código de Transporte; 21 de diciembre de 2015 relativo a la expedición del certificado de capacidad profesional que permita el ejercicio de la profesión de transportista.

### c. Solicitar el reconocimiento de cualificaciones profesionales para los nacionales de la UE o del EEE para la actividad permanente (LE)

**Autoridad competente**

El prefecto regional es competente para expedir el certificado de capacidad profesional que justifica el reconocimiento de las cualificaciones profesionales del nacional.

**Documentos de apoyo**

La solicitud de certificación se realiza enviando un archivo al prefecto, incluyendo los siguientes documentos:

- Formulario Cerfa No. 11414*05 ;
- Una identificación válida
- una prueba de residencia, para la persona que tiene su residencia habitual en Francia;
- Una copia del certificado de competencia o certificado de formación expedido por un Estado de la UE o del EEE que regula la profesión de comisario de transporte;
- En caso afirmativo, cualquier documento que justifique el ejercicio jurídico del solicitante como operador de mercancías durante un año en los últimos diez años en un Estado de la UE o del EEE que no regule el acceso a la profesión o Entrenamiento
- en su caso, cualquier documento que justifique el ejercicio efectivo del solicitante durante al menos tres años, en un estado que haya concedido a la equivalencia un certificado de formación o certificado adquirido en un tercer estado y que permita el ejercicio de ese Profesión;
- programas de formación o el contenido de la experiencia adquirida.

**Tenga en cuenta que**

Si es necesario, todos los documentos justificativos deben ser traducidos al francés por un traductor certificado.

**Procedimiento**

Cuando se envíe el archivo, el prefecto confirmará la recepción en el plazo de un mes e informará al nacional de los documentos que falten. A menos que se adopten medidas de indemnización contra el nacional, retrasando el procedimiento, el prefecto podrá expedir el certificado de capacidad profesional en el plazo de un mes.

Una vez que el nacional haya obtenido su certificado de aforo profesional, podrá inscribirse en el registro de transporte por carretera.

*Para ir más allá* Artículos 9 a 15 de la orden del 21 de diciembre de 2015 sobre la emisión del certificado de capacidad profesional que permita el ejercicio de la profesión de comisionaria de transporte

**Es bueno conocer las medidas de compensación**

En caso de diferencias entre la formación o la experiencia del nacional y los requisitos exigidos en Francia, el prefecto podrá someterlo a una medida de compensación que pueda ser un curso de adaptación o una prueba de aptitud.

El curso de adaptación debe durar al menos 80 horas y permitirle adquirir un nivel suficiente de ley aplicado al transporte, la economía del transporte y la comisión de transporte.

La prueba de aptitud es en forma de un cuestionario de opción múltiple para el cual el nacional debe obtener una puntuación de al menos 60 de 100.

*Para ir más allá* Artículo R. 1422-18 del Código de Transporte.

### d. Solicitar inscripción en el registro de transporte por carretera

El registro se realiza enviando el formulario Cerfa No. 14557*03 autoridad de transporte.