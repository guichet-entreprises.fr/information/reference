﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS038" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Mantenimiento y reparación de vehículos" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="mantenimiento-y-reparacion-de-vehiculos" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/mantenimiento-y-reparacion-de-vehiculos.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="mantenimiento-y-reparacion-de-vehiculos" -->
<!-- var(translation)="Auto" -->


Mantenimiento y reparación de vehículos
=======================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad de mantenimiento y reparación de vehículos consiste en que el profesional, lleve a cabo en un taller (garaje) los controles, controles, mantenimiento y limpieza de vehículos de motor, así como posibles reparaciones Necesario.

Por lo tanto, el profesional puede estar obligado a realizar:

- Drenaje y revisión del vehículo
- todos los ajustes y reemplazo de piezas usadas o defectuosas (electrónicas, mecánicas);
- Reparación de la carrocería y chapa metálica;
- Pintura
- remolcando el vehículo.

También puede ofrecer a la venta:

- Combustibles y lubricantes
- accesorios automotrices
- vehículos nuevos o usados.

**Tenga en cuenta que**

La actividad de control técnico sólo puede ser llevada a cabo por establecimientos aprobados por el Estado dedicados exclusivamente a esta actividad. Así pues, un establecimiento que lleve a cabo la actividad de mantenimiento y reparación de los vehículos no puede ejercer la de control técnico.

*Para ir más allá* Artículo R. 323-8 de la Ley de Tráfico de Carreteras.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para las actividades artesanales, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para actividades comerciales, es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa siempre que no utilice ningún proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad de mantenimiento y reparación de vehículos, el profesional deberá:

- estar profesionalmente calificados o estar bajo el control efectivo y permanente de una persona profesionalmente calificada;
- han completado un curso de preparación de la instalación (véase a continuación "Hacer un curso de preparación de la instalación (SPI));
- estar registrado en el directorio de comercios (RM) o en el registro de empresas en caso de instalación en los departamentos del Rin inferior, el Alto Rin o el Mosela (véase más adelante. "Solicitud de registro en el directorio de operaciones").

#### Entrenamiento

Para ser reconocido como profesionalmente cualificado, el profesional que realiza la actividad de mantenimiento y reparación del vehículo o que controla el ejercicio del vehículo, deberá ser el titular de la elección:

- Un Certificado de Aptitud Profesional (CAP) para el mantenimiento de vehículos de motor;
- un máster (BM) en repairman - gerente en mantenimiento automotriz;
- un diploma o título de igual o superior nivel aprobado o registrado en el Registro Nacional de Certificaciones Profesionales[RNCP](http://www.rncp.cncp.gouv.fr/).

A falta de uno de estos títulos, el interesado deberá justificar una experiencia profesional efectiva de tres años, en un Estado de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE), adquirido como líder autónomos o empleados en el mantenimiento y reparación de vehículos. En este caso, el interesado podrá solicitar a la CMA pertinente un certificado de cualificación profesional.

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 1 del Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996, relativa al desarrollo y la promoción del comercio y la artesanía.

**Mantenimiento del vehículo CAP**

Este diploma de Nivel V (correspondiente a un graduado general y tecnológico antes del último año) está disponible para los candidatos después de un curso de formación como estudiante, con un contrato de aprendizaje, después de un curso de educación continua, en un contrato de profesionalización, por aplicación individual o mediante la validación de la experiencia adquirida (VAE) proceso adquirido. Para obtener más información, puede ver[Sitio](http://www.vae.gouv.fr/) El oficial de VAE.

La formación normalmente dura dos años y tiene lugar en una escuela secundaria vocacional.

**BM repairman - gerente de mantenimiento de automóviles**

El máster es una designación aprobada de Nivel III (B.A. 2) emitida por la CMA, cuya formación de dos años se realiza a través de la educación continua, un contrato de profesionalización o validación de la experiencia.

*Para ir más allá* Artículos D. 337-1 y los siguientes artículos del Código de Educación.

#### Completar un curso de preparación de la instalación (SPI)

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía es de un mínimo de 30 horas y es en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que son:

- han ejercido una actividad profesional que requiere un nivel de conocimientos equivalente al proporcionado por las prácticas durante al menos tres años;
- conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que cubriría, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con en Francia para dirigir una empresa de artesanías.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Una copia del diploma certificado de Nivel III;
- Una copia del máster
- una prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982; Artículo 6-1 del Decreto 83-517 de 24 de junio de 1983.

#### Solicitud de registro en el directorio de operaciones

**Autoridad competente**

El profesional debe solicitar a la CMA su inscripción en su sede central en el plazo de un mes a partir de su actividad. Sin embargo, la solicitud se puede hacer a más tardar un mes después del inicio de la actividad tan pronto como el profesional haya notificado al presidente de la CMA, la fecha de inicio de la CMA, a más tardar el día anterior por carta recomendada con aviso de recepción.

**Documentos de apoyo**

La lista de documentos justificativos que se enviarán a la CMA depende de la naturaleza de la actividad realizada.

**hora**

Tan pronto como el CMA presenta su archivo completo, envía un recibo para la solicitud de un archivo de creación de negocios al solicitante. Merece la pena no responder en un plazo de dos meses.

*Para ir más allá* Artículos 9 y siguientes del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios o establecimiento gratuito)

**Para entrega gratuita de servicios (LPS)**

El profesional que sea miembro de la UE o del EEE podrá llevar a cabo el mantenimiento y la reparación de vehículos en Francia de forma temporal y ocasional, siempre que esté legalmente establecido en uno de estos Estados para llevar a cabo la misma actividad.

Primero tendrá que solicitarlo mediante declaración escrita a la CMA pertinente (véase infra "3o. a. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)).

Si ni la actividad ni la formación que realiza allí están reguladas en el estado en el que el profesional está legalmente establecido, la persona también debe demostrar que ha estado activa en ese estado durante al menos un año a tiempo completo o a tiempo parcial durante el de los últimos diez años antes de la actuación que quiere realizar en Francia.

**Tenga en cuenta que**

El profesional que cumpla estas condiciones está exento de los requisitos de registro del directorio de operaciones (RM) o del registro de empresas.

*Para ir más allá* Artículo 17-1 de la Ley 96-603 de 5 de julio de 1996.

**En vista de un establecimiento libre (LE)**

Para llevar a cabo el mantenimiento y la reparación de vehículos en Francia de forma permanente, el profesional nacional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un ciudadano francés (véase más arriba: "2. a. Cualificaciones profesionales");
- Poseer un certificado de competencia o título de formación requerido para el ejercicio de la actividad en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su disposición a llevar a cabo la actividad cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de esta actividad ;
- ser un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona ha estado activa durante tres años en el estado que ha admitido la equivalencia.

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones anteriores podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante: "3. b. Solicitar un certificado de cualificación profesional para un ejercicio permanente (LE)")

Si la persona no cumple con cualquiera de las condiciones anteriores, la CMA puede pedirle que realice una medida de compensación en los siguientes casos:

- si la duración de la formación certificada es al menos un año inferior a la requerida para obtener una de las cualificaciones profesionales requeridas en Francia para llevar a cabo la actividad;
- Si la formación recibida abarca temas sustancialmente diferentes de los cubiertos por una de las cualificaciones requeridas para llevar a cabo la actividad en Francia;
- Si el control efectivo y permanente de la actividad requiere, para el ejercicio de algunas de sus competencias, una formación específica que no se imparte en el Estado miembro de origen y que cubra temas sustancialmente diferentes de los cubierto por el certificado de competencia o designación de formación a que se refiere el solicitante.

*Para ir más allá* Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998.

### c. Condiciones de honorabilidad

Nadie puede ejercer la profesión de reparación y mantenimiento de vehículos si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 de la Ley 96-603, de 5 de julio de 1996.

### d. Algunas peculiaridades de la regulación de la actividad

**Normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

Es aconsejable consultar la hoja "Establecimiento que recibe al público" para obtener más información.

**Requisito de contabilización**

El profesional que lleva a cabo el mantenimiento y reparación de los vehículos, deberá visualizar los precios aplicados todos los impuestos incluidos (T.T.C.) en la entrada del establecimiento y la recepción, de una manera legible y visible para los clientes.

Además, si el profesional se dedica a remolcar o solucionar problemas, debe mostrar las tarifas T.T.C. de estas operaciones.

*Para ir más allá* : orden de 27 de marzo de 1987 relativo a las normas de publicidad de precios para servicios de mantenimiento o reparación, control técnico, solución de problemas o remolque, así como garaje para vehículos.

**Facturación**

El profesional está sujeto al cumplimiento de las obligaciones de facturación y debe emitir una nota al cliente, en caso de un beneficio de un importe superior o igual a 25 euros. Esta nota debe incluir la siguiente información:

- Su fecha de escritura;
- El nombre y la dirección del proveedor
- El nombre del cliente (si el cliente no se opone a él);
- La fecha y el lugar de ejecución del servicio
- los detalles de cada beneficio, así como el importe total a pagar (todos los impuestos incluidos y excluidos los impuestos).

El profesional debe mantener un doble de este grado, clasificarlos cronológicamente y mantenerlos durante dos años.

*Para ir más allá* ( Orden 83-50/A, de 3 de octubre de 1983, relativa a la publicidad de precios de todos los servicios.

**Regulaciones de remolque y solución de problemas de vehículos**

El profesional en caso de solución de problemas y remolque de un vehículo en autopista o carretera exprés deberá visualizar las tarifas cobradas de acuerdo con el orden del 10 de agosto de 2017 en relación con la tasa de reparación de vehículos ligeros en carreteras y carreteras exprés.

**Requisito de ofrecer piezas usadas para reparaciones de vehículos**

El profesional que realiza la actividad de mantenimiento o reparación de vehículos está obligado a ofrecer a los consumidores piezas de repuesto usadas para la reparación de su vehículo.

Sin embargo, esta obligación no se aplica cuando:

- El mantenimiento o la reparación se llevaron a cabo de forma gratuita o bajo garantía contractual;
- Las piezas de repuesto no están disponibles durante el período de inmovilización del vehículo.
- el profesional cree que las piezas de repuesto representan un riesgo significativo para el medio ambiente, la salud pública o la seguridad vial.

*Para ir más allá* Artículos L. 224-67, R. 224-22 y siguientes del Código del Consumidor.

**Reglamento sobre la calidad del artesano y los títulos de maestro artesano y mejor trabajador en Francia**

**Artesanía**

Para reclamar la condición de artesano, la persona debe justificar:

- una PAC, un BEP o un título certificado o registrado cuando se emitió al RNCP al menos equivalente (véase arriba: "2. a. Cualificaciones profesionales");
- experiencia profesional en este comercio durante al menos tres años.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de maestro artesano**

Este título se otorga a las personas, incluidos los líderes sociales de las personas jurídicas:

- Registrado en el directorio trades;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

**Tenga en cuenta que**

Las personas que no posean el título de máster pueden solicitar el título de Maestro Artesano en la Comisión Regional de Cualificaciones en dos hipótesis:

- cuando están inscritos en el directorio de oficios, tienen un título al menos equivalente al máster, y justifican la gestión y los conocimientos psicopedagógicos equivalentes a los de las correspondientes unidades de valor del máster y que tienen dos años de práctica profesional;
- cuando se han inscrito en el repertorio de comercio durante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de mejor trabajador de Francia (MOF)**

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en el ámbito artesanal, comercial, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación. Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

Para obtener más información, se recomienda que consulte[sitio web oficial](http://www.meilleursouvriersdefrance.info/) "uno de los mejores trabajadores de Francia."

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

La CMA del lugar en el que el nacional desea llevar a cabo la prestación es competente para emitir la declaración previa de actividad.

**Documentos de apoyo**

La solicitud de un informe previo de la actividad va acompañada de un archivo completo que contiene los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE;
- un documento que justifique la cualificación profesional del nacional que puede ser, a su elección:- Una copia de un diploma, título o certificado,
  - Un certificado de competencia,
  - cualquier documentación que acredite la experiencia profesional del nacional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tenga en cuenta que**

Cuando el expediente está incompleto, la CMA tiene un plazo de quince días para informar al nacional y solicitar todos los documentos que faltan.

**Resultado del procedimiento**

Al recibir todos los documentos en el archivo, el CMA tiene un mes para decidir:

- autorizar la prestación cuando el nacional justifique tres años de experiencia laboral en un Estado de la UE o del EEE, y adjuntar a dicha Decisión un certificado de cualificación profesional;
- o autorizar la disposición cuando las cualificaciones profesionales del nacional se consideren suficientes;
- o imponerle una prueba de aptitud cuando existan diferencias sustanciales entre las cualificaciones profesionales del nacional y las exigidas en Francia. En caso de negativa a realizar esta medida de compensación o si no cumple, el nacional no podrá prestar el servicio en Francia.

El silencio guardado por la autoridad competente en este plazo merece autorización para iniciar la prestación de servicios.

*Para ir más allá* Artículo 2 del Decreto de 2 de abril de 1998; Artículo 2 del Decreto de 17 de octubre de 2017 relativo a la presentación de la declaración y solicitudes previstas en el Decreto 98-246, de 2 de abril de 1998, y en el título I del Decreto 98-247, de 2 de abril de 1998.

### b. Solicitar un certificado de cualificación profesional para un ejercicio permanente (LE)

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la ACM territorialmente competente.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA pide al interesado que lo complete dentro de una quincena de la presentación del expediente. Se emite un recibo tan pronto como se completa el archivo.

**Documentos de apoyo**

La carpeta debe contener las siguientes partes:

- Solicitar un certificado de cualificación profesional
- Un certificado de competencia o título de diploma o designación de formación profesional;
- Prueba de la nacionalidad del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (CIEP) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**Qué saber**

Si es necesario, todos los documentos justificativos deben traducirse al francés.

**Tiempo de respuesta**

Dentro de los tres meses siguientes a la recepción, la CMA podrá:

- Reconocer la cualificación profesional y emitir la certificación de cualificación profesional;
- decidir someter al solicitante a una medida de compensación y notificarle dicha decisión;
- negarse a expedir el certificado de cualificación profesional.

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Remedios**

Si la CMA se niega a emitir el reconocimiento de la cualificación profesional, el solicitante podrá iniciar, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, una impugnación legal ante el tribunal administrativo pertinente. Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de indemnización, primero debe iniciar un recurso agraciado ante el prefecto del departamento en el que se basa la CMA, en el plazo de dos meses a partir de la notificación de la decisión. Cma. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998, auto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un profesional un Estado miembro de la Comunidad Europea u otro Estado parte en el acuerdo del Espacio Económico Europeo.

**Bueno saber: medidas de compensación**

La CMA, que solicita un certificado de reconocimiento de la cualificación profesional, notifica al solicitante su decisión de que realice una de las medidas de compensación. Esta Decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son imprescindibles para ejercer en Francia.

A continuación, el solicitante debe elegir entre un curso de ajuste de hasta tres años o una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en el plazo de seis meses a partir de la recepción por la CMA de la decisión del solicitante de optar por esta prueba. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final del curso de adaptación, el solicitante envía al CMA un certificado que certifica que ha completado válidamente esta pasantía, acompañado de una evaluación de la organización que lo supervisó. La CMA emite, sobre la base de este certificado, un certificado de cualificación profesional en el plazo de un mes.

La decisión de utilizar una medida de indemnización podrá ser impugnada por el interesado, que deberá presentar un recurso administrativo ante el prefecto en el plazo de dos meses a partir de la notificación de la decisión. Si su apelación es desestimada, puede iniciar una impugnación legal.

*Para ir más allá* Artículos 3 y 3-2 del Decreto 98-246, de 2 de abril de 1998, de artículo 6-1 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos de aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

### c. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable referirse a las "Formalidades de Reporte de Empresas Artesanales", "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Individual Comercial en el RCS" para obtener más información.

### d. Autorización posterior al registro

#### Instalaciones Clasificadas para la Protección Ambiental (ICPE)

Cuando la actividad del profesional suponga riesgos para el medio ambiente o la salud, la salud y la seguridad pública, el establecimiento del profesional estará sujeto a la emisión de un procedimiento de registro o presentación de informes de acuerdo con el la naturaleza de su negocio.

Como tal, el profesional primero tendrá que verificar que su actividad está dentro del ámbito de uno de los[Nomenclatura](https://aida.ineris.fr/liste_documents/1/18298/1) ICPE está disponible en el sitio web del Ministerio de Transición Ecológica y Solidaria.

Las instalaciones del profesional para el mantenimiento y la reparación de vehículos se pueden utilizar en dos áreas diferentes:

- 2930 "Talleres de Reparación y Mantenimiento de Vehículos y Vehículos Motorizados";
- 2560 "Trabajo mecánico de metales y aleaciones."

Dependiendo de la naturaleza de la actividad, el profesional tendrá que hacer la declaración, inscripción o solicitud de autorización previa.

**Instalaciones inferiores a 2930**

Para la actividad de reparación y mantenimiento de vehículos y vehículos de motor el profesional tendrá que llevar a cabo:

- una solicitud de permiso si la superficie de su taller es de más de 5.000 metros cuadrados;
- una declaración con control periódico, si el área de su taller es superior a 2000 metros cuadrados pero menor o igual a 5000 metros cuadrados.

Para la actividad de barniz, pintura o imprimación, el profesional tendrá que realizar:

- una solicitud de permiso si la cantidad máxima de productos que se pueden utilizar es de más de 100 kilos por día;
- un chequeo periódico si esta cantidad es superior a 10 kilos por día o si la cantidad anual de disolventes contenida en los productos es superior a O.5 toneladas y la cantidad máxima de productos supera los 100 kilos por día.

**Instalaciones bajo la sección 2560**

El procedimiento depende de la potencia máxima de las máquinas:

- Si esta potencia es superior a 1.000 kilovatios (kW), el profesional tendrá que registrar sus instalaciones:
- si esta potencia es superior a 150 kW pero inferior a 1.000 kW o menos tendrá que informar con control periódico.

Es aconsejable consultar el[Sitio de Aida - Ineris](https://aida.ineris.fr/liste_documents/1/18016/1) para conocer los términos de los diferentes procedimientos.

*Para ir más allá* Artículo L. 511-1 del Código de Medio Ambiente; Orden de 4 de junio de 2004 relativa a los requisitos generales de las instalaciones clasificadas sujetas a declaración en la partida 2930 relativaa a los talleres de reparación y mantenimiento de vehículos y vehículos de motor, carrocería y chapa metálica.

#### En caso de actividad relacionada con refrigerantes

El profesional que utiliza líquidos refrigerantes (particularmente en equipos como la bomba de calor o el aire acondicionado del vehículo) debe obtener un certificado de capacidad.

**Autoridad competente**

El profesional debe presentar su solicitud a una organización aprobada por el Comité francés de Acreditación ([Cofrac](https://www.cofrac.fr/)).

**Documentos de apoyo**

El archivo de solicitud debe incluir:

- La identidad del solicitante y su dirección, o si es así el nombre, la forma jurídica, el número SIRET y la dirección de la sede de la corporación;
- Una lista de las categorías de actividades que el profesional pretende llevar a cabo;
- La lista de partes interesadas involucradas en estas actividades;
- Los tipos y cantidades de herramientas utilizadas para ejercer;
- el compromiso del profesional de entregar cada año al operador que expidió el certificado, una declaración a más tardar el 31 de diciembre del año civil anterior, especificando las cantidades de fluidos adquiridos, cargados, destruidos, regenerados y almacenados en las condiciones establecidas en la orden de 30 de junio de 2008 sobre la expedición de certificados de capacidad a los operadores en virtud del artículo R. 543-99 del Código de Medio Ambiente.

**Retrasos y procedimientos**

La organización emite el certificado de capacidad en un plazo de dos meses a partir de la recepción de la aplicación. Este certificado de capacidad se expide por un período de cinco años.

*Para ir más allá* Artículos R. 543-99 y R. 543-108 y siguientes del Código de Medio Ambiente.

