﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS022" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Agente de viajes" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="agente-de-viajes" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/agente-de-viajes.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agente-de-viajes" -->
<!-- var(translation)="Auto" -->




Agente de viajes
================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El operador de viajes y viajes (agencia de viajes, por ejemplo) es un profesional que desarrolla y vende u ofrece a la venta:

- paquetes turísticos;
- servicios de transporte, alojamiento, alquiler de coches u otros servicios de viaje que no producen ellos mismos.

También se aplica a los profesionales que facilitan a los viajeros la compra de "beneficios de viaje relacionados".

*Para ir más allá* Artículos L. 211-1 y L. 211-2 del Código de Turismo.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. En el caso de un operador de viajes y estancias, se trata de una actividad comercial. Por lo tanto, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere diploma ni formación especial para llevar a cabo la actividad de operador de viajes y residencia desde el 1 de enero de 2016. Sólo las condiciones de seguro de responsabilidad profesional y garantía financiera son necesarias para llevar a cabo esta actividad.

*Para ir más allá* : Ordenanza No 2015-1682 de 17 de diciembre de 2015 simplificando ciertos regímenes de preautorización y presentación de informes para empresas y profesionales.

### b. Garantía financiera

Tener una garantía financiera suficiente es un requisito obligatorio para registrarse en el registro de los operadores de viajes y residencia. Este registro condiciona el ejercicio jurídico de la profesión en Francia.

La garantía financiera tiene por objeto garantizar, en su caso, el reembolso de los fondos y anticipos pagados por los clientes al operador de viajes y residencia para los servicios actuales o futuros. También garantiza la repatriación de los clientes en caso de cese de la situación de los pagos que ha dado lugar a la declaración concursal de los operadores de viajes y vacaciones.

La garantía financiera es el resultado de un contrato de garantía realizado, por elección, por:

- Un organismo de garantía colectiva
- Una entidad de crédito, una compañía financiera o una compañía de seguros autorizada para dar una garantía financiera;
- un grupo de asociaciones u organizaciones sin ánimo de lucro que han recibido una autorización especial por decreto del Ministro encargado del turismo y cuentan con un fondo de solidaridad suficiente.

El importe de la garantía financiera debe cubrir todos los fondos pagados por los clientes y los gastos de repatriación de los viajeros en caso de fallo de la agencia.

La suscripción a una garantía financiera da lugar a la entrega de un certificado que debe especificar:

- El nombre del garante
- las fechas de validez del contrato de garantía.

*Para ir más allá* Artículo R. 211-26 y siguientes del Código de Turismo; Decreto No 2015-1111, de 2 de septiembre de 2015, relativo a la garantía financiera y la responsabilidad civil profesional de los operadores de viajes y vacaciones y otros operadores para la venta de viajes y estancias.

### c. Seguro de responsabilidad civil

Para ejercer legalmente en Francia, el operador de viajes y residencia tiene la obligación de disponer de un seguro de responsabilidad civil profesional.

El seguro de responsabilidad profesional proporciona cobertura para la reparación de los daños causados por el operador a clientes, proveedores de servicios o terceros como resultado de fallas, errores, omisiones o negligencias cometidas durante el venta o ejecución de servicios turísticos.

El operador de viajes y viajes debe informar claramente a sus clientes de los riesgos cubiertos y de las garantías suscritas en virtud de este seguro.

Una vez asegurado, el operador de viajes y viajes recibe un certificado de suscripción, cuyo modelo está disponible en el [Sitio web de Atout France](https://registre-operateurs-de-voyages.atout-france.fr).

El certificado deberá llevar las siguientes menciones:

- Referencia a las disposiciones legales y reglamentarias existentes;
- El nombre de la compañía de seguros;
- El número de la póliza de seguro suscrita
- El período de validez del contrato (principio y fin de validez);
- El nombre y la dirección o el nombre del operador de viajes asegurado;
- el alcance de las garantías.

*Para ir más allá* Artículo L. 211-16 y R. 211-35 y siguientes del Código de Turismo.

### d. Algunas peculiaridades de la regulación de la actividad

#### Obligación de mantener los libros y documentos a disposición de los oficiales autorizados para consultarlos

Las personas físicas o jurídicas inscritas en el registro de registro de operadores de viajes y residencia, deben mantener sus libros y documentos a disposición de los agentes autorizados para su consulta.

*Para ir más allá* Artículo L. 211-5 del Código de Turismo.

#### Requisito de mencionar el registro de operadores de viajes y vacaciones en el registro de registro

Los operadores de viajes y vacaciones están obligados a mencionar su inscripción en el registro de registro de operadores de viajes y vacaciones en su signo, en los documentos entregados a terceros y en su publicidad.

*Para ir más allá* Artículos L. 211-5 y R. 211-2 del Código de Turismo.

#### Reglas para la celebración y ejecución de contratos de viajes y ventas vacacionales

La celebración de contratos de venta de viajes o estancias por parte de un operador de viajes y residencia debe cumplir determinadas condiciones de forma y sustancia detalladas en los artículos L. 211-7 y siguientes del Código de Turismo. Estas condiciones incluyen la obligación de proporcionar información previa al contrato por parte del operador de viajes y estancias al cliente, las menciones obligatorias que se incluirán en el contrato (descripción de los servicios prestados, derechos y obligaciones recíprocas de las partes en términos de precio, plazo, condiciones de pago, etc.) o los términos del cambio de contrato.

Para obtener más información, consulte los artículos L. 211-7 y los siguientes artículos del Código de Turismo.

#### Contrato de disfrute del edificio de tiempo compartido

Los operadores de viajes y estancias inscritos en el registro de operadores de viajes pueden celebrar cualquier contrato para el disfrute de un edificio de tiempo compartido o prestar su asistencia a la celebración de dichos contratos en virtud de un mandato escrito.

Para más información sobre las menciones obligatorias del contrato de mandato, así como las condiciones de remuneración del agente, es aconsejable remitirse al artículo L. 211-24 del Código de Turismo.

*Para ir más allá* Artículos L. 211-24, R. 211-45 y siguientes del Código de Turismo.

#### Requisito de información de pasajeros aéreos sobre la identidad de la aerolínea

Para los servicios que incluyen el transporte aéreo en un paquete turístico, los operadores de viajes y viajes deben proporcionar al consumidor una lista de hasta tres compañías aéreas por cada tramo de vuelo, incluida una lista de el transportista contractual y el transportista de facto que el operador turístico utilizará eventualmente.

*Para ir más allá* Artículo R. 211-15 del Código de Turismo.

#### Acreditaciones de la Asociación Internacional de Transporte Aéreo (IATA) y la SNCF

En algunos casos, los operadores de viajes y viajes deben obtener una acreditación para poder reservar y emitir billetes de avión o ferrocarril. Para obtener más información, consulte la sección "Servicio de Acreditación" del[Sitio web de la IATA](http://www.iata.org/).

#### Reglamento general aplicable a todas las instituciones públicas (ERP)

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, es aconsejable consultar el listado[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte la hoja de actividades "Formalidad de informar de una empresa comercial".

### b. Autorización posterior al registro

#### Requisito para registrar a los operadores de viajes y estancias

Para llevar a cabo legalmente la actividad de un operador de viajes y residencia, el interesado debe solicitar en primer lugar el registro en el registro de operadores de viajes y vacaciones.

**Es bueno saber**

La inscripción para los operadores de viajes y viajes debe renovarse cada tres años.

**Autoridad competente**

La solicitud de registro debe ser atendida [en línea](https://registre-operateurs-de-voyages.atout-france.fr/web/rovs/creation_compte#https://registre-operateurs-de-voyages.atout-france.fr/immatriculation/creationCompte?3) a la comisión de registro de Atout France - Agencia Francesa de Desarrollo Turístico.

**Documentos de apoyo**

La solicitud de registro debe ir acompañada de los siguientes documentos:

- certificado de seguro de responsabilidad civil profesional, de acuerdo con el [modelo proporcionado por Trump-Francia](https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=b645d64b-4c68-4da2-9a98-76e03c0cd083&groupId=10157) ;
- certificado de garantía financiera, de acuerdo con el [modelo proporcionado por Trump-Francia](%5b*https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=4387c0c1-8977-44f6-a797-eb0cedfa4172&groupId=10157*%5d(https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=4387c0c1-8977-44f6-a797-eb0cedfa4172&groupId=10157)).

**hora**

La comisión de registro de Atout-France envía, por correo electrónico, al solicitante un recibo que acredite la integridad de su expediente. A partir de la fecha de recepción, la comisión tiene un mes para:

- deciden registrar al solicitante. En este caso, se envía al solicitante un certificado de registro con el número de registro y la fecha de registro por correo electrónico;
- o negarse a registrar al solicitante. En este caso, esta decisión motivada debe dirigirse a ella por carta recomendada con notificación de recepción.

Vale la pena aceptar el registro si la comisión no toma una decisión en el plazo de un mes a partir de la recepción.

**Costo**

No hay más cuotas de inscripción.

#### Renovación del registro en el registro de operadores de viajes y viajes

El registro tiene una validez de tres años, su renovación está sujeta a las mismas condiciones que el registro.

El operador recibe, tres meses antes del final de la validez de su registro, un relanzamiento por correo electrónico y correo electrónico invitándolo a continuar con la solicitud de renovación.

**Autoridad competente**

La solicitud de renovación debe enviarse a la comisión de registro de Atout-France antes de la fecha de finalización del registro.

**Condiciones de renovación**

El operador debe solicitar la renovación al menos 30 días antes de la fecha de vencimiento.

**Documentos de apoyo**

- Certificado de garantía financiera válido
- certificado de seguro de responsabilidad civil profesional válido.

**Costo**

No hay más tarifas de renovación.

#### Cambios en los datos de archivo

El operador está obligado a informar a la junta de registro de cualquier cambio en los artículos incautados durante la solicitud de registro. Los cambios pueden estar relacionados con la forma jurídica de la empresa, la dirección, el garante, etc.

**Cómo cambiar**

El cambio debe ser notificado en el plazo de un mes a partir de la modificación cuando pueda ser anticipado o, en su defecto, dentro de un mes del evento a más tardar.

**Autoridad competente**

La modificación debe ser hecha a disposición de la junta de registro.

Los cambios validados por esta comisión dan como resultado una actualización de la hoja de descripción del operador de viaje y de viajes. Su fecha de actualización es auténtica y sirve de información tanto para el público como para otras autoridades pertinentes.

Para más información, es aconsejable consultar el [Sitio web de Atout-France](https://registre-operateurs-de-voyages.atout-france.fr/web/rovs/espace-professionnel#https://registre-operateurs-de-voyages.atout-france.fr/immatriculation/rechercheMenu?0).

### c. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades puntuales (Entrega gratuita de servicios)

Todo nacional de un Estado de la UE o del EEE legalmente establecido en uno de estos Estados para llevar a cabo la actividad de operador de viajes y residencia podrá llevar a cabo esta actividad en Francia de forma temporal y ocasional, siempre que la actividad se realice antes de cualquier actuación.

**Qué saber**

La declaración anticipada lleva de forma automática y temporal el registro de la persona en el registro de operadores de viajes.

La declaración previa, como parte de una prestación gratuita de servicios, debe ser:

- actualizado durante el año, en caso de un cambio en los elementos componentes;
- renovación una vez al año, si la persona tiene la intención de prestar servicios de forma temporal y ocasional durante el año en cuestión.

**Autoridad competente**

La declaración previa se dirige, por cualquier medio, a la comisión de registro dentro de Trump Francia.

**Tiempo de respuesta**

La primera declaración anticipada es objeto de un acuse de recibo al solicitante de registro.

**Documentos de apoyo**

La declaración deberá ir acompañada de los siguientes documentos, traducidos al francés, si procede:

- Un certificado que certifique que el solicitante de registro está legalmente establecido en un Estado de la UE o del EEE para llevar a cabo su actividad allí;
- un certificado de garantía financiera suficiente emitido por una de las estructuras autorizadas mencionadas en el artículo R. 211-26 del Código de Turismo (véase supra "2." c. Garantía financiera);
- información sobre el estado de cobertura del solicitante de registro mediante un seguro que garantice las consecuencias monetarias de la responsabilidad civil profesional.

*Para ir más allá* Artículos L. 211-20 a L. 211-22, R. 211-26, R. 211-51 y siguientes del Código de Turismo.

