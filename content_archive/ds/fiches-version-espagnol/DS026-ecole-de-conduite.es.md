﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS026" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseñanza" -->
<!-- var(title)="Escuela de conducción" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="ensenanza" -->
<!-- var(title-short)="escuela-de-conduccion" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/ensenanza/escuela-de-conduccion.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="escuela-de-conduccion" -->
<!-- var(translation)="Auto" -->


Escuela de conducción
=====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La escuela de conducción es una institución educativa de conducción y seguridad vial. Su operador se encarga de su gestión administrativa, legal y tributaria.

Recluta y coordina la actividad de los profesores de conducción y seguridad vial ("instructor de la escuela de automóviles"). El operador no es necesariamente un instructor de conducción, pero puede ser.

De conformidad con lo dispuesto en la sección D. 114-12 del Código de Relaciones Públicas y de la Administración, cualquier usuario podrá obtener un certificado informativo sobre las normas aplicables a las profesiones relacionadas con la enseñanza de la conducción costosa y la concienciación sobre la seguridad vial. Para ello, debe enviar la solicitud a la siguiente dirección: bfper-dsr@interieur.gouv.fr.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para una profesión liberal, la CFE competente es el Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El funcionamiento de una escuela de conducción está reservado a los titulares de la acreditación de la prefectura.

Este último se obtiene si el interesado cumple las siguientes condiciones:

- condiciones de honor (véase infra "2. c. Condiciones de honorabilidad");
- Tener al menos 23 años de edad
- la capacidad de administrar una institución al poseer:- un título estatal, un título superior o tecnológico o un título al menos igual a un nivel III (legal, económico, contable o comercial) o un diploma extranjero comparable,
  - Certificado de Cualificación Profesional (CQP) "responsable de las unidades de educación en seguridad vial y conducción"
  - una cualificación profesional adquirida en un Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE);
- garantías mínimas con respecto a los medios de formación de la institución. Estas garantías abarcan locales, vehículos, medios materiales y cómo se organiza la formación;
- para justificar la cualificación profesional de los profesores que deben tener licencia para ejercer.

*Para ir más allá* Artículos L. 213-1 y R. 213-2 de la Ley de Tráfico de Carreteras; 13 de septiembre de 2017 decreto sobre el reconocimiento de cualificaciones adquiridas en un Estado miembro de la Unión Europea o en otro Estado parte en el Acuerdo sobre el Espacio Económico Europeo por personas que deseen ejercer en las profesiones educación vial regulada

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Para un ejercicio temporal e informal (LPS)

No existe ninguna normativa para un nacional de la UE o del EEE que desee ejercer como operador de escuela de conducción en Francia, ya sea de forma temporal o ocasional.

Por lo tanto, sólo las medidas adoptadas para el libre establecimiento de nacionales de la UE o del EEE (véase infra "3o. c. Solicitar una aprobación de la prefectura") encontrará que se aplicará.

#### Para un ejercicio permanente (LE)

Cualquier nacional de un Estado de la UE o del EEE que esté establecido y opere legalmente una escuela de conducción en ese estado podrá llevar a cabo la misma actividad en Francia de forma permanente si:

- posee un certificado expedido por la autoridad competente de un Estado de la UE o del EEE que regula el acceso o el ejercicio de la profesión;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

Una vez que el nacional cumpla una de estas condiciones, primero deberá solicitar el reconocimiento de sus cualificaciones profesionales por parte del prefecto del departamento donde tiene previsto abrir su establecimiento (véase infra "3o). b. Solicitar el reconocimiento de las cualificaciones profesionales de la UE o del EEE para un ejercicio permanente (LE) ").

El reconocimiento de sus cualificaciones profesionales le permitirá solicitar la aprobación administrativa de la misma autoridad competente (véase infra "3o). c. Solicitar aprobación de la prefectura").

Sin embargo, si el examen de las cualificaciones profesionales atestiguadas por las credenciales de formación y la experiencia laboral muestra diferencias sustanciales con las cualificaciones necesarias para el acceso a la profesión y su ejercicio en Francia, el interesado tendrá que someterse a una medida de indemnización.

*Para ir más allá* Artículo R. 213-2-1 de la Ley de Tráfico de Carreteras; 13 de septiembre de 2017 decreto sobre el reconocimiento de cualificaciones adquiridas en un Estado miembro de la Unión Europea o en otro Estado parte en el Acuerdo sobre el Espacio Económico Europeo por personas que deseen ejercer en las profesiones educación vial regulada.

### c. Condiciones de honorabilidad

El operador de una escuela de conducción debe cumplir con las condiciones de honor y justificar:

- no haber sido condenado a una sentencia penal o correccional en virtud de la sección R. 212-4 de la Ley de Tráfico por Carretera;
- no han sido objeto de una retirada de la licencia en los últimos tres años.

**Tenga en cuenta que**

La explotación de una escuela de conducción sin aprobación administrativa se castiga con un año de prisión y una multa de 15.000 euros.

*Para ir más allá* Artículos L. 213-3 y L. 213-6 y R. 212-4 de la Ley de Tráfico de Carreteras.

### d. Algunas peculiaridades de la regulación de la actividad

#### Justificar la cualificación de los profesores

Los profesores de conducción y seguridad vial deben tener licencia para enseñar siempre y cuando participen en la educación teórica y práctica.

El permiso se expide al profesional que justifica:

- Tener una licencia de conducir válida y un período de prueba expirado, válido para las categorías de vehículos que desea enseñar a conducir;
- poseer un título o diploma como profesor de conducción y seguridad vial o, dependiendo de las condiciones, estar en formación para su preparación. En este último caso, la autorización se expedirá de forma temporal y restrictiva;
- En su caso, el reconocimiento de las cualificaciones profesionales para el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE);
- Tener al menos 20 años de edad
- cumplir con los requisitos de habilidades físicas, cognitivas y sensoriales requeridos para las licencias de conducir en las categorías C1, C, D1, D, C1E, CE, D1E y DE. Para ello, la persona tendrá que obtener el consejo de un oficial médico.

*Para ir más allá* Artículo L. 212-2 de la Ley de Tráfico de Carreteras.

#### Cumplimiento de las normas de seguridad y accesibilidad

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

Es aconsejable referirse a la lista[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

#### Obligación de contrato de seguro de responsabilidad civil profesional

El operador está obligado a realizar un seguro de responsabilidad profesional que lo cubra a él y a sus empleados por cualquier daño causado a terceros por actos realizados durante su actividad profesional.

Además, también se requerirá asegurar los vehículos en caso de daños causados por los estudiantes de la escuela a terceros durante la instrucción de conducción.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El contratista debe registrarse en el Registro Mercantil y Corporativo (SCN). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Solicitar el reconocimiento de las cualificaciones profesionales de los nacionales de la UE o del EEE para el ejercicio permanente (LE)

**Autoridad competente**

El prefecto del departamento en el que el nacional desee establecer es competente para decidir sobre la solicitud de reconocimiento de cualificaciones profesionales.

**Documentos de apoyo**

La solicitud se realiza mediante la presentación de un archivo que incluye los siguientes documentos justificativos:

- La solicitud de reconocimiento, fechada y firmada;
- Una fotocopia de un documento de identidad
- Una fotografía de identidad de menos de seis meses
- prueba de residencia de menos de seis meses
- Fotocopia de certificados de competencia o documentos de formación obtenidos para el ejercicio de la actividad de explotación de una escuela de conducción;
- Una declaración sobre su conocimiento de la lengua francesa para el ejercicio de la profesión de que se trate;
- en su caso, un certificado de competencia o un documento de formación que justifique que el nacional ha estado activo durante al menos un año en los últimos diez años en un estado que no regula la formación o su ejercicio.

**Procedimiento**

El prefecto reconoce la recepción del expediente en el plazo de un mes y dispone de dos meses a partir de esa misma fecha para decidir sobre la solicitud.

En caso de diferencias sustanciales entre la formación que ha recibido y la necesaria para llevar a cabo esta actividad en Francia, el prefecto podrá someterlo a una prueba de aptitud o a un curso de adaptación.

La prueba de aptitud, realizada en un plazo máximo de seis meses, es en forma de entrevista ante el jurado del certificado de cualificación profesional "responsable de la enseñanza de la seguridad vial y la conducción". El curso de adaptación se lleva a cabo durante un mínimo de dos meses en una escuela de conducción acreditada.

Después de comprobar los documentos en el expediente y, si es necesario, después de recibir los resultados de la medida de compensación elegida, el prefecto emitirá el reconocimiento de cualificaciones cuyo modelo se especifique en el[Anexo](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=D3225762F0A0C9E9EA272A874A63EF2F.tplgfr42s_2?cidTexte=JORFTEXT000035589366&dateTexte=20170920) 13 de septiembre de 2017.

*Para ir más allá* : decreto de 13 de septiembre de 2017 relativo al reconocimiento de cualificaciones adquiridas en un Estado miembro de la Unión Europea o en otro Estado parte en el acuerdo sobre el Espacio Económico Europeo por personas que deseen ejercer las profesiones educación vial regulada.

### c. Solicitar aprobación de la prefectura

**Autoridad competente**

El prefecto del departamento del lugar de la escuela de conducción es competente para decidir sobre la solicitud de acreditación del nacional.

**Documentos de apoyo**

La solicitud se realiza mediante la presentación de un archivo que incluye los siguientes documentos justificativos:

- El formulario de acreditación que se obtendrá de la prefectura del departamento;
- Una identificación válida
- Comprobante de residencia
- Una fotografía de identidad
- Un documento que justifica la capacidad de administrar una institución
- la justificación de la declaración de la contribución económica territorial o, en su defecto, de una declaración de registro ante el Urssaf;
- El nombre y la calidad del establecimiento (razón social, número de sirena o Siret, etc.);
- Fotocopia del título o arrendamiento de los locales
- El plan y una descripción de la sala de actividades
- la justificación de la propiedad o alquiler del vehículo o vehículos docentes, así como el certificado de seguro que cubre los daños causados a terceros con estos vehículos;
- lista de profesores de conducción y seguridad vial con su permiso para enseñar.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

Al recibir el expediente, el prefecto tendrá un mes para informar al profesional de cualquier documento que falte. La acreditación se emite por cinco años renovables. En caso de renovación, el profesional debe aplicar al menos dos meses antes de que expire.

*Para ir más allá* : orden de 8 de enero de 2001 relativa al funcionamiento de las instituciones educativas, por una tasa, de conducción de vehículos de motor y seguridad vial.

### d. Autorización (s) postinstalación

#### Si es necesario, obtenga permiso para enseñar

**Autoridad competente**

El prefecto del departamento donde reside el profesor o aquel en el que desea ejercer (para los solicitantes residentes en el extranjero) es competente para expedir la autorización de enseñanza.

**Documentos de apoyo**

El profesional debe adjuntar a la solicitud los siguientes documentos justificativos:

- La solicitud de permiso de enseñanza cumplimentada, fechada y firmada;
- Dos identificaciones fotográficas
- Una fotocopia de un documento de identidad
- si es un extranjero que no pertenezca a un Estado de la Unión Europea o sea parte en el acuerdo sobre el Espacio Económico Europeo, la justificación de que está en buena posición con respecto a la legislación y los reglamentos relativos a los extranjeros en Francia;
- Comprobante de residencia
- Fotocopia de certificados de competencia o documentos de formación obtenidos para el ejercicio de la actividad docente de conducción;
- Si es necesario, el reconocimiento de las cualificaciones profesionales;
- un certificado médico emitido por un médico con licencia.

La autorización se expide por un período renovable de cinco años. Dos meses antes de su expiración, el profesional debe solicitar la renovación presentando los mismos documentos justificativos al prefecto del departamento.

*Para ir más allá* Artículos R. 212-1 y siguientes de la Ley de Tráfico de Carreteras; orden de 8 de enero de 2001 relativa a la autorización para enseñar, por una tasa, la conducción de vehículos de motor y la seguridad vial.

**Autorización temporal y restrictiva para enseñar**

Se concede una autorización temporal y restrictiva a la persona que se dice que está en proceso de formación en la profesión de conducción. El prefecto del departamento donde se encuentra la escuela de conducción con la que el solicitante tiene previsto ejercer es competente para expedir la autorización.

Además de cumplir las condiciones anteriores, también tendrá que:

- Haber firmado un contrato de trabajo con una institución de educación de conducción y seguridad vial con licencia;
- Poseer uno de los certificados de competencia profesional que componen la designación profesional expedida por el Ministro de Empleo;
- estar inscritos en una sesión de examen para completar la validación de las habilidades necesarias para obtener el título de profesor de conducción y seguridad vial.

**Qué saber**

Esta autorización sólo se concede por doce meses.

*Para ir más allá* Artículo R. 212-2 I bis de la Ley de Tráfico de Carreteras y auto de 13 de abril de 2016 relativo a la autorización temporal y restrictiva de la práctica contemplada en el artículo R. 212-1 de la Ley de Tráfico de Carreteras.

