﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS037" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseñanza" -->
<!-- var(title)="Educación privada" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="ensenanza" -->
<!-- var(title-short)="educacion-privada" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/ensenanza/educacion-privada.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="educacion-privada" -->
<!-- var(translation)="Auto" -->


Educación privada
=================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

Una escuela privada tiene como objetivo proporcionar una educación común a un cierto número de alumnos para la educación primaria, secundaria, técnica o superior.

**Tenga en cuenta que**

El caso de las instituciones educativas privadas bajo contrato con el estado no se tratará aquí.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para una actividad liberal, es el Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Las cualificaciones profesionales de la persona que desea abrir una escuela privada difieren según el nivel de educación.

En el caso de la educación primaria, el interesado debe poseer un certificado de capacidad de educación primaria.

Cuando decida abrir una escuela secundaria, tendrá que:

- Tener al menos 25 años de edad
- justifican un certificado de prácticas que indique que ha servido durante al menos cinco años como profesor o supervisor en una institución educativa pública o privada de segundo grado de un Estado miembro de la Unión Europea (UE) o una parte en el espacio Indice Económico Económico Europeo (EEE);
- tener una licenciatura, una licenciatura, una licenciatura o un certificado de escuela secundaria.

En el caso de las instituciones de educación técnica, el solicitante deberá tener:

- un certificado de pasantía que indique que ha servido durante al menos cinco años como profesor o supervisor en una institución pública o privada de segundo grado;
- un título que le da derecho a solicitar un trabajo como profesor de educación técnica general o teórica en una escuela pública dando enseñanzas del mismo nivel que la que se solicita para ser abierta;
- un título de ingeniería en una lista elaborada en virtud del Decreto 56-931 de 14 de septiembre de 1956.

Por último, la apertura de una institución de educación superior requiere que el solicitante:

- tener al menos 25 años de edad;
- justifica los requisitos para las profesiones de médico o farmacéutico o cirujano dental o partera al proponer la enseñanza de la medicina, farmacia, odontología y maeuética.

*Para ir más allá* Artículo L. 914-3 del Código de Educación.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

El nacional de un Estado de la UE o del EEE está sujeto a las mismas condiciones de cualificación profesional que los franceses.

### c. Honorabilidad e incompatibilidad

Cualquier persona interesada en abrir una escuela primaria, secundaria o técnica privada no debe:

- han sido condenados por un delito felonario o menor contrario a la probidad y a la moral;
- han sido privados por juicio de la totalidad o parte de los derechos civiles, civiles y familiares, o han sido despojados de la patria potestad;
- se les ha prohibido permanentemente la enseñanza;
- han sido retirados de una institución educativa pública o privada de segundo grado.

Para abrir una escuela primaria privada, el profesional debe tener al menos 21 años de edad, en comparación con 25 años para una institución de educación secundaria, técnica y superior.

*Para ir más allá* Artículo L.911-5 del Código de Educación.

### d. Algunas peculiaridades de la regulación de la actividad

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Dado que las instalaciones están abiertas al público, el profesional debe cumplir con las normas relativas a las instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de la empresa, el contratista debe registrarse en el Registro de Comercio y Sociedades (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual con el RCS" para obtener más información.

### b. Hacer una declaración de la creación de una institución educativa privada

Antes del 15 de abril de 2018, había tres planes específicos para las escuelas primarias, secundarias y técnicas. Una ley del 13 de abril de 2018, que entró en vigor el 15 de abril de 2018, fusionó estos tres regímenes. Desde entonces, ha existido un régimen común para la apertura de estas tres instituciones y un régimen especial para la apertura de una institución de educación superior.

#### El esquema común para la apertura de una escuela primaria, secundaria y técnica

**La declaración previa de la apertura de una escuela primaria, secundaria y técnica**

Cualquier persona que cumpla las condiciones de capacidad (véase el artículo L.911-5 del Código de Educación) y la nacionalidad (nacionalidad francesa o nacional de la UE u otro Estado parte en el Acuerdo sobre el EEE) puede abrir una escuela primaria, secundaria o técnica.

La creación de tales instituciones está sujeta a la declaración previa a la autoridad competente en materia de educación, el rector de la academia. Este último se lo transmitirá al alcalde de la comuna en la que se encuentra el establecimiento, al prefecto de departamento y al fiscal de la República.

Sin embargo, una oposición a esta apertura puede ser formada por el rector de la academia, el alcalde, el representante estatal en el departamento y el fiscal de la República por diversas razones.

*Para ir más allá* Artículo L. 441-1 del Código de Educación.

**El contenido del archivo de declaración de apertura**

Para la persona o individuos que declaran la apertura y ejecución de la institución, el archivo debe incluir:

- una declaración:- mencionando el deseo de abrir y dirigir una escuela amigable con los estudiantes, y presentando el propósito de la enseñanza que debe estar en línea con el umbral común,
  - especificando la edad de los estudiantes y, en su caso, los diplomas o trabajos a los que la escuela los preparará,
  - especificando los horarios,
  - especificar las disciplinas si la institución se está preparando para los títulos de educación técnica;
- documentos que acrediten su identidad, edad y nacionalidad;
- La tarjeta de antecedentes penales original data de menos de tres meses en el momento del archivo;
- todos los documentos que atestiguan que la persona que abre el establecimiento y, en caso afirmativo, la persona que lo ejecutará:- es capaz (véase el artículo L.911-5 del Código de Educación),
  - es nacional francés o nacional de un Estado miembro de la UE o un Estado Parte en el Acuerdo EEE,
  - ha ocupado puestos de liderazgo, enseñanza o supervisión en una escuela pública o privada de un Estado miembro de la UE u otro Estado parte en el Acuerdo EEE durante al menos cinco años;

Por lo que se refiere al establecimiento, será necesario transmitir:

- El mapa de las instalaciones y, en su caso, cualquier terreno destinado a recibir a los estudiantes, indicando al menos el tamaño de cada superficie y su destino;
- Sus acuerdos de financiación;
- si es necesario, la certificación de la solicitud de autorización (véase el artículo L.111-8 del Código de La Construcción y vivienda).

Si el solicitante de registro es una persona jurídica, también tendrá que aprobar sus estatutos.

**Tenga en cuenta que**

El rector de la academia le da un reconocimiento a esta persona.

El rector de la academia le dice a la persona que el archivo está incompleto en el acuse de recibo, en un plazo máximo de quince días después de su emisión. Al mismo tiempo que da la indicación de que el expediente está incompleto y de que recibe los documentos requeridos, el rector de la academia lo transmite al alcalde, al prefecto de departamento y al fiscal de la República.

La misma declaración debe hacerse en caso de un cambio de local o la admisión de estudiantes internos.

La apertura de una escuela privada a pesar de la oposición de las autoridades competentes o sin cumplir las condiciones del artículo L. 441-4 del Código de Educación se castiga con una multa de 15.000 euros y el cierre de Establecimiento.

**Instituciones privadas de educación superior**

Cuando una asociación desea abrir una institución de educación superior, debe hacer la declaración al rector, al prefecto del departamento y al fiscal general del tribunal de la jurisdicción o al fiscal de la República.

Cuando una persona física desea abrir una institución de educación superior, debe hacer la declaración al rector en los departamentos donde está establecida la capital de la academia y a la autoridad del estado responsable de la materia educación en otros departamentos. Debe ser nacional francesa o nacional de un Estado miembro de la UE o un Estado parte en el Acuerdo EEE

**Tenga en cuenta que**

Una institución de educación superior debe ser administrada por al menos tres personas.

La instrucción de apertura se envía mediante el envío de un archivo que incluye:

- Un ID válido para todos los directores;
- Un CV
- Una copia de los diplomas
- un extracto del historial delictivo de menos de tres meses;
- si es necesario, los nombres, profesiones y residencias de los fundadores y directores y una copia de los estatutos de la asociación.

En caso de oposición a la apertura, el fiscal decidirá en un plazo de diez días notificar su decisión de negarse. Por lo tanto, el solicitante podrá presentar un recurso de casación dentro de una quincena de horas de notificación.

*Para ir más allá* Artículos L. 731-1 a L. 731-18 del Código de Educación.

### d. Autorización (s) postinstalación

**Declaración de apertura de cada curso de educación superior privada**

La apertura de un curso en una institución privada de educación superior debe estar sujeta a una declaración previa que incluya:

- El nombre, calidad y residencia del director de la asociación;
- Las instalaciones donde se llevan a cabo las clases;
- El propósito de la enseñanza
- Una fotocopia de un documento de identidad válido
- El currículum de un estudiante
- Una copia de los diplomas
- un extracto del boletín 3 del historial penal.

Esta declaración debe ser remitida al rector de la academia, así como a la dirección académica de los servicios educativos nacionales que actúan en el rector de la academia. Una vez recibido, el curso puede abrirse al menos 10 días después.

Cualquier cambio en los componentes de la declaración debe actualizarse.

*Para ir más allá* Artículos L. 731-3 y L. 731-4 del Código de Educación.

