﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->



<!-- var(key)="DS106" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Eliminación de amianto (eliminación o encapsulación del amianto)" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="eliminacion-de-amianto-eliminacion" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/eliminacion-de-amianto-eliminacion-o-encapsulacion-del-amianto.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="eliminacion-de-amianto-eliminacion-o-encapsulacion-del-amianto" -->
<!-- var(translation)="Auto" -->







Eliminación de amianto (eliminación o encapsulación del amianto)
================================================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición - naturaleza del trabajo

El trabajo de eliminación o encapsulación de amianto (comúnmente conocido como eliminación de amianto o tratamiento del amianto) es un trabajo que puede causar la emisión de fibras de amianto y, posteriormente, tratar el amianto o el amianto material que contiene amianto (MCA), es decir, la gestión del amianto, tal como se define en el Código de Salud Pública, ya sea por almacenamiento en una instalación adaptada, por vitrificación o por superposición total e estanca. Por lo tanto, el concepto de retirada debe interpretarse, no en el sentido físico o literal del término, sino en el sentido jurídico de la acción de tratamiento del material, desde su gestión hasta su eliminación definitiva.

Por lo tanto, la empresa de remoción o encapsulación de amianto es una empresa cuya actividad consiste en retirar o encapsular el amianto y los materiales, equipos y materiales o artículos que contengan, incluso en casos de demolición (artículo R. 4412-94/1) del Código de Trabajo).

Para ello, estas empresas deben:

- capacitar a sus trabajadores por una organización de formación certificada (artículo R. 4412-141 del Código de Trabajo), de acuerdo con los términos definidos por el decreto de 23 de febrero de 2012 que define las modalidades de formación de los trabajadores en la prevención de Riesgos relacionados con el amianto
- ser certificado por un organismo acreditado, de acuerdo con los términos definidos por los artículos R. 4412-129 a 131 del Código de Trabajo y las disposiciones de la orden modificada del 14 de diciembre de 2012 por la que se establecen los requisitos de certificación para las empresas que realizan remoción o encapsulación de amianto, materiales, equipos o artículos que lo contengan.

El contratista de una operación de remoción de amianto debe garantizar en primer lugar la competencia de la empresa a la que tiene previsto confiar esta obra (artículo R. 4412-129 del Código de Trabajo), que debe justificarse por la presentación del certificado. emitido por el organismo certificador.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad.

La actividad de eliminación de amianto es de carácter comercial, por lo que la CFE pertinente es la Cámara de Comercio e Industria (CCI).

Como parte del proceso de certificación, las empresas de tratamiento del amianto justifican la legalidad de su existencia mediante la producción del extracto de Kbis vinculado a su registro en el Registro de Comercio y Sociedades (RCS).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal.

2°. Condiciones para el ejercicio de la eliminación de amianto o la actividad encapsulante
--------------------------------------------------------------------------------------------------

Para llevar a cabo la actividad de tratamiento del amianto, la empresa debe cumplir, de conformidad con lo dispuesto en el artículo 1 de la orden modificada de 14 de diciembre de 2012, los requisitos establecidos en la norma NF X 46-010: agosto de 2012 "Trabajo de tramitación de Asbestos - Repositorio técnico para la certificación empresarial - Requisitos generales."

Esta norma obligatoria, disponible gratuitamente en el sitio web de AFNOR, establece los requisitos y criterios de evaluación asociados:

- administrativo, jurídico y económico;
- Organización
- Técnicas
- personal asignado a actividades de tratamiento del amianto;
- operaciones de tratamiento de amianto.

### a. Cualificaciones profesionales

No se permite un título profesional, título o experiencia para llevar a cabo el trabajo de amianto de amianto.

Sin embargo, la empresa justifica en el proceso de certificación de su actividad, un número suficiente de personas mayores de 18 años y titulares de un contrato indeterminado, lo que le permite asegurar por sus propios medios la operaciones de tratamiento de amianto dependiendo del tamaño o la naturaleza de las obras. Este personal tiene las siguientes habilidades: supervisión técnica, gestión de la construcción u operador de construcción.

Para ser asignado a actividades de tratamiento del amianto, el trabajador debe:

- tener las habilidades para llevar a cabo su trabajo (supervisión técnica, gestión de la construcción, operador de construcción);
- estar capacitados en la prevención de riesgos relacionados con el amianto de acuerdo con los términos definidos por el decreto de 23 de febrero de 2012, por una organización de formación (OF) certificada por un organismo certificador (OC) acreditado por el organismo de acreditación.

A partir del 1 de octubre de 2019, 31 OF fueron certificados como tales por los 3 OC:[Certibat](https://www.certibat.fr/entreprises/),[I.Cert](https://www.icert.fr/liste-des-certifies/?recherche=&dyn_certification=632&departement=tous) Y[Certificación GLOBAL](https://www.global-certification.fr/images/CERTIFICATION/AMIANTE/OF-AMIANTE/of-amiante-liste-clients-maj-06-08-2018.pdf).

### b. Cualificaciones profesionales - Nacionales europeos y no pertenecientes a la UE

Toda empresa europea o no perteneciente a la UE, siempre que intervenga en Francia, deberá aplicar la normativa francesa sobre la salud y la seguridad de los trabajadores de conformidad con el artículo L. 1262-4 9o del Código de Trabajo.

En consecuencia, una empresa con sede en otro Estado miembro o fuera de la Unión Europea que desee realizar trabajos en territorio francés para eliminar o encapsular materiales de amianto debe cumplir con la normativa francesa y Normas francesas.

Esta empresa, que también puede poseer una certificación en su país de origen, debe aportar pruebas de la equivalencia con el sistema francés de sus medidas de formación, prevención, sus obligaciones documentales y el procedimiento certificación de su país de origen (artículos R. 4412-132 y R. 4412-141 del Código del Trabajo).

Estas empresas establecidas fuera de Francia pueden desvincular al personal para llevar a cabo un servicio temporal. El uso de la prestación de servicios debe cumplir las normas de cesión del código de trabajo establecidas en las secciones L. 1262-1 y lo siguiente del Código de Trabajo: declaración previa, prestación temporal, vínculo preexistente entre el empleado y su actividad no permanente en Francia, el respeto del núcleo de las regulaciones laborales, etc.

Los empleadores que separan a los empleados en Francia están obligados a hacer una declaración previa de asignación a la inspección del trabajo del lugar donde comienza la prestación. Tele-informes,[disponible en el sitio web del SIPSI](https://www.sipsi.travail.gouv.fr/), es obligatorio desde octubre de 2016.

*Para ir más allá* Artículo R. 1263-4-1 y Artículo R. 1263-5 del Código de Trabajo.

Un empleador no podrá acogerse a las disposiciones aplicables a la asignación de trabajadores cuando realice, en el Estado en el que esté establecido, actividades únicamente relacionadas con la gestión interna o administrativa, o cuando su actividad se lleve a cabo en nacional de manera normal, estable y continua.

Mientras la actividad en territorio francés sea permanente, la empresa extranjera debe crear una escuela secundaria, que está sujeta a la normativa francesa como cuestión de derecho.

*Para ir más allá* Artículos L. 1262-1 y L. 1262-3 del Código de Trabajo

#### Condiciones de honor y sanciones penales

El profesional está obligado por las reglas de ética específicas de cada organismo certificador. Para obtener más información, es aconsejable visitar el sitio web de estas organizaciones:

- [Certificación AFNOR](https://certification.afnor.org/gestion-des-risques-sst/traitement-de-l-amiante) ;
- [Certificación GLOBAL](https://www.global-certification.fr/certification/amiante/amiante-entreprise) ;
- [Qualibat](https://www.qualibat.com/resultat-de-la-recherche/).

#### Control y sanciones

La empresa de remoción o encapsulación de amianto, como cualquier empleador o su delegado que haya cometido violaciones de las normas de salud y seguridad, incurre en un proceso penal castigado con una multa de 10.000 euros aplicable tantas veces como haya trabajadores de la empresa involucrados.

*Para ir más allá* Artículo L. 4741-1 del Código de Trabajo.

Por otra parte, cuando un oficial de control de la inspección del trabajo encuentra una situación de peligro grave e inminente resultante de la ausencia de dispositivos de protección para evitar los riesgos asociados con el trabajo de tratamiento del amianto, puede una orden temporal de detención del trabajo para eliminar a los trabajadores expuestos.

*Para ir más lejos:* Artículo L. 4731-1 del Código de Trabajo.

### c. Seguro de responsabilidad civil profesional

Como parte del proceso de certificación de su negocio de tratamiento del amianto, la empresa justifica que adopte las medidas adecuadas para cubrir su responsabilidad civil por el tratamiento del amianto a través de un seguro. 4.2 de la norma NF X 46-010 antes mencionada.

### d. Algunas peculiaridades de la regulación de la actividad

Las empresas de tratamiento de amianto deben estar certificadas por una propia organización acreditada por el Cofrac (organismo nacional de acreditación), después de su evaluación de su cumplimiento de los requisitos establecidos por NF EN ISO/CIS 17065 - Evaluación de Cumplimiento - Requisitos para organizaciones que certifican productos, procesos y servicios.

Los organismos de certificación (OC) acreditados hasta la fecha por el Cofrac son la certificación AFNOR, la certificación GLOBAL y Qualibat. Evalúan la capacidad de las empresas, que son candidatas a la certificación o ya titulares de estas últimas, para llevar a cabo trabajos de tratamiento del amianto de conformidad con los requisitos establecidos en el artículo 1 del auto de 14 de diciembre de 2012, I.e:

- las establecidas por la norma NF X 46-010 de agosto de 2012 ("Obras de procesamiento de amianto - Referencia técnica para la certificación empresarial - Requisitos generales");
- los establecidos por el NF X 46-011 de diciembre de 2014 ("El procesamiento del amianto funciona - cómo se otorgan y rastrean los certificados de las empresas");
- en el Código de Trabajo y que rigen las operaciones de amianto.

Para obtener más información, visite el sitio web de AFNOR:

- [NF X 46-010 estándar](https://www.boutique.afnor.org/norme/nf-x46-010/travaux-de-traitement-de-l-amiante-referentiel-technique-pour-la-certification-des-entreprises-exigences-generales/article/798037/fa170060) ;
- [ESTÁNDAR NF X 46-011](https://www.boutique.afnor.org/norme/nf-x46-011/travaux-de-traitement-de-l-amiante-modalites-d-attribution-et-de-suivi-des-certificats-des-entreprises/article/821097/fa059103).

3°. Procedimientos y formalidades para llevar a cabo la actividad
--------------------------------------------------------------------------

Después de evaluar la capacidad de la empresa para realizar el trabajo de acuerdo con los requisitos de la norma NF X 46-010 antes mencionada, el organismo certificador emite, cuando se cumple, un certificado en las condiciones establecidas en NF X 46-011: Diciembre 2014 "El procesamiento de amianto funciona - cómo se otorgan y rastrean los certificados de las empresas."

El proceso de certificación se lleva a cabo en cuatro etapas:

- Paso 0: Receivability (después de aprobar el examen documental);
- Paso 1: Pre-certificación (después de pasar con éxito la auditoría de asientos. La transición a la precertificación es confirmada por el órgano deciarte de la OC);
- Paso 2: Certificación de libertad condicional: dos años incompresibles con seguimiento anual (después de pasar con éxito la primera auditoría del sitio. La transición a la precertificación es confirmada por el órgano deciarte de la OC. Su mantenimiento durante el período de validez está condicionado por el paso exitoso de las operaciones de vigilancia);
- Paso 3: Certificación: cinco años con seguimiento anual (pasó con éxito la etapa de prueba y la evaluación del órgano de cizalladería de tres archivos de referencia seleccionados por la OC. La transición a la certificación es confirmada por el órgano deciserción de la OC. Su mantenimiento durante el período de validez está condicionado por el paso exitoso de las operaciones de vigilancia);
- Paso 4: Renovación de la certificación: cinco años renovables con seguimiento anual (operaciones de renovación completadas con éxito que incluyen una auditoría del sitio, una auditoría de la sede y una revisión de documentos. La renovación es confirmada por el órgano deciserción de la OC).

El OC puede tomar diferentes tipos de decisiones frente a las desviaciones de la empresa, incluyendo:

- suspensión o retirada
- alerta o emergencia en caso de un riesgo grave de amianto.

El OC también se encarga de las apelaciones de la empresa de las decisiones que la sancionan, las quejas o reclamaciones hechas por terceros.

El OC expide un certificado que menciona la fase de certificación y las actividades en las que (que) la empresa lleva a cabo su negocio de tratamiento del amianto.

El directorio de empresas certificadas, disponible para el público en los sitios de OC, también contiene esta información.

### 4°. Textos de referencia

Habida cuenta de los riesgos sanitarios y jurídicos derivados de la exposición a las fibras de amianto, la actividad de eliminación del amianto está altamente regulada por las siguientes disposiciones:

- Artículos R. 4412-94 a R. 4412-148 del Código de Trabajo y sus órdenes de ejecución:- Decreto de 23 de febrero de 2012 por el que se establecen las modalidades de formación de los trabajadores para prevenir los riesgos relacionados con el amianto,
  - Orden de 14 de agosto de 2012 modificada relativa a las condiciones de medición de los niveles de polvo, las condiciones de control del cumplimiento del límite de exposición ocupacional de las fibras de amianto y las condiciones de acreditación de los organismos medir estas mediciones,
  - El Decreto 14 de diciembre de 2012 modificó por el que se establecen las condiciones para la certificación de las empresas que realizan trabajos para eliminar o encapsular el amianto, los materiales, los equipos o los artículos que contienen,
  - Orden de 7 de marzo de 2013 relativa a la selección, mantenimiento y verificación de los equipos de protección personal utilizados en operaciones con riesgo de exposición al amianto,
  - Decreto 8 de abril de 2013 sobre normas técnicas, medidas preventivas y medidas de protección colectiva que deben aplicar las empresas en operaciones con riesgo de exposición al amianto;
- normas obligatorias por la orden modificada del 14 de diciembre de 2012:- NF X 46-010: Agosto 2012 "Trabajo de tratamiento de amianto - Referencia técnica para la certificación empresarial - Requisitos generales,"
  - NF X 46-011: Diciembre 2014 "El procesamiento de amianto funciona - Cómo se otorgan y rastrean los certificados de las empresas."

*Para ir más allá* El sitio web del Ministerio de Trabajo se actualiza[para prevenir los riesgos laborales asociados con el amianto](https://travail-emploi.gouv.fr/sante-au-travail/prevention-des-risques-pour-la-sante-au-travail/article/amiante). El sitio web de Direccte Pays-de-la-Loire ofrece[muchas herramientas para apoyar a las empresas de tratamiento del amianto](http://pays-de-la-loire.direccte.gouv.fr/amiante,3968), incluido un[guía para ayudar a las empresas de eliminación de amianto con certificación](http://pays-de-la-loire.direccte.gouv.fr/La-certification-pour-les-travaux-des-traitements-de-l-amiante).

