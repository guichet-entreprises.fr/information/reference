﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS015" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Guía-altavoz" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="guia-altavoz" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/guia-altavoz.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="guia-altavoz" -->
<!-- var(translation)="Auto" -->




Guía-altavoz
============

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El guía-profesor es un profesional cuya misión es garantizar visitas guiadas en francés o en lengua extranjera, en museos en Francia y en monumentos históricos. Su función es potenciar el patrimonio mediante el diseño de acciones de mediación cultural dirigidas al público en los territorios y lugares patrimoniales.

Aparte de la aplicación de la disposición legislativa del artículo L. 221-1 del Código de Turismo, los oficios de guía turístico, guía u operador turístico son de libre acceso en Francia y pueden ejercerse sin condiciones de diploma o título profesional para nacionales de la UE

*Para ir más allá* Artículo L. 221-1 del Código de Turismo.

### b. CFE competente

El centro de formalidades empresariales (CFE) correspondiente difiere de la actividad realizada por el interesado para crear una tienda:

- Si la actividad se lleva a cabo en una empresa individual: la competencia de la Urssaf;
- Si la actividad se lleva a cabo en forma de sociedad mercantil: jurisdicción de la Cámara de Comercio e Industria (CCI);
- si la actividad se lleva a cabo en forma de sociedad civil o de una sociedad ejercerla liberal: la competencia del registro del tribunal mercantil, o el registro del tribunal de distrito en los departamentos del Bajo Rin, el Alto Rin y el Mosela.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad de guía-profesor, el interesado debe justificar una tarjeta profesional expedida a los titulares de una certificación inscrita en el Directorio Nacional de Certificaciones Profesionales (RNCP).

La tarjeta se proporciona al titular:

- Licencia profesional como guía-profesor;
- máster con las siguientes unidades docentes:- habilidades de los profesores guía,
  - situación y práctica profesional,
  - lengua viva (distinta del francés);

**Tenga en cuenta que**

Estas unidades docentes deben haber sido validadas por el interesado. Prueba de su validación puede adoptar la forma de un certificado expedido por la institución educativa o un apéndice descriptivo adjunto al diploma.

- máster y justificación:- al menos un año de experiencia profesional acumulada en mediación de herencia oral en los últimos cinco años,
  - nivel C1 al marco común europeo de referencia para las lenguas ([CECRL](https://www.coe.int/T/DG4/Linguistic/Source/Framework_FR.pdf)) en una lengua viva extranjera, una lengua regional en Francia o lengua de signos francesa.

*Para ir más allá* Artículo 1 y apéndice de la[pedido a partir del 9 de noviembre de 2011](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024813610) en relación con las habilidades necesarias para expedir la tarjeta de guía-altavoz profesional a los titulares de una licencia profesional o diploma que confiere el título de máster.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (prestación gratuita de servicios o establecimiento libre)

**En caso de prestación gratuita de servicios (LPS)**

Todo nacional de un Estado miembro de la UE o del EEE establecido y que practique legalmente la actividad de portavoz guía en ese Estado podrá ejercer la misma actividad en Francia, de forma temporal y ocasional.

El nacional simplemente tendrá que indicar la mención del título profesional o el título de formación que ostente en ese estado en los documentos que presentará en Francia a las personas relacionadas con la recepción turística, así como a los funcionarios del museo o museo. monumento histórico visitado.

En el caso de que la profesión no esté regulada, ni en el curso de la actividad ni en el marco de la formación, en el estado en el que el profesional esté legalmente establecido, deberá haber realizado esta actividad durante al menos un año durante los diez años antes de la prestación en uno o más Estados de la UE o del EEE.

*Para ir más allá* Artículos L. 211-1, L. 221-3, L. 221-4 y R. 221-14 del Código de Turismo.

**En caso de establecimiento gratuito (LE)**

Un nacional de un Estado de la UE o del EEE establecido y que actúe legalmente como portaguías en uno de estos Estados podrá llevar a cabo la misma actividad en Francia de forma permanente.

Podrá solicitar la tarjeta profesional del prefecto, siempre y cuando justifique:

- poseer un diploma, certificado u otro título que permita el ejercicio de la actividad a título profesional en un Estado de la UE o del EEE y expedido por:- la autoridad competente de ese Estado,
  - un tercer Estado, con la certificación de un Estado de la UE o del EEE que reconozca el título y certifique que ha estado en el negocio durante un mínimo de tres años;
- Poseer un título de formación adquirido en su estado de origen, específicamente dirigido al ejercicio de la profesión;
- durante al menos un año, a tiempo completo o a tiempo parcial, en los últimos diez años en un Estado de la UE o del EEE que no regula el acceso o el ejercicio de esta actividad.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación en Francia, el prefecto competente podrá exigir que el interesado se someta a una medida de compensación.

*Para ir más allá* Artículos L.221-2, R. 221-12 y R. 221-13 del Código de Turismo.

### c. Condiciones de honorabilidad e incompatibilidad

Cualquier persona que trabaje como guía-profesor sin tener una tarjeta profesional es multada con una multa de hasta 450 euros.

*Para ir más allá* Artículo R. 221-3 del Código de Turismo y artículo 131-13 del Código Penal.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte la "Formalidad de la presentación de informes de una empresa comercial", "Registro de negocios individuales en el Registro de Comercio y Empresas" y "Formalidades de Informes Corporativos. trabajo artesanal."

### b. Solicitar una tarjeta de visita

El interesado que desee ejercer como guía-altavoz debe solicitar una tarjeta profesional.

**Autoridad competente**

El prefecto del departamento en el que el profesional desea establecerse es competente para emitir la tarjeta de guía-altavoz profesional. El nacional de un Estado de la UE o del EEE tendrá que ponerse en contacto con el prefecto de París para presentar su solicitud.

**Documentos de apoyo**

En apoyo de su solicitud, el profesional deberá presentar al prefecto correspondiente un expediente que contenga los siguientes documentos justificativos:

- El formulario de solicitud de la tarjeta, completado y firmado;
- Un documento en el que se exponen las menciones específicas, ya sean linguísticas o científicas o culturales, que se incluirán en el mapa;
- Una fotocopia de un documento de identidad válido
- Uno o más documentos fotográficos
- una copia del diploma, certificado o título obtenido.

**Resultado del procedimiento**

El prefecto dispondrá de un mes a partir de la recepción de las pruebas justificativas para reconocerla o solicitar el envío de los documentos que faltan. La decisión de emitir la tarjeta de visita tendrá lugar en un plazo de dos meses.

El silencio guardado al final de este período valdrá la pena la decisión de otorgar la tarjeta profesional.

**Remedios**

En caso de decisión negativa del prefecto, el interesado, si considera que esta decisión es cuestionable, podrá:

- o presentar una apelación agraciada ante el prefecto en un plazo de dos meses a partir de la recepción de la notificación de la decisión;
- o formar, dentro del mismo plazo, un llamamiento jerárquico al Ministro responsable de la economía y las finanzas;
- o constituir una impugnación legal directamente en el plazo de dos meses a partir de la notificación de la decisión del prefecto ante el tribunal administrativo territorialmente competente.

**Costo**

Gratis.

