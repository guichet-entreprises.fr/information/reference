﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS104" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Tratamiento de residuos: aceites" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="tratamiento-de-residuos-aceites" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/tratamiento-de-residuos-aceites.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="tratamiento-de-residuos-aceites" -->
<!-- var(translation)="Auto" -->

Tratamiento de residuos: aceites
================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad de transformación de residuos de aceites usados consiste en que el profesional, desarrolle o, como último recurso, elimine los aceites que se han vuelto inadecuados para el uso para el que fueron destinados.

Los aceites usados se consideran:

- aceites de los motores de combustión y los sistemas de transmisión;
- aceites lubricantes
- aceites de turbina;
- aceites para sistemas hidráulicos.

El propósito de la operación de recuperación es permitir el uso de residuos con fines útiles y prepararlos para su reutilización.

*Para ir más allá* Artículo R. 543-3 del Código de Medio Ambiente; Artículo 3 de la Directiva sobre residuos del Parlamento Europeo y del Consejo 2008/CE, de 19 de noviembre de 2008 y por la que se derogan determinadas directivas.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

Dado que la actividad es de carácter comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional realiza una actividad de comprayé su actividad será tanto comercial como artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El profesional que desee operar una instalación de procesamiento de aceite usado debe obtener una aprobación, cuyos términos varían en función de la naturaleza de la actividad realizada.

Así, cuando la actividad del profesional está comprendida en el sistema de instalaciones clasificadas para la protección del medio ambiente (ICPE), su aprobación se concede en las siguientes condiciones:

- al mismo tiempo que la autorización o la orden de registro;
- siempre que esta autorización o orden de registro mencione la naturaleza, el origen, las cantidades máximas permitidas y las condiciones de tratamiento de residuos.

**Tenga en cuenta que**

Cuando la instalación ya esté autorizada o sujeta a declaración, se considerará que el profesional está aprobado tan pronto como se indiquen las menciones anteriores dentro de la orden de autorización, registro o declaración.

En caso contrario, el profesional está obligado a enviar una declaración complementaria con esta información a la autoridad competente.

Además, para operar una instalación para deshacerse de estos aceites, el profesional debe obtener una aprobación específica (ver infra "3." a. Solicitud de aprobación para operar una instalación de eliminación").

*Para ir más allá* Artículos L. 541-22; R. 543-13 y R. 515-37 del Código de Medio Ambiente.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) se haga un ejercicio temporal, ocasional o permanente en Francia.

Como tal, un nacional legalmente establecido en un Estado miembro y que ejerce la actividad de transformación de residuos de aceite, está sujeto a los mismos requisitos que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

### c. Reglas profesionales

**Cumplimiento de las especificaciones**

El profesional que posee una licencia de eliminación de residuos está sujeto a una especificación y deberá, como tal:

- mantener una contabilidad que dice:


  - cantidades y fechas de recepción de aceites usados,
  - Las características físicas y químicas de los productos derivados de la regeneración o el reciclaje,
  - Los destinatarios,
  - Si es necesario, los tonelajes retirados de los productos incinerados;
- Emitir el selector aprobado con un resguardo de soporte del producto, incluyendo su tonelaje y calidad;
- Tener una capacidad mínima de almacenamiento igual a una doceava parte de la capacidad de eliminación anual de la instalación;
- Si es necesario, tome todas las disposiciones para garantizar el almacenamiento transitorio de aceites en caso de suspensión o cese de la actividad;
- presentar mensualmente a la Agencia de Gestión del Medio Ambiente y la Energía (Ademe) todas las estadísticas técnicas y económicas relativas a su actividad (el tonelaje recibido y procesado, los precios de recuperación);
- mostrar los precios de recuperación cobrados.

*Para ir más allá* : anexo a la ordenanza del 28 de enero de 1999 sobre las condiciones de eliminación de los aceites usados.

**Sanciones**

El profesional que realiza la actividad de tratamiento de residuos se multa con 750 euros si:

- no guarda registro de su actividad;
- se niega a entregar este registro a los oficiales en caso de un cheque;
- no presenta la declaración anual a Ademe en caso de que se cumpla un ICPE;

*Para ir más allá* R. 541-78 del Código de Medio Ambiente.

### d. Algunas peculiaridades de la regulación de la actividad

**Garantías financieras**

El profesional está obligado a obtener garantías financieras mientras su actividad esté comprendida en las instalaciones clasificadas para la protección del medio ambiente sujeta según autorización (véase infra "3." b. Si es necesario, proceda con las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)).

Estas garantías financieras pueden dar lugar a:

- un compromiso escrito de una institución de crédito, seguro, financiamiento o fianza mutua;
- Un fondo de garantía gestionado por Ademe;
- un depósito en manos de la Caisse des dépéts et consignments;
- un compromiso escrito con una garantía independiente de una persona que posea más de la mitad del capital del operador.

Estas garantías financieras deben garantizarse durante un mínimo de dos años y deben renovarse al menos tres meses antes de su vencimiento.

Estas garantías financieras están destinadas a cubrir los riesgos asociados con los gastos para:

- La seguridad del lugar de funcionamiento del profesional;
- Intervenciones en caso de accidente o contaminación;
- reacondicionado después de su cierre.

*Para ir más allá* Artículo R. 516-1 y siguiente del Código de Medio Ambiente; decreto de 31 de mayo de 2012 por el que se establece la lista de instalaciones clasificadas sujetas a la obligación de establecer garantías financieras en virtud del artículo R. 516-1 del Código de Medio Ambiente.

**Jerarquía de métodos de tratamiento de residuos**

El tratamiento de los aceites usados debe llevarse a cabo de conformidad con la política nacional de prevención y gestión de residuos.

Como tal, el tratamiento de los aceites usados debe centrarse en:

- Preparación para la reutilización
- Reciclaje
- Recuperación de energía
- Eliminación.

*Para ir más allá* Artículo L. 541-1 y siguiente del Código de Medio Ambiente.

**Mantener un registro cronológico de la actividad**

El profesional debe mantener un registro cronológico de su actividad de tratamiento de residuos.

Este registro debe mencionar todos los residuos:

- su designación y código de acuerdo con la clasificación de residuos peligrosos establecida en el Anexo Decreto No 2002540, de 18 de abril de 2002, sobre la clasificación de los residuos;
- su tonelaje;
- Su fecha de recepción
- El número de los resguardos de seguimiento;
- la identidad y dirección del remitente original y, en caso afirmativo, el número SIRET o, si los residuos han sido procesados o procesados, ya no identificaelel ya el origen, la identidad y el número SIRET del La instalación que realizó esta operación;
- En caso necesario, identificación de las instalaciones en las que los residuos hayan sido almacenados, reacondicionados, procesados o tratados previamente;
- La identidad del transportista, el número SIREN y el número de recibo;
- La designación de los métodos de tratamiento o su transformación;
- La fecha de reenvasado, procesamiento o reacondicionamiento de residuos;
- si es así, la fecha y la razón para negarse a cuidar de los residuos.

Este registro debe conservarse durante al menos tres años.

**Tenga en cuenta que**

El profesional que opera un ICPE también debe llevar un registro de su actividad y proporcionar a Ademe una declaración anual sobre la naturaleza y las cantidades de aceites usados que salen de sus instalaciones.

*Para ir más allá* Artículos R. 541-43 y R. 541-46 del Código de Medio Ambiente; Artículo 4 del Decreto de 7 de julio de 2005 por el que se establece el contenido de los registros mencionados en el artículo 2 del Decreto No 2005-635, de 30 de mayo de 2005, relativo al control de los circuitos de tratamiento de residuos y a los residuos peligrosos y a los residuos no peligrosos o peligrosos Radiactivo.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitud de aprobación para operar una instalación de eliminación

**Autoridad competente**

El profesional deberá presentar su solicitud en tres copias, al prefecto del departamento en el que se encuentre su instalación.

**Documentos de apoyo**

Su solicitud debe incluir:

- una descripción técnica de la instalación, que incluye:
  - procesos y capacidad de reciclado, regeneración, incineración y coincineración de aceites usados,
  - Capacidades de almacenamiento de aceite,
  - Cómo se eliminan los residuos
  - disposiciones para comprobar las características de los aceites usados;
- humano y material significa llevar a cabo controles y controles.

**Procedimiento**

La solicitud se remite al Consejo Departamental de Riesgos Ambientales y Sanitarios y Tecnológicos. El profesional podrá ser oído por este consejo y, en su caso, el prefecto deberá informarle con al menos ocho días de antelación de la fecha y lugar de la reunión.

*Para ir más allá* : decreto de 28 de enero de 1999 relativo a las condiciones de eliminación de aceites usados.

### b. Si es necesario, proceda con las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)

La transformación de aceites usados, debido a la naturaleza contaminante y peligrosa de los residuos, puede estar sujeta a reglamentos relativos a instalaciones clasificadas para la protección del medio ambiente.

El profesional debe solicitar permiso siempre y cuando su actividad:

- 2790: Tratamiento de Residuos Peligrosos;
- 2792: Tratamiento del policlorobifenilo (PCB) / policloroterfenilos (PCT) a una concentración superior a 50 partes por millón (ppm).

La solicitud de autorización debe dirigirse al prefecto del departamento en el que desea ejercer y debe incluir la Formulario Cerfa 15293*01 en el caso de una planta de energía o Formulario Cerfa 15294*01 instalaciones no energéticas.

Es aconsejable referirse a la nomenclatura del ICPE, disponible en el[Aida](https://aida.ineris.fr/) para obtener más información.

### c. Formalidades de notificación de la empresa

En el caso de la creación de una empresa mercantil, el profesional está obligado a inscribirse en el Registro de Comercio y Sociedades (RCS).

**Autoridad competente**

El profesional debe hacer una declaración a la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

Para ello, debe proporcionar documentos de apoyo dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI le envió un recibo el mismo día indicando los documentos que faltaban en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo.

Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la devolución de su expediente siempre que no se haya presentado durante los plazos mencionados anteriormente.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.