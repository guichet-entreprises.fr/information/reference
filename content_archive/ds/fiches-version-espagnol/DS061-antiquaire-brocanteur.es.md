﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS061" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Distribuidor de antiguedades" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="distribuidor-de-antiguedades" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/distribuidor-de-antiguedades.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="distribuidor-de-antiguedades" -->
<!-- var(translation)="Auto" -->


Distribuidor de antiguedades
============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El anticuario y el comerciante son profesionales cuya misión es comprar y revender antiguedades.

Estos profesionales buscarán (buscar) todo tipo de objetos de ocasión cotidiana (especialidad del distribuidor), o piezas excepcionales (referenciadas oficialmente) para evaluarlos y, si es necesario, restaurarlos (especialidad de anticuario) para la reventa.

### b. CFE competente

La CFE correspondiente depende de la naturaleza de la estructura en la que la actividad
se ejerce:

- Para una actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere diploma ni certificación para llevar a cabo la actividad de distribuidor-vendedor de antiguedades. Sin embargo, se recomienda tener experiencia profesional en el comercio o venta de arte o objetos usados. Sin embargo, se recomienda la formación en historia del arte o un diploma estatal como negociador en objetos de arte y decoración.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o una parte en el Espacio Económico Europeo (EEE) no está sujeto a ningún requisito de calificación o certificación, al igual que los franceses.

### c. Algunas peculiaridades de la regulación de la actividad

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Dado que las instalaciones están abiertas al público, el profesional debe cumplir con las normas relativas a las instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

#### b. Registro en el Registro de Objetos muebles (ROM)

Cualquier profesional que desee vender muebles usados o adquiridos de personas que no los hayan fabricado o comercializado está obligado a llenar un registro en papel listado y firmado (también llamado "registro de brocante") que contenga:

- Las características y procedencia de los objetos
- El precio de compra y cómo se liquida cada artículo
- para objetos con un precio inferior a 60,98 euros y no tienen interés artístico e histórico, bastan menciones y descripciones comunes.

También es posible utilizar un registro electrónico que requerirá un procesamiento automatizado para garantizar la integridad, intangibilidad y seguridad de los datos registrados. La vida útil de estos datos es de diez años.

El anticuario debe enviar un archivo a la prefectura del lugar de establecimiento, incluidos los siguientes documentos justificativos:

- El formulario[Cerfa 117303*01](https://www.service-public.fr/professionnels-entreprises/vosdroits/R14001) completado, fechado y firmado;
- Una identificación válida
- un certificado de registro ante el RCS o un certificado de registro ante la RMA o la recepción de la declaración de actividad.

**Costo**

Gratis.

*Para ir más allá* Artículos R. 321-3 y siguientes del Código Penal.

### c. Si es necesario, haga una declaración como titular de metales preciosos

Mientras el concesionario de la cuanta sea posea objetos de oro, plata o platino, deberá hacer una declaración previa a la oficina de garantía de la Dirección Regional de Aduanas y Derechos Indirectos.

La declaración se realiza enviando un archivo que contiene los siguientes documentos justificativos:

- La declaración que contiene el nombre del custodio o de la empresa, la designación de su actividad, el lugar de práctica y la dirección de la sede;
- Una identificación válida
- un certificado de registro ante el RCS o un certificado de registro ante la RMA o la recepción de la declaración de actividad.

Al recibir el expediente, la oficina de garantía emitirá una declaración de existencia.

*Para ir más allá* Artículo 534 del Código Fiscal General.

