﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS047" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Instituto de Belleza-Estética" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="instituto-de-belleza-estetica" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/instituto-de-belleza-estetica.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="instituto-de-belleza-estetica" -->
<!-- var(translation)="Auto" -->


Instituto de Belleza-Estética
=============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El esteticista es un profesional en el cuidado y la belleza. El profesional realiza tratamientos estéticos y modelos de confort. Estos actos no son de naturaleza médica ni paramédica.

*Para ir más allá* Ley 96-603, de 5 de julio de 1996, sobre el desarrollo y la promoción del comercio y la artesanía.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una profesión artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El interesado que desee trabajar como esteticista debe tener una cualificación profesional o un ejercicio bajo el control efectivo y permanente de una persona con esta cualificación.

Para ser considerado profesionalmente calificado, la persona debe poseer uno de los siguientes diplomas o títulos:

- Certificado de Habilidad Profesional (CAP) "Perfumería Cosmética Cosmética"
- patente profesional "Perfumería Cosmética Cosmética" patente;
- Licenciatura profesional (bac pro) "Perfumería cosmética cosmética";
- Certificado de Técnico Superior (BTS) "Estética Cosmética";
- Certificado de maestría "Estética Cosmética" expedido por la Asamblea Permanente de Cámaras de Artesanía y Artesanía (APCMA).

En ausencia de uno de estos títulos o títulos, el interesado debe justificar una experiencia profesional efectiva de tres años en el territorio de la Unión Europea (UE) o del Espacio Económico Europeo (EEE), adquirido como gestor de empresas, autónomo o autónomo en el ejercicio de la labor de esteticista. En este caso, se recomienda al interesado que se ponga en contacto con la Cámara de Comercio y Artesanía (CMA) para solicitar un certificado de cualificación profesional.

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Para un ejercicio temporal e informal (LPS)

Todo nacional de un Estado miembro de la UE o del EEE, establecido y que practique legalmente la actividad de esteticista en ese Estado, podrá ejercer la misma actividad en Francia, de forma temporal y ocasional.

En primer lugar, deberá presentar la solicitud mediante declaración a la CMA del lugar en el que desea llevar a cabo el servicio.

En el caso de que la profesión no esté regulada, ni en el curso de la actividad ni en el marco de la formación, en el país en el que el profesional esté legalmente establecido, deberá haber realizado esta actividad durante al menos un año, en el transcurso de los diez años antes de la prestación, en uno o varios Estados miembros de la UE.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la CMA competente podrá exigir que el interesado se someta a una prueba de aptitud.

*Para ir más allá* Artículo 17-1 de la Ley de 5 de julio de 1996; Artículo 2 del Decreto de 2 de abril de 1998 modificado por el decreto de 4 de mayo de 2017.

#### Para un ejercicio permanente (LE)

Para llevar a cabo la actividad de esteticista en Francia de forma permanente, la UE o el nacional del EEE deben cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que un francés;
- han trabajado como esteticista durante dos años consecutivos, ya sea de forma independiente o como líder empresarial, siempre y cuando posea un certificado de formación reconocido por un Estado de la UE o del EEE;
- poseer un certificado de competencia o certificado de formación requerido para el ejercicio de la actividad de esteticista en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su preparación para el ejercicio de la actividad de esteticista/ne cuando este certificado o título se haya obtenido en un estado de la UE o del EEE que no regule el acceso o el ejercicio de esta actividad;
- obtener un diploma, título o certificado adquirido en un tercer estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona haya estado trabajando como esteticista en el estado que ha admitido Equivalencia.

Si se cumplen las cualificaciones de eu, un nacional de un Estado de la UE o del EEE podrá solicitar un certificado de reconocimiento de la cualificación profesional.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la CMA competente podrá exigir que el interesado se someta a medidas de indemnización.

*Para ir más allá* Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 a 3-2 del Decreto de 2 de abril de 1998 modificado por el decreto de 4 de mayo de 2017.

### c. Honorabilidad

Nadie puede practicar como esteticista si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 III de la Ley 96-603, de 5 de julio de 1996.

### d. Algunas peculiaridades de la regulación de la actividad

#### Cumplimiento de las normas de seguridad y accesibilidad

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

Es aconsejable consultar el listado " [Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales de las normas de seguridad contra incendios y pánico en las instituciones públicas.

#### Reglamento sobre la calidad del artesano y el título de mejor trabajador en Francia

**Artesanía**

Para reclamar la condición de artesano, la persona debe justificar:

- una PAC, un BEP o un título certificado o registrado cuando se emitió al RNCP al menos equivalente (véase arriba: "2. a. Cualificaciones profesionales");
- experiencia profesional en este comercio durante al menos tres años.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de mejor trabajador de Francia (MOF)**

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en el ámbito artesanal, comercial, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación. Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

Para obtener más información, se recomienda que consulte[web oficial del concurso "uno de los mejores trabajadores de Francia"](http://www.meilleursouvriersdefrance.info/).

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

#### Regulación de beneficios específicos

El esteticista no puede practicar actividades de masaje en sus clientes, que están reservados para los masajistas, pero sólo las llamadas actividades de modelado. Se trata de una maniobra superficial externa realizada sobre la piel del rostro y del cuerpo humano con un fin exclusivamente estético y de confort, con exclusión de cualquier finalidad médica y terapéutica.

Del mismo modo, para la parte de depilación, no podrá utilizar sólo técnicas de depilación con alicates o cera, las otras operaciones se reservan para los médicos.

**Tenga en cuenta que**

Las regulaciones para la depilación con luz pulsada en los institutos aún no se han finalizado. Una opinión de la Agencia Nacional de Seguridad Sanitaria afirma que el esteticista que realiza esta actividad debe tener un diploma especializado. Las discusiones siguen en curso.

#### Visualización de precios

El esteticista debe mostrar fuera de su instituto las tarifas de todos los impuestos que practica. En el interior, tendrá que ofrecer un folleto completo de los servicios prestados, así como sus precios de TTC.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario tendrá que inscribirse en el Registro mercantil (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar los listados correspondientes para obtener más información.

### b. Siga el curso de preparación de la instalación (SPI)

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que son:

- han ejercido una actividad profesional que requiere un nivel de conocimientos equivalente al proporcionado por las prácticas durante al menos tres años;
- conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que cubriría, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con en Francia para dirigir una empresa de artesanías.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, artículo 6-1 del Decreto 83-517, de 24 de junio de 1983.

### c. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

La CMA del lugar en el que el nacional desea llevar a cabo la prestación es competente para emitir la declaración previa de actividad.

**Documentos de apoyo**

La solicitud de un informe previo de la actividad va acompañada de un archivo completo que contiene los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE;
- un documento que justifique la cualificación profesional del nacional que puede ser, a su elección:- Una copia de un diploma, título o certificado,
  - Un certificado de competencia,
  - cualquier documentación que acredite la experiencia profesional del nacional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tenga en cuenta que**

Cuando el expediente está incompleto, la CMA tiene un plazo de quince días para informar al nacional y solicitar todos los documentos que faltan.

**Resultado del procedimiento**

Al recibir todos los documentos en el archivo, el CMA tiene un mes para decidir:

- autorizar la prestación cuando el nacional justifique tres años de experiencia laboral en un Estado de la UE o del EEE, y adjuntar a dicha Decisión un certificado de cualificación profesional;
- o autorizar la disposición cuando las cualificaciones profesionales del nacional se consideren suficientes;
- o imponerle una prueba de aptitud cuando existan diferencias sustanciales entre las cualificaciones profesionales del nacional y las exigidas en Francia. En caso de negativa a realizar esta medida de compensación o si no cumple, el nacional no podrá prestar el servicio en Francia.

El silencio guardado por la autoridad competente en este plazo merece autorización para iniciar la prestación del servicio.

*Para ir más allá* Artículo 2 del Decreto de 2 de abril de 1998; Artículo 2 del Decreto de 17 de octubre de 2017 relativo a la presentación de la declaración y solicitudes previstas en el Decreto 98-246, de 2 de abril de 1998, y en el título I del Decreto 98-247, de 2 de abril de 1998.

### d. Si es necesario, solicite un certificado de reconocimiento de la cualificación profesional

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la ACM territorialmente competente.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA pide al interesado que lo complete dentro de una quincena de la presentación del expediente. Se emite un recibo tan pronto como se completa el archivo.

**Documentos de apoyo**

La carpeta debe contener las siguientes partes:

- Solicitar un certificado de cualificación profesional
- Un certificado de competencia o título de diploma o designación de formación profesional;
- Prueba de la nacionalidad del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (CIEP) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**Qué saber**

Si es necesario, todos los documentos justificativos deben traducirse al francés.

**Tiempo de respuesta**

Dentro de los tres meses siguientes a la recepción, la CMA podrá:

- Reconocer la cualificación profesional y emitir la certificación de cualificación profesional;
- decidir someter al solicitante a una medida de compensación y notificarle dicha decisión;
- negarse a expedir el certificado de cualificación profesional.

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Remedios**

Si la CMA se niega a emitir el reconocimiento de la cualificación profesional, el solicitante podrá iniciar, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, una impugnación legal ante el tribunal administrativo pertinente. Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de indemnización, primero debe iniciar un recurso agraciado ante el prefecto del departamento en el que se basa la CMA, en el plazo de dos meses a partir de la notificación de la decisión. Cma. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad u otro Estado parte en el acuerdo del Espacio Económico Europeo.

