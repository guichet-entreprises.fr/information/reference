﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS029" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Pensiones" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="pensiones" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/pensiones.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="pensiones" -->
<!-- var(translation)="Auto" -->




Pensiones
=========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad de las habitaciones consiste, para el profesional, para dar la bienvenida a los turistas a su casa por una tarifa, por una o más noches.

La prestación de servicios incluye la recepción de turistas, en locales equipados con un baño y un aseo, el suministro de ropa de cama, así como el desayuno.

**Tenga en cuenta que**

El profesional no puede alquilar más de cinco habitaciones por vivienda, ni acomodar a más de 15 personas al mismo tiempo.

*Para ir más allá* Artículos L. 324-3 y D. 324-13 a D. 324-14 del Código de Turismo.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI);
- en el caso de la actividad agrícola, es la cámara de agricultura.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal, independientemente del número de empleados de la empresa con la condición de que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad de las habitaciones, se debe seguir la formación en caso de flujo de bebida (para los casos en que la habitación también hace mesa de invitados). Es necesario hacer una declaración de la actividad con el ayuntamiento del municipio donde se encuentra la vivienda en alquiler.

**Tenga en cuenta que**

El profesional que gestiona un establecimiento de casa de huéspedes sin informar de su actividad está sujeto a una multa de 450 euros.

*Para ir más allá* Artículos L. 324-4 y R. 324-16 del Código de Turismo.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

No existe ninguna disposición para que el nacional de un Estado de la Unión Europea (UE) o un Estado Parte en el acuerdo del Espacio Económico Europeo (EEE) funcione como una habitación de invitados temporal y casual (LPS) o como miembro del Espacio Económico Europeo (EEE) (LE).

Como tal, el profesional está sujeto a los mismos requisitos que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

### c. Algunas peculiaridades de la regulación de la actividad

**Publicidad de precios**

**Visualización de precios dentro de la propiedad**

El profesional está obligado a informar a sus clientes del precio de los servicios ofrecidos dentro de su establecimiento. Este precio debe:

- Comprender todos los impuestos que planea y los servicios que son inseparables de la reserva
- "Tarifa actualizada."

Además, el precio final a pagar por todos los servicios prestados debe estar disponible en todo momento para el consumidor e indicarle:

- el servicio, o no, del desayuno;
- Si tiene o no acceso a una conexión a Internet desde las habitaciones
- información sobre todos los servicios disponibles dentro de la institución.

**Deber de exhibir fuera de la propiedad**

El profesional está obligado a mostrar de forma clara y legible, en la entrada principal de su establecimiento y en el lugar de recepción de su clientela:

- el precio cobrado por la noche siguiente en una habitación doble o el precio máximo cobrado por una noche en una habitación doble por un período determinado incluyendo la noche siguiente. Sin embargo, si el establecimiento no ofrece estos servicios, el profesional deberá indicar el precio correspondiente al servicio de alojamiento más practicado, así como su duración;
- si acceder o no a Internet desde las habitaciones, proporcionar o no el desayuno y, en caso afirmativo, si estos servicios están incluidos o no en el precio del servicio;
- cómo acceder a la información sobre los precios y servicios ofrecidos por el establecimiento.

Además de la información anterior, el profesional deberá, en el punto de recepción de su clientela, indicar las posibles horas de llegada y salida y, en su caso, los suplementos en caso de salida tardía.

**Requisito para mostrar en cada habitación**

En cada sala de su establecimiento, el profesional deberá poner a disposición toda la información relativa al precio de los servicios prestados, así como los procedimientos de consulta de esta información.

*Para ir más allá* : decreto de 18 de diciembre de 2015 relativo a la publicidad de los precios de los alojamientos turísticos no turísticos distintos de los alojamientos turísticos y hoteles al aire libre.

**Facturación**

El profesional está sujeto al cumplimiento de las obligaciones de facturación y debe emitir una nota al cliente en caso de un beneficio de un importe superior o igual a 25 euros. Esta nota debe incluir la siguiente información:

- Su fecha de escritura;
- El nombre y la dirección del proveedor
- El nombre del cliente (si el cliente no se opone a él);
- La fecha y el lugar de ejecución del servicio:
- los detalles de cada beneficio, así como el importe total a pagar (todos los impuestos incluidos y excluidos los impuestos).

El profesional debe mantener un doble de este grado, clasificarlos cronológicamente y mantenerlos durante dos años.

*Para ir más allá* ( Orden 83-50/A, de 3 de octubre de 1983, relativa a la publicidad de precios de todos los servicios.

**Hoja de fuentes individuales**

Tan pronto como el profesional da la bienvenida a los turistas de nacionalidad extranjera en su establecimiento, debe tenerlos llenar y firmar una tarjeta de policía individual cuyo modelo se establece en el anexo al decreto de 1 de octubre de 2015 tomado de acuerdo con Artículo R. 611-42 del Código de Entrada y Residencia de Extranjeros y del Derecho de Asilo.

Esta hoja informativa para evitar disturbios en el orden público y para fines de investigación e investigación judicial debe mencionar:

- La identidad del extranjero;
- fecha, lugar de nacimiento y nacionalidad;
- La dirección de su casa habitual
- Su número de teléfono y dirección de correo electrónico
- La fecha de su llegada al establecimiento y la fecha prevista de su partida;
- Como opción, los niños menores de quince años pueden aparecer en la tarjeta de un adulto que los acompaña.

Este registro policial debe ser guardado por el profesional durante seis meses y entregado, si se solicita, a la policía y a las unidades de gendarmería.

*Para ir más allá* Artículo R. 611-42 del Código de Entrada y Residencia de Extranjeros y del Derecho de Asilo.

**Cumplimiento de los requisitos de higiene, seguridad y seguridad**

El profesional está obligado a cumplir con todos los requisitos de seguridad, higiene y seguridad. Para más información, es aconsejable ponerse en contacto con la Dirección Departamental de Protección de la Población ([DDPP](https://www.economie.gouv.fr/dgccrf/coordonnees-des-DDPP-et-DDCSPP)).

Si el establecimiento dispone de zona de baño o piscina, el profesional deberá cumplir con las normas de salud y seguridad contra el ahogamiento.

Es aconsejable referirse a la lista[Operador de piscina - lugar de baño](https://www.guichet-entreprises.fr/fr/activites-reglementees/tourisme-loisirs-culture/exploitant-piscine-lieu-de-baignade/) para obtener más información.

*Para ir más allá* Artículo D. 324-14 del Código de Turismo.

**Transmisión musical**

El profesional que desee transmitir música dentro de su institución debe solicitar en línea la autorización previa a la Sociedad de Compositores y Editores de Música (Sacem) y pagar el monto de la regalía. El profesional debe registrarse y cumpliser el formulario de autorización en línea en el[Sitio web de Sacem](https://clients.sacem.fr/autorisations).

*Para ir más allá* Artículo D. 324-14 del Código de Turismo.

**Impuesto de residencia**

Cuando el establecimiento del profesional se encuentra en un municipio que ha instituido un impuesto de residencia, está obligado a recaudar este impuesto y a pagarlo a la autoridad local.

*Para ir más allá* Artículos L. 2333-26 y R. 2333-44 del Código General de Gobierno Local.

**Declaración en caso de venta de alimentos**

Cuando el profesional propone la venta de alimentos en su establecimiento, está obligado a hacer una declaración al DDPP del departamento donde se encuentra su establecimiento.

Para ello, debe[Formulario Cerfa 13984*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13984.do) completado y firmado.

*Para ir más allá* Artículo R. 233-4 del Código Rural y Pesca Marina.

**Si es necesario, obtenga una licencia de licor**

En caso de venta de bebidas alcohólicas para ser consumidas en el establecimiento, el profesional está obligado a solicitar una licencia de licor.

Las bebidas se dividen en cuatro grupos en función de su contenido alcohólico. La licencia emitida varía en función del grupo al que pertenezcan.

Es aconsejable referirse a la lista[Beber](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/), para obtener más información.

Además, si las bebidas alcohólicas se venden solo a la hora de comer, el profesional debe solicitar:

- una "licencia de restaurante pequeño" para ofrecer en el lugar y durante las comidas, bebidas de tercer grupo (vino, cerveza, sidra, pera, aguamiel, vinos dulces, crema y jugo de fruta entre 1,2 y 3 grados de alcohol y licores que dibujan 18 grados alcohol puro máximo);
- una "licencia de restaurante" para vender todas las bebidas permitidas para el consumo in situ de comidas.

En todos los casos, el profesional está obligado a recibir una formación específica sobre los derechos y obligaciones relativos a la venta de bebidas alcohólicas adaptadas a la naturaleza de su actividad.

Para obtener más información, consulte el listado[Catering tradicional](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/restaurant-traditionnel/).

*Para ir más allá* Artículos L. 3321-1 y L. 3331-2 del Código de Salud Pública.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Declaración de actividad

**Autoridad competente**

El profesional deberá enviar su declaración electrónicamente, por carta recomendada o en depósito físico, al ayuntamiento del municipio donde se encuentre el establecimiento de que se trate.

**Documentos de apoyo**

La declaración debe incluir la[Formulario Cerfa 13566*03](https://www.formulaires.modernisation.gouv.fr/gf/showFormulaireSignaletiqueConsulter.do?numCerfa=13566) completado y firmado. Su declaración debe mencionar:

- La identidad del profesional
- La dirección de su casa
- El número de habitaciones disponibles para alquilar;
- El número máximo de personas que pueden ser acomodadas
- períodos de previsión de arrendamiento.

**Procedimiento**

El ayuntamiento envía un recibo para la presentación de la declaración al profesional, cuyo modelo figura en el apéndice del formulario antes mencionado.

**Costo**

Gratis.

*Para ir más allá* Artículo D. 324-15 del Código de Turismo.

### b. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de la empresa, el contratista debe informar de su actividad a la Cámara de Agricultura o inscribirse en el Registro de Comercio y Sociedades (RCS).

Es aconsejable referirse a las "Formalidades de Informar de una Empresa Comercial" y "Registro de una Empresa Comercial Individual a la SCN" y a la[Cámara de Agricultura](http://www.chambres-agriculture.fr/) para obtener más información.

### c. Si es necesario, registre los estatutos de la empresa

Una vez fechados y firmados los estatutos de la sociedad, el profesional que opera un establecimiento de casa de huéspedes deberá registrarlos en la oficina del impuesto de sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

