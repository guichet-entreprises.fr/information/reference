﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS021" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Compra/Venta de Placer o Mascotas" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="compraventa-de-placer-o-mascotas" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/compraventa-de-placer-o-mascotas.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="compraventa-de-placer-o-mascotas" -->
<!-- var(translation)="Auto" -->


Compra/Venta de Placer o Mascotas
=================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El negocio de comprar o vender mascotas es para el profesional, para comprar y ofrecer a individuos o profesionales, la venta de animales domésticos y no domésticos.

Las mascotas se consideran mascotas, las que pretenden ser sujetadas por el hombre para su disfrute. Por definición, los animales no domésticos son aquellos que no entran en esta categoría.

*Para ir más allá* Artículo L. 214-6 del Código de Pesca Rural y Marina y L. 413-2 del Código de Medio Ambiente.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Dado que la actividad es de carácter comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional realiza una actividad de comprayé su actividad será tanto comercial como artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para dedicarse al negocio de la compra o venta de mascotas o servicios, el profesional debe:

- registrado en el Registro de Comercio y Sociedades (RCS) (véase infra "3o. a. REGISTRO al RCS");
- Estar calificado profesionalmente
- operar en locales de conformidad con las disposiciones sanitarias (véase infra "2. (d.)
- En caso de actividad relacionada con las mascotas, haga una declaración al prefecto (ver más abajo "3 grados). b. Declaración de actividad en relación con las mascotas");
- En caso de actividad relacionada con animales no domésticos, pida permiso para abrir su establecimiento (véase infra "3. c. Solicitar permiso para abrir una instalación animal no doméstica").

*Para ir más allá* Artículo L. 214-6 del Código Rural y Pesca Marina; Sección L. 413-2 del Código de Medio Ambiente.

**Si compras/vendes con mascotas**

Para realizar un negocio de compra o venta de mascotas de especies domésticas, el profesional debe ser el titular de:

- una de las certificaciones profesionales mencionadas en el[Apéndice II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E4347A807573D4D41A0A98B2B4ED58E7.tplgfr30s_1?idArticle=LEGIARTI000032095076&cidTexte=LEGITEXT000032095054&dateTexte=20180301) (4 de febrero de 2016) orden sobre la formación y actualización de los conocimientos necesarios de las personas que realizan actividades relacionadas con las mascotas domésticas y la autorización de los organismos de formación;
- un certificado de conocimientos relativos a las necesidades biológicas, fisiológicas, de comportamiento y de mantenimiento de mascotas expedido por una institución aprobada por el Ministro de Agricultura;
- un Certificado de Capacidad de Animales de Especies Domésticas (CCAD) emitido antes del 1 de enero de 2016.

Para saber cómo acceder a estos cursos, es aconsejable consultar la hoja "Mantenimiento de mascotas de especies domésticas".

*Para ir más allá* Artículo L. 214-6 del Código Rural y Pesca Marina.

**En caso de compra/venta de animales no domésticos**

Para llevar a cabo esta actividad, el profesional debe tener un certificado de capacidad.

Para ello, debe solicitar lo mismo al prefecto del departamento de su hogar (véase infra "3o. d. Solicitud de certificado de capacidad para animales no domésticos").

*Para ir más allá* Artículo L. 413-2 del Código de Medio Ambiente.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

**Para ejercicios temporales e informales (LPS)**

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo sobre el Espacio Económico Europeo (EEE) legalmente establecido y que participe en actividades de compra o venta de mascotas o placer, podrá, actividades temporales e informales en Francia.

Para ello, deberá hacer una declaración previa, cuyos términos varían en función de la naturaleza de su actividad.

Para conocer los términos de esta declaración, es aconsejable referirse al "Mantenimiento de Mascotas de Especies Domésticas" y "Responsable del establecimiento de animales no domésticos, de venta o de alquiler, presentación al público de especímenes vivos de vida silvestre francesa o extranjera."

**Para un ejercicio permanente (LE)**

No hay ninguna disposición para los nacionales de la UE que deseen participar en la compra o venta de mascotas o servicios.

Como tal, el nacional está sujeto a las mismas disposiciones que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

### c. Prohibiciones

Para el profesional, está prohibido vender una mascota:

- enfermos o heridos durante un evento para la presentación y venta de mascotas;
- un menor menor de 16 años sin el consentimiento de sus padres;
- por una tarifa o no, en ferias, mercadillos, ferias, exposiciones u otros eventos que no están específicamente dedicados a los animales.

*Para ir más allá* Artículos L. 214-7, R. 214-20 y R. 214-31-1 del Código Rural y Pesca Marina.

### d. Algunas peculiaridades de la regulación de la actividad

**Términos y condiciones de las instalaciones**

Los locales en los que ejerce el profesional deben cumplir las normas de salud y protección de los animales.

**Tenga en cuenta que**

El profesional que desee organizar una exposición o evento relacionado con la venta de mascotas, deberá hacer primero una declaración con el prefecto del departamento y asegurarse de que las instalaciones cumplan con las normas. cuidado de la salud y la protección animal.

*Para ir más allá* Artículo R. 214-29 del Código Rural y Pesca Marina; decreto de 3 de abril de 2014 por el que se establecen las normas sanitarias y de protección animal a las que deben cumplirse las actividades relacionadas con las mascotas domésticas en virtud del artículo L. 214-6 del Código Rural y de la Pesca Marítima).

**Obligaciones de divulgación de clientes**

Al vender una mascota, el profesional está obligado a proporcionar al comprador:

- un certificado de transferencia, o una factura en caso de venta entre profesionales;
- Un fondo sobre todas las características y necesidades del animal vendido;
- en caso de venta de perros y gatos, un certificado veterinario.

*Para ir más allá* Artículo L. 214-8 del Código Rural y Pesca Marina.

**Obligación de establecer un reglamento sanitario con un veterinario**

El profesional está obligado, con la ayuda de un veterinario sanitario, a elaborar un reglamento sanitario que especifique las condiciones de su actividad. Este Reglamento debe garantizar que se respeten las necesidades de los animales, así como la higiene y la salud del personal.

Este Reglamento debe incluir al menos la siguiente información:

- Un plan para limpiar y desinfectar locales y equipos;
- Todas las normas de higiene del establecimiento
- Procedimientos de mantenimiento de animales
- el tiempo que los animales están aislados cuando llegan a las instalaciones.

**Tenga en cuenta que**

Dos veces al año, el profesional debe asegurarse de que un veterinario sanitario visite las instalaciones para verificar el cumplimiento de las instalaciones con todos estos requisitos.

*Para ir más allá* Artículo R. 214-30 del Código Rural y Pesca Marina; Capítulo III de la[Anexo](http://agriculture.gouv.fr/file/annexesarreteanimauxdecompagniebo-maafcle83fb2bpdf) I de la orden del 3 de abril de 2014 antes mencionada.

**Obligación de mantener registros diferentes**

Durante el transcurso de su actividad, el profesional debe mantenerse al día con:

- Un registro de animales que entran y salen de los nombres y direcciones de sus propietarios;
- un registro que enumera toda la información sobre el control de la salud y la salud animal.

**Tenga en cuenta que**

Estos registros deben presentarse a la autoridad competente en cada comprobación de estado.

*Para ir más allá* Artículo R. 214-30-3 del Código Rural y Pesca Marina.

**Nombrar un veterinario**

El profesional que se dedica a la compra o venta de mascotas debe designar un veterinario sanitario para supervisar el cumplimiento de las normas sanitarias y llevar a cabo controles de protección animal. Para ello, deberá informar al prefecto del departamento donde se encuentran los animales.

*Para ir más allá* Artículos L. 203-1, R. 203-1 y R. 203-2 del Código de Pesca Rural y Marina.

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales de las normas de seguridad contra incendios y pánico en las instituciones públicas.

Es aconsejable consultar el listado " [Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Registro en el RCS para la compra o venta de mascotas

**Autoridad competente**

El comerciante debe informar de su negocio y, para ello, hacer una declaración ante la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

El interesado debe proporcionar la[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI envía un recibo al profesional el mismo día mencionando los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículos L. 214-6-3 del Código de Pesca Rural y Marina y L. 123-1 del Código de Comercio; Artículo 635 del Código Tributario General.

### b. Declaración de actividad en relación con las mascotas

**Autoridad competente**

El profesional debe dirigir su declaración a la Dirección Departamental de Protección de la Población (DDPP) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde desea establecerse.

**Documentos de apoyo**

Debe enviar al organismo correspondiente al formulario[Cerfa 15045*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15045.do) completado y firmado. Este procedimiento también se puede hacer en línea en el[Sitio web del Departamento de Agricultura](https://agriculture-portail.6tzen.fr/default/requests/Cerfa15045/).

**Procedimiento**

Una vez recibida la declaración, el prefecto envía al solicitante un recibo de una declaración.

*Para ir más allá* Artículo L. 214-6-1 del Código Rural y Pesca Marina.

### c. Solicitar permiso para abrir una instalación animal no doméstica

Las instituciones se clasifican en dos categorías:

- el primero incluye los establecimientos que plantean graves peligros o desventajas para la vida silvestre y los entornos naturales y la seguridad de las personas;
- el segundo incluye a aquellos que no presentan los peligros anteriores, pero que deben cumplir con las disposiciones para garantizar la protección de la vida silvestre y los entornos naturales y la seguridad de las personas.

**Autoridad competente**

El profesional deberá presentar una solicitud de siete copias al prefecto del departamento en el que se encuentre el establecimiento (o desde su domicilio, siempre que el establecimiento sea móvil).

**Documentos de apoyo**

La solicitud debe incluir la siguiente información:

- La identidad, la dirección y, si el solicitante es una persona jurídica, su nombre y nombre;
- La naturaleza de las actividades que el profesional desea llevar a cabo;
- La lista de las instalaciones de la instalación y el plan de la instalación;
- La lista de especies y el número de animales de cada especie en poder de la institución y su distribución en la instalación;
- Un aviso sobre cómo funciona el establecimiento
- El certificado de capacidad del administrador de la instalación.

**Tenga en cuenta que**

El nombre de la institución no debe incluir los siguientes términos, debido a las disposiciones específicas que los rigen:

- Parque Nacional;
- Reserva Natural;
- Conservatorio.

Además, cuando la instalación incluye instalaciones pre-aprobadas clasificadas para la protección del medio ambiente, la instalación está sujeta a permiso de apertura.

**Retrasos y resultados del procedimiento**

El prefecto recibe el dictamen de la Comisión Departamental sobre la Naturaleza de los Paisajes y Sitios, que puede cuestionar al solicitante. Si es necesario, será informado por el prefecto ocho días antes de su presentación.

El prefecto dispone de cinco meses a partir de la presentación de la solicitud para autorizar la apertura del establecimiento. En caso de dictamen favorable, decide la autorización de apertura que establece la lista de especies que el establecimiento puede tener y las actividades que pueden llevarse a cabo.

**Tenga en cuenta que**

Para los establecimientos de la primera categoría, el prefecto debe recabar la opinión de las autoridades locales, que tienen un período de decisión de 45 días. A falta de respuesta, el dictamen se considera favorable.

Por otra parte, en el caso de los establecimientos de la segunda categoría, el prefecto examina el cumplimiento de la aplicación con los requisitos de protección de las especies y la calidad de las instalaciones de cuidado animal no nacionales.

*Para ir más allá* Artículos R. 413-10 a R. R. 413-14 del Código de Medio Ambiente; decreto de 10 de agosto de 2004 por el que se establecen las normas generales para el funcionamiento de las instalaciones de cría de animales de especies no nacionales.

### d. Solicitud de certificado de capacidad para animales no domésticos

**Autoridad competente**

El profesional debe solicitar al prefecto del departamento de su casa o al prefecto de policía en París si no está domiciliado en un departamento francés o en Saint-Pierre-et-Miquelon.

**Documentos de apoyo**

Su solicitud debe contener la siguiente información:

- identidad, dirección y tipo de cualificación general o especial solicitada
- todos los diplomas, certificados o cualquier documento que justifique su experiencia profesional;
- cualquier documentación que justifique la competencia del solicitante para realizar una actividad profesional en relación con las mascotas y el desarrollo de un establecimiento que las albergaría.

**Retrasos y resultados del procedimiento**

Al recibir la solicitud completa del profesional, el prefecto le expide el certificado de capacidad. Este certificado podrá concederse por un período de tiempo limitado o ilimitado y menciona la especie o actividad para la que se concedió y, como opción opcional, el número de animales autorizados para su mantenimiento.

**Tenga en cuenta que**

Para la cría de ciertas categorías de animales, la consulta con la Comisión Departamental de Naturaleza, Paisajes y Sitios no es obligatoria siempre y cuando el profesional cumpla con los requisitos de calificación. Los términos de este procedimiento son establecidos por el[decretado a partir del 2 de julio de 2009](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020887078&dateTexte=20180406) citado a continuación.

*Para ir más allá* R. 413-3 a R. 413-7 del Código de Medio Ambiente; orden de 2 de julio de 2009 por la que se establecen las condiciones simplificadas en las que puede expedirse el certificado de capacidad para el mantenimiento de animales de especies no nacionales.

### e. Autorización posterior al registro

**En caso necesario, proceder a las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)**

Dependiendo de la naturaleza del negocio, el profesional puede estar obligado a declarar sus instalaciones.

Sólo las actividades de cría, venta y tránsito de perros[2120](https://aida.ineris.fr/consultation_document/10535) y animales depredadores peludos[2113](https://aida.ineris.fr/consultation_document/10535) se ven afectados por este reglamento.

Como tal, el profesional debe:

- solicitar autorización de la prefectura si tiene más de 50 perros o más de 2000 depredadores peludos;
- hacer una declaración al prefecto si tiene entre 10 y 50 perros o entre 100 y 200 carnívoros peludos.

*Para ir más allá* Artículos L. 181-1, L. 413-3 y L. 511-1 y siguientes del Código de Medio Ambiente.

Para obtener más información sobre los acuerdos de presentación de informes y autorización, es aconsejable visitar el[sitio dedicado a instalaciones clasificadas](http://www.installationsclassees.developpement-durable.gouv.fr/).

