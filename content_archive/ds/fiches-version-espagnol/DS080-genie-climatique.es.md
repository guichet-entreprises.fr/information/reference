﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS080" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Experiencia" -->
<!-- var(title)="Ingeniería climática" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="experiencia" -->
<!-- var(title-short)="ingenieria-climatica" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/experiencia/ingenieria-climatica.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="ingenieria-climatica" -->
<!-- var(translation)="Auto" -->

Ingeniería climática
====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La ingeniería climática combina todas las técnicas de calefacción, ventilación y aire acondicionado que permiten el confort térmico mediante el control del rendimiento energético de un edificio.

Un profesional de la ingeniería climática es cualquier persona que realiza una o todas las siguientes actividades:

- Puesta en marcha de sistemas e instalaciones de refrigeración y aire acondicionado, incluidas las bombas de calor y el aire acondicionado de los vehículos, que contienen refrigerantes, solos o en mezcla;
- mantenimiento y reparación de estos equipos, siempre y cuando estas operaciones requieran intervención en el circuito que contiene líquidos refrigerantes;
- controlando su impermeabilización
- desmantelarlos;
- la recuperación y carga de refrigerantes en estos dispositivos;
- cualquier otra operación realizada en equipos que requieran el manejo de líquidos refrigerantes.

*Para ir más allá* Artículo R. 543-76 del Código de Medio Ambiente.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

Si el profesional tiene una actividad de compra y reventa, su actividad es artesanal y comercial.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Sólo una persona cualificada que esté cualificada profesionalmente o puesta bajo el control efectivo y permanente de una persona cualificada puede llevar a cabo una de las actividades de ingeniería climática.

La persona que realiza una de las actividades contempladas en el párrafo "1." a. Definición" o quién controla el ejercicio, debe ser el titular de la elección:

- Un Certificado de Cualificación Profesional (CAP);
- Una Patente de Estudios Profesionales (BEP);
- un diploma o un grado de igual o superior nivel aprobado o registrado cuando se expidió al directorio nacional de certificaciones profesionales.

A falta de uno de estos títulos, el interesado deberá justificar una experiencia profesional efectiva de tres años, en un Estado de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE), adquirido como líder autónomos o autónomos en un comercio de ingeniería climática. En este caso, el interesado podrá solicitar a la CMA pertinente un certificado de cualificación profesional.

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 1 del Decreto 98-246, de 2 de abril de 1998; Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Para entrega gratuita de servicios (LPS)

El profesional casado con el eee puede dedicarse a la ingeniería climática en Francia de forma temporal y ocasional, siempre que esté legalmente establecido en uno de estos estados para llevar a cabo la misma actividad.

Primero tendrá que solicitarlo mediante declaración escrita a la CMA pertinente (véase infra "3o. b. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)).

Si ni la actividad ni la formación que la lleva a ella están reguladas en el Estado de la Institución, la persona también debe demostrar que ha estado activa en ese estado durante al menos un año a tiempo completo o a tiempo parcial en los últimos diez años anteriores a la que quiere actuar en Francia.

**Tenga en cuenta que**

El profesional que cumpla estas condiciones está exento de los requisitos de registro del directorio de operaciones (RM) o del registro de empresas.

*Para ir más allá* Artículo 17-1 de la Ley 96-603 de 5 de julio de 1996.

#### Para un establecimiento gratuito (LE)

Para llevar a cabo una actividad de ingeniería climática en Francia de forma permanente, el profesional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba: "2. a. Cualificaciones profesionales");
- Poseer un certificado de competencia o título de formación requerido para el ejercicio de la actividad en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un documento de formación que certifique su preparación para el ejercicio de la actividad cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de esta actividad;
- ser un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona ha estado activa durante tres años en el estado que ha admitido la equivalencia.

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones anteriores podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante: "3. c. Si es necesario, solicite un certificado de cualificación profesional").

Si la persona no cumple con cualquiera de las condiciones anteriores, la CMA puede pedirle que realice una medida de compensación en los siguientes casos:

- si la duración de la formación certificada es al menos un año inferior a la requerida para obtener una de las cualificaciones profesionales requeridas en Francia para llevar a cabo la actividad;
- Si la formación recibida abarca temas sustancialmente diferentes de los cubiertos por una de las cualificaciones requeridas para llevar a cabo la actividad en Francia;
- Si el control efectivo y permanente de la actividad requiere, para el ejercicio de algunas de sus competencias, una formación específica que no se imparte en el Estado miembro de origen y que cubra temas sustancialmente diferentes de los cubierto por el certificado de competencia o designación de formación a que se refiere el solicitante.

*Para ir más allá* Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998.

**Bueno saber: medidas de compensación**

La CMA, que solicita un certificado de reconocimiento de la cualificación profesional, notifica al solicitante su decisión de que realice una de las medidas de compensación. Esta Decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son imprescindibles para ejercer en Francia.

A continuación, el solicitante debe elegir entre un curso de ajuste de hasta tres años o una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final del curso de ajuste, el solicitante envía al CMA un certificado que certifica que ha completado válidamente esta pasantía, acompañado de una evaluación de la organización que lo supervisó. La CMA emite, sobre la base de este certificado, un certificado de cualificación profesional en el plazo de un mes.

La decisión de utilizar una medida de indemnización podrá ser impugnada por el interesado, que deberá presentar un recurso administrativo ante el prefecto en el plazo de dos meses a partir de la notificación de la decisión. Si su apelación es desestimada, puede iniciar una impugnación legal.

*Para ir más allá* Artículos 3 y 3-2 del Decreto 98-246, de 2 de abril de 1998; Artículo 6-1 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos para la aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

### c. Condiciones de honorabilidad e incompatibilidad

Nadie puede ejercer una profesión de ingeniería climática si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 de la Ley 96-603, de 5 de julio de 1996.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Siga el curso de preparación de la instalación (SPI)

El curso de preparación de la instalación es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

#### Condiciones de la pasantía

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

#### Excepcional aplazamiento del inicio de la pasantía

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

#### El resultado de la pasantía

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

#### Costo

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

#### Caso de exención de pasantías

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

#### Exención de pasantías para nacionales de la UE o del EEE

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que:

- han ejercido una actividad profesional que requiere un nivel de conocimientos equivalente al proporcionado por las prácticas durante al menos tres años;
- o que tengan conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que cubra, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con las Francia para la gestión de una empresa de artesanía.

#### Condiciones de la exención de prácticas

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas. Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982; Artículo 6-1 del Decreto 83-517 de 24 de junio de 1983.

### b. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

#### Autoridad competente

La CMA del lugar en el que el nacional desea llevar a cabo la prestación es competente para emitir la declaración previa de actividad.

#### Documentos de apoyo

La solicitud de un informe previo de la actividad va acompañada de un archivo completo que contiene los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE;
- un documento que justifique la cualificación profesional del nacional que puede ser, a su elección:- Una copia de un diploma, título o certificado,
  - Un certificado de competencia,
  - cualquier documentación que acredite la experiencia profesional del nacional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tenga en cuenta que**

Cuando el expediente está incompleto, la CMA tiene un plazo de quince días para informar al nacional y solicitar todos los documentos que faltan.

#### Resultado del procedimiento

Al recibir todos los documentos en el archivo, el CMA tiene un mes para decidir:

- autorizar la prestación cuando el nacional justifique tres años de experiencia laboral en un Estado de la UE o del EEE, y adjuntar a dicha Decisión un certificado de cualificación profesional;
- o autorizar la disposición cuando las cualificaciones profesionales del nacional se consideren suficientes;
- o imponerle una prueba de aptitud cuando existan diferencias sustanciales entre las cualificaciones profesionales del nacional y las exigidas en Francia. En caso de negativa a realizar esta medida de compensación o si no cumple, el nacional no podrá prestar el servicio en Francia.

El silencio guardado por la autoridad competente en estos tiempos merece autorización para iniciar la prestación de servicios.

*Para ir más allá* Artículo 2 del Decreto de 2 de abril de 1998; Artículo 2 del Decreto de 17 de octubre de 2017 relativo a la presentación de la declaración y solicitudes previstas en el Decreto 98-246, de 2 de abril de 1998, y en el título I del Decreto 98-247, de 2 de abril de 1998.

### c. Si es necesario, solicite un certificado de reconocimiento de la cualificación profesional

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

#### Autoridad competente

La solicitud debe dirigirse a la ACM territorialmente competente.

#### Procedimiento

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA pide al interesado que lo complete dentro de una quincena de la presentación del expediente. Se emite un recibo tan pronto como se completa el archivo.

#### Documentos de apoyo

La carpeta debe contener las siguientes partes:

- Solicitar un certificado de cualificación profesional
- Un certificado de competencia o título de diploma o designación de formación profesional;
- Prueba de la nacionalidad del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (Ciep) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**Qué saber**

Si es necesario, todos los documentos justificativos deben traducirse al francés.

#### Tiempo de respuesta

Dentro de los tres meses siguientes a la recepción, la CMA podrá:

- Reconocer la cualificación profesional y emitir certificado de cualificación profesional;
- decidir someter al solicitante a una medida de compensación y notificarle dicha decisión;
- negarse a expedir el certificado de cualificación profesional.

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

#### Remedios

Si la CMA se niega a emitir el reconocimiento de la cualificación profesional, el solicitante podrá iniciar, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, una impugnación legal ante el tribunal administrativo pertinente. Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de indemnización, primero debe iniciar un recurso agraciado ante el prefecto del departamento en el que se basa la CMA, en el plazo de dos meses a partir de la notificación de la decisión. Cma. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### d. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable referirse a las "Formalidades de la Declaración de la Empresa Artesanal.

### e. Caso especial de manipulación de hidrofluorocarbono (HFC)

El Reglamento nacional sobre gases de efecto invernadero tiene por objeto definir las modalidades concretas para la aplicación del Reglamento (UE) 517/2014.

Está contenida esencialmente en los artículos R. 543-75 R. 543-123 del Código de Medio Ambiente y en los autos de 29 de febrero de 2016.

#### Gases de efecto invernadero fluorados: el papel de los operadores

Los operadores que manipulen fluidos y equipos deben:

- Obtener un "certificado de capacidad" para su negocio de un organismo acreditado;
- asegurarse de que sus manejadores de fluidos obtengan un certificado de aptitud o un certificado (a título personal). El certificado de aptitud es necesario para la manipulación de gases fluorados de efecto invernadero para la producción en frío y aire acondicionado. El certificado es necesario para la manipulación de gases fluorados de efecto invernadero para su uso en extinción, dieléctrico o disolvente. El certificado de aptitud puede obtenerse ya sea a través de la formación inicial seguida por la persona, o a través de una formación profesional reconocida, o de un organismo evaluador certificado (véase más adelante: "Gases fluorados de efecto invernadero: organismos evaluadores certificados"). Los avisos al periódico oficial especifican las formaciones iniciales y profesionales reconocidas. Estas revisiones se actualizan regularmente, se pueden ver mediante la búsqueda en el sitio [legifrance.gouv.fr](http://www.legifrance.gouv.fr). Los certificados pueden obtenerse de organismos acreditados;
- después de cada intervención, una hoja de intervención y un deslizamiento de seguimiento de residuos (peligrosos) para residuos líquidos. Estos dos documentos se fusionan cuando los residuos se eliminan sin mezclarse con otros residuos;
- informar a Ademe de la cantidad de líquidos manejados y recuperados;
- Aplique un macarrobo de color al equipo después de una comprobación de impermeabilización para determinar si el equipo está sellado y permite que el equipo se reinicie.

#### Gases de efecto invernadero fluorados: el papel de los organismos acreditados

Problema de las organizaciones aprobadas:

- certificaciones de capacidad para empresas que manipulan refrigerantes de invernadero fluorados, independientemente del uso de estos fluidos. Estos certificados son válidos por un período de 5 años. Durante este tiempo, las organizaciones deben llevar a cabo una o más auditorías in situ;
- certificados para los empleados de estas empresas cuando el uso de gases fluorados de efecto invernadero es extinción, dieléctrico o disolventes. En otros casos (uso en frío y aire acondicionado), es un certificado de idoneidad que se requiere (ver arriba "Gas de efecto invernadero fluorado: papel de los operadores"). Se puede obtener de evaluadores certificados (ver infra "Fluorinated Greenhouse Gas: Certified Evaluators").

Para el uso de gases fluorados de efecto invernadero para el frío, las seis organizaciones siguientes están aprobadas para la emisión de certificados de capacidad, a partir del 1 de enero de 2018:

| Cuerpo aprobado                                | Categoría de negocio |
|------------------------------------------------|----------------------|
| Certificación Afnor                            | Ⅴ                    |
| Certificación Bureau Veritas                   | I, II, III IV y V    |
| Cemafroid                                      | I, II, III IV y V    |
| Certificación Dekra                            | Ⅴ                    |
| Qualiclimafroid                                | I, II, III y IV      |
| Servicio de Certificación Internacional de SGS | I, II, III IV y V    |
| Calificación Socotec Australia                 | I, II, III IV y V    |

#### Gases de efecto invernadero fluorados: evaluadores certificados

Los evaluadores emiten certificados individuales de aptitud a los empleados que trabajan en una empresa que manejan gases de efecto invernadero fluorados para uso en frío y aire acondicionado. La propia empresa debe tener un certificado de capacidad, que obtiene de un organismo acreditado.

Los evaluadores debidamente certificados para expedir los certificados de aptitud se mencionan en el[lista de evaluadores compilada por el Ministerio de Transición Ecológica y Solidaria](http://www.ecologique-solidaire.gouv.fr/sites/default/files/Fluides%20frigorig%C3%A8nes%20-%20liste%20des%20organismes%20%C3%A9valuateurs-1.pdf).

*Para ir más allá* :[Sitio web del Ministerio de Transición Ecológica y Solidaria](https://www.ecologique-solidaire.gouv.fr/substances-impact-climatique-fluides-frigorigenes#e3).

