﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS018" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentación" -->
<!-- var(title)="Catering tradicional" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentacion" -->
<!-- var(title-short)="catering-tradicional" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/alimentacion/catering-tradicional.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="catering-tradicional" -->
<!-- var(translation)="Auto" -->


Catering tradicional
====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La restauración tradicional se refiere a los establecimientos que sirven comidas y bebidas que se consumirán exclusivamente in situ a su retribución.

*Para ir más allá* Artículo L. 3331-2 del Código de Salud Pública.

### b. Centro competente de formalidades comerciales (CFE)

El negocio del flujo de bebidas es de naturaleza comercial. Por lo tanto, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

Por otro lado, cuando la actividad principal del restaurador es de naturaleza agrícola, la CFE competente es la cámara de agricultura.

*Para ir más allá* Artículo R. 123-3 del Código de Comercio.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

#### Entrenamiento de higiene

No se requiere diploma para abrir o administrar un restaurante. Sin embargo, los establecimientos comerciales tradicionales de servicio de alimentos deben tener al menos una persona en su fuerza de trabajo que pueda justificar la formación en higiene alimentaria.

**Tenga en cuenta que**

Se considera que han cumplido con esta obligación de formación:

- Individuos que pueden justificar al menos tres años de experiencia profesional con una empresa de alimentos como gerente u operador;
- titulares de diplomas de Nivel V y títulos profesionales listados en el directorio nacional de certificaciones profesionales.

La capacitación puede ser impartida por cualquier organización de capacitación declarada al prefecto regional. Tiene una duración de 14 horas y tiene como objetivo adquirir la capacidad necesaria para organizar y gestionar las actividades de restauración en condiciones higiénicas de acuerdo con las expectativas de la normativa y ofreciendo satisfacción al cliente.

Al final de la formación, los alumnos deben ser capaces de:

- Identificar los principales principios de regulación en relación con la restauración comercial;
- analizar los riesgos asociados con la mala higiene en la restauración comercial;
- para aplicar los principios de higiene en la restauración comercial.

Para alcanzar estos objetivos, la formación se centra en:

- Alimentos y riesgo para el consumidor
- Los fundamentos de la normativa nacional y de la UE (catering comercial dirigido);
- Plan de control de salud.

*Para ir más allá* Artículos L. 233-4, D. 233-11 y siguientes del Código Rural y de la pesca marina; decreto de 5 de octubre de 2011 relativo a las especificaciones de la formación específica en higiene alimentaria adaptadas a la actividad de los establecimientos de restauración comercial.

### b. Para los puntos de venta de bebidas, requisito de nacionalidad

No hay ningún requisito de nacionalidad para abrir un restaurante tradicional. Sin embargo, una vez que el restaurador sirve bebidas alcohólicas, debe justificar que:

- Francés;
- un nacional de un Estado de la Unión Europea (UE) o un Estado Parte en el Acuerdo del Espacio Económico Europeo (EEE);
- un nacional de un Estado que tiene un tratado recíproco con Francia, como Argelia o Canadá, por ejemplo.

Las personas de otra nacionalidad no podrán, bajo ninguna circunstancia, abrir un restaurante que sirva bebidas alcohólicas.

Para obtener más información, es aconsejable consultar el listado[Beber](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*Para ir más allá* Artículo L. 3332-3 del Código de Salud Pública.

### c. Para puntos de venta de bebidas, condiciones de honor e incompatibilidad

No hay ninguna condición de honorabilidad o incompatibilidad para abrir un restaurante tradicional. Por otra parte, mientras el restaurador sirva bebidas alcohólicas, deberá respetar las condiciones de honor y las incompatibilidades relativas a la profesión de bebida.

#### Incompatibilidades y discapacidades

Los menores no emancipados y los adultos bajo tutela no pueden ejercer la profesión de beber por su cuenta.

**Tenga en cuenta que**

La práctica de beber por un menor no emancipado o por un mayor bajo tutela es un delito castigado con una multa de 3.750 euros. El tribunal puede ordenar el cierre de la instalación por un máximo de cinco años.

Además, el ejercicio de determinadas profesiones es incompatible con el funcionamiento de un establecimiento de bebida: alguaciles, notarios, funcionarios.

*Para ir más allá* Artículos L. 3336-1 y L. 3352-8 del Código de Salud Pública.

#### Integridad

No utilice puntos de venta de bebidas en el hotel:

- personas condenadas por delitos comunes o proxenetismo. En este caso, la discapacidad es perpetua;
- personas condenadas a al menos un mes de prisión por robo, fraude, violación de confianza, recepción, hilado, recepción de delincuentes, indecencia pública, celebración de una casa de apuestas, apuestas ilegales en carreras de caballos, venta de bienes falsificados o nocivos, delitos de drogas o por re-agresión y embriaguez pública. En estos casos, la discapacidad cesa en caso de indulto y cuando la persona no ha incurrido en una pena de prisión correccional durante los cinco años siguientes a la condena.

También puede imponerse incapacidad a los condenados por el delito de poner en peligro a los menores.

Las condenas por delitos y por los delitos antes mencionados contra un adeudo que consume bebidas que se consuman in situ, implican, como resultado, legítimamente en su contra y por el mismo período de tiempo, la prohibición de operar un adeudo, desde el día en que las condenas se han convertido en definitivas. Este adeudado no puede ser utilizado, en ningún cargo, en el establecimiento que operaba, ni en el establecimiento operado por su cónyuge separado. Tampoco puede trabajar al servicio de la persona a la que ha vendido o alquilado, ni por quien gestiona dicho establecimiento.

**Tenga en cuenta que**

La violación de estas prohibiciones es un delito castigado con una multa de 3.750 euros y el cierre permanente del establecimiento.

*Para ir más allá* Artículos L. 3336-1 a L. 3336-3 y L. 3352-9 del Código de Salud Pública; Artículos 225-5 y siguientes del Código Penal.

### d. Si es necesario, obtenga una licencia para operar

Cualquier persona que desee abrir un establecimiento con una "licencia de restaurante pequeño" o "licencia de restaurante" debe someterse a una formación específica que incluya:

- conocimiento de la legislación y reglamentos aplicables a los puntos de venta de bebidas y restaurantes en el lugar;
- obligaciones de salud pública y orden público.

Esta formación es obligatoria y da lugar a la emisión de una licencia de explotación de 10 años. Al final de este período, la participación en la formación de actualización de conocimientos amplía la validez de la licencia de explotación por otros 10 años.

Esta formación también es obligatoria en caso de transferencia (cambio de propiedad), traducción (movimiento) o transferencia de una comuna a otra.

El programa de formación consiste en enseñanzas de una duración mínima de:

- 20 horas repartidas en al menos tres días;
- 6 horas si la persona está justificada por diez años de experiencia profesional como operador;
- 6 horas para la formación de actualización de conocimientos.

Para obtener más información, es aconsejable consultar el listado[Beber](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*Para ir más allá* Artículos L. 3332-1-1 y R. 3332-4-1 y los siguientes del Código de Salud Pública; decreto de 22 de julio de 2011 por el que se establece el programa y la organización de la formación necesaria para obtener los certificados previstos en el artículo R. 3332-4-1 del Código de Salud Pública.

### e. Si es necesario, obtenga una licencia

La licencia depende de la categoría de alcohol vendido y varía en función de si las bebidas se consumen en el lugar o se venden como comida para llevar.

Los puntos de venta de bebidas en el lugar se dividen en dos categorías en función del alcance de la licencia a la que estén asociados:

- La licencia de 3a categoría, conocida como "licencia restringida", incluye la autorización para vender bebidas in situ de los grupos uno y tres;
- La licencia de 4a categoría, conocida como "licencia de gran tamaño" o "licencia de año completo", incluye la autorización para vender todas las bebidas que quedan permitidas para el consumo en interiores, incluidas las del grupo 4o y 5o.

**Tenga en cuenta que**

Las licencias de segundo nivel se convierten en el derecho completo de las licencias de 3a categoría.

Los restaurantes que no posean una licencia de bebidas en el lugar deben tener una de las dos categorías de licencias para vender bebidas alcohólicas:

- la "licencia de restaurante pequeño" que permite la venta de las bebidas del 3er grupo para su consumo in situ, pero sólo con motivo de las principales comidas y como accesorios de alimentación;
- la "licencia de restaurante" en sí, que permite que todas las bebidas que están permitidas para ser consumido en el lugar, pero sólo con motivo de las comidas principales y como accesorios de alimentos, para ser vendidos en el sitio.

Los establecimientos con licencia de consumidor o licencia de restaurante en el lugar pueden vender bebidas en la categoría de su licencia para llevar.

*Para ir más allá* Artículo 502 del Código Tributario General; Artículos L. 3333-1 y siguientes del Código de Salud Pública.

**Clasificación de bebidas en cuatro grupos**

Las bebidas se dividen en cuatro grupos para la regulación de su fabricación, venta y consumo:

- refrescos (grupo 1): agua mineral o carbonatada, zumos de frutas o vegetales que no fermentan o no contienen, tras el inicio de la fermentación, trazas de alcohol superiores a 1,2 grados, limonadas, jarabes, infusiones, leche, café, té, chocolate;
- bebidas fermentadas destiladas y vinos dulces naturales (grupo 3): vino, cerveza, sidra, pera, aguamiel, a los que se unen vinos dulces naturales, así como cremas de grosella negra y zumos de frutas o verduras fermentados con 1,2 a 3 grados de alcohol, vinos de licor, aperitivos a base de vino y licores de fresa, frambuesas, cubiertas negras o cerezas, con no más de 18 grados de alcohol puro;
- grupo 4: rones, tafias, licores procedentes de la destilación de vinos, sidras, peras o frutas, y sin apoyar ninguna adición de gasolina, así como licores edulcorados con azúcar, glucosa o miel a un mínimo de 400 gramos por litro para licores anisados y un mínimo de 200 gramos por litro para otros licores y que no contengan más de medio gramo de gasolina por litro;
- Grupo 5: todas las demás bebidas alcohólicas.

### f. Algunas peculiaridades de la regulación de la actividad

#### Cumplir con las regulaciones de las instituciones públicas (ERP)

Es aconsejable referirse a la lista[Instituciones que reciben el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

#### Respetar las normas sanitarias y de higiene

El restaurador debe aplicar las normas del Reglamento CE 852/2004, de 29 de abril de 2004, relativas a la higiene alimentaria, que se refieren a:

- Las instalaciones utilizadas para la alimentación;
- Equipo
- Desperdicio de alimentos
- Suministro de agua
- Higiene personal
- alimentos, envases y envases.

El restaurador también debe aplicar las normas establecidas en el orden del 21 de diciembre de 2009 sobre normas sanitarias para la venta al por menor, el almacenamiento y el transporte de productos animales y alimentos en Contiene.

Estos incluyen:

- Condiciones de temperatura de conservación
- Procedimientos de congelación
- cómo se preparan las carnes molidas y se recibe la caza.

#### Ver precios

Las instalaciones, incluidos los operadores hoteleros, que sirven comidas, alimentos o bebidas para consumir en el lugar, están obligadas a mostrar los precios realmente pagaderos por el consumidor.

**Tenga en cuenta que**

En los establecimientos donde se recoge un servicio, el precio registrado está incluido en los impuestos y el servicio. Los documentos publicados o puestos a disposición de los clientes deben incluir las palabras "Precio de servicio incluido", seguidas de las instrucciones, entre paréntesis, de la tarifa cobrada por la remuneración de este servicio.

##### Bebidas y alimentos más comúnmente servidos

Los restauradores deben mostrar de manera visible y legible desde fuera de la propiedad y en las parcelas al aire libre reservadas para los clientes, los precios cobrados, independientemente del lugar de consumo, de las bebidas y productos alimenticios más comunes a continuación y nombrado:

- La taza de café negro
- medio montón de cerveza de barril;
- una botella de cerveza (contenida servida);
- zumo de fruta (contención servida);
- un refresco (contenido)
- agua mineral plana o espumosa (contenida)
- aperitivo anís (contenido servido);
- un plato del día;
- Un sándwich.

El nombre y los precios deben indicarse mediante letras y figuras con una altura mínima de 1,5 cm.

**Es bueno saber**

Para los establecimientos que ofrecen instalaciones o servicios de entretenimiento como entretenimiento y música, se muestran, de forma visible y legible desde el exterior, en lugar de estas menciones los precios de los siguientes servicios designados:

- entrada y, si el precio de la misma incluye una bebida, la naturaleza y la capacidad de la misma;
- un refresco (naturaleza y contenido servido);
- una bebida alcohólica servida por la copa (naturaleza y capacidad servida);
- una botella de whisky (marca y capacidad);
- una botella de vodka o ginebra (marca y capacidad);
- una botella de champán (marca y capacidad).

Dentro del establecimiento, la pantalla consiste en la indicación de un documento expuesto a la vista del público y directamente legible por la clientela de la lista elaborada por partida, las bebidas y productos ofrecidos para la venta y el precio de cada servicio.

En los restaurantes y para las bebidas servidas durante las comidas, estos documentos pueden ser reemplazados por una tarjeta puesto a disposición de los clientes y que contiene los precios de todos los servicios ofrecidos. Esta tarjeta puede ser un documento separado del menú y, si es necesario, puede ser legible en la parte posterior del menú.

*Para ir más allá* : orden de 27 de marzo de 1987 relativo a la exhibición de precios en los establecimientos que sirven comidas, productos alimenticios o bebidas que se consumirán en el establecimiento.

##### Menús y mapas

En los establecimientos que sirven comidas, menús o menús del día, así como un menú con al menos los precios de cinco vinos o de otro modo los precios de los vinos si se sirven menos de cinco, debe mostrarse de forma visible y legible desde el exterior durante duración del servicio y al menos a partir de las 11:30 a.m. para el almuerzo y las 6 p.m. para la cena.

En el caso de que ciertos menús se sirvan sólo en ciertas horas del día, esta característica debe mencionarse claramente en el documento mostrado.

En los establecimientos no vitivinícolas, se mostrará un menú con al menos la naturaleza y los precios de cinco bebidas de servicio común.

Dentro de estos establecimientos, los menús o tarjetas idénticas a las que se muestran fuera deben estar a disposición de los clientes.

Las tarjetas y menús deben incluir, para cada servicio, el precio, así como la palabra "bebida incluida" o "bebida no incluida" y, en todos los casos, indicar para las bebidas la naturaleza y capacidad ofrecidas.

*Para ir más allá* : orden de 27 de marzo de 1987 relativo a la exhibición de precios en los establecimientos que sirven comidas, productos alimenticios o bebidas que se consumirán en el establecimiento.

#### Ver "hecho en casa"

Los restauradores deben indicar en sus tarjetas o cualquier otro medio visible para todos los consumidores que un plato propuesto es "hecho en casa" por la siguiente declaración: los platos "caseros" se hacen en el lugar a partir de productos crudos.

**Es bueno saber**

El establecimiento sirve platos caseros elaborados con productos crudos. Un producto crudo es un producto alimenticio que no ha sufrido ningún cambio significativo, incluyendo calefacción, amarre, montaje o una combinación de estos procesos.

*Para ir más allá* Artículos L. 122-19 y siguientes, D. 121-13-1 y los siguientes del Código del Consumidor.

#### Mostrar el origen de las carnes

El origen de la carne de vacuno deberá indicarse mediante cualquiera de los siguientes documentos:

- "origen: (nombre del país)" cuando el nacimiento, la cría y el sacrificio del ganado del que proceden las carnes tuvieron lugar en el mismo país;
- "nacidos y criados: (nombre del país de nacimiento y nombre del país de cría) y sacrificados: (nombre del país de sacrificio)" cuando el nacimiento, la cría y el sacrificio tuvieron lugar en diferentes países.

Estas menciones se hacen a la atención del consumidor, de una manera legible y visible, por visualización, indicación en tarjetas y menús, o en cualquier otro medio.

**Tenga en cuenta que**

La venta de carne de vacuno cuyo origen no se da a conocer al consumidor constituye un billete de 5a clase. La multa es de 1.500 euros como máximo y, en caso de reincidencia, 3.000 euros.*Para ir más allá* Artículo 131-13 del Código Penal.

*Para ir más allá* : decreto de 17 de diciembre de 2002 sobre el etiquetado de la carne de vacuno en los establecimientos de servicios alimentarios.

#### Mostrar el uso de ingredientes que pueden causar alergias o intolerancias

En los lugares donde se ofrecen comidas para ser consumidas en el lugar, se pone en conocimiento del consumidor, en forma escrita, de una manera legible y visible de los lugares donde se admite al público:

- información sobre el uso de productos alimenticios que causen alergias o intolerancias;
- cómo se ponen a su disposición esta información. En este caso, el consumidor puede acceder a la información directa y libremente, disponible por escrito.

*Para ir más allá* Artículos R. 412-12 y siguientes del Código del Consumidor.

#### Cumplir con las regulaciones sobre la supresión de la embriaguez pública y la protección de los menores

##### Prohibiciones de venta de bebidas

Está prohibido:

- para vender bebidas alcohólicas a menores de edad o para ofrecerlos de forma gratuita. La persona que emite la bebida requiere que el cliente establezca una prueba de su mayoría;
- ofrecer, libre o caro, a un menor cualquier objeto que incite directamente al consumo excesivo de alcohol también está prohibido;
- venta al por menor a crédito para los grupos 3o, 4o y 5o para ser consumido en el lugar o para llevar;
- ofrecen bebidas alcohólicas gratuitas a voluntad con fines comerciales o venderlas como principal por una suma global, excepto en el contexto de partes y ferias tradicionales declaradas o fiestas de noticias y ferias autorizadas por el Representante estatal en el departamento o cuando se trata de degustaciones para la venta;
- ofrecer bebidas alcohólicas con descuento por un período limitado de tiempo sin ofrecer también bebidas sin alcohol a precio reducido.

*Para ir más allá* Artículos L. 3342-1, L. 3342-3 y L. 3322-9 del Código de Salud Pública.

##### Mostrar refrescos

En todos los puntos de venta de bebidas, es obligatorio exhibir bebidas no alcohólicas para la venta en el establecimiento. La pantalla debe incluir al menos diez botellas o recipientes y, siempre que se suministe la salida, una muestra de al menos cada categoría de las siguientes bebidas:

- jugos de frutas, jugos de verduras;
- bebidas de jugo de fruta carbonatado;
- sodas;
- limonados;
- jarabes;
- aguas ordinarias artificiales o sin reasidarse;
- aguas minerales carbonatadas o no carbonatadas.

Esta pantalla, separada de la de otras bebidas, debe instalarse de forma destacada en las instalaciones donde se sirve a los consumidores.

*Para ir más allá* Artículo L. 3323-1 del Código de Salud Pública.

##### Mostrar disposiciones sobre la supresión de la embriaguez pública y la protección de los menores

Los restauradores que vendan bebidas alcohólicas deben colocar un cartel que les recuerde las disposiciones del Código de Salud Pública relativas a la supresión de la embriaguez pública y la protección de los menores.

*Para ir más allá* Artículos L. 3342-4 del Código de Salud Pública.

#### Respetar la prohibición de fumar

No está permitido fumar en los restaurantes. La prohibición de fumar se aplica en todas las áreas cerradas y cubiertas que están abiertas al público o que son lugares de trabajo.

La señalización aparente debe recordar el principio de una prohibición de fumar.

*Para ir más allá* Artículos L. 3512-8, R. 3512-2 y R. 3512-7 del Código de Salud Pública.

**Terrazas**

La prohibición de fumar no se aplica a las terrazas que se encuentran en las aceras de una vía pública donde hay mesas y sillas disponibles para los consumidores frente a un establecimiento. Así que estos son espacios al aire libre.

Se consideran espacios al aire libre:

- las terrazas totalmente descubiertas, incluso si se cerrarían en sus lados;
- terrazas cubiertas, pero cuyo lado principal estaría completamente abierto (generalmente la fachada delantera).

En cualquier caso, la terraza debe estar separada físicamente del interior de la propiedad. Por lo tanto, está prohibido fumar en una "terraza" que sólo sería una extensión del establecimiento, de la que ninguna partición lo separaría.

*Para ir más allá* : circular de 17 de septiembre de 2008 No 2008-292 sobre cómo aplicar la segunda fase de la prohibición de fumar en lugares de uso colectivo.

**Ubicaciones solo para fumadores**

El gerente del establecimiento puede decidir crear sitios solo para fumadores. Estos lugares reservados son habitaciones cerradas, utilizadas para el consumo de tabaco y en las que no se prestan servicios. No se pueden realizar tareas de mantenimiento y mantenimiento sin que se renueve el aire, en ausencia de ningún ocupante, durante al menos 1 hora.

Estas ubicaciones deben:

- estar equipado con un dispositivo de extracción de aire de ventilación mecánica que permite una renovación mínima del aire de diez veces el volumen del sitio por hora. Este dispositivo es completamente independiente del sistema de ventilación o aire acondicionado del edificio. La habitación se mantiene en depresión continua de al menos cinco pascuas en comparación con las partes de conexión;
- Tener cierres automáticos sin posibilidad de apertura involuntaria
- No ser un lugar de paso;
- tienen una superficie no superior al 20% de la superficie total de la instalación en la que se desarrollan los emplazamientos sin el tamaño de un emplazamiento superior a 35 metros cuadrados.

*Para ir más allá* Artículos R. 3512-3 y R. 3512-4 del Código de Salud Pública.

#### Si es necesario, solicite permiso para transmitir música

Los restaurantes que deseen transmitir música sonora deben obtener el permiso de la Sociedad de Autores, Compositores y Editores de Música (Sacem).

Deben pagarle dos regalías:

- Derechos de autor que pagan por el trabajo de creadores y editores;
- para la radiodifusión a través de los medios de comunicación grabados, la remuneración justa, destinada a distribuirse entre los artistas intérpretes o ejecutantes y los productores de música. Sacem cobra esta tarifa en nombre de la Fair Compensation Collection Corporation (SPRE).

La cantidad de derechos de autor depende de:

- La comuna en la que se encuentra el establecimiento;
- El número de escaños
- El número de dispositivos de difusión instalados.

La remuneración justa está determinada por el número de escaños y el número de habitantes de la comuna del establecimiento.

Para obtener más información, consulte el sitio web oficial de la [Sacem](http://www.sacem.fr) y el [Spre](http://www.spre.fr).

*Para ir más allá* : decisión de 30 de noviembre de 2011 de la comisión en virtud del artículo L. 214-4 del Código de Propiedad Intelectual por la que se modifica la decisión de 5 de enero de 2010.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Para dar al establecimiento una existencia jurídica, el operador debe declarar su apertura a la CFE para estar inscrito en el Registro mercantil y de sociedades (RCS). Para más información, es aconsejable referirse a la hoja de formalidad "Declaración de Actividad Comercial".

#### b. Declarar establecimientos que distribuyan productos alimenticios que contengan productos animales

Cualquier operador de un establecimiento que produzca, manipule o almacene productos animales o alimenticios que contengan ingredientes animales (carne, productos lácteos, productos pesqueros, huevos, miel), destinados al consumo debe cumplir con la obligación de declarar cada uno de los establecimientos de los que es responsable, así como las actividades que se llevan a cabo allí.

**Autoridad competente**

El prefecto del departamento de aplicación del establecimiento o, para los establecimientos bajo la autoridad o tutela del Ministro de Defensa, el servicio de salud de las fuerzas armadas, de acuerdo con los términos previstos por el decreto del Ministro de Defensa.

**Documentos de apoyo**

El formulario Cerfa 1398403 lleno.

*Para ir más allá* Artículo 6 del Reglamento (CE) 852/2004; Artículo R. 233-4 del Código Rural y de la pesca marina; orden de 28 de junio de 1994 relativa a la identificación y acreditación sanitaria de establecimientos en el mercado de productos animales o animales y marcas de seguridad.

### c. Si es necesario, declarar la apertura del restaurante en caso de venta de alcohol

Una persona que quiere abrir un restaurante y vender alcohol allí está obligado a hacer una declaración preadministrativa.

Este requisito de notificación previa también se aplica en el caso de:

- Cambio en la persona del propietario o gerente
- traducción de un lugar a otro.

**Tenga en cuenta que**

Abrir una licorería sin hacer esta declaración previa en las formas requeridas es un delito castigado con una multa de 3.750 euros.

*Para ir más allá* Artículo L. 3352-3 del Código de Salud Pública.

Por otra parte, la traducción en el territorio de un municipio de un flujo ya existente no se considera como la apertura de un nuevo adeo:

- si es llevado a cabo por el propietario del fondo de comercio o sus legítimos propietarios y si no aumenta el número de adeudos existentes en ese municipio;
- si no se opera en un área protegida.

**Es bueno saber**

Este requisito de preinformación no se aplica en los departamentos del Alto Rin, el Rin inferior y el Mosela. En estos departamentos, el artículo 33 del Código Local de Profesiones, de 26 de julio de 1900, sigue en vigor. Por lo tanto, depende del conservador completar un formulario de solicitud para operar una licencia de licor disponible en los departamentos de prefectura y subprefectura de estos tres departamentos.

**Autoridad competente**

La autoridad competente es:

- El ayuntamiento de la comuna del sitio del sitio;
- en París, la prefectura de policía.

**hora**

La declaración debe hacerse al menos 15 días antes de la apertura. En el caso de una mutación por muerte, el período de notificación es de un mes.

Inmediatamente se emite un recibo.

Dentro de los tres días siguientes a la declaración, el alcalde de la comuna donde se hizo envía una copia completa al fiscal y al prefecto.

**Documentos de apoyo**

Los documentos justificativos necesarios son:

- Impresión de ciervos 11542Completado;
- La licencia de explotación
- Licencia
- prueba de nacionalidad.

Para obtener más información, es aconsejable consultar el listado[Beber](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*Para ir más allá* Artículos L. 3332-3 y siguientes del Código de Salud Pública.

