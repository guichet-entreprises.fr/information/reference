﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS007" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentación" -->
<!-- var(title)="Charcutier" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentacion" -->
<!-- var(title-short)="charcutier" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/alimentacion/charcutier.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="charcutier" -->
<!-- var(translation)="Auto" -->

Charcutier
==========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El carnicero es un profesional en la preparación y venta de productos de cerdo y delicatessen. En términos más generales, puede preparar productos crudos o cocidos y platos a base de carnes (jamones, patés, embutidos, etc.), condimentos, pescados, etc.

**Tenga en cuenta que**

La actividad de la carne de delicatessen se distingue de la de carnicero que interviene en la transformación y venta de carne. Para obtener más información, es aconsejable consultar la hoja de actividades "Carnicero". Además, el ejercicio no sedentario de la actividad del delicatessen está sujeto a la obtención de otras autorizaciones (para más información, es aconsejable consultar la hoja de actividades[Comerciante artesano viajero](https://www.guichet-entreprises.fr/fr/activites-reglementees/negoce-et-commerce-de-biens/commercant-artisan-ambulant/)).

*Para ir más allá* Ley 96-603, de 5 de julio de 1996, sobre el desarrollo y la promoción del comercio y la artesanía.

### b. Centro competente de formalidad empresarial (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

Si el profesional tiene una actividad de compra y reventa, su actividad es artesanal y comercial.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Sólo una persona cualificada que esté cualificada profesionalmente o puesta bajo el control efectivo y permanente de una persona cualificada puede llevar a cabo la actividad de una delicatessen.

Las personas que lleven a cabo o controlen la actividad de la delicatessen deben ser los titulares, de su elección:

- Un Certificado de Cualificación Profesional (CAP);
- Una Patente de Estudios Profesionales (BEP);
- un diploma o un grado de igual o nivel superior aprobado o registrado en el momento de su emisión a la[directorio nacional de certificaciones profesionales](%5b*http://www.rncp.cncp.gouv.fr/*%5d(http://www.rncp.cncp.gouv.fr/)).

el[sitio web de la Comisión Nacional de Certificación Profesional](http://www.cncp.gouv.fr/) (CNCP) ofrece una lista de todas estas cualificaciones profesionales. Estos incluyen el "deli catering deli" de la CAP, el bac pro "butcher caterer" o la patente profesional (BP) "butcher - caterer".

En ausencia de uno de estos diplomas, la persona debe justificar una experiencia profesional efectiva de tres años en un Estado de la Unión Europea (UE) o parte en el acuerdo del Espacio Económico Europeo (EEE), adquirido como líder por cuenta propia o por cuenta propia en el trabajo de una delicatessen. En este caso, el interesado puede solicitar a la CMA pertinente un certificado de cualificación profesional.

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 1 del Decreto 98-246, de 2 de abril de 1998; Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

#### En caso de servicio gratuito (LPS)

El profesional que sea miembro de la UE o del EEE podrá trabajar en Francia, de forma temporal y casual, como una delicatessen con la condición de que esté legalmente establecido en uno de estos Estados para llevar a cabo la misma actividad.

Si ni la actividad ni la formación que la lleva a cabo están reguladas en el Estado del Establecimiento, la persona también debe demostrar que ha sido una delicatessen en ese estado durante al menos el equivalente a dos años a tiempo completo en los últimos diez años. años antes de la actuación que quiere actuar en Francia.

El profesional que cumpla estas condiciones está exento de los requisitos de registro del directorio de comercios o del registro de empresas.

*Para ir más allá* Artículo 17-1 de la Ley 96-603, de 5 de julio de 1996.

#### En caso de establecimiento gratuito (LE)

Para llevar a cabo una práctica permanente en Francia, la actividad de una delicatessen, el profesional nacional de la UE o el EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase infra "2o. a. Cualificaciones profesionales");
- poseer un certificado de competencia o certificado de formación requerido para el ejercicio de la actividad de delicatessen en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su disposición a llevar a cabo la actividad de carnicero cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de Esta actividad
- obtener un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona ha estado trabajando durante tres años como una carita en el Estado que ha admitido Equivalencia.

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones anteriores podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante: "3. b. Si es necesario, solicite un certificado de cualificación profesional.").

Si la persona no cumple con cualquiera de las condiciones anteriores, la CMA puede pedirle que realice una medida de compensación en los siguientes casos:

- si la duración de la formación certificada es al menos un año inferior a la requerida para obtener una de las cualificaciones profesionales requeridas en Francia para llevar a cabo la actividad de delicatessen;
- Si la formación recibida abarca temas sustancialmente diferentes de los cubiertos por uno de los títulos o diplomas necesarios para llevar a cabo la actividad de delicatessen en Francia;
- Si el control efectivo y permanente de la actividad del delicatessen requiere, para el ejercicio de algunas de sus competencias, una formación específica que no se imparta en el Estado miembro de origen y que abarque temas sustancialmente diferentes de los cubiertos por el certificado de competencia o designación de formación mencionado por el solicitante.

*Para ir más allá* Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998.

**Bueno saber: medidas de compensación**

La CMA, que solicita un certificado de reconocimiento de la cualificación profesional, notifica al solicitante su decisión de que realice una de las medidas de compensación. Esta Decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son imprescindibles para ejercer en Francia.

A continuación, el solicitante debe elegir entre un curso de ajuste de hasta tres años o una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final del curso de ajuste, el solicitante envía al CMA un certificado que certifica que ha completado válidamente esta pasantía, acompañado de una evaluación de la organización que lo supervisó. La CMA emite, sobre la base de este certificado, un certificado de cualificación profesional en el plazo de un mes.

La decisión de utilizar una medida de indemnización podrá ser impugnada por el interesado, que deberá presentar un recurso administrativo ante el prefecto en el plazo de dos meses a partir de la notificación de la decisión. Si su apelación es desestimada, puede iniciar una impugnación legal.

*Para ir más allá* Artículos 3 y 3-2 del Decreto 98-246, de 2 de abril de 1998; Artículo 6-1 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos para la aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

### c. Condiciones de honorabilidad

Nadie puede ejercer la profesión si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 III de la Ley 96-603, de 5 de julio de 1996.

### d. Algunas peculiaridades de la regulación de la actividad

#### Regulación relativa a la calidad del artesano y los títulos de maestro artesano o mejor trabajador en Francia

**Artesanía**

Las personas que tienen una cualificación profesional específica pueden reclamar automáticamente el estatus de artesano (ver arriba "2." a. Cualificación profesional").

**Qué saber**

Las personas que no tienen una de las cualificaciones profesionales requeridas pueden reclamar la condición de artesano si justifican tres años de actividad (excluyendo el aprendizaje).

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998.

**El título de maestro artesano**

Para obtener el título de maestro artesano, el interesado (una persona física o un gerente de una empresa de artesanías) debe:

- Estar registrado en el directorio de operaciones
- Tener un máster
- justifican al menos dos años de práctica profesional.

La solicitud debe dirigirse al presidente de la ACM correspondiente.

**Qué saber**

Las personas que no poseen el título de máster pueden solicitar el título de maestro artesano en dos situaciones distintas:

- si están inscritos en el directorio de oficios, tienen un título de formación equivalente al máster, y tienen conocimientos de gestión y psicopedagógicos equivalentes a las unidades de valor de la máster y que justifiquen más de dos años de práctica profesional;
- si han estado inscritos en el repertorio de comerciodurante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

En ambos casos, el título de maestro artesano puede ser otorgado por la Comisión Regional de Cualificaciones.

Para obtener más detalles, es aconsejable acercarse más a la CMA en consideración.

*Para ir más allá* Decreto 98-247 de 2 de abril de 1998.

**El título de mejor trabajador de Francia**

El título de mejor trabajador en Francia (MOF) está reservado para aquellos que han aprobado el examen llamado "uno de los mejores trabajadores de Francia". Es un diploma estatal que atestigua la adquisición de una alta cualificación en el curso de una actividad profesional.

*Para ir más allá* :[web oficial del concurso "uno de los mejores trabajadores de Francia"](http://www.meilleursouvriersdefrance.org/) ; Artículos D. 338-9 y siguientes del Código de Educación y detenidos el 27 de diciembre de 2012.

#### Cumplimiento de las normas sanitarias

En virtud de la orden del 21 de diciembre de 2009, la delicatessen está obligada a cumplir las normas sanitarias aplicables a las actividades de venta al por menor, almacenamiento y transporte de productos animales y alimentos en contenedores.

Estas normas incluyen las temperaturas de conservación de los alimentos, los métodos de descongelación, las obligaciones de carne molida y caza silvestre, las obligaciones documentales para productos, etc.

*Para ir más allá* : decreto de 21 de diciembre de 2009 relativo a las normas sanitarias aplicables a las actividades de venta al por menor, almacenamiento y transporte de productos animales y alimentos contenidos en contenedores.

#### Regulaciones de productos

El origen de la carne (carne de vacuno, porcino, ovino, avícola, etc.) debe indicarse al consumidor que debe poder identificar el lugar de origen o el país de origen de la carne.

Además, la carne debe provenir de establecimientos con licencia (o tener una renuncia a esta obligación).

Por último, las carnes de delicatessen deben cumplir las normas de etiquetado de los alimentos.

**Es bueno saber**

Se debe informar a los consumidores de cualquier presencia de sustancias o productos que causen alergias o intolerancias en la fabricación o preparación de un producto alimenticio.

*Para ir más allá* Reglamento europeo "Paquete de Higiene"; Reglamento (CE) 1760/2000 del Parlamento Europeo y del Consejo, de 17 de julio de 2000, por el que se establece un sistema de identificación del ganado bovino y del etiquetado de la carne de vacuno; Decreto No 2015-447, de 17 de abril de 2015, sobre información al consumidor sobre alérgenos y productos alimenticios no envasados; Artículo R. 412-12 y el siguiente del código de consumo.

#### Regulaciones de liquidación

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, es aconsejable consultar la hoja de actividades[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Siga el curso de preparación de la instalación (SPI)

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

- El registro se realiza previa presentación de una identificación con la ACM territorialmente competente;
- Tiene una duración mínima de 30 horas;
- Viene en forma de cursos y trabajo práctico;
- tiene como objetivo adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

Al final de la pasantía, el participante recibe un certificado de pasantía de seguimiento que debe adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación cuesta unos 236 euros en 2016.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que:

- han trabajado durante al menos tres años exigiendo un nivel de conocimientos equivalente al proporcionado por las prácticas;
- tener conocimientos adquiridos en un Estado de la UE o del EEE o un tercer país durante una experiencia profesional que cubra, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con las Francia para la gestión de una empresa de artesanía.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas. Debe acompañar su correo con los siguientes documentos justificativos: copia del diploma aprobado en el nivel III, copia del certificado de máster, prueba de una actividad profesional que requiera un nivel equivalente de conocimiento. Además, el interesado debe pagar una tasa variable (25 euros en 2016). No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982; Artículo 6-1 del Decreto 83-517, de 24 de junio de 1983.

### b. Si es necesario, solicite un certificado de reconocimiento de la cualificación profesional

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la ACM territorialmente competente.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA le pide al solicitante que lo complete dentro de los 15 días posteriores a la presentación del expediente. Se emite un recibo tan pronto como se completa el archivo.

**Documentos de apoyo**

La carpeta debe contener:

- Solicitar un certificado de cualificación profesional
- Prueba de cualificación profesional: certificado de competencia o diploma o certificado de formación profesional;
- Prueba de la nacionalidad del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (Ciep) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**Qué saber**

Si es necesario, todos los documentos justificativos deben ser traducidos al francés por un traductor certificado.

**Tiempo de respuesta**

Dentro de los tres meses siguientes a la recepción, la CMA:

- reconoce la cualificación profesional y emite la certificación de cualificación profesional;
- decide someter al solicitante a una medida de indemnización y le notifica dicha decisión;
- se niega a expedir el certificado de cualificación profesional.

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Remedios**

Si la CMA se niega a emitir el reconocimiento de la cualificación profesional, el solicitante podrá iniciar, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, una impugnación legal ante el tribunal administrativo pertinente. Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de indemnización, primero debe iniciar un recurso agraciado ante el prefecto del departamento en el que se basa la CMA, en el plazo de dos meses a partir de la notificación de la decisión. Cma. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### c. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar la hoja de actividades "Formalidades de declaración de negocios artesanales.

### d. Otros permisos

#### Hacer una declaración de manipulación de alimentos animales

Todo establecimiento que produzca o comerciale productos alimenticios que contengan productos animales deberá hacer una declaración, antes de la apertura y en cada cambio de operador, dirección o actividad.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) en la ubicación del establecimiento.

**Documentos de apoyo**

La declaración se realiza en forma de Cerfa 13984Completado, fechado y firmado.

**Costo**

Gratis.

*Para ir más allá* Artículo R. 233-4 del Código de Pesca Rural y Marina y decreto de 28 de junio de 1994 relativo a la identificación y acreditación sanitaria de establecimientos en el mercado de productos animales o animales y marcas de seguridad.

#### Si es necesario, solicite permiso para sujetar y deshuesar canales y partes de cadáveres bovinos que contengan hueso vertebral

Sólo las carnicerías autorizadas pueden contener y deshuesar cadáveres y cadáveres de ganado de más de 30 meses de edad que contengan hueso vertebral.

**Autoridad competente**

La solicitud de autorización debe dirigirse a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) en el establecimiento de la carnicería.

**Tiempo de respuesta**

El DDCSPP envía su respuesta dentro de los dos meses siguientes a la recepción de la solicitud. La falta de respuesta dentro de este plazo equivale a una denegación de autorización.

**Documentos de apoyo**

El solicitante debe solicitar una licencia, cuyo modelo figura en el Apéndice A del Apéndice V de la orden del 21 de diciembre de 2009.

**Costo**

Gratis.

*Para ir más allá* Apéndice V de la orden del 21 de diciembre de 2009 sobre normas sanitarias para el comercio minorista, almacenamiento y transporte de productos animales y alimentos en contenedores.

#### Si es necesario, solicitar la aprobación de la salud en caso de venta de alimentos a intermediarios

Los establecimientos que preparen, transforman, manipulan o almacenen productos animales (leche cruda, carnes, huevos, etc.) deben obtener la aprobación sanitaria para comercializar sus productos a otros establecimientos.

**Autoridad competente**

La solicitud de acreditación debe enviarse al SDCSPP en la ubicación del establecimiento.

**Tiempo de respuesta**

Si la solicitud de acreditación es completa y admisible, se concede una aprobación condicional por un período de tres meses al solicitante. Durante este período, se realiza un cheque oficial. Si las conclusiones de esta visita son favorables, se concede la aprobación definitiva. De lo contrario, los puntos de incumplimiento se notifican al solicitante y la aprobación condicional podrá renovarse por otros tres meses. La duración total de la aprobación condicional no podrá exceder de seis meses.

**Documentos de apoyo**

La solicitud de acreditación sanitaria se realiza utilizando el Formulario Cerfa 13983Completado, fechado y firmado y acompañado de todos los documentos justificativos enumerados en la nota DGAL/SDSSA/N2012-8119 de 12 de junio de 2012.

*Para ir más allá* : decreto de 8 de junio de 2006 relativo a la acreditación sanitaria de establecimientos en el mercado de productos animales o alimenticios que contengan productos animales; DGAL/SDSSA/N2012-8119 supra; Artículos L. 233-2 y R. 233-1 del Código Rural y Pesca Marina.

**Es bueno saber**

Es aconsejable consultar con el SDCSPP para asegurar la necesidad de solicitar la acreditación y, si es necesario, para obtener asistencia en el establecimiento del expediente de solicitud.

#### Si es necesario, solicite una exención del requisito de acreditación de salud

Algunas empresas minoristas pueden beneficiarse de una exención del requisito de acreditación de la salud. Estos incluyen negocios que venden cantidades limitadas de alimentos a otros negocios minoristas, negocios con distancias de entrega a otros negocios minoristas de no más de 80 kilómetros, etc.

Cualquier operador de una empresa minorista que suminisre bienes animales, que él mismo haya producido, a otros establecimientos comerciales, podrá solicitar una exención del deber de aprobación, si dicha actividad se lleva a cabo de manera marginal, localizado y restringido. Para obtener más información sobre las cantidades que pueden transferirse en virtud de la exención, es aconsejable remitirse a las Listas 3 y 4 del auto de 8 de junio de 2006 relativo a la acreditación sanitaria de los establecimientos que comercializan productos de origen o productos alimenticios que contengan productos animales.

**Autoridad competente**

La solicitud de exención debe dirigirse al DDCSPP del departamento en el que se encuentra la institución.

**Documentos de apoyo**

La solicitud de exención de la subvención se realiza utilizando el formulario Cerfa 13982Completado, fechado y firmado.

**Costo**

Gratis

*Para ir más allá* Artículo L. 233-2 del Código Rural y Pesca Marina; Artículos 12 y 13 del auto de 8 de junio de 2006 supra e instrucción técnica DGAL/SDSSA/2014-823 de 10 de octubre de 2014.

