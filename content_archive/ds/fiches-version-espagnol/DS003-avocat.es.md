﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS003" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sector financiero y jurídico" -->
<!-- var(title)="Abogado" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sector-financiero-y-juridico" -->
<!-- var(title-short)="abogado" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/sector-financiero-y-juridico/abogado.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="abogado" -->
<!-- var(translation)="Auto" -->


Abogado
=======

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El abogado es un profesional legal cuya misión es asistir y/o representar a las partes en caso de disputa. Es competente para presentar una solicitud ante los tribunales, órganos disciplinarios o autoridades públicas.

El abogado también está obligado a buscar soluciones amistosas, recibir asignaciones judiciales, realizar consultas legales o escribir actos para otros (actos bajo fundación privada, correo, notificación formal, etc.).

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para una profesión liberal, la CFE competente es el Urssaf;
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

La práctica de ejercer como abogado está reservada a aquellos que están en el colegio de abogados, y que:

- tener un título de maestría en derecho (B.A. 4) o un título de formación reconocido como equivalente;
- poseer el Certificado de Aptitud para la Práctica (CAPA) obtenido en un centro de formación regional, con excepciones;
- no han sido condenados penalmente por actos contrarios al honor, la probidad o la buena moral;
- no han sido objeto de sanciones disciplinarias o administrativas de despido, despido, despido, retirada de la acreditación o autorización;
- no han sido objeto de bancarrota personal.

*Para ir más allá* Artículo 11 de la Ley 71-1130, de 31 de diciembre de 1971, de reforma de determinadas profesiones judiciales y jurídicas.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Para un ejercicio temporal e informal (LPS)

Cualquier nacional de un Estado de la UE o del EEE que esté legalmente practicando como abogado en uno de estos estados puede utilizar su título profesional en Francia de forma temporal y casual.

Tendrá que representar a sus clientes de acuerdo con las reglas que se aplican a los abogados franceses.

En el caso de representación ante un Tribunal de Apelación o en el caso de un procedimiento ante el tribunal superior sujeto a representación obligatoria, el nacional tendrá que elegir la residencia con un abogado autorizado.

*Para ir más allá* Artículos 202 a 202-3 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

#### Para un ejercicio permanente (LE)

Existen dos regímenes distintos para ejercer como abogado en Francia, en virtud de dos directivas de la Unión Europea:

- En virtud de la Directiva 98/5/CE, de 16 de febrero de 1998, cualquier nacional podrá ejercer, en cualquier otro Estado miembro, las actividades de un abogado bajo su título profesional original, previa inscripción en el Consejo Nacional de Abogados. Después de 3 años de práctica bajo su título original, y bajo ciertas condiciones, cualquier nacional puede beneficiarse de la asimilación bajo el título francés (véanse los artículos 83 y siguientes de la Ley 71-1130, de 31 de diciembre de 1971, relativa a la reforma de determinadas profesiones judiciales y jurídicas), y los artículos 93-1 y 201 del Decreto No 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado);
- En virtud de la Directiva 2005/36/CE del Parlamento Europeo y del Consejo, de 7 de septiembre de 2005, relativa al reconocimiento de cualificaciones profesionales, todo nacional de la UE o del Estado del EEE podrá establecerse en Francia para ejercer como abogado si ha completado un ciclo de educación postsecundaria de al menos un año y justifica:- o un título de formación que permita ejercer la profesión en ese estado,
  - o ejercer la profesión durante al menos un año en los últimos diez años, en un estado que no regula el acceso o el ejercicio de la profesión.

Una vez que cumpla una de estas condiciones, podrá solicitar al Consejo Nacional de Abogados el reconocimiento de sus cualificaciones profesionales (véase infra "3o. c. Solicitar el reconocimiento de las cualificaciones profesionales de la UE o del EEE para un ejercicio permanente (LE) ").

A continuación, puede solicitar el registro en el bar del lugar en el que desea ejercer.

**Qué saber**

Los documentos justificativos necesarios para su inclusión en la junta dependen de la barra elegida por el nacional.

Si existen diferencias sustanciales entre su formación y las cualificaciones profesionales requeridas en Francia, el nacional puede ser sometido a una prueba de aptitud (véase infra "3o. c. Bueno saber: medida de compensación").

*Para ir más allá* Artículos 99 y 203 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado; Decreto de 10 de octubre de 2017 por el que se establece el programa y las modalidades del examen de aptitud para los artículos 204-2 y 204-3 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

### c. Honorabilidad e incompatibilidad

#### Ética

El Consejo Nacional de Abogados ha incorporado en su Reglamento Interno Nacional (RIN), el Código de ética aplicable a todos los abogados que practican en Francia.

Como tal, el abogado debe respetar las normas éticas codificadas, incluidas las normas relativas a:

- Secreto profesional;
- respeto del principio de lo contradictorio;
- Confidencialidad de los intercambios;
- deber de cuidado.

Para más información, es aconsejable consultar el[Sitio web del Consejo Nacional de Abogados](https://www.cnb.avocat.fr/sites/default/files/rin_2017-03-31_consolidepublie-jo.pdf).

#### Incompatibilidad

La profesión jurídica es incompatible con:

- cualquier actividad comercial. Sin embargo, el desarrollo de una actividad comercial accesoria está permitido en determinadas condiciones (véase el artículo 111 del Decreto 91-1197, de 27 de noviembre de 1991, modificado en 2016);
- las funciones de socio en una sociedad, un socio general en sociedades y acciones limitadas, un gerente en una sociedad de responsabilidad limitada, presidente del consejo de administración, un miembro del consejo de administración o un miembro del consejo de administración Director general de una sociedad limitada, director general de una sociedad civil;
- el ejercicio de cualquier otra profesión excepto la enseñanza, como asociado de un miembro del Parlamento o como senador asistente, como sustituto de un juez de distrito, como miembro asesor de los tribunales de niños o en los tribunales de casos de seguridad social, así como los de árbitros, arrendamientos rurales, concejales de prud'homme, miembros del tribunal de mediación, conciliadores o receptores.

*Para ir más allá* Artículos 111 a 123 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

### d. Algunas peculiaridades de la regulación de la actividad

#### Obligación de contrato de seguro de responsabilidad civil profesional y de justificar una garantía financiera

**Seguro de responsabilidad civil profesional**

Un abogado liberal debe obtener un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

El abogado debe estar cubierto por cualquier acto que cometa en el marco de su ejercicio por un mínimo de 1.500.000 euros.

*Para ir más allá* Artículos 205 y 206 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

**Garantía financiera**

El abogado debe justificar una garantía financiera que se asignará al reembolso de los fondos recibidos durante su actividad. Este es un compromiso de fianza con un banco o institución de crédito, una compañía de seguros o una compañía de seguros mutuos. Su importe debe ser al menos igual al de los fondos que planea tener.

*Para ir más allá* Artículos 210 a 225 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

#### Formación profesional continua

El abogado debe actualizar sus conocimientos y desarrollar sus habilidades a lo largo de su carrera durante la formación continua anual.

La educación continua debe ser de al menos 20 horas por año o 40 horas en dos años consecutivos.

El abogado recién colocado en el bar también tendrá que seguir 10 horas de ética en sus primeros dos años.

Estas formaciones deberán ser declaradas al Consejo del Colegio de Abogados antes del 31 de enero del año civil pasado. El abogado tendrá que adjuntar a esta declaración cualquier prueba de que participó en dicha formación.

*Para ir más allá* Artículos 85 y 85-1 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

#### Especialización de abogados

El abogado puede especializarse en un área de derecho y obtener mención de ella en su título.

La solicitud para la especialización en el tablero debe ir acompañada del certificado de especialización obtenido. El Consejo Nacional de Abogados enumera a los abogados a los que se les permite utilizar una o dos especializaciones.

*Para ir más allá* Artículos 86 y 87 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de su actividad, el contratista debe registrarse en el Urssaf o en el Registro del Juzgado de lo Mercantil.

Además, el abogado puede ejercer su profesión en una empresa individual en forma de sociedad civil profesional (SCP) o de una empresa de práctica liberal (SEL).

Si se dedica a su actividad como contratista individual de responsabilidad limitada (EIRL), deberá presentar al Colegio una copia de la declaración de cesión de los bienes que ha presentado ante el registro especial del EIRL.

*Para ir más allá* Artículo 7 de la Ley 71-1130, de 31 de diciembre de 1971, de reforma de determinadas profesiones judiciales y jurídicas. Es aconsejable consultar con la CFE correspondiente para obtener más información.

### b. Inclusión en el Despacho de la Orden de los Abogados

La solicitud para registrarse en el Pedido está dirigida a la etiqueta por correo certificado con acuse de recibo.

Los documentos justificativos que acompañan a la solicitud pueden variar de una barra a otra.

Sin embargo, el interesado tendrá al menos que enviar:

- Una copia del boletín 3 del historial delictivo de menos de tres meses;
- Una copia del máster o documentos justificativos equivalentes en función de la situación de la persona;
- Una copia de CAPA o documentos justificativos equivalentes dependiendo de la situación de la persona;
- Una identificación válida
- El acta de la juramentación.

El Consejo de la Orden dispondrá de dos meses para decidir sobre la solicitud de inscripción. Si se le deniega, notificada por carta recomendada con reconocimiento, el solicitante puede impugnar la decisión y apelar ante el Tribunal de Apelación.

*Para ir más allá* Artículo 101 y siguientes del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

### c. Solicitar el reconocimiento de las cualificaciones profesionales de la UE o del EEE para un ejercicio permanente (LE)

**Procedimiento**

El Consejo Nacional de Abogados es responsable de decidir sobre la solicitud de reconocimiento de las cualificaciones profesionales del nacional que desea establecerse en Francia para ejercer como abogado.

**Documentos de apoyo**

En apoyo de la solicitud, el nacional deberá enviar un expediente a la autoridad competente, incluyendo:

- Una identificación válida
- cualquier documentación que justifique que ha completado con éxito una educación postsecundaria;
- copias de títulos de formación o de cualquier documento que justifique el acceso a la profesión jurídica expedida en un Estado de la UE o del EEE;
- un certificado que justifique que el nacional ha ejercido la profesión en un Estado de la UE o del EEE durante un año en los últimos diez años, cuando dicho Estado no regula la formación o el acceso a la profesión solicitada o a su ejercicio;
- un certificado de registro de menos de tres meses de la autoridad competente del Estado de la UE o del EEE en el que haya adquirido su título jurídico.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

El Consejo Nacional de Abogados dispondrá de un mes para informar al nacional de los documentos que falten. A continuación, dispondrá de tres meses para decidir sobre la solicitud o someter al nacional a una medida de compensación.

En caso de silencio, la solicitud se considerará rechazada. No obstante, el nacional puede solicitar al Tribunal de Apelación de París la reconsideración de su solicitud.

*Para ir más allá* Artículo 99 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

**Bueno saber: medida de compensación**

La prueba de aptitud organizada en caso de diferencias sustanciales entre la formación y la experiencia profesional del nacional con los requeridos en Francia consiste en una prueba oral ante un jurado. El nacional deberá pasar una presentación de diez minutos sobre un tema dibujado al azar, así como una entrevista con el jurado de unos 20 minutos.

*Para ir más allá* : decreto de 10 de octubre de 2017 por el que se establece el programa y las modalidades del examen de aptitud previstos en los artículos 204-2 y 204-3 del Decreto 91-1197, de 27 de noviembre de 1991, por el que se organiza la profesión de abogado.

