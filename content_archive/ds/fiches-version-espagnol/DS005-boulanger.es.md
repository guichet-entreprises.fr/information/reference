﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS005" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentación" -->
<!-- var(title)="pastelero" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentacion" -->
<!-- var(title-short)="pastelero" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/alimentacion/pastelero.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="pastelero" -->
<!-- var(translation)="Auto" -->


pastelero
=========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El nombre del panadero y la marca de comercio de panadería se refieren a los profesionales que, a partir de materias primas seleccionadas, aseguran el amasado de la masa, su fermentación y su conformación, así como la cocción del pan en las instalaciones. ventas al consumidor final.

La masa y los panes no pueden congelarse ni congelarse en ninguna etapa de producción o venta.

El nombre del panadero y el letrero comercial de la panadería también se pueden utilizar cuando el pan es vendido móvilmente por el profesional, o bajo su responsabilidad, cuando se cumplen las condiciones anteriores.

*Para ir más allá* Artículos L. 122-17 y L. 122-18 del Código del Consumidor.

### b. Centro competente de formalidades comerciales (CFE)

La CFE correspondiente depende de la naturaleza de la actividad realizada, de la forma jurídica de la empresa y del número de empleados empleados:

- para los artesanos y las empresas comerciales dedicadas a la actividad artesanal, siempre que no empleen a más de diez empleados, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para los artesanos y las empresas comerciales que emplean a más de diez empleados, esta es la Cámara de Comercio e Industria (CCI);
- en los departamentos del Alto y El Bajo Rin, la Cámara de Comercio de Alsacia es competente.

**Tenga en cuenta que**

En los departamentos del Bajo Rin, el Alto Rin y el Mosela, la actividad sigue siendo artesanal, independientemente del número de empleados empleados, siempre y cuando la empresa no utilice un proceso industrial. Por lo tanto, la CFE competente es la CMA o la Cámara de Comercio de Alsacia. Para más información, es aconsejable[web oficial de la Cámara de Comercio de Alsacia](http://www.cm-alsace.fr/decouvrir-la-cma/lartisanat-en-alsace).

Para más información, es aconsejable consultar[Sitio web de ICC Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites/competence-cfe).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Sólo una persona cualificada que esté cualificada profesionalmente o puesta bajo el control efectivo y permanente de una persona cualificada puede llevar a cabo la actividad de panadero.

Las personas que son o controlan la actividad de un panadero deben:

- poseer un Certificado de Cualificación Profesional (CAP), un Certificado de Estudios Profesionales (BEP) o un diploma o un nivel superior certificado o registrado en el momento de su emisión en el [directorio nacional de certificaciones profesionales](http://www.rncp.cncp.gouv.fr/) (RNCP);
- o justificar una experiencia profesional efectiva de tres años en el territorio de la Unión Europea (UE) u otro Estado parte en el acuerdo del Espacio Económico Europeo (EEE) adquirido como gerente de empresa, trabajador por cuenta propia o empleada en el cumplimiento de su deber.

En esta segunda hipótesis, el profesional podrá obtener la expedición de un certificado de cualificación profesional por la CMA regional o por la cámara regional de oficios y artesanías en la jurisdicción de la que ejerce (véase infra "3 c). Si es necesario, solicite un certificado de cualificación profesional").

Los diplomas para el ejercicio de la actividad de panadero son en particular:

- Especialidad CAP "baker";
- La especialidad "baker" patente;
- la licenciatura profesional "baker - pastelero."

Para obtener más información sobre cada uno de estos diplomas, es aconsejable consultar las disposiciones de la[decreto de 21 de febrero de 2014 por el que se establece la especialidad "panadero" de la PAC](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028699556&fastPos=1&fastReqId=1909042753&categorieLien=cid&oldAction=rechTexte),[El decreto de 15 de febrero de 2012 por el que se establece la especialidad "panadero" de la patente profesional](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025431441&fastPos=1&fastReqId=290217794&categorieLien=cid&oldAction=rechTexte) Y[el decreto del 2 de julio de 2009 por el que se crea la especialidad "panadero y pastelería" de la licenciatura profesional](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020858999&fastPos=1&fastReqId=1274048211&categorieLien=cid&oldAction=rechTexte).

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 1 del Decreto 98-246, de 2 de abril de 1998.

### b. Cualificaciones profesionales - Nacionales europeos

#### Para entrega gratuita de servicios (LPS)

El profesional que sea miembro de la UE o del EEE podrá trabajar en Francia de forma temporal y ocasional como panadero, siempre que esté legalmente establecido en uno de estos Estados para llevar a cabo la misma actividad.

Además, cuando esta actividad o formación no esté regulada en el Estado de establecimiento, deberá haber trabajado como panadero durante al menos dos años en los diez años anteriores a la actuación que pretende realizar. Francia.

**Tenga en cuenta que**

El profesional que cumpla estas condiciones está exento de los requisitos de registro del directorio de operaciones (RM) o del registro de empresas.

*Para ir más allá* Artículo 17-1 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 2 del Decreto 98-246, de 2 de abril de 1998.

#### Para un establecimiento gratuito (LE)

Para llevar a cabo una actividad permanente de panadería en Francia, el profesional nacional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba "2 grados). a. Cualificaciones profesionales");
- poseer un certificado de competencia o documento de formación necesario para el ejercicio de la actividad de panadero en un Estado de la UE o del EEE cuando dicho Estado regule el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su preparación para el ejercicio de la actividad del panadero cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de Esta actividad
- obtener un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona haya sido panadera durante tres años en el Estado miembro que haya admitido Equivalencia.

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones anteriores podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante: "3. b. Si es necesario, solicite un certificado de cualificación profesional.")

Si la persona no cumple con cualquiera de las condiciones anteriores, la CMA puede pedirle que realice una medida de compensación en los siguientes casos:

- si la duración de la formación certificada es al menos un año inferior a la requerida para obtener una de las cualificaciones profesionales requeridas en Francia para llevar a cabo la actividad de panadero;
- Si la formación recibida abarca temas sustancialmente diferentes de los cubiertos por uno de los títulos o diplomas necesarios para llevar a cabo la actividad de panadero en Francia;
- Si el control efectivo y permanente de la actividad del panadero requiere, para el ejercicio de algunas de sus responsabilidades, una formación específica que no se imparte en el Estado miembro de origen y que abarque temas sustancialmente diferentes de los cubiertos por el certificado de competencia o designación de formación mencionado por el solicitante.

La medida de compensación consiste, a elección del solicitante, en un curso de ajuste o en una prueba de aptitud (véase la infra "3 c). Si es necesario, solicite un certificado de cualificación profesional").

*Para ir más allá* Artículos 16 y 17 de la Ley 96-603, de 5 de julio de 1996; Artículos 1 y 3 del Decreto 98-246, de 2 de abril de 1998.

### c. Etapa de preparación de la instalación (SPI)

Antes de inscribirlo en el RM o, para los departamentos del Bajo Rin, el Alto Rin y el Mosela, en el registro de empresas, el futuro empresario debe haber seguido a un SPI.

**Tenga en cuenta que**

Si una causa de fuerza mayor lo impide, el profesional puede cumplir con su obligación en el plazo de un año a partir de su inscripción o registro.

El SPI se compone de:

- una primera parte dedicada a la introducción a la contabilidad general y la contabilidad analítica, así como información sobre el entorno económico, jurídico y social de la empresa artesanal y sobre la responsabilidad social y social y cuestiones ambientales;
- una segunda parte que incluye un período de seguimiento posterior al registro.

La Cámara de Comercio, el establecimiento o el centro incautado por una solicitud de prácticas está obligado a iniciar la pasantía en un plazo de 30 días. Transcurrido este plazo, el registro del futuro empresario no puede ser denegado ni diferido, sin perjuicio de las demás obligaciones que condicionen el registro.

#### Gastos de pasantía

El futuro empresario puede ser excusado de seguir al SPI:

- si tiene un grado o título de nivel 3 certificado con un título en economía y gestión empresarial o un título de maestría de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento al menos equivalente al proporcionado por la pasantía.

#### Exención de pasantías para nacionales de la UE

Para establecerse en Francia, un profesional cualificado nacional de la UE o del EEE está exento de seguir el SPI.

La CMA podrá pedir a la CMA que se someta a una prueba de aptitud o a un curso de ajuste cuando el examen de sus cualificaciones profesionales revele que el nacional no ha recibido formación de gestión o que su formación se relaciona con materiales sustancialmente diferentes de los cubiertos por el SPI.

Sin embargo, la CMA no puede pedir al profesional que se someta a una prueba de aptitud o a un curso de ajuste:

- cuando ha estado involucrado en una actividad profesional que requiere al menos el mismo nivel de conocimiento que el SPI durante al menos tres años;
- o cuando, durante al menos dos años consecutivos, ha trabajado de forma independiente o como líder empresarial, después de recibir formación para esta actividad sancionada por un diploma, título o certificado reconocido por el Estado, miembro o parte, que lo haya expedido, o considerado plenamente válido por un organismo profesional competente;
- o cuando, tras la verificación, la junta determine que los conocimientos adquiridos por el solicitante durante su experiencia profesional en un Estado miembro o parte, o en un tercer Estado, pueden abarcar, total o parcialmente, la diferencia sustancial en términos de contenido.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, sobre la formación profesional de los artesanos; Artículos 6 y 6-1 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos para la aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

### d. Condiciones de honorabilidad e incompatibilidad

Para llevar a cabo la actividad en Francia, el panadero no debe estar bajo la influencia:

- una sanción de prohibición para esta actividad (esta prohibición se aplica hasta por cinco años);
- prohibición de dirigir, gestionar, administrar o controlar, directa o indirectamente, cualquier empresa comercial o artesanal, cualquier explotación y cualquier entidad jurídica.

*Para ir más allá* Artículo 19 de la Ley 96-603, de 5 de julio de 1996, supra; Artículo 131-6 del Código Penal; Artículo L. 653-8 del Código de Comercio.

### e. Algunas peculiaridades de la regulación de la actividad

#### Artesanía

Las personas que justifican:

- ya sea una PAC, un BEP o un título certificado o registrado cuando se emitió al RNCP al menos un nivel equivalente (véase supra "2." (a. Cualificación profesional));
- o experiencia laboral en esta ocupación que dura al menos tres años.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre cualificación artesanal y RM.

#### El título de maestro artesano

El título de maestro artesano se otorga a las personas, incluyendo los líderes sociales de las personas jurídicas:

- REGISTRADO en RM;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

**Tenga en cuenta que**

En algunos casos, las personas que no poseen el máster pueden solicitar el título de Maestro Artesano de la Comisión Regional de Cualificaciones.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre cualificación artesanal y RM.

#### El título de mejor trabajador de Francia (MOF)

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en el ámbito de la artesanía, comercial, de servicio, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación.

Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

#### Cumplir con las regulaciones de las instituciones públicas (ERP)

El panadero debe cumplir con las normas de seguridad y accesibilidad para ERP. Para obtener más información, es aconsejable consultar el listado[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

#### Cumplir con las normas sanitarias

El panadero debe aplicar las normas del Reglamento CE 852/2004, de 29 de abril de 2004, relativas a la higiene alimentaria, que se refieren a:

- Las instalaciones utilizadas para la alimentación;
- Equipo
- Desperdicio de alimentos
- Suministro de agua
- Higiene personal
- alimentos, envases y envases.

Este Reglamento prevé la aplicación de procedimientos basados en los principios de HACCP*(Punto de control crítico de análisis de peligros).*

El panadero también debe aplicar las normas establecidas en el decreto de 21 de diciembre de 2009 que especifican las temperaturas de conservación de los productos animales perecederos. Estos incluyen:

- Condiciones de temperatura de conservación
- Procedimientos de congelación
- cómo se preparan las carnes molidas y se recibe la caza.

Por último, debe cumplir las normas sobre las temperaturas de conservación de los productos alimenticios perecederos de origen vegetal promulgadas por decreto de 8 de octubre de 2013.

*Para ir más allá* : REGLAMENTO CE 852/2004, de 29 de abril de 2004, relativo a la higiene alimentaria; 21 de diciembre de 2009 orden sobre normas sanitarias para la venta al por menor, almacenamiento y transporte de productos animales y alimentos en contenedores; Orden de 8 de octubre de 2013 sobre normas sanitarias para la venta al por menor, almacenamiento y transporte de productos y productos alimenticios distintos de los productos animales y los alimentos en contenedores.

#### Ver precios y etiquetado de productos

El panadero debe informar al consumidor, mediante marcado, etiquetado, exhibición o cualquier otro proceso apropiado, sobre los precios y las condiciones específicas de la venta.

**Tenga en cuenta que**

El panadero determina libremente el precio de venta de sus productos, pero la venta con pérdida está prohibida.

*Para ir más allá* Artículos L. 410-2 y L. 442-2 y los siguientes del Código del Consumidor.

Además, un alimento sólo puede comercializarse si va acompañado de una referencia para identificar el lote al que pertenece.

##### Firmar

Cada categoría de pan, que está a la vista del público, debe ir acompañada de un signo de al menos 15 cm de largo y al menos 2,5 cm de alto.

En este signo debe ser:

- El nombre exacto de la categoría de pan
- el peso en gramos para los panes vendidos por pieza (esto no es obligatorio para los panes que pesan menos de 200 g);
- El precio de la venta por pieza o kilogramo dependiendo de si son panes vendidos por partes o por peso;
- el precio de venta reportado por kilogramo para los panes vendidos sobre una base parcial que pesan más de 200 g.

##### Muestra

Un cartel blanco impreso en negro de al menos 40 cm de alto y al menos 30 cm de ancho debe colocarse en todos los puntos de venta a una altura máxima de 2 metros sobre el suelo sin un obstáculo que impida la visión de los consumidores.

Este cartel se titula "Precio del pan" y enumera todas las categorías de panes a la venta con:

- El nombre exacto
- Su peso
- su precio por pieza;
- su precio por kg (para panes vendidos por etapas que pesan 200 g o más).

Para el título, las letras deben tener una altura mínima de 2,5 cm y una anchura mínima de 1,5 cm.

Para los textos, los números deben tener una altura mínima de 2 cm y una anchura mínima de 1 cm, las letras deben tener una altura mínima de 1 cm y una anchura mínima de 0,5 cm.

Un segundo póster similar al anterior pero cuyas dimensiones y caracteres se pueden reducir a la mitad debe mostrarse en una vitrina y ser visible desde el exterior.

Para más información, es aconsejable consultar el[sitio de la Confederación Nacional de Panadería Francesa](http://www.boulangerie.org/reglementations-economiques-0).

*Para ir más allá* Artículos L. 112-1, R. 412-3 a R. 412-10 del Código del Consumidor; 78-89/P de 9 de agosto de 1978.

#### Mostrar ingredientes reconocidos como alérgenos

El uso en la fabricación o preparación de un alimento de alimentos que cause alergias o intolerancias debe ser puesto en cuenta al consumidor final.

Esta información debe indicarse en el propio producto o cerca de él para que no haya incertidumbre sobre el producto al que se refiere.

*Para ir más allá* Artículos R. 412-12 a R. 412-16 y Apéndice IV del Código del Consumidor.

#### Llamar a los panes

Los panes solo se pueden vender para la venta o venderbajo el nombre de "pan casero" o bajo un nombre equivalente:

- totalmente amasado, moldeado y cocinado en el lugar de venta al consumidor final;
- vendido al consumidor final, a modo móvil, por el profesional que llevó a cabo las operaciones de amasado, conformación y cocción en el mismo lugar.

Sólo se puede poner a la venta o vender bajo el nombre de "pan tradicional francés", "pan francés tradicional", "pan tradicional de Francia" o bajo un nombre que combina estos términos panes, cualquiera que sea su forma:

- no han sido sometidos a ningún tratamiento de congelación durante su desarrollo;
- que no contiene aditivos
- y resultante de la cocción de una masa con características definidas.

Sólo se puede poner a la venta o vender bajo un nombre con la etiqueta adicional "leaven" panes:

- respetando las características de las dos denominaciones anteriores;
- y con un potencial máximo de hidrógeno (pH) de 4,3 y un contenido de ácido acético endógeno de al menos nocientas partes por millón.

*Para ir más allá* Decreto 93-1074, de 13 de septiembre de 1993, relativo a la aplicación de la ley de 1 de agosto de 1905 en relación con determinadas categorías de pan.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El panadero debe estar registrado en el RM. Para obtener más información, consulte la hoja "Formalidades de reporte de empresas artesanales".

### b. Si es necesario, solicite una exención de SPI

Para estar exento del SPI, el interesado (francés o de la UE o nacional del EEE) debe solicitar una exención de las prácticas.

#### Autoridad competente 

La solicitud debe dirigirse al presidente de la Cámara de Comercio y Artesanía de la región.

#### Documentos de apoyo 

El solicitante debe acompañar su correo:

- Copia del diploma aprobado por el grado III;
- Copia del máster
- prueba de una actividad profesional que requiere un nivel equivalente de conocimiento.

#### hora 

Si la exención no se responde en el plazo de un mes a partir de la recepción de la solicitud, se considerará que se concede la exención.

*Para ir más allá* Artículos 6 y 6-1 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos para la aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

### c. Si es necesario, solicite un certificado de cualificación profesional

El nacional de la UE o del EEE podrá solicitar un certificado de cualificación profesional para ejercer un control efectivo y permanente sobre la actividad del panadero.

#### Autoridad competente

La solicitud debe dirigirse a la ACM regional o a la cámara regional de comercios y artesanías en la que el nacional desee ejercer.

#### Procedimiento y tiempo de respuesta

La junta emite un recibo indicando la fecha de recepción de la solicitud completa en el plazo de un mes a partir de la recepción.

En el caso de una solicitud incompleta, notifica al solicitante de la lista de documentos faltantes dentro de una quincena de recibos y emite el recibo tan pronto como se complete el expediente.

A falta de notificación de la decisión de la junta en el plazo de cuatro meses a partir de la recepción de la solicitud completa, se considerará que el reconocimiento de la cualificación profesional se adquiere al solicitante.

La junta podrá, por decisión motivada:

- expedir un certificado de cualificación cuando reconozca la cualificación;
- informar por escrito que se requiere una compensación.

#### Documentos de apoyo

La solicitud de certificación de cualificación profesional deberá ir acompañada de:

- diploma, formación profesional, certificado de competencia o certificado de actividad (véase supra);
- Prueba de la nacionalidad del solicitante;
- documentos no redactados en francés, una traducción certificada de acuerdo con el original por un traductor jurado o autorizado.

La CMA podrá solicitar la divulgación de información adicional sobre el nivel, la duración y el contenido de su formación para que pueda determinar la posible existencia de diferencias sustanciales con la formación francesa requerida.

También podrá solicitar pruebas por cualquier medio de experiencia profesional que pueda compensar, total o parcialmente, la diferencia sustancial.

#### Medidas de compensación

La CMA, que solicita el reconocimiento de la cualificación profesional, puede decidir someter al solicitante a una medida de compensación. Su decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son esenciales para llevar a cabo la actividad de panadero. Sólo estas asignaturas pueden ser sometidas a la prueba de aptitud o al curso de adaptación, que no puede durar más de tres años.

El solicitante informa a la CMA de su elección para tomar un curso de ajuste o para tomar una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final de la prueba de aptitud, la junta emite un certificado de cualificación profesional al solicitante exitoso dentro de un mes.

Si el solicitante decide hacer una pasantía de alojamiento, la CMA le envía una lista de todas las organizaciones que pueden organizar esta pasantía, dentro de un mes de recibir la decisión del solicitante. En caso contrario, el reconocimiento de la cualificación profesional se considera adquirido y el consejo establece un certificado de cualificación profesional.

Al final del curso de ajuste, el solicitante envía a la junta un certificado que certifica que ha completado esta pasantía, acompañado de una evaluación de la organización que lo organizó. Sobre la base de esta certificación, el consejo de administración emite un certificado de cualificación profesional al interesado en el plazo de un mes.

**Tenga en cuenta que**

Cualquier acción judicial contra la decisión de la CMA de solicitar una indemnización va precedida, apenas inadmisibilidad, por un recurso administrativo ejercido, en el plazo de dos meses a partir de la notificación de dicha decisión, con el prefecto departamento donde la sala tiene su sede.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-146, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad otro Estado parte en el Acuerdo sobre el EEE.

### d. Si es necesario, hacer una declaración en caso de preparación o venta de productos animales o animales

Cualquier operador de un establecimiento que produzca, manipule o almacene productos animales o alimenticios que contengan ingredientes animales (carne, productos lácteos, productos pesqueros, huevos, miel), destinados al consumo debe cumplir con la obligación de declarar cada uno de los establecimientos de los que es responsable, así como las actividades que se llevan a cabo allí.

La declaración deberá efectuarse antes de la apertura del establecimiento y renovarse en caso de cambio de operador, dirección o naturaleza de la actividad.

#### Autoridad competente

La declaración deberá dirigirse al prefecto del departamento de aplicación del establecimiento o, en el caso de los establecimientos bajo la autoridad o tutela del Ministro de Defensa, al servicio de salud del ejército, de acuerdo con los términos previstos por el decreto de la Ministro de Defensa.

#### Documentos de apoyo 

El solicitante debe completar el formulario Cerfa 13984*03.

*Para ir más allá* Artículos R. 231-4 y R. 233-4 del Código Rural y de la pesca marina; Artículo 2 del Decreto de 28 de junio de 1994 sobre la identificación y acreditación sanitaria de establecimientos en el mercado de productos animales o animales y marcas de seguridad.

