﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS040" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Experiencia" -->
<!-- var(title)="Experto en automoción" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="experiencia" -->
<!-- var(title-short)="experto-en-automocion" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/experiencia/experto-en-automocion.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="experto-en-automocion" -->
<!-- var(translation)="Auto" -->


Experto en automoción
=====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El experto en automoción es un profesional que lleva a cabo una misión de seguridad vial. Es responsable de escribir informes de terceros sobre cualquier daño a los vehículos de motor y ciclos y sus derivados. En particular, el experto en automoción determina el origen, la consistencia y el valor de este daño y su reparación. También es responsable de determinar el valor de cualquier vehículo.

De conformidad con lo dispuesto en la Sección D. 114-12 del Código de Relaciones Públicas y la Administración, cualquier usuario podrá obtener un certificado de información sobre las normas aplicables a la profesión automotriz. Para ello, debe enviar la solicitud a la siguiente dirección: devenir-expert-automobile@interieur.gouv.fr.

*Para ir más allá* Artículo L. 326-4 de la Ley de Tráfico de Carreteras.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- En el caso de la creación de una empresa individual, la CFE competente es la Urssaf;
- En caso de actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- En caso de creación de una empresa comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

Para más información, es aconsejable visitar los sitios web de Urssaf,[CPI en París](http://www.entreprises.cci-paris-idf.fr/web/formalites),[CPI de Estrasburgo](http://strasbourg.cci.fr/cfe/competences),[CFE-CMA](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023) [Y el servicio público](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

#### Calificación obligatoria

Para ejercer como experto en automoción, la persona debe ser titular, a la derecha:

- el certificado profesional de experto en automoción o el reconocimiento de esta calidad o el diploma estatal de experto en automoción o la transcripción del diploma estatal de experto en automoción emitido por el rector de la academia;
- un título expedido por otro Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE), o un título reconocido por uno de estos Estados como equivalente a los títulos mencionados anteriormente;
- ya sea a partir de la experiencia profesional en la experiencia en automoción en otro estado de la UE o del EEE.

*Para ir más allá* Decreto 95-493, de 25 de abril de 1995, por el que se establece y regula el diploma general de experto en automoción.

#### Cualificación adicional obligatoria para controlar los vehículos dañados

Si el profesional desea controlar los vehículos dañados, deberá, además de la cualificación obligatoria, obtener una cualificación profesional adicional específica.

La obtención de esta cualificación adicional está condicionada al seguimiento de la formación específica para actualizar y actualizar los conocimientos jurídicos y técnicos necesarios para llevar a cabo los procedimientos del vehículo. Dañado. El seguimiento de esta formación, que dura 8 horas durante un día, da lugar a la entrega de un certificado al participante.

Sólo tres organismos son aprobados por el Ministerio de Transporte para proporcionar esta capacitación:

- [Universidad BCA](http://www.bca.fr/nos-offres-pro/les-services-specifiques) ;
- Instituto de Capacitación en Vehículos Motorizados ([IFOR2A](%5b*http://anea.fr/ifor2a/*%5d(http://anea.fr/ifor2a/))) ;
- Instituto Nacional de Seguridad Vial e Investigación ([Inserr](%5b*http://www.inserr.fr/formation-categorie/experts-vehicules-endommages*%5d(http://www.inserr.fr/formation-categorie/experts-vehicules-endommages))).

**Qué saber**

La calificación para el control de los vehículos dañados es válida hasta el 31 de diciembre del año siguiente a la formación. Por lo tanto, para mantener esta cualificación, el interesado debe tomar un curso anual de actualización.

La lista nacional de expertos en automoción especifica para cada experto, si lo hubiera, que está capacitado para llevar a cabo controles de vehículos dañados. Esta formación se considera adquirida para expertos que han tenido un título de experto en automóviles durante menos de un año.

Algunos profesionales pueden llevar a cabo actividades de control de vehículos dañados sin haber se han sometido a una formación específica. De hecho, esta cualificación también se concede, en determinadas condiciones especificadas por el decreto de 26 de julio de 2011, a los expertos en automóviles que, a su elección:

- han recibido formación en otro Estado de la UE o del EEE equivalente;
- experiencia profesional en el control de los vehículos dañados adquiridos en otro Estado de la UE o del EEE.

*Para ir más allá* Artículo R. 326-11 de la Ley de Tráfico de Carreteras; de 26 de julio de 2011 relativo a la obtención y mantenimiento de la cualificación para el control de vehículos dañados para expertos en automoción.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

#### En caso de entrega gratuita de servicios (LPS)

Se considera que el profesional nacional de la UE o del EEE, que está legalmente establecido en uno de estos Estados para trabajar como experto en automoción, tiene la cualificación profesional para ejercer en Francia, de forma ocasional y temporal, misma actividad.

Sin embargo, si ni la actividad ni la formación que allí se lleva a cabo están reguladas en el Estado del Establecimiento, la persona debe demostrar que ha sido un experto en automoción por el equivalente a un año a tiempo completo en los diez años anteriores la actuación que quiere realizar en Francia.

En todos los casos, el experto se coloca en la lista nacional de expertos en automoción de forma temporal.

**Calificación necesaria para el control de vehículos dañados**

Si el EXPERTO nacional de la UE o del EEE desea llevar a cabo la actividad de seguimiento temporal y ocasional de vehículos dañados en Francia, deberá aportar pruebas, por cualquier medio, de que ha ejercido y tiene una práctica de seguimiento reparaciones y recirculación de vehículos en las mismas condiciones que las aplicables a los que operan permanentemente en Francia.

Si ni la actividad ni la formación que la lleva a ella están reguladas en el estado de la institución, el experto debe haber trabajado a tiempo completo durante al menos dos años en los diez años anteriores a la prestación.

Si una revisión de las pruebas documentales presentadas para justificar la cualificación del experto para el control de los vehículos dañados revela una diferencia sustancial entre la realidad de las cualificaciones del solicitante y las Francia, y en la medida en que esta diferencia pueda afectar a la seguridad de las personas, la calificación de "vehículos dañados" (VE) sólo puede reconocerse y concederse después de que el experto haya completado la formación continua en el control de vehículos. Dañado.

Después de comprobar su calificación para el control de vehículos dañados, el reclamante se coloca en la lista por un período de un año con mención de su calificación "EV".

*Para ir más allá* Artículo L. 326-4 de la Ley de Tráfico de Carreteras y Sección 2-II y 2-III de la orden de 26 de julio de 2011 mencionada anteriormente.

#### En caso de Establecimiento Libre (LE)

Para ejercer en Francia, de forma permanente, la persona debe someterse a los mismos requisitos de cualificación que los aplicables a los nacionales franceses (véase más adelante: "2. a. Cualificaciones profesionales").

**Es bueno saber**

El registro en la lista nacional de expertos en automoción es obligatorio para los nacionales europeos que deseen ejercer esta profesión en Francia de forma permanente.

**Calificación necesaria para el control de vehículos dañados**

Si el estado de establecimiento del solicitante regula el acceso a la profesión, su ejercicio o formación que conduzca al ejercicio de la actividad de control de vehículos dañados, el nacional deberá justificar su competencia o formación o reconocido en uno de estos estados reconociéndolo como calificado para llevar a cabo estas operaciones. Concretamente, estas operaciones deben permitir al experto expedir, al término del procedimiento de control de los vehículos dañados, un certificado de cumplimiento que garantice que el vehículo cumple de alguna manera con su recepción y que puede circular en condiciones normales de seguridad.

Si el estado de establecimiento del solicitante no regula esta ocupación, el nacional deberá demostrar por cualquier medio que ha trabajado durante dos años a tiempo completo en los diez años anteriores y poseer al menos una certificado de competencia o un documento de formación que acredite que ha sido preparado para el ejercicio de esta profesión. Sin embargo, dos años de experiencia profesional no se deben pagar cuando la designación o designaciones de capacitación de detenidos sancionan la formación regulada.

Si una revisión de las pruebas documentales presentadas para justificar la cualificación del experto para el control de los vehículos dañados revela una diferencia sustancial entre la realidad de las cualificaciones del solicitante y las Francia, y en la medida en que esta diferencia pueda afectar a la seguridad de las personas, la calificación "EV" sólo puede reconocerse y concederse después de que el experto haya completado la formación continua en el control de los vehículos dañados.

*Para ir más allá* Artículo L. 326-4 de la Ley de Tráfico de Carreteras y Sección 2-I y 2-III de la Orden del 26 de julio de 2011.

### c. Condiciones de honorabilidad e incompatibilidad

#### Condiciones de honorabilidad

Ninguna persona puede participar como experto en automoción si ha sido declarado culpable de robo, fraude, recepción, violación de la confianza, agresión sexual, resta por un custodio de la autoridad pública, falso testimonio, corrupción o la venta ambulante de influencias, la falsificación o por un delito castigado con robo, fraude o violación de la confianza.

*Para ir más allá* Artículo L. 326-2 de la Ley de Tráfico de Carreteras.

**Es bueno saber**

En caso de condena por hechos constitutivos de una violación de honor o probidad, el tribunal podrá, como sentencia complementaria, prohibir, temporal o permanentemente, el ejercicio de la actividad.

*Para ir más allá* Artículo L. 326-9 de la Ley de Tráfico de Carreteras.

#### Incompatibilidades

La práctica de ser un experto en automoción es incompatible con:

- Ocupar una oficina de funcionarios públicos o ministeriales;
- actividades relacionadas con la producción, venta, arrendamiento, reparación y representación de vehículos de motor y piezas accesorias;
- la profesión de aseguradora.

**Es bueno saber**

Las condiciones en las que un experto en automoción ejerce su profesión no deben menoscabar su independencia.

*Para ir más allá* Artículo L. 326-6 de la Ley de Tráfico de Carreteras.

### d. Seguro de responsabilidad civil

El experto en automoción debe contratar un contrato de seguro que garantice la responsabilidad civil en la que pueda incurrir como resultado de sus actividades profesionales.

Para más detalles sobre las condiciones mínimas del contrato de seguro obligatorio, es aconsejable referirse al decreto de 5 de febrero de 2002 por el que se modifica el auto de 13 de agosto de 1974 relativo a las condiciones mínimas de la responsabilidad que los expertos en automóviles deben asumir.

*Para ir más allá* Artículo L. 326-7 de la Ley de Tráfico de Carreteras y se detuvo el 5 de febrero de 2002.

### e. Algunas peculiaridades de la regulación de la actividad

#### Práctica de ingreso salarial de la profesión de experto en automoción

Es posible ejercer la profesión de experto en automoción de forma independiente o en un empleado.

Las personas que practican a modo de empleado están sujetas a las mismas obligaciones que las de los trabajadores por cuenta propia: cumplir el requisito de cualificación profesional, las condiciones de honorabilidad, respetar las normas de incompatibilidad y suscribirse seguro de responsabilidad civil profesional (véase más adelante: "2. Condiciones de instalación").

Además, los expertos en automoción asalariados deben proporcionar al Ministerio de Transporte una copia de su contrato de trabajo o un certificado de empleado.

#### Reglas de accesibilidad y seguridad

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte las hojas de actividades "Formalidades de notificación de una empresa comercial", "Registro de una empresa individual en el registro de comercios y empresas" o "Formalidades de informes" empresa artesanal."

### b. Pedir inclusión en la lista nacional de expertos en automoción

Para ejercer legalmente como experto en automoción, el interesado debe estar inscrito en la lista nacional de expertos en automoción, [Sitio web de seguridad vial](http://www.securite-routiere.gouv.fr/connaitre-les-regles/le-vehicule/la-liste-nationale-des-experts-automobile). El experto que ejerce sin ser registrado está sujeto a sanciones administrativas, penales y civiles.

**Autoridad competente**

La solicitud debe enviarse al Ministerio de Transporte.

**Tiempo de respuesta**

El Ministro responsable del transporte confirma la recepción del expediente en el plazo de un mes a partir de la recepción e informa al solicitante, en su caso, de los documentos que falten. A continuación, el Ministro dispone de tres meses a partir de la fecha de recepción del expediente completo para decidir sobre la solicitud de registro. Esta decisión debe estar justificada. El silencio guardado al final de este período vale la pena aceptar la solicitud de registro.

**Documentos de apoyo**

La solicitud deberá ir acompañada de los siguientes documentos:

- el formulario de registro completado, fechado y firmado, cuyo modelo está disponible en el [Sitio web de seguridad vial](http://www.securite-routiere.gouv.fr/connaitre-les-regles/le-vehicule/les-experts-en-automobile) ;
- Un documento válido que establezca el estado civil de la persona (documento de identidad, pasaporte, etc.);
- copia de uno de los siguientes documentos:- el certificado profesional de experto en automoción o el reconocimiento de la calidad del experto en automoción o el diploma de experto en automoción o la transcripción del diploma de experto en automoción emitido por el rector de la academia,
  - el título expedido por otro Estado miembro de la UE o del EEE o un título reconocido por uno de estos Estados equivalente a los valores antes mencionados,
  - cualquier pieza que establezca la experiencia profesional de la persona en la experiencia en automoción en otro estado de la UE o del EEE;
- un certificado de seguro que indica que la persona está cubierta por un seguro de responsabilidad civil para el ejercicio de su profesión como "experto en automoción" que data de menos de tres meses;
- un extracto del 3er boletín del historial delictivo (o un documento equivalente para los extranjeros), de menos de tres meses de edad;
- Un extracto de Kbis o el certificado de registro emitido por INSEE;
- una declaración sobre el honor que atestigua que el experto:- no tiene una oficina de funcionarios públicos o ministeriales ni participa en ninguna actividad incompatible con la condición de experto en automoción,
  - no fue objeto de una sentencia en virtud del artículo 326-2 de la Ley de Tráfico de Carreteras,
  - no está sujeto a una condena por hechos que constituyan una violación de honor o probidad;
- uno de los documentos que informan las condiciones de la práctica profesional de la actividad (empleados, liberales o líderes empresariales);
- Si el experto desea llevar a cabo las evaluaciones sobre "vehículo gravemente dañado" o "vehículo económicamente irreparable" (VGE/VEI), proporcione una copia del certificado de seguimiento de la formación correspondiente.

**Qué saber**

Si es necesario, los documentos deben ser traducidos al francés por un traductor certificado.

**Costo**

Gratis.

*Para ir más allá* Artículo L. 326-4, y siguientes y artículos R. 326-5 y siguientes de la Ley de Tráfico de Carreteras.

### c. Hacer una declaración previa de actividad para los nacionales de la UE que realizan una actividad única (LPS)

Los nacionales que vivan en un Estado de la UE o del EEE que deseen llevar a cabo la actividad de experto en automoción en Francia de manera ad hoc y ocasional deberán hacer una declaración previa.

Esta declaración escrita da lugar a una auditoría de las cualificaciones del reclamante para garantizar que su desempeño no afectará la seguridad o la salud del destinatario del servicio, debido a la falta de cualificaciones profesionales de la Proveedor.

La declaración da lugar a la inclusión en la lista nacional de expertos en automoción durante un año. Esta lista está disponible en el [Sitio web de seguridad vial](http://www.securite-routiere.gouv.fr/connaitre-les-regles/le-vehicule/la-liste-nationale-des-experts-automobile).

**Autoridad competente**

La declaración debe dirigirse al Ministerio de Transporte.

**Tiempo de respuesta**

En el plazo de un mes a partir de la recepción de la solicitud y de los documentos justificativos requeridos, el Ministro responsable del transporte informa al demandante de su decisión. El Ministro podrá decidir someter al solicitante a una entrevista profesional o a una pasantía profesional si una revisión de los documentos proporcionados revela una diferencia sustancial entre sus cualificaciones profesionales y las en Francia, si es probable que esta diferencia dañe la seguridad de las personas.

Merece la pena aceptar el ejercicio de la actividad descrita en la declaración la ausencia de una solicitud de información adicional o notificación del resultado de la auditoría de cualificaciones.

**Documentos de apoyo**

La solicitud deberá ir acompañada de los siguientes documentos:

- el [formulario de inscripción](http://media.afecreation.fr/file/08/2/ficheinscription_2015.90082.pdf) completado, fechado y firmado (el campo "SIRET" no debe completarse);
- prueba de la identidad y nacionalidad del reclamante;
- un certificado que certifique que el reclamante está legalmente establecido en otro Estado de la UE o del EEE para ejercer como experto en automoción, y que, cuando se expide el certificado, no existe prohibición, ni siquiera temporal, Ejercicio
- prueba de las cualificaciones profesionales del solicitante
- cuando la profesión de experto en automoción no esté regulada en el estado del establecimiento, las pruebas por cualquier medio de que el reclamante haya ejercido esta ocupación durante al menos el equivalente a dos años a tiempo completo en los diez años anteriores;
- prueba de que el reclamante está cubierto por una póliza de seguro que garantiza la responsabilidad en la que puede incurrir;
- si el experto desea llevar a cabo las evaluaciones VGE/VEI, prueba de que ha ejercido y de que tiene una práctica de seguimiento de reparaciones y recirculación de vehículos en las mismas condiciones que para aquellos que deseen practicar permanentemente en Francia.

**Qué saber**

Si es necesario, todos los documentos deben ser traducidos al francés por un traductor certificado.

**Costo**

Gratis.

**Tenga en cuenta que**

Si es necesario, el Ministro podrá pedir al reclamante que justifique que dispone de las competencias linguísticas necesarias para desempeñar las funciones de experto en automoción en Francia.

*Para ir más allá* Artículos R. 326-4 y siguientes de la Ley de Tráfico de Carreteras.

**Solicitud de renovación del registro**

Si, al final del primer año, el demandante desea volver a realizar su actividad de forma temporal y ocasional en Francia, envía, por cualquier medio, al Ministro responsable del transporte una solicitud de renovación de su inscripción en la lista por un período de un año. Esta solicitud de renovación va acompañada de pruebas de que el demandante está cubierto por un contrato de seguro que garantiza la responsabilidad civil en la que puede incurrir como resultado de las actividades de un experto en automóviles.

