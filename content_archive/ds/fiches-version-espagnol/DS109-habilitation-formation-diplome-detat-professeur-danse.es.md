<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS109" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseñanza" -->
<!-- var(title)="Habilitación de los centros para formar candidatos al diploma de Estado de profesor de danza" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="ensenanza" -->
<!-- var(title-short)="habilitacion-formacion-diploma-de-estado-profesor-danza" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/ensenanza/habilitacion-formacion-diploma-de-estado-profesor-danza.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="habilitacion-formacion-diploma-de-estado-profesor-danza" -->
<!-- var(translation)="Auto" -->

# Habilitación de los centros para formar candidatos al diploma de Estado de profesor de danza [FR]

Actualización más reciente : <!-- begin-var(last-update) -->2020-12<!-- end-var -->

# 1°. Définition de l’activité

## a. Définition

Afin de pouvoir exercer, les centres de formation au diplôme d’État de professeur de danse doivent obtenir une habilitation du ministère de la Culture.

L'exigence d'habilitation a pour objectif de vérifier que les enseignements sont conformes aux programmes définis par la réglementation en vigueur et dispensés par des formateurs qualifiés. Elle permet également de s'assurer de la faisabilité de l'offre de formation. Ces éléments garantissent que les étudiants bénéficient d'un enseignement leur permettant d'acquérir les compétences attestées par le diplôme d'État de professeur de danse. 

La durée limitée à cinq ans de l'habilitation permet de vérifier que les établissements sont en capacité de conduire de manière pérenne les formations, tant en ce qui concerne les aspects administratifs et financiers que pédagogiques. En outre, la durée des formations étant de deux ans, il est possible, lors de l'examen du renouvellement de l'habilitation, d'évaluer précisément les résultats obtenus par au moins deux promotions d'étudiants, ce qui permet de demander aux centres d'apporter tout ajustement nécessaire à l'offre de formation lorsque sont constatés des manquements aux modalités de mise en œuvre des cursus d'études. 

## 2°. Conditions d'installation 

Afin de pouvoir exercer, les centres de formation au diplôme d’État de professeur de danse doivent obtenir une habilitation du ministère de la Culture.

L’habilitation est délivrée sur décision du ministre de la Culture, après avis circonstancié du directeur régional des affaires culturelles compétent et de l'inspection de la création artistique.

L'habilitation porte sur une ou plusieurs des trois options constitutives du diplôme d’État de professeur de danse. **La délivrance de l'habilitation, ou son renouvellement, est subordonnée aux conditions suivantes :**

- le centre de formation assure, dans la ou les options concernées, soit seul soit dans le cadre d'une mutualisation contractuelle avec un ou plusieurs autres centres de formation habilités, l'ensemble de la formation ainsi que l'organisation matérielle de l'évaluation des candidats ;
- la compatibilité des locaux avec l'offre de formation : nombre de salles, dimensions, application des dispositions prévues aux articles R. 462-1 à R. 462-5 du Code de l'éducation susvisés (voir en ce sens les mesures applicables aux écoles de danse) ;
- l’organisme doit disposer d'un personnel pédagogique qualifié en nombre suffisant pour chaque discipline enseignée ;
- la viabilité économique de l'activité du centre, notamment au regard de l'offre territoriale existante.

L'habilitation de l'organisme pour la formation au diplôme d'État de professeur de danse, ou son renouvellement, est délivrée par le ministre chargé de la culture pour une durée de cinq ans renouvelable.

Si, au cours de la période d'habilitation, des modifications interviennent concernant les modalités d'organisation et de suivi de la formation ou son contenu ou l'équipe pédagogique, l'organisme est tenu d'en informer, sans délai, la direction régionale des affaires culturelles, qui la transmet, assortie de ses observations, au directeur général de la création artistique dans un délai de quinze jours maximum.

Lorsque les conditions de l'habilitation ne sont plus réunies, la suspension ou le retrait de l'habilitation sont prononcés par décision motivée du ministre de la Culture qui fixe les modalités de sa mise en œuvre.

## 3°. Démarches et formalités d'installation 

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Effectuer une demande d’habilitation auprès du ministère de la Culture

Le **dossier de demande d'habilitation** à assurer la formation au diplôme d'État de professeur de danse comprend :

- les pièces relatives aux caractéristiques générales de l'organisme, notamment son statut juridique ainsi que le détail de ses activités ;
- la présentation de l'équipe pédagogique pressentie pour assurer la formation ainsi que la copie des diplômes d'enseignement de la danse des professeurs de danse qui la composent ou de leur dispense et les caractéristiques concernant l'organisation et le contenu de la formation ;
- le budget prévisionnel général de l'organisme et le budget spécifique de l'activité d'enseignement par option proposée ;
- le nombre et les dimensions de chacune des salles affectées à la formation ainsi que, pour celles destinées à la pratique dansée, la nature des sols et les aménagements sanitaires ;
- l'organisation des enseignements, et en particulier le planning détaillé de la formation (les volumes horaires, les spécialités, les noms des intervenants, les studios occupés), le calendrier de la formation ainsi que les conditions d'organisation de l'évaluation des unités de formation ;
- la liste des établissements pressentis pour mettre à disposition des élèves-sujets ou accueillir des mises en situation des étudiants dans le cadre de leur formation ;
- dans le cas d'une mutualisation avec un ou plusieurs autres centres de formation habilités, les modalités d'organisation des opérations conduites conjointement.

La demande peut être effectuée [en ligne](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/DANSE_ETABL_habilitation_01/?__CSRFTOKEN__=2bc79b9a-9655-4484-8540-44886db7a900).

Le **dossier de renouvellement de l'habilitation** à assurer la formation au diplôme d'État de professeur de danse comprend les pièces suivantes :

- une attestation sur l'honneur certifiant qu'aucun changement n'est intervenu en ce qui concerne les documents fournis lors de la précédente demande ou, le cas échéant, les pièces et les renseignements relatifs aux modifications intervenues depuis cette demande ;
- le relevé des éléments ayant contribué à l'amélioration des compétences ou qualifications de l'équipe pédagogique ;
- un bilan détaillé de l'activité de formation des quatre dernières années. Le bilan détaillé de l'activité de formation précise :
  - le nombre de formations dispensées au cours de cette période ;
  - le nombre de participants ayant suivi ces formations, le bilan de l'insertion professionnelle des élèves diplômés qui ont été formés dans le centre ;
  - le nombre de candidats admis à présenter les épreuves d'évaluation lors de chaque session d'examen organisée par le centre en distinguant ceux ayant suivi une formation dans le centre en vue de l'épreuve concernée ;
  - le descriptif et le bilan des modalités d'accueil des candidats ayant obtenu une validation partielle de l'expérience au cours de la procédure de validation des acquis de l'expérience organisée selon les termes du chapitre III du présent arrêté et de son annexe VI, conformément aux articles R. 335-9 à R. 335-11 du Code de l'éducation ou des ressortissants européens soumis à des mesures de compensation dans le cadre de la procédure de reconnaissance de qualifications professionnelles prévue à l'article L. 362-1-1 du Code de l'éducation ;
  - le budget des deux  derniers exercices en recettes et en dépenses de l'activité de formation au diplôme d'État ;
  - dans le cas d'une mutualisation avec un ou plusieurs autres centres de formation habilités, les modalités d'organisation des opérations conduites conjointement.

La demande de renouvellement est adressée par courrier recommandé avec accusé de réception, un an avant l'expiration de la période de cinq ans, à la direction régionale des affaires culturelles territorialement compétente, qui la transmet, assortie de son avis, au directeur général de la création artistique dans un délai maximum de quinze jours.

L'accusé de réception de la demande de renouvellement d'habilitation constatant que le dossier est complet est émis par la direction générale de la création artistique.

La décision faisant suite à la demande de renouvellement de l'habilitation est notifiée dans un délai de dix mois à compter de la date de l'accusé de réception de la demande.