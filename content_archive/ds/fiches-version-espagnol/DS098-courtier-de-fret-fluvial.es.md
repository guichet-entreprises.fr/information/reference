﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS098" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="River Freight Broker" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="river-freight-broker" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/river-freight-broker.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="river-freight-broker" -->
<!-- var(translation)="Auto" -->


River Freight Broker
====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El corredor de carga fluvial es una persona profesional, individual o jurídica, obligada a conectar contratistas y transportistas públicos de mercancías en barco, con el fin de celebrar un contrato de transporte entre ellos. El corredor de carga fluvial también puede ser necesario para supervisar el buen desempeño del contrato.

*Para ir más allá* Artículo L. 4441-1 del Código de Transporte.

### b. Centro competente de formalidad empresarial

El centro de formalidades empresariales (CFE) correspondiente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. En el caso del corredor de carga fluvial, se trata de una actividad comercial. Por lo tanto, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

Un comerciante es una persona que realiza actos de comercio como de costumbre. Se consideran actos comerciales de compra de bienes para la reventa, alquiler de muebles, operaciones de fabricación o transporte, las actividades de intermediarios (incluyendo corretaje y agencia), etc.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para ejercer como corredor de carga fluvial, la persona debe tener un certificado de capacidad profesional. Esta certificación de capacidad profesional permite registrarse en el registro de corredores de carga fluvial.

El certificado de capacidad profesional se expide a aquellos que, a elección:

- licenciado en educación superior con formación jurídica, económica, contable, comercial o técnica para dirigir un negocio;
- tener un título de educación superior sancionando la formación para actividades de transporte;
- justifican el ejercicio de los deberes de gestión o gestión durante al menos tres años consecutivos, siempre que estos deberes no hayan sido terminados por más de tres años en la fecha de la solicitud, dentro de:- una empresa de corretaje de carga fluvial,
  - una empresa de transporte de carga fluvial,
  - otra empresa si la actividad se encuentra en el sector del transporte.

Para obtener más información sobre cómo obtener el certificado de capacidad profesional, es aconsejable consultar a continuación. "3°. b. Autorizaciones posteriores al registro."

*Para ir más allá* Artículos R. 4441-3 y R. 4441-4 del Código de Transporte.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

#### En el caso de la libre prestación de servicios

Los nacionales de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal y casual están exentos del requisito de registro en el corredores de carga fluviales.

*Para ir más allá* Artículo R. 4441-2 del Código de Transporte.

#### En caso de establecimiento gratuito

Los nacionales de la UE o del EEE que deseen trabajar permanentemente en Francia deben cumplir las mismas condiciones que las aplicables a los franceses. En particular, deben tener un certificado de capacidad profesional y registrarse en el registro de corredores de carga fluvial (véase más adelante: "2. Condiciones de instalación").

*Para ir más allá* Artículo R. 4441-2 del Código de Transporte.

### c. Condiciones de honorabilidad

Sólo las personas que no tienen prohibido ejercer una profesión industrial o comercial y que no están registradas en el expediente nacional prohibido Francia.

*Para ir más allá* Artículos L. 4441-2 y R. 4441-5 del Código de Transporte.

### d. Algunas peculiaridades de la regulación de la actividad

#### Regulaciones de liquidación

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para más información, es aconsejable consultar el listado " [Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El profesional debe estar inscrito en el Registro Mercantil y Mercantil (RCS). Para obtener más información, consulte las tarjetas "Formalidad de reportaje de una empresa comercial" y "Registro de una empresa comercial individual en el RCS".

### b. Autorización posterior al registro

#### Solicitar la certificación de la capacidad para ejercer la profesión

**Autoridad competente**

La solicitud de certificación está dirigida a la Dirección Regional de Medio Ambiente, Desarrollo y Vivienda (Dreal) de Hauts-de-France.

**Documentos justificativos obligatorios**

Para obtener el certificado de capacidad profesional, la persona debe redactar un fichero que consista en los siguientes documentos

- La solicitud de certificación de capacidad profesional, escrita en papel gratuito (si la persona confía en su experiencia en puestos directivos o directivos, describe en detalle la naturaleza y duración de estos deberes, así como la motivaciones para su solicitud);
- Formulario Cerfa 1042901 lleno, fechado y firmado;
- Una tarjeta de registro civil individual
- un sobre, de 26x32 cm de tamaño, sellado a la tarifa postal actual (50 g) para envíos recomendados con previo aviso de recepción;
- una carta recomendada de presentación con aviso de recibo.

**Documentos de apoyo adicionales específicos para las personas que toman un título**

La carpeta debe contener:

- Fotocopia certificada del diploma
- un certificado de la escuela que emitió el diploma, que atestigua el seguimiento de 200 horas de instrucción de gestión en la preparación del diploma de educación superior.

**Documentos de apoyo adicionales específicos para personas con experiencia laboral**

La carpeta debe contener:

- Si el solicitante es un empleado, las fotocopias certificadas como conformes con el contrato de trabajo y los recibos de pago (enero y diciembre de cada uno de los tres años de experiencia adquirida) para determinar la naturaleza de los derechos y el tiempo que se ejercitaron;
- en su caso, un extracto del registro en el registro de comercio y empresas (kbis extraído) de menos de tres meses o el certificado de inscripción en el registro en poder de la Cámara Nacional de Barcos Artesanales (CNBA);
- El certificado de afiliación con un fondo de pensiones (para ejecutivos o para trabajadores no salariales, dependiendo de la situación del solicitante) que especifique la fecha de inicio de la afiliación;
- en caso necesario, fotocopias del certificado de poderes bancarios del banco y de las delegaciones de firma a disposición del solicitante durante el tiempo que dure sus funciones.

*Para ir más allá* Artículos R. 4441-3 y R. 4441-4 del Código de Transporte.

#### Solicitar inscripción en el registro de corredores de carga fluvial

Para poder actuar legalmente como agente de carga fluvial en Francia, el interesado debe estar inscrito en el registro de corredores de mercancías fluviales. Este registro menciona el nombre y las funciones de la persona interesada.

**Autoridad competente**

La solicitud debe enviarse al servicio "Transporte y Vehículo" del Dreal des Hauts-de-France.

**Tiempo de respuesta**

Después de haber enviado el expediente de solicitud a la Dreal, el Dreal emite un acuse de recibo a la persona interesada y comienza la investigación de la solicitud. Una vez completada la investigación, Dreal emite, si es necesario, un certificado de cumplimiento de las regulaciones del negocio del corredor de carga fluvial. El hecho de que el Dreal no respondiera en el plazo de dos meses a partir de la emisión del registro completo merece la pena llegar a un acuerdo.

**Procedimiento**

El registro en el registro da como resultado la emisión de un certificado de registro personal e intransferible que permite al bróker realizar cualquier transacción de corretaje en el área metropolitana.

**Documentos justificativos obligatorios**

Para obtener el registro en el registro del corredor de carga fluvial, los solicitantes deben adjuntar un archivo que consta de:

- un formulario de ciervo 10428 completado, fechado y firmado;
- Una copia de una pieza de identificación del certificado de capacidad y del funcionario jurídico de la empresa (documento nacional de identidad, pasaporte u otro documento equivalente para los nacionales de la UE);
- una copia del certificado de capacidad para ejercer la profesión de corredor de carga fluvial de la persona que proporciona la gestión efectiva y permanente de la empresa o, dentro de ella, la actividad del corredor de carga fluvial.

Además, una vez recibido el certificado de cumplimiento de la normativa de la actividad del corredor de carga fluvial emitida después de que la autoridad competente haya investigado el expediente, el interesado deberá:

- Completar los trámites de reportar un negocio a la CFE;
- Proporcionar el extracto original del registro comercial y de la empresa (extracto de Kbis) con la mención de la actividad del corredor de carga fluvial;
- Proporcionar una declaración de identificación de la empresa emitida por INSEE;
El certificado de registro se expedirá una vez cumplimentados estos trámites, previa presentación del extracto del registro.

**Documentos de apoyo adicionales específicos para las empresas que desean registrarse**

La carpeta debe contener:

- el extracto original del registro comercial y de la empresa que contiene la referencia a la actividad del corredor de carga fluvial (puede sustituirse temporalmente por la provisión de la recepción de la solicitud de inscripción en el registro de comercio y empresas);
- una copia de los certificados de Urssaf, impuestos directos, impuestos indirectos que señalan la situación de la empresa con respecto a estas organizaciones;
- El certificado original de no concursal, liquidación o liquidación legal si es una empresa ya inscrita en el registro de comercio para otra actividad;
- una copia de los estatutos, fechada y firmada por los socios, mencionando en la sección de materia la actividad del corredor de carga fluvial y el nombramiento del funcionario legal o copia de las actas de la reunión que decidieron la adición de la actividad y/o el nombramiento de funcionarios legales;
- una copia de la inserción de la constitución de la empresa en el periódico de publicidad legal.

**Documentos de apoyo adicionales específicos para las personas que poseen el certificado de capacidad profesional que no son el administrador legal de la empresa**

La carpeta debe contener:

- Una copia del certificado de membresía en un fondo de pensiones ejecutivo en nombre del certificado y de la empresa;
- una copia del contrato de trabajo firmado por el administrador legal de la empresa y por el certificado, incluyendo una cláusula de compensación, especificando que el certificador garantizará la dirección efectiva y permanente de la actividad del corredor de carga fluvial en el Dentro de la empresa;
- una copia de las actas de la reunión general nombrándolo.

**Es bueno saber**

La administración encargada del expediente de solicitud de inscripción en el registro de corredores de carga fluvial lleva al expediente un extracto de los antecedentes penales del solicitante (boletín 2), establecido hace menos de tres meses.

**Costo**

Gratis.

**Remedio**

Existen dos tipos de remedios:

- Litigio: este recurso se ejerce ante el tribunal administrativo por el control jurisdiccional competentemente en el plazo de dos meses a partir de la notificación de la decisión impugnada o en el plazo de dos meses a partir de la respuesta expresa a un recurso administrativo Pre-pre-condición
- recurso administrativo: este recurso de casación se dirige a la autoridad que formuló la Decisión impugnada (recurso administrativo ex gratia) o al supervisor del autor de dicha Decisión (recurso administrativo jerárquico). Debe ejercerse en el plazo de dos meses a partir del litigio, prorrogable por dos meses. Es posible ejercer un remedio jerárquico sin iniciar primero una apelación agraciada o sin esperar la respuesta a la apelación agraciada.

*Para ir más allá* : decreto de 25 de marzo de 1997 relativo a la composición del expediente de inscripción en el registro de corredores de mercancías fluviales; Artículos R. 4441-6 y R. 4441-7 del Código de Transporte; registro en el registro de corredores de carga fluvial (Cerfa 50191*02).

