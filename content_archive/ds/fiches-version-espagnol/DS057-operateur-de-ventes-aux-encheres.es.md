﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS057" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Operador de subastas" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="operador-de-subastas" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/operador-de-subastas.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="operador-de-subastas" -->
<!-- var(translation)="Auto" -->


Operador de subastas
====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El operador de ventas voluntaria es un profesional cuya actividad consiste en organizar y llevar a cabo la venta de bienes nuevos o usados durante una subasta pública, y garantizar que la transacción con el licitador se desarrolle sin problemas.

**Tenga en cuenta que**

La persona física profesional que realiza esta actividad durante una subasta toma el título de subastador de ventas voluntarias excluyendo cualquier otro tipo de venta.

*Para ir más allá* Artículos L. 321-1 a L. 321-3 del Código de Comercio.

### b. Centro competente de formalidad empresarial

El Centro de Formalidades Comerciales (CFE) correspondiente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, se trata del registro del Tribunal Mercantil o del registro del tribunal de distrito en los departamentos del Bajo Rin, el Alto Rin y el Mosela;
- en el caso de la creación de una empresa individual, la CFE competente es el Urssaf.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

**En el caso de un operador de persona física**

Para ejercer, el operador de ventas voluntario debe:

- Ser francés o nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE);
- no han sido autores de hechos que hayan dado lugar a una condena penal definitiva por actos contrarios al honor, la probidad o la buena moral o actos similares que hayan dado lugar a una sanción disciplinaria o administrativa de impeachment, delist, revocación, retirada de la acreditación o autorización en la profesión que había ejercido previamente;
- tener las cualificaciones necesarias para liderar una venta, es decir, haber pasado el examen del acceso a las prácticas y haber completado una pasantía de dos años, incluyendo al menos un año en Francia, que incluye la formación teórica y práctica;
- haber hecho una declaración previa de actividad con el[Consejo de Subastas Voluntarias de Muebles en Subastas Públicas](https://www.conseildesventes.fr/) (Consejo de Ventas Voluntarias) (véase infra "3. a. Predeclaración de actividad").

**En el caso de un operador corporativo**

Si es así, debe:

- Estar legalmente constituida y tener su sede, administración o establecimiento principal en Francia o dentro de un Estado miembro;
- Tener al menos un establecimiento o sucursal en Francia
- Tener al menos uno de los ejecutivos profesionalmente cualificado para llevar a cabo la actividad de operador de ventas voluntaria;
- proporcionar pruebas de que sus líderes no han sido condenados definitivamente por actos contrarios al honor, la probidad o la moral, ni ninguna sanción disciplinaria;
- hacer una declaración previa de actividad en las mismas condiciones que el operador de la persona física (véase infra "3 grados). a. Declaración de actividad").

*Para ir más allá* Artículos L. 321-4 y R. 321-18 del Código de Comercio.

**Tenga en cuenta que**

Las ventas voluntarias pueden ser realizadas por notarios y funcionarios judiciales en municipios donde no exista una oficina judicial subastadora, sujeta a formación (véase el artículo R. 321-18-1 del Código de Comercio). A continuación, llevan a cabo su actividad como auxiliar (véase el artículo L. 321-2 del Código de Comercio).

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

**Para ejercicios temporales o casuales (LPS)**

Todo nacional de un Estado miembro de la UE o del EEE que esté legalmente establecido y que actúe como comisario de ventas voluntarias en ese Estado podrá llevar a cabo la misma actividad en Francia de forma temporal y ocasional.

Para ello, debe hacer una declaración previa a la[Consejo de Ventas Voluntarias](https://www.conseildesventes.fr/) (véase infra "5. b. Predeclaración para los nacionales de la UE o del EEE para el ejercicio temporal y casual").

Cuando ni la actividad ni la formación estén reguladas en el Estado miembro en el que el profesional esté legalmente establecido, deberá haber llevado a cabo esta actividad en uno o varios Estados miembros de la UE durante al menos un año en los diez años anteriores a la Ventaja.

*Para ir más allá* Artículos L. 321-4 y siguientes del Código de Comercio.

**Para un ejercicio permanente (LE)**

Todo nacional de la UE legalmente establecido en un Estado miembro y que opere como operador de ventas voluntario podrá llevar a cabo esta actividad de forma permanente. Para ello debe:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba "2 grados). a. Cualificaciones profesionales");
- poseer un certificado de competencia o certificado de formación requerido para el ejercicio de la actividad de subastador en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- distensión de formación que acredite su preparación para el ejercicio de la actividad del subastador cuando se haya obtenido este certificado o título en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de esta actividad;
- tener un certificado de competencia o un documento de capacitación que certifique la preparación para el ejercicio de la actividad de un subastador voluntario de ventas y justifique, en un Estado miembro o un Estado parte en el acuerdo sobre el Espacio que no regule el acceso o el ejercicio de esta profesión, un ejercicio a tiempo completo de la profesión durante al menos un año en los diez años anteriores o por un período equivalente en el caso de ejercicio a tiempo parcial, en virtud del que este ejercicio debe ser certificado por la autoridad competente del Estado.

Cuando la formación recibida por el profesional abarca temas sustancialmente diferentes de los exigidos para el ejercicio de la profesión en Francia, el[Consejo de Ventas Voluntarias](https://www.conseildesventes.fr/) puede exigirle que se someta a una medida de compensación (véase infra "3o. c. Bueno saber: medidas de compensación").

Tan pronto como el nacional cumpla con estas condiciones, debe solicitar al Consejo de Ventas Voluntarias el reconocimiento de sus cualificaciones (véase infra "3o. c. Si es necesario, solicite el reconocimiento del nacional para un ejercicio permanente (LE) ").

*Para ir más allá* Artículos R. 321-65 y el siguiente del Código de Comercio.

### c. Condiciones de honor y sanciones penales

**Reglas profesionales**

El operador de ventas voluntaria está obligado por las siguientes normas profesionales:

- Probidad
- buenos modales;
- Honor.

Cualquier incumplimiento o acción contraria a estas normas puede ser objeto de una condena o de una prohibición de practicar.

*Para ir más allá* Artículo L. 321-4 del Código de Comercio.

**Incompatibilidades**

El profesional que subasta la propiedad se considera que actúa como agente del propietario de estos bienes. Además, no puede realizar la compra en su propia cuenta o en la venta de la propiedad del propietario.

*Para ir más allá* Artículo L. 321-5 del Código de Comercio.

**Sanciones penales**

El profesional se enfrenta a una pena de dos años de prisión y una multa de 375.000 euros si procede o tiene una o más ventas voluntarias de muebles en subastas públicas:

- sin haber hecho la declaración previa de actividad (véase infra "3.3. a. Predeclaración de actividad");
- sin solicitar la cualificación cuando el profesional es nacional de la UE (véase más adelante "3 grados). c. Si es necesario, solicite el reconocimiento del nacional para un ejercicio permanente (LE);
- aunque no tiene las cualificaciones necesarias para organizar o llevar a cabo ventas voluntarias en subastas públicas o está prohibido la celebración de dicha venta.

*Para ir más allá* Artículo L. 321-15 del Código de Comercio.

### d. Seguros

El profesional que actúe como operador de ventas voluntaria debe:

- Abrir una cuenta en una institución de crédito para recibir fondos mantenidos en nombre de otros durante su actividad;
- Contrate un seguro de responsabilidad civil profesional
- seguro de compra o un bono para garantizar los fondos recibidos durante su actividad. Estas garantías sólo pueden ser otorgadas por una institución de crédito o una compañía de financiación autorizada, o una compañía de seguros o una compañía de seguros mutuos.

**Tenga en cuenta que**

Los nacionales de la UE o del EEE están sujetos a los mismos requisitos.

*Para ir más allá* Artículos L. 321-6, R. 321-10 a R. 321-17 y R. 321-56 del Código de Comercio.

### e. Algunas peculiaridades de la regulación de la actividad

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP). Es aconsejable referirse a la lista[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

**Medida publicitaria**

Dentro de los ocho días siguientes a cada venta voluntaria de muebles en subasta pública, el profesional debe anunciarse con el Consejo de Ventas Voluntarias.

**Documentos de apoyo**

El profesional deberá enviarle un documento en el que se mencione toda la información relativa a la organización de la venta, sus medios técnicos y financieros, así como la siguiente información:

- la fecha, el lugar, así como la identidad del profesional que procederá a la venta y la fecha de su declaración a la[Consejo de Ventas Voluntarias](https://www.conseildesventes.fr/) ;
- La calidad del vendedor como comerciante o artesano cuando el vendedor vende nuevos productos;
- El nuevo carácter de la propiedad;
- En su caso, la propiedad de la propiedad puesta a la venta cuando el propietario es un operador de ventas voluntaria organizador o su empleado, gerente o socio;
- La intervención de un experto en la organización de la venta;
- mención del estatuto de limitaciones para las acciones de responsabilidad civil incurridas durante una venta voluntaria, es decir, un período de cinco años a partir de la subasta o premio.

**Tenga en cuenta que**

El profesional debe mantener un registro electrónico de ventas y un directorio que contenga los minutos de las ventas realizadas todos los días.

*Para ir más allá* Artículos L. 321-7 y R. 321-32 a R. 321-35 del Código de Comercio.

**Obligación de llevar un registro de metales preciosos**

Si durante el ejercicio del profesional está en posesión de oro, plata, platino o aleaciones de estos metales, debe llevar un registro llamado "libro de policía" que contenga toda la información relativa a la venta y la propiedad vendida.

*Para ir más allá* Artículos 537 y 538 del Código Tributario General.

**Obligaciones contra el blanqueo de dinero y la lucha contra el terrorismo**

Durante su actividad, el operador de subastas debe cumplir todas las obligaciones relativas a la lucha contra el blanqueo de dinero y el terrorismo.

*Para ir más allá* Artículo L. 561-2 del Código Monetario y Financiero.

**Registro diario de ventas**

El profesional debe llevar un registro diario de las características, procedencia y método de liquidación de la propiedad vendida.

En caso de incumplimiento de estas obligaciones o en caso de menciones inexactas en el registro, el profesional se enfrenta a una pena de seis meses de prisión y una multa de 30.000 euros.

*Para ir más allá* Artículo L. 321-10 del Código de Comercio; 321-7 y 321-8 del Código Penal.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Pre-declaración de actividad

El profesional debe hacer una declaración por carta recomendada con previo aviso de recibo o por cualquier otro medio a la[Consejo de Subastas Voluntarias de Muebles en Subastas Públicas](https://www.conseildesventes.fr/).

Su declaración debe incluir:

- Prueba de identidad
- un certificado que certifique que no es objeto de ninguna condena penal, sanción disciplinaria o prohibición de ejercer;
- Un documento que justifica que las personas responsables de dirigir las ventas estén cualificadas profesionalmente;
- Una copia del arrendamiento o título de los locales donde se lleva a cabo la actividad, así como el último balance o balance de previsión;
- Un documento que justifica la apertura de una cuenta en una entidad de crédito para recibir fondos en nombre de otros;
- Un documento que justifica que el profesional ha contratado un seguro de responsabilidad civil profesional;
- un documento que justifique que el profesional ha contratado un seguro o una fianza que garantice la representación de los fondos mantenidos en nombre de otros.

*Para ir más allá* Artículos R. 321-1 a R. 321-4, y Sección R. 321-56 del Código de Comercio.

### b. Declaración de actividad del nacional de la UE para el ejercicio temporal y casual (LPS)

El nacional de la UE debe hacer una declaración de actividad en las mismas condiciones que el nacional francés (véase más arriba "3o. a. Predeclaración de actividad").

Sin embargo, su solicitud debe ir acompañada de los siguientes documentos adicionales, si los hubiere, con su traducción:

- una prueba de identidad y nacionalidad, o, en el caso de una persona jurídica, una copia de sus estatutos y un documento que justifique su inscripción en un registro público;
- Un documento que justifique que el profesional está legalmente establecido en un Estado miembro;
- prueba de que el profesional ha sido un operador de subastas durante al menos un año en los últimos diez años en un Estado miembro cuando dicho Estado no regula el acceso a la actividad o a su ejercicio;
- una declaración emitida por el Estado miembro con menos de tres meses de edad o, en su defecto, un certificado en el honor de que no incurra ni siquiera en la prohibición temporal de llevar a cabo esta actividad;
- Un documento en el que se mencione la fecha, ubicación, identidad y calificación de la persona encargada de la venta propuesta;
- prueba hace menos de tres meses de que el profesional tiene seguro de responsabilidad profesional y seguro o fianza que garantiza la representación de los fondos mantenidos en nombre de otros.

*Para ir más allá* Artículos R. 321-56 a R. 321-61 del Código de Comercio.

### c. Si es necesario, solicite el reconocimiento del nacional para un ejercicio permanente (LE)

**Autoridad competente**

El nacional debe solicitar la calificación por carta con notificación de recepción a la[Consejo de Ventas Voluntarias](https://www.conseildesventes.fr/).

**Documentos de apoyo**

Su solicitud deberá ir acompañada de los siguientes documentos, si los hubiere traducidos al francés:

- prueba de la identidad, nacionalidad y residencia del solicitante;
- Una copia del certificado de competencia o designación de formación del solicitante que le da acceso a la actividad de un subastador voluntario de ventas;
- Si el nacional posee un diploma o documento de formación expedido por un tercer país y reconocido por un país miembro de la UE o el EEE, un certificado expedido por la autoridad competente de ese Estado miembro que certifique la duración del ejercicio profesional de la fechas de solicitante y ejercicio;
- una prueba del ejercicio de la actividad de subastador voluntario en los últimos diez años, ya que ni la formación ni la actividad están reguladas en el Estado miembro;
- prueba de su cualificación profesional
- un documento que justifique que no es objeto de ninguna condena penal, sanción disciplinaria o prohibición del ejercicio de la actividad de un comisionado de ventas voluntario.

**Tiempo y remedios**

La Comisión reconoce la recepción de la solicitud en el plazo de un mes e informa al solicitante si falta una pieza. La decisión de la Comisión se envía mediante carta recomendada al solicitante en el plazo de tres meses a partir de la recepción del expediente completo. La decisión del Consejo está sujeta a recurso por carta recomendada con notificación de recepción en el registro del Tribunal de Apelación de París.

*Para ir más allá* Artículo A. 321-27 del Código de Comercio.

**Bueno saber: medidas de compensación**

El Consejo de Ventas Voluntarias podrá exigir al profesional que se someta a una prueba de selección ante el jurado profesional encargado del examen del acceso al curso de formación o a un curso de adaptación de hasta tres años.

*Para ir más allá* Artículo R. 321-67 del Código de Comercio.

### b. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de su actividad, el profesional debe inscribirse en el Registro Mercantil y Mercantil (RSC) o registrarse en el Urssaf.

Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

