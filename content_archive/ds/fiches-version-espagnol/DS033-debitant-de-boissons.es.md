﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS033" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentación" -->
<!-- var(title)="Beber" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentacion" -->
<!-- var(title-short)="beber" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/alimentacion/beber.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="beber" -->
<!-- var(translation)="Auto" -->

Beber
=====

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de actividad
---------------------------

### a. Definición

Cualquier persona que al por menor bebidas alcohólicas no productoras opera como un punto de venta de bebidas.

Estos incluyen:

- establecimientos de bebida en el lugar: cafeterías, pubs, discotecas, restaurantes;
- puntos de venta de bebidas para llevar: supermercados, supermercados, tiendas de vinos, venta a distancia o venta por Internet.

### b. Centro competente de formalidades comerciales (CFE)

El negocio del flujo de bebidas es de naturaleza comercial. Por lo tanto, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

*Para ir más allá* Artículo R. 123-3 del Código de Comercio.

2°. Condiciones de instalación
------------------------------

### a. Condición de nacionalidad

Para ejercer como usuario de bebidas, el profesional debe justificar que:

- Francés;
- un nacional de un Estado de la Unión Europea (UE) o un Estado Parte en el Acuerdo del Espacio Económico Europeo (EEE);
- un nacional de un Estado que ha firmado un tratado de reciprocidad con Francia, por ejemplo Argelia, Canadá.

Las personas de otra nacionalidad no pueden, bajo ninguna circunstancia, abrir una licorería.

Sin embargo, este requisito de nacionalidad no se aplica a la apertura de puntos de venta de bebidas de ningún tipo para ser consumidos in situ en el recinto de exposiciones o ferias organizadas por el Estado, autoridades públicas o asociaciones reconocidas. establecimientos de servicio público durante la duración de los eventos (véase la casilla infra "Débitos Temporales de Bebidas").

*Para ir más allá* Artículo L. 3332-3 y L. 3334-1 del Código de Salud Pública.

**Es bueno saber**

La apertura de un café, cabaret o débito de bebidas a consumir en el lugar, la venta de alcohol sin justificar la nacionalidad francesa, la de otro Estado miembro de la UE u otro Estado parte en el acuerdo EEE es un delito punible una multa de 3.750 euros.

*Para ir más allá* Artículo L. 3352-3 del Código de Salud Pública.

### b. Condiciones de honorabilidad e incompatibilidad

#### Discapacidades

Los menores no emancipados y los adultos bajo tutela no pueden ejercer la profesión de beber por su cuenta.

**Tenga en cuenta que**

La práctica de beber por un menor no emancipado o por un mayor bajo tutela es un delito castigado con una multa de 3.750 euros. El tribunal puede ordenar el cierre de la instalación por un máximo de cinco años.

*Para ir más allá* Artículos L. 3336-1 y L. 3352-8 del Código de Salud Pública.

#### Incompatibilidades

El ejercicio de determinadas profesiones es incompatible con el funcionamiento de un licorería, incluidas las profesiones de funcionario judicial, notario, funcionarios y delegado menor.

*Para ir más allá* Artículo 20 del Decreto No 56-222, de 29 de febrero de 1956, relativo a la aplicación de la ordenanza de 2 de noviembre de 1945 relativa al estatuto de funcionarios judiciales; Artículo 13 del Decreto No 45-0117, de 19 de diciembre de 1945, para la aplicación del estatuto notarial; Artículo 25 septies de la Ley 83-634, de 13 de julio, que tiene los derechos y obligaciones de los funcionarios; Sección 251-19 del Código de Minería.

#### Integridad

No utilice puntos de venta de bebidas en el hotel:

- personas condenadas por delitos comunes o proxenetismo. En este caso, la discapacidad es perpetua;
- personas condenadas a al menos un mes de prisión por robo, fraude, violación de confianza, recepción, hilado, recepción de delincuentes, indecencia pública, celebración de una casa de apuestas, apuestas ilegales en carreras de caballos, venta de bienes falsificados o nocivos, delitos de drogas o por re-agresión y embriaguez pública. En estos casos, la discapacidad cesa en caso de indulto y cuando la persona no ha incurrido en una pena de prisión correccional durante los cinco años siguientes a la condena.

También puede imponerse incapacidad a los condenados por el delito de poner en peligro a los menores.

Las condenas por delitos y por los delitos antes mencionados contra un adeudo que consume bebidas que se consuman in situ, implican, como resultado, legítimamente en su contra y por el mismo período de tiempo, la prohibición de operar un adeudo, desde el día en que las condenas se han convertido en definitivas. Este adeudado no puede ser utilizado, en ningún cargo, en el establecimiento que operaba, ni en el establecimiento operado por su cónyuge separado. Tampoco puede trabajar al servicio de la persona a la que ha vendido o alquilado, ni por quien gestiona dicho establecimiento, ni en el establecimiento que sea operado por su cónyuge separado.

*Para ir más allá* Artículos L. 3336-1 a L. 3336-3 del Código de Salud Pública; Artículos 225-5 y siguientes del Código Penal.

**Es bueno saber**

La violación de estas prohibiciones es un delito castigado con una multa de 3.750 euros y el cierre permanente del establecimiento.

*Para ir más allá* Artículo L. 3352-9 del Código de Salud Pública.

### c. Condiciones de ubicación

#### Restringir el número de flujos

No se puede abrir un consumo in situ de 3a categoría en municipios en los que el número total de establecimientos de esta naturaleza y establecimientos de 4a categoría alcance o supere la proporción de un adeudo por 450 habitantes, o fracción de ese número.

Sin embargo, esta prohibición no se aplica a la apertura de un establecimiento cuando se produce como resultado de una transferencia realizada:

- En la región donde se encuentra;
- dentro de un hotel o un camping y parque de caravanas clasificados en el sentido del artículo L. 311-6 del Código de Turismo.

Además, la apertura de un nuevo establecimiento de 4a categoría está prohibida fuera de los casos de apertura de una salida temporal (véase el recuadro infra "Débitos temporales de bebidas").

La apertura de una licorería en violación de estas disposiciones es un delito castigado con una multa de 3.750 euros.

*Para ir más allá* Artículos L. 3332-1, L. 3332-2 y L. 3352-1 del Código de Salud Pública.

**Es bueno saber**

Estas prohibiciones no se aplican a los propietarios de restaurantes con la "licencia de restaurante pequeño" o "licencia de restaurante".

*Para ir más allá* Artículo L. 3331-2 del Código de Salud Pública.

#### Zonas protegidas

Un establecimiento local de bebidas no se puede abrir en áreas protegidas determinadas por orden prefectural. La orden establece la distancia a la que no se puede establecer el flujo alrededor de ciertos edificios y establecimientos, incluyendo:

- edificios dedicados a cualquier culto;
- cementerios;
- instituciones de educación pública y escuelas privadas, así como todas las instituciones de formación o ocio juvenil;
- Prisiones;
- cuarteles, campamentos, arsenales y todos los edificios ocupados por el ejército, el personal marítimo y aéreo;
- Edificios asignados a la explotación de empresas estatales de transporte;
- centros de salud, residencias de ancianos y cualquier centro público o privado para la prevención, tratamiento y atención que impliquen hospitalización, así como dispensarios departamentales;
- estadios, piscinas, campos deportivos públicos o privados.

Estas dos últimas zonas están necesariamente protegidas por decreto de la prefectura.

Las zonas de protección determinadas por orden incluyen el interior de los edificios y establecimientos en cuestión.

Un decreto de la prefectura también puede prohibir el establecimiento de una salida de bebidas en empresas industriales o comerciales, dado el tamaño de la fuerza de trabajo de los trabajadores o sus condiciones de trabajo. Las empresas que suelen tener más de mil empleados deben ser protegidas.

La apertura de una licorería en violación de estas disposiciones es un delito castigado con una multa de 3.750 euros.

*Para ir más allá* Artículos L. 3335-1 y artículos subsiguientes, y la Sección L. 3352-2 del Código de Salud Pública.

**Es bueno saber**

Estas prohibiciones no se aplican a los propietarios de restaurantes con la "licencia de restaurante pequeño" o "licencia de restaurante".

*Para ir más allá* Artículo L. 3331-2 del Código de Salud Pública.

### D. Obtener una licencia para operar

#### Venta de bebidas para consumir en el establecimiento

Cualquier persona que desee abrir un punto de venta de bebidas de 3o y 4o grado o un establecimiento con una "licencia de restaurante pequeño" o "licencia de restaurante" debe someterse a una formación específica que incluya:

- conocimiento de la legislación y reglamentos aplicables a los puntos de venta de bebidas y restaurantes en el lugar;
- obligaciones de salud pública y orden público.

Esta formación es obligatoria y da lugar a la emisión de una licencia de explotación de 10 años. Al final de este período, la participación en la formación de actualización de conocimientos amplía la validez de la licencia de explotación por otros 10 años.

Esta formación también es obligatoria en caso de transferencia (cambio de propiedad), traducción (movimiento) o transferencia de una comuna a otra.

El programa de formación consiste en enseñanzas de una duración mínima de:

- 20 horas repartidas en al menos tres días;
- 6 horas si la persona está justificada, en la fecha de apertura, transferencia, traducción o transferencia de una experiencia profesional de diez años como operador;
- 6 horas para la formación de actualización de conocimientos.

*Para ir más allá* Artículos L. 3332-1-1 y R. 3332-4-1 y los siguientes del Código de Salud Pública; decreto de 22 de julio de 2011 por el que se establece el programa y la organización de la formación necesaria para obtener los certificados previstos en el artículo R. 3332-4-1 del Código de Salud Pública.

#### Venta de bebidas para llevar

En todas las tiendas que no sean puntos de venta de bebidas en el lugar, cualquier persona que quiera vender bebidas alcohólicas entre las 10 p.m. y las 8 a.m. primero debe someterse a una formación específica sobre:

- conocimiento de la legislación y reglamentos aplicables al comercio minorista, la venta a distancia y la venta a distancia;
- obligaciones de salud pública y orden público.

**Tenga en cuenta que**

La venta a distancia se considera una toma de distancia.

Esta formación se traduce en la emisión de una licencia para vender bebidas alcohólicas por la noche. Dura 7 horas en un día.

*Para ir más allá* Artículos L. 3331-4, L. 3332-1-1 y R. 3332-7 del Código de Salud Pública.

### e. Obtener una licencia

La licencia depende de la categoría de alcohol vendido y varía en función del modo de consumo, in situ o de comida para llevar.

No es necesario que se autordelaz de bebidas temporales.

#### Licencias para puntos de venta de bebidas y restauradores en el lugar

Los puntos de venta de bebidas en el lugar se dividen en dos categorías en función del alcance de la licencia a la que estén asociados:

- La licencia de 3a categoría, conocida como "licencia restringida", incluye la autorización para vender bebidas de los grupos 1 y 3 en el lugar;
- la 4a categoría "licencia grande" o "licencia de año completo" incluye la autorización para vender todas las bebidas que permanecen permitidas para el consumo en interiores, incluidas las del 4o y 5o grupo, para consumir en el lugar.

**Tenga en cuenta que**

La Ordenanza No 2015-1682 de 17 de diciembre de 2015 eliminó la Licencia de Segunda Clase. Como resultado, las licencias de 2a categoría se convierten en el derecho de carga de las licencias de 3a categoría.

*Para ir más allá* Artículo 502 del Código Tributario General; Sección L. 3333-1 del Código de Salud Pública.

**Clasificación de bebidas en cuatro grupos**

Las bebidas se dividen en cuatro grupos para la regulación de su fabricación, venta y consumo:

- refrescos (grupo 1): agua mineral o carbonatada, zumos de frutas o vegetales que no fermentan o no contienen, tras el inicio de la fermentación, trazas de alcohol superiores a 1,2 grados, limonadas, jarabes, infusiones, leche, café, té, chocolate;
- bebidas fermentadas destiladas y vinos dulces naturales (grupo 3): vino, cerveza, sidra, pera, aguamiel, a los que se unen vinos dulces naturales, así como cremas de grosella negra y zumos de frutas o verduras fermentados con 1,2 a 3 grados de alcohol, vinos de licor, aperitivos a base de vino y licores de fresa, frambuesas, cubiertas negras o cerezas, con no más de 18 grados de alcohol puro;
- grupo 4: rones, tafias, licores procedentes de la destilación de vinos, sidras, peras o frutas, y sin apoyar ninguna adición de gasolina, así como licores edulcorados con azúcar, glucosa o miel a un mínimo de 400 gramos por litro para licores anisados y un mínimo de 200 gramos por litro para otros licores y que no contengan más de medio gramo de gasolina por litro;
- Grupo 5: todas las demás bebidas alcohólicas.

Los restaurantes que no posean una licencia de bebidas en el lugar deben tener una de las dos categorías de licencias para vender bebidas alcohólicas:

- la "licencia de restaurante pequeño" que permite la venta de las bebidas del 3er grupo para su consumo in situ, pero sólo con motivo de las principales comidas y como accesorios de alimentación;
- la "licencia de restaurante" en sí, que permite que todas las bebidas que están permitidas para ser consumido en el lugar, pero sólo con motivo de las comidas principales y como accesorios de alimentos, para ser vendidos en el sitio.

Los establecimientos con licencia de consumidor o licencia de restaurante en el lugar pueden vender bebidas en la categoría de su licencia para llevar.

*Para ir más allá* Artículos L. 3331-1 a L. 3331-3 del Código de Salud Pública.

#### Licencias para bebidas para llevar

Para vender bebidas alcohólicas, otros establecimientos para llevar de bebidas deberán estar provistos de una de las dos categorías de licencias siguientes:

- La "licencia de pequeña licencia para llevar" incluye la autorización de venta para llevar las bebidas del 3er grupo;
- la propia "licencia de compra" incluye la autorización para vender todas las bebidas para la venta.

*Para ir más allá* Artículos L. 3331-1 y L. 3331-2 del Código de Salud Pública.

### f. Algunas peculiaridades de la regulación de la actividad

#### Cumplir con las regulaciones para las instituciones que reciben el público

Para obtener más información, se recomienda consultar el listado " [Instituciones que reciben el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

#### Prohibiciones de venta de bebidas alcohólicas

Está prohibido:

- para vender bebidas alcohólicas a menores de edad o para ofrecerlos de forma gratuita. La persona que emite la bebida requiere que el cliente establezca una prueba de su mayoría;
- ofrecer, libre o caro, a un menor cualquier objeto que incite directamente al consumo excesivo de alcohol también está prohibido;
- menores de 16 años en establecimientos de bebidas que no estén acompañados por su padre, madre, tutor o cualquier otra persona mayor de 18 años a cargo o supervisión. Sin embargo, los menores mayores de trece años, incluso no acompañados, pueden ser recibidos en puntos de venta de bebidas con una licencia de primera clase;
- venta al por menor a crédito para los grupos 3o, 4o y 5o para ser consumido en el lugar o para llevar;
- ofrecen bebidas alcohólicas gratuitas a voluntad con fines comerciales o venderlas como principal por una suma global, excepto en el contexto de partes y ferias tradicionales declaradas o fiestas de noticias y ferias autorizadas por el Representante estatal en el departamento o cuando se trata de degustaciones para la venta;
- en los puntos de venta de combustible, vender bebidas alcohólicas para llevar entre las 6 p.m. y las 8 a.m. y las bebidas alcohólicas frías;
- ofrecer bebidas alcohólicas con descuento por un período limitado de tiempo sin ofrecer también bebidas no alcohólicas a precio reducido;
- vender y distribuir bebidas del grupo 3 a 5 en estadios, salas de educación física, gimnasios y, en general, en todas las instalaciones físicas y deportivas, a menos que se renuncie por decreto ministerial o Municipal.

*Para ir más allá* Artículos L. 3342-1, L. 3342-3, L. 3335-4 y L. 3322-9 del Código de Salud Pública.

#### Prohibiciones del uso de menores

Está prohibido emplear o recibir menores en una pasantía en los puntos de venta de bebidas que se consumirán en el lugar, con la excepción del cónyuge del deudor y sus padres o aliados hasta el 4o grado inclusive.

En los puntos de venta de bebidas autorizados, esta prohibición no se aplica a los menores de 16 años que reciben capacitación con uno o más períodos de negocios para adquirir cualificación sancionada por un diploma o una designación aprobada.

*Para ir más allá* Artículo L. 3336-4 del Código de Salud Pública.

#### Mostrar bebidas no alcohólicas

En todos los puntos de venta de bebidas, es obligatorio exhibir bebidas no alcohólicas para la venta en el establecimiento. La pantalla debe incluir al menos diez botellas o recipientes y, siempre que se suministe la salida, una muestra de al menos cada categoría de las siguientes bebidas:

- jugos de frutas, jugos de verduras;
- bebidas de jugo de fruta carbonatado;
- sodas;
- limonados;
- jarabes;
- aguas ordinarias artificiales o sin reasidarse;
- aguas minerales carbonatadas o no carbonatadas.

Esta pantalla, separada de la de otras bebidas, debe instalarse de forma destacada en las instalaciones donde se sirve a los consumidores.

*Para ir más allá* Artículo L. 3323-1 del Código de Salud Pública.

#### Mostrar disposiciones sobre la supresión de la embriaguez pública y la protección de los menores

Los restauradores que vendan bebidas alcohólicas deben colocar un cartel que les recuerde las disposiciones del Código de Salud Pública relativas a la supresión de la embriaguez pública y la protección de los menores.

*Para ir más allá* Artículos L. 3342-4 del Código de Salud Pública.

#### Vea los precios de las bebidas y alimentos más comúnmente servidos

Las instalaciones, incluidos los operadores hoteleros, que sirven comidas, alimentos o bebidas para consumir en el lugar, están obligadas a mostrar los precios realmente pagaderos por el consumidor.

**Tenga en cuenta que**

En los establecimientos donde se recoge un servicio, el precio registrado está incluido en los impuestos y el servicio. Los documentos publicados o puestos a disposición de los clientes deben incluir las palabras "Precio de servicio incluido", seguidas de las instrucciones, entre paréntesis, de la tarifa cobrada por la remuneración de este servicio.

Los operadores de bebidas deben mostrar los precios cobrados, independientemente de dónde se consuman, los precios de bebidas y alimentos en cualquier lugar de consumo, desde fuera del establecimiento y en las parcelas exteriores reservadas a los clientes. más comúnmente servidos, que se enumeran a continuación y nombrados:

- La taza de café negro
- medio montón de cerveza de barril;
- una botella de cerveza (contenida servida);
- zumo de fruta (contención servida);
- un refresco (contenido)
- agua mineral plana o espumosa (contenida)
- aperitivo anís (contenido servido);
- un plato del día;
- Un sándwich.

El nombre y los precios deben indicarse mediante letras y figuras con una altura mínima de 1,5 cm.

**Es bueno saber**

Para los establecimientos que ofrecen instalaciones o servicios de entretenimiento como entretenimiento y música, se muestran, de forma visible y legible desde el exterior, en lugar de estas menciones los precios de los siguientes servicios designados:

- entrada y, si el precio de la misma incluye una bebida, la naturaleza y la capacidad de la misma;
- un refresco (naturaleza y contenido servido);
- una bebida alcohólica servida por la copa (naturaleza y capacidad servida);
- una botella de whisky (marca y capacidad);
- una botella de vodka o ginebra (marca y capacidad);
- una botella de champán (marca y capacidad).

Dentro del establecimiento, la pantalla consiste en la indicación de un documento expuesto a la vista del público y directamente legible por la clientela de la lista elaborada por partida, las bebidas y productos ofrecidos para la venta y el precio de cada servicio.

En los restaurantes y para las bebidas servidas durante las comidas, estos documentos pueden ser reemplazados por una tarjeta puesto a disposición de los clientes y que contiene los precios de todos los servicios ofrecidos. Esta tarjeta puede ser un documento separado del menú y, si es necesario, puede ser legible en la parte posterior del menú.

*Para ir más allá* : orden de 27 de marzo de 1987 relativo a la exhibición de precios en los establecimientos que sirven comidas, productos alimenticios o bebidas que se consumirán en el establecimiento.

#### Respetar la prohibición de fumar

No está permitido fumar en los restaurantes. La prohibición de fumar se aplica en todas las áreas cerradas y cubiertas que están abiertas al público o que son lugares de trabajo.

La señalización aparente debe recordar el principio de una prohibición de fumar.

*Para ir más allá* Artículos L. 3512-8, R. 3512-2 y R. 3512-7 del Código de Salud Pública.

**Terrazas**

La prohibición de fumar no se aplica a las terrazas, ya que se trata de un terreno en la acera de una vía pública donde hay mesas y sillas disponibles para los consumidores, frente a un establecimiento y, por lo tanto, un espacio al aire libre.

Se consideran espacios al aire libre:

- las terrazas totalmente descubiertas, incluso si se cerrarían en sus lados;
- terrazas cubiertas, pero cuyo lado principal estaría completamente abierto (generalmente la fachada delantera).

En cualquier caso, la terraza debe estar separada físicamente del interior de la propiedad. Por lo tanto, está prohibido fumar en una "terraza" que sólo sería una extensión del establecimiento, de la que ninguna partición lo separaría.

*Para ir más allá* : circular de 17 de septiembre de 2008 No 2008-292 sobre cómo aplicar la segunda fase de la prohibición de fumar en lugares de uso colectivo.

**Ubicaciones solo para fumadores**

El gerente del establecimiento puede decidir crear sitios solo para fumadores. Estos lugares reservados son habitaciones cerradas, utilizadas para el consumo de tabaco y en las que no se presta ningún servicio. No se pueden realizar tareas de mantenimiento y mantenimiento sin que se renueve el aire, en ausencia de ningún ocupante, durante al menos 1 hora.

Estas ubicaciones deben:

- estar equipado con un dispositivo de extracción de aire de ventilación mecánica que permite una renovación mínima del aire de diez veces el volumen del sitio por hora. Este dispositivo es completamente independiente del sistema de ventilación o aire acondicionado del edificio. La habitación se mantiene en depresión continua de al menos cinco pascuas en comparación con las partes de conexión;
- Tener cierres automáticos sin posibilidad de apertura involuntaria
- No ser un lugar de paso;
- tienen una superficie no superior al 20% de la superficie total de la instalación en la que se desarrollan los emplazamientos sin el tamaño de un emplazamiento superior a 35 metros cuadrados.

*Para ir más allá* Artículos R. 3512-3 y R. 3512-4 del Código de Salud Pública.

#### Si es necesario, solicite permiso para transmitir música

Los restaurantes que deseen transmitir música sonora deben obtener el permiso de la Sociedad de Autores, Compositores y Editores de Música (Sacem).

Deben pagarle dos regalías:

- Derechos de autor que pagan por el trabajo de creadores y editores;
- para la radiodifusión a través de los medios de comunicación grabados, la remuneración justa, destinada a distribuirse entre los artistas intérpretes o ejecutantes y los productores de música. Sacem cobra esta tarifa en nombre de la Fair Compensation Collection Corporation (SPRE).

La cantidad de derechos de autor depende de:

- La comuna en la que se encuentra el establecimiento;
- El número de escaños
- El número de dispositivos de difusión instalados.

La remuneración justa está determinada por el número de escaños y el número de habitantes de la comuna del establecimiento.

Para más información, es aconsejable consultar los sitios web oficiales de la [Sacem](http://www.Sacem.fr) y el [Spre](http://www.spre.fr).

*Para ir más allá* : decisión de 30 de noviembre de 2011 de la comisión en virtud del artículo L. 214-4 del Código de Propiedad Intelectual por la que se modifica la decisión de 5 de enero de 2010.

#### Si es necesario, cumpla con las regulaciones sanitarias

El usuario de bebidas alimentarias debe aplicar las normas del Reglamento CE 852/2004, de 29 de abril de 2004, relativas a la higiene alimentaria, que se refieren a:

- Las instalaciones utilizadas para la alimentación;
- Equipo
- Desperdicio de alimentos
- Suministro de agua
- Higiene personal
- alimentos, envases y envases.

También debe aplicar las normas establecidas en el Decreto de 21 de diciembre de 2009 sobre normas sanitarias para el comercio minorista, el almacenamiento y el transporte de productos animales y de alimentos que contengan, Particular:

- Condiciones de temperatura de conservación
- Procedimientos de congelación
- cómo se preparan las carnes molidas y se recibe la caza.

Además, los establecimientos comerciales tradicionales de servicio de alimentos deben tener al menos una persona en su fuerza de trabajo que pueda justificar la formación en higiene alimentaria. Para obtener más información, consulte la lista "Restauración tradicional".

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

#### Inscripción en el RCS

Para obtener más información, consulte la hoja informativa "Declaración de un negocio".

### b. Declarar la apertura del punto de venta de bebidas, un cambio de titularidad o lugar de operación

Una persona que quiere abrir una cafetería, un cabaret, una licorería para ser consumida en el lugar, un restaurante o una comida para llevar y vender alcohol allí está obligado a hacer una declaración previa a la autoridad administrativa.

Este requisito de notificación previa también se aplica en el caso de:

- mutación en la persona del propietario o gerente de un café o licorería que vende alcohol para ser consumido en el lugar;
- traducción de un lugar a otro.

Por otra parte, la traducción en el territorio de un municipio de un flujo ya existente no se considera como la apertura de un nuevo adeo:

- si es llevado a cabo por el propietario del fondo de comercio o sus legítimos propietarios y si no aumenta el número de adeudos existentes en ese municipio;
- si no se opera en un área protegida por un decreto de la prefectura (véase supra "condiciones relativas a la ubicación de la implantación").

*Para ir más allá* Artículos L. 3332-3 y siguientes del Código de Salud Pública.

**Es bueno saber**

Abrir una licorería sin hacer esta declaración previa en las formas requeridas es un delito castigado con una multa de 3.750 euros.

*Para ir más allá* Artículo L. 3352-3 del Código de Salud Pública.

**Autoridad competente**

La autoridad competente es:

- El ayuntamiento de la comuna del sitio del sitio;
- en París, la prefectura de policía.

**Tenga en cuenta que**

En el caso de las velocidades operadas en aeronaves y vehículos ferroviarios, la declaración se realiza en el lugar donde la empresa tiene su sede o establecimiento principal. Si la sede central y el establecimiento principal están en el extranjero, la declaración deberá efectuarse en el lugar donde la empresa tenga su establecimiento principal en Francia. En el caso de las corrientes operadas en buques y buques, la declaración se hace en lugar de la inscripción.

*Para ir más allá* Artículo R. 3332-2 del Código de Salud Pública.

**hora**

La declaración debe hacerse al menos 15 días antes de la apertura. En el caso de una mutación por muerte, el período de notificación es de un mes.

Inmediatamente se emite un recibo.

Dentro de los tres días siguientes a la declaración, el alcalde de la comuna donde se hizo envía una copia completa al fiscal y al prefecto.

**Documentos de apoyo**

Los documentos justificativos son:

- Impresión de ciervos 11542Completado, fechado y firmado;
- La licencia de explotación
- Licencia
- prueba de nacionalidad.

**Tenga en cuenta que**

Este requisito de preinformación no se aplica en los departamentos del Alto Rin, el Rin inferior y el Mosela. En estos departamentos, el artículo 33 del Código Local de Profesiones, de 26 de julio de 1900, sigue en vigor. Por lo tanto, depende del conservador completar un formulario de solicitud para operar una licencia de licor disponible en los departamentos de prefectura y subprefectura de estos tres departamentos.

*Para ir más allá* Artículo L. 3332-5 del Código de Salud Pública

**Débitos temporales de bebidas**

**Exposiciones y ferias organizadas por organizaciones públicas**

Se permite la apertura de puntos de venta de bebidas para el consumo in situ dentro de los recintos de exposiciones o ferias organizadas por el Estado, las autoridades públicas o las asociaciones reconocidas como instituciones públicas durante el Eventos. No obstante lo dispuesto en las normas de Derecho común:

- no existe ningún requisito de nacionalidad;
- abrir un flujo de 4a categoría es posible.

Cada apertura está sujeta a la opinión conforme del comisario general de la exposición o feria o cualquier persona con la misma calidad. Este aviso se adjunta a la declaración firmada al ayuntamiento, o a la prefectura policial de París, y a la receta buralista de las contribuciones indirectas.

*Para ir más allá* Artículo L. 3334-1 del Código de Salud Pública.

**Ferias, ventas y festivos**

No están obligados a informar de antemano:

- Personas que establecen cafeterías o puntos de venta de bebidas en una feria, venta o fiesta pública;
- asociaciones que establecen cafeterías o puntos de venta de bebidas durante la duración de los eventos públicos que organizan.

En estas dos hipótesis, la apertura del adeudo temporal requiere una autorización de la autoridad municipal, hasta el límite, para los eventos organizados por las asociaciones, de cinco autorizaciones anuales por asociación.

En los puntos de venta y cafeterías abiertos para estos eventos, solo se pueden vender o ofrecer bebidas de los grupos 1 y 3. En los departamentos de Guadalupe, Guyana y Martinica, una orden puede autorizar la venta de bebidas del 4o grupo dentro del límite de cuatro días al año.

*Para ir más allá* Artículo L. 3334-2 del Código de Salud Pública.

**Tenga en cuenta que**

La apertura temporal de un adeudo fuera de los casos enumerados constituye un delito de apertura ilegal castigado con una multa de 3.750 euros, en función de la categoría del adeudo abierto y según la norma violada.

*Para ir más allá* Artículos L. 3352-2 y L. 3352-3 del Código de Salud Pública.

