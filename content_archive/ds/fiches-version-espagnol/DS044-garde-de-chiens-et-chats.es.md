﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS044" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sector animal" -->
<!-- var(title)="Mantener perros y gatos" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sector-animal" -->
<!-- var(title-short)="mantener-perros-y-gatos" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/sector-animal/mantener-perros-y-gatos.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="mantener-perros-y-gatos" -->
<!-- var(translation)="Auto" -->


Mantener perros y gatos
=======================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El cuidado de perros y gatos, también conocido como cuidador de mascotas, es una actividad de pago que consiste en mantener perros y/o gatos en locales y con equipos adaptados a sus necesidades y estado.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- En caso de actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las profesiones liberales, la CFE competente es la Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

La actividad de cuidado de perros y gatos está reservada para el profesional que:

- hizo la declaración al prefecto del lugar de instalación;
- ha establecido y utiliza instalaciones que cumplen con las normas sanitarias y de protección animal;
- cuando esté en contacto directo con animales, justifique:- tienen una certificación profesional,
  - han tenido formación en una institución autorizada por el Ministro de Agricultura para adquirir conocimiento de las necesidades biológicas, fisiológicas, de comportamiento y de mantenimiento de las mascotas, y tiene un certificado de conocimiento,
  - o poseer un certificado de capacidad expedido por la autoridad administrativa emitido antes de la publicación de la orden de comercio y protección de mascotas del 7 de octubre de 2015.

*Para ir más allá* Artículos L. 214-6-1 y R. 214-25 del Código Rural y de la pesca marina; Decreto de 4 de febrero de 2016 relativo a la acción de formación y actualización de los conocimientos necesarios para las personas que realizan actividades relacionadas con las mascotas de especies domésticas y la autorización de organismos de formación; Sitio[mesdémarches](http://mesdemarches.agriculture.gouv.fr/demarches/particulier/vivre-avec-un-animal-de-compagnie/article/obtenir-un-certificat-de-capacite-409?id_rubrique=54) Ministerio de Agricultura y Alimentación.

### b. Cualificaciones profesionales-UE o Nacionales del EEE (Servicio Gratuito o Establecimiento Libre)

#### Para una entrega de servicio gratuita (LPS)

El nacional de un Estado miembro de la UE o del EEE, que sea un trabajador de cuidado de niños en uno de estos Estados, podrá utilizar su título profesional en Francia de forma temporal o ocasional.

Debe solicitar, antes de su primera actuación, mediante declaración al Director Regional de Alimentación, Agricultura y Silvicultura (véase infra "3o). c. Hacer una predeclaración de actividad para el nacional de la UE para un ejercicio temporal e informal (LPS)).

Cuando ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado en el que esté legalmente establecida, el profesional deberá haberla realizado en uno o varios Estados miembros durante al menos un año, durante los diez años que preceder el rendimiento.

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones requeridas para el acceso a la profesión y su ejercicio en Francia, el interesado podrá ser sometido a una prueba de aptitud en un un mes a partir de la recepción de la solicitud de declaración por parte de la autoridad competente.

*Para ir más allá* Artículos L. 214-6-1 y R. 214-25-2 del Código Rural y Pesca Marina.

#### Para un establecimiento gratuito (LE)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si:

- posee un certificado de formación o un certificado de competencia expedido por una autoridad competente de otro Estado miembro que regula el acceso o el ejercicio de la profesión;
- ha trabajado a tiempo completo o a tiempo parcial durante un año en los últimos diez años en otro Estado miembro que no regula la formación ni el ejercicio de la profesión.

A partir de entonces, podrá solicitar un certificado de conocimientos al director regional de alimentación, agricultura y silvicultura (véase infra "3o. d. Solicitar un certificado de conocimientos para el ejercicio permanente de la UE o del EEE )

Cuando el examen de las cualificaciones profesionales revele diferencias sustanciales en las cualificaciones requeridas para el acceso a la profesión y a su práctica en Francia, el interesado podrá ser sometido a una prueba de aptitud o a una curso de adaptación.

*Para ir más allá* Artículos L. 214-6-1 y R. 214-25-1 del Código Rural y Pesca Marina.

### c. Condiciones de honorabilidad e incompatibilidad

El profesional está obligado a mantener perros y gatos en las instalaciones y con la ayuda de instalaciones y equipos adecuados. En caso de condiciones insalubres, el prefecto podrá prohibir el mantenimiento de los animales en las instalaciones.

El profesional tendrá que asegurarse de que el cuidado de los animales no cause ningún sufrimiento o efectos adversos sobre ellos.

*Para ir más allá* Artículos L. 214-1 y R. 214-33 del Código Rural y pesca marina.

### d. Algunas peculiaridades de la regulación de la actividad

#### Formación profesional continua

El profesional que se dedica a una actividad de cuidado de perros y gatos debe someterse a una formación profesional continua para mantener sus conocimientos, a más tardar diez años después de la fecha de emisión de los documentos de formación requeridos (véase supra "2. (a) o después de la fecha de evaluación.

La formación, que dura 7 horas, debe llevarse a cabo con una organización de formación autorizada. El becario recibirá un certificado de formación que se presentará previa solicitud a los servicios de control.

*Para ir más allá* Artículo 4 de la orden de 4 de febrero de 2016.

#### Requisito para llevar registros específicos de animales vigilados

El profesional que cuida perros y gatos debe completar un registro de entrada y salida de animales con los nombres y direcciones de los propietarios, así como un registro de vigilancia de la salud para la salud de los animales mantenidos.

*Para ir más allá* Artículo R. 214-30-3 del Código Rural y Pesca Marina.

#### Desarrollar un reglamento sanitario

El profesional debe redactar un reglamento sanitario en colaboración con un veterinario sanitario e incluir:

- Un plan para limpiar y desinfectar locales y equipos;
- Las normas de higiene que deben seguir el personal o el público
- procedimientos de mantenimiento y cuidado de animales, incluidos el control sanitario, la profilaxis y las medidas que deben adoptarse en caso de un evento sanitario;
- la duración de los períodos de aislamiento de los animales.

*Para ir más allá* Artículo R. 214-30 del Código Rural y Pesca Marina.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte la "Formalidad de la presentación de informes de una empresa comercial", "Registro de negocios individuales en el Registro de Comercio y Empresas" y "Formalidades de Informes Corporativos. trabajo artesanal."

### b. Hacer una declaración de actividad

**Autoridad competente**

La Dirección Departamental de Protección de la Población (DDPP) es responsable de decidir sobre la solicitud de declaración de actividad.

**Documentos de apoyo**

el[Aplicación](http://www.mesdemarches.agriculture.gouv.fr/demarches/particulier/vivre-avec-un-animal-de-compagnie/article/declarer-un-etablissement-d-302) puede ser enviado por correo o directamente en línea, junto con todos los siguientes documentos de apoyo:

- El formulario[Cerfa No. 15045*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15045.do) Completado y firmado;
- consentimiento de un veterinario sanitario.

*Para ir más allá* Artículo R. 214-28 del Código Rural y Pesca Marina.

### c. Hacer una predeclaración de actividad para los nacionales de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El Director Regional de Alimentación, Agricultura y Silvicultura es responsable de decidir sobre la solicitud del nacional de un ejercicio temporal y ocasional.

**Renovación de la predeclaración**

Debe renovarse una vez al año y con cada cambio en la situación laboral.

**Documentos de apoyo**

En apoyo de su solicitud de declaración, el nacional deberá, por cualquier medio, remitir a la autoridad competente un expediente que contenga los siguientes documentos justificativos:

- Prueba de la nacionalidad del profesional
- un certificado que lo certifique:- está legalmente establecido en un Estado de la UE o del EEE,
  - ejerce una o más profesiones cuya práctica en Francia requiere la celebración de un certificado de capacidad,
  - y no incurre en una prohibición de ejercer, ni siquiera temporalmente, al expedir el certificado;
- prueba de sus cualificaciones profesionales
- Un certificado de seguro de responsabilidad profesional;
- además, cuando ni la actividad profesional ni la formación están reguladas en la UE o en el Estado del EEE, prueban por cualquier medio que el nacional haya participado en esta actividad durante un año, a tiempo completo o a tiempo parcial, en los últimos diez años.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**hora**

El prefecto regional tiene un mes desde el momento en que se recibe el archivo para tomar su decisión:

- permitir que el nacional preste su primer servicio;
- someter a la persona a una medida de compensación en forma de prueba de aptitud, si resulta que las cualificaciones y la experiencia laboral que utiliza son sustancialmente diferentes de las requeridas para el ejercicio de la profesión en Francia;
- informarles de una o más dificultades que puedan retrasar la toma de decisiones. En este caso, tendrá dos meses para decidir, a partir de la resolución de las dificultades.

En ausencia de una respuesta de la autoridad competente dentro de estos plazos, puede comenzar la prestación de servicios.

*Para ir más allá* Artículo R. 204-1 del Código Rural y Pesca Marina.

### d. Solicitar un certificado de conocimientos para la UE o el EEE nacional para un ejercicio permanente

**Autoridad competente**

El Director Regional de Alimentación, Agricultura y Silvicultura es responsable de expedir el certificado de conocimientos.

**Documentos de apoyo**

La solicitud debe ir acompañada de los siguientes documentos justificativos:

- un certificado de competencia o un certificado de formación de un nivel equivalente o inmediatamente inferior al exigido en Francia para llevar a cabo esta actividad;
- si el acceso o la práctica de esta actividad no está regulado en el Estado de origen, una prueba de un año de experiencia laboral a tiempo completo en los últimos diez años.

**Costo**

Hay una tarifa fija para procesar el archivo e implementar:

- o una evaluación en forma de un cuestionario de opción múltiple, por un monto de 61 euros;
- o una nueva prueba en forma de un cuestionario de opción múltiple, por un monto de 31 euros.

*Para ir más allá* Artículos L. 204-1 y R. 214-25-1 del Código Rural y de la pesca marina; decreto de 15 de enero de 2002 por el que se establecen las condiciones de cobro de la tasa adeudada por los solicitantes para la expedición del certificado de conocimientos necesario para el ejercicio de actividades relacionadas con las mascotas domésticas.

