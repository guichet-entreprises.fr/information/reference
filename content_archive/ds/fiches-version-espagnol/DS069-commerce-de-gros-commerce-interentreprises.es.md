﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS069" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Comercio al por mayor, comercio de negocio a negocio" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="comercio-al-por-mayor-comercio" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/comercio-al-por-mayor-comercio-de-negocio-a-negocio.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="comercio-al-por-mayor-comercio-de-negocio-a-negocio" -->
<!-- var(translation)="Auto" -->

Comercio al por mayor, comercio de negocio a negocio
====================================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

El comercio al por mayor (de empresa a empresa) consiste en la compra y reventa de bienes profesionales a una clientela predominantemente profesional (mayoristas, intermediarios) independientemente de la cantidad vendida.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

Dado que la actividad es de carácter comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional realiza una actividad de comprayé su actividad será tanto comercial como artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo el negocio de venta al por mayor o de negocio a negocio no se requieren cualificaciones profesionales. Sin embargo, un profesional que desee unirse a un mercado de interés nacional (MIN) debe solicitar permiso.

*Para ir más allá* Artículo L. 761-1 y el siguiente Código de Comercio.

#### Instalación en un Mercado Nacional de Interés (MIF)

Las MIFI son plataformas de servicio público reservadas a los profesionales y para garantizar la organización y seguridad de los canales de distribución de productos agrícolas y alimentarios. El acceso a estos MIF está regulado y restringido a los comerciantes y productores con licencia.

Por lo tanto, para formar parte de ella, el profesional que opera una empresa mayorista debe hacer una solicitud de instalación (véase infra "3." a. Solicitud de permiso para instalar dentro de un FOMIN").

Esta solicitud de autorización incluye proyectos de localización o ampliación de locales para recibir ventas al por mayor de más de 1000 metros cuadrados.

*Para ir más allá* En: R. 761-12-1 del Código de Comercio.

#### Comercio al por mayor de productos alimenticios para animales

Para llevar a cabo la actividad de preparación, transformación, manipulación o almacenamiento de productos alimenticios animales, el profesional deberá:

- obtener la aprobación de salud si opera en un mercado sin descuento directo al consumidor;
- hacer una declaración de manipulación de los alimentos siempre y cuando esté en contacto con el consumidor.

**Solicitud de aprobación sanitaria para alimentos animales al por mayor**

Mientras la actividad del comerciante se refiera a productos animales destinados al consumo, primero deberá obtener una autorización de práctica.

**Tenga en cuenta que**

Si es necesario, la instalación debe incluir al menos a una persona que haya sido capacitada específica en las normas de higiene alimentaria o que tenga al menos tres años de experiencia profesional en un establecimiento del sector alimentario como gerente u operador.

**Autoridad competente**

Antes de iniciar su actividad, el profesional debe enviar su solicitud en línea o por correo a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) donde desee establecerse.

**Documentos de apoyo**

El profesional debe completar el Forma Cerfa 13983Disponible en la página web del Ministerio de Agricultura.

Su aplicación también debe incluir:

- Los elementos descriptivos del establecimiento de la demandante;
- El plan de control de salud
- todos los elementos adjuntos a la Apéndice II del Decreto de 8 de junio de 2006 sobre la acreditación sanitaria de los establecimientos que comercializan productos animales o alimenticios que contengan productos animales.

**Procedimiento**

Una vez recibido el expediente por completo, la autoridad competente expide una aprobación condicional por un período de tres meses. Al final de este período, se lleva a cabo un control de los equipos e instalaciones y en caso de cumplimiento, el establecimiento está aprobado y recibe un número de acreditación. En ausencia de una respuesta superior a dos meses, la solicitud de acreditación se considera rechazada.

**Costo**

Gratis.

*Para ir más allá* Artículo L. 233-2 del Código Rural y Pesca Marina; 8 de junio de 2006.

**Gastos de acreditación sanitaria**

Algunas instituciones están exentas de obtener dicha acreditación si cumplen las tres condiciones siguientes:

- la cantidad máxima de productos que venden no supera el umbral establecido para cada producto dentro de la segunda columna de la Apéndice III del decreto antes mencionado;
- Esta cantidad por categoría de producto representa un máximo del 30% de la producción total del establecimiento para este producto;
- la distancia entre el establecimiento y los establecimientos entregados no supera un radio de ochenta kilómetros.

Si es necesario, debe enviar una declaración previa al prefecto de la ubicación de su establecimiento en línea a través de la Forma Cerfa 13982*05.

Su predeclaración también debe incluir:

- Una lista detallada de los productos vendidos, así como la lista de establecimientos receptores (actividad, dirección y distancia del establecimiento del solicitante);
- la cantidad semanal de productos vendidos y producidos para cada producto.

*Para ir más allá* Título III de la orden de 8 de junio de 2006, en mis pasos de la [sitio web oficial](http://agriculture.gouv.fr/) Ministerio de Agricultura y Alimentación.

**Si es necesario, hacer una declaración de explotación de un comercio al por mayor de productos animales no sujeto a aprobación**

Mientras el profesional opere un establecimiento que no esté sujeto a aprobación sanitaria, deberá hacer una declaración de operación.

**Autoridad competente**

El profesional deberá presentar su declaración al prefecto del lugar donde desee establecer su establecimiento.

**Documentos de apoyo**

El profesional debe completar el formulario Cerfa 1398403 en línea en el sitio web del Ministerio de Agricultura.

**Costo**

Gratis.

*Para ir más allá* Artículos L. 233-2 R. 233-4 del Código Rural y Pesca Marina

### b. Cualificaciones profesionales - Nacionales Europeos (Servicio Gratuito o Establecimiento Libre)

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) lleve a cabo el comercio al por mayor de forma temporal y ocasional en Francia.

Como tal, el profesional nacional eu está sujeto a los mismos requisitos profesionales que el nacional francés (véase más arriba "2o. a.Cualificaciones profesionales").

### c. Algunas peculiaridades de la regulación de la actividad

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

Es aconsejable consultar la hoja "Establecimiento que recibe al público" para obtener más información.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitud de autorización de instalación dentro de un FOMIN

**Autoridad competente**

El profesional deberá presentar su solicitud en dos copias por el pliegue recomendado con notificación de recepción al prefecto o electrónicamente.

**Documentos de apoyo**

Su aplicación se divide en dos partes:

La primera parte debe mencionar:

- La identidad del solicitante y el número de identificación único
- un extracto de registro en el Registro de Comercio y Sociedades (RCS), de registro equivalente para el extranjero o de los estatutos registrados en los servicios fiscales de la empresa si está en proceso de establecimiento;
- Productos previstos durante la venta al por mayor;
- las áreas previstas por el proyecto, a saber:- ventas globales,
  - dedicadoa a los productos,
  - Reserva
  - estacionamiento de clientes, proveedores y el aparcamiento necesario para las maniobras de entrega;
- las limitaciones técnicas específicas del proyecto.

La segunda parte debe especificar las condiciones para la construcción y el funcionamiento de las instalaciones y mencionar:

- Los medios de transporte necesarios para la actividad y su compatibilidad con los existentes;
- Capacidad de recepción para la carga y descarga de mercancías;
- consumo de energía y el impacto del proyecto en el medio ambiente.

Esta parte también debe incluir un plan del área de ventas (explotado y proyectado).

**Procedimiento**

Cuando se completa la primera parte del expediente, el prefecto informa al gerente del FOMIN y le envía una copia de la primera parte de la declaración. A continuación, el gerente debe informar al prefecto de la disponibilidad de la superficie solicitada por el operador. Mientras sea propietario de todas las superficies, el gerente del FOMIN envía una propuesta de instalación al prefecto, que incluye:

- Un mapa de la ubicación de las superficies dentro del mercado;
- Las características de la tierra y las instalaciones que puede poner a su disposición;
- Regulación del mercado interior;
- condiciones para el suministro de fluidos y el tratamiento de residuos y aguas residuales;
- condiciones financieras para poner a disposición los locales.

**Costo**

Gratis.

*Para ir más allá* Artículos A. 761-11 a A. 761-14 del Código de Comercio.

### b. Formalidades de notificación de la empresa

**Autoridad competente**

El comerciante debe informar de su negocio, y para ello, debe hacer una declaración con la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

El interesado debe proporcionar la documentos de apoyo dependiendo de la naturaleza de su actividad.

**hora**

El centro de formalidades de negocios del CCI envía los documentos que faltan al profesional el mismo día. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

#### c. Si es necesario, el registro de los estatutos de la empresa

El profesional deberá, una vez fechados y firmados los estatutos de la sociedad, registrarlos en la Oficina del Impuesto sobre Sociedades (SIE) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.