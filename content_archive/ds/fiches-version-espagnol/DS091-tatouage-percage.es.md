﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS091" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Perforación de tatuajes" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="perforacion-de-tatuajes" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/perforacion-de-tatuajes.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="perforacion-de-tatuajes" -->
<!-- var(translation)="Auto" -->


Perforación de tatuajes
=======================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de actividad0
---------------------------

### a. Definición

El tatuador es un profesional que implementa técnicas de tatuaje de ruptura de la piel, incluyendo maquillaje permanente y perforación corporal, con la excepción de la perforación del pabellón auditivo y el ala de la nariz cuando es hecho por la técnica de la pistola perforadora de oído.

*Para ir más allá* Artículo R. 1311-1 del Código de Salud Pública.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- En el caso de la creación de una empresa individual, la CFE competente es la Urssaf;
- En caso de actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- En caso de creación de una empresa comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

#### Obligación de someterse a formación en higiene y seguridad

Las personas que practican técnicas de tatuaje, maquillaje permanente y perforación corporal deben haber tenido capacitación en condiciones de higiene y seguridad. Esta formación proporciona los conocimientos necesarios sobre la infertilidad de los equipos utilizados, sobre las normas de higiene a respetar durante el tatuaje y sobre la higiene del lugar de práctica donde se llevan a cabo estas prácticas.

**Condiciones de formación**

El entrenamiento dura al menos 21 horas repartidas en tres días consecutivos.

**Autoridad competente**

La formación sólo puede ser imparte por uno de los organismos de formación acreditados por la Agencia Regional de Salud (ARS) territorialmente competente, cuya lista figura en [Sitio web del Ministerio de Salud](%5b*http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing*%5d(http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing)). Al final del procedimiento, se da un certificado de formación al participante.

###### Tenga en cuenta que

El profesional debe mostrar el certificado de formación en el área de recepción del cliente.

**Exención de formación**

La capacitación incluye:

- de un título médico estatal o de una universidad en higiene hospitalaria;
- titulares de un "título de formación" equivalente a uno de estos títulos.

*Para ir más allá* : Artículos R. 1311-3 y siguientes del Código de Salud Pública y detenidos el 12 de diciembre de 2008 para la aplicación del artículo R. 1311-3 del Código de Salud Pública y relativos a la formación de las personas que implementan técnicas de robo perforación de la piel y el cuerpo.

#### Actividad de maquillaje permanente o semipermanente

Para practicar técnicas de maquillaje permanentes o semipermanentes, el interesado debe tener cualificaciones profesionales específicas o estar bajo el control efectivo y permanente de una persona que justifique tales cualificaciones.

Personas profesionalmente calificadas que, a elección:

- poseer el Certificado de Aptitud Profesional (CAP) de Estética, el Certificado de Estudios Profesionales (BEP) de Estética, un diploma o un título de igual o superior nivel aprobado o registrado en el momento de su emisión al directorio nacional certificaciones profesionales ([RNCP](%5b*http://www.rncp.cncp.gouv.fr/*%5d(http://www.rncp.cncp.gouv.fr/))) y se expida para la práctica de la labor de esteticista;
- una experiencia laboral efectiva de tres años en un Estado de la Unión Europea (UE) o en un Estado Parte en el acuerdo del Espacio Económico Europeo (EEE), adquirida como gestor de empresas, autónomo o autónomo empleado en el cumplimiento de su deber. En este caso, se recomienda al interesado que se ponga en contacto con la Cámara de Comercio y Artesanía (CMA) para solicitar un certificado de reconocimiento de la cualificación profesional.

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

#### En caso de entrega gratuita de servicios

Un nacional de un Estado de la UE o del EEE que desee llevar a cabo esta actividad de forma ocasional y temporal en Francia está sujeto a una obligación de formación y presentación de informes, cuyos términos pueden diferir de los aplicables a De nacionalidad francesa.

Cuando se trata de capacitación, los nacionales pueden elegir entre:

- seguir la denominada formación "clásica", es decir, la misma formación a la que están sometidos los ciudadanos franceses (véase más arriba: "2. a. Cualificaciones profesionales");
- o participar en una formación "específica", antes del evento en el que el nacional quiere participar en Francia. En este caso, esta formación se lleva a cabo bajo la responsabilidad del organizador del evento.

**Condiciones de formación "específica"**

El entrenamiento dura al menos siete horas y solo es válido para el evento o evento para el que se emitió.

**Autoridad competente**

La formación "específica" es impartida por uno de los organismos de formación acreditados por la agencia regional de salud territorialmente competente, cuya lista figura en [Sitio web del Ministerio de Salud](%5b*http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing#nb2-18*%5D(http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing#nb2-18)).

*Para ir más allá* : Orden del 23 de diciembre de 2008 por la que se establecen los informes de las actividades de ruptura de la piel, incluido el maquillaje permanente y la perforación corporal.

#### En caso de Establecimiento Libre

El nacional de un Estado de la UE o del EEE que desee establecerse en Francia para llevar a cabo la actividad de perforador de tatuajes debe cumplir las mismas condiciones de formación que los nacionales (es decir, someterse a un deber de formación y pre-informe de la actividad).

Bajo ciertas condiciones, el nacional puede obtener una exención de formación. Para mayor aclaración, es aconsejable referirse al artículo 8 del decreto de 12 de diciembre de 2008.

*Para ir más allá* Artículo 8 de la orden del 12 de diciembre de 2008 para la aplicación de la Sección R. 1311-3 del Código de Salud Pública y relativa a la formación de personas que implementan técnicas de tatuaje y perforación corporal que rompen la piel.

### c. Algunas peculiaridades de la regulación de la actividad

#### Reglas de accesibilidad y seguridad

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

#### Normas de higiene y seguridad

Los profesionales que realicen técnicas de perforación de tatuajes deben seguir las siguientes reglas:

- El material que penetre en la barrera cutánea o entre en contacto con la piel o la membrana mucosa del cliente y los soportes directos de este material deben ser de un solo uso y estériles o esterilizados antes de cada uso;
- las instalaciones deben incluir una sala reservada exclusivamente para la realización de estas técnicas. Esta habitación debe limpiarse diariamente y las superficies utilizadas deben ser desinfectadas entre cada cliente;
- el profesional debe quitarse las joyas y usar guantes estériles, cambiados para cada cliente. Los guantes también se cambian, para el mismo cliente, después de cualquier gesto séptico durante el acto y en caso de perforaciones sucesivas en diferentes áreas corporales. Debe preparar el área para perforar o tatuar usando un antiséptico;
- el profesional debe utilizar pinzas o agujas esterilizadas de un solo uso. La mesa de trabajo debe estar desinfectada, equipada con un campo estéril de un solo uso.

*Para ir más allá* Artículo R. 1311-4 y el siguiente del Código de Salud Pública; Orden de 11 de marzo de 2009 sobre buenas prácticas de higiene y seguridad para la aplicación de técnicas de ruptura de la piel, incluido el maquillaje permanente y la perforación corporal, con la excepción de la técnica de pistola de perforación de oídos; decreto de 6 de marzo de 2013 por el que se establece la lista de sustancias que no pueden incluirse en la composición de los productos para tatuajes.

#### Normas de eliminación de residuos

Los residuos producidos se equiparan con los residuos de las actividades de atención de riesgos infecciosos. Por lo tanto, su eliminación está sujeta a una legislación estricta (separación, clasificación, recogida, envasado, etiquetado, eliminación o procedimiento específico de eliminación, etc.). Para obtener más información, consulte las secciones R. 1335-5 al 1335-8 y R. 1335-13 a 1335-14 del Código de Salud Pública.

*Para ir más allá* Artículo R. 1311-5 del Código de Salud Pública.

#### Reglas para los productos utilizados para perforar tatuajes

Los tallos utilizados durante una perforación inicial hasta la curación y los tallos utilizados después de la curación deben cumplir con ciertas disposiciones, incluyendo las del níquel.

Los productos utilizados para realizar un tatuaje deben cumplir con las disposiciones de las Secciones L. 513-10-1 y lo siguiente del Código de Salud Pública y cumplir con la siguiente definición: "cualquier sustancia o preparación para colorear destinada, por rotura y entrada para crear una marca en las partes superficiales del cuerpo humano con la excepción de los productos que son dispositivos médicos." Por lo tanto, el profesional debe tener cuidado, por ejemplo, de utilizar sólo pigmentos autorizados en los productos de tatuaje que utiliza.

*Para ir más allá* Artículos L. 513-10-1, L513-10-2 y R. 1311-10 del Código de Salud Pública y ordenados a partir del 6 de marzo de 2013 para enumerar sustancias que no pueden incluirse en la composición de los productos tatuados.

#### Reglas para informar al cliente antes de cualquier beneficio con respecto a los riesgos a los que está expuesto: información oral y

Las personas que implementan técnicas de tatuaje y perforación, incluidas las pistolas perforadoras en los oídos, deben informar a sus clientes, antes de someterse a estas técnicas, de los riesgos a los que están expuestos y, después de la precauciones que deben tomarse.

**Requisito de visualización visible**

El profesional debe publicar información sobre los riesgos para el cliente y las precauciones a tomar después del procedimiento en los lugares en los que realiza intervenciones.

**El deber del cliente de proporcionar información oral**

El profesional debe informar oralmente al cliente, dependiendo de la técnica utilizada, de lo siguiente:

- La naturaleza irreversible de los tatuajes que implican la modificación permanente del cuerpo;
- La naturaleza potencialmente dolorosa de los actos;
- El riesgo de infección
- riesgos alérgicos, incluyendo tintas de tatuaje y joyas perforantes;
- Búsqueda de contraindicaciones al terreno o tratamientos que el cliente está experimentando;
- El tiempo de curación adaptado a la técnica que se ha implementado y los riesgos de cicatrización;
- precauciones a seguir después de que se hayan llevado a cabo las técnicas, en particular para permitir una curación rápida.

**Obligación de información escrita del cliente**

Toda la información transmitida oralmente al cliente también debe ser dada al cliente por escrito. Este texto se complementa, si es necesario, con indicaciones sobre el cuidado después de la realización del gesto.

*Para ir más allá* Artículo R. 1311-12 del Código de Salud Pública y detenido el 3 de diciembre de 2008.

#### Clientes menores

Está prohibido realizar un tatuaje, maquillaje permanente o procedimiento de perforación corporal en un menor sin obtener primero el consentimiento por escrito de una persona con la autoridad parental o su tutor. El profesional debe conservar la prueba de este consentimiento durante tres años.

*Para ir más allá* Artículo R. 1311-11 del Código de Salud Pública.

#### Disposiciones para la perforación del ala nasal y el pabellón de orejas

Las técnicas de perforación del ala nasal y el pabellón auditivo sólo pueden ser implementadas por profesionales que, a su elección:

- llevó a cabo la declaración de actividad del perforador de tatuajes (véase más adelante: "3. b. Hacer una declaración de actividad");
- tienen una actividad principal bajo el código NAF 47.77Z: "Comercio al por menor de relojes y joyas en tiendas especializadas" o 32.12Z "Fabricación de joyas y artículos de joyería";
- convenios colectivos:


  - convenio colectivo nacional para el comercio minorista de relojes y joyas,
  - convenio colectivo nacional de joyería, joyería, platería y actividades conexas.

*Para ir más allá* Artículo R. 1311-7 del Código de Salud Pública; de 29 de octubre de 2008 para la aplicación del artículo R. 1311-7 del Código de Salud Pública y relativo a la perforación mediante la técnica de ametralladora y se detuvo el 11 de marzo de 2009 relativo a las buenas prácticas de higiene y seguridad para la aplicación de la perforando el pabellón de la oreja y el ala de la nariz mediante la técnica de pistola perforadora.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte la "Formalidad de la presentación de informes de una empresa comercial", "Registro de negocios individuales en el Registro de Comercio y Empresas" y "Formalidades de Informes Corporativos. trabajo artesanal."

### b. Hacer una declaración de actividad

El profesional que desee implementar una o más técnicas de tatuaje, maquillaje permanente o perforación corporal debe hacer una declaración previa.

**Autoridad competente**

La declaración previa deberá dirigirse al Director General de la Agencia Regional de Salud (ARS) del lugar donde se lleva a cabo la actividad, antes del inicio de la actividad.

**hora**

Cualquier solicitud de una declaración completa da como resultado un recibo que se envía al solicitante de registro. Por otra parte, si la declaración es irregular o incompleta, se invita al solicitante de registro a regularizar o completar su declaración.

**Documentos de apoyo**

La declaración se hace en papel libre y debe mencionar:

- El nombre y apellido del solicitante de registro;
- La dirección del lugar o lugar donde se lleva a cabo la actividad;
- La naturaleza de la técnica o técnica implementada;
- certificado de formación o el título aceptado en equivalencia.

**Costo**

Gratis.

**Es bueno saber**

El proceso de notificación de actividad no implica a aquellos que implementan perforaciones de pistola (oreja y ala de la nariz).

**Tenga en cuenta que**

Incluso cuando el profesional no trabaja más de 5 días laborables al año en un lugar, debe declarar esta actividad. Para más detalles, sobre el procedimiento de presentación de informes, conviene remitirse a los artículos 5 y 6 del auto de 23 de diciembre de 2008.

*Para ir más allá* Artículo R. 1311-2 del Código de Salud Pública y ordenado el 23 de diciembre de 2008 en el que se establecen los informes de las actividades de robo en la piel, incluyendo maquillaje permanente, y perforación corporal.

