﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS087" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Paisajista" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="paisajista" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/paisajista.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="paisajista" -->
<!-- var(translation)="Auto" -->


Paisajista
==========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El paisajista es un profesional en espacios verdes. Su actividad consiste en crear, desarrollar y mantener espacios naturales públicos (jardines, parques, carriles de tráfico, etc.) o privados.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- Para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI);
- para una actividad liberal, la CFE competente es el Urssaf;
- para una actividad agrícola, la CFE competente es la cámara de agricultura.

**Tenga en cuenta que**

Si la actividad es de naturaleza agrícola y comercial, la CFE pertinente será la CPI.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para ejercer la profesión de paisajista, el profesional debe justificar cualificaciones profesionales que varían en función de la naturaleza de sus funciones.

**Paisajista de diseño**

El profesional debe poseer un diploma sancionando la formación cultural, científica y técnica en el diseño paisajístico.

*Para ir más allá* Artículo 174 de la Ley No 2016-1087, de 8 de agosto de 2016, para la recaptura de la biodiversidad, la naturaleza y los paisajes; Decreto No 2017-673 de 28 de abril de 2017 relativo al uso del título de paisajista de diseño.

**Paisajista encargada de llevar a cabo pequeñas obras de construcción de estructuras ajardinadas**

El profesional debe estar profesionalmente cualificado o ejercer bajo el control efectivo y permanente de un profesional.

Para ser reconocido como profesionalmente calificado, el profesional debe poseer uno de los siguientes diplomas o títulos de formación:

- una patente agrícola profesional "Obras de paisajismo" (BPA);
- un certificado de aptitud profesional agrícola "Jardinero De Paisaje" (CAPA);
- un certificado profesional, una licenciatura profesional o un certificado de "Paisajismo" de un técnico agrícola senior (BTSA);
- una licencia profesional "Coordinador de Proyectos de Paisaje Especializado: Patrimonio Natural y Paisajes Costeros" o "Humanidades y Ciencias Sociales mencionan paisajismo: diseño, gestión, mantenimiento";
- un diploma estatal (DE) como paisajista.

En ausencia de uno de estos títulos o títulos, el interesado debe justificar una experiencia profesional efectiva de tres años en el territorio de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) adquirida como líder empresarial, trabajadores por cuenta propia o asalariados en la práctica del trabajo paisajístico. En este caso, se recomienda al interesado que solicite a la CMA un certificado de cualificación profesional.

*Para ir más allá* : Artículo 16 Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996.

**Paisajista responsable del mantenimiento de espacios verdes y del uso de fitofarmacéuticos y/o biocidas**

El profesional debe poseer un certificado individual para la actividad de "Venta, venta de fitofarmacéuticos" que está sujeto a las siguientes condiciones:

- han seguido y validado:- formación adaptada a esta actividad,
  - formación y una prueba de pruebas de conocimientos,
  - Una prueba de una hora
- habiendo obtenido, en los últimos cinco años, uno de los diplomas Anexo del auto de 29 de agosto de 2016 por el que se establecen y establecen las condiciones de obtención del certificado individual para la actividad "venta, venta de fitofarmacéuticos".

Para la actividad relacionada con el uso de biocidas, el profesional debe poseer un certificado individual que pueda solicitar electrónicamente ante el Ministerio de Ambiente, que le expedirá el certificado individual en un plazo de dos meses.

*Para ir más allá* Artículo R. 254-1 del Código Rural y Pesca Marina; pedido a partir del 29 de agosto de 2016 supra; decretado a partir del 9 de octubre de 2013 condiciones para la práctica de la actividad profesional de usuario y distribuidor de ciertos tipos de biocidas.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

**Con el fin de la prestación gratuita de servicios**

El profesional que sea un Estado miembro de la UE o del EEE que esté legalmente establecido y que actúe como paisajista podrá llevar a cabo la misma actividad en Francia de forma temporal y ocasional. Para ello, el nacional debe:

- Poseer un diploma, título o certificado de formación necesario para ejercer su especialidad y expedido en el Estado miembro de origen;
- solicitar un informe previo de su actividad antes de su primera prestación de servicios en Francia (véase más adelante. "2) Solicitud de preinforme para la prestación de servicios gratuitos");
- cuando la profesión no esté regulada, ni en el curso de la actividad ni en el marco de la formación, en el país en el que el profesional esté legalmente establecido, deberá haber realizado esta actividad durante al menos un año en los últimos diez años años antes de la prestación en uno o más Estados miembros de la UE.

**Tenga en cuenta que**

Un ciudadano de la UE que desee ejercer como paisajista de diseño está exento del requisito de preinforme y puede llevar a cabo su actividad temporal y ocasional en Francia bajo el título profesional adquirido en ese Estado.


*Para ir más allá* Artículos R. 254-9 y siguientes del Código Rural y Pesca Marina; Artículo 17-1 de la Ley de 5 de julio de 1996; Artículo 2 de la decreto del 2 de abril de 1998 modificado por el decreto del 4 de mayo de 2017 ; Artículo 7 del Decreto No 2017-673 de 28 de abril de 2017 relativo a la utilización del título de paisajista de diseño.
**En vista de un establecimiento libre**

Para poder llevar a cabo su trabajo como paisajista en Francia, el profesional que es estado miembro de la UE o del EEE deberá, para llevar a cabo su trabajo como paisajista en Francia:

- Poseer un diploma, título o certificado de formación expedido en el Estado miembro de origen necesario para ejercer su especialidad;
- solicitar el reconocimiento de cualificación (véase más adelante. "2. d. Solicitud de cualificación para un establecimiento libre");
- someterse a una medida de compensación si la autoridad competente considera que la formación del solicitante se refiere a sujetos sustancialmente diferentes de la formación necesaria para llevar a cabo la actividad en Francia. En caso necesario, la autoridad competente podrá exigir al solicitante que se someta a una prueba de aptitud o a un curso de adaptación de hasta tres años.

*Para ir más allá* Artículo R. 204-1 del Código Rural y Pesca Marina; Artículos 4 a 6 del Decreto de 28 de abril de 2017 supra; 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996.

### c. Condiciones de honorabilidad

Nadie puede practicar como paisajista si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 de la Ley 96-603, de 5 de julio de 1996.

### d. Procedimientos y formalidades para los nacionales de la UE

#### Solicitud de pre-informes para la prestación gratuita de servicios

La naturaleza de la declaración previa depende de la actividad profesional realizada por el nacional de la UE.

**Paisajista profesional: mantenimiento de espacios verdes y uso de fitofarmacéuticos y/o biocidas**

**Autoridad competente**

Un nacional que tenga un certificado de actividad individual debe solicitar al director regional de alimentación, agricultura y silvicultura del lugar donde desea prestar su servicio.

**Procedimiento y documentos justificativos**

El solicitante dirige su solicitud por cualquier medio acompañado de los siguientes documentos justificativos:

- Prueba de nacionalidad
- un certificado que certifique que está legalmente establecido en un Estado de la UE o del EEE y que no existe prohibición de ejercer;
- prueba de que ha estado en esta actividad durante al menos un año en los últimos diez años cuando el acceso a la profesión no está regulado en el Estado miembro.

**Tenga en cuenta que**

En caso de cambio en la situación laboral, la solicitud debe renovarse.

*Para ir más allá* Artículo R. 204-1 del Código Rural y Pesca Marina.

**Paisajista encargada de llevar a cabo pequeñas obras de construcción de estructuras ajardinadas**

**Autoridad competente**

El nacional debe solicitar a la CMA del lugar donde desee llevar a cabo su servicio.

**Documentos de apoyo**

La solicitud de un informe previo de la actividad va acompañada de un archivo completo que contiene los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE;
- un documento que justifique la cualificación profesional del nacional que puede ser, a su elección:- Una copia de un diploma, título o certificado,
  - Un certificado de competencia,
  - cualquier documentación que acredite la experiencia profesional del nacional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tenga en cuenta que**

Cuando el expediente está incompleto, la CMA tiene un plazo de quince días para informar al nacional y solicitar todos los documentos que faltan.

**Resultado del procedimiento**

Al recibir todos los documentos en el archivo, el CMA tiene un mes para decidir:

- autorizar la prestación cuando el nacional justifique tres años de experiencia laboral en un Estado de la UE o del EEE, y adjuntar a dicha Decisión un certificado de cualificación profesional;
- o autorizar la disposición cuando las cualificaciones profesionales del nacional se consideren suficientes;
- ya sea para imponer una prueba de aptitud o un curso de ajuste cuando hay diferencias sustanciales entre las cualificaciones profesionales del nacional y las requeridas en Francia. En caso de denegación de esta medida de compensación o en caso de incumplimiento en su ejecución, el nacional no podrá llevar a cabo la prestación de servicios en Francia.

El silencio guardado por la autoridad competente en estos tiempos merece autorización para iniciar la prestación de servicios.

*Para ir más allá* Artículo 2 del Decreto de 2 de abril de 1998; Artículo 2 de la 17 de octubre de 2017 respecto a la presentación de la declaración y las solicitudes previstas en el Decreto 98-246, de 2 de abril de 1998, y en el título 1 del Decreto 98-247, de 2 de abril de 1998.

#### Solicitud de cualificación para un establecimiento libre

Un profesional que trabaje en una actividad artesanal que desee tener un diploma reconocido distinto del requerido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la ACM territorialmente competente.

**Procedimiento**

Se le da un recibo al solicitante en el plazo de un mes a partir de la recepción de la solicitud de la CMA. Si el expediente está incompleto, la autoridad informa al interesado dentro de los quince días de la presentación.

**Documentos de apoyo**

La carpeta debe contener las siguientes partes:

- Solicitar un certificado de cualificación profesional
- Un certificado de competencia o el diploma o certificado de formación profesional del solicitante;
- prueba de nacionalidad
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, una prueba de ejercicio de esta actividad durante tres años.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (CIEP) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**Qué saber**

Si es necesario, todos los documentos justificativos deben traducirse al francés.

**Tiempo de respuesta**

Dentro de los tres meses siguientes a la emisión del recibo, la CMA podrá decidir:

- Reconocer la cualificación profesional y emitir la certificación de cualificación profesional;
- someter al solicitante a una medida de compensación y notificarle dicha decisión;
- negarse a expedir el certificado de cualificación profesional.

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Remedios**

Si la CMA se niega a emitir el reconocimiento de la cualificación profesional, el solicitante podrá iniciar, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, una impugnación legal ante el tribunal administrativo pertinente.

Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de indemnización, primero debe iniciar un recurso agraciado ante el prefecto del departamento en el que se basa la CMA, en el plazo de dos meses a partir de la notificación de la decisión. Cma. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### e. Algunas peculiaridades de la regulación de la actividad

#### Disposiciones específicas para el profesional responsable del mantenimiento de espacios verdes y el uso de fitofarmacéuticos y/o biocidas

**Información de salud**

El profesional que trabaja como paisajista encargado del mantenimiento de los espacios verdes está sujeto al cumplimiento de las disposiciones relativas a la vigilancia, la prevención y la lucha contra los riesgos para la salud.

*Para ir más allá* Artículos L. 201-1 y siguientes del Código Rural y Pesca Marina.

**Copia del certificado de seguro**

El profesional que realiza esta actividad deberá facilitar al prefecto regional una copia de su certificado de seguro de responsabilidad profesional cada año.

*Para ir más allá* Artículos R. 254-19 y siguientes del Código Rural y Pesca Marina.

#### Disposiciones comunes

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

**Caso de trabajadores jóvenes**

El profesional deberá cumplir las disposiciones relativas a los trabajadores jóvenes, incluida la prohibición de confiarles:

- trabajo temporal en alturas:
  - cuando la prevención del riesgo de caídas no esté garantizada por medidas de protección colectiva,
  - tratar con árboles y otras especies leñosas y semi-leñosas;
- la instalación y desmontaje de andamios;
- manipulación, monitoreo o control de dispositivos presurizados, misiones en lugares confinados o con materiales fundidos.

*Para ir más allá* Artículos D. 4153-30 y los siguientes artículos del Código de Trabajo.

**Seguro**

Además del seguro obligatorio de responsabilidad civil, el profesional que se dedica a la construcción de obras paisajísticas puede estar obligado a contratar un seguro que cubra las garantías de diez años y bienales.

*Para ir más allá* Artículos 1779 y siguientes del Código Civil.

**Servicios humanos**

Un profesional que realiza trabajos de jardinería como parte de una actividad de servicios humanos puede informar de su actividad a la dirección regional de negocios, competencia, consumo, trabajo y empleo (Direccte).

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Siga el curso de preparación de la instalación (SPI)

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo una educación en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que son:

- han ejercido una actividad profesional que requiere un nivel de conocimientos equivalente al proporcionado por las prácticas durante al menos tres años;
- conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que cubriría, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con en Francia para dirigir una empresa de artesanías.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982; Artículo 6-1 del Decreto 83-517 de 24 de junio de 1983.

### b. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte la "Formalidad de la presentación de informes de una empresa comercial", "Registro de negocios individuales en el Registro de Comercio y Empresas" y "Formalidades de Informes Corporativos. trabajo artesanal."

### c. Si es necesario, registre los estatutos de la empresa

Una vez que los estatutos de la empresa han sido fechados y firmados, el paisajista debe registrarlos en la Oficina del Impuesto de Sociedades (SIE) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio cuando los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

### d. Solicitud de aprobación para empresas, en caso de uso de fitofarmacéuticos

**Certificación de pre-aprobación**

El profesional responsable del mantenimiento de los espacios verdes y del uso de fitofarmacéuticos deberá, antes de su solicitud de aprobación, obtener una certificación de la empresa.

La emisión de esta certificación es posterior a una auditoría realizada por un organismo certificador encargado de verificar que se cumplen adecuadamente las condiciones de propiedad del certificado individual.

**Tenga en cuenta que**

Esta certificación previa se transmitirá.

*Para ir más allá* Artículo R. 254-3 del Código Rural y Pesca Marina; Orden de 25 de noviembre de 2011 relativa al marco de certificación previsto en el artículo R. 254-3 del Código Rural y de la Pesca Marítima "organización general".

**Formalidades para solicitar la acreditación**

Para llevar a cabo esta actividad, el profesional debe obtener una certificación de ejercicio. Para ello, debe:

- ser el titular de la certificación previa (véase más arriba "3.00). d. Precertificación de la solicitud de acreditación");
- Haber contratado un seguro de responsabilidad civil profesional
- Ser reconocido por una organización externa como que opera con respeto a la protección de la salud pública y el medio ambiente y la buena información del usuario;
- han celebrado un contrato de seguimiento necesario para mantener la certificación.

**Autoridad competente**

El prefecto de la región en la que se encuentra la sede central de la empresa es competente para emitir esta acreditación.

**hora**

La falta de respuesta del prefecto regional es el caso de que se rechace la solicitud de acreditación.

*Para ir más allá* Artículo L. 254-1; Artículos R. 254-15 a R. 254-19 de R. 254-3 del Código Rural y Pesca Marina; Decreto No 2011-1325, de 18 de octubre de 2011, por el que se establecen las condiciones para la expedición, renovación, suspensión y retirada de las aprobaciones comerciales y certificados individuales para la venta, distribución gratuita, consejos sobre el uso de fitofarmacéuticos.