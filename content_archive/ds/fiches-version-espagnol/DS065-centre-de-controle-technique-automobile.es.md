﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS065" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Experiencia" -->
<!-- var(title)="Centro de Control Técnico Automotriz" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="experiencia" -->
<!-- var(title-short)="centro-de-control-tecnico-automotriz" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/experiencia/centro-de-control-tecnico-automotriz.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="centro-de-control-tecnico-automotriz" -->
<!-- var(translation)="Auto" -->


Centro de Control Técnico Automotriz
====================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El Centro de Control Técnico de Automoción es un establecimiento encargado de comprobar el estado de trabajo de los vehículos ligeros (menos de 3,5 toneladas) y pesados (peso de carga total superior a 3,5 toneladas).

El centro se puede integrar en una red de control o no.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una profesión artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para operar un centro de control técnico automotriz, la persona debe haber completado un curso de capacitación de 35 horas en una organización de capacitación, o debe tener un certificado de prácticas que justifique la realización de uno de los formación para permitir el acceso a la profesión de controlador técnico automotriz.

Para ser un controlador técnico de vehículos ligeros, el individuo debe tener un Certificado de Cualificación Profesional (CQP) o una designación profesional como controlador técnico de vehículos ligeros o cumplir con los siguientes requisitos:

#### Cualificaciones adquiridas en Francia 

El interesado deberá justificar una de las siguientes cualificaciones:

- un diploma de Nivel IV del Ministerio de Educación (Mantenimiento de Vehículos Profesionales Opción de Bachillerato coches privados o licenciatura en opción de mantenimiento de vehículos profesionales vehículos industriales o vehículos por carretera) o un diploma equivalente al Directorio Nacional de Certificaciones Profesionales (RNCP);
- un diploma de Nivel III del Departamento de Educación (diploma de experto en automoción o opción de certificado de técnico de automóvil estimóculo "vehículos especiales" o opción de auto post-venta de técnico superior " (vehículos industriales")," o un título equivalente al Directorio Nacional de Certificaciones Profesionales (RNCP). ;
- y formación inicial sobre el control técnico de vehículos ligeros, consistente en una parte teórica en un centro de formación de al menos 245 horas y una parte práctica en un centro especializado de al menos 70 horas.

#### Cualificaciones adquiridas en otro Estado miembro de la UE o en un Estado parte en el acuerdo del Espacio Económico Europeo 

El interesado deberá justificar la formación previa sancionada por un certificado reconocido por el Estado de origen o considerada plenamente válida por un organismo profesional competente y una experiencia de tres años, en los diez años anteriores, en los diez años anteriores, en controlador técnico de vehículos ligeros.

*Ser un controlador técnico de vehículos pesados*, el individuo debe justificar un Certificado de Cualificación Profesional (CQP) o una designación profesional como controlador técnico de vehículos pesados o cumplir con los siguientes requisitos:

#### Cualificaciones adquiridas en Francia 

El interesado deberá justificar una de las siguientes cualificaciones:

- un diploma de Nivel IV del Ministerio de Educación Nacional (bachillerato profesional "mantenimiento de vehículos" opciones "coches privados" o "vehículos industriales") o un diploma equivalente al directorio nacional de certificaciones (RNCP);
- un diploma de Nivel III del Ministerio de Educación Nacional (diploma de experto automotriz, certificado técnico superior "posventa automotriz" opciones "vehículos especiales" o "vehículos industriales" o certificado de técnico superior " mantenimiento de vehículos" opciones "coches privados" o "vehículos de transporte por carretera") o un diploma equivalente al Directorio Nacional de Certificaciones Profesionales (RNCP);
- y formación inicial sobre el control técnico de vehículos pesados, consistente en una parte teórica en un centro de formación de al menos 245 horas y una parte práctica en un centro de control técnico para vehículos pesados de al menos 105 horas.

#### Cualificaciones adquiridas en otro Estado miembro de la UE o en un Estado parte en el Acuerdo sobre el Espacio Económico Europeo 

El interesado deberá justificar la formación previa sancionada por un certificado reconocido por el Estado de origen o considerada plenamente válida por un organismo profesional competente y una experiencia de tres años consecutivos, durante los diez años consecutivos como controlador técnico para vehículos pesados.

*Para ir más allá* Apéndice IV del Decreto de 18 de junio de 1991 sobre el establecimiento y la organización del control técnico de vehículos que no pesen más de 3,5 toneladas; Apéndice IV de la orden de 27 de julio de 2004 sobre el control técnico de los vehículos pesados.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Para un ejercicio temporal e informal (LPS)

El nacional de un Estado de la UE o del EEE, legalmente establecido en uno de estos Estados, puede utilizar su título de responsable técnico en Francia, ya sea de forma temporal o ocasional.

Tendrá que reportarlo al prefecto del departamento en el que planea llevar a cabo su actividad (ver infra "3o). b. Hacer una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)).

Cuando ni la profesión ni la formación estén reguladas en ese estado, debió haber participado en esta actividad durante al menos un año en los últimos diez años.

*Para ir más allá* Artículo L. 323-1 de la Ley de Tráfico de Carreteras.

#### Para un ejercicio permanente (LE)

El nacional de un Estado de la UE o del EEE, establecido y ejerce legalmente como responsable técnico en dicho Estado, podrá ejercer la misma actividad en Francia de forma permanente.

Las formalidades y procedimientos aplicables a la UE o al nacional del EEE son los mismos que para el nacional francés.

### c. Condiciones de honorabilidad

El controlador técnico automotriz no debe haber sido objeto de una condena en la segunda votación del historial penal.

Además, el controlador no debe participar en la reparación o el comercio automotriz, ya sea como un independiente o un empleado.

La actividad del centro de control técnico tampoco debe ir acompañada de una actividad de reparación o venta de vehículos.

*Para ir más allá* Artículos L. 323-1 y R. 323-13 de la Ley de Tráfico de Carreteras.

### d. Algunas peculiaridades de la regulación de la actividad

#### Cumplimiento de las normas de seguridad y accesibilidad

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

Es aconsejable consultar la hoja "Establecimiento que recibe al público" para obtener más información.

#### Requisito de contabilización

Los operadores de los centros de control técnico deben mostrar el letrero que acredite la acreditación de la prefectura a plena vista.

*Para ir más allá* Apéndice 1 del Apéndice V del Decreto de 18 de junio de 1991 sobre el establecimiento y la organización del control técnico de vehículos que no pesen más de 3,5 toneladas; Apéndice V de la Orden de 27 de julio de 2004.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El contratista debe registrarse en el Registro Mercantil y Corporativo (SCN). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Hacer una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El prefecto del departamento en el que el nacional desea llevar a cabo un control técnico es competente para decidir sobre la solicitud de declaración previa de actividad.

**Documentos de apoyo**

La solicitud se realiza mediante la presentación de un archivo que incluye los siguientes documentos:

- Una fotocopia de un documento de identidad válido
- Un certificado que certifique que el nacional está legalmente establecido en la UE o en el Estado del EEE;
- prueba de sus cualificaciones profesionales
- prueba por cualquier medio de que el nacional ha sido un controlador técnico durante al menos 1 año en los últimos diez años;
- una copia del contrato de trabajo o una carta de compromiso del centro de control del empleador.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

El prefecto del departamento tendrá un mes para enviar un recibo de la declaración al nacional. Si el prefecto se niega, el nacional podrá presentar una impugnación legal ante el tribunal administrativo en el plazo de dos meses a partir de la notificación de la decisión.

### c. Siga el curso de preparación de la instalación (SPI)

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

Exención de pasantías para los nacionales de la UE o del EEE: en principio, un profesional cualificado que sea nacional o del EEE está exento del SPI si justifica con la CMA una cualificación en la gestión empresarial que le otorgue un nivel de equivalente a lo previsto en la pasantía.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que son:

- han ejercido una actividad profesional que requiere un nivel de conocimientos equivalente al proporcionado por las prácticas durante al menos tres años;
- conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que cubriría, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con en Francia para dirigir una empresa de artesanías.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, artículo 6-1 del Decreto 83-517, de 24 de junio de 1983.

### d. Buscar la aprobación de la prefectura para el controlador técnico

#### Para vehículos ligeros que no superen las 3,5 toneladas

**Autoridad competente**

El prefecto del departamento es competente para decidir sobre la solicitud de acreditación.

**Documentos de apoyo**

La solicitud de aprobación de la prefectura se presenta mediante la presentación de un expediente de doble copia que incluye los siguientes documentos justificativos:

- una solicitud de certificación como responsable del tratamiento, indicando el centro de control (así como la red de control aprobada a la que finalmente se adjunta) en la que el solicitante tiene la intención de llevar a cabo su actividad en la capacidad principal, y especificando en qué capacidad ( operador o empleado);
- Boletín 2 de sus antecedentes penales que demuestre que el solicitante no fue condenado (documento solicitado directamente por el prefecto del historial penal nacional);
- Una copia válida de un documento para justificar la identidad del controlador;
- los documentos justificativos para la calificación necesaria para llevar a cabo la actividad del responsable del tratamiento (véase el apéndice IV) acompañados de una hoja de resumen coherente con el modelo del apéndice 1 de este apéndice. Si un extranjero es extranjero, deberá presentar un documento equivalente preparado hace menos de tres meses en la fecha de la solicitud de acreditación y escrito en francés o acompañado de una traducción oficial;
- la opinión de la red de control aprobada de la que depende el solicitante, o en el caso de un responsable no vinculado a una red, la opinión del organismo técnico central, siguiendo el modelo del apéndice 2 del presente apéndice;
- Si el responsable del tratamiento es un empleado, una copia del contrato de trabajo o una carta de compromiso del centro de control del empleador;
- una declaración de honor, siguiendo el modelo del apéndice 3 de este apéndice, que certifique la exactitud de la información proporcionada, certificando que no está sujeta a una suspensión o retirada de la acreditación, y comprometiéndose a no ejercer, durante la duración de la certificación, cualquier actividad en la reparación o el comercio de automóviles y no utilizar los resultados de los controles para cualquier propósito distinto de los previstos por la normativa.

**Procedimiento**

El silencio del prefecto en un plazo de cuatro meses vale la pena aceptar la solicitud de acreditación.

#### Para vehículos pesados

**Autoridad competente**

El prefecto del departamento es competente para decidir sobre la solicitud de acreditación.

**Documentos de apoyo**

La solicitud de aprobación de la prefectura se presenta presentando un expediente en dos copias, incluidos los siguientes documentos justificativos:

- Solicitar la certificación como controlador
- Una copia de un documento de identidad válido
- Una copia del boletín 2 del antecedente penal;
- Una hoja de resumen de la experiencia y calificación del controlador;
- Una declaración de honor que certifique la exactitud de la información proporcionada;
- la opinión de la red de control aprobada de la que depende el solicitante o en el caso de un responsable no vinculado a una red, la opinión del organismo técnico central.

### e. Solicitar la aprobación de la prefectura para el centro de control técnico

#### Para vehículos pesados

**Autoridad competente**

El prefecto del departamento del lugar de establecimiento del centro es competente para decidir sobre la solicitud de acreditación.

**Documentos de apoyo**

La solicitud de acreditación debe presentarse en dos copias para un centro de control conectado a una red y tres copias para un centro no conectado a la red.

Para el centro de control conectado a una red, las piezas son:

- Solicitud de aprobación con membrete;
- una certificación de la red de control, siguiendo el modelo del apéndice 5 de este apéndice, que certifica que las instalaciones han sido objeto de una auditoría inicial favorable (con indicación de la fecha y referencia del informe) y que el expediente está de acuerdo con el requisitos de este capítulo, y una copia del informe de auditoría original;
- las especificaciones a que se refiere el artículo R. 323-14 de la Ley de Tráfico por Carretera incluyen:- Una descripción de la organización y los medios materiales, siguiendo el modelo del apéndice 7 de este apéndice y la lista de controladores relacionados,
  - Un plan de situación para identificar el derecho de vía inmobiliaria y la zona de control en relación con el medio ambiente,
  - un plan de masa a 1/100 escala que muestre todas las superficies cubiertas e indique la ubicación del equipo de control,
  - el compromiso del solicitante, siguiendo el modelo del apéndice 6 de este apéndice,
  - El modelo de los minutos que se utilizarán en el centro de control;
- Copia de la notificación de aprobación de red para el control técnico de vehículos pesados;
- la prueba de que la instalación forma parte del perímetro de acreditación de la red de conformidad con los artículos 22 y 32 de esta orden o un recibo emitido por el organismo acreditador que indique que la solicitud de integración de la instalación dentro del perímetro de acreditación de la red se ha presentado;
- compromiso del solicitante de cumplir con las especificaciones anteriores.

Para un centro de control independiente, las piezas son:

- Una solicitud de aprobación en membrete
- una prueba relativa al registro en el directorio de comercios o al registro de comercios y empresas que datan de hace menos de tres meses, en los que se identifica el establecimiento correspondiente al centro de control;
- una copia del certificado de acreditación del quirófano o sociedad operadora o un recibo expedido por el organismo acreditador según lo estipulado en el artículo 22 de esta orden que certifique que el centro ha presentado, para su certificación, su sistema de plena calidad y de acuerdo con la norma NF EN ISO/CIS 17020: 2012;
- las especificaciones a que se refiere el artículo R. 323-14 de la Ley de Tráfico por Carretera incluyen:- Una descripción de la organización y los medios materiales, siguiendo el modelo del apéndice 7 de este apéndice y la lista de controladores relacionados,
  - Un plan de situación para identificar el derecho de vía inmobiliaria y la zona de control en relación con el medio ambiente,
  - un plan de masa a 1/100 escala que muestre todas las superficies cubiertas e indique la ubicación del equipo de control,
  - el compromiso del solicitante, siguiendo el modelo del apéndice 6 de este apéndice,
  - procedimientos del párrafo 1.2 del Apéndice V de esta orden,
  - El modelo de los minutos que se utilizarán en el centro de control;
- Referencias técnicas para evaluar la experiencia del solicitante en el control técnico;
- El informe de auditoría inicial favorable preparado por un organismo aprobado por el Ministro de Transportes;
- la opinión del organismo técnico central siguiendo el modelo del apéndice 8 de este apéndice (aviso solicitado directamente por el prefecto al organismo técnico central que recibe la solicitud de acreditación);
- La certificación del cumplimiento de la herramienta informática expedida por la CTA en virtud de lo dispuesto en el artículo 37 de esta orden;
- compromiso del solicitante de cumplir con las especificaciones anteriores.

*Para ir más allá* : orden de 27 de julio de 2004 relativo al control técnico de los vehículos pesados.

#### Para vehículos ligeros que no superen las 3,5 toneladas

**Autoridad competente**

El prefecto del departamento del lugar de establecimiento del centro es competente para decidir sobre la solicitud de acreditación.

**Documentos de apoyo**

La solicitud de acreditación debe presentarse en tres copias y debe incluir los siguientes documentos justificativos.

Para un centro de control de red, las piezas son:

- Una solicitud de aprobación en membrete
- una prueba relativa al registro en el directorio de comercios o al registro de comercios y empresas que datan de hace menos de tres meses, en los que se identifica el establecimiento correspondiente al centro de control;
- Una certificación de pertenencia a una red de control aprobada, siguiendo el apéndice 5 modelo de este apéndice;
- una certificación de la red de control que certifica que el centro de control ha sido objeto de una auditoría inicial favorable (con indicación de la fecha y referencia del informe) y que el expediente cumple con los requisitos de este capítulo, y una copia de la Informe de la auditoría inicial;
- las especificaciones a que se refiere el artículo R. 323-14, párrafo 2, de la Ley de Tráfico por Carretera incluyen:- Una descripción de la organización y los medios materiales, siguiendo el modelo del apéndice 6 de este apéndice y la lista de controladores relacionados,
  - Un plan de situación para identificar el derecho de vía inmobiliaria y la zona de control en relación con el medio ambiente,
  - un plan de masa a escala 1/100 que muestre todas las superficies cubiertas e indique la ubicación del equipo de control,
  - el compromiso del solicitante, siguiendo el apéndice 4 del presente apéndice,
  - El modelo de los minutos que se utilizarán en el centro de control,
  - compromiso del solicitante de cumplir con las especificaciones anteriores.

Para un centro de control independiente:

- Una solicitud de aprobación en membrete
- una prueba relativa al registro en el directorio de comercios o al registro de comercios y empresas que datan de hace menos de tres meses, en los que se identifica el establecimiento correspondiente al centro de control;
- Un informe de auditoría inicial favorable preparado por un organismo acreditado;
- la opinión del organismo técnico central, siguiendo el modelo del Apéndice 7 del presente apéndice (aviso solicitado directamente por el prefecto al organismo técnico central que recibe la solicitud de acreditación);
- las especificaciones a que se refiere el artículo R. 323-14, párrafo 2, de la Ley de Tráfico por Carretera incluyen:- Una descripción de la organización y los medios materiales, siguiendo el modelo del apéndice 6 de este apéndice y la lista de controladores relacionados,
  - Un plan de situación para identificar el derecho de vía inmobiliaria y la zona de control en relación con el medio ambiente,
  - Un plan de masa a escala 1/100 que muestra todas las superficies cubiertas e indica la ubicación del equipo de control;
- compromiso del solicitante, siguiendo el modelo del apéndice 4 de este apéndice:- establecer todos los documentos, relativos a su actividad, prescritos por el Ministro responsable del transporte,
  - facilitar la misión de los oficiales designados por estos últimos para supervisar el buen funcionamiento de los centros de control,
  - firmar el acuerdo de asistencia técnica previsto en el artículo 29 de esta orden;
- los procedimientos internos del centro de control para garantizar el cumplimiento de los requisitos del artículo R. 323-14 de la mencionada Ley de Tráfico por Carretera, así como el párrafo 1 del Capítulo II del Título II de esta orden, incluyendo:- aprobación y autorización de un controlador técnico,
  - organización de la formación y cualificación de los controladores técnicos,
  - dominio del software de control técnico,
  - integridad, seguridad y mantenimiento del sistema informático,
  - gestión, mantenimiento y mantenimiento de equipos de control,
  - transmisión de datos sobre los controles técnicos realizados,
  - explotación de los indicadores proporcionados por el organismo técnico central,
  - auditoría de las instalaciones de control y controladores,
  - gestión y archivo de las actas de control técnico,
  - organización y realización de controles técnicos,
  - métodos alternativos de prueba en caso de imposibilidad de control,
  - tratamiento de los remedios amistosos a disposición del público,
  - gestión de la base documental de los textos reglamentarios y su evolución,
  - gestión de herramientas específicas del gas para los centros interesados;
- La certificación del cumplimiento de la herramienta informática expedida por la CTA en virtud de lo dispuesto en el artículo 29 de esta orden;
- El modelo de los minutos que se utilizarán en el centro de control;
- compromiso del solicitante de cumplir con las especificaciones anteriores.

*Para ir más allá* : orden de 18 de junio de 1991 relativa al establecimiento y organización del control técnico de vehículos que no pesen más de 3,5 toneladas.

