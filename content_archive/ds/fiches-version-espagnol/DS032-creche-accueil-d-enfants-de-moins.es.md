﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS032" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Cr'che - Hogar para niños menores de 6 años" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="cr-che-hogar-para-ninos-menores" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/cr-che-hogar-para-ninos-menores-de-6-anos.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="cr-che-hogar-para-ninos-menores-de-6-anos" -->
<!-- var(translation)="Auto" -->


Cr'che - Hogar para niños menores de 6 años
===========================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La función de las instituciones y los servicios de atención comunitaria es mantener a los niños menores de seis años de manera no permanente y garantizar su salud, seguridad, bienestar y desarrollo.

Estos establecimientos de alojamiento, de forma ocasional o estacional, pueden adoptar las siguientes formas:

- Ser administrado por una persona privada (física o moral);
- público y/o educativo (ocio, vacaciones escolares, permiso de trabajo) fuera del hogar de los padres.

Esto incluye:

- instalaciones colectivas de acogida que incluyen:- viveros colectivos,
  - guarderías,
  - servicios de cuidado familiar o guarderías familiares que proporcionan atención no permanente a los niños en los hogares de asistentes maternos (la misma institución o la llamada "multi-host" puede asociar cuidados colectivos y de crianza o hospitalidad regular y ocasional;
- Viveros parentales dirigidos por la asociación de padres;
- jardines de infancia (incluidos exclusivamente los niños mayores de dos años de escolarización o de escolarización a tiempo parcial);
- micro-creches con una capacidad limitada a diez plazas.

**Tenga en cuenta que**

La capacidad de estos establecimientos es limitada en función de la naturaleza del establecimiento.

*Para ir más allá* Artículos R. 2324-17 y las siguientes secciones del Código de Salud Pública.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la estructura en la que se lleve a cabo la actividad:

- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para una profesión liberal, es el Urssaf;
- para las sociedades civiles, es el registro del tribunal comercial o el registro del tribunal de distrito (para los departamentos del Bajo Rin, el Alto Rin y el Mosela, es el registro del tribunal de distrito).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El profesional que desee dirigir un centro de acogida para niños menores de seis años debe justificar cualificaciones profesionales que varían en función de la naturaleza de la institución y de la función del profesional.

**Tenga en cuenta que**

Una vez que el profesional está calificado para dirigir una de las siguientes instituciones, puede desempeñar las mismas funciones en una institución de menor capacidad.

#### Director

**Establecimiento de más de cuarenta y un lugares**

Un profesional que desee dirigir una institución debe tener uno de los siguientes títulos:

- un título médico estatal;
- un diploma estatal como niñera con al menos tres años de experiencia profesional;
- un diploma estatal como educador de niños pequeños proporcionó:- justifica la certificación que acredite su competencia en el ámbito de la gestión o la gestión,
  - justifica tres años de experiencia profesional,
  - que el establecimiento o servicio incluye a una persona de tiempo completo:- un diploma estatal como niñera,
    * un título estatal de enfermería que justifique al menos un año de experiencia con niños pequeños.

**Tenga en cuenta que**

El director de una institución con capacidad para más de sesenta plazas debe estar asistido por un asistente titular de uno de los títulos anteriores o mencionado en los artículos R. 2324-35 y R. 2324-46.

*Para ir más allá* Artículos R.2324-34 y las siguientes secciones del Código de Salud Pública.

**Establecer una capacidad de cuarenta o menos**

Los siguientes profesionales pueden estar a cargo de un centro de acogida para niños menores de seis años con capacidad de menos de cuarenta plazas o la responsabilidad técnica de una instalación de crianza:

- Un niñera certificado con tres años de experiencia profesional
- un educador de niños pequeños con tres años de experiencia profesional si practica con la ayuda de un profesional con al menos un año de experiencia con niños pequeños y un graduado estatal como:- niños,
  - Enfermera.

*Para ir más allá* Artículo R. 2324-35 del Código de Salud Pública.

**Instalaciones con una capacidad de 20 o menos**

Los siguientes profesionales pueden administrar un centro de cuidado infantil para niños menores de seis años, con una capacidad inferior a 20 plazas o menos:

- Un niñera certificado con tres años de experiencia profesional
- un educador de niños pequeños con tres años de experiencia laboral.

*Para ir más allá* Artículo R. 2324-35 del Código de Salud Pública.

**Qué saber**

Las microcres, cuya capacidad se limita a diez plazas, no están obligadas a tener un director, sino una referencia técnica. Sin embargo, cuando varios establecimientos son gestionados por la misma persona y la capacidad total de estos establecimientos es superior a 20 plazas, este último está obligado a designar un director.

*Para ir más allá* Artículo R. 2324-36 y R. 2324-36-1 del Código de Salud Pública.

**Medidas derogatorias**

Si ningún profesional que cumpla con los requisitos de cualificación anteriores puede administrar un centro de cuidado infantil para niños menores de seis años, se prevén exenciones en función del tamaño de la institución de que se trate.

Así, la gestión de un establecimiento o servicio con capacidad de más de cuarenta plazas puede confiarse a un titular profesional:

- un diploma estatal como educador de niños pequeños si justifica tres años de experiencia laboral, incluyendo al menos dos años como:- Director
  - Subdirector
  - gerente técnico de dicho establecimiento.
- grado estatal como partera o enfermera y justificando:- Tres años de experiencia profesional como director o subdirector de dicha institución;
  - o una certificación mínima de Nivel II, inscrita en el directorio nacional de certificaciones profesionales que acrediten sus habilidades de coaching o gestión y de tres años de experiencia con niños menores de tres años.

El profesional puede gestionar instituciones con una capacidad de entre veinte y uno y cuarenta puestos siempre y cuando justifique un diploma de Estado como asistente de servicio social, un educador especialista, un asesor de economía social y familiar. , psicomotricista, o un DESS o un maestro 2 que justifique:

- Tres años de experiencia como director, subdirector o jefe de dicha institución;
- Certificación mínima de Nivel II en el directorio nacional de certificaciones profesionales que justifica habilidades en coaching o gestión y tres años de experiencia con niños menores de tres años.

El profesional puede gestionar una institución de veinte plazas o menos si posee uno de los siguientes diplomas:

- un diploma estatal:- Partera
  - Enfermería
  - asistente de servicio social,
  - educador especialista,
  - asesor de economía social y familiar,
  - psicomotriz;
- un título de posgrado especializado (DESS) o un máster II (B.A. 5) en psicología y que garantiza tres años de experiencia como director, subdirector o gerente técnico de dicha institución o tres años de experiencia con niños pequeños.

El profesional que ha estado a cargo de una institución de crianza durante tres años puede dirigir una institución dirigida por una persona privada.

*Para ir más allá* Artículo R. 2324-46 del Código de Salud Pública.

#### Personal

La institución para niños menores de seis años debe estar formada por profesionales con los siguientes títulos para al menos el 40% de la plantilla:

- un diploma estatal como niñera;
- un diploma estatal como educador de niños pequeños;
- un diploma como asistente de cuidado infantil;
- un título estatal de enfermería;
- un título estatal como un psicomomotrician.

Además de estos profesionales, la institución debe estar formada por titulares profesionales de hasta el 60% de la plantilla:

- títulos de formación:- un certificado de aptitud profesional de la primera infancia,
  - un certificado de trabajador familiar o un diploma estatal como técnico de intervención social y familiar,
  - un certificado estatal como técnico facilitador de la educación popular y la juventud, opción de primera infancia,
  - un certificado de estudios profesionales, salud y opción social,
  - Un certificado de aptitud para las funciones de ayuda en el hogar
- cinco años de experiencia profesional como asistente de guardería
- tres años de experiencia profesional con niños en una institución pública para niños menores de seis años.

*Para ir más allá* Artículo R. 2323-42 del Código de Salud Pública y auto de 26 de diciembre de 2000 relativo al personal de instituciones y servicios para niños menores de seis años.

**Tenga en cuenta que**

Si una institución o servicio tiene una capacidad de más de diez plazas, dependiendo del número de niños y su edad, debe estar compuesto por profesionales cualificados en los siguientes campos:

- Psicología
- Social
- Salud;
- Educativo;
- Cultural.

Si esta capacidad supera los diez puestos, el personal debe estar compuesto por un médico especialista o pediatra, o, en su defecto, un médico general con experiencia profesional en el campo de la pediatría.

*Para ir más allá* Artículos R. 2324-38 y R. 2324-39 del Código de Salud Pública.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o ejercicio libre (LE))

No existe un reglamento para un nacional de un miembro de la Unión Europea (UE) o parte del Espacio Económico Europeo (EEE) que desee ejercer en un centro de guardería menor de seis años en Francia, como miembro del Espacio Económico Europeo (EEE) temporal y casual o permanente.

Por lo tanto, el nacional de la UE o del EEE estará sujeto a las mismas condiciones de instalación que el profesional francés (véase más arriba "2o. Condiciones de instalación").

### c. Condiciones de honorabilidad

Para ejercer como profesional en un centro de acogida para niños menores de seis años, el profesional no debe haber sido incapacitado y/o condenado definitivamente por:

- o una pena de al menos dos meses de prisión por personas perjudicadas;
- Agresión sexual
- dañar o poner en peligro a los menores;
- Recepción;
- uso ilícito de narcóticos.

Además, el administrador de dicha institución debe asegurarse de que todo el personal no esté sujeto a las condenas anteriores.

*Para ir más allá* Artículos L. 2324-1 párrafo 5 y R. 2324-33 del Código de Salud Pública; Artículo L. 133-6 del Código de Acción Social y Familias.

### d. Algunas peculiaridades de la regulación de la actividad

**Capacidad máxima**

La capacidad de alojar a niños menores de seis años en las instituciones anteriores (véase más arriba "1.0th). La definición de actividad") es limitada.

Mientras el número semanal de niños no supere el cien por cien de la capacidad proporcionada por la autorización creativa, podrá considerarse una franja infantil si no supera:

- 10% de la capacidad de establecimientos con 20 o menos plazas;
- 15% de la capacidad de establecimientos entre veintiún y cuarenta lugares;
- 20% de la capacidad de establecimientos de cuarenta y uno o más lugares.

*Para ir más allá* Artículo R. 2324-27 del Código de Salud Pública.

**Delegación**

El gerente de un centro de cuidado temporal para niños menores de seis años puede, por escrito, delegar la gestión a un profesional. Para ello, deberá enviar una copia al presidente del consejo departamental del departamento que haya expedido la autorización para la creación del establecimiento.

Este documento contiene la siguiente información sobre el alcance de la delegación:

- La definición e implementación del proyecto de liquidación o servicio;
- Animación y gestión de recursos humanos;
- Gestión presupuestaria, financiera y contable
- coordinación con instituciones externas y partes interesadas.

*Para ir más allá* Artículo R. 2324-37-2 del Código de Salud Pública.

**Seguro de responsabilidad civil**

El gerente del establecimiento debe obtener un seguro de responsabilidad civil para sus empleados y para los voluntarios y trabajadores externos y no salariales que participen en la acogida de los niños.

Además, si se ha producido un accidente que resulta en la hospitalización o muerte de uno de los niños, el gerente debe reportarlo inmediatamente al presidente del consejo del condado.

*Para ir más allá* Artículo R. 2324-44-1 del Código de Salud Pública.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitud de autorización o aviso de creación, extensión y transformación

La creación, ampliación o transformación de un centro de acogida para niños menores de seis años está sujeta a la emisión de una autorización, cuyos términos dependen de la naturaleza de la institución.

#### Emitir una autorización para la creación de una institución o servicio de gestión privada

**Autoridad competente**

La solicitud de autorización deberá dirigirse al presidente del consejo departamental del departamento en el que se encuentre el establecimiento.

**Documentos de apoyo**

El profesional debe presentar una solicitud con:

- Un estudio de necesidades
- La dirección del servicio de establecimiento o recepción;
- Los estatutos de la institución o servicio de acogida o del órgano de gobierno de las instituciones y servicios aprobados por una persona privada;
- los objetivos, los acuerdos de acogida y los medios aplicados de acuerdo con el contexto público acogido y local (capacidad, dotación de personal, cualificación del personal, etc.);
- El proyecto de liquidación o servicio incluye:- un proyecto educativo (incluyendo arreglos para la recepción, atención, desarrollo, despertar y bienestar de los niños)
  - un proyecto social, (especificando cómo se integrará la institución o el servicio en su entorno social),
  - los servicios ofrecidos, (duración, ritmos de recepción, etc.),
  - Si es necesario, los arreglos especiales hechos para acomodar a niños con discapacidades o enfermedades crónicas,
  - Presentación de las habilidades profesionales del personal,
  - para los servicios de atención familiar, los arreglos de capacitación continua para los cuidadores de niños, su apoyo profesional y el seguimiento de los niños en el hogar,
  - Definir el lugar de las familias y su participación en la vida del establecimiento o servicio,
  - Cómo se casan las relaciones con agencias externas;
- El mapa de las instalaciones
- copia de la decisión de autorizar al público y los documentos que justifican la autorización (véase la Sección R. 11-19-29 del Código de Edificios y Hábitat).

*Para ir más allá* Artículos L. 2324-1 y R. 2324-18 del Código de Salud Pública.

**hora**

El presidente del consejo de condado, tras recibir el expediente completo, busca la opinión del alcalde del municipio del establecimiento y tiene un plazo de tres meses, a partir de la fecha de recepción del expediente completo, para tomar su decisión. La falta de una respuesta durante el período antes mencionado vale la pena el permiso de apertura de la institución.

**Tenga en cuenta que**

La autorización del presidente del consejo del condado menciona información sobre el establecimiento (capacidad, beneficios, edad de los niños, condiciones de funcionamiento) e información relativa al personal.

*Para ir más allá* Artículos R. 2324-19 y las siguientes secciones del Código de Salud Pública.

#### Emitir un aviso para la creación de una institución pública y un servicio

La autorización para la creación, ampliación y transformación de instituciones y servicios públicos es emitida por la comunidad pública tras el asesoramiento del presidente del consejo departamental.

**Autoridad competente y documentos justificativos**

El profesional envía un archivo con los mismos documentos justificativos que los requeridos para la autorización emitida a instituciones dirigidas por una persona privada (véase supra "3.3. a. Emitir una autorización para la creación de una institución o servicio de gestión privada) a la comunidad pública que busca el asesoramiento del presidente del consejo departamental.

Este último dispone de tres meses desde el momento en que se recibe el expediente completo, con el fin de dar su opinión, incluyendo:

- Ventajas
- Capacidad de recepción
- Cómo funciona
- personal de la instalación.

*Para ir más allá* Artículos R. 2324-21 y siguientes del Código de Salud Pública.

#### Instituciones educativas de acogida colectiva

La organización de estas instituciones para niños menores de seis años, fuera del período escolar, está sujeta a la emisión de una autorización del representante estatal en el departamento después de un aviso del médico a cargo del departamento protección materna de los niños.

La autoridad competente y los documentos justificativos son los mismos que los necesarios para expedir una autorización para abrir un establecimiento o servicio privado o público (véase más arriba "3". (a) Expedir una autorización para la creación de una institución o servicio de gestión privada").

*Para ir más allá* Artículo L. 2324-1 párrafo 3 del Código de Salud Pública.

### b. Formalidades de notificación de la empresa

**Autoridad competente**

El profesional debe informar de su empresa, y para ello, debe hacer una declaración con la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

El interesado debe proporcionar la[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

El centro de formalidades de negocios del CCI envía los documentos que faltan al profesional el mismo día. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### c. Si es necesario, registre los estatutos de la empresa

El profesional, una vez fechados y firmados los estatutos de la sociedad, deberá inscribirlos en la Oficina del Impuesto sobre Sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

