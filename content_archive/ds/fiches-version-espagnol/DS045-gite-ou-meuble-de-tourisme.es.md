﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS045" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Casa o turista amueblado" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="casa-o-turista-amueblado" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/casa-o-turista-amueblado.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="casa-o-turista-amueblado" -->
<!-- var(translation)="Auto" -->




Casa o turista amueblado
========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

Las cabañas se definen como turistas amueblados. Se trata de villas, apartamentos o estudios amueblados, para uso exclusivo del arrendatario, ofrecidos en alquiler a una clientela que pasa una estancia caracterizada por un alquiler por día, semana o mes, y que no elige residencia allí.

El arrendamiento debe concluirse por una duración máxima y no renovable de 90 días consecutivos a la misma persona.

*Para ir más allá* I del artículo L. 324-1-1 del Código de Turismo; Artículo 1-1 de la Ley de 2 de enero de 1970 por el que se regulan las condiciones de las actividades relativas a determinadas operaciones relacionadas con fondos inmobiliarios y comerciales.

**Es bueno saber**

Cuando los alojamientos turísticos se encuentran en las zonas rurales, se utiliza el término "casa rural".

### b. Centro competente de formalidades comerciales (CFE)

El Centro de Formalidad Empresarial (CFE) correspondiente depende de la naturaleza de la actividad:

- Si la actividad va acompañada de servicios complementarios de servicio hotelero (por ejemplo, limpieza de la casa de campo) o de ocio (por ejemplo, el alquiler de vehículos todoterreno), se trata de una actividad comercial. Por consiguiente, la CFE pertinente es la Cámara de Comercio e Industria;
- Si el alquiler de una casa de campo está vinculado a una granja o si constituye una extensión de la misma, la actividad debe ser declarada con la cámara de agricultura;
- Si el alquiler de una casa de campo es una extensión de una granja pero no es administrado por el agricultor, la CFE competente es el registro del tribunal comercial;
- Si la actividad no es agrícola ni comercial, la CFE competente es el registro del Tribunal de Comercio.

**Tenga en cuenta que**

Este es el registro del tribunal de distrito en los departamentos de Baja Rin, Alto Rin y Mosela.

*Para ir más allá* Artículo R. 123-3 del Código de Comercio y Sección L. 311-2-1 del Código Rural y Pesca Marina.

2°. Condiciones de instalación
--------------------------------------

### a. Condición de nacionalidad

No hay ningún requisito de nacionalidad para abrir una casa de campo.

Sin embargo, una vez que el arrendatario sirve bebidas alcohólicas, debe justificar que él es:

- Francés;
- un nacional de un Estado de la Unión Europea (UE) o un Estado Parte en el Acuerdo del Espacio Económico Europeo (EEE);
- o un nacional de un Estado que ha celebrado un tratado de reciprocidad con Francia, como Argelia o Canadá, por ejemplo.

Para obtener más información, consulte la tarjeta "Beverage Flow".

*Para ir más allá* Artículo L. 3332-3 del Código de Salud Pública.

### b. Condiciones de honor e incompatibilidad relacionadas con el funcionamiento del flujo de bebidas

No hay ninguna condición de honorabilidad o incompatibilidad para abrir una casa de campo.

Por otra parte, mientras el restaurador sirva bebidas alcohólicas, deberá respetar las condiciones de honor y las incompatibilidades relativas a la profesión de bebida. Para obtener más información, consulte la lista "Flujo de bebidas".

### c. Si es necesario, obtenga una licencia de explotación y una licencia para el funcionamiento de la salida de bebidas

Cuando el propietario desea vender alcohol en su casa de campo, debe obtener una licencia y una licencia.

Para obtener más información, consulte la lista "Flujo de bebidas".

### d. Algunas peculiaridades de la regulación de la actividad

#### Declarar sobre el derecho a sus obligaciones

Cualquier persona que participe o preste su asistencia por una tarifa o de forma gratuita, a través de una actividad de intermediarios o negociaciones o mediante la provisión de una plataforma digital, al alquiler de un turista amueblado sujeto al artículo L. 324-1-1 Del Código de Turismo y de los artículos L. 631-7 y siguientes del Código de Edificación y Vivienda informa al arrendatario de las obligaciones previas de presentación de informes o autorizaciones previstas en estos artículos y obtiene de él, antes de su publicación o aplicación. anuncio de alquiler en línea, una declaración de honor que acredite el cumplimiento de estas obligaciones, indicando si la vivienda constituye o no su residencia principal, así como, en su caso, el número de declaración, obtenido en virtud del III de Artículo L. 324-1-1 del Código de Turismo. Publica, en cualquier anuncio relacionado con este número de declaración proporcionado, su número de declaración.

En los municipios que hayan aplicado el procedimiento de inscripción mencionado en el artículo L. 324-1-1, el municipio podrá, hasta el 31 de diciembre del año siguiente al que se alquiló un alojamiento turístico, pedir a un intermediario, para transmitirle el número de días durante los cuales este turista amueblado fue alquilado a través de él. Transmite esta información en el plazo de un mes, recordando la dirección del proporcionado y su número de declaración. El municipio puede solicitar un recuento individualizado para una lista de turistas proporcionada dentro de un perímetro determinado. En estos mismos municipios, el intermediario ya no ofrece un alojamiento turístico declarado como la residencia principal del arrendatario cuando es consciente, especialmente cuando proporciona una plataforma digital para darle la conocimiento o control de los datos almacenados, que este proporcionado fue alquilado, a través de ella, más de cien 20 días en un solo año calendario. Cumple sus obligaciones sobre la base de la declaración sobre el honor mencionada al mismo Yo. El dispositivo de retiro de la oferta puede ser agrupado por varias personas mencionadas en el mismo I. Si es necesario, este régimen agrupado está certificado cada año antes del 31 de diciembre por un tercero independiente.

*Para ir más allá* Artículo L. 324-2-1 del Código de Turismo.

#### Ver clasificación amueblada

El arrendatario del proporcionado o su agente puede informar de la clasificación de su amueblado (cf. "d. Si es necesario, pida la clasificación del proporcionado" mostrando un signo. Debe mostrar, de manera visible dentro de lo amueblado, la decisión de clasificar.

El número de estrellas en el signo corresponde al número de estrellas otorgadas por la decisión de clasificación.

*Para ir más allá* Artículo D. 324-2 y siguiente Código de Turismo; Artículo 1 del Decreto de 22 de diciembre de 2010 sobre los signos de alojamiento turístico.

#### Proporcione un contrato de arrendamiento estacional con una descripción de los locales alquilados al inquilino 

Cualquier oferta o contrato de alquiler de temporada debe tomar forma escrita y contener la indicación del precio de venta, así como una declaración descriptiva de las instalaciones.

Bueno saber: será castigado con una multa de 3.750 euros, toda persona que, con motivo de un alquiler de temporada o una oferta de alquiler de temporada de un espacio amueblado, con fines de habitabilidad, haya proporcionado información claramente inexacta sobre la ubicación del edificio, consistencia y estado de las instalaciones, elementos de confort o mobiliario. En caso de reincidencia, la multa podrá aumentarse a 7.500 euros.

*Para ir más allá* Artículo L. 324-2 del Código de Turismo; Decreto 67-128, de 14 de febrero de 1967, por el que se reprime la producción de información inexacta en caso de una oferta proporcionada o un contrato de alquiler de temporada.

#### Cumplir con las regulaciones de las instituciones públicas (ERP) 

El arrendatario debe cumplir con las normas de seguridad y accesibilidad para los ERP cuando la capacidad máxima de alojamiento es de más de 15 personas. Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

#### Entregar la facturación al cliente 

El arrendatario debe dar nota al cliente cuando el precio del servicio sea superior o igual a 25 euros (IVA incluido).

Para los servicios con un precio inferior a 25 euros (IVA incluido), la emisión de una nota es opcional, pero debe darse al cliente si se solicita.

La nota debe mencionar:

- La fecha en que se escribió la nota
- El nombre y la dirección del proveedor
- El nombre del cliente, a menos que el cliente se oponga;
- La fecha y el lugar de ejecución del servicio.

*Para ir más allá* : decreto de 3 de octubre de 1983 relativo a la publicidad de precios para todos los servicios.

#### Pagar el impuesto de residencia 

Los municipios pueden pedir a los turistas que se alojan en su territorio que paguen un impuesto de residencia. Este impuesto permite a los municipios financiar gastos relacionados con las visitas turísticas o la protección de sus espacios naturales.

El impuesto de residencia se aplica a los adultos que no residen en la comuna y que no tienen una residencia allí por la que están sujetos al impuesto residencial.

La tasa del impuesto de residencia se establece, para cada naturaleza y para cada categoría de alojamiento, por persona y por noche de estancia.

Esta tasa se decide por deliberación del ayuntamiento tomada antes del 1 de octubre del año para ser aplicable el año siguiente. Deliberación, si existe, establece las fechas de inicio y finalización de los períodos de recogida dentro del año.

**Tenga en cuenta que**

A partir del 1 de enero de 2019, profesionales que, electrónicamente, prestan un servicio de reserva o alquiler o conexión para el alquiler de alojamientos y que son intermediarios de pago (en primer lugar, las plataformas digital) están obligados a recaudar el impuesto de residencia en nombre de los arrendatarios no profesionales y a pagarlo al municipio.

*Para ir más allá* Artículos L. 2333-26 y siguientes del Código General de Autoridades Locales.

#### Complete una hoja de políticas para clientes extranjeros 

A los efectos de prevenir disturbios en el orden público, investigaciones judiciales e investigaciones en interés de individuos, los inquilinos de los turistas amueblados están obligados a llenar, o hacer que se llene, y firmar por el extranjero, a su llegada, una tarjeta individual Policía.

Los datos personales recogidos de esta manera incluyen:

- nombres y apellidos;
- Fecha y lugar de nacimiento
- Nacionalidad
- El hogar habitual del extranjero;
- El número de teléfono móvil y la dirección de correo electrónico del extranjero;
- La fecha de llegada al establecimiento y la fecha de salida prevista.

Los niños menores de 15 años pueden aparecer en la tarjeta de un adulto.

Los registros así establecidos deberán conservarse durante un período de seis meses y entregarse, a petición de éstos, a las unidades policiales y de gendarmería. Esta transmisión se puede hacer de forma desmaterializada.

Las personas o corporaciones que alquilan locales desnudos no están sujetas a estas obligaciones.

*Para ir más allá* : orden de 1 de octubre de 2015 en virtud del artículo R. 611-42 del Código de Entrada y Residencia de Extranjeros y del Derecho de Asilo.

#### Publicidad de la cabaña 

Con el fin de dar a conocer su casa de campo, el arrendatario debe cumplir con los requisitos para la instalación y el mantenimiento de los carteles publicitarios para garantizar la protección del medio ambiente.

*Para ir más allá* Artículos L. 581-1 y siguientes artículos R. 581-1 y siguientes del Código de Medio Ambiente.

#### Si es necesario, cumpla con las regulaciones de la piscina 

##### Declarar la instalación de una piscina

Cualquier persona que instale una piscina deberá hacer la declaración al ayuntamiento del lugar de su instalación antes de su apertura.

Esta declaración, junto con un expediente justificativo, incluye el compromiso de que la instalación de la piscina cumplirá con las normas de higiene y seguridad establecidas en los artículos D. 1332-1 y siguientes del Código de Salud Pública.

*Para ir más allá* Artículos L. 1332-1 del Código de Salud Pública.

##### Mantenimiento de piscinas

La persona a cargo de una piscina está obligada a controlar la calidad del agua e informar al público sobre los resultados de dicho seguimiento, someterse a un control sanitario, cumplir con las normas y límites de calidad establecidos por decreto, y utilizar únicamente productos y procesos eficaces de tratamiento, limpieza y desinfección de agua que no supongan un peligro para la salud de los bañistas y del personal de mantenimiento y baño de la piscina. Artificial.

Para obtener más información, consulte la hoja "Grupo de operaciones, lugar de natación".

*Para ir más allá* Artículos L. 1332-1 y siguientes, D. 1332-1 y más allá del Código de Salud Pública; decreto de 7 de abril de 1981 por el que se establecen las disposiciones técnicas aplicables a las piscinas.

Mantener la piscina segura

La piscina debe estar equipada con un dispositivo de seguridad para evitar el riesgo de ahogamiento.

El dispositivo consta de:

- Una barrera protectora
- Una manta
- Refugio
- una alarma.

El incumplimiento de las disposiciones de seguridad de la piscina se castiga con una multa de 45.000 euros.

*Para ir más allá* Artículos L. 128-1 a L. 128-3, R. 128-2 y L. 152-12 del Código de Construcción y Vivienda.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Un arrendatario, ya sea individual o profesional, debe completar una declaración de inicio de actividad (Cerfa No. 11921También llamado formulario POi). Si la actividad es comercial, el arrendatario deberá declarar la apertura del depósito para ser inscrito en el Registro mercantil y de sociedades (RCS). Para más información, es aconsejable consultar la hoja de formalidad "declaración de actividad comercial".

### b. Declarar alquiler amueblado por turismo

La persona que ofrezca un alojamiento turístico clasificado o no clasificado deberá hacer una declaración en el ayuntamiento rellenando el Formulario Cerfa No. 14004*04.

Cuando el espacio residencial es la residencia principal del arrendador, está exento de pre-informes. Sin embargo, en las ciudades que tengan el procedimiento de registro en vigor, todos los alquileres turísticos, ya sea la residencia principal o secundaria, tendrán que tener un número de registro para publicar en cada anuncio de alquiler. . Las plataformas de alquiler intermedias (Airbnb, Abritel, etc.) tienen la obligación de desconectar cada anuncio que no contenga un número de registro.

En estos municipios, cualquier persona que ofrezca un alojamiento turístico que se declare como su residencia principal no podrá hacerlo más allá de ciento veinte días en el mismo año natural, salvo obligación profesional, motivo de salud o caso de fuerza destacado.

**Es bueno saber**

El incumplimiento de esta obligación de notificación se castiga con una multa de hasta 450 euros.

*Para ir más allá* Artículos L. 324-1-1, D. 324-1-1 y R. 324-1-2 del Código de Turismo; Artículo 131-13 del Código Penal.

La residencia principal se entiende como la vivienda ocupada al menos ocho meses al año.

Cualquier cambio en la información contenida en la declaración es objeto de una nueva declaración en el ayuntamiento.

**Autoridad competente**

El alcalde de la comuna donde se encuentra el amueblado.

**Exposiciones: Cerfa 14004*04**

La declaración especifica la identidad y la dirección del solicitante de registro, la dirección del alojamiento turístico, el número de habitaciones en la zona amueblada, el número de camas, los períodos de alquiler o previstos y, en su caso, la fecha de la decisión de clasificación y la nivel de clasificación de los turistas amueblados.

*Para ir más allá* Artículos L. 324-1-1 y D. 324-1-1 del Código de Turismo; Artículo 2 de la Ley de 6 de julio de 1989 para mejorar las relaciones de alquiler y modificar la Ley 86-1290, de 23 de diciembre de 1986.

### c. Si es necesario, obtenga permiso para cambiar el uso de la vivienda

En algunas ciudades, es obligatorio obtener una autorización del ayuntamiento para poder cambiar el uso del alojamiento en turismo amueblado (paso de la vivienda principal en turismo amueblado).

**Tenga en cuenta que**

Una deliberación del Ayuntamiento puede definir un sistema temporal de autorización de cambio de uso que permita a una persona física alquilar por períodos cortos de espacio para alojamiento residencial a una clientela pasajera que no elija una casa allí. Los locales de uso residencial con esta autorización temporal no cambian su destino.

El hecho de que un espacio amueblado se utiliza repetidamente durante cortos períodos de tiempo a una clientela pasajera que no elige una casa es un cambio de uso.

Este requisito de autorización se refiere a:

- municipios con más de 200.000 habitantes;
- los municipios de los departamentos de hauts-de-Seine, Seine-Saint-Denis y Val-de-Marne, independientemente de su tamaño.

En una decisión administrativa, esta obligación puede aplicarse a otros municipios.

*Para ir más allá* Artículos L. 631-7 a L. 631-9 del Código de Construcción y Vivienda.

### d. Si es necesario, solicite la clasificación de los

El arrendatario del proporcionado o su agente que desea obtener la clasificación transmite una solicitud de clasificación en el turismo amueblado a una organización de su elección. El ranking indicará al cliente un nivel de confort y servicio.

La decisión de clasificar a un turista proporcionado en una categoría, es pronunciada por la organización que realizó la visita de clasificación.

*Para ir más allá* Artículo D. 324-3 del Código de Turismo.

**Autoridad competente**

Esta visita de clasificación se realiza:

- ya sea mediante organismos acreditados por cualquier organismo europeo equivalente signatario del acuerdo multilateral adoptado como parte de la coordinación europea de los organismos de acreditación;
- o por organizaciones que, a partir del 22 de julio de 2009, poseían la acreditación requerida para la emisión de certificados turísticos.

*Para ir más allá* Artículo L. 324-1 del Código de Turismo.

**hora**

La organización que realizó la visita de presentación tiene un mes a partir de la fecha en que la visita del apartamento amueblado terminó a entregar al arrendatario del proporcionado o a su agente el certificado de visita, que incluye:

- Un informe de control que acredite el cumplimiento de la tabla de clasificación en la categoría solicitada;
- La cuadrícula de control proporcionada por el evaluador
- una propuesta de decisión de clasificación para la categoría indicada en el informe de control.

El arrendatario del prdecino o de su agente dispone de un plazo de quince días a partir de la recepción de este certificado de visita para rechazar la propuesta de clasificación.  Al final de este período y en ausencia de denegación, se adquiere la clasificación.  La clasificación se pronuncia por un período de cinco años.

**Documentos de apoyo**

Cerfa 11819*03

*Para ir más allá* Artículo D. 324-4 del Código de Turismo.

#### Si es necesario, obtenga una etiqueta (Géte de France, Clévacances, Fleur de soleil, etc.)

Cualquier persona con un espacio rural puede crear alojamiento y recibir la etiqueta de la marca de la empresa de Francia si el alojamiento tiene los criterios necesarios para la lista de mazorcas.

Para obtener más información, se recomienda que consulte[sitio de las Gotes de Francia](https://www.gites-de-france.com/obtenir-le-label.html).

