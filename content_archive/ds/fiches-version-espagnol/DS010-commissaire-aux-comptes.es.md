﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS010" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sector financiero y jurídico" -->
<!-- var(title)="Auditor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sector-financiero-y-juridico" -->
<!-- var(title-short)="auditor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/sector-financiero-y-juridico/auditor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="auditor" -->
<!-- var(translation)="Auto" -->


Auditor
=======

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El Auditor ("CAC") es un profesional cuya misión principal es certificar que las cuentas anuales son regulares y sinceras y dar una imagen precisa de los resultados de las operaciones del año pasado, así como la situación financiera y activos de la empresa al final de este año fiscal. Interviene en una entidad a la que ha sido nombrado voluntaria u obligatoriamente. Durante su misión, se le pedirá que revele al fiscal cualquier irregularidad que haya conocido.

El ACC también está obligado a informar sobre los principales acontecimientos en la vida de la empresa, como una ampliación de capital, una transformación o el pago de dividendos.

En principio, el CAC se designa para seis ejercicios. Bajo ciertas condiciones, como en un grupo o en el caso de la certificación voluntaria de una empresa situada por debajo de los umbrales de la pequeña empresa, puede ser designada para tres ejercicios. Las disposiciones limitan la posibilidad de renovar el mandato del CAC en algunos sectores e imponen una rotación de los signatarios (Pie de comercio y filiales de pie de comercio, entidades jurídicas privadas no comerciales con actividad económica y que superen asociaciones que reciben subvenciones por encima de un umbral: véanse los artículos L. 822-14 y L.823-3-1 del Código de Comercio).

### b. CFE competente

El centro de formalidades comerciales (CFE) correspondiente varía en función de la forma jurídica elegida por el interesado para resolver:

- Si la actividad se lleva a cabo en una empresa individual, Urssaf es competente;
- Si la actividad se lleva a cabo en forma de empresa mercantil, la Cámara de Comercio e Industria (CCI) es competente;
- Si la actividad se lleva a cabo en forma de sociedad civil o de empresa ejercerla liberal, es competente el registro del Juzgado de lo Mercantil o el registro del tribunal de distrito de los departamentos del Bajo Rin, el Alto Rin y el Mosela.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para ejercer su profesión, el CAC debe incluirse en la lista elaborada por el Consejo Superior de la Oficina del Comisario de Auditores.

Para ello, deberá cumplir las siguientes condiciones:

- ser nacional francés o ser nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE) u otro Estado extranjero cuando admita a los nacionales franceses a ejercer el control legal de la Cuentas
- no haber sido condenado penalmente por actos contrarios al honor o a la probidad, o haber recibido una sanción disciplinaria que conduzca a una inhabilitación por actos similares;
- no haber sido objeto de quiebra personal ni de ninguna de las medidas de prohibición o decomiso del Libro VI del Código de Comercio.

El registro en la lista ACC está abierto al profesional que justifique:

- o poseer el Certificado de Capacidad para Actuar como Auditor (CAFCAC);
- o poseer el Diploma de Contabilidad (DEC);
- ya sea haber superado una prueba de aptitud para el nacional aprobada por las autoridades de su Estado de la UE o del EEE para ejercer el control jurídico de las cuentas o, aunque no se haya aprobado, cumplir las condiciones de título, diploma y formación práctica para obtener dicha acreditación de conformidad con las disposiciones de la Directiva 2006/43/CE;
- o han aprobado una prueba de aptitud para el nacional de un Estado no perteneciente a la UE que admita a los nacionales franceses para ejercer el control legal de las cuentas, poseer un diploma o título del mismo nivel que la CAFCAC o el DEC y justificar una experiencia profesionales de tres años en el campo de la auditoría legal de cuentas,
- han realizado una pasantía profesional de un período establecido por el Reglamento con un auditor o una persona aprobada por un Estado miembro de la Unión Europea para ejercer el control jurídico de las cuentas.

Para asistir a la CAFCAC, debe ser el titular:

- un máster y han superado las pruebas preparatorias de certificado para funciones cacicos cuyo programa, como el del CAFCAC, se especifica en un[decreto del 5 de marzo de 2013](https://www.legifrance.gouv.fr/affichTexte.docidTexte=JORFTEXT000027143986&categorieLien=id) ;
- o un título de posgrado en estudios contables regidos por el Decreto 81-537 de 12 de mayo de 1981 relativo al grado de posgrado en contabilidad o el grado de posgrado en contabilidad y finanzas, o haber validado al menos cuatro de las siete pruebas para obtener un título de posgrado en contabilidad y gestión (DSCG);
- tienen un título considerado equivalente al título de posgrado en contabilidad o al título de posgrado en contabilidad y estudios financieros.

Además, es imperativo que justifique la realización de un certificado de realización de una pasantía profesional, la duración de este último debe ser de tres años. Podrá concederse una exención total o parcial por decisión de la Guardia del Sello, Ministro de Justicia, si el candidato justifica al menos quince años de experiencia en el ámbito financiero, contable y jurídico (véanse los artículos R.822-3 y siguientes de la Código de Comercio para el contenido de esta pasantía y la exención).

Para asistir al DEC, el interesado deberá justificar:

- Poseer el DSCG o Diploma de Contabilidad de Posgrado (DECS);
- han realizado una pasantía que en principio es de tres años, en una empresa de contabilidad o en estructuras similares.

*Para ir más allá* Artículos 63 y siguientes del Decreto No 2012-432 de 30 de marzo de 2012.

El CAFCAC incluye una prueba de elegibilidad y una prueba de admisión, cuyos términos y condiciones están[Artículo A. 822-1](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=58CC1389C7F69724803027999EB5D10B.tplgfr33s_1idArticle=LEGIARTI000027146372&cidTexte=LEGITEXT000005634379&dateTexte=20171207) Código de Comercio.

*Para ir más allá* Artículos L. 822-1, L. 822-1-1, R. 822-2 y el siguiente del Código de Comercio; decreto de 5 de marzo de 2013 por el que se establece el programa del certificado de idoneidad para las funciones de auditor y el certificado preparatorio de las funciones de auditor.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

**En caso de entrega gratuita de servicios (LPS)**

No existe ningún reglamento para un nacional de un Estado miembro de la Unión Europea (UE) o parte del Espacio Económico Europeo (EEE) que desee ejercer como CAC en Francia de forma temporal y casual.

Por lo tanto, sólo se aplicarán las medidas adoptadas para el libre establecimiento de nacionales de la UE o del EEE.

**En caso de Establecimiento Libre (LE)**

Un nacional de un Estado de la UE o del EEE que opere legalmente como CAC en ese Estado podrá establecerse en Francia para llevar a cabo la misma actividad de forma permanente.

Tendrá que solicitar la inscripción en la LISTA del CAC ante el Consejo Superior de la Oficina del Comisionado de Auditores, a la espera de haber superado previamente una prueba de aptitud.

*Para ir más allá* Artículos L. 822-1-2 y R. 822-6 del Código de Comercio.

### c. Condiciones de honorabilidad e incompatibilidad

Se debe seguir un conjunto de reglas éticas, que incluyen:

- El secreto profesional de los datos que procesa;
- Integridad, incluida la justificación de no ser el autor de actos incriminatorios o disciplinarios;
- independencia e imparcialidad, a fin de evitar cualquier conflicto de intereses entre su misión de supervisión y asesoramiento y cualquier situación de autorrevisión;
- incompatibilidad de ejercicios con:- una actividad o acto que infrinja su independencia,
  - el trabajo de un empleado. Sin embargo, un auditor puede proporcionar instrucción relacionada con el ejercicio de su profesión o tener un trabajo remunerado con un auditor o un contador
  - una actividad comercial, con la excepción, por una parte, de actividades comerciales accesorias a la profesión de contable y, por otra, a actividades comerciales accesorias realizadas por la sociedad multiprofesional

**Tenga en cuenta que**

Podrán aplicarse sanciones penales contra el CAC, que funcionaría sin estar en la lista del Consejo Superior del Auditor o que no cumpliría las normas de incompatibilidad mencionadas anteriormente. En ambos casos, se enfrenta a una pena de un año de prisión y una multa de 15.000 euros.

También se prevén sanciones penales en las situaciones establecidas en los artículos L.820-6 y L. 820-7 del Código de Comercio, especialmente cuando el profesional se abstiene de revelar los hechos delictivos que conoce de vez en cuando.

*Para ir más allá* Artículos L. 820-5, L. 820-7, L. 822-10 y siguientes, Artículos R. 822-20 y posteriores, y Apéndice 8-1 del Código de Comercio.

### d. Algunas peculiaridades de la regulación de la actividad

**Obligación de contrato de seguro de responsabilidad civil** Profesional**

El CAC Liberal tiene la obligación de comprar un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

En cualquier caso, el ACC debe proporcionar una garantía financiera de más de 76.224,51 euros al año por reclamación.

*Para ir más allá* Artículos L. 822-17, R. 822-36 y A. 822-31 del Código de Comercio.

**Obligación de someterse a formación profesional continua**

Con el fin de mantener y mejorar sus conocimientos en las áreas de contabilidad, finanzas y derecho, los CAC deben someterse a un curso de formación profesional continua de ciento veinte horas durante tres años consecutivos. Se completan al menos 20 horas en un solo año (ver A. 822-28-2).

*Para ir más allá* Artículos L. 822-4, R. 822-22, A. 822-28-1 y el siguiente Código de Comercio.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte la "Formalidad de la presentación de informes de una empresa comercial", "Registro de negocios individuales en el Registro de Comercio y Empresas" y "Formalidades de Informes Corporativos. trabajo artesanal."

### b. Pedir inclusión en la lista del CAC

**Aprobar una prueba de preidonebilidad para la UE o el EEE u otro nacional de Estado extranjero cuando éste admita a los nacionales franceses para ejercer el control legal de las cuentas**

En este caso, el nacional tendrá que solicitarlo mediante expediente remitido al Ministro responsable de justicia. El archivo debe incluir los siguientes documentos justificativos traducidos al francés por un traductor certificado:

- Una identificación válida
- cualquier diploma, certificado o título que acredite las cualificaciones profesionales del nacional;
- cualquier certificado de pasantía o formación para evaluar el contenido y el nivel de educación postsecundaria.

Una vez recibido el expediente, el Ministro de Justicia emitirá un recibo al nacional y tendrá cuatro meses para decidir sobre la solicitud de la prueba de aptitud. El silencio guardado dentro de este tiempo valdrá la pena aceptarlo.

Una vez que se le permite realizar el examen, el nacional debe aprobar un examen escrito y oral en francés, con una puntuación media de 10 o más.

**Registro obligatorio en la lista de CAC**

El interesado (francés o nacional) que desee ejercer en la profesión de CAC debe solicitar el registro en una lista mantenida por el Consejo Superior de la Oficina del Comisionado de Auditores.

**Autoridad competente**

La solicitud de inclusión definitiva en la lista del CAC se presenta con el Consejo Superior de la Oficina del Comisionado de Auditores.

**Documentos de apoyo**

La solicitud se puede hacer por correo postal o directamente en línea en el[Sitio](https://www.cncc.fr/liste-cac.html) Compañía Nacional de Auditores. Además de su solicitud motivada, el profesional deberá presentar toda la siguiente información justificada:

- Una identificación válida
- Un CV
- una copia de uno de los diplomas mencionados en el párrafo "2." a. Cualificaciones profesionales";
- Certificado de realización de prácticas;
- En caso afirmativo, un certificado de éxito en la prueba de cualificación del nacional;
- un certificado de incompatibilidad con el ejercicio de la profesión;
- un certificado de honor que justifique que no está sujeto a quiebra personal o al autor de hechos que han dado lugar a una condena penal.

**Resultado del procedimiento**

Una vez recibidas las monedas, el Consejo Superior emitirá un recibo al interesado. Si, en el plazo de cuatro meses a partir de la recepción, la autoridad competente no ha decidido sobre la solicitud, se considerará que se concede el registro.

De este modo, podrá ejercer legalmente la función del CAC después de haber sido juramentado ante el primer presidente del Tribunal de Apelación territorialmente competente.

*Para ir más allá* Artículos L. 822-1, L. 822-1-1, L.822-1-2, R.822-9, R. 822-12 del Código de Comercio.

**Si es necesario, la inclusión de la empresa en la lista del Consejo Superior**

Siempre que los estatutos de la sociedad CAC se hayan presentado ante el registro del Tribunal Mercantil territorialmente competente, su representante legal deberá solicitar que la empresa se incluya en la lista de ACCs en poder del Consejo Superior.

En este caso, el archivo de solicitud debe incluir todos los siguientes documentos justificativos:

- Una copia de los estatutos
- Una solicitud de cada socio pide el registro;
- Una lista de accionistas o socios, incluida su información de registro civil, el número de derechos de voto en la empresa y la prueba de su solicitud de inclusión en la LISTA de Las CSC;
- un certificado de presentación de los estatutos de la empresa al registro del tribunal comercial competente.

El registro de la empresa CAC se realizará en el mismo procedimiento y plazos que los aplicables al CAC que ejerce como individuo.

