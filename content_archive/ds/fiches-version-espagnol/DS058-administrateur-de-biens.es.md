﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS058" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Administrador de propiedades" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="administrador-de-propiedades" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/administrador-de-propiedades.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="administrador-de-propiedades" -->
<!-- var(translation)="Auto" -->

Administrador de propiedades
============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El administrador de la propiedad es un profesional obligatorio cuya actividad consiste en la gestión de bienes raíces en nombre de un individuo, un sindicato de copropietarios o una empresa de bienes raíces civil a diario.

Como tal, puede ser responsable de la gestión diaria de los activos (pago de impuestos, facturas, mantenimiento), la resolución de diversas disputas que les conciernen, o su gestión de alquiler (encontrar inquilinos y gestionar el alquiler).

*Para ir más allá* Artículo 1 de la Ley 70-9, de 2 de enero de 1970, por el que se regulan las condiciones para la realización de actividades relativas a determinadas operaciones relacionadas con fondos inmobiliarios y comerciales.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad.

- Para una actividad comercial, la CFE correspondiente será la Cámara de Comercio e Industria (CCI);
- Si se configura una empresa individual, es el Urssaf;
- para las sociedades civiles, la CFE competente es el registro del Tribunal de Comercio o el registro del tribunal de distrito en los departamentos del Bajo Rin, el Alto Rin y el Mosela.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal, independientemente del número de empleados de la empresa con la condición de que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para ser administrador de la propiedad, el profesional debe tener una tarjeta de visita y debe:

- Estar calificado profesionalmente
- justificar una garantía financiera
- han contratado un seguro de responsabilidad civil profesional.

Para ser reconocida como una persona profesionalmente calificada, la persona debe:

- o sostenga uno de los siguientes grados:- un título a nivel De Bac-3 que castigue los estudios jurídicos, económicos o comerciales,
  - un diploma o título en el directorio nacional de certificaciones profesionales ([RNCP](http://www.rncp.cncp.gouv.fr/grand-public/recherche)),
  - un Certificado de Técnico Superior (BTS) marcado como "Profesiones Inmobiliarias"
  - diplomado del Instituto de Estudios Económicos y Jurídicos aplicado a la construcción y la vivienda (ICH);
- o justificar al menos diez años de experiencia profesional (cuatro años para ejecutivos) en la actividad pertinente.

Una vez que cumplan con estas condiciones, pueden solicitar una tarjeta de visita (ver infra "3 grados). a. Solicitud de una tarjeta de visita").

*Para ir más allá* Artículo 3 de la citada Ley de 2 de enero de 1970; Artículos 12-15 del Decreto 72-678, de 20 de julio de 1972, por los que se establecen las condiciones de aplicación de la Ley No 70-9, de 2 de enero de 1970, por la que se regulan las condiciones de las actividades relativas a determinadas operaciones relativas a edificios y fondos comerciales.

**Formación continua**

La formación continua de 14 horas al año o 42 horas durante tres años consecutivos de práctica es obligatoria para cualquier profesional inmobiliario.

**Tenga en cuenta que**

La renovación de su tarjeta de visita está condicionada al cumplimiento de este requisito.

*Para ir más allá* Artículos 1 y siguientes del Decreto No 2016-173 de 18 de febrero de 2016 sobre la educación continua de los profesionales inmobiliarios.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

**Para ejercicios temporales e informales (LPS)**

Todo nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE), que actúe como administrador de la propiedad, podrá llevar a cabo la misma actividad en Francia de forma temporal e informal.

Para ello, el interesado debe hacer una declaración previa antes de su primera actuación ante el Presidente de la CPI Territorial, o ante la cámara departamental de Ile-de-France (véase infra "3o. b. Predeclaración para el nacional de la UE para un ejercicio temporal y ocasional").

Además, cuando ni el acceso a la actividad ni su ejercicio estén regulados en ese Estado miembro, el nacional deberá justificar haber llevado a cabo esta actividad durante al menos un año en los diez años anteriores a su primer beneficio.

*Para ir más allá* Artículo 8-1 de la Ley de 2 de enero de 1970 y artículos 16-6 y 16-7 del Decreto de 20 de julio de 1972.

**Para un ejercicio permanente (LE)**

Todo nacional de la UE legalmente establecido que actúe como administrador de la propiedad podrá llevar a cabo la misma actividad en Francia de forma permanente. Para ello, el profesional debe:

- Poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente del Estado miembro que regula la actividad;
- Cuando la actividad no esté regulada por el Estado miembro, justifique haber sido administrador de la propiedad durante al menos un año en los últimos diez años;
- estar en posesión de las habilidades linguísticas necesarias para llevar a cabo su actividad en Francia.

Una vez que el profesional cumple con estos requisitos, él o ella puede solicitar una tarjeta de visita (ver infra "3o. a. Solicitud de una tarjeta de visita").

*Para ir más allá* Artículo 16-1 del Decreto de 20 de julio de 1972.

### c. Condiciones de honorabilidad

El administrador inmobiliario, como profesional de bienes raíces, debe llevar a cabo su actividad con toda probidad y moralidad.

Además, no debe haber sido objeto de:

- una condena por menos de diez años por un delito;
- una pena de al menos tres meses de prisión sin una sentencia condicional, incluyendo:- Estafa
  - violación de la confianza,
  - Recibir
  - Lavado de dinero
  - corrupción activa o pasiva, tráfico de influencias, resta o malversación de propiedad,
  - falsificación de valores o valores fiduciarios, falsificación de las marcas de la autoridad,
  - participación en una asociación de delincuentes,
  - Narcotráfico
  - proxenetismo o prostitución,
  - bancarrota, práctica de sharking de préstamos, evasión de impuestos, búsqueda bancaria.

**Tenga en cuenta que**

El profesional que es objeto de tal incapacidad durante el transcurso de su actividad, incurre en una pena de cinco años de prisión y una multa de 375.000 euros por el delito de fraude y debe dejar de ejercer en el plazo de un mes a partir de la decisión La final.

*Para ir más allá* Artículo 9 y siguientes de la Ley de 2 de enero de 1970.

### d. Algunas peculiaridades de la regulación de la actividad

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales de las normas de seguridad contra incendios y pánico en las instituciones públicas.

Es aconsejable consultar el listado " [Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

**Obligación de justificar una garantía financiera**

El administrador de la propiedad, que posee o está solicitando la tarjeta de visita, debe solicitar una garantía financiera de al menos la cantidad máxima de fondos que planea tener. Esta garantía está destinada a cubrir los créditos que han sido diseñados durante su actividad. Además, el titular de la tarjeta solo podrá recibir pagos o reembolsos dentro del importe de dicha garantía.

Una vez que haya contratado esta garantía, el fondo de depósito y depósito le emite un certificado de garantía.

*Para ir más allá* Artículo 27 y siguientes del decreto de 20 de julio de 1972.

**Contrate un seguro de responsabilidad civil profesional**

El administrador inmobiliario, como profesional de bienes raíces, debe obtener un seguro para cubrir los riesgos financieros en el curso de su actividad.

Esta póliza de seguro debe incluir su información de contacto, así como la de la agencia de seguros. El límite de la garantía no puede ser inferior a 76.224,51 euros al año para el mismo asegurado.

*Para ir más allá* Artículo 49 del Decreto de 20 de julio de 1972, decreto de 1 de julio de 2005 por el que se establecen las condiciones de seguro y la forma del documento justificante previsto en el Decreto de 20 de julio de 1972.

**Obligación de estar en posesión de una orden judicial**

El administrador de la propiedad que se dedica a la administración de propiedades o actividades de fideicomisario de condominios está obligado a:

- estar en posesión de una orden escrita firmada, mencionando el alcance de sus facultades para recibir bienes, dinero o valores en la administración de los activos a su cargo;
- mantener un registro de las órdenes recibidas Modelo 15 de septiembre de 1972.

**Tenga en cuenta que**

El profesional que realiza una de estas actividades podrá recibir sumas distintas de las resultantes de la administración de los activos de los que es responsable, pero sólo con carácter temporal a partir de entonces:

- ha estado gestionando la propiedad, que ha sido objeto del contrato durante más de tres años;
- que estas sumas se incluyan en el importe de la garantía financiera;
- que se le ha dado un mandato especial para ocupar este cargo;
- que los riesgos incurridos durante esta actividad están cubiertos por seguros específicos para actividades de administración de propiedades o fideicomisarios de condominios o por seguros especiales o complementarios contratados con una compañía de seguros.

*Para ir más allá* Artículo 6 de la Ley de 2 de enero de 1970; Artículos 64 a 71 del Decreto de 29 de julio de 1972.

**Requisito de contabilización**

El profesional que ostente la tarjeta profesional deberá colocar un cartel en todas las instalaciones en las que realice su actividad indicando:

- El número de su tarjeta
- El importe de su garantía suscrita
- si es necesario, el nombre y la dirección del gerente.

Además, todos los documentos expedidos deben incluir la información:

- tarjeta de visita (número y lugar de emisión);
- La naturaleza de su negocio
- El nombre y el nombre de su empresa
- Si es así, el nombre y la dirección del garante
- si es así, la declaración de que no debe recibir o retener fondos distintos de los destinados al ejercicio de su actividad.

*Para ir más allá* Artículos 93 a 95 del Decreto de 20 de julio de 1972.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitud de tarjeta de visita

**Autoridad competente**

El profesional debe solicitar al Presidente de la CPI Territorial o a la Cámara Departamental de Ile-de-France en cuyo ámbito de competencia sea su sede si el solicitante es una persona jurídica o su institución principal si el solicitante es una persona física.

**Documentos de apoyo**

Su petición debe mencionar:

- La naturaleza de las transacciones propuestas;
- si el solicitante es una persona física:- su estado civil,
  - su profesión,
  - Su domicilio y dirección profesional
- si el solicitante es una persona jurídica:- su nombre, su forma jurídica y la finalidad de su actividad,
  - su sede central,
  - información sobre sus representantes legales o legales (estado civil, ocupación, residencia).

Además, su solicitud debe ir acompañada de los siguientes documentos:

- El formulario Cerfa 15312*01 completado y firmado;
- Prueba de la aptitud profesional del solicitante
- Un certificado de garantía financiera
- un certificado de seguro de responsabilidad civil profesional;
- un extracto del Registro mercantil (RCS) de menos de un mes de edad si la persona está registrada, o el doble de la demanda;
- dependiendo del caso:- certificado con el número de cuenta, emitido por la entidad de crédito que abrió la cuenta,
  - o un certificado de apertura en nombre de cada principal de cuentas bancarias;
- en su caso, la declaración de honor de que el solicitante no posee ningún otro fondo, directa o indirectamente que los que representan su remuneración;
- boletín 2 del antecedente penal nacional o equivalente para los nacionales de otro Estado miembro de la UE.

**Costo**

La solicitud debe ir acompañada de la tasa de solicitud de un importe de[120 euros](https://www.entreprises.cci-paris-idf.fr/web/formalites/demande-carte-professionnelle-immobilier).

**hora**

Si el expediente está incompleto, la autoridad competente envía al profesional una lista de los documentos que faltan dentro de una quincena de horas de su recepción. Si su expediente no se completa en un plazo de dos meses, su solicitud de tarjeta será rechazada.

*Para ir más allá* Artículos 2 y 3 del Decreto de 20 de julio de 1972 supra.

### b. Predeclaración del nacional de la UE para el ejercicio temporal y casual (LPS)

**Autoridad competente**

El nacional debe enviar su solicitud, por carta recomendada con notificación de recepción, o electrónicamente, al presidente de la CPI territorial o a la cámara departamental de Ile-de-France en la jurisdicción de la que desea prestar su servicio Servicios.

**Documentos de apoyo**

Su solicitud debe incluir:

- un certificado que certifique que está legalmente establecido en un Estado miembro de la UE o del EEE y que no está prohibido ejercer la actividad de administrador de la propiedad;
- un documento que justifique que ha participado en esta actividad durante al menos un año en los últimos diez años si el Estado miembro en el que está establecida no regula esta actividad;
- prueba de nacionalidad
- un documento que acredite que tiene la garantía financiera necesaria para ejercer (véase más arriba "2". d. Obligación de justificar una garantía financiera");
- Una prueba de que tiene un seguro de responsabilidad civil profesional;
- una declaración de honor que no posee, ni ha recibido directa o indirectamente otros fondos, efectos o valores distintos de los incluidos en su remuneración o honorarios.

*Para ir más allá* Artículo 16-6 del Decreto de 20 de julio de 1972.

### c. Formalidades de notificación de la empresa

El profesional está obligado a registrarse en el Registro Mercantil (RCS) ante la CPI en caso de creación de una empresa mercantil o a hacer una declaración de actividad ante el Urssaf en caso de creación de una empresa individual.

Es aconsejable consultar el "Registro de una empresa comercial individual en el RCS" para obtener más información.

### d. Requisito para declarar una escuela secundaria

Si se abre una sucursal o secundaria, el profesional debe declararlos primero.

**Autoridad competente**

El profesional debe solicitar a la CPI la ubicación de la escuela secundaria.

**Documentos de apoyo**

Su solicitud debe contener el formulario Cerfa 15312*01 idénticas a las requeridas para la emisión de la tarjeta de visita y los documentos justificativos necesarios para este procedimiento.

**Retrasos y procedimientos**

La CPI emite un recibo de devolución tan pronto como se recibe la solicitud.

*Para ir más allá* Artículo 3 de la Ley de 2 de enero de 1970; Artículo 8 del decreto de 20 de julio de 1972

