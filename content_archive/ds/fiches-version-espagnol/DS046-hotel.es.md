﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS046" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Hotel" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="hotel" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/hotel.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="hotel" -->
<!-- var(translation)="Auto" -->


Hotel
=====

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

Un hotel es un alojamiento turístico mercante. Es un establecimiento comercial que ofrece alojamiento a corto plazo (día, semana o mes) a una clientela visitante que no elige vivir allí. El profesional que opera un hotel es responsable del alquiler de las habitaciones o apartamentos amueblados de su establecimiento, y la emisión de todos los servicios puestos a disposición de su clientela. El alquiler es para un paquete e incluye el mantenimiento diario de la habitación y la cama, así como el suministro de toallas. La propiedad también puede ofrecer otros servicios (lavandería, catering, piscina, bienestar, seminarios, etc.). Cada una de estas actividades complementarias está sujeta a su propia normativa.

**Tenga en cuenta que**

La actividad del hotel se puede llevar a cabo durante todo el año o solo de forma estacional.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Dado que el negocio es de carácter comercial, la CFE es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

Si el operador elige la condición de contratista de responsabilidad limitada (EIRL), hace una declaración de los activos profesionales asignados a esta actividad con la CFE que remitirá su expediente al contenido del Registro de Comercio y Sociedades (RCS). Si opera en forma de sociedad mercantil, debe estar registrada en el RCS después de registrar sus estatutos en el Servicio de Impuesto de Sociedades (SIE) de la sede social de la sociedad.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere ningún requisito de cualificación para el profesional que desee operar un hotel.

Sin embargo, dependiendo del caso, el profesional tendrá que hacer una declaración en caso de preparación o manipulación de productos animales o animales (véase infra "3o. d. Declaración en caso de preparación o venta de productos animales o animales").

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo sobre el Espacio Económico Europeo, con miras a ejercer en Francia la actividad de hotelero, de forma temporal e informal (LPS) o (LE).

Como tal, el profesional está sujeto a los mismos requisitos que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

### c. Responsabilidad

El operador de un hotel puede ser responsable de robo o daños cometidos dentro de su establecimiento. Esta responsabilidad incluye robos y daños causados por todo el personal y terceros que entran y vienen dentro del hotel.

*Para ir más allá* Artículo L. 311-9 del Código de Turismo y artículos 1952 a 1954 del Código Civil.

### d. Algunas peculiaridades de la regulación de la actividad

**Provisiones de flujo de bebidas**

En caso de venta de bebidas alcohólicas para ser consumidas en el establecimiento, el profesional está obligado a solicitar una licencia de licor.

Las bebidas se dividen en cuatro grupos en función de su contenido alcohólico. La licencia emitida varía en función del grupo al que pertenezcan.

Es aconsejable referirse a la lista[Beber](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/), para obtener más información.

Además, si las bebidas alcohólicas se venden solo a la hora de comer, el profesional debe solicitar:

- una "licencia de restaurante pequeño" para ofrecer en el lugar y durante las comidas, bebidas de tercer grupo (vino, cerveza, sidra, pera, aguamiel, vinos dulces, crema y jugo de fruta entre 1,2 y 3 grados de alcohol y licores que dibujan 18 grados alcohol puro máximo);
- una "licencia de restaurante" para vender todas las bebidas permitidas para el consumo in situ de comidas.

En todos los casos, el profesional está obligado a recibir una formación específica sobre los derechos y obligaciones relativos a la venta de bebidas alcohólicas adaptadas a la naturaleza de su actividad.

Para obtener más información, consulte el listado[Catering tradicional](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/restaurant-traditionnel/).

*Para ir más allá* Artículos L. 3321-1 y L. 3331-2 del Código de Salud Pública; Artículo L. 313-1 del Código de Turismo.

**Cumplimiento de las normas de seguridad contra incendios y accesibilidad**

Como un hotel es un establecimiento de acceso público (ERP), el profesional está obligado a garantizar el cumplimiento de las normas de seguridad contra incendios, así como las normas de accesibilidad aplicables a la categoría de establecimientos de tipo O de acuerdo con la nomenclatura del Código de construcción y vivienda (artículo R. 123-48). Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden modificada de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en los ERP. Es aconsejable referirse a la lista[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) y el sitio web de la Delegación Ministerial de Accesibilidad para obtener más información.

**Transmisión musical**

El profesional que desee transmitir música dentro de su hotel debe solicitar en línea la autorización previa a la Sociedad de Compositores y Editores de Música (Sacem) y pagar el monto de la regalía. El profesional debe registrarse y cumpliser el formulario de autorización en línea en el[Sacem](https://clients.sacem.fr/autorisations), que también es responsable de recaudar una compensación equitativa por la[Spre](https://www.spre.fr/).

**Impuesto de residencia**

El impuesto de residencia o el impuesto de residencia a tipo fijo pueden ser instituidos por un municipio o una agrupación de municipios. Dependiendo del método de recaudación, la operación paga el impuesto real a la autoridad local de los clientes o directamente el importe del impuesto a tipo fijo.

*Para ir más allá* Artículos L. 2333-26, R. 2333-43 y R. 2333-44 del Código General de Gobierno Local.

**Contribución a la radiodifusión pública**

El profesional está obligado a pagar una cuota siempre y cuando pone a disposición de sus clientes, un receptor de televisión. Esta tarifa varía dependiendo del número de dispositivos que tiene (como indicación, el precio es de 138 euros en la metrópolis y 89 euros para los departamentos de ultramar).

**Tenga en cuenta que**

Se aplica una deducción desde tres dispositivos. Además, si la actividad anual del hotel no supera los nueve meses, el profesional puede beneficiarse de una reducción del 25% en la contribución.

*Para ir más allá* Artículo 1605 y siguiente del Código Tributario General.

### Publicidad de precios

El profesional debe mostrarde forma clara y legible, fuera del hotel y en el lugar de recepción de su clientela:

- el precio de todos los impuestos y servicios incluidos en euros para la noche siguiente en una habitación doble o el precio máximo cobrado por una noche en una habitación doble para un período determinado incluyendo la noche siguiente. Sin embargo, si el establecimiento no ofrece estos servicios, el profesional muestra el precio correspondiente al servicio de alojamiento más practicado, así como su duración;
- si usted puede o no acceder a Internet desde las habitaciones, si proporcionar o no el desayuno, y si es así, si estos servicios están incluidos en el precio del servicio o no;
- cómo acceder a la información sobre los precios y servicios ofrecidos por el establecimiento.

Además de la información anterior, el profesional indica en la recepción del cliente las posibles horas de llegada y salida y, en su caso, los suplementos en caso de salida tardía.

**Requisito para mostrar en cada habitación**

En cada habitación de su hotel, el profesional pone a disposición toda la información relativa al precio de los servicios prestados incidentalmente a las noches o estancias, así como las condiciones de consulta de esta información.

*Para ir más allá* : decreto de 18 de diciembre de 2015 relativo a la publicidad de los precios de los alojamientos turísticos no turísticos distintos de los alojamientos turísticos y hoteles al aire libre.

**Facturación**

El profesional emite una nota al cliente, en caso de un beneficio de una cantidad superior o igual a 25 euros. Esta nota debe incluir la siguiente información:

- Su fecha de escritura;
- El nombre y la dirección del proveedor
- El nombre del cliente (si el cliente no se opone a él);
- La fecha y el lugar de ejecución del servicio
- los detalles de cada beneficio, así como el importe total a pagar (todos los impuestos incluidos y excluidos los impuestos).

El profesional mantiene el doble de este grado, los clasifica cronológicamente y los mantiene durante dos años.

*Para ir más allá* ( Orden 83-50/A, de 3 de octubre de 1983, relativa a la publicidad de precios de todos los servicios.

**Hoja de fuentes individuales**

Tan pronto como el profesional da la bienvenida a los turistas de nacionalidad extranjera a su establecimiento, tiene el turista llenar y firmar una tarjeta de policía individual cuyo modelo se establece en anexo al decreto de 1 de octubre de 2015 tomado en aplicación Artículo R. 611-42 del Código de Entrada y Residencia de Extranjeros y del Derecho de Asilo.

Esta hoja informativa para evitar disturbios en el orden público y para las investigaciones e investigaciones forenses debe mencionar:

- La identidad del extranjero;
- fecha, lugar de nacimiento y nacionalidad;
- La dirección de su casa habitual
- Su número de teléfono y dirección de correo electrónico
- La fecha de su llegada al establecimiento y la fecha prevista de su partida;
- Como opción, los niños menores de quince años pueden aparecer en la tarjeta de un adulto que los acompaña.

Este registro policial se mantiene durante seis meses y se entrega, en caso de solicitud, a la policía y a las unidades de la gendarmería.

*Para ir más allá* Artículo R. 611-42 del Código de Entrada y Residencia de Extranjeros y del Derecho de Asilo.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

**Autoridad competente**

El profesional que opera un hotel mantiene informes de su negocio a la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

El interesado proporciona la[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI envía un recibo al profesional el mismo día mencionando los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si la CFE se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### b. Solicitud de clasificación en hoteles turísticos

Un operador podrá, independientemente del número de habitaciones, solicitar la clasificación oficial en un hotel turístico para mejorar su oferta.

**Autoridad competente**

El ranking se basa en criterios correspondientes a un número creciente de estrellas (1 a 5). Está fijado por una tabla de clasificación aprobada por el Ministro responsable del turismo.

**Tiempo y procedimiento**

La decisión individual sobre la clasificación es tomada, sobre la delegación del Estado, por la agencia de desarrollo turístico[Trump Francia](http://www.atout-france.fr/), en el plazo de un mes a partir de la recepción del archivo completo. La falta de respuesta dentro de este período de tiempo vale la pena la aceptación. El ranking es válido por cinco años.

Para obtenerlo, el operador se pone en contacto primero con una agencia de evaluación de cumplimiento acreditada por el[Comité francés de acreditación](https://www.cofrac.fr/) (Cofrac). El evaluador expide un certificado de visita dentro de los quince días de la visita. Sólo es válido si se llevó a cabo en los tres meses anteriores a la transmisión del expediente completo a Atout France.

La decisión de clasificación podrá modificarse o derogarse si, después de un procedimiento contradictorio, el operador no ha corregido las discrepancias de cumplimiento encontradas como resultado de la reclamación de un cliente (artículo D. 311-10 del Código de Turismo).

**Tenga en cuenta que**

El profesional cuyo establecimiento esté clasificado está obligado a fijar en la fachada un cartel de conformidad con lo dispuesto en el decreto de 19 de febrero de 2010 relativo al signo de los hoteles turísticos.

*Para ir más allá* Artículo L. 311-6, y artículos D. 311-4 y el siguiente del Código de Turismo; 23 de diciembre de 2009 se establecen las normas y el procedimiento de clasificación de los hoteles turísticos.

### c. Solicitud de premio "Palacio"

Este dispositivo está destinado a distinguir el hotel turístico de 5 estrellas "con características excepcionales", incluyendo el interés histórico, estético o patrimonial, y cumplir con condiciones especiales (incluyendo la elegibilidad superficies y servicios de dormitorios).

**Procedimiento**

El laudo se lleva a cabo en dos etapas: una primera etapa de revisión del cumplimiento del expediente con las condiciones de elegibilidad por atout France, y una segunda etapa de análisis del expediente por una comisión de atribución que emite un aviso. Esta distinción, válida por cinco años, es otorgada por el Ministro responsable del turismo.

**Tenga en cuenta que**

La falta de respuesta en un plazo de cuatro meses es una negativa a otorgar el premio.

*Para ir más allá* : orden de 3 de octubre de 2014 relativa a la "Distinción del Palacio".

### d. Declaración en caso de preparación o venta de productos animales o animales

Tan pronto como el hotel prepara, procesa, procesa, maneja o almacena alimentos, su propietario está obligado a hacer una declaración al prefecto del lugar donde se encuentra su establecimiento.

Para ello, el profesional tendrá que[Formulario Cerfa 13984*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13984.do) consumido y firmado a la Dirección Departamental de Protección de la Población (DDPP) del departamento donde se encuentra su establecimiento.

*Para ir más allá* Artículos L. 233-2, R. 233-4 del Código Rural y Pesca Marina.

### e. Si es necesario, registre los estatutos de la empresa

Una vez fechados y firmados los estatutos de la empresa, el profesional que opera un hotel deberá registrarlos en la[Departamento del Impuesto de Sociedades](http://www2.impots.gouv.fr/sie/ifu.htm) (SIE) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

