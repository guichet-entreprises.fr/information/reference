﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS031" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Controlador de ascensor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="controlador-de-ascensor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/controlador-de-ascensor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="controlador-de-ascensor" -->
<!-- var(translation)="Auto" -->

Controlador de ascensor
=======================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

El controlador del ascensor es un profesional cuya misión es comprobar periódicamente el correcto funcionamiento de los ascensores y la seguridad de las personas que los utilizan.

La misión del profesional es:

- Verificar el cumplimiento por parte de los ascensores de las obligaciones de seguridad
- identificar defectos que representen un peligro para la seguridad de las personas y/o que afecten al correcto funcionamiento del dispositivo.

*Para ir más allá* : Sección L. 125-2-3 del Código de Construcción y Vivienda ; Artículo R. 125-2-4 del Código de Construcción y Vivienda ; 7 de agosto de 2012 orden de los controles técnicos que deben realizarse en las instalaciones de los ascensores.

### b. Centro competente de formalidades comerciales (CFE)

La CFE correspondiente depende de la naturaleza de la actividad:

- para una actividad liberal, la CFE competente es el Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, es el registro del Tribunal de Comercio, o el registro del tribunal de distrito para los departamentos del Bajo Rin, el Alto Rin y el Mosela.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados, siempre que no utilice un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El profesional que desee realizar la actividad del controlador del ascensor tiene 3 posibilidades:

- Caso 1: Ser un[controlador técnico de la construcción](https://www.guichet-qualifications.fr/fr/professions-reglementees/controleur-technique-de-la-construction/) con una acreditación que le permite intervenir en ascensores;
- Caso 2: ser un organismo autorizado en uno de los Estados miembros de la Unión Europea o en uno de los demás Estados partes en el Acuerdo sobre el Espacio Económico Europeo, encargado de evaluar el cumplimiento de los ascensores sujetos al marcado CE (en este caso no se requieren cualificaciones profesionales para llevar a cabo la actividad);
- Caso 3: ser certificado por un organismo acreditado por el Comité francés de Acreditación o por una organización que sea signatario del acuerdo multilateral europeo tomado como parte de la coordinación europea de los organismos de acreditación (en este caso no hay Cualificaciones profesionales requeridas para llevar a cabo la actividad);

*Para ir más allá* : Artículos R. 125-2-5 del Código de Construcción y Vivienda.

### b. Cualificaciones profesionales - Nacionales europeos (LPS o LE)

#### Para entrega gratuita de servicios (LPS)

El profesional que sea miembro de la Unión Europea (UE) o parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) podrá trabajar en Francia, de forma temporal y ocasional, como controlador de ascensores, siempre que establecido en ese estado para llevar a cabo la misma actividad.

El interesado tendrá que llevar a cabo diferentes procedimientos de acuerdo con las diversas posibilidades descritas en 2 grados. tiene.:

- Caso 1: Aplicar el procedimiento descrito en la hoja[Controlador técnico de la construcción](https://www.guichet-qualifications.fr/fr/professions-reglementees/controleur-technique-de-la-construction/)
- Casos 2 y 3, no se requieren cualificaciones profesionales.

#### Para un establecimiento gratuito (LE)

El nacional de un Estado miembro de la UE o del EEE que desee llevar a cabo la actividad de controlador de ascensores en Francia de forma permanente debe aplicar procedimientos diferentes en función de las diversas posibilidades descritas en 2 grados. tiene.:

- Caso 1: Consulte el enchufe[Controlador técnico de la construcción](https://www.guichet-qualifications.fr/fr/professions-reglementees/controleur-technique-de-la-construction/) ;
- Casos 2 y 3, no se requieren cualificaciones profesionales.

### c. Condiciones de honorabilidad

El profesional que opere la actividad del controlador del ascensor deberá actuar de acuerdo con los principios:

- imparcialidad;
- independencia con el propietario que lo llama, o con una empresa que probablemente realice trabajos en un ascensor o su mantenimiento.

### d. Algunas peculiaridades de la regulación de la actividad

**Seguro**

El controlador del ascensor es necesario, como profesional, para tomar un seguro de responsabilidad civil profesional.

**Informes elaborados por el responsable técnico**

El profesional de control del ascensor debe proporcionar al propietario:

- un documento que acredite el honor de que:- tiene las cualificaciones profesionales para llevar a cabo su actividad,
  - ha contratado un seguro de responsabilidad civil profesional,
  - opera de manera imparcial e independiente;
- un informe, en el plazo de un mes a partir de su intervención, que contenga todas las operaciones realizadas y los defectos encontrados.

Además, el profesional debe presentar un informe al Ministro responsable de la construcción si considera que el ascensor controlado no cumple con los requisitos esenciales mencionados en el apartado R. 125-2-13.

Por último, antes del 1 de marzo de cada año, el responsable del ascensor hace balance de los controles técnicos realizados durante el año natural anterior y hace esta evaluación al Ministro responsable de la vivienda.

*Para ir más allá* : Artículo R. 125-2-6 del Código de Construcción y Vivienda ; 7 de agosto de 2012 supra.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Caso 1

Ver el enchufe [Controlador técnico de la construcción](https://www.guichet-qualifications.fr/fr/professions-reglementees/controleur-technique-de-la-construction/).

Punto de contacto:[cact@developpement-durable.gouv.fr](mailto:cact@developpement-durable.gouv.fr).

*Para ir más allá* : Orden de 26 de noviembre de 2009 por la que se establecen las condiciones prácticas de acceso al ejercicio de la actividad del controlador técnico.

### Caso 2

Punto de contacto para solicitudes de notificación a Francia:[ascenseurs@developpement-durable.gouv.fr](mailto:ascenseurs@developpement-durable.gouv.fr)

*Para ir más allá* Capítulo IV titulado Notificación de los organismos de evaluación del cumplimiento de la Directiva 2014/33/UE["Directiva de ascensor"](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014L0033 la directive 2014/) ; Artículos R. 125-2-28 a R. 125-2-38 del Código de Construcción y Vivienda.

### Caso 3

Consulte el[página dedicada al control técnico de ascensores en la página web del organismo certificador llamado SGS](https://www.sgsgroup.fr/fr-fr/construction/services-related-to-machinery-and-equipment/equipment-certification-and-calibration/technical-controller-of-elevators-certification).

Punto de contacto:[fr.cdp.ascenseurs@sgs.com](mailto:fr.cdp.ascenseurs@sgs.com).

*Para ir más allá* : decretado a partir del 13 de diciembre de 2004 criterios de competencia para las personas que realizan controles técnicos en ascensores y ascensores decretado el 15 de junio de 2005 modificación del decreto antes mencionado.

### d. Formalidades de notificación de la empresa

**Autoridad competente**

El controlador del ascensor debe informar a su empresa, y para ello debe hacer una declaración ante la CPI.

**Documentos de apoyo**

El interesado debe proporcionar la documentos de apoyo dependiendo de la naturaleza de su actividad.

**hora**

El centro de formalidades comerciales del CCI envía un recibo al profesional el mismo día, elaborando los documentos que faltan en el archivo. El profesional tiene entonces un período de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si la CFE se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### d. Si es necesario, registre los estatutos de la empresa

Una vez que el estado de la empresa ha sido fechado y firmado, el controlador del ascensor debe registrarlos con el[Departamento del Impuesto de Sociedades]((http://www2.impots.gouv.fr/sie/ifu.htm)) (SIE) si:

- El acto implica una transacción particular sujeta al registro;
- la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.