﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS034" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Diagnóstico Inmobiliario" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="diagnostico-inmobiliario" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/diagnostico-inmobiliario.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="diagnostico-inmobiliario" -->
<!-- var(translation)="Auto" -->

Diagnóstico Inmobiliario
========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

El diagnóstico inmobiliario es un profesional cuya actividad consiste, con motivo de una transacción inmobiliaria, de establecer un balance técnico del edificio.

El archivo de diagnóstico técnico (DDT) puede incluir:

- Rendimiento energético (diagnóstico del rendimiento energético (EDP)) del edificio para la emisión de la etiqueta energética del edificio;
- La seguridad de sus instalaciones de gas;
- Instalaciones eléctricas
- presencia de amianto, plomo o cualquier otra causa de condiciones insalubres.

**Tenga en cuenta que**

Los documentos establecidos en relación con su actividad incluyen la mención de la certificación del profesional.

*Para ir más allá* Artículos L. 271-4 y R. 271-3 del Código de Construcción y Vivienda.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad.

La actividad de diagnóstico inmobiliario es de carácter comercial, por lo que la CFE competente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad de diagnóstico inmobiliario, el profesional debe tener uno de los siguientes diplomas:

- una licencia profesional que mencione "BTP trades: energía y desempeño ambiental de los edificios" o "Seguridad de bienes y personas";
- un diploma en construcción e investigación de obras públicas por el Conservatorio Nacional de Artes y Oficios (Cnam);
- un título de técnico de diagnóstico inmobiliario del Instituto Técnico de Gas y Aire (ITGA);
- un diploma de diagnóstico inmobiliario de una institución de educación superior.

Estos diplomas están disponibles para los candidatos de un curso de formación en condición de estudiante o estudiante, contrato de aprendizaje, después de la educación continua, contrato de profesionalización, solicitud individual o a través del proceso de validación de la experiencia (VAE). Para obtener más información, consulte el sitio web oficial de la[Vae](http://www.vae.gouv.fr/).

En ausencia de cualquiera de los títulos anteriores, el profesional debe tener tres años de experiencia profesional como técnico o oficial de máster en el campo de la construcción, o cualquier otra función equivalente.

*Para ir más allá* Directorio nacional de certificaciones profesionales ([RNCP](http://www.rncp.cncp.gouv.fr)) ; Apéndice 2 del decreto de 16 de octubre de 2006 por el que se establecen los criterios para certificar las competencias de las personas que realizan el diagnóstico del rendimiento energético o el certificado de consideración de las normas y criterios térmicos acreditación de organismos de certificación.

Una vez que el profesional cumple estas condiciones, debe llevar a cabo el procedimiento de certificación de sus habilidades (ver infra "3." a. Certificación de habilidades").

*Para ir más allá* Artículo R. 271-1 del Código de La Construcción y Vivienda.

### b. Cualificaciones profesionales - Nacionales Europeos (Servicio Gratuito o Establecimiento Libre)

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido podrá ejercer de forma temporal y casual o la misma actividad en Francia.

Por esta razón, el nacional está sujeto a los mismos requisitos que el nacional francés (véase más arriba "2". a. Cualificaciones profesionales") y, por lo tanto, deben justificar un título de un nivel equivalente al exigido a un nacional francés.

Además, debe obtener la certificación de un organismo europeo signatario del acuerdo multilateral europeo adoptado como parte de la coordinación europea de los organismos de acreditación.

*Para ir más allá* Apéndice 2 del auto de 16 de octubre de 2006 supra.

### c. Condiciones de honor y sanciones penales

El profesional debe llevar a cabo su actividad de forma imparcial e independiente y, como tal, no debe tener ninguna relación para interferir con estas obligaciones.

**Tenga en cuenta que**

El profesional puede estar obligado a cumplir con las reglas éticas específicas de cada organismo certificador. Para obtener más información, es aconsejable visitar el sitio web de estas organizaciones.

*Para ir más allá* Artículos L. 271-6 y R. 271-3 del Código de Construcción y Vivienda.

**Sanciones penales**

El diagnóstico inmobiliario se enfrenta a una multa de hasta 1.500 euros (o 3.000 euros en caso de reincidencia) si:

- hace un diagnóstico sin estar calificado profesionalmente, sin haber contratado un seguro (ver infra "2. (d. Seguros) o en falta de conocimiento de las obligaciones de imparcialidad e independencia;
- está certificado por un organismo no acreditado para emitir la certificación.

*Para ir más allá* Artículo R. 271-4 del Código de La Construcción y Vivienda.

### d. Algunas peculiaridades de la regulación de la actividad

**Seguro**

El diagnóstico de bienes raíces está obligado a tomar un seguro para descubrir los riesgos incurridos durante su actividad. El importe de esta garantía no puede ser inferior a 300.000 euros por siniestralidad y de 500.000 euros por año de seguro.

**Tenga en cuenta que**

Si el profesional es un empleado, le confiesa a su empleador que contrate dicho seguro por los actos realizados durante su actividad profesional.

*Para ir más allá* Artículos L. 271-6 y R. 271-2 del Código de Construcción y Vivienda.

**Obligaciones y metodología específica para cada diagnóstico**

Antes de cada diagnóstico técnico, el profesional entrega al cliente un documento que atestigua el honor de que se encuentra en una situación regular y que dispone de los medios necesarios para llevar a cabo su actuación. Además, es necesario seguir las normas metodológicas y presentar un informe a la autoridad competente en función de la naturaleza del diagnóstico realizado.

Por lo tanto, de una manera no exhaustiva:

- todas las EED deben remitirse a la Agencia de Gestión del Medio Ambiente y la Energía ([Ademe](http://www.ademe.fr/)) para el enfoque estadístico, la evaluación y la mejora metodológica (véase el artículo L. 134-4-2 del Código de la Construcción y la Vivienda);
- después de un diagnóstico de la instalación de gas, el profesional debe preparar un informe de visita de acuerdo con el modelo establecido en el Anexo 1 de la Detenido 6 de abril de 2007 se define el modelo y el método para lograr el estado de la instalación de gas interior;
- en caso de diagnóstico de instalaciones eléctricas, deberá elaborar un informe de acuerdo con el modelo establecido en el Detenido 28 de septiembre de 2017 definiendo el modelo y método para lograr el estado de instalación de electricidad interior en edificios residenciales;
- Tras un diagnóstico de la presencia de termomitos en el edificio de que se trate, el profesional deberá elaborar un informe de conformidad con las disposiciones de la Detenido 29 de marzo de 2007 definiendo el modelo y método de logro de la condición del edificio para la presencia de termitas.

**Monitoreo**

El profesional certificado es supervisado por el organismo certificador y debe proporcionar:

- La situación de las reclamaciones y reclamaciones en su contra durante su actividad;
- todos los informes e información sobre la misión llevada a cabo (fecha, locales, tipo de misión, clase energética de locales).

**Tenga en cuenta que**

Esta información puede solicitarse durante el proceso de renovación de la certificación.

*Para ir más allá* Artículo 2-1 del auto de 16 de octubre de 2006 supra; Sección R. 271-1 del Código de Vivienda y Construcción.

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

Es aconsejable consultar la hoja "Establecimiento que recibe al público" para obtener más información.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Certificación de habilidades

El candidato a la certificación debe presentar un expediente de solicitud al organismo certificador que juzgará su admisibilidad. Una vez elegible, el candidato debe ser objeto de:

- examen teórico para verificar que la persona tiene el conocimiento de:- generalidades sobre el edificio,
  - La térmica del edificio,
  - El sobre del edificio,
  - diferentes sistemas (tecnologías de calefacción, implantación de energías renovables, etc.),
  - Textos reglamentarios
- un examen práctico del análisis situacional de un candidato para verificar que es capaz de evaluar el consumo de energía de un edificio y desarrollar un diagnóstico del rendimiento energético teniendo en cuenta los detalles del caso Tratado.

La certificación otorgada al profesional tiene una validez de cinco años. Al final de este período debe proceder a su renovación.

**Tenga en cuenta que**

Un profesional sólo puede poseer una certificación y debe proporcionar una declaración de honor que acredite que no tiene otra certificación.

*Para ir más allá* Artículo R. 134-4 del Código de La Construcción y Vivienda; 16 de octubre de 2006.

### b. Formalidades de notificación de la empresa

**Autoridad competente**

El diagnóstico inmobiliario debe proceder a la declaración de su empresa. Para ello, debe hacer una declaración a la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

El interesado debe proporcionar la documentos de apoyo dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales del CCI envía un recibo al profesional el mismo día indicando los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo.

Una vez que el expediente se haya completado, la CPI le dirá al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la devolución de su expediente siempre que no se haya presentado durante los plazos mencionados anteriormente.

Si la CFE se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

#### c. Si es necesario, el registro de los estatutos de la empresa

El diagnóstico de bienes inmuebles debe, una vez que los estatutos de la empresa estén fechados y firmados, registrarlos en la Oficina del Impuesto de Sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio cuando los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

