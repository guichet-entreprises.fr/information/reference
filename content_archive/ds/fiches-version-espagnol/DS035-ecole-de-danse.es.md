﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS035" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseñanza" -->
<!-- var(title)="Escuela de danza" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="ensenanza" -->
<!-- var(title-short)="escuela-de-danza" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/ensenanza/escuela-de-danza.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="escuela-de-danza" -->
<!-- var(translation)="Auto" -->


Escuela de danza
================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

La escuela de danza es una institución donde la danza se enseña en todas sus formas (clásica, contemporánea, jazz, deportes, urbana, baile de salón, etc.). Como parte de la escuela, el profesor de danza transmite sus conocimientos a sus alumnos a través del proyecto pedagógico que desarrolla.

*Para ir más allá* Artículos L. 462-1 y los siguientes artículos del Código de Educación.

### b. Centro competente de formalidades comerciales (CFE)

La CFE correspondiente depende de la naturaleza de la actividad y de la forma jurídica de la empresa:

- para las profesiones liberales, la CFE competente es la Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

Para más información, es aconsejable visitar los sitios web de Urssaf,[CPI en París](http://www.entreprises.cci-paris-idf.fr/web/formalites),[CPI de Estrasburgo](http://strasbourg.cci.fr/cfe/competences) Y[Servicio público](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere diploma para operar una escuela de baile. Por otro lado, la escuela de danza sólo puede emplear profesores de danza cuya cualificación profesional o experiencia es reconocida.

Para más información, es aconsejable consultar la hoja de actividades " [Profesor de danza](https://www.guichet-entreprises.fr/es/ds/enseignement/professeur-de-danse.html) ».

*Para ir más allá* Artículo L. 462-1 del Código de Educación.

### b. Condiciones de honorabilidad - incompatibilidades

Ninguna persona puede operar a cambio de remuneración, ya sea directamente o a través de otra persona, una institución donde se enseña la danza, si ha sido objeto de una condena en virtud de la Sección L. 362-5 del Código de Educación, es decir, una pena de prisión sin pena condicional de más de cuatro meses por delitos de violación, agresión sexual, agresión sexual a un menor o proxenetismo.

*Para ir más allá* Artículos L. 362-5 y L. 462-2 del Código de Educación; Artículos 222-22 y siguientes del Código Penal.

### c. Seguro de responsabilidad civil

El operador de una escuela de baile debe contratar un contrato de seguro que cubra su responsabilidad civil, la de los maestros, asistentes y los que allí se les enseña.

*Para ir más allá* Artículo L. 462-1 del Código de Educación.

### d. Algunas peculiaridades de la regulación de la actividad

#### Reglamento general aplicable a todas las instituciones públicas (ERP)

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas para las instituciones que reciben el público:

- Fuego
- Accesibilidad.

Para más información, es aconsejable consultar la hoja de actividades " [Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

#### Regulaciones específicas de la escuela de baile

##### Cumplir con los estándares de salud y seguridad

Las salas de baile tienen al menos un escritorio de confort y una ducha. Cuando hay más de veinte usuarios admitidos simultáneamente, estas instalaciones sanitarias y sanitarias se incrementan en una unidad por cada veinte usuarios adicionales o una fracción de ese número.

Los operadores deben tener un kit de emergencia (diseñado para proporcionar primeros auxilios en caso de accidente) y un medio de comunicación para alertar rápidamente a los servicios de emergencia.

Los explotadores de los establecimientos en los que se proporciona instrucción de danza están obligados, en un plazo de 8 días, a informar al prefecto del departamento de cualquier accidente que se haya producido en su establecimiento que requiera hospitalización.

*Para ir más allá* Artículos R. 462-2 a R. 462-4 del Código de Educación.

##### En cuanto a la organización de equipos espaciales y terrestres

En las salas de baile utilizadas con fines didácticos, el área de evolución de los bailarines está cubierta con un material suave, flexible y resistente que lo hace sin escupitajos. No descansa directamente sobre terrenos duros como hormigón o baldosas.

Cuando la zona de evolución se compone de un suelo de parquet, los elementos que lo componen se producen a partir de madera con una estructura y cohesión que evitará la formación de esparderas o roturas.

Finalmente, durante la clase de baile, el área cambiante y el espacio de los salones están libres de cualquier obstáculo que represente una amenaza para la seguridad de los estudiantes.

*Para ir más allá* Artículo R. 462-1 del Código de Educación.

##### Exhibiciones obligatorias en el lugar de instrucción

Algunos documentos deben ser publicados en las instalaciones de la escuela de danza:

- Una copia de la recepción de un estado de cuenta de apertura o un cambio en la actividad realizado a la prefectura;
- reproducción de los artículos R. 462-1 a R. 462-9, R. 362-1 y R. 362-2 del Código de Educación, relativos a las normas técnicas, de higiene y de seguridad;
- una junta de organización de socorro que se muestra en una ubicación accesible para profesores y usuarios. Incluye las direcciones y números de teléfono de los servicios y agencias que se utilizan en caso de emergencia;
- la lista de profesores, con una indicación de la fecha de graduación o exención.

*Para ir más allá* Artículo R. 462-5 del Código de Educación.

##### En cuanto a los estudiantes

Los operadores de danza deben asegurarse, antes del inicio de cada período de enseñanza, de que los estudiantes dispongan de un certificado médico que acredite la falta de contraindicación a la enseñanza que se les haya proporcionado. Este certificado debe renovarse cada año. A petición de cualquier profesor, se debe exigir un certificado que acredite un examen médico adicional.

*Para ir más allá* Artículo R. 362-2 del Código de Educación.

La escuela solo puede acomodar a estudiantes mayores de cuatro años.

Los niños de cuatro y cinco años sólo pueden participar en actividades de excitación física (es decir, excluyendo todas las técnicas específicas de la disciplina enseñada).

Los niños de cinco y seis años sólo pueden participar en una actividad introductoria (es decir, excluir todas las técnicas específicas de la disciplina que se enseña).

En general, todas las actividades realizadas por niños de cuatro a siete años incluidos no deben implicar ningún trabajo gravoso para el cuerpo, extensiones excesivas o articulaciones forzadas.

*Para ir más allá* Artículos L. 462-1 y R. 362-1 del Código de Educación.

##### Transmisión de música durante clases o pasantías

La escuela de danza que desee transmitir música sonora debe solicitar permiso a la Sociedad de Compositores y Editores de Música (Sacem) antes de la apertura de la escuela.

A continuación, se celebró un contrato entre el Sacem y la escuela de danza que preveía, a cambio del pago de una tasa fija, la autorización para difundir las obras del repertorio francés e internacional gestionada por el Sacem. El importe de la cuota varía según el número de estudiantes y el tipo de cursos impartidos. Para más detalles, es aconsejable consultar el [Sitio web oficial de Sacem](https://www.Sacem.fr/).

**Qué saber**

Sacem también es responsable de recaudar, en nombre de la Society for the Collection of Fair Compensation (SPRE), una remuneración justa adeudada a los artistas intérpretes o ejecutantes y productores por el uso de música grabada. La escuela de baile también tendrá que pagar esta cuota al Sacem.

Los cursos de baile se basan en una tarifa basada en su duración en días.

*Para ir más allá* : Código de Propiedad Intelectual y decisión del 8 de diciembre de 2010 de la comisión en virtud del artículo L. 214-4 del Código de Propiedad Intelectual por la que se modifica la Decisión del 5 de enero de 2010.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para más información, es aconsejable consultar las hojas de actividad "Formalidad de informar de una empresa comercial" y "Registro de una empresa individual en el registro de comercio y empresas.

### b. Autorización posterior al registro

#### Declaración de apertura de una sala de enseñanza de baile

La apertura de un establecimiento donde se imparte danza debe declararse al menos dos meses antes de su apertura.

**Autoridad competente**

La declaración debe dirigirse al prefecto del departamento donde se encuentran las instalaciones.

**Tiempo de respuesta**

El prefecto podrá, en el plazo de un mes a partir de la declaración, prohibir la apertura del establecimiento si no se cumplen las garantías técnicas, de higiene y de seguridad.

**Documentos de apoyo**

La declaración de apertura de una escuela de baile debe incluir:

- Formulario Cerfa 1045203 lleno, fechado y firmado;
- documentos relacionados con las características de la habitación (plano que muestra la escala, el área total, el número de habitaciones de confort, el número de duchas y lavabos, el número de estudios y, para cada estudio, especifican su área, altura, naturaleza y características del suelo, iluminación y modo de ventilación);
- certificado de seguro de responsabilidad civil.

**Resultado del procedimiento**

A menos que se realice la orden de apertura, el prefecto envía al declarante una recepción de una declaración de apertura. Se debe mostrar una copia de este recibo en el establecimiento.

**Costo**

Gratis.

*Para ir más allá* Artículos L. 462-1 y L. 462-4 del Código de Educación.

#### Declaración de cambio de actividad o cierre de una sala de enseñanza de baile

El operador debe informar de la modificación o cierre de su escuela de danza dentro de los 15 días de la modificación o cierre.

**Autoridad competente**

La declaración deberá dirigirse al prefecto del departamento donde se encuentren los locales, dentro de los 15 días siguientes al cambio de actividad o al cierre del establecimiento.

**Tiempos de respuesta**

El prefecto podrá, en el plazo de un mes a partir de la declaración de la modificación, ordenar el cierre del establecimiento por un máximo de tres meses, si no se cumplen las garantías técnicas, higiénicas y de seguridad.

**Evidencia de una declaración de cambio**

La declaración debe incluir:

- Formulario Cerfa 1045403 lleno, fechado y firmado;
- documentos relacionados con las características de la habitación antes de la modificación: un mapa que muestra la escala, el área total, el número de salas de confort, el número de duchas y lavabos, el número de estudios y, para cada estudio, especificar su área, su altura, características de la naturaleza y del suelo, métodos de iluminación y ventilación;
- documentos relacionados con las características de la habitación después de la modificación: un mapa que muestra la escala, el área total, el número de habitaciones de confort, el número de duchas y lavabos, el número de estudios y, para cada estudio, especificar su área, su altura, características de la naturaleza y del suelo, métodos de iluminación y ventilación;
- certificado de seguro de responsabilidad civil.

**Evidencia de una declaración de cierre**

La declaración debe incluir:

- Formulario Cerfa 1045303 lleno, fechado y firmado;
- documentos relativos a las características de los locales: su superficie, el número de estudios y sus respectivas superficies;
- certificado de seguro de responsabilidad civil.

*Para ir más allá* Artículos L. 462-1 y L. 462-4 del Código de Educación.

