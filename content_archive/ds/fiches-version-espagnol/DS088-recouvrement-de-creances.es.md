﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS088" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sector financiero y jurídico" -->
<!-- var(title)="Recuperación de la deuda" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sector-financiero-y-juridico" -->
<!-- var(title-short)="recuperacion-de-la-deuda" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/sector-financiero-y-juridico/recuperacion-de-la-deuda.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="recuperacion-de-la-deuda" -->
<!-- var(translation)="Auto" -->


Recuperación de la deuda
========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El cobro de la deuda ocurre cuando un profesional actúa en nombre de un acreedor con un deudor que no ha pagado, en la fecha de vencimiento, la cantidad o los montos que debe.

Revivirá al deudor tantas veces como sea posible a través de cartas de cobro, subidas telefónicas o, si es necesario, visitas a la casa del deudor antes de posibles avisos. Una vez recuperadas las sumas adeudadas por el deudor, el profesional las devolverá directamente al acreedor.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- En caso de actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las profesiones liberales, la CFE competente es la Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El profesional que esté obligado a proporcionar asesoramiento jurídico o a redactar actos bajo semilla privada debe tener un título de derecho (B.A. 3) o, en su defecto, justificar la jurisdicción legal apropiada para consultar y redactar acciones. asuntos legales.

*Para ir más allá* Artículo 54 de la Ley 71-1130, de 31 de diciembre de 1971, de reforma de determinadas profesiones judiciales y jurídicas.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

No existen disposiciones para un nacional de un Estado miembro de la Unión Europea (UE) o parte del Espacio Económico Europeo (EEE) que desee ejercer en el cobro de deudas en Francia de forma temporal y casual o en título permanente.

Por lo tanto, sólo las medidas adoptadas para el nacional francés tendrán que aplicarse (véase infra "3o. Procedimientos y trámites de instalación").

### c. Condiciones de honorabilidad e incompatibilidad

El profesional encargado del cobro de la deuda está obligado por el secreto profesional de la información que tendría que conocer de cada una de las partes.

El profesional encargado del cobro de la deuda debe respetar ciertas formalidades, incluidas las mencionadas a continuación (véase más adelante "2. d. Algunas peculiaridades de la regulación de la actividad").

Si incumple estas obligaciones, se le multaría con 1.500 euros y hasta 3.000 euros en caso de reincidencia.

*Para ir más allá* Artículo R. 124-7 del Código de Procedimientos de Ejecución Civil.

### d. Algunas peculiaridades de la regulación de la actividad

#### Obligación de contrato de seguro de responsabilidad civil profesional

El profesional encargado del cobro de la deuda está obligado a realizar un seguro de responsabilidad civil profesional que le cubra por actos realizados durante su actividad profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empleador el que contrate dicho seguro a sus empleados.

*Para ir más allá* Artículo R. 124-2 del Código de Procedimientos de Ejecución Civil.

#### Obligación de abrir una cuenta reservada para recibir fondos

El profesional está obligado a abrir una cuenta en una institución de crédito que se utilizará sólo para recibir fondos del cobro de deudas.

Antes de cualquier ejercicio de la actividad, el profesional tendrá que transmitir un certificado que justifique la apertura de esta cuenta al fiscal de la República del Tribunal Superior del lugar de instalación.

*Para ir más allá* Artículo R. 124-2 del Código de Procedimientos de Ejecución Civil.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte las tarjetas "Formalidad de reportaje de una empresa comercial" y "Registro de una empresa comercial individual en el RCS".

### b. Hacer una declaración de la actividad de cobro de deudas

**Autoridad competente**

El profesional que desee llevar a cabo una actividad de cobro de deudas debe declarar primero esta actividad ante el fiscal de la República del Tribunal Superior del lugar de instalación.

**Documentos de apoyo**

La declaración se hace presentando un archivo con los siguientes documentos:

- un certificado de seguro de responsabilidad civil profesional;
- Un documento que justifica la apertura de una cuenta bancaria asignada a los fondos recaudados en el cobro de deudas;
- Si es así, una copia de los estatutos de la empresa
- un extracto de Kbis de la empresa.

**Procedimiento**

El fiscal confirmará la recepción del expediente y enviará un recibo a la persona interesada con un número de registro.

**Costo**

Gratis.

### c. Autorización posterior al registro

#### Obligación de celebrar un acuerdo con el acreedor

El profesional tiene la obligación de celebrar un acuerdo en el que se le otorguen la facultad de recibir fondos en nombre del acreedor.

La convención debe incluir toda la siguiente información:

- la base y el importe de las sumas adeudadas, con la indicación separada de los distintos elementos de los créditos o créditos que deben recuperarse sobre el deudor;
- los términos y condiciones de la garantía otorgada al acreedor contra las consecuencias monetarias de la responsabilidad civil contraída como resultado de la actividad de cobro de la deuda;
- Las condiciones para determinar la remuneración pagada por el acreedor;
- condiciones para la remesa de fondos recibidos en nombre del acreedor.

**Qué saber**

El profesional tiene la obligación de informar al acreedor de todas las sumas que recibiría del deudor o de cualquier propuesta hecha por el deudor para pagar la deuda por cualquier medio que no sea el pago inmediato.

Una vez recuperados y cobrados los fondos, el profesional de cobro de deudas tendrá que devolverlos en el plazo de un mes al acreedor.

*Para ir más allá* Artículos R. 124-3, R. 124-5 y R. 124-6 del Código de Procedimientos Civiles de Ejecución.

#### Obligación de informar al deudor de la existencia de un procedimiento de cobro de deudas

El profesional debe informar al deudor de que un procedimiento de cobro de deudas está en su contra. Le enviará una carta con las siguientes palabras:

- su nombre o nombre, dirección o sede central, indicación de que está involucrado en una actividad de recolección amistosa;
- Nombre o nombre, dirección u oficina central del acreedor;
- la base y el importe de la suma adeudada en el principal, intereses y otros accesorios, distinguiendo los diferentes elementos de la deuda, excluyendo los costos que quedan a expensas del acreedor;
- la indicación de tener que pagar la cantidad adeudada y las condiciones de pago de la deuda;
- reproducción de los párrafos tercero y cuarto del artículo L. 118-1 del Código de Procedimientos de Ejecución Civil: " *Los costos de recuperación asumidos sin un título exigible siguen siendo a expensas del acreedor, a menos que se refieran a un acto que la ley exige al acreedor. Cualquier estipulación en sentido contrario se considerará no escrita, a menos que exista cualquier otra disposición legal en contrario.* »
Una vez recibidos los fondos por parte del deudor, el profesional le dará una aprobación de la gestión justificando el pago de las cantidades ganadas.

*Para ir más allá* Artículo R. 124-4 del Código de Procedimientos de Ejecución Civil.

