<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS112" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Actividades de recaudación y distribución del derecho de retransmisión simultánea, integral y sin modificación por cable de una obra transmitida desde un Estado miembro sobre el territorio nacional" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="derecho-de-retransmision-por-cable" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/derecho-de-retransmision-por-cable.html" -->
<!-- var(last-update)="2021-01" -->
<!-- var(url-name)="derecho-de-retransmision-por-cable" -->
<!-- var(translation)="Auto" -->

# Actividades de recaudación y distribución del derecho de retransmisión simultánea, integral y sin modificación por cable de una obra transmitida desde un Estado miembro sobre el territorio nacional [FR]

Actualización más reciente : <!-- begin-var(last-update) -->2021-01<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition 

L'article 9 de la directive 93/83 du 27 septembre 1993 relative à la coordination de certaines règles applicables à la radiodiffusion par satellite et à la retransmission par câble crée une obligation de gestion collective pour le droit de retransmission simultanée, intégrale et sans changement par câble d’une œuvre télédiffusée à partir d'un État membre sur le territoire national.

L'activité de gestion collective des droits de retransmission par câble est soumise à agrément du ministère de la Culture.

## 2°. Conditions d'installation 

Pour être titulaire d’un agrément du ministère de la Culture permettant d’exercer l’activité de gestion collective du droit de retransmission simultanée, intégrale et sans changement par câble d’une œuvre télédiffusée à partir d'un État membre sur le territoire national, il est nécessaire de démontrer (article L. 132-20-1 du Code de la propriété intellectuelle) :

- la qualification professionnelle des dirigeants des organismes. La notion de « qualification professionnelle » doit être entendue au sens de « compétence professionnelle » : aucune exigence de niveau d'étude ni de qualification particulier n'est requise ;
- les moyens que ceux-ci peuvent mettre en œuvre pour assurer le recouvrement des droits et l'exploitation de leur répertoire ;
- l’importance de leur répertoire.

## 3°. Démarches et formalités d'installation 

### a. Demande d'agrément

#### Autorité compétente

L’organisme qui souhaite exercer l’activité de gestion collective du droit de retransmission simultanée, intégrale et sans changement par câble d’une œuvre télédiffusée à partir d'un État membre sur le territoire national doit adresser sa demande d'agrément au ministère de la Culture. 

#### Pièces justificatives

Cette demande doit être accompagnée des éléments suivants (article R. 323-1 du Code de la propriété intellectuelle) : 

- toute preuve de la gestion effective du droit d'autoriser la retransmission par câble, à raison du nombre des ayants droit et de l'importance économique exprimée en revenu ou en chiffre d'affaires ;
- toutes pièces permettant de justifier la qualification de ses gérants et mandataires sociaux appréciée en fonction :
  - soit de la nature et du niveau de leurs diplômes,
  - soit de leur expérience de la gestion d'organismes professionnels ;
- toutes informations relatives à l'organisation administrative et aux conditions d'installation et d’équipement ;
- toutes informations relatives aux perceptions reçues ou attendues à l'occasion de la retransmission par câble, simultanée, intégrale et sans changement, sur le territoire national, à partir d'un État membre de l’Union européenne et aux données nécessaires pour leur répartition ;
- copie des conventions passées avec les tiers relatives à la retransmission par câble, simultanée, intégrale et sans changement, sur le territoire national, à partir d'un État membre de la Communauté européenne ;
- le cas échéant, copie des conventions passées avec les organisations professionnelles étrangères chargées de la perception et de la répartition des droits.

#### Procédure

L’intéressé doit adresser sa demande d'agrément par lettre recommandée avec avis de réception au ministre chargé de la culture qui en délivre récépissé. 

La demande peut également être effectuée [en ligne](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/PROPR_DEMAN_agrement_04/?__CSRFTOKEN__=643c9f26-9b48-4aba-ab02-25e89d798e5c). 

Lorsque le dossier n'est pas complet, le ministre chargé de la culture demande par lettre recommandée avec avis de réception un dossier complémentaire qui doit être remis dans la même forme dans un délai d'un mois à compter de la réception de cette lettre.

L'agrément est délivré par arrêté du ministre chargé de la culture, publié au Journal officiel de la République française. Il est accordé pour cinq années et est renouvelable dans les mêmes conditions que l'agrément initial.

#### Changements de situation

Si l'organisme cesse de remplir l'une des conditions fixées à l'article R. 323-1 du Code de la propriété intellectuelle, l'administration lui adresse une mise en demeure par lettre recommandée avec avis de réception. Le bénéficiaire de l'agrément dispose d'un délai d'un mois pour présenter ses observations. Faute de régularisation de la situation, l'agrément peut être retiré par arrêté du ministre chargé de la culture, publié au Journal officiel de la République française.

Tout changement de statut ou de règlement général, toute cessation de fonction d'un membre des organes dirigeants et délibérants d'un organisme agréé sont communiqués au ministre chargé de la culture dans un délai de quinze jours à compter de la décision correspondante. Le défaut de déclaration peut entraîner retrait de l'agrément

## 4°. Textes de référence 

- Code de la propriété intellectuelle, partie législative, articles L.132-20-1 et L. 217-2 ;
- Code de la propriété intellectuelle, partie réglementaire, articles R.323-1 à R. 323-5.