﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS056" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Servicios humanos" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="servicios-humanos" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/servicios-humanos.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="servicios-humanos" -->
<!-- var(translation)="Auto" -->


Servicios humanos
=================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El profesional que se dedica a una actividad de servicios humanos (SAP) ayuda a mejorar la vida diaria de las personas. Estos servicios de diversa naturaleza se llevan a cabo en los hogares de los beneficiarios del beneficio o de sus hogares.

La actividad de SAP incluye un conjunto de actividades que pueden estar sujetas a una declaración opcional y algunas de las cuales están sujetas a aprobación.

Las actividades sujetas a aprobación incluyen:

# atención domiciliaria para niños menores de 3 años y menores de 18 años con discapacidades;
# niños menores de 3 años y menores de 18 años con discapacidades en sus viajes fuera de sus hogares (paseos, transporte, actos cotidianos);
# asistencia en las actividades diarias de la vida o asistencia a la integración social a las personas mayores y con discapacidad o a las personas con enfermedades crónicas que necesitan tales servicios en el hogar, excluyendo los actos de atención relacionados con actos de atención relacionados con actos de atención Médica
# conducir el vehículo personal de los ancianos, discapacitados o con condiciones crónicas de casa a trabajo, en el lugar de vacaciones, para procedimientos administrativos;
# apoyo a los ancianos, personas con discapacidad o enfermedades crónicas, en sus viajes fuera de sus hogares (paseos, movilidad y asistencia de transporte, actos cotidianos).

Las actividades que no están sujetas a aprobación son:

# limpieza y tareas domésticas;
# pequeños trabajos de jardinería, incluyendo trabajos de limpieza;
# pequeños trabajos diy llamados "hombre todas las manos";
# cuidado de niños en el hogar por encima de una edad establecida por decreto conjunto del Ministro de Economía y el Ministro de la Familia;
# apoyo escolar en casa o educación en casa
# cosméticos caseros para dependientes;
# preparar las comidas en casa, incluido el tiempo que pasa en las carreras;
# entrega de comidas caseras;
# recogida y entrega a domicilio de ropa de cama planchada;
# entrega de comestibles a domicilio;
# Soporte de TI basado en el hogar
# cuidados y paseos de mascotas, con excepción de la atención veterinaria y el aseo, para los dependientes;
# Mantenimiento temporal, mantenimiento y vigilancia, en el hogar, de la residencia principal y secundaria;
# asistencia administrativa en el hogar;
# acompañar a los niños mayores de tres años en sus viajes fuera de sus hogares (paseos, transporte, actos cotidianos);
# teleasistencia y asistencia en vídeo;
# Intérprete de lenguaje de señas, técnico escrito y codificador de lenguaje hablado completado;
# el vehículo personal de las personas mencionadas en el 20 de la II de este artículo, de casa a obra, en el lugar de vacaciones, para los procedimientos administrativos;
# Apoyo temporalmente dependiente en sus viajes fuera de sus hogares (paseos, asistencia de movilidad y transporte, actos cotidianos);
# asistencia a las personas temporalmente dependientes, en el hogar, excluyendo la atención médica;
# coordinación y prestación de los 20 servicios enumerados anteriormente.

Todas las actividades de SAP, estén o no bajo la acreditación, están sujetas, de manera opcional, para ser reportadas para calificar para los beneficios fiscales y sociales de AMPs.

*Para ir más allá* Artículos L.7231-1, L.7233-2 y D.7231-1 del Código de Trabajo.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para las actividades artesanales, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- Para las actividades comerciales, es la Cámara de Comercio e Industria (CCI);
- para el ejercicio de una actividad liberal, la CFE competente es el Urssaf.

Todos los procedimientos pueden llevarse a cabo en línea directamente en guichet-entreprises.fr, que transmite a la CFE pertinente.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Condiciones para obtener la acreditación

Para su aprobación:

# el profesional debe disponer, dentro o dentro de la red de la que forme parte, los medios humanos, materiales y financieros para llevar a cabo los servicios SAP para los que se solicita la acreditación;
# debe comprometerse a cumplir las especificaciones establecidas en el decreto de 1 de octubre de 2018 en virtud del artículo R.7232-6 del Código de Trabajo;
# Los ejecutivos no deben haber sido objeto de una condena penal o de una sanción civil, comercial o administrativa que les prohíba administrar, administrar o dirigir una corporación o llevar a cabo una actividad comercial;
# cuando la actividad de SAP esté relacionada con menores, dirigentes, supervisores y partes interesadas no deben registrarse en el expediente judicial automatizado nacional de los autores de delitos sexuales, o equivalentes, para los nacionales de un Estado miembro Unión Europea u otro Estado parte en el acuerdo del Espacio Económico Europeo.

Una vez que el profesional cumple con estas condiciones, debe solicitar la certificación para ejercer su profesión.

*Para ir más allá* Artículo R. R7232-6 del Código de Trabajo.

### b. Cualificaciones profesionales

Para llevar a cabo las actividades aprobadas por SAP, el profesional debe justificar las cualificaciones profesionales especificadas en el decreto de 1 de octubre de 2018 por el que se establecen las especificaciones establecidas en el artículo R. 7232-6 del Código de Trabajo.

### c. Cualificaciones profesionales - Nacionales Europeos (Servicio Gratuito o Establecimiento Libre)

Un nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) que desee llevar a cabo una actividad sap a Francia de forma temporal y ocasional (como parte de una prestación gratuita de servicios (LPS ) o de forma permanente (como parte de un establecimiento libre (LE)) está sujeto a los requisitos de cualificación profesional especificados en el orden de 1 de octubre de 2018 por el que se establecen las especificaciones establecidas en el artículo R. 7232-6 del Código de Trabajo.

### d. Algunas peculiaridades de la regulación de la actividad

#### Medidas fiscales

Siempre que el profesional lleve a cabo su actividad SAP exclusivamente (sin ninguna otra actividad dentro de la misma estructura), puede beneficiarse de las ventajas fiscales (véanse los artículos 279 y 199 sexdecies del Código General Tributario). Para ello, el profesional debe hacer una declaración de actividad (ver infra "3 grados. Procedimientos y formalidades de instalación para actividades aprobadas").

Para algunas actividades, el profesional sólo puede beneficiarse de estas prestaciones si el beneficio se presta como parte de un conjunto de actividades realizadas en el país (véase el artículo III D. 7231-1 del Código de Trabajo para averiguar qué actividades participan).

*Para ir más allá* En: circular del 11 de abril de 2019 (NOR: ECOI1907576C).

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Registro de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Solicitud de acreditación

#### Autoridad competente

La solicitud de aprobación está dirigida al prefecto del departamento electrónicamente o por carta recomendada con notificación de recepción.

#### Documentos de apoyo

La solicitud de acreditación establece:

# El nombre de la organización
# El discurso de la organización y sus instituciones
# La naturaleza de los servicios prestados y del público o de los clientes interesados;
# Las condiciones de empleo del personal;
# los medios de explotación implementados.

Se adjunta un archivo a la solicitud de aprobación, que incluye:

# un extracto del registro de comercios y sociedades o del directorio de comercios o una copia de los estatutos de la persona jurídica, o, en su caso, para los nacionales de un Estado miembro de la Unión Europea u otro Estado parte en el Acuerdo sobre la Zona Economía europea, un documento equivalente;
# Elementos para evaluar el nivel de calidad de los servicios implementados;
# Un modelo de documento que proporciona información a clientes y usuarios sobre asuntos fiscales y servicios administrativos en materia estadística;
# La lista de subcontratistas;

*Para ir más allá* : la solicitud de aprobación se presenta en las condiciones establecidas en los artículos R.7232-1 a R.7232-3 del Código de Trabajo y en los puntos 42 y 67 del pliego de condiciones de 1 de octubre de 2018.

**Tenga en cuenta que**

Los profesionales con sede legal en otro Estado miembro de la UE o parte en el Acuerdo sobre el Espacio Económico Europeo adjuntan a su expediente cualquier información y documentos relativos a su situación en relación con la aplicación de la obligaciones, en su caso, por la legislación aplicable en el estado en el que se basan, con el fin de revisar su solicitud de acreditación.

#### Resultado del procedimiento

Si el archivo está incompleto, el prefecto informa al profesional y lo invita a presentar los documentos o la información que faltan. El silencio mantenido por el prefecto durante más de tres meses a partir de la fecha de recepción de una solicitud completa de acreditación conlleva una decisión de aceptación.

*Para ir más allá* Artículo R7232-4 del Código de Trabajo

**Tenga en cuenta que**

Si el profesional tiene previsto llevar a cabo la actividad sujeta a acreditación en varios departamentos, el prefecto del departamento del lugar de establecimiento del principal establecimiento profesional reúne la opinión de los presidentes del Consejo Departamental del departamentos, a través de los prefectos territorialmente competentes.

#### Validez y renovación

La acreditación es válida por cinco años y cualquier solicitud de renovación debe enviarse al prefecto del departamento a más tardar tres meses antes del final de la acreditación anterior.

*Para ir más allá* Artículos R. 7232-7 y los siguientes artículos del Código de Trabajo.

#### Informe de negocios

El profesional certificado produce al menos un estado trimestral de actividad y cada año una evaluación cualitativa y cuantitativa de la actividad realizada durante el año pasado, así como un cuadro estadístico anual. Estos documentos se envían electrónicamente al prefecto, que los pone a disposición del Ministro responsable de la economía. Cuando el profesional tiene varios establecimientos, los estados estadísticos y el balance anual distinguen la actividad realizada por cada institución.

*Para ir más allá* Artículos R. 7232-9 y los siguientes artículos del Código de Trabajo.

4°. Procedimientos y trámites de instalación para las actividades de presentación de informes
------------------------------------------------------------------------------------------------------------------

### a. Registro de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Utilidad fiscal de la declaración

La declaración permite a los profesionales tener derecho a las prestaciones fiscales previstas en el artículo L. 7233-2 del Código del Trabajo (crédito fiscal previsto en el artículo 199 sexdecies del Código General Tributario y, en su caso, tipo reducido del IVA previsto en la i Artículo 279 o D del artículo 278-0 bis de la misma oda C para algunas de sus actividades aprobadas), así como las exenciones de las cotizaciones sociales a que se refiere el artículo L. 241-10 del Código de la Seguridad Social para las Actividades de Servicio Personal sección D. 7231-1 del Código de Trabajo.

La declaración abarca tanto las actividades que requieren aprobación o autorización previa como las actividades que pueden llevarse a cabo libremente.

El profesional se compromete a llevar a cabo únicamente las actividades especificadas en su declaración, entre las actividades de SAP enumeradas en el artículo D. 7231-1 del Código de Trabajo y previa obtención de la acreditación para las actividades bajo su jurisdicción.

*Para ir más allá* En: circular del 11 de abril de 2019 (NOR: ECOI1907576C).

### c. Solicitar el registro de la declaración

#### Autoridad competente

La declaración se dirige al Dirrecte territorialmente competente electrónicamente o por carta recomendada con notificación de recepción. La solicitud se realiza a través de la aplicación NOVA (www.entreprises.gouv.fr/services-a-la-personne) o en su defecto en un formulario estándar.

#### Contenido del expediente de declaración

El archivo de informes incluye:

# El nombre y la dirección del profesional
# La dirección de la institución principal del profesional y la dirección de sus escuelas secundarias;
# Mención de las actividades de SAP
# para los profesionales sujetos a la condición de actividad exclusiva, el compromiso del representante legal de ejercer exclusivamente los servicios a la persona sujeta a la declaración, excluyendo cualquier otro servicio o prestación de bienes (artículo L. 7232-1-1 del Código de Trabajo);
# para los profesionales exentos de la condición de actividad exclusiva, el compromiso del representante legal de establecer una contabilidad separada para contabilizar los gastos y productos relacionados con sus actividades de servicios personales por sí solo ( Artículo L. 7232-1-2 del Código de Trabajo);
# para las actividades mencionadas en los artículos 2o, 4o, 5o de la I y 8o, 9o, 10o, 15o, 18o y 19o del II del Artículo D. 7231-1 del Código de Trabajo, el profesional se compromete a asociar los servicios de prestación y transporte con una o más actividades de servicios personales. (una condición conocida como oferta global prevista en el período III del artículo D. 7231-1).

Esta lista es exhaustiva, el Dirrecte no puede solicitar ningún otro documento.

*Para ir más allá* Artículos R.7232-16 a R.7232-22 del Código de Trabajo y Circular de 11 de abril de 2019 (NOR: ECOI1907576C).

#### Resultado del procedimiento

Tras la recepción del fichero, se verifica su integridad, incluyendo la inscripción en el directorio Sirene o en el directorio nacional de asociaciones.

Cuando la declaración se completa, el Dirrecte la registra en un plazo de ocho días y emite un recibo por vía electrónica o postal al solicitante.

Cuando el expediente está incompleto, se envía una carta al solicitante para informar al solicitante de la información faltante.

*Para ir más allá* Artículo R7232-18 del Código de Trabajo y Circular de 11 de abril de 2019 (NOR: ECOI1907576C).

**Tenga en cuenta que**

El profesional que ha hecho una declaración se compromete a fijar en todos sus medios comerciales el logotipo que identifica el sector de los servicios humanos. Se pone a disposición de las corporaciones y contratistas individuales mediante la descarga a NOVA, después de la aceptación de las condiciones específicas de la licencia de los servicios de seguimiento a la persona.

#### Validez y renovación

La declaración entra en vigor el día de la presentación cuando el expediente está completo y cumple con todos los requisitos reglamentarios. No tiene un tiempo limitado y tiene un alcance nacional.

*Para ir más allá* En: circular del 11 de abril de 2019 (NOR: ECOI1907576C).

#### Informe de negocios

El profesional que ha realizado una rentabilidad produce al menos un estado trimestral de actividad y cada año una evaluación cualitativa y cuantitativa de la actividad realizada durante el año pasado, así como un cuadro estadístico anual. Estos documentos se envían electrónicamente al prefecto, que los pone a disposición del Ministro responsable de la economía. Cuando el profesional tiene varios establecimientos, los estados estadísticos y el balance anual distinguen la actividad realizada por cada institución.

*Para ir más allá* Artículo R.7232-19 del Código de Trabajo.

5°. Facturación SAP
-----------------------------

### a. El proyecto de ley

Los proveedores de SAP deben presentar una factura que muestre:

# El nombre y la dirección del profesional
# El número y la fecha de registro de la declaración si se solicita, así como el número y la fecha de expedición de la autorización cuando las actividades están comprendidas en el artículo L. 7232-1;
# El nombre y la dirección del destinatario del servicio
# La naturaleza exacta de los servicios prestados
# La cantidad de dinero realmente pagada por el servicio;
# Un número de registro de la coadyuvante que permita su identificación en los registros de los empleados de la empresa o de la asociación que proporciona;
# trabajadores por hora aplica todos los impuestos incluidos o, en su caso, la tarifa plana de la prestación;
# Contando el tiempo invertido
# el precio de los distintos servicios y cuando los beneficios son apoyados financieramente directamente por el blanqueador del servicio, el precio sigue siendo a expensas del beneficiario de la prestación;
# Si es así, los gastos de viaje
# Si procede, el nombre y el número de registro del subcontratista que realizó el servicio;
# cuando el profesional esté certificado bajo la Sección L. 7231-1 pero no esté declarado bajo la Sección L. 7232-1-1, las cotizaciones, facturas y documentos comerciales indican que los beneficios proporcionados no califican para los beneficios fiscales Artículo L. 7233-2.

Del mismo modo, cuando la actividad de SAP es ofrecida al usuario por un tercero en forma de tarjeta, cupón o pase de regalo, las cotizaciones, facturas y documentos comerciales declaran que los servicios prestados no califican para beneficios fiscales. Artículo L. 7233-2.

Cuando los beneficios de SAP están sujetos a impuestos sobre el IVA, las tasas, los precios y los gastos de viaje incluyen este impuesto.

*Para ir más allá* Artículo D.7233-1, D.7233-2 del Código de Trabajo y Circular de 11 de abril de 2019 (NOR:ECOI1907576C).

### b. Certificado fiscal

El profesional declarado deberá proporcionar a cada uno de sus clientes un certificado fiscal anual antes del 31 de marzo del año 1, a fin de que puedan beneficiarse de la prestación fiscal definida en el artículo 199 sexdecies del Código General Tributario en virtud del imposición del año N.

Este certificado debe incluir:

# Nombre, dirección y número de identificación del profesional
# El número y la fecha de registro de la declaración
# El nombre y la dirección de la persona que se benefició del servicio, el número de su cuenta adeudada, en su caso, el importe efectivamente pagado;
# un resumen de las intervenciones tomadas (nombre y número del código de identificación del orador, fecha y duración de la intervención).

En los casos en que las prestaciones se paguen en la CESU prefinanciada, el certificado deberá indicar al cliente que está obligado a identificarse claramente con las autoridades tributarias, en su declaración de impuestos anual, el importe de los CESUs que tiene personalmente financiado, esta cantidad por sí sola da lugar a beneficios fiscales.

Los pagos en efectivo no califican para un certificado de impuestos.

*Para ir más allá* Artículo D. 7233-3, Artículo D. 7233-4 del Código de Trabajo y Circular de 11 de abril de 2019 (NOR:ECOI1907576C).

#### Remedios

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si la CFE se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

#### Costo

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

