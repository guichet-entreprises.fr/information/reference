﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS067" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentación" -->
<!-- var(title)="Venta al por menor de alimentos" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentacion" -->
<!-- var(title-short)="venta-al-por-menor-de-alimentos" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/alimentacion/venta-al-por-menor-de-alimentos.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="venta-al-por-menor-de-alimentos" -->
<!-- var(translation)="Auto" -->


Venta al por menor de alimentos
===============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La venta al por menor de alimentos consiste en la compra de bienes para el consumo por parte de particulares. Esta actividad se realiza en tiendas de mayor o menor tamaño, pero también por Internet o en mercados.

Los productos a consumir no se procesan antes de ser revendidos.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Cualificaciones profesionales

No se requiere un diploma específico para la operación de un negocio minorista de alimentos. Sin embargo, es aconsejable tener conocimientos de contabilidad y gestión.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) no está sujeto a ningún requisito de cualificación o certificación, al igual que los franceses.

### c. Algunas peculiaridades de la regulación de la actividad

#### Visualización de precios

El operador de una empresa minorista de alimentos debe informar al público de los precios cobrados mediante el marcado, etiquetado, exhibición o cualquier otro proceso apropiado.

Cada producto debe tener un precio unitario de medición, es decir, un precio por kilo o litro, y un precio de venta.

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

#### Videovigilancia

Las tiendas minoristas de alimentos pueden equiparse con sistemas de videovigilancia de los clientes. Por lo tanto, el operador deberá indicar su presencia para no infringir su privacidad.

El operador también tendrá que declarar su instalación con la prefectura del establecimiento y completar el formulario[Cerfa 13806*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13806.do), acompañado de cualquier documento que detalle el sistema utilizado.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El contratista debe registrarse en el Registro Mercantil y Corporativo (SCN). Es aconsejable consultar los "Formalidades de Reporte de Empresas Artesanales" para obtener más información.

### b. Si es necesario, solicite una autorización de funcionamiento

La persona debe solicitar permiso para operar su negocio de venta al por menor de alimentos y, si es necesario, antes de conceder el permiso de construcción en los siguientes casos:

- La creación de una tienda minorista con una superficie comercial de más de 1.000 metros cuadrados, como resultado de una nueva construcción o de la transformación de un edificio existente;
- Ampliación del espacio comercial de una tienda minorista que ya ha alcanzado el umbral de 1.000 metros cuadrados o se espera que supere el proyecto completando el proyecto;
- cualquier cambio en la línea de negocio de un negocio con una superficie de ventas de más de 1.000 metros cuadrados cuando el nuevo negocio de la tienda es la comida;
- Creación de un complejo comercial con una superficie total de ventas de más de 1.000 metros cuadrados;
- la ampliación de la superficie de venta de un complejo comercial que ya ha alcanzado el umbral de 1.000 metros cuadrados o se espera que supere con la finalización del proyecto;
- reapertura al público en el mismo sitio de una tienda minorista con una superficie comercial de más de 1.000 metros cuadrados cuyas instalaciones han dejado de funcionar durante tres años;
- creación o ampliación de un punto de desistimiento permanente por parte de los clientes de compras minoristas controladas por telemática, organizadas para el acceso al automóvil.

La solicitud se envía en doce copias a la Comisión Departamental de Planificación Comercial (CDAC) de la prefectura del departamento donde se encuentra el comercio, que dispone de dos meses para autorizar la operación.

La solicitud va acompañada de todos los siguientes documentos justificativos:

- un plan indicativo que muestra el área de ventas de las tiendas;
- Información:- delineación de la zona de captación del proyecto,
  - transporte público y acceso peatonal y ciclista que se sirven en el comercio;
- un estudio con los elementos para evaluar los efectos del proyecto en:- Accesibilidad de la oferta comercial,
  - el flujo de turismos y vehículos de reparto, así como un acceso seguro a la vía pública,
  - Gestión del espacio,
  - consumo de energía y contaminación,
  - paisajes y ecosistemas.

En caso de denegación, la decisión podrá ser recurrida ante la Comisión Nacional de Planificación Comercial, que se pronunciará en un plazo de cuatro meses.

### c. Autorización posterior a la instalación

#### Hacer una declaración en caso de preparación o venta de productos animales o animales

Cualquier operador de un establecimiento que produzca, manipule o almacene productos animales o productos alimenticios que contengan ingredientes animales (carne, productos lácteos, productos pesqueros, huevos, miel) para el consumo debe cumplir con la obligación de declarar cada una de las instituciones de las que es responsable, así como las actividades que se llevan a cabo allí.

Debe hacerse antes de la apertura y renovación del establecimiento en caso de cambio de operador, dirección o naturaleza de la actividad.

La declaración debe dirigirse al prefecto del departamento de aplicación del establecimiento o, en el caso de los establecimientos bajo la autoridad o tutela del Ministro encargado de la defensa, al servicio de salud de las fuerzas armadas.

Para ello, el operador tendrá que completar el formulario[Cerfa No. 13984*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13984.do) que enviará a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP).

#### Solicitar aprobación sanitaria si los alimentos se venden a intermediarios

Los establecimientos que preparen, transformen, manipulen o almacenen productos o productos alimenticios animales en recipientes para consumo humano (leche cruda, carnes, huevos, etc.) deben obtener una aprobación sanitaria para comercializar sus productos a otros establecimientos.

**Autoridad competente**

La solicitud de acreditación debe dirigirse al DDPP de la ubicación del establecimiento.

**hora**

Si la solicitud de acreditación es completa y admisible, se concede una aprobación condicional por un período de tres meses al solicitante.

Durante este período, se realiza un cheque oficial. Si las conclusiones de esta visita son favorables, se concede la aprobación definitiva. De lo contrario, los puntos de incumplimiento se notifican al solicitante y la aprobación condicional podrá renovarse por otros tres meses. La duración total de la aprobación condicional no podrá exceder de seis meses.

**Documentos de apoyo**

La solicitud de acreditación sanitaria se realiza utilizando el formulario CERFA No 1398303 Completado, fechado y firmado y acompañado de todos los documentos justificativos enumerados en el Apéndice II del Decreto de 8 de junio de 2006 relativo a la acreditación sanitaria de establecimientos en el mercado de productos animales o alimenticios que contengan productos animales.

Estos documentos se refieren a la presentación de la empresa, la descripción de sus actividades y el plan de gestión de la salud.

*Para ir más allá* Artículos L. 233-2 y R. 233-1 del Código Rural y de la pesca marina; DGAL/SDSSA/N2012-8119 de 12 de junio de 2012 relativo al procedimiento de acreditación y a la composición del fichero.

**Es bueno saber**

Es aconsejable consultar con el SDCSPP para asegurar la necesidad de solicitar la acreditación y, si es necesario, para obtener asistencia en el establecimiento del expediente de solicitud.

#### Si es necesario, obtenga una exención del requisito de acreditación de salud

Los establecimientos afectados por la exención son las tiendas:

- ceding cantidades limitadas de alimentos a otros puntos de venta;
- las distancias de entrega a otros puntos de venta no superan los 80 kilómetros.

Las categorías de productos abarcadas por la exención incluyen:

- Productos lácteos
- leches tratadas térmicamente
- productos hechos de cáscaras de huevo o leche cruda que han sido sometidos a un tratamiento desinfectante.

El titular podrá obtener esta exención si la distancia a los establecimientos entregados no supera los 80 km (o más por decisión de la prefectura) y:

- para las leches tratadas térmicamente, si produce un máximo de 800 litros por semana cuando vende el 30% o menos de su producción total o 250 litros por semana cuando vende más del 30% de su producción total;
- para los productos lácteos y para los productos lácteos o de huevo de leche cruda que han sido sometidos a un tratamiento desinfectante, si produce un máximo de 250 kg por semana cuando vende el 30% o menos de su producción total o 100 kg si vende más del 30% de su producción total.

**Autoridad competente**

La solicitud de exención debe dirigirse al DDCSPP del departamento en el que se encuentra la institución.

**Documentos de apoyo**

La solicitud de exención de la subvención se realiza utilizando el formulario Cerfa No. 13982Completado, fechado y firmado.

**Costo**

Gratis.

*Para ir más allá* Artículo L. 233-2 del Código Rural y Pesca Marina; Artículos 12, 13, Anexos 3 y 4 de la Orden de 8 de junio de 2006 sobre la Acreditación Sanitaria de establecimientos que ofrezcan productos animales o alimenticios que contengan productos animales; DGAL/SDSSA/2014-823 de 10 de octubre de 2014.

