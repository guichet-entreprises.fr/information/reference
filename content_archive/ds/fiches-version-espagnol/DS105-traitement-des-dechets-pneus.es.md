﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS105" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Tratamiento de residuos: neumáticos" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="tratamiento-de-residuos-neumaticos" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/tratamiento-de-residuos-neumaticos.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="tratamiento-de-residuos-neumaticos" -->
<!-- var(translation)="Auto" -->

Tratamiento de residuos: neumáticos
===================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad de transformación de los residuos de neumáticos consiste en llevar a cabo todas las operaciones de reutilización y valorización de estos residuos.

El propósito de la operación de recuperación es permitir el uso de residuos con fines útiles y prepararlos para su reutilización.

*Para ir más allá* Artículo R. 543-138 del Código de Medio Ambiente.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad: La actividad es de carácter comercial, la CFE competente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional realiza una actividad de comprayé su actividad será tanto comercial como artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requieren cualificaciones profesionales para el profesional que desee llevar a cabo la actividad de tratamiento de residuos de neumáticos.

Sin embargo, cuando su actividad se enmarca en instalaciones clasificadas para la protección del medio ambiente, el profesional estará obligado a realizar garantías financieras y a llevar a cabo trámites específicos (véase infra "2". Garantías financieras" y "3 grados. Procedimientos y trámites de instalación").

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) ni un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) lleven a cabo la actividad de tratamiento de residuos de neumáticos, con carácter temporal y temporal. casual o permanente en Francia.

Como tal, el interesado está sujeto a los mismos requisitos que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

### c. Algunas peculiaridades de la regulación de la actividad

**Jerarquía de tratamiento de residuos**

El profesional responsable del tratamiento de los residuos de neumáticos está obligado por la política nacional de prevención y gestión de residuos. Esta política prevé una jerarquía de métodos de tratamiento de residuos con el fin de promover la llamada economía circular.

Como tal, los residuos de neumáticos deben estar sujetos a:

- Preparación para la reutilización
- Reciclaje
- métodos de desarrollo como la recuperación de energía.

*Para ir más allá* Artículos L. 541-1 y R. 543-140 del Código de Medio Ambiente.

**Garantías financieras**

El profesional está obligado a obtener garantías financieras mientras su actividad esté comprendida en las instalaciones clasificadas para la protección del medio ambiente sujeta según autorización (véase infra "3." b. Si es necesario, proceda con las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)).

Estas garantías financieras pueden dar lugar a:

- un compromiso escrito de una institución de crédito, seguro, financiamiento o fianza mutua;
- Un fondo de garantía gestionado por Ademe;
- un depósito en manos de la Caisse des dépéts et consignments;
- un compromiso escrito con una garantía independiente de una persona que posea más de la mitad del capital del operador.

Estas garantías financieras deben garantizarse durante un mínimo de dos años y deben renovarse al menos tres meses antes de su vencimiento.

Estas garantías financieras están destinadas a cubrir los riesgos asociados con los gastos para:

- La seguridad del lugar de funcionamiento del profesional;
- Intervenciones en caso de accidente o contaminación;
- reacondicionado después de su cierre.

*Para ir más allá* Artículo R. 516-1 y siguiente del Código de Medio Ambiente; decreto de 31 de mayo de 2012 por el que se establece la lista de instalaciones clasificadas sujetas a la obligación de establecer garantías financieras en virtud del artículo R. 516-1 del Código de Medio Ambiente.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Si es necesario, proceda con las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)

Es probable que la actividad de los residuos neumáticos esté comprendida en la normativa de instalaciones clasificadas para la protección del medio ambiente (ICPE).

La actividad está comprendida en el ámbito de aplicación de la partida 2791: Instalaciones de tratamiento de residuos no peligrosos, con exclusión de las instalaciones de los artículos 2720, 2760, 2771, 2780, 2781, 2782 y 2971.

El profesional está obligado a realizar:

- una solicitud de autorización si la cantidad de residuos tratados es superior o igual a 10 toneladas al día (t/d);
- informes previos si esta cantidad es inferior a 10t/d.

Es aconsejable referirse a la nomenclatura del ICPE, disponible en el[Aida](https://aida.ineris.fr/) para obtener más información.

### b. Formalidades de notificación de la empresa

En el caso de la creación de una empresa mercantil, el profesional está obligado a inscribirse en el Registro de Comercio y Sociedades (RCS).

**Autoridad competente**

El profesional debe hacer una declaración a la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

Para ello, debe proporcionar[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI le envió un recibo el mismo día indicando los documentos que faltaban en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la devolución de su expediente siempre que no se haya presentado durante los plazos mencionados anteriormente.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

