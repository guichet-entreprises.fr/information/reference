﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS089" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentación" -->
<!-- var(title)="Salón de té" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentacion" -->
<!-- var(title-short)="salon-de-te" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/alimentacion/salon-de-te.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="salon-de-te" -->
<!-- var(translation)="Auto" -->


Salón de té
===========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El salón de té está abierto al público cuya actividad es ofrecer a sus clientes refrescos y potencialmente alimentos para ser consumidos en el lugar.

**Tenga en cuenta que**

Si el establecimiento ofrece bollería en el establecimiento, se aplican disposiciones específicas. Si es así, es aconsejable consultar la tarjeta "Pastry". Del mismo modo, para conocer más sobre las normativas aplicables a la apertura de un restaurante es aconsejable consultar las tarjetas "Restaurante Tradicional" o "Comida Rápida".

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para las actividades artesanales, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el operador profesional tiene una actividad de comida rápida asociada a la fabricación de artesanías, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El profesional que desee abrir una sala de té no está sujeto a ningún requisito de diploma. Sin embargo, debe:

- Tomar un curso de preparación de instalación (SPI)
- estar inscritos en el registro mercantil o en el registro de sociedades en caso de instalación en los departamentos de la parte inferior del Rin, del Alto Rin o del Mosela.

**Curso de preparación de instalación (SPI)**

Antes del registro, el profesional debe someterse a un SPI que consta de dos partes:

- la primera parte tiene como objetivo formar al profesional en la gestión de su negocio introduciéndolo en particular en la contabilidad general y en el entorno económico, jurídico y social de una empresa artesanal;
- la segunda parte de la pasantía es acompañar al profesional después de su inscripción en el registro de oficios.

Sin embargo, el profesional puede ser excusado de realizar un SPI tan pronto como él o ella:

- se previene por un caso de fuerza mayor. Si es necesario, debe completar esta pasantía dentro de un año de su inscripción;
- completó un curso de formación en gestión de al menos treinta horas y organizado por la Cámara de Comercio sin comercio y artesanía regional, departamental o interdepartamental;
- recibió apoyo para la creación de un negocio que dura un mínimo de treinta horas entregado por una red de ayuda para establecer un negocio. Como tal, el profesional debe:- han recibido formación en gestión,
  - y estar inscritos en el directorio nacional de certificaciones profesionales;
- ha ejercido una actividad profesional que requiere al menos el mismo nivel que el proporcionado por el SPI durante al menos tres años.

*Para ir más allá* Ley No 82-1091, de 23 de diciembre de 1982, sobre la formación profesional de los artesanos; Decreto No 83-517, de 24 de junio de 1983, por el que se establecen las condiciones de aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

**Solicitud de registro en el directorio de operaciones**

**Autoridad competente**

El profesional debe solicitar a la CMA su inscripción en su sede central en el plazo de un mes a partir de su actividad. Sin embargo, la solicitud se puede hacer a más tardar un mes después del inicio de la actividad tan pronto como el profesional haya notificado al presidente de la CMA, la fecha de inicio de la CMA, a más tardar el día anterior por carta recomendada con aviso de recepción.

**Documentos de apoyo**

La lista de documentos justificativos que se enviarán a la CMA depende de la naturaleza de la actividad realizada.

**hora**

Tan pronto como el CMA presenta su archivo completo, envía un recibo para la solicitud de un archivo de creación de negocios al solicitante. Merece la pena no responder en un plazo de dos meses.

*Para ir más allá* Artículos 9 y siguientes del Decreto No 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

### b. Cualificaciones profesionales - Nacionales europeos (Prestación gratuita de servicios o establecimiento gratuito)

#### Para entrega gratuita de servicios (LPS)

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) opere como operador de tienda de té de forma temporal y ocasional en Francia.

Como tal, el nacional profesional de la UE está sujeto a los mismos requisitos profesionales que el nacional francés (véase más arriba "2. a. Cualificaciones profesionales").

#### Para un establecimiento gratuito (LE)

Un nacional de un Estado miembro de la UE o del EEE que opere una tienda de té podrá, si está legalmente establecido en dicho Estado, llevar a cabo la misma actividad en Francia. El nacional de la UE está exento de la obligación de realizar un SPI (véase más arriba "2.0). a. Curso de preparación de la instalación").

No obstante, la CMA podrá decidir someter al nacional a una prueba de aptitud o a un curso de adaptación si encuentra diferencias sustanciales entre su formación y la requerida durante el examen de sus cualificaciones profesionales. para el ejercicio de la actividad en Francia.

*Para ir más allá* Artículo 2 de la citada Ley de 23 de diciembre de 1982; Artículo 6-1 del Decreto de 24 de junio de 1983.

### d. Algunas peculiaridades de la regulación de la actividad

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

**Cumplimiento de las normas sanitarias**

El profesional que opere una oferta de salón de té o que contenga productos o productos alimenticios animales debe cumplir con las normas sanitarias que incluyen:

- Condiciones de suministro del producto
- La temperatura a la que se almacenan y congelan estos productos;
- la temperatura de los productos.

*Para ir más allá* : decreto de 21 de diciembre de 2009 relativo a las normas sanitarias aplicables a las actividades de venta al por menor, almacenamiento y transporte de productos animales y alimentos que contengan.

**Capacitación en higiene alimentaria**

El establecimiento de comida rápida debe incluir al menos una persona con capacitación de higiene alimentaria las 14 horas en su personal.

Sin embargo, los profesionales están exentos de recibir dicha formación:

- con al menos tres años de experiencia profesional con una empresa de alimentos como gerente u operador;
- uno de los diplomas enumerados en anexo del decreto de 25 de noviembre de 2011 relativo a la lista de diplomas y títulos con fines profesionales cuyos titulares se consideren que cumplen con el requisito específico de formación en higiene alimentaria adaptado a la actividad de los establecimientos de catering comercial.

*Para ir más allá* Artículo D. 233-11 del Código Rural y Pesca Marina; decreto de 5 de octubre de 2011 relativo a las especificaciones de la formación específica en higiene alimentaria adaptadas a la actividad de los establecimientos de restauración comercial.

**Transmisión musical**

El operador que desee transmitir música en su salón de té debe solicitar en línea la autorización previa a la Sociedad de Compositores y Editores de Música (Sacem) y pagar el monto de la regalía. El profesional debe registrarse y cumpliser el formulario de autorización en línea en el[Sacem]((%3Chttps://clients.sacem.fr/autorisations%3E)).

**Publicidad de precios y menciones obligatorias**

El profesional que opera un establecimiento que sirve bebidas y alimentos para ser consumidos en el lugar debe hacer los precios pagados por el consumidor en todas sus tarjetas y menús, así como fuera de Establecimiento.

Además, el profesional que opera una sala de té debe hacer visible la información sobre los alérgenos contenidos en los productos.

*Para ir más allá* : orden de 27 de marzo de 1987 relativo a la exhibición de precios en los establecimientos que sirven comidas, productos alimenticios o bebidas que se consumirán en el establecimiento.

**Reglamento sobre horarios de apertura**

La sala de té como establecimiento de comida rápida está sujeta a las disposiciones relativas a la hora de apertura y cierre de los puntos de venta de bebidas que se consumirán en el establecimiento, establecidas por decreto de la prefectura.

Para más información es aconsejable acercarse a la prefectura del lugar donde se establece la actividad.

**Visualización de zonas para fumadores**

El profesional que opera un salón de té debe cumplir con la obligación de informar de la prohibición de fumar específica para los lugares de uso colectivo.

*Para ir más allá* Artículos R. 3512-2 y siguientes del Código de Salud Pública.

**Títulos de restaurantes**

Los operadores de salas de té que trabajen como restauradores o asimilados están sujetos a las disposiciones legales relativas al uso de títulos de restaurantes.

*Para ir más allá* Decreto No 2010-1460, de 30 de noviembre de 2010, sobre las condiciones de uso del título del restaurante; Sitio web oficial de la compañía[Comisión Nacional de Títulos de Restaurantes (CNTR)](http://www.cntr.fr/V2/home.php).

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Registro de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### a. Formalidades de notificación de la empresa

**Autoridad competente**

El operador de la sala de té debe informar de su empresa, y para ello, debe hacer una declaración a la Cámara de Comercio e Industria.

**Documentos de apoyo**

El interesado debe proporcionar la documentos de apoyo dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Empresariales y Empresariales de la Cámara de Comercio e Industria envía un recibo al profesional el mismo día mencionando los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la Cámara de Comercio e Industria le indicará al solicitante a qué organizaciones se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código General Tributario (CGI).

### b. Si es necesario, registre los estatutos de la empresa

El operador de la sala de té debe, una vez que los estatutos de la empresa han sido fechados y firmados, registrarlos en la Oficina del Impuesto de Sociedades (SIE) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

