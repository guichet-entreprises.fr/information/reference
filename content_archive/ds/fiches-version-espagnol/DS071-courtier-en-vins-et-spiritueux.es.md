﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS071" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Corredor de vinos y licores" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="corredor-de-vinos-y-licores" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/corredor-de-vinos-y-licores.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="corredor-de-vinos-y-licores" -->
<!-- var(translation)="Auto" -->


Corredor de vinos y licores
===========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El corredor de vinos y bebidas espirituosas conocido como el "corredor de campaña" es un intermediario profesional cuya actividad consiste en conectar a los diversos actores en la producción de vino y vino.

En este cargo, es responsable de investigar, informar y asegurar la correcta ejecución de la transacción entre productores o vendedores de uvas, mostos, vinos y licores y operadores (comerciantes, viticultores y cooperativas).

*Para ir más allá* Artículos 1 y 5 de la Ley 49-1652, de 31 de diciembre de 1949, por la que se regula la profesión de corredor de vino conocida como "corredor de campaña".

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Dado que la actividad de un corredor de vinos y bebidas espirituosas es de carácter comercial, la CFE competente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para ser un corredor de vinos y bebidas espirituosas, el profesional debe:

- disfrutando de sus derechos civiles;
- Ser un ciudadano francés o un regular en territorio francés;
- tener una tarjeta de visita.

*Para ir más allá* Artículo 2 de la citada Ley de 31 de diciembre de 1949.

**Es bueno saber**

La Ordenanza No 2015-1682, de 17 de diciembre de 2015, por la que se simplificaban determinados regímenes de preautorización y presentación de informes para las empresas y los profesionales, que entró en vigor el 1o de enero de 2016, preveía sustituir la tarjeta de visita por una registrado en el Registro Nacional de Corredores de Vino. Sin embargo, los textos de aplicación relativos a la aplicación de esta declaración se están debatiendo actualmente.

*Para ir más allá* Artículo 3 de la Ordenanza No 2015-1682, de 17 de diciembre de 2015, por la que se simplifican determinados regímenes de preautorización y presentación de informes para empresas y profesionales; Artículo 3 de la citada Ley de 31 de diciembre de 1949.

#### Hacer una pasantía con un corredor de vino

Antes de solicitar una tarjeta de visita, el solicitante debe hacer una pasantía con un profesional.

Esta pasantía de seis meses tiene como objetivo permitir al profesional adquirir el conocimiento de:

- aspectos de la práctica de la profesión de corredor de vino y bebidas espirituosas;
- conceptos generales de la industria vitivinícola y vitivinícola, así como los acuerdos interprofesionales vigentes en la región en la que el profesional está realizando sus prácticas.

*Para ir más allá* Artículo 2 del Decreto No 2007-222, de 19 de febrero de 2007, relativo al ejercicio de la profesión de corredor de vinos y bebidas espirituosas.

#### Obtener una tarjeta de visita

Para ejercer, el interesado debe obtener una tarjeta de visita y:

- (ver infra "3 grados). a. Solicitud de la tarjeta de visita");
- presentarse a un panel de jueces presidido por un juez consular y compuesto por:- un enólogo,
  - un representante de la profesión de corredor de vinos y bebidas espirituosas,
  - presidente de la Cámara Territorial de Comercio e Industria (CCI).

El propósito de esta revisión es evaluar el conocimiento y la experiencia profesional del candidato con el fin de practicar como un corredor de vinos y bebidas espirituosas, y se centra en:

- evaluando su conocimiento enológico
- su capacidad de sabor;
- su conocimiento del derecho comercial y los contratos de corretaje.

Cuando el solicitante ha aprobado este examen, el Presidente de la CPI emite su tarjeta profesional en el plazo de un mes a partir de la decisión de la junta de revisión.

*Para ir más allá* Artículo 2 del Decreto 51-372 de 27 de marzo de 1951 por el que se regula la administración pública para la aplicación de la Ley 49-1652, de 31 de diciembre de 1949, por la que se regula la profesión de corredor de vino conocida como "corredor de campaña".

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

**En caso de ejercicio temporal y ocasional (LPS)**

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE) se ejercite temporal y ocasionalmente en Francia.

Como tal, el nacional está sujeto a los mismos requisitos que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

**En caso de ejercicio permanente (LE)**

Todo nacional de la UE o del EEE legalmente establecido que opere como agente de vinos y bebidas espirituosas podrá llevar a cabo la misma actividad en Francia de forma permanente.

Para ello, el interesado deberá:

- cumplir las siguientes condiciones:- no se le prohíba ejercer una profesión comercial o industrial,
  - no participar en una actividad incompatible con la práctica del corredor de vinos y bebidas espirituosas,
  - Estar legalmente establecido en el Estado miembro
- justificar:- haber mantenido el negocio durante dos años consecutivos,
  - poseer un certificado de competencia o un certificado de formación expedido por el Estado miembro que regule el acceso o la práctica a la profesión,
  - Cuando el Estado no regule la actividad, para poseer un certificado de competencia o un documento de formación que justifique que ha sido preparada para el ejercicio de esta actividad,
  - poseer un diploma, título o certificado para llevar a cabo la actividad de corredor de vinos y bebidas espirituosas y expedido por un tercer Estado y reconocido por un Estado miembro, y haber llevado a cabo esta actividad durante al menos tres años en ese Estado Miembro.

Una vez que el nacional cumpla estas condiciones, deberá solicitar la emisión de una tarjeta de visita (véase infra "3o. a. Solicitud de la tarjeta de visita").

*Para ir más allá* Artículo 2-2 del Decreto 51-372 de 27 de marzo de 1951, por el que se regula la administración pública para la aplicación de la Ley 49-1652, de 31 de diciembre de 1949, por la que se regula la profesión de corredor de vino conocida como "corredor de campaña".

### c. Condiciones de honorabilidad

Para ejercer, el profesional no tiene que:

- prohibido ejercer una profesión comercial o industrial o dirigir, administrar, administrar o controlar una empresa comercial o industrial;
- comprar o vender vinos y licores por cuenta propia, excepto por las necesidades familiares o de vino de su propiedad;
- licencia como distribuidor de vinos y bebidas espirituosas al por mayor o al por menor.

*Para ir más allá* Artículo 2 de la Ley 49-1652, de 31 de diciembre de 1949, por la que se regula la profesión de corredor de vino conocida como "corredor de campaña".

**Incompatibilidades**

No ser un corredor de vinos y licores:

- funcionarios y agentes del Estado o de las autoridades locales;
- empleados de fondos de la seguridad social y subsidios familiares;
- consejeros, directivos, directivos y empleados de sindicatos cooperativos de bodegas o de bodegas o sindicatos de bodegas;
- Miembros de los consejos de administración de cooperativas de crédito agrícola;
- empleados de los comerciantes de vino;
- viticultores o químicos;
- reenviadores, almacenistas o acconiers;
- bebidas, restauradores y hoteleros;
- directores, empleados o empleados de periódicos relacionados con la viticultura y el comercio de vinos y bebidas espirituosas.

*Para ir más allá* Artículo 1 del Decreto 51-372 de 27 de marzo de 1951 por el que se regula la administración pública para la aplicación de la Ley 49-1652, de 31 de diciembre de 1949, por la que se regula la profesión de corredor de vino conocida como "corredor de campaña".

### d. Algunas peculiaridades de la regulación de la actividad

**Menciones obligatorias en caso de acuerdo de venta**

Una vez que la transacción se realiza entre el productor y el comerciante, el corredor está obligado a colocar las siguientes declaraciones en la confirmación de la venta:

- identidad (nombre de nacimiento, nombre, si existe, nombre de uso);
- La dirección de su casa
- El número de pedido de su tarjeta de visita
- la dirección de la sede central de la empresa en nombre de la cual opera.

**Tenga en cuenta que**

La compensación de corretaje se pagará de este acuerdo.

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que las normas de seguridad y accesibilidad aplicables a todos los[establecimientos de recepción pública](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitud de tarjeta de visita

**Autoridad competente**

El profesional debe presentar su solicitud al Presidente de la CPI de la región en la que desea ejercer.

**Documentos de apoyo**

Su solicitud debe incluir:

- si está inscrito en el Registro Mercantil (RCS) o si se menciona como funcionario o socio de una empresa, un extracto de registro que data de menos de tres meses o, en su defecto, un documento que acredite el honor de que no sea objeto de no prohíbe el ejercicio de una profesión comercial;
- un certificado que justifica que ha realizado una pasantía con un corredor profesional (véase supra "2.0). Prácticas con un corredor de vinos"). Este documento no es necesario para el nacional de la UE;
- para el nacional de la UE, un documento que justifica que ha sido un corredor de vinos y bebidas espirituosas durante dos años consecutivos;
- En caso contrario, un certificado de competencia o un certificado de formación expedido por un Estado miembro que justifique su preparación para el ejercicio de la profesión;
- Una copia de su tarjeta de identificación
- dos fotografías de identidad recientes.

**Tiempo y procedimiento**

Una vez validado el examen de aptitud, el presidente de la CPI emite su tarjeta profesional. El profesional se coloca entonces en la lista de corredores de vino y bebidas espirituosas establecida por la red CCI.

**Tenga en cuenta que**

Si el solicitante es nacional de la UE, su tarjeta es emitida por el Presidente de la CPI en el plazo de dos meses a partir de la recepción de su solicitud completa.

Esta tarjeta firmada por el Presidente de la CPI menciona:

- La identidad del profesional, su fecha y lugar de nacimiento, su dirección;
- en su caso, su número de registro en el RCS y, en el caso de registro como persona jurídica, su nombre y la dirección de la sede de la empresa;
- La identificación de la CPI que emitió su tarjeta;
- Número de pedido y fecha de emisión.

*Para ir más allá* Decreto No 2007-222, de 19 de febrero de 2007, relativo al ejercicio de la profesión de corredor de vinos y bebidas espirituosas; Orden de 19 de febrero de 2007 relativa al ejercicio de la profesión de corredor de vinos y bebidas espirituosas; Artículos 1 y 2 de la Orden de 19 de febrero de 2007 relativos al ejercicio de la profesión de corredor de vinos y bebidas espirituosas.

### b. Registro mercantil en el Registro mercantil y de empresas (RCS)

**Autoridad competente**

El comerciante debe informar de su negocio, y para ello debe hacer una declaración con la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

El interesado debe proporcionar la[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales del CCI envía un recibo al profesional el mismo día en el que se enumeran los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo L. 123-1 del Código de Comercio; Artículo 635 del Código Tributario General.

