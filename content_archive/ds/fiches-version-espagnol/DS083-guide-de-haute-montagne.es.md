﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS083" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Guía de alta montaña" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="guia-de-alta-montana" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/guia-de-alta-montana.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="guia-de-alta-montana" -->
<!-- var(translation)="Auto" -->


Guía de alta montaña
====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El guía de montaña es un profesional que dirige y educa y educa todo tipo de montañismo y actividades relacionadas en seguridad y autonomía. Puede practicar como acompañante, profesor y entrenador con fines educativos, de ocio o deportivos. También puede actuar como asesor técnico y consultor en actividades de montaña o realizar tareas de rescate de montaña.

El guía de montaña realiza las siguientes actividades:

- conducir y acompañar a personas en excursiones o ascensos de montaña a roca, nieve, hielo y terreno mixto;
- conducir y acompañar a la gente en las excursiones de esquí, montañeses de esquí y excursiones de esquí fuera de pista;
- enseñar montañismo, escalada y esquí de turismo, montañismo de esquí y esquí fuera de pista;
- formación en prácticas de competencia en las disciplinas antes mencionadas.

El trabajo de guía de montaña se puede practicar a tiempo completo o como una ocupación estacional.

*Para ir más allá* Artículo 1 y Apéndice V del decreto de 16 de junio de 2014 relativo a la formación específica del Diploma Estatal de Montañuña - Guía de Alta Montaña.

### b. Centro competente de formalidades comerciales (CFE)

El centro de formalidades empresariales (CFE) correspondiente depende de la naturaleza de la actividad y de la forma jurídica de la empresa:

- para las profesiones liberales, la CFE competente es la Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

Para más información, es aconsejable visitar los sitios web[Urssaf](https://www.cfe.urssaf.fr/saisiepl/),[CPI en París](http://www.entreprises.cci-paris-idf.fr/web/formalites),[CPI de Estrasburgo](http://strasbourg.cci.fr/cfe/competences) Y[Servicio público](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

La persona que desea ejercer la profesión de guía de montaña debe tener la condición de educador deportivo y tener el diploma estatal de montañero - guía de montaña.

La Escuela Nacional de Esquí y Montañismo (Ensa) ofrece formación para este diploma. Para más información, es aconsejable consultar[Sitio web de Ensa](http://www.Ensa.sports.gouv.fr/index.php?option=com_content&view=article&id=516&Itemid=772).

*Para ir más allá* Artículos L. 212-1 y D. 212-68 y los siguientes artículos del Código del Deporte; decreto de 16 de junio de 2014 relativo a la formación específica del Diploma Estatal de Montañería - un guía de montaña.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

#### Para el servicio gratuito

Los nacionales de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional, siempre que se hayan referido al prefecto del departamento Isére una declaración previa de actividad.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad allí durante al menos el equivalente a un año de tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá* Artículos L. 212-7, R. 212-92 a R. 212-94 y A. 212-221 del Código del Deporte.

**Bueno saber: la diferencia sustancial**

Para la supervisión del montañismo por parte de un guía de montaña, la diferencia sustancial, que probablemente existirá entre la cualificación profesional del solicitante de registro y la cualificación profesional requerida en el territorio nacional, se aprecia en referencia a la formación del diploma de guía de montaña del certificado estatal de montañeses para:

- Conocimientos teóricos y prácticos sobre seguridad
- habilidades técnicas de seguridad.

En virtud de la ZPE, donde el prefecto cree que hay una diferencia sustancial, él o ella puede decidir someter al declarante a la totalidad o parte de la prueba de aptitud o un curso de ajuste. Esta decisión se toma tras el asesoramiento de la sección permanente de montañismo del Comité de Formación y Empleo del Consejo Superior de Deportes de Montaña remitido al Polo Nacional de Comercios de Esquí y Montañismo.

*Para ir más allá* Artículos A. 212-222, A. 212-224 y A. 212-228 del Código del Deporte.

#### Para el Establecimiento Libre

Un nacional de un Estado de la UE o del EEE puede establecerse en Francia para ejercer permanentemente si cumple una de las cuatro condiciones siguientes.

##### Si el Estado de origen regula el acceso o el ejercicio de la actividad:

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE y que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

##### Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado certificado de competencia o formación expedido por la autoridad competente de uno de estos Estados, que atestigua la disposición a llevar a cabo la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si la cualificación profesional del nacional difiere sustancialmente de la cualificación requerida en Francia que no garantizaría la seguridad de los profesionales y de terceros, puede estar obligado a someterse a un aptitud o realizar un curso de ajuste (véase infra. "medidas de compensación").

Por otra parte, si el prefecto cree que no hay diferencia sustancial o cuando se ha identificado una diferencia sustancial y el solicitante de registro ha cumplido con la prueba de aptitud, el prefecto expide un certificado de libertad al declarante. establecimiento y una tarjeta de educador deportivo profesional.

El nacional debe tener el conocimiento de la lengua francesa necesaria para ejercer su actividad en Francia, en particular para garantizar el ejercicio seguro de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá* Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

**Bueno saber: la diferencia sustancial**

Para la supervisión del montañismo por parte de un guía de montaña, la diferencia sustancial, que probablemente existirá entre la cualificación profesional del solicitante de registro y la cualificación profesional requerida en el territorio nacional, se aprecia en referencia a la formación del diploma de guía de montaña del Certificado Estatal de Montañesa, ya que incorpora:

- Conocimientos teóricos y prácticos sobre seguridad
- habilidades técnicas de seguridad.

En el contexto de la libertad de establecimiento, cuando el prefecto considera que existe una diferencia sustancial, remite el asunto a la Comisión de Reconocimiento de Cualificaciones, adjuntando al expediente el dictamen de la sección permanente. Esta decisión se toma tras el dictamen de la sección permanente de montañismo del Comité de Formación y Empleo del Consejo Superior de Deportes de Montaña, remitido al Centro Nacional de Comercios de Esquí y Montañismo,

Después de pronunciarse sobre la existencia de una diferencia sustancial, la Comisión de Reconocimiento de Cualificaciones propone, si es necesario, al prefecto someter al declarante a una prueba de aptitud o a un curso de adaptación.

Cuando el solicitante de registro ha superado la prueba de aptitud, el prefecto le expide un certificado de establecimiento libre y una tarjeta profesional de un educador deportivo.

*Para ir más allá* Artículos A. 212-222, A. 212-223 y A. 212-228, R. 212-88 a R. 212-91 del Código del Deporte.

### c. Condiciones de honorabilidad e incompatibilidad

Está prohibido ejercer como guía de montaña en Francia para personas que hayan sido condenadas por cualquier delito o por uno de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como estupefacientes o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 235-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie podrá enseñar, facilitar o supervisar una actividad física o deportiva con menores, si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier condición, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro de vacaciones y ocio, así como de grupos de jóvenes, o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá* Artículo L. 212-9 del Código del Deporte.

### d. Algunas de las peculiaridades de la regulación

#### El curso de reentrenamiento

Los titulares del Diploma Estatal de Montañería - Guía de Montaña son sometidos cada seis años a un curso de actualización con el fin de actualizar sus habilidades profesionales. El reciclaje es una condición del ejercicio de la profesión.

Está organizado por la Ensa.

*Para ir más allá* : decreto de 11 de marzo de 2015 relativo al contenido y organización del reciclaje de los titulares de diplomas de guía de montaña.

#### El entorno específico

La práctica de un guía de montaña es una actividad que se lleva a cabo en un entorno específico. Implica el cumplimiento de medidas especiales de seguridad. Por lo tanto, sólo las organizaciones bajo la tutela del deporte pueden formar a los futuros profesionales.

*Para ir más allá* Artículos L. 212-2 y R. 212-7 del Código del Deporte.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte las hojas de actividades "Formalidad de informar de una empresa comercial" o "Registro de una empresa individual en el Registro de Comercio y Empresas."

### b. Requisito de presentación de informes (con el fin de obtener la tarjeta de educador deportivo profesional)

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad al prefecto del departamento del lugar donde tenga intención de ejercer como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial](https://eaps.sports.gouv.fr).

**hora**

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

**Documentos de apoyo**

La declaración debe incluir:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos, certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si tiene una renovación de devolución, debe ponerse en contacto con:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Además, en todos los casos, la propia prefectura solicitará la liberación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

**Costo**

Gratis.

*Para ir más allá* Artículos L. 212-11, R. 212-85 y A. 212-176 y siguientes del Código del Deporte.

### c. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades puntuales (Entrega gratuita de servicios) 

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

**Autoridad competente**

La declaración previa de actividad debe dirigirse al prefecto del Departamento de Isáre.

**hora**

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- o una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- o un recibo de un estado de entrega de servicios si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- o que está llevando a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al solicitante a una prueba de aptitud (véase infra "3 grados). e. Medidas de Compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

**Documentos de apoyo**

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá* Artículos R. 212-92 y artículos subsiguientes A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### d. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades permanentes (establecimiento libre)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración previa de actividad.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También puede decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "3 e. Medidas de Compensación").

**Autoridad competente**

La declaración previa de actividad debe dirigirse al prefecto del Departamento de Isáre.

**hora**

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

**Documentos de apoyo para la primera declaración de actividad**

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o del documento de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

**Evidencia para una declaración de renovación de la actividad**

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas, de menos de un año de edad.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá* Artículos R. 212-88 a R. 212-91, A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

#### e. Medidas de compensación

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión remite la sección permanente del esquí alpino al Comité de Formación y Empleo del Consejo Superior de Deportes de Montaña después de la revisión e investigación del caso. En el mes de su remisión, emitió un aviso al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (naturaleza de las pruebas, las modalidades de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere que se lleve a cabo una medida de compensación, el solicitante de registro tiene un mes para elegir entre la prueba de aptitud y el curso de alojamiento. El prefecto entonces emite una tarjeta de visita al solicitante de registro que ha cumplido con las medidas pendientes. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- Si no requiere una medida de compensación, el prefecto expide una tarjeta profesional al interesado.

*Para ir más allá* Artículos R. 212-90-1 y R. 212-92 del Código del Deporte.

