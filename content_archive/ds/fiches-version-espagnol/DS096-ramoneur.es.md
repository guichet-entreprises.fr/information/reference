﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS096" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Deshollinador" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="deshollinador" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/deshollinador.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="deshollinador" -->
<!-- var(translation)="Auto" -->


Deshollinador
=============

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

Profesional encargado de mantener los conductos de la chimenea y limpiar chimeneas, estufas, incineradores, calderas, conductos de ventilación y dispositivos de eliminación de humo. El barrido implica varios tubos de escape: gas, madera, fuel oil, cubiertas de aceite, dispositivos de ventilación mecánica controlada (VMC), etc.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- En el caso de la creación de una empresa individual, la CFE competente es la Urssaf;
- En caso de actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- En caso de creación de una empresa comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El interesado que desee llevar a cabo el envejecimiento de un barrido de chimenea debe tener una cualificación profesional o un ejercicio bajo el control efectivo y permanente de una persona con esta cualificación.

Esta cualificación profesional consiste, por ejemplo, en:

- Certificado de Cualificación Profesional (CAP)
- Una Patente de Estudios Profesionales (BEP);
- un grado o designación igual o superior aprobado o registrado en el momento de su emisión en el [directorio nacional de certificaciones profesionales](http://www.rncp.cncp.gouv.fr/).

el [Sitio web de la Comisión Nacional de Certificación Profesional (CNCP)](http://www.cncp.gouv.fr/) propone una lista de todas las cualificaciones profesionales posibles.

En ausencia de uno de estos títulos o títulos, el interesado debe justificar una experiencia profesional efectiva de tres años en el territorio de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) adquirida como líder empresarial, trabajadores por cuenta propia o asalariados en la alineación. En este caso, se recomienda al interesado que se ponga en contacto con la Cámara de Comercio y Artesanía (CMA) para solicitar un certificado de reconocimiento de la cualificación profesional.

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

#### Para entrega gratuita de servicios (LPS)

El profesional nacional de la UE o del EEE podrá ejercer un control efectivo y permanente de la actividad de barrido de chimeneas en Francia de forma temporal y ocasional, siempre que esté legalmente establecido en uno de estos Estados para llevar a cabo la misma actividad.

Si ni la actividad ni la formación que la lleva a ella están reguladas en el Estado del Establecimiento, la persona también debe demostrar que ha sido barrendero de chimeneas en ese estado durante al menos el equivalente a dos años a tiempo completo en los últimos diez años. años antes de la actuación que quiere actuar en Francia.

*Para ir más allá* Artículo 17-1 de la Ley 96-603, de 5 de julio de 1996.

#### Para un establecimiento gratuito (LE)

Para llevar a cabo de forma permanente en Francia el control efectivo y permanente de la actividad de barrido de chimeneas, el profesional nacional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba "2 grados). a. Cualificaciones profesionales");
- poseer un certificado de competencia o certificado de formación requerido para el ejercicio de la actividad de barrido de chimeneas en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su preparación para el ejercicio de la actividad de barrido de chimeneas cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de este Actividad
- obtener un diploma, título o certificado adquirido en un tercer estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona ha sido barrendero durante tres años en el estado que ha admitido Equivalencia.

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones anteriores podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante «3»). b. Si es necesario, solicite un certificado de cualificación profesional.")

Si la persona no cumple alguna de las condiciones anteriores, la Citada Cámara de Comercios y Artesanía (CMA) podrá pedirle que realice una medida de compensación en los siguientes casos:

- si la duración de la formación certificada es al menos un año inferior a la requerida para obtener una de las cualificaciones profesionales requeridas en Francia para llevar a cabo la actividad de barrido de chimeneas;
- Si la formación recibida abarca temas sustancialmente diferentes de los cubiertos por uno de los títulos o diplomas necesarios para llevar a cabo la actividad de barrido de chimeneas en Francia;
- Si el control efectivo y permanente de la actividad de barrido de chimeneas requiere, para el ejercicio de algunas de sus competencias, una formación específica que no se imparte en el Estado miembro de origen y que abarque temas sustancialmente diferentes de los cubiertos por el certificado de competencia o designación de formación mencionado por el solicitante.

*Para ir más allá* Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998.

**Bueno saber: medidas de compensación**

La CMA, que solicita un certificado de reconocimiento de la cualificación profesional, notifica al solicitante su decisión de que realice una de las medidas de compensación. Esta Decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son imprescindibles para ejercer en Francia.

A continuación, el solicitante debe elegir entre un curso de ajuste de hasta tres años o una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final del curso de ajuste, el solicitante envía al CMA un certificado que certifica que ha completado válidamente esta pasantía, acompañado de una evaluación de la organización que lo supervisó. La CMA emite, sobre la base de este certificado, un certificado de cualificación profesional en el plazo de un mes.

La decisión de utilizar una medida de indemnización podrá ser impugnada por el interesado, que deberá presentar un recurso administrativo ante el prefecto en el plazo de dos meses a partir de la notificación de la decisión. Si su apelación es desestimada, puede iniciar una impugnación legal.

*Para ir más allá* Artículos 3 y 3-2 del Decreto 98-246, de 2 de abril de 1998; Artículo 6-1 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos para la aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

### c. Condiciones de honorabilidad

Nadie puede ejercer la profesión si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 III de la Ley 96-603, de 5 de julio de 1996.

### d. Seguro obligatorio de responsabilidad civil

El profesional debe tomar un seguro de responsabilidad civil profesional. Permite que se cubra por los daños causados a otros, ya sean causados directamente o por sus empleados, locales o equipos.

Los artesanos que participan en las obras de construcción y construcción están obligados a realizar una asistencia de responsabilidad civil de diez años.

Las referencias del contrato de seguro deben figurar en las cotizaciones y facturas del artesano.

*Para ir más allá* Artículo L. 241-1 del Código de Seguros y Artículo 22-2 de la Ley 96-603 de 5 de julio de 1996.

### e. Algunas peculiaridades de la regulación de la actividad

#### Respetar las normas de accesibilidad y seguridad

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, es aconsejable consultar el listado[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

#### Información al consumidor

Antes de realizar cualquier trabajo, la empresa debe informar al consumidor de la siguiente información:

- Tasas de trabajo por hora del TTC;
- Cómo contar el tiempo invertido
- Precios tTC para los diversos servicios de tarifa plana ofrecidos;
- Gastos de viaje, si los hubiere;
- La naturaleza gratuita o pagada de la cotización y su costo, si existe;
- cualquier otra condición de compensación.

Esta información debe mostrarse de forma visible y legible en las instalaciones de la empresa si recibe clientes. Si el servicio se ofrece en el lugar de la intervención, esta información debe ser presentada al cliente antes de cualquier trabajo en un documento escrito.

*Para ir más allá* Artículo 2 de la Orden del 2 de marzo de 1990 sobre la Publicidad de los Precios de los Servicios de Solución de Problemas, Reparación y Mantenimiento en el Sector de La Construcción y Equipamiento para el Hogar.

#### Publicidad

Todos los anuncios escritos deben incluir:

- El nombre, nombre y dirección de la empresa
- su número de registro en el registro de operaciones y empresas (RCS) o en el directorio de operaciones;
- Tasas de mano de obra TTC por hora para cada categoría de precios de servicio o unitarios;
- Gastos de viaje si la empresa va a la casa del consumidor
- Si la cotización paga o no;
- cualquier otra condición de remuneración, si la hubiera.

*Para ir más allá* Artículo 3 del auto de 2 de marzo de 1990 supra.

#### Obligación de tomar nota

El profesional debe enviar al consumidor una nota después de que el servicio haya sido completado y antes del pago del precio. Además, el consumidor ha fínquese una descarga por las piezas, artículos o aparatos sustituidos que el consumidor se ha negado a conservar.

*Para ir más allá* Artículo 5 del Auto de 2 de marzo de 1990 y Orden 83-50/A, de 3 de octubre de 1983, relativa a la publicidad de precios de todos los servicios.

#### Conducir dispositivos autopropulsados y de elevación

Los empleados de la empresa sólo pueden conducir vehículos autopropulsados y equipos de elevación (grues, carros independientes, carretillas elevadoras, etc.) si han recibido una autorización de conducción emitida por el empleador.

Para emitir esta autorización, el empleador lleva a cabo una evaluación del trabajador teniendo en cuenta:

- Una prueba de aptitud para conducir realizada por el médico ocupacional
- un control de los conocimientos y conocimientos del operador para el funcionamiento seguro de los equipos de trabajo. La obtención del Certificado de Aptitud para Conducir de forma segura (Caces) permite dar fe del control del conocimiento y el know-how del empleado. Este certificado debe ser emitido por organismos de pruebas certificados;
- un control del conocimiento del lugar y las instrucciones a seguir en el sitio o sitios de uso.

*Para ir más allá* Artículo R. 4323-56 del Código de Trabajo y auto de 2 de diciembre de 1998 relativo a la formación en el desarrollo de equipos móviles autónomos y dispositivos de carga o elevación.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Siga el curso de preparación de la instalación (SPI)

El SPI es un requisito obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

- El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente.
- Tiene una duración mínima de 30 horas.
- Viene en forma de cursos y trabajo práctico.
- Su objetivo es adquirir los conocimientos básicos esenciales en los ámbitos jurídico, fiscal, social, contable, esencial para la creación de un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

Al final de la pasantía, el participante recibe un certificado de pasantía de seguimiento que debe adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación cuesta unos 236 euros en 2016.

**Casos de exención de la pasantía**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por el SPI para las personas que, a elección:

- han ejercido una actividad profesional que requiere un nivel de conocimientos equivalente al proporcionado por las prácticas durante al menos tres años;
- conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que cubriría, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con las Francia para la gestión de una empresa de artesanía.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas. Debe acompañar su correo con documentos justificativos (copia del diploma aprobado en el nivel III, copia del certificado del máster, prueba de una actividad profesional que requiera un nivel equivalente de conocimiento) y pagar una tasa (25 euros en 2016, como indicación). No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de SPI.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, artículo 6-1 del Decreto 83-517, de 24 de junio de 1983, supra.

### b. Si es necesario, solicite un certificado de reconocimiento de la cualificación profesional

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la ACM territorialmente competente.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA le pide al solicitante que lo complete dentro de los 15 días posteriores a la presentación del expediente. Se emite un recibo tan pronto como se completa el archivo.

**Documentos de apoyo**

La carpeta debe contener:

- Solicitar un certificado de cualificación profesional
- Prueba de cualificación profesional: certificado de competencia o diploma o certificado de formación profesional;
- Prueba de la nacionalidad del solicitante
- si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE: un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia: las pruebas del ejercicio de la actividad durante tres años.

La CMA puede solicitar más información sobre su formación o la experiencia laboral del solicitante para determinar si existen diferencias sustanciales con la cualificación profesional requerida Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (Ciep) para obtener información adicional sobre el nivel de formación de un diploma o un certificado o designación extranjero, el solicitante tendrá que pagar una tasa Adicional.

**Qué saber**

Si es necesario, todos los documentos justificativos deben ser traducidos al francés por un traductor certificado.

**Tiempo de respuesta**

En el plazo de tres meses a partir de la recepción, la CMA reconoce la cualificación profesional y emite el certificado de cualificación profesional;

- decide someter al solicitante a una medida de indemnización y le notifica dicha decisión;
- se niega a expedir el certificado de cualificación profesional.

A falta de una decisión en el plazo de cuatro meses, se considera que la solicitud de certificación de cualificación profesional ha sido adquirida.

**Remedios**

Si la CMA se niega a emitir el reconocimiento de la cualificación profesional, el solicitante puede iniciar una acción judicial ante el tribunal administrativo pertinente en el plazo de dos meses a partir de la notificación de la denegación de la CMA. Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de indemnización, primero debe iniciar un recurso agraciado ante el prefecto del departamento en el que se basa la CMA, en el plazo de dos meses a partir de la notificación de la decisión. Cma. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### c. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte la "Formalidad de la presentación de informes de una empresa comercial", "Registro de negocios individuales en el Registro de Comercio y Empresas" y "Formalidades de Informes Corporativos. trabajo artesanal."

### d. Hacer una declaración previa de actividad para los nacionales de la UE que participan en una actividad única (Entrega gratuita de servicios)

Los nacionales de la UE o del EEE que deseen ejercer en Francia de forma ocasional y ad hoc están sujetos a una declaración previa a su primer servicio en suelo francés.

Esta declaración previa de actividad debe renovarse cada año si el interesado tiene la intención de volver a ejercer en Francia.

**Autoridad competente**

La declaración previa de actividad se dirige a la CMA en la jurisdicción desde la que el solicitante de registro tiene previsto llevar a cabo su desempeño.

**Recibo**

La CMA emite un recibo al solicitante de registro indicando la fecha en la que recibió el informe previo completo del expediente de actividad. Si el archivo está incompleto, el CMA notifica dentro de una quincena la lista de documentos que faltan al solicitante de registro. Emite el recibo tan pronto como se completa el archivo.

**Documentos de apoyo**

La notificación previa de las actividades debe incluir:

- La declaración fechada y firmada, en la que se menciona la información sobre el seguro civil profesional obligatorio;
- Prueba de la nacionalidad del solicitante de registro;
- prueba de cualificaciones profesionales: cualificaciones formativas, certificado de competencia expedido por la autoridad competente en el Estado del establecimiento, cualquier documento que acredite la experiencia profesional que indique su naturaleza y duración, etc.
- si procede, las pruebas de que el solicitante de registro ha estado trabajando durante al menos dos años de actividad de barrido de chimeneas a tiempo completo durante los últimos diez años;
- un certificado de establecimiento en un estado de la UE o del EEE para llevar a cabo la actividad de barrido de chimeneas;
- un certificado de no condena a la prohibición, incluso temporal, de ejercer.

Todos los documentos deben ser traducidos al francés (por un traductor certificado) si no están redactados en francés.

**hora**

En el plazo de un mes a partir de la recepción del informe previo completo del expediente de actividad, la CMA emite un certificado de cualificación profesional al declarante o notifica la necesidad de un examen posterior. En este último caso, la CMA notifica su decisión final en el plazo de dos meses a partir de la recepción del informe previo completo del expediente de actividad. En ausencia de notificación dentro de este plazo, se considera que la predeclaración ha sido adquirida y, por lo tanto, la prestación del servicio puede comenzar.

En apoyo de su decisión, la CMA podrá ponerse en contacto con la autoridad competente del estado de solución del solicitante de registro para cualquier información relativa a la legalidad de la institución y su ausencia de sanción disciplinaria o penal de naturaleza. Profesional.

**Es bueno saber**

Si la CMA encuentra una diferencia sustancial entre la cualificación profesional requerida para ejercer en Francia y la declarada por el reclamante y esta diferencia puede afectar negativamente a la salud o la seguridad del beneficiario del servicio, el solicitante de registro se invita a someterse a una prueba de aptitud. Si se niega a hacerlo, la prestación de servicios no podrá llevarse a cabo. La prueba de aptitud debe realizarse dentro de los tres meses siguientes a la presentación del expediente completo de actividad previa al informe. Si no se cumple este plazo, se considera que el reconocimiento de la cualificación profesional ha sido adquirido y la prestación de servicios puede comenzar. Para obtener más información sobre la prueba de aptitud, consulte "Bueno saber: Medidas de compensación."

**Uso**

Cualquier decisión de la CMA de someter al solicitante de registro a una prueba de aptitud puede ser objeto de un recurso administrativo ante el prefecto del departamento en el que la CMA se base en un plazo de tres meses a partir de la notificación de la decisión de la CMA. Si la apelación no tiene éxito, el solicitante de registro puede iniciar un litigio.

**Costo**

La pre-informe es gratuita. Sin embargo, si el solicitante de registro participa en una prueba de aptitud, puede que tenga que contribuir a los costos de la organización de esta medida.

*Para ir más allá* Artículo 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 y siguientes del Decreto 98-246, de 2 de abril de 1998, supra; Artículos 1 y 6 del auto de 28 de octubre de 2009 antes mencionados.

