﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS036" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseñanza" -->
<!-- var(title)="Educador deportivo" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="ensenanza" -->
<!-- var(title-short)="educador-deportivo" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/ensenanza/educador-deportivo.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="educador-deportivo" -->
<!-- var(translation)="Auto" -->

Educador deportivo
==================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El educador deportivo es un profesional que enseña, facilita y supervisa una actividad física o deportiva por una tarifa. También puede capacitar a los practicantes de una manera regular, estacional u ocasional.

Es responsable de la seguridad del público del que es responsable y garantiza el buen estado del equipo utilizado.

El educador deportivo se adapta a las características de su público, da instrucciones, corrige gestos y posturas para ayudarle a progresar.

*Para ir más allá* Artículo L. 212-1 del Código del Deporte.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- En el caso de la creación de una empresa individual, la CFE competente es la Urssaf;
- En caso de creación de una empresa comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI);
- en el caso de la creación de una sociedad civil, la CFE competente es el registro del Tribunal de Comercio (se prevé una excepción a los departamentos del Bajo Rin, del Alto Rin y del Mosela en la que el interesado debe inscribirse en el registro del tribunal de distrito ).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para ejercer como educador deportivo, la persona debe tener un diploma, un título profesional o un certificado de cualificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el[directorio nacional de certificaciones profesionales](http://www.rncp.cncp.gouv.fr/).

Entre los diplomas, títulos o certificados de titulación que cumplen estos dos criterios se encuentran:

- Certificado de Cualificación Profesional (CQP);
- El Certificado Profesional de Juventud, Educación Popular y Deporte (BPJEPS);
- Diploma Estatal de Juventud, Educación Popular y Deporte (DEJEPS).

Cada uno de ellos se otorga por una disciplina deportiva específica (esgrima, judo-jujitsu, golf, etc.) o un grupo de actividades (actividades ecuestres, todo el ocio público, automovilismo, etc.).

La lista de diplomas, títulos o certificados para ejercer como educador deportivo en Francia se elabora en el Apéndice II-1 del Artículo A212-1 del Código del Deporte. Esta lista también especifica las condiciones de práctica asociadas a cada grado (límites geográficos de ejercicio, la categoría de público que el interesado puede supervisar, la naturaleza exacta de la actividad deportiva de que se trate, la duración máxima anual ejercicio, etc.).

**Bueno saber: el educador deportivo en prácticas**

Las personas que están en el proceso de formación que conduce a un diploma, título o certificado de calificación pueden ejercer como educador deportivo en prácticas e iniciar un negocio. Sin embargo, en este caso, deben cumplir con la normativa adjunta a estos diplomas, títulos o certificados de cualificación, y estar bajo la autoridad de un tutor y haber cumplido con los requisitos previos a su situación pedagógica.

*Para ir más allá* Artículo L. 212-1 y el siguiente del Código del Deporte; Apéndice II-1 (Artículo A212-1) del Código del Deporte.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

#### Para entrega gratuita de servicios (LPS)

Los nacionales de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) legalmente establecidos en uno de estos Estados podrán llevar a cabo la misma actividad en Francia de forma temporal y ocasional, siempre que se hayan referido al prefecto de departamento cuando la entrega se lleve a cabo una declaración previa de actividad.

Si la actividad o formación que conduce a ella no está regulada en el Estado miembro de origen o en el estado del lugar de establecimiento, el nacional también deberá justificar la realización de esta actividad durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes del beneficio.

Los nacionales europeos que deseen ejercer en Francia de forma temporal u ocasional deben contar con las competencias linguísticas necesarias para llevar a cabo la actividad en Francia, en particular para garantizar la seguridad de las actividades y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá* Artículos L. 212-7 y R. 212-92 a R. 212-94 del Código del Deporte.

#### Para un establecimiento gratuito (LE)

Un nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer de forma permanente, si cumple una de las cuatro condiciones siguientes:

##### Si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:

- poseer un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- ser titular de un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber llevado a cabo esta actividad durante al menos dos años a tiempo completo en ese Estado.

##### Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:

- justificar haber estado activo en un Estado de la UE o del EEE, a tiempo completo durante al menos dos años en los últimos diez años, o, en el caso de ejercicio a tiempo parcial, justificar una actividad de duración equivalente y poseer un certificado cualificación expedida por la autoridad competente de uno de estos Estados, que atestigua la preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia;
- titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Si el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considerará satisfecho el requisito de cualificación exigido para ejercer.

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver más abajo "Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá* Artículos L. 212-7 y R. 212-88 a R. 212-90 del Código del Deporte.

### c. Condiciones de honorabilidad e incompatibilidad

Está prohibido ejercer como educador deportivo en Francia para personas que hayan sido condenadas por cualquier delito o por cualquiera de los siguientes delitos:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como estupefacientes o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 235-25 a L. 232-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie puede enseñar, facilitar o supervisar una actividad física o deportiva con menores si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier cargo, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro vacacional y de ocio, así como de grupos de jóvenes o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá* Artículo L. 212-9 del Código del Deporte.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Hacer la declaración de educador deportivo

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad ante el prefecto del departamento del lugar donde tenga la intención de ejercer su actividad como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de prácticas o al ejercicio principal, o directamente en línea en el[sitio web oficial del Ministerio de Deportes](https://eaps.sports.gouv.fr.).

**hora**

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

**Documentos de apoyo**

Los documentos justificativos necesarios son:

- Cerfa 12699*02 ;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración sobre el honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos de certificados invocados;
- Una copia de la autorización de ejercicio, la equivalencia del diploma, si procede;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si se renueva la devolución, debe adjuntar:

- Formulario Cerfa 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

La propia prefectura solicitará la comunicación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

**Costo**

Gratis

*Para ir más allá* Artículos L. 212-11, R. 212-85, A. 212-176 a A. 212-178 del Código del Deporte.

### b. Proceder con los trámites de presentación de informes de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, se recomienda consultar las hojas de actividades "Declaración de una empresa comercial" y "Registro de una empresa individual en el Registro de Comercio y Empresas."

### c. Hacer una declaración previa de actividad para los nacionales de la UE que participan en una actividad única (beneficio de servicio gratuito)

Los nacionales de la UE o del EEE legalmente establecidos en uno de estos Estados que deseen ejercer en Francia de forma temporal u ocasional deberán hacer una declaración previa de actividad antes de la primera prestación de servicios.

Si el reclamante desea realizar una nueva prestación en Francia, deberá renovarse esta declaración previa.

Con el fin de evitar daños graves a la seguridad de los beneficiarios, el prefecto podrá, durante la primera prestación, llevar a cabo un control preliminar de las cualificaciones profesionales del demandante.

**Autoridad competente**

La declaración previa de actividad debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP) del departamento donde el Departamento en el que declarando deseos de realizar su actuación.

**hora**

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- un recibo de entrega de servicio si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- que lleva a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el el prefecto somete al reclamante a una prueba de aptitud (véase infra "Bueno saber: medidas de compensación").

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

**Documentos de apoyo**

El archivo de informe previo de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-3 del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad
- Una copia del certificado de competencia o título de formación;
- Una copia de los documentos que acrediten que el solicitante de registro está legalmente establecido en el Estado miembro de la institución y que no incurre en ninguna prohibición, ni siquiera temporal, de ejercer (traducida al francés por un traductor certificado);
- en caso de que ni la actividad ni la formación que conduzcan a esta actividad estén reguladas en el Estado miembro del Establecimiento, una copia de los documentos que justifiquen que el solicitante de registro haya llevado a cabo esta actividad en dicho Estado durante al menos el equivalente a dos años tiempo completo en los últimos diez años (traducido al francés por un traductor certificado);
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - una copia de un documento que acredite la experiencia profesional adquirida en Francia.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá* Artículos R. 212-92 y artículos subsiguientes A. 212-182-2 y artículos subsiguientes y Apéndice II-12-3 del Código del Deporte.

### d. Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades permanentes (establecimiento libre)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, y que desee establecerse en Francia, deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

En caso de una diferencia sustancial con respecto a la cualificación requerida en Francia, el prefecto podrá remitir el comité de reconocimiento de cualificaciones al Ministro de Deportes para que le asesore. También pueden decidir someter al nacional a una prueba de aptitud o a un curso de adaptación (véase más adelante: "Bien saber: medidas de compensación").

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

**hora**

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada.

**Documentos de apoyo para la primera declaración de actividad**

El archivo de informe de actividad debe incluir:

- Una copia del formulario de declaración que figuran en la Lista II-12-2-a del Código del Deporte;
- Un documento de identidad con foto
- Una copia de un documento de identidad válido
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, de menos de un año de edad (traducido por un traductor certificado);
- Una copia del certificado de competencia o título de formación, acompañada de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas), traducido al francés por un traductor certificado;
- Si es así, una copia de cualquier prueba que justifique la experiencia laboral (traducida al francés por un traductor certificado);
- Si el documento de formación se ha obtenido en un tercer estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalencia en un Estado de la UE o del EEE que regula la actividad;
- uno de los tres documentos para elegir (si no, se llevará a cabo una entrevista):- Una copia de un certificado de cualificación expedido después de la formación en francés,
  - Una copia de un certificado de nivel francés expedido por una institución especializada,
  - Una copia de un documento que acredite la experiencia profesional adquirida en Francia;
- documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte (traducidos al francés por un traductor certificado) en el Estado miembro de origen.

**Evidencia para una declaración de renovación de la actividad**

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas, que data de menos de un año.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá* Artículos R. 212-88 a R. 212-91, Artículos A. 212-182 y Apéndices II-12-2-a y II-12-b del Código del Deporte.

**Bueno saber: medidas de compensación**

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de cualificaciones, puesta en comisión del Ministro encargado del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá* Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.