﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS001" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Agente inmobiliario - fideicomisario de condominios - Administrador de la propiedad" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="agente-inmobiliario-fideicomisario" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/agente-inmobiliario-fideicomisario-de-condominios-administrador.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agente-inmobiliario-fideicomisario-de-condominios-administrador" -->
<!-- var(translation)="Auto" -->


Agente inmobiliario - fideicomisario de condominios - Administrador de la propiedad
===================================================================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El agente inmobiliario, el administrador de la propiedad y el fideicomisario del condominio (conocido como "profesionales inmobiliarios") son profesionales cuyas misiones son participar en las operaciones en propiedad de otros y relacionadas con:

- comprar, vender, investigar, intercambiar, alquilar o subarrendarte, estacionalmente o no, desnudos o amueblados, de edificios construidos o no dedos;
- Compra, venta o arrendamiento de fondos comerciales;
- la venta de un rebaño (fondo de ganado) vivo o muerto;
- suscribir, comprar, vender acciones o acciones en empresas inmobiliarias o empresas de vivienda;
- La compra, venta de acciones no negociables cuando el activo social incluye un edificio o un fondo comercial;
- Gestión de la propiedad
- la venta de listas o archivos relacionados con la compra, venta, alquiler o subarrendar, desnudo sin desnudos o amueblados de edificios construidos o no, o la venta de fondos comerciales (excluidas las publicaciones por prensa);
- la celebración de cualquier contrato de disfrute de la construcción de tiempo compartido;
- desempeño de las funciones de fideicomisario de condominios.

*Para ir más allá* Artículos 1 y siguientes de la Ley 70-9, de 2 de enero de 1970, por la que se regulan las condiciones para la realización de actividades relativas a determinadas operaciones que impliquen fondos inmobiliarios y comerciales.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- En caso de actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las profesiones liberales, la CFE competente es la Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo esta actividad, el interesado debe obtener una tarjeta de visita (véase infra "2. d. Solicitud de tarjeta de visita para un ejercicio permanente (LE)).

Para ello, el profesional de bienes raíces debe:

- poseer uno de los siguientes grados:- una licenciatura (B.A. 3) sancionando estudios legales, económicos o de negocios,
  - un diploma o título inscrito en el directorio nacional de certificaciones profesionales de un nivel equivalente (B.A. 3) y estudios sancionadores de la misma naturaleza,
  - Patente del Técnico Superior (BTS) "Profesiones Inmobiliarias"
  - El diploma del Instituto de Estudios Económicos y Jurídicos aplicado a la construcción y la vivienda;
- licenciatura o diploma o título en el directorio nacional de certificaciones profesionales de un nivel equivalente (nivel IV) y sanción de estudios jurídicos, económicos o comerciales y tener 3 años de experiencia profesional en bienes raíces;
- tienen 10 años de experiencia profesional para no ejecutivos o cuatro años para ejecutivos de bienes raíces.

*Para ir más allá* Artículos 11 a 14 del Decreto 72-678 de 20 de julio de 1972.

### b. Cualificaciones profesionales - Nacionales europeos (prestación gratuita de servicios o establecimiento libre)

#### En caso de prestación gratuita de servicios (LPS)

Un profesional de bienes raíces que sea miembro de un Estado de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) podrá, siempre que esté legalmente establecido, llevar a cabo su actividad de forma temporal e informal.

Para ello, tendrá que presentar una solicitud de declaración previa ante el presidente de la Cámara de Comercio e Industria Territorial o la Cámara Departamental de Ile-de-France.

Si ni la actividad profesional ni la formación están reguladas en la UE o en el Estado del EEE, el interesado podrá trabajar en Francia de forma temporal y ocasional si justifica haber realizado dicha actividad durante al menos un año durante el 10 años antes del beneficio.

**Tenga en cuenta que**

Además, el agente nacional de práctica sin derecho a la propiedad podrá solicitar la emisión de una tarjeta de visita europea (véase infra "2". d. Tarjeta Profesional Europea (CPE) para nacionales que trabajen como agente sin sueldo inmobiliario").

*Para ir más allá* Artículo 8-1 de la Ley de 2 de enero de 1970.

#### En caso de establecimiento gratuito (LE)

Para trabajar como profesional de bienes raíces en Francia de forma permanente, el nacional de la UE o del EEE deberá cumplir una de las siguientes condiciones:

- Poseer un certificado de competencia o certificado de formación expedido por una autoridad competente del Estado miembro de origen;
- cuando la UE o el Estado del EEE no regulen esta ocupación, habiendo estado activo durante al menos un año en los diez años anteriores.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar una tarjeta de visita (ver más abajo "3 grados). d. Solicitud de tarjeta de visita para un ejercicio permanente (establecimiento libre)").

**Tenga en cuenta que**

El nacional también debe tener suficiente conocimiento de la lengua francesa.

### c. Condiciones de honorabilidad e incompatibilidad

El nacional que ha sido objeto de una condena final durante menos de diez años no puede ejercer como profesional de bienes raíces:

- por delitos, por delitos;
- una pena de al menos tres meses de prisión sin una sentencia condicional;
- y un despido de las funciones de funcionario público o ministerial.

Además, el profesional no debe haber sido sometido a:

- quiebra o medida sin práctica;
- la supresión de las funciones de administrador o agente judicial;
- prohibido ejercer como profesional durante al menos seis meses.

*Para ir más allá* Artículo 9 de la Ley de 2 de enero de 1970.

### d. Formalidades y procedimientos para el nacional de la UE

#### Solicitud de pre-informepara un ejercicio temporal e informal (LPS)

El interesado deberá presentar una solicitud mediante carta recomendada o electrónicamente al presidente de la Cámara de Comercio e Industria Territorial o a la Cámara Departamental de Ile-de-France en la jurisdicción de la que tiene previsto llevar a cabo su primera actuación.

Esta solicitud debe ir acompañada de un archivo que incluya:

- un certificado que justifique que está legalmente establecido en un Estado miembro de la UE o del EEE y que no está prohibido ejercer;
- Cuando el Estado miembro no regule la actividad, cualquier documento que justifique la experiencia laboral durante al menos un año en los diez años anteriores a la prestación;
- Un certificado de nacionalidad
- Un certificado de garantía financiera
- un certificado de seguro de responsabilidad civil profesional;
- si es así, una declaración sobre el honor de que no posee fondos, efectos o valores, que no sean los representativos de su remuneración.

*Para ir más allá* Artículo 16-6 del Decreto de 20 de julio de 1972.

#### Solicitud de tarjeta de visita para un ejercicio permanente (LE)

El nacional de un Estado miembro de la UE o del EEE debe solicitar lo que sea el Presidente de la Cámara de Comercio e Industria Territorial o la Cámara Departamental de Ile-de-France.

Esta solicitud debe ir acompañada de un archivo con los siguientes documentos justificativos:

- El formulario[Cerfa 15312*01](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15312.do) completado y firmado;
- Prueba de la aptitud profesional del solicitante
- Un certificado de garantía financiera
- un certificado de seguro de responsabilidad civil profesional;
- un extracto del Registro de Comercio y Sociedades (RCS) de menos de un mes de edad si la persona está registrada, o el doble de la demanda;
- dependiendo del caso:- certificado con el número de cuenta emitido por la entidad de crédito que abrió la cuenta,
  - o un certificado de apertura en nombre de cada principal de cuentas bancarias;
- en caso afirmativo, la declaración de honor de que el solicitante no posee ningún otro fondo, directa o indirectamente, que los que representan su remuneración;
- Boletín 2 con antecedentes penales nacionales o equivalente para los nacionales de otro Estado miembro de la UE;
- [tasas de archivo de investigación](https://www.entreprises.cci-paris-idf.fr/web/formalites/demande-carte-professionnelle-immobilier) 120 euros a pagar online.

##### hora

Si el expediente está incompleto, la autoridad competente envía al profesional una lista de los documentos que faltan dentro de una quincena de horas de su recepción. Si su expediente no se completa en un plazo de dos meses, su solicitud de tarjeta será rechazada.

*Para ir más allá* Artículos 2 y siguientes del decreto de 20 de julio de 1972.

#### Tarjeta Profesional Europea (CPE) para nacionales que trabajan como agente sin sueldo

Un nacional de un Estado miembro de la UE o del EEE que trabaje como agente inmobiliario podrá solicitar la emisión de un CPE, lo que le permitirá obtener su cualificación profesional reconocida en otro Estado de la UE.

**Tenga en cuenta que**

El procedimiento CPE puede utilizarse cuando el nacional desea operar en otro Estado de la UE de forma permanente como persona temporal y ocasional.

El CPE es válido:

- indefinidamente en el caso de un acuerdo a largo plazo (establecimiento libre);
- 18 meses para la prestación de servicios con carácter temporal.

##### Solicitud de CPE

Para solicitar un CPE, el nacional debe:

- Cree una cuenta de usuario en el[Servicio de autenticación de la Comisión Europea](https://webgate.ec.europa.eu/cas) ;
- a continuación, rellene su perfil de CPE (identidad, información de contacto...).

**Tenga en cuenta que**

También es posible crear una aplicación CPE descargando los documentos de apoyo escaneados.

##### Costo

Por cada solicitud de CPE, las autoridades del país de acogida y del país de origen pueden cobrar una tasa de revisión de los archivos, cuyo importe varía en función de la situación.

##### Es hora de solicitar un CPE para un ejercicio temporal e informal (LPS)

En el plazo de una semana, la autoridad del país de origen reconoce la recepción de la solicitud del CPE, informa si faltan documentos e informa de los costos. A continuación, las autoridades del país anfitrión revisan el caso.

Si no se requiere verificación con el país anfitrión, la autoridad del país de origen revisa la solicitud y toma una decisión final en un plazo de tres semanas.

Si se requieren verificaciones dentro del país anfitrión, la autoridad del país de origen tiene un mes para revisar la solicitud y reenviarla al país anfitrión. A continuación, el país anfitrión toma una decisión final en un plazo de tres meses.

##### Plazos para una solicitud de CPE para una actividad permanente (LE)

En el plazo de una semana, la autoridad del país de origen reconoce la recepción de la solicitud del CPE, informa si faltan documentos e informa de los costos. A continuación, el país de origen dispone de un mes para revisar la solicitud y reenviarla al país anfitrión. Este último toma la decisión final en un plazo de tres meses.

Si las autoridades del país anfitrión creen que el nivel de educación o formación o experiencia laboral está por debajo de los estándares requeridos en ese país, pueden pedir al solicitante que realice una prueba de aptitud o que complete una pasantía Adaptación.

##### Emisión de la aplicación CPE

Si se concede la solicitud para el CPE, después es posible obtener un certificado CPE en línea.

Si las autoridades del país anfitrión no tocan una decisión dentro del tiempo asignado, las cualificaciones se reconocen tácitamente y se emite un CPE. A continuación, es posible obtener un certificado CPE de su cuenta en línea.

Si se desestima la solicitud de CPE, la decisión de denegación debe estar justificada y presentar los recursos para impugnar dicha denegación.

*Para ir más allá* Artículos 16-8 y siguientes del decreto de 20 de julio de 1972.

### e. Algunas peculiaridades de la regulación de la actividad

#### Educación continua obligatoria

El profesional inmobiliario debe completar una formación continua de 14 horas al año o 42 horas durante tres años consecutivos de práctica.

La renovación de su tarjeta de visita está condicionada al cumplimiento de este requisito.

*Para ir más allá* Artículos 1 y siguientes del Decreto No 2016-173 de 18 de febrero de 2016 relativo a la formación continua de los profesionales inmobiliarios.

#### Seguro civil de trabajo

El profesional de bienes raíces debe tomar un seguro de responsabilidad civil profesional para estar cubierto contra riesgos financieros en el curso de su actividad.

Esta garantía debe incluir los datos de contacto del profesional inmobiliario, así como los de su aseguradora. El límite de este seguro civil profesional no puede ser inferior a 76.224,51 euros al año para el mismo asegurado.

*Para ir más allá* Artículo 49 del Decreto de 20 de julio de 1972; decreto de 1 de julio de 2005 por el que se establecen las condiciones de seguro y la forma del documento justificante previsto por el decreto de 20 de julio de 1972.

#### Garantía financiera

El profesional de bienes raíces, que posee la tarjeta profesional o que la solicita (ver infra "3.00). a. Solicitud de una tarjeta de visita para un año permanente"), debe obtener una garantía financiera de al menos la cantidad máxima de fondos que planea tener. Esta garantía está destinada a cubrir los créditos que han sido diseñados durante su actividad.

El titular de la tarjeta se compromete a recibir pagos o reembolsos solo dentro del monto de esa garantía.

Una vez que haya contratado esta garantía, el fondo de depósito y depósito le emite un certificado de garantía.

*Para ir más allá* Artículos 27 y siguientes del decreto de 20 de julio de 1972.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

#### Autoridad competente

El profesional inmobiliario debe proceder a la declaración de su empresa. Para ello, deberá presentar una declaración ante la Cámara de Comercio e Industria (CCI).

#### Documentos de apoyo

El interesado debe proporcionar la[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

#### hora

El Centro de Formalidades Comerciales de la CPI envía un recibo al profesional el mismo día y, si es necesario, indica los documentos que faltan en el archivo. La persona tiene entonces un período de 15 días para completarlo. Una vez que el expediente se haya completado, la CPI le dirá al solicitante a qué agencias se ha remitido su expediente.

#### Remedios

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si la CFE se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

#### Costo

El coste de esta declaración dependerá de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### b. Si es necesario, el registro de los estatutos de la empresa

El profesional inmobiliario debe, una vez que los estatutos de la empresa han sido fechados y firmados, registrarlos en la oficina del impuesto de sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

#### Autoridad competente

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

#### Documentos de apoyo

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

### c. Predeclaración de actividad para una escuela secundaria

El profesional inmobiliario también debe hacer una declaración previa de actividad para cada establecimiento o sucursal.

#### Autoridad competente

La declaración previa deberá dirigirse por el solicitante a la CPI territorial o a la cámara departamental de Ile-de-France del lugar de situación del establecimiento o sucursal secundaria.

#### Documentos de apoyo

La declaración anticipada debe indicar:

- Cuando la solicitud es hecha por una persona física, la identidad y el hogar personal del solicitante de registro;
- cuando la solicitud se realiza en nombre de una corporación:- El nombre, la forma jurídica, el asiento y el propósito de la corporación,
  - estado civil, residencia, ocupación y calidad del representante legal o legal.

Una vez realizada la declaración, el profesional recibe un recibo para la devolución.

**Tenga en cuenta que**

Cuando se realicen cambios, ya sea en la dirección de la institución o en el responsable, el profesional deberá notificarlos a la autoridad competente mencionada anteriormente.

*Para ir más allá* Artículo 3 de la Ley de 2 de enero de 1970 y artículos 2 y 8 del Decreto de 20 de julio de 1972.

