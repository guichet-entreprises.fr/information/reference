﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS052" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentación" -->
<!-- var(title)="Pastelero" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentacion" -->
<!-- var(title-short)="pastelero" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/alimentacion/pastelero.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="pastelero" -->
<!-- var(translation)="Auto" -->

Pastelero
=========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

El pastelero es un profesional que prepara y vende pasteles y postres frescos. Domina las recetas de diferentes tipos de pasta y rellenos, así como las técnicas de glaseado y cobertura.

Hace sus productos en el laboratorio: es una sala equipada con planos de trabajo, placas de cocción, hornos, cámaras de fermentación y refrigeración. Utiliza diferentes accesorios como moldes, látigos, cepillos y zócalos.

### b. Centro competente de formalidad empresarial

El centro de formalidades empresariales (CFE) depende de la naturaleza de la actividad, de la forma jurídica de la empresa y del número de empleados:

- para los artesanos y las empresas comerciales dedicadas a la actividad artesanal, siempre que no empleen a más de diez empleados, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para los artesanos y las empresas comerciales que emplean a más de diez empleados, esta es la Cámara de Comercio e Industria (CCI);
- en los departamentos del Alto y El Bajo Rin, la Cámara de Comercio de Alsacia es competente.

**Tenga en cuenta que**

En los departamentos del Bajo Rin, el Alto Rin y el Mosela, la actividad sigue siendo artesanal, independientemente del número de empleados empleados, siempre y cuando la empresa no utilice un proceso industrial. Por lo tanto, la CFE competente es la CMA o la Cámara de Comercio de Alsacia. Para más información, es aconsejable consultar el[sitio web oficial de la Cámara de Comercio de Alsacia](http://www.cm-alsace.fr/decouvrir-la-cma/lartisanat-en-alsace).

Para más información, es aconsejable consultar [Sitio web de ICC Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites/competence-cfe).

*Para ir más allá* Artículo R. 123-3 del Código de Comercio.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Sólo una persona cualificada profesionalmente o bajo el control efectivo y permanente de una persona cualificada puede llevar a cabo la actividad de chef pastelero.

Las personas que trabajan como pasteleros o que controlan el ejercicio deben:

- poseer un Certificado de Cualificación Profesional (CAP), un Certificado de Estudios Profesionales (BEP) o un diploma o un nivel superior certificado o registrado en el momento de su emisión en el [directorio nacional de certificaciones profesionales](http://www.rncp.cncp.gouv.fr/) (RNCP);
- o justificar una experiencia profesional efectiva de tres años en el territorio de la Unión Europea (UE) u otro Estado parte en el acuerdo del Espacio Económico Europeo (EEE) adquirido como gerente de empresa, trabajador por cuenta propia o empleada en el cumplimiento de su deber.

Los diplomas para el ejercicio de la industria de la pastelería incluyen:

- Especialidad CAP "pastelería"
- la licenciatura profesional "baker - pastelero."

Para obtener más información sobre cada uno de estos diplomas, es aconsejable consultar la 23 de abril de 2008 por el que se modifica el decreto de 20 de marzo de 2007 por el que se establece el certificado de pastelería y pastelería profesional 2 de julio de 2009 creando la especialidad de "pastelería" de la licenciatura profesional y estableciendo sus condiciones para la emisión.

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 1 del Decreto 98-246, de 2 de abril de 1998.

### b. Cualificaciones profesionales - Nacionales europeos

#### Para entrega gratuita de servicios (LPS)

El profesional que sea miembro de la UE o del EEE podrá trabajar en Francia de forma temporal y ocasional como chef pastelero, siempre que esté legalmente establecido en uno de estos Estados para llevar a cabo la misma actividad.

Además, cuando esta actividad o formación no esté regulada en el Estado de Establecimiento, deberá haber trabajado como chef de pastelería durante al menos dos años en los diez años anteriores a la actuación que pretende realizar. Francia.

**Tenga en cuenta que**

El profesional que cumpla estas condiciones está exento de los requisitos de registro del directorio de comercios o del registro de empresas.

*Para ir más allá* Artículo 17-1 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 2 del Decreto 98-246, de 2 de abril de 1998.

#### Para un establecimiento gratuito (LE)

Para llevar a cabo una actividad de pastelería permanente en Francia, el profesional nacional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba "2 grados). a. Cualificaciones profesionales");
- poseer un certificado de competencia o documento de formación necesario para el ejercicio de la actividad de pastelería en un Estado de la UE o del EEE cuando dicho Estado regule el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su preparación para el ejercicio de la actividad de pastelería cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de Esta actividad
- ser titular de un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona haya sido un chef de pastelería en el Estado miembro que haya admitido durante tres años Equivalencia.

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones anteriores podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante «3»). b. Si es necesario, solicite un certificado de cualificación profesional.")

Si la persona no cumple con cualquiera de las condiciones anteriores, la CMA puede pedirle que realice una medida de compensación en los siguientes casos:

- si la duración de la formación certificada es al menos un año inferior a la requerida para obtener una de las cualificaciones profesionales requeridas en Francia para llevar a cabo la actividad de chef pastelero;
- Si la formación recibida abarca temas sustancialmente diferentes de los cubiertos por uno de los títulos o diplomas requeridos para trabajar como pastelero en Francia;
- Si el control efectivo y permanente de la empresa de pastelería requiere, para el ejercicio de algunas de sus responsabilidades, una formación específica que no se imparta en el Estado miembro de origen y que abarque temas sustancialmente diferentes de los cubiertos por el certificado de competencia o designación de formación mencionado por el solicitante.

La medida de compensación consiste, a elección del solicitante, en un curso de ajuste o en una prueba de aptitud (véase la infra "3 c). Si es necesario, solicite un certificado de cualificación profesional").

*Para ir más allá* Artículos 16 y 17 de la Ley 96-603, de 5 de julio de 1996; Artículos 1 y 3 del Decreto 98-246, de 2 de abril de 1998.

### c. Condiciones de honorabilidad e incompatibilidad

Para llevar a cabo la actividad en Francia, el pastelero no debe estar bajo la influencia:

- prohibición de hacerlo (esta prohibición se aplica hasta cinco años);
- prohibición de dirigir, administrar, administrar o controlar, directa o indirectamente, cualquier empresa comercial o artesanal, granja o corporación.

*Para ir más allá* Artículo 19 de la Ley 96-603, de 5 de julio de 1996, supra; Artículo 131-6 del Código Penal; Artículo L. 653-8 del Código de Comercio.

### d. Algunas peculiaridades de la regulación de la actividad

#### Reglamento sobre la calidad del artesano y los títulos de maestro artesano y mejor trabajador en Francia

##### Artesanía

Las personas que justifican:

- ya sea una PAC, un BEP o un título certificado o registrado cuando se emitió al RNCP al menos un nivel equivalente (véase supra "2." a. Cualificaciones profesionales");
- o experiencia laboral en este comercio durante al menos tres años.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

##### El título de maestro artesano

Este título se otorga a las personas, incluidos los líderes sociales de las personas jurídicas:

- Registrado en el directorio trades;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

**Tenga en cuenta que**

Las personas que no poseen el título de máster pueden solicitar a la Comisión Regional de Cualificaciones el título de Maestro Artesano bajo dos supuestos:

- cuando están inscritos en el directorio de oficios, tienen un título al menos equivalente al máster, y justifican la gestión y los conocimientos psicopedagógicos equivalentes a los de las correspondientes unidades de valor del máster y que tienen dos años de práctica profesional;
- cuando se han inscrito en el repertorio de comercio durante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

##### El título de mejor trabajador de Francia (MOF)

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en el ámbito artesanal, comercial, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación. Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

Para obtener más información, se recomienda que consulte[web oficial del concurso "uno de los mejores trabajadores de Francia"](http://www.meilleursouvriersdefrance.info/).

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

#### Cumplir con las regulaciones de las instituciones públicas (ERP)

El chef pastelero debe cumplir con las normas de seguridad y accesibilidad para ERP. Para obtener más información, es aconsejable consultar la hoja de actividades[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

#### Cumplir con las normas sanitarias

El chef pastelero debe aplicar las normas del REGLAMENTO 852/2004, de 29 de abril de 2004, relativas a:

- Las instalaciones utilizadas para la alimentación;
- Equipo
- Desperdicio de alimentos
- Suministro de agua
- Higiene personal
- alimentos, envases y envases.

También debe respetar los principios del Punto de Control Crítico de Análisis de Peligros (HACCP) del Reglamento anterior.

El chef pastelero también debe aplicar las normas establecidas en el decreto del 21 de diciembre de 2009 que especifican las temperaturas de almacenamiento de los productos animales perecederos:

Estos incluyen:

- Condiciones de temperatura de conservación
- Procedimientos de congelación
- cómo se preparan las carnes molidas y se recibe la caza.

Por último, debe respetar las temperaturas de conservación de los productos alimenticios perecederos de origen vegetal.

*Para ir más allá* : Reglamento de 29 de abril de 2004 y Decreto de 21 de diciembre de 2009 relativo a las normas sanitarias aplicables a las actividades de venta al por menor, almacenamiento y transporte de productos animales y alimentos en contenedores; Orden de 8 de octubre de 2013 sobre normas sanitarias para la venta al por menor, almacenamiento y transporte de productos y productos alimenticios distintos de los productos animales y los alimentos en contenedores.

#### Ver precios y etiquetado de productos

El chef pastelero debe informar al consumidor, mediante marcado, etiquetado, exhibición o cualquier otro proceso apropiado, sobre los precios y las condiciones específicas de la venta y ejecución de los servicios.

*Para ir más allá* Artículo L. 112-1 del Código del Consumidor.

**Tenga en cuenta que**

El pastelero determina libremente el precio de venta de sus pasteles, pero la venta con pérdida está prohibida.

*Para ir más allá* Artículos L. 410-2, L. 442-1 y siguientes del Código del Consumidor.

#### Mostrar ingredientes reconocidos como alérgenos

El uso en la fabricación o preparación de un alimento de alimentos que cause alergias o intolerancias debe ser puesto en cuenta al consumidor final.

Esta información debe indicarse en el propio producto o cerca de él para que no haya incertidumbre sobre el producto al que se refiere.

*Para ir más allá* Artículos R. 412-12 a R. 412-16 y Apéndice IV del Código del Consumidor.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El chef pastelero debe estar registrado en el directorio de oficios y artesanías (RMA).

Para obtener más información, es aconsejable consultar la hoja de actividades "Formalidades de Reporte de Empresas Artesanales".

### b. Siga el curso de preparación de la instalación (SPI)

Antes de inscribirse en el directorio de comercios o, para los departamentos del Bajo Rin, el Alto Rin y el Mosela, en el registro de empresas, el futuro empresario debe haber seguido a un SPI.

**Tenga en cuenta que**

Si una causa de fuerza mayor lo impide, el profesional puede cumplir con su obligación en el plazo de un año a partir de su inscripción o registro.

El SPI se compone de:

- una primera parte dedicada a la introducción a la contabilidad general y la contabilidad analítica, así como a la información sobre el entorno económico, jurídico y social de las actividades artesanales y sobre la responsabilidad social y social y cuestiones ambientales;
- una segunda parte que incluye un período de soporte posterior al registro.

El propósito de las prácticas es, a través de cursos y trabajos prácticos, permitir a los futuros artesanos conocer las condiciones de su instalación, problemas de financiación, técnicas para la previsión y control de su funcionamiento, medir los conocimientos necesarias para la sostenibilidad de su negocio e informarles sobre las oportunidades de educación continua adaptadas a su situación.

La cámara de oficios, el establecimiento o el centro incautado por una solicitud de prácticas está obligada a iniciar la pasantía en un plazo de treinta días. Transcurrido este plazo, el registro del futuro empresario no puede ser denegado ni diferido, sin perjuicio de las demás obligaciones que condicionen el registro.

#### Gastos de pasantía

El futuro empresario puede ser excusado de seguir al SPI:

- si tiene un grado o título de nivel 3 certificado con un título en economía y gestión empresarial o un título de maestría de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento al menos equivalente al proporcionado por la pasantía.

#### Exención de pasantías para nacionales de la UE

Para establecerse en Francia, un profesional cualificado nacional de la UE o del EEE está exento de seguir el SPI.

La CMA podrá pedir a la CMA que se someta a una prueba de aptitud o a un curso de ajuste cuando el examen de sus cualificaciones profesionales revele que el nacional no ha recibido formación de gestión o que su formación se relaciona con materiales sustancialmente diferentes de los cubiertos por el SPI.

Sin embargo, la CMA no puede pedir al profesional que someta a tal medida de compensación:

- es decir, cuando ha estado en una actividad profesional que requiere al menos el mismo nivel de conocimiento que el SPI durante al menos tres años;
- o cuando, durante al menos dos años consecutivos, ha trabajado de forma independiente o como líder empresarial, después de recibir formación para esta actividad sancionada por un diploma, título o certificado reconocido por el Estado, miembro o parte, que lo haya expedido, o considerado plenamente válido por un organismo profesional competente;
- cuando, después de la verificación, la junta determine que los conocimientos adquiridos por el solicitante durante su experiencia profesional en un Estado miembro o parte, o en un tercer Estado, diferencia sustancial en el contenido.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, sobre la formación profesional de los artesanos; Artículos 6 y 6-1 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos para la aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

#### Condiciones de exención

La solicitud de exención debe dirigirse al Presidente de la ACM Regional.

El solicitante debe acompañar su correo:

- Copia del diploma aprobado por el grado III;
- Copia del máster
- prueba de una actividad profesional que requiere un nivel equivalente de conocimiento.

Si la exención no se responde en el plazo de un mes a partir de la recepción de la solicitud, se considerará que se concede la exención.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, sobre la formación profesional de los artesanos; Artículos 1 a 7 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos para la aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

### c. Si es necesario, solicite un certificado de cualificación profesional

El nacional de la UE o del EEE podrá solicitar un certificado de cualificación profesional para ejercer un control efectivo y permanente sobre la actividad de pastelería.

#### Autoridad competente

La solicitud debe dirigirse a la ACM regional o a la cámara regional de comercios y artesanías en la que el nacional desee ejercer.

#### Procedimiento y tiempo de respuesta

La junta emite un recibo indicando la fecha de recepción de la solicitud completa en el plazo de un mes a partir de la recepción.

En el caso de una solicitud incompleta, notifica al solicitante de la lista de documentos faltantes dentro de los 15 días posteriores a la recepción y emite el recibo tan pronto como se complete el archivo.

A falta de notificación de la decisión de la junta en el plazo de cuatro meses a partir de la recepción de la solicitud completa, se considerará que el reconocimiento de la cualificación profesional se adquiere al solicitante.

La junta podrá, por decisión motivada:

- expedir un certificado de cualificación cuando reconozca la cualificación;
- informar por escrito que se requiere una compensación.

#### Documentos de apoyo

La solicitud de certificación de cualificación profesional deberá ir acompañada de:

- diploma, formación profesional, certificado de competencia o certificado de actividad (véase supra "2". b. Cualificaciones profesionales - nacionales europeos");
- Prueba de la nacionalidad del solicitante;
- documentos no redactados en francés, una traducción certificada de acuerdo con el original por un traductor jurado o autorizado.

La CMA podrá solicitar la divulgación de información adicional sobre el nivel, la duración y el contenido de su formación para que pueda determinar la posible existencia de diferencias sustanciales con la formación francesa requerida.

También podrá solicitar pruebas por cualquier medio de experiencia profesional que pueda compensar, total o parcialmente, la diferencia sustancial.

#### Medidas de compensación

La CMA, que solicita el reconocimiento de la cualificación profesional, puede decidir someter al solicitante a una medida de compensación. Su decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son esenciales para llevar a cabo la actividad de pastelería. Sólo estas asignaturas pueden ser sometidas a la prueba de aptitud o al curso de adaptación, que no puede durar más de tres años.

El solicitante informa a la CMA de su elección para tomar un curso de ajuste o para tomar una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final de la prueba de aptitud, la junta emite un certificado de cualificación profesional al solicitante exitoso dentro de un mes.

Si el solicitante decide hacer una pasantía de alojamiento, la CMA le envía una lista de todas las organizaciones que pueden organizar esta pasantía, dentro de un mes de recibir la decisión del solicitante. En caso contrario, el reconocimiento de la cualificación profesional se considera adquirido y el consejo establece un certificado de cualificación profesional.

Al final del curso de ajuste, el solicitante envía a la junta un certificado que certifica que ha completado esta pasantía, acompañado de una evaluación de la organización que lo organizó. Sobre la base de esta certificación, el consejo de administración emite un certificado de cualificación profesional al interesado en el plazo de un mes.

**Tenga en cuenta que**

Cualquier acción judicial contra la decisión de la CMA de solicitar una indemnización va precedida, apenas inadmisibilidad, por un recurso administrativo ejercido, en el plazo de dos meses a partir de la notificación de dicha decisión, con el prefecto departamento donde la sala tiene su sede.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998, sobre la cualificación profesional necesaria para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996, relativa al desarrollo y la promoción del comercio y la artesanía; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad otro Estado parte en el Acuerdo sobre el EEE.

### d. Si es necesario, hacer una declaración en caso de preparación o venta de productos animales o animales

Cualquier operador de un establecimiento que produzca, manipule o almacene productos animales o alimenticios que contengan ingredientes animales (carne, productos lácteos, productos pesqueros, huevos, miel), destinados al consumo debe cumplir con la obligación de declarar cada uno de los establecimientos de los que es responsable, así como las actividades que se llevan a cabo allí.

La declaración deberá efectuarse antes de la apertura del establecimiento y renovarse en caso de cambio de operador, dirección o naturaleza de la actividad.

#### Autoridad competente

La declaración debe dirigirse al prefecto del departamento de aplicación del establecimiento o, en el caso de los establecimientos bajo la autoridad o tutela del Ministro de Defensa, al servicio de salud de las fuerzas armadas.

#### Documentos de apoyo 

El solicitante debe completar el formulario Cerfa 13984*03.

*Para ir más allá* Artículos R. 231-4 y R. 233-4 del Código Rural y de la pesca marina; Artículo 2 del Decreto de 28 de junio de 1994 sobre la identificación y acreditación sanitaria de establecimientos en el mercado de productos animales o animales y marcas de seguridad.

### e. Si es necesario, solicitar la aprobación sanitaria en caso de venta de alimentos a intermediarios

Los establecimientos que preparen, transformen, manipulen o almacenen productos o productos alimenticios animales en recipientes para consumo humano (leche cruda, carnes, huevos, etc.) deben obtener una aprobación sanitaria para comercializar sus productos a otros establecimientos.

#### Autoridad competente

La solicitud de acreditación debe dirigirse a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) en la ubicación del establecimiento.

#### Tiempo de respuesta

Si la solicitud de acreditación es completa y admisible, se concede una aprobación condicional por un período de tres meses al solicitante.

Durante este período, se realiza un cheque oficial. Si las conclusiones de esta visita son favorables, se concede la aprobación definitiva. De lo contrario, los puntos de incumplimiento se notifican al solicitante y la aprobación condicional podrá renovarse por otros tres meses. La duración total de la aprobación condicional no podrá exceder de seis meses.

#### Documentos de apoyo

La solicitud de acreditación sanitaria se realiza utilizando el Formulario Cerfa 13983Completado, fechado y firmado y acompañado de todos los documentos justificativos enumerados por el[nota DGAL/SDSSA/N2012-8119](http://agriculture.gouv.fr/ministere/note-de-service-dgalsdssan2012-8119-du-12062012) 12 de junio de 2012 relativo al proceso de acreditación y a la composición del expediente.

Estos documentos se refieren a la presentación de la empresa, la descripción de sus actividades y el plan de gestión de la salud.

*Para ir más allá* Artículos L. 233-2 y R. 233-1 del Código Rural y de la pesca marina; Orden de 8 de junio de 2006 sobre la acreditación sanitaria de los establecimientos que comercialen productos o productos alimenticios animales que contengan productos animales; DGAL/SDSSA/N2012-8119 de 12 de junio de 2012 relativo al procedimiento de acreditación y a la composición del fichero.

**Es bueno saber**

Es aconsejable consultar con el SDCSPP para asegurar la necesidad de solicitar la acreditación y, si es necesario, para obtener asistencia en el establecimiento del expediente de solicitud.

### f. Si es necesario, obtenga una exención del requisito de acreditación de la salud

Los establecimientos afectados por la exención son las tiendas:

- ceding cantidades limitadas de alimentos a otros puntos de venta;
- las distancias de entrega a otros puntos de venta no superan los 80 kilómetros.

Las categorías de productos abarcadas por la exención incluyen:

- Productos lácteos
- leches tratadas térmicamente
- productos hechos de cáscaras de huevo o leche cruda que han sido sometidos a un tratamiento desinfectante.

El chef pastelero puede obtener esta exención si la distancia con los establecimientos entregados no supera los 80 km (o más por decisión de la prefectura) y:

- para las leches tratadas térmicamente, si produce un máximo de 800 litros por semana cuando vende el 30% o menos de su producción total o 250 litros por semana cuando vende más del 30% de su producción total;
- para los productos lácteos y para los productos lácteos o de huevo de leche cruda que hayan sido sometidos a un tratamiento desinfectante, si produce un máximo de 250 kg por semana cuando vende el 30% o menos de su producción total o 100 kg si vende más del 30% de su producción total;

#### Autoridad competente

La solicitud de exención debe dirigirse al DDCSPP del departamento en el que se encuentra la institución.

#### Documentos de apoyo

La solicitud de exención de la subvención se realiza utilizando el formulario Cerfa 13982Completado, fechado y firmado.

#### Costo

Gratis

*Para ir más allá* Artículo L. 233-2 del Código Rural y Pesca Marina; Artículos 12, 13, Anexos 3 y 4 de la Orden de 8 de junio de 2006 sobre la Acreditación Sanitaria de establecimientos que ofrezcan productos animales o alimenticios que contengan productos animales;[DGAL/SDSSA/2014-823 de 10 de octubre de 2014.](https://info.agriculture.gouv.fr/gedei/site/bo-agri/instruction-2014-823)