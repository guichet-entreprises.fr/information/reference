﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS013" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sector financiero y jurídico" -->
<!-- var(title)="Contador" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sector-financiero-y-juridico" -->
<!-- var(title-short)="contador" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/sector-financiero-y-juridico/contador.html" -->
<!-- var(last-update)="2021-07" -->
<!-- var(url-name)="contador" -->
<!-- var(translation)="Auto" -->


Contador
========

Actualización más reciente: : <!-- begin-var(last-update) -->Julio 2021<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El contador es un profesional cuya misión es revisar y apreciar las cuentas, dar fe de la regularidad y sinceridad de las cuentas de resultados, y también mantener, centralizar, abrir, detener, monitorear, enderezar y consolidar cuenta para empresas y organizaciones.

También se permiten actividades auxiliares directamente relacionadas con el trabajo contable a los profesionales contables (consultas, realizar todos los estudios y trabajos de carácter estadístico, económico, administrativo, legal, social o fiscal y avisar a cualquier autoridad u organismo público o privado que les autorice) pero sin poder ser el foco principal de su actividad y sólo si son empresas en las que se llevan a cabo misiones contabilidad de carácter permanente u habitual o en la medida en que tales consultas, estudios, obras u opiniones estén directamente relacionados con el trabajo contable del que son responsables.

*Para ir más allá* Artículos 2 y 22 de la Ordenanza 45-2138, de 19 de septiembre de 1945, por la que se crea el Colegio de Contadores Públicos y se regula el título y la profesión de contador público.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para una profesión liberal, la CFE competente es el Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

#### Legislación nacional

La profesión de contable está reservada a cualquier persona en la Mesa de la Orden de Contadores, que justifica:

- disfrutando de sus derechos civiles;
- no haber sido condenado a una sentencia penal o correccional;
- que presenta las garantías de moralidad que el Consejo de la Orden considera necesarias;
- adtivamente:
  - licenciado en Francés en Contabilidad (DEC),
  - o tener cuarenta años de edad y justificar quince años de actividad en la ejecución de trabajos de organización o revisión contable, incluyendo al menos 5 años en funciones o misiones que impliquen el ejercicio de importantes responsabilidades de orden administrativo, financiero y contable
  - o, si es nacional de un Estado miembro de la Unión Europea (UE) o de una parte en el Acuerdo sobre el Espacio Económico Europeo (EEE), justifique un certificado de competencia o un certificado de formación reconocido como equivalente y expedido en tal Estado y, el si han completado una prueba de aptitud (véase el punto b), o, si no es nacional, justifican un diploma reconocido equivalente al Diploma de Contabilidad (CED), y han completado con éxito una prueba de aptitud.

*Para ir más allá* Artículos 3, 7*Bis*, 26 y 27 de la Ordenanza 45-2138, de 19 de septiembre de 1945; Decreto No 2012-432 de 30 de marzo de 2012 relativo a la contabilidad.

Cuando se trata del ejercicio de una actividad contable corporativa, los profesionales pueden constituir:

- Sociedades contables: entidades con personalidad jurídica, con excepción de las formas jurídicas que confieren a sus socios la condición de comerciante. Los contadores deben, directa o indirectamente, por una empresa inscrita en la Orden, poseer más de la mitad del capital y más de dos tercios de los derechos de voto. Ninguna persona o grupo de intereses, fuera de la Orden, tendrá, directamente o por persona interpuesta, una parte del capital o derechos de voto que puedan poner en peligro el ejercicio de la profesión, la independencia de los socios contables o su cumplimiento de las normas inherentes a su condición y ética. La oferta pública de valores financieros sólo está permitida para valores excluyendo el acceso, incluso diferido o condicional, al capital. Los gerentes, el presidente de la empresa simplificada, el presidente del consejo de administración o los miembros del consejo de administración deben contadores, miembros de la empresa. La sociedad que es miembro de la Orden comunica anualmente a los consejos de la Orden, desde el cual informa de la lista de sus asociados y de cualquier cambio realizado en esta lista;
- Sociedades de cartera contables: entidades cuyo objeto principal sea la tenencia de los valores de los contables colegiados, así como la participación en cualquier grupo de derecho extranjero que tenga como objeto el ejercicio de la profesión de contador colegiado. Estas empresas, inscritas en el tablero de pedidos, pueden tener actividades auxiliares directamente relacionadas con su objeto y destinadas exclusivamente a empresas o grupos en los que tengan participaciones;
- 1sociaciones de gestión y contabilidad: entidades creadas por iniciativa de cámaras territoriales de comercio e industria, cámaras de oficios o cámaras de agricultura, u organizaciones profesionales de industriales, comerciantes, artesanos, agricultores o profesionales. Ninguna asociación puede ser registrada en la junta si tiene menos de trescientos miembros en el momento de la solicitud. Los directores y directores de estas asociaciones deben justificar el incumplimiento de sus obligaciones fiscales y sociales;
- Sucursales contables: nacionales de otros Estados miembros de la UE u otros Estados partes en el Acuerdo sobre el Espacio Económico Europeo, así como entidades jurídicas constituidas de conformidad con legislación de uno de estos Estados, que tiene su sede legal, sede o institución principal en uno de estos Estados, que ejercen legalmente la profesión de contabilidad, tiene derecho a constituir, para el ejercicio de ramas que no tienen personalidad jurídica. Las sucursales no son miembros del Colegio de Contadores Públicos. Están en la pizarra. Su trabajo está bajo la responsabilidad de un contador que interactúe dentro de la rama y representante ordinal específicamente designado en esta capacidad al consejo regional del Colegio de Contadores Públicos por los constituyen;
- Sociedades profesionales de práctica: el objetivo de la práctica conjunta de varias de las profesiones de abogado, abogado en el Consejo de Estado y el Tribunal de Casación, subastador judicial, funcionario judicial, notario, administrador judicial, agente judicial, consultor de propiedad industrial, auditor y contable. La sociedad de práctica sindicación multiprofesional debe incluir al menos un miembro de cada una de las profesiones que ejerce. La empresa puede tomar cualquier forma social, excepto aquellos que dan a sus socios el estatus de un comerciante. Se rige por las normas específicas de la forma social elegida. Cualquiera que sea la forma de acción social elegida por la sociedad multiprofesional que ejerce, e incluso cuando no se ha incorporado como una empresa de ejercicio liberal, ciertas disposiciones del Título I de la Ley 90-1258 de 31 de diciembre de 1990 son Aplicable. Todo el capital y los derechos de voto están en manos de las siguientes personas:- 1) toda persona física que practique, dentro o fuera de la sociedad, una de las profesiones mencionadas en el artículo 31-3 de la Ley 90-1258, de 31 de diciembre de 1990, y que practique conjuntamente en la sociedad,
  - 2) toda persona jurídica cuyo capital total y derechos de voto sea directa o indirectamente propiedad de una o más de las personas mencionadas en el 1o,
  - 3) toda persona física o jurídica, establecida legalmente en otro Estado miembro de la Unión Europea o parte en el acuerdo sobre el Espacio Económico Europeo o en la Confederación Suiza, que realmente lleve a cabo, en uno de estos Estados, una actividad un estatuto legislativo o reglamentario o subordinado a la posesión de una cualificación nacional o internacional reconocida, cuyo ejercicio en Francia está comprendido en una de las profesiones mencionadas en el artículo 31-3 y que se ejerce conjuntamente en la sociedad ; para las personas jurídicas, todo el capital y los derechos de voto se mantienen en las condiciones previstas en el 1o o 2o;
- Sociedades de cartera financiera de las profesiones liberales: entidades constituidas entre particulares o corporaciones que ocestan una o más profesiones profesionales sujetas a estatuto legislativo o reglamentario o cuyo título está protegido o las personas 6o del artículo 6, del artículo 5, de la Ley 90-1258, de 31 de diciembre de 1990, sociedades de capital financiero para la participación de acciones o acciones de sociedades mencionadas en el artículo 1, párrafo primero, de la Ley 90-1258, de 31 Diciembre de 1990 con la finalidad del ejercicio de la misma profesión así como de la participación en cualquier grupo de Derecho extranjero con la finalidad del ejercicio de la misma profesión. Estas empresas podrán realizar cualquier otra actividad sujeta a la finalidad exclusiva de las empresas o grupos de los que posean intereses. Estas sociedades pueden constituirse en forma de sociedades de responsabilidad limitada, sociedades limitadas, acciones simplificadas o sociedades limitadas regidas por el Libro II del Código de Comercio. Más de la mitad del capital y el derecho de voto deben estar ocupados por personas de la misma profesión que las sociedades en poder de las acciones o acciones. Los gerentes, el presidente, los ejecutivos, el presidente del consejo de administración, los miembros del consejo de administración, el presidente del consejo de supervisión y los consejeros, así como al menos dos tercios de los miembros del consejo de administración o el consejo de supervisión de la corporación simplificada, debe ser elegido entre los que están autorizados a formar estas empresas.

*Para ir más allá* Artículos 7, 7*Ter*, 7*D* 45-2138 de 19 de septiembre de 1945 y los artículos 31-1 a 31-2, y los artículos 31-3 y artículos de la Ley 90-1258, de 31 de diciembre de 1990, relativa al ejercicio en forma de sociedades profesionales sujetas a estatus legislativo o reglamentario o sociedades protegidas y financieras de las profesiones.

#### Entrenamiento

Para solicitar al Colegio como contable, el candidato, que no utiliza los procedimientos mencionados en los artículos 7 *Bis*, 26 o 27 de la Ordenanza 45-2138, de 19 de septiembre de 1945, deben haber validado sucesivamente los siguientes diplomas, excepto las exenciones concedidas a los titulares de determinados títulos y diplomas:

- licenciado en contabilidad y gestión (DCG) (B.A.3);
- El grado de grado en contabilidad y gestión (DSCG) a nivel de máster (B.A. 5);
- el diploma de contabilidad (DEC) en el nivel de bac 8, después de una pasantía de tres años en una firma de contabilidad. Para este diploma, el candidato debe pasar una prueba escrita sobre la revisión legal y contractual de las cuentas, una prueba oral ante un jurado, y apoyar su tesis al final de su pasantía.

Estos diplomas también se pueden obtener a través del Procedimiento de Validación de Experiencia (VAE).

*Para ir más allá* Artículo 3 de la Ordenanza 45-2138 de 19 de septiembre de 1945, Artículos 45 y siguientes del Decreto No 2012-432 de 30 de marzo de 2012 y decreto de 13 de febrero de 2019 por el que se establecen las disposiciones relativas a la obtención del DEC por parte de la VAE.

### Costos relacionados

Hay una tarifa fija por grado y por evento. En 2018, para el DCG, el precio por evento se fija en 22 euros a la tasa de 14 eventos. En cuanto al DSCG, es de 30 euros por evento a un ritmo de 8 eventos. En cuanto al DEC, es de 50 euros a la tasa de 3 eventos. Además, se pagan algunos cursos de preparación para estos diplomas. Para obtener más información, es aconsejable acercarse a las organizaciones que las proporcionan.

El procedimiento de reconocimiento de las cualificaciones profesionales de los nacionales de la UE o del EEE es gratuito.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Nacionales de la UE: para el ejercicio temporal y casual (LPS)

La profesión de contable o parte de las actividades contables podrá ser realizada en Francia de forma temporal y ocasional por cualquier nacional de un Estado miembro de la UE o parte en el Acuerdo EEE, con sujeción a:

1) estar legalmente establecido, de forma permanente, en uno de los Estados mencionados para llevar a cabo la totalidad o parte de la actividad de contable;

2) cuando esta ocupación o la formación que la lleve a cabo no estén reguladas en el Estado de establecimiento, que haya ejercido más esta ocupación en uno o varios de los Estados mencionados durante al menos un año en los diez años anteriores a la rendimiento que pretende lograr en Francia.

La prestación de contabilidad se lleva a cabo bajo el título profesional del Estado de establecimiento cuando dicho título existe en ese Estado. Este título está indicado en la lengua oficial del Estado de Establecimiento. En los casos en que este título profesional no exista en el Estado del establecimiento, el demandante menciona su diploma o título de formación en la lengua oficial de ese Estado. La ejecución de este beneficio contable está sujeta a una declaración escrita al Consejo Superior del Colegio de Contadores Públicos antes del primer beneficio. La declaración escrita especifica la cobertura del seguro u otros medios de protección personal o colectiva con respecto a la responsabilidad profesional de este proveedor. La declaración prevista para la realización de la primera prestación en Francia de forma temporal y ocasional se repite en caso de cambio importante en los elementos de la declaración y se renueva cada año si el reclamante planea llevar a cabo esta actividad durante el año en cuestión. Tras la recepción de esta declaración, el Consejo Superior del Colegio de Contadores Públicos envía una copia al Consejo Regional del Colegio de Contadores Públicos en el que debe llevarse a cabo la provisión de conocimientos contables. Tras la recepción de esta transmisión, el consejo regional registra al solicitante de registro para el año considerado en el consejo de la Orden.

*Para ir más allá* Artículo 26-1 de la Ordenanza 45-2138 de 19 de septiembre de 1945.

#### Nacionales de la UE: para un ejercicio permanente (LE)

Cualquier nacional de un Estado miembro de la UE o parte en el Acuerdo EEE que cumpla una de las dos condiciones podrá incluirse en el cuadro de la Orden como contable, sin tener un título en contabilidad, a ningún nacional de un Estado miembro de la UE o parte en el Acuerdo EEE que cumpla una de dos condiciones:

- 1) poseer un certificado de competencia o cualificaciones de formación contemplados en el artículo 11 de la Directiva modificada 2005/36/CE del Parlamento Europeo y del Consejo, de 7 de septiembre de 2005, relativa al reconocimiento de cualificaciones profesionales que permitan el ejercicio de la profesión en un Estado miembro de la UE o parte en el Acuerdo EEE. Este certificado o título es expedido por la autoridad competente de ese Estado y sanciona la formación adquirida en la UE o en el EEE, ya sea por un tercer país, siempre que se proporcione un certificado de la autoridad competente del Estado miembro de la UE o sea parte en el acuerdo EEE que haya reconocido el título, certificado u otro título, que certifique que el titular tiene, en ese Estado, al menos tres años de trabajo a tiempo completo o de un período de tiempo parcial equivalente durante el diez Años. Los certificados de competencia o documentos de capacitación son emitidos por el estado de origen, designado de conformidad con las disposiciones legislativas, reglamentarias o administrativas de ese Estado;
- 2) haber trabajado a tiempo completo como contable durante un año o a tiempo parcial durante un período total equivalente al menos en los diez años anteriores en un Estado miembro de la UE o en una parte en el Acuerdo EEE que no regule la y que tengan uno o más certificados de competencia o credenciales de prueba de formación expedidas por otro estado que no regule esta profesión. Estos certificados de solvencia o credenciales de formación cumplen las siguientes condiciones:

  - (a) serán expedidos por la autoridad competente del Estado mencionado en el párrafo primero, designado de conformidad con las disposiciones legislativas, reglamentarias o administrativas de dicho Estado,
  - b) certificar la preparación del propietario para el ejercicio de la profesión de que se trate.

Sin embargo, la condición de una experiencia laboral de un año mencionada no es necesaria cuando el título o los documentos de formación del solicitante sancionan la formación regulada directamente orientada al ejercicio de la profesión. Contabilidad.

A menos que los conocimientos, habilidades y habilidades adquiridos durante la experiencia laboral, ya sea a tiempo completo o a tiempo parcial, o aprendizaje permanente, y que haya sido validado por una autoridad competente para este fin en un Estado miembro de la UE o parte en el Acuerdo Europeo eee o en un tercer país designado de conformidad con las disposiciones legislativas, reglamentarias o administrativas de ese Estado, es probable que esta verificación sea innecesaria, el interesado debe ser someterse a una prueba de aptitud:

- 1) cuando la formación que justifique abarque temas sustancialmente diferentes de los del programa de diplomas contables franceses;
- 2) cuando el Estado en el que haya obtenido un certificado de competencia o un documento de formación contemplado en el artículo 11 de la CE 2005/36/modificado por el Parlamento Europeo y el Consejo el 7 de septiembre de 2005 relativo al reconocimiento de las cualificaciones profesionales que utiliza o al Estado en el que ejerció la profesión no regula esta profesión ni la regula de manera sustancialmente diferente de la normativa francesa.

*Para ir más allá* Artículo 26 de la Ordenanza 45-2138 de 19 de septiembre de 1945 y artículos 97 a 99 y 103 del Decreto No 2012-432 de 30 de marzo de 2012.

En el momento de una solicitud en este sentido, ya sea para un establecimiento o para una prestación temporal y ocasional de servicios en Francia, la autoridad competente concede acceso parcial a las actividades de contabilidad cuando todas las condiciones se llenan:

- 1) el profesional está plenamente cualificado para llevar a cabo en un Estado miembro de la UE o parte en el Acuerdo EEE la actividad profesional para la que se solicita acceso parcial;
- 2) las diferencias entre la actividad profesional legalmente realizada en el Estado miembro de la UE o parte en el Acuerdo EEE y la profesión de contable en Francia son tan importantes que la aplicación de medidas de compensación equivaldría a imponer el solicitante de seguir el programa completo de educación y formación necesario en Francia para tener pleno acceso a la profesión en Francia;
- 3) el trabajo solicitado puede separarse objetivamente de otras actividades de la profesión contable en Francia, ya que puede llevarse a cabo de forma autónoma en el Estado miembro de origen.

El acceso parcial podrá denegarse si esta denegación está justificada por razones imperiosas de interés general, si es adecuada para garantizar la consecución del objetivo perseguido y si no va más allá de lo necesario para alcanzar este objetivo.

La actividad profesional se lleva a cabo bajo el título profesional del Estado de origen cuando se ha concedido acceso parcial. Los profesionales que tienen acceso parcial indican claramente a los destinatarios de los servicios el alcance de sus actividades profesionales.

Las disposiciones de esto no se aplican a los profesionales que reciben el reconocimiento automático de sus cualificaciones profesionales de conformidad con los artículos 49*Bis* y 49*Ter* 2005/36/CE modificada por el Parlamento Europeo y el Consejo, de 7 de septiembre de 2005, relativa al reconocimiento de las cualificaciones profesionales.

Las solicitudes de acceso parcial a una institución se examinan con arreglo al mismo procedimiento que las solicitudes previstas en el artículo 26 de la Orden 45-2138, de 19 de septiembre de 1945.

Los profesionales que han sido autorizados a participar parcialmente en la actividad contable no son miembros del Colegio de Contadores Públicos. Se colocan en el consejo de la Orden de acuerdo con las condiciones establecidas en la Sección II de la Ordenanza 45-2138 de 19 de septiembre de 1945. Están sujetos a las disposiciones legislativas y reglamentarias relativas a la profesión de contador público. Pagan cuotas sobre la misma base y en las mismas condiciones que los miembros de la Orden.

*Para ir más allá* Artículo 26-0 de la Ordenanza 45-2138 de 19 de septiembre de 1945 y artículos 97 a 99-1 y 103 del Decreto No 2012-432 de 30 de marzo de 2012.

### c. Condición de honorabilidad

Los profesionales de la contabilidad deben cumplir con las disposiciones legislativas y reglamentarias que rigen su profesión y los reglamentos internos del Colegio, que se establecen por decisión del Consejo Superior. En todos los casos, los profesionales de la contabilidad se responsabilpan de su trabajo y actividades. La responsabilidad propia de las sociedades miembros de la Orden y de las asociaciones de gestión y contabilidad deja la responsabilidad personal de cada contador o profesional que haya sido autorizado a realizar parcialmente la actividad experiencia contable debido al trabajo que realiza en nombre de estas empresas, sucursales o asociaciones. El trabajo y las actividades deben ir acompañados de la firma personal del contador, del empleado o profesional que haya sido autorizado a llevar a cabo parcialmente la actividad contable, así como el visado o la firma social. Los miembros de la la Orden, que, al ser socios o accionistas de una sociedad reconocida por ella, ejercen su actividad en dicha sociedad, así como los miembros de la Orden asalariados por un colega o sociedad que figure en el consejo de administración, de una sociedad multiprofesional de práctica, una sucursal o una asociación de gestión y contabilidad, y los profesionales que han sido autorizados a participar parcialmente en la actividad contable pueden llevar a cabo sus deberes o mandatos en su propio nombre y en su propio nombre. directamente encomendados por clientes o miembros. Ejercen este derecho en las condiciones de los convenios que finalmente los vinculan a esas empresas o a sus empleadores.

Los profesionales de la contabilidad están obligados por el secreto profesional en el ejercicio de sus funciones. Sin embargo, se desvinculan en los casos de información abierta contra ellos o de enjuiciamientos interpuestos contra ellos por las autoridades públicas o en acciones interpuestas ante las salas disciplinarias de la Orden.

También están sujetos a las normas de incompatibilidad. Como tal, no pueden:

- ser empleado, excepto para otro contador o una empresa que esté legalmente practicando contabilidad pública, un miembro de la Compañía Nacional de Auditores, una sucursal contable o una asociación de gestión y gestión. Contabilidad;
- Llevar a cabo otra actividad comercial o un acto de intermediación, salvo que se lleve a cabo con carácter accesorio y no pueda poner en peligro el ejercicio de la profesión o la independencia de los contables colegiados, así como el cumplimiento por éstos de las normas inherentes en su estatus y su ética. Las condiciones y límites al ejercicio de estas actividades para la realización de estos actos se establecen mediante norma profesional aprobada por decreto del 12 de marzo de 2021 del Ministerio de Economía;
- tienen el mandato de recibir, retener o emitir fondos o valores, o de renunciar. Sin embargo, como cómplice, contables, contables, contables, sucursales, asociaciones de gestión y contabilidad, empleados mencionados en los artículos 83 ter y 83 trimestre, y empresas multiprofesionales la cuenta bancaria del pedido podrá, a través de la cuenta bancaria de su cliente o miembro, proceder al cobro amistoso de sus deudas y al pago de sus deudas, para lo cual se les ha confiado un mandato, en las condiciones establecidas por decreto. La emisión de fondos puede efectuarse cuando corresponda al pago de deudas fiscales o sociales para las que se haya confiado un mandato al profesional;
- como agente de negocios, asumir una misión de representación ante los tribunales de la orden judicial o administrativa, realizar trabajos contables, contables o contables para las empresas en las que se directa o indirectamente tienen intereses sustanciales.

Por último, los profesionales de la contabilidad están sujetos a un código de ética. Este Código de ética contiene reglas relacionadas con los deberes generales, deberes con clientes o miembros, deberes de fraternidad y deberes para el Colegio.

*Para ir más allá* Artículos 12, 21, 22 de la Ordenanza 45-2138 de 19 de septiembre de 1945, Artículos 141 y siguientes del Decreto No 2012-432 de 30 de marzo de 2012 y ordenado el 25 de noviembre de 2020 relativo a la normativa interna del Colegio de Contadores Públicos.

### d. Garantía financiera

No se requiere ninguna garantía financiera.

### e. Seguro de responsabilidad civil profesional

Los profesionales de la contabilidad están obligados, si están establecidos en Francia, a justificar un contrato de seguro para garantizar la responsabilidad civil incurrida debido a todo su trabajo y actividades. El importe de las garantías de seguro consumidos no puede ser inferior, por asegurado, a 500.000 euros por siniestralidad y a 1.000.000 de euros por año de seguro. Las Partes podrán acordar disposiciones más favorables.

Cuando las consecuencias pecuniarias de la responsabilidad civil no están cubiertas por dicho contrato, están garantizadas por un contrato de seguro suscrito por el Consejo Superior de la Orden en beneficio de quien será propiedad. Los profesionales de la contabilidad participan en el pago de primas para este contrato.

*Para ir más allá* Artículo 17 de la Ordenanza 45-2138 de 19 de septiembre de 1945 y artículos 134 y artículos del Decreto No 2012-432 de 30 de marzo de 2012.

### f. Algunas peculiaridades de la regulación de la actividad

#### Visado fiscal

El profesional de contabilidad puede emitir la visa de impuestos a los clientes o miembros para que sus ingresos profesionales sujetos al impuesto sobre la renta bajo un plan real no se incrementen. Para ello, debe tener:

- Recibió una autorización del Comisionado del Gobierno al Consejo Regional del Colegio de Contadores;
- firmó un acuerdo de tres años con las autoridades fiscales.

*Para ir más allá* (7) Del artículo 158 y del artículo 1649*Cuarto* L del Código General Tributario.

#### Misión de terceros de confianza

Como parte de su declaración anual del impuesto sobre la renta, la persona puede pedir a un contador:

- Recoger documentos justificativos que se utilizarán para justificar una deducción de los ingresos globales o un crédito fiscal;
- certificar la ejecución de estas operaciones;
- teletransmitir la declaración a las autoridades fiscales.

Esta misión sólo es posible si el contador tiene un acuerdo con la administración y un contrato con su cliente que enmarca su ámbito de intervención.

*Para ir más allá* Artículo 170*Ter* Código Fiscal General y artículos 95 ZA y artículos de la Lista II al Código Tributario General.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

La declaración de la creación de una empresa es un trámite único y obligatorio para cualquier nuevo negocio. Este enfoque permite:

- registro automático en el directorio nacional de empresas y establecimientos (Sirene)
- recibo de un número de identificación (número Siret) y, en su caso, un número de IVA comunitario.

La Dirección General de Finanzas Públicas (DGFIP) ha habilitado un espacio íntegramente dedicado a los creadores de empresas en el sitio [impots.gouv.fr](https://www.impots.gouv.fr/portail/). La documentación útil es accesible desde el enlace "Crear mi empresa" (Inicio / Profesional / Crear mi empresa).

La creación de un archivo se realiza en línea a través de [guichet-entreprises.fr](https://www.guichet-entreprises.fr). El expediente cumplimentado se enviará automáticamente al organismo competente para su tramitación. En Francia, los centros de trámites comerciales (CFE) se encargan de recoger su expediente. Las EFC son los primeros interlocutores. La CFE permite realizar en el mismo lugar o de forma desmaterializada los trámites de creación, modificación relacionados con la operación o terminación de su negocio. Cada centro es responsable de las empresas cuya sede, establecimiento principal o establecimiento secundario se encuentra dentro de su jurisdicción territorial. La CFE depende de la naturaleza de la estructura en la que se desarrolle la actividad:

- para una profesión liberal independiente, la CFE competente es la de URSSAF (suscripción de un formulario P0PL);
- para una empresa comercial, la CFE competente es la de la cámara de comercio e industria (suscripción de un formulario M0);
- para una sociedad civil, la CFE competente es la del registro del tribunal mercantil (suscripción de un formulario M0);
- para una sociedad civil de los departamentos de Bah-Rhin, Haut-Rhin y Moselle, la CFE competente es la del tribunal de distrito (suscripción de un formulario M0).

**Situación de los nacionales de la UE o del EEE sujetos a retención fiscal (PAS) por ejercer una actividad temporal y ocasional (LPS) que emplea al menos a una persona afiliada a un régimen social y/o sujeta al impuesto sobre la renta en Francia y/o sujeto a IVA u otro impuesto en Francia:**

Suscripción de un Formulario 15928*02 EEO:

- ya sea de una de las EFC competentes:
  - Urssaf para una profesión liberal,
  - la Cámara de Comercio e Industria (CCI) para empresas comerciales,
  - el registro del tribunal mercantil de sociedades civiles,
  - el registro del tribunal de distrito de sociedades civiles en los departamentos de Bas-Rhin, Haut-Rhin y Moselle;
- ya sea directamente del organismo receptor competente:
  - el Centro Nacional de Empresas Extranjeras (CNFE), si la empresa emplea al menos a una persona afiliada a un régimen de seguridad social en Francia,
  - el departamento fiscal de no residentes (DINR), si la empresa no necesita designar un representante fiscal.

**Bueno saber**

El servicio de impuestos comerciales del que depende el representante fiscal sigue siendo competente cuando la empresa debe designar un representante fiscal y no es responsable del PAS.

### b. Autorización posterior al registro

Los profesionales de la contabilidad están obligados a inscribirse en el Colegio de Contadores Públicos.

*Para ir más allá* Artículos 3, 7, 7*Bis*, 7 *Ter*, 7 *D*, 26, 26-1 y 27 de la Ordenanza 45-2138 de 19 de septiembre de 1945 y los artículos 31-1 a 31-2, y los artículos 31-3 y siguientes de la Ley No 90-1258, de 31 de diciembre de 1990.

#### Contactos e información disponible

El Consejo Superior del Colegio de Contadores Públicos y los Consejos Regionales del Colegio de Contadores Públicos son las autoridades competentes para la inclusión del Colegio de Contadores Públicos y el reconocimiento de Calificaciones.

      Conseil supérieur de l’Ordre des expert-comptable

      Immeuble Le Jour

      200-2016, rue Raymond Losserand 75680 Paris cedex 14

[Sitio web del Consejo Superior de la Orden de Contadores Públicos](https://www.experts-comptables.fr/page-d-accueil)

En cuanto a la formación inicial, la CSOEC tiene varias páginas dedicadas en su sitio web:

- [Currículo](https://www.experts-comptables.fr/devenir-expert-comptable/le-cursus) ;
- [La pasantía](https://www.experts-comptables.fr/devenir-expert-comptable/le-stage).

En cuanto al reconocimiento de las cualificaciones profesionales, la CSOEC tiene en su sitio web una página[hoja informativa](https://www.experts-comptables.fr/devenir-expert-comptable/la-reconnaissance-des-qualifications) En esta área:

Los pasos y más información se pueden obtener poniéndose en contacto con el departamento de formación de la CSOEC:

- Correo electrónico:[communication@cs.experts-comptables.org](mailto:communication@cs.experts-comptables.org) ;
- o Dominique Nechelis:[dnechelis@cs.experts-comptables.org](mailto:dnechelis@cs.experts-comptables.org) ;
- o Sophie Parisot : [sparisot@cs.experts-comptables.org](mailto:sparisot@cs.experts-comptables.org).

Por último, un[formulario de contacto](https://www.experts-comptables.fr/devenir-expert-comptable/contact-formation) también está disponible en el sitio web del Consejo Superior del Colegio de Contadores Públicos.

### c. Hacer una declaración previa de actividad para los nacionales de la UE que realicen una actividad puntual

La profesión de contable o parte de las actividades contables podrá ser realizada en Francia de forma temporal y ocasional por cualquier nacional de un Estado miembro de la UE o parte en el Acuerdo EEE, con sujeción a:

- 1) estar legalmente establecido, de forma permanente, en uno de los Estados mencionados para llevar a cabo la totalidad o parte de la actividad de contable;
- 2) cuando esta ocupación o la formación que la lleve a cabo no estén reguladas en el Estado de establecimiento, que haya ejercido más esta ocupación en uno o varios de los Estados mencionados durante al menos un año en los diez años anteriores a la rendimiento que pretende lograr en Francia.

La prestación de contabilidad se lleva a cabo bajo el título profesional del Estado de establecimiento cuando dicho título existe en ese Estado. Este título está indicado en la lengua oficial del Estado de Establecimiento. En los casos en que este título profesional no exista en el Estado del establecimiento, el demandante menciona su diploma o título de formación en la lengua oficial de ese Estado. La ejecución de este beneficio contable está sujeta a una declaración escrita al Consejo Superior del Colegio de Contadores Públicos antes del primer beneficio. La declaración escrita especifica la cobertura del seguro u otros medios de protección personal o colectiva con respecto a la responsabilidad profesional de este proveedor. La declaración prevista para la realización de la primera prestación en Francia de forma temporal y ocasional se repite en caso de cambio importante en los elementos de la declaración y se renueva cada año si el reclamante planea llevar a cabo esta actividad durante el año en cuestión. Tras la recepción de esta declaración, el Consejo Superior del Colegio de Contadores Públicos envía una copia al Consejo Regional del Colegio de Contadores Públicos en el que debe llevarse a cabo la provisión de conocimientos contables. Tras la recepción de esta transmisión, el consejo regional registra al solicitante de registro para el año considerado en el consejo de la Orden.

*Para ir más allá* Artículo 26-1 de la Ordenanza 45-2138 de 19 de septiembre de 1945.

### d. Hacer un informe previo de la actividad de una escuela secundaria

En Francia, cuando un profesional o empresa tiene una o más oficinas cuya gestión se lleva a cabo in situ y permanentemente por un miembro de la Orden que ejercerá como empleado o socio de una empresa reconocida por la Orden, está registrado también debido a esta o a las oficinas que se encuentran en la mesa de circunscripción correspondiente. Esta o estas oficinas se marcan por separado.

Lo mismo ocurre cuando una asociación de gestión y contabilidad tiene una o más oficinas permanentemente abiertas a sus miembros en las que la supervisión del trabajo se lleva a cabo de forma regular y efectiva por un empleado que es miembro de la Orden, un empleado autorizado a ejercer como contable registrado como resultado de la misma mesa de circunscripciones o de los empleados previamente designados como responsables de los servicios contables de un centro de gestión con licencia y autorizados en relación con el sección 1649*Cuarto* D del Código General Tributario, ejerciendo de manera eficaz y regular dentro de dicha asociación. Este registro debe ser solicitado por la asociación de gestión y contabilidad de la Comisión Nacional de Registro. Previa notificación de la Comisión Nacional de Registro, el consejo regional del distrito en la jurisdicción de la oficina u oficinas procede inmediatamente a registrar a la asociación de gestión y contabilidad en la junta de este Montar. Esta o estas oficinas se marcan por separado. Cuando la asociación de gestión y contabilidad ya esté registrada, deberá solicitarse al consejo regional del que dependa el registro de una oficina secundaria de nueva creación. En caso de dificultad, la solicitud de registro se presenta a la Comisión Nacional de Registro.

*Para ir más allá* Artículo 118 del Decreto No 2012-432 de 30 de marzo de 2012.

Personas nacionales de otros Estados miembros de la UE u otros Estados partes en el Acuerdo EEE, así como entidades jurídicas constituidas de conformidad con la legislación de uno de estos Estados y que tengan su sede legal, su principal o su institución principal en uno de estos Estados, que ejercen legalmente la profesión de contabilidad pública, tienen derecho a establecer sucursales para el ejercicio de su profesión que no tengan la personalidad jurídica. Estas sucursales son las únicas con derecho a utilizar el término "rama contable". Las sucursales no son miembros del Colegio de Contadores Públicos. Están en la pizarra. Su trabajo está bajo la responsabilidad de un contador que interactúe dentro de la rama y representante ordinal específicamente nombrado para el consejo regional del Colegio de Contadores Públicos. Las sucursales están sujetas a legislación y reglamentos relativos a la profesión de contador público.

*Para ir más allá* Artículo 7 *D* 45-2138 de 19 de septiembre de 1945.