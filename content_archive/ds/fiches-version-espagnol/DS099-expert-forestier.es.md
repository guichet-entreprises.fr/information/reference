﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS099" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Experiencia" -->
<!-- var(title)="Experto forestal" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="experiencia" -->
<!-- var(title-short)="experto-forestal" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/experiencia/experto-forestal.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="experto-forestal" -->
<!-- var(translation)="Auto" -->


Experto forestal
================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El experto forestal es un profesional que lleva a cabo misiones de expertos en silvicultura sobre propiedades blandas y edificios.

Sus misiones pueden incluir:

- Asesoramiento, experiencia o evaluación de activos forestales en caso de compra, venta, patrimonio o disputa;
- estudios de impacto (ambiental y paisajístico)
- diagnósticos de salud de árboles y ornamentos;
- Auditoría ambiental
- gestión de cacerías y estanques.

*Para ir más allá* Artículo L. 171-1 del Código Rural y Pesca Marina.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para una profesión liberal, la CFE competente es el Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad de experto forestal el profesional debe estar inscrito en la lista de expertos forestales establecida por el comité del Consejo Nacional de Experiencia en Tierras Agrícolas y Forestales (CNEFAF).

Para ello, la persona debe cumplir las siguientes condiciones:

- justificar una práctica profesional a título personal o bajo la responsabilidad de un aprendiz:- al menos siete años como experto en bosques,
  - o al menos tres años si el profesional posee un título o diploma sancionando al menos cuatro años de educación postsecundaria en las disciplinas agrícola, agronómica, ambiental, forestal, jurídica o económica;
- no haber sido condenado por hechos contrarios al honor, la probidad o la moral en los últimos cinco años;
- no una sanción disciplinaria o administrativa por despido, retirada de la lista o retirada de la acreditación o autorización;
- no han sido golpeados con bancarrota personal.

*Para ir más allá* Artículo R. 171-10 del Código Rural y Pesca Marina.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

**Para ejercicios temporales e informales (LPS)**

El nacional de una Unión Europea (UE) o del Espacio Económico Europeo (EEE) puede utilizar su título profesional en Francia de forma temporal y casual sin estar en la lista de expertos forestales, sujeto a:

- estar legalmente establecido en uno de estos Estados para ejercer como experto forestal;
- cuando ni la profesión ni la formación estén reguladas en ese Estado, habiendo ejercido como experto forestal en ese Estado durante al menos un año en los últimos diez años;
- estar asegurado contra las consecuencias pecuniarias de su responsabilidad civil profesional.

Para ello, el nacional tendrá que solicitarlo, antes de su primer beneficio mediante declaración dirigida a la CNEFAF (véase infra "3o. a. Predeclaración de actividad para el nacional de la UE para un ejercicio temporal e informal (LPS)").

*Para ir más allá* Artículo L. 171-2 del Código Rural y Pesca Marina.

**Para un ejercicio permanente (LE)**

Todo nacional de un Estado de la UE o del EEE que esté establecido y practique legalmente la actividad de experto forestal en ese Estado podrá llevar a cabo la misma actividad en Francia de forma permanente, siempre que cumpla las mismas condiciones que el profesional francés ( c. supra "2." a. Cualificaciones profesionales").

Tendrá que solicitar al comité del CNEFAF su inclusión en la lista nacional de expertos en tierra y agricultura (véase infra "3o). b. Solicitud de inclusión en la lista de expertos forestales para un ejercicio permanente (LE)).

Si existen diferencias sustanciales entre la formación y la experiencia profesional del nacional y las requeridas en Francia, el comité podrá decidir someterlo a una medida de compensación.

*Para ir más allá* Artículos L. 171-3 y R. 717-10 del Código Rural y Pesca Marina.

### c. Condiciones de honorabilidad e incompatibilidad

**Reglas y deberes profesionales**

El experto forestal está obligado por las normas y deberes profesionales durante el ejercicio de su actividad, y se compromete a:

- Respetar la independencia necesaria para llevar a cabo la profesión;
- hacer una declaración imparcial;
- Respetar el secreto profesional
- abstenerse de cualquier práctica desleal hacia sus colegas.

*Para ir más allá* Artículo L. 171-1 y Sección R. 172-1 y siguientes del Código Rural y la pesca marina.

**Incompatibilidades**

El ejercicio de la profesión de experto forestal es incompatible con:

- una oficina de funcionarios públicos y ministeriales;
- cualquier función que pudiera menoscabar su independencia, en particular las de adquirir bienes personales o inmobiliarios de manera común para su reventa.

*Para ir más allá* Artículo L. 171-1 del Código Rural y Pesca Marina.

### d. Algunas peculiaridades de la regulación de la actividad

**Obligación de seguro**

El experto forestal está obligado a proporcionar al comité de la CNEFAF una prueba anual de la suscripción de una póliza de seguro de responsabilidad civil profesional.

*Para ir más allá* Artículo L. 171-1 párrafo 8 del Código Rural y Pesca Marina.

**Formación continua**

El experto forestal debe recibir formación continua para permanecer en la lista del Comité del CNEFAF. Se requiere presentar el certificado de seguimiento de esta educación continua cada año.

*Para ir más allá* Artículo R. 171-16 del Código Rural y Pesca Marina; la regulación interna de la[CNEFAF](http://www.cnefaf.fr).

**Sanciones disciplinarias**

Durante el transcurso de su actividad, el experto forestal está sujeto a un procedimiento de seguimiento y control por parte del Comité del CNEFAF.

Como tal, si incumple las reglas profesionales, se enfrenta a las siguientes sanciones disciplinarias:

- Culpar
- Una advertencia
- una suspensión temporal de tres meses a tres años;
- cancelación en caso de mala conducta grave.

*Para ir más allá* Artículo L. 171-1 párrafo 7 del Código Rural y Pesca Marina.

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Declaración anticipada de actividad del nacional de la UE para el ejercicio temporal e informal (LPS)

**Autoridad competente**

El experto forestal debe presentar una solicitud al CNEFAF.

**Documentos de apoyo**

El archivo de solicitud debe incluir:

- Id
- un certificado que justifique que está legalmente establecido en un Estado de la UE o del EEE;
- pruebas de que el nacional ha estado dedicado a la tierra y a la experiencia agrícola durante al menos un año en los últimos diez años;
- un certificado de seguro de responsabilidad civil.

**Qué saber**

Los documentos de apoyo deben estar escritos en francés o traducidos por un traductor certificado.

*Para ir más allá* Artículos R. 171-17- a R. 171-17-3 del Código Rural y Pesca Marina.

### b. Solicitud de inclusión en la lista de expertos forestales para un ejercicio permanente (LE)

**Autoridad competente**

El profesional que solicite el registro en la lista de expertos forestales deberá presentar su solicitud al CNEFAF por carta recomendada.

**Documentos de apoyo**

La solicitud deberá contener lo siguiente, en su caso, acompañado de su traducción al francés:

- Todas las pruebas que justifican el estado civil del solicitante;
- Una copia de sus títulos o diplomas
- prueba de experiencia laboral
- Un cv que detalla las actividades profesionales anteriores del profesional (fecha y lugar de práctica);
- una prueba o incumplimiento, un compromiso de sub-wrrite el seguro de responsabilidad profesional;
- un extracto de antecedentes penales no 3 menos de tres meses de edad o cualquier documento equivalente expedido por la autoridad competente del Estado de la UE o el EEE de menos de tres meses;
- una declaración de honor o cualquier otra evidencia de que la persona está cumpliendo con las condiciones de honor;
- en caso necesario, una declaración de la actividad prevista en forma de empresa.

**Procedimiento**

Una vez recibido el expediente, el comité dispone de tres meses para informar al nacional de su decisión de hacer una lista o no.

No obstante, en caso de diferencias sustanciales entre la formación profesional y la experiencia del nacional y las requeridas en Francia, el comité podrá someterlo a la medida de compensación de su elección, que puede ser un curso de adaptación, ser una prueba de aptitud.

El silencio guardado por un período de tres meses vale la pena aceptar la decisión.

La renovación de la solicitud está sujeta a la producción del certificado de seguro de responsabilidad profesional.

*Para ir más allá* Artículo R. 171-10 a R. 171-13 del Código de Pesca Rural y Marina.

### d. Formalidades de notificación de la empresa

**Autoridad competente**

El experto forestal debe hacer la declaración de su empresa, y para ello debe hacer una declaración con la CPI.

**Documentos de apoyo**

El interesado debe proporcionar la[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI envía un recibo al profesional el mismo día mencionando los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo.

Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### e. Si es necesario, registre los estatutos de la empresa

El experto forestal deberá, una vez que los estatutos de la empresa hayan sido fechados y firmados, registrarlos en la Oficina del Impuesto sobre Sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

