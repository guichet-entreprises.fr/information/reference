﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS066" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Peluquería en casa" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="peluqueria-en-casa" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/peluqueria-en-casa.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="peluqueria-en-casa" -->
<!-- var(translation)="Auto" -->


Peluquería en casa
==================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La peluquería en casa es un profesional que ofrece tratamientos estéticos e higiénicos del cabello, naturales o artificiales, sin instalación fija de un material pesado y sofisticado. Viaja a los hogares de las personas, a los establecimientos colectivos (casas de cura) o a su lugar de trabajo y resort.

La peluquería en casa ofrece una amplia gama de servicios: corte, alisado, plegado, coloración, tratamiento del cuero cabelludo, cuidado del cabello, etc. También puede ofrecer la venta de productos, especialmente cosméticos, a sus clientes.

*Para ir más allá* : Ley 46-1173, de 23 de mayo de 1946, por la que se regulan las condiciones de acceso a la profesión de peluquero.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Sólo una persona profesionalmente calificada puede trabajar como peluquera en casa.

Los titulares se consideran calificados profesionalmente:

- Certificado de Habilidad Profesional (CAP) "peinado";
- o un diploma o un grado de igual o nivel superior aprobado o registrado en el momento de su emisión en el [directorio nacional de certificaciones profesionales](http://www.rncp.cncp.gouv.fr/). Esto puede incluir, por ejemplo, la patente profesional (BP) "peinado".

Una persona profesional no calificada puede iniciar un negocio de peluquería en casa, pero no puede llevar a cabo la actividad por sí mismo. Debe delegar el ejercicio a una persona cualificada (salario, cónyuge o pareja de compañeros de trabajo).

**Tenga en cuenta que**

Los peluqueros para hombres que ejercen la profesión sólo de forma incidental o adicional a otra profesión no están sujetos a la obligación de cualificación profesional de ejercer con la condición de que intervengan en municipios inferiores a 2 000 habitantes.

*Para ir más allá* Artículo 3 de la Ley 46-1173, de 23 de mayo de 1946, por la que se regulan las condiciones de acceso a la profesión de peluquero y el Decreto 97-558, de 29 de mayo de 1997, relativo a las condiciones de acceso a la profesión de peluquero.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

#### Para entrega gratuita de servicios (LPS)

El profesional que sea miembro de un Estado de la Unión Europea (UE) o un Estado Parte en el Acuerdo del Espacio Económico Europeo (EEE) podrá realizar una peluquería casera en Francia de forma temporal y casual, siempre que sea legalmente establecidos en uno de estos estados para llevar a cabo esta actividad.

Si ni la actividad ni la formación que la lleva a ella están reguladas en el Estado del Establecimiento, la persona también debe demostrar que ha sido peluquero en su país en el estado en el que tiene su sede durante al menos el equivalente a dos años a tiempo completo en el en los últimos diez años antes de la actuación que quiere actuar en Francia.

El profesional que desee ejercer de forma ad hoc y ocasional en Francia está exento del requisito de registro en el directorio de comercios o en el registro de empresas. Por lo tanto, también está exento de asistir al curso de preparación de la instalación (SPI).

*Para ir más allá* Artículo 3-1 de la ley de 23 de mayo de 1946.

#### Para un establecimiento gratuito (LE)

Para desempeñar un empleo permanente en Francia, el profesional nacional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba "2 grados). a. Cualificaciones profesionales");
- han operado legalmente en un Estado de la UE o del EEE durante seis años consecutivos de forma independiente o como líder. En este caso, el ejercicio de la actividad no debe haber sido terminado por más de diez años en el momento en que la persona solicitó un certificado de cualificación profesional de la CMA (véase infra "3o). b. Si es necesario, solicitar un certificado de cualificación profesional");
- han operado legalmente en un estado de la UE o del EEE durante tres años consecutivos, ya sea de forma independiente o como líder empresarial después de recibir formación previa de al menos tres años sancionada por un certificado reconocido por el Estado (o considerado válido por un organismo profesional competente bajo una delegación estatal). El período de ejercicio se incrementa a cuatro años si la formación ha durado sólo dos años;
- han operado legalmente en un estado de la UE o del EEE durante tres años consecutivos de forma independiente y ha estado en el trabajo como empleado durante al menos cinco años. En este caso, el ejercicio de la actividad no debe haber finalizado durante más de diez años cuando el interesado solicitó un certificado de cualificación profesional a la CMA (véase infra "3o). b. Si es necesario, solicitar un certificado de cualificación profesional");
- tener un certificado de competencia o documento de formación requerido por un Estado de la UE o del EEE que regula el acceso o el ejercicio de la actividad de peluquería en su territorio. Esta certificación, que certifica un nivel de cualificación profesional equivalente o inmediatamente inferior al exigido a los nacionales franceses (véase supra "2". a. Cualificaciones profesionales"), se emite sobre la base de la formación o examen de no graduación o certificado sin formación u ocupación previa en uno de estos estados durante tres Años
- tener un certificado de competencia o un certificado de formación que certifique su preparación para la realización de la actividad de peluquería, obtenido en un estado de la UE o del EEE que no regula el acceso o ejercicio de esta actividad. Esta certificación, que certifica un nivel de cualificación profesional equivalente o inmediatamente inferior al exigido a los nacionales franceses (véase supra "2". a. Cualificaciones profesionales"), se emite sobre la base de la formación o examen de no graduación o certificado sin formación u ocupación previa en uno de estos estados durante tres Años
- tener un diploma, título o certificado que permita el ejercicio de la actividad de peluquería adquirida en un tercer estado y admitida en equivalencia por un Estado de la UE o del EEE y haber llevado a cabo esta actividad durante tres años en el estado que ha admitido Equivalencia.

El profesional que cumpla una de las condiciones antes mencionadas se considera profesionalmente cualificado para establecerse en Francia. Puede solicitar a la CMA la certificación de cualificación profesional para llevar a cabo la actividad de peluquería en casa (ver infra "3. b. Si es necesario, solicite un certificado de cualificación profesional").

Además, el nacional debe tener suficiente conocimiento de la lengua francesa para poder ejercer la profesión de peluquero en casa.

**Tenga en cuenta que**

En caso de un chequeo, el profesional cualificado dispone de cuatro meses para presentar el certificado de cualificación profesional.

*Para ir más allá* Artículos 5, 6 y 10 del Decreto de 29 de mayo de 1997 supra.

### c. Condiciones de honorabilidad e incompatibilidad

Nadie puede ejercer la profesión si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 III de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 131-6 del Código Penal y Artículo L. 653-8 del Código de Comercio.

### d. Algunas peculiaridades de la regulación de la actividad

#### Títulos profesionales

**El título de artesano**

Los líderes empresariales artesanales, sus cónyuges o asociados y sus asociados que están personalmente y generalmente involucrados en la actividad de la empresa pueden solicitar al presidente de la CMA la calidad de la artesanía.

Estas personas deben justificar, por elección:

- Un diploma de nivel V en el comercio (CAP, BEP, etc.);
- Una designación certificada de nivel V en el comercio o ocupación conexa;
- tres años de experiencia en el comercio.

Para más información, es aconsejable acercarse a la CMA territorialmente competente.

**El título de maestro artesano**

Para obtener el título de maestro artesano, el interesado (una persona física o un gerente de una empresa de artesanías) debe:

- Estar registrado en el directorio de operaciones
- Tener un máster (BM);
- justifican al menos dos años de práctica profesional.

La solicitud debe dirigirse al presidente de la ACM correspondiente.

**Qué saber**

Las personas que no poseen la WB pueden solicitar el título de Maestro Artesano en dos situaciones distintas:

- si están inscritos en el directorio de oficios, tienen un título de formación equivalente al máster, y tienen conocimientos de gestión y psicopedagógicos equivalentes a las unidades de valor de la máster y que justifiquen más de dos años de práctica profesional;
- si han estado inscritos en el repertorio de comerciodurante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

En ambos casos, el título de maestro artesano puede ser otorgado por la Comisión Regional de Cualificaciones.

Para más detalles, es aconsejable acercarse a la CMA territorialmente competente.

*Para ir más allá* Decreto 98-247 de 2 de abril de 1998.

**El título de mejor trabajador de Francia**

El título de mejor trabajador en Francia (MOF) está reservado para aquellos que han aprobado el examen llamado "uno de los mejores trabajadores de Francia". Es un diploma estatal que atestigua la adquisición de una alta cualificación en el curso de una actividad profesional.

*Para ir más allá* : [web oficial del concurso "uno de los mejores trabajadores de Francia"](http://www.meilleursouvriersdefrance.org/) ; Artículos D. 338-9 y siguientes del Código de Educación y detenidos el 27 de diciembre de 2012.

#### Tarifas publicitarias

El peluquero en casa tiene la obligación de informar al cliente, mediante marcado, etiquetado, exhibición o cualquier otro procedimiento adecuado, de los precios y las condiciones específicas de la venta y ejecución de sus servicios. Para todos los servicios prestados, se debe divulgar la lista de impuestos libres y todos los impuestos incluidos (TTC).

*Para ir más allá* Artículo L. 112-1 del Código del Consumidor.

#### Obligación de emitir una factura al cliente

En el caso de que los servicios de peluquería y estética se realicen en casa (o en cualquier otro lugar de alojamiento del cliente), el profesional deberá establecer una tarjeta de triple copia.

La factura debe incluir:

- Identificación del cliente, la empresa y el empleado involucrado;
- El rendimiento o los servicios prestados
- Cómo pagar
- Detalles del monto del beneficio
- la firma del cliente y del profesional.

*Para ir más allá* Artículo 12-3 del Acuerdo del Grupo Nacional sobre Peluquería y Profesiones Conexas de 10 de julio de 2006.

#### Uso de productos nocivos

Los productos que contengan ácido tioglicólico, sus sales o ésteres, con una concentración de entre el 8% y el 11%, deben manipularse con cuidado al usarlos, especialmente cuando se utilizan para rizar, relajar o ondular el cabello. . Sólo las personas titulares de un certificado profesional, un certificado de máster, un título equivalente o cuya capacidad profesional haya sido validada por la Comisión Nacional de Peluquería podrán utilizar estos productos.

*Para ir más allá* Decreto 98-848, de 21 de septiembre de 1998, por el que se establecen las condiciones para el uso profesional de productos que contengan ácido tioglicólico, sus sales o ésteres.

#### Regulación de una actividad andante

Como profesional que no practica el sedentarismo, el peluquero en casa está sujeto a condiciones de ejercicio adicionales. Por ejemplo, debe obtener la tarjeta del artesano viajero.

Para obtener más información, es aconsejable consultar el listado[Comerciante de viaje artesanal](https://www.guichet-entreprises.fr/fr/activites-reglementees/negoce-et-commerce-de-biens/commercant-artisan-ambulant/).

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Siga el curso de preparación de la instalación (SPI)

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

- El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente.
- Tiene una duración mínima de 30 horas.
- Viene en forma de cursos y trabajo práctico.
- Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

Al final de la pasantía, el participante recibe un certificado de pasantía de seguimiento que debe adjuntarse a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación cuesta unos 236 euros en 2016.

**Casos de exención de la pasantía**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que:

- han trabajado durante al menos tres años exigiendo un nivel de conocimientos equivalente al proporcionado por las prácticas;
- tener conocimientos adquiridos en un Estado de la UE o del EEE o un tercer país durante una experiencia profesional que cubra, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con las Francia para la gestión de una empresa de artesanía.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas. Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiere un nivel equivalente de conocimiento.

Además, el interesado debe pagar una tasa variable (25 euros en 2016). No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, y artículo 6-1 del Decreto 83-517, de 24 de junio de 1983.

### b. Si es necesario, solicite un certificado de cualificación profesional

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la ACM territorialmente competente, es decir, en la que el interesado desea ejercer.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA le pide al solicitante que lo complete dentro de los 15 días posteriores a la presentación del expediente. Se emite un recibo tan pronto como se completa el archivo.

**Documentos de apoyo**

La carpeta debe contener:

- Solicitar un certificado de cualificación profesional
- Prueba de cualificación profesional: certificado de competencia o diploma o certificado de formación profesional;
- Prueba de la nacionalidad del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (Ciep) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**Qué saber**

Si es necesario, todos los documentos justificativos deben ser traducidos al francés por un traductor certificado.

**Tiempo de respuesta**

Dentro de los tres meses siguientes a la recepción, la CMA:

- reconoce la cualificación profesional y emite la certificación de cualificación profesional;
- decide someter al solicitante a una medida de indemnización y le notifica dicha decisión;
- se niega a expedir el certificado de cualificación profesional.

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Medidas de compensación**

La CMA podrá someter al solicitante a una medida de compensación consistente en una prueba de adaptación o aptitud. La medida de compensación es necesaria en una de las siguientes situaciones:

- si la duración de la formación es al menos un año menor que la requerida para obtener uno de los diplomas o títulos necesarios para trabajar como peluquero en casa (véase más adelante "2. a. Cualificaciones profesionales");
- si la capacitación recibida cubre temas sustancialmente diferentes de los cubiertos por uno de los diplomas o títulos requeridos para trabajar como peluquero en casa (véase infra "2o. a. Cualificaciones profesionales");
- Si el ejercicio de la actividad de peluquería en el hogar (o el control efectivo y permanente de esta actividad) requiere, para el ejercicio de algunas de sus responsabilidades, una formación específica que no se imparte en el Estado miembro de origen y que se ocupe de sujetos sustancialmente diferentes de los cubiertos por el certificado de competencia o designación de formación mencionado por el solicitante.

Antes de solicitar una indemnización, la CMA verifica si los conocimientos adquiridos por el solicitante durante su experiencia profesional en un Estado de la UE, el EEE o un estado de un tercer país en términos de duración o contenido.

**Costo**

Gratis. Sin embargo, si el nacional va a someterse a una medida de compensación, tendrá que pagar una tasa por la investigación del caso.

**Remedios**

Si la CMA se niega a emitir el reconocimiento de la cualificación profesional, el solicitante podrá iniciar, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, una impugnación legal ante el tribunal administrativo pertinente. Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de indemnización, primero debe iniciar un recurso agraciado ante el prefecto del departamento en el que se basa la CMA, en el plazo de dos meses a partir de la notificación de la decisión. Cma. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; decreto de 28 de octubre de 2009 en virtud de los decretos de 29 de mayo de 1997 y 2 de abril de 1998 relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad Europea u otro Estado parte en el Acuerdo sobre el Espacio Económico Europeo.

