﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS019" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Teñido no industrial" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="tenido-no-industrial" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/tenido-no-industrial.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="tenido-no-industrial" -->
<!-- var(translation)="Auto" -->


Teñido no industrial
====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El teñido no industrial es una actividad cuya misión es lavar y teñir telas. Esta actividad se asocia a menudo con la lavandería, la lavandería y planchar la ropa de los clientes.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para las actividades artesanales, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El profesional que desee abrir un establecimiento de teñido no industrial deberá:

- Tomar un curso de preparación de instalación (SPI)
- en función de la naturaleza de su actividad, regístrese en el Registro mercantil y de artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

#### Preparación para la instalación

Antes de registrarse, el profesional debe completar un curso de preparación de la instalación (SPI). El SPI es un requisito obligatorio para cualquier persona que solicite el registro en el repertorio de oficios y artesanías.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para la creación de un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo una educación en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado de un Estado miembro de la Unión Europea (UE) u otro Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) está exento del SPI si justifica con la CMA una cualificación en la gestión le confiere un nivel de conocimiento equivalente al proporcionado por la pasantía.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que:

- han trabajado durante al menos tres años exigiendo un nivel de conocimientos equivalente al proporcionado por las prácticas;
- tener conocimientos adquiridos en un Estado de la UE o del EEE o un tercer país durante una experiencia profesional que cubra, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con las Francia para la gestión de una empresa de artesanía.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982; Artículo 6-1 del Decreto 83-517 de 24 de junio de 1983.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

No se prevé que el nacional de un Estado miembro de la UE o del EEE lleve a cabo un teñido no industrial de forma temporal, casual o permanente en Francia.

Como tal, el nacional profesional de la UE está sujeto a los mismos requisitos profesionales que el nacional francés (véase más arriba "3o. Procedimientos y trámites de instalación").

### c. Condiciones de honorabilidad

Nadie puede participar en el negocio de la muerte si ha sido objeto de:

- una condena penal o una prohibición de dirigir, administrar o administrar un negocio comercial o artesanal o una corporación;
- penalización adicional por prohibir el uso de la actividad profesional.

*Para ir más allá* Artículo 131-6 del Código Penal.

### d. Algunas peculiaridades de la regulación de la actividad

#### Reglamento sobre la calidad del artesano y los títulos de maestro artesano y mejor trabajador en Francia

El profesional de la fábrica de teñido puede valerse de la calidad del artesano, el título de maestro artesano o el título de mejor trabajador de Francia (MOF).

**Artesanía**

Para reclamar la condición de artesano, la persona debe justificar:

- una PAC, un BEP o un título certificado o registrado cuando fue emitido al Directorio Nacional de Certificaciones Profesionales (RNCP) de al menos un nivel equivalente (véase supra "2." a. Cualificaciones profesionales");
- experiencia profesional en este comercio durante al menos tres años.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de maestro artesano**

Este título se otorga a las personas, incluidos los líderes sociales de las personas jurídicas:

- Registrado en el directorio trades;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

**Tenga en cuenta que**

Las personas que no poseen el título de máster pueden solicitar a la Comisión Regional de Cualificaciones el título de Maestro Artesano bajo dos supuestos:

- cuando están inscritos en el directorio de oficios, tienen un título al menos equivalente al máster, y justifican la gestión y los conocimientos psicopedagógicos equivalentes a los de las correspondientes unidades de valor del máster y que tienen dos años de práctica profesional;
- cuando se han inscrito en el repertorio de comercio durante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de mejor trabajador de Francia (MOF)**

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en el ámbito artesanal, comercial, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación. Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

Para obtener más información, se recomienda que consulte[sitio web oficial](http://www.meilleursouvriersdefrance.info/) "uno de los mejores trabajadores de Francia."

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

#### Información y precio de los servicios

El profesional que se dedica a la actividad de teñido no industrial debe informar a los consumidores de los precios cobrados por cada servicio ofrecido. Por lo tanto, deberá hacer visible y legible a sus clientes, en el escaparate o fallando en la entrada del establecimiento, así como en el exterior, las tarifas cobradas (TTC) así como la naturaleza de los siguientes servicios:

- Lavandería:- hoja blanca,
  - hoja de colores,
  - camisa de hombre,
  - peso lavado y sin secar, por mínimo de 4 kg por kilogramo;
- para la limpieza en seco:- pantalones de hombre y mujer,
  - Chaqueta
  - Falda
  - Vestido
  - abrigo o impermeable.

*Para ir más allá* : orden de 27 de marzo de 1987 relativo a la publicidad de precios de los servicios en el sector de la lavandería y la tintorería.

#### Regulación de riesgos y protección del medio ambiente

La actividad de teñido no industrial puede considerarse una instalación clasificada para la protección del medio ambiente (ICPE) y, como tal, están comprendidas en las regulaciones específicas de estas instalaciones, debido a los peligros o inconvenientes entorno o orden público.

Así, el profesional tendrá que comprobar si su actividad está comprendida en instalaciones clasificadas consultando la nomenclatura de las ICPEs disponibles en el[Sitio web de Aida](https://aida.ineris.fr/liste_documents/1/18028/1) y, si es necesario, proceder a:

- en caso de actividad de lavandería, lavandería lavandería lavandería excluyendo la limpieza en seco:- un registro si la capacidad de lavado es de más de cinco toneladas por día,
  - una declaración si la capacidad de lavandería es superior a 500 kg por día pero menor o igual a cinco toneladas por día;
- Si se utilizan disolventes:- una solicitud de autorización si la capacidad de las máquinas es superior a 50 kg,
  - un chequeo periódico si la capacidad de las máquinas es superior a 0,5 kg y es inferior o igual a 50 kg;
- Si la actividad se basa en los beneficios de los tintes, impresiones, imprimaciones, recubrimientos, blanqueo y lavado de materiales textiles, el profesional tendrá que solicitar la autorización si la cantidad de fibra y tela que se puede procesar más de una tonelada al día y una declaración si es superior a 50 kg por día, pero menor o igual a una tonelada por día.

*Para ir más allá* Artículo R. 511-9 del Código Ambiental y su[Anexos](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=241B87CE2555E6349D1C8DC16D6ECBDB.tplgfr25s_1?idArticle=LEGIARTI000036078650&cidTexte=LEGITEXT000006074220&dateTexte=20180215&categorieLien=id&oldAction=) ; decreto de 21 de noviembre de 2017 por el que se modifica la nomenclatura de las instalaciones clasificadas para la protección del medio ambiente.

#### Cumplimiento de obligaciones para las instituciones que reciben el público (ERP)

Dado que las instalaciones están abiertas al público, el profesional debe cumplir con las normas relativas a las instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, es aconsejable consultar el listado[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

**Requisito de retención de ropa**

Cualquier prenda confiada a la tintorería que no haya sido retirada más allá de un año en la planta de teñido se considerará abandonada. Si es necesario, el profesional puede ser autorizado por el juez del tribunal de distrito o el presidente del tribunal superior para revenderlo en subastas públicas.

*Para ir más allá* Artículo 1 de la Ley de 31 de diciembre de 1903 relativa a la venta de determinados artículos abandonados.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

**Autoridad competente**

El profesional que se dedica a una actividad de teñido no industrial debe informar de su empresa, y para ello, debe hacer una declaración ante la CPI.

**Documentos de apoyo**

El interesado debe proporcionar la[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI envía un recibo al profesional el mismo día mencionando los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si la CFE se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### b. Si es necesario, registre los estatutos de la empresa

El profesional que lleva a cabo la actividad de teñido no industrial deberá, una vez que los estatutos de la sociedad estén fechados y firmados, inscribirlos en el departamento de impuestos sobre sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio cuando los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

