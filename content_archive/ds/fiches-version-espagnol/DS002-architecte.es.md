﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS002" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construcción - Bienes raíces" -->
<!-- var(title)="Arquitecto" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construccion-bienes-raices" -->
<!-- var(title-short)="arquitecto" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/construccion-bienes-raices/arquitecto.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="arquitecto" -->
<!-- var(translation)="Auto" -->


Arquitecto
==========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El arquitecto es responsable de las diversas fases del diseño y construcción de la obra.

En primer lugar, se requiere realizar un estudio de viabilidad del terreno antes de trazar los primeros planos del futuro edificio. Obtendrá el permiso de construcción y negociará precios con los diversos contratistas que trabajarán en el sitio. A lo largo del proyecto, tendrá que tener en cuenta las normativas de planificación, las limitaciones legales y técnicas, así como el presupuesto del cliente y los requisitos de plazos.

Una vez diseñados los planos, el arquitecto coordina los equipos responsables de la construcción de la obra hasta la entrega de la obra.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para una profesión liberal, la CFE competente es el Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El ejercicio de la profesión de arquitecto está reservado a los inscritos en la tabla regional de arquitectos. La inscripción está abierta a los titulares del Diploma de Arquitecto Estatal que hayan completado un año adicional de formación que conduzca a la certificación para practicar el máster en su propio nombre (HMONP).

La formación en arquitectura se imparte en 20 escuelas superiores nacionales de arquitectura de toda Francia, en el Instituto Superior Nacional de Ciencias Aplicadas (INSA) de Estrasburgo y en la Escuela Especial de Arquitectura (ESA) de París.

Es accesible después de la graduación y dura cinco años. El acceso se realiza después de revisar el archivo del candidato, con una entrevista que puede incluir pruebas.

Los estudios consisten en dos ciclos:

- un título de pregrado de tres años que conduce al Diploma de Estudios de Arquitectura (DEEA);
- un segundo ciclo en dos años (nivel maestro) que conduce al Diploma Estatal de Arquitecto (DEA).

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Para un ejercicio temporal e informal (LPS)

Un estado nacional de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) que actúe legalmente como arquitecto en uno de estos Estados podrá utilizar su título profesional en Francia de forma temporal o ocasional.

Tendrá que hacer la solicitud, antes de su actuación, la solicitud mediante declaración escrita al Consejo Regional de la Orden de Arquitectos (véase infra "3". b. Hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)").

En caso de diferencias sustanciales entre sus cualificaciones profesionales y la formación requerida en Francia, el consejo podrá someterlo a una prueba de aptitud que consiste en una prueba oral de treinta minutos. Abarcará la totalidad o parte de las materias contempladas en el artículo 9 del Decreto del 17 de diciembre de 2009 sobre el reconocimiento de cualificaciones profesionales para el ejercicio de la profesión de la arquitectura. Dependiendo del tema, se le puede pedir al nacional un proyecto arquitectónico para evaluar sus habilidades.

*Para ir más allá* Artículo 10-1 de la Ley 77-2, de 3 de enero de 1977, de arquitectura; Artículos 10 a 14 del Decreto No 2009-1490 de 2 de diciembre de 2009 sobre el reconocimiento de cualificaciones profesionales para el ejercicio de la profesión de arquitecto.

#### Para un ejercicio permanente (LE)

El nacional de un Estado de la UE o del EEE podrá establecerse en Francia para ejercer permanentemente si tiene:

- el diploma estatal de arquitecto u otro diploma francés de arquitecto reconocido por ese estado, y el HMONP;
- un diploma, certificado u otro título extranjero reconocido por ese Estado que permita el ejercicio de la profesión de arquitecto y reconocida por el Estado.

En ambos casos, podrá solicitar directamente su inscripción en la lista regional de arquitectos ante el Consejo Regional de la Orden de Arquitectos (véase infra "3o. d. Solicitar inscripción en la lista del arquitecto regional").

También puede establecerse en Francia de forma permanente, la UE o el nacional del EEE que:

- posee un certificado de formación expedido por un tercer Estado pero reconocido por un Estado de la UE o del EEE y ha permitido ejercer la profesión de arquitecto durante al menos tres años;
- fue reconocido como calificado por el Ministro de Cultura, después de una revisión de todos sus conocimientos, cualificaciones y experiencia profesional;
- fue reconocido como calificado por el Ministro de Cultura después de presentar referencias profesionales que atestian que se distinguió por la calidad de sus logros arquitectónicos, tras el asesoramiento de una comisión nacional.

Una vez que el nacional cumpla una de estas condiciones, podrá solicitar el reconocimiento de sus cualificaciones profesionales antes de solicitar el registro en la junta (véase más adelante "3o). c. Si es necesario, solicite el reconocimiento previo de sus cualificaciones profesionales para la UE o el Nacional del EEE para su inclusión en la lista de arquitectos regionales").

Si existen diferencias sustanciales entre su formación y las cualificaciones profesionales requeridas en Francia, el nacional podrá ser sometido a una prueba de aptitud organizada en un plazo de seis meses a partir de su decisión.

*Para ir más allá* Artículo 10 de la Ley 77-2, de 3 de enero de 1977, de arquitectura; Artículos 1 a 9 del Decreto No 2009-1490 de 2 de diciembre de 2009 sobre el reconocimiento de cualificaciones profesionales para el ejercicio de la profesión arquitectónica.

### c. Algunas peculiaridades de la regulación de la actividad

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Dado que las instalaciones están abiertas al público, el profesional debe cumplir con las normas relativas a las instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

#### Obligaciones legales

El arquitecto deberá declarar al consejo regional los vínculos que pueda tener con una persona física o jurídica cuya actividad se beneficie de la construcción.

No participará en actividades comerciales que no sean de conocimiento sin fines distintos, independientes o públicos.

También está obligado a conllevar un seguro de responsabilidad profesional, cuando ejerce a título liberal, cubriendo los actos realizados durante su misión.

#### Cumplimiento del código de ética

Las disposiciones del Código de ética se imponen a todos los arquitectos que practican en Francia.

Como tal, el arquitecto debe respetar los principios de respeto a la confidencialidad profesional, la competencia y los conflictos de intereses. Para obtener más información, visite el sitio web de la Orden de Arquitectos.

*Para ir más allá* Artículos 18 y 19 de la Ley 77-2 del 3 de enero de 1977 sobre arquitectura.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de la empresa, el contratista debe registrarse en el Registro de Comercio y Sociedades (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Hacer una predeclaración de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)

**Autoridad competente**

El Consejo Regional de la Orden de Arquitectos del lugar de entrega es responsable de decidir sobre la solicitud de declaración previa.

**Documentos de apoyo**

La solicitud de declaración previa de actividad se realiza mediante la presentación de un archivo que incluye:

- una declaración en la que se mencione la intención del nacional de prestar servicios en Francia;
- Un certificado de seguro civil profesional del nacional de menos de tres meses;
- Una copia de los títulos de formación
- Una copia del certificado que certifique que el nacional está establecido en un Estado de la UE o del EEE y no está prohibido de ejercer;
- una copia de un documento de identidad válido.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Procedimiento**

La autoridad competente dispone de un mes a partir de la recepción de la declaración para decidir sobre la solicitud de declaración. Cuando el solicitante esté obligado a pasar una prueba de aptitud, el consejo regional tendrá un mes adicional para decidir.

*Para ir más allá* Artículos 14 a 17 del Decreto del 17 de diciembre de 2009 sobre el reconocimiento de cualificaciones profesionales para el ejercicio de la profesión arquitectónica.

### c. Si es necesario, solicitar el reconocimiento previo de sus cualificaciones profesionales para la UE o el NACIONAL del EEE en vista de su inscripción en la lista de arquitectos regionales

**Autoridad competente**

El Ministro de Cultura es responsable de decidir sobre las solicitudes de reconocimiento de las cualificaciones profesionales del nacional.

**Documentos de apoyo**

La solicitud de reconocimiento deberá dirigirse a la autoridad competente y presentarse por expediente, en dos copias, que contenga los siguientes documentos:

- Una copia de un documento de identidad válido
- Una copia de los títulos de formación
- En su caso, una copia de la descripción detallada del currículo y su volumen por hora, así como una copia de la autorización para llevar el título de arquitecto;
- En su caso, una copia del título de formación expedido por un tercer Estado pero reconocido por un Estado de la UE o del EEE, así como una copia:- ya sea la certificación expedida por el Estado de la UE o el EEE que acredite el ejercicio de la profesión de arquitectura durante al menos tres años en ese Estado,
  - cualquier documento expedido por el Estado de la UE o el EEE que acredite el ejercicio de la profesión arquitectónica;
- una descripción de la formación y la experiencia laboral relevante para el ejercicio de la actividad.

En el caso del nacional que se habría distinguido por la calidad de sus logros en el campo de la arquitectura, el expediente incluirá:

- Un CV
- Una carta que describe sus motivaciones
- una colección que presenta una variada selección de estudios y proyectos realizados que muestran su aspecto exterior, diseño interior e inserción en el sitio, ilustrados con fotografías, planos y cortes, así como bocetos y dibujos;
- Una lista de referencias al trabajo y estudios realizados;
- Una copia de artículos y publicaciones dedicados a los logros presentados o a su autor;
- Certificados y recomendaciones de empleadores y clientes
- un certificado expedido por la UE o el Estado del EEE que certifique que las actividades del solicitante son arquitectónicas.

**Procedimiento**

La autoridad competente confirmará la recepción del expediente en el plazo de un mes. Una vez recibido el expediente completo, el Ministro responsable de la cultura dispondrá de cuatro meses para tomar su decisión, previa asesoría del Consejo Nacional de la Orden de Arquitectos.

En caso de diferencias sustanciales entre la formación y las cualificaciones requeridas en Francia, podrá decidir someter al nacional a la misma prueba de aptitud que la solicitada en el caso de la prestación gratuita de servicios.

*Para ir más allá* Artículos 3 a 7 del Decreto No 2009-1490, de 2 de diciembre de 2009, sobre el reconocimiento de cualificaciones profesionales para el ejercicio de la profesión de arquitectura; Artículos 2 a 13 del Decreto de 17 de diciembre de 2009 sobre las modalidades de reconocimiento de cualificaciones profesionales para el ejercicio de la profesión de arquitecto.

### d. Solicitar el registro en la lista de arquitectos regionales

**Autoridad competente**

El Consejo Regional del Colegio de Arquitectos es responsable de decidir sobre las solicitudes para inscribirse en el consejo de la Orden.

**Documentos de apoyo**

La solicitud se realiza enviando un archivo en dos copias con los siguientes documentos justificativos:

- Una copia de un documento de identidad válido
- Una copia del título de formación
- un certificado de seguro civil profesional del interesado data de hace menos de tres meses;
- un extracto de los antecedentes penales.

**Procedimiento**

El consejo regional confirmará la recepción del expediente en el plazo de un mes. El silencio del consejo en un plazo de dos meses valdrá la pena la decisión de rechazar la solicitud.

*Para ir más allá* Artículo 1 del Decreto de 17 de diciembre de 2009 sobre el reconocimiento de las cualificaciones profesionales para el ejercicio de la profesión arquitectónica.

