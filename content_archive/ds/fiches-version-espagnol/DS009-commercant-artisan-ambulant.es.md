﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS009" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Comerciante, artesano" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="comerciante-artesano" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/comerciante-artesano.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="comerciante-artesano" -->
<!-- var(translation)="Auto" -->


Comerciante, artesano
=====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El comerciante o el artesano viajero lleva a cabo su actividad de manera no sedentario, en la vía pública, especialmente en salones, mercados y ferias. Este régimen se aplica en dos supuestos:

- Cuando el profesional opera fuera de la comuna donde se encuentra su vivienda o establecimiento principal;
- cuando no tiene hogar o residencia fija por más de seis meses.

*Para ir más allá* Artículos L. 123-29 y R. 123-208-1 del Código de Comercio.

### b. Centro competente de formalidades comerciales (CFE)

La CFE correspondiente depende de la naturaleza de la actividad realizada, de la forma jurídica de la empresa y del número de empleados empleados:

- para los artesanos y las empresas comerciales dedicadas a la actividad artesanal, siempre que no empleen a más de diez empleados, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para los artesanos y las empresas comerciales que emplean a más de diez empleados, esta es la Cámara de Comercio e Industria (CCI);
- en los departamentos del Alto y El Bajo Rin, la Cámara de Comercio de Alsacia es competente

**Es bueno saber**

En los departamentos del Bajo Rin, el Alto Rin y el Mosela, la actividad sigue siendo artesanal, independientemente del número de empleados empleados, siempre y cuando la empresa no utilice un proceso industrial. Por lo tanto, la CFE competente es la CMA o la Cámara de Comercio de Alsacia. Para más información, es aconsejable consultar el<span id=«c.-codes-ape-à-titre-indicatif" class="anchor»> id="«c.-codes-ape-à-titre-indicatif"" class=""></span id=«c.-codes-ape-à-titre-indicatif" class="anchor»>>web oficial de la Cámara de Comercio de Alsacia.

Para más información, es aconsejable consultar[el SITIO DEL CCI en París](http://www.entreprises.cci-paris-idf.fr/web/formalites/competence-cfe).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Las cualificaciones profesionales requeridas dependen de la profesión itinerante. No existe un requisito de calificación adicional específico para el ejercicio de una actividad de caminar. Para obtener más información, es aconsejable consultar las hojas de actividad correspondientes a la actividad realizada.

### b. Condiciones de honorabilidad e incompatibilidad

No hay condiciones de honorabilidad específicas para el ejercicio de una actividad de caminar. Por otro lado, el comerciante o el artesano viajero deben respetar las condiciones e incompatibilidades específicas de la profesión ejercitada. Para obtener más información, es aconsejable consultar las hojas de actividad pertinentes.

### c. Garantía financiera

Cuando el comerciante o artesano viajero desarrolla su actividad en la vía pública o en un lugar público sin tener residencia o residencia fija durante más de seis meses en Francia, debe darse a conocer a las autoridades fiscales y depositar una suma como garantía de la recaudación de impuestos que se le adeudan. Este depósito debe realizarse antes de cualquier ejercicio de su actividad.

**Tenga en cuenta que**

En este caso, las obligaciones fiscales se cumplen con el departamento fiscal al que es responsable el municipio al que están adscritas estas personas.

Para una actividad de venta de bienes o servicios realizada sin vehículo, esta cantidad es de 150 euros. Se incrementa en 76 euros por el uso de un vehículo, 150 euros para dos vehículos y 300 euros para más de dos vehículos.

Este depósito es reembolsable a aquellos que justifiquen el pago, exención o no debido, durante todo el período de validez del recibo, el impuesto profesional, el impuesto sobre el valor añadido y los impuestos conexos y el impuesto sobre Ingresos.

Se emite un recibo a cambio del pago de este depósito. El recibo menciona la identidad del solicitante, su comuna de embargo, la naturaleza de la actividad que desarrolla, los medios de explotación, así como las identidades y hogares de sus agentes. Además, a cada asistente se le emite un certificado especial.

El recibo tiene una validez de tres meses a partir de su establecimiento. Sólo puede ser utilizado por su propietario, y para las condiciones de funcionamiento mencionadas en el la misma. La renovación del recibo da lugar a la presentación de un nuevo depósito.

Para obtener el recibo, el comerciante o artesano de la calle debe proporcionar:

- una fotografía reciente de sí mismo y sus asistentes;
- prueba de su identidad y el lugar de su comuna de apego;
- prueba de la identidad y la residencia o el lugar de la comuna de apego de sus asistentes;
- un permiso de residencia válido en el territorio nacional.

Este recibo debe ser producido a petición de los oficiales de policía judicial, oficiales de la ley de fraude y jueces de la corte de distrito.

*Para ir más allá* Artículos 302 octies, 111 quaterdecies y 50 quindecies Anexo 4 del Código General Tributario.

### d. Seguro de responsabilidad civil

El seguro de responsabilidad civil es obligatorio para algunos profesionales. Para más información, es aconsejable consultar la hoja de actividad relativa a la profesión ejercitada.

### e. Algunas peculiaridades de la regulación de la actividad

#### Mapa que permite el ejercicio de una actividad de caminar

**Hacer un informe previo**

El ejercicio de una actividad comercial o artesanal itinerante requiere una declaración previa que permita obtener la tarjeta necesaria para el ejercicio de la actividad andante.

Sujeto a esta formalidad:

- personas y corporaciones que operan fuera del territorio de la comuna donde se encuentra su vivienda o establecimiento principal;
- personas que no tienen residencia o residencia fija durante más de seis meses en un Estado miembro de la Unión Europea (UE).

**Tenga en cuenta que**

La persona que no tiene una vivienda o una residencia fija es aquel que no tiene un establecimiento principal o que no ha estado en un estado de la UE durante al menos seis meses como propietario o inquilino de una vivienda amueblada con muebles que le pertenecen.

Sin embargo, esta formalidad no se aplica:

- Agentes comerciales;
- vendedores de prensa-vendedores-peddlers;
- Operadores de taxis;
- VRPs, vendedores de casas independientes y lienzos bancarios y financieros;
- profesionales que realizan visitas incidentales en uno o más municipios vecinos para vender sus productos o para prestar servicios desde establecimientos fijos.

*Para ir más allá* Artículos L. 123-29 y R. 123-208-1 del Código de Comercio.

**Es bueno saber**

En caso de control, el tendero o el artesano de la calle presenta esta tarjeta. Del mismo modo, el cónyuge empleado o colaborador presenta una copia de la tarjeta de la persona para la que trabaja, un documento que establece un vínculo con el titular de la tarjeta y un documento que justifica su identidad.

*Para ir más allá* Artículos R. 123-208-4 del Código de Comercio.

**Autoridad competente**

La declaración debe hacerse a la CFE de la CPI O CMA, dependiendo de la naturaleza de la actividad. Esta autoridad es la que luego emite la tarjeta.

*Para ir más allá* Artículo R. 123-208-2 del Código de Comercio.

**Cómo conseguirlo**

La declaración puede presentarse in situ para su recepción, por correo, por carta recomendada con previo aviso de recepción o en línea para determinadas CFEs.

La tarjeta tiene una validez de cuatro años. Es renovable. En este caso, la tarjeta antigua que ha caducado debe ser dada contra la nueva tarjeta que la reemplaza.

En caso de que un cambio afecte a su negocio, el titular de la tarjeta debe presentar una declaración de modificación para su actualización o retirada.

Para permitir el ejercicio inmediato de la actividad andante, un certificado provisional, válido por un mes, puede ser expedido por la CFE, a petición del contratista, a la espera de la obtención de la tarjeta final.

*Para ir más allá* Artículo R. 123-208-3 del Código de Comercio.

**Tiempo de respuesta**

La tarjeta que permite el ejercicio de una actividad comercial o artesanal itinerante se expide en el plazo máximo de un mes, a partir de la recepción del expediente de declaración. En el caso de una declaración simultánea de la presentación de una solicitud para iniciar una empresa, el plazo de un mes va desde el registro hasta el registro de publicidad legal.

Durante este período y hasta que se reciba la tarjeta final, el solicitante de registro podrá presentar un certificado provisional expedido, a petición suya, por la CPI o la CAM.

Si se renueva la tarjeta, la fecha límite para la emisión de la nueva tarjeta se incrementa a 15 días a partir del momento en que se recibe el archivo de devolución completo.

**Documentos de apoyo**

El archivo de informes debe incluir:

- Formulario Cerfa 1402202 fechada y firmada;
- un extracto de menos de tres meses de edad del RCS o del RM O, o, para las personas exentas de registro, el certificado de inscripción en el directorio de empresas y establecimientos (Sirene) y, para las asociaciones que realizan una actividad Sirene y copiando su estado;
- para los nacionales de la UE que no tienen un establecimiento en Francia pero han declarado su actividad comercial o artesanal en otro Estado de la UE, prueba de esta declaración;
- Una copia del Documento de Identidad o, si es necesario, del permiso de tráfico o residencia;
- dos fotografías de identidad recientes.

Cuando la declaración se realiza junto con una declaración de creación de negocio sujeta a inscripción en un registro de publicidad legal, vale la pena presentar la declaración previa de una actividad empresarial o artesanías itinerantes. En este caso, el solicitante sólo produce dos fotografías de identidad recientes.

El fichero de renovación se lleva a cabo en las mismas condiciones que la solicitud original.

*Para ir más allá* Artículos R. 123-208-2 y siguientes, A. 123-80-1 y más allá del Código de Comercio.

**Costo**

La tarjeta se emite por el pago de una tasa de 15 euros.

*Para ir más allá* Artículos R. 123-208-3 y A. 123-80-5 del Código de Comercio.

#### Folleto de tráfico especial en caso de ausencia de residencia fija

Los viajeros y artesanos que no tengan una residencia o residencia fija durante más de seis meses en un estado de la UE deben tener un folleto especial de tráfico emitido por las autoridades administrativas. Lo mismo se aplica a las personas que los acompañan a ellos y a sus asistentes si son mayores de dieciséis años y no tienen hogar o residencia fija en Francia durante más de seis meses.

**Tenga en cuenta que**

Los empleadores deben asegurarse de que sus empleados de hecho se les proporciona este documento, cuando se les requiere hacerlo.

El folleto de circulación especial tiene una validez de cinco años. Para obtener la prórroga de la validez, el titular debe presentar personalmente su solicitud al Comisionado de la República o al Comisionado Adjunto de la República, ya sea desde el municipio de su municipio de conexión o desde el lugar más cercano a su lugar de residencia.

Inmediatamente se le da un recibo de depósito, que vale un título de circulación por un período de tres meses. En el momento de la presentación, el solicitante deberá indicar la prefectura o subprefectura de la que desea retirar su título ampliado.

**Qué saber**

La falta de un folleto de tráfico es un billete de 5a clase.

**Autoridad competente**

Para solicitar el folleto de tráfico especial, el comerciante o el artesano viajero debe presentarse personalmente al prefecto o al subprefecto del municipio donde se encuentra la comuna a la que desea estar adscrito. En París, la solicitud debe hacerse al prefecto de la policía.

**Documentos de apoyo**

En apoyo de su solicitud, el comerciante o el artesano viajero debe:

- presentar pruebas de identidad y nacionalidad y fotografía de identidad en tres copias;
- indicar simultáneamente la comuna a la que desea estar adscrita y la razón de la elección de la comuna;
- Si no es nacional francés, proporcione el documento válido que permita la entrada a Francia y, si es necesario, su permiso de residencia y su tarjeta de trabajador o comerciante extranjera.

**Tiempo de respuesta**

Si el prefecto no responde en el plazo de dos meses, se rechaza la solicitud.

Se emite un certificado de título de tráfico provisional válido, válido por un máximo de un mes.

*Para ir más allá* Decreto No 2014-1292.

**Costo**

El folleto de tráfico no está sujeto a ningún visado.

*Para ir más allá* Ley 69-3 Artículo 2; Decreto 70-708.

#### Restricciones al comercio callejero

**Venta de bebidas alcohólicas**

Los vendedores ambulantes tienen prohibido vender al por menor, ya sea para consumo in situ o para llevar, bebidas alcohólicas de los grupos cuarto y quinto, como ron o licores de la destilación de vinos, sidras.

Para obtener más información, es aconsejable consultar la hoja de actividades[Flujo de bebidas](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*Para ir más allá* Artículos L. 3321-1 y L. 3322-6 del Código de Salud Pública.

**Comercio de mascotas**

La venta de perros, gatos y otras mascotas está prohibida en ferias, mercados, mercadillos, ferias, exposiciones u otros eventos no específicamente dedicados a animales, excepto en la exención de la prefectura.

*Para ir más allá* Artículo L. 214-7 del Código Rural y Pesca Marina.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, se recomienda consultar las hojas de actividades "Declaración de una empresa comercial" y "Registro de una empresa individual en el Registro de Comercio y Sociedad."

### b. Autorización posterior al registro

#### Solicitar permiso para instalar en la vía pública

El comerciante o el artesano viajero deben solicitar un permiso para instalar en el dominio público. Este permiso varía dependiendo del tipo de lugar ocupado:

- En el caso de los ayuntamientos, mercados y ferias, se trata de una solicitud de ubicación en un mercado que se dirija al ayuntamiento, al placier municipal o al organizador del evento;
- la ocupación bajo la influencia se refiere a terrazas abiertas, exhibiciones, estacionamientos de furgonetas, requiere que la autoridad administrativa responsable de la policía de tránsito solicite un permiso de estacionamiento;
- en el caso de una ocupación privada con derecho de paso (terrazas cerradas, quioscos fijados al suelo), se debe solicitar el permiso de tráfico a la autoridad administrativa responsable de la gestión del dominio.

