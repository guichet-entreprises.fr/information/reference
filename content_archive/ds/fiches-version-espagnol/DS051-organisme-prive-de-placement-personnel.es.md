﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS051" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Organización privada de personal" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="organizacion-privada-de-personal" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/organizacion-privada-de-personal.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="organizacion-privada-de-personal" -->
<!-- var(translation)="Auto" -->


Organización privada de personal
================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad de una organización privada de personal es conectar rutinariamente a los solicitantes de empleo y a las empresas que buscan personal.

La organización que ofrece estos servicios de colocación no debe, bajo ninguna circunstancia, exigir compensación al solicitante de empleo (sujeto a las disposiciones relativas a los agentes deportivos e intermitentes de la feria), ni debe participar en la relación futura de la Trabajo.

**Tenga en cuenta que**

Estas organizaciones pueden ser especializadas y, por lo tanto, ofrecen sólo puestos de trabajo en sectores específicos.

*Para ir más allá* Artículos L. 5321-1 y L. 5321-2 del Código de Trabajo.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad.

Dado que la actividad de inversión es de carácter comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para crear una organización privada de personal, no se requieren cualificaciones profesionales.

Sin embargo, el interesado debe proceder a la declaración de su empresa (véase infra "3o. Procedimientos y trámites de instalación").

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios o establecimiento gratuito)

No existen disposiciones especiales para el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE) para llevar a cabo esta actividad de forma temporal y casual o permanente en Francia.

Por lo tanto, el nacional de la UE está sujeto a los mismos requisitos que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

### c. Condiciones de honorabilidad

#### Sanciones administrativas

Mientras el solicitante de empleo sea discriminado durante el procedimiento de colocación, la autoridad administrativa competente podrá exigir al organismo que cierre hasta tres meses.

*Para ir más allá* Artículos L. 5323-1 y L. 1132-1 del Código de Trabajo.

#### Sanciones penales

El hecho de que un profesional de una agencia de empleo exija una indemnización a un demandante de empleo se castiga con seis meses de prisión y una multa de 3.750 euros.

**Procedimiento**

Una vez que se encuentran las infracciones, el organismo privado debe presentar sus comentarios dentro de una quincena. Si el prefecto no cumple más allá de este período, puede enviarle un aviso formal por carta recomendada con notificación de recepción y ordenar su cierre por un máximo de tres meses.

*Para ir más allá* Artículo L. 5324-1 del Código de Trabajo.

### d. Algunas peculiaridades de la regulación de la actividad

**Transmisión de información**

La agencia privada de personal debe proporcionar al prefecto información sobre:

- Facturación
- número de solicitantes de empleo recibidos, colocados y registrados por la organización y divididos por edad y género.

*Para ir más allá* Artículo R. 5323-8 del Código de Trabajo.

**Contrato de servicio**

Una organización privada de personal puede celebrar un contrato de prestación de servicios para el cuidado de los solicitantes de empleo con una agencia de empleo pública, que incluye:

- Empleo estatal y servicios profesionales de igualdad;
- Centro de empleo;
- una institución industrial y comercial de última generación (Epic) responsable de la formación profesional de adultos.

Si es necesario, la agencia de colocación privada debe proporcionar al organismo público y al Centro de Empleo toda la información relativa al solicitante de empleo. Por ejemplo, la organización privada debe proporcionar la siguiente información, gracias al archivo único del solicitante de empleo:

- cómo se adaptará el proyecto de acceso personalizado al empleo (PPAE) del solicitante con el tiempo. Este proyecto reúne todas las características del trabajo buscado;
- Actualización de la lista de solicitantes de empleo
- Compensación
- operaciones de seguimiento y búsqueda de empleo.

*Para ir más allá* Artículos L. 5311-1, R. 5323-12 a R. 5323-14 del Código de Trabajo.

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

Es aconsejable consultar el listado " [Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales de las normas de seguridad contra incendios y pánico en las instituciones públicas.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

**Autoridad competente**

El profesional debe proceder a la declaración de su empresa. Para ello, deberá presentar una declaración ante la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

El interesado debe proporcionar la[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales del CCI envía un recibo al profesional por los documentos que faltan el mismo día. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### b. Si es necesario, registre los estatutos de la empresa

El profesional deberá, una vez fechados y firmados los estatutos de la sociedad, registrarlos en la Oficina del Impuesto sobre Sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

