﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS014" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Experiencia" -->
<!-- var(title)="Geometros-experto" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="experiencia" -->
<!-- var(title-short)="geometros-experto" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/experiencia/geometros-experto.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="geometros-experto" -->
<!-- var(translation)="Auto" -->


Geometros-experto
=================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El topógrafo-experto es un profesional cuya misión es:

- llevar a cabo estudios topográficos y trabajar estableciendo los límites de la tierra en el uso de la tierra de las misiones de planificación del uso de la tierra;
- realizar cualquier operación técnica o estudios sobre la evaluación, gestión o gestión de los activos de la tierra.

*Para ir más allá* Artículo 1 de la Ley 46-942, de 7 de mayo de 1946, por la que se establece la Orden de los Topógrafos Expertos.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la estructura en la que se lleve a cabo la actividad:

- para una profesión liberal, la CFE competente es el Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, es el registro del tribunal comercial o el registro del tribunal de distrito. Para los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Requisito de edad

Para ser un topógrafo experto, el profesional debe tener 25 años.

*Para ir más allá* Artículo 3, párrafo 3, de la Ley 46-942, de 7 de mayo de 1946, por la que se establece la Orden de los Topógrafos Expertos.

### b. Cualificaciones profesionales

Sólo una persona profesionalmente cualificada por la Orden de los Topógrafos Expertos puede trabajar como agrimensor experto. Para ello, el interesado deberá:

- Ser titular:- o el título de ingeniería topógrafo emitido por:- Escuela Superior de Topógrafos y Topógrafos (ESGT) en Le Mans,
    * Escuela Especial de Obras Públicas (ESTP) en París,
    * Instituto de Ciencias Aplicadas (INSA) en Estrasburgo,
  - o diploma de experto alegador de tierras emitido por el gobierno (DPLG),
  - han sido objeto del procedimiento de validación de la experiencia (VAE);
- han trabajado una pasantía profesional de dos años.

*Para ir más allá* Artículo 16 del Decreto 96-478, de 31 de mayo de 1996, relativo a la regulación de la profesión de topógrafo-experto y código de funciones profesionales; y el Artículo 6 del Decreto No 2010-1406 de 12 de noviembre de 2010 relativo al diploma de experto sapodon-experto del gobierno.

### c. Cualificaciones profesionales - Nacionales Europeos (Servicio Gratuito o Establecimiento Libre)

#### Para entrega gratuita de servicios (LPS)

El nacional profesional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE) podrá ejercer en Francia de forma temporal y ocasional la actividad de topógrafo-experto, siempre que esté legalmente establecido estados para llevar a cabo la misma actividad.

Tendrá que solicitar lo mismo al consejo regional en el plazo de entrega de los servicios.

El candidato también debe tener las habilidades de idiomas necesarias para ejercer la profesión en Francia.

*Para ir más allá* Artículo 2-1 de la Ley de 7 de mayo de 1946; Artículo 39 del Decreto 96-478, de 31 de mayo de 1996, relativo a la regulación de la profesión de topógrafo-experto y código de funciones profesionales.

#### Para un establecimiento gratuito (LE)

Para ejercer permanentemente en territorio francés, el nacional de un Estado miembro de la UE o del EEE debe estar en posesión de:

- ya sea un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado que regula el ejercicio de la profesión;
- o un certificado que indique que ha estado en el negocio durante un año en los últimos diez años antes de la solicitud en un estado que no regula la profesión.

Una vez que cumpla una de estas condiciones, puede solicitar al Ministro de Planificación la calificación.

**Tenga en cuenta que**

Un topógrafo experto que haya solicitado el reconocimiento también puede solicitar su inclusión en la Orden de los Topógrafos Expertos (véase más adelante "3 grados). b. Solicitud de registro ante la Orden de los Topógrafos-Expertos").

*Para ir más allá* Artículo 7-1 del Decreto 31 de mayo de 1996.

### d. Condiciones de honorabilidad e incompatibilidad

#### Integridad

Para llevar a cabo su actividad el profesional no tiene que:

- han sido objeto de un procedimiento de reparación, liquidación o quiebra;
- ser un servidor público o ser despedido por actos contrarios al honor o a la probidad;
- haber sido autor de actos penalmente reprobables contrarios al honor;
- prohibido o suspendido de la práctica.

**Tenga en cuenta que**

Los nacionales de un Estado miembro de la UE o del EEE no deben haber sido objeto de las mismas sanciones.

*Para ir más allá* Artículo 3, párrafo 2 de la Ley de 7 de mayo de 1946.

#### Incompatibilidades

El ejercicio de la profesión de topógrafo-experto debe llevarse a cabo de forma independiente, por lo que es incompatible con:

- una oficina de funcionarios públicos o ministeriales;
- un mandato comercial o cualquier empleo remunerado por salario o salario.

**Condiciones de práctica para intermediarios y gestión de propiedades**

Los profesionales que se dedican a la gestión inmobiliaria y los intermediarios deben cumplir con ciertas obligaciones, incluyendo:

- obtener la aprobación del Consejo Regional del Colegio de Expertos en Topografía;
- No participar simultáneamente en actividades de bienes raíces y expertos en topógrafos durante la misma misión;
- Contrate un seguro de responsabilidad personal por estas actividades;
- Llevar una contabilidad separada de sus actividades como agrimensor experto;
- tienen una orden escrita.

*Para ir más allá* Artículo 8 de la Ley de 7 de mayo de 1946.

### e. Algunas peculiaridades de la regulación de la actividad

**Obligación de respetar las reglas éticas de la profesión**

El agrimensor-experto está sujeto, a lo largo de su desempeño, al cumplimiento de las normas disciplinarias. El Consejo Regional de la Orden en el que se registrará garantiza el cumplimiento de las siguientes normas:

- garantía de independencia en cada una de sus misiones. Cabe señalar que la pertenencia a la Orden es incompatible con una oficina de funcionarios públicos o ministeriales;
- la obligación de asesorar y ser transparente a sus clientes;
- la obligación hasta la fecha y firmar todos los planes y documentos elaborados, así como de sellarlo;
- El deber de memoria (mantener y mantener documentos y archivos hechos);
- Secreto profesional
- posible uso de la publicidad únicamente con fines informativos.

**Qué saber**

Cualquier publicidad del topógrafo-experto se presentará al consejo regional de la Orden antes de su emisión.

*Para ir más allá* Artículo 8 de la Ley de 7 de mayo de 1946.

**Obligación de contrato de seguro de responsabilidad civil**

Como profesional, el topógrafo-experto debe tomar un seguro de responsabilidad civil profesional. Mientras el profesional practique como empleado, esta obligación recae en su empleador.

*Para ir más allá* Artículos L. 241-1 y siguientes del Código de Seguros.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitud de reconocimiento de cualificación

Todo nacional de un Estado de la UE o del EEE que desee establecerse en Francia y ser reconocido como agrimensor experto debe solicitar el reconocimiento de la cualificación.

**Autoridad competente**

El profesional debe solicitar el reconocimiento de cualificación al Ministro responsable de la planificación urbana.

**Documentos de apoyo**

Esta solicitud debe enviarse por duplicado e incluir todos los siguientes documentos justificativos:

- Una tarjeta de registro civil y una identificación de menos de tres meses de edad;
- Una copia de los diplomas, certificados o títulos que acrediten la formación recibida, así como una descripción del contenido de los estudios realizados, las prácticas realizadas y el número correspondiente de horas;
- Un certificado de competencia o un certificado de formación;
- cualquier documento que justifique que el agrimensor experto ha ejercido bien durante un año en los últimos diez años en un Estado miembro de la UE o del EEE.

**hora**

El Ministro responsable de urbanismo dispondrá de tres meses a partir de la recepción del expediente completo para decidir sobre la solicitud. Puede decidir si reconoce las cualificaciones profesionales del nacional y, en este caso, le permite llevar a cabo la actividad de topógrafo-experto en Francia, o someterlo a una medida de compensación.

**Tenga en cuenta que**

La falta de respuesta en un plazo de tres meses equivale al rechazo de la solicitud.

**Bueno saber: Medidas de compensación**

El Ministro de Planificación podrá decidir que el nacional completará un curso de ajuste por un máximo de tres años o se someterá a una prueba de aptitud. Este último consiste en la presentación y discusión de un expediente sobre la experiencia profesional y los conocimientos del candidato para el ejercicio de la profesión. El expediente se entrega al interesado antes de que se presente a un jurado, que luego dará a conocer los resultados de la prueba al Ministro responsable de la planificación urbana.

Una vez calificado, el solicitante podrá solicitar el registro en la Orden de los Topógrafos Expertos (ver "3o. b. Solicitud de registro ante la Orden de los Topógrafos-Expertos").

*Para ir más allá* Artículo 2-1 de la Ley de 7 de mayo de 1946; Artículos 7 y siguientes del decreto de 31 de mayo de 1996.

### b. Solicitud de inscripción ante el Colegio de Topógrafos-Expertos

**Autoridad competente**

El nacional que desee llevar a cabo la actividad de topógrafo-experto deberá presentar, mediante carta recomendada al presidente del Consejo Regional de la Orden cuando desee establecer, una solicitud de inscripción a la Orden de la Profesión que incluya:

- Un formulario de información proporcionado por el consejo regional en tres copias;
- Una copia de un diploma que emita el título de topógrafo-experto o una decisión ministerial que reconozca la calificación o, en su defecto, el acuse de recibo del expediente completo de la solicitud de reconocimiento por el Ministro responsable de la planificación;
- tres fotografías de identidad;
- Un certificado de pago de la tasa de reclamación;
- Una tarjeta de registro civil válida y una identificación;
- un extracto del expediente penal o un documento equivalente;
- para el nacional, un título en francés o un diploma profundo en francés, o, si es necesario, un certificado de posesión de las competencias del idioma en el ejercicio de la profesión en Francia establecido por el topógrafo-experto solicitante ha completado una pasantía.

Una vez recibida la solicitud en su totalidad, el presidente del consejo regional reconoce la solicitud en el plazo de un mes.

**Costos**

El importe de la compensación por los gastos de archivo es fijado cada año por el Consejo Superior tras la aprobación del Comisario del Gobierno (como indicación, fue de 200 euros en 2017).

**Remedio**

El solicitante podrá presentar un recurso ante el Consejo Superior de la Orden en el plazo de dos meses a partir de la notificación de la decisión, que tendrá cuatro meses para decidir.

Una vez registrado, el agrimensor experto toma un juramento ante el consejo regional y recibe un número de registro a la Orden emitida por el Consejo Superior, así como una tarjeta profesional.

*Para ir más allá* : orden de 12 de noviembre de 2009 relativo a la inclusión en el consejo de la Orden de los Topógrafos-Expertos.

### c. Formalidades de notificación de la empresa

**Autoridad competente**

El topógrafo-experto debe informar de su empresa, y para ello, debe hacer una declaración ante la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

El interesado debe proporcionar la[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

La CFE de la Cámara de Comercio e Industria envía un recibo al profesional el mismo día en el que se enumeran los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado en el plazo especificado anteriormente.

Si la CFE se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

#### Si es necesario, registre los estatutos de la empresa

El topógrafo-experto debe, una vez que los estatutos de la empresa han sido fechados y firmados, registrarlos en la Oficina del Impuesto de Sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

#### Predeclaración de actividad para una escuela secundaria

El topógrafo-experto puede trabajar en una empresa topógrafo-experto, que luego se compone de:

- una oficina principal en el distrito del consejo regional donde el profesional figura en la Orden de los Topógrafos Expertos;
- oficinas secundarias, oficinas permanentes o obras de construcción ubicadas en cualquier circunscripción regional. La creación de oficinas secundarias está sujeta a declaración previa.

**Autoridad competente**

La declaración de apertura previa debe ser dirigida por el profesional al presidente del consejo regional correspondiente.

**Documentos de apoyo**

La declaración anticipada debe indicar:

- Lo siguiente sobre el profesional:- su identidad,
  - su dirección comercial,
  - Su número de registro a la Orden;
- Elementos relacionados con la empresa:- su nombre y dirección,
  - su número de registro a la Orden,
  - La identidad de los expertos-gestreters asociados,
  - el nombre y la calidad del topógrafo-experto a cargo de la oficina secundaria.

**Tenga en cuenta que**

El cambio de domicilio de una escuela secundaria es objeto de información previa por parte del topógrafo-experto responsable de la oficina en el consejo regional.

*Para ir más allá* Artículo 30 del Decreto de 31 de mayo de 1996; Orden de Topógrafos Expertos.

