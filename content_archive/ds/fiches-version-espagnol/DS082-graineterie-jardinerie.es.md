﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS082" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Sederie - Jardinería" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="sederie-jardineria" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/sederie-jardineria.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="sederie-jardineria" -->
<!-- var(translation)="Auto" -->


Sederie - Jardinería
====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad de siembra y jardinería consiste en la venta profesional de plantas y suministros para el jardín y el medio ambiente. Durante su actividad, también se requiere producir y comercializar semillas y plantas.

La sembración o la jardinería también pueden involucrar una tienda de mascotas. Si es así, es aconsejable referirse a la lista[Compra/Venta de Placer o Mascotas](https://www.guichet-entreprises.fr/fr/activites-reglementees/negoce-et-commerce-de-biens/achat-vente-danimaux-dagrement-ou-de-compagnie/).

**Es bueno saber**

La práctica de esta actividad está regulada siempre y cuando el profesional proceda a la venta de fitofármacos o semillas tratadas.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. La actividad es de carácter comercial, sin embargo, en caso de preparación de plantas y composiciones florales la actividad será comercial y artesanal.

- Para las actividades artesanales, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el operador profesional tiene una actividad de comida rápida asociada a la fabricación de artesanías, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requieren cualificaciones profesionales para operar un negocio de semillas y jardinería. Sin embargo, el profesional está obligado a:

- tomar un curso de preparación de la instalación (SPI) para el registro en el directorio de operaciones;
- Si los fitofarmacéuticos se ponen a la venta, sea el titular:- un certificado individual específico llamado "Certiphyto"
  - aprobación para la práctica de un distribuidor de fitofarmacéuticos (ver infra "3o. b. Solicitud de aprobación").*Para ir más allá* Artículo L. 254-1 del Código Rural y Pesca Marina.

**Curso de preparación de instalación (SPI)**

Para registrarlo en el directorio de operaciones, el profesional que se dedica a una actividad artesanal deberá llevar a cabo un SPI compuesto por dos partes:

- la primera parte tiene como objetivo formar al profesional en la gestión de su negocio introduciéndolo, en particular, en la contabilidad general y en el entorno económico, jurídico y social de una empresa artesanal;
- la segunda parte de la pasantía es acompañar al profesional después de su inscripción en el directorio de oficios.

Sin embargo, el profesional puede ser excusado de realizar un SPI tan pronto como él o ella:

- se previene por un caso de fuerza mayor. Si es necesario, tendrá que completar esta pasantía en el plazo de un año a partir de su inscripción;
- completó un curso de formación en gestión de al menos treinta horas y organizado por la Cámara de Comercio sin comercio y artesanía regional, departamental o interdepartamental;
- recibió apoyo para la creación de un negocio que dura un mínimo de treinta horas entregado por una red de ayuda para establecer un negocio. Como tal, el profesional debe:- han recibido formación en gestión,
  - y estar inscritos en el directorio nacional de certificaciones profesionales;
- ha ejercido una actividad profesional que requiere al menos el mismo nivel que el proporcionado por el SPI durante al menos tres años.

*Para ir más allá* Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos; Decreto 83-517, de 24 de junio de 1983, por el que se establecen las condiciones de aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

**Certificado individual para la actividad de "venta, venta de fitofarmacéuticos"**

El certificado individual se expide al profesional que:

- (a) haya sido sometido a una capacitación que incluya una prueba de verificación de conocimientos de una hora;
- o se sometió a una prueba de una hora y media, consistente en treinta preguntas sobre el programa de formación establecido en la Lista II del auto de 29 de agosto de 2016;
- o tiene uno de los diplomas en el calendario I lista de la orden del 29 de agosto de 2016, obtenida en los cinco años anteriores a la fecha de solicitud.

Una vez que el profesional cumple una de estas condiciones, debe solicitar el certificado (ver infra "3o. a. Solicitar un certificado de práctica individual").

*Para ir más allá* : decreto de 29 de agosto de 2016 por el que se establecen y establecen las condiciones de obtención del certificado individual para la actividad "venta, venta de fitofarmacéuticos".

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

**En preparación para un LPS**

Todo nacional de un Estado miembro de la Unión Europea (UE) o de un Estado Parte en el Acuerdo del Espacio Económico Europeo (EEE), legalmente establecido y comprometido con una comercialización o venta de productos fitofarmacéuticos, podrá ejercer como temporal y casual, la misma actividad en Francia.

Cuando ni el acceso a la profesión ni su ejercicio estén regulados en ese Estado miembro, el nacional deberá justificar haber llevado a cabo esta actividad durante al menos un año en los últimos diez años.

Una vez que cumpla estas condiciones, deberá, antes de su primera prestación de servicios, hacer una declaración al Director Regional de Alimentación, Agricultura y Silvicultura (DRAAF) (véase más adelante "3. c. Predeclaración para el nacional de la UE para un ejercicio temporal y ocasional").

*Para ir más allá* Artículos R. 254-9 y L. 204-1 del Código Rural y pesca marina.

**En preparación para un LE**

Cualquier nacional de un Estado miembro de la UE o del EEE, dedicado a la venta o venta de fitofarmacéuticos, podrá llevar a cabo la misma actividad de forma permanente en Francia.

Mientras posea un certificado individual de ejercicio expedido por la autoridad competente de un Estado miembro, se considerará que posee el certificado necesario para ejercer en Francia.

*Para ir más allá* II del artículo R. 254-9 del Código Rural y de la Pesca Marina.

### c. Seguros

El profesional está obligado, para obtener su aprobación, a obtener un seguro que cubra su responsabilidad civil profesional.

*Para ir más allá* Artículo L. 254-2 del Código Rural y Pesca Marina.

### d. Algunas peculiaridades de la regulación de la actividad

**Obligación de información sanitaria**

El profesional que se dedique a actividades relacionadas con plantas y/o animales deberá, si detecta o sospecha un riesgo para la salud, informar inmediatamente al prefecto.

También deberá llevar a cabo o llevar a cabo, durante su actividad, todas las medidas destinadas a prevenir y combatir posibles riesgos para la salud.

*Para ir más allá* Artículo L. 201-7 y el siguiente del Código Rural y Pesca Marina.

**Obligaciones para la venta de fitofarmacéuticos a usuarios no profesionales**

El profesional que trabaje como jardinero de semillas sólo puede ofrecer a la venta a personas no profesionales productos con las palabras "empleo autorizado en jardines".

Sin embargo, las exenciones son posibles para las personas que actúan en nombre de profesionales. En caso afirmativo, estos usuarios deben justificar la posesión de un contrato o un certificado de delegación a un tercero o una prueba de la calidad profesional de dicho tercero.

Además, en el caso de una venta de productos que no llevan esta mención, el profesional está obligado a garantizar de antemano la calidad profesional del futuro comprador. Para ello, deberá comprobar que éste dispone de un certificado de práctica individual en una de las siguientes categorías:

- "Responsable de la toma de decisiones en el trabajo y los servicios";
- "Toma de decisiones en el trabajo agrícola";
- "Aplicador Comunitario Territorial."

En cada venta, el profesional está obligado a:

- productos separados etiquetados como "uso autorizado en jardines" y aquellos que no lo hacen, utilizando un proceso de señalización explícito;
- proporcionar toda la información sobre el uso de los productos y los riesgos que plantean para la salud y el medio ambiente, así como directrices de seguridad para prevenir riesgos.

*Para ir más allá* Artículos R. 254-20; R. 254-22 del Código Rural y Pesca Marina; Pedido del 6 de enero de 2016 sobre la documentación requerida para la compra de fitofármacos de la gama de usos "profesionales".

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales de las normas de seguridad contra incendios y pánico en las instituciones públicas.

Es aconsejable referirse a la lista[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

**Registro de actividad**

El profesional debe mantener un registro cronológico de las ventas de fitofarmacéuticos, indicando:

- Información del producto (nombre comercial, número de autorización, cantidad vendida, importe de regalías);
- El número y la fecha de facturación
- El código postal del usuario final del producto
- Documentos que justifican el estatus profesional del usuario
- toda la información sobre la semilla tratada (las especies vegetales en cuestión, el número y la fecha de la facturación).

Cada año, el profesional debe hacer un balance del año calendario anterior, incluyendo toda la información mencionada anteriormente durante la venta de cada producto fitofarmacéutico.

Además, cada año antes del 1 de abril, debe transmitir esta información a la agencia de agua y a la junta de agua electrónicamente, así como la carga de contaminación difusa.

*Para ir más allá* Artículo R. 254-23 y el siguiente del Código Rural y la pesca marina.

**Disposiciones específicas para la comercialización de semillas y plantas**

Sólo las semillas y plantas pueden comercializarse en Francia, a las que se hace referencia en un catálogo oficial de especies vegetales cultivadas y variedades cuando se prevé nifería para las especies de que se trate.

Los catálogos franceses y europeos de variedades están disponibles en el sitio web del National Interprofessional Seed and Plant Group ([GNIS](http://www.gnis.fr/catalogue-varietes/)).

*Para ir más allá* Decreto 81-605, de 18 de mayo de 1981, para la aplicación de la ley de 1 de agosto de 1905 relativa a la supresión del fraude en relación con el comercio de semillas y plantas.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitud de certificado individual de ejercicio

**Autoridad competente**

El profesional deberá presentar su solicitud a la Dirección Regional de Alimentación, Agricultura y Silvicultura (DRAAF) del lugar de su residencia, o en su caso, el lugar de la sede de la organización donde se llevó a cabo su formación.

**Documentos de apoyo**

Los solicitantes deben crear una cuenta en línea en el sitio web[service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) para acceder al servicio de aplicación de certificados.

Su solicitud debe incluir, dependiendo del caso:

- Una prueba de entrenamiento de seguimiento y, si es necesario, un pase a la prueba;
- copia de su diploma o título de formación.

**Tiempo y procedimiento**

El certificado se expide dentro de los dos meses siguientes a la presentación de su solicitud. En ausencia de la emisión del certificado después de este período, y a menos que la notificación de denegación, los documentos justificativos anteriores valen la pena un certificado individual de hasta dos meses.

Este certificado tiene una validez de cinco años y es renovable en las mismas condiciones.

*Para ir más allá* Artículo R. 254-11 del Código Rural y Pesca Marina.

### b. Solicitud de aprobación

**Autoridad competente**

El profesional debe presentar su solicitud al prefecto de la región en la que se encuentra la sede central de la empresa. Si el profesional tiene su sede social en un Estado miembro distinto de Francia, tendrá que presentar su solicitud al prefecto de la región en la que llevará a cabo su primera actividad.

**Documentos de apoyo**

Su solicitud debe incluir el[Formulario de solicitud](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14581.do) completado y firmado, así como los siguientes documentos:

- Un certificado de póliza de seguro de responsabilidad civil profesional;
- un certificado expedido por una organización externa que justifica que realiza esta actividad de acuerdo con las condiciones que garantizan la protección de la salud pública y el medio ambiente y la buena información de los usuarios. Esta certificación de la empresa se obtiene a raíz de una auditoría realizada por un organismo certificador sobre la base de un repositorio establecido para el pedido de 27 de abril de 2017;
- copia de un contrato con una organización externa con un contrato que proporciona el seguimiento necesario para mantener la certificación.

**Tenga en cuenta que**

Por otra parte, los microdistribuidores deben presentar los siguientes documentos a THE DRAAF:

- un certificado de seguro de responsabilidad civil profesional;
- prueba de la tenencia del certificado Certiphyto por todos sus empleados;
- Un documento que justifique que está sujeto al sistema fiscal de microempresas;
- Si es así, la lista de sitios donde el profesional es probable que lleve a cabo su actividad.

**Tiempo y procedimiento**

El profesional que inicie su actividad deberá solicitar una aprobación provisional que se concederá por un período de seis meses no renovable. Después de esto, podrá solicitar la aprobación final.

El silencio guardado por el prefecto de la región más allá de un período de dos meses, vale la pena la decisión de rechazo.

Además, el profesional está obligado a proporcionar al prefecto una copia de su certificado de seguro cada año.

*Para ir más allá* Artículo R. 254-15-1 a R. 254-17 del Código Rural y Pesca Marina; orden de 27 de abril de 2017 por la que se modifica el auto de 25 de noviembre de 2011 relativo al marco de certificación previsto en el artículo R. 254-3 del Código de Pesca Rural y Marítima para la actividad "distribución de fitofarmacéuticos a los no usuarios profesionales."

**Tenga en cuenta que**

Incluso si solicita varias actividades, la acreditación obtenida será única.

### c. Predeclaración del nacional de la UE para el ejercicio temporal y casual (LPS)

**Autoridad competente**

El nacional debe solicitar por cualquier medio al DRAAF del lugar donde se presta su primer servicio.

**Documentos de apoyo**

Su solicitud debe incluir el certificado individual de ejercicio expedido por el Estado miembro y, en su caso, con su traducción al francés por un traductor certificado.

**Tenga en cuenta que**

Esta declaración debe renovarse cada año y en caso de cambio de situación laboral.

*Para ir más allá* III del artículo R. 254-9 del Código Rural y de la Pesca Marina.

### d. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de su actividad, el profesional tendrá que inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro de Comercios y Empresas (RCS). Es aconsejable referirse a los "Formalidades de Reporte de Empresas Artesanales" "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Individual Comercial en el RCS" para más información.

### e. Regístrese en GNIS

**Autoridad competente**

El profesional debe solicitar el registro al GNIS.

**Documentos de apoyo**

Su solicitud debe incluir el[Cuestionario](https://media.afecreation.fr/file/30/4/questionnaire_en_vue_de_delivrer_l%27attestation_d%27enregistrement_au_gnis.56304.pdf) previsto según el fin, completado y firmado.

**Tiempo y procedimiento**

Una vez presentada la solicitud, el GNIS envía al profesional un certificado de registro.

**Tenga en cuenta que**

En caso de un cambio en el volumen o la naturaleza de la actividad, el profesional está obligado a informar a la GNIS.

*Para ir más allá* : decreto de 7 de noviembre de 1996 sobre las condiciones de inscripción en las categorías profesionales de la sección Plantas;[Sitio web oficial de GNIS](http://www.gnis.fr/).

