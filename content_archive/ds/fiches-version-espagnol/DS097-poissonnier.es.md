﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS097" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentación" -->
<!-- var(title)="Pescadero" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentacion" -->
<!-- var(title-short)="pescadero" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/alimentacion/pescadero.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="pescadero" -->
<!-- var(translation)="Auto" -->


Pescadero
=========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La pescadería recibe, almacena, procesa y comercializa mariscos y ciertos peces de agua dulce de acuerdo con las normas de higiene, salud, seguridad y trazabilidad en el trabajo, de acuerdo con la requisitos reglamentarios actuales.

Después de la compra, prepara los peces y mariscos que presenta y mejora. Garantiza el acto de venta, en particular dando la bienvenida y asesorando a la clientela. A petición del cliente, practica desnatar, descamación, evisceración, cortar y filetear el pescado.

**Tenga en cuenta que**

Algunas especies se mantienen vivas, especialmente los crustáceos.

Trabaja en pescaderías o en los mercados.

El pescadero también puede tener una actividad de catering y hacer comidas preparadas y terrinas.

*Para ir más allá* Apéndice I del decreto del 20 de marzo de 2007 por el que se crea el Certificado de Habilidades Profesionales (CAP) "Pescador".

### b. Centro competente de formalidad empresarial

El centro de formalidades empresariales (CFE) depende de la naturaleza de la actividad, de la forma jurídica de la empresa y del número de empleados:

- para los artesanos y las empresas comerciales dedicadas a la actividad artesanal, siempre que no empleen a más de diez empleados, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para los artesanos y las empresas comerciales que emplean a más de diez empleados, esta es la Cámara de Comercio e Industria (CCI);
- en los departamentos de la Parte Alta y Baja del Rin, la Cámara de Comercio de Alsacia es competente.

**Tenga en cuenta que**

En los departamentos del Bajo Rin, el Alto Rin y el Mosela, la actividad sigue siendo artesanal, independientemente del número de empleados empleados, siempre y cuando la empresa no utilice un proceso industrial. Por lo tanto, la CFE competente es la CMA o la Cámara de Comercio de Alsacia. Para más información, es aconsejable consultar el[web oficial de la Cámara de Comercio de Alsacia](http://www.cm-alsace.fr/decouvrir-la-cma/lartisanat-en-alsace).

*Para ir más allá* Artículo R. 123-3 del Código de Comercio.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Sólo una persona cualificada que esté cualificada profesionalmente o puesta bajo el control efectivo y permanente de una persona cualificada puede llevar a cabo la actividad de pesca.

Las personas que se dedican a la actividad de la pescadería o que controlan el ejercicio de las pescaderías deben:

- poseer un Certificado de Cualificación Profesional (CAP), un Certificado de Estudios Profesionales (BEP) o un diploma o un nivel superior certificado o registrado en el momento de su emisión en el [directorio nacional de certificaciones profesionales](http://www.rncp.cncp.gouv.fr/) (RNCP);
- o justificar una experiencia profesional efectiva de tres años en el territorio de la Unión Europea (UE) u otro Estado parte en el acuerdo del Espacio Económico Europeo (EEE) adquirido como gerente de empresa, trabajador por cuenta propia o empleada en el cumplimiento de su deber.

Los diplomas para la práctica de la actividad de la pesca de pesca incluyen:

- Cap especialidad "pescador";
- licenciatura profesional en pescadería-escaly-procesadores.

Para obtener más información sobre cada uno de estos diplomas, es aconsejable consultar las disposiciones de la decreto de 24 de junio de 2009 la creación de la especialidad del "procesador de escamas de pescado" de la licenciatura profesional y el establecimiento de sus condiciones para la emisión y decreto del 20 de marzo de 2007 la creación del certificado "fishmonger" de aptitud profesional.

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 1 del Decreto 98-246, de 2 de abril de 1998.

### b. Cualificaciones profesionales - Nacionales europeos

#### Para una entrega de servicio gratuita (LPS)

El profesional que sea miembro de la UE o del EEE podrá trabajar en Francia de forma temporal y ocasional como pescadero, siempre que esté legalmente establecido en uno de estos Estados para llevar a cabo la misma actividad.

Además, cuando esta actividad o formación no esté regulada en el Estado de Establecimiento, debe haber sido pescadero durante al menos dos años en los diez años anteriores a la prestación que pretende. para lograr en Francia.

**Tenga en cuenta que**

El profesional que cumpla estas condiciones está exento de los requisitos de registro del directorio de operaciones (RM) o del registro de empresas.

*Para ir más allá* Artículo 17-1 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 2 del Decreto 98-246, de 2 de abril de 1998.

#### Para un establecimiento gratuito (LE)

Para llevar a cabo una actividad de pesca de pesca en Francia de forma permanente, el profesional nacional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba "2 grados). a. Cualificaciones profesionales");
- poseer un certificado de competencia o título de formación requerido para la actividad de la pescadería en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su disposición a llevar a cabo la actividad de pesca cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de Esta actividad
- obtener un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona haya sido pescada en el Estado miembro que haya admitido durante tres años Equivalencia.

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones anteriores podrá solicitar un certificado de reconocimiento de cualificaciones profesionales (véase más adelante «3»). b. Si es necesario, solicite un certificado de cualificación profesional").

Si la persona no cumple con cualquiera de las condiciones anteriores, la CMA puede pedirle que realice una medida de compensación en los siguientes casos:

- si la duración de la formación certificada es al menos un año inferior a la requerida para obtener una de las cualificaciones profesionales requeridas en Francia para llevar a cabo la actividad de la pescadería;
- Si la formación recibida abarca temas sustancialmente diferentes de los cubiertos por uno de los títulos o diplomas requeridos para trabajar como pescadero en Francia;
- Si el control efectivo y permanente de la actividad de la pescadería requiere, para el ejercicio de algunas de sus competencias, una formación específica que no esté prevista en el Estado miembro de origen y que abarque temas sustancialmente diferentes de los cubiertos por el certificado de competencia o designación de formación mencionado por el solicitante.

La medida de compensación consiste, a elección del solicitante, en un curso de ajuste o en una prueba de aptitud (véase la infra "3 c). Si es necesario, solicite un certificado de cualificación profesional").

*Para ir más allá* Artículos 16 y 17 de la Ley 96-603, de 5 de julio de 1996; Artículos 1 y 3 del Decreto 98-246, de 2 de abril de 1998.

### c. Condiciones de honorabilidad e incompatibilidad

Para llevar a cabo la actividad en Francia, la pescadería no debe estar bajo la influencia:

- prohibición de hacerlo (esta prohibición se aplica hasta cinco años);
- prohibición de dirigir, administrar, administrar o controlar, directa o indirectamente, cualquier empresa comercial o artesanal, granja o corporación.

*Para ir más allá* Artículo 19 de la Ley 96-603, de 5 de julio de 1996, supra; Artículo 131-6 del Código Penal; Artículo L. 653-8 del Código de Comercio.

### d. Algunas peculiaridades de la regulación de la actividad

#### Reglamento sobre la calidad del artesano y los títulos de maestro artesano y mejor trabajador en Francia

##### Artesanía

Las personas que justifican:

- ya sea una PAC, un BEP o un título certificado o registrado cuando se emitió al RNCP al menos un nivel equivalente (véase supra "2." a. Cualificaciones profesionales");
- o experiencia laboral en este comercio durante al menos tres años.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

##### El título de maestro artesano

Este título se otorga a las personas, incluidos los líderes sociales de las personas jurídicas:

- Registrado en el directorio trades;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

**Tenga en cuenta que**

Las personas que no poseen el máster pueden solicitar a la Comisión Regional de Cualificaciones el título de Maestro Artesano en dos situaciones:

- cuando están inscritos en el directorio de oficios, tienen un título al menos equivalente al máster, y justifican la gestión y los conocimientos psicopedagógicos equivalentes a los de las correspondientes unidades de valor del máster y que tienen dos años de práctica profesional;
- cuando se han inscrito en el repertorio de comercio durante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

##### El título de mejor trabajador de Francia (MOF)

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en el ámbito artesanal, comercial, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación. Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

Para obtener más información, se recomienda que consulte[web oficial del concurso "uno de los mejores trabajadores de Francia"](http://www.meilleursouvriersdefrance.info/).

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

#### Cumplir con las regulaciones de las instituciones públicas (ERP)

La pesca de pesca debe cumplir con las normas de seguridad y accesibilidad para los ERP. Para obtener más información, es aconsejable consultar la hoja de actividades[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

#### Cumplir con las normas sanitarias

La pesca de pesca debe aplicar las normas del REGLAMENTO 852/2004, de 29 de abril de 2004, relativas a:

- Las instalaciones utilizadas para la alimentación;
- Equipo
- Desperdicio de alimentos
- Suministro de agua
- Higiene personal
- alimentos, envases y envases.

También debe respetar los principios del Punto de Control Crítico de Análisis de Peligros (HACCP) del Reglamento anterior.

La pescadería también debe aplicar las normas establecidas en el decreto de 21 de diciembre de 2009 que especifican las temperaturas de conservación de los productos animales perecederos. Estos incluyen:

- Condiciones de temperatura de conservación
- Procedimientos de congelación
- cómo se preparan las carnes molidas y se recibe la caza.

Por último, debe respetar las temperaturas de conservación de los productos alimenticios perecederos de origen vegetal.

*Para ir más allá* : Reglamento de 29 de abril de 2004 y Decreto de 21 de diciembre de 2009 relativo a las normas sanitarias aplicables a las actividades de venta al por menor, almacenamiento y transporte de productos animales y alimentos en contenedores; Orden de 8 de octubre de 2013 sobre normas sanitarias para la venta al por menor, almacenamiento y transporte de productos y productos alimenticios distintos de los productos animales y los alimentos en contenedores.

#### Ver precios e información obligatoria del producto

##### Precio

El pescador deberá informar al consumidor, mediante marcado, etiquetado, exhibición o cualquier otro proceso apropiado, sobre los precios y las condiciones específicas de la venta y ejecución de los servicios.

*Para ir más allá* Artículo L. 112-1 del Código del Consumidor.

**Tenga en cuenta que**

El pescadero determina libremente el precio de venta de sus productos, pero la venta con pérdida está prohibida.

*Para ir más allá* Artículos L. 410-2 y L. 442-2 del Código del Consumidor.

##### Identificación de lote

Un alimento sólo puede comercializarse si va acompañado de una referencia para identificar el lote al que pertenece. Los términos y condiciones varían dependiendo de si el producto está preenvasado o no.

*Para ir más allá* Artículos R. 412-3 a R. 412-10 del Código del Consumidor; Reglamento (UE) 1169/2011 del Parlamento Europeo y del Consejo, de 25 de octubre de 2011, relativo a la información al consumidor sobre los alimentos.

##### Origen de los productos

Los productos de pesca y acuicultura solo pueden ofrecerse a la venta al consumidor final o a la comunidad si la señalización o etiquetado apropiado indica:

- El nombre comercial de la especie y su nombre científico;
- el método de producción, en particular las siguientes referencias: "pescado" o "pescado en agua dulce" o "alto";
- La zona de captura o cría del producto y la categoría de artes de pesca utilizadas para la captura;
- excepto por una excepción, si el producto ha sido descongelarse;
- si es así, la fecha mínima de durabilidad.

Esta información se puede compartir utilizando información comercial como vallas publicitarias o carteles.

Para obtener más información, puede ver[Dirección General de Competencia, Consumo y Lucha contra el Fraude](http://www.economie.gouv.fr/dgccrf/linformation-des-consommateurs-sur-produits-peche-et-laquaculture) (DGCCRF).

*Para ir más allá* Reglamento (UE) 1379/2013 del Parlamento Europeo y del Consejo, de 11 de diciembre de 2013, por el que se establece una organización común de mercados en el sector de la pesca y la acuicultura.

##### Ingredientes reconocidos como alérgenos

El uso en la fabricación o preparación de un alimento de alimentos que cause alergias o intolerancias debe ser puesto en cuenta al consumidor final.

Esta información debe indicarse en el propio producto o cerca de él para que no haya incertidumbre sobre el producto al que se refiere.

*Para ir más allá* Artículos R. 412-12 a R. 412-16 del Código del Consumidor.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El pescador debe estar registrado en el directorio de Comercios y Oficios (RMA) y, si es necesario, en el Registro Mercantil y Corporativo (RCS). Para obtener más información, es aconsejable consultar las hojas de actividades "Formalidades de Reporte de Empresas Artesanales".

### b. Siga el curso de preparación de la instalación (SPI)

Antes de inscribirse en el directorio de comercios o, para los departamentos del Bajo Rin, el Alto Rin y el Mosela, en el registro de empresas, el futuro empresario debe haber seguido a un SPI.

**Tenga en cuenta que**

Si una causa de fuerza mayor lo impide, el profesional puede cumplir con su obligación en el plazo de un año a partir de su inscripción o registro.

El SPI se compone de:

- una primera parte dedicada a la introducción a la contabilidad general y la contabilidad analítica, así como a la información sobre el entorno económico, jurídico y social de las actividades artesanales y sobre la responsabilidad social y social y cuestiones ambientales;
- una segunda parte que incluye un período de seguimiento posterior al registro.

El propósito de las prácticas es, a través de cursos y trabajos prácticos, permitir a los futuros artesanos conocer las condiciones de su instalación, problemas de financiación, técnicas para la previsión y control de su funcionamiento, medir los conocimientos necesarias para la sostenibilidad de su negocio e informarles sobre las oportunidades de educación continua adaptadas a su situación.

La cámara de oficios, el establecimiento o el centro incautado por una solicitud de prácticas está obligada a iniciar la pasantía en un plazo de treinta días. Transcurrido este plazo, el registro del futuro empresario no puede ser denegado ni diferido, sin perjuicio de las demás obligaciones que condicionen el registro.

#### Gastos de pasantía

El futuro empresario puede ser excusado de seguir al SPI:

- si tiene un grado o título de nivel 3 certificado con un título en economía y gestión empresarial o un título de maestría de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento al menos equivalente al proporcionado por la pasantía.

#### Exención de pasantías para nacionales de la UE

Para establecerse en Francia, un profesional cualificado nacional de la UE o del EEE está exento de seguir el SPI.

La CMA podrá pedir a la CMA que se someta a una prueba de aptitud o a un curso de ajuste cuando el examen de sus cualificaciones profesionales revele que el nacional no ha recibido formación de gestión o que su formación se relaciona con materiales sustancialmente diferentes de los cubiertos por el SPI.

Sin embargo, la CMA no puede pedir al profesional que someta a tal medida de compensación:

- cuando ha estado involucrado en una actividad profesional que requiere al menos el mismo nivel de conocimiento que el SPI durante al menos tres años;
- cuando, durante al menos dos años consecutivos, ha trabajado de forma independiente o como líder empresarial, después de recibir formación para esta actividad sancionada por un diploma, título o certificado reconocido por el Estado, miembro o parte, que lo haya expedido o considerado plenamente válido por un organismo profesional competente;
- cuando, tras la verificación, la junta constata que los conocimientos adquiridos por el solicitante durante su experiencia profesional en un Estado miembro o parte, o en un tercer Estado, pueden abarcar, total o parcialmente, la diferencia sustancial en términos de contenido.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, sobre la formación profesional de los artesanos; Artículos 6 y 6-1 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos para la aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

#### Condiciones de exención

La solicitud de exención debe dirigirse al Presidente de la ACM Regional.

El solicitante debe acompañar su correo:

- Copia del diploma aprobado por el grado III;
- Copia del máster
- prueba de una actividad profesional que requiere un nivel equivalente de conocimiento.

Si la exención no se responde en el plazo de un mes a partir de la recepción de la solicitud, se considerará que se concede la exención.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, sobre la formación profesional de los artesanos; Artículos 1 a 7 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos para la aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

### c. Si es necesario, solicite un certificado de cualificación profesional

El nacional de la UE o del EEE podrá solicitar un certificado de cualificación profesional para ejercer un control efectivo y permanente sobre la actividad de la pescadería.

#### Autoridad competente

La solicitud debe dirigirse a la ACM regional o a la cámara regional de comercios y artesanías en la que el nacional desee ejercer.

#### Procedimiento y tiempo de respuesta

La junta emite un recibo indicando la fecha de recepción de la solicitud completa en el plazo de un mes a partir de la recepción.

En el caso de una solicitud incompleta, notifica al solicitante de la lista de documentos faltantes dentro de los 15 días posteriores a la recepción y emite el recibo tan pronto como se complete el archivo.

A falta de notificación de la decisión de la junta en el plazo de cuatro meses a partir de la recepción de la solicitud completa, se considerará que el reconocimiento de la cualificación profesional se adquiere al solicitante.

La junta podrá, por decisión motivada:

- expedir un certificado de reconocimiento de la cualificación profesional cuando reconozca la cualificación profesional;
- informar por escrito que se requiere una compensación.

#### Documentos de apoyo

La solicitud de certificación de cualificación profesional deberá ir acompañada de:

- formación profesional, certificado de competencia o certificado de actividad (véase supra "2.2). b. Cualificaciones profesionales - nacionales europeos");
- Prueba de la nacionalidad del solicitante;
- documentos no redactados en francés, una traducción certificada de acuerdo con el original por un traductor jurado o autorizado.

La CMA podrá solicitar la divulgación de información adicional sobre el nivel, la duración y el contenido de su formación para que pueda determinar la posible existencia de diferencias sustanciales con la formación francesa requerida.

También podrá solicitar pruebas por cualquier medio de experiencia profesional que pueda compensar, total o parcialmente, la diferencia sustancial.

#### Medidas de compensación

La CMA, que solicita el reconocimiento de la cualificación profesional, puede decidir someter al solicitante a una medida de compensación. En su decisión se enumeran los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son esenciales para llevar a cabo la actividad de pesca. Sólo estas asignaturas pueden ser sometidas a la prueba de aptitud o al curso de adaptación, que no puede durar más de tres años.

El solicitante informa a la CMA de su elección para tomar un curso de ajuste o para tomar una prueba de aptitud.

**La prueba de aptitud** toma la forma de un examen de panel. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final de la prueba de aptitud, la junta emite un certificado de cualificación profesional al solicitante exitoso dentro de un mes.

Si el solicitante decide realizar una**curso de adaptación**, la CMA le envía una lista de todas las organizaciones que pueden organizar esta pasantía, en el plazo de un mes a partir de la recepción de la decisión del solicitante. En caso contrario, el reconocimiento de la cualificación profesional se considera adquirido y el consejo establece un certificado de cualificación profesional.

Al final del curso de ajuste, el solicitante envía a la junta un certificado que certifica que ha completado esta pasantía, acompañado de una evaluación de la organización que lo organizó. Sobre la base de esta certificación, el consejo de administración emite un certificado de cualificación profesional al interesado en el plazo de un mes.

**Tenga en cuenta que**

Cualquier acción legal contra la decisión de la CMA de solicitar una indemnización va precedida, por inadmisibilidad, por un recurso administrativo ejercido, en el plazo de dos meses a partir de la notificación de dicha decisión, con la prefecto del departamento donde tiene su sede la cámara.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-146, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad otro Estado parte en el Acuerdo sobre el EEE.

### d. Hacer una declaración en caso de preparación o venta de productos animales o animales

Cualquier operador de un establecimiento que produzca, manipule o almacene productos animales o alimenticios que contengan ingredientes animales (carne, productos lácteos, productos pesqueros, huevos, miel), destinados al consumo debe cumplir con la obligación de declarar cada uno de los establecimientos de los que es responsable, así como las actividades que se llevan a cabo allí.

La declaración deberá efectuarse antes de la apertura del establecimiento y renovarse en caso de cambio de operador, dirección o naturaleza de la actividad.

#### Autoridad competente

La declaración debe dirigirse al prefecto del departamento de aplicación del establecimiento o, en el caso de los establecimientos bajo la autoridad o tutela del Ministro de Defensa, al servicio de salud de las fuerzas armadas.

#### Documentos de apoyo 

El solicitante debe completar el formulario Cerfa 13984*03.

*Para ir más allá* Artículos R. 231-4 y R. 233-4 del Código Rural y de la pesca marina; Artículo 2 del Decreto de 28 de junio de 1994 sobre la identificación y acreditación sanitaria de establecimientos en el mercado de productos animales o animales y marcas de seguridad.

### e. Si es necesario, solicitar la aprobación sanitaria en caso de venta de alimentos a intermediarios

Los establecimientos que preparen, transforman, manipulan o almacenen productos animales para el consumo humano (leche cruda, carne, huevos, productos pesqueros, miel) deben obtener una aprobación sanitaria para comercializar sus productos productos de otros establecimientos.

#### Autoridad competente

La solicitud de acreditación debe dirigirse a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) en la ubicación del establecimiento.

#### Tiempo de respuesta

Si la solicitud de acreditación es completa y admisible, se concede una aprobación condicional por un período de tres meses al solicitante.

Durante este período, se realiza un cheque oficial. Si las conclusiones de esta visita son favorables, se concede la aprobación definitiva. De lo contrario, los puntos de incumplimiento se notifican al solicitante y la aprobación condicional podrá renovarse por otros tres meses. La duración total de la aprobación condicional no podrá exceder de seis meses.

#### Documentos de apoyo

La solicitud de acreditación sanitaria se realiza utilizando el Formulario Cerfa 13983Completado, fechado y firmado y acompañado de todos los documentos justificativos enumerados por el[nota DGAL/SDSSA/N2012-8119](http://agriculture.gouv.fr/ministere/note-de-service-dgalsdssan2012-8119-du-12062012) 12 de junio de 2012 relativo al proceso de acreditación y a la composición del expediente.

Estos documentos se refieren a la presentación de la empresa, la descripción de sus actividades y el plan de gestión de la salud.

**Es bueno saber**

Es aconsejable consultar con el DCSPP para asegurar la necesidad de solicitar la acreditación y, si es necesario, para obtener asistencia en el establecimiento del expediente de solicitud.

*Para ir más allá* Artículos L. 233-2 y R. 233-1 del Código Rural y de la pesca marina; Orden de 8 de junio de 2006 sobre la acreditación sanitaria de los establecimientos que comercialen productos o productos alimenticios animales que contengan productos animales; DGAL/SDSSA/N2012-8119 de 12 de junio de 2012 relativo al procedimiento de acreditación y a la composición del fichero.

### f. Si es necesario, obtenga una exención del requisito de acreditación de la salud

Los establecimientos afectados por la exención son las tiendas:

- ceding cantidades limitadas de alimentos a otros puntos de venta;
- las distancias de entrega a otros puntos de venta no superan los 80 kilómetros.

Las categorías de productos abarcadas por la exención incluyen:

- sin procesar (refrigerado o congelado, preparado o entero) o procesado (salado, ahumado, cocido, etc.);
- productos lácteos, leches tratadas térmicamente, productos de huevo de cáscara y/o leche cruda que han sido sometidos a un tratamiento desinfectante;
- comidas pre-preparadas o preparaciones de cocina que son el plato principal de una comida.

El pescadero puede obtener esta exención cuando la distancia con los establecimientos entregados no exceda de 80 km (o más por decisión de la prefectura) y si cede hasta el máximo:

- 250 kg por semana cuando vende el 30% o menos de su producción total;
- 100 kg si vende más del 30% de su producción total.

**Tenga en cuenta que**

El límite de entrega y los límites cuantitativos en virtud de la exención del requisito de acreditación sanitaria no se aplican en el caso de la transferencia gratuita de alimentos a establecimientos de caridad.

**Es bueno saber**

Se debe enviar una nueva declaración al SDCSPP cuando se realicen cambios significativos en los productos o cantidades entregados, excepto en el contexto de la donación de alimentos o de los establecimientos entregados.

*Para ir más allá* :[DGAL/SDSSA/2014-823 instrucción técnica](https://info.agriculture.gouv.fr/gedei/site/bo-agri/instruction-2014-823) 10 de octubre de 2014.

#### Autoridad competente

La solicitud de exención debe dirigirse al DDCSPP del departamento en el que se encuentra la institución.

#### Documentos de apoyo

La solicitud de exención de la subvención se realiza utilizando el formulario Cerfa 13982Completado, fechado y firmado.

#### Costo

Gratis

*Para ir más allá* Artículo L. 233-2 del Código Rural y Pesca Marina; Artículos 12, 13, Anexos 3 y 4 de la Orden de 8 de junio de 2006 sobre la Acreditación Sanitaria de establecimientos que ofrezcan productos animales o alimenticios que contengan productos animales;[DGAL/SDSSA/2014-823 instrucción técnica](https://info.agriculture.gouv.fr/gedei/site/bo-agri/instruction-2014-823) 10 de octubre de 2014.

