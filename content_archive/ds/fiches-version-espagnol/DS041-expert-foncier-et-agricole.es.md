﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS041" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Experiencia" -->
<!-- var(title)="Experto en tierra y agricultura" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="experiencia" -->
<!-- var(title-short)="experto-en-tierra-y-agricultura" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/experiencia/experto-en-tierra-y-agricultura.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="experto-en-tierra-y-agricultura" -->
<!-- var(translation)="Auto" -->


Experto en tierra y agricultura
===============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El experto en tierra y agricultura es un profesional que lleva a cabo misiones de especialización en tierra y agricultura que se ocupan de la propiedad y los edificios.

Es particularmente activo en las siguientes áreas:

- estimación inmobiliaria y rural;
- proyectos de redacción y asesoramiento jurídico;
- uso de la tierra y gestión ambiental sostenible (asesoramiento, apoyo);
- producción agropecua-animal o vegetal;
- Estimación de la granja y sus componentes;
- estimación del daño.

*Para ir más allá* Artículo L. 171-1 del Código Rural y Pesca Marina.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para una profesión liberal, la CFE competente es el Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

La práctica de la profesión de experto en tierra y agricultura está reservada a los profesionales de la lista nacional de expertos en tierra y agrícolas.

El registro está abierto a la persona que cumpla con las siguientes condiciones:

- justificar una práctica profesional:- al menos siete años,
  - o al menos tres años, para los titulares de un título de al menos cuatro años de educación postsecundaria, en las disciplinas agrícola, agronómica, ambiental, forestal, jurídica o económica;
- no haber sido condenado por hechos contrarios al honor, la probidad o la buena moral en los últimos cinco años;
- no una sanción disciplinaria o administrativa por despido, despido, despido, retirada de la acreditación o autorización;
- no han sido golpeados con bancarrota personal.

En el caso de una actividad profesional de la sociedad civil (PCS), todos los socios deben tener el título de experto forestal.

En el caso del ejercicio de la actividad en un holding liberal (SEL), más del 50% del capital debe estar en manos de expertos forestales en ejercicio.

En el caso de una actividad de empresa comercial, al menos el 50 % del capital debe estar en manos de expertos forestales o antiguos expertos, siempre que no hayan sido cancelados.

*Para ir más allá* Artículos L. 171-1, R. 171-10 y R. 173-58 del Código Rural y Pesca Marina.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Para un ejercicio temporal e informal (Entrega de servicio gratuito)

El nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) puede utilizar su título profesional en Francia, ya sea temporal u ocasionalmente, sujeto a:

- estar legalmente establecido en uno de estos Estados para ejercer como experto en tierra y agrícola;
- cuando ni la profesión ni la formación estén reguladas en ese Estado para haber ejercido como experto en tierra y agricultura en ese Estado durante al menos un año en los últimos diez años;
- estar asegurado contra las consecuencias pecuniarias de su responsabilidad civil profesional.

Para ello, el nacional deberá solicitar, antes de su primer beneficio, mediante declaración dirigida al Consejo Nacional de ExperienciaS Terrestres, Agrícolas y Forestales (CNEFAF) (véase infra "3o. c. Si es necesario, hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)").

*Para ir más allá* Artículo L. 171-2 del Código Rural y Pesca Marina.

#### Para un ejercicio permanente (Establecimiento Libre)

Todo nacional de un Estado de la UE o del EEE, establecido y que practique legalmente como experto en tierras y agricultura en ese Estado, podrá llevar a cabo la misma actividad en Francia de forma permanente, siempre que se cumplan las mismas condiciones Profesional francés (véase supra "2.00. a. Cualificaciones profesionales").

Tendrá que solicitar al comité del CNEFAF su inclusión en la lista nacional de expertos en tierra y agricultura (véase infra "3o). b. Solicitar inclusión en la lista de expertos en tierra y agricultura").

Si existen diferencias sustanciales entre la formación y la experiencia profesional del nacional y las requeridas en Francia, el comité podrá decidir someterla a una medida de compensación.

*Para ir más allá* Artículos L. 171-3 y R. 717-10 del Código Rural y Pesca Marina.

### c. Algunas peculiaridades de la regulación de la actividad

#### Cumplimiento del Código de ética para expertos en tierras y agrícolas

Las disposiciones del Código de ética se imponen a todos los expertos en tierra y agrícolas que profisionen en Francia.

Como tal, el experto en tierra y agrícola se compromete a:

- Respetar la independencia necesaria para llevar a cabo la profesión;
- hacer una declaración imparcial;
- Respetar el secreto profesional
- abstenerse de cualquier práctica desleal hacia sus colegas.

*Para ir más allá* Artículos R. 172-1 a R. 172-10 del Código Rural y Pesca Marina.

#### Incompatibilidades

La profesión de experto en tierra y agricultura es incompatible:

- con las oficinas de los funcionarios públicos y ministeriales;
- con todas las funciones que pudieran menoscabar su independencia, en particular, la de adquirir bienes personales o inmobiliarios de manera habitual para su reventa (por ejemplo, comercio, comerciante de bienes, etc.).

*Para ir más allá* Artículo L. 171-1 del Código Rural y Pesca Marina.

#### Seguro

El experto liberal en tierra y agrícola debe conseguir un seguro de responsabilidad civil profesional.

Por otro lado, si ejerce como empleado, este seguro sólo es opcional. En este caso, le confunde al empresario la realización de dicho seguro para sus empleados por los actos realizados durante su actividad profesional.

*Para ir más allá* Artículo L. 171-1 párrafo 8 del Código Rural y Pesca Marina.

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Dado que las instalaciones están abiertas al público, el profesional debe cumplir con las normas relativas a las instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Es aconsejable consultar la hoja "Establecimiento que recibe al público" para obtener más información.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de su actividad, el contratista debe registrarse en el Registro Mercantil (RCS), hacer una declaración ante el Urssaf o en el Registro del Juzgado de lo Mercantil. Para más información, es aconsejable consultar con la CFE pertinente y en caso de la creación de una empresa comercial, consultar las hojas "Formalidad de informar de una empresa comercial" y "Registro de una empresa comercial individual en el RCS."

### b. Pedir inclusión en la lista de expertos en tierra y agricultura

**Autoridad competente**

El comité del CNEFAF es responsable de decidir sobre la solicitud de inclusión en la lista de expertos en tierra y agrícolas.

**Documentos de apoyo**

La solicitud se realiza enviando un archivo por correo certificado con acuse de recibo, incluidos los siguientes documentos justificativos:

- Una identificación válida
- Una copia del título o diploma de formación que permita al solicitante llevar a cabo su actividad;
- cualquier prueba que justifique el ejercicio de la profesión en Francia o, en su caso, en la UE o en el Estado del EEE;
- Un currículum que indica las actividades profesionales anteriores del candidato con las fechas y lugares del ejercicio;
- una justificación o, en su defecto, un compromiso de substitulación de una póliza de seguro de responsabilidad civil profesional;
- un extracto de antecedentes penales no 3 menos de tres meses de edad o cualquier documento equivalente expedido por la autoridad competente del Estado de la UE o el EEE de menos de tres meses;
- una declaración de honor o cualquier otra evidencia de que la persona está cumpliendo con las condiciones de honor;
- en caso necesario, una declaración de la actividad prevista en forma de empresa.

**Procedimiento**

Una vez recibido el expediente, el comité dispone de tres meses para informar al solicitante de su decisión de enumerarlo o no.

Sin embargo, cuando el solicitante es nacional de la UE o del EEE y existen diferencias sustanciales entre su formación profesional y experiencia y las requeridas en Francia, el comité puede someterlo a la opción, ya sea para un curso de ajuste o para una prueba de aptitud.

En cualquier caso, vale la pena aceptar la decisión el silencio guardado durante un período de tres meses.

La renovación de la solicitud está sujeta a la producción del certificado de seguro de responsabilidad profesional.

**El caso de las personas jurídicas**

CpS y SELs de tierras y conocimientos agrícolas deben incluirse en la lista nacional. La solicitud debe incluir los siguientes documentos justificativos:

- Una copia de los estatutos y, en su caso, del reglamento interno;
- Documentos de registro civil de los asociados;
- para THE SEL, la justificación de que los expertos en tierra y agrícolas que trabajan en la empresa cumplen las condiciones de práctica de la profesión, así como la distribución de capital entre los socios;
- CPS, los documentos necesarios para solicitar el registro de socios que aún no están en la lista nacional.

**Qué saber**

Deben registrarse antes de completar las formalidades con la CFE correspondiente.

*Para ir más allá* Artículos R. 171-10 a R. 171-13 del Código de Pesca Rural y Marina.

### c. Si es necesario, hacer una declaración previa de actividad para el nacional de la UE o del EEE que realice actividades temporales y ocasionales (LPS)

**Autoridad competente**

El CNEFAF es responsable de emitir la declaración previa de actividad.

**Documentos de apoyo**

La solicitud se realiza enviando un archivo a la autoridad correspondiente, incluidos los siguientes documentos justificativos:

- Id
- un certificado que justifique que está legalmente establecido en un Estado de la UE o del EEE;
- prueba de que el nacional ha llevado a cabo actividades de conocimientos agrícolas y de tierra durante al menos un año en los diez años anteriores;
- un certificado de seguro de responsabilidad civil.

**Qué saber**

Los documentos de apoyo deben estar escritos en francés o traducidos por un traductor certificado.

*Para ir más allá* Artículos R. 171-17- a R. 171-17-3, y R 173-57 del Código Rural y Pesca Marina.

