﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS027" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="acampada" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="acampada" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/acampada.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="acampada" -->
<!-- var(translation)="Auto" -->

Acampada
========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

Camping es una actividad turística que consiste en albergar tiendas de campaña, caravanas, residencias móviles de ocio y viviendas recreativas ligeras en un terreno construido específicamente. Pueden ser explotados permanentemente (durante todo el año) o estacionalmente y dar la bienvenida a una clientela que no elige vivir allí.

Esta tierra puede o no estar formada por sitios con equipos comunes.

*Para ir más allá* Artículos R. 331-1, D. 331-1-1 y los siguientes del Código de Turismo.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI);
- para una actividad agrícola, es la cámara de la agricultura.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El profesional que desee operar un camping no está sujeto a ningún requisito de diploma, sin embargo, debe:

- cumplir con las disposiciones:- relacionados con la ubicación del camping,
  - en términos de planificación urbana e inclusión en los paisajes;
- dependiendo del caso, ser licenciado o pre-informado.

*Para ir más allá* Artículos L. 443-1 y siguientes y Sección R. 421-18 del Código de Planificación.

#### Reglamento Nacional de Planificación

El profesional no podrá crear, instalar o configurar un camping en determinados lugares, entre ellos:

- costas marinas y sitios de interés general (histórico, artístico, científico, etc.);
- Dentro del perímetro de los sitios patrimoniales sobresalientes;
- en algunas áreas, siempre y cuando un plan de planificación urbana local lo disponga, y siempre y cuando dañe el orden público o el medio ambiente.

**Tenga en cuenta que**

Las autoridades pertinentes podrán emitir excepciones en función de la naturaleza del sitio protegido.

*Para ir más allá* Artículos R. 111-32 y los siguientes artículos del Código de Planificación; Artículos L. 341-1 y siguientes del Código de Medio Ambiente; Sección L. 631-1 del Código de Patrimonio.

#### Urbanismo e integración en los paisajes

Se requiere el profesional, para la disposición e instalación de su camping:

- Limitar el impacto visual de sus instalaciones;
- organizar su espacio de una manera que lo haga homogéneo, respetuoso con el medio ambiente y seguro.

*Para ir más allá* Artículos A. 111-7 y A. 111-8 del Código de Planificación.

#### Permisos de planificación o declaraciones anticipadas

El profesional está obligado a solicitar un permiso de planificación o una declaración previa a la instalación. Esta solicitud varía en función de la capacidad del camping:

- En el caso del desarrollo de un camping con una capacidad de hasta veinte personas o seis alojamientos, el profesional debe hacer una solicitud de declaración previa (ver infra "3. a. Predeclaración");
- En el caso del desarrollo de un terreno con una capacidad de más de veinte personas o hasta seis adaptaciones, el profesional debe solicitar un permiso de desarrollo (ver infra "3o. b. Solicitud de permisos de planificación").

Además, el profesional debe hacer una declaración en todos los casos que certifique la finalización y el cumplimiento de la obra (DAACT) (véase infra "3.3. c. Declaración de finalización y cumplimiento del trabajo (DAACT)) ").

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE) opere un camping de forma temporal e informal o permanente en Francia.

Como tal, el nacional profesional de la UE está sujeto a los mismos requisitos profesionales que el nacional francés (véase más arriba "2. a. Cualificaciones profesionales").

### c. Algunas peculiaridades de la regulación de la actividad

**Impuesto de residencia**

Tan pronto como se prevé un impuesto de residencia en la comuna donde se encuentra el camping, el profesional debe cobrarlo a sus clientes y pagarlo a la autoridad local.

Para más detalles, es aconsejable consultar el[sitio web dedicado al impuesto de residencia](http://taxesejour.impots.gouv.fr/DTS_WEB/FR/).

*Para ir más allá* Artículo L. 2333-26 del Código General de Autoridades Locales.

**Requisitos de salud**

El profesional que opera un camping está sujeto al cumplimiento de los requisitos sanitarios mínimos relativos a:

- agua para el consumo y las aguas residuales;
- Recolección de residuos domésticos
- mantenimiento de las zonas comunes.

Si no se cumplen estos requisitos, el profesional incurre en el cierre temporal del camping y su evacuación.

*Para ir más allá* : decreto de 17 de julio de 1985 relativo a las condiciones sanitarias mínimas comunes a los terrenos desarrollados para la recepción de campistas y caravanas y a la tierra especialmente destinada al establecimiento de viviendas recreativas ligeras.

**Obligaciones de advertencia y evacuación**

El profesional que opera un camping está obligado a cumplir con los requisitos de información, alerta y evacuación establecidos por la autoridad que expidió el permiso de planificación o haber registrado su declaración anticipada (véase la infra "3" . Procedimientos y trámites de instalación").

Como tal, el profesional debe:

- Envíe un documento a la llegada de cada huésped de las directrices de seguridad del camping.
- Ver la información de seguridad a una velocidad de un cartel por cada 5.000 metros cuadrados;
- proporcionar al cliente un libro de requisitos de seguridad.

*Para ir más allá* Artículos R. 125-15 a R. 125-22 del Código de Medio Ambiente.

**Normas internas y avisos de información**

El profesional del camping debe tener un folleto de regulación e información de bye de acuerdo con los modelos establecidos en las Listas 1 y 2 de la Detenido 17 de febrero de 2014 sobre el requisito de campings o parques de caravanas, así como para que los parques recreativos residenciales tengan un modelo de regulación interna y un folleto informativo sobre las condiciones de alquiler de las parcelas año.

Estos documentos deben ponerse a disposición de los clientes.

*Para ir más allá* Artículo D. 331-1-1 del Código de Turismo; 17 de febrero de 2014 antes mencionado.

**Requisito de divulgación anticipada del cliente**

El profesional deberá entregar al cliente, antes de cualquier contrato de alquiler de una ubicación de su camping, toda la información relativa a su establecimiento y las condiciones de alquiler. Este documento debe mencionar todos los elementos del apéndice de la Detenido 22 de octubre de 2008 sobre información anticipada del consumidor sobre las características del alojamiento de alquiler en hoteles al aire libre.

*Para ir más allá* Artículo L. 113-3 del Código del Consumidor.

**Facturación**

El profesional está sujeto al cumplimiento de las obligaciones de facturación y debe emitir una nota al cliente en caso de un beneficio de un importe superior o igual a 25 euros. Esta nota debe incluir la siguiente información:

- Su fecha de escritura;
- El nombre y la dirección del proveedor
- El nombre del cliente (si el cliente no se opone a él);
- La fecha y el lugar de ejecución del servicio
- los detalles de cada beneficio, así como el importe total a pagar (todos los impuestos incluidos y excluidos los impuestos).

El profesional debe mantener el doble de estas calificaciones, clasificarlas cronológicamente y mantenerlas durante dos años.

*Para ir más allá* ( Orden 83-50/A, de 3 de octubre de 1983, relativa a la publicidad de precios de todos los servicios.

**Especificaciones en caso de zonas de alto riesgo**

Cuando la instalación se encuentra en una zona de riesgo (natural o tecnológica), el profesional debe establecer un sistema de información al cliente para garantizar la alerta, su información y su evacuación.

*Para ir más allá* Artículos L. 443-2 y R. 443-9 y siguientes del Código de Planificación.

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

Es aconsejable consultar la hoja "Establecimiento que recibe al público" para obtener más información.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

**Mantenimiento del camping**

El profesional debe mantener su establecimiento (zonas comunes, parcelas, vegetación, etc.).

En caso contrario, la autoridad competente podrá poner en conocimiento del profesional que lleve a cabo este mantenimiento y para llevar a cabo los ajustes y reparaciones necesarios.

*Para ir más allá* Artículo R. 480-7 del Código de Planificación.

**Solicitud de clasificación de camping**

El profesional puede solicitar una clasificación dentro de una categoría de establecimientos de acuerdo con los criterios establecidos por una tabla de clasificación elaborada por la agencia de desarrollo turístico en Francia ([Trump Francia](https://www.classement.atout-france.fr/)).

Para ello, el profesional debe dar, electrónicamente, a Trump France, un certificado de visita después de la evaluación de un organismo aprobado especificando el número de lugares de la tierra.

El camping puede clasificarse por cinco años en una de las siguientes categorías:

- "Turismo" si más de la mitad de los campos están destinados a alquiler para una clientela pasajera (alquiler por una noche, una semana o un mes);
- "Leisure" si más de la mitad de los sitios son para alquiler por más de un mes.

Además, el profesional que opera un camping clasificado deberá mostrar un cartel en la entrada de su establecimiento:

- cumplir con el Anexo 5 de la Detenido 22 de diciembre de 2010 si su establecimiento está clasificado en la categoría "Turismo";
- en cumplimiento del anexo de la Detenido 4 de noviembre de 2014 si su establecimiento está clasificado en la categoría "Espacio Natural".

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Pre-declaración

**Autoridad competente**

El profesional deberá presentar su solicitud en dos copias en un pliegue recomendado con notificación de recepción al ayuntamiento de la comuna en la que desee establecerse.

**Documentos de apoyo**

La solicitud debe contener:

- el Forma Cerfa No. 13404 completado y firmado;
- las partes mencionadas en el formulario, así como su comprobante de presentación.

**Resultado del procedimiento**

Al presentar la solicitud, la autoridad competente envía un número de registro a la solicitud indicando la fecha a partir de la cual pueden comenzar los trabajos. La autoridad también muestra la declaración y la naturaleza del proyecto en el ayuntamiento.

En caso de oposición al proyecto, se informa al solicitante por carta recomendada con notificación de recepción o electrónicamente. Sin embargo, la falta de respuesta durante el período de prueba esperado vale la pena permiso para llevar a cabo la actividad.

**Remedios**

El profesional tiene dos vías de recurso contra la decisión:

- recurso administrativo en un plazo de dos meses:- agraciado ante la autoridad competente,
  - jerárquicamente ante una autoridad superior;
- una impugnación legal al juez administrativo en un plazo de dos meses.

*Para ir más allá* Artículos R. 421-23 c), R. 423-1 y artículos subsiguientes, y artículos A. 441-1 y siguientes del Código de Planificación.

### b. Solicitud de permiso de planificación

**Autoridad competente**

El profesional deberá presentar su solicitud en cuatro copias en un pliegue recomendado con notificación de recepción al ayuntamiento del municipio en el que se planifiquen los alojamientos.

**Tenga en cuenta que**

En algunos casos, el prefecto puede ser responsable de expedir el permiso de planificación (véase el artículo R. 422-2 del Código de Planificación).

**Documentos de apoyo**

La solicitud debe contener:

- el Forma Cerfa No. 13409 completado y firmado;
- las partes mencionadas en el formulario, así como su comprobante de presentación.

**Resultado del procedimiento**

Cuando se presenta, el alcalde envía un número de registro al profesional y emite un recibo indicando la fecha en que se proporciona el permiso tácito. También publica un aviso de presentación de la solicitud en el ayuntamiento en un plazo de quince días a partir de la presentación de la solicitud en la que se especifica la naturaleza del proyecto.

El alcalde dispone de tres meses para tomar su decisión y notificar al profesional por carta recomendada con notificación de recibo o electrónicamente. La falta de respuesta durante el período de prueba es el resultado de la aceptación de la solicitud de permiso.

**Tenga en cuenta que**

El período de prueba puede organizarse en algunos casos (véanse los artículos R. 423-24 a R. 423-33 del Código de Planificación).

**Remedios**

La decisión de la autoridad competente puede estar sujeta a los mismos recursos que los previstos para la declaración previa (véase supra "3.0). a. Remedios").

*Para ir más allá* Artículos R. 421-19 c), R. 423-1 y artículos subsiguientes, y artículos A. 441-4 y siguientes del Código de Planificación.

### c. Declaración de Cumplimiento y Finalización (DAACT)

El profesional con licencia o pre-informado sólo puede comenzar el trabajo después de completar el DAACT.

**Autoridad competente**

La autoridad competente es la que expidió el permiso o había registrado la declaración previa.

**Documentos de apoyo**

La solicitud debe contener:

- el Forma Cerfa No. 13408 completado y firmado;
- dependiendo del caso:
  - un certificado de que la obra cumple con las normas de accesibilidad aplicables a los establecimientos que reciben el público,
  - un certificado emitido por un controlador técnico en caso de riesgos sísmicos.

*Para ir más allá* Artículo R. 443-8 del Código de Planificación; Secciones R. 511-19-27 y L. 112-19 del Código de Construcción y Vivienda.

### d. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de la empresa, el contratista debe informar de su actividad a la Cámara de Agricultura o inscribirse en el Registro de Comercio y Sociedades (RCS).

Es aconsejable referirse a las "Formalidades de Informar de una Empresa Comercial" y "Registro de una Empresa Comercial Individual a la SCN" y a la[Cámara de Agricultura](http://www.chambres-agriculture.fr/) para obtener más información.

### e. Si es necesario, registre los estatutos de la empresa

El explotador del camping deberá, una vez fechados y firmados los estatutos de la sociedad, registrarlos en la oficina del impuesto de sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.