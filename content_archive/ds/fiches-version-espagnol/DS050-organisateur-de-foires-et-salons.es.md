﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS050" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Organizador de ferias y ferias" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="organizador-de-ferias-y-ferias" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/organizador-de-ferias-y-ferias.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="organizador-de-ferias-y-ferias" -->
<!-- var(translation)="Auto" -->

Organizador de ferias y ferias
==============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

El organizador de ferias y ferias es un profesional cuya actividad es garantizar la promoción y gestión de un evento comercial. También es responsable de proporcionar ubicaciones para los profesionales que planean exponer y ofrecer sus productos y servicios durante el evento.

Estos eventos comerciales colectivos y temporales pueden ser:

- las denominadas ferias, dedicadas a una sucursal profesional específica, cuyos expositores pueden vender sus productos in situ para el uso personal del comprador;
- ferias abiertas al público donde los profesionales ofrecen sus productos para la venta directa;
- ferias en las que los expositores ofrecen sus bienes para la venta directa con la retirada de los bienes.

**Tenga en cuenta que**

Estas actividades se pueden realizar en parques de exposiciones equipados con instalaciones permanentes y albergando expositores durante todo el año. Sin embargo, estos parques expositivos están sujetos a regulaciones específicas que no se abordarán en esta tarjeta.

*Para ir más allá* Artículo L. 762-2 del Código de Comercio.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las empresas individuales, la CFE pertinente es la Urssaf;
- Para las actividades comerciales, es la Cámara de Comercio e Industria (CCI);
- en el caso de la creación de una sociedad civil, la CFE competente es el registro del Tribunal de Comercio, o el registro del tribunal de distrito en los departamentos del Bajo Rin, el Alto Rin y el Mosela.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Con el fin de llevar a cabo la actividad de organización de ferias y ferias, el profesional no está sujeto a ningún requisito de diploma. Sin embargo, debe hacer una declaración previa antes de cada evento comercial, cuyo carácter depende de la estructura en la que tenga lugar la transacción.

#### En el caso de un evento comercial dentro de un parque de exposiciones registrado

Cuando el evento comercial se celebra dentro de uno de estos parques, el organizador de ferias y ferias deberá proporcionar al operador del lugar la información necesaria para inscribir el evento en la declaración de la programa anual de parques, que debe tener lugar antes del comienzo del último trimestre del año anterior. En caso contrario, la declaración se mantiene directamente (véase infra "3." a. Predeclaración").

Como tal, debe proporcionar:

- Las características numéricas del evento (como estimación para la primera sesión):- La superficie cuadrada,
  - El número de expositores y visitas
  - El número de visitantes a ferias, incluyendo, de forma opcional, el número de visitantes extranjeros,
  - opcional, el número y el espacio neto ocupados por expositores extranjeros;
- elementos identificativos (nombre, dirección, número de Siret, sitio web, etc.).

Cuando el evento ya ha sido objeto de una sesión anterior, el profesional deberá proporcionar las cifras controladas para esa última sesión.

Todos estos datos cifrados deben ser supervisados por una agencia acreditada por el Comité de Acreditación de Francia[Cofrac](https://www.cofrac.fr/). Sin embargo, cuando la superficie neta del evento es inferior a 1.000 m2, este control puede ser llevado a cabo por el propio operador del parque.

*Para ir más allá* Artículos R. 762-4 y 762-5 del Código de Comercio; Artículo A. 762-3 del Código de Comercio; Apéndice II del Apéndice 7-10 del Código de Comercio.

#### En el caso de un evento comercial fuera de un parque expositivo

Cuando el evento tipo feria tiene lugar fuera de cualquier parque expositivo, su organizador debe hacer la predeclaración directamente (ver infra "3." a. Predeclaración").

*Para ir más allá* Artículo R. 762-10 del Código de Comercio.

### b. Cualificaciones profesionales - Nacionales Europeos (Servicio Gratuito o Establecimiento Libre)

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE) lleve a cabo la actividad de organización de ferias y ferias con carácter temporal y casual y permanente en Francia.

Como tal, el profesional nacional eu está sujeto a los mismos requisitos profesionales que el nacional francés (véase más arriba "2o. a. Cualificaciones profesionales").

### c. Algunas peculiaridades de la regulación de la actividad

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

Es aconsejable consultar la hoja "Establecimiento que recibe al público" para obtener más información.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales de las normas de seguridad contra incendios y pánico en las instituciones públicas.

**Protesta por venta de mascotas**

El organizador de un evento comercial dedicado a la venta de mascotas deberá hacer una declaración previa al prefecto del departamento donde tendrá lugar el evento. Como tal, también tendrá que garantizar el cumplimiento de las disposiciones sanitarias y de protección animal.

**Tenga en cuenta que**

La venta o venta de mascotas de forma gratuita en eventos comerciales solo está permitida si estos eventos están dedicados exclusivamente a animales.

*Para ir más allá* Artículos L. 214-7 y R. 214-31 y R. 214-31-1 del Código Rural y Pesca Marina.

**Provisiones para puntos de venta temporales de bebidas**

El profesional que desee abrir un punto de venta temporal de bebidas durante la feria o feria debe solicitar el permiso desde el ayuntamiento del lugar del evento (o a la prefectura de policía si tiene lugar en París).

**Tenga en cuenta que**

Sólo se permitirá la venta de bebidas de los grupos uno y tres de la clasificación de bebidas (ver Sección L. 3321-1 del Código de Salud Pública).

*Para ir más allá* L. 3334-2 del Código de Salud Pública.

**Transmisión musical**

El organizador que desee transmitir música durante su evento, debe solicitar en línea la autorización previa a la Sociedad de Compositores y Editores de Música (Sacem) y pagar el monto de la regalía. El profesional debe registrarse y cumpliser el formulario de autorización en línea en el[Sacem]((%3Chttps://clients.sacem.fr/autorisations%3E)).

**Precio máximo de los bienes vendidos en una feria**

El profesional que organiza una feria debe garantizar que el valor de los productos ofrecidos a la venta en el establecimiento no exceda de 80 euros todos los impuestos incluidos (TTC).

Tan pronto como se supere esta cantidad, el evento será reclasificado como una "sala pública" o "venta de desembalaje".

*Para ir más allá* Artículo D. 762-13 del Código de Comercio.

**Disposiciones específicas para la venta de muebles usados**

En el caso de la venta de dichos artículos durante un evento comercial, el profesional debe mantener un registro para identificar a los vendedores.

Este registro debe mencionar:

- La identidad de cada vendedor profesional
- En el caso de un vendedor no profesional, un documento que acredite el honor de no participar en otros dos eventos comerciales durante el año;
- cuando se trata de una persona jurídica, los elementos para identificarla y la identidad y calidad de su representante.

Este registro debe estar a disposición de los servicios de policía, gendarmería, aduanas y competencia, de consumo y de lucha contra el fraude durante la manifestación.

**Tenga en cuenta que**

Si no se cumple esta obligación o si hay menciones inexactas en el registro, el profesional se enfrenta a una pena de seis meses de prisión y una multa de 30.000 euros.

Un modelo de este registro se adjunta a la Apéndice II del decreto de 21 de julio de 1992 por el que se establecen los modelos de registros previstos en el Decreto No 8861040, de 14 de noviembre de 1988, relativo a la venta o el intercambio de determinados objetos.

*Para ir más allá* Artículos R. 321-9 a R. 321-12 del Código Penal.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Predeclaración (excluyendo el programa anual de parques de exposiciones)

**Autoridad competente**

El organizador de ferias y ferias deberá enviar su declaración electrónicamente al prefecto del departamento en el que el evento se llevará a cabo al menos dos meses antes de su inicio, utilizando el[servicio de teledeclaración](https://www.foiresetsalons.entreprises.gouv.fr/teleprocedure.php).

*Para ir más allá* : service-public.fr artículo del sitio web sobre el[organización de ferias y ferias](https://www.service-public.fr/professionnels-entreprises/vosdroits/F22688).

**Resultado del procedimiento**

Dentro de una quincena después de recibir la solicitud, el prefecto debe:

- informar al solicitante de que su expediente está incompleto. Este último tiene diez días para completarlo;
- para proporcionarle, por medio de la desmaterialización, un recibo en caso de un recibo completo.

Si se modifica alguno de los elementos de la declaración, el profesional debe enviar un declaración rectificativa al prefecto electrónicamente a través de la[servicio de teledeclaración](https://www.foiresetsalons.entreprises.gouv.fr/teleprocedure.php).

*Para ir más allá* Artículos A. 762-4, A. 762-8 y Apéndices IV, V y XI de la Lista 7-10 del Código de Comercio.

### b. Formalidades de notificación de la empresa

**Autoridad competente**

El organizador de ferias y ferias debe hacer la declaración de su empresa, y para ello, debe hacer una declaración a la CPI.

**Documentos de apoyo**

El interesado debe proporcionar la documentos de apoyo dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI envía un recibo al profesional el mismo día mencionando los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### c. Si es necesario, registre los estatutos de la empresa

El organizador de ferias y ferias deberá, una vez fechados y firmados los estatutos de la sociedad, inscribirlos en la Oficina del Impuesto sobre Sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

