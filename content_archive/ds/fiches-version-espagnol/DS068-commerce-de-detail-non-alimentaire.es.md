﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS068" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Venta al por menor de productos no alimentarios" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="venta-al-por-menor-de-productos" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/venta-al-por-menor-de-productos-no-alimentarios.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="venta-al-por-menor-de-productos-no-alimentarios" -->
<!-- var(translation)="Auto" -->


Venta al por menor de productos no alimentarios
===============================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La venta al por menor no alimentaria consiste en la compra de productos nuevos o usados para la venta a particulares. Esta actividad se realiza en tiendas, mercados o también a través de Internet.

Los productos no se procesan hasta que se revenden.

### b. Centro competente de formalidad empresarial

El Centro de Formalidades Comerciales (CFE) correspondiente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Cualificaciones profesionales

No se requiere un diploma específico para la operación de un negocio minorista no alimentario. Sin embargo, se aconseja al individuo tener conocimiento de contabilidad y gestión.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) no está sujeto a ningún requisito de cualificación o certificación, al igual que los franceses.

### c. Algunas peculiaridades de la regulación de la actividad

#### Visualización de precios

El operador de una empresa minorista no alimentaria debe informar al público de los precios cobrados, utilizando la vía de marcado, etiquetado, exhibición o cualquier otro proceso apropiado.

Cada producto debe tener un precio unitario y un precio de venta.

**Tenga en cuenta que**

Para determinados productos (zapatos, productos de cuero, muebles, etc.), el etiquetado debe incluir información sobre los materiales utilizados, sobre la identificación del fabricante o el proceso de implementación.

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas de ERP:

- Fuego
- Accesibilidad.

Para obtener más información, es aconsejable consultar el listado[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

#### Videovigilancia

Los puntos de venta no alimentarios pueden equiparse con sistemas de videovigilancia de los clientes. Por lo tanto, el operador deberá indicar su presencia para no infringir su privacidad.

El operador también tendrá que declarar su instalación con la prefectura del establecimiento y completar el formulario[Cerfa 13806*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13806.do), acompañado de cualquier documento que detalle el sistema utilizado.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El contratista debe registrarse en el Registro Mercantil y Corporativo (SCN). Es aconsejable consultar los "Formalidades de Reporte de Empresas Artesanales" para obtener más información.

### b. Si es necesario, solicite una autorización de funcionamiento

La persona debe solicitar permiso para operar su negocio minorista no alimentario y, si es necesario, antes de que se conceda el permiso de construcción en los siguientes casos:

- Creación de una tienda minorista con una superficie comercial de más de 1.000 metros cuadrados resultantes de una nueva construcción o de la transformación de un edificio existente;
- Ampliación del espacio comercial de una tienda minorista que ya ha alcanzado el umbral de 1.000 metros cuadrados o se espera que supere el proyecto completando el proyecto;
- cualquier cambio en la línea de negocio de un negocio con una superficie de ventas de más de 1.000 metros cuadrados cuando el nuevo negocio de la tienda es la comida;
- Creación de un complejo comercial con una superficie total de ventas de más de 1.000 metros cuadrados;
- la ampliación de la superficie de venta de un complejo comercial que ya ha alcanzado el umbral de 1.000 metros cuadrados o se espera que supere con la finalización del proyecto;
- reapertura al público en el mismo sitio de una tienda minorista con una superficie comercial de más de 1.000 metros cuadrados cuyas instalaciones han dejado de funcionar durante tres años;
- creación o ampliación de un punto de desistimiento permanente por parte de los clientes de compras minoristas controladas por telemática, organizadas para el acceso al automóvil.

La solicitud se envía en doce copias a la Comisión Departamental de Planificación Comercial (CDAC) de la prefectura del departamento donde se encuentra el comercio, que dispone de dos meses para autorizar la operación.

La solicitud va acompañada de todos los siguientes documentos justificativos:

- un plan indicativo que muestra el área de ventas de las tiendas;
- Información:- delineación de la zona de captación del proyecto,
  - transporte público y acceso peatonal y ciclista que se sirven en el comercio;
- un estudio con los elementos para evaluar los efectos del proyecto en:- Accesibilidad de la oferta comercial,
  - el flujo de turismos y vehículos de reparto, así como un acceso seguro a la vía pública,
  - Gestión del espacio,
  - consumo de energía y contaminación,
  - paisajes y ecosistemas.

En caso de denegación, la decisión podrá ser recurrida ante la Comisión Nacional de Planificación Comercial, que se pronunciará en un plazo de cuatro meses.

*Para ir más allá* Artículos L. 752-1 y siguientes del Código de Comercio.

