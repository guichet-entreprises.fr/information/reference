﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS064" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Joyero-joyero" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="joyero-joyero" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/joyero-joyero.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="joyero-joyero" -->
<!-- var(translation)="Auto" -->


Joyero-joyero
=============

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El joyero es un profesional cuya misión principal es fabricar y reparar joyas ya sea en metales (oro, plata o platino), una especialidad del joyero, o compuesta de piedras (preciosas o finas), una especialidad del joyero.

Crea, transforma y restaura anillos, pulseras, collares y broches.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

Si el profesional tiene una actividad de compra y reventa, su actividad es artesanal y comercial.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Cualificaciones profesionales

El interesado que desee convertirse en joyero debe tener un diploma mínimo de nivel V o una cualificación de formación que pueda ser:

- un certificado de aptitud profesional (CAP) "arte y técnicas de joyería", "arte de joyas y joyas" o "fitter de opción de orfebrería";
- una patente artesanal (BMA) "opción de joyería";
- un diploma en artesanía (DMA) "arte de joya sin joyas".

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE) lleve a cabo la actividad de joyero-joya sin carácter temporal e informal o permanente en Francia.

Como tal, el nacional está sujeto a los mismos requisitos profesionales que el profesional francés (véase más arriba "3o. Procedimientos y trámites de instalación").

### c. Condiciones de honorabilidad e incompatibilidad

Nadie puede participar en joyas o joyas si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 de la Ley 96-603, de 5 de julio de 1996.

### d. Algunas peculiaridades de la regulación de la actividad

#### Cumplimiento de las normas de seguridad y accesibilidad

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

Es aconsejable referirse a la lista[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

#### Obligación de llevar un libro de policía

Mientras el profesional posea oro, plata y platino, debe enumerar en un registro su compra, recibo y reventa.

El libro de policía adopta la forma de un registro (papel o desmaterializado) que debe conservarse durante seis años y que debe mencionar cada operación realizada por el joyero.

#### Perforación

Para garantizar la calidad y procedencia de una joya, el joyero utiliza la técnica de punzonado que consiste en marcar el objeto con un punzón. Dependiendo de la forma del punzón (cuadrado, diamante, águila, minerva, concha, etc.), el joyero determinará la pureza y el valor de la joya.

#### Requisitos de perforación de la oreja y las alas de la nariz

Las técnicas de perforación para el ala nasal y el pabellón auditivo sólo pueden ser implementadas por profesionales que:

- tienen un negocio principal bajo el código NAF 47.77Z "Comercio al por menor de relojes y joyas en tiendas especializadas" o 32.12Z "Fabricación de joyas y artículos de joyería";
- convenios colectivos:- convenio colectivo nacional para el comercio minorista de relojes y joyas,
  - convenio colectivo nacional de joyería, joyería, platería y actividades conexas.

*Para ir más allá* Artículo R. 1311-7 del Código de Salud Pública; orden de 29 de octubre de 2008 para la aplicación del artículo R. 1311-7 del Código de Salud Pública y relativo a la perforación mediante la técnica de la pistola de oídos; orden de 11 de marzo de 2009 relativo a las buenas prácticas de higiene y seguridad para la aplicación de la perforación del pabellón auditivo y del ala de la nariz mediante la técnica de la pistola perforadora.

#### El título de mejor trabajador de Francia (MOF)

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en los ámbitos artesanal, comercial, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación. Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

Para obtener más información, se recomienda que consulte[web oficial del concurso "uno de los mejores trabajadores de Francia"](https://www.meilleursouvriersdefrance.org/).

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

#### El título de maestro artesano

Este título se otorga a las personas, incluidos los líderes sociales de las personas jurídicas:

- Registrado en el directorio trades;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

**Tenga en cuenta que**

Las personas que no poseen el título de máster pueden solicitar a la Comisión Regional de Cualificaciones el título de Maestro Artesano bajo dos supuestos:

- cuando están inscritos en el directorio de oficios, tienen un título al menos equivalente al máster, y justifican la gestión y los conocimientos psicopedagógicos equivalentes a los de las correspondientes unidades de valor del máster y que tienen dos años de práctica profesional;
- cuando se han inscrito en el repertorio de comercio durante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable referirse a las "Formalidades de la Declaración de la Empresa Artesanal.

### b. Siga el curso de preparación de la instalación (SPI)

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido formación sancionada por un título aprobado en el Nivel III (B.A. 2), incluyendo una educación en economía y gestión empresarial, o por el máster otorgado por un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que:

- han trabajado durante al menos tres años exigiendo un nivel de conocimientos equivalente al proporcionado por las prácticas;
- tener conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que pueda cubrir total o parcialmente la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con en Francia para dirigir una empresa de artesanías.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982; Artículo 6-1 del Decreto 83-517 de 24 de junio de 1983.

### c. Si es necesario, haga una declaración como titular de metales preciosos

Mientras el joyero posea objetos de oro, plata o platino, deberá hacer una declaración previa a la oficina de garantía de la dirección regional de aduanas y derechos indirectos.

La declaración se realiza enviando un archivo que contiene los siguientes documentos justificativos:

- La declaración que contiene el nombre del custodio o de la empresa, la designación de su actividad, el lugar de práctica y la dirección de la sede;
- Una identificación válida
- un certificado de registro ante el RCS o un certificado de registro ante el RMA, o la recepción de la declaración de actividad.

Al recibir el expediente, la oficina de garantía emitirá una declaración de existencia.

*Para ir más allá* Artículo 534 del Código Fiscal General.

