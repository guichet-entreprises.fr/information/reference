﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS039" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Establecimiento que recibe el público" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="establecimiento-que-recibe-el-publico" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/establecimiento-que-recibe-el-publico.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="establecimiento-que-recibe-el-publico" -->
<!-- var(translation)="Auto" -->

Establecimiento que recibe el público
=====================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

Una instalación de recepción pública (ERP) es un edificio, espacio o recinto para personas ajenas. El público es admitido de forma gratuita o por una tarifa, de manera abierta o invitacional.

*Para ir más allá* Artículo R. 123-2 del Código de La Construcción y Vivienda.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Esto último puede variar de una institución a otra.

Además, para obtener más información, es necesario remitirse a la ficha técnica de la actividad de que se trate.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para operar una institución de recepción pública, no se requieren cualificaciones profesionales especiales. Sin embargo, tal requisito puede ser requerido dependiendo de la naturaleza de la actividad del profesional.

Sin embargo, el operador está obligado a cumplir con las siguientes obligaciones:

- Solicitar permiso de planificación, desarrollar o modificar un ERP
- solicitud de apertura del ERP (consulte "3 grados). b. Solicitud de permiso para abrir un ERP");
- garantizar que respete:- requisitos de accesibilidad para personas con movilidad reducida (PMR) (ver infra "2 grados). d. Requisito de accesibilidad de la institución a los PPR"),
  - normas de incendio y seguridad (ver infra "2." d. Aplicación de la seguridad y protección contra los riesgos de incendio y pánico);
- cuando se prevén trabajos de desarrollo dentro de la institución, debe expedir al ayuntamiento un certificado de finalización de la obra (véase infra "3o. c. Si es necesario, proporcione un certificado después de la finalización de la obra").

**Solicitud de permiso para construir, desarrollar o modificar un ERP**

Una vez que el profesional está considerando construir una nueva instalación, o desarrollar o modificar una instalación existente, debe obtener una autorización previa emitida en nombre del estado.

Para ello, debe solicitar una autorización previa (véase infra "3o. a. Solicitar permiso para construir, desarrollar o modificar un ERP").

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

No existe ninguna disposición específica para que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE) opere un PIR de forma temporal y ocasional o de forma permanente en Francia.

Como tal, el nacional está sujeto a los mismos requisitos que el nacional francés (véase más arriba "2." Condiciones de instalación").

### c. Sanciones penales

El operador de un ERP es multado con 1.500 euros si:

- No cumple con los requisitos de seguridad y accesibilidad de los ERP;
- No lleva a cabo los trámites de comprobación de los diseños y locales;
- abre su establecimiento sin tener permiso de apertura;
- no permite que la comisión de seguridad lleve a cabo los derechos de visita durante la construcción o el desarrollo de la instalación.

**Tenga en cuenta que**

La multa se aplicará tantas veces como días de apertura sin autorización previa.

*Para ir más allá* Artículos R. 152-6 a R. 152-7 del Código de Construcción y Vivienda.

### d. Algunas peculiaridades de la regulación de la actividad

**Requisito de accesibilidad a los PPM**

El profesional debe asegurarse de que su establecimiento sea accesible a los PEMP en las mismas condiciones que las personas con capacidad física o, en su defecto, presentar una calidad de uso equivalente.

Para ello, durante su construcción, deberá asegurarse de que los locales estén dispuestos fuera (plazas de aparcamiento) así como en el interior (ascensores, equipos, etc.) para que los PLL puedan moverse de forma independiente, acceder a las instalaciones y el equipo, localizar y beneficiarse de todos los servicios ofrecidos.

Todos los ajustes que deben realizarse para garantizar el cumplimiento del edificio en construcción están determinados por el Detenido 20 de abril de 2017 sobre la accesibilidad a las personas con discapacidad en las instalaciones que reciben servicios públicos durante su construcción e instalaciones abiertas al público durante su desarrollo.

*Para ir más allá* Artículos L. 111-7 y R. 111-19 a R. 111-19-5 del Código de Construcción y Vivienda.

Al considerar el trabajo o desarrollo en un ERP existente, deben asegurarse de que el trabajo de desarrollo mantiene las condiciones de accesibilidad existentes.

*Para ir más allá* Artículos R. 111-19-7 a R. 111-19-12 del Código de Construcción y Vivienda; 8 de diciembre de 2014 en las que se establecen las disposiciones para la aplicación de los artículos R. 111-19-7 a R. 111-19-11 del Código de La Construcción y Vivienda y el Artículo 14 del Decreto No 2006-555 relativo a la accesibilidad a las personas con discapacidad en el Código de la Construcción y la Vivienda y el Artículo 14 del Decreto No 2006-555 relativo a la accesibilidad a las personas con discapacidad en el Código de la Construcción y la Vivienda y el Artículo 14 del Decreto No 2006-555 relativo según la accesibilidad a las personas con discapacidad en el Código de la Construcción y la Vivienda y el Artículo 14 del Decreto No 2006-555 relativo a la accesibilidad a las personas con discapacidad en el Código de la Construcción y la Vivienda y el Artículo 14 del Decreto No 2006-555 relativo a la accesibilidad a las personas con instalaciones públicas ubicadas en un entorno construido existente e instalaciones existentes abiertas al público.

**Poner en marcha la seguridad y la protección contra el fuego y los riesgos de pánico**

Los profesionales que deseen llevar a cabo la construcción o el desarrollo de un ERP deben cumplir con todos los requisitos de seguridad y protección contra riesgos de incendio.

El establecimiento debe incluir:

- Alojamiento para garantizar la evacuación de los ocupantes de las instalaciones;
- Equipos de extinción de incendios
- una o más fachadas que bordean espacios abiertos para una rápida evacuación del público;
- Materiales para resistir el fuego
- Salidas seguras y áreas de espera para riesgos potenciales
- Dispositivos de iluminación de seguridad eléctrica
- equipos (ascensores, ascensores, calefacción, aire acondicionado, ventilación, etc.) que son seguros.

Todos estos requisitos están sujetos a un reglamento de seguridad establecido en la orden del 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en los ERPs.

*Para ir más allá* Artículos L. 123-1 y R. 123-1 y siguientes del Código de Construcción y Vivienda.

**Clasificación de establecimientos**

Los ERP se clasifican según la naturaleza de su actividad (el tipo de actividad) y su capacidad (personal). Esta clasificación da como resultado un número de identificación que determinará los requisitos de seguridad y protección contra incendios.

**Tenga en cuenta que**

Durante cada construcción de un nuevo ERP, el propietario propone una clasificación que luego será validada o no por la comisión de seguridad departamental.

*Para ir más allá* Artículo R. 123-18 del Código de Construcción y Vivienda.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitar permiso para construir, desarrollar o modificar un ERP

**Autoridad competente**

El operador debe presentar su solicitud en cuatro copias al prefecto y al alcalde.

**Documentos de apoyo**

Su aplicación debe contener la Forma Cerfa No. 13824 completado y firmado, así como todos los documentos de apoyo requeridos.

Además, el solicitante debe adjuntar a su solicitud un expediente que permita al alcalde verificar el cumplimiento del establecimiento con las normas de seguridad.

Este archivo debe incluir:

- Un aviso descriptivo detallando todos los materiales utilizados dentro del edificio (interiores, exteriores, grandes obras, decoración, etc.);
- uno o más planos que especifican la anchura de los pasajes, salidas, espacios libres, escaleras o, más ampliamente, todos los modos de evacuación y circulación del edificio;
- si es necesario, el certificado de verificación de la aplicación de medidas para proteger un gasoducto que transporta gas natural, hidrocarburos o productos químicos.

**Procedimiento**

La solicitud es investigada dentro de los cuatro meses siguientes a la presentación del expediente, ya sea por el departamento de permisos de construcción o por el alcalde en todos los demás casos. Esta autoridad presenta la solicitud para verificar el cumplimiento por parte del proyecto de las normas sobre accesibilidad de los PPR al Comité Asesor Departamental de Seguridad y Accesibilidad (o a la Comisión Departamental de Seguridad en los Departamentos de París, Hauts-de-Seine, Sena-Saint-Denis y Val-de-Marne).

**hora**

Si no hay respuesta disponible en un plazo de cuatro meses, se considerará que se concede la autorización.

*Para ir más allá* Artículos R. 111-19-13 a R. 111-19-30 del Código de Construcción y Vivienda.

### b. Solicitar permiso para abrir un ERP

**Autoridad competente**

La autorización de apertura está dirigida al alcalde.

**Documentos de apoyo**

El titular deberá facilitar, con el fin de revisar su solicitud, el certificado de control por un técnico autorizado del cumplimiento de las instalaciones con las normas de accesibilidad.

**Procedimiento**

La autoridad debe garantizar que las instalaciones y las instalaciones interiores y exteriores sean accesibles al público y a los PMP. Después de recibir un dictamen favorable de la autoridad de supervisión y accesibilidad, la autorización de apertura se dirige al operador del establecimiento mediante carta recomendada con notificación de recepción.

**Tenga en cuenta que**

Cuando la autorización de apertura es emitida por el alcalde, debe entregar una copia al prefecto.

*Para ir más allá* Artículos L. 111-7, L. 111-8-3 y R. 111-19-29 del Código de Construcción y Vivienda.

### c. Si es necesario, proporcione un certificado después de la finalización de la obra

**Autoridad competente**

Una vez finalizada la obra, el operador deberá enviar una declaración de finalización y cumplimiento (DAACT) al ayuntamiento, mediante un pliegue recomendado con previo aviso de recepción.

**Documentos de apoyo**

Esta declaración debe incluir el Forma Cerfa 13408 Completado y firmado, así como todos los documentos de apoyo requeridos.

**Procedimiento**

Esta declaración de un controlador técnico aprobado debe permitir al operador certificar que el trabajo se llevó a cabo de acuerdo con los requisitos de seguridad y accesibilidad.

**Tenga en cuenta que**

El operador se enfrenta a una multa de 1.500 euros o 3.000 euros en caso de reincidencia si el certificado no ha sido realizado por un responsable autorizado.

*Para ir más allá* Artículos L. 111-7-4 y R. 111-19-27 a R. 111-19-28 del Código de Construcción y Vivienda; Artículo R. 462-1 del Código de Planificación.

