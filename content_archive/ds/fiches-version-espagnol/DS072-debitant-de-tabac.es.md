﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS072" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Débito de tabaco" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="debito-de-tabaco" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/debito-de-tabaco.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="debito-de-tabaco" -->
<!-- var(translation)="Auto" -->

Débito de tabaco
================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El minorista de tabaco, también conocido como buralista, es un profesional especializado en la venta al por menor de diferentes tipos de tabaco listo para comer (cigarrillos, puros, tabaco de liar, etc.).

Existen dos categorías de puntos de venta de tabaco:

- Flujo regular de tabaco
- puntos de venta especiales de tabaco en el sector del transporte (redes ferroviarias, aeropuertos, etc.) u otros sectores públicos.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad de abofeteo del tabaco, el interesado debe haber se haber sometido a una formación profesional en uno de los[centros de formación](http://www.douane.gouv.fr/articles/a10942-formation-pour-la-vente-au-detail-des-tabacs-manufactures) aprobado por la Dirección General de Aduanas y Derechos Indirectos.

La formación incluye cinco módulos que les permitirán adquirir conocimientos de la normativa relacionada con la gestión de una salida de tabaco y los fundamentos de la gestión empresarial.

**Tenga en cuenta que**

Tan pronto como el gerente de una salida de tabaco tiene que renovar su contrato de gestión (ver infra "3.0. c. Obligación de celebrar un contrato de gestión"), deberá, en el plazo de seis meses a partir de la renovación, someterse a un curso de formación profesional continua de un día en uno de los centros de formación.

El profesional que justifique una formación inicial podrá establecerse en forma de:

- explotación individual;
- una asociación compuesta únicamente por individuos.

*Para ir más allá* : orden de 25 de agosto de 2010 relativa a las modalidades de formación profesional inicial y continua para la venta al por menor de tabaco manufacturado.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

No existe ningún reglamento para el nacional de un Estado miembro de la Unión Europea (UE) o parte del Espacio Económico Europeo (EEE) que desee llevar a cabo la actividad de acarideo de tabaco en Francia, de forma temporal e informal o Permanente.

Por lo tanto, sólo las medidas adoptadas para el nacional francés tendrán que aplicarse (véase infra "3o. Procedimientos y trámites de instalación").

### c. Condiciones de honorabilidad e incompatibilidad

Los solicitantes que deseen gestionar una salida de tabaco deben cumplir con condiciones de honor e incompatibilidad tales como:

- Ser francés o nacional de un estado de la UE o del EEE
- Ser mayores de edad
- No estar bajo tutela o curatorial;
- No ser gerente de otra salida de tabaco o sustituto de un gerente actual;
- proporcionar garantías de honor y probidad que serán apreciadas proporcionando un extracto del boletín 2 de su historial penal;
- disfrutar de sus derechos civiles.

**Tenga en cuenta que**

El futuro gerente debe justificar que está físicamente en condiciones de tener un débito de tabaco, incluso proporcionando un certificado médico establecido por un médico aprobado por la agencia regional de salud.

### d. Algunas peculiaridades de la regulación de la actividad

#### Límites a la implantación de una salida de tabaco

El establecimiento de una salida de tabaco sólo puede hacerse dentro del límite de un establecimiento para 3.500 habitantes.

Sin embargo, dado que un municipio tiene menos de 3.500 habitantes y aún no tiene una salida de tabaco, se permite su aplicación.

#### Prohibiciones de aplicación

Además de la condición relacionada con el número de habitantes, pueden existir prohibiciones sobre la ubicación de una salida de tabaco, incluyendo:

- En los centros comerciales donde se encuentran los puntos de venta de comida de autoservicio;
- centros comerciales
- dentro del ámbito de una salida de tabaco temporalmente cerrada;
- En zona protegida.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte las hojas de actividades "Formalidad de informar de una empresa comercial" y "Registro de una empresa comercial individual en el RCS."

### b. Condiciones para la creación de una salida de tabaco

La aplicación es un procedimiento mediante el cual la administración decide, ya sea por iniciativa propia o a petición de una persona interesada, la operación de una nueva salida de tabaco. Se puede hacer de dos maneras:

- por transferencia (prioridad);
- en su defecto, mediante la llamada a las aplicaciones.

#### Implementación de transferencia

El Director Regional de Aduanas autoriza al buralista a operar a una tarifa existente. A continuación, actuará como sucesor de la gestión de flujos.

**Procedimiento**

El Director Regional de Aduanas publica un aviso en un periódico de anuncios legales en el departamento del lugar donde se encuentra el adeudo. Los interesados en tener productos de tabaco ya en la práctica tendrán tres meses para solicitar la transferencia de su adeudo. El director elige el débito basado en [criterios precisos](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=F3F3B0498EBFDA155B5B9619E7C33A61.tplgfr35s_3?idArticle=LEGIARTI000032881684&cidTexte=LEGITEXT000022409542&dateTexte=20180125) y notificará al solicitante exitoso de la autorización de transferencia.

*Para ir más allá* Artículos 14 a 17 del Decreto de 28 de junio de 2010.

#### Implementación por convocatoria

Cuando el procedimiento de transferencia no haya sido posible, el Director Regional de Aduanas podrá iniciar un proceso de solicitud de dos meses durante el cual los solicitantes deberán presentar un expediente que cumpla con las especificaciones de la Administración.

La administración conservará la oferta que ofrezca las mejores garantías y luego hará que firme un contrato de gestión.

*Para ir más allá* Artículos 18 y 19 del Decreto del 28 de junio de 2010.

### c. Obligación de celebrar un contrato de gestión

El adeudado autorizado para iniciar la actividad de adeudo del tabaco debe, después de haberestablecido, celebrar un contrato de gestión con la administración. El contrato de tres años, que es tácitamente renovable, permitirá al buralista abastecer a los puntos de venta de tabaco.

*Para ir más allá* Artículo 2 del Decreto de 28 de junio de 2010.

### d. Autorización posterior al registro

#### Obligación de proporcionar un plan de diseño de espacios comerciales

Una vez que el profesional ha recibido permiso para instalar la tienda, deberá enviar por correo certificado, antes del inicio de la actividad, un plan de arreglo de los locales al director regional de aduanas territorialmente competente.

Este acuerdo debe tener en cuenta una especificación establecida en el anexo del decreto de 13 de diciembre de 2011.

El silencio de la administración dentro de una quincena de enviar el plan vale la pena aceptar el plan de diseño.

*Para ir más allá* : decreto de 13 de diciembre de 2011 relativo al régimen de adeudo de tabaco.

#### Otras obligaciones

Muchas obligaciones recaen sobre el buralista en el cargo, incluyendo:

- indicar la presencia de su punto de venta por un signo que tiene la forma adecuada de un diamante rojo y que posiblemente puede estar marcado como "tabaco";
- No vender tabaco a menores;
- Publicar "no fumar" en los terrenos del establecimiento;
- Respeto[normas de seguridad](https://www.guichet-entreprises.fr/fr/creation-dentreprise/les-prealables-a-la-creation-dentreprise/letablissement-recevant-du-public-erp/obligations-de-securite/) como institución receptora pública (ERP).

