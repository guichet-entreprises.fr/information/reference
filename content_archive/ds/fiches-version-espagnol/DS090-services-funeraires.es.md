﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS090" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Servicios funerarios" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="servicios-funerarios" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/servicios-funerarios.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="servicios-funerarios" -->
<!-- var(translation)="Auto" -->


Servicios funerarios
====================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

Los servicios funerarios sirven a una misión de servicio público que incluye:

- Transporte de cuerpos antes y después de la cerveza;
- La organización del funeral;
- thanatopraxia (que se ocupa del cuidado de la conservación del difunto);
- proporcionando cubiertas, ataúdes y sus accesorios interiores y exteriores, así como urnas cinerrosas;
- La gestión y el uso de cámaras funerarias;
- el suministro de coches de duelo y de duelo;
- la provisión de personal y los objetos y servicios necesarios para funerales, entierros, exhumaciones y cremaciones, con la excepción de placas funerarias, emblemas religiosos, flores, diversas obras de impresión y mármol funerario.

Estos servicios pueden ser prestados por cualquier empresa que haya recibido una autorización de la prefectura.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para una actividad artesanal (queatopraxia, presentación corporal y mármol), la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Cualquier persona que desee ejercer como maestro de ceremonias, thanatopracteur o funeral o consejero asimilado, debe tener un diploma nacional.

La formación que conduce a la profesión de maestro de ceremonias se establece en 70 horas y 140 horas para la formación que conduce a la profesión de funeral o consejero asimilado.

Para trabajar como thanatopracteur, la persona debe obtener su diploma nacional después de completar una formación teórica de 195 horas y una formación práctica de doce meses.

*Para ir más allá* Artículos D. 2223-132, D.2223-55-2 y siguientes del Código General de Autoridades Locales.

### b. Cualificaciones profesionales-UE o Nacionales del EEE (Servicio Gratuito o Establecimiento Libre)

**Para un ejercicio permanente (LE)**

Cualquier nacional de un Estado de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) podrá establecerse en Francia para ejercer permanentemente si justifica:

- experiencia profesional como gerente:- tres años seguidos,
  - dos años consecutivos acompañados de formación previa sancionada por un certificado reconocido por el Estado,
  - dos años consecutivos con tres años de experiencia laboral como empleado;
- tres años consecutivos de experiencia profesional como empleado y un certificado que justifica la formación previa en servicios funerarios.

Si el nacional no cumple estas condiciones, tendrá que justificar:

- un diploma, certificado o título en thanatopraxia expedido por un Estado que regula la profesión, o el ejercicio de esa profesión durante un año en los últimos diez años, cuando ni su formación ni su actividad están reguladas en ese estado;
- certificado de competencia o certificado de formación sancionando una actividad distinta de thanatopracteur, o el ejercicio de dicha actividad durante un año en los últimos diez años, cuando ni su formación ni su actividad regulado en ese estado.

El prefecto del lugar de instalación del nacional será competente para decidir sobre la solicitud de reconocimiento de cualificaciones de su competencia.

*Para ir más allá* Artículos L. 2223-48 y L. 2223-49 del Código General de Gobierno Local.

**Para un ejercicio temporal e informal (LPS)**

Cualquier nacional de un Estado de la UE o del EEE podrá realizar una actividad de servicio funerario temporal y ocasional en Francia, siempre que cumpla una de las siguientes condiciones:

- Estar legalmente establecidos en ese Estado para llevar a cabo la misma actividad;
- han estado en esta actividad durante un año en los últimos diez años antes de la entrega, cuando ni la formación ni la actividad están reguladas en ese estado;
- han recibido una Despeje por la prefectura competente (véase infra "3.3. c. Solicitar un aclaramiento de la prefectura").

*Para ir más allá* Artículos L. 2223-23 y L. 2223-47 del Código General de Gobierno Local.

### c. Condiciones de honorabilidad e incompatibilidad

Para poder desempeñar las funciones del jefe de un servicio funerario, el interesado no deberá:

- ser golpeado con la bancarrota personal
- han sido condenados a prisión por uno de los siguientes delitos o delitos menores:- ilegalmente llevando a cabo una actividad profesional regulada,
  - corrupción activa o pasiva o tráfico de influencias,
  - intimidación de una persona en un servicio público,
  - Estafa
  - violación de la confianza,
  - violación de entierro o violación de respeto debido a los muertos,
  - Vuelo
  - agresión moral o agresión sexual,
  - Recibir
  - Agresiones dolosas;
- han sido objeto de una condena dictada por un tribunal extranjero y reconocida como tal por la legislación francesa.

*Para ir más allá* Artículo L. 2223-24 del Código General de Autoridades Locales.

### d. Algunas peculiaridades de la regulación de la actividad

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

#### Información para compartir con las familias

Los documentos y presupuestos proporcionados a las familias de las personas fallecidas deben incluir el nombre de la institución, su representante legal, la dirección del operador y, en su caso, su número de registro en el registro comercial o en el directorio de comercios, así como una indicación de su forma jurídica, la autorización que posee y, en su caso, el importe de su capital.

Las cotizaciones deben distinguir entre los importes asignados a los suministros y servicios y los pagados a terceros en compensación por prestaciones.

*Para ir más allá* Artículos R. 2223-24 a R. 2223-32-1 del Código General de Autoridades Locales.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable referirse a las "Formalidades de Reporte de Empresas Artesanales", "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Individual Comercial en el RCS" para obtener más información.

### b. Seguir el curso de preparación de la instalación (SPI) para la actividad de thanatopracteur y marbrier

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que son:

- han ejercido una actividad profesional que requiere un nivel de conocimientos equivalente al proporcionado por las prácticas durante al menos tres años;
- conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que cubriría, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con en Francia para dirigir una empresa de artesanías.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982; Artículo 6-1 del Decreto No 83-517 de 24 de junio de 1983.

### c. Autorización posterior al registro

#### Solicitar una autorización de la prefectura

Los establecimientos que prestan uno de los servicios funerarios mencionados en el "1o. a. Definición" debe haber sido autorizado por el prefecto del sitio. El prefecto de París también será responsable de decidir sobre las solicitudes de los extranjeros que deseen establecerse en Francia.

La solicitud de autorización se realiza mediante la presentación de un archivo que incluye:

- una declaración que indique el nombre de la institución, su forma jurídica, la información sobre el estado civil del representante legal y, en su caso, un extracto del registro de comercio y empresas o el directorio de operaciones;
- Una lista de actividades que se llevarán a cabo;
- justificaciones de la fiscalidad y las cotizaciones a la seguridad social de la institución;
- Certificados que justifiquen la capacidad profesional de los funcionarios y agentes;
- La situación actualizada del personal empleado
- si es necesario, un certificado de conformidad de todos los vehículos utilizados para transportar cuerpos antes y después de la cerveza (ver infra "3.0). d. Si es necesario, obtener un certificado de conformidad con los vehículos utilizados para la cerveza);
- si es necesario, el certificado de conformidad de la cámara funeraria (véase infra "3.3. d. Si es necesario, obtener permiso para administrar una cámara funeraria");
- en su caso, cualquier certificación de que su personal posea diplomas relacionados con las profesiones de thanatopracteur, maestro de ceremonias o funeral o consejero asimilado;
- si procede, certificación del cumplimiento del crematorio (véase más adelante "Si corresponde, obtener una certificación de cumplimiento del crematorio").

La autorización se concede por un período de seis años. En caso de cambio, el jefe de la escuela tendrá que notificar al prefecto en un plazo de dos meses.

*Para ir más allá* Artículos L. 2223-23 y R. 2223-56 a R. 2223-65 del Código General de Gobierno Local.

#### Si es necesario, obtenga permiso para administrar una cámara funeraria

Las instituciones o asociaciones que deseen administrar o utilizar una cámara funeraria deben obtener una autorización y presentar un certificado de cumplimiento. La solicitud debe dirigirse en primer lugar al prefecto territorialmente competente en forma de expediente que incluya:

- Una nota explicativa
- Un plan de situación
- un proyecto de opinión pública que también se publicará en dos periódicos locales o regionales.

El prefecto dispondrá de dos meses para obtener la opinión del ayuntamiento y otros dos meses para decidir sobre la solicitud. El silencio guardado valdrá la pena el permiso.

Una vez concedida la autorización, la empresa o asociación deberá asegurarse de que la junta funeraria cumple con el Comité de Acreditación COFRAC francés) o por otro organismo de acreditación signatario del contrato de reconocimiento. Multilaterales.

*Para ir más allá* Artículos R. 2223-59, R. 2223-74 y D. 2223-87 del Código General de Gobierno Local.

#### Si es necesario, obtenga un certificado de conformidad del crematorio

Toda institución que desee tener un crematorio debe obtener en primer lugar una autorización de la administración y un certificado de conformidad expedido por la COFRAC o por otro organismo de acreditación que sea signatario de la reconocimiento multilateral. El silencio de la administración sobre la creación de un crematorio se ganará el rechazo de la solicitud de autorización.

*Para ir más allá* Artículos R. 2223-99 a R. 2223-109 del Código General de Autoridades Locales.

#### Si es necesario, obtenga un certificado de conformidad con los vehículos utilizados para la cerveza

El vendedor de un vehículo de transporte utilizado para la cerveza debe haber obtenido un certificado de conformidad expedido por la COFRAC u otro organismo de acreditación que sea signatario del acuerdo multilateral de reconocimiento.

Los vehículos de transporte están sujetos a una visita de cumplimiento cada tres años o dentro de los seis meses posteriores a la renovación de la solicitud de despacho.

*Para ir más allá* Artículos D. 2223-110 a D. 2223-115 del Código General de Gobierno Local.

