﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS081" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Gestión y reciclaje de residuos" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="gestion-y-reciclaje-de-residuos" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/gestion-y-reciclaje-de-residuos.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="gestion-y-reciclaje-de-residuos" -->
<!-- var(translation)="Auto" -->

Gestión y reciclaje de residuos
===============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La gestión y reciclaje de residuos es una actividad que consiste en que el profesional garantice la gestión de los residuos desde su producción hasta su tratamiento. Esta actividad incluye la recolección, el transporte, la operación de recuperación (permitiendo que los residuos se utilicen para fines útiles como sustituto de otras sustancias) o la eliminación de residuos.

El reciclaje, por otro lado, es una operación de reciclaje que consiste en tratar los residuos para su reutilización ya sea para su finalidad original o para otro fin.

*Para ir más allá* Artículo L. 541-1-1 del Código de Medio Ambiente.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Dado que la actividad es de carácter comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional realiza una actividad de comprayé su actividad será tanto comercial como artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requieren cualificaciones profesionales para el profesional que desee llevar a cabo la actividad de gestión y reciclaje de residuos.

Sin embargo, cuando su actividad esté comprendida en las instalaciones clasificadas para la protección del medio ambiente (ICPE), deberá llevar a cabo formalidades específicas (véase infra "3". a. Si es necesario, proceda con las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)).

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

No existen disposiciones específicas para el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) para llevar a cabo actividades de gestión y reciclado de residuos con carácter temporal. casual o permanente en Francia.

Como tal, el nacional está vinculado a los mismos requisitos que el nacional francés (véase infra "3o. Procedimientos y trámites de instalación").

### c. Responsabilidad y sanciones

**Responsabilidad**

Cualquier profesional que produzca o posea residuos es responsable de su gestión hasta que se eliminen o reciclen, incluso si son tratados por un tercero.

*Para ir más allá* Artículo L. 541-2-1 del Código de Medio Ambiente.

**Sanciones**

En caso de incumplimiento de obligaciones específicas en la gestión y reciclado de residuos, el profesional se enfrenta a una pena de dos años de prisión y una multa de 75.000 euros.

*Para ir más allá* Artículo L. 541-46 del Código de Medio Ambiente.

### d. Algunas peculiaridades de la regulación de la actividad

**Nomenclatura de residuos**

El profesional está obligado a determinar la naturaleza de los residuos que gestiona. También deberá incluir en todos los documentos de trazabilidad el Código de estos residuos, establecido de acuerdo con la nomenclatura establecida en el anexo de la[Decisión 2000/532/CE](https://Aida.ineris.fr/consultation_document/33804) 03/05/00 por la que se sustituye la Decisión 94/3/CE por la que se establece una lista de residuos con arreglo al artículo 1, letra a), de la Directiva 75/442/CEE del Consejo sobre residuos y de la Decisión 94/904/CE del Consejo por la que se establece una lista de residuos peligrosos con arreglo al artículo 1, apartado 4, Directiva 91/689/CEE del Consejo de Residuos Peligrosos.

*Para ir más allá* Artículo R. 541-7 del Código de Medio Ambiente.

**Cumplimiento de la política nacional de prevención y gestión de residuos**

El profesional que trabaja en la gestión y el reciclado de residuos está vinculado por la política nacional de prevención y gestión de residuos. Esta política tiene por objeto promover la llamada economía circular.

Como tal, el profesional está obligado a:

- Prevenir y minimizar la producción y la nocividad de los residuos;
- Respetar la jerarquía de los métodos de tratamiento de residuos
- Asegurar que la gestión de residuos no dañe la salud humana o el medio ambiente;
- respetar el principio de proximidad garantizando que el transporte de residuos sea limitado en distancia y volumen;
- mejorar la eficiencia del uso de los recursos y agotar los recursos.

*Para ir más allá* Artículo L. 541-1 del Código de Medio Ambiente.

**Obligación de llevar un registro de tratamiento de residuos**

El profesional que gestiona los residuos y recicla los residuos debe mantener un registro cronológico de residuos dentro y fuera de la instalación, comuniqueándoles:

- Naturaleza
- Tratamiento
- Envío.

Además, cada año, el profesional está obligado a declarar anualmente la naturaleza y cantidades de estos residuos.

El profesional debe mantener este registro durante al menos cinco años.

*Para ir más allá* Artículo L. 541-7 del Código de Medio Ambiente; Artículos 1 y 2 de la orden del 29 de febrero de 2012 en los que se establecen los contenidos de los registros a que se refieren los artículos R. 541-43 y R. 541-46 del Código de Medio Ambiente.

**Transporte de residuos**

El profesional que, durante su actividad, garantiza el transporte de residuos está obligado a declarar su empresa, siempre y cuando la cantidad de residuos transportados:

- más de 0,1 toneladas por carga de residuos peligrosos;
- 0,5 toneladas por carga de residuos no peligrosos.

Una vez que el profesional cumpla con estas condiciones, deberá enviar un expediente de declaración al prefecto del departamento donde se encuentra su sede central (véase infra "3o. b. Hacer una declaración para una actividad de transporte de residuos").

**Tenga en cuenta que**

Algunos profesionales no están obligados a cumplir con este requisito, incluidas las operaciones de ICPE en virtud del artículo 2790 de la nomenclatura ICPE (Tratamiento de Residuos Peligrosos).

*Para ir más allá* Artículo R. 541-50 y el siguiente del Código de Medio Ambiente.

**Suscribirse a garantías financieras**

El profesional está obligado a obtener garantías financieras mientras su actividad esté comprendida en las instalaciones clasificadas para la protección del medio ambiente sujeta según autorización (véase infra "3." b. Si es necesario, proceda con las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)).

Estas garantías financieras pueden dar lugar a:

- un compromiso escrito de una institución de crédito, seguro, financiamiento o fianza mutua;
- Un fondo de garantía gestionado por Ademe;
- un depósito en manos de la Caisse des dépéts et consignments;
- un compromiso escrito con una garantía independiente de una persona que posea más de la mitad del capital del operador.

Estas garantías financieras deben garantizarse durante un mínimo de dos años y deben renovarse al menos tres meses antes de su vencimiento.

Estas garantías financieras están destinadas a cubrir los riesgos asociados con los gastos para:

- La seguridad del lugar de funcionamiento del profesional;
- Intervenciones en caso de accidente o contaminación;
- reacondicionado después de su cierre.

*Para ir más allá* Artículo R. 516-1 y siguiente del Código de Medio Ambiente; decreto de 31 de mayo de 2012 por el que se establece la lista de instalaciones clasificadas sujetas a la obligación de establecer garantías financieras en virtud del artículo R. 516-1 del Código de Medio Ambiente.

**Borde de seguimiento de residuos peligrosos (BSDD)**

El profesional que gestiona y recicla productos clasificados como peligrosos según la nomenclatura de residuos (véase supra "2." d. Nomenclatura de Residuos") para llenar un resbalón de seguimiento de residuos de acuerdo con el formulario Cerfa No.12571*01.

*Para ir más allá* Artículo R. 541-45 del Código de Medio Ambiente; orden de 29 de julio de 2005 por la que se establece el formulario de deslizamiento de residuos peligrosos mencionado en el artículo 4 del Decreto No 2005-635, de 30 de mayo de 2005.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Si es necesario, proceda con las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)

Dependiendo del tipo y la cantidad de residuos a tratar, la actividad de gestión y reciclado de residuos puede estar comprendida en la normativa de instalaciones clasificadas de protección del medio ambiente (ICPE).

Por lo tanto, el profesional estará obligado a determinar la rúbrica a la que pertenece su actividad y a proceder, en su caso, ya sea a:

- Una solicitud de permiso
- Pre-declaración
- registro de su actividad en la prefectura.

Es aconsejable referirse a la nomenclatura del ICPE, disponible en el[Aida](https://aida.ineris.fr/) para obtener más información.

### b. Hacer una declaración para una actividad de transporte de residuos

**Autoridad competente**

El profesional debe enviar su expediente al prefecto del departamento en el que se encuentra su sede central o, en su defecto, a su domicilio.

**Documentos de apoyo**

Su archivo de notificación debe incluir:

- una declaración de acuerdo con el modelo establecido en el Apéndice I el auto de 12 de agosto de 1998 relativo a la composición del expediente de declaración y a la recepción de la declaración para el ejercicio de la actividad de transporte de residuos;
- un extracto de su registro en el Registro de Comercio y Empresas (RCS) o, en su defecto, en el directorio de operaciones, que data de menos de tres meses.

**Tiempo y procedimiento**

Al recibir el expediente completo, el prefecto envía al solicitante un recibo de acuerdo con el modelo establecido en el Apéndice II 12 de agosto de 1998 supra. El prefecto emite un número de copia igual al número de vehículos asignados a los residuos de transporte.

**Tenga en cuenta que**

Esta declaración debe renovarse cada cinco años.

### c. Formalidades de notificación de la empresa

En el caso de la creación de una empresa mercantil, el profesional está obligado a inscribirse en el Registro de Comercio y Sociedades (RCS).

**Autoridad competente**

El profesional debe hacer una declaración a la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

Para ello, debe proporcionar documentos de apoyo dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI le envió un recibo el mismo día indicando los documentos que faltaban en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la devolución de su expediente siempre que no se haya presentado durante los plazos mencionados anteriormente.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

