﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS020" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sector animal" -->
<!-- var(title)="Veterinaria" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sector-animal" -->
<!-- var(title-short)="veterinaria" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/sector-animal/veterinaria.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="veterinaria" -->
<!-- var(translation)="Auto" -->


Veterinaria
===========

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El veterinario es un profesional especializado en medicina animal y cirugía.

Su misión incluye:

- Proteger y cuidar a los animales
- Diagnosticar enfermedades físicas y conductuales, lesiones, dolor y malformaciones de animales;
- Recetas seguras para medicamentos
- administrar medicamentos por medios parenterales;
- garantizar la inocuidad de los alimentos y la salud pública, incluso contribuyendo al control de la higiene en las industrias agroalimentarias;
- Para preservar el medio ambiente
- para desarrollar investigación y formación.

### b. CFE competente

El centro de formalidades empresariales (CFE) correspondiente difiere de la actividad realizada por el interesado para crear una tienda:

- Si la actividad se lleva a cabo en una empresa individual: la competencia de la Urssaf;
- Si la actividad se lleva a cabo en forma de sociedad mercantil: la jurisdicción de la Cámara de Comercio e Industria (CCI);
- si la actividad se lleva a cabo en forma de sociedad civil o de una sociedad ejercerla liberal: la competencia del registro del tribunal mercantil, o el registro del tribunal de distrito en los departamentos del Bajo Rin, el Alto Rin y el Mosela.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para ejercer como veterinario en Francia, el interesado deberá:

- poseer el título veterinario estatal expedido a partir del 21 de diciembre de 1980 en Francia u otro título de formación[decretado a partir del 21 de mayo de 2004](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000019327053) ;
- ser nacional francés o nacional de un Estado miembro de la Unión Europea (UE)o parte del Acuerdo del Espacio Económico Europeo (EEE);
- inscribir su diploma en uno de los consejos regionales del Colegio de Veterinarios antes de su inclusión en la junta directiva de la Orden.

*Para ir más allá* Artículos L. 241-1 y L. 241-2 del Código Rural y Pesca Marina (Código Rural y Pesca Marina).

**Tenga en cuenta que**

La práctica ilegal de medicina animal o cirugía se castiga con dos años de prisión y una multa de 30.000 euros.

*Para ir más allá* Artículos L. 243-1 y L. 243-4 del Código Rural y Pesca Marina.

Los veterinarios pueden trabajar como parte de:

- Una empresa individual
- Sociedad Civil Profesional (SCP);
- una sociedad operativa liberal;
- todas las formas de sociedades de Derecho nacional (E.SA, SAS, SARL, por ejemplo) o las sociedades constituidas de conformidad con la legislación estatal de la UE o del EEE y que tengan su sede legal, sede o institución principal, por lo tanto no dan a sus asociados el estatus de un comerciante.

En el caso de un ejercicio basado en la empresa, se requieren las siguientes condiciones acumulativas para:

- más de la mitad del capital social y los derechos de voto deben ser ocupados, directamente o a través de empresas inscritas en el Colegio Regional de Veterinarios ("Orden"), por personas que actúen legalmente en la profesión veterinaria en ejercicio dentro de la sociedad
- queda prohibida la propiedad directa o indirecta de acciones o acciones del capital social:- personas físicas o jurídicas que, no ejerciendo como veterinarios, presten servicios, productos o equipos utilizados en la práctica profesional veterinaria,
  - personas físicas o jurídicas comprometidas, profesionalmente o de acuerdo con su finalidad social, en la cría, producción o transferencia, gratuita o costosa, de animales o de transformación de productos animales;
- Los administradores, el presidente de la sociedad simplificada, el presidente del consejo de administración o los miembros del consejo de administración deben ser personas que estén legalmente en ejercicio de la profesión de veterinario;
- la identidad de los socios es conocida y la admisión de cualquier nuevo socio está sujeta a la aprobación previa por decisión colectiva tomada por la mayoría de los socios.

**Tenga en cuenta que**

El veterinario tendrá que pagar una cuota ordinal anual, cuyo importe total varía dependiendo de si se trata de una persona física o jurídica.

Para obtener más información sobre el importe de esta tasa, acérquese al Colegio Regional de Veterinarios.

*Para ir más allá* Artículo L241-17 del Código Rural y Pesca Marina.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### En caso de entrega gratuita de servicios (LPS)

El nacional de un Estado de la UE o del EEE, que actúe legalmente como veterinario en uno de estos Estados, podrá utilizar su título profesional en Francia, ya sea temporal u ocasionalmente. Debe solicitarlo, antes de su primera actuación, mediante declaración dirigida a la Orden Nacional de Veterinarios (véase infra "5o. a. Hacer una declaración previa de actividad para los nacionales de la UE o del EEE que realicen actividades temporales y ocasionales (LPS)").

En el caso de que la profesión no esté regulada en el país en el que el profesional esté legalmente establecido, ya sea como parte de la actividad o como parte de la formación, deberá haber realizado esta actividad durante al menos un año, en los últimos diez años años antes del beneficio, en uno o más Estados de la UE o del EEE.

**Qué saber**

Para ejercer la profesión veterinaria de forma temporal o ocasional, el nacional debe tener las aptitudes linguísticas necesarias.

*Para ir más allá* Artículo L. 241-3 del Código Rural y Pesca Marina.

#### En caso de Establecimiento Libre (LE)

Un nacional de la UE o del EEE que desee trabajar permanentemente en Francia está comprendido en dos regímenes distintos. En ambos casos, debe tener las habilidades de idiomas necesarias para llevar a cabo la actividad en Francia.

**El sistema de reconocimiento automático de diplomas**

El artículo L. 241-1 del Código de Pesca Rural y Marítima establece un régimen de reconocimiento automático en Francia de los títulos y títulos obtenidos en un Estado de la UE o del EEE y con sujeción al decreto de 21 de mayo de 2004.

Corresponde al consejo regional de la orden de los veterinarios comprobar la regularidad de los diplomas y otros títulos de formación, conceder el reconocimiento automático y luego decidir sobre la solicitud de inclusión en la lista de la Orden, formalidades se especifican en los párrafos "3." a. Solicitar inscripción en el Colegio de Veterinarios."

*Para ir más allá* : Artículo L241-2 del Código de Pesca Rural y Marina, y ordenado el 21 de mayo de 2004 por el que se establece la lista de títulos, certificados o títulos veterinarios mencionados en el artículo L. 241-2 del Código Rural.

**El régimen despectivo: la autorización para ejercer**

Para ejercer en Francia, un nacional con un título veterinario no mencionado por el decreto de 21 de mayo de 2004 y el artículo L. 241-2 del Código Rural y pesca marítima debe obtener una autorización individual expedida por el Ministro agricultura (véase más adelante. "3°. (a) Si es necesario, solicite una licencia para ejercer para el nacional de la UE o del EEE que realice actividades permanentes (LE)).

Esta autorización está sujeta a una comprobación de conocimientos exitosa[Escuela Nacional de Veterinaria de Nantes (ONIRIS)](http://www.oniris-nantes.fr/etudes/examen-de-controle-des-connaissances/examen-de-controle-des-connaissances-pour-lexercice-du-metier-de-veterinaire-en-france/) y cuyos términos se establecen mediante el decreto de 3 de mayo de 2010.

*Para ir más allá* Artículo R. 241-25 y R. 241-26 del Código de Pesca Rural y Marina; decreto de 3 de mayo de 2010 relativo a la organización del control de conocimientos para veterinarios cuyos diplomas no reciban reconocimiento automático en Francia.

### c. Condiciones de honorabilidad e incompatibilidad

Las disposiciones del código de ética se imponen a los veterinarios que practican en Francia.

Como tal, deben respetar los principios de moralidad, probidad y dedicación esenciales para el ejercicio de la actividad. También están sujetos al secreto médico y deben ejercerse de forma independiente.

Además, la calidad del veterinario asociado de la sociedad civil profesional es incompatible con la fabricación, importación, exportación, distribución y explotación de medicamentos veterinarios, así como con la fabricación, importación y distribución de medicamentos sujetos a ensayos clínicos.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de presentación de informes corporativos

#### Guarde su diploma o título equivalente como veterinario

El interesado deberá, antes de ejercer la profesión veterinaria en Francia, registrar su título.

**Autoridad competente**

La solicitud de registro del diploma debe dirigirse al Presidente del Consejo Regional de la Orden en la región donde la persona desee establecerse.

**Documentos de apoyo**

En apoyo de su solicitud, el veterinario debe enviar un expediente que contenga:

- Una copia de una documento de identidad válido o un documento que acredite la nacionalidad del solicitante;
- copia certificada del Diploma Estatal de Médico Veterinario o de uno de los títulos cubiertos por la orden del 21 de mayo de 2004.

**Resultado del procedimiento**

Una vez que haya registrado su diploma, debe inscribirse en la junta directiva del Colegio de Veterinarios.

**Costo**

Gratis.

*Para ir más allá* Artículo R. 241-27 del Código Rural y Pesca Marina.

#### Solicitar inscripción en el Colegio de Veterinarios

La inscripción para el Colegio de Veterinarios es obligatoria para hacer legal el ejercicio de la profesión.

**Autoridad competente**

La solicitud de inscripción está dirigida al Presidente del Consejo Regional del Colegio de Veterinarios de la región donde la persona desea establecer su residencia profesional.

**Procedimiento**

Puede presentarse directamente ante el consejo regional de la Orden de que se trate o enviarse a él por correo certificado con notificación de recepción.

**Documentos de apoyo**

el[solicitud de registro](https://www.veterinaire.fr/fileadmin/user_upload/documents/exercer-metier/inscription/dossier-inscription-individuel.pdf) en el orden de la tabla de los veterinarios se acompaña de un archivo de documentos justificativos que incluye:

- prueba de conocimiento de la lengua francesa y del sistema de pesos y medidas utilizados en el territorio francés;
- Una copia de una documento de identidad válido o un documento que acredite la nacionalidad del solicitante;
- una copia del título estatal de médico veterinario o de cualquier otro título sancionador de la formación y reconocido en un Estado miembro, lo que permite el ejercicio en Francia;
- La solicitud de registro del diploma tal como se ha mencionado anteriormente;
- Un extracto de antecedentes penales de menos de 3 meses de edad;
- Una prueba de residencia administrativa profesional;
- una declaración manuscrita escrita en lengua francesa en la que, bajo juramento, declara que tiene conocimiento del código de ética veterinaria y se compromete a ejercer su profesión con conciencia, honor y probidad;
- En caso necesario, una copia del contrato de trabajo entre el veterinario y su empleador o cuando desee ejercer la profesión en actividades compartidas, una copia del contrato que le concierne;
- Una tarjeta de informe del censo completada y firmada;
- Una identificación con foto.

En el caso de un ejercicio en forma de sociedad a que se refiere el párrafo "2." a. Cualificaciones profesionales", el veterinario deberá proporcionar:

- Una copia de los estatutos de la empresa
- Certificación del registro por orden de cada socio;
- Una tarjeta de informe del censo completada y firmada;
- en el caso de ejercicio en forma de SEL o cualquier otra empresa autorizada para ejercer:- prueba de la liberación total o parcial de las entradas de efectivo,
  - una nota en la que se indique el importe del capital social, el número de acciones, su importe y distribución, los nombres de los veterinarios con sus acciones, los criterios de reparación de los beneficios y la fecha de inicio de la actividad,
  - prueba de residencia administrativa profesional;
- en el caso de un ejercicio en forma de CPS, un certificado del secretario del tribunal que se ha pronunciado sobre la sede de la empresa, señalando la presentación en el registro de la solicitud y los documentos utilizados para su inscripción en el registro de comercio y sociedades.

**Tenga en cuenta que**

Todos los documentos presentados en apoyo de la solicitud deberán ir acompañados, si no están escritos en francés, con una traducción certificada por un traductor juramentada o facultada para intervenir ante las autoridades judiciales o otro Estado de la UE, el EEE o la Confederación Suiza.

*Para ir más allá* Artículos R. 242-85 y R. 241-40 del Código Rural y Pesca Marina.

**hora**

El Presidente reconoce haber recibido el expediente completo en el plazo de un mes a partir de su registro.

El consejo regional del Colegio debe decidir sobre la solicitud de inscripción en un plazo de tres meses a partir de la recepción del expediente completo de solicitud. Si no se responde a una respuesta dentro de este plazo, la solicitud de registro se considera rechazada.

Este período se incrementa a seis meses para los nacionales de terceros países cuando se llevará a cabo una investigación fuera de la Francia metropolitana. A continuación, se notifica al interesado.

También podrá prorrogarse por un período no superior a dos meses por el consejo regional cuando se haya ordenado una investigación fuera del territorio nacional.

**Resultado del procedimiento**

Una vez que la Orden haya validado el registro, el nacional tiene derecho a ejercer su profesión en Francia.

**Remedio**

En caso de denegación de registro, podrá interponerse un recurso ante el Consejo Nacional de la Orden en el plazo de dos meses a partir de la notificación de la denegación de registro.

*Para ir más allá* Artículos R. 242-85 y siguientes del Código Rural y Pesca Marina.

#### Hacer una declaración previa de actividad para los nacionales de la UE que realizan actividades temporales u ocasionales (LPS)

Cualquier nacional de la UE o del EEE que esté establecido y realice legalmente actividades veterinarias en uno de estos Estados podrá ejercer en Francia de forma temporal u ocasional si hace la declaración previa.

**Autoridad competente**

La declaración debe enviarse por correo o correo electrónico, antes del primer servicio, al consejo superior del Colegio Veterinario Nacional.

**Renovación de la predeclaración**

La declaración previa debe renovarse cada año y en caso de cambio de situación laboral.

**Documentos de apoyo**

La predeclaración debe ir acompañada de un archivo completo con los siguientes documentos justificativos:

- Una copia de una documento de identidad válido o un documento que acredite la nacionalidad del solicitante;
- el[Forma](https://www.veterinaire.fr/exercer-le-metier/les-conditions-dexercice-en-france/la-libre-prestation-de-service-lps.html) Declaración previa de entrega de servicios, cumplimentada, fechada y firmada;
- un certificado de la autoridad competente del Estado miembro de establecimiento o parte de la UE en el EEE que certifique que el interesado está legalmente establecido en ese Estado y que no está prohibido ejercer, acompañado, en su caso, de una traducción en Establecido por un traductor certificado;
- prueba de cualificaciones profesionales
- Cuando el acceso o la práctica de la profesión veterinaria no requieran la posesión de un certificado de capacidad en el Estado miembro, prueba por cualquier medio de ejercer la profesión durante al menos dos años, en los diez años anteriores.

**hora**

En el plazo de un mes a partir de la recepción de la declaración, el Consejo Superior del Colegio informa al solicitante:

- Si puede o no comenzar a prestar servicios;
- cuando la verificación de las cualificaciones profesionales muestra una diferencia sustancial con la formación requerida en Francia, debe demostrar haber adquirido los conocimientos y habilidades que faltan Aptitud. Si cumple con este cheque, se le informa en el plazo de un mes que puede comenzar la prestación de servicios;
- cuando la revisión del archivo resalta una dificultad que requiere más información, las razones del retraso en la revisión del archivo. Luego tiene un mes para obtener la información adicional solicitada. En este caso, antes de que finalice el segundo mes a partir de la recepción de esta información, el Consejo Nacional informa al demandante, tras revisar su expediente:- si puede o no comenzar la prestación de servicios,
  - cuando la verificación de las cualificaciones profesionales del demandante muestre una diferencia sustancial con la formación requerida en Francia, debe demostrar que ha adquirido los conocimientos y habilidades que faltan, sujeto a una prueba de aptitud. Si cumplen con este control, se les informa en el plazo de un mes que pueden comenzar a proporcionar servicios. De lo contrario, se le informa de que no puede comenzar la prestación de servicios.

En ausencia de una respuesta del Consejo Superior del Colegio dentro de estos plazos, la prestación de servicios puede comenzar.

*Para ir más allá* Artículo R. 204-1 del Código Rural y Pesca Marina.

**Costo**

La inscripción en el consejo de la Orden es gratuita, pero crea la obligación de someterse a las cuotas ordinales obligatorias, cuyo importe es fijado anualmente por el consejo superior del Colegio.

#### Si es necesario, solicite una licencia para ejercer para el nacional de la UE o del EEE que realice actividades permanentes (LE)

Si el nacional no está en virtud del régimen de reconocimiento automático, debe solicitar una licencia para ejercer.

**Autoridad competente**

La solicitud de autorización para ejercer debe enviarse al Ministerio de Agricultura, Agroalimentación y Silvicultura

**Procedimiento**

La autorización de ejercicio está sujeta a la verificación de todos los conocimientos, cuyos términos se establecen en el auto de 3 de mayo de 2010.

el[Revisión](http://www.oniris-nantes.fr/fileadmin/redaction/Etude/Controle_des_connaissances/2017/Octobre_2017/notice_modalitesderoulesexamenCC-V18.pdf) consiste en una prueba de elegibilidad en forma de cuatro cuestionarios de opción múltiple (MQC) y pruebas orales y prácticas.

Para acceder a las pruebas orales y prácticas, el candidato debe obtener una puntuación media de 10 sobre 20 en todos los presupuestos, sin puntuación inferior a 5 de 20.

**Documentos de apoyo**

La admisión al examen sancionando la verificación de conocimientos está sujeta al envío de un expediente completo dirigido a ONIRIS. El archivo debe incluir los siguientes documentos justificativos:

- Una hoja informativa completa, fechada y firmada del candidato;
- Una carta de solicitud de autorización para ejercer para el Ministro de Agricultura;
- Un CV
- Una fotocopia de un documento de identidad válido
- Una copia de un extracto del antecedente penal (boletín 3);
- Una copia certificada del título veterinario y su traducción al francés por un traductor certificado;
- una forma que menciona las disciplinas elegidas.

El archivo completo debe enviarse por correo a ONIRIS antes del 31 de diciembre del año anterior a la inspección.

**Resultado del procedimiento**

El éxito de las pruebas de control de conocimientos se hace público por orden del Ministro responsable de la agricultura, merece la pena la autorización para ejercer la profesión veterinaria en Francia. Para que el ejercicio sea efectivo, el nacional también tendrá que registrarse en el Colegio de Veterinarios (véase supra "5.0). b. Solicitar inscripción en el Colegio de Veterinarios").

*Para ir más allá* Artículos R. 241-25 y R. 241-26 del Código de Pesca Rural y Marina.

**Costo**

La cuota de inscripción para el examen se fija en 250 euros y debe abonarse al CONTABLE de la ONIRIS antes del 31 de diciembre del año anterior a la inspección.

### b. Si es necesario, registre los estatutos de la empresa

Cuando el veterinario crea una empresa para llevar a cabo su actividad, deberá inscribir sus estatutos, una vez fechados y firmados, ante la Oficina del Impuesto sobre Sociedades (SIE) de la sede de la empresa.

En el caso de la creación de una sociedad mercantil, esta formalidad puede llevarse a cabo después de que el expediente haya sido presentado a la CFE, pero en el plazo máximo de un mes a partir de su firma.

*Para ir más allá* Artículos 635 y 862 del Código Tributario General.

**Autoridad competente**

La Oficina Tributaria De las Empresas (IES) - El centro de registro de la sede central es responsable del registro de los estatutos.

**Documentos de apoyo**

La solicitud de registro de los estatutos es un expediente que contiene las 4 copias de los estatutos.

**Costo**

Gratis.

### c. Proceder con los trámites de declaración corporativa

El propósito de esta formalidad es dar a la empresa una existencia legal (empresa o empresa individual).

**Autoridad competente**

El Centro de Formalidades Comerciales (CFE)

**hora**

La CFE envía un recibo al solicitante de registro el día en que se recibe el archivo (o el primer día hábil siguiente) indicando:

- Si se siente incompetente, el nombre de la CFE pertinente a la que se envió el expediente el mismo día;
- si se considera competente:- para un expediente incompleto, los documentos justificativos que deben ser traídos dentro de una quincena de la recepción del recibo,
  - para un archivo completo, las organizaciones a las que se transmite el mismo día.

**Remedio**

Si el expediente no es remitido por la CFE al final de estos plazos, el solicitante de registro puede obtener la devolución inmediata de su expediente con el fin de remitir el asunto directamente a las agencias receptoras (Insee, administración tributaria, agencias sociales, etc.).

Cuando la CFE se niega a recibir el expediente, el solicitante de registro puede apelar ante el Tribunal Administrativo en el plazo de dos meses a partir de la notificación de denegación.

**Documentos de apoyo**

[Lista de documentos justificativos](https://www.afecreation.fr/pid14820/pj-prof-liberale-reglementee.html) para una actividad liberal regulada.

**Costo**

El costo de esta formalidad varía, en particular, dependiendo de la forma jurídica.

