﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS103" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Recogida de residuos: neumáticos" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="recogida-de-residuos-neumaticos" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/recogida-de-residuos-neumaticos.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="recogida-de-residuos-neumaticos" -->
<!-- var(translation)="Auto" -->

Recogida de residuos: neumáticos
================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad de recogida de residuos de neumáticos consiste en que el profesional (el recolector) garantice la eliminación de los residuos de los productores o distribuidores, y se haga cargo de su agrupación y transporte para instalaciones de procesamiento.

*Para ir más allá* Artículo R. 543-138 del Código de Medio Ambiente.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Dado que la actividad es de carácter comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional realiza una actividad de comprayé su actividad será tanto comercial como artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para llevar a cabo la actividad de recogida de neumáticos, el profesional debe:

- Tener licencia
- registro en el registro "Declaración del sistema de la responsabilidad ampliada del productor (REP)" en poder del Organismo de Gestión del Medio Ambiente y la Energía (Ademe), (véase infra "3." b. Proceder al registro en el registro de Ademe");
- declarar su actividad con el prefecto del departamento en el que desea ejercer (ver infra "3o. c. Solicitud de declaración de actividad").

*Para ir más allá* Artículo R. 543-145 del Código de Medio Ambiente.

**Aprobación**

Para obtener la acreditación, el profesional debe:

- Tener la capacidad financiera y técnica para llevar a cabo su negocio;
- Celebrar una promesa contractual o contractual con al menos un productor de neumáticos que haya establecido un sistema de recogida individual o contribuya a un organismo ecológico previsto a tal efecto;
- comprometerse a cumplir con todas las obligaciones establecidas en las especificaciones (véase infra "2o. c. Obligaciones profesionales").

Una vez que el profesional cumpla con estas condiciones, deberá solicitarlo al prefecto del departamento en el que se encuentre su instalación (véase infra "3o). a. Solicitud de aprobación").

*Para ir más allá* Artículo R. 543-143 del Código de Medio Ambiente.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) lleve a cabo la actividad de recogida de residuos de neumáticos, con carácter temporal y temporal. casual o permanente en Francia.

Como tal, el profesional está sujeto a los mismos requisitos que el nacional francés (véase más adelante. "3°. Procedimientos y trámites de instalación").

### c. Obligaciones profesionales

El profesional certificado está obligado a cumplir con todas las disposiciones de las especificaciones. Por lo tanto, debe:

- la recogida de los residuos de neumáticos que los productores le den a su disposición en cada departamento en el que estén aprobados;
- presentar al prefecto, en el plazo de dos meses a partir de la emisión de su aprobación, todos los contratos con los productores;
- recoger lotes de residuos que pesen una tonelada o más dentro de una quincena de depósito;
- recoger los residuos depositados gratuitamente por los productores que hayan creado un sistema individual o un organismo de eco-recogida para los titulares o distribuidores en virtud del artículo R. 543-144 del Código de Medio Ambiente;
- entregar únicamente los residuos de neumáticos a los que operen instalaciones de agrupación autorizadas;
- comunicar a la Agencia de Gestión de Medio Ambiente y Energía (Ademe) a más del 31 de marzo del año en curso las cantidades de residuos de neumáticos recogidas en el año anterior, así como su destino preciso.

**Tenga en cuenta que**

En caso de incumplimiento de estas obligaciones, el prefecto podrá ordenar al profesional que realice sus observaciones y, en su caso, retirar su acreditación.

*Para ir más allá* Artículo R. 543-146 del Código de Medio Ambiente; anexo a la orden de 15 de diciembre de 2015 sobre la recogida de residuos de neumáticos.

### d. Algunas peculiaridades de la regulación de la actividad

**Jerarquía de tratamiento de residuos**

El profesional responsable de la recogida de residuos de neumáticos está obligado por la política nacional de prevención y gestión de residuos. Esta política prevé una jerarquía de métodos de tratamiento de residuos con el fin de promover la llamada economía circular.

Como tal, los residuos de neumáticos deben estar sujetos a:

- Preparación para la reutilización
- Reciclaje
- métodos de desarrollo como la recuperación de energía.

*Para ir más allá* Artículos L. 541-1 y R. 543-140 del Código de Medio Ambiente.

**Declaración anual a Ademe**

El profesional está obligado a informar electrónicamente a Ademe, cada año antes del 31 de marzo, de las cantidades de residuos de neumáticos recogidas por categoría, tipo de titulares y por departamento.

El modelo de esta declaración se establece en el[Apéndice 3](http://www.bulletin-officiel.developpement-durable.gouv.fr/fiches/BO20171/bo20171.pdf) 30 de diciembre de 2016 sobre la divulgación de información relativa a la gestión de los residuos de neumáticos.

*Para ir más allá* Artículo R. 543-150 del Código de Medio Ambiente.

**Mantener un registro cronológico de los residuos transportados o recogidos**

El profesional debe mantener un registro cronológico que indique para cada uno de los residuos recogidos:

- La fecha de su secuestro y descarga;
- su naturaleza y cantidad;
- El número de matrícula del vehículo o vehículos que proporcionaron el transporte;
- Si es así, el número del resguardo de seguimiento y el número de notificación;
- El nombre y la dirección de la persona que se lo entregó al coleccionista;
- el nombre y la dirección de la instalación a la que fue enviada.

Este registro debe conservarse durante al menos tres años.

*Para ir más allá* Artículo 3 de la orden de 29 de febrero de 2012 en la que se establecen los contenidos de los registros a que se refieren los artículos R. 541-43 y R. 541-46 del Código de Medio Ambiente.

**Garantías financieras**

Mientras su actividad recoja un volumen de residuos de 1.000 m3 o más y se enderezca en instalaciones clasificadas para la protección del medio ambiente, el profesional está obligado a sacar garantías financieras.

Estas garantías pueden dar lugar a:

- un compromiso escrito de una institución de crédito, seguro, financiamiento o fianza mutua;
- Un fondo de garantía gestionado por Ademe;
- un depósito en manos de la Caisse des dépéts et consignments;
- un compromiso escrito con una garantía independiente de una persona que posea más de la mitad del capital del operador.

Estas garantías financieras deben garantizarse durante un mínimo de dos años y deben renovarse al menos tres meses antes de su vencimiento.

Estas garantías financieras están destinadas a cubrir los riesgos asociados con los gastos para:

- La seguridad del lugar de funcionamiento del profesional;
- Intervenciones en caso de accidente o contaminación;
- reacondicionado después de su cierre.

*Para ir más allá* Artículos R. 516-1 y siguientes del Código de Medio Ambiente.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitud de aprobación

**Autoridad competente**

El profesional debe solicitar al prefecto del departamento en el que desea ejercer. Esta solicitud debe enviarse en tantas copias como de los departamentos interesados.

**Documentos de apoyo**

Su solicitud debe incluir:

- su identidad en el caso de un solicitante natural o el nombre, la forma jurídica, la dirección de la sede central y la calidad del firmante de la solicitud en el caso de un solicitante legal;
- un contrato o promesa de compromiso de uno o más productores para los que el profesional desea recoger los residuos y la siguiente información:


  - la lista de departamentos en los que desea ejercer,
  - la descripción de los recursos humanos y materiales a su disposición,
  - su compromiso de cumplir con todas las obligaciones establecidas en las especificaciones;
- para una actividad de recogida:


  - Una copia del recibo para la declaración de la actividad de transporte de residuos de carretera a carretera,
  - La información de contacto de las instalaciones de recogida autorizadas de las que el profesional depositará los residuos una vez recogidos;
- para una actividad de agrupación:


  - Una copia de la autorización de funcionamiento o un recibo para una declaración de actividad,
  - Capacidad máxima de almacenamiento para residuos de neumáticos.

**Retrasos y procedimientos**

Una vez recibida la solicitud, el prefecto envía el expediente a la dirección regional de medio ambiente, planificación y vivienda para asesoramiento, así como, para información, a los prefectos de los departamentos interesados. El prefecto tiene seis meses para decidir sobre la solicitud desde el momento en que se recibe. Esta acreditación se emite por un máximo de cinco años.

**Tenga en cuenta que**

Seis meses antes de su expiración, el profesional debe presentar una nueva solicitud de acreditación al prefecto.

*Para ir más allá* Artículos 4 a 6 de la orden de 15 de diciembre de 2015 sobre la recogida de residuos de neumáticos.

### b. Proceder al registro en el registro de Ademe

**Autoridad competente**

El profesional debe registrarse a más tardar un mes antes de su primera declaración anual a Ademe.

**Documentos de apoyo**

El registro se realiza electrónicamente en el[sitio del sistema de informes de las corrientes REP](https://www.syderep.ademe.fr/).

*Para ir más allá* Artículo 1 del Decreto de 30 de diciembre de 2016 relativo a la divulgación de información relativa a la gestión de los residuos de neumáticos.

### c. Solicitud de declaración de actividad

**Autoridad competente**

El profesional debe presentar una solicitud ante el prefecto del departamento donde se encuentra su sede central o, en su defecto, su domicilio.

**Documentos de apoyo**

Su solicitud debe incluir:

- una declaración coherente con la [Modelo](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C7A4C308C6097CFE7090975B4CC9DA46.tplgfr22s_1?idArticle=LEGIARTI000006893211&cidTexte=LEGITEXT000005626655&dateTexte=20180424) Listada para la Lista I de la orden de 12 de agosto de 1998 sobre la composición del expediente de declaración y la recepción de la declaración para el ejercicio de la actividad de transporte de residuos;
- un extracto de su registro en el Registro de Comercio y Empresas (RCS) o, en su defecto, en el directorio de operaciones, que data de menos de tres meses.

**Tiempo y procedimiento**

Al recibir el archivo de declaración completo, el prefecto emite:

- un recibo de acuerdo con el [Modelo](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C7A4C308C6097CFE7090975B4CC9DA46.tplgfr22s_1?idArticle=LEGIARTI000006893212&cidTexte=LEGITEXT000005626655&dateTexte=20180424) Lista II de la sentencia del 12 de agosto de 1998 supra;
- un número de copias cumplibles y numeradas de este recibo igual al número de vehículos asignados para recoger los residuos. El profesional está obligado a guardar estas copias a bordo de cada uno de sus vehículos de colección.

La declaración debe renovarse cada cinco años.

*Para ir más allá* Artículos R. 541-50 y siguientes del Código de Medio Ambiente.

### d. Si es necesario, proceder con las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)

La actividad de recogida de residuos de neumáticos puede estar sujeta al Reglamento sobre Instalaciones Clasificadas para la Protección del Medio Ambiente (ICPE).

Es probable que la actividad esté comprendida en la partida 2714: tránsito, agrupación o clasificación de residuos no peligrosos de papel/cartón, plásticos, caucho, textiles, madera.

Por lo tanto, dependiendo del volumen recogido, el profesional deberá realizar:

- una solicitud de permiso en caso de volumen mayor o igual a 1.000 m3;
- una declaración si este volumen es mayor o igual que 100 m3 pero menor que 1.000 m3.

Es aconsejable referirse a la nomenclatura del ICPE, disponible en el[Aida](https://aida.ineris.fr/) para obtener más información.

### e. Formalidades de notificación de la empresa

En el caso de la creación de una empresa mercantil, el profesional está obligado a inscribirse en el Registro de Comercio y Sociedades (RCS).

**Autoridad competente**

El profesional debe hacer una declaración a la Cámara de Comercio e Industria (CCI).

**Documentos de apoyo**

Para ello, debe proporcionar documentos de apoyo dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI le envió un recibo el mismo día indicando los documentos que faltaban en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la devolución de su expediente siempre que no se haya presentado durante los plazos mencionados anteriormente.

Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

