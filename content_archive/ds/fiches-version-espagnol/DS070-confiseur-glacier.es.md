﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS070" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentación" -->
<!-- var(title)="Confitería-glaciar" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentacion" -->
<!-- var(title-short)="confiteria-glaciar" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/alimentacion/confiteria-glaciar.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="confiteria-glaciar" -->
<!-- var(translation)="Auto" -->

Confitería-glaciar
==================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

El repostero es un profesional cuya especialidad es la fabricación de confitería, un producto a base de azúcar, que va desde dulces hasta chocolate, desde dragas hasta pasteles de frutas.

El glaciar es un profesional cuya especialidad es la transformación de productos alimenticios en helados, helados, sorbetes o helados de agua.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

Si el profesional tiene una actividad de compra y reventa, su actividad es artesanal y comercial.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Sólo una persona cualificada o sometida a un control efectivo y permanente de una persona cualificada puede llevar a cabo la actividad de confitería-glaciar.

La persona que realiza una de las actividades contempladas en el párrafo "1." a. Definición" o quién controla el ejercicio, debe ser el titular de la elección:

- Un Certificado de Cualificación Profesional (CAP);
- Una Patente de Estudios Profesionales (BEP);
- un diploma o un grado de igual o superior nivel aprobado o registrado cuando se expidió al directorio nacional de certificaciones profesionales.

A falta de uno de estos títulos, el interesado deberá justificar una experiencia profesional efectiva de tres años, en un Estado de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE), adquirido como líder trabajadorpor por cuenta propia o asalariado en el trabajo de confitero-cercer. En este caso, el interesado podrá solicitar a la CMA pertinente un certificado de cualificación profesional.

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía; Artículo 1 del Decreto 98-246, de 2 de abril de 1998; Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional exigida para las actividades del artículo 16 de la Ley 96-603, de 5 de julio de 1996.

### b. Cualificaciones profesionales: nacionales de la UE o del EEE (servicio gratuito o establecimiento gratuito)

#### Para una entrega de servicio gratuita (LPS)

El profesional nacional de la UE o del EEE puede trabajar en Francia, de forma temporal y ocasional, como confitero-icero con la condición de que esté legalmente establecido en uno de estos Estados para llevar a cabo la misma actividad.

Primero tendrá que solicitarlo mediante declaración escrita a la CMA pertinente (véase infra "3o. b. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)).

Si ni la actividad ni la formación que la lleva a ella están reguladas en el estado en el que el profesional está legalmente establecido, la persona también debe demostrar que ha estado en el Estado durante al menos un año a tiempo completo o a tiempo parcial durante el diez años antes de la actuación que quiere actuar en Francia.

**Tenga en cuenta que**

El profesional que cumpla con estas condiciones está exento de los requisitos de registro del Directorio de Comercios (RM) o del registro de empresas.

*Para ir más allá* Artículo 17-1 de la Ley 96-603 de 5 de julio de 1996.

#### Para un establecimiento gratuito (LE)

Para llevar a cabo la actividad de confitería-glaciar en Francia de forma permanente, el profesional nacional de la UE o del EEE debe cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba: "2. a. Cualificaciones profesionales");
- Poseer un certificado de competencia o título de formación requerido para el ejercicio de la actividad en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un documento de formación que certifique su preparación para el ejercicio de la actividad cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o el ejercicio de esta actividad;
- ser un diploma, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que la persona ha estado activa durante tres años en el estado que ha admitido la equivalencia.

**Tenga en cuenta que**

Un nacional de un Estado de la UE o del EEE que cumpla una de las condiciones anteriores podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante: "3. c. Si es necesario, solicite un certificado de cualificación profesional.")

Si la persona no cumple con cualquiera de las condiciones anteriores, la CMA puede pedirle que realice una medida de compensación en los siguientes casos:

- si la duración de la formación certificada es al menos un año inferior a la requerida para obtener una de las cualificaciones profesionales requeridas en Francia para llevar a cabo la actividad;
- Si la formación recibida abarca temas sustancialmente diferentes de los cubiertos por una de las cualificaciones requeridas para llevar a cabo la actividad en Francia;
- Si el control efectivo y permanente de la actividad requiere, para el ejercicio de algunas de sus competencias, una formación específica que no se imparte en el Estado miembro de origen y que cubra temas sustancialmente diferentes de los cubierto por el certificado de competencia o designación de formación a que se refiere el solicitante.

*Para ir más allá* Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996, de los artículos 3 a 3 a 3 del Decreto 98-246, de 2 de abril de 1998.

**Bueno saber: medidas de compensación**

La CMA, que solicita un certificado de reconocimiento de la cualificación profesional, notifica al solicitante su decisión de que realice una de las medidas de compensación. Esta Decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son imprescindibles para ejercer en Francia.

A continuación, el solicitante debe elegir entre un curso de ajuste de hasta tres años o una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en el plazo de seis meses a partir de la recepción por la CMA de la decisión del solicitante de optar por esta prueba. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final del curso de adaptación, el solicitante envía al CMA un certificado que certifica que ha completado válidamente esta pasantía, acompañado de una evaluación de la organización que lo supervisó. La CMA emite, sobre la base de este certificado, un certificado de cualificación profesional en el plazo de un mes.

La decisión de utilizar una medida de indemnización podrá ser impugnada por el interesado, que deberá presentar un recurso administrativo ante el prefecto en el plazo de dos meses a partir de la notificación de la decisión. Si su apelación es desestimada, puede iniciar una impugnación legal.

*Para ir más allá* Artículos 3 y 3-2 del Decreto 98-246, de 2 de abril de 1998, de artículo 6-1 del Decreto 83-517, de 24 de junio de 1983, por el que se establecen los requisitos de aplicación de la Ley 82-1091, de 23 de diciembre de 1982, relativa a la formación profesional de los artesanos.

### c. Condiciones de honorabilidad e incompatibilidad

Nadie puede ejercer como confitero si es objeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 de la Ley 96-603, de 5 de julio de 1996.

### d. Algunas peculiaridades de la regulación de la actividad

#### Reglamento sobre la calidad del artesano y los títulos de maestro artesano y mejor trabajador en Francia

**Artesanía**

Para reclamar la condición de artesano, la persona debe justificar:

- una PAC, un BEP o un título certificado o registrado cuando se emitió al RNCP al menos equivalente (véase arriba: "2. (a)
- experiencia profesional en este comercio durante al menos tres años.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de maestro artesano**

Este título se otorga a las personas, incluidos los líderes sociales de las personas jurídicas:

- Registrado en el directorio trades;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

**Tenga en cuenta que**

Las personas que no poseen el título de máster pueden solicitar a la Comisión Regional de Cualificaciones el título de Maestro Artesano bajo dos supuestos:

- cuando están inscritos en el directorio de oficios, tienen un título al menos equivalente al máster, y justifican la gestión y los conocimientos psicopedagógicos equivalentes a los de las correspondientes unidades de valor del máster y que tienen dos años de práctica profesional;
- cuando se han inscrito en el repertorio de comercio durante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de mejor trabajador de Francia (MOF)**

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en el ámbito artesanal, comercial, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación. Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

Para obtener más información, se recomienda que consulte[sitio web oficial](http://www.meilleursouvriersdefrance.info/) "uno de los mejores trabajadores de Francia."

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

#### Cumplir con las regulaciones de las instituciones públicas (ERP)

El confitero debe cumplir con las normas de seguridad y accesibilidad para ERP. Para obtener más información, es aconsejable consultar la hoja de actividades "ERP".

**Cumplir con las normas sanitarias**

El repostero debe aplicar las normas del REGLAMENTO 852/2004, de 29 de abril de 2004, relativas a:

- Las instalaciones utilizadas para la alimentación;
- Equipo
- Desperdicio de alimentos
- Suministro de agua
- Higiene personal
- alimentos, envases y envases.

También debe respetar los principios del Punto de Control Crítico de Análisis de Peligros (HACCP) del Reglamento anterior.

El confitero también debe aplicar las normas establecidas en el decreto del 21 de diciembre de 2009 que especifican las temperaturas de almacenamiento de los productos animales perecederos.

Estos incluyen:

- Condiciones de temperatura de conservación
- procedimientos de congelación.

Por último, debe respetar las temperaturas de conservación de los productos alimenticios perecederos de origen vegetal.

*Para ir más allá* : Reglamento de 29 de abril de 2004 y Decreto de 21 de diciembre de 2009 relativo a las normas sanitarias aplicables a las actividades de venta al por menor, almacenamiento y transporte de productos animales y alimentos en contenedores; Orden de 8 de octubre de 2013 sobre normas sanitarias para la venta al por menor, almacenamiento y transporte de productos y productos alimenticios distintos de los productos animales y los alimentos en contenedores.

#### Ver precios y etiquetado de productos

El confitero deberá informar al consumidor, mediante marcado, etiquetado, exhibición o cualquier otro proceso adecuado, sobre los precios y las condiciones específicas de la venta y ejecución de los servicios.

*Para ir más allá* Artículo L. 112-1 del Código del Consumidor.

**Tenga en cuenta que**

El repostero determina libremente el precio de venta de sus productos, pero la venta con pérdida está prohibida.

*Para ir más allá* Artículos L. 410-2, L. 442-1 y el siguiente código de consumo.

#### Mostrar ingredientes reconocidos como alérgenos

El uso en la fabricación o preparación de productos alimenticios que causen alergias o intolerancias debe ser puesto en cuenta al consumidor final.

Esta información debe indicarse en el propio producto o cerca de él para que no haya incertidumbre sobre el producto al que se refiere.

*Para ir más allá* Artículos R. 412-12 a R. 412-16 y Apéndice IV del Código del Consumidor.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Siga el curso de preparación de la instalación (SPI)

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**
La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que:

- han trabajado durante al menos tres años, lo que requiere un nivel de conocimientos equivalente al proporcionado por la pasantía;
- tener conocimientos adquiridos en un Estado de la UE o del EEE o un tercer país durante una experiencia profesional que cubra, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con las Francia para la gestión de una empresa de artesanía.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, artículo 6-1 del Decreto 83-517, de 24 de junio de 1983.

### b. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

La CMA del lugar en el que el nacional desea llevar a cabo la prestación es competente para emitir la declaración previa de actividad.

**Documentos de apoyo**

La solicitud de un informe previo de la actividad va acompañada de un archivo completo que contiene los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE;
- un documento que justifique la cualificación profesional del nacional que puede ser, a su elección:- Una copia de un diploma, título o certificado,
  - Un certificado de competencia,
  - cualquier documentación que acredite la experiencia profesional del nacional.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tenga en cuenta que**

Cuando el expediente está incompleto, la CMA tiene un plazo de quince días para informar al nacional y solicitar todos los documentos que faltan.

**Resultado del procedimiento**

Al recibir todos los documentos en el archivo, el CMA tiene un mes para decidir:

- autorizar la prestación cuando el nacional justifique tres años de experiencia laboral en un Estado de la UE o del EEE, y adjuntar a dicha Decisión un certificado de cualificación profesional;
- o autorizar la disposición cuando las cualificaciones profesionales del nacional se consideren suficientes;
- o imponerle una prueba de aptitud cuando existan diferencias sustancialmente diferentes entre las cualificaciones profesionales del nacional y las exigidas en Francia. En caso de negativa a realizar esta medida de compensación o si no cumple, el nacional no podrá prestar el servicio en Francia.

El silencio guardado por la autoridad competente en estos tiempos merece autorización para iniciar la prestación del servicio.

*Para ir más allá* Artículo 2 del Decreto de 2 de abril de 1998; Artículo 2 del Decreto de 17 de octubre de 2017 relativo a la presentación de la declaración y solicitudes previstas en el Decreto 98-246, de 2 de abril de 1998, y en el título I del Decreto 98-247, de 2 de abril de 1998.

### c. Si es necesario, solicite un certificado de reconocimiento de la cualificación profesional

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la ACM territorialmente competente.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA pide al interesado que lo complete dentro de una quincena de la presentación del expediente. Se emite un recibo tan pronto como se completa el archivo.

**Documentos de apoyo**

La carpeta debe contener las siguientes partes:

- Solicitar un certificado de cualificación profesional
- Un certificado de competencia o título de diploma o designación de formación profesional; Prueba de la nacionalidad del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (CIEP) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**Qué saber**

Si es necesario, todos los documentos justificativos deben traducirse al francés.

**Tiempo de respuesta**

Dentro de los tres meses siguientes a la recepción, la CMA podrá:

- Reconocer la cualificación profesional y emitir la certificación de cualificación profesional;
- decidir someter al solicitante a una medida de compensación y notificarle dicha decisión;
- negarse a expedir el certificado de cualificación profesional.

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Remedios**

Si la CMA se niega a emitir el reconocimiento de la cualificación profesional, el solicitante podrá iniciar, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, una impugnación legal ante el tribunal administrativo pertinente. Del mismo modo, si el interesado desea impugnar la decisión de la CMA de someterla a una medida de indemnización, primero debe iniciar un recurso agraciado ante el prefecto del departamento en el que se basa la CMA, en el plazo de dos meses a partir de la notificación de la decisión. Cma. Si no tiene éxito, puede optar por un litigio ante el tribunal administrativo correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998; Decreto de 28 de octubre de 2009 en virtud de los Decretos 97-558 de 29 de mayo de 1997 y No 98-246, de 2 de abril de 1998, relativo al procedimiento de reconocimiento de las cualificaciones profesionales de un nacional profesional de un Estado miembro de la Comunidad u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### d. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable referirse a las "Formalidades de la Declaración de la Empresa Artesanal.

### e. Si es necesario, hacer una declaración en caso de preparación o venta de productos animales o animales

Cualquier operador de un establecimiento que produzca, manipule o almacene productos animales o alimenticios que contengan ingredientes animales (carne, productos lácteos, productos pesqueros, huevos, miel), destinados al consumo debe cumplir con la obligación de declarar cada uno de los establecimientos de los que es responsable, así como las actividades que se llevan a cabo allí.

Debe hacerse antes de la apertura y renovación del establecimiento en caso de cambio de operador, dirección o naturaleza de la actividad.

La declaración debe dirigirse al prefecto del departamento de aplicación del establecimiento o, en el caso de los establecimientos bajo la autoridad o tutela del Ministro de Defensa, al servicio de salud de las fuerzas armadas.

Para ello, el confitero-glaciar tendrá que llenar el formulario Cerfa No. 13984*03 enviará a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP).

*Para ir más allá* Artículos R. 231-4 y R. 233-4 del Código Rural y Pesca Marina; Artículo 2 del Decreto de 28 de junio de 1994 sobre la identificación y acreditación sanitaria de establecimientos en el mercado de productos animales o animales y marcas de seguridad.

### f. Si es necesario, solicitar la aprobación de la salud en caso de venta de alimentos a intermediarios

Los establecimientos que preparen, transformen, manipulen o almacenen productos o productos alimenticios animales en recipientes para consumo humano (leche cruda, carnes, huevos, etc.) deben obtener una aprobación sanitaria para comercializar sus productos a otros establecimientos.

**Autoridad competente**

La solicitud de acreditación debe enviarse al SDCSPP en la ubicación del establecimiento.

**hora**

Si la solicitud de acreditación es completa y admisible, se concede una aprobación condicional por un período de tres meses al solicitante.

Durante este período, se realiza un cheque oficial. Si las conclusiones de esta visita son favorables, se concede la aprobación definitiva. De lo contrario, los puntos de incumplimiento se notifican al solicitante y la aprobación condicional podrá renovarse por otros tres meses. La duración total de la aprobación condicional no podrá exceder de seis meses.

**Documentos de apoyo**

La solicitud de acreditación sanitaria se realiza utilizando el formulario Cerfa 13983*03 con fecha y firmada y acompañada de todos los documentos justificativos enumerados en Apéndice II del Decreto de 8 de junio de 2006 sobre la acreditación sanitaria de los establecimientos que comercializan productos animales o alimenticios que contengan productos animales.

Estos documentos se refieren a la presentación de la empresa, la descripción de sus actividades y el plan de gestión de la salud.

*Para ir más allá* Artículos L. 233-2 y R. 233-1 del Código Rural y de la pesca marina; DGAL/SDSSA/N2012-8119 de 12 de junio de 2012 relativo al procedimiento de acreditación y a la composición del fichero.

**Es bueno saber**

Es aconsejable consultar con el SDCSPP para asegurar la necesidad de solicitar la acreditación y, si es necesario, para obtener asistencia en el establecimiento del expediente de solicitud.

### f. Si es necesario, obtenga una exención del requisito de acreditación de la salud

Los establecimientos afectados por la exención son las tiendas:

- ceding cantidades limitadas de alimentos a otros puntos de venta;
- las distancias de entrega a otros puntos de venta no superan los 80 kilómetros.

Las categorías de productos abarcadas por la exención incluyen:

- Productos lácteos
- leches tratadas térmicamente
- productos hechos de cáscaras de huevo o leche cruda que han sido sometidos a un tratamiento desinfectante.

El repostero puede obtener esta exención si la distancia con los establecimientos entregados no supera los 80 km (o más por decisión de la prefectura) y:

- para las leches tratadas térmicamente, si produce un máximo de 800 litros por semana cuando vende el 30% o menos de su producción total o 250 litros por semana cuando vende más del 30% de su producción total;
- para los productos lácteos y para los productos lácteos o de huevo de leche cruda que hayan sido sometidos a un tratamiento desinfectante, si produce un máximo de 250 kg por semana cuando vende el 30% o menos de su producción total o 100 kg si vende más del 30% de su producción total;

**Autoridad competente**

La solicitud de exención debe dirigirse al DDCSPP del departamento en el que se encuentra la institución.

**Documentos de apoyo**

La solicitud de exención de la subvención se hace utilizando el Cerfa 13982*05 completado, fechado y firmado.

**Costo**

Gratis

*Para ir más allá* Artículo L. 233-2 del Código Rural y Pesca Marina; Artículos 12, 13, Anexos 3 y 4 de la Orden de 8 de junio de 2006 sobre la Acreditación Sanitaria de establecimientos que ofrezcan productos animales o alimenticios que contengan productos animales; DGAL/SDSSA/2014-823 de 10 de octubre de 2014.