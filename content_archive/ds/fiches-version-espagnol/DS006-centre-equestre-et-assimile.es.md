﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS006" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Centro ecuestre y asimilado" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="centro-ecuestre-y-asimilado" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/centro-ecuestre-y-asimilado.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="centro-ecuestre-y-asimilado" -->
<!-- var(translation)="Auto" -->


Centro ecuestre y asimilado
===========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El centro ecuestre, y asimilado, es un establecimiento abierto al público en el que el profesional ofrece la práctica de equitación y, como opción, las siguientes actividades:

- la enseñanza de equitación;
- formación, formación y todo el cuidado que se presta a los equinos;
- la venta de caballos operaba en el centro;
- Organización de concursos y competiciones;
- El paseo ecuestre.

El centro también puede ofrecer las siguientes actividades comerciales:

- Alquilar equinos al público para actividades de ocio (caminar y turismo);
- Transporte de caballos
- Alquilar cajas a los propietarios;
- la venta de alimentos o cualquier cosa necesaria para montar a caballo;
- la agricultura equina.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- sea cual sea la forma jurídica elegida, la CFE competente será la Cámara de Agricultura;
- En caso de actividad comercial, la CFE correspondiente será la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El profesional que desee ejercer en un centro ecuestre debe tener la condición de educador deportivo y tener una tarjeta profesional (ver infra "3o. a. Declaración de educador deportivo para obtener una tarjeta profesional"). Sin embargo, este requisito de cualificación sólo es necesario si el centro ecuestre ofrece una actividad de enseñanza, animación o coaching ecuestre.

Si es así, la persona debe tener un diploma, un título profesional o un certificado de cualificación:

- garantizar su competencia en términos de seguridad de los profesionales y terceros en la actividad física o deportiva considerada;
- grabado en el[directorio nacional de certificaciones profesionales](http://www.rncp.cncp.gouv.fr/).

Entre los diplomas, títulos o certificados de titulación que cumplen estos dos criterios se encuentran:

- Certificado de Cualificación Profesional (CQP);
- El Certificado Profesional de Juventud, Educación Popular y Deporte (BPJEPS);
- Diploma Estatal de Juventud, Educación Popular y Deporte (DEJEPS).

Cada uno de ellos se otorga por una disciplina deportiva específica (esgrima, judo-jujitsu, golf, etc.) o un grupo de actividades (actividades ecuestres, todo el ocio público, automovilismo, etc.).

La lista de diplomas, títulos o certificados para ejercer como educador deportivo en Francia se elabora en el[Apéndice II-1 del Artículo A212-1 del Código del Deporte](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000018752146&dateTexte=&categorieLien=cid). Esta lista también especifica las condiciones de práctica asociadas a cada grado (límites geográficos de ejercicio, la categoría de público que el interesado puede supervisar, la naturaleza exacta de la actividad deportiva de que se trate, la duración máxima anual ejercicio, etc.).

**Tenga en cuenta que**

Los profesionales con los siguientes títulos solo pueden ejercerse bajo la responsabilidad de una persona que posea un PJEPS.

- Una Patente de Animador Pony (BAP);
- un certificado de cualificación profesional como asistente de curador (CQP ASA);
- Un certificado de aptitud profesional como animador asistente técnico (BAPAAT);
- un diploma de acompañante de turismo ecuestre (ATE) o guía de turismo ecuestre (GTE) otorgado por la Federación Francesa de Equitación (FFE) para el acompañamiento de senderismo o senderismo. Estos diplomas no le permiten enseñar equitación.

**Bueno saber: el educador deportivo en prácticas**

Las personas que están en el proceso de formación que conduce a un diploma, título o certificado de calificación pueden ejercer como educador deportivo en prácticas e iniciar un negocio. Sin embargo, en este caso, deben cumplir con la normativa adjunta a estos diplomas, títulos o certificados de cualificación, estar bajo la autoridad de un tutor y haber cumplido con los requisitos previos a su situación pedagógica.

*Para ir más allá* Artículos L. 212-1 y los siguientes artículos del Código del Deporte; Apéndice II-1 (Artículo A212-1) del Código del Deporte.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

Todo nacional de un Estado miembro de la Unión Europea (UE) o de un Estado parte en el acuerdo sobre el Espacio Económico Europeo (EEE) legalmente establecido en ese Estado y que ejerza la actividad de educador deportivo dentro de un centro ecuestre podrá ejercer, como temporal y casual, o permanente, la misma actividad en Francia.

**Para ejercicios temporales e informales (LPS)**

El ciudadano de la UE puede trabajar como educador deportivo en un centro ecuestre si justifica:

- han estado activos en uno o más Estados miembros durante al menos un año en los últimos diez años, cuando ni el acceso a la profesión ni la formación están regulados en ese Estado miembro;
- tener las habilidades de idiomas necesarias para ejercer su profesión en Francia.

Tan pronto como el nacional cumpla estas condiciones, deberá, antes de cualquier actuación, enviar una declaración de actividad al prefecto del departamento en el que desea ejercer (véase infra "3o. b. Hacer una declaración previa para el nacional de la UE para un ejercicio temporal y casual (LPS)").

*Para ir más allá* Artículo L. 212-7 del Código del Deporte.

**Para un ejercicio permanente (LE)**

El nacional de la UE puede ser un educador deportivo permanente si cumple una de las siguientes condiciones:

- si el Estado miembro de origen regula el acceso o el ejercicio de la actividad:- tener un certificado de competencia o un certificado de formación expedido por la autoridad competente de un Estado de la UE o del EEE que certifique un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia,
  - poseer un título adquirido en un tercer Estado y admitido en equivalencia con un Estado de la UE o del EEE y justificar haber mantenido esa actividad durante al menos dos años a tiempo completo en ese Estado;
- Si el Estado miembro de origen no regula el acceso o el ejercicio de la actividad:- han llevado a cabo esta actividad en un Estado de la UE o del EEE, a tiempo completo durante al menos un año en los últimos diez años y han tenido un certificado de competencia o un certificado de formación expedido por la autoridad competente de uno de estos Estados que da fe de una preparación para el ejercicio de la actividad, así como un nivel de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia,
  - titular de un certificado de cualificación al menos equivalente al nivel inmediatamente inferior al exigido en Francia, expedido por la autoridad competente de un Estado de la UE o del EEE y sancionando la formación reglada específicamente el ejercicio de la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte, consistente en un ciclo de estudio complementado, en su caso, por formación profesional, prácticas o práctica profesional.

Una vez que el nacional cumple una de las cuatro condiciones mencionadas anteriormente, se considera que el nacional está cualificado profesionalmente y debe hacer una declaración previa de actividad ante la autoridad competente (véase infra "3o. c. Hacer una declaración previa para el nacional de la UE para un ejercicio permanente (LE)).

Sin embargo, si las cualificaciones profesionales del nacional difieren sustancialmente de las cualificaciones requeridas en Francia que no garantizarían la seguridad de los profesionales y de los terceros, puede estar obligado a presentarse a una prueba de aptitud o completar un curso de ajuste (ver infra "3 grados). c. Bueno saber: medidas de compensación").

El nacional debe tener el conocimiento de la lengua francesa necesaria para llevar a cabo su actividad en Francia, en particular para garantizar la seguridad de las actividades físicas y deportivas y su capacidad para alertar a los servicios de emergencia.

*Para ir más allá* Artículos L. 212-7, y los artículos R. 212-88 a R. 212-90 del Código del Deporte.

### c. Condiciones de honor y sanciones penales

**Integridad**

El profesional no puede ejercer como educador deportivo en Francia si ha sido condenado por un delito o por uno de los siguientes delitos:

- Homicidio voluntario;
- tortura o actos de barbación;
- violencia o amenazas;
- Agresiones sexuales;
- tráfico o uso ilícito de estupefacientes;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- violaciones de los artículos L. 232-25 a L. 232-27 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie puede enseñar, facilitar o supervisar una actividad física o deportiva con menores si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier cargo, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro vacacional y de ocio, así como de grupos de jóvenes o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

*Para ir más allá* Artículo L. 212-9 del Código del Deporte.

**Sanciones penales**

El profesional se enfrenta a una pena de un año de prisión y una multa de 15.000 euros si:

- practica como profesor, instructor, educador, entrenador o facilitador de actividad física o deportiva en un centro ecuestre sin estar profesionalmente calificado;
- un ciudadano francés o de la UE que no esté cualificado profesionalmente.

*Para ir más allá* Artículo L. 212-8 del Código del Deporte.

### d. Algunas peculiaridades de la regulación de la actividad

**Declaración de detención de equinos**

Cualquier centro ecuestre que posea equinos debe informar al Instituto Francés de Equitación y Equitación (IFCE) antes de la llegada del primer animal.

Esta declaración, cuyo modelo se establece en el[Apéndice I de la orden del 25 de junio de 2018](https://www.legifrance.gouv.fr/eli/arrete/2018/6/25/AGRG1802984A/jo/texte#JORFSCTA000037131512) los procedimientos de notificación para los titulares de equinos y las plazas de aparcamiento deben incluir:

- La identidad y dirección del propietario del animal
- la dirección de las plazas de aparcamiento si es diferente de la del titular.

Esta identificación da lugar a la asignación de un único número nacional.

La orden de 25 de junio de 2018 especifica en el Apéndice I la lista de organizaciones de terceros desde las que el titular puede confiar la responsabilidad de llevar a cabo esta declaración a la IFCE. Por lo tanto, la FFE envía periódicamente la lista de miembros de ifCE una vez que han declarado su lugar de detención de equinos.

*Para ir más allá* Artículo L. 212-19, y artículos D. 212-47 y el siguiente del Código Rural y Pesca Marina.

**Solicitud de permiso para operar**

El centro ecuestre debe hacer una declaración a la prefectura tan pronto como:

- apodera de las tierras de cultivo en ausencia de un título agrícola;
- se dedica a la agricultura equina. Si es necesario, el profesional tendrá que justificar diplomas agrícolas o cinco años de experiencia en los últimos quince años, como operador, empleado, ayudante doméstico o trabajador agrícola.

Esta declaración deberá dirigirse en papel gratuito por carta recomendada con notificación de recepción al prefecto de la región en el territorio en el que se encuentra el centro o en el lugar donde se encuentra la sede de la operación del solicitante de registro.

*Para ir más allá* Artículos R. 331-1 y siguientes del Código Rural y Pesca Marina.

**Declaración del censo de equipos deportivos**

El centro ecuestre propietario de equipos deportivos debe hacer una declaración para identificar las características del equipo deportivo. Debe dirigirse en el plazo de tres meses a partir de la apertura del centro al prefecto del departamento en el que se encuentra.

Esta declaración se puede hacer directamente en línea a través de un formulario de declaración en el[Sitio web del Ministerio del Deporte](http://www.res.sports.gouv.fr/Declaration_en_Ligne.aspx).

*Para ir más allá* Artículo L. 312-2 del Código del Deporte.

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

Le aconsejó que se refiriera a la hoja "Establecimiento que recibe al público" para obtener más información.

**Seguro obligatorio**

El profesional que desee abrir un centro ecuestre debe llevar un seguro de responsabilidad civil que los cubra a ellos y a sus empleados contra los riesgos asociados a su actividad profesional.

Si se incumple el deber, el profesional se enfrenta a una pena de seis meses de prisión y una multa de 7.500 euros.

Además, el profesional debe informar a los miembros de una asociación deportiva o federación del interés de la compra de seguros con el fin de cubrir los riesgos asociados a la práctica deportiva.

*Para ir más allá* Artículos L. 321-1 y L. 321-4 del Código del Deporte.

**Requisito de divulgación**

El profesional deberá mostrar la siguiente información dentro del centro ecuestre:

- los diplomas o títulos profesionales de todos los que practican en el centro, así como sus tarjetas profesionales o certificados de prácticas;
- todos los textos relativos a las medidas de higiene y seguridad y las normas técnicas aplicables a la supervisión de las actividades físicas y deportivas;
- El certificado de seguro del centro
- números de emergencia.

*Para ir más allá* Artículo R. 322-5 del Código del Deporte.

**Otras disposiciones**

También hay muchas regulaciones específicas para la posesión de equinos, incluyendo:

- la presencia de una prueba de kit de emergencia para proporcionar primeros auxilios y un medio de comunicación para alertar a los servicios de emergencia;
- normas de higiene y seguridad relacionadas con el uso de equinos;
- la obligación de llamar a un veterinario
- concursos (consultar con la Federación Francesa de Equitación ([Ffe](https://www.ffe.com/))) ;
- paseos a caballo a lo largo de ríos, costas, bosques o en la vía pública;
- transporte de equinos.

Para más información es aconsejable consultar con el[IFCE](http://www.ifce.fr/).

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Declaración del educador deportivo para obtener una tarjeta profesional

Toda persona que desee ejercer cualquiera de las profesiones reguladas por el artículo L. 212-1 del Código del Deporte deberá declarar su actividad ante el prefecto del departamento del lugar donde tenga la intención de ejercer su actividad como director. Esta declaración desencadena la obtención de una tarjeta de visita.

La declaración debe renovarse cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del departamento de ejercicios o al principal lugar de práctica, o directamente en línea en el[sitio web oficial del Ministerio del Deporte](https://eaps.sports.gouv.fr).

**hora**

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

**Documentos de apoyo**

La solicitud debe incluir:

- un formulario de declaración[Cerfa 12699*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_12699.do) ;
- Documentos de apoyo:- Una copia de un documento de identidad válido,
  - Un documento de identidad con foto,
  - Una declaración sobre el honor que acredite la exactitud de la información en el formulario,
  - Una copia de cada uno de los diplomas, títulos o certificados invocados,
  - Una copia de la autorización para ejercer, la equivalencia de un diploma, si la hubiera,
  - un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

Si se renueva la devolución, debe adjuntar:

- Formulario Cerfa No. 12699*02 ;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate que datan de menos de un año.

La propia prefectura solicitará la comunicación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

**Costo**

Gratis

*Para ir más allá* Artículos L. 212-11, R. 212-85, y Los artículos A. 212-176 a A. 212-178 del Código del Deporte.

### b. Hacer una declaración previa para el nacional de la UE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

El nacional debe solicitar al prefecto del departamento donde desee llevar a cabo la mayor parte de su actividad.

**Documentos de apoyo**

La solicitud del nacional debe incluir:

- el formulario estándar adjunto a la[Apéndice II-12-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000021257586&dateTexte=&categorieLien=cid) El Código del Deporte completado y firmado;
- los siguientes documentos justificativos, si los hay, con su traducción al francés:- Una fotografía de la identidad del solicitante,
  - Una copia de su identificación,
  - Una copia de su certificado de competencia o su certificado de formación,
  - Una copia de los documentos que justifican que el nacional está legalmente establecido en el Estado miembro y que no está prohibido ejercer sus actividades,
  - una copia de cualquier documento que justifique que ha sido educador deportivo durante al menos un año en los últimos diez años si no se regula ninguna actividad ni formación en el Estado miembro del establecimiento;
- una declaración de honor que acredite la exactitud de la información proporcionada.

**hora**

En el plazo de un mes a partir de la recepción del expediente de declaración, el prefecto notifica al reclamante:

- una solicitud de información adicional (en este caso, el prefecto tiene dos meses para dar su respuesta);
- un recibo de entrega de servicio si no lleva a cabo una verificación de calificaciones. En este caso, la prestación del servicio puede comenzar;
- que lleva a cabo la verificación de calificaciones. En este caso, el prefecto emite entonces al demandante un recibo que le permite iniciar su actuación o, si la verificación de las cualificaciones revela diferencias sustanciales con las cualificaciones profesionales requeridas en Francia, el prefecto somete al reclamante a una prueba de aptitud o a un curso de ajuste.

En todos los casos, a falta de una respuesta dentro de los plazos antes mencionados, se considera que el demandante está legalmente activo en Francia.

*Para ir más allá* Artículos R. 212-92 a R. 212-93 y Sección A. 212-182-2 del Código del Deporte.

### c. Hacer una declaración previa para el nacional de la UE para un ejercicio permanente (LE)

Todo nacional de la UE o del EEE cualificado para llevar a cabo la totalidad o parte de las actividades mencionadas en el artículo L. 212-1 del Código del Deporte y que desee establecerse en Francia deberá hacer una declaración al prefecto del departamento en el que pretenda ejercicio como director.

Esta declaración permite al solicitante de registro obtener una tarjeta profesional y, por lo tanto, ejercer legalmente en Francia en las mismas condiciones que los nacionales franceses.

La declaración debe renovarse cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental encargada de Cohesión Social (DDCS) o a la Dirección Departamental encargada de Cohesión Social y Protección de la Población (DDCSPP).

**Documentos de apoyo para la primera declaración de actividad**

El archivo de informe de actividad debe incluir:

- formulario de declaración cumplimentado y firmado, cuyo modelo se establece en el[Apéndice II-12-2-a](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000021257548&dateTexte=&categorieLien=cid) Código de Deporte;
- los siguientes documentos justificativos, si los hay, con su traducción al francés:- Un documento de identidad con foto,
  - Una copia de un documento de identidad válido,
  - un certificado médico de no contradicción con la práctica y supervisión de las actividades físicas o deportivas (menos de un año),
  - una copia del certificado de competencia o título de formación, acompañado de documentos que describan el curso de formación (programa, volumen por hora, naturaleza y duración de las prácticas realizadas),
  - Si es así, una copia de cualquier evidencia que justifique su experiencia profesional,
  - Si el documento de formación se ha obtenido en un tercer Estado, se han admitido copias de los documentos que acrediten que el título ha sido admitido como equivalente en un Estado de la UE o del EEE que regula la actividad,
  - documentos que acrediten que el solicitante de registro no fue objeto de ninguna de las condenas o medidas contempladas en los artículos L. 212-9 y L. 212-13 del Código del Deporte en el Estado miembro de origen.

**Evidencia para una declaración de renovación de la actividad**

El archivo de renovación de actividad debe incluir:

- Una copia del formulario de renovación de la devolución, modelado en el Anexo II-12-2-b del Código del Deporte;
- Un documento de identidad con foto
- un certificado médico de no contradictorio con la práctica y supervisión de actividades físicas o deportivas que datan de menos de un año.

**Retrasos y resultados del procedimiento**

La decisión del prefecto de expedir la tarjeta de visita se produce en un plazo de tres meses a partir de la presentación del expediente completo por parte del solicitante de registro. Este plazo podrá prorrogarse un mes por decisión motivada. Si el prefecto decide no expedir la tarjeta profesional o someter al declarante a una medida de compensación (prueba de aptitud o pasantía), su decisión debe estar motivada (véase infra "Bueno saber: medida de compensación").

**Tenga en cuenta que**

La solicitud debe renovarse cada cinco años.

**Costo**

Gratis.

**Remedios**

Cualquier litigio debe ejercitarse en el plazo de dos meses a partir de la notificación de la decisión al tribunal administrativo pertinente.

*Para ir más allá* Artículos R. 212-88 a R. 212-91, Sección A. 212-182 y Listas II-12-2-a y II-12-b del Código del Deporte.

**Bueno saber: medidas de compensación**

Si existe una diferencia sustancial entre la calificación del solicitante y la exigida en Francia para llevar a cabo la misma actividad, el prefecto remite la comisión de reconocimiento de las cualificaciones colocadas al Ministro responsable del deporte. Esta comisión, después de revisar e investigar el expediente, emite, dentro del mes de su remisión, un aviso que envía al prefecto.

En su opinión, la comisión puede:

- creemos que existe efectivamente una diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia. En este caso, la comisión propone someter al solicitante de registro a una prueba de aptitud o a un curso de ajuste. Define la naturaleza y las modalidades precisas de estas medidas de compensación (la naturaleza de las pruebas, los términos de su organización y evaluación, el período de organización, el contenido y la duración de la pasantía, los tipos de estructuras que pueden acoger al aprendiz, etc.) ;
- que no hay diferencia sustancial entre la calificación del solicitante de registro y la requerida en Francia.

Tras recibir el dictamen de la comisión, el prefecto notifica al solicitante de registro su decisión motivada (no está obligado a seguir el consejo de la comisión):

- Si el solicitante de registro requiere compensación, tiene un mes para elegir entre la prueba de aptitud y el curso de adaptación. A continuación, el prefecto expide una tarjeta de visita al solicitante de registro que ha cumplido con las medidas de compensación. Por otra parte, si el curso o la prueba de aptitud no es satisfactorio, el prefecto notifica su decisión motivada de negarse a expedir la tarjeta profesional al interesado;
- si no requiere compensación, el prefecto emite una tarjeta de visita al interesado.

*Para ir más allá* Artículos R. 212-84 y D. 212-84-1 del Código del Deporte.

### d. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de la empresa, el contratista debe informar de su actividad a la Cámara de Agricultura o inscribirse en el Registro de Comercio y Sociedades (RCS).

Es aconsejable referirse a las "Formalidades de Informar de una Empresa Comercial" y "Registro de una Empresa Comercial Individual a la SCN" y a la[Cámara de Agricultura](http://www.chambres-agriculture.fr/) para obtener más información.

### e. Si es necesario, registre los estatutos de la empresa

El explotador de un centro ecuestre deberá, una vez fechados y firmados los estatutos de la sociedad, registrarlos en la Oficina del Impuesto sobre Sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

