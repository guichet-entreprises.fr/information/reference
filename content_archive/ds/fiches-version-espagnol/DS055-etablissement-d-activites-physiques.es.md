﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS055" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Establecimiento comercial de actividad física y deportiva (EAPS)" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="establecimiento-comercial-de-actividad" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/establecimiento-comercial-de-actividad-fisica-y-deportiva-eaps.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="establecimiento-comercial-de-actividad-fisica-y-deportiva-eaps" -->
<!-- var(translation)="Auto" -->


Establecimiento comercial de actividad física y deportiva (EAPS)
================================================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

Una instalación de actividad física y deportiva (EAPS) se define por la naturaleza de su actividad, que consiste en organizar la práctica de diversas actividades deportivas. Un EAPS puede ser identificado siempre y cuando incluya equipos fijos o móviles, proponga la práctica de una o más actividades durante un cierto período de tiempo (continuo o estacional) y defina las modalidades de su ejercicio.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Dado que la actividad es de carácter comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional se dedica a una actividad de compra venta, su actividad será tanto comercial como artesanal.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para operar un EAPS, no se requieren calificaciones especiales. Por otro lado, si el profesional se dedica a una actividad de animación, enseñanza, formación o coaching, tendrá que justificar un diploma para llevar a cabo esta actividad.

Para obtener más información, consulte el listado[Educador deportivo](https://www.guichet-entreprises.fr/fr/activites-reglementees/enseignement/educateur-sportif/) y tarjetas "Facilitador deportivo" sobre las actividades involucradas.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE) que desee operar un EAPS en Francia de forma temporal y casual como parte de un (LPS) o la prestación permanente de servicios como parte de un establecimiento libre (LE).

Como tal, el interesado está sujeto a los mismos requisitos que el nacional francés (véase "3o. Condiciones de instalación").

### c. Condiciones de honorabilidad

Nadie puede operar un EAPS si es o ha sido condenado por cualquier delito o delito:

- tortura y actos de barbación;
- Agresiones sexuales;
- narcotráfico;
- Poner en peligro a los demás;
- proxenetismo y los delitos resultantes;
- poner en peligro a los menores;
- uso ilícito de sustancias o plantas clasificadas como estupefacientes o provocación al uso ilícito de estupefacientes;
- violaciones de los artículos L. 232-25 a L. 232-28 del Código del Deporte;
- como castigo complementario a un delito fiscal: prohibición temporal de ejercer, directamente o por persona interpuesta, en nombre de sí mismo o de otros, cualquier profesión industrial, comercial o liberal ( Artículo 1750 del Código General Tributario).

Además, nadie puede enseñar, facilitar o supervisar una actividad física o deportiva con menores si ha sido objeto de una medida administrativa que le prohíba participar, en cualquier cargo, en la gestión y supervisión de instituciones y organismos sujetos a la legislación o reglamentos relativos a la protección de menores en un centro vacacional y de ocio, así como de grupos de jóvenes o si ha sido objeto de una medida administrativa para suspenderlos mismas funciones.

Además, si el profesional incumple estas obligaciones, se enfrenta a una pena de un año de prisión y una multa de 15.000 euros.

*Para ir más allá* Artículos L. 322-1, L. 212-9 y A. 322-1 del Código del Deporte.

### d. Algunas peculiaridades de la regulación de la actividad

**Obligación de seguro**

El operador de un EAPS está obligado a tomar un seguro de responsabilidad civil para cubrir los riesgos asociados con el ejercicio de su negocio. Este seguro también debe cubrir a todos los maestros, personas admitidas en la escuela de forma regular o casual, y a todos los asistentes del operador.

El profesional que ha contratado dicho seguro recibe un certificado que indica:

- Textos de referencia (legales y reglamentarios);
- El nombre de la compañía de seguros
- El número de pólizas de seguro suscritas
- El período de validez del contrato
- El nombre y la dirección del asegurado
- El alcance del importe de las garantías.

**Tenga en cuenta que**

El hecho de que el profesional no contrate dicho seguro se castiga con seis meses de prisión y una multa de 7.500 euros.

*Para ir más allá* Artículos L. 321-7, D. 321-1 y D. 321-4 del Código del Deporte.

**Garantías de salud y seguridad**

Todo EAPS debe contener en sus instalaciones:

- un kit de emergencia que contiene todos los productos y dispositivos para proporcionar primeros auxilios y alertar a los servicios de emergencia en caso de accidente;
- una tabla de organización de emergencia que enumera las direcciones y números de teléfono de los socorristas.

Además, el operador de la instalación debe mostrar una copia dentro de las instalaciones y de manera visible:

- todos los diplomas o títulos de formación para profesores, animadores o educadores deportivos que trabajen dentro del EAPS, así como su tarjeta profesional o, en su caso, su certificado de aprendiz;
- garantías de higiene y seguridad y normas técnicas aplicables a la supervisión de las actividades físicas y deportivas;
- certificación del contrato de seguro.

*Para ir más allá* Artículos L. 322-1 y R. 322-4 y los siguientes artículos del Código del Deporte.

**Tenga en cuenta que**

En caso de incumplimiento de estas obligaciones de seguro o garantías de salud y seguridad, el operador incurre en un cierre temporal o permanente de su establecimiento.

**Requisito de información**

**Al prefecto**

El operador de un EAPS está obligado a informar al prefecto si se produce un accidente o situación grave que podría suponer un riesgo grave para los profesionales.

*Para ir más allá* Artículo R. 322-6 del Código del Deporte.

**Hacia los participantes físicos y deportivos**

El operador de un EAPS debe informar a los futuros profesionales de las capacidades necesarias para realizar una actividad antes de que comience la actividad.

*Para ir más allá* Artículo A. 322-3 del Código del Deporte.

**Tenga en cuenta que**

También pueden ser necesarios algunos requisitos para ciertas actividades debido a su naturaleza (actividad náutica, actividad relacionada con el equino, actividad de caída libre, etc.). Para obtener más información, consulte la ficha técnica de la actividad en cuestión.

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

Es aconsejable referirse a la lista[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, se recomienda hacer referencia a la "Declaración de una empresa comercial" y "Registro de una empresa individual en el Registro de Comercio y Sociedades."

### b. Si es necesario, haga una declaración como educador deportivo

Siempre que la instalación incluya educadores deportivos, el operador deberá asegurarse de que ha realizado una declaración previa para llevar a cabo su actividad y que posee una tarjeta profesional.

Además, los profesionales deben renovar sus rendimientos cada cinco años.

**Autoridad competente**

La declaración debe dirigirse a la Dirección Departamental de Cohesión Social (DDCS) o a la Dirección Departamental de Cohesión Social y Protección de la Población (DDCSPP) del Departamento de Práctica principal o ejercicio, o directamente en línea en el[sitio web oficial del Ministerio del Deporte](https://eaps.sports.gouv.fr.).

**hora**

En el plazo de un mes a partir de la presentación del expediente de declaración, la prefectura envía un acuse de recibo al solicitante de registro. La tarjeta de visita, válida durante cinco años, se dirige al solicitante de registro.

**Documentos de apoyo**

La solicitud del profesional debe contener los siguientes documentos:

- Formulario de declaración de Cerfa 12699Completado y firmado;
- Una copia de un documento de identidad válido
- Un documento de identidad con foto
- Una declaración de honor que acredite la exactitud de la información en el formulario;
- Una copia de cada uno de los diplomas, títulos y certificados invocados;
- Una copia de la autorización para ejercer o, en su caso, la equivalencia del diploma;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate que datan de menos de un año.

Si se renueva la devolución, deberá adjuntar:

- Formulario Cerfa No. 12699Completado y firmado;
- Un documento de identidad con foto
- Una copia del certificado de revisión válido para las cualificaciones sujetas al requisito de reciclaje;
- un certificado médico de no contradictorio con la práctica y supervisión de las actividades físicas o deportivas de que se trate, de menos de un año de edad.

La propia prefectura solicitará la comunicación de un extracto de menos de tres meses de los antecedentes penales del solicitante de registro para verificar que no hay discapacidad o prohibición de práctica.

**Costo**

Gratis

*Para ir más allá* Artículos L. 212-11 y R. 212-85, y los artículos A. 212-176 a A. 212-178 del Código del Deporte.

### c. Autorización posterior al registro

**Declaración del censo de equipos deportivos**

El operador de un EAPS titular de equipos deportivos está obligado a hacer una declaración al prefecto del departamento en el que desea ejercer en el plazo de tres meses a partir del inicio de su actividad.

Todos los productos utilizados o desarrollados para la actividad física y deportiva se consideran equipos deportivos.

Esta declaración debe identificar:

- Equipamiento deportivo, sus características, su propietario y, en su caso, su operador;
- Si es así, la naturaleza de los cambios realizados
- el cesionor del equipo en caso de transferencia.

**Tenga en cuenta que**

Si se incumple esta obligación de notificación, se multa al operador con una multa de 150 euros.

*Para ir más allá* Artículo L. 312-2, y artículos R. 312-2 a R. 312-7 del Código del Deporte.

