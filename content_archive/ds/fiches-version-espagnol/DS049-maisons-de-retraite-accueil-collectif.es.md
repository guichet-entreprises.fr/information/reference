﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS049" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Casas de jubilación - Hogar Colectivo para Personas Mayores" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="casas-de-jubilacion-hogar-colectivo" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/casas-de-jubilacion-hogar-colectivo-para-personas-mayores.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="casas-de-jubilacion-hogar-colectivo-para-personas-mayores" -->
<!-- var(translation)="Auto" -->

Casas de jubilación - Hogar Colectivo para Personas Mayores
===========================================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

Las instalaciones de alojamiento para personas mayores (EHPA), más comúnmente conocidas como residencias de ancianos, son instalaciones de acogida y alojamiento colectivo que brindan atención a los ancianos, ya sea temporal o permanentemente durante el día. como por la noche.

**Es bueno saber**

Las residencias de ancianos no están medicalizadas. Sin embargo, si son el hogar de personas mayores dependientes, deben obtener una autorización de la agencia regional de salud (ARS) y firmar un acuerdo con el Consejo General para adquirir la condición de refugio para (EHPAD) y proporcionar atención médica. Estos EHPADs no estarán cubiertos en esta tarjeta.

*Para ir más allá* Artículo L. 312-1 del Código de Acción Social y Familias.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las actividades comerciales, es la Cámara de Comercio e Industria (CCI);
- si se crea una asociación:
  - si la asociación a una actividad lucrativa, la CFE competente será el servicio de las asociaciones de la prefectura del departamento y el registro del tribunal comercial,
  - si la asociación emplea empleados, la CFE competente será la Urssaf.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El profesional que desee abrir o ejecutar un EHPA debe:

- Mantener un nivel de calificación en el nivel bac de 3 o bac 4;
- hacer una convocatoria de proyectos sociales o médico-sociales cuando desee beneficiarse de la financiación pública (total o parcialmente) (véase infra "3.3). a. Hacer un archivo de llamada al proyecto");
- solicitar permiso para crear un EHPA (ver infra "3 grados). b. Solicitud de permiso para crear un EHPA").

**Director de la escuela**

Para servir como director de uno o más HPA, el profesional debe justificar una certificación de Nivel II (B.A. 3 o bac 4) registrada en el directorio nacional de certificaciones profesionales.[RNCP](http://www.cncp.gouv.fr).

También puede dirigir un establecimiento con menos de diez empleados o un establecimiento con una capacidad inferior a veinticinco plazas, el profesional si:

- Tiene:- un grado social o de salud de nivel III (Bac 2),
  - al menos tres años de experiencia profesional en el sector de la salud o médico-social;
- ha completado la capacitación de coaching o se compromete a tomarlo dentro de cinco años.

**Tenga en cuenta que**

Cuando el gerente de una EHPA (persona física o jurídica) confía la gestión a un profesional, debe transmitir a la autoridad competente la autorización para abrir la institución, un documento escrito que indique:

- Las habilidades y misiones encomendadas al líder;
- la naturaleza y el alcance de la delegación, incluidos:- Implementación del proyecto de liquidación o servicio,
  - Gestión y animación de los recursos humanos,
  - gestión presupuestaria, financiera y contable,
  - coordinación con instituciones externas y partes interesadas.

Este profesional debe, para poder dirigir esta institución, una certificación de Nivel I (Bac 5).

*Para ir más allá* Artículos D. 312-176-5 y D. 312-176-10 del Código de Acción Social y Familias.

**Personal**

Los servicios ofrecidos dentro de una EHPA deben ser realizados por equipos multidisciplinares cualificados con el fin de garantizar el confort y la calidad de residencia de las personas mayores.

*Para ir más allá* Artículo L. 312-1 del Código de Acción Social y Familias.

### b. Cualificaciones profesionales - Nacionales Europeos (Servicio Gratuito o Establecimiento Libre)

**Para una entrega de servicio gratuita (LPS)**

No se prevé que el nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo sobre el Espacio Económico Europeo (EEE) ejecute un AAN de forma temporal y ocasional en Francia. Como tal, el nacional está sujeto a los mismos requisitos que el nacional francés (véase más arriba "2." a. Cualificaciones profesionales").

**A la vista de un establecimiento gratuito (LE)**

Un ciudadano de la UE legalmente establecido que sea el líder de un EHPA puede realizar la misma actividad de forma temporal en Francia.

Para ello, la persona debe poseer un título, diploma o certificación profesional de un nivel equivalente al requerido para el nacional francés (véase más arriba "2o. a. Cualificaciones profesionales").

*Para ir más allá* Artículos D. 312-176-11 a D. 312-176-13 del Código de Acción Social y Familias.

### c. Algunas peculiaridades de la regulación de la actividad

#### Cumplimiento de las normas de seguridad y accesibilidad

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP). Es aconsejable consultar la hoja "Establecimiento que recibe al público" para obtener más información.

#### Requisito de tener un sistema de ambientador

El EHPA debe estar equipado con una habitación equipada con un sistema de refrigeración por aire cuyas condiciones se establecen dentro del orden siguiente.

*Para ir más allá* Artículo D. 312-161 del Código de Acción Social y Familias; decreto de 7 de julio de 2005 por el que se establecen las especificaciones del plan organizativo que se aplicará en caso de crisis sanitaria o climática y las condiciones para la instalación de un sistema fijo de refrigeración por aire o la provisión de una habitación o habitación en las instituciones mencionadas en la Sección L. 313-12 del Código de Acción Social y Familias.

#### Contrato de residencia

Mientras la duración de la estancia sea superior a dos meses (de forma continua o discontinua), deberá celebrarse un contrato de residencia entre el representante del establecimiento y la persona admitida.

Este contrato debe incluir la siguiente información:

- Los objetivos del cuidado del anfitrión;
- Los beneficios necesarios
- La descripción de la estancia y recepción y las condiciones de participación financiera del beneficiario;
- Tarifa de alojamiento.

**Tenga en cuenta que**

Un endoso debe especificar en un plazo máximo de seis meses los objetivos y beneficios adecuados para la persona que recibe.

*Para ir más allá* Artículos L. 313-12 y D. 311-1 del Código de Acción Social y Familias.

#### Receptores de beneficios

El nivel de pérdida de autonomía de los adultos mayores determina su pertenencia a uno de los seis llamados "grupos de isorecursos" (GIR) teniendo en cuenta la condición de la persona de acuerdo con la escala establecida en el Apéndice 3-6 Código de Acción Social y Familias.

Un EHPA solo puede acomodar a personas clasificadas dentro de los grupos GIR 6 o 5. Además, no puede mantener más del 10% de los residentes que han vuelto a depender de los grupos de la GIR 1 o 2. Si es necesario, EHPA tendrá que asociarse con un EHPAD circundante.

*Para ir más allá* Artículo R. 314-170-6 del Código de Acción Social y Familias.

#### Obligaciones diversas

**Documentos obligatorios**

Cada EHPA debe tener:

- Un reglamento que defina todas las normas que deben respetarse en términos de vida colectiva dentro de la institución y los derechos de cada persona alojada;
- Un registro de las entradas y salidas de cada residente incluyendo toda la información de identidad;
- un folleto de bienvenida y una carta de derechos y libertades de la persona acogido. Estos documentos deben ser entregados a cada residente a la llegada.

*Para ir más allá* Artículos L. 311-4, L. 311-7 y L. 331-2 del Código de Acción Social y Familias.

**Procedimiento de evaluación**

Los directores están obligados a realizar evaluaciones de sus actividades por parte de una agencia acreditada por la Agencia Nacional para la Evaluación y Calidad de las Instituciones y Servicios Sociales y Médico-Sociales. A continuación, deben transmitirlos a la autoridad que les ha expedido la autorización para abrir.

*Para ir más allá* Artículo L. 312-8 del Código de Acción Social y Familias.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Hacer un archivo de llamada al proyecto

Cuando el proyecto para crear, transformar o ampliar una EHPA utilice financiación pública, deberá seleccionarse tras una licitación emitida por la autoridad competente de conformidad con las especificaciones establecidas.

**Procedimiento de licitación**

La autoridad competente establece un calendario para las necesidades requeridas por la categoría de establecimiento para cubrir toda la oferta. A continuación, se proporciona al profesional dos meses para comentar el calendario.

**Autoridad competente**

El presidente del consejo del condado es competente para expedir la autorización, si la institución proporciona beneficios que pueden ser atendidos por la asistencia social departamental o cuando caen bajo una jurisdicción desmedida por la ley. Departamento.

**Documentos de apoyo**

La solicitud del solicitante debe ser atendida por una carta recomendada con previo aviso de recepción o cualquier otro medio para certificar la fecha de recepción e incluir:

- Todos los elementos que pueden identificarlo
- una declaración de honor que certifique que no es objeto de una condena definitiva por ninguno de los delitos contemplados en los artículos L. 313-22 y L. 313-22-1 del Código de Acción Social y Familias;
- una declaración sobre el honor de que no está sujeto a un procedimiento para suspender o prohibir el ejercicio de la actividad de un líder de EHPA;
- Una copia de la última certificación de las cuentas de la institución
- elementos descriptivos de su actividad social y medico-social y la situación financiera de su negocio;
- cualquier documento para describir el proyecto que cumpla con las especificaciones de la oferta;
- una declaración descriptiva de las principales características a las que el proyecto, cuyo contenido mínimo se fija en el Detenido 30 de agosto de 2010 sobre el contenido mínimo de la descripción de las principales características del proyecto presentado en el marco de la demanda de procedimiento de proyectos a que se refiere el artículo L. 313-1-1 del Código de Acción Social y Familias;
- En caso afirmativo, las variantes propuestas;
- Si varios de los gerentes de la institución se unen para proponer el proyecto, una declaración descriptiva de la cooperación propuesta.

**Resultado del procedimiento**

Una vez escuchada la solicitud, los candidatos son escuchados por una comisión de información y selección cuya composición se establece en el artículo R. 313-1 del Código de Acción Social y Familias. Esta comisión clasifica sus proyectos.

**Costo**

Gratis.

*Para ir más allá* Artículos R. 313-4 y los siguientes artículos del Código de Acción Social y Familias.

### b. Solicitud de autorización para crear un EHPA

**Autoridad competente**

El solicitante debe presentar su solicitud al presidente del consejo departamental del departamento en el que desea ejercer.

**Documentos de apoyo**

La solicitud debe incluir:

- para proyectos que no requieren financiación pública:- La identidad del solicitante
  - una declaración descriptiva de las principales características del proyecto, incluyendo:- La naturaleza de las prestaciones prestadas y sus beneficiarios,
    * La capacidad y las necesidades planificadas de la instalación,
    * Una distribución proyectada del personal por tipo de cualificaciones,
    * Un registro financiero
- para los proyectos que requieren financiación pública, los documentos justificativos son los necesarios para el candidato para la convocatoria de proyectos (véase supra "3o. a. Hacer un archivo de llamada al proyecto").

**hora**

Si no se hace respuesta en el plazo de seis meses, la solicitud se considerará rechazada. El solicitante podrá, por un período de dos meses, cuestionar a la autoridad los motivos de su denegación que, en su caso, deban dirigirse a él en el plazo de un mes.

**Remedios**

La decisión del prefecto puede estar sujeta a una apelación agraciada, jerárquica o contenciosa en un plazo de dos meses (ante el tribunal administrativo).

**Resultado del procedimiento**

La autorización se expide si el proyecto cumple con las normas de organización y operación, cumple con los objetivos establecidos y las necesidades médico-sociales y, en caso de convocatoria de proyectos, cumple con las especificaciones establecidas.

**Tenga en cuenta que**

Esta autorización se otorga por un período limitado de quince años. Su renovación es tácita, a menos que la autoridad indique lo contrario.

*Para ir más allá* Artículos L. 313-2, R. 313-1 y siguientes del Código de Acción Social y Familias.

### c. Formalidades de notificación de la empresa

El objetivo de este requisito es proporcionar una base jurídica para la empresa si se trata de una empresa comercial.

**Autoridad competente**

El propietario de la EHPA debe presentar la declaración de su negocio, y para ello debe hacer una declaración a la CPI.

**Documentos de apoyo**

El interesado debe proporcionar la documentos de apoyo dependiendo de la naturaleza de su actividad.

**hora**

El Centro de Formalidades Comerciales de la CPI envía un recibo al profesional el mismo día mencionando los documentos que faltan en el archivo. Si es necesario, el profesional dispone de un plazo de quince días para completarlo. Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si el centro de formalidad empresarial se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### d. Si es necesario, registre los estatutos de la empresa

El responsable de una EHPA, una vez fechados y firmados los estatutos de la empresa, debe registrarlos en la Oficina del Impuesto sobre Sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

### e. Autorización posterior al registro

#### Visita de cumplimiento

Una vez obtenida la autorización, y dentro de los dos meses siguientes a la apertura del establecimiento, se debe realizar una visita de cumplimiento dentro de la institución para asegurarse de que cumple con los requisitos de calidad y normas. organización y operación.

**Autoridad competente**

El profesional debe solicitar a la autoridad que emitió la autorización.

**Documentos de apoyo**

La solicitud de visita debe incluir:

- un proyecto de liquidación tal como se define en el artículo L. 311-8 del Código de Acción Social y Familias;
- las normas de funcionamiento de la escuela y un folleto de bienvenida en el artículo L. 311-4 del Código de Acción Social y Familias;
- elementos de liquidación (presupuesto, plan espacial, mesa de personal, modelo de contrato de residentes, etc.).

**Procedimiento**

La visita debe hacerse a más tardar tres semanas antes de la fecha de apertura del establecimiento. Al final de la reunión, se prepara un informe y se envía al profesional. La instalación puede comenzar a funcionar tan pronto como el resultado de la visita sea positivo. Por otra parte, si la institución no cumple, la autoridad competente envía al profesional, en el plazo de quince días, todos los cambios a los que debe realizar y el tiempo a su disposición para hacerlo. En caso necesario, la apertura del establecimiento estará sujeta a una contravisita del cumplimiento de las instalaciones del establecimiento en las mismas condiciones que la anterior.

*Para ir más allá* Artículos L. 311-8, D. 313-11 y posteriores, y D. 312-160 del Código de Acción Social y Familias.