﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS053" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseñanza" -->
<!-- var(title)="Profesor de danza" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="ensenanza" -->
<!-- var(title-short)="profesor-de-danza" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/ensenanza/profesor-de-danza.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="profesor-de-danza" -->
<!-- var(translation)="Auto" -->


Profesor de danza
=================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El profesor de danza es un profesional que enseña danza a un público diverso (desde niños pequeños hasta adultos). Ejerce tanto en instituciones públicas como privadas e interviene en la definición de los componentes pedagógicos, educativos y artísticos de la enseñanza.

Sólo las actividades de profesor de danza clásica, danza contemporánea y danza jazz se ven afectadas por esta hoja y por las regulaciones detalladas a continuación.

*Para ir más lejos:* Artículos L. 362-1 y siguientes del Código de Educación.

### b. Centro competente de formalidad empresarial

El centro de formalidades empresariales (CFE) correspondiente depende de la naturaleza de la actividad y de la forma jurídica de la empresa:

- para las profesiones liberales, la CFE competente es la Urssaf;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

Para obtener más información, visite el urssaf, el CCI de París, la CPI de Estrasburgo y el sitio web Service-public.fr

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

El interesado sólo podrá enseñar bajo el título de profesor de danza, o un título equivalente, si posee determinados diplomas o cualificaciones obligatorias.

Para poder ejercer como profesor de danza, el interesado debe, a elección:

- han obtenido un diploma de maestro de danza emitido por el estado o un certificado de aptitud para las tareas de los maestros de danza;
- han obtenido un diploma francés o extranjero reconocido equivalente al diploma estatal.

**Es bueno saber**

Algunas personas poseen el diploma de maestro de danza emitido por el estado. Se trata de artistas coreográficos que han recibido formación educativa y justifican una actividad profesional de al menos tres años en una de las siguientes instituciones:

- el ballet de la Opera Nacional de París;
- los ballets de los teatros del encuentro de los teatros de ópera municipales de Francia;
- Centros coreográficos nacionales;
- empresas de un Estado miembro de la Unión Europea (UE) u otro Estado parte en el Acuerdo sobre el Espacio Económico Europeo (EEE), cuyo listado se establece por decreto del Ministro responsable de la cultura.

Si la persona no tiene uno de estos diplomas, todavía puede enseñar danza con la condición que tiene, a su elección:

- una exención concedida debido a su fama particular o experiencia confirmada en la enseñanza de la danza;
- exención para aquellos que habían estado enseñando danza durante más de tres años al 11 de julio de 1989.

Para obtener más información sobre la expedición del certificado de exención o sobre los casos de reconocimiento de equivalencia de diploma, es aconsejable acercarse al Ministerio responsable de la cultura y la comunicación.

*Para ir más lejos:* Artículos L. 362-1 y siguientes del Código de Educación.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

#### En caso de establecimiento gratuito (LE)

Los nacionales que se encuentran en una de las siguientes situaciones pueden establecerse legalmente en Francia para enseñar danza por un precio, o hacer uso del título de profesor de danza.

- Si la persona tiene un diploma o título obtenido en un Estado de la UE o del EEE que regula el acceso o el ejercicio de la actividad, se requiere un certificado de competencia o certificado de formación expedido por las autoridades competentes;
- Si la persona tiene un diploma o título obtenido en un tercer país (fuera de la UE o del EEE), el interesado debe tener una cualificación de formación que haya sido reconocida por un Estado de la UE o del EEE que le haya permitido ejercer legalmente en ese Estado durante un período de un período mínimo de un año, siempre que esta experiencia laboral esté certificada por el estado en el que fue adquirida;
- Si la persona tiene un diploma o título obtenido en un Estado de la UE o del EEE, que no regula el acceso o el ejercicio de la profesión, la persona debe tener un certificado de competencia o un certificado de formación expedido por las autoridades Estado a certificar su disposición a ejercer la profesión, siempre que la persona esté justificada para llevar a cabo esta actividad por un año a tiempo completo en los últimos diez años en un Estado de la UE o EEE o a tiempo parcial durante un período de tiempo total equivalente. Esta justificación no es necesaria cuando la formación que conduce a esta profesión está regulada en el Estado miembro o en el Acuerdo EEE en el que ha sido validada.

Después de examinar si los conocimientos, las aptitudes y las aptitudes adquiridas por el solicitante, durante su experiencia profesional o su aprendizaje permanente y haber sido debidamente validados por un organismo competente, en un Estado miembro o en un tercer país, no pueden cubrir, total o parcialmente, diferencias sustanciales en la formación, el Ministro responsable de cultura podrá exigir que el solicitante se someta a medidas de compensación, la elección de este último, ya sea como un curso de ajuste o como una prueba de aptitud. Si el solicitante posee un certificado de competencia en el sentido del artículo 11 de la Directiva 2005/36/CE, de 7 de septiembre de 2005, relativo al reconocimiento de cualificaciones profesionales, el Ministro podrá prescribir el curso de adaptación o la prueba de aptitud.

#### En caso de prestación gratuita de servicios (LPS)

Se considera que un nacional de un Estado de la UE o del EEE que desee impartir danza en Francia de forma temporal e informal cumple los requisitos de cualificación profesional exigidos en ambas condiciones:

- estar legalmente establecidos en uno de estos Estados para llevar a cabo la misma actividad;
- si la actividad o formación que conduce a ella no está regulada en el estado en el que está establecida, haber ejercido la actividad de profesor de danza (en una de las opciones de danza clásica, contemporánea o jazz) durante al menos el equivalente de un año en el tiempo por un período de tiempo equivalente en los diez años anteriores a la prestación.

Si cumple las condiciones de cualificación profesional, el interesado sólo debe hacer una declaración previa de actividad antes de su primera actuación, para poder ejercer como profesor de danza en Francia, de manera ocasionales y temporales.

*Para ir más lejos:* Artículo 5 del Decreto de 23 de diciembre de 2008 sobre las condiciones de ejercicio de la profesión de profesor de danza aplicable a los nacionales de un Estado miembro de la Comunidad Europea u otro Estado Parte en el Acuerdo sobre el Espacio Económico Europeo, enmendada por la orden del 25 de julio de 2011 y la Sección L. 362-1-1 del Código de Educación.

#### Acceso parcial

El Ministro responsable del Espacio Económico Europeo podrá conceder acceso parcial a una actividad profesional en el marco de la profesión de profesor de danza, caso por caso, a los nacionales de la Unión Europea o a un Estado parte en el acuerdo sobre el Espacio Económico Europeo cultura cuando se cumplen las tres condiciones siguientes:

- el profesional está plenamente cualificado para llevar a cabo, en el Estado de origen, la actividad profesional para la que se solicita acceso parcial;
- las diferencias entre la actividad profesional legalmente realizada en el Estado de origen y la profesión regulada en Francia de un profesor de danza son tan importantes que la aplicación de medidas de compensación equivaldría a imponer al solicitante seguir el programa integral de educación y formación necesario en Francia para tener pleno acceso a esta profesión;
- la actividad profesional puede separarse objetivamente de otras actividades de la profesión de profesor de danza en Francia, ya que puede llevarse a cabo de forma independiente en el Estado de origen.

El acceso parcial podrá denegarse por razones imperiosas de interés público si esta denegación es proporcionada a la protección de dicho interés.

Las solicitudes de acceso parcial se consideran, según proceda, como solicitudes con el fin de establecer o prestar servicios temporales y ocasionales a la profesión.

### c. Condiciones de honorabilidad e incompatibilidad

Para poder ejercer como profesor de danza, el interesado no debe haber sido objeto de una condena:

- una pena de prisión sin una pena condicional de más de cuatro meses;
- por un delito de violación, agresión sexual, agresión sexual de un menor o proxenetismo.

*Para ir más lejos:* Artículo L. 362-5 del Código de Educación y artículos 222-22 y siguientes del Código Penal.

### d. Seguro de responsabilidad civil

El profesor de baile puede obtener un seguro de responsabilidad civil profesional. Permite que se cubra por daños a otros, ya sea directa o indirectamente en el origen.

**Es bueno saber**

El operador de la escuela de danza está obligado a contratar un contrato de seguro que cubra la responsabilidad civil de los profesores que ejercen allí.

*Para ir más lejos:* Artículo L. 462-1 del Código de Educación.

### e. Algunas peculiaridades de la regulación de la actividad

#### Obligación de verificar que los estudiantes tienen un certificado médico

El profesor de danza debe asegurarse, antes del inicio de cada período de enseñanza, de que los estudiantes tengan un certificado médico que acredite la ausencia de contraindicación a la enseñanza que se les ha proporcionado. Este certificado debe renovarse cada año. A petición de cualquier maestro, se puede requerir un certificado que acredite un examen médico adicional.

*Para ir más lejos:* Artículo R. 362-2 del Código de Educación.

#### Enseñar a los niños sobre la danza

En la mayoría de los tiempos, al practicar junto a los menores, los profesores de danza están sujetos a las siguientes obligaciones:

- los niños de cuatro y cinco años sólo pueden participar en actividades de excitación física (es decir, excluyendo todas las técnicas específicas de la disciplina enseñada);
- los niños de cinco y seis años sólo pueden participar en una actividad introductoria en la disciplina elegida (es decir, excluir todas las técnicas específicas de la disciplina que se enseña).

En general, todas las actividades realizadas por niños de cuatro a siete años incluidos no deben implicar ningún trabajo gravoso para el cuerpo, extensiones excesivas o articulaciones forzadas.

*Para ir más lejos:* Artículo R. 362-1 del Código de Educación.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, es aconsejable consultar las hojas de actividad "Formalidad de informar de una empresa comercial" o "Registro de una empresa individual en el registro de comercio y empresas."

### b. Si es necesario, solicite el reconocimiento de cualificaciones profesionales o renuncia a diplomas

#### Solicitar el reconocimiento de cualificaciones profesionales

Los nacionales de un Estado de la UE o del EEE que hayan obtenido sus cualificaciones profesionales en uno de estos Estados y que deseen establecerse en Francia para ejercer como profesorderos de danza, deben solicitar el reconocimiento de su cualificaciones en una o más opciones (clásica, contemporánea, jazz).

**Autoridad competente**

La solicitud de reconocimiento de cualificaciones profesionales debe dirigirse a la Dirección General de Creación Artística.

**hora**

La Dirección General de Creación Artística emite un recibo en el plazo de un mes a partir de la recepción de la solicitud de reconocimiento de cualificaciones profesionales.

El Ministro responsable de la cultura decide sobre la solicitud de reconocimiento de cualificaciones profesionales por decisión motivada en un plazo de cuatro meses a partir de la recepción del expediente completo.

**Cuestiones de demanda**

Después de revisar la solicitud, el Ministro de Cultura puede, a su elección:

- expedir un certificado de reconocimiento de cualificaciones profesionales al solicitante;
- ofrecer al solicitante someterse a una prueba de aptitud o a un curso de ajuste si ve diferencias sustanciales en la formación.

Si no hay respuesta en el plazo de cuatro meses, se considerará que se concede la solicitud de reconocimiento.

**Documentos de apoyo**

Los solicitantes deben adjuntar un expediente consistente en el formulario Cerfa 14531 a su solicitud de reconocimiento de cualificaciones profesionales.y documentos de apoyo, traducidos al francés, si los hay. Estas pruebas documentales varían en función de la situación del solicitante:

- si el solicitante posee un certificado de competencia o un certificado de formación expedido por las autoridades competentes de un Estado de la UE o del EEE que regula el acceso a la profesión o a su práctica y que permite que la profesión sea ejercitada legalmente en Estado:- Una copia de la identificación,
  - La descripción de la experiencia profesional del solicitante,
  - Una copia de los certificados de competencia profesional, documentos de formación o cualquier documento que acredite una cualificación profesional, expedido por la autoridad competente y que permita la práctica legal de la profesión,
  - el contenido de los estudios y prácticas realizados durante la formación inicial, indicando el número de horas por asignatura para las enseñanzas teóricas, la duración de las prácticas, el campo en el que se realizaron y, en su caso, el resultado de la evaluaciones realizadas; si es necesario, la encuesta de cursos de educación continua que muestra el contenido y la duración de estas pasantías. Estos elementos son emitidos y atestiguados por la estructura de formación en cuestión;
- si el solicitante posee un título de formación expedido por un tercer Estado reconocido por un Estado de la UE o del EEE y que ha permitido ejercer la profesión en dicho Estado durante al menos tres años:- Una copia de la identificación,
  - La descripción de la experiencia profesional del solicitante,
  - copiando el reconocimiento por un estado de la UE o del EEE del certificado de formación expedido por un tercer Estado,
  - una copia del documento que certifique el ejercicio por el solicitante de la profesión de profesor de danza durante un mínimo de tres años en el estado que haya reconocido el título de formación,
  - el contenido de los estudios y prácticas realizados durante la formación inicial indicando el número de horas por asignatura para las enseñanzas teóricas, la duración de las prácticas, el campo en el que se realizaron y, en su caso, los resultados de las evaluaciones logrado; si es necesario, la encuesta de cursos de educación continua que muestra el contenido y la duración de estas pasantías. Estos elementos son emitidos y atestiguados por la estructura de formación en cuestión. Cuando el plazo mínimo de un año no se haya realizado en el Estado que haya reconocido el diploma, certificado o título, el titular deberá ser reconocido como calificado por el Ministro responsable de la cultura, habida cuenta de los conocimientos y cualificaciones atestiguados por dicho diploma, certificado o título y por toda la formación y experiencia profesional adquirida;
- si el solicitante posee un certificado de competencia o un certificado de formación expedido por las autoridades competentes de un Estado de la UE o del EEE que no regula el acceso o el ejercicio de la profesión y el solicitante justifica el ejercicio de esta actividad a tiempo completo durante un año en los últimos diez años:- Una copia de la identificación,
  - La descripción de la experiencia profesional del solicitante,
  - Una copia de los certificados de competencia profesional, documentos de formación o cualquier documento que acredite una cualificación profesional, expedida por la autoridad competente de un Estado miembro de la UE o del EEE,
  - evidencia por cualquier medio de que la persona ha servido como profesor de danza durante al menos el equivalente de un año a tiempo completo o a tiempo parcial por un período de tiempo equivalente en los diez años anteriores a la solicitud. Esta evidencia no es necesaria cuando la formación que conduce a esta ocupación está regulada en la UE o en el Estado del EEE en el que se ha validado,
  - el contenido de los estudios y prácticas realizados durante la formación inicial indicando el número de horas por asignatura para las enseñanzas teóricas, la duración de las prácticas, el campo en el que se realizaron y, en su caso, los resultados de las evaluaciones logrado; si es necesario, la encuesta de cursos de educación continua que muestra el contenido y la duración de estas pasantías. Estos elementos son emitidos y atestiguados por la estructura de formación en cuestión.

**Costo**

Gratis.

*Para ir más lejos:* Artículos 5 y siguientes del auto de 23 de diciembre de 2008 y artículos L. 362-1-1 y siguientes del Código de Educación.

#### Si es necesario, solicite el reconocimiento de la equivalencia en el diploma estatal de profesor de danza

Esta aplicación proporciona una equivalencia de un diploma con el diploma estatal de profesor de danza.

**Autoridad competente**

La solicitud debe dirigirse a la Dirección General de Creación Artística.

**Tiempo de respuesta**

La respuesta a la solicitud se notifica en un plazo de diez meses a partir de la fecha del acuse de recibo de la solicitud. La decisión de rechazar debe estar justificada.

**Documentos de apoyo**

Los solicitantes deben adjuntar un expediente a su solicitud de reconocimiento de equivalencia que incluya:

- Formulario Cerfa 1044903 lleno, fechado y firmado;
- Una copia del diploma de profesor de danza para la que se requiere el reconocimiento de equivalencia;
- El currículum del solicitante
- cualquier exposición oficial que describa la formación que conduce al diploma: una descripción completa de la formación detallando la organización, la planificación, el volumen de tiempo y el currículo estudiado para cada asignatura, las condiciones de entrada en la formación, el modo evaluación de las enseñanzas, cómo se emite el diploma, etc. Todos estos documentos son preparados por la institución educativa que ha emitido el diploma.

**Tenga en cuenta que**

Cualquier pieza escrita en un idioma extranjero debe ir acompañada de un traductor certificado.

**Costo**

Gratis

*Para ir más lejos:* Artículos L. 362-1, Sección L. 362-4 del Código de Educación y Sección 25 de la Orden del 20 de julio de 2015 sobre Diferentes Caminos a la Profesión del Profesorado de Danza bajo la Sección L. 362-1 del Código de Educación.

#### Solicitar la exención del diploma estatal de profesor de danza

La exención del diploma podrá concederse en dos casos.

***Si la persona ha estado enseñando danza durante más de tres años a partir del 11 de julio de 1989***

**Autoridad competente**

La solicitud de exención debe dirigirse a la Dirección Regional de Asuntos Culturales del lugar de residencia del solicitante.

**Tiempo de respuesta**

Si no hay respuesta en un plazo de diez meses, la exención se considera concedida.

**Documentos de apoyo**

Los solicitantes deben adjuntar un expediente a su solicitud de exención que incluya:

- Formulario Cerfa 1044603 completado, fechado y firmado;
- Currículum del solicitante (seguimiento de su formación y actividades profesionales);
- Fotocopia de un documento de identidad (o tarjeta de residencia para extranjeros);
- Comprobante de residencia (liberación de alquiler, factura EDF-GDF, etc.);
- para los maestros asalariados: un certificado expedido por el empleador que acredite a los técnicos enseñados y el número de horas de instrucción trabajadas en este (o estos) años técnicos (o estos) antes del 10 de julio de 1989 (el número de horas no puede ser inferior a 100 por año);
- para los maestros no salariales: una prueba de la técnica (o) enseñada a lo largo de los tres años objeto de revisión y la hoja de inscripción al Fondo de Pensiones de Educación y Artes Aplicadas (Crea).

**Tenga en cuenta que**

Cualquier pieza escrita en un idioma extranjero debe ir acompañada de un traductor certificado.

**Costo**

Gratis.

*Para ir más lejos:* Artículo L. 362-4 del Código de Educación.

***Si la persona tiene una reputación particular o experiencia confirmada***

**Autoridad competente**

La solicitud de exención debe dirigirse a la Dirección General de Creación Artística.

**Tiempo de respuesta**

Si no hay respuesta en un plazo de diez meses, la exención se considera concedida.

**Documentos de apoyo**

Los solicitantes deben adjuntar un expediente a su solicitud de exención que incluya:

- Forma Cerfa 1045003 completado, fechado y firmado;
- Fotocopia de un documento de identidad oficial o tarjeta de residencia para extranjeros residentes en Francia;
- El currículum detallado del solicitante, incluido el seguimiento de la formación inicial y continua, así como el directorio de baile;
- todos los documentos (recortes de prensa, contratos, documentos educativos, diplomas, etc.) que puedan ilustrar la trayectoria profesional;
- En su caso, el certificado del empleador que acredite la técnica enseñada, sus períodos, frecuencia, volúmenes por hora, etc.
- Si corresponde, la hoja de inscripción al Fondo de Pensiones para la Educación y las Artes Aplicadas (Crea);
- si es necesario, una copia de la exención, para los candidatos que han trabajado en la enseñanza de la danza en Francia.

**Tenga en cuenta que**

Cualquier pieza escrita en un idioma extranjero debe ir acompañada de un traductor certificado.

**Costo**

Gratis.

*Para ir más lejos:* Artículos L. 362-1, Sección L. 362-4 del Código de Educación y Sección 25 de la Orden del 20 de julio de 2015 sobre Diferentes Caminos a la Profesión del Profesorado de Danza bajo la Sección L. 362-1 del Código de Educación.

### c. Hacer una declaración previa de actividad para los nacionales de la UE que realizan una actividad única (prestación gratuita de servicios)

Un nacional de un Estado de la UE o del EEE que desee ejercer como profesor de danza de forma temporal y ocasional en Francia debe hacer una declaración previa.

La solicitud deberá renovarse anualmente si el demandante tiene previsto ejercer su actividad durante el año de que se trate o en caso de un cambio material en su situación.

**Autoridad competente**

La declaración previa de actividad debe dirigirse a la Dirección General de Creación Artística.

**Tiempo de respuesta**

Al recibir el archivo de la persona, se emite un recibo en el plazo de un mes y le informa, si es necesario, de cualquier documento que falte. El Ministro responsable de la cultura decide sobre la solicitud por decisión motivada en un plazo de cuatro meses a partir de la recepción del expediente completo. Sin una respuesta dentro de este plazo, se considera que se concede el reconocimiento.

**Documentos de apoyo**

El solicitante de registro debe adjuntar a su declaración un expediente que consta de los siguientes documentos:

- Formulario Cerfa 14531Completado, fechado y firmado;
- información sobre contratos de seguro u otros medios de protección personal o colectiva en materia de responsabilidad personal (estos documentos no deben tener más de tres meses);
- un certificado que certifique que el demandante está legalmente establecido en un Estado miembro para ejercer como profesor de danza en una o más de las opciones (danza clásica, contemporánea o jazz);
- prueba de cualificaciones profesionales en una o más de las opciones (danza clásica, contemporánea o jazz);
- cuando la profesión de profesor de danza no esté regulada en el Estado miembro del lugar de establecimiento del demandante, las pruebas de que el demandante ha estado enseñando durante al menos el equivalente a un año a tiempo completo en los diez años antes de presentar la declaración. Esta justificación no es necesaria cuando la formación que conduce a esta ocupación está regulada en el Estado de la UE o en el EEE en el que se ha validado.

**Costo**

Gratis.

