﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS054" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentación" -->
<!-- var(title)="Comida rápida - Comida para llevar" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentacion" -->
<!-- var(title-short)="comida-rapida-comida-para-llevar" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/alimentacion/comida-rapida-comida-para-llevar.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="comida-rapida-comida-para-llevar" -->
<!-- var(translation)="Auto" -->


Comida rápida - Comida para llevar
==================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La comida rápida y la comida para llevar son para los establecimientos que sirven comidas presentadas en paquetes desechables para ser consumidos en el lugar o para llevar. Este tipo de actividad incluye comida rápida, pizzerías que ofrecen venta para llevar, tiendas de sándwiches, friteries, etc.

### b. Centro competente de formalidad empresarial

El centro de formalidades empresariales (CFE) depende de la naturaleza de la actividad, de la forma jurídica de la empresa y del número de empleados:

- para los artesanos y las empresas comerciales dedicadas a la actividad artesanal, siempre que no empleen a más de diez empleados, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para los artesanos y las empresas comerciales que emplean a más de diez empleados, esta es la Cámara de Comercio e Industria (CCI);
- en los departamentos de la Parte Alta y Baja del Rin, la Cámara de Comercio de Alsacia es competente.

**Es bueno saber**

En los departamentos del Bajo Rin, el Alto Rin y el Mosela, la actividad sigue siendo artesanal, independientemente del número de empleados empleados, siempre y cuando la empresa no utilice un proceso industrial. Por lo tanto, la CFE competente es la CMA o la Cámara de Comercio de Alsacia. Para más información, es aconsejable consultar el[web oficial de la Cámara de Comercio de Alsacia](http://www.cm-alsace.fr/).

*Para ir más allá* Artículo R. 123-3 del Código de Comercio.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

#### Entrenamiento de higiene

No se requiere diploma para abrir o administrar un restaurante. Sin embargo, los establecimientos de comida rápida deben tener al menos una persona en su fuerza de trabajo que pueda justificar la formación en higiene alimentaria.

**Tenga en cuenta que**

Se considera que han cumplido con esta obligación de formación:

- Individuos que pueden justificar al menos tres años de experiencia profesional con una empresa de alimentos como gerente u operador;
- títulos de Nivel V y títulos profesionales inscritos en el[directorio nacional de certificaciones profesionales](http://www.rncp.cncp.gouv.fr/) (RNCP) que se incluyen en el anexo del decreto de 25 de noviembre de 2011 relativo a la lista de diplomas y títulos con fines profesionales cuyos titulares se consideran que cumplen con la obligación específica de formación en higiene alimentaria adaptada a establecimientos de restauración comercial.

La capacitación puede ser impartida por cualquier organización de capacitación declarada al prefecto regional. Tiene una duración de 14 horas y tiene como objetivo adquirir la capacidad necesaria para organizar y gestionar las actividades de restauración en condiciones higiénicas de acuerdo con las expectativas de la normativa y ofreciendo satisfacción al cliente.

Al final de la formación, los alumnos deben ser capaces de:

- Identificar los principales principios de regulación en relación con la restauración comercial;
- analizar los riesgos asociados con la mala higiene en la restauración comercial;
- para aplicar los principios de higiene en la restauración comercial.

Para alcanzar estos objetivos, la formación se centra en:

- Alimentos y riesgo para el consumidor
- Los fundamentos de la normativa nacional y de la UE (destinados a la actividad de restauración comercial);
- Plan de control de salud.

*Para ir más allá* Artículos L. 233-4 y D. 233-11 a D. 233-13 del Código Rural y pesca marina; Decreto de 5 de octubre de 2011 relativo a las especificaciones de la formación específica en higiene alimentaria adaptadas a la actividad de los establecimientos de restauración comercial; anexo al decreto de 25 de noviembre de 2011 relativo a la lista de diplomas y títulos con fines profesionales cuyos titulares se consideren que cumplen con el requisito específico de formación en higiene alimentaria adaptado a la actividad de la establecimientos de restauración comercial.

### b. Para los puntos de venta de bebidas, requisito de nacionalidad

No hay ningún requisito de nacionalidad para abrir un restaurante de comida rápida. Sin embargo, una vez que el restaurador sirve bebidas alcohólicas, debe justificar que:

- Francés;
- un nacional de un Estado de la Unión Europea (UE) o un Estado Parte en el Acuerdo del Espacio Económico Europeo (EEE);
- un nacional de un Estado que ha celebrado un tratado de reciprocidad con Francia, como Argelia o Canadá.

Las personas de otra nacionalidad no pueden abrir un restaurante que sirva bebidas alcohólicas bajo ninguna circunstancia. Sin embargo, este requisito de nacionalidad no es necesario en caso de un adeudo temporal. Para obtener más información, es aconsejable consultar la hoja de actividades[Beber](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*Para ir más allá* Artículos L. 3334-1 y L. 3352-3 del Código de Salud Pública.

### c. Para puntos de venta de bebidas, condiciones de honor e incompatibilidad

No hay ninguna condición de honorabilidad o incompatibilidad para abrir un restaurante de comida rápida. Por otra parte, mientras el restaurador sirva bebidas alcohólicas, deberá respetar las condiciones de honor y las incompatibilidades relativas a la profesión de bebida.

#### Incompatibilidades y discapacidades

Los menores no emancipados y los adultos bajo tutela no pueden ejercer la profesión de beber por su cuenta.

**Tenga en cuenta que**

La práctica de beber por un menor no emancipado o por un mayor bajo tutela es un delito castigado con una multa de 3.750 euros. El tribunal puede ordenar el cierre de la instalación por un máximo de cinco años.

Además, el ejercicio de determinadas profesiones es incompatible con el funcionamiento de un establecimiento de bebida: alguaciles, notarios, funcionarios, etc.

*Para ir más allá* Artículos L. 3336-1 y L. 3352-8 del Código de Salud Pública.

#### Integridad

No utilice puntos de venta de bebidas en el hotel:

- Personas condenadas por delitos comunes o proxenetismo;
- personas condenadas a al menos un mes de prisión por robo, fraude, violación de confianza, recepción, hilado, recepción de delincuentes, indecencia pública, celebración de una casa de apuestas, apuestas ilegales en carreras de caballos, venta de bienes falsificados o nocivos, delitos de drogas o por re-agresión y embriaguez pública. En estos casos, la discapacidad cesa en caso de indulto y cuando la persona no ha incurrido en una pena de prisión correccional durante los cinco años siguientes a la condena.

También puede imponerse incapacidad a los condenados por el delito de poner en peligro a los menores.

Las condenas por delitos y por los delitos antes mencionados contra un adeudo que consume bebidas que se consuman in situ, implican, como resultado, legítimamente en su contra y por el mismo período de tiempo, la prohibición de operar un adeudo, desde el día en que las condenas se han convertido en definitivas. Este adeudado no puede ser utilizado, en ningún cargo, en el establecimiento que operaba, ni en el establecimiento operado por su cónyuge separado. Tampoco puede trabajar al servicio de la persona a la que ha vendido o alquilado, ni por quien gestiona dicho establecimiento.

**Tenga en cuenta que**

La violación de estas prohibiciones es un delito castigado con una multa de 3.750 euros y el cierre permanente del establecimiento.

*Para ir más allá* Artículos L. 3336-1 a L. 3336-3 y L. 3352-9 del Código de Salud Pública; Artículos 225-5 y siguientes del Código Penal.

### d. Si es necesario, obtenga una licencia para operar

Cualquier persona que desee abrir un establecimiento con una "licencia de restaurante pequeño" o "licencia de restaurante" debe someterse a una formación específica que incluya:

- conocimiento de la legislación y reglamentos aplicables a los puntos de venta de bebidas y restaurantes en el lugar;
- obligaciones de salud pública y orden público.

Esta formación es obligatoria y da lugar a la emisión de una licencia de explotación de 10 años. Al final de este período, la participación en la formación de actualización de conocimientos amplía la validez de la licencia de explotación por otros 10 años.

Esta formación también es obligatoria en caso de transferencia (cambio de propiedad), traducción (movimiento) o transferencia de una comuna a otra.

El programa de formación consiste en enseñanzas de una duración mínima de:

- 20 horas repartidas en al menos 3 días;
- 6 horas si la persona está justificada por diez años de experiencia profesional como operador;
- 6 horas para la formación de actualización de conocimientos.

Para obtener más información, es aconsejable consultar la hoja de actividades[Beber](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*Para ir más allá* Artículos L. 3332-1-1 y R. 3332-4-1 y los siguientes del Código de Salud Pública; decreto de 22 de julio de 2011 por el que se establece el programa y la organización de la formación necesaria para obtener los certificados previstos en el artículo R. 3332-4-1 del Código de Salud Pública.

### e. Si es necesario, obtenga una licencia

#### Licencia de tomaderos y restauradores de bebidas en el lugar

La licencia depende de la categoría de alcohol vendido y varía en función de si las bebidas se consumen en el lugar o se venden como comida para llevar.

Los puntos de venta de bebidas en el lugar se dividen en dos categorías en función del alcance de la licencia a la que estén asociados:

- La licencia de 3a categoría, conocida como "licencia restringida", incluye la autorización para vender bebidas de los grupos 1 y 3 en el lugar;
- La licencia de 4a categoría, conocida como "licencia de gran tamaño" o "licencia de año completo", incluye la autorización para vender todas las bebidas que quedan permitidas para el consumo en interiores, incluidas las del grupo 4o y 5o.

**Tenga en cuenta que**

La Ordenanza No 2015-1682 de 17 de diciembre de 2015 eliminó la Licencia de Segunda Clase. Como resultado, las licencias de 2a categoría se convierten en el derecho completo de las licencias de 3a categoría.

Los restaurantes que no posean una licencia de bebidas en el lugar deben tener una de las dos categorías de licencias para vender bebidas alcohólicas:

- la "licencia de restaurante pequeño" que permite la venta de las bebidas del 3er grupo para su consumo in situ, pero sólo con motivo de las principales comidas y como accesorios de alimentación;
- la "licencia de restaurante" en sí, que permite que todas las bebidas que están permitidas para ser consumido en el lugar, pero sólo con motivo de las comidas principales y como accesorios de alimentos, para ser vendidos en el sitio.

Los establecimientos con licencia de consumidor o licencia de restaurante en el lugar pueden vender bebidas en la categoría de su licencia para llevar.

*Para ir más allá* Artículos L. 3331-1 y siguientes del Código de Salud Pública.

**Clasificación de bebidas en cuatro grupos**

Las bebidas se dividen en cuatro grupos para la regulación de su fabricación, venta y consumo:

- refrescos (grupo 1): agua mineral o carbonatada, zumos de frutas o vegetales que no fermentan o no contienen, tras el inicio de la fermentación, trazas de alcohol superiores a 1,2 grados, limonadas, jarabes, infusiones, leche, café, té, chocolate;
- bebidas fermentadas destiladas y vinos dulces naturales (grupo 3): vino, cerveza, sidra, pera, aguamiel, a los que se unen vinos dulces naturales, así como cremas de grosella negra y zumos de frutas o verduras fermentados con 1,2 a 3 grados de alcohol, vinos de licor, aperitivos a base de vino y licores de fresa, frambuesas, cubiertas negras o cerezas, con no más de 18 grados de alcohol puro;
- grupo 4: rones, tafias, licores procedentes de la destilación de vinos, sidras, peras o frutas, y sin apoyar ninguna adición de gasolina, así como licores edulcorados con azúcar, glucosa o miel a un mínimo de 400 gramos por litro para licores anisados y un mínimo de 200 gramos por litro para otros licores y que no contengan más de medio gramo de gasolina por litro;
- Grupo 5: todas las demás bebidas alcohólicas.

*Para ir más allá* Artículo L. 3321-1 del Código de Salud Pública.

#### Licencias para bebidas para llevar

Para vender bebidas alcohólicas, otros establecimientos para llevar de bebidas deberán estar provistos de una de las dos categorías de licencias siguientes:

- La "licencia de pequeña licencia para llevar" incluye la autorización de venta para llevar las bebidas del 3er grupo;
- la propia "licencia de compra" incluye la autorización para vender todas las bebidas para la venta.

*Para ir más allá* Artículo L. 3331-3 del Código de Salud Pública.

### f. Algunas peculiaridades de la regulación de la actividad

#### Cumplir con las regulaciones de las instituciones públicas (ERP)

Para obtener más información, es aconsejable consultar la hoja de actividades[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

#### Respetar las normas sanitarias y de higiene

El restaurador debe aplicar las normas del Reglamento CE 852/2004, de 29 de abril de 2004, relativas a la higiene alimentaria, que se refieren a:

- Las instalaciones utilizadas para la alimentación;
- Equipo
- Desperdicio de alimentos
- Suministro de agua
- Higiene personal
- alimentos, envases y envases.

Este Reglamento prevé la aplicación de procedimientos basados en los principios de HACCP*(Punto de control crítico de análisis de peligros).*

El restaurador también debe aplicar las normas establecidas en el orden del 21 de diciembre de 2009 sobre normas sanitarias para la venta al por menor, el almacenamiento y el transporte de productos animales y alimentos en Contiene.

Estos incluyen:

- Condiciones de temperatura de conservación
- Procedimientos de congelación
- cómo se preparan las carnes molidas y se recibe la caza.

Por último, debe cumplir las normas sobre las temperaturas de conservación de los productos alimenticios perecederos de origen vegetal promulgadas por decreto de 8 de octubre de 2013.

*Para ir más allá* : REGLAMENTO CE 852/2004, de 29 de abril de 2004, relativo a la higiene alimentaria; 21 de diciembre de 2009 orden sobre normas sanitarias para la venta al por menor, almacenamiento y transporte de productos animales y alimentos en contenedores; Orden de 8 de octubre de 2013 sobre normas sanitarias para la venta al por menor, almacenamiento y transporte de productos y productos alimenticios distintos de los productos animales y los alimentos en contenedores.

#### Ver precios e información obligatoria del producto

##### Precios de las bebidas y alimentos más comúnmente servidos

Los restauradores deben mostrar de manera visible y legible desde fuera de la propiedad y en las parcelas al aire libre reservadas para los clientes, los precios cobrados, independientemente del lugar de consumo, de las bebidas y productos alimenticios más comunes a continuación y nombrado:

- La taza de café negro
- medio montón de cerveza de barril;
- una botella de cerveza (contenida servida);
- zumo de fruta (contención servida);
- un refresco (contenido)
- agua mineral plana o espumosa (contenida)
- aperitivo anís (contenido servido);
- un plato del día;
- Un sándwich.

El nombre y los precios deben indicarse mediante letras y figuras con una altura mínima de 1,5 cm.

*Para ir más allá* : orden de 27 de marzo de 1987 relativo a la exhibición de precios en los establecimientos que sirven comidas, productos alimenticios o bebidas que se consumirán en el establecimiento.

##### Menús y mapas

En los establecimientos que sirven comidas, menús o menús del día, así como un menú con al menos los precios de cinco vinos o de otro modo los precios de los vinos si se sirven menos de cinco, debe mostrarse de forma visible y legible desde el exterior durante duración del servicio y al menos a partir de las 11:30 a.m. para el almuerzo y las 6 p.m. para la cena.

En el caso de que ciertos menús se sirvan sólo en ciertas horas del día, esta característica debe mencionarse claramente en el documento mostrado.

En los establecimientos no vitivinícolas, se mostrará un menú con al menos la naturaleza y los precios de cinco bebidas de servicio común.

Dentro de estos establecimientos, los menús o tarjetas idénticas a las que se muestran fuera deben estar a disposición de los clientes.

Las tarjetas y menús deben incluir, para cada servicio, el precio, así como la palabra "bebida incluida" o "bebida no incluida" y, en todos los casos, indicar para las bebidas la naturaleza y capacidad ofrecidas.

*Para ir más allá* : orden de 27 de marzo de 1987 relativo a la exhibición de precios en los establecimientos que sirven comidas, productos alimenticios o bebidas que se consumirán en el establecimiento.

##### Mención "casera"

Los restauradores deben indicar en sus tarjetas o cualquier otro medio visible para todos los consumidores que un plato propuesto es "hecho en casa".

Un plato es "hecho en casa" cuando se hace en el lugar con productos crudos.

*Para ir más allá* Artículos L. 122-19 en L. 122-21 y D. 122-1 a D. 122-3 del Código del Consumidor.

##### Origen de las carnes

El origen de la carne de vacuno deberá indicarse mediante cualquiera de los siguientes documentos:

- "origen: (nombre del país)" cuando el nacimiento, la cría y el sacrificio del ganado del que proceden las carnes tuvieron lugar en el mismo país;
- "nacidos y criados: (nombre del país de nacimiento y nombre del país de cría) y sacrificados: (nombre del país de sacrificio)" cuando el nacimiento, la cría y el sacrificio tuvieron lugar en diferentes países.

Estas menciones se hacen a la atención del consumidor, de una manera legible y visible, por visualización, indicación en tarjetas y menús, o en cualquier otro medio.

**Tenga en cuenta que**

La venta de carne de vacuno cuyo origen no se da a conocer al consumidor constituye un billete de 5a clase. La multa es de 1.500 euros como máximo y, en caso de reincidencia, 3.000 euros.

*Para ir más allá* : decreto de 17 de diciembre de 2002 sobre el etiquetado de la carne de vacuno en los establecimientos de servicios alimentarios; Artículo 131-13 del Código Penal.

##### Ingredientes que pueden causar alergias o intolerancias

En los lugares donde se ofrecen comidas para ser consumidas en el lugar, se pone en conocimiento del consumidor, en forma escrita, de una manera legible y visible de los lugares donde se admite al público:

- información sobre el uso de productos alimenticios que causen alergias o intolerancias;
- cómo se ponen a su disposición esta información. En este caso, el consumidor puede acceder a la información directa y libremente, disponible por escrito.

*Para ir más allá* Artículos R. 412-12 a R. 412-16 del Código del Consumidor.

#### Cumplir con las regulaciones sobre la supresión de la embriaguez pública y la protección de los menores

##### Prohibiciones de venta de bebidas

Está prohibido:

- para vender bebidas alcohólicas a menores de edad o para ofrecerlos de forma gratuita. La persona que emite la bebida requiere que el cliente establezca una prueba de su mayoría;
- ofrecer, gratis o caro, a un menor cualquier objeto que incite directamente al consumo excesivo de alcohol;
- venta al por menor a crédito para los grupos 3o, 4o y 5o para ser consumido en el lugar o para llevar;
- vender 3 a 5 bebidas del grupo en estadios, salas de educación física, gimnasios y, en general, todas las instalaciones físicas y deportivas;
- ofrecer bebidas alcohólicas gratuitas con fines comerciales de forma gratuita o venderlas como principal por una suma global, excepto en el contexto de partes y ferias tradicionales declaradas o fiestas de noticias y ferias autorizadas por el Representante del Estado cuando se trata de degustaciones a la venta;
- ofrecer bebidas alcohólicas con descuento por un período limitado de tiempo sin ofrecer también bebidas sin alcohol a precio reducido.

*Para ir más allá* Artículos L. 3335-4, L. 3342-1, L. 3342-3, L. 3322-9 y R. 3335-9 del Código de Salud Pública; Artículo 131-1 del Código Penal.

##### Mostrar bebidas no alcohólicas

En todos los puntos de venta de bebidas, es obligatorio exhibir bebidas no alcohólicas para la venta en el establecimiento. La pantalla debe incluir al menos diez botellas o recipientes y, siempre que se suministe la salida, una muestra de al menos cada categoría de las siguientes bebidas:

- jugos de frutas, jugos de verduras;
- bebidas de jugo de fruta carbonatado;
- sodas;
- limonados;
- jarabes;
- aguas ordinarias artificiales o sin reasidarse;
- aguas minerales carbonatadas o no carbonatadas.

Esta pantalla, separada de la de otras bebidas, debe instalarse de forma destacada en las instalaciones donde se sirve a los consumidores.

*Para ir más allá* Artículo L. 3323-1 del Código de Salud Pública.

##### Mostrar disposiciones sobre la supresión de la embriaguez pública y la protección de los menores

Los restauradores que vendan bebidas alcohólicas deben colocar un cartel que les recuerde las disposiciones del Código de Salud Pública relativas a la supresión de la embriaguez pública y la protección de los menores.

*Para ir más allá* Artículo L. 3342-4 del Código de Salud Pública.

#### Respetar la prohibición de fumar

No está permitido fumar en los restaurantes. La prohibición de fumar se aplica en todas las áreas cerradas y cubiertas que están abiertas al público o que son lugares de trabajo.

Un**Señalización** recordar el principio de una prohibición de fumar.

Sin embargo, esta regla no se aplica a las terrazas que son espacios al aire libre.

Además, el gerente del establecimiento puede decidir crear sitios solo para fumadores. Se trata de habitaciones cerradas, utilizadas para el consumo de tabaco y en las que no se prestan servicios.

Para obtener más información sobre la normativa aplicable a terrazas y parcelas reservadas, puede consultar las hojas de actividades[Beber](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/) Y[Catering tradicional](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/restaurant-traditionnel/).

*Para ir más allá* Artículos L. 3512-8 y R. 3512-2 a R. 3512-7 del Código de Salud Pública; circular 2008-292, de 17 de septiembre de 2008, sobre las modalidades de aplicación de la segunda fase de la prohibición del tabaquismo en lugares de uso colectivo.

#### Si es necesario, solicite permiso para transmitir música

Los restaurantes que deseen transmitir música sonora deben obtener el permiso de la Sociedad de Autores, Compositores y Editores de Música (Sacem).

Deben pagarle dos regalías:

- derechos de autor que pagan por el trabajo de creadores y editores. El importe depende del municipio en el que se encuentre el establecimiento, del número de plazas y del número de dispositivos de radiodifusión instalados;
- para la radiodifusión a través de los medios de comunicación grabados, la remuneración justa, destinada a distribuirse entre los artistas intérpretes o ejecutantes y los productores de música. Sacem cobra esta tarifa en nombre de la Fair Compensation Collection Corporation (SPRE). Se determina por el número de escaños y el número de habitantes de la comuna del establecimiento.

Para más información, es aconsejable consultar los sitios web oficiales de la[Sacem](https://clients.Sacem.fr/autorisations/etablissement-de-restauration-rapide?keyword=Restauration+rapide) y el[Spre](http://www.spre.fr/).

*Para ir más allá* Artículos L. 214-1 y los siguientes artículos del Código de Propiedad Intelectual; Decisión del 30 de noviembre de 2011 de la Comisión en virtud del artículo L. 214-4 del Código de Propiedad Intelectual por la que se modifica la Decisión del 5 de enero de 2010.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, se recomienda consultar las hojas de actividades "Declaración de una empresa comercial" y "Registro de una empresa individual en el Registro de Comercio y Empresas."

### b. Declarar establecimientos que distribuyan productos alimenticios que contengan productos animales

Todo operador de un establecimiento que produzca, manipule o almacene productos animales (carne, productos lácteos, productos pesqueros, huevos, miel) para consumo humano debe cumplir la obligación de declarar cada uno de los instituciones de las que es responsable, así como las actividades que se llevan a cabo allí.

La declaración deberá efectuarse antes de la apertura del establecimiento y renovarse en caso de cambio de operador, dirección o naturaleza de la actividad.

#### Autoridad competente 

La declaración debe dirigirse al prefecto del departamento de aplicación del establecimiento o, en el caso de los establecimientos bajo la autoridad o tutela del Ministro de Defensa, al servicio de salud de las fuerzas armadas.

#### Documentos de apoyo

El solicitante debe presentar el Formulario Cerfa 1398403 lleno.

*Para ir más allá* Artículo R. 233-4 del Código Rural y Pesca Marina; Artículo 2 del Decreto de 28 de junio de 1994 sobre la identificación y acreditación sanitaria de establecimientos en el mercado de productos animales o animales y marcas de seguridad.

### c. Si es necesario, declarar la apertura del restaurante en caso de venta de alcohol

Una persona que quiere abrir un restaurante y vender alcohol allí está obligado a hacer una declaración preadministrativa. Este requisito de notificación previa también se aplica en el caso de:

- Cambio en la persona del propietario o gerente
- traducción de un lugar a otro. Sin embargo, la presentación de informes no es obligatoria cuando la traducción es llevada a cabo por el titular del fondo comercial o sus titulares de derechos, siempre que no aumente el número de adeudos existentes en la comuna y cuando se lleve a cabo en un área desprotegida.

**Tenga en cuenta que**

Abrir una licorería sin hacer esta declaración previa en las formas requeridas es un delito castigado con una multa de 3.750 euros.

*Para ir más allá* Artículo L. 3352-3 del Código de Salud Pública.

**Es bueno saber**

Este requisito de preinformación no se aplica en los departamentos del Alto Rin, el Rin inferior y el Mosela. En estos departamentos, el artículo 33 del Código Local de Profesiones, de 26 de julio de 1900, sigue en vigor. Por lo tanto, depende del conservador completar un formulario de solicitud para operar una licencia de licor disponible en los departamentos de prefectura y subprefectura de estos tres departamentos.

Para obtener más información, puede ver la hoja de actividades [Beber](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*Para ir más allá* Artículos L. 3332-3 a L. 3332-7 del Código de Salud Pública.

#### Autoridad competente

La declaración debe dirigirse al ayuntamiento del municipio del lugar de establecimiento. En París, se lleva a cabo en la prefectura de policía.

#### hora

La declaración debe hacerse al menos 15 días antes de la apertura. En el caso de una mutación por muerte, el período de notificación es de un mes. Inmediatamente se emite un recibo.

Dentro de los 3 días de la declaración, el alcalde de la comuna donde se hizo envía una copia completa al fiscal y al prefecto.

#### Documentos de apoyo

Los documentos justificativos necesarios son:

- Impresión de ciervos 11542Completado;
- La licencia de explotación
- Licencia
- prueba de nacionalidad.

