﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS085" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Herrero" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="herrero" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/herrero.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="herrero" -->
<!-- var(translation)="Auto" -->

Herrero
=======

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->

1°. Definición de la actividad
-----------------------------

### a. Definición

El farrier es un profesional cuya misión principal es hacer y poner hierros en las pezuñas de caballos y otros equinos.

Se encarga del parage de la pezuña (dar al pie del caballo su forma y longitud óptimas) antes de dar forma al hierro que se adaptará exactamente a la apariencia del caballo. En algunos casos, de acuerdo con el veterinario, tendrá que hacer accesorios ortopédicos o terapéuticos.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- Para una actividad artesanal, la CFE competente es la Cámara de Comercios y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI).

Si el profesional tiene una actividad de compra y reventa, su actividad es artesanal y comercial.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Cualificaciones profesionales

El interesado que desee trabajar como farrier debe tener una cualificación profesional o un ejercicio bajo el control efectivo y permanente de una persona con esta cualificación.

Para ser considerado profesionalmente calificado, la persona debe poseer uno de los siguientes diplomas o títulos:

- Certificado de Habilidades Profesionales (CAP) farrier agrícola;
- patente técnica del farrier (BTM).

*Para ir más allá* Artículo 16 de la Ley 96-603, de 5 de julio de 1996, de desarrollo y promoción del comercio y la artesanía, Decreto 98-246, de 2 de abril de 1998, relativo a la cualificación profesional necesaria para las actividades del artículo 16 de la Ley no. 96-603 de 5 de julio de 1996.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

#### Para un ejercicio temporal e informal (Entrega de servicio gratuito)

Todo nacional de un Estado miembro de la Unión Europea (UE) o parte en el acuerdo del Espacio Económico Europeo (EEE), que esté establecido y practique legalmente la actividad de farrier en ese Estado, podrá ejercer en Francia, temporalmente y ocasional, la misma actividad.

En primer lugar, deberá presentar la solicitud mediante declaración a la CMA del lugar en el que desea llevar a cabo el servicio.

En el caso de que la profesión no esté regulada, ni en el curso de la actividad ni en el marco de la formación, en el país en el que el profesional esté legalmente establecido, deberá haber realizado esta actividad durante al menos un año, en el transcurso de los diez años antes de la prestación, en uno o varios Estados miembros de la UE.

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la CMA competente podrá exigir que el interesado se someta a una prueba de aptitud.

*Para ir más allá* Artículo 17-1 de la Ley de 5 de julio de 1996; Artículo 2 de la decreto del 2 de abril de 1998 modificado por el decreto del 4 de mayo de 2017.

### Para un ejercicio permanente (Establecimiento Libre)

Para llevar a cabo la actividad de farrier en Francia de forma permanente, la UE o el nacional del EEE deben cumplir una de las siguientes condiciones:

- tienen las mismas cualificaciones profesionales que las requeridas para un francés (véase más arriba: "2. a. Cualificaciones profesionales");
- poseer un certificado de competencia o certificado de formación requerido para el ejercicio de la actividad de los farrier en un Estado de la UE o del EEE cuando dicho Estado regula el acceso o el ejercicio de esta actividad en su territorio;
- tener un certificado de competencia o un certificado de formación que certifique su preparación para el ejercicio de la actividad de los farrier cuando este certificado o título se haya obtenido en un Estado de la UE o del EEE que no regule el acceso o El ejercicio de esta actividad
- ser titular de un título, título o certificado adquirido en un tercer Estado y admitido en equivalencia por un Estado de la UE o del EEE con la condición adicional de que el interesado haya estado en un punto de venta en el Estado que haya admitido Equivalencia.

Una vez que el nacional de un Estado de la UE o del EEE cumpla una de las condiciones anteriores, podrá solicitar un certificado de reconocimiento de la cualificación profesional (véase más adelante: "5. b. Solicitar un certificado de cualificación profesional para el ejercicio permanente de la UE o del EEE para un ejercicio permanente (LE))

Cuando existan diferencias sustanciales entre la cualificación profesional del nacional y la formación requerida en Francia, la CMA competente podrá exigir que el interesado se someta a medidas de compensación (véase infra "5o. a. Bueno saber: medidas de compensación").

*Para ir más allá* Artículos 17 y 17-1 de la Ley 96-603, de 5 de julio de 1996; Artículos 3 a 3-2 del Decreto de 2 de abril de 1998 modificado por el decreto de 4 de mayo de 2017.

### c. Condiciones de honorabilidad, reglas éticas, ética

Nadie puede practicar como un farrier si es el sujeto de:

- la prohibición de ejecutar, administrar, administrar o controlar directa o indirectamente una empresa comercial o artesanal;
- una pena de prohibición de la actividad profesional o social por cualquiera de los delitos o delitos menores previstos en el artículo 131-6 del Código Penal.

*Para ir más allá* Artículo 19 III de la Ley 96-603, de 5 de julio de 1996.

### d. Algunas peculiaridades de la regulación de la actividad

#### Reglamento sobre la calidad del artesano y los títulos de maestro artesano y mejor trabajador en Francia

**Artesanía**

Para reclamar la condición de artesano, la persona debe justificar:

- una PAC, un BEP o un título certificado o registrado cuando se emitió al RNCP al menos equivalente (véase arriba: "2. a. Cualificaciones profesionales");
- experiencia profesional en este comercio durante al menos tres años.

*Para ir más allá* Artículo 1 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de maestro artesano**

Este título se otorga a las personas, incluidos los líderes sociales de las personas jurídicas:

- Registrado en el directorio trades;
- titulares de un máster en comercio;
- justificando al menos dos años de práctica profesional.

**Tenga en cuenta que**

Las personas que no poseen el título de máster pueden solicitar a la Comisión Regional de Cualificaciones el título de Maestro Artesano bajo dos supuestos:

- cuando están inscritos en el directorio de oficios, tienen un título al menos equivalente al máster, y justifican la gestión y los conocimientos psicopedagógicos equivalentes a los de las correspondientes unidades de valor del máster y que tienen dos años de práctica profesional;
- cuando se han inscrito en el repertorio de comercio durante al menos diez años y tienen un know-how reconocido por promover la artesanía o participar en actividades de formación.

*Para ir más allá* Artículo 3 del Decreto 98-247, de 2 de abril de 1998, sobre la cualificación artesanal y el repertorio de oficios.

**El título de mejor trabajador de Francia (MOF)**

El diploma profesional "uno de los mejores trabajadores de Francia" es un diploma estatal que atestigua la adquisición de una alta cualificación en el ejercicio de una actividad profesional en el ámbito artesanal, comercial, industrial o agrícola.

El diploma se clasifica en el nivel III de la nomenclatura interdepartamental de los niveles de formación. Se emite después de un examen llamado "uno de los mejores trabajadores de Francia" bajo una profesión llamada "clase" adscrita a un grupo de oficios.

Para más información, se recomienda consultar la web oficial del concurso "uno de los mejores trabajadores de Francia".

*Para ir más allá* Artículo D. 338-9 del Código de Educación.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Siga el curso de preparación de la instalación (SPI)

El curso de preparación de la instalación (SPI) es un requisito previo obligatorio para cualquier persona que solicite el registro en el directorio de operaciones.

**Condiciones de la pasantía**

El registro se realiza previa presentación de una pieza de identificación con la CMA territorialmente competente. La pasantía tiene una duración mínima de 30 horas y se realiza en forma de cursos y trabajo práctico. Su objetivo es adquirir los conocimientos esenciales en los ámbitos jurídico, fiscal, social y contable necesario para crear un negocio artesanal.

**Excepcional aplazamiento del inicio de la pasantía**

En caso de fuerza mayor, el interesado podrá llevar a cabo el SPI en el plazo de un año a partir del registro de su empresa en el directorio de operaciones. Esto no es una exención, sino simplemente un aplazamiento de la pasantía, que sigue siendo obligatoria.

**El resultado de la pasantía**

El participante recibirá un certificado de práctica de seguimiento que deberá adjuntar a su expediente de declaración de negocios.

**Costo**

La pasantía vale la pena. Como indicación, la formación costó unos 260 euros en 2017.

**Caso de exención de pasantías**

El interesado podrá ser eximido de completar la pasantía en dos situaciones:

- si ya ha recibido un título o diploma aprobado en el nivel III, incluyendo un título en economía y gestión empresarial, o un máster de un CMA;
- si ha estado en una actividad profesional durante al menos tres años requiriendo un nivel de conocimiento equivalente al proporcionado por la pasantía.

**Exención de pasantías para nacionales de la UE o del EEE**

En principio, un profesional cualificado nacional de la UE o del EEE está exento del SPI si justifica con la CMA una cualificación en gestión empresarial que le otorgue un nivel de conocimiento equivalente al previsto por las prácticas.

La cualificación en la gestión empresarial se reconoce como equivalente a la proporcionada por las prácticas para las personas que son:

- han ejercido una actividad profesional que requiere un nivel de conocimientos equivalente al proporcionado por las prácticas durante al menos tres años;
- conocimientos adquiridos en un Estado de la UE o del EEE o en un tercer país durante una experiencia profesional que cubriría, total o parcialmente, la diferencia sustancial de contenido.

Para aquellos que no cumplan estas condiciones, la Sala Consular podrá exigirles que se sometan a una medida de compensación si el examen de sus cualificaciones profesionales muestra diferencias sustanciales con en Francia para dirigir una empresa de artesanías.

**Condiciones de la exención de prácticas**

Para estar exento del SPI, el interesado (francés o UE o nacional del EEE) debe solicitar al Presidente de la ACM de que se trate una exención de prácticas.

Debe acompañar su correo con los siguientes documentos justificativos:

- Copia del diploma aprobado por el Nivel III;
- Copia del máster;
- prueba de una actividad profesional que requiera un nivel equivalente de conocimiento;
- pagando tasas variables.

No responder dentro de un mes de recibir la solicitud vale la pena aceptar la solicitud para una exención de pasantía.

*Para ir más allá* Artículo 2 de la Ley 82-1091, de 23 de diciembre de 1982, artículo 6-1 del Decreto 83-517, de 24 de junio de 1983.

### b. Solicitar una predeclaración de actividad para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

**Autoridad competente**

La CMA del lugar en el que el nacional desea llevar a cabo la prestación es competente para emitir la declaración previa de actividad.

**Documentos de apoyo**

La solicitud de un informe previo de la actividad va acompañada de un archivo que contiene los siguientes documentos justificativos:

- Una fotocopia de un documento de identidad válido
- un certificado que justifique que el nacional está legalmente establecido en un Estado de la UE o del EEE;
- un documento que justifique la cualificación profesional del nacional que puede ser, a su elección:- Una copia de un diploma, título o certificado,
  - un certificado de competencia.

**Qué saber**

Si es necesario, las piezas deben ser traducidas al francés por un traductor certificado.

**Tenga en cuenta que**

Cuando el expediente está incompleto, la CMA tiene un plazo de quince días para informar al nacional y solicitar todos los documentos que faltan.

**Resultado del procedimiento**

Al recibir todos los documentos en el archivo, el CMA tiene un mes para decidir:

- autorizar la prestación cuando el nacional justifique tres años de experiencia laboral en un Estado de la UE o del EEE, y adjuntar a dicha Decisión un certificado de cualificación profesional;
- o autorizar la disposición cuando las cualificaciones profesionales del nacional se consideren suficientes;
- o imponerle una prueba de aptitud cuando existan diferencias sustanciales entre las cualificaciones profesionales del nacional y las exigidas en Francia. En caso de negativa a realizar esta medida de compensación o si no cumple, el nacional no podrá prestar el servicio en Francia.

El silencio guardado por la autoridad competente en estos tiempos merece autorización para iniciar la prestación del servicio.

*Para ir más allá* Artículo 2 del Decreto de 2 de abril de 1998; Artículo 2 de la 17 de octubre de 2017 respecto a la presentación de la declaración y las solicitudes previstas en el Decreto 98-246, de 2 de abril de 1998, y el título I del Decreto 98-247, de 2 de abril de 1998.

### c. Solicitar un certificado de reconocimiento de la cualificación profesional para el nacional de la UE o del EEE en caso de ejercicio permanente (LE)

El interesado que desee obtener un diploma reconocido distinto del exigido en Francia o su experiencia profesional podrá solicitar un certificado de reconocimiento de la cualificación profesional.

**Autoridad competente**

La solicitud debe dirigirse a la CMA correspondiente del lugar en el que la persona desee resolver.

**Procedimiento**

Se envía un recibo de solicitud al solicitante en el plazo de un mes a partir de la recepción de la CMA. Si el expediente está incompleto, la CMA pide al interesado que lo complete dentro de una quincena de la presentación del expediente. Se emite un recibo tan pronto como se completa.

**Documentos de apoyo**

La solicitud de certificación de cualificación profesional es un expediente que contiene los documentos
Apoyo:

- Una solicitud de certificado de cualificación profesional
- una prueba de cualificación profesional en forma de certificado de competencia o un diploma o un certificado de formación profesional;
- Una fotocopia del documento de identidad válido del solicitante
- Si se ha adquirido experiencia laboral en el territorio de un Estado de la UE o del EEE, un certificado sobre la naturaleza y la duración de la actividad expedida por la autoridad competente en el Estado miembro de origen;
- si la experiencia profesional ha sido adquirida en Francia, las pruebas del ejercicio de la actividad durante tres años.

**Qué saber**

Si es necesario, todos los documentos justificativos deben ser traducidos al francés por un traductor certificado.

La CMA podrá solicitar más información sobre su formación o experiencia profesional para determinar la posible existencia de diferencias sustanciales con la cualificación profesional requerida en Francia. Además, si la CMA se acerca al Centro Internacional de Estudios Educativos (CIEP) para obtener información adicional sobre el nivel de formación de un diploma o certificado o una designación extranjera, el solicitante tendrá que pagar una tasa Adicional.

**hora**

Dentro de los tres meses siguientes a la recepción, la CMA podrá decidir:

- Reconocer y expedir el certificado de cualificación profesional;
- someter al nacional a una medida de compensación y le notifica esta decisión;
- negarse a expedir el certificado de cualificación profesional.

**Qué saber**

En ausencia de una decisión en el plazo de cuatro meses, se considerará adquirida la solicitud de certificado de cualificación profesional.

**Remedios**

Si la CMA rechaza la solicitud de cualificación profesional de la CMA, el solicitante puede impugnar la decisión. Por lo tanto, en el plazo de dos meses a partir de la notificación de la denegación de la CMA, puede formar:

- una apelación agraciada al prefecto del departamento de CMA pertinente;
- una impugnación legal ante el tribunal administrativo pertinente.

**Costo**

Gratis.

**Bueno saber: medidas de compensación**

La CMA notifica al solicitante su decisión de que realice una de las medidas de compensación. Esta Decisión enumera los temas no cubiertos por la cualificación atestiguada por el solicitante y cuyos conocimientos son imprescindibles para ejercer en Francia.

A continuación, el solicitante debe elegir entre un curso de ajuste de hasta tres años y una prueba de aptitud.

La prueba de aptitud toma la forma de un examen ante un jurado. Se organiza en un plazo de seis meses a partir de la recepción de la decisión del solicitante de optar por el evento. En caso contrario, se considerará que la cualificación ha sido adquirida y la CMA establece un certificado de cualificación profesional.

Al final del curso de ajuste, el solicitante envía al CMA un certificado que certifica que ha completado válidamente esta pasantía, acompañado de una evaluación de la organización que lo supervisó. Sobre esta base, la CMA emite un certificado de cualificación profesional en el plazo de un mes.

La decisión de utilizar una medida de indemnización podrá ser impugnada por el interesado, que deberá presentar un recurso administrativo ante el prefecto en el plazo de dos meses a partir de la notificación de la decisión. Si su apelación es desestimada, puede iniciar una impugnación legal.

**Costo**

Se puede cobrar una tarifa fija que cubra la investigación del caso.

Para obtener más información, es aconsejable acercarse a la CMA correspondiente.

*Para ir más allá* Artículos 3 a 3-2 del Decreto 98-246, de 2 de abril de 1998, de 28 de octubre de 2009 adoptados en los decretos 97-558 de 29 de mayo de 1997 y 98-246, de 2 de abril de 1998, relativos al procedimiento de reconocimiento de las cualificaciones profesionales de un Estado miembro de la Comunidad Europea u otro Estado parte en el acuerdo del Espacio Económico Europeo.

### d. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar los "Formalidades de Reporte de Empresas Artesanales" para obtener más información.