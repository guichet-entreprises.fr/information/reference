﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS084" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Otros servicios" -->
<!-- var(title)="Lavandería de autoservicio" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="otros-servicios" -->
<!-- var(title-short)="lavanderia-de-autoservicio" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/otros-servicios/lavanderia-de-autoservicio.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="lavanderia-de-autoservicio" -->
<!-- var(translation)="Auto" -->

Lavandería de autoservicio
==========================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

El operador de una lavandería automática de autoservicio proporciona al público lavadoras, hilanderas centrífugas y secadoras sin necesidad de estar presente en las instalaciones.

Paralelamente a esta actividad principal, el operador también puede ofrecer servicios de planchado o retoque.

### b. Centro competente de formalidad empresarial

El Centro de Formalidades Comerciales (CFE) correspondiente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Cualificaciones profesionales
-----------------------------------------

### a. Cualificaciones profesionales

No se requiere un diploma específico para el funcionamiento de una lavandería automática de autoservicio. Sin embargo, se aconseja al individuo tener conocimiento de contabilidad y gestión.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) no está sujeto a ningún requisito de cualificación o certificación, al igual que los franceses.

### c. Algunas peculiaridades de la regulación de la actividad

#### Cumplir con las normas de seguridad de la lavadora

El operador de una lavandería automática de autoservicio debe cumplir con las normas de seguridad de sus máquinas, incluyendo:

- Revisar sus dispositivos de seguridad al menos una vez a la semana
- registrar sus observaciones en un registro especial que se pondrá a disposición de los clientes;
- Mostrar un documento visible e inalterable con el número de persona a la que se llega en caso de un informe de mal funcionamiento de la máquina;
- sosteniendo:
  - Un declaración de cumplimiento,
  - Un Declaración de cumplimiento CE,
  - una declaración del fabricante de que la máquina tiene las características de seguridad en caso de un fallo o mal funcionamiento.

El operador que no cumpla con esta normativa será castigado con una multa de 1.500 euros, incrementada a 3.000 euros en caso de reincidencia.

*Para ir más allá* Decreto No 2012-412 de 23 de marzo de 2012 sobre la seguridad de las lavadoras y hilanderos puestos a disposición del público.

#### Visualización de precios

El operador de una lavandería automática deberá informar al público de los precios cobrados mediante la vía de marcado, etiquetado, exhibición o cualquier otro proceso apropiado.

*Para ir más allá* Artículo L. 112-1 del Código del Consumidor.

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, es aconsejable consultar el listado[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

#### Videovigilancia

Las lavanderías automáticas de autoservicio pueden equiparse con sistemas de videovigilancia de los clientes. Por lo tanto, el operador deberá indicar su presencia para no infringir su privacidad.

El operador también tendrá que declarar su instalación con la prefectura del establecimiento y completar el formulario Cerfa 13806*03, acompañado de cualquier documento que detalle el sistema utilizado.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El contratista debe registrarse en el Registro Mercantil y Corporativo (SCN). Es aconsejable consultar los "Formalidades de Reporte de Empresas Artesanales" para obtener más información.

### b. Si es necesario, proceda con las formalidades relacionadas con las instalaciones clasificadas para la protección del medio ambiente (ICPE)

El funcionamiento de una lavandería automática puede ser peligroso o contaminante que puede estar comprendido en la normativa sobre instalaciones clasificadas.

Lavandería lavanderías entran en el[2340](https://aida.ineris.fr/consultation_document/10599) y especificar el[Formalidades](https://www.service-public.fr/professionnels-entreprises/vosdroits/F33414) el operador debe realizar:

- Un registro para máquinas con una capacidad de lavado de más de 5 toneladas por día;
- una declaración para máquinas con una capacidad de lavado de menos de 5 toneladas pero más de 500 kilos por día.

Cuando el operador reanude un ICPE, no estará sujeto a procedimientos de registro y presentación de informes. Por otro lado, tendrá que informar al prefecto, en el plazo de un mes a partir de la toma de control de la operación.

**Tenga en cuenta que**

En caso de declaración, el procedimiento es gratuito. Por otra parte, en caso de registro y solicitud de autorización, cabe esperar el coste de los procedimientos de investigación y/o publicación del público.

