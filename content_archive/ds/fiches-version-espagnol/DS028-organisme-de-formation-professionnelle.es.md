﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS028" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseñanza" -->
<!-- var(title)="Organización de formación profesional" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="ensenanza" -->
<!-- var(title-short)="organizacion-de-formacion-profesional" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/ensenanza/organizacion-de-formacion-profesional.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="organizacion-de-formacion-profesional" -->
<!-- var(translation)="Auto" -->


Organización de formación profesional
=====================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

Una organización de formación profesional (o proveedor de formación) es una persona física o jurídica que ofrece educación continua a:

- integración, reintegración en el trabajo o mantenimiento del empleo;
- desarrollar las habilidades profesionales de aquellos que han completado la formación.

La capacitación también puede permitir que las personas que han dejado voluntariamente su trabajo (para ayudar a un pariente dependiente o para cuidar de sus hijos) regresen al trabajo.

El acceso de los empleados a las actividades de formación puede llevarse a cabo:

- Por iniciativa del empleador como parte del plan de capacitación;
- por iniciativa del empleado en el contexto de una licencia de ausencia (comprobación de habilidades, licencia de formación individual, enseñanza e investigación, etc.);
- en el contexto de un acuerdo de formación profesional (celebrado por una entidad jurídica privada o pública con la organización);
- contrato de profesionalización (celebrado entre la organización y un pasante individual).

**Tenga en cuenta que**

La lista de acciones de educación continua figura en el artículo L. 6313-1 del Código de Trabajo.

*Para ir más allá* Artículo L. 6311-1 del Código de Trabajo.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- En el caso de la creación de una empresa individual, la CFE competente es la Urssaf;
- Para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI);
- en el caso de la creación de una sociedad civil, es el registro del tribunal comercial, o el registro del tribunal de distrito en los departamentos del Bajo Rin, el Alto Rin y el Mosela.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa).

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

Para operar una organización de formación profesional, no se requieren cualificaciones profesionales.

No obstante, el profesional deberá asegurarse de que los miembros de su personal justifiquen las cualificaciones y cualidades necesarias para llevar a cabo la actividad docente y de coaching dentro de cada formación específica.

Tan pronto como se celebre el primer acuerdo de formación profesional y a más tardar tres meses, la persona debe informar de su actividad al prefecto regional en el lugar de su ejercicio principal (véase infra "3o). a. Declaración de actividad").

*Para ir más allá* Artículo L. 6351-1 del Código de Trabajo.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios (LPS) o establecimiento libre (LE))

Un nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el Acuerdo del Espacio Económico Europeo (EEE) no está sujeto a ningún requisito de cualificación profesional del mismo modo que el nacional francés.

Sin embargo, cuando el organismo que opera en Francia tiene su sede en un Estado miembro de la UE o del EEE, su operador debe designar un representante domiciliado en Francia. En caso necesario, el titular deberá hacer la declaración de actividad con el prefecto de la región en la que esté domiciliado dicho representante.

**Tenga en cuenta que**

Esta obligación de notificación no es aplicable a los organismos cuya sede social se encuentre en un Estado miembro pero que operen en Francia de forma temporal o ocasional.

*Para ir más allá* Artículo R. 6351-3 del Código de Trabajo.

### c. Condiciones de honor y sanciones

El profesional que desee operar una organización de formación profesional no debe haber sido objeto de una condena penal por violaciones de la probidad, el honor o la buena moral.

*Para ir más allá* Artículo L. 6352-2 del Código de Trabajo.

**Sanciones financieras**

Cuando la organización no proporciona capacitación (total o parcialmente) está obligado a pagar todas las sumas pagadas por el contratista.

*Para ir más allá* Artículo L.6354-3 del Código de Trabajo.

**Sanciones penales**

El operador de una organización de formación profesional está sujeto a sanciones penales por incumplimiento de las disposiciones relativas al ejercicio de su actividad. En particular, se enfrenta a una multa de 4.500 euros si:

- no ha hecho una declaración previa de su actividad o en un desconocimiento de las disposiciones obligatorias (véase infra "3o. a. Declaración de actividad");
- no puede justificar las cualificaciones y cualidades de los formadores dentro de la institución;
- no cumple con las condiciones de honor;
- no establece un reglamento interno dentro de su institución;
- no nombra un auditor a pesar de que supera los umbrales;
- no abordar el balance pedagógico y financiero.

*Para ir más allá* Artículos L. 6355-1 y los siguientes artículos del Código de Trabajo.

### d. Algunas peculiaridades de la regulación de la actividad

**Reglamento interno de los pasantes**

Toda organización de formación profesional deberá establecer un reglamento interno en un plazo de tres meses a partir del inicio de la actividad.

Este reglamento interno debe determinar:

- Medidas de seguridad y salud aplicables al establecimiento;
- las normas sobre disciplina y posibles sanciones;
- los términos del personal de formación para actividades de formación de más de quinientas horas.

**Tenga en cuenta que**

Cuando la organización tiene varias instituciones, este reglamento interno se puede adaptar a cada una.

*Para ir más allá* Artículo L. 6352-3 del Código de Trabajo.

**Obligaciones contables**

Dependiendo de la naturaleza de su actividad, el operador debe elaborar un informe contable cada año.

Por ejemplo, las organizaciones de formación pública deben tener en cuenta por separado su actividad continua de formación profesional. Por otro lado, las organizaciones de formación en derecho privado deben:

- Hacer un informe anual sobre el resultado, la cuenta de resultados y un apéndice;
- designar un auditor y un sustituto si, al final del ejercicio, superan los siguientes umbrales:- tres empleados,
  - 153.000 euros en ventas o recursos,
  - 230.000 euros del balance total.

Además, el operador de la organización de formación profesional debe presentar un informe educativo y financiero antes del 30 de abril de cada año al prefecto regional (véase infra "3o. c. Autorización posterior al registro).

*Para ir más allá* Artículo L. 6352-6 del Código de Trabajo.

**Cumplimiento de las normas de seguridad y accesibilidad**

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

Es aconsejable consultar la hoja "Establecimiento que recibe al público" para obtener más información.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

**Acuerdo de formación entre la organización de formación y el comprador**

Todas las actividades de formación se llevan a cabo de acuerdo con un programa preestablecido. En cada formación, la organización debe celebrar un contrato con su cliente de formación indicando:

- El título, la naturaleza, la duración y los procedimientos de la formación;
- y, en caso afirmativo, las contribuciones de las personas públicas.

*Para ir más allá* Artículos L. 6353-2 y R. 6353-2 del Código de Trabajo.

**Contrato de formación entre una persona física y la organización de formación**

Cuando la organización está considerando capacitar a un individuo individual,0105 persona individual empleado individual, debe celebrar un contrato con la persona antes de que se finalice y se pague la cuota de capacitación.

Este contrato debe especificar, a pena de nulidad:

- La naturaleza, duración, programa y finalidad de la formación y su personal;
- El nivel de cualificaciones requerido para acceder a esta formación;
- las condiciones para la expedición de la formación, los métodos de control de conocimientos y, en su caso, la naturaleza de la sanción;
- Las cualificaciones de la persona que proporciona la formación;
- tasas de capacitación y condiciones de liquidación.

**Tenga en cuenta que**

El pasante podrá retirarse durante 10 días a partir de la firma del contrato. Si este es el caso, el pasante debe presentar una solicitud por carta recomendada con notificación de recibo y no se pueden reclamar cargos.

*Para ir más allá* Artículos L. 6353-3 y los siguientes artículos del Código de Trabajo.

#### Información de los pasantes

La organización debe proporcionar al aprendiz de capacitación toda la información relacionada con:

- El plan de estudios, objetivos y modalidades para evaluar la formación;
- La lista de instructores
- Información de contacto para las relaciones con el aprendiz
- la normativa interna aplicable a la formación.

*Para ir más allá* Artículos L. 6353-8 y los siguientes artículos del Código de Trabajo.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Declaración de actividad

**Autoridad competente**

El operador de la organización de formación profesional debe enviar su declaración al prefecto de la región en la que se encuentra su principal institución.

**Documentos de apoyo**

La declaración deberá ir acompañada de los siguientes documentos:

- Una fotocopia de la prueba del número de sirena
- Boletín 3 de antecedentes penales del operador de menos de un mes de edad;
- Una copia del primer acuerdo de formación profesional o del primer contrato celebrado cuando, en su defecto, se demuestre que se prevé un primer logro;
- Si es así, una prueba de registro en el[lista pública de agencias responsables de llevar a cabo evaluaciones de competencias](https://www.data.gouv.fr/fr/datasets/liste-publique-des-organismes-de-formation-l-6351-7-1-du-code-du-travail/) ;
- una copia del programa de formación y listas de partes interesadas mencionando sus títulos, cualificaciones y la entrega y relación contractual que los vinculan con la organización de formación.

**Procedimiento**

El prefecto regional reconoce la recepción de la solicitud dentro de los treinta días siguientes a su presentación y envía un recibo al solicitante. A continuación, registra la actividad del operador.

**Tenga en cuenta que**

El hecho de que el prefecto no responda más allá de los treinta días es un caso en el que se rechaza la solicitud. Además, podrá hacerse una declaración rectificativa en un plazo de 30 días al prefecto regional para modificar la declaración de actividad.

*Para ir más allá* Artículos R. 6351-1 a R. 6351-7 del Código de Trabajo.

### b. Formalidades de notificación de la empresa

Dependiendo de la naturaleza de su actividad, el profesional debe o debe registrarse en el Registro de Comercio y Sociedades (RSC) o registrarse en el Urssaf.

Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### c. Autorización posterior al registro

#### Enviar una evaluación educativa y financiera

Antes del 30 de abril de cada año, el proveedor de capacitación debe enviar esta evaluación al prefecto regional.

Este balance debe ir acompañado del balance del ejercicio y mencionar:

- Actividades de formación llevadas a cabo durante el ejercicio;
- El número de aprendices aceptados;
- El número de horas y horas de formación en prácticas, así como su volumen por hora, la naturaleza y el nivel de formación;
- La asignación de fondos recibidos y el importe de las facturas emitidas;
- Datos contables sobre la formación profesional continua;
- todos los ingresos procedentes de la inversión de los fondos recibidos.

**Tenga en cuenta que**

El prefecto regional también puede requerir que se realice una lista de los servicios de capacitación.

*Para ir más allá* Artículos L. 6352-11 y R. 6352-22 y siguientes del Código de Trabajo.

