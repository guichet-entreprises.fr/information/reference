﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS012" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Emprendedor de entretenimiento" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="emprendedor-de-entretenimiento" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/emprendedor-de-entretenimiento.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="emprendedor-de-entretenimiento" -->
<!-- var(translation)="Auto" -->


Emprendedor de entretenimiento
==============================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La actividad del emprendedor de los llamados espectáculos en vivo (muestra la presencia física de al menos un artista frente a un público) consiste en:

- lugares de explotación utilizados para las representaciones públicas (categoría 1);
- producir espectáculos o recorridos, y ser responsable de la etapa artística (categoría 2);
- programas o giras, sin ser responsable del conjunto de arte (categoría 3).

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para una profesión artesanal (en el caso de un espectáculo de marionetas), la CFE competente es la Cámara de Comercio y Artesanía (CMA);
- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

El artículo L. 110-1 del Código de Comercio establece que se presume que la actividad de un empresario de entretenimiento en vivo es un acto de comercio. Sin embargo, la actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa en el estado no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Para emprendedores con sede en Francia

Tener un recibo de declaración por valor de una licencia para llevar a cabo la actividad de empresario de espectáculos (véase infra "3. b. Solicitar una licencia de contratista de entretenimiento en vivo"), la persona (un empleador individual en particular, o que tiene una empresa en su nombre) debe:

- justifican la competencia profesional o la experiencia:- ya sea por poseer un título de educación superior o un grado del mismo nivel registrado en el[directorio nacional de certificaciones profesionales](https://certificationprofessionnelle.fr/),
  - justificando una experiencia laboral de al menos seis meses,
  - ya sea por haber sido sometido a una formación profesional de al menos 125 horas en el campo del entretenimiento o justificando un conjunto de habilidades, incluido en un directorio establecido por la comisión conjunta de formación en empleo de la serie en vivo a más tardar el 1er octubre de 2020;
- gozan de la capacidad jurídica para llevar a cabo una actividad comercial y estar inscritos en el registro mercantil y corporativo o en el directorio de comercios cuando estén sujetos a esta obligación;
- establecer que se cumplen las obligaciones de seguridad de los lugares (para los operadores de centros de espectáculos - categoría 3) y han recibido formación en seguridad para espectáculos sobre el repertorio disponible en el[comisión conjunta sitio web de formación en empleo en vivo espectáculo en vivo](https://www.cpnefsv.org/formations-agreees/formation-securite-licence-dentrepreneurs-exploitants-lieux) ;
- Estar al día con respecto a las obligaciones del empleador en virtud del Código del Trabajo, el sistema de seguridad social o las disposiciones relativas a la protección de la propiedad intelectual y artística;
- habiendo hecho su declaración a través del teleservicio ad hoc del Ministerio de Cultura y cumplido con los plazos establecidos en el Código del Trabajo con respecto a la declaración.

En el caso de las personas jurídicas, una o más personas a cargo de la organización deben cumplir las condiciones de competencia o experiencia.

*Para ir más lejos:* Artículos L. 7122-4 y L. 7122-7 y R. 7122-2 y R. 7122-3 del Código de Trabajo.

### b. Para los nacionales de la UE o del EEE (prestación gratuita de servicios o establecimiento libre)

#### Para un ejercicio temporal e informal (LPS)

Cualquier nacional de un Estado miembro de la Unión Europea (UE) o un Estado parte en el acuerdo del Espacio Económico Europeo (EEE) legalmente establecido podrá desempeñar las mismas prestaciones temporales y ocasionales que un contratista de rendimiento. actividad en Francia.

Como tal, el nacional debe, antes de su primera actuación, informar al prefecto regional del lugar donde desea hacerlo (véase infra "3o. d. Si es necesario, hacer una declaración por escrito para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)). Esta declaración debe abordarse a través de la[Teleservicio del Ministerio de Cultura](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_declaration_00/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7).

*Para ir más lejos:* Artículo L. 7122-6 del Código de Trabajo.

#### Para un ejercicio permanente (LE)

Cualquier nacional de un Estado de la UE o del EEE podrá establecerse en Francia para llevar a cabo la actividad de empresario de espectáculos en directo, sin declarar su actividad, sujeto a la producción de un título de efecto equivalente expedido en uno de estos Estados en condiciones comparables (véase infra "3.00). c. Si es necesario, solicitar el reconocimiento del efecto equivalente del título a la UE o al nacional del EEE para un ejercicio permanente (LE) ").

*Para ir más lejos:* Artículo L. 7122-5 del Código de Trabajo.

### c. Algunas peculiaridades de la regulación de la actividad

#### Cumplimiento de las normas de seguridad y accesibilidad

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida.

*Para ir más lejos:* Orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales del Reglamento de Seguridad contra Incendios y Pánico en las instituciones públicas (ERP).

Es aconsejable referirse a la lista[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

#### particularidades

El contratista de rendimiento debe indicar en todos los medios que promuevan su espectáculo, así como en la taquilla de cualquier espectáculo el número de recibo de declaración que vale la pena licencia, válido para el director o empresarios de la feria personas vivas que producen o transmiten el programa

*Para ir más lejos:* Artículo R. 7122-12 del Código de Trabajo.

#### Creación de una taquilla

En cada entrada del espectáculo, el contratista debe emitir una entrada a cada uno de los espectadores. Para ello, deberá configurar una venta manual o informatizada de billetes y hacer la declaración previa a la dirección de los servicios fiscales de los que depende.

Cuando la taquilla esté informatizada, tendrá que transmitir la siguiente información:

- El nombre del software, su número de versión y, en su caso, su fecha, así como la identidad de su diseñador o el nombre del paquete de software;
- Configuración del ordenador
- El sistema operativo
- Lenguaje de programación
- El formato del software de origen o ejecutable proporcionado por el diseñador;
- Descripción funcional del sistema
- Facsímil una nota, un cupón de gestión y un estado de cuenta de recibo;
- Seguridad.

*Para ir más lejos:* Artículos 50 sexies I y 290 trimestre del Código General Tributario.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario tendrá que inscribirse en el Registro mercantil (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Informar de una Empresa Comercial" o "Formalidades de Reporte de Empresas Artesanales" para obtener más información.

### b. Declarar una actividad de emprendedor de entretenimiento en vivo

El contratista de entretenimiento deberá declarar al menos un mes antes de la primera actuación su actividad para obtener un recibo válido que le permita llevar a cabo su actividad en una de las tres categorías mencionadas anteriormente.

#### Autoridad competente

El profesional tendrá que solicitar al prefecto regional del lugar donde se encuentra su establecimiento principal a través del[teleservicio en línea en el sitio web del Ministerio de Cultura](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_declaration_00/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7).

#### Documentos de apoyo

La solicitud es hecha por el[enviar una carpeta en línea](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_declaration_00/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7) incluyendo toda la siguiente información y documentos justificativos:

- Precisión en las categorías de licencias solicitadas
- Identificación de la persona contratante o persona jurídica (denominación, forma jurídica, dirección);
- cualquier documento que justifique la capacidad profesional del solicitante o, en el caso de las sociedades, la presencia de una persona que justifique dicha capacidad, que podrá ser:- copiando el diploma de educación superior o un título del mismo nivel registrado en el directorio nacional de certificaciones profesionales,
  - prueba de experiencia laboral de al menos seis meses (contratos de trabajo u otros documentos oficiales),
  - un certificado de seguimiento de 125 horas de formación profesional en el campo del entretenimiento;
- documentos relacionados con la identificación de la persona o corporación y la capacidad de dirigir una actividad comercial o empresarial:- El número de identificación de la empresa,
  - el código de la actividad principal realizada o prevista y la finalidad de la persona jurídica contenida en sus estatutos,
  - el número de registro en el registro de comercios y empresas, el directorio de operaciones (cuando está sujeto al requisito de registro en uno de estos registros) o el directorio nacional de asociaciones en el caso de las asociaciones,
  - un certificado de honor (caja de verificación) que certifique la ausencia de una condena o sanción que prohíba el ejercicio de una actividad comercial;
- La referencia al convenio colectivo aplicable;
- Un cronograma de programación planificada y la descripción del proyecto de actuaciones en vivo;
- un compromiso sobre el honor (caja de verificación) de unirse al escritorio único para el espectáculo en vivo (GUSO) o a las organizaciones de protección social de la feria, así como a las instituciones a las que la membresía se hace obligatoria por los convenios colectivos nacionales de ejecución en directo o por convenio colectivo de trabajo;
- para aquellos que, antes de la declaración, ya han participado en actuaciones en vivo:- un certificado en el honor (caja de verificación) de cuentas actualizadas de contribuciones y contribuciones sociales (GUSO u organizaciones de protección social de la feria; FNAS, CASC) o un protocolo para escalonar y comprometerse con él,
  - un certificado de honor (caja de verificación) que certifique que la empresa no tiene deudas con respecto al pago de derechos de autor, o un protocolo para las deudas asombrosas y el compromiso de cumplirlo,
  - Un calendario de espectáculos de los tres años pasados según el modelo a descargar;
- para los contratistas de la categoría 3, la justificación de haber recibido formación adaptada a la naturaleza de los lugares (para una persona jurídica que justifique la presencia en ella de una o más personas que cumplan esta condición);
- para los establecimientos sujetos a la obligación de la comisión de vigilar los riesgos de incendio y pánico en los establecimientos que reciben el público:- El informe de visita válido emitido de conformidad con la normativa vigente por dicha comisión y con un dictamen favorable,
  - o, cuando la declaración se presente en una fecha anterior a la aprobación de la comisión, el compromiso sobre el honor de no explotar el lugar en ausencia de notificación favorable de dicha comisión y la fecha de aprobación de la comisión,
  - o, para establecimientos como carpas, tiendas de campaña y estructuras itinerantes, el certificado de cumplimiento mencionado en el artículo CTS 3 del auto de 25 de junio de 1980 supra;
- para establecimientos de 5a categoría, un certificado sobre el honor (caja de verificación) de la clasificación en la 5a categoría;
- un certificado de honor (caja de verificación) según el cual, cuando el lugar alberga actuaciones en vivo que transmiten música amplificada, está equipado de acuerdo con las normas de seguridad de la salud con respecto a los riesgos de ruido.

#### Procedimiento

La autoridad competente emite el recibo de la declaración que, a menos que la administración se oponga, valdrá una licencia durante cinco años. Al final de estos cinco años, el contratista tendrá que solicitar la renovación a través del teleservicio.

**Qué saber**

En caso de silencio del prefecto de la región en el plazo de un mes, tras la recepción del fichero, se considerará que el recibo vale la pena una licencia.

*Para ir más lejos:* Artículos R. 7122-2 a 7122-5 del Código de Trabajo; Artículos 1 y 2 del auto de 27 de septiembre de 2019 adoptados en virtud del Código de Trabajo (parte reglamentaria) por los que se establece la lista de documentos e información necesarios para el ejercicio de la actividad de empresario de espectáculos en directo.

### c. En caso necesario, solicitar el reconocimiento del efecto equivalente de su título para la UE o el nacional del EEE para su establecimiento en Francia (LE)

#### Autoridad competente

El prefecto regional es competente para decidir sobre la solicitud de reconocimiento.

#### Documentos de apoyo

La solicitud se presenta mediante la presentación de un expediente en el[Teleservicio del Ministerio de Cultura](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_reconnaissance_00/?__CSRFTOKEN__=ad5455d4-c481-4c7e-96e1-cd93b63df8f4) incluyendo los siguientes documentos justificativos:

- El formulario completado, fechado y firmado;
- Estado civil del contratista o, en su caso, información de la empresa si el solicitante es una persona jurídica;
- el tipo o tipos de actividades empresariales previstas (categoría 1, 2 o 3);
- Una declaración descriptiva de las condiciones para la expedición del título de efecto equivalente producido y mencionado en el artículo L. 7122-3 y documentos justificativos;
- copia del título para el que se solicita el reconocimiento.

#### Procedimiento

El nacional envía su solicitud en Internet. Cuando el prefecto de la región reconoce el título de efecto equivalente, emite un recibo de devolución para la categoría de título en el plazo de un mes a partir de la solicitud del título.

De lo contrario, el prefecto de la región informa por cualquier medio de su negativa a reconocer la equivalencia del título mediante una decisión motivada dentro del mismo plazo.

El silencio guardado por la administración durante un mes a partir de la presentación del título merece el reconocimiento de equivalencia.

*Para ir más lejos:* Artículo R. 7122-7 del Código de Trabajo; Artículo 5 del auto de 27 de septiembre de 2019 adoptado en virtud del Código de Trabajo (parte reglamentaria) por el que se establece la lista de documentos e información necesaria para el ejercicio de la actividad de empresario de espectáculos en directo.

### d. Si es necesario, proporcione información anticipada para el nacional de la UE o del EEE para un ejercicio temporal e informal (LPS)

#### Autoridad competente

El prefecto regional es responsable de decidir sobre la declaración escrita del nacional.

#### Documentos de apoyo

La solicitud se realiza mediante la presentación en línea de la[Teleservicio del Ministerio de Cultura](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_information_01/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7) un archivo con los siguientes documentos justificativos:

- Estado civil del contratista o, en su caso, información de la empresa si el solicitante es una persona jurídica;
- La finalidad social de la organización y, en su caso, las referencias de su registro a un registro profesional;
- La naturaleza de las interpretaciones o ejecuciones, el número, la duración y las fechas previstas en las interpretaciones o ejecuciones;
- el nombre o nombre, la dirección del operador del o lugares de representación previstos y, en su caso, su número de licencia y la dirección del local, si es diferente de la del operador;
- el número de empleados contratados y el número de empleados desplazados por la distinción del personal artístico, técnico y administrativo y de los artistas declarados autónomos;
- El número máximo de asientos en el recinto o salas donde tendrá lugar la actuación;
- número de IVA intracomunitario de la organización.

#### Procedimiento

El nacional envía la información antes del prefecto regional a través del teleservicio al menos un mes antes de la fecha programada del espectáculo.

*Para ir más lejos:* Artículo R. 7122-8 del Código de Trabajo; Artículo 6 del auto de 27 de septiembre de 2019 adoptado en virtud del Código de Trabajo (parte reglamentaria) por el que se establece la lista de documentos e información necesaria para el ejercicio de la actividad de empresario de espectáculos en directo.

