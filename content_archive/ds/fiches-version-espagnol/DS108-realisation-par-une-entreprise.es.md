﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS108" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Dirigida por una empresa de excavaciones de arqueología preventiva" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="dirigida-por-una-empresa-de-excavaciones" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/dirigida-por-una-empresa-de-excavaciones-de-arqueologia-preventiva.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="dirigida-por-una-empresa-de-excavaciones-de-arqueologia-preventiva" -->
<!-- var(translation)="Auto" -->




Dirigida por una empresa de excavaciones de arqueología preventiva
==================================================================

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La arqueología preventiva, que es una cuestión de misiones de servicio público de conformidad con el artículo L. 521-1 del Código del Patrimonio, tiene como objetivo garantizar la conservación o salvaguardia mediante el estudio científico de elementos del patrimonio arqueológico cuando estéamenazado por obras públicas o privadas.

Cuando las personas (desarrolladores) tienen la intención de llevar a cabo trabajos y desarrollo en el territorio, el Estado puede estar obligado a prescribir un diagnóstico para determinar si tal trabajo puede afectar o no a elementos del patrimonio. Arqueológico. Si el diagnóstico arqueológico pone de relieve la presencia de un yacimiento arqueológico cuyas características y estado de conservación lo justifiquen, la realización de una excavación para salvaguardar el patrimonio y la información científica contenida en el restos son impuestos por el estado.

Las excavaciones se llevan a cabo bajo la supervisión del promotor que puede optar por utilizar la institución pública de carácter administrativo responsable de realizar los diagnósticos de arqueología preventiva ([Inrap](https://www.inrap.fr/)), ya sea a un servicio arqueológico territorial o a una persona de derecho público o privado que posea una acreditación emitida por el Estado.

### b. CFE competente

2°. Condiciones de instalación
--------------------------------------

### a. Para profesionales con sede en Francia

Una persona de derecho público o privado puede llevar a cabo excavaciones de arqueología preventiva según el estado, siempre que la persona posea una certificación emitida por el Estado de su competencia científica.

Para ello, la persona debe presentar una solicitud de acreditación a los servicios gubernamentales pertinentes. La acreditación permite realizar excavaciones preventivas en todo el país. Puede limitarse a ciertas áreas o períodos de investigación arqueológica. La solicitud de aprobación podrá especificar los períodos o áreas deseadas.

El expediente de solicitud debe incluir las cualificaciones, especialidades y experiencia profesional, en el ámbito de la investigación arqueológica y la conservación del patrimonio, del personal empleado por la organización cuya acreditación se solicita. El expediente también debe establecer la capacidad financiera del solicitante y el cumplimiento de los requisitos sociales, financieros y contables.

Debe señalarse que cuando la persona que tiene previsto llevar a cabo el trabajo es una persona privada, el operador de búsqueda no puede ser controlado, directa o indirectamente, por este último o uno de sus accionistas.

### b. Para ciudadanos europeos (LPS o LE)

Se valoran en las mismas condiciones que para los ciudadanos franceses.

### c. Condición de honorabilidad

La solicitud de certificación debe incluir un certificado de honor de que el solicitante no entra en ninguno de los casos mencionados en el artículo 48 del Decreto No 2016-360 de 25 de marzo de 2016 relativo a la contratación pública.

### d. Garantía financiera

La solicitud de certificación debe incluir todos los documentos para establecer la capacidad financiera del solicitante, incluidas las cuentas certificadas y los fundamentos para presentarlas ante el Tribunal de Lo Mercantil.

### e. Algunas peculiaridades de la regulación de la actividad

La actividad de llevar a cabo operaciones de excavación de arqueología preventiva se rige por consideraciones científicas primero antes de ser económica. Se trata de atender una solicitud (la de los desarrolladores que poseen una orden de búsqueda estatal) y una oferta (incluida la de un operador autorizado) bajo el control científico y técnico del Estado.

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Solicitar la acreditación

La solicitud de acreditación debe dirigirse al Ministro de Cultura, a través de la[teleservicio en línea en el sitio web del Ministerio de Cultura](https://mesdemarches.culture.gouv.fr/loc_re/mcc/requests/ARCHE_PREVE_agrement_01/?__CSRFTOKEN__=c29f1117-9058-4a4d-a539-a342ef0db0f1).

La solicitud de acreditación debe incluir:

- La presentación de la organización y del personal científico justificando la acreditación para los períodos y campos solicitados;
- cualificaciones, especialidades y experiencia profesional, en el campo de la investigación arqueológica y la conservación del patrimonio, del personal empleado por la organización cuya acreditación es necesaria;
- Un certificado que especifique la naturaleza del contrato de trabajo o que justifique la promesa de contratar personal;
- El proyecto científico que la organización pretende desarrollar o implementar mientras dure la acreditación;
- La presentación de los medios técnicos y operativos de que dispone la organización para llevar a cabo búsquedas preventivas;
- todos los documentos para establecer la capacidad financiera de la organización, incluidas las cuentas certificadas y la justificación para presentarlas ante el Tribunal de Comercio;
- El documento de evaluación del riesgo para la salud y la seguridad de los trabajadores en la sección R. 4121-1 del Código del Trabajo;
- La declaración de honor en el artículo 48 del Decreto No 2016-360, de 25 de marzo de 2016, sobre contratación pública;
- cuando una asociación solicite la aprobación:- una copia o copia del Diario Oficial de la República Francesa que contenga la inserción mencionada en el artículo 5 de la Ley de 1 de julio de 1901 relativa al contrato de asociación, o, en los departamentos del Bajo Rin, el Alto Rin y el Mosela, una copia de La decisión del tribunal o del tribunal superior que registra la asociación,
  - En lugar de las cuentas certificadas, el informe moral y el informe financiero aprobado según la última junta general;
- En el caso de una solicitud de renovación de la acreditación, el expediente deberá incluir una evaluación científica de la actividad realizada durante la duración de la acreditación anterior. Esta revisión presenta en períodos o campos los resultados científicos de las operaciones llevadas a cabo por el operador en el contexto de su acreditación, así como las perspectivas científicas que pretende desarrollar.

El Ministro de Cultura y el Ministro de Investigación, previa consulta con el Consejo Nacional de Investigaciones Arqueológicas, votarán en un plazo de seis meses a partir de la recepción del expediente completo. Merece la pena aprobar la ausencia de una decisión expresa al final de este período. La acreditación tiene una validez de cinco años.

Cada año, la persona autorizada deberá presentar a la autoridad competente del Estado una evaluación científica, administrativa, social, técnica y financiera de su actividad en arqueología preventiva. Esta evaluación debe incluir:

- una presentación de las operaciones arqueológicas finalizadas y en curso, acompañada de un informe de los trabajos y estudios a realizar, las fechas de previsión del informe de la transacción y un recuento de los gastos de previsión necesarios. llevarlas a cabo, así como documentos que justifiquen la capacidad financiera de la persona autorizada para completar estas transacciones;
- Cuentas certificadas del año pasado
- Un balance social
- un programa anual para prevenir los riesgos laborales y mejorar las condiciones de trabajo, en el sentido del artículo L. 4612-16 del Código de Trabajo;
- un organigrama y una declaración actualizada de la fuerza de trabajo.

### b. Hacer una declaración previa de actividad para los nacionales de la UE que realicen una actividad única

Los nacionales europeos están sujetos a los mismos requisitos que los nacionales franceses y los legalmente establecidos en Francia. Si ya está previsto un sistema de acreditación similar por su Estado de origen, los solicitantes extranjeros pueden aprovecharlo, con sujeción a la evaluación de la equivalencia de esta aprobación con la prevista por la normativa francesa.

### c. Hacer un informe previo de la actividad para una escuela secundaria

Una escuela secundaria con autonomía legal debe tener una licencia si desea llevar a cabo excavaciones arqueológicas preventivas.

4°. Textos de referencia
---------------------------------------------

- Artículo L. 522-1 y L.522-8 del Código del Patrimonio;
- Artículos R. 522-8 a R. 522-13 del Código de Patrimonio.

