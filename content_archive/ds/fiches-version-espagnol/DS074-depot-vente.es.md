﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS074" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Depósito-venta" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="deposito-venta" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/deposito-venta.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="deposito-venta" -->
<!-- var(translation)="Auto" -->


Depósito-venta
==============

Actualización más reciente: : <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1°. Definición de la actividad
-----------------------------

### a. Definición

La venta de depósitos permite a los individuos (depositantes) comercializar muebles de segunda mano a través de comerciantes (custodios) en tiendas de exposiciones.

El depositante y el custodio aceptarán un precio de venta en el que el custodio cobrará una comisión para pagarse una vez que se venda la propiedad.

### b. Centro competente de formalidad empresarial

El Centro de Formalidades Comerciales (CFE) correspondiente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

2°. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere diploma o certificación para ejercer en un depósito-venta. Sin embargo, se recomienda tener cualidades contables para gestionar el inventario.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o una parte en el Espacio Económico Europeo (EEE) no está sujeto a ningún requisito de calificación o certificación, al igual que los franceses.

### c. Condiciones de honorabilidad e incompatibilidad

Cualquier custodio que no mantenga el registro de objetos de mobiliario a diario (ver infra "2o. d. La obligación de llevar un registro de muebles (ROM)) se castiga con seis meses de prisión y con una multa de 30.000 euros.

*Para ir más allá* Artículo 321-7 del Código Penal.

### d. Algunas peculiaridades de la regulación de la actividad

#### Obligación de llevar un registro de objetos de mobiliario (ROM)

Cualquier profesional que desee vender muebles usados o adquiridos de personas que no los hayan fabricado o comercializado está obligado a llenar un registro en papel listado y firmado (también llamado "registro de brocante") que contenga:

- Las características y procedencia de los objetos
- El precio de compra y cómo se liquida cada artículo
- para objetos con un precio inferior a 60,98 euros y no tienen interés artístico e histórico, bastan menciones y descripciones comunes.

También es posible utilizar un registro electrónico que requerirá un procesamiento automatizado para garantizar la integridad, intangibilidad y seguridad de los datos registrados. La vida útil de estos datos es de diez años.

*Para ir más allá* Artículos R. 321-3 y siguientes del Código Penal.

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, es aconsejable consultar el listado[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

3°. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Hacer un informe previo

El custodio está obligado a hacer una declaración previa que vale la pena solicitar la inscripción en el registro de revendedores de objetos de mobiliario. La solicitud se dirige al prefecto del lugar de instalación del distribuidor de muebles o al prefecto de París (especialmente para el nacional de un estado de la UE o del EEE) y debe incluir las siguientes piezas:

- una forma Cerfa 1173301 lleno, fechado y firmado;
- Fotocopia del documento de identidad del profesional francés o nacional de un Estado de la UE o del EEE;
- un certificado de registro en el registro de comercios y empresas o un certificado de registro en el directorio de operaciones.

*Para ir más allá* Artículo R. 32161 del Código Penal.

### c. Si es necesario, haga una declaración como titular de metales preciosos

Mientras el custodio posea objetos de oro, plata o platino, deberá hacer una declaración previa a la oficina de garantía de la Dirección Regional de Aduanas y Derechos Indirectos.

La declaración debe contener el nombre del custodio o empresa, la designación de su actividad, el lugar de práctica y la dirección de la sede central.

Al recibir el expediente, la oficina de garantía emitirá una declaración de existencia.

*Para ir más allá* Artículo 534 del Código Fiscal General.

