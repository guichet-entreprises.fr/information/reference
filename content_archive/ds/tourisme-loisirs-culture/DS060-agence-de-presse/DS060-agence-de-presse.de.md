﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS060" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourismus, Freizeit, Kultur" -->
<!-- var(title)="Nachrichtenagentur" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourismus-freizeit-kultur" -->
<!-- var(title-short)="nachrichtenagentur" -->
<!-- var(url)="https://www.guichet-entreprises.fr/de/ds/tourismus-freizeit-kultur/nachrichtenagentur.html" -->
<!-- var(last-update)="2020-04-15 17:24:14" -->
<!-- var(url-name)="nachrichtenagentur" -->
<!-- var(translation)="Auto" -->


Nachrichtenagentur
==================

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:24:14<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

### a. Definition

Die Nachrichtenagentur ist ein kommerzielles Unternehmen, das alle Informationen, die unter seiner redaktionellen Verantwortung Gegenstand der journalistischen Behandlung waren, sammelt, verarbeitet, ausbildet und professionell zur Verfügung stellt. Sein Umsatz muss mindestens die Hälfte aus der Lieferung dieser Artikel an Medienkunden (Presseverlag, Radiosender, Fernsehsender, Websites usw.) stammen.

Die Agenturen auf der Von der Liste erstellten Liste [Gemeinsame Kommission für Veröffentlichungen und Nachrichtenagenturen](http://www.cppap.fr/) (CPPAP) sind die einzigen, die den Begriff "Nachrichtenagentur" beanspruchen können.

**Was Sie wissen sollten**

Vor der Beantragung der Eintragung in die CPPAP-Liste muss das Unternehmen mindestens sechs Monate operiert haben. Dieser Übungszeitraum ermöglicht die Analyse des erzielten Umsatzes, um zu überprüfen, ob mindestens die Hälfte davon von Medienkunden stammt.

*Um weiter zu gehen* Artikel 1 der Verordnung 45-2646 vom 2. November 1945 zur Regelung von Nachrichtenagenturen.

### b. Zuständiges Business Formality Centre

Das relevante Zentrum für Geschäftsformalitäten (CFE) hängt von der Art der Struktur ab, in der die Tätigkeit ausgeübt wird. Im Falle einer Nachrichtenagentur handelt es sich um eine kommerzielle Tätigkeit. Das relevante CFE ist daher die Industrie- und Handelskammer (IHK).

Weitere Informationen finden Sie unter[Website des ICC Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites).

Zwei Grad. Tätigkeitsregelung
-----------------------------

#### Wirtschaftlicher Nutzen für Nachrichtenagenturen

Nachrichtenagenturen profitieren von einigen wirtschaftlichen Vorteilen, darunter:

- Befreiung von der Körperschaftsteuer (Artikel 1458-2 des Allgemeinen Steuergesetzbuchs);
- die Anwendung eines ermäßigten Mehrwertsteuersatzes auf die Weitergabe von Informationen an die Medien (Artikel 298 Oktate des Allgemeinen Steuergesetzbuchs);
- die Senkung der für den Beruf spezifischen Sozialabgaben (Dekret vom 26. März 1987 zur Festsetzung der Beihilfe für den Satz der Sozialversicherungsbeiträge, die für die Beschäftigung bestimmter Kategorien von Journalisten geschuldet werden);
- die Möglichkeit, Zuschüsse von der [strategischer Fonds für die Presseentwicklung](http://www.culturecommunication.gouv.fr/Politiques-ministerielles/Presse/Aides-a-la-presse/Le-fonds-strategique-pour-le-developpement-de-la-presse-aides-directes/1.-Presentation-du-Fonds-strategique-pour-le-developpement-de-la-presse2).

#### Verbot der Beteiligung ausländischer Gelder

Jeder Nachrichtenagentur oder einem ihrer Mitarbeiter ist es untersagt, Gelder oder Leistungen direkt oder indirekt von einer ausländischen Regierung zu erhalten (mit Ausnahme von Geldern, die gegen eine Leistung erhalten werden).

*Um weiter zu gehen* Artikel 5 der Verordnung vom 2. November 1945 und Abschnitt 8 des Aktes vom 1. August 1986 erwähnt.

#### Verbot des Erhalts oder der Zusagen eines Geldbetrags oder einer anderen Leistung

Es ist verboten, dass eine Nachrichtenagentur oder ihre Mitarbeiter einen Geldbetrag oder einen anderen Vorteil erhalten oder ihnen zugesagt werden, um Informationen über Finanzwerbung zu übertragen. Jedem redaktionellen Werbeartikel muss das Wort "Werbung" oder "kommuniziert" vorangestellt werden.

*Um weiter zu gehen* Artikel 5 der Verordnung vom 2. November 1945 und Abschnitt 10 des oben erwähnten Rechtsakts vom 1. August 1986.

#### Informationspflicht bei einer Überweisung

Die Nachrichtenagentur muss informieren:

- jede Übertragung oder Zusage, seine sozialen Rechte zu übertragen, die dem Erwerber mindestens ein Drittel des Grundkapitals oder der Stimmrechte einräumt;
- jede Übertragung oder Zusage, das Eigentum zu übertragen oder den Betrieb eines Pressetitels oder Online-Pressedienstes.

*Um weiter zu gehen* Artikel 6 der Verordnung vom 2. November 1945 und Abschnitt 6 des Gesetzes vom 1. August 1986.

#### Herausgeber

Nur eine Person, die Eigentümer, Mieter-Manager oder inhaber der Mehrheit des Sozialkapitals oder des Stimmrechts der Nachrichtenagentur ist, kann als Herausgeber bestellt werden.

Darüber hinaus ist es obligatorisch, einen Co-Direktor für die Nachrichtenagentur zu ernennen, wenn der Herausgeber parlamentarische Immunität genießt.

Der Regisseur (und gegebenenfalls Co-Regisseur) muss alt sein, seine Bürgerrechte haben und seine Bürgerrechte genießen.

*Um weiter zu gehen* Artikel 6 des Pressefreiheitsgesetzes vom 29. Juli 1881; Artikel 9 des genannten Gesetzes vom 1. August 1986; Artikel 2 der Verordnung vom 2. November 1945.

#### Verbot aller Werbung und Trinkgeld

Die Nachrichtenagentur ist verboten:

- in irgendeiner Form von Werbung für Dritte zu werben;
- Bereitstellung kostenloser Informationen für Unternehmen, die Presseveröffentlichungen veröffentlichen, Herausgeberelektronischer öffentlicher Kommunikationsdienste oder Nachrichtenagenturen.

*Um weiter zu gehen* Artikel 3 der Verordnung vom 2. November 1945.

#### Einhaltung der Barrierefreiheits- und Sicherheitsvorschriften

Wenn die Räumlichkeiten für die Öffentlichkeit zugänglich sind, muss der Fachmann die Vorschriften über öffentliche Einrichtungen (ERP) einhalten:

- Feuer
- Zugänglichkeit.

Weitere Informationen finden Sie im Aktivitätsblatt[Einrichtung, die die Öffentlichkeit empfängt](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

**Gut zu wissen**

Verstößen gegen eine dieser Voraussetzungen droht eine Geldstrafe von 6.000 Euro und/oder eine sechsmonatige Freiheitsstrafe (Artikel 9 der Verordnung vom 2. November 1945).

Drei Grad. Installationsverfahren und Formalitäten
--------------------------------------------------

### a. Meldeformalitäten des Unternehmens

Die Formalitäten hängen von der Rechtsnatur des Unternehmens ab. Weitere Informationen finden Sie im Tätigkeitsblatt "Meldeformalitäten eines Handelsunternehmens".

### b. Bitte um Aufnahme in die Liste der Nachrichtenagenturen

Nur Agenturen auf der CPPAP-Liste können den Namen "Nachrichtenagentur" verwenden. Gegebenenfalls wird die Registrierung für maximal fünf Jahre gewährt, die verlängerbar ist. Andererseits kann die Erstregistrierung nur für maximal zwei Jahre gewährt werden.

**Zuständige Behörde**

Der Antrag auf Registrierung ist an das CPPAP-Sekretariat gerichtet.

**Verfahren**

Das CPPAP registriert den Registrierungsantrag und überprüft die Registrierungsdatei. Sie kann eine Vor-Ort-Untersuchung mit dem Unternehmen durchführen, um bestimmte Informationen in der Akte zu bestätigen oder zu klären. Nach der Einweisung gibt das CPPAP eine Mitteilung aus. Wenn sie für die Registrierung ist, wird ein interministerielles Dekret zur Unterzeichnung der für Kommunikation und Haushalt zuständigen Minister vorgeschlagen, bevor sie im Amtsblatt veröffentlicht wird.

**Zeit**

Als Hinweis: Es dauert zwei bis vier Monate, bis die Registrierung oder Erneuerung der Registrierung in die Liste der Nachrichtenagenturen erfolgt. Darüber hinaus erfordert die Veröffentlichung des interministeriellen Beschlusses im Amtsblatt eine Verzögerung von etwa sechs bis acht Monaten ab der positiven Anmeldung. Die Eintragung tritt am Tag nach der Veröffentlichung im Amtsblatt in Kraft.

**Belege**

Dem Antrag sind folgende Unterlagen beizufügen:

- Form Form Cerfa 1240503 ausgefüllt, datiert und unterschrieben;
- Ein Kbis-Extrakt, weniger als drei Monate alt;
- Aktualisierte, datierte und signierte Status;
- ein Detail pro Kunde des Gesamtumsatzes für das abgelaufene oder seit Beginn des Geschäftsjahres für Erstanträge, kategorisiert nach Kategorien, Medien und Nicht-Medien (Medien, Institutionen und Unternehmen, andere). Diese vom Geschäftsführer des Unternehmens erstellte Liste muss von einem Wirtschaftsprüfer für Unternehmen mit einem Umsatz von mehr als 250.000 Euro beglaubigt werden;
- eine Unternehmenspräsentation über die Art ihrer Aktivitäten und ihre Entwicklungsaussichten;
- Die Gewinn- und Verlustrechnung für das letzte Jahr beendete den Anmeldetag;
- eine Kopie eines Gehaltsbelegs für den Bezugsmonat der Lohnabrechnung wichtiger Mitarbeiter.

**Kosten**

kostenlos.

**Heilmittel**

Wenn Sie sich weigern, sich in die Liste der Nachrichtenagenturen eintragendes Ablehnungsschreiben zu registrieren, wird ein mit Gründen versehenes Ablehnungsschreiben an den Antragsteller gerichtet. Der Anmelder kann dann gemäß den oben genannten Bestimmungen des Beschlusses vom 2. November 1945 eine erneute Prüfung seiner Anmeldung beantragen, indem er eine neue Akte einreicht. Das CPPAP überprüft diese neue Anwendung auf der nächsten Sitzung.

Darüber hinaus kann der Antragsteller die Weigerung des CPPAP auch anfechten, indem er

- oder ex gratia appellieren an den Präsidenten des CPPAP, innerhalb von zwei Monaten nach Dernot der Verweigerung der Registrierung oder Erneuerung der Registrierung;
- oder durch Einreichung einer Klage beim Verwaltungsgericht innerhalb von zwei Monaten nach Zustellung der Verweigerung der Registrierung oder Erneuerung der Eintragung. Diese Frist verlängert sich, wenn zuvor ein anmutiger Rechtsbehelf eingelegt wurde.

