﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS060" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Agencia de noticias" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="agencia-de-noticias" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/agencia-de-noticias.html" -->
<!-- var(last-update)="2020-04-15 17:24:14" -->
<!-- var(url-name)="agencia-de-noticias" -->
<!-- var(translation)="Auto" -->


Agencia de noticias
===================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:24:14<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

### a. Definición

La agencia de noticias es una empresa comercial que recopila, procesa, forma y proporciona profesionalmente toda la información que ha sido objeto, bajo su responsabilidad editorial, del tratamiento periodístico. Su volumen de negocios debe provenir al menos a la mitad del suministro de estos artículos a los clientes de medios (publicación de prensa, estaciones de radio, canales de televisión, sitios web, etc.).

Las agencias de la lista elaborada por el [Comisión Mixta de Publicaciones y Agencias de Noticias](http://www.cppap.fr/) (CPPAP) son los únicos que pueden reclamar el término "agencia de noticias".

**Qué saber**

Antes de solicitar el registro en la lista CPPAP, la empresa debe haber operado durante al menos seis meses. Este período de ejercicio permite el análisis del volumen de negocios alcanzado, con el fin de verificar que al menos la mitad de la misma proviene de clientes de medios de comunicación.

*Para ir más allá* Artículo 1 de la Ordenanza 45-2646, de 2 de noviembre de 1945, por la que se regulan las agencias de noticias.

### b. Centro competente de formalidad empresarial

El centro de formalidades empresariales (CFE) correspondiente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. En el caso de una agencia de noticias, es una actividad comercial. Por lo tanto, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

Para más información, es aconsejable consultar[Sitio web de ICC Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites).

Dos grados. Regulación de la actividad
--------------------------------------

#### Beneficios económicos para las agencias de noticias

Las agencias de noticias se benefician de algunos beneficios económicos, entre ellos:

- exención del impuesto sobre la propiedad de sociedades (artículo 1458-2 del Código Fiscal General);
- la aplicación de un tipo reducido del IVA a la transferencia de información a los medios de comunicación (artículo 298 octies del Código General Tributario);
- la reducción de las tasas de seguridad social específicas de la profesión (decreto de 26 de marzo de 1987 por el que se fija la asignación aplicable a la tasa de cotizaciones a la seguridad social debidas para el empleo de determinadas categorías de periodistas);
- la posibilidad de recibir subvenciones de la [fondo estratégico para el desarrollo de la prensa](http://www.culturecommunication.gouv.fr/Politiques-ministerielles/Presse/Aides-a-la-presse/Le-fonds-strategique-pour-le-developpement-de-la-presse-aides-directes/1.-Presentation-du-Fonds-strategique-pour-le-developpement-de-la-presse2).

#### Prohibición de la participación de fondos extranjeros

Cualquier agencia de noticias, o cualquiera de sus empleados, tiene prohibido recibir fondos o beneficios directa o indirectamente de un gobierno extranjero (excepto los fondos recibidos a cambio de un beneficio).

*Para ir más allá* Se menciona el artículo 5 de la ordenanza de 2 de noviembre de 1945 y el artículo 8 del acto de 1 de agosto de 1986.

#### Prohibición de recibir o ser prometido una suma de dinero o cualquier otro beneficio

Está prohibido que cualquier agencia de noticias, o cualquiera de sus empleados, reciba o se le prometa una suma de dinero, o cualquier otro beneficio, con el propósito de pasar a la información de la publicidad financiera. Cualquier artículo de publicidad editorial debe ir precedido por la palabra "publicidad" o "comunicado".

*Para ir más allá* Artículo 5 de la ordenanza de 2 de noviembre de 1945 y artículo 10 del acta de 1 de agosto de 1986 mencionada anteriormente.

#### Obligación de informar sobre una transferencia

La agencia de noticias debe informar:

- cualquier transferencia o promesa de transferir sus derechos sociales dando al cesionario al menos un tercio del capital social o derechos de voto;
- cualquier transferencia o promesa de transferir la propiedad o el funcionamiento de un título de publicación de prensa o servicio de prensa en línea.

*Para ir más allá* Artículo 6 de la ordenanza de 2 de noviembre de 1945 y artículo 6 de la Ley de 1 de agosto de 1986.

#### Director del editor

Sólo una persona que posee, arrendatario-administrador o que posee la mayoría del capital social o los derechos de voto de la agencia de noticias puede ser nombrado como editor.

Además, si el editor goza de inmunidad parlamentaria, es obligatorio nombrar un codirector de la agencia de noticias.

El director (y codirector, si los hubiere) debe ser mayores de edad, tener sus derechos civiles y disfrutar de sus derechos civiles.

*Para ir más allá* Artículo 6 de la Ley de Libertad de Prensa de 29 de julio de 1881; Artículo 9 de la citada Ley de 1 de agosto de 1986; Artículo 2 de la ordenanza de 2 de noviembre de 1945.

#### Prohibición de toda publicidad y propina

La agencia de noticias tiene prohibido:

- participar en cualquier forma de publicidad para terceros;
- proporcionar información gratuita a las empresas que publican publicaciones de prensa, editores de servicios de comunicación pública electrónica o agencias de noticias.

*Para ir más allá* Artículo 3 de la ordenanza de 2 de noviembre de 1945.

#### Respetar las normas de accesibilidad y seguridad

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas sobre instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, es aconsejable consultar la hoja de actividades[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

**Es bueno saber**

Los infractores de uno de estos requisitos se enfrentan a una multa de 6.000 euros y/o una pena de seis meses de prisión (artículo 9 de la ordenanza de 2 de noviembre de 1945 supra).

Tres grados. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Las formalidades dependen de la naturaleza jurídica del negocio. Para obtener más información, consulte la hoja de actividades "Formalidades de notificación de una empresa comercial".

### b. Pedir inclusión en la lista de agencias de noticias

Sólo las agencias de la lista CPPAP pueden usar el nombre "agencia de noticias". Si es necesario, el registro se concede por un máximo de cinco años, renovable. Por otra parte, el primer registro sólo puede concederse por un máximo de dos años.

**Autoridad competente**

La solicitud de registro se dirige a la secretaría de la CPPAP.

**Procedimiento**

El CPPAP registra la solicitud de registro y verifica el archivo de registro. Puede llevar a cabo una investigación in situ con la empresa para confirmar o aclarar cierta información en el archivo. Después de la instrucción, el CPPAP emite un aviso. Si está a favor del registro, se propone un decreto interministerial para la firma de los ministros encargados de la comunicación y del presupuesto antes de publicarse en el Diario Oficial.

**hora**

Como indicación, se tarda de dos a cuatro meses en obtener el registro o la renovación del registro en la lista de agencias de noticias. Además, la publicación de la orden interministerial en el Diario Oficial requiere un retraso de unos seis a ocho meses a partir del anuncio favorable para el registro. El registro entra en vigor al día siguiente de la publicación en el Diario Oficial.

**Documentos de apoyo**

La solicitud deberá ir acompañada de los siguientes documentos:

- Formulario Cerfa 1240503 completado, fechado y firmado;
- Un extracto de Kbis, menos de tres meses de edad;
- Estado actualizado, fechado y firmado;
- un detalle por cliente de los ingresos totales del último año terminado o desde el inicio del ejercicio para las primeras solicitudes, categorizados por categorías, medios de comunicación y no medios (medios de comunicación, instituciones y empresas, entre otros). Esta lista, elaborada por el gerente de la empresa, debe ser certificada por un contable para empresas con una facturación de más de 250.000 euros;
- Una nota de presentación de la empresa por la naturaleza de sus actividades y sus perspectivas de desarrollo;
- La cuenta de resultados del último año terminó la fecha de presentación;
- una copia de un resguardo salarial para el mes de referencia de la nómina de los empleados clave.

**Costo**

Gratis.

**Remedios**

Si se niega a registrarse en la lista de agencias de noticias, se envía una carta de denegación motivada al solicitante. A continuación, el solicitante puede solicitar un nuevo examen de su solicitud presentando un nuevo expediente, de conformidad con las disposiciones de la orden de 2 de noviembre de 1945 antes mencionada. El CPPAP revisa esta nueva solicitud en la próxima reunión.

Además, el solicitante también puede impugnar la negativa de la CPPAP:

- o ex gratia apelar al Presidente de la CPPAP, dentro de los dos meses siguientes a la notificación de la denegación de registro o renovación del registro;
- o presentando una impugnación legal ante el Tribunal Administrativo, dentro de los dos meses siguientes a la notificación de la denegación de registro o renovación del registro. Este plazo se extiende si se ha iniciado previamente una apelación agraciada.

