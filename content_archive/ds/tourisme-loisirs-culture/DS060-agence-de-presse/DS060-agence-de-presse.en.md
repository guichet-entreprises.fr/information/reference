﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS060" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="News agency" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="news-agency" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/news-agency.html" -->
<!-- var(last-update)="2020-04-15 17:24:14" -->
<!-- var(url-name)="news-agency" -->
<!-- var(translation)="Auto" -->


News agency
===========

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:24:14<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Defining the activity
------------------------

### a. Definition

The news agency is a commercial company that collects, processes, trains and professionally provides all the information that has been the subject, under its editorial responsibility, of journalistic treatment. Its turnover must come at least half from the supply of these items to media customers (press publishing, radio stations, television channels, websites, etc.).

The agencies on the list drawn up by the [Joint Commission of Publications and News Agencies](http://www.cppap.fr/) (CPPAP) are the only ones that can claim the term "news agency."

**What to know**

Before applying for registration on the CPPAP list, the company must have operated for at least six months. This exercise period allows the analysis of the turnover achieved, in order to verify that at least half of it comes from media customers.

*To go further* Article 1 of Ordinance 45-2646 of November 2, 1945, regulating news agencies.

### b. Competent Business Formality Centre

The relevant business formalities centre (CFE) depends on the nature of the structure in which the activity is carried out. In the case of a news agency, it is a commercial activity. The relevant CFE is therefore the Chamber of Commerce and Industry (CCI).

For more information, it is advisable to consult[ICC Paris website](http://www.entreprises.cci-paris-idf.fr/web/formalites).

Two degrees. Activity regulation
--------------------------------

#### Economic benefits to news agencies

News agencies benefit from some economic benefits, including:

- exemption from corporate property tax (Article 1458-2 of the General Tax Code);
- the application of a reduced VAT rate on the transfer of information to the media (Article 298 octies of the General Tax Code);
- the reduction of social security charges specific to the profession (decree of 26 March 1987 setting the allowance applicable to the rate of social security contributions due for the employment of certain categories of journalists);
- the possibility of receiving grants from the [strategic fund for press development](http://www.culturecommunication.gouv.fr/Politiques-ministerielles/Presse/Aides-a-la-presse/Le-fonds-strategique-pour-le-developpement-de-la-presse-aides-directes/1.-Presentation-du-Fonds-strategique-pour-le-developpement-de-la-presse2).

#### Prohibition of participation of foreign funds

Any news agency, or any of its employees, is prohibited from receiving funds or benefits directly or indirectly from a foreign government (except for funds received in return for a benefit).

*To go further* Article 5 of the ordinance of November 2, 1945 and section 8 of the act of August 1, 1986 mentioned.

#### Prohibition of receiving or being promised a sum of money or any other benefit

It is forbidden for any news agency, or any of its employees, to receive or be promised a sum of money, or any other benefit, for the purpose of transvesting into information of financial advertising. Any editorial advertising article must be preceded by the word "advertisement" or "communicated."

*To go further* Article 5 of the ordinance of November 2, 1945 and section 10 of the act of August 1, 1986 mentioned above.

#### Obligation to inform on a transfer

The news agency must inform:

- any transfer or promise to transfer his social rights giving the transferee at least one third of the share capital or voting rights;
- any transfer or promise to transfer ownership or the operation of a press publication title or online press service.

*To go further* Article 6 of the ordinance of November 2, 1945, and section 6 of the Act of August 1, 1986.

#### Editor's director

Only a person who owns, tenant-manager or who holds the majority of the social capital or voting rights of the news agency can be appointed as editor.

In addition, if the editor enjoys parliamentary immunity, it is mandatory to appoint a co-director to the news agency.

The director (and co-director, if any) must be of age, have his civil rights and enjoy his civil rights.

*To go further* Article 6 of the Press Freedom Act of 29 July 1881; Article 9 of the aforementioned Law of 1 August 1986; Article 2 of the ordinance of November 2, 1945.

#### Prohibition of all advertising and gratuity

The news agency is prohibited from:

- engage in any form of advertising for third parties;
- provide free information to companies that publish press publications, publishers of electronic public communication services or news agencies.

*To go further* Article 3 of the ordinance of November 2, 1945.

#### Respect accessibility and safety rules

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, it is advisable to refer to the activity sheet[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

**Good to know**

Violators of one of these requirements face a fine of 6,000 euros and/or a six-month prison sentence (Article 9 of the ordinance of 2 November 1945 above).

Three degrees. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the activity sheet "Reporting formalities of a commercial company."

### b. Ask for inclusion on the list of news agencies

Only agencies on the CPPAP list can use the name "news agency." If necessary, registration is granted for a maximum of five years, renewable. On the other hand, the first registration can only be granted for a maximum of two years.

**Competent authority**

The application for registration is addressed to the CPPAP secretariat.

**Procedure**

The CPPAP registers the application for registration and verifies the registration file. It may conduct an on-site investigation with the company to confirm or clarify certain information in the file. After instruction, the CPPAP issues a notice. If it is in favour of registration, an inter-ministerial decree is proposed for the signature of the ministers in charge of communication and the budget before being published in the Official Journal.

**Time**

As an indication, it takes two to four months to obtain registration or renewal of the registration on the list of news agencies. In addition, the publication of the inter-ministerial order in the Official Journal requires a delay of about six to eight months from the favourable notice for registration. The registration takes effect the day after the publication in the Official Journal.

**Supporting documents**

The application must be accompanied by the following documents:

- Form Cerfa 1240503 completed, dated and signed;
- A Kbis extract, less than three months old;
- Updated, dated and signed statuses;
- one detail per customer of total revenue for the last year ended or since the beginning of the fiscal year for first applications, categorized by categories, media and non-media (media, institutions and companies, others). This list, drawn up by the manager of the company, must be certified by an accountant for companies with a turnover of more than 250,000 euros;
- A company presentation note out of the nature of its activities and its prospects for development;
- The income statement for the last year ended the filing date;
- a copy of a salary slip for the reference month of the payroll of key employees.

**Cost**

Free.

**Remedies**

If you refuse to register on the list of news agencies, a reasoned refusal letter is sent to the applicant. The applicant can then request a re-examination of his application by filing a new file, in accordance with the provisions of the order of November 2, 1945 mentioned above. The CPPAP reviews this new application at the next meeting.

In addition, the applicant may also challenge the CPPAP's refusal by:

- or ex gratia appeal to the President of the CPPAP, within two months of notification of the refusal to register or renew registration;
- or by filing a legal challenge with the Administrative Court, within two months of notification of the refusal to register or renew registration. This period is extended if a graceful appeal has been initiated beforehand.

