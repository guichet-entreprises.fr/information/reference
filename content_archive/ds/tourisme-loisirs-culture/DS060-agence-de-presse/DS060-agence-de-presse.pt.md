﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS060" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Lazer, Cultura" -->
<!-- var(title)="Agência de notícias" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-lazer-cultura" -->
<!-- var(title-short)="agencia-de-noticias" -->
<!-- var(url)="https://www.guichet-entreprises.fr/pt/ds/turismo-lazer-cultura/agencia-de-noticias.html" -->
<!-- var(last-update)="2020-04-15 17:24:14" -->
<!-- var(url-name)="agencia-de-noticias" -->
<!-- var(translation)="Auto" -->


Agência de notícias
===================

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:24:14<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

### a. Definição

A agência de notícias é uma empresa comercial que coleta, processa, treina e presta profissionalmente todas as informações que tem sido objeto, sua responsabilidade editorial, de tratamento jornalístico. Seu volume de negócios deve vir pelo menos metade do fornecimento desses itens para clientes de mídia (imprensa, estações de rádio, canais de televisão, sites, etc.).

As agências da lista elaborada pelo [Comissão Mista de Publicações e Agências de Notícias](http://www.cppap.fr/) (CPPAP) são os únicos que podem reivindicar o termo "agência de notícias".

**O que saber**

Antes de solicitar o registro na lista do CPPAP, a empresa deve ter operado por pelo menos seis meses. Esse período de exercício permite a análise do volume de negócios alcançado, a fim de verificar que pelo menos metade dele vem de clientes de mídia.

*Para ir mais longe* Artigo 1º da Portaria 45-2646, de 2 de novembro de 1945, que regulamenta as agências de notícias.

### b. Centro de Formalidade Empresarial Competente

O centro de formalidades empresariais relevantes (CFE) depende da natureza da estrutura em que a atividade é realizada. No caso de uma agência de notícias, é uma atividade comercial. A CFE relevante é, portanto, a Câmara de Comércio e Indústria (CCI).

Para obter mais informações, é aconselhável consultar[Site da ICC Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites).

Dois graus. Regulação da atividade
----------------------------------

#### Benefícios econômicos para agências de notícias

As agências de notícias se beneficiam de alguns benefícios econômicos, incluindo:

- isenção do Imposto sobre a Propriedade Depropriedade Empresarial (artigo 1458-2 do Código Tributário Geral);
- a aplicação de uma alíquota reduzida do IVA sobre a transferência de informações para a mídia (artigo 298 º do Código Tributário Geral);
- a redução dos encargos previdenciários específicos da profissão (decreto de 26 de março de 1987 que estabelece o subsídio aplicável à alíquota das contribuições previdenciárias devidas para o emprego de determinadas categorias de jornalistas);
- a possibilidade de receber subsídios do [fundo estratégico para o desenvolvimento da imprensa](http://www.culturecommunication.gouv.fr/Politiques-ministerielles/Presse/Aides-a-la-presse/Le-fonds-strategique-pour-le-developpement-de-la-presse-aides-directes/1.-Presentation-du-Fonds-strategique-pour-le-developpement-de-la-presse2).

#### Proibição de participação de fundos estrangeiros

Qualquer agência de notícias, ou qualquer de seus funcionários, está proibida de receber fundos ou benefícios direta ou indiretamente de um governo estrangeiro (exceto os fundos recebidos em troca de um benefício).

*Para ir mais longe* Artigo 5º da portaria de 2 de novembro de 1945 e seção 8 do ato de 1º de agosto de 1986 mencionado.

#### Proibição de receber ou ser prometido uma soma de dinheiro ou qualquer outro benefício

É proibido para qualquer agência de notícias, ou qualquer de seus funcionários, receber ou receber uma quantia de dinheiro, ou qualquer outro benefício, com o propósito de transtransportar em informações de publicidade financeira. Qualquer artigo publicitário editorial deve ser precedido pela palavra "propaganda" ou "comunicado".

*Para ir mais longe* Artigo 5º da portaria de 2 de novembro de 1945 e seção 10 do ato de 1º de agosto de 1986 mencionado acima.

#### Obrigação de informar sobre uma transferência

A agência de notícias deve informar:

- qualquer transferência ou promessa de transferência de seus direitos sociais dando ao cessionário pelo menos um terço do capital social ou direitos de voto;
- qualquer transferência ou promessa de transferir a propriedade ou a operação de um título de publicação de imprensa ou serviço de imprensa on-line.

*Para ir mais longe* Artigo 6º da portaria de 2 de novembro de 1945, e seção 6 da Lei de 1º de agosto de 1986.

#### Diretor do editor

Apenas uma pessoa que possui, inquilino-gerente ou que detém a maioria do capital social ou direitos de voto da agência de notícias pode ser nomeada como editora.

Além disso, se o editor goza de imunidade parlamentar, é obrigatório nomear um co-diretor para a agência de notícias.

O diretor (e co-diretor, se houver) deve ser maior de idade, ter seus direitos civis e usufruir de seus direitos civis.

*Para ir mais longe* Artigo 6º da Lei da Liberdade de Imprensa de 29 de Julho de 1881; Artigo 9º da referida Lei de 1 º de Agosto de 1986; Artigo 2º da portaria de 2 de novembro de 1945.

#### Proibição de toda publicidade e gratuidade

A agência de notícias está proibida de:

- envolver-se em qualquer forma de publicidade para terceiros;
- fornecer informações gratuitas para empresas que publicam publicações de imprensa, editoras de serviços eletrônicos de comunicação pública ou agências de notícias.

*Para ir mais longe* Artigo 3º da portaria de 2 de novembro de 1945.

#### Respeite as regras de acessibilidade e segurança

Se o local for aberto ao público, o profissional deverá cumprir as regras sobre instituições públicas (ERP):

- Fogo
- Acessibilidade.

Para obter mais informações, é aconselhável consultar a folha de atividades[Estabelecimento recebendo o público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

**É bom saber**

Os infratores de um desses requisitos enfrentam uma multa de 6.000 euros e/ou uma sentença de seis meses de prisão (artigo 9º da portaria de 2 de Novembro de 1945 acima).

Três graus. Procedimentos de instalação e formalidades
------------------------------------------------------

### a. Formalidades de relatórios da empresa

As formalidades dependem da natureza jurídica do negócio. Para obter mais informações, consulte a folha de atividades "Reportando formalidades de uma empresa comercial".

### b. Peça inclusão na lista de agências de notícias

Apenas agências na lista cppap podem usar o nome "agência de notícias". Se necessário, a inscrição é concedida por um máximo de cinco anos, renováveis. Por outro lado, a primeira inscrição só pode ser concedida por um período máximo de dois anos.

**Autoridade competente**

O pedido de inscrição é endereçado à secretaria do CPPAP.

**Procedimento**

O CPPAP registra o pedido de registro e verifica o arquivo de registro. Ele pode realizar uma investigação no local com a empresa para confirmar ou esclarecer certas informações no arquivo. Após instrução, o CPPAP emite um aviso. Se for a favor do registro, propõe-se um decreto interministerial para a assinatura dos ministros encarregados da comunicação e do orçamento antes de ser publicado no Diário Oficial.

**Tempo**

Como indicação, leva de dois a quatro meses para obter o registro ou renovação do registro na lista de agências de notícias. Além disso, a publicação da portaria interministerial no Diário Oficial requer um atraso de cerca de seis a oito meses a partir do edital favorável para inscrição. O registro entra em vigor no dia seguinte à publicação no Diário Oficial.

**Documentos de suporte**

A solicitação deve estar acompanhada dos seguintes documentos:

- Formulário Cerfa 1240503 concluído, datado e assinado;
- Um extrato de Kbis, com menos de três meses de idade;
- Status atualizados, datados e assinados;
- um detalhe por cliente da receita total do último ano encerrado ou desde o início do ano fiscal para as primeiras aplicações, categorizadapor categorias, mídia e não-mídia (mídia, instituições e empresas, outros). Esta lista, elaborada pelo gerente da empresa, deve ser certificada por um contador para empresas com faturamento superior a 250.000 euros;
- Uma nota de apresentação da empresa fora da natureza de suas atividades e suas perspectivas de desenvolvimento;
- A demonstração de resultados do último ano encerrou a data de apresentação;
- uma cópia de um boleto salarial para o mês de referência da folha de pagamento dos principais funcionários.

**Custo**

Livre.

**Remédios**

Se você se recusar a se registrar na lista de agências de notícias, uma carta de recusa fundamentada é enviada ao requerente. O requerente pode então solicitar um novo exame de sua solicitação mediante a apresentação de um novo arquivo, de acordo com as disposições da ordem de 2 de novembro de 1945 mencionada acima. O CPPAP analisa esta nova aplicação na próxima reunião.

Além disso, o requerente também pode contestar a recusa do CPPAP por:

- ou ex gratia apelar ao Presidente do CPPAP, no prazo de dois meses a partir da notificação da recusa em registrar ou renovar o registro;
- ou ajuizando uma contestação judicial ao Tribunal Administrativo, no prazo de dois meses após a notificação da recusa em registrar ou renovar o registro. Este período é prorrogado se um apelo gracioso tiver sido iniciado previamente.

