﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS060" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Tempo libero, Cultura" -->
<!-- var(title)="Agenzia di stampa" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-tempo-libero-cultura" -->
<!-- var(title-short)="agenzia-di-stampa" -->
<!-- var(url)="https://www.guichet-entreprises.fr/it/ds/turismo-tempo-libero-cultura/agenzia-di-stampa.html" -->
<!-- var(last-update)="2020-04-15 17:24:14" -->
<!-- var(url-name)="agenzia-di-stampa" -->
<!-- var(translation)="Auto" -->


Agenzia di stampa
=================

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:24:14<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

### a. Definizione

L'agenzia di stampa è una società commerciale che raccoglie, elabora, forma e fornisce professionalmente tutte le informazioni che sono state oggetto, sotto la sua responsabilità editoriale, del trattamento giornalistico. Il suo fatturato deve provenire almeno la metà dalla fornitura di tali elementi ai clienti dei media (pubblicazione stampa, stazioni radiofoniche, canali televisivi, siti web, ecc.).

Le agenzie nell'elenco redatto dal [Commissione congiunta di pubblicazioni e agenzie di stampa](http://www.cppap.fr/) (CPPAP) sono gli unici che possono rivendicare il termine "agenzia di stampa".

**Cosa sapere**

Prima di presentare domanda per la registrazione nell'elenco CPPAP, la società deve aver operato per almeno sei mesi. Questo periodo di esercizio consente l'analisi del fatturato realizzato, al fine di verificare che almeno la metà provenga da clienti di media.

*Per andare oltre* Articolo 1 dell'ordinanza 45-2646 del 2 novembre 1945, che regolava le agenzie di stampa.

### b. Centro competente per le formalità aziendali

Il centro di formalità imprenditoriale (CFE) competente dipende dalla natura della struttura in cui viene svolta l'attività. Nel caso di un'agenzia di stampa, si tratta di un'attività commerciale. Il CFE pertinente è quindi la Camera di Commercio e Industria (CCI).

Per ulteriori informazioni, si consiglia di consultare[Sito web dell'ICC di Parigi](http://www.entreprises.cci-paris-idf.fr/web/formalites).

Due gradi. Regolamento delle attività
-------------------------------------

#### Vantaggi economici per le agenzie di stampa

Le agenzie di stampa beneficiano di alcuni vantaggi economici, tra cui:

- esenzione dall'imposta sugli immobili societari (articolo 1458-2 del codice fiscale generale);
- l'applicazione di un'aliquota IVA ridotta sul trasferimento di informazioni ai media (articolo 298 ocità del codice fiscale generale);
- la riduzione degli oneri previdenziali specifici per la professione (decreto del 26 marzo 1987 che fissa l'indennità applicabile al tasso di contributi previdenziali dovuto all'impiego di determinate categorie di giornalisti);
- la possibilità di ricevere sovvenzioni dal [fondo strategico per lo sviluppo della stampa](http://www.culturecommunication.gouv.fr/Politiques-ministerielles/Presse/Aides-a-la-presse/Le-fonds-strategique-pour-le-developpement-de-la-presse-aides-directes/1.-Presentation-du-Fonds-strategique-pour-le-developpement-de-la-presse2).

#### Divieto di partecipazione di fondi esteri

Qualsiasi agenzia di stampa, o uno qualsiasi dei suoi dipendenti, è vietato ricevere fondi o prestazioni direttamente o indirettamente da un governo straniero (ad eccezione dei fondi ricevuti in cambio di un beneficio).

*Per andare oltre* L'articolo 5 dell'ordinanza del 2 novembre 1945 e la sezione 8 dell'atto del 1 agosto 1986 hanno menzionato.

#### Divieto di ricevere o di ricevere una somma di denaro o qualsiasi altra prestazione

È vietato a qualsiasi agenzia di stampa, o a uno qualsiasi dei suoi dipendenti, ricevere o ricevere una somma di denaro, o qualsiasi altro beneficio, allo scopo di trasvestirsi in informazioni di pubblicità finanziaria. Qualsiasi articolo pubblicitario editoriale deve essere preceduto dalla parola "pubblicità" o "comunicato".

*Per andare oltre* Articolo 5 dell'ordinanza del 2 novembre 1945 e della sezione 10 dell'atto del 1 agosto 1986 menzionato in precedenza.

#### Obbligo di informare su un trasferimento

L'agenzia di stampa deve informare:

- qualsiasi trasferimento o promessa di trasferire i suoi diritti sociali consegnando al cessionato almeno un terzo del capitale sociale o dei diritti di voto;
- qualsiasi trasferimento o promessa di trasferire la proprietà o l'operazione di un titolo di pubblicazione stampa o di un servizio stampa online.

*Per andare oltre* Articolo 6 dell'ordinanza del 2 novembre 1945 e della sezione 6 della legge del 1o agosto 1986.

#### Direttore del redattore

Solo una persona che possiede, lo stesso locatario o detiene la maggioranza del capitale sociale o i diritti di voto dell'agenzia di stampa possono essere nominati come redattori.

Inoltre, se l'editore gode dell'immunità parlamentare, è obbligatorio nominare un co-direttore dell'agenzia di stampa.

Il direttore (e il co-direttore, se presente) devono avere i suoi diritti civili e godere dei suoi diritti civili.

*Per andare oltre* Articolo 6 del Press Freedom Act del 29 luglio 1881; articolo 9 della suddetta legge del 1o agosto 1986; Articolo 2 dell'ordinanza del 2 novembre 1945.

#### Divieto di tutta la pubblicità e la gratuità

L'agenzia di stampa è vietata a:

- impegnarsi in qualsiasi forma di pubblicità per terzi;
- fornire informazioni gratuite alle aziende che pubblicano pubblicazioni stampa, editori di servizi di comunicazione elettronica pubblica o agenzie di stampa.

*Per andare oltre* Articolo 3 dell'ordinanza del 2 novembre 1945.

#### Rispettare le regole di accessibilità e sicurezza

Se i locali sono aperti al pubblico, il professionista deve rispettare le norme sulle istituzioni pubbliche (ERP):

- Fuoco
- Accessibilità.

Per ulteriori informazioni, si consiglia di fare riferimento alla scheda attività[Istituzione che riceve il pubblico](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

**Buono a sapersi**

I trasgressori di uno di questi requisiti devono essere multati per 6.000 euro e/o una pena detentiva di sei mesi (articolo 9 dell'ordinanza del 2 novembre 1945 sopra).

Tre gradi. Procedure di installazione e formalità
-------------------------------------------------

### a. Le formalità di rendicontazione delle società

Le formalità dipendono dalla natura giuridica dell'azienda. Per ulteriori informazioni, fare riferimento al foglio attività "Segnalazione delle formalità di una società commerciale".

### b. Chiedere l'inclusione nell'elenco delle agenzie di stampa

Solo le agenzie nell'elenco CPPAP possono utilizzare il nome "agenzia di stampa". Se necessario, la registrazione è concessa per un massimo di cinque anni, rinnovabile. D'altra parte, la prima registrazione può essere concessa solo per un massimo di due anni.

**Autorità competente**

La domanda di registrazione è indirizzata al segretariato del CPPAP.

**Procedura**

Il CPPAP registra la domanda di registrazione e verifica il file di registrazione. Può condurre un'indagine in loco con la società per confermare o chiarire alcune informazioni nel file. Dopo l'istruzione, il CPPAP emette un avviso. Se è favorevole alla registrazione, viene proposto un decreto interministeriale per la firma dei ministri incaricati della comunicazione e del bilancio prima di essere pubblicato nella Gazzetta ufficiale.

**Tempo**

A dire l'indicazione, ci vogliono da due a quattro mesi per ottenere la registrazione o il rinnovo della registrazione nell'elenco delle agenzie di stampa. Inoltre, la pubblicazione dell'ordinanza interministeriale nella Gazzetta ufficiale richiede un ritardo di circa sei-otto mesi rispetto all'avviso favorevole di registrazione. La registrazione ha effetto il giorno successivo alla pubblicazione nella Gazzetta ufficiale.

**Documenti di supporto**

La domanda deve essere corredata dei seguenti documenti:

- Modulo Cerfa 1240503 completato, datato e firmato;
- Un estratto di Kbis, meno di tre mesi;
- Stati aggiornati, datati e firmati;
- un dettaglio per cliente dei ricavi totali dell'ultimo anno concluso o dall'inizio dell'anno fiscale per le prime applicazioni, categorizzato per categorie, media e non media (media, istituzioni e aziende, altri). Questo elenco, redatto dal manager della società, deve essere certificato da un contabile per le aziende con un fatturato superiore a 250.000 euro;
- Una nota di presentazione aziendale sulla natura delle sue attività e delle sue prospettive di sviluppo;
- Il conto economico dell'ultimo anno ha concluso la data di deposito;
- una copia di una busta paga per il mese di riferimento della busta paga dei dipendenti chiave.

**Costo**

Gratuito.

**Rimedi**

Se ti rifiuti di registrarti nell'elenco delle agenzie di stampa, al richiedente viene inviata una lettera di rifiuto motivata. Il richiedente può quindi richiedere un riesame della sua domanda presentando un nuovo fascicolo, conformemente alle disposizioni dell'ordinanza del 2 novembre 1945 di cui sopra. Il CPPAP rivede questa nuova applicazione al prossimo incontro.

Inoltre, il richiedente può anche contestare il rifiuto del CPPAP:

- o ex gratia presentare ricorso al Presidente del CPPAP, entro due mesi dalla notifica del rifiuto di registrare o rinnovare la registrazione;
- o presentando una contestazione legale al Tribunale amministrativo, entro due mesi dalla notifica del rifiuto di registrare o rinnovare la registrazione. Questo periodo viene prorogato se in anticipo è stato avviato un ricorso grazioso.

