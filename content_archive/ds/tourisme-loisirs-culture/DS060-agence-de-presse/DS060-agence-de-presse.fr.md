﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS060" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Agence de presse" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="agence-de-presse" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/agence-de-presse.html" -->
<!-- var(last-update)="2020-04-15 17:24:14" -->
<!-- var(url-name)="agence-de-presse" -->
<!-- var(translation)="None" -->


# Agence de presse

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:24:14<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’agence de presse est une société commerciale qui collecte, traite, met en forme et fournit à titre professionnel tous les éléments d’information ayant fait l’objet, sous sa responsabilité éditoriale, d’un traitement journalistique. Son chiffre d’affaires doit provenir au moins pour moitié de la fourniture de ces éléments à des clients médias (publication de presse, stations de radio, chaînes de télévision, sites internet, etc.).

Les agences inscrites sur la liste établie par la [Commission paritaire des publications et des agences de presse](http://www.cppap.fr/) (CPPAP) sont les seules à pouvoir se prévaloir de l’appellation « agence de presse ».

**À savoir**

Avant de présenter une demande d’inscription sur la liste établie par la CPPAP, l’entreprise doit avoir fonctionné au moins six mois. Ce délai d’exercice permet d’analyser le chiffre d’affaires réalisé, afin de vérifier qu’au moins la moitié de celui-ci provient de clients médias.

*Pour aller plus loin* : article 1er de l’ordonnance n° 45-2646 du 2 novembre 1945 portant réglementation des agences de presse.

### b. Centre de formalités des entreprises compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de la structure dans laquelle l’activité est exercée. Dans le cas d’une agence de presse, il s’agit d’une activité commerciale. Le CFE compétent est donc la chambre de commerce et d’industrie (CCI).

Pour plus d’informations, il est conseillé de consulter [le site de la CCI Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites).

## 2°. Règlementation de l’activité

#### Avantages économiques accordés aux agences de presse

Les agences de presse bénéficient de certains avantages économiques, dont notamment :

* l’exonération de la cotisation foncière des entreprises (article 1458-2° du Code général des impôts) ;
* l’application d’un taux réduit de TVA sur la cession d’éléments d’information aux médias (article 298 octies du Code général des impôts) ;
* l’allègement de charges sociales spécifiques à la profession (arrêté du 26 mars 1987 fixant l’abattement applicable au taux des cotisations de sécurité sociale dues pour l’emploi de certaines catégories de journalistes) ;
* la possibilité de recevoir des subventions du [fonds stratégique pour le développement de la presse](http://www.culturecommunication.gouv.fr/Politiques-ministerielles/Presse/Aides-a-la-presse/Le-fonds-strategique-pour-le-developpement-de-la-presse-aides-directes/1.-Presentation-du-Fonds-strategique-pour-le-developpement-de-la-presse2).

#### Interdiction de participation de fonds étrangers

Il est interdit à toute agence de presse, ou à l'un de ses collaborateurs, de recevoir directement ou indirectement des fonds ou avantages d'un gouvernement étranger (exception faite des fonds reçus en contrepartie d’une prestation).

*Pour aller plus loin* : article 5 de l’ordonnance du 2 novembre 1945 précitée et article 8 de la loi du 1er août 1986 précitée.

#### Interdiction de recevoir ou de se faire promettre une somme d’argent ou tout autre avantage

Il est interdit à toute agence de presse, ou à l'un de ses collaborateurs, de recevoir ou de se faire promettre une somme d'argent, ou tout autre avantage, aux fins de travestir en information de la publicité financière. Tout article de publicité à présentation rédactionnelle doit être précédé de la mention « publicité » ou « communiqué ».

*Pour aller plus loin* : article 5 de l’ordonnance du 2 novembre 1945 précitée et article 10 de la loi du 1er août 1986 précitée.

#### Obligation d’information lors d’une cession

L’agence de presse doit informer :

* de toute cession ou promesse de cession de ses droits sociaux ayant pour effet de donner au cessionnaire au moins un tiers du capital social ou des droits de vote ;
* de tout transfert ou promesse de transfert de la propriété ou de l’exploitation d’un titre de publication de presse ou d’un service de presse en ligne.

*Pour aller plus loin* : article 6 de l’ordonnance du 2 novembre 1945 précitée et article 6 de la loi du 1er août 1986 précitée.

#### Direction de la publication

Seule peut être désignée en qualité de directeur de la publication la personne qui est propriétaire, locataire-gérante ou qui détient la majorité du capital social ou des droits de vote de l’agence de presse.

De plus, si le directeur de la publication jouit de l’immunité parlementaire, il est obligatoire de nommer un co-directeur au sein de l’agence de presse.

Le directeur (et le co-directeur, le cas échéant) doit être majeur, disposer de ses droits civils et jouir de ses droits civiques.

*Pour aller plus loin* : article 6 de la loi du 29 juillet 1881 sur la liberté de la presse ; article 9 de la loi du 1er août 1986 précitée ; article 2 de l’ordonnance du 2 novembre 1945 précitée.

#### Interdiction de toute publicité et gratuité

L’agence de presse a l’interdiction de :

* se livrer à de toute forme de publicité en faveur des tiers ;
* de fournir gratuitement des éléments d’information à des entreprises éditrices de publications de presse, à des éditeurs de services de communication au public par voie électronique ou à des agences de presse.

*Pour aller plus loin* : article 3 de l’ordonnance du 2 novembre 1945 précitée.

#### Respecter les règles d’accessibilité et de sécurité

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

* en matière d’incendie ;
* en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche activité [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

**Bon à savoir**

Les contrevenants à l’une de ces prescriptions encourent une condamnation à une amende de 6 000 euros et/ou à une peine de six mois d’emprisonnement (article 9 de l’ordonnance du 2 novembre 1945 précitée).

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise. Pour plus d’informations, il est conseillé de se reporter à la fiche activité « Formalités de déclaration d’une société commerciale ».

### b. Demander l’inscription sur la liste des agences de presse

Seules les agences inscrites sur la liste établie par la CPPAP peuvent se prévaloir de l’appellation « agence de presse ». Le cas échéant, l’inscription est accordée pour une durée maximale de cinq ans, renouvelable. En revanche, la première inscription ne peut être accordée que pour une durée de deux ans maximum.

**Autorité compétente**

La demande d’inscription est adressée au secrétariat de la CPPAP.

**Procédure**

La CPPAP enregistre la demande d’inscription et procède à la vérification du dossier d’inscription. Elle peut diligenter, une enquête sur place, auprès de la société, afin de confirmer ou de préciser certains renseignements figurant dans le dossier. Après instruction, la CPPAP émet un avis. S’il est favorable à l’inscription, un arrêté interministériel est proposé à la signature des ministres en charge de la communication et du budget avant d’être publié au Journal officiel.

**Délais**

À titre indicatif, deux à quatre mois sont nécessaires pour obtenir l’inscription ou le renouvellement de l’inscription sur la liste des agences de presse. De plus, la publication de l’arrêté interministériel au Journal officiel nécessite un délai de six à huit mois environ à compter de l’avis favorable pour l’inscription. L’inscription prend effet au lendemain de la parution au Journal officiel.

**Pièces justificatives**

La demande d’inscription doit être accompagnée des pièces suivantes :

* le formulaire Cerfa 12405*03 complété, daté et signé ;
* un extrait Kbis, datant de moins de trois mois ;
* les statuts à jour, datés et signés ;
* un détail par clients du chiffre d'affaires total correspondant au dernier exercice clos ou depuis le début de l'exercice pour les premières demandes, classé par catégories, médias et hors-médias (médias, institutionnels et entreprises, autres). Cette liste, établie par le gérant de la société, doit être certifiée par un expert-comptable pour les entreprises disposant d'un chiffre d'affaires supérieur à 250 000 euros ;
* une note de présentation de l'entreprise précisant la nature de ses activités et ses perspectives d'évolution ;
* le compte de résultat du dernier exercice clos à la date de dépôt du dossier ;
* la copie d'un bulletin de salaire du mois de référence de la masse salariale des principaux collaborateurs.

**Coût**

Gratuit.

**Voies de recours**

En cas de refus d’inscription sur la liste des agences de presse, un courrier de refus motivé est adressé au demandeur. Le demandeur peut alors solliciter un nouvel examen de sa demande en déposant un nouveau dossier, conforme aux dispositions de l’ordonnance du 2 novembre 1945 précitée. La CPPAP examine cette nouvelle demande dès la séance suivante.

Par ailleurs, le demandeur peut également contester le refus de la CPPAP en exerçant :

* soit recours gracieux auprès du président de la CPPAP, dans un délai de deux mois suivant la notification du refus d’inscription ou de renouvellement d’inscription ;
* soit en exerçant un recours contentieux auprès du tribunal administratif, dans un délai de deux mois suivant la notification du refus d’inscription ou de renouvellement d’inscription. Ce délai est prorogé si un recours gracieux a été préalablement initié.