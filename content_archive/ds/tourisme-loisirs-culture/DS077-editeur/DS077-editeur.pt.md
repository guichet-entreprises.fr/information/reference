﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS077" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Lazer, Cultura" -->
<!-- var(title)="Editor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-lazer-cultura" -->
<!-- var(title-short)="editor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/pt/ds/turismo-lazer-cultura/editor.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="editor" -->
<!-- var(translation)="Auto" -->


Editor
======

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

### a. Definição

A editora, seja individual ou jurídica, é responsável por examinar obras que sejam literárias, técnicas ou musicais. Ele garante a impressão, a reprodução em uma série de cópias e a distribuição em pontos de venda.

O profissional troca constantemente com os autores no contexto da correção das obras, sua promoção ou a gestão de prazos. Também garantirá sua rentabilidade comercial.

### b. CFE competente

A CFE relevante depende da natureza da estrutura em que a atividade é realizada:

- para empresas comerciais, é a Câmara de Comércio e Indústria (CCI);
- para as sociedades civis, este é o registro do Tribunal Comercial;
- para sociedades civis nos departamentos do Reno Inferior, Alto Reno e Mosela, este é o registro do tribunal distrital.

**É bom saber**

A atividade é considerada comercial desde que a empresa tenha mais de dez funcionários (exceto no Reno Inferior, Alto Reno e Moselle, onde a atividade permanece artesanal, independentemente do número de funcionários da empresa desde que ela não utiliza um processo industrial). Por outro lado, se a empresa possui dez ou menos funcionários, sua atividade é considerada artesanal. Por fim, se o profissional tem uma atividade de compra e revenda, sua atividade é artesanal e comercial.

Dois graus. Condições de instalação
-----------------------------------

### a. Qualificações profissionais

Não é necessário diploma específico para o uso da profissão de editor. No entanto, é aconselhável que o indivíduo tenha um sólido conhecimento de negócios e contabilidade, mas também conheça bem o mercado editorial.

### b. Qualificações Profissionais - Cidadãos da UE ou EEE (Serviço Livre ou Estabelecimento Livre)

Um cidadão de um Estado-Membro da União Europeia (UE) ou uma parte da Área Econômica Europeia (EEE) não está sujeito a quaisquer requisitos de qualificação ou certificação, assim como os franceses.

### c. Algumas peculiaridades da regulação da atividade

#### Obrigação de definir o preço dos livros

A editora é obrigada a definir o preço de venda dos livros que oferece ao público. Quando a venda é concluída através de um varejista, o preço de venda ao público deve ser entre 95% e 100% do preço definido pelo editor, sem nunca ser menor.

*Para ir mais longe* Ato 81-766 de 10 de agosto de 1981 sobre o preço do livro.

#### Obrigação de celebrar um contrato de publicação

Se um editor deseja publicar uma obra, deve primeiro assinar um contrato com seu autor pelo qual o autor cederá o direito de fazer cópias da obra.

O contrato deve incluir uma série de cláusulas obrigatórias, incluindo:

- O número mínimo de cópias para a primeira tiragem;
- Condições de fabricação
- Remuneração do autor;
- cada direito transferido, bem como a extensão e duração de sua operação.

O editor deve publicar o livro dentro de um período razoável de tempo (geralmente três meses) o risco de ter o contrato rescindido.

Como parte de seu contrato de publicação, a editora garantirá a disponibilidade de cópias em papel, a presença no catálogo digital ou reedições, bem como a apresentação ao autor dos resultados do livro na França e país estrangeiro.

Três graus. Procedimentos de instalação e formalidades
------------------------------------------------------

### a. Formalidades de relatórios da empresa

Dependendo da natureza do negócio, o empreendedor deve se cadastrar no Registro de Comércio e Artesanato (RMA) ou no Registro Comercial e Corporativo (RCS). É aconselhável consultar as "Formalidades de Relatório de Empresas Comerciais" e "Registro de uma Empresa Individual Comercial no RCS" para obter mais informações.

### b. Obrigação de depósito legal

A editora deve fazer o arquivamento legal das obras a serem publicadas pela Biblioteca Nacional da França, pelo Centro Nacional de Cinema e pelo Instituto Nacional do Audiovisual, de acordo com a publicação. Este arquivamento deve ocorrer no mais tarde do dia em que a obra for colocada em circulação.

Ele é acompanhado por uma declaração de três cópias[Menciona](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=F1F4957223AE6D5E109A05CBDACD5FE0.tplgfr26s_2?idArticle=LEGIARTI000031173581&cidTexte=LEGITEXT000005617545&dateTexte=20180201) dependem do tipo de trabalho publicado.

Para o arquivamento de publicações digitais, o procedimento é diferente do depósito legal de livros, em particular. Deve ser feito de acordo com o[Termos](http://www.bnf.fr/fr/professionnels/depot_legal/a.dl_sites_web_mod.html) sites que fornecem coleta automática usando software de computador gratuito.

*Para ir mais longe* Artigos L. 131-1 e L. 131-2, e Artigos R. 131-1 ao R. 131-7 do Código patrimonial; Artigo 2º do decreto de 12 de janeiro de 1995 que estabelece as referências obrigatórias sobre as declarações que acompanham o arquivamento legal de documentos impressos, gráficos e fotográficos.

