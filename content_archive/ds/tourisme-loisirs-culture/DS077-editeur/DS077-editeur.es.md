﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS077" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Editor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="editor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/editor.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="editor" -->
<!-- var(translation)="Auto" -->


Editor
======

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

### a. Definición

El editor, ya sean individuales o legales, es responsable de examinar las obras literarias, técnicas o musicales. Garantiza la impresión, reproducción en una serie de copias y distribución en puntos de venta.

El profesional intercambia constantemente con los autores en el contexto de la corrección de las obras, su promoción o la gestión de plazos. También garantizará su rentabilidad comercial.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

Dos grados. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere un diploma específico para el ejercicio de la profesión de editor. Sin embargo, es aconsejable que el individuo tenga un sólido conocimiento de negocio y contabilidad, pero también conozca bien el mercado editorial.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o una parte en el Espacio Económico Europeo (EEE) no está sujeto a ningún requisito de calificación o certificación, al igual que los franceses.

### c. Algunas peculiaridades de la regulación de la actividad

#### Obligación de fijar el precio de los libros

El editor está obligado a fijar el precio de venta de los libros que ofrece al público. Cuando la venta se completa a través de un minorista, el precio de venta al público debe estar entre el 95% y el 100% del precio establecido por el editor, sin ser nunca más bajo.

*Para ir más allá* Ley 81-766 de 10 de agosto de 1981 sobre el precio del libro.

#### Obligación de celebrar un contrato de publicación

Si un editor desea publicar una obra, primero debe celebrar un contrato con su autor mediante el cual el autor cederá el derecho a hacer copias de la obra.

El contrato debe incluir una serie de cláusulas obligatorias, entre las que se incluyen:

- El número mínimo de copias para la primera tirada de impresión;
- Condiciones de fabricación
- La remuneración del autor;
- cada derecho transferido, así como la extensión y duración de su funcionamiento.

El editor debe publicar el libro dentro de un período de tiempo razonable (generalmente tres meses) a riesgo de que se rescinda el contrato.

Como parte de su contrato de publicación, el editor garantizará la disponibilidad de copias en papel, la presencia en el catálogo digital o reimpresiones, así como la presentación al autor de los resultados del libro en Francia y país extranjero.

Tres grados. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Obligación de depósito legal

El editor deberá realizar la presentación legal de las obras que deberán ser publicadas por la Biblioteca Nacional de Francia, el Centro Nacional de Cine y el Instituto Audiovisual Nacional según la publicación. Esta presentación debe tener lugar a más tardar el día en que se ponga en circulación el trabajo.

Se acompaña de una declaración de tres copias[Menciona](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=F1F4957223AE6D5E109A05CBDACD5FE0.tplgfr26s_2?idArticle=LEGIARTI000031173581&cidTexte=LEGITEXT000005617545&dateTexte=20180201) depende del tipo de trabajo publicado.

Para la presentación de publicaciones digitales, el procedimiento es diferente del depósito legal de libros, en particular. Debe hacerse de acuerdo con el[Términos](http://www.bnf.fr/fr/professionnels/depot_legal/a.dl_sites_web_mod.html) sitios web que proporcionan la recopilación automática utilizando software informático gratuito.

*Para ir más allá* Artículos L. 131-1 y L. 131-2, y artículos R. 131-1 a R. 131-7 del Código del Patrimonio; Artículo 2 del Decreto de 12 de enero de 1995 por el que se establecen las referencias obligatorias sobre las declaraciones que acompañan a la presentación legal de documentos impresos, gráficos y fotográficos.

