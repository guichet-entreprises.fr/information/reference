﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS077" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourismus, Freizeit, Kultur" -->
<!-- var(title)="Verlag" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourismus-freizeit-kultur" -->
<!-- var(title-short)="verlag" -->
<!-- var(url)="https://www.guichet-entreprises.fr/de/ds/tourismus-freizeit-kultur/verlag.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="verlag" -->
<!-- var(translation)="Auto" -->


Verlag
======

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

### a. Definition

Der Verlag ist, ob individuell oder rechtlich, für die Prüfung literarischer, technischer oder musikalischer Werke verantwortlich. Es gewährleistet den Druck, die Reproduktion in einer Reihe von Kopien und den Vertrieb in Einzelhandelsgeschäften.

Der Fachmann tauscht sich ständig mit den Autoren im Zusammenhang mit der Korrektur der Werke, ihrer Förderung oder der Verwaltung von Fristen aus. Sie wird auch ihre wirtschaftliche Rentabilität sichern.

### b. zuständigeCFE

Die betreffende CFE hängt von der Art der Struktur ab, in der die Tätigkeit ausgeübt wird:

- für Handelsunternehmen ist es die Industrie- und Handelskammer (IHK);
- für Zivilgesellschaften ist dies das Register des Handelsgerichts;
- für die Zivilgesellschaften in den Abteilungen Niederrhein, Oberrhein und Mosel ist dies das Register des Amtsgerichts.

**Gut zu wissen**

Die Tätigkeit gilt als kaufmännisch, solange das Unternehmen mehr als zehn Mitarbeiter beschäftigt (außer am Niederrhein, am Oberrhein und an der Mosel, wo die Tätigkeit unabhängig von der Anzahl der Mitarbeiter des Unternehmens handwerklich bleibt, sofern es verwendet kein industrielles Verfahren). Auf der anderen Seite, wenn das Unternehmen zehn oder weniger Mitarbeiter hat, wird seine Tätigkeit als handwerklich betrachtet. Schließlich, wenn der Profi eine Kauf- und Wiederverkaufstätigkeit hat, ist seine Tätigkeit sowohl handwerklich als auch kommerziell.

Zwei Grad. Installationsbedingungen
-----------------------------------

### a. Berufliche Qualifikationen

Für die Ausübung des Verlegerberufs ist kein spezifisches Diplom erforderlich. Es ist jedoch ratsam, dass der Einzelne über fundierte Kenntnisse in Wirtschaft und Rechnungswesen verfügt, aber auch den Verlagsmarkt gut kennt.

### b. Berufsqualifikationen - EU- oder EWR-Bürger (Freier Dienst oder freie Niederlassung)

Ein Staatsangehöriger eines Mitgliedstaats der Europäischen Union (EU) oder eine Partei des Europäischen Wirtschaftsraums (EWR) unterliegt ebenso wenig den Qualifikations- oder Zertifizierungsanforderungen wie die Franzosen.

### c. Einige Besonderheiten der Regelung der Tätigkeit

#### Verpflichtung zur Preisbesamkeit von Büchern

Der Verlag ist verpflichtet, den Verkaufspreis der Bücher, die er der Öffentlichkeit anbietet, festzulegen. Wenn der Verkauf über einen Einzelhändler abgeschlossen wird, sollte der Verkaufspreis für die Öffentlichkeit zwischen 95 % und 100 % des vom Verlag festgesetzten Preises liegen, ohne jemals niedriger zu sein.

*Um weiter zu gehen* Gesetz 81-766 vom 10. August 1981 über den Preis des Buches.

#### Verpflichtung zum Abschluss eines Verlagsvertrags

Will ein Verlag ein Werk veröffentlichen, muss er zunächst mit seinem Autor einen Vertrag abschließen, durch den der Autor das Recht abtritt, Kopien des Werkes anzufertigen.

Der Vertrag muss eine Reihe von zwingenden Klauseln enthalten, darunter:

- Die Mindestanzahl von Kopien für die erste Druckauflage;
- Herstellungsbedingungen
- Die Vergütung des Autors;
- jedes übertragene Recht sowie Umfang und Dauer seines Betriebs.

Der Herausgeber muss das Buch innerhalb einer angemessenen Frist (in der Regel drei Monate) auf Die Gefahr der Vertragsauflösung veröffentlichen.

Im Rahmen seines Veröffentlichungsvertrags wird der Verlag die Verfügbarkeit von Papierkopien, die Präsenz im digitalen Katalog oder die Nachdrucke sowie die Präsentation der Ergebnisse des Buches in Frankreich an den Autor sicherstellen und ausland.

Drei Grad. Installationsverfahren und Formalitäten
--------------------------------------------------

### a. Meldeformalitäten des Unternehmens

Je nach Art des Unternehmens muss sich der Unternehmer im Handwerksregister (RMA) oder im Handels- und Unternehmensregister (RCS) anmelden. Es ist ratsam, sich auf die "Commercial Company Reporting Formalities" und "Registration of a Commercial Individual Company at the RCS" für weitere Informationen zu beziehen.

### b. Gesetzliche Pfandpflicht

Der Verlag muss die rechtliche Einreichung von Werken vornehmen, die von der Nationalbibliothek Frankreichs, dem Nationalen Filmzentrum und dem Nationalen Audiovisuellen Institut veröffentlicht werden sollen, entsprechend der Veröffentlichung. Diese Anmeldung muss spätestens am Tag der Inbetriebnahme der Arbeiten erfolgen.

Sie wird von einer Erklärung aus drei Kopien begleitet, die[Erwähnt](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=F1F4957223AE6D5E109A05CBDACD5FE0.tplgfr26s_2?idArticle=LEGIARTI000031173581&cidTexte=LEGITEXT000005617545&dateTexte=20180201) hängt von der Art der veröffentlichten Arbeit ab.

Bei der Einreichung digitaler Veröffentlichungen unterscheidet sich das Verfahren insbesondere von der gesetzlichen Hinterlegung von Büchern. Es muss nach den[Bedingungen](http://www.bnf.fr/fr/professionnels/depot_legal/a.dl_sites_web_mod.html) Websites, die eine automatische Erfassung mit freier Computersoftware ermöglichen.

*Um weiter zu gehen* Artikel L. 131-1 und L. 131-2 und Artikel R. 131-1 bis R. 131-7 des Kulturerbegesetzbuches; Artikel 2 des Dekrets vom 12. Januar 1995, in dem die obligatorischen Verweise auf die Erklärungen zur rechtlichen Einreichung gedruckter, grafischer und fotografischer Dokumente enthalten sind.

