﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS077" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Tempo libero, Cultura" -->
<!-- var(title)="Editore" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-tempo-libero-cultura" -->
<!-- var(title-short)="editore" -->
<!-- var(url)="https://www.guichet-entreprises.fr/it/ds/turismo-tempo-libero-cultura/editore.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="editore" -->
<!-- var(translation)="Auto" -->


Editore
=======

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

### a. Definizione

L'editore, individuale o legale, è responsabile dell'esame di opere letterarie, tecniche o musicali. Garantisce la stampa, la riproduzione in un certo numero di copie e la distribuzione nei punti vendita al dettaglio.

Il professionista si scambia costantemente con gli autori nel contesto della correzione delle opere, della loro promozione o della gestione delle scadenze. Garantirà anche la loro redditività commerciale.

### b. CFE competente

Il CFE in questione dipende dalla natura della struttura in cui viene svolta l'attività:

- per le imprese commerciali, è la Camera di Commercio e Industria (CCI);
- per le società civili, questo è il registro del Tribunale commerciale;
- per le società civili nei dipartimenti del Basso Reno, Alto Reno e Mosella, questo è il registro del tribunale distrettuale.

**Buono a sapersi**

L'attività è considerata commerciale fintanto che l'azienda ha più di dieci dipendenti (ad eccezione del Basso Reno, dell'Alto Reno e della Mosella, dove l'attività rimane artigianale indipendentemente dal numero di dipendenti dell'azienda, a condizione che non utilizza un processo industriale). D'altra parte, se l'azienda ha dieci o meno dipendenti, la sua attività è considerata artigianale. Infine, se il professionista ha un'attività di acquisto e rivendita, la sua attività è sia artigianale che commerciale.

Due gradi. Condizioni di installazione
--------------------------------------

### a. Qualifiche professionali

Non è richiesto alcun diploma specifico per la pratica della professione di editore. Tuttavia, è consigliabile per l'individuo avere una solida conoscenza del business e della contabilità, ma anche conoscere bene il mercato dell'editoria.

### b. Qualifiche professionali - Cittadini dell'UE o del CEE (Servizio gratuito o Stabilimento gratuito)

Un cittadino di uno Stato membro dell'Unione europea (UE) o di un partito allo Spazio economico europeo (AEA) non è soggetto ad alcun requisito di qualificazione o certificazione, così come i francesi.

### c. Alcune peculiarità della regolamentazione dell'attività

#### Obbligo di fissare il prezzo dei libri

L'editore è tenuto a fissare il prezzo di vendita dei libri che offre al pubblico. Quando la vendita viene completata attraverso un rivenditore, il prezzo di vendita al pubblico dovrebbe essere tra il 95% e il 100% del prezzo fissato dall'editore, senza mai essere inferiore.

*Per andare oltre* Atto 81-766 del 10 agosto 1981 sul prezzo del libro.

#### Obbligo di stipulare un contratto editoriale

Se un editore desidera pubblicare un'opera, deve prima stipulare un contratto con il suo autore con il quale l'autore cederà il diritto di fare copie dell'opera.

Il contratto deve includere una serie di clausole obbligatorie, tra cui:

- Il numero minimo di copie per la prima tiratura;
- Condizioni di produzione
- La remunerazione dell'autore;
- ogni diritto trasferito, nonché l'estensione e la durata del suo funzionamento.

L'editore deve pubblicare il libro entro un periodo di tempo ragionevole (di solito tre mesi) con il rischio di che il contratto sia risolto.

Nell'ambito del contratto editoriale, l'editore garantirà la disponibilità di copie cartacee, la presenza nel catalogo digitale o ristampe, nonché la presentazione all'autore dei risultati del libro in Francia e paese straniero.

Tre gradi. Procedure di installazione e formalità
-------------------------------------------------

### a. Le formalità di rendicontazione delle società

A seconda della natura dell'attività, l'imprenditore deve registrarsi al Registro dei mestieri e dell'artigianato (RMA) o al Registro del commercio e delle imprese (RCS). Si consiglia di fare riferimento alle "Commercial Company Reporting Formalities" e "Registration of a Commercial Individual Company presso l'RCS" per ulteriori informazioni.

### b. Obbligo di deposito legale

L'editore deve effettuare il deposito legale delle opere che saranno pubblicate dalla Biblioteca Nazionale di Francia, dal Centro Nazionale del Cinema e dall'Istituto Nazionale di Audiovisivo secondo la pubblicazione. Tale deposito deve avvenire entro e non oltre il giorno in cui il lavoro viene messo in circolazione.

È accompagnato da una dichiarazione di tre copie[Cita](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=F1F4957223AE6D5E109A05CBDACD5FE0.tplgfr26s_2?idArticle=LEGIARTI000031173581&cidTexte=LEGITEXT000005617545&dateTexte=20180201) dipende dal tipo di lavoro pubblicato.

Per il deposito di pubblicazioni digitali, la procedura è diversa dal deposito legale dei libri, in particolare. Deve essere fatto secondo il[Termini](http://www.bnf.fr/fr/professionnels/depot_legal/a.dl_sites_web_mod.html) siti web che forniscono la raccolta automatica utilizzando software libero del computer.

*Per andare oltre* Articoli L. 131-1 e L. 131-2 e Articoli da 131-1 a R. 131-7 del Codice Patrimonio; Articolo 2 del decreto del 12 gennaio 1995 che illustra i riferimenti obbligatori sulle dichiarazioni che accompagnano il deposito legale di documenti stampati, grafici e fotografici.

