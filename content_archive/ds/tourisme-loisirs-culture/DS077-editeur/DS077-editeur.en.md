﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS077" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Publisher" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="publisher" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/publisher.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="publisher" -->
<!-- var(translation)="Auto" -->


Publisher
=========

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Defining the activity
------------------------

### a. Definition

The publisher, whether individual or legal, is responsible for examining works that are literary, technical or musical. It ensures the printing, reproduction in a number of copies, and distribution in retail outlets.

The professional constantly exchanges with the authors in the context of the correction of the works, their promotion or the management of deadlines. It will also ensure their commercial profitability.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

Two degrees. Installation conditions
------------------------------------

### a. Professional qualifications

No specific diploma is required for the practice of the profession of publisher. However, it is advisable for the individual to have a solid knowledge of business and accounting, but also to know the publishing market well.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Free Establishment)

A national of a Member State of the European Union (EU) or a party to the European Economic Area (EEA) is not subject to any qualification or certification requirements, as are the French.

### c. Some peculiarities of the regulation of the activity

#### Obligation to set the price of books

The publisher is required to set the selling price of the books it offers to the public. When the sale is completed through a retailer, the sale price to the public should be between 95% and 100% of the price set by the publisher, without ever being lower.

*To go further* Act 81-766 of August 10, 1981 on the price of the book.

#### Obligation to enter into a publishing contract

If a publisher wishes to publish a work, it must first enter into a contract with its author by which the author will cede the right to make copies of the work.

The contract must include a number of mandatory clauses, including:

- The minimum number of copies for the first print run;
- Manufacturing conditions
- The author's remuneration;
- each right transferred as well as the extent and duration of its operation.

The publisher must publish the book within a reasonable period of time (usually three months) at the risk of having the contract terminated.

As part of its publishing contract, the publisher will ensure the availability of paper copies, the presence in the digital catalogue or reprints, as well as the presentation to the author of the results of the book in France and foreign country.

Three degrees. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Legal deposit obligation

The publisher must make the legal filing of works to be published by the National Library of France, the National Film Centre and the National Audiovisual Institute according to the publication. This filing must take place no later than the day the work is put into circulation.

It is accompanied by a three-copy statement[Mentions](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=F1F4957223AE6D5E109A05CBDACD5FE0.tplgfr26s_2?idArticle=LEGIARTI000031173581&cidTexte=LEGITEXT000005617545&dateTexte=20180201) depend on the type of work published.

For the filing of digital publications, the procedure is different from the legal deposit of books, in particular. It must be done according to the[Terms](http://www.bnf.fr/fr/professionnels/depot_legal/a.dl_sites_web_mod.html) websites that provide automatic collection using free computer software.

*To go further* Articles L. 131-1 and L. 131-2, and Articles R. 131-1 to R. 131-7 of the Heritage Code; Article 2 of the decree of 12 January 1995 setting out the mandatory references on the declarations accompanying the legal filing of printed, graphic and photographic documents.

