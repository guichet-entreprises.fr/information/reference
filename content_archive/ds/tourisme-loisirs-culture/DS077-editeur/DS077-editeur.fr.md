﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS077" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Editeur" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="editeur" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/editeur.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="editeur" -->
<!-- var(translation)="None" -->


# Editeur

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'éditeur, personne physique ou morale, a pour mission d'examiner des œuvres aussi bien littéraires que techniques ou musicales. Il en assure l'impression, la reproduction en un certain nombre d'exemplaires, et la diffusion dans des points de vente.

Le professionnel échange en permanence avec les auteurs dans le cadre de la correction des œuvres, de leur promotion ou encore de la gestion des délais. Il s'assurera également de leur rentabilité commerciale.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

* pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
* pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
* pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucun diplôme spécifique n'est requis pour l'exercice de la profession d'éditeur. Toutefois, il est conseillé à l'intéressé d'avoir de solides connaissances en matière commerciale et comptable, mais aussi de bien connaître le marché de l'édition.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE) n'est soumis à aucune condition de diplôme ou de certification, au même titre que les Français.

### c. Quelques particularités de la réglementation de l'activité

#### Obligation de fixer le prix des livres

L'éditeur est tenu de fixer le prix de vente des livres qu'il propose au public. Lorsque la vente se conclu par le biais d'un détaillant, le prix de vente au public devra être compris entre 95 % et 100 % du prix fixé par l'éditeur, sans jamais être inférieur.

*Pour aller plus loin* : loi n° 81-766 du 10 août 1981 relative au prix du livre.

#### Obligation de conclure un contrat d'édition

Dès lors qu'un éditeur souhaite éditer une œuvre, il doit au préalable conclure un contrat avec son auteur par lequel ce dernier cédera le droit de fabriquer des exemplaires de l’œuvre.

Le contrat devra comporter un certain nombre de clauses obligatoires, et notamment :

* le nombre minimal d'exemplaires pour le premier tirage ;
* les conditions de fabrication ;
* la rémunération de l'auteur ;
* chaque droit cédé ainsi que l'étendue et la durée de son exploitation.

L'éditeur devra publier l'ouvrage dans un délai raisonnable (généralement trois mois) au risque de voir le contrat résilié.

Dans le cadre de son contrat d'édition, l'éditeur s'assurera de la disponibilité des exemplaires papiers, de la présence dans le catalogue numérique ou encore des réimpressions, ainsi que de la présentation à l'auteur des résultats de l'ouvrage en France et à l'étranger.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS). Il est conseillé de se reporter aux fiches « Formalités de déclaration d'une société commerciale » et « Immatriculation d'une entreprise individuelle commerciale au RCS » pour de plus amples informations.

### b. Obligation de dépôt légal

L'éditeur doit procéder au dépôt légal des œuvres à paraître auprès de la Bibliothèque nationale de France, du Centre national du cinéma et de l'Institut national de l'audiovisuel selon la publication. Ce dépôt doit intervenir au plus tard le jour de la mise en circulation de l’œuvre.

Il est accompagné d'une déclaration établie en trois exemplaires dont les [mentions](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=F1F4957223AE6D5E109A05CBDACD5FE0.tplgfr26s_2?idArticle=LEGIARTI000031173581&cidTexte=LEGITEXT000005617545&dateTexte=20180201) dépendent du type d'œuvre édité.

Pour le dépôt des publications numériques, la procédure est différente du dépôt légal des livres, notamment. Elle doit être faite selon les [modalités](http://www.bnf.fr/fr/professionnels/depot_legal/a.dl_sites_web_mod.html) propres aux sites web qui prévoient une collecte automatique à l'aide d'un logiciel informatique libre.

*Pour aller plus loin* : articles L. 131-1 et L. 131-2, et articles R. 131-1 à R. 131-7 du Code du patrimoine ; article 2 de l'arrêté du 12 janvier 1995 fixant les mentions obligatoires figurant sur les déclarations accompagnant le dépôt légal des documents imprimés, graphiques et photographiques.