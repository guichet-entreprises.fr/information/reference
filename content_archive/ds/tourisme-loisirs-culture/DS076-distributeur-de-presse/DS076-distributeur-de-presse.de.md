﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS076" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourismus, Freizeit, Kultur" -->
<!-- var(title)="Presseverteiler" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourismus-freizeit-kultur" -->
<!-- var(title-short)="presseverteiler" -->
<!-- var(url)="https://www.guichet-entreprises.fr/de/ds/tourismus-freizeit-kultur/presseverteiler.html" -->
<!-- var(last-update)="2020-04-15 17:24:16" -->
<!-- var(url-name)="presseverteiler" -->
<!-- var(translation)="Auto" -->


Presseverteiler
===============

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:24:16<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

### a. Definition

Mehrere Ebenen von Interessenträgern sorgen für die Verbreitung der Presse:

- Pressekuriere sind für den Empfang der Verlagstitel und deren Sortierung und -verteilung verantwortlich;
- Presseverwalter oder Großhändler wiederum verteilen die Titel an Presseanstalten oder Einzelhändler;
- Nachrichtensprecher (Newsagenten und Einzelhändler) sind in den Direktvertrieb an die Öffentlichkeit involviert.

Weitere Informationen zu den verschiedenen Ebenen der Presseverteilung ist es ratsam,[Website des Higher Council of Press Messengers](http://www.csmp.fr/) (CSMP).

### b. zuständigeCFE

Die betreffende CFE hängt von der Art der Struktur ab, in der die Tätigkeit ausgeübt wird:

- für Handelsunternehmen ist es die Industrie- und Handelskammer (IHK);
- für Zivilgesellschaften ist dies das Register des Handelsgerichts;
- für die Zivilgesellschaften in den Abteilungen Niederrhein, Oberrhein und Mosel ist dies das Register des Amtsgerichts.

**Gut zu wissen**

Die Tätigkeit gilt als kaufmännisch, solange das Unternehmen mehr als zehn Mitarbeiter beschäftigt (außer am Niederrhein, am Oberrhein und an der Mosel, wo die Tätigkeit unabhängig von der Anzahl der Mitarbeiter des Unternehmens handwerklich bleibt, sofern es verwendet kein industrielles Verfahren). Auf der anderen Seite, wenn das Unternehmen zehn oder weniger Mitarbeiter hat, wird seine Tätigkeit als handwerklich betrachtet. Schließlich, wenn der Profi eine Kauf- und Wiederverkaufstätigkeit hat, ist seine Tätigkeit sowohl handwerklich als auch kommerziell.

Zwei Grad. Installationsbedingungen
-----------------------------------

### a. Berufliche Qualifikationen

**Pressekurier**

Ein Newsmail-Unternehmen kann nur erstellt werden, wenn mindestens drei Mitarbeiter es bilden. Als solcher wird sein Direktor zu rechtfertigen haben:

- Seien Sie französischer Staatsbürger;
- Be of age
- der Fülle seiner Bürgerrechte und seiner vollen Besitzfähigkeit.

Der Beitrag von Verlagen oder Presseunternehmen zum Sozialkapital eines Pressekurierunternehmens kann nur geleistet werden, wenn sie mit ihr einen Transport-, Gruppierungs- oder Vertriebsvertrag abgeschlossen haben.

Bis heute gibt es zwei Pressenachrichten-Unternehmen in Frankreich: Presstalis und die Lyon Press Messengers (MLP).

*Um weiter zu gehen* Gesetz 47-585 vom 2. April 1947.

**Pressesender**

Die Verwaltung einer Zeitungsausgabe unterliegt der Einhaltung der von den Rathäusern festgelegten Bedingungen, darunter:

- Verkäufer in einem Kiosk oder Pressegeschäft sein
- die beruflichen Fähigkeiten in der Verwaltung eines Kiosks zu rechtfertigen, einschließlich einer angemessenen Berufsausbildung;
- im zweiten Wahlgang des Strafregisters nicht strafrechtlich verurteilt wurden.

### b. Berufsqualifikationen - EU- oder EWR-Bürger (Freier Dienst oder freie Niederlassung)

Ein Staatsangehöriger eines Mitgliedstaats der Europäischen Union (EU) oder eine Vertragspartei des Europäischen Wirtschaftsraums (EWR) unterliegt denselben Bedingungen für berufliche Qualifikationen wie die Franzosen.

### c. Einige Besonderheiten der Regelung der Tätigkeit

#### Beachten Sie gegebenenfalls die allgemeinen Vorschriften für alle öffentlichen Einrichtungen (ERP)

Da die Räumlichkeiten der Öffentlichkeit zugänglich sind, muss der Fachmann die Vorschriften über öffentliche Einrichtungen (ERP) einhalten:

- Feuer
- Zugänglichkeit.

Weitere Informationen finden Sie im Blatt "Öffentliche Aufnahmeeinrichtung (ERP)".

#### Entschädigung für Vertriebsmitarbeiter

Der CSMP legt die Vergütungsbedingungen der Vertriebsmitarbeiter fest. Infolgedessen erhalten sie einen Prozentsatz des der Öffentlichkeit gesetzten Verkaufspreises.

#### Nominierung für den Betrieb von Kiosken

Die Firma Mediakiosk betreibt die Errichtung von Kiosken in Frankreich. Wer sich in der Provinz etablieren möchte, muss zunächst einen Betriebsvertrag mit dem Unternehmen unterzeichnen. Wenn er nach Paris zieht, muss er sich gegebenenfalls bewerben, indem er sich auf einer von Mediakiosk gepflegten Liste anmeldet. Der Bürgermeister von Paris wird sich dann um die Wahl des Kandidaten kümmern.

Drei Grad. Installationsverfahren und Formalitäten
--------------------------------------------------

### a. Meldeformalitäten des Unternehmens

Je nach Art des Unternehmens muss sich der Unternehmer im Handwerksregister (RMA) oder im Handels- und Unternehmensregister (RCS) anmelden. Es ist ratsam, sich auf die "Commercial Company Reporting Formalities" und "Registration of a Commercial Individual Company at the RCS" für weitere Informationen zu beziehen.

### b. Akkreditierung für Verwalter und Presseanstalten

Verwalter und Presseveranstalter müssen von der Netzkommission (CDR), die im Rahmen des CSMP tätig ist, eine Akkreditierung erhalten. Diese Genehmigung ermächtigt diese Agenten des Verkaufs der Presse, Vertriebsverträge mit den Pressekurier-Unternehmen (für die Verwahrer) oder mit den Verwahrstellen (für die Rundfunkanstalten) abzuschließen.

Die CDR erhält die Bewerbungsakte, die die Projektvorlage, die fachlichen Fähigkeiten des Antragstellers, den Standort und das Layout des Depots oder der Verkaufsstelle enthalten muss.

Die Akte wird vom SECRETARIAT der CDR geprüft, das über ihre Ordnungsmäßigkeit entscheidet. Im Falle einer unvollständigen Akte hat der Antragsteller acht Tage Zeit, um die fehlenden Dokumente weiterzugeben.

Nach Erhalt der vollständigen Akte und nach Überprüfung der Exponate kann die CDR entscheiden:

- Akzeptieren Sie die Anfrage
- den Antrag teilweise oder unter Auflagen anzunehmen;
- den Antrag abzulehnen.

Die Person, deren Antrag abgelehnt wurde, kann eine erneute Prüfung beantragen.

*Um weiter zu gehen* Artikel 9 der[interne Regulierung](http://csmp.fr/Menu/Le-CSMP/Documentation/Reglement-interieur) CSMP.

### c. Registrierungspflicht bei den Presseverkäufern

Nach der Genehmigung müssen Verwalter und Rundfunkanstalten die Aufnahme in die Liste der Presseverkäufer der CSMP beantragen.

Sie müssen jeweils eine[Form](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB), die sie entweder ansprechen werden:

- an die Pressemitteilung, wenn die Anfrage vom Verwahrer kommt;
- wenn die Anfrage von einem Sender kommt.

**Kosten**

Die Registrierung auf der Liste wird bezahlt. Die Kosten sind je nach Verteilungsgrad der Presse unterschiedlich. Für weitere Informationen ist es ratsam, sich mit dem[CSMP](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB).

