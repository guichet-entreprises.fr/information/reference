﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS076" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Tempo libero, Cultura" -->
<!-- var(title)="Distributore di stampa" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-tempo-libero-cultura" -->
<!-- var(title-short)="distributore-di-stampa" -->
<!-- var(url)="https://www.guichet-entreprises.fr/it/ds/turismo-tempo-libero-cultura/distributore-di-stampa.html" -->
<!-- var(last-update)="2020-04-15 17:24:16" -->
<!-- var(url-name)="distributore-di-stampa" -->
<!-- var(translation)="Auto" -->


Distributore di stampa
======================

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:24:16<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

### a. Definizione

Diversi livelli di parti interessate garantiscono la distribuzione della stampa:

- I corrieri della stampa sono responsabili della ricezione dei titoli degli editori e della gestione dello smistamento e della distribuzione;
- i custodi della stampa o i grossisti a loro volta distribuiscono i titoli alle emittenti di stampa o ai dettaglianti;
- i giornalisti (giornali e rivenditori) sono coinvolti nella vendita diretta al pubblico.

Per ulteriori informazioni sui diversi livelli di distribuzione della stampa, si consiglia di fare riferimento[sito web del Consiglio Superiore dei Messaggeri Stampa](http://www.csmp.fr/) (CSMP).

### b. CFE competente

Il CFE in questione dipende dalla natura della struttura in cui viene svolta l'attività:

- per le imprese commerciali, è la Camera di Commercio e Industria (CCI);
- per le società civili, questo è il registro del Tribunale commerciale;
- per le società civili nei dipartimenti del Basso Reno, Alto Reno e Mosella, questo è il registro del tribunale distrettuale.

**Buono a sapersi**

L'attività è considerata commerciale fintanto che l'azienda ha più di dieci dipendenti (ad eccezione del Basso Reno, dell'Alto Reno e della Mosella, dove l'attività rimane artigianale indipendentemente dal numero di dipendenti dell'azienda, a condizione che non utilizza un processo industriale). D'altra parte, se l'azienda ha dieci o meno dipendenti, la sua attività è considerata artigianale. Infine, se il professionista ha un'attività di acquisto e rivendita, la sua attività è sia artigianale che commerciale.

Due gradi. Condizioni di installazione
--------------------------------------

### a. Qualifiche professionali

**Corrieri per la stampa**

Una società di newsmail può essere creata solo se almeno tre collaboratori lo compongono. In quanto tale, il suo direttore dovrà giustificare:

- Essere un cittadino francese;
- Essere minorenni
- della pienezza dei suoi diritti civili e di essere in pieno possesso della sua capacità civile.

Il contributo degli editori o delle società di stampa al capitale sociale di una società di corriere della stampa può essere dato solo se hanno stipulato un contratto di trasporto, raggruppamento o distribuzione.

Ad oggi, in Francia esistono due società di messaggistica stampa: Presstalis e Lyon Press Messengers (MLP).

*Per andare oltre* Legge 47-585 del 2 aprile 1947.

**Emittenti di stampa**

La gestione di un salone di giornale è soggetta al rispetto delle condizioni stabilite dai municipi, tra cui:

- Essere un venditore in un chiosco o un negozio di stampa
- giustificare le competenze professionali nella gestione di un chiosco, compreso aver ricevuto un'adeguata formazione professionale;
- non sono stati oggetto di una condanna penale per il secondo scrutinio della fedina penale.

### b. Qualifiche professionali - Cittadini dell'UE o del CEE (Servizio gratuito o Stabilimento gratuito)

Un cittadino di uno Stato membro dell'Unione europea (UE) o di un partito allo Spazio economico europeo (AEA) è soggetto alle stesse condizioni di qualifiche professionali dei francesi.

### c. Alcune peculiarità della regolamentazione dell'attività

#### Se necessario, rispettare le normative generali applicabili a tutte le istituzioni pubbliche (ERP)

Poiché i locali sono aperti al pubblico, il professionista deve rispettare le norme relative alle istituzioni pubbliche (ERP):

- Fuoco
- Accessibilità.

Per ulteriori informazioni, fare riferimento al foglio "Public Receiving Establishment (ERP)".

#### Compensazione per gli agenti di vendita

Il CSMP determina le condizioni remunerazioni degli agenti di vendita. Di conseguenza, riceveranno una percentuale del prezzo di vendita impostato al pubblico.

#### Nomination per il funzionamento dei chioschi

La società Mediakiosk gestisce la creazione di chioschi in Francia. Coloro che sono interessati a stabilirsi nella provincia dovranno prima firmare un contratto operativo con l'azienda. Quando si trasferisce a Parigi, se necessario, dovrà fare domanda iscrivendosi a una lista gestita da Mediakiosk. Il sindaco di Parigi si occuperà poi della scelta del candidato.

Tre gradi. Procedure di installazione e formalità
-------------------------------------------------

### a. Le formalità di rendicontazione delle società

A seconda della natura dell'attività, l'imprenditore deve registrarsi al Registro dei mestieri e dell'artigianato (RMA) o al Registro del commercio e delle imprese (RCS). Si consiglia di fare riferimento alle "Commercial Company Reporting Formalities" e "Registration of a Commercial Individual Company presso l'RCS" per ulteriori informazioni.

### b. Ottenere l'accreditamento per custodi e emittenti stampa

I custodi e le emittenti di stampa devono ottenere l'accreditamento dalla Commissione di rete (CDR) che opera all'interno del CSMP. Questa approvazione autorizza questi agenti della vendita della stampa ad stipulare contratti di distribuzione con le società di corriere (per i custodi) o con i custodi (per le emittenti).

Il CDR riceve il fascicolo di candidatura che deve includere la presentazione del progetto, le competenze professionali del richiedente, la posizione e il layout del deposito o del punto vendita.

Il fascicolo è in fase di revisione da parte del SECRETARIAT del CDR, che deciderà sulla sua regolarità. In caso di file incompleto, il richiedente avrà otto giorni di tempo per trasmettere i documenti mancanti.

Al ricevimento del fascicolo completo e dopo aver esaminato le esposizioni, il CDR può decidere:

- Accettare la richiesta
- accettare la richiesta parzialmente o a condizioni;
- per rifiutare la domanda.

L'individuo, che ha avuto la sua domanda negata, può chiedere una riconsiderazione.

*Per andare oltre* L'articolo 9 del[regolamento interno](http://csmp.fr/Menu/Le-CSMP/Documentation/Reglement-interieur) di CSMP.

### c. Obbligo di registrarsi nel file degli agenti di vendita della stampa

Una volta approvati, i custodi e le emittenti devono richiedere l'inserimento nell'elenco dei agenti di vendita della stampa del CSMP.

Ognuno di essi dovrà compilare un[modulo](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB) che affronteranno:

- alla stampa messaggio quando la richiesta proviene dal custode;
- custode quando la richiesta proviene da un'emittente.

**Costo**

La registrazione nell'elenco è pagata. Il suo costo è diverso a seconda del livello di distribuzione della stampa. Per ulteriori informazioni, si consiglia di verificare con il[CSMP](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB).

