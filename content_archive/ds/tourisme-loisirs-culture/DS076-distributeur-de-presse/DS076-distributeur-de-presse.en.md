﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS076" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Press distributor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="press-distributor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/press-distributor.html" -->
<!-- var(last-update)="2020-04-15 17:24:16" -->
<!-- var(url-name)="press-distributor" -->
<!-- var(translation)="Auto" -->


Press distributor
=================

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:24:16<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Defining the activity
------------------------

### a. Definition

Several levels of stakeholders ensure the distribution of the press:

- Press couriers are responsible for receiving publishers' titles and managing their sorting and distribution;
- press custodians or wholesalers in turn distribute the titles to press broadcasters or retailers;
- newscasters (newsagents and retailers) are involved in direct sales to the public.

For more information on the different levels of press distribution, it is advisable to refer to the[website of the Higher Council of Press Messengers](http://www.csmp.fr/) (CSMP).

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

Two degrees. Installation conditions
------------------------------------

### a. Professional qualifications

**Press couriers**

A newsmail company can only be created if at least three associates make it up. As such, its director will have to justify:

- Be a French national;
- Be of age
- of the fullness of his civil rights and being in full possession of his civil capacity.

The contribution of publishers or press companies to the social capital of a press courier company can only be made if they have entered into a transport, grouping or distribution contract with it.

To date, two press messaging companies exist in France: Presstalis and the Lyon Press Messengers (MLP).

*To go further* Law 47-585 of April 2, 1947.

**Press broadcasters**

The management of a newspaper outlet is subject to compliance with conditions set by the town halls, including:

- Be a salesman at a kiosk or press store
- justify professional skills in the management of a kiosk, including having received appropriate vocational training;
- have not been the subject of a criminal conviction on the second ballot of the criminal record.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Free Establishment)

A national of a Member State of the European Union (EU) or a party to the European Economic Area (EEA) is subject to the same conditions of professional qualifications as the French.

### c. Some peculiarities of the regulation of the activity

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

As the premises are open to the public, the professional must comply with the rules relating to public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

#### Compensation for sales agents

The CSMP determines the remuneration conditions of sales agents. As a result, they will receive a percentage of the sale price set to the public.

#### Nomination for the operation of kiosks

The company Mediakiosk manages the establishment of kiosks in France. Those interested in establishing themselves in the province will first have to sign an operating contract with the company. When he moves to Paris, if necessary, he will have to apply by registering on a list maintained by Mediakiosk. The mayor of Paris will then take care of choosing the candidate.

Three degrees. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Obtain accreditation for custodians and press broadcasters

Custodians and press broadcasters must obtain accreditation from the Network Commission (CDR) which operates within the CSMP. This approval authorizes these agents of the sale of the press to enter into distribution contracts with the press courier companies (for the custodians) or with the custodians (for the broadcasters).

The CDR receives the application file that must include the project submission, the applicant's professional skills, the location and layout of the depot or point of sale.

The file is being reviewed by the SECRETARIAT of the CDR, which will decide on its regularity. In the event of an incomplete file, the applicant will have eight days to pass on the missing documents.

Upon receipt of the full file and after reviewing the exhibits, the CDR may decide:

- Accept the request
- to accept the request partially or subject to conditions;
- to refuse the application.

The individual, who has had his application denied, may request reconsideration.

*To go further* Article 9 of the[internal regulation](http://csmp.fr/Menu/Le-CSMP/Documentation/Reglement-interieur) CSMP.

### c. Obligation to register with the press sales agents' file

Once approved, custodians and broadcasters must apply for inclusion on the CSMP's list of press sales agents.

They will each have to fill out a[Form](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB) that they will address either:

- to the press messaging when the request comes from the custodian;
- custodian when the request comes from a broadcaster.

**Cost**

Registration on the list is paid for. Its cost is different depending on the level of distribution of the press. For more information, it is advisable to check with the[CSMP](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB).

