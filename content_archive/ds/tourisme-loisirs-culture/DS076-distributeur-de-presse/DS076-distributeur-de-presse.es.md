﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS076" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Distribuidor de prensa" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="distribuidor-de-prensa" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/distribuidor-de-prensa.html" -->
<!-- var(last-update)="2020-04-15 17:24:16" -->
<!-- var(url-name)="distribuidor-de-prensa" -->
<!-- var(translation)="Auto" -->


Distribuidor de prensa
======================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:24:16<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

### a. Definición

Varios niveles de partes interesadas garantizan la distribución de la prensa:

- Los mensajeros de prensa son responsables de recibir los títulos de los editores y de gestionar su clasificación y distribución;
- los custodios de prensa o mayoristas a su vez distribuyen los títulos a los organismos de radiodifusión de prensa o minoristas;
- los presentadores de noticias (agentes de noticias y minoristas) participan en ventas directas al público.

Para obtener más información sobre los diferentes niveles de distribución de la prensa, es aconsejable consultar la[sitio web del Consejo Superior de Mensajeros de Prensa](http://www.csmp.fr/) (CSMP).

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

Dos grados. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

**Mensajeros de prensa**

Una empresa de correo electrónico sólo se puede crear si al menos tres asociados lo componen. Como tal, su director tendrá que justificar:

- Ser de nacionalidad francesa;
- Ser mayores de edad
- de la plenitud de sus derechos civiles y estar en plena posesión de su capacidad civil.

La contribución de los editores o empresas de prensa al capital social de una empresa de mensajería de prensa sólo podrá realizarse si han celebrado un contrato de transporte, agrupación o distribución con ella.

Hasta la fecha, existen dos empresas de mensajería de prensa en Francia: Presstalis y Lyon Press Messengers (MLP).

*Para ir más allá* Ley 47-585 de 2 de abril de 1947.

**Emisoras de prensa**

La gestión de un punto de venta de periódicos está sujeta al cumplimiento de las condiciones establecidas por los ayuntamientos, entre las que se incluyen:

- Ser vendedor en un quiosco o tienda de prensa
- justificar las competencias profesionales en la gestión de un quiosco, incluida la formación profesional adecuada;
- no han sido objeto de una condena penal en la segunda votación del historial delictivo.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o una parte en el Espacio Económico Europeo (EEE) está sujeto a las mismas condiciones de cualificación profesional que los franceses.

### c. Algunas peculiaridades de la regulación de la actividad

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Dado que las instalaciones están abiertas al público, el profesional debe cumplir con las normas relativas a las instituciones públicas (ERP):

- Fuego
- Accesibilidad.

Para obtener más información, consulte la hoja "Establecimiento de recepción pública (ERP)".

#### Compensación para agentes de ventas

El CSMP determina las condiciones de remuneración de los agentes de ventas. Como resultado, recibirán un porcentaje del precio de venta establecido al público.

#### Nominación para el funcionamiento de quioscos

La empresa Mediakiosk gestiona el establecimiento de quioscos en Francia. Los interesados en establecerse en la provincia primero tendrán que firmar un contrato de operación con la empresa. Cuando se traslade a París, si es necesario, tendrá que solicitarlo registrándose en una lista mantenida por Mediakiosk. El alcalde de París se encargará de elegir al candidato.

Tres grados. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

Dependiendo de la naturaleza del negocio, el empresario debe inscribirse en el Registro de Comercios y Artesanía (RMA) o en el Registro Mercantil y Corporativo (RCS). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Obtener la acreditación para los custodios y los organismos de radio

Los custodios y los organismos de radiodifusión de prensa deben obtener la acreditación de la Comisión de la Red (CDR) que opera dentro del CSMP. Esta aprobación autoriza a estos agentes de la venta de la prensa a celebrar contratos de distribución con las empresas de mensajería de prensa (para los custodios) o con los custodios (para los organismos de radiodifusión).

El CDR recibe el expediente de solicitud que debe incluir la presentación del proyecto, las habilidades profesionales del solicitante, la ubicación y el diseño del depósito o punto de venta.

El expediente está siendo revisado por el SECRETARIAT del CDR, que decidirá sobre su regularidad. En caso de un archivo incompleto, el solicitante tendrá ocho días para transmitir los documentos que faltan.

Tras la recepción del expediente completo y después de revisar las pruebas documentales, el CDR podrá decidir:

- Acepte la solicitud
- aceptar la solicitud parcial o sujeta a condiciones;
- para rechazar la solicitud.

El individuo, a quien se le ha denegado su solicitud, puede solicitar una reconsideración.

*Para ir más allá* Artículo 9 de la[regulación interna](http://csmp.fr/Menu/Le-CSMP/Documentation/Reglement-interieur) CSMP.

### c. Obligación de registrarse en el expediente de los agentes de ventas de prensa

Una vez aprobados, los custodios y los organismos de radiodifusión deben solicitar su inclusión en la lista de agentes de ventas de prensa del CSMP.

Cada uno de ellos tendrá que llenar un[Forma](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB) que abordarán:

- a la mensajería de prensa cuando la solicitud proviene del custodio;
- custodio cuando la solicitud proviene de un organismo de radiodifusión.

**Costo**

El registro en la lista se paga. Su coste es diferente dependiendo del nivel de distribución de la prensa. Para obtener más información, es aconsejable consultar con el[CSMP](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB).

