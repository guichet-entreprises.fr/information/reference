﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS076" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Distributeur de presse" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="distributeur-de-presse" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/distributeur-de-presse.html" -->
<!-- var(last-update)="2020-04-15 17:24:16" -->
<!-- var(url-name)="distributeur-de-presse" -->
<!-- var(translation)="None" -->


# Distributeur de presse

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:24:16<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Plusieurs niveaux d'intervenants assurent la distribution de la presse :

* les messageries de presse sont chargées de recevoir les titres des éditeurs et de gérer leur tri et leur répartition ;
* les dépositaires de presse ou grossistes distribuent à leur tour les titres aux diffuseurs de presse ou détaillants ;
* les diffuseurs de presse (marchands de journaux et détaillants) s'occupent de la vente directe au public.

Pour plus d'informations sur les différents niveaux de distribution de presse, il est conseillé de se reporter au [site du Conseil supérieur des messageries de presse](http://www.csmp.fr/) (CSMP).

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

* pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
* pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
* pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

**Les messageries de presse**

Une société de messagerie de presse ne peut être créée que si au moins trois associés la composent. Son directeur devra, à ce titre, justifier :

* être de nationalité française ;
* être majeur ;
* de la plénitude de ses droits civiques et être en pleine possession de sa capacité civile.

L'apport des éditeurs ou des entreprises de presse au capital social d'une société de messagerie de presse ne peut se faire que si ces derniers ont conclu un contrat de transport, de groupage ou de distribution avec elle.

À ce jour, deux sociétés de messagerie de presse existent en France : Presstalis et les Messageries lyonnaises de presse (MLP).

*Pour aller plus loin* : loi n° 47-585 du 2 avril 1947.

**Les diffuseurs de presse**

La gérance d'un point de vente de journaux est soumise au respect de conditions fixées par les mairies, dont notamment :

* être vendeur dans un kiosque ou un magasin de presse ;
* justifier de compétences professionnelles dans la gestion d'un kiosque, notamment en ayant suivi une formation professionnelle adaptée ;
* ne pas avoir fait l'objet d'une condamnation pénale inscrite au bulletin n° 2 du casier judiciaire.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE) est soumis aux mêmes conditions de qualifications professionnelles que les Français.

### c. Quelques particularités de la réglementation de l'activité

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Les locaux étant ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

* en matière d’incendie ;
* en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « Établissement recevant du public (ERP) ».

#### Rémunération des agents de la vente

La CSMP détermine les conditions de rémunération des agents de la vente. Ainsi, ces derniers percevront un pourcentage du prix de vente fixé au public.

#### Candidature pour l'exploitation des kiosques

La société Médiakiosk gère l'implantation des kiosques en France. L'intéressé qui souhaiterait s'implanter en province devra au préalable signer un contrat de d'exploitation avec cette société. Lorsqu'il s'installe à Paris, le cas échéant, il devra en faire la candidature en s'inscrivant sur une liste tenue par Médiakiosk. La mairie de Paris se chargera alors de choisir le candidat.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS). Il est conseillé de se reporter aux fiches « Formalités de déclaration d'une société commerciale » et « Immatriculation d'une entreprise individuelle commerciale au RCS » pour de plus amples informations.

### b. Obtenir un agrément pour les dépositaires et les diffuseurs de presse

Les dépositaires et les diffuseurs de presse doivent obtenir un agrément auprès de la Commission du réseau (CDR) qui intervient au sein du CSMP. Cet agrément autorise ces agents de la vente de la presse à conclure soit des contrats de distributions avec les sociétés de messagerie de presse (pour les dépositaires) ou avec les dépositaires (pour les diffuseurs).

La CDR reçoit le dossier de demande qui doit comprendre la présentation du projet, les compétences professionnelles du demandeur, la localisation et l'aménagement du dépôt ou du point de vente.

Le dossier est examiné par le secrétariat de la CDR qui se prononcera sur sa régularité. En cas de dossier incomplet, le demandeur aura huit jours pour transmettre les pièces manquantes.

À réception du dossier complet et après examen des pièces, la CDR pourra décider :

* d'accepter la demande ;
* d'accepter partiellement la demande ou sous conditions ;
* de refuser la demande.

L'intéressé, qui a vu sa demande refusée peut demander son réexamen.

*Pour aller plus loin* : article 9 du [règlement intérieur](http://csmp.fr/Menu/Le-CSMP/Documentation/Reglement-interieur) de la CSMP.

### c. Obligation de s'inscrire au fichier des agents de la vente de la presse

Une fois l'agrément obtenu, les dépositaires et les diffuseurs doivent demander leur inscription sur la liste des agents de la vente de la presse du CSMP.

Ils devront chacun remplir un [formulaire](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB) qu'ils adresseront soit :

* à la messagerie de la presse lorsque la demande vient du dépositaire ;
* au dépositaire de presse lorsque la demande vient d'un diffuseur.

**Coût**

L'inscription sur la liste est payante. Son coût est différent selon le niveau de distribution de la presse. Pour plus d'informations, il est conseillé de se renseigner auprès de la [CSMP](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB).