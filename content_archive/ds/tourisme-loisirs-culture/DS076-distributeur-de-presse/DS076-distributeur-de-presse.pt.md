﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS076" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Lazer, Cultura" -->
<!-- var(title)="Distribuidor de imprensa" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-lazer-cultura" -->
<!-- var(title-short)="distribuidor-de-imprensa" -->
<!-- var(url)="https://www.guichet-entreprises.fr/pt/ds/turismo-lazer-cultura/distribuidor-de-imprensa.html" -->
<!-- var(last-update)="2020-04-15 17:24:16" -->
<!-- var(url-name)="distribuidor-de-imprensa" -->
<!-- var(translation)="Auto" -->


Distribuidor de imprensa
========================

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:24:16<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

### a. Definição

Vários níveis de stakeholders garantem a distribuição da imprensa:

- Os correios de imprensa são responsáveis por receber títulos dos editores e gerenciar sua classificação e distribuição;
- os custodiantes de imprensa ou atacadistas, por sua vez, distribuem os títulos para pressionar emissoras ou varejistas;
- jornalistas (jornalistas e varejistas) estão envolvidos em vendas diretas ao público.

Para obter mais informações sobre os diferentes níveis de distribuição da imprensa, é aconselhável consultar o[site do Conselho Superior de Mensageiros da Imprensa](http://www.csmp.fr/) (CSMP).

### b. CFE competente

A CFE relevante depende da natureza da estrutura em que a atividade é realizada:

- para empresas comerciais, é a Câmara de Comércio e Indústria (CCI);
- para as sociedades civis, este é o registro do Tribunal Comercial;
- para sociedades civis nos departamentos do Reno Inferior, Alto Reno e Mosela, este é o registro do tribunal distrital.

**É bom saber**

A atividade é considerada comercial desde que a empresa tenha mais de dez funcionários (exceto no Reno Inferior, Alto Reno e Moselle, onde a atividade permanece artesanal, independentemente do número de funcionários da empresa desde que ela não utiliza um processo industrial). Por outro lado, se a empresa possui dez ou menos funcionários, sua atividade é considerada artesanal. Por fim, se o profissional tem uma atividade de compra e revenda, sua atividade é artesanal e comercial.

Dois graus. Condições de instalação
-----------------------------------

### a. Qualificações profissionais

**Mensageiros de imprensa**

Uma empresa de correio de notícias só pode ser criada se pelo menos três associados compensarem. Como tal, seu diretor terá que justificar:

- Seja um cidadão francês;
- Seja maior de idade
- da plenitude de seus direitos civis e estar em plena posse de sua capacidade civil.

A contribuição de editoras ou empresas de imprensa para o capital social de uma empresa de correio de imprensa só pode ser feita se eles tiverem firmado um contrato de transporte, agrupamento ou distribuição com ele.

Até o momento, existem duas empresas de mensagens de imprensa na França: Presstalis e Lyon Press Messengers (MLP).

*Para ir mais longe* Lei 47-585 de 2 de abril de 1947.

**Emissoras de imprensa**

A gestão de um jornal está sujeita ao cumprimento das condições estabelecidas pelas prefeituras, incluindo:

- Seja um vendedor em um quiosque ou loja de imprensa
- justificar habilidades profissionais na gestão de um quiosque, incluindo ter recebido formação profissional adequada;
- não foram objeto de uma condenação criminal na segunda votação da ficha criminal.

### b. Qualificações Profissionais - Cidadãos da UE ou EEE (Serviço Livre ou Estabelecimento Livre)

Um cidadão de um Estado-Membro da União Europeia (UE) ou uma parte da Área Econômica Europeia (EEE) está sujeito às mesmas condições de qualificação profissional que os franceses.

### c. Algumas peculiaridades da regulação da atividade

#### Se necessário, cumpra as normas gerais aplicáveis a todas as instituições públicas (ERP)

Como as instalações são abertas ao público, o profissional deve cumprir as regras relativas às instituições públicas (ERP):

- Fogo
- Acessibilidade.

Para obter mais informações, consulte a folha "Estabelecimento de Recebimento Público (ERP)".

#### Remuneração para agentes de vendas

O CSMP determina as condições de remuneração dos agentes de vendas. Como resultado, eles receberão uma porcentagem do preço de venda definido ao público.

#### Nomeação para operação de quiosques

A empresa Mediakiosk gerencia a implantação de quiosques na França. Os interessados em se estabelecer na província terão primeiro que assinar um contrato operacional com a empresa. Quando ele se mudar para Paris, se necessário, ele terá que se inscrever em uma lista mantida pela Mediakiosk. O prefeito de Paris cuidará da escolha do candidato.

Três graus. Procedimentos de instalação e formalidades
------------------------------------------------------

### a. Formalidades de relatórios da empresa

Dependendo da natureza do negócio, o empreendedor deve se cadastrar no Registro de Comércio e Artesanato (RMA) ou no Registro Comercial e Corporativo (RCS). É aconselhável consultar as "Formalidades de Relatório de Empresas Comerciais" e "Registro de uma Empresa Individual Comercial no RCS" para obter mais informações.

### b. Obter credenciamento para custodiantes e emissoras de imprensa

Os custodiantes e as emissoras de imprensa devem obter o credenciamento da Comissão de Rede (CDR) que opera dentro do CSMP. Essa aprovação autoriza esses agentes da venda da imprensa a celebrar contratos de distribuição com as empresas de correio de imprensa (para os custodiados) ou com os custodiados (para as emissoras).

O CDR recebe o arquivo de solicitação que deve incluir a submissão do projeto, as habilidades profissionais do candidato, a localização e o layout do depósito ou ponto de venda.

O arquivo está sendo analisado pela secretaria da CDR, que decidirá sobre sua regularidade. Em caso de um arquivo incompleto, o requerente terá oito dias para repassar os documentos faltantes.

Após o recebimento do arquivo completo e após a revisão das exposições, o CDR pode decidir:

- Aceite o pedido
- aceitar o pedido parcial ou sujeito a condições;
- para recusar a solicitação.

O indivíduo, que teve seu pedido negado, pode pedir reconsideração.

*Para ir mais longe* Artigo 9º do[regulação interna](http://csmp.fr/Menu/Le-CSMP/Documentation/Reglement-interieur) O CSMP.

### c. Obrigação de se registrar no arquivo dos agentes de vendas de imprensa

Uma vez aprovados, os custodiantes e os radiodifusores devem solicitar a inclusão na lista de agentes de vendas de imprensa do CSMP.

Cada um terá que preencher um[Forma](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB) que eles abordarão em qualquer um deles:

- para a imprensa mensagens quando o pedido vem do custodiante;
- custodiante quando o pedido vem de uma emissora.

**Custo**

A inscrição na lista é paga. Seu custo é diferente dependendo do nível de distribuição da imprensa. Para obter mais informações, é aconselhável verificar com o[CSMP](http://csmp.fr/Menu/Le-CSMP/Documentation/Inscription-Agents-de-la-vente/(language)/eng-GB).

