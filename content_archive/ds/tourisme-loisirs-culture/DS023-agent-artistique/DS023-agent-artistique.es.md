﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS023" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Agente artístico" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="agente-artistico" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/agente-artistico.html" -->
<!-- var(last-update)="2020-04-15 17:24:09" -->
<!-- var(url-name)="agente-artistico" -->
<!-- var(translation)="Auto" -->


Agente artístico
================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:24:09<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

### a. Definición

El agente artístico es un profesional encargado, de antemano, por artistas de interpretación para proporcionarles compromisos, representar sus intereses o proporcionarles una inversión. Actúa como intermediario entre el artista y su productora. El agente artístico puede representar todo tipo de clientes, pero generalmente se especializa en un campo particular (artistas líricos, artistas de variedades, músicos, actores, actores, etc.).

Al agente artístico se le pueden confiar varios tipos de asignaciones, incluyendo:

- Defender las actividades e intereses profesionales del artista;
- Asistencia, gestión, seguimiento y administración de la carrera del artista;
- Investigar y celebrar contratos de trabajo para el artista;
- promover la carrera del artista a todos los profesionales de las artes en cuestión;
- Revisar cualquier propuesta dirigida al artista;
- Gestionar la agenda del artista y las relaciones con la prensa;
- negociación y revisión legal del contenido de los contratos del artista.

*Para ir más allá* Artículos L. 7121-9 y R. 7121-1 del Código de Trabajo.

**Qué saber**

El agente artístico también puede ser llamado empresario o gerente.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. En el caso de un agente artístico, se trata de una actividad comercial. Por lo tanto, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

*Para ir más allá* Artículo L. 7121-11 del Código de Trabajo.

**Es bueno saber**

Un comerciante es una persona que realiza actos de comercio como de costumbre. Se consideran actos de comercio: la compra de bienes para la reventa, el alquiler de muebles, operaciones de fabricación o transporte, las actividades de intermediarios (incluyendo corretaje y agencia), etc.

Dos grados. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere diploma o certificación para ejercer como agente artístico. Sin embargo, se recomienda obtener formación en gestión, comunicación o presentar experiencia profesional en el sector del entretenimiento y el arte.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

Los nacionales de un Estado de la Unión Europea (UE) o un Estado Parte en el acuerdo del Espacio Económico Europeo (EEE) no están sujetos, como los franceses, a ningún requisito de grado o certificación.

### c. Incompatibilidades

Nadie podrá ejercer la actividad de agente artístico si ejerce, directamente o por persona interpuesta, la actividad de productor de obras cinematográficas o audiovisuales.

Sujeto al respeto por la incompatibilidad anterior, un agente artístico puede producir una actuación en vivo, cuando tiene una licencia para realizar actuaciones en vivo. En este caso, no podrá cobrar ninguna comisión a todos los artistas que componen la distribución del espectáculo.

Para más información, es aconsejable consultar el listado " [Emprendedor de entretenimiento](https://www.guichet-entreprises.fr/fr/activites-reglementees/tourisme-loisirs-culture/entrepreneur-de-spectacles/) ».

*Para ir más allá* Artículos L. 7121-9 y L. 7121-12 del Código de Trabajo.

### d. Algunas peculiaridades de la regulación de la actividad

#### Obligación de recibir una orden judicial

El mandato entre un agente artístico y un artista se rige por el 1984 y siguiendo los artículos del Código Civil.

El mandato entre el agente artístico y el artista se establece de forma gratuita. El mandato debe especificar, como mínimo:

- La misión o misiones encomendadas y las modalidades de presentación de informes sobre su ejecución periódica;
- sus condiciones salariales;
- mandato u otros términos por los cuales termine.

*Para ir más allá* Artículo R. 7121-6 del Código de Trabajo.

#### Compensación para el agente artístico

Las cantidades que los agentes artísticos pueden recibir en remuneración por sus servicios y, en particular, por la colocación se calculan como un porcentaje de la remuneración total del artista (remuneración fija o proporcional a la operación). Este porcentaje no podrá exceder del 10% de la remuneración bruta del artista. Si el agente artístico participa en la organización y desarrollo de la carrera del artista, de acuerdo con las prácticas profesionales vigentes, su remuneración puede ser de hasta el 15% del importe de la remuneración bruta del artista.

Estas sumas podrán, por acuerdo entre el agente artístico y el artista intérprete o ejecutante, ser total o parcialmente responsabilidad del artista. En este caso, el agente artístico le da al artista el pago realizado por el artista. No obstante, la remuneración del agente artístico también puede ser pagada por el empleador del artista, total o parcialmente. Corresponde al artista y a su empleador definir, en el contrato de trabajo que los une, los términos de la remuneración del agente.

*Para ir más allá* Artículos L. 7121-13, D. 7121-7 y D. 7121-8 del Código de Trabajo.

#### Si es necesario, cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas para las instituciones que reciben el público:

- Fuego
- Accesibilidad.

Para más información, es aconsejable consultar el listado " [Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

Tres grados. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

La empresa debe estar inscrita en el Registro Mercantil y Mercantil (RCS). Para obtener más información, se recomienda ver las "Formalidades de Informes de Empresas Comerciales" y "Registro de una Empresa Comercial en el RCS."

### b. Autorización posterior al registro

Desde el 1 de enero de 2016, se ha suprimido el requisito de registro de agentes artísticos.

*Para ir más allá* : Ordenanza No 2015-1682 de 17 de diciembre de 2015 simplificando ciertos regímenes de preautorización y presentación de informes para empresas y profesionales.

