﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS023" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Tempo libero, Cultura" -->
<!-- var(title)="Agente artistico" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-tempo-libero-cultura" -->
<!-- var(title-short)="agente-artistico" -->
<!-- var(url)="https://www.guichet-entreprises.fr/it/ds/turismo-tempo-libero-cultura/agente-artistico.html" -->
<!-- var(last-update)="2020-04-15 17:24:09" -->
<!-- var(url-name)="agente-artistico" -->
<!-- var(translation)="Auto" -->


Agente artistico
================

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:24:09<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

### a. Definizione

L'agente artistico è un professionista commissionato, in anticipo, da artisti che si esibiscono a fornire loro impegni, a rappresentare i loro interessi o a fornire loro un investimento. Funge da intermediario tra l'artista e la loro società di produzione. L'agente artistico può rappresentare tutti i tipi di clienti, ma generalmente è specializzato in un particolare campo (artisti lirici, artisti di varietà, musicisti, attori, attori, ecc.).

All'agente artistico possono essere affidati diversi tipi di incarichi, tra cui:

- Difendere le attività professionali e gli interessi dell'artista;
- Assistenza, gestione, monitoraggio e gestione della carriera dell'artista;
- Ricerca e conclusione di contratti di lavoro per l'artista;
- promuovere la carriera dell'artista a tutti i professionisti delle arti in questione;
- Rivedere tutte le proposte indirizzate all'artista;
- Gestire l'agenda dell'artista e le relazioni con la stampa;
- negoziazione e revisione legale del contenuto dei contratti dell'artista.

*Per andare oltre* Articoli L. 7121-9 e R. 7121-1 del Codice del Lavoro.

**Cosa sapere**

L'agente artistico può anche essere chiamato impresario o manager.

### b. Competente Business Formalities Centre (CFE)

Il CFE in questione dipende dalla natura della struttura in cui viene svolta l'attività. Nel caso di un agente artistico, si tratta di un'attività commerciale. Il CFE pertinente è quindi la Camera di Commercio e Industria (CCI).

*Per andare oltre* Articolo L. 7121-11 del codice del lavoro.

**Buono a sapersi**

Un commerciante è una persona che esegue atti di commercio come al solito. Questi sono considerati atti di commercio: l'acquisto di beni per la rivendita, il noleggio di mobili, operazioni di produzione o di trasporto, le attività di intermediari (compresa l'intermediazione e l'agenzia), ecc.

Due gradi. Condizioni di installazione
--------------------------------------

### a. Qualifiche professionali

Non è richiesto alcun diploma o certificazione per esercitarsi come agente artistico. Tuttavia, si raccomanda di ottenere una formazione in gestione, comunicazione o per presentare esperienza professionale nel settore dell'intrattenimento e dell'artisticità.

### b. Qualifiche professionali - Cittadini europei (LPS o LE)

I cittadini di uno Stato dell'Unione europea (UE) o di un partito statale dell'accordo sullo Spazio economico europeo (AEA) non sono soggetti, come i francesi, a qualsiasi grado o requisiti di certificazione.

### c. Incompatibilità

Nessuno può svolgere l'attività di agente artistico se esercita, direttamente o per persona interposed, l'attività di produttore di opere cinematografiche o audiovisive.

Soggetto al rispetto della precedente incompatibilità, un agente artistico può produrre una performance dal vivo, quando ha una licenza per intraprendere spettacoli dal vivo. In questo caso, non può raccogliere alcuna commissione su tutti gli artisti che compongono la distribuzione dello spettacolo.

Per ulteriori informazioni, si consiglia di fare riferimento all'elenco " [Imprenditore di intrattenimento](https://www.guichet-entreprises.fr/fr/activites-reglementees/tourisme-loisirs-culture/entrepreneur-de-spectacles/) ».

*Per andare oltre* Articoli L. 7121-9 e L. 7121-12 del Codice del Lavoro.

### d. Alcune peculiarità della regolamentazione dell'attività

#### Obbligo di ricevere un mandato

Il mandato tra un agente artistico e un artista è disciplinato dal 1984 e a seguito degli articoli del Codice Civile.

Il mandato tra l'agente artistico e l'artista è stabilito gratuitamente. Il mandato deve specificare, come minimo:

- la missione o le missioni affidate e le modalità per riferire sulla loro esecuzione periodica;
- le loro condizioni retributivi;
- mandato o altri termini con cui termina.

*Per andare oltre* Articolo R. 7121-6 del codice del lavoro.

#### Compensazione per l'agente artistico

Le somme che gli agenti artistici possono ricevere in retribuzione per i loro servizi e in particolare dal collocamento sono calcolate come percentuale della retribuzione totale dell'artista (retribuzione fissa o proporzionale all'operazione). Questa percentuale non può superare il 10% della retribuzione lorda dell'artista. Se l'agente artistico è coinvolto nell'organizzazione e nello sviluppo della carriera dell'artista, in conformità con le pratiche professionali in vigore, la sua retribuzione può essere fino al 15% dell'importo della remunerazione lorda dell'artista.

Queste somme possono, per accordo tra l'agente artistico e l'esecutore, essere in tutto o parte della responsabilità dell'artista. In questo caso, l'agente artistico dà all'artista il pagamento effettuato dall'artista. Tuttavia, la remunerazione dell'agente artistico può essere pagata anche dal datore di lavoro dell'artista, in tutto o in parte. Spetta all'artista e al suo datore di lavoro definire, nel contratto di lavoro che li unisce, i termini della remunerazione dell'agente.

*Per andare oltre* Articoli L. 7121-13, D. 7121-7 e D. 7121-8 del Codice del Lavoro.

#### Se necessario, rispettare le normative generali applicabili a tutte le istituzioni pubbliche (ERP)

Se i locali sono aperti al pubblico, il professionista deve rispettare le regole per gli istituti che ricevono il pubblico:

- Fuoco
- Accessibilità.

Per ulteriori informazioni, si consiglia di fare riferimento all'elenco " [Istituzione che riceve il pubblico](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

Tre gradi. Procedure di installazione e formalità
-------------------------------------------------

### a. Le formalità di rendicontazione delle società

La società deve essere iscritta al Registro del Commercio e delle Imprese (RCS). Per ulteriori informazioni, si consiglia di vedere le "Formalità di reporting delle società commerciali" e "Registrazione di un'impresa commerciale presso l'RCS".

### b. Autorizzazione post-registrazione

Dal 1o gennaio 2016, l'obbligo di registrarsi per agenti artistici è stato abolito.

*Per andare oltre* : Ordinanza n. 2015-1682 del 17 dicembre 2015 semplificando alcuni regimi di pre-autorizzazione e rendicontazione per aziende e professionisti.

