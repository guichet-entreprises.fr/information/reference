﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS023" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourismus, Freizeit, Kultur" -->
<!-- var(title)="Künstlerischer Agent" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourismus-freizeit-kultur" -->
<!-- var(title-short)="kunstlerischer-agent" -->
<!-- var(url)="https://www.guichet-entreprises.fr/de/ds/tourismus-freizeit-kultur/kunstlerischer-agent.html" -->
<!-- var(last-update)="2020-04-15 17:24:09" -->
<!-- var(url-name)="kunstlerischer-agent" -->
<!-- var(translation)="Auto" -->


Künstlerischer Agent
====================

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:24:09<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

### a. Definition

Der künstlerische Agent ist ein professioneller Mitarbeiter, der im Voraus von ausführenden Künstlern beauftragt wird, ihnen Verpflichtungen zu geben, ihre Interessen zu vertreten oder ihnen eine Investition zu geben. Er fungiert als Vermittler zwischen dem Künstler und seiner Produktionsfirma. Der künstlerische Agent kann alle Arten von Kunden vertreten, ist aber in der Regel auf ein bestimmtes Gebiet spezialisiert (Lyriker, Varieté-Künstler, Musiker, Schauspieler, Schauspieler, etc.).

Der künstlerische Agent kann mit verschiedenen Arten von Aufgaben betraut werden, darunter:

- Verteidigung der beruflichen Aktivitäten und Interessen des Künstlers;
- Unterstützung, Verwaltung, Überwachung und Verwaltung der Laufbahn des Künstlers;
- Erforschung und Abschluss von Arbeitsverträgen für den Künstler;
- Förderung der Karriere des Künstlers für alle Fachleute in den betreffenden Künsten;
- Überprüfung aller Vorschläge, die an den Künstler gerichtet sind;
- Verwaltung der Agenda des Künstlers und der Beziehungen zur Presse;
- Verhandlung und rechtliche Überprüfung des Inhalts der Verträge des Künstlers.

*Um weiter zu gehen* Artikel L. 7121-9 und R. 7121-1 des Arbeitsgesetzbuches.

**Was Sie wissen sollten**

Der künstlerische Agent kann auch Impresario oder Manager genannt werden.

### b. Kompetenzzentrum für Geschäftsformalitäten (CFE)

Die betreffende CFE hängt von der Art der Struktur ab, in der die Tätigkeit ausgeübt wird. Im Falle eines künstlerischen Agenten handelt es sich um eine kommerzielle Tätigkeit. Das relevante CFE ist daher die Industrie- und Handelskammer (IHK).

*Um weiter zu gehen* Artikel L. 7121-11 des Arbeitsgesetzbuches.

**Gut zu wissen**

Ein Händler ist eine Person, die Wie üblich Handelshandlungen durchführt. Diese sind Handelshandlungen: der Kauf von Waren zum Weiterverkauf, die Vermietung von Möbeln, Herstellungs- oder Transportvorgänge, die Tätigkeiten von Vermittlern (einschließlich Makler und Agentur) usw.

Zwei Grad. Installationsbedingungen
-----------------------------------

### a. Berufliche Qualifikationen

Für die Ausübung eines künstlerischen Agenten ist kein Diplom oder eine Zertifizierung erforderlich. Es wird jedoch empfohlen, eine Ausbildung in Management, Kommunikation zu erhalten oder Berufserfahrung im Unterhaltungs- und Kunstbereich zu präsentieren.

### b. Berufsqualifikationen - Europäische Staatsangehörige (LPS oder LE)

Staatsangehörige eines Staates der Europäischen Union (EU) oder eines Vertragsstaats des Abkommens über den Europäischen Wirtschaftsraum (EWR) unterliegen nicht, wie die Franzosen, irgendwelchen Grad- oder Zertifizierungsanforderungen.

### c. Inkompatibilitäten

Niemand darf die Tätigkeit eines künstlerischen Agenten ausüben, wenn er direkt oder von der Person, die intergesetzt ist, die Tätigkeit des Produzenten von Filmen oder audiovisuellen Werken ausübt.

Vorbehaltlich der Achtung der vorherigen Inkompatibilität kann ein künstlerischer Agent eine Live-Performance produzieren, wenn er eine Lizenz für Live-Auftritte besitzt. In diesem Fall darf er keine Provision für alle Künstler einziehen, die die Verteilung der Show ausmachen.

Für weitere Informationen ist es ratsam, auf die Auflistung " [Unterhaltungsunternehmer](https://www.guichet-entreprises.fr/fr/activites-reglementees/tourisme-loisirs-culture/entrepreneur-de-spectacles/) ».

*Um weiter zu gehen* Artikel L. 7121-9 und L. 7121-12 des Arbeitsgesetzbuches.

### d. Einige Besonderheiten der Regelung der Tätigkeit

#### Verpflichtung zum Erhalt eines Haftbefehls

Das Mandat zwischen einem künstlerischen Agenten und einem Künstler wird durch das 1984 und die folgenden Artikel des Bürgerlichen Gesetzbuches geregelt.

Das Mandat zwischen dem künstlerischen Agenten und dem Künstler wird kostenlos festgelegt. Im Mandat sollte mindestens Folgendes festgelegt werden:

- Die betrauten Missionen und die Modalitäten für die Berichterstattung über ihre regelmäßige Ausführung;
- ihre Lohnbedingungen;
- Amtszeit oder andere Amtszeiten, mit denen sie endet.

*Um weiter zu gehen* Artikel R. 7121-6 des Arbeitsgesetzbuches.

#### Entschädigung für den künstlerischen Agenten

Die Summen, die künstlerische Agenten als Vergütung für ihre Leistungen und insbesondere aus der Vermittlung erhalten können, werden als Prozentsatz der Gesamtvergütung des Künstlers (feste oder anteilige Vergütung für den Betrieb) berechnet. Dieser Prozentsatz darf 10 % der Bruttovergütung des Künstlers nicht übersteigen. Wenn der künstlerische Agent an der Organisation und Entwicklung der Karriere des Künstlers beteiligt ist, kann seine Vergütung bis zu 15% der Bruttovergütung des Künstlers betragen.

Diese Summen können im Einvernehmen zwischen dem künstlerischen Agenten und dem Ausübenden ganz oder teilweise in die Verantwortung des Künstlers fallen. In diesem Fall gibt der künstlerische Agent dem Künstler die vom Künstler geleistete Zahlung. Die Vergütung des künstlerischen Vertreters kann jedoch auch ganz oder teilweise vom Arbeitgeber des Künstlers gezahlt werden. Es ist Anschaffungsbericht des Künstlers und seines Arbeitgebers, im Arbeitsvertrag, der sie eint, die Bedingungen der Vergütung des Vertreters festzulegen.

*Um weiter zu gehen* Artikel L. 7121-13, D. 7121-7 und D. 7121-8 des Arbeitsgesetzbuches.

#### Beachten Sie gegebenenfalls die allgemeinen Vorschriften für alle öffentlichen Einrichtungen (ERP)

Wenn die Räumlichkeiten der Öffentlichkeit zugänglich sind, muss der Fachmann die Regeln für Einrichtungen, die die Öffentlichkeit empfangen, einhalten:

- Feuer
- Zugänglichkeit.

Für weitere Informationen ist es ratsam, auf die Auflistung " [Einrichtung, die die Öffentlichkeit empfängt](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

Drei Grad. Installationsverfahren und Formalitäten
--------------------------------------------------

### a. Meldeformalitäten des Unternehmens

Das Unternehmen muss im Handels- und Handelsregister (RCS) eingetragen sein. Weitere Informationen finden Sie in den "Commercial Company Reporting Formalities" und "Registration of a Commercial Enterprise at the RCS".

### b. Genehmigung nach der Registrierung

Seit dem 1. Januar 2016 ist die Meldepflicht für künstlerische Agenten abgeschafft.

*Um weiter zu gehen* : Verordnung Nr. 2015-1682 vom 17. Dezember 2015 zur Vereinfachung bestimmter Vorautorisierungs- und Melderegelungen für Unternehmen und Berufstätige.

