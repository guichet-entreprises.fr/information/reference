﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS023" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Lazer, Cultura" -->
<!-- var(title)="Agente artístico" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-lazer-cultura" -->
<!-- var(title-short)="agente-artistico" -->
<!-- var(url)="https://www.guichet-entreprises.fr/pt/ds/turismo-lazer-cultura/agente-artistico.html" -->
<!-- var(last-update)="2020-04-15 17:24:09" -->
<!-- var(url-name)="agente-artistico" -->
<!-- var(translation)="Auto" -->


Agente artístico
================

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:24:09<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

### a. Definição

O agente artístico é um profissional comissionado, antecipadamente, por artistas performáticos para lhes proporcionar compromissos, representar seus interesses ou dar-lhes um investimento. Atua como intermediário entre o artista e sua produtora. O agente artístico pode representar todos os tipos de clientes, mas geralmente se especializa em um determinado campo (artistas líricos, artistas de variedades, músicos, atores, atores, etc.).

O agente artístico pode ser confiado a vários tipos de atribuições, incluindo:

- Defesa das atividades e interesses profissionais do artista;
- Assistência, gestão, acompanhamento e administração da carreira do artista;
- Pesquisa e celebração de contratos de trabalho para o artista;
- promover a carreira do artista a todos os profissionais das artes em questão;
- Revisão de quaisquer propostas dirigidas ao artista;
- Gestão da agenda do artista e das relações com a imprensa;
- negociação e revisão jurídica do conteúdo dos contratos do artista.

*Para ir mais longe* Artigos L. 7121-9 e R. 7121-1 do Código do Trabalho.

**O que saber**

O agente artístico também pode ser chamado de empresário ou gerente.

### b. Centro de Formalidades Empresariais Competentes (CFE)

A CFE relevante depende da natureza da estrutura em que a atividade é realizada. No caso de um agente artístico, é uma atividade comercial. A CFE relevante é, portanto, a Câmara de Comércio e Indústria (CCI).

*Para ir mais longe* Artigo 7121-11 do Código do Trabalho.

**É bom saber**

Um comerciante é uma pessoa que realiza atos de comércio como de costume. São considerados atos de comércio: a compra de bens para revenda, o aluguel de móveis, operações de fabricação ou transporte, as atividades de intermediários (incluindo corretagem e agência), etc.

Dois graus. Condições de instalação
-----------------------------------

### a. Qualificações profissionais

Nenhum diploma ou certificação é necessário para exercer a prática como agente artístico. No entanto, recomenda-se obter formação em gestão, comunicação ou apresentar experiência profissional no setor de entretenimento e artístico.

### b. Qualificações Profissionais - Nacionais Europeus (LPS ou LE)

Os cidadãos de um estado da União Europeia (UE) ou uma parte estatal do acordo da Área Econômica Europeia (EEE) não estão sujeitos, como os franceses, a qualquer grau ou requisitos de certificação.

### c. Incompatibilidades

Ninguém poderá exercer a atividade de agente artístico se exercer, direta ou por pessoa interposta, a atividade de produtor de obras cinematográficas ou audiovisuais.

Sujeito ao respeito pela incompatibilidade anterior, um agente artístico pode produzir uma performance ao vivo, quando possui licença para realizar performances ao vivo. Neste caso, ele não pode receber nenhuma comissão sobre todos os artistas que compõem a distribuição do show.

Para obter mais informações, é aconselhável consultar a listagem " [Empreendedor de entretenimento](https://www.guichet-entreprises.fr/fr/activites-reglementees/tourisme-loisirs-culture/entrepreneur-de-spectacles/) ».

*Para ir mais longe* Artigos L. 7121-9 e L. 7121-12 do Código do Trabalho.

### d. Algumas peculiaridades da regulação da atividade

#### Obrigação de receber um mandado

O mandato entre agente artístico e artista é regido pelo 1984 e pelos artigos do Código Civil.

O mandato entre o agente artístico e o artista é estabelecido gratuitamente. O mandato deve especificar, no mínimo:

- A missão ou missões confiadas e as modalidades de reportagem sobre sua execução periódica;
- suas condições de pagamento;
- mandato ou outros termos pelos quais termina.

*Para ir mais longe* Artigo R. 7121-6 do Código do Trabalho.

#### Remuneração para o agente artístico

As somas que os agentes artísticos podem receber em remuneração por seus serviços e, em particular, a partir da colocação são calculadas como percentual da remuneração total do artista (remuneração fixa ou proporcional à operação). Esse percentual não pode exceder 10% da remuneração bruta do artista. Se o agente artístico estiver envolvido na organização e desenvolvimento da carreira do artista, de acordo com as práticas profissionais vigentes, sua remuneração poderá ser de até 15% do valor da remuneração bruta do artista.

Essas somas podem, por acordo entre o agente artístico e o artista, ser inteira ou parte da responsabilidade do artista. Neste caso, o agente artístico dá ao artista o pagamento feito pelo artista. No entanto, a remuneração do agente artístico também pode ser paga pelo empregador do artista, total ou parcialmente. Cabe ao artista e seu empregador definir, no contrato de trabalho que os une, os termos da remuneração do agente.

*Para ir mais longe* Artigos L. 7121-13, D. 7121-7 e D. 7121-8 do Código do Trabalho.

#### Se necessário, cumpra as normas gerais aplicáveis a todas as instituições públicas (ERP)

Se o local for aberto ao público, o profissional deve cumprir as regras para as instituições que recebem o público:

- Fogo
- Acessibilidade.

Para obter mais informações, é aconselhável consultar a listagem " [Estabelecimento recebendo o público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

Três graus. Procedimentos de instalação e formalidades
------------------------------------------------------

### a. Formalidades de relatórios da empresa

A empresa deve estar registrada no Registro de Comércio e Empresas (RCS). Para obter mais informações, recomenda-se ver as "Formalidades de Relatórios de Empresas Comerciais" e "Registro de uma Empresa Comercial no RCS".

### b. Autorização de pós-registro

Desde 1 º de janeiro de 2016, a exigência de registro de agentes artísticos foi abolida.

*Para ir mais longe* : Portaria nº 2015-1682, de 17 de dezembro de 2015, simplificando certos regimes de pré-autorização e notificação para empresas e profissionais.

