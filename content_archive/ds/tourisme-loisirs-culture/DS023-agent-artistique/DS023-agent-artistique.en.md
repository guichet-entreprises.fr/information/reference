﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS023" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Artistic agent" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="artistic-agent" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/artistic-agent.html" -->
<!-- var(last-update)="2020-04-15 17:24:09" -->
<!-- var(url-name)="artistic-agent" -->
<!-- var(translation)="Auto" -->


Artistic agent
==============

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:24:09<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Defining the activity
------------------------

### a. Definition

The artistic agent is a professional commissioned, in advance, by performing artists to provide them with commitments, represent their interests or provide them with an investment. He acts as an intermediary between the artist and their production company. The artistic agent can represent all kinds of clients but generally specializes in a particular field (lyric artists, variety artists, musicians, actors, actors, etc.).

The artistic agent may be entrusted with several types of assignments, including:

- Defending the artist's professional activities and interests;
- Assistance, management, monitoring and administration of the artist's career;
- Researching and concluding employment contracts for the artist;
- promoting the artist's career to all professionals in the arts in question;
- Reviewing any proposals addressed to the artist;
- Managing the artist's agenda and relations with the press;
- negotiation and legal review of the content of the artist's contracts.

*To go further* Articles L. 7121-9 and R. 7121-1 of the Labour Code.

**What to know**

The artistic agent can also be called impresario or manager.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. In the case of an artistic agent, it is a commercial activity. The relevant CFE is therefore the Chamber of Commerce and Industry (CCI).

*To go further* Article L. 7121-11 of the Labour Code.

**Good to know**

A trader is a person who performs acts of commerce as usual. These are considered acts of commerce: the purchase of goods for resale, the rental of furniture, manufacturing or transport operations, the activities of intermediaries (including brokerage and agency), etc.

Two degrees. Installation conditions
------------------------------------

### a. Professional qualifications

No diploma or certification is required to practice as an artistic agent. However, it is recommended to obtain training in management, communication or to present professional experience in the entertainment and artistic sector.

### b. Professional Qualifications - European Nationals (LPS or LE)

Nationals of a European Union (EU) state or a state party to the European Economic Area (EEA) agreement are not subject, like the French, to any degree or certification requirements.

### c. Incompatibilities

No one may carry out the activity of artistic agent if he exercises, directly or by person interposed, the activity of producer of film or audiovisual works.

Subject to respect for the previous incompatibility, an artistic agent may produce a live performance, when he holds a license to undertake live performances. In this case, he may not collect any commission on all the artists who make up the distribution of the show.

For more information, it is advisable to refer to the listing " [Entertainment entrepreneur](https://www.guichet-entreprises.fr/fr/activites-reglementees/tourisme-loisirs-culture/entrepreneur-de-spectacles/) ».

*To go further* Articles L. 7121-9 and L. 7121-12 of the Labour Code.

### d. Some peculiarities of the regulation of the activity

#### Obligation to receive a warrant

The mandate between an artistic agent and an artist is governed by the 1984 and following articles of the Civil Code.

The mandate between the artistic agent and the artist is established free of charge. The mandate should specify, at a minimum:

- The mission or missions entrusted and the modalities for reporting on their periodic execution;
- their pay conditions;
- term of office or other terms by which it ends.

*To go further* Article R. 7121-6 of the Labour Code.

#### Compensation for the artistic agent

The sums that artistic agents can receive in remuneration for their services and in particular from the placement are calculated as a percentage of the artist's total remuneration (fixed or proportional remuneration to the operation). This percentage may not exceed 10% of the artist's gross remuneration. If the artistic agent is involved in the organization and development of the artist's career, in accordance with the professional practices in force, his remuneration can be up to 15% of the amount of the artist's gross remuneration.

These sums may, by agreement between the artistic agent and the performer, be in whole or part of the artist's responsibility. In this case, the artistic agent gives the artist the payment made by the artist. Nevertheless, the remuneration of the artistic agent may also be paid by the artist's employer, in whole or in part. It is up to the artist and his employer to define, in the employment contract that unites them, the terms of the agent's remuneration.

*To go further* Articles L. 7121-13, D. 7121-7 and D. 7121-8 of the Labour Code.

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

If the premises are open to the public, the professional must comply with the rules for institutions receiving the public:

- Fire
- accessibility.

For more information, it is advisable to refer to the listing " [Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

Three degrees. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The company must be registered in the Register of Trade and Companies (RCS). For more information, it is recommended to see the "Commercial Company Reporting Formalities" and "Registration of a Commercial Enterprise at the RCS."

### b. Post-registration authorisation

Since 1 January 2016, the requirement to register for artistic agents has been abolished.

*To go further* : Ordinance No. 2015-1682 of December 17, 2015 simplifying certain pre-authorization and reporting regimes for companies and professionals.

