﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS023" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Agent artistique" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="agent-artistique" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/agent-artistique.html" -->
<!-- var(last-update)="2020-04-15 17:24:09" -->
<!-- var(url-name)="agent-artistique" -->
<!-- var(translation)="None" -->


# Agent artistique

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:24:09<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’agent artistique est un professionnel mandaté, contre rétribution, par des artistes du spectacle pour leur procurer des engagements, représenter leurs intérêts ou leur assurer un placement. Il intervient en qualité d’intermédiaire entre l’artiste et leur maison de production. L’agent artistique peut représenter toutes sortes de clients mais se spécialise généralement dans un domaine particulier (artistes lyriques, artistes de variétés, musiciens, acteurs, comédiens, etc.).

L’agent artistique peut se voir confier plusieurs types de missions, dont :

* la défense des activités et des intérêts professionnels de l'artiste ;
* l’assistance, la gestion, le suivi et l’administration de la carrière de l'artiste ;
* la recherche et la conclusion de contrats de travail pour l'artiste ;
* la promotion de la carrière de l'artiste auprès de l'ensemble des professionnels du milieu artistique en cause ;
* l’examen de toutes propositions adressées à l'artiste ;
* la gestion de l'agenda de l'artiste et de ses relations avec la presse ;
* la négociation et l’examen juridique du contenu des contrats de l'artiste.

*Pour aller plus loin* : articles L. 7121-9 et R. 7121-1 du Code du travail.

**À savoir**

L’agent artistique peut également être appelé impresario ou manager.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée. Dans le cas d’un agent artistique, il s’agit d’une activité commerciale. Le CFE compétent est donc la chambre de commerce et d’industrie (CCI).

*Pour aller plus loin* : article L. 7121-11 du Code du travail.

**Bon à savoir**

Un commerçant est une personne qui accomplit des actes de commerce à titre habituel. Sont considérés comme des actes de commerce : l’achat de biens en vue de leur revente, la location de meubles, les opérations de manufacture ou de transport, les activités d’intermédiaires de commerce (courtage et agence notamment), etc.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucun diplôme ou certification n’est requis pour exercer en qualité d’agent artistique. Cependant, il est recommandé d’obtenir une formation en management, en communication ou de présenter une expérience professionnelle dans le secteur du spectacle et du milieu artistique.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

Les ressortissants d’un État de l’Union européenne (UE) ou d’un État partie à l’accord sur l’Espace économique européen (EEE) ne sont soumis, au même titre que les Français, à aucune condition de diplôme ou de certification.

### c. Incompatibilités

Nul ne peut exercer l'activité d'agent artistique s'il exerce, directement ou par personne interposée, l'activité de producteur d'œuvres cinématographiques ou audiovisuelles.

Sous réserve du respect de l'incompatibilité précédente, un agent artistique peut produire un spectacle vivant, lorsqu'il est titulaire d'une licence d'entrepreneur de spectacles vivants. Dans ce cas, il ne peut percevoir aucune commission sur l'ensemble des artistes composant la distribution du spectacle.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Entrepreneur de spectacles](https://www.guichet-entreprises.fr/fr/activites-reglementees/tourisme-loisirs-culture/entrepreneur-de-spectacles/) ».

*Pour aller plus loin* : articles L. 7121-9 et L. 7121-12 du Code du travail.

### d. Quelques particularités de la réglementation de l’activité

#### Obligation de recevoir un mandat

Le mandat conclu entre un agent artistique et un artiste est régi par les articles 1984 et suivants du Code civil.

Le mandat entre l’agent artistique et l’artiste est établi à titre gratuit. Le mandat doit préciser, au minimum :

* la ou les mission(s) confiée(s) et les modalités pour rendre compte de leur exécution périodique ;
* leurs conditions de rémunération ;
* le terme du mandat ou les autres modalités par lesquelles il prend fin.

*Pour aller plus loin* : article R. 7121-6 du Code du travail.

#### Rémunération de l’agent artistique

Les sommes que les agents artistiques peuvent percevoir en rémunération de leurs services et notamment du placement se calculent en pourcentage sur l'ensemble des rémunérations de l'artiste (rémunérations fixes ou proportionnelles à l’exploitation). Ce pourcentage ne peut excéder 10 % du montant des rémunérations brutes de l’artiste. Si l’agent artistique intervient en matière d’organisation et de développement de la carrière de l’artiste, conformément aux usages professionnels en vigueur, sa rémunération peut atteindre 15 % du montant des rémunérations brutes de l’artiste.

Ces sommes peuvent, par accord entre l'agent artistique et l'artiste du spectacle, être en tout ou partie mises à la charge de l'artiste. Dans ce cas, l'agent artistique donne quittance à l'artiste du paiement opéré par ce dernier. Néanmoins, la rémunération de l’agent artistique peut également être mise à la charge de l’employeur de l’artiste, en tout ou partie. Il appartient à l’artiste et à son employeur de définir, dans le contrat de travail qui les unit, les modalités de la rémunération de l’agent.

*Pour aller plus loin* : articles L. 7121-13, D. 7121-7 et D. 7121-8 du Code du travail.

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public :

* en matière d’incendie ;
* en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

L’entreprise doit être déclarée au registre du commerce et des sociétés (RCS). Pour plus d’informations, il est recommandé de consulter les fiches « Formalités de déclaration d’une société commerciale » et « Immatriculation d’une entreprise commerciale au RCS ».

### b. Autorisation(s) post-immatriculation

Depuis le 1er janvier 2016, l’obligation d’inscription au registre national pour les agents artistiques a été supprimée.

*Pour aller plus loin* : ordonnance n° 2015-1682 du 17 décembre 2015 portant simplification de certains régimes d’autorisation préalable et de déclaration des entreprises et des professionnels.