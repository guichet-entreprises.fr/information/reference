﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS078" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourismus, Freizeit, Kultur" -->
<!-- var(title)="Presseunternehmen - Veröffentlichung von Zeitschriften" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourismus-freizeit-kultur" -->
<!-- var(title-short)="presseunternehmen-veroffentlichung" -->
<!-- var(url)="https://www.guichet-entreprises.fr/de/ds/tourismus-freizeit-kultur/presseunternehmen-veroffentlichung-von-zeitschriften.html" -->
<!-- var(last-update)="2020-04-15 17:24:18" -->
<!-- var(url-name)="presseunternehmen-veroffentlichung-von-zeitschriften" -->
<!-- var(translation)="Auto" -->


Presseunternehmen - Veröffentlichung von Zeitschriften
======================================================

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:24:18<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

### a. Definition

Das Presseunternehmen ermöglicht die Veröffentlichung von Zeitungen, Zeitschriften oder Zeitschriften, die der öffentlichkeits- oder einer bestimmten Öffentlichkeit zugänglich gemacht werden. Die Übertragung kann online von einem Fachmann erfolgen, der die redaktionelle Kontrolle über seine Inhalte hat und journalistisch behandelt wird.

### b. zuständigeCFE

Die betreffende CFE hängt von der Art der Struktur ab, in der die Tätigkeit ausgeübt wird:

- für Handelsunternehmen ist es die Industrie- und Handelskammer (IHK);
- für Zivilgesellschaften ist dies das Register des Handelsgerichts;
- für die Zivilgesellschaften in den Abteilungen Niederrhein, Oberrhein und Mosel ist dies das Register des Amtsgerichts.

**Gut zu wissen**

Die Tätigkeit gilt als kaufmännisch, solange das Unternehmen mehr als zehn Mitarbeiter beschäftigt (außer am Niederrhein, am Oberrhein und an der Mosel, wo die Tätigkeit unabhängig von der Anzahl der Mitarbeiter des Unternehmens handwerklich bleibt, sofern es verwendet kein industrielles Verfahren). Auf der anderen Seite, wenn das Unternehmen zehn oder weniger Mitarbeiter hat, wird seine Tätigkeit als handwerklich betrachtet. Schließlich, wenn der Profi eine Kauf- und Wiederverkaufstätigkeit hat, ist seine Tätigkeit sowohl handwerklich als auch kommerziell.

Zwei Grad. Installationsbedingungen
-----------------------------------

### a. Berufliche Qualifikationen

Für die Durchführung der Veröffentlichung von Zeitschriften ist kein spezifisches Diplom erforderlich. Dem Einzelnen wird jedoch empfohlen, über fundierte Kenntnisse des Geschäfts und der Buchhaltung sowie des Verlagsmarktes zu verfügen.

### b. Berufsqualifikationen - EU- oder EWR-Bürger (Freier Dienst oder freie Niederlassung)

Ein Staatsangehöriger eines Mitgliedstaats der Europäischen Union (EU) oder eine Vertragspartei des Europäischen Wirtschaftsraums (EWR) unterliegt denselben Bedingungen für berufliche Qualifikationen wie die Franzosen.

### c. Einige Besonderheiten der Regelung der Tätigkeit

#### Verpflichtung zur Einreichung einer ersten Erklärung der gesetzlichen Einzahlung

Die Pressefirma muss mit der rechtlichen Einreichung der zu veröffentlichenden Werke fortfahren, von der Nationalbibliothek Frankreichs, dem Nationalen Filmzentrum und dem Nationalen Audiovisuellen Institut nach der Veröffentlichung. Diese Anmeldung muss spätestens am Tag der Inbetriebnahme der Arbeiten erfolgen.

Sie wird von einer Erklärung in drei Exemplaren begleitet, deren Erwähnungen von der Art der veröffentlichten Arbeiten abhängen.

Bei der Einreichung digitaler Veröffentlichungen unterscheidet sich das Verfahren insbesondere von der gesetzlichen Hinterlegung von Büchern. Es muss nach den spezifischen Bedingungen von Websites erfolgen, die eine automatische Erfassung mit freier Computersoftware vorsehen.

*Um weiter zu gehen* Artikel L. 131-1, L. 131-2 und R. 131-1 bis R. 131-7 des Kulturerbe-Codes; Artikel 2 des Dekrets vom 12. Januar 1995, in dem die obligatorischen Verweise auf die Erklärungen zur rechtlichen Einreichung gedruckter, grafischer und fotografischer Dokumente enthalten sind.

### Inhalt der veröffentlichten Veröffentlichungen

Das Presseunternehmen, das Zeitschriften für junge Menschen veröffentlicht, darf keine Inhalte verbreiten, die pornografisch sind oder Ideen von Hass, Diskriminierung oder Angriff auf die Menschenwürde vermitteln können.

*Um weiter zu gehen* Gesetz 49-956 vom 16. Juli 1949 über Veröffentlichungen für Jugendliche.

Drei Grad. Installationsverfahren und Formalitäten
--------------------------------------------------

### a. Meldeformalitäten des Unternehmens

Der Auftragnehmer muss sich beim Handels- und Unternehmensregister (SCN) registrieren. Es ist ratsam, sich auf die "Commercial Company Reporting Formalities" und "Registration of a Commercial Individual Company at the RCS" für weitere Informationen zu beziehen.

### b. Meldeformalitäten des Unternehmens

**Zuständige Behörde**

Der Leiter einer Druckerei muss sein Unternehmen melden, und dazu muss eine Erklärung an den IStGH.

**Belege**

Der Betroffene muss die erforderlichen Belege je nach Art seiner Tätigkeit vorlegen.

**Zeit**

Das Geschäftsformalitätszentrum des IStGH sendet die fehlenden Dokumente gegebenenfalls am selben Tag an den Fachmann, gegebenenfalls die fehlenden Dokumente, die in den Akten sind. Der Profi hat dann eine Frist von fünfzehn Tagen, um es abzuschließen.

Sobald die Akte vollständig ist, teilt der IStGH dem Antragsteller mit, an welche Stellen seine Akte weitergeleitet wurde.

**Heilmittel**

Der Anmelder kann die Akteneinsicht einholen, sofern sie nicht zu den vorgenannten Fristen eingereicht worden ist. Verweigert das Zentrum für Geschäftsformalitäten den Erhalt der Akte, so hat der Kläger Beschwerde beim Verwaltungsgericht.

**Kosten**

Die Kosten dieser Erklärung hängen von der Rechtsform des Unternehmens ab.

*Um weiter zu gehen* Abschnitt 635 des Allgemeinen Steuergesetzbuches.

### c. Registrieren Sie gegebenenfalls die Satzung der Gesellschaft

Der Leiter einer Pressegesellschaft muss diese nach Derdatum und Unterzeichnung der Satzung beim Körperschaftsteueramt (IES) anmelden, wenn:

- Das Gesetz betrifft eine bestimmte Transaktion, die der Registrierung unterliegt;
- wenn die Form der Handlung dies erfordert.

**Zuständige Behörde**

Die Registrierungsstelle ist die Registrierungsstelle des EIS.

**Belege**

Der Fachmann hat dem EIS zwei Exemplare der Satzung vorzulegen.

*Um weiter zu gehen* Abschnitt 635 des Allgemeinen Steuergesetzbuches.

