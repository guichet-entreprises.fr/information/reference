﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS078" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Empresa de prensa - Publicación de publicaciones periódicas" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="empresa-de-prensa-publicacion" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/empresa-de-prensa-publicacion-de-publicaciones-periodicas.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="empresa-de-prensa-publicacion-de-publicaciones-periodicas" -->
<!-- var(translation)="Auto" -->


Empresa de prensa - Publicación de publicaciones periódicas
===========================================================

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

### a. Definición

La empresa de prensa permite la publicación de periódicos, revistas o publicaciones periódicas puestos a disposición del público en general o de un público en particular. La radiodifusión puede ser realizada en línea por un profesional que tiene control editorial de su contenido, que es objeto de tratamiento periodístico.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad:

- para las empresas comerciales, es la Cámara de Comercio e Industria (CCI);
- para las sociedades civiles, este es el registro del Tribunal de Comercio;
- para las sociedades civiles en los departamentos del Bajo Rin, el Alto Rin y el Mosela, este es el registro del tribunal de distrito.

**Es bueno saber**

La actividad se considera comercial siempre y cuando la empresa tenga más de diez empleados (excepto en el Bajo Rin, el Alto Rin y el Mosela, donde la actividad sigue siendo artesanal independientemente del número de empleados de la empresa, siempre que no utiliza un proceso industrial). Por otro lado, si la empresa tiene diez o menos empleados, su actividad se considera artesanal. Por último, si el profesional tiene una actividad de compra y reventa, su actividad es tanto artesanal como comercial.

Dos grados. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere un diploma específico para llevar a cabo la actividad de publicación de publicaciones periódicas. Sin embargo, se aconseja al individuo tener un conocimiento sólido del negocio y la contabilidad, así como del mercado editorial.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o una parte en el Espacio Económico Europeo (EEE) está sujeto a las mismas condiciones de cualificación profesional que los franceses.

### c. Algunas peculiaridades de la regulación de la actividad

#### Requisito de presentar una declaración inicial de depósito legal

La empresa de prensa debe proceder a la presentación legal de las obras a publicar, desde la Biblioteca Nacional de Francia, el Centro Nacional de Cine y el Instituto Nacional Audiovisual según la publicación. Esta presentación debe tener lugar a más tardar el día en que se ponga en circulación el trabajo.

Se acompaña de una declaración hecha en tres copias cuyas menciones dependen del tipo de trabajo publicado.

Para la presentación de publicaciones digitales, el procedimiento es diferente del depósito legal de libros, en particular. Debe hacerse de acuerdo con los términos específicos de los sitios web que prevén la recopilación automática utilizando software informático libre.

*Para ir más allá* Artículos L. 131-1, L. 131-2, y R. 131-1 a R. 131-7 del Código del Patrimonio; Artículo 2 del Decreto de 12 de enero de 1995 por el que se establecen las referencias obligatorias sobre las declaraciones que acompañan a la presentación legal de documentos impresos, gráficos y fotográficos.

### Contenido de las publicaciones publicadas

La empresa de prensa que publica publicaciones periódicas dedicadas a los jóvenes no debe difundir contenidos pornográficos o que puedan transmitir ideas de odio, discriminación o ataque a la dignidad humana.

*Para ir más allá* Ley 49-956, de 16 de julio de 1949, de publicaciones para jóvenes.

Tres grados. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El contratista debe registrarse en el Registro Mercantil y Corporativo (SCN). Es aconsejable consultar las "Formalidades de Reporte de Empresas Comerciales" y "Registro de una Empresa Comercial Individual en el RCS" para obtener más información.

### b. Formalidades de notificación de la empresa

**Autoridad competente**

El jefe de una empresa de prensa debe informar de su empresa, y para ello debe hacer una declaración a la CPI.

**Documentos de apoyo**

El interesado deberá facilitar los documentos justificativos requeridos en función de la naturaleza de su actividad.

**hora**

El centro de formalidades comerciales de la CPI envía los documentos que faltan al profesional el mismo día, según corresponda, los documentos que faltan en el archivo. El profesional tiene entonces un período de quince días para completarlo.

Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores. Si el centro de formalidades comerciales se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### c. Si es necesario, registre los estatutos de la empresa

El responsable de una empresa de prensa deberá, una vez fechados y firmados los estatutos de la sociedad, registrarlos en la Oficina del Impuesto sobre Sociedades (IES) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es el brazo de registro de la EIS.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

