﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS078" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Entreprise de presse – Publication de périodiques" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="entreprise-de-presse-publication" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/entreprise-de-presse-publication-de-periodiques.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="entreprise-de-presse-publication-de-periodiques" -->
<!-- var(translation)="None" -->


# Entreprise de presse – Publication de périodiques

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'entreprise de presse permet la publication de journaux, revues ou encore périodiques mis à la disposition du grand public ou d'un public en particulier. La diffusion peut se faire en ligne par un professionnel qui a la maîtrise éditoriale de son contenu, qui fait l'objet d'un traitement journalistique.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

* pour les sociétés commerciales, il s’agit de la chambre de commerce et d'industrie (CCI) ;
* pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
* pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucun diplôme spécifique n'est requis pour exercer l'activité consistant à publier des périodiques. Toutefois, il est conseillé à l'intéressé d'avoir de solides connaissances commerciales et comptables, ainsi que du marché de l'édition.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE) est soumis aux mêmes conditions de qualifications professionnelles que les Français.

### c. Quelques particularités de la réglementation de l'activité

#### Obligation d'effectuer une déclaration initiale de dépôt légal

L'entreprise de presse doit procéder au dépôt légal des œuvres à paraître, auprès de la Bibliothèque nationale de France, du Centre national du cinéma et de l'Institut national de l'audiovisuel selon la publication. Ce dépôt doit intervenir au plus tard le jour de la mise en circulation de l’œuvre.

Il est accompagné d'une déclaration établie en trois exemplaires dont les mentions dépendent du type d'œuvre éditée.

Pour le dépôt des publications numériques, la procédure est différente du dépôt légal des livres, notamment. Elle doit être faite selon les modalités propres aux sites web qui prévoient une collecte automatique à l'aide d'un logiciel informatique libre.

*Pour aller plus loin* : articles L. 131-1, L. 131-2, et R. 131-1 à R. 131-7 du Code du patrimoine ; article 2 de l'arrêté du 12 janvier 1995 fixant les mentions obligatoires figurant sur les déclarations accompagnant le dépôt légal des documents imprimés, graphiques et photographiques.

### Contenu des publications diffusées

L'entreprise de presse qui publie des périodiques dédiés à la jeunesse ne doit pas diffuser des contenus à caractère pornographique ou susceptibles de véhiculer des idées de haine, discriminantes ou encore d'atteinte à la dignité humaine.

*Pour aller plus loin* : loi n° 49-956 du 16 juillet 1949 sur les publications destinées à la jeunesse.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

L’entrepreneur doit s’immatriculer au registre du commerce et des sociétés (RCS). Il est conseillé de se reporter aux fiches « Formalités de déclaration d'une société commerciale » et « Immatriculation d'une entreprise individuelle commerciale au RCS » pour de plus amples informations.

### b. Formalités de déclaration de l’entreprise

**Autorité compétente**

Le dirigeant d'une entreprise de presse doit procéder à la déclaration de son entreprise, et doit pour cela effectuer une déclaration auprès de la CCI.

**Pièces justificatives**

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

**Délais**

Le centre des formalités des entreprises de la CCI adresse le jour même, un récépissé au professionnel dressant le cas échéant, les pièces manquantes au dossier. Le professionnel dispose alors d'un délai de quinze jours pour le compléter.

Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

**Voies de recours**

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus. Dès lors que le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

**Coût**

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### c. Le cas échéant, enregistrer les statuts de la société

Le dirigeant d'une entreprise de presse doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

* l'acte comporte une opération particulière soumise à un enregistrement ;
* si la forme même de l'acte l'exige.

**Autorité compétente**

L'autorité compétente en matière d'enregistrement est le pôle enregistrement du SIE.

**Pièces justificatives**

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.