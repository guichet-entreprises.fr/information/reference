﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS078" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Press company - Publication of periodicals" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="press-company-publication-periodicals" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/press-company-publication-of-periodicals.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="press-company-publication-of-periodicals" -->
<!-- var(translation)="Auto" -->


Press company - Publication of periodicals
==========================================

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Defining the activity
------------------------

### a. Definition

The press company allows the publication of newspapers, magazines or periodicals made available to the general public or to a particular public. Broadcasting can be done online by a professional who has editorial control of its content, who is the subject of journalistic treatment.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

Two degrees. Installation conditions
------------------------------------

### a. Professional qualifications

No specific diploma is required to carry out the activity of publishing periodicals. However, the individual is advised to have a solid knowledge of the business and accounting, as well as the publishing market.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Free Establishment)

A national of a Member State of the European Union (EU) or a party to the European Economic Area (EEA) is subject to the same conditions of professional qualifications as the French.

### c. Some peculiarities of the regulation of the activity

#### Requirement to file an initial declaration of legal deposit

The press company must proceed with the legal filing of the works to be published, from the National Library of France, the National Film Centre and the National Audiovisual Institute according to the publication. This filing must take place no later than the day the work is put into circulation.

It is accompanied by a statement made in three copies whose mentions depend on the type of work published.

For the filing of digital publications, the procedure is different from the legal deposit of books, in particular. It must be done according to the specific terms of websites that provide for automatic collection using free computer software.

*To go further* Articles L. 131-1, L. 131-2, and R. 131-1 to R. 131-7 of the Heritage Code; Article 2 of the decree of 12 January 1995 setting out the mandatory references on the declarations accompanying the legal filing of printed, graphic and photographic documents.

### Content of published publications

The press company that publishes periodicals dedicated to young people must not disseminate content that is pornographic or that can convey ideas of hatred, discrimination or attack on human dignity.

*To go further* Law 49-956 of 16 July 1949 on publications for young people.

Three degrees. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The contractor must register with the Trade and Corporate Register (SCN). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Company reporting formalities

**Competent authority**

The head of a press company must report his company, and to do so must make a declaration to the ICC.

**Supporting documents**

The person concerned must provide the required supporting documents depending on the nature of his activity.

**Time**

The ICC's business formalities centre sends the missing documents to the professional on the same day, as appropriate, the missing documents on file. The professional then has a period of fifteen days to complete it.

Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines. If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*To go further* Section 635 of the General Tax Code.

### c. If necessary, register the company's statutes

The head of a press company must, once the company's statutes have been dated and signed, register them with the Corporate Tax Office (IES) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is the registration arm of the EIS.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*To go further* Section 635 of the General Tax Code.

