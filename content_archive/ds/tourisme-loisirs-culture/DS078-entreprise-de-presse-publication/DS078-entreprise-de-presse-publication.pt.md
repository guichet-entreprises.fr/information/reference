﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS078" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Lazer, Cultura" -->
<!-- var(title)="Empresa de imprensa - Publicação de periódicos" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-lazer-cultura" -->
<!-- var(title-short)="empresa-de-imprensa-publicacao" -->
<!-- var(url)="https://www.guichet-entreprises.fr/pt/ds/turismo-lazer-cultura/empresa-de-imprensa-publicacao-de-periodicos.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="empresa-de-imprensa-publicacao-de-periodicos" -->
<!-- var(translation)="Auto" -->


Empresa de imprensa - Publicação de periódicos
==============================================

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

### a. Definição

A assessoria de imprensa permite a publicação de jornais, revistas ou periódicos disponibilizados ao público em geral ou a um público específico. A transmissão pode ser feita online por um profissional que tem controle editorial de seu conteúdo, que é objeto de tratamento jornalístico.

### b. CFE competente

A CFE relevante depende da natureza da estrutura em que a atividade é realizada:

- para empresas comerciais, é a Câmara de Comércio e Indústria (CCI);
- para as sociedades civis, este é o registro do Tribunal Comercial;
- para sociedades civis nos departamentos do Reno Inferior, Alto Reno e Mosela, este é o registro do tribunal distrital.

**É bom saber**

A atividade é considerada comercial desde que a empresa tenha mais de dez funcionários (exceto no Reno Inferior, Alto Reno e Moselle, onde a atividade permanece artesanal, independentemente do número de funcionários da empresa desde que ela não utiliza um processo industrial). Por outro lado, se a empresa possui dez ou menos funcionários, sua atividade é considerada artesanal. Por fim, se o profissional tem uma atividade de compra e revenda, sua atividade é artesanal e comercial.

Dois graus. Condições de instalação
-----------------------------------

### a. Qualificações profissionais

Não é necessário diploma específico para exercer a atividade de publicação de periódicos. No entanto, o indivíduo é aconselhado a ter um sólido conhecimento do negócio e da contabilidade, bem como do mercado editorial.

### b. Qualificações Profissionais - Cidadãos da UE ou EEE (Serviço Livre ou Estabelecimento Livre)

Um cidadão de um Estado-Membro da União Europeia (UE) ou uma parte da Área Econômica Europeia (EEE) está sujeito às mesmas condições de qualificação profissional que os franceses.

### c. Algumas peculiaridades da regulação da atividade

#### Exigência de apresentar uma declaração inicial de depósito legal

A assessoria de imprensa deve proceder com o arquivamento legal das obras a serem publicadas, da Biblioteca Nacional da França, do Centro Nacional de Cinema e do Instituto Nacional do Audiovisual, de acordo com a publicação. Este arquivamento deve ocorrer no mais tarde do dia em que a obra for colocada em circulação.

Acompanha-se uma declaração feita em três exemplares cujas menções dependem do tipo de trabalho publicado.

Para o arquivamento de publicações digitais, o procedimento é diferente do depósito legal de livros, em particular. Deve ser feito de acordo com os termos específicos dos sites que preveem a coleta automática usando software de computador gratuito.

*Para ir mais longe* Artigos L. 131-1, L. 131-2 e R. 131-1 a R. 131-7 do Código patrimonial; Artigo 2º do decreto de 12 de janeiro de 1995 que estabelece as referências obrigatórias sobre as declarações que acompanham o arquivamento legal de documentos impressos, gráficos e fotográficos.

### Conteúdo de publicações publicadas

A empresa de imprensa que publica periódicos dedicados aos jovens não deve divulgar conteúdo pornográfico ou que possa transmitir ideias de ódio, discriminação ou ataque à dignidade humana.

*Para ir mais longe* Lei 49-956, de 16 de julho de 1949, sobre publicações para jovens.

Três graus. Procedimentos de instalação e formalidades
------------------------------------------------------

### a. Formalidades de relatórios da empresa

O contratante deve se cadastrar no Registro Comercial e Societário (SCN). É aconselhável consultar as "Formalidades de Relatório de Empresas Comerciais" e "Registro de uma Empresa Individual Comercial no RCS" para obter mais informações.

### b. Empresa de relatórios formalidades

**Autoridade competente**

O chefe de uma empresa de imprensa deve informar sua empresa, e para isso deve fazer uma declaração ao ICC.

**Documentos de suporte**

O interessado deve fornecer os documentos de suporte necessários, dependendo da natureza de sua atividade.

**Tempo**

O centro de formalidades comerciais do ICC envia os documentos faltantes ao profissional no mesmo dia, conforme apropriado, os documentos faltantes no arquivo. O profissional então tem um prazo de quinze dias para completá-lo.

Uma vez que o arquivo esteja concluído, o ICC informa ao requerente para quais agências seu arquivo foi encaminhado.

**Remédios**

O requerente poderá obter a devolução de seu processo desde que não tenha sido apresentado durante os prazos acima. Se o centro de formalidades empresariais se recusar a receber o processo, o requerente tem um recurso ao tribunal administrativo.

**Custo**

O custo desta declaração depende da forma legal da empresa.

*Para ir mais longe* Seção 635 do Código Tributário Geral.

### c. Se necessário, registre os estatutos da empresa

O chefe de uma empresa de imprensa deve, uma vez que os estatutos da empresa tenham sido datados e assinados, registrá-los no Escritório de Tributação Empresarial (IES) se:

- O ato envolve uma determinada transação sujeita ao registro;
- se a própria forma do ato exige isso.

**Autoridade competente**

A autoridade de registro é o braço de registro do EIS.

**Documentos de suporte**

O profissional deverá submeter duas cópias do estatuto ao EIS.

*Para ir mais longe* Seção 635 do Código Tributário Geral.

