﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS078" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Tempo libero, Cultura" -->
<!-- var(title)="Società di stampa - Pubblicazione di periodici" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-tempo-libero-cultura" -->
<!-- var(title-short)="societa-di-stampa-pubblicazione" -->
<!-- var(url)="https://www.guichet-entreprises.fr/it/ds/turismo-tempo-libero-cultura/societa-di-stampa-pubblicazione-di-periodici.html" -->
<!-- var(last-update)="2020-04-15 17:24:17" -->
<!-- var(url-name)="societa-di-stampa-pubblicazione-di-periodici" -->
<!-- var(translation)="Auto" -->


Società di stampa - Pubblicazione di periodici
==============================================

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:24:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

### a. Definizione

La società di stampa consente la pubblicazione di giornali, riviste o periodici messi a disposizione del pubblico o di un particolare pubblico. La trasmissione può essere effettuata online da un professionista che ha il controllo editoriale del suo contenuto, che è oggetto di trattamento giornalistico.

### b. CFE competente

Il CFE in questione dipende dalla natura della struttura in cui viene svolta l'attività:

- per le imprese commerciali, è la Camera di Commercio e Industria (CCI);
- per le società civili, questo è il registro del Tribunale commerciale;
- per le società civili nei dipartimenti del Basso Reno, Alto Reno e Mosella, questo è il registro del tribunale distrettuale.

**Buono a sapersi**

L'attività è considerata commerciale fintanto che l'azienda ha più di dieci dipendenti (ad eccezione del Basso Reno, dell'Alto Reno e della Mosella, dove l'attività rimane artigianale indipendentemente dal numero di dipendenti dell'azienda, a condizione che non utilizza un processo industriale). D'altra parte, se l'azienda ha dieci o meno dipendenti, la sua attività è considerata artigianale. Infine, se il professionista ha un'attività di acquisto e rivendita, la sua attività è sia artigianale che commerciale.

Due gradi. Condizioni di installazione
--------------------------------------

### a. Qualifiche professionali

Non è richiesto alcun diploma specifico per svolgere l'attività di pubblicazione di periodici. Tuttavia, l'individuo è consigliato di avere una solida conoscenza del business e della contabilità, così come il mercato editoriale.

### b. Qualifiche professionali - Cittadini dell'UE o del CEE (Servizio gratuito o Stabilimento gratuito)

Un cittadino di uno Stato membro dell'Unione europea (UE) o di un partito allo Spazio economico europeo (AEA) è soggetto alle stesse condizioni di qualifiche professionali dei francesi.

### c. Alcune peculiarità della regolamentazione dell'attività

#### Obbligo di presentare una dichiarazione iniziale di deposito legale

La società di stampa deve procedere con il deposito legale delle opere da pubblicare, dalla Biblioteca Nazionale di Francia, dal Centro Nazionale del Cinema e dall'Istituto Nazionale di Audiovisivo secondo la pubblicazione. Tale deposito deve avvenire entro e non oltre il giorno in cui il lavoro viene messo in circolazione.

Essa è accompagnata da una dichiarazione fatta in tre copie le cui menzioni dipendono dal tipo di lavoro pubblicato.

Per il deposito di pubblicazioni digitali, la procedura è diversa dal deposito legale dei libri, in particolare. Deve essere fatto secondo i termini specifici dei siti web che prevedono la raccolta automatica utilizzando software libero del computer.

*Per andare oltre* Articoli L. 131-1, L. 131-2 e Da 131-1 a R. 131-7 del Codice Patrimonio; Articolo 2 del decreto del 12 gennaio 1995 che illustra i riferimenti obbligatori sulle dichiarazioni che accompagnano il deposito legale di documenti stampati, grafici e fotografici.

### Contenuto delle pubblicazioni pubblicate

La società di stampa che pubblica periodici dedicati ai giovani non deve diffondere contenuti pornografici o in grado di trasmettere idee di odio, discriminazione o attacco alla dignità umana.

*Per andare oltre* Legge 49-956 del 16 luglio 1949 sulle pubblicazioni per i giovani.

Tre gradi. Procedure di installazione e formalità
-------------------------------------------------

### a. Le formalità di rendicontazione delle società

Il contraente deve registrarsi al Registro del Commercio e delle Imprese (SCN). Si consiglia di fare riferimento alle "Commercial Company Reporting Formalities" e "Registration of a Commercial Individual Company presso l'RCS" per ulteriori informazioni.

### b. Formalità di rendicontazione delle società

**Autorità competente**

Il capo di una società di stampa deve segnalare la sua società, e per farlo deve fare una dichiarazione alla Corte penale internazionale.

**Documenti di supporto**

L'interessato deve fornire i documenti giustificativi richiesti a seconda della natura della sua attività.

**Tempo**

Il Centro formalità commerciali della Corte penale internazionale invia i documenti mancanti al professionista lo stesso giorno, a seconda dei casi, i documenti mancanti in archivio. Il professionista ha quindici giorni per completarlo.

Una volta completato il fascicolo, la Corte penale internazionale comunica al richiedente a quali agenzie è stato inoltrato il fascicolo.

**Rimedi**

Il richiedente può ottenere la restituzione del suo fascicolo, purché non sia stato presentato durante i termini di cui sopra. Se il centro di formalità imprenditoriale si rifiuta di ricevere il fascicolo, il richiedente ha un ricorso dinanzi al tribunale amministrativo.

**Costo**

Il costo di questa dichiarazione dipende dalla forma giuridica della società.

*Per andare oltre* sezione 635 del codice tributario generale.

### c. Se necessario, registrare gli statuti della società

Il capo di una società di stampa deve, una volta che gli statuti della società sono stati datati e firmati, registrarli presso l'Ufficio delle imposte aziendali (IES) se:

- L'atto riguarda una particolare transazione soggetta a registrazione;
- se la forma stessa dell'atto lo richiede.

**Autorità competente**

L'autorità di registrazione è il braccio di registrazione del SIE.

**Documenti di supporto**

Il professionista deve presentare due copie degli statuti al SIE.

*Per andare oltre* sezione 635 del codice tributario generale.

