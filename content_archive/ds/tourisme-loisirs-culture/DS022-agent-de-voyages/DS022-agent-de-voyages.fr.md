﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS022" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Agent de voyages" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="agent-de-voyages" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/agent-de-voyages.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agent-de-voyages" -->
<!-- var(translation)="None" -->

# Agent de voyages

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’opérateur de voyages et de séjours (agence de voyage, par exemple) est un professionnel qui élabore et vend ou offre à la vente :

- des forfaits touristiques ;
- des services de voyage portant sur le transport, l'hébergement, la location de véhicule ou d'autres services de voyage qu'ils ne produisent pas eux-mêmes.

Il s'applique également aux professionnels qui facilitent aux voyageurs l'achat de « prestations de voyage liées ».

*Pour aller plus loin* : articles L. 211-1 et L. 211-2 du Code du tourisme.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée. Dans le cas d’un opérateur de voyages et de séjours, il s’agit d’une activité commerciale. Le CFE compétent est donc la chambre de commerce et d’industrie (CCI).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucun diplôme, ni formation particulière n’est exigé pour exercer l’activité d’opérateur de voyages et de séjour depuis le 1er janvier 2016. Seules les conditions d'assurance de responsabilité civile professionnelle et de garantie financière sont nécessaires pour exercer cette activité.

*Pour aller plus loin* : ordonnance n° 2015-1682 du 17 décembre 2015 portant simplification de certains régimes d'autorisation préalable et de déclaration des entreprises et des professionnels.

### b. Garantie financière

Disposer d’une garantie financière suffisante est un prérequis obligatoire à l'immatriculation au registre des opérateurs de voyages et de séjour. Cette immatriculation conditionne l’exercice légal de la profession en France.

La garantie financière vise à assurer, le cas échéant, le remboursement des fonds et des avances versées par les clients à l’opérateur de voyages et de séjour pour des prestations en cours ou à venir. Elle permet également de garantir le rapatriement des clients dans le cas d’une situation de cessation des paiements ayant entraîné le dépôt de bilan de opérateur de voyages et de séjours.

La garantie financière résulte d’un contrat de cautionnement pris, au choix, par :

- un organisme de garantie collective ;
- un établissement de crédit, une société de financement ou une entreprise d’assurances habilité à donner une garantie financière ;
- un groupement d’associations ou d’organismes sans caractère lucratif ayant reçu une autorisation particulière par arrêté du ministre en charge du tourisme et disposant d’un fonds de solidarité suffisant.

Le montant de la garantie financière  doit couvrir l'intégralité des fonds versés par les clients et les frais de rapatriement des voyageurs en cas de défaillance de l'agence.

La souscription à une garantie financière donne lieu à la remise d’une attestation qui doit obligatoirement préciser :

- la dénomination du garant ;
- les dates de validité du contrat de garantie.

*Pour aller plus loin* : article R. 211-26 et suivants du Code du tourisme ; décret n° 2015-1111 du 2 septembre 2015 relatif à la garantie financière et à la responsabilité civile professionnelle des opérateur de voyages et de séjour et autres opérateurs de la vente de voyages et de séjours.

### c. Assurance de responsabilité civile

Pour exercer légalement en France, l’opérateur de voyages et de séjour a l’obligation de disposer d’une assurance de responsabilité civile professionnelle.

L'assurance de responsabilité civile professionnelle permet de prendre en charge la réparation d'un dommage causé par l'opérateur à des clients, à des prestataires ou à des tiers par suite de fautes, erreurs, omissions ou négligences commises à l'occasion de la vente ou de l'exécution des prestations touristiques.

L'opérateur de voyages et de séjour doit indiquer clairement à ses clients les risques couverts et les garanties souscrites au titre de cette assurance.

Une fois assuré, l’opérateur de voyages et de séjour reçoit une attestation de souscription dont un modèle est disponible sur le [site d’Atout France](https://registre-operateurs-de-voyages.atout-france.fr).

L’attestation doit impérativement porter les mentions suivantes :

- la référence aux dispositions légales et réglementaires en vigueur ;
- la raison sociale de l'entreprise d'assurances ;
- le numéro du contrat d'assurance souscrit ;
- la période de validité du contrat (début et fin de validité) ;
- le nom et l'adresse ou la dénomination sociale et l'adresse de l'opérateur de voyages assuré ;
- l'étendue des garanties.

*Pour aller plus loin* : article L. 211-16 et suivants et R. 211-35 et suivants du Code du tourisme.

### d. Quelques particularités de la réglementation de l’activité

#### Obligation de tenir les livres et documents à la disposition des agents habilités à les consulter

Les personnes physiques ou morales inscrites au registre d'immatriculation des opérateur de voyages et de séjour, doivent tenir leurs livres et documents à la disposition des agents habilités pour consultation.

*Pour aller plus loin* : article L. 211-5 du Code du tourisme.

#### Obligation de mentionner l’inscription au registre d’immatriculation des opérateurs de voyage et de séjours

Les opérateur de voyages et de séjour ont l’obligation de mentionner leur inscription au registre d’immatriculation des opérateurs de voyages et de séjours sur leur enseigne, dans les documents remis aux tiers et dans leur publicité.

*Pour aller plus loin* : articles L. 211-5 et R. 211-2 du Code du tourisme.

#### Règles relatives à la conclusion et à l’exécution des contrats de vente de voyages et de séjours

La conclusion de contrats de vente de voyage ou de séjours par un opérateur de voyages et de séjour doit respecter certaines conditions de forme et de fond détaillées aux articles L. 211-7 et suivants du Code du tourisme. Parmi ces conditions, on peut citer l’obligation de remise d’informations précontractuelles par l’opérateur de voyages et de séjour au client, les mentions obligatoires devant figurer dans le contrat (description des prestations fournies, droits et obligations réciproques des parties en matière de prix, de calendrier, de modalités de paiement, etc.) ou les conditions de modification du contrat.

Pour plus d’informations, il est conseillé de se reporter aux articles L. 211-7 et suivants du Code du tourisme.

#### Contrat de jouissance d’immeuble à temps partagé

Les opérateur de voyages et de séjour immatriculés au registre des opérateurs de voyages peuvent conclure tout contrat de jouissance d'immeuble à temps partagé ou prêter leur concours à la conclusion de tels contrats en vertu d’un mandat écrit.

Pour plus d’informations concernant les mentions obligatoires du contrat de mandat ainsi que les conditions de rémunération du mandataire, il est conseillé de se référer à l’article L. 211-24 du Code du tourisme.

*Pour aller plus loin* : articles L. 211-24, R. 211-45 et suivants du Code du tourisme.

#### Obligation d'information des passagers aériens sur l'identité du transporteur aérien

Pour les prestations incluant un transport aérien dans un forfait touristique, les opérateur de voyages et de séjour  doivent transmettre au consommateur, pour chaque tronçon de vol, une liste comprenant au maximum trois transporteurs, au nombre desquels figurent le transporteur contractuel et le transporteur de fait auquel l'organisateur du voyage aura éventuellement recours.

*Pour aller plus loin* : article R. 211-15 du Code du tourisme.

#### Agréments de l'International air transport association (IATA) et de la SNCF

Dans certains cas, les opérateur de voyages et de séjour doivent obtenir un agrément afin d’être habilités à réserver et à émettre des billets des compagnies aériennes ou ferroviaires. Pour plus d’informations, il est conseillé de se reporter à la rubrique « Service accréditation » du [site de l'IATA](http://www.iata.org/).

#### Réglementation générale applicable à tous les établissements recevant du public (ERP)

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Autorisation(s) post-immatriculation

#### Obligation d’immatriculation au registre des opérateurs de voyages et de séjours

Pour exercer légalement l’activité d’opérateur de voyages et de séjour, l’intéressé doit préalablement demander son immatriculation au registre des opérateurs de voyages et de séjours.

**Bon à savoir**

L’immatriculation au registre des opérateurs de voyages et de séjours doit être renouvelée tous les trois ans.

**Autorité compétente**

La demande d’immatriculation doit être adressée [en ligne](https://registre-operateurs-de-voyages.atout-france.fr/web/rovs/creation_compte#https://registre-operateurs-de-voyages.atout-france.fr/immatriculation/creationCompte?3) à la commission d’immatriculation d’Atout France – Agence de développement touristique de la France.

**Pièces justificatives**

La demande d’immatriculation doit être accompagnée des documents suivants :

- l’attestation d’assurance de responsabilité civile professionnelle, conforme au [modèle fourni par Atout-France](https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=b645d64b-4c68-4da2-9a98-76e03c0cd083&groupId=10157) ;
- l’attestation de garantie financière, conforme au [modèle fourni par Atout-France](%5b*https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=4387c0c1-8977-44f6-a797-eb0cedfa4172&groupId=10157*%5d(https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=4387c0c1-8977-44f6-a797-eb0cedfa4172&groupId=10157)).

**Délais**

La commission d’immatriculation d’Atout-France adresse, par courriel, au demandeur un récépissé attestant de la complétude de son dossier. À compter de la date du récépissé, la commission dispose d’un délai d’un mois pour :

- décider de procéder à l’immatriculation du demandeur. Dans ce cas, un certificat d’immatriculation comportant le numéro d’immatriculation et la date d’enregistrement est adressé au demandeur par courriel ;
- ou refuser de procéder à l’immatriculation du demandeur. Dans ce cas, cette décision motivée doit lui être adressée par lettre recommandée avec avis de réception.

L’absence de décision de la commission dans le délai d’un mois suivant le récépissé vaut acceptation de l’immatriculation.

**Coût**

Il n'y a plus de frais d'immatriculation.

#### Le renouvellement d’immatriculation au registre des opérateurs de voyages et de séjours

L’immatriculation est valable trois ans, son renouvellement est soumis aux mêmes conditions que l’immatriculation.

L’opérateur reçoit, trois mois avant la fin de la validité de son immatriculation, une relance par courriel et courrier l’invitant à procéder à la demande de renouvellement.

**Autorité compétente**

La demande de renouvellement doit être adressée à la commission d’immatriculation d’Atout-France précédant la date de fin de validité de l’immatriculation.

**Modalités de renouvellement**

L’opérateur doit procéder à la demande de renouvellement au moins 30 jours avant la date d’échéance.

**Pièces justificatives**

- l'attestation de garantie financière en cours de validité ;
- l'attestation d'assurance de responsabilité civile professionnelle en cours de validité.

**Coût**

Il n'y a plus de frais de renouvellement.

#### Modifications des données du dossier

L’opérateur est tenu d’informer la commission d’immatriculation de tout changement dans les éléments saisis lors de la demande d’immatriculation. Les changements peuvent concerner la forme juridique de la société, l’adresse, le garant, etc.

**Modalités de modification**

La modification doit être impérativement notifiée dans le mois qui la précède quand elle peut être anticipée ou, à défaut, dans le mois qui suit l'événement au plus tard.

**Autorité compétente**

La modification doit être portée à la connaissance de la commission d'immatriculation.

Les modifications validées par cette commission entraînent une mise à jour de la fiche descriptive de l'opérateur de voyages et de séjour. Sa date de mise à jour fait foi et tient lieu d’information à destination tant du public que des autres autorités compétentes.

Pour plus d’informations, il est conseillé de se reporter au [site d’Atout-France](https://registre-operateurs-de-voyages.atout-france.fr/web/rovs/espace-professionnel#https://registre-operateurs-de-voyages.atout-france.fr/immatriculation/rechercheMenu?0).

### c. Effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité ponctuelle (Libre Prestation de Services)

Tout ressortissant d’un État de l’UE ou de l’EEE légalement établi dans l’un de ces États pour y exercer l’activité d’opérateur de voyages et de séjour  peut exercer cette activité en France de manière temporaire et occasionnelle à condition d’en faire la déclaration préalablement à toute prestation.

**À savoir**

La déclaration préalable emporte automatiquement et de façon temporaire, immatriculation de l’intéressé sur le registre des opérateurs de voyages.

La déclaration préalable, dans le cadre d’une libre prestation de services, doit être :

- actualisée en cours d’année, en cas de changement dans les éléments la composant ;
- renouvelée une fois par an, si la personne compte fournir des services d’une manière temporaire et occasionnelle au cours de l’année concernée.

**Autorité compétente**

La déclaration préalable est adressée, par tout moyen, à la commission d’immatriculation au sein d’Atout France.

**Délai de réponse**

La première déclaration préalable fait l’objet d’un accusé de réception communiqué au déclarant.

**Pièces justificatives**

La déclaration doit être accompagnée des pièces suivantes, traduites en français le cas échéant :

- une attestation certifiant que le déclarant personne physique est légalement établi dans un État de l’UE ou de l’EEE pour y exercer son activité ;
- une attestation de garantie financière suffisante délivrée par l’une des structures habilitées mentionnées à l’article R. 211-26 du Code du tourisme (cf. supra « 2°. c. Garantie financière) ;
- une information sur l’état de couverture du déclarant par une assurance garantissant les conséquences pécuniaires de la responsabilité civile professionnelle.

*Pour aller plus loin* : articles L. 211-20 à L. 211-22, R. 211-26, R. 211-51 et suivants du Code du tourisme.