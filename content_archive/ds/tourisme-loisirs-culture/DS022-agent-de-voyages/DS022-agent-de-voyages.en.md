﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS022" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Travel agent" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="travel-agent" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/travel-agent.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="travel-agent" -->
<!-- var(translation)="Auto" -->




Travel agent
============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The travel and travel operator (travel agency, for example) is a professional who develops and sells or offers for sale:

- tourist packages;
- travel services on transportation, accommodation, car rental or other travel services that they do not produce themselves.

It also applies to professionals who make it easier for travellers to purchase "related travel benefits."

*To go further* Articles L. 211-1 and L. 211-2 of the Tourism Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. In the case of a travel and stay operator, it is a commercial activity. The relevant CFE is therefore the Chamber of Commerce and Industry (CCI).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

No diploma or special training is required to carry out the activity of travel and residence operator since January 1, 2016. Only the conditions of professional liability insurance and financial guarantee are necessary to carry out this activity.

*To go further* : Ordinance No. 2015-1682 of December 17, 2015 simplifying certain pre-authorization and reporting regimes for companies and professionals.

### b. Financial guarantee

Having a sufficient financial guarantee is a mandatory prerequisite for registration in the register of travel and residence operators. This registration conditions the legal practice of the profession in France.

The financial guarantee is intended to ensure, if necessary, the reimbursement of funds and advances paid by customers to the travel and residence operator for current or future services. It also ensures the repatriation of customers in the event of a cessation of payments situation that has resulted in the bankruptcy filing of travel and holiday operators.

The financial guarantee is the result of a surety contract made, by choice, by:

- A collective guarantee body
- A credit institution, a finance company or an insurance company authorized to give a financial guarantee;
- a group of associations or non-profit organizations that have received special authorisation by decree from the Minister in charge of tourism and have a sufficient solidarity fund.

The amount of the financial guarantee must cover all funds paid by clients and the costs of repatriating travellers in the event of a failure of the agency.

The subscription to a financial guarantee gives rise to the delivery of a certificate which must specify:

- The name of the guarantor
- the validity dates of the warranty contract.

*To go further* Article R. 211-26 and following of the Tourism Code; Decree No. 2015-1111 of 2 September 2015 relating to the financial guarantee and professional civil liability of travel and holiday operators and other operators for the sale of travel and stays.

### c. Liability insurance

In order to practice legally in France, the travel and residence operator has the obligation to have professional liability insurance.

Professional liability insurance provides coverage for the repair of damage caused by the operator to customers, service providers or third parties as a result of faults, errors, omissions or negligence committed during the sale or execution of tourism services.

The travel and travel operator must clearly inform its customers of the risks covered and the guarantees underwritten under this insurance.

Once insured, the travel and travel operator receives a certificate of subscription, a model of which is available on the [Atout France website](https://registre-operateurs-de-voyages.atout-france.fr).

The certificate must bear the following mentions:

- Reference to existing legal and regulatory provisions;
- The name of the insurance company;
- The number of the insurance policy underwritten
- The validity period of the contract (beginning and end of validity);
- The name and address or name of the insured travel operator;
- the extent of the guarantees.

*To go further* Article L. 211-16 and R. 211-35 and following of the Tourism Code.

### d. Some peculiarities of the regulation of the activity

#### Obligation to keep books and documents available to authorized officers to consult them

Individuals or legal entities registered in the registration register of travel and residence operators, must keep their books and documents available to the authorized agents for consultation.

*To go further* Article L. 211-5 of the Tourism Code.

#### Requirement to mention the registration of travel and holiday operators in the registration register

Travel and holiday operators are obliged to mention their registration in the registration register of travel and holiday operators on their sign, in documents given to third parties and in their advertising.

*To go further* Articles L. 211-5 and R. 211-2 of the Tourism Code.

#### Rules for the conclusion and execution of travel and holiday sales contracts

The conclusion of contracts for the sale of travel or stays by a travel and residence operator must comply with certain conditions of form and substance detailed in articles L. 211-7 and following of the Tourism Code. These conditions include the obligation to provide pre-contract information by the travel and stay operator to the customer, the mandatory mentions to be included in the contract (description of the services provided, rights and reciprocal obligations of the parties in terms of price, timing, payment terms, etc.) or the terms of the contract change.

For more information, please refer to articles L. 211-7 and the following articles of the Tourism Code.

#### Timeshare building enjoyment contract

Travel and stay operators registered in the register of travel operators may enter into any contract for the enjoyment of a timeshare building or lend their assistance to the conclusion of such contracts under a written mandate.

For more information on the mandatory mentions of the mandate contract as well as the conditions of remuneration of the agent, it is advisable to refer to Article L. 211-24 of the Tourism Code.

*To go further* Articles L. 211-24, R. 211-45 and following of the Tourism Code.

#### Air passenger information requirement on the identity of the airline

For services including air transport in a tourist package, travel and travel operators must provide the consumer with a list of up to three carriers for each leg of flight, including a list of the contract carrier and the de facto carrier that the tour operator will eventually use.

*To go further* Article R. 211-15 of the Tourism Code.

#### Accreditations of the International Air Transport Association (IATA) and SNCF

In some cases, travel and travel operators must obtain an accreditation in order to be able to book and issue airline or rail tickets. For more information, please refer to the "Accreditation Service" section of the[IATA website](http://www.iata.org/).

#### General regulation applicable to all public institutions (ERP)

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, it is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the "Formality of Reporting a Commercial Company" activity sheet.

### b. Post-registration authorisation

#### Requirement to register travel and stay operators

In order to legally carry out the activity of a travel and residence operator, the person concerned must first apply for registration in the register of travel and holiday operators.

**Good to know**

Registration for travel and travel operators must be renewed every three years.

**Competent authority**

The application for registration must be addressed [online](https://registre-operateurs-de-voyages.atout-france.fr/web/rovs/creation_compte#https://registre-operateurs-de-voyages.atout-france.fr/immatriculation/creationCompte?3) to the registration commission of Atout France - French Tourism Development Agency.

**Supporting documents**

The application for registration must be accompanied by the following documents:

- certificate of professional liability insurance, in accordance with the [model provided by Trump-France](https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=b645d64b-4c68-4da2-9a98-76e03c0cd083&groupId=10157) ;
- financial guarantee certificate, in accordance with the [model provided by Trump-France](%5b*https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=4387c0c1-8977-44f6-a797-eb0cedfa4172&groupId=10157*%5d(https://registre-operateurs-de-voyages.atout-france.fr/c/document_library/get_file?uuid=4387c0c1-8977-44f6-a797-eb0cedfa4172&groupId=10157)).

**Timeframe**

The registration commission of Atout-France sends, by email, to the applicant a receipt attesting to the completeness of his file. From the date of receipt, the commission has one month to:

- decide to register the applicant. In this case, a registration certificate with the registration number and registration date is sent to the applicant by email;
- or refuse to register the applicant. In this case, this reasoned decision must be addressed to it by letter recommended with notice of receipt.

The failure of the commission to make a decision within one month of receipt is worth accepting the registration.

**Cost**

There are no more registration fees.

#### Renewal of registration in the register of travel and travel operators

The registration is valid for three years, its renewal is subject to the same conditions as the registration.

The operator receives, three months before the end of the validity of its registration, a relaunch by email and mail inviting it to proceed with the renewal application.

**Competent authority**

The renewal application must be sent to the registration commission of Atout-France before the end date of the registration.

**Renewal terms**

The operator must apply for renewal at least 30 days before the due date.

**Supporting documents**

- Valid financial guarantee certificate
- professional liability insurance certificate valid.

**Cost**

There are no more renewal fees.

#### Changes to file data

The operator is required to inform the registration board of any changes in the items seized during the registration application. Changes may relate to the legal form of the company, the address, the guarantor, etc.

**How to change**

The change must be notified within one month of the amendment when it can be anticipated or, failing that, within a month of the event at the latest.

**Competent authority**

The amendment must be made known to the registration board.

The changes validated by this commission result in an update of the travel and travel operator's description sheet. Its update date is authentic and serves as information for both the public and other relevant authorities.

For more information, it is advisable to refer to the [Atout-France website](https://registre-operateurs-de-voyages.atout-france.fr/web/rovs/espace-professionnel#https://registre-operateurs-de-voyages.atout-france.fr/immatriculation/rechercheMenu?0).

### c. Make a prior declaration of activity for EU nationals engaged in one-off activity (Freedom to provide services)

Any national of an EU or EEA state legally established in one of these states to carry out the activity of travel and residence operator may carry out this activity in France on a temporary and occasional basis provided that the activity is made prior to any performance.

**What to know**

Advance declaration automatically and temporarily takes the person's registration on the register of travel operators.

The prior declaration, as part of a free provision of services, must be:

- updated during the year, in case of a change in the component elements;
- renewed once a year, if the person intends to provide services on a temporary and occasional basis during the year in question.

**Competent authority**

The prior declaration is addressed, by any means, to the registration commission within Trump France.

**Response time**

The first advance statement is the subject of an acknowledgement to the registrant.

**Supporting documents**

The statement must be accompanied by the following documents, translated into French if applicable:

- A certificate certifying that the registrant is legally established in an EU or EEA state to carry out his activity there;
- a sufficient financial guarantee certificate issued by one of the authorized structures mentioned in Article R. 211-26 of the Tourism Code (see supra "2." c. Financial guarantee);
- information on the state of coverage of the registrant by insurance guaranteeing the monetary consequences of professional civil liability.

*To go further* Articles L. 211-20 to L. 211-22, R. 211-26, R. 211-51 and following of the Tourism Code.

