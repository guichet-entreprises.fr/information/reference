﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS095" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Lazer, Cultura" -->
<!-- var(title)="Clube de vídeo" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-lazer-cultura" -->
<!-- var(title-short)="clube-de-video" -->
<!-- var(url)="https://www.guichet-entreprises.fr/pt/ds/turismo-lazer-cultura/clube-de-video.html" -->
<!-- var(last-update)="2020-04-15 17:24:19" -->
<!-- var(url-name)="clube-de-video" -->
<!-- var(translation)="Auto" -->


Clube de vídeo
==============

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:24:19<!-- end-var -->


<!-- begin-include(disappear-ds-fr) -->
Cette activité n'est plus réglementée.
<!-- end-include -->

1. Definindo a atividade
------------------------

### a. Definição

Um videoclube é um estabelecimento que oferece ao público o aluguel de filmes em um meio físico (fitas de vídeo, DVDs, etc.) destinados apenas ao uso pessoal e distribuição privada do inquilino.

### b. Centro de Formalidades Empresariais Competentes (CFE)

A CFE relevante depende da natureza da estrutura em que a atividade é realizada. Para uma atividade comercial, a CFE relevante é a Câmara de Comércio e Indústria (CCI).

Dois graus. Condições de instalação
-----------------------------------

### a. Qualificações profissionais

Nenhum diploma específico é necessário para o funcionamento de um clube de vídeo. No entanto, o indivíduo é aconselhado a ter conhecimento de contabilidade e gestão.

### b. Qualificações Profissionais - Cidadãos Europeus (Free Service Delivery ou Free Establishment)

Um cidadão de um Estado-Membro da União Europeia (UE) ou parte do Acordo sobre a Área Econômica Europeia (EEE) que deseja operar um clube de vídeo em caráter temporário e casual ou permanente na França não está sujeito a quaisquer condições de diploma ou certificação, bem como o nacional francês.

### c. Algumas peculiaridades da regulação da atividade

#### Exibição de preço

O operador de um videoclube deve informar ao público os preços cobrados pelo aluguel dos filmes, utilizando a rota de marcação, rotulagem, exibição ou qualquer outro processo apropriado.

*Para ir mais longe* Artigo 112-1 do Código do Consumidor.

#### Conformidade com as normas de segurança e acessibilidade

Enquanto as premissas em que o profissional atua forem abertas ao público, ele deve garantir que as normas de segurança e acessibilidade aplicáveis a todas as instituições públicas (ERP) sejam respeitadas.

Por exemplo, medidas de prevenção contra incêndios e acesso a instalações para pessoas com mobilidade reduzida (RMP) devem ser tomadas.

É aconselhável consultar a listagem[Estabelecimento recebendo o público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para mais informações.

*Para ir mais longe* : ordem de 25 de junho de 1980 aprovando as disposições gerais das normas de segurança contra incêndio e pânico em instituições públicas.

#### Vigilância por vídeo

O videoclube pode ser equipado com sistemas de vigilância por vídeo dos clientes. O operador terá, portanto, de indicar sua presença para não infringir sua privacidade.

Ele também terá que declarar sua instalação com a prefeitura do estabelecimento e preencher o formulário[Cerfa 13806*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13806.do), acompanhado de qualquer documento detalhando o sistema utilizado.

**Imposto sobre a distribuição de conteúdo audiovisual**

O operador de um clube de vídeo é obrigado a pagar um imposto sobre a transmissão de conteúdo audiovisual. Isso é doado para o[Centro Nacional de Cinema e Imagens Animadas](https://www.cnc.fr/).

A base deste imposto é baseada no valor livre de impostos adicionado do preço de aluguel dos vídeos.

*Para ir mais longe* Artigo 1609, do Código Tributário Geral.

Três graus. Procedimentos de instalação e formalidades
------------------------------------------------------

### a. Registro no registro de comércio e empresas

O contratante deve se cadastrar no Registro Comercial e Societário (SCN). É aconselhável consultar as "Formalidades comerciais de relatórios de negócios" para obter mais informações.

### b. Empresa de relatórios formalidades

**Autoridade competente**

O operador de um clube de vídeo deve informar sua empresa, e para isso deve fazer uma declaração com o ICC.

**Documentos de suporte**

A pessoa em causa deve fornecer o[documentos de apoio](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependendo da natureza de sua atividade.

**Tempo**

O centro de formalidades comerciais do ICC envia um recibo ao profissional no mesmo dia, elaborando os documentos faltantes no arquivo. O profissional então tem um prazo de quinze dias para completá-lo.

Uma vez que o arquivo esteja concluído, o ICC informa ao requerente para quais agências seu arquivo foi encaminhado.

**Remédios**

O requerente poderá obter a devolução de seu processo desde que não tenha sido apresentado durante os prazos acima.

Se o centro de formalidade empresarial se recusar a receber o processo, o requerente tem um recurso ao tribunal administrativo.

**Custo**

O custo desta declaração depende da forma legal da empresa.

*Para ir mais longe* Seção 635 do Código Tributário Geral.

### c. Se necessário, registre os estatutos da empresa

O operador de um clube de vídeo deve, uma vez que os estatutos da empresa tenham sido datados e assinados, registrá-los no fisco corporativo ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) se:

- O ato envolve uma determinada transação sujeita ao registro;
- se a própria forma do ato exige isso.

**Autoridade competente**

A autoridade de registro é:

- O serviço de publicidade de terrenos da localização do edifício, onde os atos envolvem contribuição de direito imobiliário ou imobiliário;
- Centro de registro ies para todos os outros casos.

**Documentos de suporte**

O profissional deverá submeter duas cópias do estatuto ao EIS.

*Para ir mais longe* Seção 635 do Código Tributário Geral.

