﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS095" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Tempo libero, Cultura" -->
<!-- var(title)="Video club" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-tempo-libero-cultura" -->
<!-- var(title-short)="video-club" -->
<!-- var(url)="https://www.guichet-entreprises.fr/it/ds/turismo-tempo-libero-cultura/video-club.html" -->
<!-- var(last-update)="2020-04-15 17:24:18" -->
<!-- var(url-name)="video-club" -->
<!-- var(translation)="Auto" -->


Video club
==========

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:24:18<!-- end-var -->


<!-- begin-include(disappear-ds-fr) -->
Cette activité n'est plus réglementée.
<!-- end-include -->

1. Definizione dell'attività
----------------------------

### a. Definizione

Un video club è uno stabilimento che offre al pubblico il noleggio di film su un supporto fisico (videocassette, DVD, ecc.) destinati solo all'uso personale e alla distribuzione privata dell'inquilino.

### b. Competente Business Formalities Centre (CFE)

Il CFE in questione dipende dalla natura della struttura in cui viene svolta l'attività. Per un'attività commerciale, il CFE pertinente è la Camera di Commercio e Industria (CCI).

Due gradi. Condizioni di installazione
--------------------------------------

### a. Qualifiche professionali

Non è richiesto alcun diploma specifico per il funzionamento di un video club. Tuttavia, l'individuo è consigliato di avere conoscenza della contabilità e della gestione.

### b. Qualifiche professionali - Cittadini europei (consegna gratuita del servizio o stabilimento gratuito)

Un cittadino di uno Stato membro dell'Unione europea (UE) o parte dell'accordo sullo Spazio economico europeo (AEA) che desidera gestire un video club su base temporanea e casuale o permanente in Francia non è soggetto a condizioni di diploma o certificazione, nonché la cittadina francese.

### c. Alcune peculiarità della regolamentazione dell'attività

#### Visualizzazione dei prezzi

L'operatore di un video club deve informare il pubblico dei prezzi praticati per il noleggio dei film, utilizzando il percorso di marcatura, etichettatura, visualizzazione o qualsiasi altro processo appropriato.

*Per andare oltre* Articolo L. 112-1 del Codice del Consumo.

#### Conformità agli standard di sicurezza e accessibilità

Finché i locali in cui il professionista opera sono aperti al pubblico, deve garantire che siano rispettati gli standard di sicurezza e accessibilità applicabili a tutte le istituzioni pubbliche (ERP).

Ad esempio, devono essere adottate misure di prevenzione degli incendi e l'accesso ai locali per le persone con mobilità ridotta (PMR).

Si consiglia di fare riferimento all'elenco[Istituzione che riceve il pubblico](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) per ulteriori informazioni.

*Per andare oltre* : ordine del 25 giugno 1980 che approva le disposizioni generali delle norme di sicurezza antincendio e antipanico nelle istituzioni pubbliche.

#### Videosorveglianza

Il video club può essere dotato di sistemi per la videosorveglianza dei clienti. L'operatore dovrà quindi indicare la propria presenza per non violare la propria privacy.

Dovrà anche dichiarare la sua installazione con la prefettura dello stabilimento e compilare il modulo[Cerfa 13806*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13806.do), accompagnato da qualsiasi documento che descrive il sistema utilizzato.

**Tassa sulla distribuzione di contenuti audiovisivi**

L'operatore di un video club è tenuto a pagare una tassa sulla trasmissione di contenuti audiovisivi. Questo viene donato al[Centro Nazionale per il Cinema e l'Imaging Animato](https://www.cnc.fr/).

La base di questa tassa si basa sull'importo del valore aggiunto esente da imposte del prezzo di noleggio dei video.

*Per andare oltre* Articolo 1609 sexdecies B del codice fiscale generale.

Tre gradi. Procedure di installazione e formalità
-------------------------------------------------

### a. Registrazione nel registro degli scambi e delle imprese

Il contraente deve registrarsi al Registro del Commercio e delle Imprese (SCN). Si consiglia di fare riferimento alle "Commercial Business Reporting Formalities" per ulteriori informazioni.

### b. Formalità di rendicontazione delle società

**Autorità competente**

L'operatore di un video club deve segnalare la sua società, e per farlo deve fare una dichiarazione con la CpI.

**Documenti di supporto**

L'interessato deve fornire il[documenti giustificativi](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) a seconda della natura della sua attività.

**Tempo**

Il Centro di formalità aziendali della CPI invia una ricevuta al professionista lo stesso giorno, redigenando i documenti mancanti in archivio. Il professionista ha quindici giorni per completarlo.

Una volta completato il fascicolo, la Corte penale internazionale comunica al richiedente a quali agenzie è stato inoltrato il fascicolo.

**Rimedi**

Il richiedente può ottenere la restituzione del suo fascicolo, purché non sia stato presentato durante i termini di cui sopra.

Se il centro di formalità commerciale rifiuta di ricevere il fascicolo, il richiedente ha un ricorso dinanzi al tribunale amministrativo.

**Costo**

Il costo di questa dichiarazione dipende dalla forma giuridica della società.

*Per andare oltre* sezione 635 del codice tributario generale.

### c. Se necessario, registrare gli statuti della società

L'operatore di un video club deve, una volta che gli statuti della società sono stati datati e firmati, registrarli presso l'ufficio d'imposta sulle società ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) se:

- L'atto riguarda una particolare transazione soggetta a registrazione;
- se la forma stessa dell'atto lo richiede.

**Autorità competente**

L'autorità di registrazione è:

- Il servizio di pubblicità fondiaria della sede dell'edificio, dove gli atti comportano un contributo di diritto immobiliare o immobiliare;
- Centro di registrazione IES per tutti gli altri casi.

**Documenti di supporto**

Il professionista deve presentare due copie degli statuti al SIE.

*Per andare oltre* sezione 635 del codice tributario generale.

