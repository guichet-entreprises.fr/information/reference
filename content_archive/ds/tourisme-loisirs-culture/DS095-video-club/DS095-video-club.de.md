﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS095" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourismus, Freizeit, Kultur" -->
<!-- var(title)="Videoclub" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourismus-freizeit-kultur" -->
<!-- var(title-short)="videoclub" -->
<!-- var(url)="https://www.guichet-entreprises.fr/de/ds/tourismus-freizeit-kultur/videoclub.html" -->
<!-- var(last-update)="2020-04-15 17:24:19" -->
<!-- var(url-name)="videoclub" -->
<!-- var(translation)="Auto" -->


Videoclub
=========

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:24:19<!-- end-var -->


<!-- begin-include(disappear-ds-fr) -->
Cette activité n'est plus réglementée.
<!-- end-include -->

1. Definieren der Aktivität
---------------------------

### a. Definition

Ein Videoclub ist eine Einrichtung, die der Öffentlichkeit die Vermietung von Filmen auf einem physischen Medium (Videobänder, DVDs usw.) anbietet, die nur für den persönlichen Gebrauch und die private Verteilung des Mieters bestimmt sind.

### b. Kompetenzzentrum für Geschäftsformalitäten (CFE)

Die betreffende CFE hängt von der Art der Struktur ab, in der die Tätigkeit ausgeübt wird. Für eine gewerbliche Tätigkeit ist die Industrie- und Handelskammer (IHK) die zuständige CFE.

Zwei Grad. Installationsbedingungen
-----------------------------------

### a. Berufliche Qualifikationen

Für den Betrieb eines Videoclubs ist kein spezifisches Diplom erforderlich. Dem Einzelnen wird jedoch empfohlen, Über Kenntnisse der Buchhaltung und Desleitung zu verfügen.

### b. Berufsqualifikationen - Europäische Staatsangehörige (Free Service Delivery oder Free Establishment)

Ein Staatsangehöriger eines Mitgliedstaats der Europäischen Union (EU) oder parteiisch für das Abkommen über den Europäischen Wirtschaftsraum (EWR), der einen Videoclub vorübergehend und gelegentlich oder dauerhaft in Frankreich betreiben möchte, unterliegt keinen Bediplomaten- oder Zertifizierung sowie die französische Staatsangehörige.

### c. Einige Besonderheiten der Regelung der Tätigkeit

#### Preisanzeige

Der Betreiber eines Videoclubs muss die Öffentlichkeit über die Preise informieren, die für die Vermietung der Filme berechnet werden, und das unter Verwendung des Weges der Kennzeichnung, Kennzeichnung, Anzeige oder eines anderen geeigneten Verfahrens.

*Um weiter zu gehen* Artikel L. 112-1 des Verbraucherschutzgesetzes.

#### Einhaltung von Sicherheits- und Barrierefreiheitsstandards

Solange die Räumlichkeiten, in denen der Fachmann tätig ist, der Öffentlichkeit zugänglich sind, muss er sicherstellen, dass die für alle öffentlichen Einrichtungen geltenden Sicherheits- und Zugänglichkeitsstandards (ERP) eingehalten werden.

So müssen beispielsweise Brandschutzmaßnahmen und der Zugang zu Räumlichkeiten für Menschen mit eingeschränkter Mobilität (PMR) ergriffen werden.

Es ist ratsam, auf die Auflistung zu verweisen[Einrichtung, die die Öffentlichkeit empfängt](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) für weitere Informationen.

*Um weiter zu gehen* : Verordnung vom 25. Juni 1980 zur Genehmigung der allgemeinen Bestimmungen der Brand- und Paniksicherheitsvorschriften in öffentlichen Einrichtungen.

#### Videoüberwachung

Der Videoclub kann mit Systemen zur Videoüberwachung von Kunden ausgestattet werden. Der Betreiber muss daher seine Anwesenheit angeben, um seine Privatsphäre nicht zu verletzen.

Er muss auch seine Installation bei der Präfektur des Betriebs erklären und das Formular ausfüllen[Cerfa 13806*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13806.do), zusammen mit einem Dokument, das das verwendete System detailliert dargibt.

**Steuer auf die Verbreitung audiovisueller Inhalte**

Der Betreiber eines Videoclubs ist verpflichtet, eine Steuer auf die Übertragung audiovisueller Inhalte zu entrichten. Diese wird an die[National Centre for Cinema and Animated Imaging](https://www.cnc.fr/).

Die Bemessungsgrundlage dieser Steuer basiert auf dem steuerfreien Mehrwert des Mietpreises der Videos.

*Um weiter zu gehen* Artikel 1609 sexdecies B des Allgemeinen Steuergesetzbuches.

Drei Grad. Installationsverfahren und Formalitäten
--------------------------------------------------

### a. Eintragung in das Handels- und Gesellschaftsregister

Der Auftragnehmer muss sich beim Handels- und Unternehmensregister (SCN) registrieren. Weitere Informationen sollten sie auf die "Commercial Business Reporting Formalities" verweisen.

### b. Meldeformalitäten des Unternehmens

**Zuständige Behörde**

Der Betreiber eines Videoclubs muss sein Unternehmen melden und dazu eine Erklärung beim ICC abgeben.

**Belege**

Die betroffene Person muss die[Belege](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) je nach Art ihrer Tätigkeit.

**Zeit**

Das Geschäftsformalitätszentrum des ICC sendet dem Fachmann noch am selben Tag eine Quittung, in der die fehlenden Unterlagen in den Akten erstellt werden. Der Profi hat dann eine Frist von fünfzehn Tagen, um es abzuschließen.

Sobald die Akte vollständig ist, teilt der IStGH dem Antragsteller mit, an welche Stellen seine Akte weitergeleitet wurde.

**Heilmittel**

Der Anmelder kann die Akteneinsicht einholen, sofern sie nicht zu den vorgenannten Fristen eingereicht worden ist.

Verweigert das Business Formality Center den Erhalt der Akte, so hat der Kläger Beschwerde beim Verwaltungsgericht.

**Kosten**

Die Kosten dieser Erklärung hängen von der Rechtsform des Unternehmens ab.

*Um weiter zu gehen* Abschnitt 635 des Allgemeinen Steuergesetzbuches.

### c. Registrieren Sie gegebenenfalls die Satzung der Gesellschaft

Der Betreiber eines Videoclubs muss diese nach Derdatum und Unterzeichnung der Satzung beim Körperschaftssteueramt ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) wenn:

- Das Gesetz betrifft eine bestimmte Transaktion, die der Registrierung unterliegt;
- wenn die Form der Handlung dies erfordert.

**Zuständige Behörde**

Die Registrierungsstelle ist:

- Die Grundstückswerbung für den Standort des Gebäudes, wenn die Handlungen einen Beitrag des Immobilien- oder Immobilienrechts beinhalten;
- IES-Registrierungszentrum für alle anderen Fälle.

**Belege**

Der Fachmann hat dem EIS zwei Exemplare der Satzung vorzulegen.

*Um weiter zu gehen* Abschnitt 635 des Allgemeinen Steuergesetzbuches.

