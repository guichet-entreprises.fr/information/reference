﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS095" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Videoclub" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="videoclub" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/videoclub.html" -->
<!-- var(last-update)="2020-04-15 17:24:18" -->
<!-- var(url-name)="videoclub" -->
<!-- var(translation)="Auto" -->


Videoclub
=========

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:24:18<!-- end-var -->


<!-- begin-include(disappear-ds-fr) -->
Cette activité n'est plus réglementée.
<!-- end-include -->

1. Definición de la actividad
-----------------------------

### a. Definición

Un videoclub es un establecimiento que ofrece al público el alquiler de películas en un medio físico (cintas de vídeo, DVDs, etc.) destinadas únicamente al uso personal y a la distribución privada del inquilino.

### b. Centro competente de formalidades comerciales (CFE)

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Para una actividad comercial, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

Dos grados. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere un diploma específico para el funcionamiento de un videoclub. Sin embargo, se aconseja al individuo tener conocimiento de contabilidad y gestión.

### b. Cualificaciones profesionales - Nacionales Europeos (Entrega gratuita de servicios o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) que desee operar un videoclub de forma temporal y casual o permanente en Francia no está sujeto a ninguna condición de diploma o certificación, así como el nacional francés.

### c. Algunas peculiaridades de la regulación de la actividad

#### Visualización de precios

El operador de un club de vídeo deberá informar al público de los precios cobrados por el alquiler de las películas, utilizando la ruta de marcado, etiquetado, exhibición o cualquier otro proceso apropiado.

*Para ir más allá* Artículo L. 112-1 del Código del Consumidor.

#### Cumplimiento de las normas de seguridad y accesibilidad

Mientras los locales en los que opera el profesional estén abiertos al público, debe garantizar que se respeten las normas de seguridad y accesibilidad aplicables a todas las instituciones públicas (ERP).

Por ejemplo, deben adoptarse medidas de prevención de incendios y acceso a locales para personas con movilidad reducida (PMR).

Es aconsejable referirse a la lista[Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) para obtener más información.

*Para ir más allá* : orden de 25 de junio de 1980 por la que se aprueban las disposiciones generales de las normas de seguridad contra incendios y pánico en las instituciones públicas.

#### Videovigilancia

El videoclub puede equiparse con sistemas de videovigilancia de los clientes. Por lo tanto, el operador deberá indicar su presencia para no infringir su privacidad.

También tendrá que declarar su instalación con la prefectura del establecimiento y completar el formulario[Cerfa 13806*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13806.do), acompañado de cualquier documento que detalle el sistema utilizado.

**Impuesto sobre la distribución de contenidos audiovisuales**

El operador de un club de vídeo está obligado a pagar un impuesto sobre la emisión de contenidos audiovisuales. Esto se dona a la[Centro Nacional de Cine e Imágenes Animadas](https://www.cnc.fr/).

La base de este impuesto se basa en la cantidad libre de impuestos del valor añadido del precio de alquiler de los videos.

*Para ir más allá* Artículo 1609 sexdecies B del Código General Tributario.

Tres grados. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Inscripción en el registro de comercio sin comercio y empresas

El contratista debe registrarse en el Registro Mercantil y Corporativo (SCN). Es aconsejable consultar las "Formalidades de Reporte comercial" para obtener más información.

### b. Formalidades de notificación de la empresa

**Autoridad competente**

El operador de un club de vídeo debe informar de su empresa, y para ello debe hacer una declaración ante la CPI.

**Documentos de apoyo**

El interesado debe proporcionar la[documentos de apoyo](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) dependiendo de la naturaleza de su actividad.

**hora**

El centro de formalidades comerciales de la CPI envía un recibo al profesional el mismo día, elaborando los documentos que faltan en el archivo. El profesional tiene entonces un período de quince días para completarlo.

Una vez completado el expediente, la CPI le dice al solicitante a qué agencias se ha remitido su expediente.

**Remedios**

El solicitante podrá obtener la restitución de su expediente siempre que no se haya presentado durante los plazos anteriores.

Si el centro de formalidad empresarial se niega a recibir el expediente, el solicitante tiene un recurso ante el tribunal administrativo.

**Costo**

El coste de esta declaración depende de la forma jurídica de la empresa.

*Para ir más allá* Artículo 635 del Código Tributario General.

### c. Si es necesario, registre los estatutos de la empresa

El operador de un club de vídeo deberá, una vez fechados y firmados los estatutos de la empresa, registrarlos en la oficina del impuesto de sociedades ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) si:

- El acto implica una transacción particular sujeta al registro;
- si la forma misma del acto lo requiere.

**Autoridad competente**

La autoridad de registro es:

- El servicio de publicidad de la tierra de la ubicación del edificio, donde los actos impliquen una contribución del derecho inmobiliario o inmobiliario;
- Centro de registro del IES para todos los demás casos.

**Documentos de apoyo**

El profesional debe presentar dos copias de los estatutos a la EIS.

*Para ir más allá* Artículo 635 del Código Tributario General.

