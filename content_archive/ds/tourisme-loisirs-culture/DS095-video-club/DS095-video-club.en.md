﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS095" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Video club" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="video-club" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/video-club.html" -->
<!-- var(last-update)="2020-04-15 17:24:18" -->
<!-- var(url-name)="video-club" -->
<!-- var(translation)="Auto" -->


Video club
==========

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:24:18<!-- end-var -->


<!-- begin-include(disappear-ds-fr) -->
Cette activité n'est plus réglementée.
<!-- end-include -->

1. Defining the activity
------------------------

### a. Definition

A video club is an establishment offering the public the rental of films on a physical medium (videotapes, DVDs, etc.) intended only for the personal use and private distribution of the tenant.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. For a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI).

Two degrees. Installation conditions
------------------------------------

### a. Professional qualifications

No specific diploma is required for the operation of a video club. However, the individual is advised to have knowledge of accounting and management.

### b. Professional Qualifications - European Nationals (Free Service Delivery or Free Establishment)

A national of a Member State of the European Union (EU) or party to the Agreement on the European Economic Area (EEA) wishing to operate a video club on a temporary and casual or permanent basis in France is not subject to any conditions of diploma or certification, as well as the French national.

### c. Some peculiarities of the regulation of the activity

#### Price display

The operator of a video club must inform the public of the prices charged for the rental of the films, using the route of marking, labelling, display or any other appropriate process.

*To go further* Article L. 112-1 of the Consumer Code.

#### Compliance with safety and accessibility standards

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility (PMR) must be taken.

It is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

*To go further* : order of June 25, 1980 approving the general provisions of the fire and panic safety regulations in public institutions.

#### Video surveillance

The video club can be equipped with systems for video surveillance of customers. The operator will therefore have to indicate their presence in order not to infringe on their privacy.

He will also have to declare his installation with the prefecture of the establishment and complete the form[Cerfa 13806*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13806.do), accompanied by any document detailing the system used.

**Tax on the distribution of audiovisual content**

The operator of a video club is required to pay a tax on the broadcast of audiovisual content. This is donated to the[National Centre for Cinema and Animated Imaging](https://www.cnc.fr/).

The base of this tax is based on the tax-free amount of value added of the rental price of the videos.

*To go further* Article 1609 sexdecies B of the General Tax Code.

Three degrees. Installation procedures and formalities
------------------------------------------------------

### a. Registration in the register of trade and companies

The contractor must register with the Trade and Corporate Register (SCN). It is advisable to refer to the "Commercial Business Reporting Formalities" for more information.

### b. Company reporting formalities

**Competent authority**

The operator of a video club must report his company, and to do so must make a declaration with the ICC.

**Supporting documents**

The person concerned must provide the[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Time**

The ICC's business formalities centre sends a receipt to the professional on the same day, drawing up the missing documents on file. The professional then has a period of fifteen days to complete it.

Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the business formality centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*To go further* Section 635 of the General Tax Code.

### c. If necessary, register the company's statutes

The operator of a video club must, once the company's statutes have been dated and signed, register them with the corporate tax office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*To go further* Section 635 of the General Tax Code.

