﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS095" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Vidéo-club" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="video-club" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/video-club.html" -->
<!-- var(last-update)="2020-04-15 17:24:18" -->
<!-- var(url-name)="video-club" -->
<!-- var(translation)="None" -->


# Vidéo-club

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:24:18<!-- end-var -->

<!-- begin-include(disappear-ds-fr) -->
Cette activité n'est plus réglementée.
<!-- end-include -->

## 1°. Définition de l’activité

### a. Définition

Un vidéo-club est un établissement proposant au public la location de films sur un support physique (vidéocassettes, DVD, etc.) destinés uniquement à l'usage personnel et à la diffusion privée du locataire.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée. Pour une activité commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucun diplôme spécifique n'est requis pour l'exploitation d'un vidéo-club. Toutefois, il est conseillé à l'intéressé d'avoir des connaissances en matière comptable et en gestion.

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services ou Libre établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) souhaitant exploiter un vidéo-club à titre temporaire et occasionnel ou permanent en France n'est soumis à aucune condition de diplôme ou de certification, au même titre que le ressortissant français.

### c. Quelques particularités de la réglementation de l’activité

#### Affichage des prix

L'exploitant d'un vidéo-club doit informer le public des prix pratiqués pour la location des films, et ce, en utilisant la voie du marquage, de l'étiquetage, de l'affichage ou par tout autre procédé approprié.

*Pour aller plus loin* : article L. 112-1 du Code de la consommation.

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite (PMR) doivent notamment être prises.

Il est conseillé de se reporter à la fiche [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) pour de plus amples informations.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public.

#### Vidéosurveillance

Le vidéo-club peut être équipé de systèmes permettant la vidéosurveillance des clients. L'exploitant devra dès lors indiquer leur présence afin de ne pas porter atteinte à la vie privée de ces derniers.

Il devra également déclarer son installation auprès de la préfecture du lieu de l'établissement et remplir le formulaire [Cerfa n° 13806*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13806.do), accompagné de tout document détaillant le système utilisé.

**Taxe sur la diffusion de contenus audiovisuels**

L'exploitant d'un vidéo-club est tenu de s'acquitter d'une taxe sur la diffusion de contenus audiovisuels. Celle-ci est reversée au [Centre national du cinéma et de l'imagerie animée](https://www.cnc.fr/).

L'assiette de cette taxe est basée sur le montant hors taxe de la valeur ajoutée du prix de location des vidéos.

*Pour aller plus loin* : article 1609 sexdecies B du Code général des impôts.

## 3°. Démarches et formalités d’installation

### a. Immatriculation au registre du commerce et des sociétés

L’entrepreneur doit s’immatriculer au registre du commerce et des sociétés (RCS). Il est conseillé de se reporter à la fiche « Formalités de déclaration des entreprises commerciales » pour de plus amples informations.

### b. Formalités de déclaration de l’entreprise

**Autorité compétente**

L'exploitant d'un vidéo-club doit procéder à la déclaration de son entreprise, et doit pour cela effectuer une déclaration auprès de la CCI.

**Pièces justificatives**

L'intéressé doit fournir les [pièces justificatives](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) requises selon la nature de son activité.

**Délais**

Le centre des formalités des entreprises de la CCI adresse le jour même un récépissé au professionnel dressant, le cas échéant, les pièces manquantes au dossier. Le professionnel dispose alors d'un délai de quinze jours pour le compléter.

Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

**Voies de recours**

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le centre de formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

**Coût**

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### c. Le cas échéant, enregistrer les statuts de la société

L'exploitant d'un vidéo-club doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises ([SIE](http://www2.impots.gouv.fr/sie/ifu.htm)) si :

* l'acte comporte une opération particulière soumise à un enregistrement ;
* si la forme même de l'acte l'exige.

**Autorité compétente**

L'autorité compétente en matière d'enregistrement est :

* le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
* le pôle enregistrement du SIE pour tous les autres cas.

**Pièces justificatives**

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.