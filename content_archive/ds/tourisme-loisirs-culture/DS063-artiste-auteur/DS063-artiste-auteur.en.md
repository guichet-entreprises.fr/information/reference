﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS063" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Artist-author" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="artist-author" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/artist-author.html" -->
<!-- var(last-update)="2020-04-15 17:24:15" -->
<!-- var(url-name)="artist-author" -->
<!-- var(translation)="Auto" -->


Artist-author
=============

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:24:15<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Defining the activity
------------------------

### a. Definition

The artist-author is a professional who engages in an original creative activity in the following areas:

- Audiovisual;
- Musical;
- Cinematographic
- choreographic;
- Graphic;
- Literary;
- Photographic
- Plastic;
- Software.

**Note that**

The intermittent authors of the show or authors who benefit from copyrights paid by the House of Artists or the Association for the Management of The Social Security of Authors (Agessa) are not considered to be artists-authors.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out. For the artist-author, the competent CFE is the Urssaf.

Two degrees. Professional qualifications
----------------------------------------

### a. Professional qualifications

No specific diploma is required to qualify as an artist-author.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Free Establishment)

A national of a Member State of the European Union (EU) or party to the Agreement on the European Economic Area (EEA) is not subject to any qualification or certification requirements, as are the French.

### c. Some peculiarities of the regulation of the activity

#### Workshop request

Artists who wish to practice in a workshop must apply to the Regional Directorate of Cultural Affairs (DRAC) in their region or the Cultural Affairs Directorate of their municipality.

Three degrees. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The contractor must register with the Urssaf register. The artist-author will have to complete the form[Cerfa 13231*01](https://www.cfe.urssaf.fr/saisiepl/CFE_Declaration).

The declaration will result in the attribution of an EPA code by the INSEE and a Siret number.

### b. Make a declaration of start of activity

Depending on the nature of their activity, artist-authors must be affiliated with a social welfare organization in order to receive social security coverage.

To do this, they must make a declaration at the first sale or in case of a firm order from either:

- The House of Artists for graphic and visual artists;
- of the Agessa for writers, authors of literary or scientific writings, translators, music composers, choreographers, authors of film, audiovisual and multimedia works, photographers.

This declaration should enable the organizations to verify that the applicant's activity is well within the scope of that of an artist-author and that it results in a base of contributions and social contributions in the artists-authors.

For artist-authors who are part of the social system of the House of Artists, the first statement must include the following supporting documents:

- The[Declaration](http://www.secu-artistes-auteurs.fr/eform/submit/declaration_de_debut_d_exercice) ;
- proof of the first sale: a photocopy of an invoice, a copyright note, a contract or a purchase order issued in the current year.

The Artists' House will have three weeks to issue a receipt for the start of activity with an identifier number that will be used for future activity tax returns.

For artist-authors under the Agessa, the declaration must take place after one year of practice and must contain the following pieces:

- The[declaration form](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/Demande%20d%27ouverture%20de%20droits%20%C3%A0%20l%27Assurance%20Maladie%20%28Cerfa%20S1219%29.pdf) ;
- The[tax and activity forms](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20de%20revenus%20et%20d'activit%C3%A9s%20Agessa.pdf), to be completed over the last 2 years;
- The[copyright details](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9tail%20des%20droits%20d'auteur.pdf) ;
- A copy of the tax return for the reference year, certified as compliant, as well as tax notices for the last two calendar years;
- for foreign nationals, a copy of the residence permit, proof of copyright collection (copies of copyright transfer contracts, copyright notes, etc.) over the last 2 years.

