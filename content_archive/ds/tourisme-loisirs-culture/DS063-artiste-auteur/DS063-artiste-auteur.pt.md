﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS063" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Lazer, Cultura" -->
<!-- var(title)="Artista-autor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-lazer-cultura" -->
<!-- var(title-short)="artista-autor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/pt/ds/turismo-lazer-cultura/artista-autor.html" -->
<!-- var(last-update)="2020-04-15 17:24:15" -->
<!-- var(url-name)="artista-autor" -->
<!-- var(translation)="Auto" -->


Artista-autor
=============

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:24:15<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

### a. Definição

O artista-autor é um profissional que se engaja em uma atividade criativa original nas seguintes áreas:

- Audiovisual;
- Musical;
- Cinematográficas
- coreográfico;
- Gráfico;
- Literário;
- Fotográfica
- Plástico;
- Software.

**Note que**

Os autores intermitentes da mostra ou autores que se beneficiam de direitos autorais pagos pela Casa dos Artistas ou pela Associação para a Gestão da Seguridade Social dos Autores (Agessa) não são considerados artistas-autores.

### b. CFE competente

A CFE relevante depende da natureza da estrutura em que a atividade é realizada. Para o artista-autor, a CFE competente é a Urssaf.

Dois graus. Qualificações profissionais
---------------------------------------

### a. Qualificações profissionais

Nenhum diploma específico é necessário para se qualificar como artista-autor.

### b. Qualificações Profissionais - Cidadãos da UE ou EEE (Serviço Livre ou Estabelecimento Livre)

Um cidadão de um Estado-Membro da União Europeia (UE) ou parte do Acordo sobre a Área Econômica Europeia (EEE) não está sujeito a quaisquer requisitos de qualificação ou certificação, assim como os franceses.

### c. Algumas peculiaridades da regulação da atividade

#### Solicitação de workshop

Os artistas que desejam praticar em uma oficina devem se inscrever na Diretoria Regional de Assuntos Culturais (DRAC) em sua região ou na Diretoria de Assuntos Culturais de seu município.

Três graus. Procedimentos de instalação e formalidades
------------------------------------------------------

### a. Formalidades de relatórios da empresa

O contratante deve se cadastrar no cadastro da Urssaf. O artista-autor terá que preencher o formulário[Cerfa 13231*01](https://www.cfe.urssaf.fr/saisiepl/CFE_Declaration).

A declaração resultará na atribuição de um código EPA pelo INSEE e um número de Siret.

### b. Faça uma declaração de início da atividade

Dependendo da natureza de sua atividade, os artistas-autores devem estar afiliados a uma organização de assistência social para receber cobertura previdenciária.

Para fazer isso, eles devem fazer uma declaração na primeira venda ou em caso de uma ordem firme de qualquer um:

- A Casa dos Artistas para artistas gráficos e visuais;
- da Agessa para escritores, autores de escritos literários ou científicos, tradutores, compositores musicais, coreógrafos, autores de obras cinematográficas, audiovisuais e multimídia, fotógrafos.

Esta declaração deve permitir que as organizações verifiquem se a atividade do requerente está bem no âmbito da de um artista-autor e que resulta em uma base de contribuições e contribuições sociais no artistas-autores.

Para os artistas-autores que fazem parte do sistema social da Casa dos Artistas, a primeira declaração deve incluir os seguintes documentos de apoio:

- O[Declaração](http://www.secu-artistes-auteurs.fr/eform/submit/declaration_de_debut_d_exercice) ;
- comprovante da primeira venda: uma fotocópia de uma nota fiscal, uma nota de direitos autorais, um contrato ou uma ordem de compra emitida no ano corrente.

A Casa dos Artistas terá três semanas para emitir um recibo para o início da atividade com um número identificador que será usado para futuras declarações de imposto de atividade.

Para os artistas-autores da Agessa, a declaração deve ocorrer após um ano de prática e deve conter as seguintes peças:

- O[forma de declaração](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/Demande%20d%27ouverture%20de%20droits%20%C3%A0%20l%27Assurance%20Maladie%20%28Cerfa%20S1219%29.pdf) ;
- O[formas fiscais e de atividade](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20de%20revenus%20et%20d'activit%C3%A9s%20Agessa.pdf), a ser concluído ao longo dos últimos 2 anos;
- O[detalhes de direitos autorais](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9tail%20des%20droits%20d'auteur.pdf) ;
- Cópia da declaração de imposto de renda para o ano de referência, certificada como compatível, bem como avisos fiscais dos últimos dois anos corridos;
- para estrangeiros, uma cópia da autorização de residência, comprovante de coleta de direitos autorais (cópias de contratos de transferência de direitos autorais, notas de direitos autorais, etc.) nos últimos 2 anos.

