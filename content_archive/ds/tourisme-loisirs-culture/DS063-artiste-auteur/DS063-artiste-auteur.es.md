﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS063" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Ocio, Cultura" -->
<!-- var(title)="Artista-autor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-ocio-cultura" -->
<!-- var(title-short)="artista-autor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/turismo-ocio-cultura/artista-autor.html" -->
<!-- var(last-update)="2020-04-15 17:24:15" -->
<!-- var(url-name)="artista-autor" -->
<!-- var(translation)="Auto" -->


Artista-autor
=============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:24:15<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

### a. Definición

El artista-autor es un profesional que participa en una actividad creativa original en las siguientes áreas:

- Audiovisual;
- Musical;
- Cinematográfica
- coreografía;
- Gráfico;
- Literario;
- Fotográfico
- Plástico;
- Software.

**Tenga en cuenta que**

Los autores intermitentes de la serie o autores que se benefician de los derechos de autor pagados por la Casa de los Artistas o la Asociación para la Gestión de la Seguridad Social de los Autores (Agessa) no se consideran artistas-autores.

### b. CFE competente

La CFE pertinente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. Para el artista-autor, la CFE competente es la Urssaf.

Dos grados. Cualificaciones profesionales
-----------------------------------------

### a. Cualificaciones profesionales

No se requiere un diploma específico para calificar como artista-autor.

### b. Cualificaciones profesionales - Nacionales de la UE o del EEE (Servicio gratuito o establecimiento gratuito)

Un nacional de un Estado miembro de la Unión Europea (UE) o parte en el Acuerdo sobre el Espacio Económico Europeo (EEE) no está sujeto a ningún requisito de cualificación o certificación, al igual que los franceses.

### c. Algunas peculiaridades de la regulación de la actividad

#### Solicitud de taller

Los artistas que deseen ejercer en un taller deben solicitar a la Dirección Regional de Asuntos Culturales (DRAC) en su región o a la Dirección de Asuntos Culturales de su municipio.

Tres grados. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

El contratista debe registrarse en el registro de Urssaf. El artista-autor tendrá que completar el formulario[Cerfa 13231*01](https://www.cfe.urssaf.fr/saisiepl/CFE_Declaration).

La declaración dará lugar a la atribución de un código EPA por el INSEE y un número Siret.

### b. Hacer una declaración de inicio de actividad

Dependiendo de la naturaleza de su actividad, los artistas-autores deben estar afiliados a una organización de bienestar social para recibir cobertura de seguridad social.

Para ello, deberán hacer una declaración en la primera venta o en el caso de un pedido firme de:

- La Casa de los Artistas para artistas gráficos y visuales;
- de la Agessa para escritores, autores de escritos literarios o científicos, traductores, compositores musicales, coreógrafos, autores de obras cinematográficas, audiovisuales y multimedia, fotógrafos.

Esta declaración debe permitir a las organizaciones verificar que la actividad del solicitante está bien dentro del ámbito de la de un artista-autor y que da lugar a una base de contribuciones y contribuciones sociales en el artistas-autores.

Para los artistas-autores que forman parte del sistema social de la Casa de los Artistas, la primera declaración debe incluir los siguientes documentos justificativos:

- el[Declaración](http://www.secu-artistes-auteurs.fr/eform/submit/declaration_de_debut_d_exercice) ;
- prueba de la primera venta: fotocopia de una factura, nota de derechos de autor, contrato o orden de compra emitida en el año en curso.

La Casa de los Artistas tendrá tres semanas para emitir un recibo para el inicio de la actividad con un número de identificador que se utilizará para futuras declaraciones de impuestos de actividad.

Para los artistas-autores de la Agessa, la declaración debe tener lugar después de un año de práctica y debe contener las siguientes piezas:

- el[formulario de declaración](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/Demande%20d%27ouverture%20de%20droits%20%C3%A0%20l%27Assurance%20Maladie%20%28Cerfa%20S1219%29.pdf) ;
- el[formularios de impuestos y actividades](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20de%20revenus%20et%20d'activit%C3%A9s%20Agessa.pdf), que se completará en los últimos 2 años;
- el[detalles de los derechos de autor](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9tail%20des%20droits%20d'auteur.pdf) ;
- Una copia de la declaración de impuestos para el año de referencia, certificada como conforme, así como avisos fiscales de los últimos dos años calendario;
- para los extranjeros, una copia del permiso de residencia, una prueba de la colección de derechos de autor (copias de contratos de transferencia de derechos de autor, notas de derechos de autor, etc.) en los últimos 2 años.

