﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS063" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Artiste-auteur" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="artiste-auteur" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/artiste-auteur.html" -->
<!-- var(last-update)="2020-04-15 17:24:15" -->
<!-- var(url-name)="artiste-auteur" -->
<!-- var(translation)="None" -->


# Artiste-auteur

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:24:15<!-- end-var -->

## 1°. Définition de l'activité

### a. Définition

L'artiste-auteur est un professionnel qui exerce une activité de création originale dans les domaines suivants :

* audiovisuel ;
* musical ;
* cinématographique ;
* chorégraphique ;
* graphique ;
* littéraire ;
* photographique ;
* plastique ;
* logiciel.

**À noter**

Ne sont pas considérés comme des artistes-auteurs les intermittents du spectacle ou les auteurs qui bénéficient de droits d'auteurs versés par la Maison des artistes ou l'Association pour la gestion de la sécurité sociale des auteurs (Agessa).

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée. Pour l'artiste-auteur, le CFE compétent est l'Urssaf.

## 2°. Qualifications professionnelles

### a. Qualifications professionnelles

Aucun diplôme spécifique n'est requis pour être qualifié d'artiste-auteur.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Service ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) n'est soumis à aucune condition de diplôme ou de certification, au même titre que les Français.

### c. Quelques particularités de la réglementation de l'activité

#### Demande d'atelier

Les artistes qui souhaitent exercer dans un atelier doivent en faire la demande auprès de la direction régionale des affaires culturelles (DRAC) de leur région ou de la direction des affaires culturelles de leur municipalité.

## 3°. Démarches et formalités d'installation

### a. Formalités de déclaration de l’entreprise

L’entrepreneur doit s’immatriculer au registre de l'Urssaf. L'artiste-auteur devra compléter le formulaire [Cerfa n° 13231*01](https://www.cfe.urssaf.fr/saisiepl/CFE_Declaration).

La déclaration donnera lieu à l'attribution d'un code APE par l'Insee et d'un numéro Siret.

### b. Effectuer une déclaration de début d'activité

Selon la nature de leur activité, les artistes-auteurs doivent être affiliés à un organisme de protection sociale pour bénéficier d'une couverture sociale.

Pour cela , ils doivent effectuer une déclaration dès la première vente ou en cas de commande ferme auprès soit :

* de la Maison des artistes pour les auteurs d'arts graphiques et plastiques ;
* de l'Agessa pour les écrivains, auteurs d'écrits littéraires ou scientifiques, traducteurs, compositeurs de musique, chorégraphes, auteurs d'œuvres cinématographiques, audiovisuelles et multimédia, photographes.

Cette déclaration doit permettre aux organismes de vérifier que l'activité du demandeur entre bien dans le champ d'exercice de celle d'artiste-auteur et qu'elle donne lieu à une assiette de cotisations et de contributions sociales dans le régime des artistes-auteurs.

Pour les artistes-auteurs relavant du régime social de la Maison des artistes, la première déclaration doit comprendre les pièces justificatives suivantes :

* la [déclaration](http://www.secu-artistes-auteurs.fr/eform/submit/declaration_de_debut_d_exercice) ;
* les justificatifs de la réalisation de la première vente : photocopie d’une facture, d’une note de droit d’auteur, d’un contrat ou d’un bon de commande émis lors de l’année en cours.

La Maison des artistes disposera d'un délai de trois semaines pour délivrer un récépissé de début d'activité avec un numéro d'identifiant qui servira pour les prochaines déclarations de revenus d'activité.

Pour les artistes-auteurs relevant de l'Agessa, la déclaration doit intervenir après un an d'exercice et doit contenir les pièces suivantes :

* le [formulaire de déclaration](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/Demande%20d%27ouverture%20de%20droits%20%C3%A0%20l%27Assurance%20Maladie%20%28Cerfa%20S1219%29.pdf) ;
* les [formulaires de déclaration de revenus et d’activités](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20de%20revenus%20et%20d'activit%C3%A9s%20Agessa.pdf), à compléter sur les 2 dernières années ;
* les [formulaires de détail des droits d’auteurs](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9tail%20des%20droits%20d'auteur.pdf) ;
* la copie de la déclaration fiscale de l’année de référence, certifiée conforme, ainsi que les avis d’imposition des deux dernières années civiles ;
* pour les ressortissants étrangers, une copie du titre de séjour, les justificatifs de perception de droits d’auteurs (copies des contrats de cession de droit d’auteur, note de droit d’auteurs, etc.) sur les 2 dernières années.