﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS063" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourismus, Freizeit, Kultur" -->
<!-- var(title)="Künstler-Autor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourismus-freizeit-kultur" -->
<!-- var(title-short)="kunstler-autor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/de/ds/tourismus-freizeit-kultur/kunstler-autor.html" -->
<!-- var(last-update)="2020-04-15 17:24:15" -->
<!-- var(url-name)="kunstler-autor" -->
<!-- var(translation)="Auto" -->


Künstler-Autor
==============

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:24:15<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

### a. Definition

Der Künstler-Autor ist ein Profi, der eine originelle kreative Tätigkeit in den folgenden Bereichen ausübt:

- Audiovisuelles;
- Musikalisch;
- Kinematographische
- choreographisch;
- Grafik;
- Literarisch;
- Fotografischen
- Kunststoff;
- Software.

**Beachten Sie, dass**

Die zeitweiligen Autoren der Schau oder Autoren, die von Urheberrechten profitieren, die vom Haus der Künstler oder der Association for the Management of The Social Security of Authors (Agessa) bezahlt werden, gelten nicht als Künstler-Autoren.

### b. zuständigeCFE

Die betreffende CFE hängt von der Art der Struktur ab, in der die Tätigkeit ausgeübt wird. Für den Künstler-Autor ist der kompetente CFE der Urssaf.

Zwei Grad. Berufsqualifikationen
--------------------------------

### a. Berufliche Qualifikationen

Es ist kein spezifisches Diplom erforderlich, um sich als Künstler-Autor zu qualifizieren.

### b. Berufsqualifikationen - EU- oder EWR-Bürger (Freier Dienst oder freie Niederlassung)

Ein Staatsangehöriger eines Mitgliedstaats der Europäischen Union (EU) oder Partei des Abkommens über den Europäischen Wirtschaftsraum (EWR) unterliegt ebenso wenig den Qualifikations- oder Zertifizierungsanforderungen wie die Franzosen.

### c. Einige Besonderheiten der Regelung der Tätigkeit

#### Workshop-Anfrage

Künstler, die in einem Workshop praktizieren möchten, müssen sich bei der Regionalen Direktion für Kulturelle Angelegenheiten (DRAC) in ihrer Region oder bei der Direktion Kultur ihrer Gemeinde bewerben.

Drei Grad. Installationsverfahren und Formalitäten
--------------------------------------------------

### a. Meldeformalitäten des Unternehmens

Der Auftragnehmer muss sich im Urssaf-Register registrieren. Der Künstler-Autor muss das Formular ausfüllen[Cerfa 13231*01](https://www.cfe.urssaf.fr/saisiepl/CFE_Declaration).

Die Erklärung wird zur Zuteilung eines EPA-Codes durch das INSEE und einer Siret-Nummer führen.

### b. Erklärung über den Beginn der Tätigkeit abgeben

Je nach Art ihrer Tätigkeit müssen Künstler-Autoren einer Sozialhilfeorganisation angeschlossen sein, um eine sozialversicherungiver Versicherung zu erhalten.

Dazu müssen sie beim ersten Verkauf oder im Falle eines festen Auftrags eine Erklärung abgeben, von dem sie entweder

- Das Haus der Künstler für grafische und bildende Künstler;
- der Agessa für Schriftsteller, Autoren literarischer oder wissenschaftlicher Schriften, Übersetzer, Musikkomponisten, Choreographen, Autoren von Film-, audiovisuellen und multimedialen Werken, Fotografen.

Diese Erklärung sollte es den Organisationen ermöglichen, zu überprüfen, ob die Tätigkeit des Antragstellers in den Anwendungsbereich eines Künstler-Autors fällt und zu einer Beitrags- und Sozialbeitragsbasis in der Künstler-Autoren.

Für Künstler-Autoren, die Teil des sozialdemokratischen Systems des Hauses der Künstler sind, muss die erste Aussage die folgenden Belege enthalten:

- das[Erklärung](http://www.secu-artistes-auteurs.fr/eform/submit/declaration_de_debut_d_exercice) ;
- Nachweis des Erstverkaufs: eine Kopie einer Rechnung, eines Urheberrechtsvermerks, eines Vertrags oder einer im laufenden Jahr erteilten Bestellung.

Das Künstlerhaus hat drei Wochen Zeit, um eine Quittung für den Beginn der Tätigkeit mit einer Kennungsnummer auszustellen, die für zukünftige Steuererklärungen verwendet wird.

Für Künstler-Autoren unter dem Agessa muss die Erklärung nach einem Jahr Praxis erfolgen und folgende Arbeiten enthalten:

- das[Deklarationsformular](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/Demande%20d%27ouverture%20de%20droits%20%C3%A0%20l%27Assurance%20Maladie%20%28Cerfa%20S1219%29.pdf) ;
- das[Steuer- und Tätigkeitsformulare](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20de%20revenus%20et%20d'activit%C3%A9s%20Agessa.pdf), die in den letzten 2 Jahren abgeschlossen werden soll;
- das[Copyright-Details](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9tail%20des%20droits%20d'auteur.pdf) ;
- eine Kopie der Steuererklärung für das Bezugsjahr, die als konform bescheinigt wurde, sowie Steuerbescheide für die letzten zwei Kalenderjahre;
- für Ausländer eine Kopie der Aufenthaltserlaubnis, Nachweis der Urheberrechtssammlung (Kopien von Urheberrechtsübertragungsverträgen, Urheberrechtsvermerke usw.) in den letzten 2 Jahren.

