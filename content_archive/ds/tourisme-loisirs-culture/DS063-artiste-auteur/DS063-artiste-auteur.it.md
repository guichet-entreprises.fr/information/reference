﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS063" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Turismo, Tempo libero, Cultura" -->
<!-- var(title)="Artista-autore" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="turismo-tempo-libero-cultura" -->
<!-- var(title-short)="artista-autore" -->
<!-- var(url)="https://www.guichet-entreprises.fr/it/ds/turismo-tempo-libero-cultura/artista-autore.html" -->
<!-- var(last-update)="2020-04-15 17:24:15" -->
<!-- var(url-name)="artista-autore" -->
<!-- var(translation)="Auto" -->


Artista-autore
==============

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:24:15<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

### a. Definizione

L'artista-autore è un professionista che si impegna in un'attività creativa originale nelle seguenti aree:

- Audiovisivo;
- Musical;
- Cinematografiche
- coreografico;
- Grafica;
- Letterario;
- Fotografica
- Plastica;
- Software.

**Si noti che**

Gli autori intermittenti dello spettacolo o gli autori che beneficiano dei diritti d'autore pagati dalla Casa degli Artisti o dall'Associazione per la Gestione della Previdenza Sociale degli Autori (Agessa) non sono considerati artisti-autori.

### b. CFE competente

Il CFE in questione dipende dalla natura della struttura in cui viene svolta l'attività. Per l'artista-autore, il CFE competente è l'Urssaf.

Due gradi. Qualifiche professionali
-----------------------------------

### a. Qualifiche professionali

Non è richiesto alcun diploma specifico per qualificarsi come artista-autore.

### b. Qualifiche professionali - Cittadini dell'UE o del CEE (Servizio gratuito o Stabilimento gratuito)

Un cittadino di uno Stato membro dell'Unione europea (UE) o parte dell'accordo sullo Spazio economico europeo (AEA) non è soggetto ad alcun requisito di qualificazione o certificazione, così come i francesi.

### c. Alcune peculiarità della regolamentazione dell'attività

#### Richiesta di officina

Gli artisti che desiderano praticare in un workshop devono rivolgersi alla Direzione Regionale degli Affari Culturali (DRAC) nella loro regione o alla Direzione Affari Culturali del loro comune.

Tre gradi. Procedure di installazione e formalità
-------------------------------------------------

### a. Le formalità di rendicontazione delle società

L'appaltatore deve registrarsi presso il registro Urssaf. L'artista-autore dovrà compilare il modulo[Cerfa 13231*01](https://www.cfe.urssaf.fr/saisiepl/CFE_Declaration).

La dichiarazione comporterà l'attribuzione di un codice EPA da parte dell'INSEE e di un numero Siret.

### b. Fare una dichiarazione di inizio dell'attività

A seconda della natura della loro attività, artista-autori devono essere affiliati a un'organizzazione di assistenza sociale al fine di ricevere una copertura di sicurezza sociale.

A tale scopo, devono effettuare una dichiarazione alla prima vendita o in caso di ordine fermo da:

- La Casa degli Artisti per artisti grafici e visivi;
- dell'Età per scrittori, autori di scritti letterari o scientifici, traduttori, compositori musicali, coreografi, autori di film, opere audiovisive e multimediali, fotografi.

La presente dichiarazione dovrebbe consentire alle organizzazioni di verificare che l'attività della ricorrente rientri nell'ambito di tale ambito di un artista-autore e che si traduca in una base di contributi e contributi sociali nel artisti-autori.

Per gli artisti-autori che fanno parte del sistema sociale della Casa degli Artisti, la prima dichiarazione deve includere i seguenti documenti giustificativi:

- Le[Dichiarazione](http://www.secu-artistes-auteurs.fr/eform/submit/declaration_de_debut_d_exercice) ;
- prova della prima vendita: una fotocopia di una fattura, una nota di copyright, un contratto o un ordine di acquisto emesso nell'anno in corso.

La Casa degli Artisti avrà tre settimane per emettere una ricevuta per l'inizio dell'attività con un numero identificativo che verrà utilizzato per le future dichiarazioni dei redditi.

Per gli artisti-autori nell'ambito dell'Età, la dichiarazione deve aver luogo dopo un anno di pratica e deve contenere i seguenti pezzi:

- Le[modulo di dichiarazione](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/Demande%20d%27ouverture%20de%20droits%20%C3%A0%20l%27Assurance%20Maladie%20%28Cerfa%20S1219%29.pdf) ;
- Le[moduli fiscali e di attività](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20de%20revenus%20et%20d'activit%C3%A9s%20Agessa.pdf), da completare negli ultimi 2 anni;
- Le[dettagli sul copyright](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9tail%20des%20droits%20d'auteur.pdf) ;
- Una copia della dichiarazione dei redditi per l'anno di riferimento, certificata come conforme, nonché avvisi fiscali per gli ultimi due anni di calendario;
- per i cittadini stranieri, una copia del permesso di soggiorno, la prova della raccolta del copyright (copie di contratti di trasferimento del copyright, note sul copyright, ecc.) negli ultimi 2 anni.

