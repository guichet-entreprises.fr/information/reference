﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS072" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Tobacconist" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="tobacconist" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/tobacconist.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="tobacconist" -->
<!-- var(translation)="Auto" -->

Tobacconist
================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The tobacco retailer, also known as a buralist, is a professional specializing in the retail sale of different types of ready-to-eat tobacco (cigarettes, cigars, rolling tobacco, etc.).

There are two categories of tobacco outlets:

- Regular tobacco flow
- special tobacco outlets in the transport sector (rail networks, airports, etc.) or other public sectors.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

In order to carry out the tobacco-debiting activity, the person concerned must have undergon vocational training in one of the[training centres](http://www.douane.gouv.fr/articles/a10942-formation-pour-la-vente-au-detail-des-tabacs-manufactures) approved by the Directorate General of Customs and Indirect Duties.

The training includes five modules that will enable them to acquire knowledge of the regulations related to the management of a tobacco outlet and the basics of business management.

**Please note**

As soon as the manager of a tobacco outlet has to renew his management contract (see infra "3.0. c. Obligation to enter into a management contract"), he must, within six months of the renewal, undergo a one-day continuing vocational training course at one of the training centres.

The professional who justifies an initial training will be able to establish himself in the form of:

- individual exploitation;
- a partnership consisting solely of individuals.

*For further information*: order of 25 August 2010 relating to the modalities of initial and continuous vocational training for the retail sale of manufactured tobacco.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

There are no regulations for the national of a Member State of the European Union (EU) or part of the European Economic Area (EEA) wishing to carry out the tobacco-debiting activity in France, on a temporary and casual basis or Permanent.

Therefore, only the measures taken for the French national will find to apply (see infra "3°. Installation procedures and formalities").

### c. Conditions of honorability and incompatibility

Applicants who wish to manage a tobacco outlet must meet conditions of honour and incompatibility such as:

- Be French or a national of an EU or EEA state
- Be of age
- Not be under guardianship or curatorial;
- Not to be a manager of another tobacco outlet or a substitute for a current manager;
- provide guarantees of honour and probity that will be appreciated by providing an excerpt from bulletin 2 of his criminal record;
- enjoy his civil rights.

**Please note**

The future manager must justify that he is physically fit to hold a tobacco debit, including by providing a medical certificate established by a doctor approved by the regional health agency.

### d. Some peculiarities of the regulation of the activity

#### Limits on the implantation of a tobacco outlet

The establishment of a tobacco outlet can only be done within the limit of one establishment for 3,500 inhabitants.

However, since a municipality has less than 3,500 inhabitants and does not already have a tobacco outlet, its implementation is allowed.

#### Implementation bans

In addition to the condition related to the number of inhabitants, prohibitions may exist on the location of a tobacco outlet, including:

- In shopping malls where self-service food outlets are located;
- shopping malls
- within the scope of a temporarily closed tobacco outlet;
- In protected areas.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the activity sheets "Formality of Reporting a Commercial Company" and "Registration of a Commercial Individual Company at the RCS."

### b. Conditions for setting up a tobacco outlet

Implementation is a procedure by which the administration decides, either on its own initiative or at the request of an interested person, the operation of a new tobacco outlet. It can be done in two ways:

- by transfer (priority);
- failing that, by call for applications.

#### Transfer implementation

The Regional Director of Customs authorizes the buralist to operate at an existing rate. He will then act as successor to the flow management.

**Procedure**

The Regional Director of Customs publishes a notice in a newspaper of legal announcements in the department of the place where the debit is located. Those interested in having tobacco products already in practice will have three months to request the transfer of their debit. The director chooses the debitant based on [precise criteria](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=F3F3B0498EBFDA155B5B9619E7C33A61.tplgfr35s_3?idArticle=LEGIARTI000032881684&cidTexte=LEGITEXT000022409542&dateTexte=20180125) and will notify the successful applicant of the transfer authorization.

*For further information*: Articles 14 to 17 of the Decree of 28 June 2010.

#### Implementation by call for applications

Where the transfer procedure has not been possible, the Regional Director of Customs may initiate a two-month application process during which applicants will be required to submit a file that meets the specifications of the administration.

The administration will retain the bid that offers the best guarantees and then have it sign a management contract.

*For further information*: Articles 18 and 19 of the June 28, 2010 decree.

### c. Obligation to enter into a management contract

The debitant authorized to start the tobacco debit activity must, after having established himself, enter into a management contract with the administration. The three-year contract, which is tacitly renewable, will allow the buralist to supply tobacco outlets.

*For further information*: Article 2 of the decree of 28 June 2010.

### d. Post-registration authorisation

#### Obligation to provide a commercial space layout plan

Once the professional has received permission to set up shop, he must send by registered mail, prior to the start of the activity, a plan of arrangement of the premises to the regional director of customs territorially competent.

This arrangement must take into account a specification set out in the annex of the decree of 13 December 2011.

The administration's silence within a fortnight of sending the plan is worth accepting the layout plan.

*For further information*: decree of 13 December 2011 relating to the arrangement of tobacco debit.

#### Other obligations

Many obligations fall to the buralist in office, including:

- indicate the presence of its point of sale by a sign that has the proper shape of a red diamond and which may possibly be marked "tobacco";
- Do not sell tobacco to minors;
- Post "no smoking" in the grounds of the establishment;
- respect[safety standards](https://www.guichet-entreprises.fr/fr/creation-dentreprise/les-prealables-a-la-creation-dentreprise/letablissement-recevant-du-public-erp/obligations-de-securite/) as a public-receiving institution (ERP).

