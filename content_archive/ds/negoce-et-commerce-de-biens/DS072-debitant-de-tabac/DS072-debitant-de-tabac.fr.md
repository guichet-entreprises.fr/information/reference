﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS072" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Débitant de tabac" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="debitant-de-tabac" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/debitant-de-tabac.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="debitant-de-tabac" -->
<!-- var(translation)="None" -->

# Débitant de tabac

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le débitant de tabac, aussi appelé buraliste, est un professionnel spécialisé dans la vente au détail de différents types de tabac prêts à la consommation (cigarettes, cigares, tabac à rouler, etc.).

Deux catégories de débits de tabac existent :

- le débit de tabac ordinaire ;
- le débit de tabac spécial implanté sur le secteur des transports (réseaux ferré, aéroportuaire, etc.) ou autres secteurs publics.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité de débitant de tabac, l'intéressé doit avoir suivi une formation professionnelle dans l'un des [centres de formation](http://www.douane.gouv.fr/articles/a10942-formation-pour-la-vente-au-detail-des-tabacs-manufactures) agréés par la direction générale des douanes et droits indirects.

La formation comprend cinq modules qui lui permettront d'acquérir les connaissances de la réglementation liée à la gestion d'un débit de tabac et les notions de gestion de base d'un commerce.

**À noter**

Dès lors que le gérant d'un débit de tabac doit renouveler son contrat de gérance (cf. infra « 3°. c. Obligation de conclure un contrat de gérance »), il devra, dans les six mois précédent le renouvellement, suivre une formation professionnelle continue d'une journée auprès de l'un des centres de formation.

Le professionnel qui justifie d'une formation initiale pourra s'implanter sous la forme soit :

- d'une exploitation individuelle ;
- d'une société en nom collectif composée uniquement de personnes physiques.

*Pour aller plus loin* : arrêté du 25 août 2010 relatif aux modalités de formation professionnelle initiale et continue pour la vente au détail des tabacs manufacturés.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un État membre de l’Union européenne (UE) ou partie de l’Espace économique européen (EEE) souhaitant exercer l'activité de débitant de tabac en France, à titre temporaire et occasionnel ou permanent.

Dès lors, seules les mesures prise pour le ressortissant français trouveront à s'appliquer (cf. infra « 3°. Démarches et formalités d’installation »).

### c. Conditions d'honorabilité et incompatibilités

Le candidat qui souhaite gérer un débit de tabac doit remplir des conditions d'honorabilité et d'incompatibilité telles que :

- être français ou ressortissant d'un État de l'UE ou de l'EEE ;
- être majeur ;
- ne pas être sous tutelle ou curatelle ;
- ne pas être gérant d'un autre débit de tabac ou suppléant d'un gérant en exercice ;
- présenter des garanties d'honorabilité et de probité qui seront appréciées en fournissant un extrait du bulletin n° 2 de son casier judiciaire ;
- jouir de ses droits civiques.

**À noter**

Le futur gérant doit justifier qu'il est apte physiquement à tenir un débit de tabac, notamment en fournissant un certificat médical établi par un médecin agréé par l'agence régional de santé.

### d. Quelques particularités de la réglementation de l'activité

#### Limites à l'implantation d'un débit de tabac

L'implantation d'un débit de tabac ne peut se faire que dans la limite d'un établissement pour 3 500 habitants.

Toutefois, dès lors qu'une commune compte moins de 3 500 habitants et ne comporte pas déjà de débit de tabac, son implantation est autorisée.

#### Interdictions d'implantation

Outre la condition liée au nombre d'habitants, des interdictions peuvent exister sur l'endroit d'implantation d'un débit de tabac, et notamment :

- dans les galeries marchandes où se trouvent des établissement de vente de produits alimentaires en libre service ;
- dans les centres commerciaux ;
- dans le périmètre d'implantation d'un débit de tabac fermé provisoirement ;
- en zone protégée.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l'entreprise.

### b. Conditions d'implantation d'un débit de tabac

L'implantation est une procédure par laquelle l'administration décide, soit de sa propre initiative, soit sur demande d'une personne intéressée, de l'exploitation d'un nouveau débit de tabac. Elle peut se faire de deux façons :

- par transfert (prioritairement) ;
- à défaut, par appel à candidatures.

#### Implantation par transfert

Le directeur régional des douanes autorise le buraliste à exercer son activité dans un débit existant. Il interviendra alors comme successeur à la gérance du débit.

##### Procédure

Le directeur régional des douanes publie un avis dans un journal d'annonces légales dans le département du lieu où se trouve le débit. Les intéressés, débitants de tabac déjà en exercice, auront trois mois pour demander le transfert de leur débit. Le directeur choisit le débitant en fonction de [critères précis](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=F3F3B0498EBFDA155B5B9619E7C33A61.tplgfr35s_3?idArticle=LEGIARTI000032881684&cidTexte=LEGITEXT000022409542&dateTexte=20180125) et notifiera au candidat retenu l'autorisation de transfert.

*Pour aller plus loin* : articles 14 à 17 du décret du 28 juin 2010.

#### Implantation par appel à candidatures

Lorsque la procédure par transfert n'a pas pu se faire, le directeur régional des douanes peut engager une procédure par appel à candidatures d'une durée de deux mois pendant laquelle les candidats devront présenter un dossier remplissant le cahier des charges de l'administration.

L'administration retiendra la candidature qui présente les meilleures garanties puis lui fera signer un contrat de gérance.

*Pour aller plus loin* : articles 18 et 19 du décret du 28 juin 2010.

### c. Obligation de conclure un contrat de gérance

Le débitant autorisé à commencer l'activité de débit de tabac doit, après s'être implanté, conclure un contrat de gérance avec l'administration. D'une durée de trois ans et renouvelable tacitement, le contrat permettra au buraliste d'approvisionner sont point de vente en tabacs.

*Pour aller plus loin* : article 2 du décret du 28 juin 2010.

### d. Autorisation(s) post-immatriculation

#### Obligation de fournir un plan d'agencement du local commercial

Lorsque le professionnel a reçu l'autorisation de s'implanter, il doit transmettre par courrier recommandé, préalablement au commencement de l'activité, un plan d'agencement du local au directeur interrégional des douanes territorialement compétent.

Cet agencement doit tenir compte d'un cahier des charges fixé à l'annexe de l'arrêté du 13 décembre 2011.

Le silence gardé de l'administration dans un délai de quinze jours suivant l'envoi du plan vaut acceptation du plan d'agencement.

*Pour aller plus loin* : arrêté du 13 décembre 2011 relatif à l'agencement du débit de tabac.

#### Autres obligations

De nombreuses obligations incombent au buraliste en exercice, parmi lesquelles :

- indiquer la présence de son point de vente par une enseigne qui a obligatoirement la forme d'un losange rouge et qui peut éventuellement porter la mention « tabac » ;
- ne pas vendre de tabac aux mineurs ;
- afficher la mention « interdiction de fumer » dans l'enceinte de son établissement ;
- respecter les [normes de sécurité](https://www.guichet-entreprises.fr/fr/creation-dentreprise/les-prealables-a-la-creation-dentreprise/letablissement-recevant-du-public-erp/obligations-de-securite/) en tant qu'établissement recevant du public (ERP).