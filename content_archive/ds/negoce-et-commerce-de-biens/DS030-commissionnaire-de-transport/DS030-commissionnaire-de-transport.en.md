﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS030" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Forwarding agent" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="forwarding-agent" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/forwarding-agent.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="forwarding-agent" -->
<!-- var(translation)="Auto" -->

Forwarding agent
======================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

The transport commissionaire is a professional whose mission is to organize the movement of goods from one place to another, according to the modes and means of his choice.

He acts as an intermediary since he enters into a transport commission contract with his client, and one or more transport contracts with carriers that he charters on his behalf.

Section R. 1411-1 of the Transportation Code states that the various activities of the Transportation Commissioner are:

- Grouping operations: sending goods from multiple shippers or to multiple recipients, assembled and formed into a single batch for transport;
- Charter operations: shipments are entrusted without prior grouping to public carriers;
- City office operations: the commissionaire takes care of parcels or retail shipments and hands them separately to either public carriers or other transport commissionaires;
- Transport organisation operations: the commissionaire takes care of goods to and from the national territory, and provides transport by one or more public carriers through any route.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for a liberal profession, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Professional qualifications
----------------------------------------

### a. Professional qualifications

The profession of transport commissionaire is reserved for anyone with a certificate of registration in the road transport register.

In order to be registered, the person must be in possession of a certificate of professional capacity issued by the prefect of the region as long as he justifies either:

- to have a diploma in higher education, having received legal, economic, accounting, commercial or technical training, or a technical education diploma, having received training in transport activities;
- to have passed the tests of a written exam conducted at an accredited training centre
- recognition of professional qualifications acquired in a European Union (EU) member state or party to the European Economic Area (EEA) agreement.

**Please note**

The professional capacity required to practise as a transport commissionaire is acquired as long as the person has undergone training in business management of at least 200 hours of training or when he has completed an 80-hour internship. ensuring a sufficient level of law applied to transport, transport economics and the transport commission.

*For further information*: Article R. 1422-4 of the Transport Code; Articles 7 and 8 of the Order of 21 December 2015 relating to the issuance of the certificate of professional capacity allowing the exercise of the profession of transport commissionaire.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

#### For a temporary and casual exercise (Freedom to provide services)

There are no regulations for a national of an EU or EEA who wishes to practise as a transport commissionaire in France, either on a temporary or casual basis.

Therefore, only the measures taken for the Freedom of establishment of EU or EEA nationals will apply.

### For a permanent exercise (Freedom of establishment)

In order to carry out the activity of transport commissionaire in France on a permanent basis, the EU or EEA national must meet one of the following conditions:

- hold a certificate of competency or training certificate required for the exercise of the activity of transport commissionaire in an EU or EEA state when that state regulates access or the exercise of this activity on its territory;
- have worked full-time or part-time for one year in the past ten years in another state that does not regulate training or the practice of the profession;
- have a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has been a transport commissionaire in the State for three years, admitted equivalence.

Once he fulfils one of these conditions, the national will be able to apply for recognition of his professional qualifications from the prefect of the region in which he wishes to practice his profession (see infra "3°. c. Request recognition of his professional qualifications for the EU or EEA national for permanent activity (LE)).

Where there are substantial differences between the professional qualification of the national and the training required in France, the territorially competent prefect may require that the person concerned submit to compensation measures.

*For further information*: Articles R. 1422-11 and R. 1422-15 of the Transportation Code.

### c. Conditions of honorability, ethical rules, ethics

The transport commissioner must meet conditions of honour and must not, among other things:

- have been convicted by a French court, registered in bulletin 2 of the criminal record, or by a foreign court and recorded in an equivalent document, and pronouncing a ban on practising a commercial profession or Industrial;
- driving without a licence
- Resort to concealed labour
- to transport so-called dangerous goods.

*For further information*: Article R. 1422-7 of the Transportation Code.

### d. Some peculiarities of the regulation of the activity

#### Commissionary's obligations

The commissionaire must keep in a register in the form of a book-newspaper, information relating to the nature and quantity of the goods he carries.

In addition, it is guaranteed their arrival on time, as well as in case of damage or loss.

*For further information*: Articles L. 132-3 to L. 132-9 of the Code of Commerce.

#### Mandatory mentions of the transport commission contract

When the transport contract is concluded, the commissionaire must ensure that the following information is mentioned:

- nature and purpose of transport;
- how the service is carried out both with regard to the actual transport and the conditions for the removal and delivery of the objects transported;
- the respective obligations of the sender, the commissionaire, the carrier and the recipient;
- prices for transport as well as ancillary services.

*For further information*: Article L. 1432-2 of the Transportation Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the contractor must register with the Register of Trade and Companies (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Request a certificate of professional ability

The professional who wishes to carry out the activity of transport commissionaire must have a certificate of professional capacity.

**Competent authority**

The regional prefect is responsible for handing over the certificate of professional capacity.

**Supporting documents**

The application for certification is sent to the Regional Directorate of Environment, Planning and Housing and includes the following supporting documents:

- The form Cerfa 11414*05 ;
- Proof of residence
- The document justifying its situation in relation to the obligations of the national service;
- A photocopy of the diploma or graduation title
- When the applicant is employed, a photocopy of his employment contract and pay slips;
- a certificate of membership in a pension fund.

*For further information*: Article R. 1422-4 of the Transport Code; order of 21 December 2015 relating to the issuance of the certificate of professional capacity allowing the exercise of the profession of transport commissionaire.

### c. Request recognition of professional qualifications for EU or EEA nationals for permanent activity (LE)

**Competent authority**

The regional prefect is competent to issue the certificate of professional capacity justifying the recognition of the national's professional qualifications.

**Supporting documents**

The application for certification is made by sending a file to the prefect, including the following documents:

- Form Cerfa No. 11414*05 ;
- A valid piece of identification
- a proof of residence, for the person who has his or her usual residence in France;
- A copy of the certificate of competence or training certificate issued by an EU or EEA state that regulates the profession of transport commissionaire;
- If so, any document justifying the applicant's legal exercise as a freight operator for one year in the past ten years in an EU or EEA state that does not regulate access to the profession or the Training
- if applicable, any document justifying the applicant's actual exercise for at least three years, in a state that has granted equivalence a training certificate or certificate acquired in a third state and allowing the exercise of that Profession;
- training programs or the content of the experience gained.

**Please note**

If necessary, all supporting documents must be translated into French by a certified translator.

**Procedure**

When the file is sent, the prefect will acknowledge receipt within one month and inform the national of any missing documents. Unless compensation measures are taken against the national, delaying the procedure, the prefect will be able to issue the certificate of professional capacity within one month.

Once the national has obtained his certificate of professional capacity, he will be able to register in the road transport register.

*For further information*: Articles 9 to 15 of the December 21, 2015 order on the issuance of the certificate of professional capacity allowing the practice of the profession of transport commissionaire

**Good to know compensation measures**

In case of differences between the training or experience of the national and the requirements required in France, the prefect may submit him to a compensation measure that may be an adjustment course or an aptitude test.

The adaptation course must last at least 80 hours and allow it to acquire a sufficient level of law applied to transport, transport economics and the transport commission.

The aptitude test is in the form of a multiple-choice questionnaire for which the national must obtain a score of at least 60 out of 100.

*For further information*: Article R. 1422-18 of the Transportation Code.

### d. Request registration on the road transport register

Registration is done by sending the form Cerfa No. 14557*03 transport authority.