﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS030" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Commissionnaire de transport" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="commissionnaire-de-transport" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/commissionnaire-de-transport.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="commissionnaire-de-transport" -->
<!-- var(translation)="None" -->

# Commissionnaire de transport

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l'activité

### a. Définition

Le commissionnaire de transport est un professionnel dont la mission est d'organiser le déplacement des marchandises d'un lieu à un autre, selon les modes et les moyens de son choix.

Il intervient en qualité d'intermédiaire puisqu'il conclut un contrat de commission de transport avec son client, et un ou plusieurs contrats de transport avec des transporteurs qu'il affrète en son nom.

L'article R. 1411-1 du Code des transports précise que les diverses activités du commissionnaire de transport sont :

- les opérations de groupage : envoi de marchandises provenant de plusieurs expéditeurs ou à l'adresse de plusieurs destinataires, réunies et constituées en un lot unique en vue de leur transport ;
- les opérations d'affrètement : des envois sont confiés sans groupage préalable à des transporteurs publics ;
- les opérations de bureau de ville : le commissionnaire prend en charge des colis ou expéditions de détail et les remet séparément soit à des transporteurs publics, soit à d'autres commissionnaires de transport ;
- les opérations d'organisation de transport : le commissionnaire prend en charge des marchandises en provenance ou à destination du territoire national, et en assure l'acheminement par les soins d'un ou plusieurs transporteurs publics par quelque voie que ce soit.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une profession libérale, le CFE compétent est l'Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Qualifications professionnelles

### a. Qualifications professionnelles

La profession de commissionnaire de transport est réservée à toute personne titulaire d'un certificat d'inscription au registre de transport routier.

Pour être inscrit, l'intéressé doit être en possession d'une attestation de capacité professionnelle délivrée par le préfet de région dès lors qu'il justifie soit :

- d'être titulaire d'un diplôme de l'enseignement supérieur, en ayant suivi une formation juridique, économique, comptable, commerciale ou technique, ou d'un diplôme d'enseignement technique, en ayant suivi une formation aux activités du transport ;
- d'avoir réussi les épreuves d'un examen écrit effectué dans un centre de formation agréé ;
- de la reconnaissance de ses qualifications professionnelles acquises dans un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE).

**À noter**

La capacité professionnelle requise pour exercer la profession de commissionnaire au transport est acquise dès lors que l'intéressé a suivi une formation à la gestion d'entreprise d'au moins 200 heures de formation ou lorsqu'il a suivi un stage de 80 heures lui assurant un niveau suffisant en droit appliqué au transport, à l'économie des transports et à la commission de transport.

*Pour aller plus loin* : article R. 1422-4 du Code des transports ; articles 7 et 8 de l'arrêté du 21 décembre 2015 relatif à la délivrance de l'attestation de capacité professionnelle permettant l'exercice de la profession de commissionnaire de transport.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### Pour un exercice temporaire et occasionnel (Libre Prestation de Services)

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un État de l'UE ou de l'EEE souhaitant exercer la profession de commissionnaire de transport en France, à titre temporaire ou occasionnel.

Dès lors, seules les mesures prises pour le libre établissement des ressortissants de l'UE ou de l'EEE trouveront à s'appliquer.

### Pour un exercice permanent (Libre Établissement)

Pour exercer l’activité de commissionnaire de transport en France à titre permanent, le ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- être titulaire d’une attestation de compétence ou d’un titre de formation requis pour l’exercice de l’activité de commissionnaire de transport dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- avoir exercé la profession à temps plein ou à temps partiel, pendant un an au cours des dix dernières années dans un autre État qui ne réglemente ni la formation, ni l'exercice de la profession ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité de commissionnaire de transport dans l’État qui a admis l’équivalence.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander la reconnaissance de ses qualifications professionnelles auprès du préfet de région dans laquelle il souhaite exercer sa profession (cf. infra « 3°. c. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE) »).

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, le préfet territorialement compétent peut exiger que l'intéressé se soumette à des mesures de compensation.

*Pour aller plus loin* : articles R. 1422-11 et R. 1422-15 du Code des transports.

### c. Conditions d'honorabilité, règles déontologiques, éthique

Le commissionnaire au transport doit respecter des conditions d'honorabilité et ne doit pas, notamment :

- avoir fait l'objet d’une condamnation par une juridiction française, inscrite au bulletin n° 2 du casier judiciaire, ou par une juridiction étrangère et inscrite dans un document équivalent, et prononçant une interdiction d’exercer une profession commerciale ou industrielle ;
- conduire sans permis ;
- recourir au travail dissimulé ;
- faire transporter des marchandises dites dangereuses.

*Pour aller plus loin* : article R. 1422-7 du Code des transports.

### d. Quelques particularités de la réglementation de l'activité

#### Obligations du commissionnaire de transport

Le commissionnaire doit conserver dans un registre sous forme de livre-journal, les informations relatives à la nature et à la quantité des marchandises qu'il transporte.

Par ailleurs, il est tenu garant de leur arrivée dans les délais impartis, ainsi qu'en cas d'avaries ou de pertes.

*Pour aller plus loin* : articles L. 132-3 à L. 132-9 du Code de commerce.

#### Mentions obligatoires du contrat de commission de transport

Lors de la conclusion du contrat de transport, le commissionnaire doit s'assurer de la mention des informations suivantes :

- nature et objet du transport ;
- modalités d'exécution du service tant en ce qui concerne le transport proprement dit que les conditions d'enlèvement et de livraison des objets transportés ;
- obligations respectives de l'expéditeur, du commissionnaire, du transporteur et du destinataire ;
- prix du transport ainsi que celui des prestations accessoires prévues.

*Pour aller plus loin* : article L. 1432-2 du Code des transports.

## 3°. Démarche et formalités d'installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre du commerce et des sociétés (RCS).

### b. Demander une attestation de capacité professionnelle

Le professionnel qui souhaite exercer l'activité de commissionnaire de transport doit être titulaire d'une attestation de capacité professionnelle.

**Autorité compétente**

Le préfet de région est compétent pour remettre l'attestation de capacité professionnelle.

**Pièces justificatives**

La demande d'attestation est envoyée à la direction régionale de l'environnement, de l'aménagement et du logement et comprend les pièces justificatives suivantes :

- le formulaire Cerfa n° 11414*05 ;
- un justificatif de domicile ;
- le document justifiant sa situation au regard des obligations du services national ;
- une photocopie du diplôme ou du titre de fin d'études ;
- lorsque le demandeur est salarié, une photocopie de son contrat de travail et ses bulletins de paie ;
- un certificat d'affiliation à une caisse de retraite.

*Pour aller plus loin* : article R. 1422-4 du Code des transports ; arrêté du 21 décembre 2015 relatif à la délivrance de l'attestation de capacité professionnelle permettant l'exercice de la profession de commissionnaire de transport.

### c. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE)

**Autorité compétente**

Le préfet de région est compétent pour remettre l'attestation de capacité professionnelle justifiant la reconnaissance de qualifications professionnelles du ressortissant.

**Pièces justificatives**

La demande d'attestation se fait par l'envoi d'un dossier au préfet, comportant les pièces suivantes :

- le formulaire Cerfa n° 11414*05 ;
- une pièce d'identité en cours de validité ;
- un justificatif de domicile, pour la personne qui a sa résidence habituelle en France ;
- une copie de l'attestation de compétence ou du titre de formation délivré par un État de l'UE ou de l'EEE qui réglemente la profession de commissionnaire de transport ;
- le cas échéant, tout document justifiant que le demandeur a exercé légalement des fonctions d'organisateur de transport de marchandises, pendant un an au cours des dix dernières années, dans un État de l'UE ou de l'EEE qui ne réglemente ni l'accès à la profession, ni la formation ;
- le cas échéant, tout document justifiant que le demandeur a exercé effectivement l'activité pendant au moins trois ans, dans un État qui a admis en équivalence un titre de formation ou un certificat acquis dans un État tiers et permettant l'exercice de cette profession ;
- les programmes des formations ou le contenu de l'expérience acquise.

**À noter**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français par un traducteur agréé.

**Procédure**

À l'envoi du dossier, le préfet en accusera réception dans un délai d'un mois et informera le ressortissant de tout document manquant. Sauf si des mesures de compensation sont prises à l'égard du ressortissant, retardant la procédure, le préfet pourra délivrer l'attestation de capacité professionnelle dans un délai d'un mois.

Une fois que le ressortissant a obtenu son attestation de capacité professionnelle, il pourra s'inscrire au registre de transport routier.

*Pour aller plus loin* : articles 9 à 15 de l'arrêté du 21 décembre 2015 relatif à la délivrance de l'attestation de capacité professionnelle permettant l'exercice de la profession de commissionnaire de transport

**Bon à savoir mesures de compensation**

En cas de différences entre la formation ou l'expérience du ressortissant et les exigences requises en France, le préfet pourra le soumettre à une mesure de compensation qui pourra être un stage d'adaptation ou une épreuve d'aptitude.

Le stage d'adaptation doit durer au moins 80 heures et lui permettre d'acquérir un niveau suffisant en droit appliqué au transport, à l'économie des transports et à la commission de transport.

L'épreuve d'aptitude se présente sous la forme d'un questionnaire à choix multiple pour lequel le ressortissant devra obtenir une note d'au moins 60 sur 100.

*Pour aller plus loin* : article R. 1422-18 du Code des transports.

### d. Demander son inscription au registre de transport routier

L'inscription au registre se fait par l'envoi du formulaire Cerfa n° 14557*03 à la direction des transports territorialement compétente.