﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS079" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="pt" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comércio e comércio de mercadorias" -->
<!-- var(title)="Galeria de Arte" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-e-comercio-de-mercadorias" -->
<!-- var(title-short)="galeria-de-arte" -->
<!-- var(url)="https://www.guichet-entreprises.fr/pt/ds/comercio-e-comercio-de-mercadorias/galeria-de-arte.html" -->
<!-- var(last-update)="2020-04-15 17:24:01" -->
<!-- var(url-name)="galeria-de-arte" -->
<!-- var(translation)="Auto" -->


Galeria de Arte
===============

Última atualização: : <!-- begin-var(last-update) -->2020-04-15 17:24:01<!-- end-var -->



<!-- begin-include(disclaimer-trans-pt) -->

**Aviso sobre a qualidade da tradução automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página foi traduzida usando uma ferramenta de tradução automática e podem conter erros. Os usuários são aconselhados a verificar a precisão das informações fornecidas nesta página antes de empreender qualquer acção.

O Serviço de Banco Empresa não pode ser responsabilizada pela operação de informação que será devido imprecisa a uma tradução não-automática fiel ao original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definindo a atividade
------------------------

### a. Definição

Um lugar aberto ao público onde um dono de galeria expõe e vende obras de arte. Ele é pago recebendo uma comissão sobre as vendas que faz nacional ou internacionalmente.

**O que saber**

A atividade do dono da galeria é distinta da de alguns artistas que vendem diretamente de seu estúdio.

### b. Centro de Formalidade Empresarial Competente

O centro de formalidades empresariais relevantes (CFE) depende da natureza da estrutura em que a atividade é realizada. No caso de uma galeria de arte, é uma atividade comercial. A CFE relevante é, portanto, a Câmara de Comércio e Indústria (CCI).

**É bom saber**

Um comerciante é uma pessoa que realiza atos de comércio como de costume. São considerados atos de comércio: a compra de bens para revenda, o aluguel de móveis, operações de fabricação ou transporte, as atividades de intermediários (incluindo corretagem e agência), etc.

Dois graus. Condições de instalação
-----------------------------------

### a. Qualificações profissionais

Nenhum diploma ou treinamento especial é necessário para praticar como proprietário de galeria. No entanto, é melhor adquirir um bom domínio do mercado da arte e uma cultura artística sólida. Portanto, é aconselhável se formar em uma escola de arte ou universidade.

O [Site da Comissão Nacional de Certificação Profissional](http://www.rncp.cncp.gouv.fr/) (CNCP) oferece uma lista de todas essas qualificações profissionais.

### b. Qualificações Profissionais - Nacionais Europeus (LPS ou LE)

#### Em caso de prestação gratuita de serviços (LPS)

Não há especificidade para os cidadãos de um dos Estados-Membros da União Europeia (UE) ou da Área Econômica Europeia (EEE) que desejam estabelecer-se na França de forma ad hoc e temporária.

#### Em caso de estabelecimento livre (LE)

Não há especificidade para os cidadãos de qualquer um dos Estados-Membros da UE ou do EEE que queiram se estabelecer permanentemente na França.

### c. Algumas peculiaridades da regulação da atividade

#### Cumprir as normas gerais aplicáveis a todas as instituições públicas (ERP)

Se o local for aberto ao público, o profissional deve cumprir as regras para as instituições que recebem o público:

- Fogo
- Acessibilidade.

Para obter mais informações, é aconselhável consultar a listagem " [Estabelecimento recebendo o público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

#### Faça uma declaração com a Casa dos Artistas ou agessa

O dono da galeria é considerado uma emissora comercial de obras de artistas-autores. Como tal, deve informar anualmente seu volume de negócios ou comissões. É com base nesta declaração que é calculado o valor da contribuição para a seguridade social e a formação profissional. Essas declarações podem ser feitas online no [site oficial de segurança social de autores artistas](http://www.secu-artistes-auteurs.fr/).

Se a declaração for feita com a Casa dos Artistas, ela assume a forma de uma declaração anual a ser feita antes de 1º de maio.

Por outro lado, se a declaração for feita com a associação para a gestão da seguridade social dos autores (Agessa), ela assume a forma de uma declaração trimestral.

Para obter mais informações, é aconselhável consultar o [site oficial da segurança social de artistas-autores](http://www.secu-artistes-auteurs.fr/).

*Para ir mais longe* Os artigos R. 382-17 e o seguinte do Código Previdenciário.

#### Exigência de manter um registro policial caso o trabalho não venha diretamente do estúdio do artista

Se o dono da galeria vende obras que não vêm diretamente do estúdio do artista, ele deve manter um registro conhecido como "registro policial". Este registro deve ser mantido diariamente e conter uma descrição dos objetos adquiridos ou mantidos à venda. Além disso, este cadastro deve permitir a identificação dessas obras, bem como a das pessoas que as venderam.

Um modelo de registro é proposto na Agenda 1 da ordem de 21 de julho de 1992.

Um profissional que foge dessa obrigação enfrenta uma sentença de seis meses de prisão e uma multa de 30.000 euros.

*Para ir mais longe* Artigos 321-7, R.321-3 a R.321-7 do Código Penal e Anexo 1 da Ordem de 21 de Julho de 1992.

#### Exigência de manter um registro policial para metais preciosos trabalhados ou não trabalhados

O proprietário da galeria deve manter um registro "policial" de suas compras, vendas, recibos e entregas de metais preciosos ou obras contendo esses materiais (ouro, prata, platina ou qualquer liga desses metais), sejam eles feitos ou não. O proprietário da galeria deve manter este registro à disposição do poder público, em qualquer requisição.

O objetivo deste registro é garantir a rastreabilidade das obras de metais preciosos e limitar o risco de recebimento.

*Para ir mais longe* 537 a 539 do Código Tributário Geral e circular de 22 de julho de 2010 sobre a garantia de metais preciosos e os termos de registro (o chamado "livro policial").

#### Obrigação de compra de pessoas conhecidas ou com entrevistados conhecidos por eles

Os comerciantes de ouro, prata, platina ou contêineres só podem comprar essas mercadorias de pessoas conhecidas ou com os entrevistados conhecidos por eles.

*Para ir mais longe* Seção 539 do Código Tributário Geral.

#### Exigência de exibição de preço

Um rótulo discreto sobre obras de arte originais (e objetos da antiguidade) em exposição é permitido em galerias de arte. Além disso, a capacidade do cliente de visualizar uma tabela de preços é suficiente para cumprir a obrigação do galerista de informar o consumidor sobre o preço das mercadorias em exposição.

*Para ir mais longe* : circular de 19 de julho de 1988 na exposição dos preços dos livros originais e usados.

Três graus. Procedimentos de instalação e formalidades
------------------------------------------------------

### a. Formalidades de relatórios da empresa

A empresa deve estar registrada no Registro de Comércio e Empresas (RCS). Para obter mais informações, recomenda-se ver as "Formalidades de Relatórios de Empresas Comerciais" e "Registro de uma Empresa Comercial no RCS".

### b. Se necessário, faça uma declaração de existência com a Casa dos Artistas

O dono da galeria que distribui obras de arte a classificação de artes gráficas e plásticas deve fazer a declaração à Casa dos Artistas.

As artes gráficas e plásticas incluem pinturas, desenhos, gravuras, gravuras, litografias, esculturas, tapeçarias e tecidos de parede artesanal, exemplos únicos de cerâmica, etc. Para mais detalhes, é aconselhável consultar o [site oficial da Casa dos Artistas](%5b*http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/*%5d(http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/)).

Esta afirmação identifica o declarante como uma emissora comercial de obras de artistas-autores, em dívida, como tal, à contribuição correspondente.

**Autoridade competente**

A declaração deve ser endereçada à Casa dos Artistas.

**Tempo**

A declaração deve ocorrer no prazo de oito dias após o início da atividade do inscrito. A Casa dos Artistas reconhece o recebimento da declaração de atividade e emite um identificador chamado número do pedido.

**Documentos de suporte**

A declaração deve incluir:

- O [Formulário de Declaração de Existência](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d%E2%80%99existence.pdf) concluído, datado e assinado;
- uma cópia do Certificado de Registro do Diretório de Empresas e Estabelecimentos (Sirene).

**Custo**

Livre.

*Para ir mais longe* Os artigos L. 382-4 e R. 382-20 do Código Previdenciário.

### c. Se necessário, faça uma declaração de existência com a Agessa

O dono da galeria que distribui obras de arte pertencentes ao ramo de escritores ou fotógrafos, em particular, deve fazer a declaração à Agessa.

**Autoridade competente**

A declaração deve ser dirigida à Agessa.

**Tempo**

A Agessa reconhece o recebimento da declaração de existência e emite um recibo com um número de referência.

**Documentos de suporte**

A declaração de existência é feita preenchendo o [Formulário de Declaração de Existência](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d'existence.pdf).

**Custo**

Livre.

*Para ir mais longe* Os artigos L. 382-4 e R. 382-20 do Código Previdenciário.

### d. Se necessário, faça uma declaração como titular de metais preciosos

Os indivíduos que possuem materiais de ouro, prata ou platina, trabalhados ou não trabalhados, como parte de sua atividade profissional devem se manifestar às autoridades fiscais mediante a apresentação de uma declaração de existência.

**Autoridade competente**

A declaração de existência é realizada com o gabinete de garantia territorialmente competente, vinculado à diretoria regional de aduaneiros e indiretos. É possível consultar sobre o [site oficial da Alfândega](%5b*http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees*%5d(http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees)), a lista de escritórios de garantia próximos ao seu departamento de negócios.

**Documentos de suporte**

Deve conter:

- a declaração de existência em papel livre datado e assinado, afirmando:- O nome do profissional ou da empresa,
  + A designação exata da atividade,
  + O endereço da sede, bem como o local de prática;
- Fotocópia da carteira de identidade nacional, passaporte, certidão de nascimento, caderneta de família ou carteira de motorista;
- os endereços da sede e o local de atuação devem ser justificados pela produção de uma cópia de um arrendamento comercial ou profissional, a fim de permitir os controles da administração;
- um certificado de registro no Registro de Comércios e Empresas (RCS) ou no diretório de comércios ou uma cópia do extrato Kbis da empresa mencionando a atividade de venda ou compra de obras de metais preciosos.

**Custo**

Livre.

*Para ir mais longe* Seções 533 e 534 do Código Tributário Geral e artigo 211 A do Calendário III do Código Tributário Geral.

