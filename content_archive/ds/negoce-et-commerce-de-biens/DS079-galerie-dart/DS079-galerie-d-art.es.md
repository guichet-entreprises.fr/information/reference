﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS079" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="es" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Comercio de mercancías" -->
<!-- var(title)="Galería de Arte" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="comercio-de-mercancias" -->
<!-- var(title-short)="galeria-de-arte" -->
<!-- var(url)="https://www.guichet-entreprises.fr/es/ds/comercio-de-mercancias/galeria-de-arte.html" -->
<!-- var(last-update)="2020-04-15 17:24:01" -->
<!-- var(url-name)="galeria-de-arte" -->
<!-- var(translation)="Auto" -->


Galería de Arte
===============

Actualización más reciente: : <!-- begin-var(last-update) -->2020-04-15 17:24:01<!-- end-var -->



<!-- begin-include(disclaimer-trans-es) -->

**Aviso sobre la calidad de la traducción automática**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Esta página ha sido traducida utilizando una herramienta de traducción automática y puede contener errores. Se aconseja a los usuarios para comprobar la exactitud de la información proporcionada en esta página antes de emprender cualquier acción.

El Servicio de Empresa del Banco no se hace responsable de la operación de la información que va a ser inexacta debido a una traducción no automática fiel al original.<!-- alert-end:warning -->

<!-- end-include -->
1. Definición de la actividad
-----------------------------

### a. Definición

Un lugar abierto al público donde un galerista expone y vende obras de arte. Se le paga al recibir una comisión por las ventas que realiza a nivel nacional o internacional.

**Qué saber**

La actividad del propietario de la galería es distinta de la de algunos artistas que venden directamente desde su estudio.

### b. Centro competente de formalidad empresarial

El centro de formalidades empresariales (CFE) correspondiente depende de la naturaleza de la estructura en la que se lleve a cabo la actividad. En el caso de una galería de arte, se trata de una actividad comercial. Por lo tanto, la CFE pertinente es la Cámara de Comercio e Industria (CCI).

**Es bueno saber**

Un comerciante es una persona que realiza actos de comercio como de costumbre. Se consideran actos de comercio: la compra de bienes para la reventa, el alquiler de muebles, operaciones de fabricación o transporte, las actividades de intermediarios (incluyendo corretaje y agencia), etc.

Dos grados. Condiciones de instalación
--------------------------------------

### a. Cualificaciones profesionales

No se requiere ningún diploma especial o capacitación para ejercer como galerista. Sin embargo, lo mejor es adquirir un buen dominio del mercado del arte y una sólida cultura artística. Por lo tanto, es aconsejable graduarse de una escuela de arte o universidad.

el [Sitio web de la Comisión Nacional de Certificación Profesional](http://www.rncp.cncp.gouv.fr/) (CNCP) ofrece una lista de todas estas cualificaciones profesionales.

### b. Cualificaciones profesionales - Nacionales Europeos (LPS o LE)

#### En caso de prestación gratuita de servicios (LPS)

No existe especificidad para los nacionales de uno de los Estados miembros de la Unión Europea (UE) o del Espacio Económico Europeo (EEE) que deseen establecerse en Francia con carácter ad hoc y temporal.

#### En caso de establecimiento gratuito (LE)

No existe especificidad para los nacionales de ninguno de los Estados miembros de la UE o del EEE que quieran establecerse en Francia de forma permanente.

### c. Algunas peculiaridades de la regulación de la actividad

#### Cumplir con la normativa general aplicable a todas las instituciones públicas (ERP)

Si las instalaciones están abiertas al público, el profesional debe cumplir con las normas para las instituciones que reciben el público:

- Fuego
- Accesibilidad.

Para más información, es aconsejable consultar el listado " [Establecimiento que recibe el público](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

#### Hacer una declaración con la Casa de los Artistas o agessa

El propietario de la galería es considerado una emisora comercial de obras de artistas-autores. Como tal, debe informar anualmente de su volumen de negocios o comisiones. Sobre la base de esta declaración se calcula el importe de la contribución a la seguridad social y a la formación profesional. Estas declaraciones se pueden hacer en línea en el [sitio web oficial de la seguridad social de los autores de artistas](http://www.secu-artistes-auteurs.fr/).

Si la declaración se hace con la Casa de los Artistas, toma la forma de una declaración anual que se hace antes del 1 de mayo.

Por otro lado, si la declaración se hace con la asociación para la gestión de la seguridad social de los autores (Agessa), toma la forma de una declaración trimestral.

Para más información, es aconsejable consultar el [sitio web oficial de la seguridad social de los artistas-autores](http://www.secu-artistes-auteurs.fr/).

*Para ir más allá* Artículos R. 382-17 y los siguientes del Código de la Seguridad Social.

#### Requisito para mantener un registro policial en caso de que la obra no provenga directamente del estudio del artista

Si el propietario de la galería vende obras que no provienen directamente del estudio del artista, debe llevar un registro conocido como "registro policial". Este registro debe conservarse diariamente y contener una descripción de los objetos adquiridos o mantenidos para la venta. Además, este registro debe permitir la identificación de estas obras, así como la de las personas que las vendieron.

En la Lista 1 de la orden del 21 de julio de 1992 se propone un modelo de registro.

Un profesional que eluda esta obligación se enfrenta a una pena de seis meses de prisión y una multa de 30.000 euros.

*Para ir más allá* Artículos 321-7, R.321-3 a R.321-7 del Código Penal y Apéndice 1 de la Orden de 21 de julio de 1992.

#### Requisito para mantener un registro policial de metales preciosos trabajados o no trabajados

El propietario de la galería debe llevar un registro "policial" de sus compras, ventas, recibos y entregas de metales preciosos u obras que contengan estos materiales (oro, plata, platino o cualquier aleación de estos metales), estén o no hechos. El propietario de la galería debe mantener este registro a disposición de las autoridades públicas, en cualquier solicitud.

El objetivo de este registro es garantizar la trazabilidad de las obras de metales preciosos y limitar el riesgo de recepción.

*Para ir más allá* Artículos 537 a 539 del Código General Tributario y circular de 22 de julio de 2010 sobre la garantía de los metales preciosos y las condiciones del mantenimiento de registros (el denominado "libro policial").

#### Obligación de compra a personas conocidas o con encuestados que conozcan

Los comerciantes de oro, plata, platino o contenedores solo pueden comprar estos productos a personas conocidas o con los encuestados que conozcan.

*Para ir más allá* Artículo 539 del Código Fiscal General.

#### Requisito de visualización de precios

Una etiqueta discreta en las obras de arte originales (y objetos de la antiguedad) en exhibición está permitido en las galerías de arte. Además, la capacidad del cliente para ver una lista de precios es suficiente para cumplir con la obligación del galerista de informar al consumidor sobre el precio de los productos expuestos.

*Para ir más allá* : circular del 19 de julio de 1988 sobre la exhibición de los precios de los libros originales y usados.

Tres grados. Procedimientos y trámites de instalación
-----------------------------------------------------

### a. Formalidades de notificación de la empresa

La empresa debe estar inscrita en el Registro Mercantil y Mercantil (RCS). Para obtener más información, se recomienda ver las "Formalidades de Informes de Empresas Comerciales" y "Registro de una Empresa Comercial en el RCS."

### b. Si es necesario, hacer una declaración de existencia con la Casa de los Artistas

El galerista que distribuya obras de arte bajo la clasificación de las artes gráficas y plásticas debe hacer la declaración a la Casa de los Artistas.

Las artes gráficas y plásticas incluyen pinturas, dibujos, grabados, grabados, litografías, esculturas, tapices y tejidos de pared hechos a mano, ejemplos únicos de cerámica, etc. Para más detalles, es aconsejable consultar el [sitio web oficial de la Casa de los Artistas](%5b*http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/*%5d(http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/)).

Esta declaración identifica al declarante como un organismo de radiodifusión comercial de obras de artistas-autores, contempladas, como tales, a la contribución correspondiente.

**Autoridad competente**

La declaración debe dirigirse a la Casa de los Artistas.

**hora**

La declaración debe tener lugar dentro de los ocho días siguientes al inicio de la actividad del solicitante de registro. La Casa de los Artistas reconoce haber recibido la declaración de actividad y emite un identificador llamado número de pedido.

**Documentos de apoyo**

La declaración debe incluir:

- el [Formulario de Declaración de Existencia](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d%E2%80%99existence.pdf) completado, fechado y firmado;
- una copia del Certificado de Registro para el Directorio de Empresas y Establecimientos (Sirene).

**Costo**

Gratis.

*Para ir más allá* Artículos L. 382-4 y R. 382-20 del Código de la Seguridad Social.

### c. Si es necesario, haga una declaración de existencia con el Agessa

El galerista que distribuya obras de arte pertenecientes a la rama de escritores o fotógrafos en particular deberá hacer la declaración a la Agessa.

**Autoridad competente**

La declaración debe dirigirse a la Agessa.

**hora**

La Agessa confirma la recepción de la declaración de existencia y emite un recibo con un número de referencia.

**Documentos de apoyo**

La declaración de existencia se hace rellenando la [Formulario de Declaración de Existencia](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d'existence.pdf).

**Costo**

Gratis.

*Para ir más allá* Artículos L. 382-4 y R. 382-20 del Código de la Seguridad Social.

### d. Si es necesario, haga una declaración como titular de metales preciosos

Las personas que poseen materiales de oro, plata o platino, ya sean trabajados o no trabajados, como parte de su actividad profesional deben darse a conocer a las autoridades fiscales mediante la presentación de una declaración de existencia.

**Autoridad competente**

La declaración de existencia se lleva a cabo con la oficina de garantía territorialmente competente, adscrita a la Dirección Regional de Aduanas y Derechos Indirectos. Es posible consultar [sitio web oficial de Aduanas](%5b*http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees*%5d(http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees)), la lista de oficinas de garantía cercanas a su departamento de negocios.

**Documentos de apoyo**

Debe contener:

- la declaración de existencia en papel libre fechada y firmada, indicando:- El nombre del profesional o de la empresa,
  + La designación exacta de la actividad,
  + La dirección de la sede central, así como el lugar de práctica;
- Fotocopia del documento nacional de identidad, pasaporte, certificado de nacimiento, folleto familiar o licencia de conducir;
- las direcciones de la sede central y del lugar de práctica deben justificarse por la producción de una copia de un arrendamiento comercial o profesional a fin de permitir los controles de la administración;
- un certificado de registro en el Registro de Comercios y Empresas (RCS) o en el directorio de operaciones o una copia del extracto Kbis de la empresa mencionando la actividad de venta o compra de obras de metales preciosos.

**Costo**

Gratis.

*Para ir más allá* Artículos 533 y 534 del Código Tributario General y artículo 211 A de la Lista III del Código Tributario General.

