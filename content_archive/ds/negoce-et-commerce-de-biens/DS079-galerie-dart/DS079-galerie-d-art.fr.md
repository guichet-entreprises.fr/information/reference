﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS079" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Galerie d'art" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="galerie-d-art" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/galerie-d-art.html" -->
<!-- var(last-update)="2020-04-15 17:24:01" -->
<!-- var(url-name)="galerie-d-art" -->
<!-- var(translation)="None" -->


# Galerie d'art

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:24:01<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Lieu ouvert au public dans lequel un galeriste expose et vend des œuvres d'art. Il est rémunéré en percevant une commission sur les ventes qu’il réalise à l’échelle nationale ou internationale.

**À savoir**

L’activité de galeriste est distincte de celle de certains artistes qui vendent directement depuis leur atelier.

### b. Centre de formalités des entreprises compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de la structure dans laquelle l’activité est exercée. Dans le cas d’une galerie d’art, il s’agit d’une activité commerciale. Le CFE compétent est donc la chambre de commerce et d’industrie (CCI).

**Bon à savoir**

Un commerçant est une personne qui accomplit des actes de commerce à titre habituel. Sont considérés comme des actes de commerce : l’achat de biens en vue de leur revente, la location de meubles, les opérations de manufacture ou de transport, les activités d’intermédiaires de commerce (courtage et agence notamment), etc.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucun diplôme ou formation particulière n’est exigé pour exercer en qualité de galeriste. Cependant, il est préférable d’acquérir une bonne maîtrise du marché de l'art et une culture artistique solide. Il est donc conseillé d’être diplômé d’une école d’art ou d’un établissement universitaire.

Le [site de la Commission nationale de la certification professionnelle](http://www.rncp.cncp.gouv.fr/) (CNCP) propose une liste de l’ensemble de ces qualifications professionnelles.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### En cas de libre prestation de services (LPS)

Il n’existe aucune spécificité pour les ressortissants d’un des États membres de l'Union européenne (UE) ou de l'Espace économique européen (EEE) voulant s’établir en France de façon ponctuelle et temporaire.

#### En cas de libre établissement (LE)

Il n’existe aucune spécificité pour les ressortissants d’un des États membres de l'UE ou de l’EEE voulant s’établir en France de façon permanente.

### c. Quelques particularités de la réglementation de l’activité

#### Respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public :

* en matière d’incendie ;
* en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

#### Effectuer une déclaration auprès de la Maison des artistes ou de l’Agessa

Le galeriste est considéré comme un diffuseur commercial d’œuvres d’artistes-auteurs. À ce titre, il doit effectuer chaque année une déclaration de son chiffre d’affaires ou de ses commissions. C’est sur la base de cette déclaration qu’est calculé le montant de la contribution à la sécurité sociale et à la formation professionnelle dont il doit s’acquitter. Ces déclarations peuvent être réalisées en ligne sur le [site officiel de la sécurité sociale des artistes auteurs](http://www.secu-artistes-auteurs.fr/).

Si la déclaration s’effectue auprès de la Maison des artistes, elle prend la forme d’une déclaration annuelle devant intervenir avant le 1er mai.

En revanche, si la déclaration s’effectue auprès de l’association pour la gestion de la sécurité sociale des auteurs (Agessa), elle prend la forme d’une déclaration trimestrielle.

Pour plus d’informations, il est conseillé de se reporter au [site officiel de la sécurité sociale des artistes-auteurs](http://www.secu-artistes-auteurs.fr/).

*Pour aller plus loin* : articles R. 382-17 et suivants du Code de la sécurité sociale.

#### Obligation de tenir un registre de police dans le cas où l’œuvre ne provient pas directement de l’atelier de l’artiste

Si le galeriste vend des œuvres qui ne proviennent pas directement de l’atelier de l’artiste, il doit tenir un registre dit « registre de police ». Ce registre doit être tenu quotidiennement et contenir une description des objets acquis ou détenus en vue de la vente. De plus, ce registre doit permettre l’identification de ces œuvres ainsi que celle des personnes qui les ont vendues.

Un modèle de registre est proposé à l’annexe 1 de l’arrêté du 21 juillet 1992.

Le professionnel qui se soustrait à cette obligation s’expose à une peine de six mois d’emprisonnement et de 30 000 euros d’amende.

*Pour aller plus loin* : articles 321-7, R.321-3 à R.321-7 du Code pénal et annexe 1 de l’arrêté du 21 juillet 1992.

#### Obligation de tenir un registre de police pour les métaux précieux ouvrés ou non ouvrés

Le galeriste doit tenir un registre « de police » retraçant ses achats, ventes, réception et livraisons de métaux précieux ou d’ouvrages contenant ces matières (or, argent, platine ou tout alliage de ces métaux), ouvrés ou non. Le galeriste doit tenir à disposition des autorités publiques ce registre, sur toute réquisition.

Ce registre vise à assurer la traçabilité des ouvrages en métaux précieux et limiter les risques de recel.

*Pour aller plus loin* : articles 537 à 539 du Code général des impôts et circulaire du 22 juillet 2010 sur la garantie des métaux précieux et les modalités de tenue du registre (dit « livre de police »).

#### Obligation d’achat auprès de personnes connues ou ayant des répondants connus d’eux

Les marchands d’or, d’argent, de platine ou d’ouvrages en contenant ne peuvent acheter ces biens qu’auprès de personnes connues ou ayant des répondants connus d’eux.

*Pour aller plus loin* : article 539 du Code général des impôts.

#### Obligation d’affichage des prix

L’apposition d’une étiquette discrète sur les œuvres d’art originales (et les objets ayant valeur d’antiquité) exposées à la vue du public est autorisée dans les galeries d’art. De plus, la possibilité offerte au client de consulter une liste des prix suffit à remplir l’obligation pour le galeriste d’informer le consommateur sur le prix des biens exposés.

*Pour aller plus loin* : circulaire du 19 juillet 1988 sur l’affichage des prix des œuvres d’art originales et des livres d’occasion.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

L’entreprise doit être déclarée au registre du commerce et des sociétés (RCS). Pour plus d’informations, il est recommandé de consulter les fiches « Formalités de déclaration d’une société commerciale » et « Immatriculation d’une entreprise commerciale au RCS ».

### b. Le cas échéant, effectuer une déclaration d'existence auprès de la Maison des artistes

Le galeriste diffusant des œuvres d’art relevant de la qualification des arts graphiques et plastiques doit en faire la déclaration auprès de la Maison des artistes.

Les arts graphiques et plastiques regroupent notamment les peintures, dessins, gravures, estampes, lithographies, sculptures, tapisseries et textiles muraux faits à la main, exemplaires uniques de céramiques, etc. Pour plus de précisions, il est conseillé de se reporter au [site officiel de la Maison des artistes](%5b*http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/*%5d(http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/)).

Cette déclaration permet d’identifier le déclarant comme diffuseur commercial d’œuvres d’artistes-auteurs, redevable, à ce titre, de la contribution correspondante.

**Autorité compétente**

La déclaration doit être adressée à la Maison des artistes.

**Délai**

La déclaration doit intervenir dans les huit jours suivant le début de l’activité du déclarant. La Maison des artistes accuse réception de la déclaration d’activité et délivre un identifiant appelé numéro d’ordre.

**Pièces justificatives**

La déclaration doit contenir :

* le [formulaire de déclaration d’existence](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d%E2%80%99existence.pdf) rempli, daté et signé ;
* une copie du certificat d’inscription au répertoire des entreprises et des établissements (Sirene).

**Coût**

Gratuit.

*Pour aller plus loin* : articles L. 382-4 et R. 382-20 du Code de la sécurité sociale.

### c. Le cas échéant, effectuer une déclaration d'existence auprès de l'Agessa

Le galeriste diffusant des œuvres d’art relevant de la branche des écrivains ou des photographes notamment doit en faire la déclaration auprès de l’Agessa.

**Autorité compétente**

La déclaration doit être adressée à l’Agessa.

**Délai**

L’Agessa accuse réception de la déclaration d’existence et délivre un récépissé avec un numéro de référence.

**Pièces justificatives**

La déclaration d’existence se fait en remplissant en ligne le [formulaire de déclaration d’existence](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d'existence.pdf).

**Coût**

Gratuit.

*Pour aller plus loin* : articles L. 382-4 et R. 382-20 du Code de la sécurité sociale.

### d. Le cas échéant, effectuer une déclaration de détenteur de métaux précieux

Les personnes qui détiennent des matières d’or, d’argent ou de platine, ouvrées ou non, dans le cadre de leur activité professionnelle doivent se faire connaître de l’administration fiscale en remplissant une déclaration d’existence.

**Autorité compétente**

La déclaration d’existence s’effectue auprès du bureau de garantie territorialement compétent, rattaché à la direction régionale des douanes et droits indirects. Il est possible de consulter sur le [site officiel des douanes](%5b*http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees*%5d(http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees)), la liste des bureaux de garantie proches du département de votre activité.

**Pièces justificatives**

Elle doit contenir :

* la déclaration d’existence sur papier libre datée et signée, indiquant :
  * le nom du professionnel ou de la société,
  * la désignation exacte de l'activité,
  * l'adresse du siège social ainsi que du lieu d'exercice ;
* la photocopie de la carte nationale d'identité, du passeport, d'un acte de naissance, du livret de famille ou du permis de conduire ; 
* les adresses du siège social et du lieu d'exercice devront être justifiées par la production de la copie d'un bail commercial ou professionnel afin de permettre les contrôles de l'administration ;
* une attestation d’immatriculation au registre du commerce et des sociétés (RCS) ou au répertoire des métiers ou une copie de l’extrait Kbis de la société mentionnant l’activité de ventes ou d’achat d’ouvrages en métaux précieux.

**Coût**

Gratuit.

*Pour aller plus loin* : articles 533 et 534 du Code général des impôts et l’article 211 A de l'annexe III du Code général des impôts.