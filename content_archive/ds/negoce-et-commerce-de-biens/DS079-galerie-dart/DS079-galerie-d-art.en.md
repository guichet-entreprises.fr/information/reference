﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS079" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Art Gallery" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="trade" -->
<!-- var(title-short)="art-gallery" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/trade/art-gallery.html" -->
<!-- var(last-update)="2020-04-15 17:24:01" -->
<!-- var(url-name)="art-gallery" -->
<!-- var(translation)="Auto" -->


Art Gallery
===========

Latest update: : <!-- begin-var(last-update) -->2020-04-15 17:24:01<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice about the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page has been translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page before undertaking any action.

The Enterprise Bank Service can not be held responsible for the operation of information that will be inaccurate due to a non-automatic translation faithful to the original.<!-- alert-end:warning -->

<!-- end-include -->
1. Defining the activity
------------------------

### a. Definition

A place open to the public where a gallery owner exhibits and sells works of art. He is paid by receiving a commission on sales he makes nationally or internationally.

**What to know**

The activity of gallery owner is distinct from that of some artists who sell directly from their studio.

### b. Competent Business Formality Centre

The relevant business formalities centre (CFE) depends on the nature of the structure in which the activity is carried out. In the case of an art gallery, it is a commercial activity. The relevant CFE is therefore the Chamber of Commerce and Industry (CCI).

**Good to know**

A trader is a person who performs acts of commerce as usual. These are considered acts of commerce: the purchase of goods for resale, the rental of furniture, manufacturing or transport operations, the activities of intermediaries (including brokerage and agency), etc.

Two degrees. Installation conditions
------------------------------------

### a. Professional qualifications

No special diploma or training is required to practice as a gallery owner. However, it is best to acquire a good mastery of the art market and a solid artistic culture. It is therefore advisable to graduate from an art school or university.

The [National Professional Certification Commission website](http://www.rncp.cncp.gouv.fr/) (CNCP) offers a list of all of these professional qualifications.

### b. Professional Qualifications - European Nationals (LPS or LE)

#### In case of free provision of services (LPS)

There is no specificity for nationals of one of the Member States of the European Union (EU) or the European Economic Area (EEA) who wish to settle in France on an ad hoc and temporary basis.

#### In case of free establishment (LE)

There is no specificity for nationals of any of the EU or EEA Member States who want to settle in France permanently.

### c. Some peculiarities of the regulation of the activity

#### Comply with general regulations applicable to all public institutions (ERP)

If the premises are open to the public, the professional must comply with the rules for institutions receiving the public:

- Fire
- accessibility.

For more information, it is advisable to refer to the listing " [Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

#### Make a declaration with the House of Artists or agessa

The gallery owner is considered a commercial broadcaster of works by artist-authors. As such, it must report its turnover or commissions annually. It is on the basis of this declaration that the amount of the contribution to social security and vocational training is calculated. These statements can be made online on the [official social security website of artists authors](http://www.secu-artistes-auteurs.fr/).

If the declaration is made with the House of Artists, it takes the form of an annual declaration to be made before May 1.

On the other hand, if the declaration is made with the association for the management of the social security of the authors (Agessa), it takes the form of a quarterly declaration.

For more information, it is advisable to refer to the [official website of the social security of artists-authors](http://www.secu-artistes-auteurs.fr/).

*To go further* Articles R. 382-17 and the following from the Social Security Code.

#### Requirement to keep a police register in case the work does not come directly from the artist's studio

If the gallery owner sells works that do not come directly from the artist's studio, he must keep a register known as a "police register." This register must be kept daily and contain a description of the objects acquired or held for sale. In addition, this register must allow the identification of these works as well as that of the people who sold them.

A registry model is proposed in Schedule 1 of the July 21, 1992 order.

A professional who evades this obligation faces a six-month prison sentence and a fine of 30,000 euros.

*To go further* Articles 321-7, R.321-3 to R.321-7 of the Penal Code and Appendix 1 of the Order of 21 July 1992.

#### Requirement to maintain a police register for worked or unworked precious metals

The gallery owner must keep a "police" record of his purchases, sales, receipts and deliveries of precious metals or works containing these materials (gold, silver, platinum or any alloy of these metals), whether or not they are made. The gallery owner must keep this register at the disposal of the public authorities, on any requisition.

The purpose of this register is to ensure the traceability of precious metal works and to limit the risk of receiving.

*To go further* Articles 537 to 539 of the General Tax Code and circular of 22 July 2010 on the guarantee of precious metals and the terms of record keeping (so-called "police book").

#### Obligation to purchase from persons known to or with respondents known to them

Gold, silver, platinum or container merchants may only purchase these goods from persons known to or with respondents known to them.

*To go further* Section 539 of the General Tax Code.

#### Price display requirement

A discreet label on original works of art (and objects of antiquity) on display is permitted in art galleries. In addition, the customer's ability to view a price list is sufficient to fulfill the gallerist's obligation to inform the consumer about the price of the goods on display.

*To go further* : circular of July 19, 1988 on the display of the prices of original and used books.

Three degrees. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The company must be registered in the Register of Trade and Companies (RCS). For more information, it is recommended to see the "Commercial Company Reporting Formalities" and "Registration of a Commercial Enterprise at the RCS."

### b. If necessary, make a declaration of existence with the House of Artists

The gallery owner distributing works of art under the classification of graphic and plastic arts must make the declaration to the House of Artists.

Graphic and plastic arts include paintings, drawings, engravings, prints, lithographs, sculptures, tapestries and handmade wall textiles, unique examples of ceramics, etc. For more details, it is advisable to refer to the [official website of the House of Artists](%5b*http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/*%5d(http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/)).

This statement identifies the declarant as a commercial broadcaster of works by artist-authors, beholden, as such, to the corresponding contribution.

**Competent authority**

The statement must be addressed to the House of Artists.

**Time**

The declaration must take place within eight days of the start of the registrant's activity. The Artists' House acknowledges receipt of the declaration of activity and issues an identifier called the order number.

**Supporting documents**

The statement should include:

- The [Declaration of Existence form](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d%E2%80%99existence.pdf) completed, dated and signed;
- a copy of the Certificate of Registration for the Directory of Companies and Establishments (Sirene).

**Cost**

Free.

*To go further* Articles L. 382-4 and R. 382-20 of the Social Security Code.

### c. If necessary, make a declaration of existence with the Agessa

The gallery owner who distributes works of art belonging to the branch of writers or photographers in particular must make the declaration to the Agessa.

**Competent authority**

The statement must be addressed to the Agessa.

**Time**

The Agessa acknowledges receipt of the declaration of existence and issues a receipt with a reference number.

**Supporting documents**

The declaration of existence is made by filling out the [Declaration of Existence form](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d'existence.pdf).

**Cost**

Free.

*To go further* Articles L. 382-4 and R. 382-20 of the Social Security Code.

### d. If necessary, make a declaration as a precious metals holder

Individuals who hold gold, silver or platinum materials, whether worked or unworked, as part of their professional activity must make themselves known to the tax authorities by filing a declaration of existence.

**Competent authority**

The declaration of existence is carried out with the territorially competent guarantee office, attached to the regional directorate of customs and indirect duties. It is possible to consult on the [official Customs website](%5b*http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees*%5d(http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees)), the list of warranty offices close to your business department.

**Supporting documents**

It must contain:

- the declaration of existence on free paper dated and signed, stating:- The name of the professional or the company,
  + The exact designation of the activity,
  + The address of the head office as well as the place of practice;
- Photocopy of national identity card, passport, birth certificate, family booklet or driver's license;
- the addresses of the head office and the place of practice must be justified by the production of a copy of a commercial or professional lease in order to allow the controls of the administration;
- a certificate of registration in the Register of Trades and Companies (RCS) or in the directory of trades or a copy of the company's Kbis extract mentioning the activity of selling or purchasing precious metal works.

**Cost**

Free.

*To go further* Sections 533 and 534 of the General Tax Code and Article 211 A of Schedule III of the General Tax Code.

