﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS079" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="de" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Handel und Handel mit Waren" -->
<!-- var(title)="Kunstgalerie" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="handel-und-handel-mit-waren" -->
<!-- var(title-short)="kunstgalerie" -->
<!-- var(url)="https://www.guichet-entreprises.fr/de/ds/handel-und-handel-mit-waren/kunstgalerie.html" -->
<!-- var(last-update)="2020-04-15 17:24:01" -->
<!-- var(url-name)="kunstgalerie" -->
<!-- var(translation)="Auto" -->


Kunstgalerie
============

Neueste Aktualisierung: : <!-- begin-var(last-update) -->2020-04-15 17:24:01<!-- end-var -->



<!-- begin-include(disclaimer-trans-de) -->

**Hinweis über die Qualität der maschinellen Übersetzung**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Diese Seite wurde bisher mit Hilfe eines maschinellen Übersetzungstool übersetzt und Fehler enthalten kann. Benutzern wird empfohlen, die Genauigkeit der Informationen auf dieser Seite zur Verfügung gestellt zu überprüfen, bevor sie Maßnahmen unternehmen.

Der Enterprise Bank-Dienst kann nicht verantwortlich für den Betrieb der Informationen gemacht werden, die ungenau aufgrund einer nicht-automatischen Übersetzung des Original treu sein werden.<!-- alert-end:warning -->

<!-- end-include -->
1. Definieren der Aktivität
---------------------------

### a. Definition

Ein öffentlich zugänglicher Ort, an dem ein Galerist Kunstwerke ausstellt und verkauft. Er wird bezahlt, indem er eine Provision für Verkäufe erhält, die er national oder international tätigt.

**Was Sie wissen sollten**

Die Tätigkeit des Galeristen unterscheidet sich von der einiger Künstler, die direkt aus ihrem Atelier verkaufen.

### b. Zuständiges Business Formality Centre

Das relevante Zentrum für Geschäftsformalitäten (CFE) hängt von der Art der Struktur ab, in der die Tätigkeit ausgeübt wird. Im Falle einer Kunstgalerie handelt es sich um eine kommerzielle Tätigkeit. Das relevante CFE ist daher die Industrie- und Handelskammer (IHK).

**Gut zu wissen**

Ein Händler ist eine Person, die Wie üblich Handelshandlungen durchführt. Diese sind Handelshandlungen: der Kauf von Waren zum Weiterverkauf, die Vermietung von Möbeln, Herstellungs- oder Transportvorgänge, die Tätigkeiten von Vermittlern (einschließlich Makler und Agentur) usw.

Zwei Grad. Installationsbedingungen
-----------------------------------

### a. Berufliche Qualifikationen

Für die Ausübung als Galerist ist kein spezielles Diplom oder eine Ausbildung erforderlich. Am besten ist es jedoch, eine gute Beherrschung des Kunstmarktes und eine solide Kunstkultur zu erwerben. Es ist daher ratsam, eine Kunstschule oder Universität zu absolvieren.

das [Website der National Professional Certification Commission](http://www.rncp.cncp.gouv.fr/) (CNCP) bietet eine Liste aller dieser beruflichen Qualifikationen.

### b. Berufsqualifikationen - Europäische Staatsangehörige (LPS oder LE)

#### Bei freier Erbringung von Dienstleistungen (LPS)

Es gibt keine Besonderheit für Staatsangehörige eines der Mitgliedstaaten der Europäischen Union (EU) oder des Europäischen Wirtschaftsraums (EWR), die sich ad hoc und vorübergehend in Frankreich niederlassen möchten.

#### Bei freier Niederlassung (LE)

Es gibt keine Besonderheit für Staatsangehörige eines EU- oder EWR-Mitgliedstaates, die sich dauerhaft in Frankreich niederlassen wollen.

### c. Einige Besonderheiten der Regelung der Tätigkeit

#### Einhaltung der allgemeinen Vorschriften für alle öffentlichen Einrichtungen (ERP)

Wenn die Räumlichkeiten der Öffentlichkeit zugänglich sind, muss der Fachmann die Regeln für Einrichtungen, die die Öffentlichkeit empfangen, einhalten:

- Feuer
- Zugänglichkeit.

Für weitere Informationen ist es ratsam, auf die Auflistung " [Einrichtung, die die Öffentlichkeit empfängt](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

#### Erklärung mit dem Haus der Künstler oder agessa

Der Galerist gilt als kommerzieller Sender von Werken von Künstler-Autoren. Daher muss sie ihren Umsatz oder ihre Provisionen jährlich melden. Auf der Grundlage dieser Erklärung wird die Höhe des Beitrags zur sozialen Sicherheit und zur beruflichen Bildung berechnet. Diese Aussagen können online auf der [offizielle Website der sozialen Sicherheit von Künstlerautoren](http://www.secu-artistes-auteurs.fr/).

Wenn die Erklärung mit dem Haus der Künstler abgegeben wird, erfolgt sie in Form einer jährlichen Erklärung, die vor dem 1. Mai abgegeben wird.

Wird die Erklärung hingegen mit dem Verein für die Verwaltung der sozialversicherung der Autoren (Agessa) abgegeben, so erfolgt sie in Form einer vierteljährlichen Erklärung.

Weitere Informationen finden Sie im [offizielle Website der Sozialversicherung von Künstler-Autoren](http://www.secu-artistes-auteurs.fr/).

*Um weiter zu gehen* Artikel R. 382-17 und die folgenden aus dem Sozialgesetzbuch.

#### Anforderung zur Eintragung eines Polizeiregisters für den Fall, dass das Werk nicht direkt aus dem Atelier des Künstlers kommt

Wenn der Galerist Werke verkauft, die nicht direkt aus dem Atelier des Künstlers stammen, muss er ein Register führen, das als "Polizeiregister" bekannt ist. Dieses Register muss täglich geführt werden und eine Beschreibung der erworbenen oder zum Verkauf gehaltenen Gegenstände enthalten. Darüber hinaus muss dieses Register die Identifizierung dieser Werke sowie der Personen, die sie verkauft haben, ermöglichen.

Ein Registrierungsmodell wird in Anhang 1 der Bestellung vom 21. Juli 1992 vorgeschlagen.

Einem Profi, der sich dieser Verpflichtung entzieht, drohen eine sechsmonatige Haftstrafe und eine Geldstrafe von 30.000 Euro.

*Um weiter zu gehen* Artikel 321-7, R.321-3 bis R.321-7 des Strafgesetzbuches und Anlage 1 des Beschlusses vom 21. Juli 1992.

#### Anforderung zur Eintragung eines Polizeiregisters für bearbeitete oder nicht verarbeitete Edelmetalle

Der Galerist muss eine "polizeiliche" Aufzeichnung seiner Käufe, Verkäufe, Quittungen und Lieferungen von Edelmetallen oder Werken, die diese Materialien enthalten (Gold, Silber, Platin oder eine Legierung dieser Metalle), unabhängig davon, ob sie hergestellt werden oder nicht, führen. Der Galerist muss dieses Register bei jeder Anforderung der öffentlichen Hand zur Verfügung stellen.

Ziel dieses Registers ist es, die Rückverfolgbarkeit von Edelmetallwerken zu gewährleisten und das Empfangsrisiko zu begrenzen.

*Um weiter zu gehen* Artikel 537 bis 539 des Allgemeinen Steuergesetzbuches und Rundschreiben vom 22. Juli 2010 über die Garantie von Edelmetallen und die Bedingungen der Buchführung (sog. "Polizeibuch").

#### Verpflichtung zum Kauf von Personen, die ihnen bekannt sind oder mit ihnen bekannten Beschwerdegegnern

Gold-, Silber-, Platin- oder Containerhändler dürfen diese Waren nur von Personen beziehen, die ihnen bekannt sind oder mit ihnen bekannten Befragten.

*Um weiter zu gehen* Abschnitt 539 des Allgemeinen Steuergesetzbuches.

#### Preisanzeigeanforderung

Ein dezentes Etikett auf Originalkunstwerken (und Objekten der Antike) ist in Kunstgalerien erlaubt. Darüber hinaus reicht die Möglichkeit des Kunden, eine Preisliste einzusehen, aus, um der Verpflichtung des Galeristen nachzukommen, den Verbraucher über den Preis der ausgestellten Ware zu informieren.

*Um weiter zu gehen* : Rundschreiben vom 19. Juli 1988 über die Anzeige der Preise für Original- und gebrauchte Bücher.

Drei Grad. Installationsverfahren und Formalitäten
--------------------------------------------------

### a. Meldeformalitäten des Unternehmens

Das Unternehmen muss im Handels- und Handelsregister (RCS) eingetragen sein. Weitere Informationen finden Sie in den "Commercial Company Reporting Formalities" und "Registration of a Commercial Enterprise at the RCS".

### b. falls erforderlich, eine Existenzerklärung mit dem Haus der Künstler

Der Galerist, der Kunstwerke unter der Klassifikation der graphischen und bildenden Kunst vertreibt, muss die Erklärung an das Haus der Künstler abgeben.

Grafische und bildende Kunst umfasst Gemälde, Zeichnungen, Gravuren, Drucke, Lithographien, Skulpturen, Wandteppiche und handgefertigte Wandtextilien, einzigartige Beispiele von Keramik, etc. Für weitere Informationen ist es ratsam, auf die [offizielle Website des Hauses der Künstler](%5b*http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/*%5d(http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/)).

Diese Aussage identifiziert den Anmelder als kommerziellen Sender von Werken von Künstlerautoren, die als solche dem entsprechenden Beitrag zusehen sind.

**Zuständige Behörde**

Die Erklärung muss an das Haus der Künstler gerichtet werden.

**Zeit**

Die Erklärung muss innerhalb von acht Tagen nach Beginn der Tätigkeit des Registranten erfolgen. Das Künstlerhaus bestätigt den Eingang der Tätigkeitserklärung und gibt eine Kennung namens Bestellnummer aus.

**Belege**

Die Erklärung sollte Folgendes enthalten:

- das [Formular "Existenzerklärung"](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d%E2%80%99existence.pdf) ausgefüllt, datiert und unterzeichnet;
- eine Kopie der Registrierungsbescheinigung für das Verzeichnis der Unternehmen und Betriebe (Sirene).

**Kosten**

kostenlos.

*Um weiter zu gehen* Artikel L. 382-4 und R. 382-20 des Sozialgesetzbuches.

### c. Falls erforderlich, eine Existenzerklärung mit dem Agessa

Der Galerist, der Kunstwerke vertreibt, die insbesondere zum Zweig von Schriftstellern oder Fotografen gehören, muss die Erklärung an die Agessa abgeben.

**Zuständige Behörde**

Die Erklärung muss an die Agessa gerichtet werden.

**Zeit**

Die Agessa bestätigt den Eingang der Existenzerklärung und stellt eine Quittung mit einer Referenznummer aus.

**Belege**

Die Existenzerklärung erfolgt durch Ausfüllen der [Formular "Existenzerklärung"](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d'existence.pdf).

**Kosten**

kostenlos.

*Um weiter zu gehen* Artikel L. 382-4 und R. 382-20 des Sozialgesetzbuches.

### d. Stellen Sie erforderlichenfalls eine Erklärung als Edelmetallhalter

Personen, die im Rahmen ihrer beruflichen Tätigkeit Gold-, Silber- oder Platinmaterialien im Besitz von Gold-, Silber- oder Platinmaterialien besitzen, müssen sich im Rahmen ihrer beruflichen Tätigkeit durch Einreichung einer Existenzerklärung bei den Steuerbehörden melden.

**Zuständige Behörde**

Die Niederlassungserklärung wird bei der gebietszuständigen Garantiestelle durchgeführt, die der regionalen Direktion für Zoll und indirekte Zölle beigefügt ist. Es ist möglich, sich über die [offizielle Zoll-Website](%5b*http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees*%5d(http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees)), die Liste der Garantiebüros in der Nähe Ihrer Geschäftsabteilung.

**Belege**

Sie muss Folgendes enthalten:

- die Existenzerklärung auf freiem Papier datiert und unterzeichnet, mit der Angabe:- Der Name des Fachmanns oder des Unternehmens,
  + Die genaue Bezeichnung der Tätigkeit,
  + Die Anschrift der Zentrale sowie der Ort der Praxis;
- Fotokopie des Personalausweises, Reisepasses, Geburtsurkunde, Familienheft oder Führerschein;
- die Anschriften des Hauptsitzes und des Praxisortes müssen durch die Vorlage einer Kopie eines gewerblichen oder gewerblichen Mietvertrags gerechtfertigt sein, um die Kontrolle der Verwaltung zu ermöglichen;
- eine Bescheinigung über die Eintragung in das Register der Gewerke und Unternehmen (RCS) oder im Handelsverzeichnis oder eine Kopie des Kbis-Extrakts des Unternehmens, in dem die Tätigkeit des Verkaufs oder kaufs von Edelmetallwerken erwähnt wird.

**Kosten**

kostenlos.

*Um weiter zu gehen* Abschnitte 533 und 534 des Allgemeinen Steuergesetzbuches und Artikel 211 A des Anhangs III des Allgemeinen Steuergesetzbuches.

