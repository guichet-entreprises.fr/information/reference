﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS079" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="it" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Commercio e commercio di merci" -->
<!-- var(title)="Galleria d'arte" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="commercio-e-commercio-di-merci" -->
<!-- var(title-short)="galleria-d-arte" -->
<!-- var(url)="https://www.guichet-entreprises.fr/it/ds/commercio-e-commercio-di-merci/galleria-d-arte.html" -->
<!-- var(last-update)="2020-04-15 17:24:01" -->
<!-- var(url-name)="galleria-d-arte" -->
<!-- var(translation)="Auto" -->


Galleria d'arte
===============

Ultimo aggiornamento: : <!-- begin-var(last-update) -->2020-04-15 17:24:01<!-- end-var -->



<!-- begin-include(disclaimer-trans-it) -->

**Avviso per la qualità della traduzione automatica**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Questa pagina è stata tradotta con un mezzo di traduzione automatica e possono contenere errori. Gli utenti sono invitati a verificare l'esattezza delle informazioni fornite in questa pagina prima di intraprendere qualsiasi azione.

Il Service Bank Enterprise non può essere ritenuta responsabile per il funzionamento di informazioni che saranno dovuti inesatto traduzione non automatico fedele all'originale.<!-- alert-end:warning -->

<!-- end-include -->
1. Definizione dell'attività
----------------------------

### a. Definizione

Un luogo aperto al pubblico dove un galalleno espone e vende opere d'arte. Egli è pagato ricevendo una commissione sulle vendite che fa a livello nazionale o internazionale.

**Cosa sapere**

L'attività del galalleno è distinta da quella di alcuni artisti che vendono direttamente dal loro studio.

### b. Centro competente per le formalità aziendali

Il centro di formalità imprenditoriale (CFE) competente dipende dalla natura della struttura in cui viene svolta l'attività. Nel caso di una galleria d'arte, è un'attività commerciale. Il CFE pertinente è quindi la Camera di Commercio e Industria (CCI).

**Buono a sapersi**

Un commerciante è una persona che esegue atti di commercio come al solito. Questi sono considerati atti di commercio: l'acquisto di beni per la rivendita, il noleggio di mobili, operazioni di produzione o di trasporto, le attività di intermediari (compresa l'intermediazione e l'agenzia), ecc.

Due gradi. Condizioni di installazione
--------------------------------------

### a. Qualifiche professionali

Non è richiesto alcun diploma speciale o formazione per esercitarsi come gallerista. Tuttavia, è meglio acquisire una buona padronanza del mercato dell'arte e una solida cultura artistica. Si consiglia quindi di laurearsi in una scuola d'arte o di un'università.

Le [Sito web della National Professional Certification Commission](http://www.rncp.cncp.gouv.fr/) (CNCP) offre un elenco di tutte queste qualifiche professionali.

### b. Qualifiche professionali - Cittadini europei (LPS o LE)

#### In caso di fornitura gratuita di servizi (LPS)

Non esiste una specificità per i cittadini di uno degli Stati membri dell'Unione europea (UE) o dello Spazio economico europeo (AEA) che desiderano stabilirsi in Francia su base ad hoc e temporanea.

#### In caso di libera costituzione (LE)

Non esiste una specificità per i cittadini di nessuno degli Stati membri dell'UE o del CEA che desiderano stabilirsi in Francia in modo permanente.

### c. Alcune peculiarità della regolamentazione dell'attività

#### Conformità alle normative generali applicabili a tutte le istituzioni pubbliche (ERP)

Se i locali sono aperti al pubblico, il professionista deve rispettare le regole per gli istituti che ricevono il pubblico:

- Fuoco
- Accessibilità.

Per ulteriori informazioni, si consiglia di fare riferimento all'elenco " [Istituzione che riceve il pubblico](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

#### Fare una dichiarazione con la Casa degli Artisti o agessa

Il proprietario della galleria è considerato un'emittente commerciale di opere di artisti-autori. In quanto tale, deve segnalare annualmente il fatturato o le commissioni. È sulla base di questa dichiarazione che viene calcolato l'importo del contributo alla sicurezza sociale e alla formazione professionale. Queste dichiarazioni possono essere fatte online sul [sito ufficiale di sicurezza sociale di autori di artisti](http://www.secu-artistes-auteurs.fr/).

Se la dichiarazione viene fatta con la Camera degli Artisti, essa assume la forma di una dichiarazione annuale da fare prima del 1o maggio.

D'altra parte, se la dichiarazione è fatta con l'associazione per la gestione della sicurezza sociale degli autori (Agessa), assume la forma di una dichiarazione trimestrale.

Per ulteriori informazioni, si consiglia di fare riferimento alla [sito ufficiale della sicurezza sociale di artisti-autori](http://www.secu-artistes-auteurs.fr/).

*Per andare oltre* Articoli R. 382-17 e quanto segue dal codice di previdenza sociale.

#### Obbligo di tenere un registro di polizia nel caso in cui l'opera non provenga direttamente dallo studio dell'artista

Se il proprietario della galleria vende opere che non provengono direttamente dallo studio dell'artista, deve tenere un registro noto come "registro della polizia". Questo registro deve essere conservato quotidianamente e contenere una descrizione degli oggetti acquisiti o detenuti per la vendita. Inoltre, questo registro deve consentire l'identificazione di queste opere così come quella delle persone che li hanno venduti.

Un modello di registro è proposto nella pianificazione 1 dell'ordine del 21 luglio 1992.

Un professionista che elude questo obbligo rischia una pena detentiva di sei mesi e una multa di 30.000 euro.

*Per andare oltre* Articoli da 321 a 7, da R.321-3 a R.321-7 del Codice Penale e dell'Appendice 1 dell'Ordine del 21 luglio 1992.

#### Obbligo di mantenere un registro di polizia per i metalli preziosi lavorati o non lavorati

Il gallerista deve tenere un registro "polizia" dei suoi acquisti, vendite, ricevute e forniture di metalli preziosi o opere contenenti questi materiali (oro, argento, platino o qualsiasi lega di questi metalli), indipendentemente dal fatto che siano realizzati o meno. Il proprietario della galleria deve tenere questo registro a disposizione delle autorità pubbliche, su qualsiasi requisito.

Lo scopo di questo registro è quello di garantire la tracciabilità delle opere metalliche preziose e di limitare il rischio di ricezione.

*Per andare oltre* Articoli da 537 a 539 del Codice fiscale generale e circolare del 22 luglio 2010 sulla garanzia dei metalli preziosi e dei termini di conservazione dei registri (il cosiddetto "libro della polizia").

#### Obbligo di acquisto da persone note o con intervistati a loro

I commercianti di oro, argento, platino o container possono acquistare questi beni solo da persone note o con intervistati a loro noti.

*Per andare oltre* sezione 539 del codice tributario generale.

#### Requisito di visualizzazione dei prezzi

Nelle gallerie d'arte è ammessa un'etichetta discreta sulle opere d'arte originali (e sugli oggetti dell'antichità). Inoltre, la capacità del cliente di visualizzare un listino prezzi è sufficiente per adempiere all'obbligo del gallerista di informare il consumatore sul prezzo dei beni esposti.

*Per andare oltre* : circolare del 19 luglio 1988 sulla visualizzazione dei prezzi dei libri originali e usati.

Tre gradi. Procedure di installazione e formalità
-------------------------------------------------

### a. Le formalità di rendicontazione delle società

La società deve essere iscritta al Registro del Commercio e delle Imprese (RCS). Per ulteriori informazioni, si consiglia di vedere le "Formalità di reporting delle società commerciali" e "Registrazione di un'impresa commerciale presso l'RCS".

### b. Se necessario, fare una dichiarazione di esistenza con la Casa degli Artisti

Il proprietario della galleria che distribuisce opere d'arte sotto la classificazione delle arti grafiche e plastiche deve fare la dichiarazione alla Camera degli Artisti.

Le arti grafiche e plastiche includono dipinti, disegni, incisioni, stampe, litografie, sculture, arazzi e tessuti da parete fatti a mano, esempi unici di ceramiche, ecc. Per maggiori dettagli, si consiglia di fare riferimento alla [sito ufficiale della Casa degli Artisti](%5b*http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/*%5d(http://www.lamaisondesartistes.fr/site/qui-peut-sinscrire-2/)).

Questa dichiarazione identifica il dichiarante come un'emittente commerciale di opere di artisti-autori, in quanto tale, al contributo corrispondente.

**Autorità competente**

La dichiarazione deve essere indirizzata alla Camera degli Artisti.

**Tempo**

La dichiarazione deve aver luogo entro otto giorni dall'inizio dell'attività del dichiarante. La Casa degli Artisti riconosce la ricezione della dichiarazione di attività ed emette un identificatore chiamato numero d'ordine.

**Documenti di supporto**

La dichiarazione deve includere:

- Le [Forma di dichiarazione dell'esistenza](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d%E2%80%99existence.pdf) completato, datato e firmato;
- una copia del Certificato di Registrazione per l'Elenco delle Imprese e degli Stabilimenti (Sirene).

**Costo**

Gratuito.

*Per andare oltre* Articoli L. 382-4 e R. 382-20 del codice di previdenza sociale.

### c. Se necessario, fare una dichiarazione di esistenza con il Agessa

Il proprietario della galleria che distribuisce opere d'arte appartenenti al ramo di scrittori o fotografi in particolare deve fare la dichiarazione al Agessa.

**Autorità competente**

La dichiarazione deve essere indirizzata all'Agessa.

**Tempo**

L'Agessa riconosce la ricezione della dichiarazione di esistenza ed emette una ricevuta con un numero di riferimento.

**Documenti di supporto**

La dichiarazione dell'esistenza è fatta compilando il [Forma di dichiarazione dell'esistenza](http://www.secu-artistes-auteurs.fr/sites/default/files/pdf/D%C3%A9claration%20d'existence.pdf).

**Costo**

Gratuito.

*Per andare oltre* Articoli L. 382-4 e R. 382-20 del codice di previdenza sociale.

### d. Se necessario, fare una dichiarazione come porta metalli preziosi

Le persone che detengono materiali d'oro, d'argento o di platino, lavorati o non lavorati, come parte della loro attività professionale devono farsi conoscere alle autorità fiscali presentando una dichiarazione di esistenza.

**Autorità competente**

La dichiarazione di esistenza viene effettuata presso l'ufficio di garanzia territorialmente competente, collegato alla direzione regionale dei doveri doganali e indiretti. È possibile consultare il [sito ufficiale delle dogane](%5b*http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees*%5d(http://www.douane.gouv.fr/articles/a10978-organisation-des-bureaux-de-garantie-et-organismes-de-controle-agrees)), l'elenco degli uffici di garanzia vicino al reparto aziendale.

**Documenti di supporto**

Deve contenere:

- la dichiarazione di esistenza su carta libera datata e firmata, affermando:- Il nome del professionista o dell'azienda,
  + L'esatta designazione dell'attività,
  + L'indirizzo della sede centrale e il luogo di pratica;
- Fotocopia della carta d'identità nazionale, passaporto, certificato di nascita, libretto di famiglia o patente di guida;
- gli indirizzi della sede centrale e il luogo di pratica devono essere giustificati dalla produzione di una copia di un contratto di locazione commerciale o professionale al fine di consentire i controlli dell'amministrazione;
- un certificato di registrazione nel Registro delle Compravendite e delle Imprese (RCS) o nell'elenco delle operazioni o una copia dell'estratto Kbis della società che menziona l'attività di vendita o di acquisto di opere metalliche preziose.

**Costo**

Gratuito.

*Per andare oltre* Sezioni 533 e 534 del codice fiscale generale e dell'articolo 211 A del programma III del codice fiscale generale.

