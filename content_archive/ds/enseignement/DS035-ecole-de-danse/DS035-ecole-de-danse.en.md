﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS035" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Education" -->
<!-- var(title)="Dance school" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="education" -->
<!-- var(title-short)="dance-school" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/education/dance-school.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="dance-school" -->
<!-- var(translation)="Auto" -->


Dance school
============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The dance school is an institution where dance is taught in all its forms (classical, contemporary, jazz, sports, urban, ballroom dance, etc.). As part of the school, the dance teacher transmits his knowledge to his students through the pedagogical project he develops.

*To go further* Articles L. 462-1 and the following articles of the Education Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the activity and the legal form of the business:

- for the liberal professions, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

For more information, it is advisable to visit the Urssaf's websites,[ICC In Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites),[Strasbourg ICC](http://strasbourg.cci.fr/cfe/competences) And[Public Service](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

No diploma is required to operate a dance school. On the other hand, the dance school can only employ dance teachers whose professional qualification or experience is recognized.

For more information, it is advisable to refer to the activities sheet " [Dance teacher](https://www.guichet-entreprises.fr/en/ds/enseignement/professeur-de-danse.html) ».

*To go further* Article L. 462-1 of the Education Code.

### b. Conditions of honorability - incompatibilities

No person may operate for remuneration, either directly or through another person, an institution where dance is taught, if he has been the subject of a conviction under Section L. 362-5 of the Education Code, that is, a sentence of imprisonment without a conditional sentence of more than four months for offences of rape, sexual assault, sexual assault on a minor or pimping.

*To go further* Articles L. 362-5 and L. 462-2 of the Education Code; Articles 222-22 and the following of the Penal Code.

### c. Liability insurance

The operator of a dance school must take out an insurance contract covering his or her civil liability, that of teachers, attendants and those who are taught there.

*To go further* Article L. 462-1 of the Education Code.

### d. Some peculiarities of the regulation of the activity

#### General regulation applicable to all public institutions (ERP)

If the premises are open to the public, the professional must comply with the rules for institutions receiving the public:

- Fire
- accessibility.

For more information, it is advisable to refer to the activities sheet " [Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

#### Dance school-specific regulations

##### Meet health and safety standards

The dance halls have at least one comfort desk and a shower. When there are more than twenty users admitted simultaneously, these sanitary and sanitary facilities are increased by one unit per twenty additional users or a fraction of that number.

Operators are required to have an emergency kit (designed to provide first aid in the event of an accident) and a means of communication to quickly alert emergency services.

Operators of the establishments in which dance instruction is provided are required, within 8 days, to inform the prefect of the department of any accidents that have occurred in their establishment that require hospitalization.

*To go further* Articles R. 462-2 to R. 462-4 of the Education Code.

##### Regarding the organization of space and ground-related equipment

In dance halls used for teaching purposes, the dancers' evolution area is covered with a smooth, supple, resistant material that makes it unspeally laid. It does not rest directly on hard ground such as concrete or tile.

When the evolution area is made up of a parquet floor, the elements that make it up are produced from wood with a structure and cohesion that will prevent the formation of spled or ruptures.

Finally, during the dance class, the changing area and the space of the halls are free of any obstacles that pose a threat to the safety of the students.

*To go further* Article R. 462-1 of the Education Code.

##### Mandatory displays at the place of instruction

Some documents must be posted on the premises of the dance school:

- A copy of the receipt of an opening statement or a change in activity made to the prefecture;
- reproduction of articles R. 462-1 to R. 462-9, R. 362-1 and R. 362-2 of the Education Code, relating to technical, hygiene and safety rules;
- a relief organization board displayed in a location accessible to teachers and users. It includes the addresses and telephone numbers of the services and agencies that are used in the event of an emergency;
- the list of teachers, with an indication of the date of graduation or exemption.

*To go further* Article R. 462-5 of the Education Code.

##### Regarding students

Dance operators must ensure, prior to the start of each teaching period, that students have a medical certificate attesting to the absence of contraindication to the teaching provided to them. This certificate must be renewed every year. At the request of any teacher, a certificate attesting to an additional medical examination must be required.

*To go further* Article R. 362-2 of the Education Code.

The school can only accommodate students over the age of four.

Four- and five-year-olds can only engage in physical arousal activities (i.e. excluding all techniques specific to the discipline taught).

Five- and six-year-olds can only engage in an introductory activity (i.e. exclude all techniques specific to the discipline being taught).

In general, all activities performed by children aged four to seven years included must not involve any burdensome work for the body, excessive extensions or forced joints.

*To go further* Articles L. 462-1 and R. 362-1 of the Education Code.

##### Broadcasting music during classes or internships

The dance school wishing to broadcast sound music must seek permission from the Society of Composers and Music Publishers (Sacem) before the opening of the school.

A contract was then concluded between the Sacem and the dance school providing, in exchange for the payment of a flat fee, the authorisation to broadcast the works of the French and international repertoire managed by the Sacem. The amount of the fee varies depending on the number of students and the type of courses taught. For more details, it is advisable to refer to the [Sacem's official website](https://www.Sacem.fr/).

**What to know**

Sacem is also responsible for collecting, on behalf of the Society for the Collection of Fair Compensation (SPRE), fair remuneration owed to performers and producers for the use of recorded music. The dance school will also have to pay this fee to the Sacem.

Dance courses are based on a fee-based fee based on their duration in days.

*To go further* : Intellectual Property Code and December 8, 2010 decision of the commission under Article L. 214-4 of the Intellectual Property Code amending the January 5, 2010 decision.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, it is advisable to refer to the activity sheets "Formality of reporting a commercial company" and "Registration of an individual company in the register of trade and companies.

### b. Post-registration authorisation

#### Opening statement of a dance teaching room

The opening of an establishment where dance is taught must be declared at least two months before it opens.

**Competent authority**

The statement must be addressed to the prefect of the department where the premises are located.

**Response time**

The prefect may, within a month of the declaration, prohibit the opening of the establishment if the technical, hygiene and safety guarantees are not fulfilled.

**Supporting documents**

The opening statement of a dance school must include:

- Form Cerfa 1045203 filled, dated and signed;
- documents relating to the characteristics of the room (plan showing the scale, total area, number of comfort rooms, number of showers and sinks, number of studios and, for each studio, specify its area, height, nature and soil features, lighting and ventilation mode);
- certificate of liability insurance.

**Outcome of the procedure**

Unless the opening order is made, the prefect sends the declarant a receipt of an opening declaration. A copy of this receipt must be displayed in the establishment.

**Cost**

Free.

*To go further* Articles L. 462-1 and L. 462-4 of the Education Code.

#### Statement of activity change or closure of a dance teaching room

The operator must report the modification or closure of his dance school within 15 days of the modification or closure.

**Competent authority**

The declaration must be addressed to the prefect of the department where the premises are located, within 15 days of the change in the activity or the closure of the establishment.

**Response times**

The prefect may, within a month of the declaration of amendment, order the closure of the establishment for a maximum of three months, if the technical, hygienic and safety guarantees are not fulfilled.

**Evidence of a declaration of change**

The statement should include:

- Form Cerfa 1045403 filled, dated and signed;
- documents relating to the characteristics of the room before modification: a map showing the scale, total area, number of comfort rooms, number of showers and sinks, number of studios and, for each studio, specify its area, its height, nature and soil characteristics, lighting and ventilation methods;
- documents relating to the characteristics of the room after modification: a map showing the scale, the total area, the number of comfort rooms, the number of showers and sinks, the number of studios and, for each studio, specify its area, its height, nature and soil characteristics, lighting and ventilation methods;
- certificate of liability insurance.

**Evidence of a closure declaration**

The statement should include:

- Form Cerfa 1045303 filled, dated and signed;
- documents relating to the characteristics of the premises: its surface area, the number of studios and their respective surfaces;
- certificate of liability insurance.

*To go further* Articles L. 462-1 and L. 462-4 of the Education Code.

