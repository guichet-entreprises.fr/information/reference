﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS035" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseignement" -->
<!-- var(title)="Ecole de danse" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="enseignement" -->
<!-- var(title-short)="ecole-de-danse" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/enseignement/ecole-de-danse.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="ecole-de-danse" -->
<!-- var(translation)="None" -->

# Ecole de danse

Dernière mise à jour : <!-- begin-var(last-update) -->2020-12<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’école de danse est un établissement où est dispensé l'enseignement de la danse sous toutes ses formes (classique, contemporaine, jazz, sportive, urbaine, danse de salon, etc.). Dans le cadre de l’école, l’enseignant en danse transmet ses acquis à ses élèves grâce au projet pédagogique qu’il élabore.

*Pour aller plus loin* : articles L. 462-1 et suivants du Code de l’éducation.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de l’activité exercée et de la forme juridique de l’entreprise :

- pour les professions libérales, le CFE compétent est l’Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre du commerce et de l’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

Pour plus d’informations, il est conseillé de consulter les sites de l'Urssaf, [de la CCI de Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites), [de la CCI de Strasbourg](http://strasbourg.cci.fr/cfe/competences) et [du Service public](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucun diplôme n’est nécessaire pour pouvoir exploiter une école de danse. En revanche, l'école de danse ne peut employer que des professeurs de danse dont la qualification professionnelle ou l'expérience est reconnue.

Pour plus d’informations, il est conseillé de se reporter à la fiche activités « [Professeur de danse](https://www.guichet-entreprises.fr/fr/ds/enseignement/professeur-de-danse.html) ».

*Pour aller plus loin* : article L. 462-1 du Code de l’éducation.

### b. Conditions d’honorabilité - incompatibilités

Nul ne peut exploiter contre rémunération, soit directement, soit par l'intermédiaire d'une autre personne, un établissement où est dispensé un enseignement de la danse, s'il a fait l'objet d'une condamnation visée à l'article L. 362-5 du Code de l’éducation, c’est-à-dire une peine d’emprisonnement sans sursis supérieure à quatre mois pour les infractions de viol, agression sexuelle, atteinte sexuelle sur mineur ou proxénétisme.

*Pour aller plus loin* : articles L. 362-5 et L. 462-2 du Code de l’éducation ; articles 222-22 et suivants du Code pénal.

### c. Assurance de responsabilité civile

L'exploitant d’une école de danse doit souscrire un contrat d'assurance couvrant sa responsabilité civile, celle des enseignants, des préposés et des personnes qui y suivent un enseignement.

*Pour aller plus loin* : article L. 462-1 du Code de l’éducation.

### d. Quelques particularités de la réglementation de l’activité

#### Réglementation générale applicable à tous les établissements recevant du public (ERP)

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche activités « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) ».

#### Réglementation spécifique aux écoles de danse

##### Respecter les normes d’hygiène et de sécurité

Les salles de danse comportent au moins un cabinet d'aisances et une douche. Lorsque les usagers admis simultanément sont plus de vingt, ces équipements hygiéniques et sanitaires sont augmentés d'une unité par vingtaine d'usagers supplémentaires ou fraction de ce nombre.

Les exploitants sont dans l’obligation de se doter d'une trousse de secours (destinée à apporter les premiers soins en cas d'accident) et d'un moyen de communication permettant d'alerter rapidement les services de secours.

Les exploitants des établissements dans lesquels est dispensé un enseignement de la danse sont tenus, dans un délai de 8 jours, d'informer le préfet du département de tout accident survenu dans leur établissement ayant nécessité une hospitalisation.

*Pour aller plus loin* : articles R. 462-2 à R. 462-4 du Code de l’éducation.

##### Concernant l’organisation de l’espace et l’équipement relatif au sol

Dans les salles de danse exploitées à des fins d'enseignement, l'aire d'évolution des danseurs est recouverte d'un matériau lisse, souple, résistant et posé de manière homogène la rendant peu glissante. Elle ne repose pas directement sur un sol dur tel que le béton ou le carrelage.

Lorsque l'aire d'évolution est constituée d'un parquet, les éléments qui le constituent sont produits à partir de bois ayant une structure et une cohésion de nature à éviter la formation d'échardes ou les ruptures.

Enfin, pendant le cours de danse, l'aire d'évolution et l'espace des salles sont libres de tout obstacle constituant une menace pour la sécurité des élèves.

*Pour aller plus loin* : article R. 462-1 du Code de l’éducation.

##### Affichages obligatoires sur le lieu d’enseignement

Certains documents doivent obligatoirement être affichés dans les locaux de l’école de danse :

- la copie du récépissé de déclaration d’ouverture ou de modification d’activité faite à la préfecture ;
- la reproduction des articles R. 462-1 à R. 462-9, R. 362-1 et R. 362-2 du Code de l'éducation, relatifs aux règles techniques, d'hygiène et de sécurité ;
- un tableau d'organisation des secours affiché en un endroit accessible aux enseignants et aux usagers. Il comporte les adresses et numéros de téléphone des services et organismes auxquels il est fait appel en cas d'urgence ;
- la liste des enseignants, avec indication de la date d’obtention du diplôme d’État ou de la dispense.

*Pour aller plus loin* : article R. 462-5 du Code de l'éducation.

##### Concernant les élèves

Les exploitants de danse doivent s'assurer, avant le début de chaque période d'enseignement, que les élèves sont munis d'un certificat médical attestant l'absence de contre-indication à l'enseignement qui leur est dispensé. Ce certificat doit être renouvelé chaque année. À la demande de tout enseignant, un certificat attestant d’un examen médical supplémentaire doit être requis.

*Pour aller plus loin* : article R. 362-2 du Code de l’éducation.

L'établissement ne peut recevoir que des élèves âgés de plus de quatre ans.

Les enfants de quatre et cinq ans ne peuvent pratiquer que des activités d’éveil corporel (c’est-à-dire excluant toutes les techniques propres à la discipline enseignée).

Les enfants de cinq et six ans ne peuvent pratiquer qu’une activité d’initiation (c’est-à-dire excluant toutes les techniques propres à la discipline enseignée).

De manière générale, l’ensemble des activités pratiquées par les enfants de quatre à sept ans inclus ne doit comporter aucun travail contraignant pour le corps, ni des extensions excessives, ni articulations forcées.

*Pour aller plus loin* : articles L. 462-1 et R. 362-1 du Code de l’éducation.

##### Diffusion de musique lors de cours ou de stages

L’école de danse souhaitant diffuser de la musique de sonorisation doit, en demander l’autorisation à la Société des auteurs compositeurs et éditeurs de musique (Sacem) avant l’ouverture de l’établissement.

Un contrat est alors conclu entre la Sacem et l’école de danse prévoyant, contre le paiement d’une redevance forfaitaire, l’autorisation de diffuser les œuvres du répertoire français et international gérées par la Sacem. Le montant de la redevance varie notamment en fonction du nombre d’élèves et du type de cours dispensé. Pour plus de précisions, il est conseillé de se reporter au [site officiel de la Sacem](https://www.Sacem.fr/).

**À savoir**

La Sacem est également chargée de collecter, pour le compte de la Société pour la perception de la rémunération équitable (SPRE), la rémunération équitable due aux artistes interprètes et producteurs pour l’utilisation de musique enregistrée. L’école de danse devra donc également s’acquitter de cette redevance auprès de la Sacem.

Les stages de danse relèvent quant à eux d’un forfait de droits d’auteur déterminé selon leur durée en jours.

*Pour aller plus loin* : Code de la propriété intellectuelle et décision du 8 décembre 2010 de la commission prévue à l'article L. 214-4 du Code de la propriété intellectuelle portant modification de la décision du 5 janvier 2010.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Autorisation(s) post-immatriculation

#### Déclaration d’ouverture d’un local d’enseignement de la danse

L’ouverture d’un établissement où est enseigné la danse doit être déclarée au moins deux mois avant son ouverture.

**Autorité compétente**

La déclaration doit être adressée au préfet du département où le local se situe.

**Délai de réponse**

Le préfet peut, dans le mois qui suit la déclaration, interdire l'ouverture de l’établissement si les garanties sur le plan technique, d’hygiène et de sécurité ne sont pas remplies. 

**Pièces justificatives**

La déclaration d’ouverture d’un établissement d’enseignement de la danse doit contenir :

- le formulaire Cerfa 10452*03 rempli, daté et signé ;
- les documents relatifs aux caractéristiques du local (plan indiquant l’échelle, la surface totale, le nombre de cabinets d’aisances, le nombre de douches et de lavabos, le nombre de studios et, pour chaque studio, préciser sa superficie, sa hauteur, la nature et les caractéristiques du sol, les modalités d’éclairage et le mode de ventilation) ;
- l’attestation d’assurance de responsabilité civile.

**Issue de la procédure**

Sauf décision d’interdiction d’ouverture, le préfet adresse au déclarant un récépissé de déclaration d’ouverture. Une copie de ce récépissé doit être affichée dans l’établissement.

**Coût**

Gratuit.

*Pour aller plus loin* : articles L. 462-1 et L. 462-4 du Code de l’éducation.

#### Déclaration de modification d’activité ou de fermeture d’un local d’enseignement de la danse

L’exploitant doit déclarer la modification ou la fermeture de son établissement d’enseignement de la danse dans les 15 jours qui suivent la modification ou la fermeture.

**Autorité compétente**

La déclaration doit être adressée au préfet du département où le local se situe, dans les 15 jours suivant la modification de l’activité ou la fermeture de l’établissement.

**Délais de réponse**

Le préfet peut, dans le mois qui suit la déclaration de modification, prononcer la fermeture de l’établissement pour une durée maximale de trois mois, si les garanties techniques, hygiéniques et de sécurité ne sont pas remplies. 

**Pièces justificatives d’une déclaration de modification**

La déclaration doit contenir :

- le formulaire Cerfa 10454*03 rempli, daté et signé ;
- les documents relatifs aux caractéristiques du local avant modification : un plan indiquant l’échelle, la surface totale, le nombre de cabinets d’aisances, le nombre de douches et de lavabos, le nombre de studios et, pour chaque studio, préciser sa superficie, sa hauteur, la nature et les caractéristiques du sol, les modalités d’éclairage et le mode de ventilation ;
- les documents relatifs aux caractéristiques du local après modification : un plan indiquant l’échelle, la surface totale, le nombre de cabinets d’aisances, le nombre de douches et de lavabos, le nombre de studios et, pour chaque studio, préciser sa superficie, sa hauteur, la nature et les caractéristiques du sol, les modalités d’éclairage et le mode de ventilation ;
- l’attestation d’assurance de responsabilité civile.

**Pièces justificatives d’une déclaration de fermeture**

La déclaration doit contenir :

- le formulaire Cerfa 10453*03 rempli, daté et signé ;
- les documents relatifs aux caractéristiques du local : sa surface, le nombre de studios et leurs surfaces respectives ;
- l’attestation d’assurance de responsabilité civile.

*Pour aller plus loin* : articles L. 462-1 et L. 462-4 du Code de l’éducation.