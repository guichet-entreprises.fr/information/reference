<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS109" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Education" -->
<!-- var(title)="Training centre accredited to train candidates to the state diploma of dance professor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="education" -->
<!-- var(title-short)="training-centre-state-diploma-dance-professor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/education/training-centre-state-diploma-dance-professor.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="training-centre-state-diploma-dance-professor" -->
<!-- var(translation)="Auto" -->

# Training centre accredited to train candidates to the state diploma of dance professor

Latest update: <!-- begin-var(last-update) -->2020-12<!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

# 1°. Definition of the activity

## a. Definition

In order to be able to practice, training centers for the State diploma of dance teacher must obtain authorization from the Ministry of Culture.

The objective of the authorization requirement is to verify that the courses comply with the programs defined by the regulations in force and provided by qualified trainers. It also ensures the feasibility of the training offer. These elements ensure that students benefit from an education allowing them to acquire the skills attested by the State diploma of dance teacher.

The limited duration of five years of the authorization makes it possible to verify that the establishments are capable of carrying out the training courses on a long-term basis, both in terms of administrative, financial and educational aspects. In addition, the duration of the training being two years, it is possible, during the examination of the renewal of the accreditation, to accurately assess the results obtained by at least two promotions of students, which makes it possible to ask the centers to make any necessary adjustments to the training offer when deficiencies are noted in the methods of implementing study programs.

## 2°. Installation conditions

In order to be able to practice, training centers for the State diploma of dance teacher must obtain authorization from the Ministry of Culture.

The authorization is issued by decision of the Minister of Culture, after detailed advice from the competent regional director of cultural affairs and the artistic creation inspectorate.

The accreditation relates to one or more of the three options constituting the State diploma of dance teacher. **The issuance of the authorization, or its renewal, is subject to the following conditions:**

- the training center provides, in the option (s) concerned, either alone or as part of a contractual pooling with one or more other authorized training centers, all of the training as well as the material organization of the candidate assessment;
- the compatibility of the premises with the training offer: number of rooms, dimensions, application of the provisions provided for in Articles R. 462-1 to R. 462-5 of the aforementioned Education Code (see in this regard the applicable measures dance schools);
- the organization must have a sufficient number of qualified teaching staff for each discipline taught;
- the economic viability of the centre's activity, in particular with regard to the existing territorial offer.

The body's authorization for training for the State diploma of dance teacher, or its renewal, is issued by the Minister responsible for culture for a renewable period of five years.

If, during the authorization period, changes occur concerning the methods of organization and monitoring of the training or its content or the teaching team, the organization is required to inform management without delay. regional cultural affairs office, which forwards it, together with its observations, to the director general of artistic creation within a maximum period of fifteen days.

When the conditions of the authorization are no longer met, the suspension or withdrawal of the authorization is pronounced by a reasoned decision of the Minister of Culture who sets the terms of its implementation.

## 3°. Installation procedures and formalities

### a. Company declaration formalities

The formalities depend on the legal nature of the business.

### b. Apply for authorization from the Ministry of Culture

The **accreditation request file** to provide training for the State diploma of dance teacher includes:

- documents relating to the general characteristics of the organization, in particular its legal status and details of its activities;
- the presentation of the teaching team approached to provide the training as well as the copy of the dance teaching diplomas of the dance teachers who compose it or of their dispensation and the characteristics concerning the organization and the content of the training;
- the general estimated budget of the organization and the specific budget of the teaching activity by option proposed;
- the number and dimensions of each of the rooms assigned to training as well as, for those intended for dance practice, the type of floors and sanitary fittings;
- the organization of the courses, and in particular the detailed planning of the training (the number of hours, the specialties, the names of the speakers, the studios occupied), the training schedule as well as the conditions of organization of the evaluation training units;
- the list of establishments approached to make available to student subjects or to host role-plays for students as part of their training;
- in the case of pooling with one or more other authorized training centers, the organizational arrangements for operations carried out jointly.

The request can be made [online](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/DANSE_ETABL_habilitation_01/?__CSRFTOKEN__=2bc79b9a-9655-4484-8540-44886db7a900).

The **authorization renewal file** to provide training for the State diploma of dance teacher includes the following documents:

- a sworn statement certifying that no change has taken place with regard to the documents provided during the previous request or, where applicable, the documents and information relating to the modifications made since this request;
- the list of elements that have contributed to improving the skills or qualifications of the teaching team;
- a detailed assessment of the training activity of the last four years. The detailed report of the training activity specifies:
  - the number of training sessions provided during this period;
  - the number of participants who have followed these training courses, the assessment of the professional integration of graduate students who have been trained in the center;
  - the number of candidates admitted to sit the assessment tests during each examination session organized by the center, distinguishing those who have undergone training in the center for the test concerned;
  - the description and assessment of the reception procedures for candidates who have obtained partial validation of experience during the validation procedure for prior experience organized under the terms of Chapter III of this order and its Annex VI , in accordance with Articles R. 335-9 to R. 335-11 of the Education Code or European nationals subject to compensation measures within the framework of the procedure for the recognition of professional qualifications provided for in Article L. 362-1-1 of the Education Code;
  - the budget for the last two financial years in revenue and expenditure for the State diploma training activity;
  - in the case of pooling with one or more other authorized training centers, the organizational arrangements for operations carried out jointly.

The renewal request is sent by registered mail with acknowledgment of receipt, one year before the expiration of the five-year period, to the regionally competent regional directorate of cultural affairs, which forwards it, together with its opinion, to the director general of artistic creation within a maximum period of fifteen days.

The acknowledgment of receipt of the authorization renewal request stating that the file is complete is issued by the general direction of artistic creation.

The decision following the request for renewal of the authorization is notified within ten months from the date of the acknowledgment of receipt of the request.

## 4°. Reference texts

Order of 23 July 2019 as amended relating to the various access routes to the profession of dance teacher in application of Article L. 362-1 of the Education Code