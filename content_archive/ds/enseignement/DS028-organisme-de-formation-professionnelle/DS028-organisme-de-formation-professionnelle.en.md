﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS028" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Education" -->
<!-- var(title)="Vocational training centre" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="education" -->
<!-- var(title-short)="vocational-training-centre" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/education/vocational-training-centre.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="vocational-training-centre" -->
<!-- var(translation)="Auto" -->


Vocational training centre
================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

A vocational training organization (or training provider) is a legal or physical person offering continuing education to:

- integration, reintegration into work or maintaining employment;
- developing the professional skills of those who have completed the training.

Training can also allow people who have voluntarily quit their work (to help a dependent relative or to take care of their children) to return to work.

Employees' access to training activities can be carried out either:

- At the employer's initiative as part of the training plan;
- at the employee's initiative in the context of a leave of absence (skills check, individual training leave, teaching and research, etc.);
- in the context of a vocational training agreement (concluded by a private or public legal entity with the organization);
- professionalization contract (concluded between the organization and an individual intern).

**Please note**

The list of continuing education actions is set out in Article L. 6313-1 of the Labour Code.

*For further information*: Article L. 6311-1 of the Labour Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- In the case of the creation of an individual company, the competent CFE is the Urssaf;
- For a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI);
- in the case of the creation of a civil society, it is the registry of the commercial court, or the registry of the district court in the departments of lower Rhine, Upper Rhine and Moselle.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To operate a vocational training organization, no professional qualifications are required.

However, the professional must ensure that the members of his staff justify the qualifications and qualities necessary to carry out the teaching and coaching activity within each specific training.

As soon as the first vocational training agreement is concluded and no later than three months, the person must report his activity to the regional prefect at the site of his main exercise (see infra "3o). a. Declaration of activity").

*For further information*: Article L. 6351-1 of the Labour Code.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

A national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement is not subject to any professional qualification requirements in the same way as the French national.

However, when the body operating in France is headquartered in an EU or EEA member state, its operator must appoint a representative domiciled in France. If necessary, the operator must make the declaration of activity with the prefect of the region in which that representative is domiciled.

**Please note**

This reporting obligation is not applicable to bodies whose head office is located in a Member State but which operates in France on a temporary or casual basis.

*For further information*: Article R. 6351-3 of the Labour Code.

### c. Conditions of honourability and sanctions

The professional who wishes to operate a vocational training organisation must not have been the subject of a criminal conviction for breaches of probity, honour or good morals.

*For further information*: Article L. 6352-2 of the Labour Code.

**Financial sanctions**

When the organization does not provide training (totally or partially) it is required to repay all the sums paid by the contractor.

*For further information*: Article L.6354-3 of the Labour Code.

**Criminal sanctions**

The operator of a vocational training organization is liable to criminal penalties for breaches of the provisions relating to the exercise of its activity. In particular, he faces a fine of 4,500 euros if he:

- has not made a prior declaration of its activity or in a lack of knowledge of the mandatory provisions (see infra "3°. a. Declaration of activity");
- cannot justify the qualifications and qualities of the trainers within the institution;
- does not meet the conditions of honour;
- does not establish an internal regulation within its institution;
- does not appoint an auditor even though he exceeds the thresholds;
- not to address the pedagogical and financial balance sheet.

*For further information*: Articles L. 6355-1 and the following articles of the Labour Code.

### d. Some peculiarities of the regulation of the activity

**Interns' internal regulations**

Any vocational training organization must establish an internal regulation within three months of starting the activity.

This internal regulation must determine:

- Safety and health measures applicable to the establishment;
- the rules on discipline and possible sanctions;
- the terms of the training staff for training activities lasting more than five hundred hours.

**Please note**

Where the organization has several institutions, this internal regulation can be adapted to suit each.

*For further information*: Article L. 6352-3 of the Labour Code.

**Accounting obligations**

Depending on the nature of its activity, the operator must draw up an accounting report each year.

For example, public training organizations must take a separate account of their continuing vocational training activity. On the other hand, private law training organizations must:

- Make an annual report on the result, the income statement and an appendix;
- appoint an auditor and a substitute if, at the end of the fiscal year, they exceed the following thresholds:- three employees,
  - 153,000 euros in sales or resources,
  - 230,000 euros of total balance sheet.

In addition, the operator of the vocational training organization must submit an educational and financial report by 30 April of each year to the regional prefect (see infra "3°. v. Post-registration authorisation).

*For further information*: Article L. 6352-6 of the Labour Code.

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERPs) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

It is advisable to refer to the "Establishment receiving the public" sheet for more information.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

**Training agreement between training organization and buyer**

All training activities are carried out according to a pre-established programme. At each training, the organization must enter into a contract with its training client stating:

- The title, the nature, duration and procedures of the training;
- and, if so, the contributions of public persons.

*For further information*: Articles L. 6353-2 and R. 6353-2 of the Labour Code.

**Training contract between a natural person and the training organization**

When the organization is considering training an individual individual individual individual,0105-person individual employee, it must enter into a contract with the individual before it is finalized and the training fee is paid.

This contract must specify, on pain of nullity:

- The nature, duration, program and purpose of the training and its staff;
- The level of qualifications required to access this training;
- the conditions for issuing the training, the methods of knowledge control and, if necessary, the nature of the sanction;
- The qualifications of the person providing the training;
- training fees and settlement terms.

**Please note**

The intern may withdraw for 10 days from the signing of the contract. If this is the case, the intern must submit an application by recommended letter with notice of receipt and no fees may be claimed.

*For further information*: Articles L. 6353-3 and the following articles of the Labour Code.

#### Intern information

The organization must provide the training trainee with all the information related to:

- The curriculum, objectives and modalities for evaluating training;
- The list of trainers
- Contact information for relationships with the trainee
- the internal regulations applicable to training.

*For further information*: Articles L. 6353-8 and the following articles of the Labour Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Statement of activity

**Competent authority**

The operator of the vocational training organization must send his statement to the prefect of the region in which his main institution is located.

**Supporting documents**

The statement must be accompanied by the following documents:

- A photocopy of the proof of the Siren number
- Bulletin 3 of the operator's criminal record less than a month old;
- A copy of the first vocational training agreement or the first contract entered into where, failing that, proof that a first achievement is planned;
- If so, a proof of registration on the[public list of agencies responsible for carrying out competency assessments](https://www.data.gouv.fr/fr/datasets/liste-publique-des-organismes-de-formation-l-6351-7-1-du-code-du-travail/) ;
- a copy of the training program and lists of stakeholders mentioning their titles, qualifications and the delivery and contractual relationship that bind them to the training organization.

**Procedure**

The regional prefect acknowledges receipt of the application within thirty days of its filing and sends a receipt to the applicant. It then registers the operator's activity.

**Please note**

The prefect's failure to respond beyond thirty days is a case in which the request is rejected. In addition, an amending statement may be made within 30 days to the regional prefect to amend the declaration of activity.

*For further information*: Articles R. 6351-1 to R. 6351-7 of the Labour Code.

### b. Company reporting formalities

Depending on the nature of his activity, the professional must or must register with the Register of Trade and Companies (RSC) or register with the Urssaf.

It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### c. Post-registration authorisation

#### Send an educational and financial assessment

Before April 30 of each year, the training provider must send this assessment to the regional prefect.

This balance sheet must be accompanied by the balance sheet of the fiscal year and mention:

- Training activities carried out during the fiscal year;
- The number of trainees accepted;
- The number of trainee hours and hours of training, as well as their hourly volume, the nature and level of training;
- The allocation of funds received and the amount of invoices issued;
- Accounting data on continuing vocational training;
- all proceeds from the investment of the funds received.

**Please note**

The regional prefect may also require a list of training services to be performed.

*For further information*: Articles L. 6352-11 and R. 6352-22 and following of the Labour Code.

