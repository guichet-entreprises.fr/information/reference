﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS028" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseignement" -->
<!-- var(title)="Organisme de formation professionnelle" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="enseignement" -->
<!-- var(title-short)="organisme-de-formation-professionnelle" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/enseignement/organisme-de-formation-professionnelle.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="organisme-de-formation-professionnelle" -->
<!-- var(translation)="None" -->

# Organisme de formation professionnelle

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Un organisme de formation professionnelle (ou dispensateur de formation) est une personne morale ou physique proposant une formation continue en vue de favoriser :

- l'insertion, la réinsertion professionnelle ou le maintien de l'emploi ;
- le développement des compétences professionnelles des personnes ayant suivi la formation.

La formation peut également permettre le retour à l'emploi des personnes ayant cessé volontairement leur activité professionnelle (pour aider un proche en situation de dépendance ou s'occuper de ses enfants).

L'accès des salariés aux actions de formation peut être effectué soit :

- à l'initiative de l'employeur dans le cadre du plan de formation ;
- à l'initiative du salarié dans le cadre d'un congé (bilan de compétences, congés individuel de formation, enseignement et recherche, etc.) ;
- dans le cadre d'une convention de formation professionnelle (conclue par une personne morale de droit privé ou public avec l'organisme) ;
- dans le cadre d'un contrat de professionnalisation (conclu entre l'organisme et un stagiaire à titre individuel).

**À noter**

La liste des actions de formation continue est fixée à l'article L. 6313-1 du Code du travail.

*Pour aller plus loin* : article L. 6311-1 du Code du travail.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

- en cas de création d'une entreprise individuelle, le CFE compétent est l'Urssaf ;
- pour une activité commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI) ;
- en cas de création d'une société civile, il s'agit du greffe du tribunal de commerce, ou le greffe du tribunal d'instance dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exploiter un organisme de formation professionnelle, aucune qualification professionnelle n'est requise.

Toutefois, le professionnel doit s'assurer que les membres de son personnel justifient des titres et des qualités nécessaires à l'exercice de l'activité d'enseignement et d'encadrement au sein de chaque formation spécifique.

L'intéressé doit, dès la conclusion de la première convention de formation professionnelle et au plus tard dans les trois mois suivants, procéder à la déclaration de son activité auprès du préfet de région du lieu de son exercice principal (cf. infra « 3°. a. Déclaration d'activité »).

*Pour aller plus loin* : article L. 6351-1 du Code du travail.

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services (LPS) ou Libre établissement (LE))

Le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) n'est soumis à aucune exigence de qualifications professionnelles au même titre que le ressortissant français.

Toutefois lorsque l'organisme qui exerce son activité en France a son siège social au sein d'un État membre de l'UE ou de l'EEE, son exploitant doit désigner un représentant domicilié en France. Le cas échéant, l'exploitant doit procéder à la déclaration d'activité auprès du préfet de la région au sein de laquelle est domicilié ce représentant.

**À noter**

Cette obligation de déclaration n'est pas applicable aux organismes dont le siège social est situé dans un État membre mais exerçant une activité en France à titre temporaire ou occasionnel.

*Pour aller plus loin* : article R. 6351-3 du Code de travail.

### c. Conditions d’honorabilité et sanctions

Le professionnel qui souhaite exploiter un organisme de formation professionnelle ne doit pas avoir fait l'objet d'une condamnation pénale pour des manquements à la probité, l'honneur ou les bonnes mœurs.

*Pour aller plus loin* : article L. 6352-2 du Code du travail.

**Sanctions financières**

Lorsque l'organisme ne dispense pas la formation (totalement ou partiellement) il est tenu de rembourser l'ensemble des sommes versées par le cocontractant.

*Pour aller plus loin* : article L.6354-3 du Code du travail.

**Sanctions pénales**

L'exploitant d'un organisme de formation professionnelle encourt des sanctions pénales en cas de manquements aux dispositions relatives à l'exercice de son activité. Ainsi, il encourt notamment une amende de 4 500 euros dès lors qu'il :

- n'a pas procédé à la déclaration préalable de son activité ou en méconnaissance des dispositions obligatoires (cf. infra « 3°. a. Déclaration d'activité ») ;
- ne peut justifier des titres et qualités des formateurs au sein de son établissement ;
- ne satisfait pas aux conditions d'honorabilité ;
- n'établit pas de règlement intérieur au sein de son établissement ;
- ne désigne pas de commissaire aux comptes alors même qu'il dépasse les seuils fixés ;
- ne pas adresser le bilan pédagogique et financier.

*Pour aller plus loin* : articles L. 6355-1 et suivants du Code du travail.

### d. Quelques particularités de la réglementation de l’activité

**Règlement intérieur applicable aux stagiaires**

Tout organisme de formation professionnelle doit établir un règlement intérieur dans les trois mois suivant le début de l'activité.

Ce règlement intérieur doit déterminer :

- les mesures de sécurité et de santé applicables à l'établissement ;
- les règles applicables en matière de discipline et des sanctions envisageables ;
- les modalités de la représentation des stagiaires pour les actions de formation d'une durée supérieure à cinq cents heures.

**À noter**

Lorsque l'organisme comporte plusieurs établissements, ce règlement intérieur peut être adapté en fonction de chacun.

*Pour aller plus loin* : article L. 6352-3 du Code du travail.

**Obligations comptables**

Selon la nature de son activité l'exploitant doit dresser un bilan comptable chaque année.

Ainsi, les organismes de formation publics doivent tenir un compte séparé de leur activité de formation professionnelle continue. En revanche, les organismes de formation de droit privé doivent :

- dresser un bilan annuel mentionnant le résultat, le compte de résultat et une annexe ;
- désigner un commissaire aux comptes et un suppléant si, à la clôture de l'exercice comptable, ils dépassent les seuils suivants :
  - trois salariés,
  - 153 000 euros de chiffre d'affaires ou de ressources,
  - 230 000 euros de total de bilan.

En outre, l'exploitant de l'organisme de formation professionnelle doit transmettre un bilan pédagogique et financier au plus tard le 30 avril de chaque année au préfet de région (cf. infra « 3°. c. Autorisation(s) post-immatriculation »).

*Pour aller plus loin* : article L. 6352-6 du Code du travail.

**Respect des normes de sécurité et d'accessibilité**

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des Établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

**Convention de formation entre l'organisme de formation et l'acheteur**

L'ensemble des actions de formation sont réalisées selon un programme préétablie. Lors de chaque formation l'organisme doit conclure avec son client de formation un contrat mentionnant :

- l'intitulé, la nature, la durée et les modalités de déroulement de la formation ;
- son prix et le cas échéant, les contributions des personnes publiques.

*Pour aller plus loin* : articles L. 6353-2 et R. 6353-2 du Code du travail.

**Contrat de formation entre une personne physique et l'organisme de formation**

Lorsque l'organisme envisage de dispenser une formation à une personne physique (stagiaire) à titre individuel, il doit conclure avec elle un contrat avant son inscription définitive et le règlement des frais de formation.

Ce contrat doit préciser, sous peine de nullité :

- la nature, la durée, le programme et l'objet de la formation ainsi que ses effectifs ;
- le niveau des qualifications requises pour accéder à cette formation ;
- les conditions de délivrance de la formation, les modalités de contrôle de connaissances et le cas échéant, la nature de la sanction ;
- les qualifications de la personne dispensant la formation ;
- les frais de formation et les modalités de règlement.

**À noter**

Le stagiaire peut se rétracter durant un délai de dix jours à compter de la signature du contrat. Le cas échéant, le stagiaire doit adresser une demande par lettre recommandée avec avis de réception et aucun frais ne pourra lui être réclamé.

*Pour aller plus loin* : articles L. 6353-3 et suivants du Code du travail.

#### Informations du stagiaire

L'organisme doit communiquer au stagiaire de la formation l'ensemble des informations relatives :

- au programme, aux objectifs et aux modalités d'évaluation de la formation ;
- la liste des formateurs ;
- les coordonnées des relations avec le stagiaire ;
- le règlement intérieur applicable à la formation.

*Pour aller plus loin* : articles L. 6353-8 et suivants du Code du travail.

## 3°. Démarches et formalités d’installation

### a. Déclaration d'activité

**Autorité compétente**

L'exploitant de l'organisme de formation professionnelle doit adresser sa déclaration au préfet de la région au sein de laquelle se trouve son principal établissement.

**Pièces justificatives**

La déclaration doit être accompagnée des documents suivants :

- une photocopie du justificatif d'attribution du numéro Siren ;
- le bulletin n° 3 du casier judiciaire de l'exploitant datant de moins d'un mois ;
- une copie de la première convention de formation professionnelle ou du premier contrat conclu où à défaut la preuve qu'une première réalisation est prévue ;
- le cas échéant, un justificatif d'inscription sur la [liste publique des organismes chargés de réaliser les bilans de compétences](https://www.data.gouv.fr/fr/datasets/liste-publique-des-organismes-de-formation-l-6351-7-1-du-code-du-travail/) ;
- une copie du programme de formation et les listes des intervenants mentionnant leurs titres, leurs qualités ainsi que la prestation et le lien contractuel qui les lient à l'organisme de formation.

**Procédure**

Le préfet de région accuse réception de la demande dans les trente jours suivants son dépôt et adresse un récépissé au demandeur. Il procède ensuite à l'enregistrement de l'activité de l'exploitant.

**À noter**

L'absence de réponse de la part du préfet au delà d'un délai de trente jours vaut rejet de la demande. En outre, une déclaration rectificative peut être apportée dans un délai de trente jours auprès du préfet de région en vue de modifier la déclaration d'activité.

*Pour aller plus loin* : articles R. 6351-1 à R. 6351-7 du Code du travail.

### b. Formalités de déclaration de l’entreprise

Selon la nature de son activité le professionnel doit ou non s'immatriculer au registre du commerce et des sociétés (RSC) ou s'immatriculer auprès de l'Urssaf.

### c. Autorisation(s) post-immatriculation

#### Adresser un bilan pédagogique et financier

Avant le 30 avril de chaque année, le prestataire de formation doit adresser ce bilan au préfet de région.

Ce bilan doit être accompagné du bilan de l'exercice comptable et mentionner :

- les activités de formations effectuées au cours de l'exercice comptable ;
- le nombre de stagiaires accueillis ;
- le nombre d'heures-stagiaires et d'heures de formation ainsi que leur volume horaire, la nature et le niveau des formations ;
- la répartition des fonds reçus et le montant des factures émises ;
- les données comptables relatives à la formation professionnelle continue ;
- l'ensemble des produits perçus grâce au placement des fonds reçus.

**À noter**

Le préfet de région peut également exiger la liste des prestations de formation à accomplir.

*Pour aller plus loin* : articles L. 6352-11 et R. 6352-22 et suivants du Code du travail.