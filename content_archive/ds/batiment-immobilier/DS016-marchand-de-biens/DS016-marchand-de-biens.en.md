﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS016" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Property trader" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="property-trader" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/property-trader.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="property-trader" -->
<!-- var(translation)="Auto" -->


Property trader
=================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The property dealer is a professional specializing in the purchase and resale of real estate, building land, commercial funds or shares or shares of real estate companies.

Working in collaboration with architects and master technicians, he rehabilitates the property and resells it after adding value to it.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for civil societies, this is the registry of the Commercial Court;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Professional qualifications
----------------------------------------

### a. Professional qualifications

No specific diploma is required to practise as a goods dealer. However, it is essential to know the regulations in real estate.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

A national of a Member State of the European Union (EU) or party to the Agreement on the European Economic Area (EEA) is not subject to any qualification or certification requirements, as are the French.

### c. Some peculiarities of the regulation of the activity

#### Obligation to take out property and casualty insurance and 10-year liability insurance

As soon as the property dealer has work done on the property he intends to resell, he is required to take out so-called "damage damage" insurance to repair the damage that could result.

In addition, at the opening of his site, he will have to be able to justify that he has taken out a ten-year liability insurance.

*To go further* Articles L. 242-1 and L. 241-1 of the Insurance Code.

#### Rented properties

The property dealer who wishes to purchase a building in which tenants live must continue their rental lease. Similarly, the seller of the building will not be able to evict them when he sells his property.

*To go further* Article 1743 of the Civil Code.

#### Renovation of a building for resale

When the merchant of goods undertakes to carry out work of great importance on the building, he must obtain prior authorizations in the town hall before carrying them out. However, when it comes to smaller works such as single-storey terrace, swimming pool of less than ten square metres or walls less than two metres high, it is exempt.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the contractor must or does not register with the Register of Trade and Companies (RCS). It is advisable to refer to the "Reporting Formalities of a Commercial Company" for more information.

### b. If necessary, register the company's statutes

The merchant of goods must, once the company's statutes are dated and signed, register them with the Corporate Tax Office (IES) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building when the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*To go further* Section 635 of the General Tax Code.

