﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS016" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Marchand de biens" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="marchand-de-biens" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/marchand-de-biens.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="marchand-de-biens" -->
<!-- var(translation)="None" -->

# Marchand de biens

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l'activité

### a. Définition

Le marchand de biens est un professionnel spécialisé dans l'achat et la revente de biens immobiliers, de terrains à bâtir, de fonds de commerce ou encore d'actions ou parts de sociétés immobilières.

Travaillant en collaboration avec des architectes et des techniciens maîtres d’œuvre, il réhabilite les biens et les revend après y avoir apporté une plus-value.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d'industrie (CCI) ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Qualifications professionnelles

### a. Qualifications professionnelles

Aucun diplôme spécifique n'est requis pour exercer la profession de marchand de biens. Toutefois, il est indispensable de connaître la réglementation en matière immobilière.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) n'est soumis à aucune condition de diplôme ou de certification, au même titre que les Français.

### c. Quelques particularités de la réglementation de l'activité

#### Obligation de souscrire une assurance dommages-ouvrage et une assurance de responsabilité décennale

Dès lors que le marchand de biens fait réaliser des travaux sur le bien immobilier qu'il compte revendre, il est tenu de souscrire une assurance dite « dommages-ouvrage » pour réparer les dommages qui pourraient en découler.

De plus, à l'ouverture de son chantier, il devra être en mesure de justifier qu'il a souscrit une assurance de responsabilité décennale.

*Pour aller plus loin* : articles L. 242-1 et L. 241-1 du Code des assurances.

#### Biens immobiliers loués

Le marchand de biens qui souhaite acheter un immeuble dans lequel des locataires habitent doit poursuivre leur bail locatif. De même, le vendeur de l'immeuble ne pourra pas les expulser lorsqu'il vend son bien.

*Pour aller plus loin* : article 1743 du Code civil.

#### Rénovation d'un immeuble en vue de sa revente

Lorsque le marchand de biens entreprend de faire des travaux de grande importance sur l'immeuble, il doit obligatoirement obtenir les autorisations préalables en mairie avant de les réaliser. Toutefois, lorsqu'il s'agit de travaux de moindre importance de type terrasse de plain-pied, piscine de moins de dix mètres carrés ou murs dont la hauteur est inférieure à deux mètres, il en est dispensé.

## 3°. Démarches et formalités d'installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit ou non s’immatriculer au registre du commerce et des sociétés (RCS).

### b. Le cas échéant, enregistrer les statuts de la société

Le marchand de biens doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

**Autorité compétente**

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

**Pièces justificatives**

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.