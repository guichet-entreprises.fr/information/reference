﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Le référentiel d'information de Guichet Entreprises est mis à disposition
# selon les termes de la licence Creative Commons Attribution - Pas de
# Modification 4.0 International.

# Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
# suivante :
# http://creativecommons.org/licenses/by-nd/4.0/
# ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
# Mountain View, California, 94041, USA.
# -----------------------------------------------------------------------------

import logging
import sys
import os
import re
import shutil
import git
import pathlib
import time

from pymdtools import common
from pymdtools import instruction
from pymdtools import filetools
from pymdtools import mistunege as mistune
from pymdtools import mdfile
from pymdtools import mdrender
from pymdtools import convert_md_to_pdf


from . import ref
from . import folder
from . import normalize

# -----------------------------------------------------------------------------
def short_name(name):
    name = common.str_to_ascii(name)
    name = name.replace("'", " ")
    return common.slugify(common.limit_str(name, 30, ' ', min_last_word=3))

# -----------------------------------------------------------------------------
def url_name(name):
    name = common.str_to_ascii(name)
    name = name.replace("'", " ")
    return common.slugify(common.limit_str(name, 60, ' ', min_last_word=3))

# -----------------------------------------------------------------------------
def file_in_repo(repo, file_path):
    # -------------------------------------------------------------------------
    '''
    repo is a gitPython Repo object
    path_to_file is the full path to the file from the repository root
    returns true if file is found in the repo at the specified path,
    false otherwise
    '''
    # -------------------------------------------------------------------------
    rel_file_path = os.path.relpath(file_path, repo.working_tree_dir)
    pathdir = os.path.dirname(rel_file_path)

    # Build up reference to desired repo path
    rsub = repo.head.commit.tree
    for path_element in pathdir.split(os.path.sep):
        try:
            rsub = rsub[path_element]
        except KeyError:
            return False

    rel_file_path = rel_file_path.replace("\\", "/")

    return rel_file_path in rsub

# -----------------------------------------------------------------------------
def add_file_to_repo(repo, file_path):
    # -------------------------------------------------------------------------
    '''
    Add fileto the repo
    '''
    # -------------------------------------------------------------------------
    rel_file_path = os.path.relpath(file_path, repo.working_tree_dir)
    repo.index.add([rel_file_path])

# -----------------------------------------------------------------------------
def git_repo():
    main_folder = os.path.join(__get_this_folder(), "..")
    main_folder = common.set_correct_path(main_folder)
    return git.Repo(main_folder)

# -----------------------------------------------------------------------------
def move_file(from_filename, to_filename):
    logging.info("Move from %s ---> %s", from_filename, to_filename)
    repo = git_repo()

    if file_in_repo(repo, from_filename):
        repo.index.move((from_filename, to_filename))
    else:
        os.rename(from_filename, to_filename)


# -----------------------------------------------------------------------------
# check and change the include file in the header
#
# @return the MD.
# -----------------------------------------------------------------------------
def check_newline(md_content, unused_key=None, unused_lang=None):
    result = md_content.content
    result = result.replace("\r\n", "\n")
    result = result.replace("\n\n\n", "\n\n")
    result = result.replace("\n\n\n\n", "\n\n")
    result = result.replace("\n\n\n\n\n", "\n\n")
    result = result.replace("->\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n\n\n\n<!-", "->\n<!-")
    md_content.content = result

    return md_content

# -----------------------------------------------------------------------------
# check and change the include file in the header
#
# @return the MD.
# -----------------------------------------------------------------------------
def code_ape(md_content, unused_key=None, unused_lang=None):
    toc = md_content.toc
    __ape_re__ = r"<li><a.*>(?P<title>.*APE.*)</a></li>"
    match_begin = re.search(__ape_re__, toc)

    # finish if no match
    if not match_begin:
        print("APE Title not found")
        return md_content

    # There is a match
    title = match_begin.group('title')
    ape_part = r"(?P<part>###\s" + re.escape(title) + r".*)\s##\s2"
    ape_part = re.compile(ape_part, re.MULTILINE | re.DOTALL)
    match_part = re.search(ape_part, md_content.content)
    if not match_part:
        print("APE Part not found  -> nothing done")
        return md_content
    part = match_part.group('part')

    verif = re.search("##", part[3:])
    if verif:
        print("WARNING bad part of APE -> nothing done")
        return md_content

    md_content.content = md_content.content.replace(part, "")

    return md_content

# -----------------------------------------------------------------------------
# check and change the include file in the header
#
# @return the MD.
# -----------------------------------------------------------------------------
def header_ge(md_content, unused_key=None, unused_lang=None):
    md_content.set_include_file("ge.txt")
    md_content.del_include_file("license.txt")
    md_content.del_include_file("license-short.txt")
    md_content.sel_include_file("license-short.%s.txt" % md_content['lang'])
    return md_content

# -----------------------------------------------------------------------------
# check and change the include file in the header
#
# @return the MD.
# -----------------------------------------------------------------------------
def sanitize_description(md_content):
    lang = md_content['lang'].lower()[:2]
    main_domain = md_content['domain'] if 'domain' in md_content else ""
    main_category = md_content['category']

    category = {
        "Directive Qualification Professionnelle": {
            "url-domain": "www.guichet-qualifications.fr",
            "url-short": "dqp",
        },
        "Directive Services": {
            "url-domain": "www.guichet-entreprises.fr",
            "url-short": "ds",
        },
    }

    target_value = {
        "author": "Guichet Entreprises",
        "lang": lang,
        "category": main_category,
        "domain": main_domain,
        "url-name": "index",
        "url-domain": category[main_category]["url-domain"],
        "category-short": category[main_category]["url-short"],
        "domain-short": short_name(main_domain), }

    target_value["url"] = "https://%(url-domain)s/"\
        "%(lang)s/%(category-short)s/" \
        "%(domain-short)s/%(url-name)s.html" % target_value

    check_key = ["url"]

    for var in check_key:
        if var not in md_content:
            print("Add the key=%s: value=%s  " % (var, target_value[var]))
            md_content[var] = target_value[var]
            continue

        if md_content[var] == target_value[var]:
            continue

        print("Problem with the key=%s: value=%s  " % (var, md_content[var]))
        print("                 --> should be=%s  " % target_value[var])
        md_content[var] = target_value[var]

    return md_content


# -----------------------------------------------------------------------------
#
# -----------------------------------------------------------------------------
def check_disclaimer(md_content, unused_key=None, unused_lang=None):
    content = md_content.content
    references = instruction.refs_in_md_text(content)

    lang = md_content['lang'].lower()
    ref_disclaimer_pro = "disclaimer-eu-" + lang
    ref_disclaimer_auto = "disclaimer-trans-" + lang

    if md_content['translation'].lower() == "none":
        if ref_disclaimer_pro not in references and \
                ref_disclaimer_auto not in references:
            logging.info("    --> everything is OK")
            return md_content
        logging.warning("    --> Bad reference")
        return md_content

    refid = ""
    if md_content['translation'].lower()[0:3] == "pro":
        refid = ref_disclaimer_pro
    else:
        refid = ref_disclaimer_auto

    logging.info("    ref is %s", refid)

    if refid in references:
        logging.info("    --> already done")
        return md_content

    match = instruction.get_title_from_md_text(content, return_match=True)
    match = match.group(0).lstrip()
    content = content.replace(match, match + "\n<!-- begin-include(%s) -->"
                              "\n<!-- end-include -->\n" % refid, 1)
    logging.info("    --> done")
    md_content.content = content
    return md_content

# -----------------------------------------------------------------------------
# check title
#
# @return the MD.
# -----------------------------------------------------------------------------
def check_last_update(md_content, unused_key=None, lang=None):
    if 'last-update' not in md_content:
        logging.info("    --> Need to add, the last update")
        md_content['last-update'] = ""

    if len(md_content['last-update'].strip()) == 0:
        file_date = os.path.getmtime(md_content.full_filename)
        stamp = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(file_date))
        md_content['last-update'] = stamp
        logging.info("    --> %s", stamp)

    if md_content.filename.startswith('__'):
        return md_content

    __var_re__ = r"<!--\s+begin-var\(\s*last-update\s*\)\s+-->"
    match = re.search(__var_re__, md_content.content)

    if match:
        return md_content

    logging.info("    --> Add the text in the md")

    text = {
        'bg': 'Последна актуализация:', 'cs': 'Poslední aktualizace:',
        'da': 'Seneste opdatering:', 'de': 'Neueste Aktualisierung:',
        'et': 'Viimane uuendus:', 'el': 'Τελευταία ενημέρωση:',
        'en': 'Latest update:', 'es': 'Actualización más reciente:',
        'fr': 'Dernière mise à jour : ', 'ga': 'Nuashonrú is déanaí:',
        'hr': 'Najnovije ažuriranje:', 'it': 'Ultimo aggiornamento:',
        'lv': 'Jaunākais atjauninājums:', 'lt': 'Naujausias atnaujinimas:',
        'hu': 'Legújabb frissítés:', 'mt': 'L-a ħħar aġġornament:',
        'nl': 'Laatste update:', 'pl': 'Najnowsza aktualizacja:',
        'pt': 'Última atualização:', 'ro': 'Cea mai recentă actualizare:',
        'sk': 'Posledná aktualizácia:', 'sl': 'Najnovejša posodobitev:',
        'fi': 'Viimeisin päivitys:', 'sv': 'Senaste uppdateringen:',
        'tr': 'Son güncelleme:', 'ru': 'Последнее обновление:',
    }

    if lang not in text:
        lang = "fr"

    add_text = \
        "\n\n%s <!-- begin-var(last-update) -->" \
        "<!-- end-var -->\n" % text[lang]

    match = instruction.get_title_from_md_text(md_content.content,
                                               return_match=True)
    match = str(match.group(0).strip())
    md_content.content = md_content.content.replace(match,
                                                    match + add_text, 1)

    return md_content


# -----------------------------------------------------------------------------
# check and change the the filename
#
# @return the MD.
# -----------------------------------------------------------------------------
def correct_filename(md_content, key, lang, lang_ref="fr"):
    data = ref.get_ref(key, lang=lang)
    data_ref = ref.get_ref(key, lang=lang_ref)
    main_folder = os.path.join(__get_this_folder(), "..")
    main_folder = common.set_correct_path(main_folder)

    if md_content.full_filename != data['filename']:
        logging.error("You need to refresh the reference data")
        logging.error("   %s != %s", md_content.full_filename,
                      data['filename'])
        return md_content

    target_filename = "%s-%s.%s.md" % (data['key'],
                                       data_ref['title-short'], lang)
    if md_content.filename != target_filename:
        print("Problem with the filename=%s  " % md_content.filename)
        print("            --> should be=%s  " % target_filename)
        from_filename = md_content.full_filename
        to_filename = os.path.join(
            md_content.filename_path, target_filename)

        # move_file(from_filename, to_filename)
        # md_content.full_filename = to_filename

    return md_content

# -----------------------------------------------------------------------------
# check and change the the filename
#
# @return the MD.
# -----------------------------------------------------------------------------
def check_sources(md_content, key, unused_lang, lang_ref="fr"):
    # data_ref = ref.get_ref(key, lang=lang_ref)
    filename = md_content.full_filename
    src_filename = os.path.splitext(os.path.splitext(filename)[0])[0] \
        + ".sources.md"

    if os.path.isfile(src_filename):
        return md_content

    print("Problem with %s" % src_filename)

    list_src = list(pathlib.Path(
        md_content.filename_path).glob('*.sources.md'))

    print("Found %d possible sources" % len(list_src))
    if len(list_src) == 0:
        print("Create sources files")
        content = "# ** %s **\n\n" % md_content.title
        common.set_file_content(src_filename, content)
        return md_content

    if len(list_src) > 1:
        print("Can not work")
        return md_content

    real_src = list_src[0]
    move_file(real_src, src_filename)

    return md_content

# -----------------------------------------------------------------------------
# check and change the the filename
#
# @return the MD.
# -----------------------------------------------------------------------------
def check_in_git(md_content, unused_key, unused_lang):
    filename = md_content.full_filename
    src_filename = os.path.splitext(os.path.splitext(filename)[0])[0] \
        + ".sources.md"

    if not file_in_repo(git_repo(), filename):
        print("--> PROBLEM %s is not in the repo" % filename)
        add_file_to_repo(git_repo(), filename)

    if os.path.isfile(src_filename):
        if not file_in_repo(git_repo(), src_filename):
            print("--> PROBLEM %s is not in the repo" % src_filename)
            print("--> ADD %s to the repo" % src_filename)
            add_file_to_repo(git_repo(), src_filename)

    return md_content

# -----------------------------------------------------------------------------
# check and change from another ref
#
# @return the check function.
# -----------------------------------------------------------------------------
def check_from_ref(reference):

    exclude_key = ['filename']

    def the_check_fun(md_content, key, lang):
        if key not in reference:
            return md_content
        if lang not in reference[key]:
            return md_content

        data = reference[key][lang]

        for k in data:
            if k not in exclude_key and k in md_content:
                if 'mymemory' in data[k].lower():
                    continue
                if data[k] != md_content[k]:
                    print("--> var=%s is        '%s'" % (k, md_content[k]))
                    print("--> var=%s should be  '%s'" % (k, data[k]))
                    md_content[k] = data[k]

        return md_content

    return the_check_fun

# -----------------------------------------------------------------------------
# check and change the include file in the header
#
# @return the MD.
# -----------------------------------------------------------------------------
def sanitize_var_ge(md_content, key, lang="fr"):
    key = folder.check_key(key)
    lang = lang.lower()[:2]
    main_title = md_content.title
    main_domain = md_content['domain']
    main_category = key.upper()[:2]

    category = {
        "long": {
            "DQ": "Directive Qualification Professionnelle",
            "DS": "Directive Services",
        },
        "short": {"DQ": "dqp", "DS": "ds"},
        "url-short": {"DQ": "gq", "DS": "ge"},
        "url": {
            "DQ": "www.guichet-qualifications.fr",
            "DS": "www.guichet-entreprises.fr"},
    }

    target_value = {
        "key": key,
        "author": "Guichet Entreprises",
        "lang": lang,
        "category": category["long"][main_category],
        "domain": main_domain,
        "title": main_title,
        "url-name": url_name(main_title),
        "url-domain": category["url"][main_category],
        "url-domain-short": category["url-short"][main_category],
        "category-short": category["short"][main_category],
        "domain-short": short_name(main_domain),
        "title-short": short_name(main_title),
    }

    target_value["url"] = "https://%(url-domain)s/"\
        "%(lang)s/%(category-short)s/" \
        "%(domain-short)s/%(url-name)s.html" % target_value

    for var in target_value:
        if var not in md_content:
            print("Add the key=%s: value=%s  " % (var, target_value[var]))
            md_content[var] = target_value[var]
            continue

        if md_content[var] == target_value[var]:
            continue

        print("Problem with the key=%s: value=%s  " % (var, md_content[var]))
        print("                 --> should be=%s  " % target_value[var])
        md_content[var] = target_value[var]

    if lang == 'fr':
        if 'translation' not in md_content:
            print("        Set translation none")
            md_content['translation'] = "None"
    # else:
    #     if 'translation' not in md_content:
    #         print("        Set translation pro")
    #         md_content['translation'] = "Pro"

    return md_content


# -----------------------------------------------------------------------------
# get min max from TOC
# -----------------------------------------------------------------------------
def get_min_max(toc, current=None):
    result = current
    if isinstance(toc, list):
        for item in toc:
            result = get_min_max(item, result)
        return result

    key_level = 'level'
    key_children = 'children'

    if key_level in toc:
        if result is None:
            result = [toc[key_level], toc[key_level]]

        if result[0] > toc[key_level]:
            result[0] = toc[key_level]

        if result[1] < toc[key_level]:
            result[1] = toc[key_level]

    if key_children in toc:
        result = get_min_max(toc[key_children], result)

    return result

# -----------------------------------------------------------------------------
# render to test the TOC
# -----------------------------------------------------------------------------
class CheckToc(mdrender.MdRenderer):

    ###########################################################################
    # Initialisation
    ###########################################################################
    def __init__(self, toc, **kwargs):
        mistune.Renderer.__init__(self, **kwargs)
        self.__toc_stack = [toc]
        min_max = get_min_max(toc)
        self.__min_level = min_max[0]
        self.__max_level = min_max[1]
        print("min %s max %s" % (self.__min_level, self.__max_level))

    ###########################################################################
    # Get the level of header
    # @return the current level
    ###########################################################################
    @property
    def current_element(self):
        if len(self.toc_stack) == 0:
            return None
        if len(self.toc_stack[-1]) == 0:
            return None
        return self.toc_stack[-1][0]

    ###########################################################################
    # Get the level of header
    # @return the current level
    ###########################################################################
    @property
    def toc_stack(self):
        return self.__toc_stack

    ###########################################################################
    # set the new level
    # @param value The value to set
    ###########################################################################
    def next_item(self):
        if self.current_element is None:
            return

        children = self.current_element['children']

        self.toc_stack[-1].pop(0)
        self.__toc_stack.append(children)

        while len(self.toc_stack) > 0 and len(self.toc_stack[-1]) == 0:
            self.__toc_stack.pop()

    ###########################################################################
    # Check the header
    ###########################################################################
    def header(self, text, level, raw=None):
        if level < self.__min_level or \
                self.__max_level < level:
            return super().header(text, level, raw)

        result = "%03d - '%s'" % (level, text)
        print(">>> %s" % result)

        current_el = self.current_element
        if current_el is None:
            print("    No more element")
            return result

        if level != current_el['level']:
            print("    level %03d should be %03d" % (level,
                                                     current_el['level']))

        if text != current_el['name']:
            print("         name '%s'\n"
                  "    should be '%s'" % (text, current_el['name']))

        self.next_item()

        return result

# -----------------------------------------------------------------------------
# render to test the TOC
# -----------------------------------------------------------------------------
class DeleteNum(mdrender.MdRenderer):

    ###########################################################################
    # Check the header
    ###########################################################################
    def header(self, text, level, raw=None):
        (text, unused_) = re.subn(r"(([1-9]\°\.\s+)|([a-z]\.\s+))",
                                  r"", text)
        return super().header(text, level, raw)

# -----------------------------------------------------------------------------
# check title
#
# @return the MD.
# -----------------------------------------------------------------------------
def check_toc(md_content, unused_key, unused_lang):
    # check_toc_renderer = CheckToc()
    # renderer = mistune.Renderer(use_xhtml=True)
    # use this renderer instance
    start_folder = os.path.split(__get_this_filename())[0]
    content = filetools.get_template_file("ds.md", start_folder=start_folder)
    toc = mdfile.MarkdownContent(content=content).toc

    markdown = mistune.Markdown(renderer=CheckToc(toc))
    markdown(md_content.content)

    # check_toc(str(md_content.content))
    # toc = md_content.toc
    # print(toc)
    # for var in the_ref:
    #     print("%s=%s" % (var, the_ref[var]))

    return md_content

# -----------------------------------------------------------------------------
# check title
#
# @return the MD.
# -----------------------------------------------------------------------------
def delete_num(md_content, unused_key, unused_lang):
    markdown = mistune.Markdown(renderer=DeleteNum())
    md_content.content = markdown(md_content.content)
    return md_content

# -----------------------------------------------------------------------------
# check title
#
# @return the MD.
# -----------------------------------------------------------------------------
def check_num(md_content, unused_key, unused_lang):
    __link_re__ = r"\«\s+(([1-9]\°\.\s+)|([a-z]\.\s+))+" \
        r"(?P<content>[\s\S]*?)\s+\»"

    result = md_content.content
    current_text = result

    # search the var
    match_var = re.search(__link_re__, current_text)

    while match_var is not None:
        ref_link = match_var.group('content')
        print('Find the refence "%s"' % ref_link)

        result += current_text[0:match_var.end(0)]
        current_text = current_text[match_var.end(0):]
        match_var = re.search(__link_re__, current_text)

    return md_content

# -----------------------------------------------------------------------------
# check title
#
# @return the MD.
# -----------------------------------------------------------------------------
def correct_md(md_content, unused_key, unused_lang):
    md_content.content = normalize.correct_markdown_text(md_content.content)
    return md_content

# -----------------------------------------------------------------------------
# check title
#
# @return the MD.
# -----------------------------------------------------------------------------
def beautify(md_content, unused_key, unused_lang):
    md_content.beautify()
    # text = md_content.content
    # (text, unused_) = re.subn(r":\n\n", r":\n", text)
    # text = text.rstrip()
    # md_content.content = text
    return md_content

# -----------------------------------------------------------------------------
# check title
#
# @return the MD.
# -----------------------------------------------------------------------------
def convert_pdf(md_content, unused_key, unused_lang):
    pdf_filename = convert_md_to_pdf(md_content.full_filename)
    logging.info("create %s", pdf_filename)
    return md_content

# -----------------------------------------------------------------------------
# check title
#
# @return the MD.
# -----------------------------------------------------------------------------
def find_key_from_git(key, git_folder, category, domain, name):
    result = {}
    re_path = r"(?P<category>[a-zA-Z0-9-]+)[/|\\]" \
        r"(?P<domain>[a-zA-Z0-9-]+)[/|\\]" \
        r"(?P<key>[A-Z]+[0-9]+)-(?P<name>[a-zA-Z0-9-]+)"
    repo = git.Repo(git_folder)

    result['files'] = []
    result['folder'] = None
    result['path_ref'] = None

    for entrie in repo.index.entries:
        match = re.search(re_path, entrie[0])
        diff = None
        if match:
            diff = match.group('name').upper() != name.upper() or \
                match.group('domain').upper() != domain.upper() or \
                match.group('category').upper() != category.upper()
        if match and diff and match.group('key').upper() == key.upper():
            result['files'].append(entrie)
            this_folder = entrie[0][:match.end('name')]
            this_path_ref = entrie[0][:match.start('category')]

            if result['folder'] is None:
                result['folder'] = this_folder

            if result['folder'] != this_folder:
                logging.info("too many folder '%s' '%s'",
                             result['folder'], this_folder)
                raise Exception("too many folder '%s' '%s'" %
                                (result['folder'], this_folder))

            if result['path_ref'] is None:
                result['path_ref'] = this_path_ref

            if result['path_ref'] != this_path_ref:
                logging.info("too many folder '%s' '%s'",
                             result['path_ref'], this_path_ref)
                raise Exception("too many folder '%s' '%s'" %
                                (result['path_ref'], this_path_ref))

    return result

# -----------------------------------------------------------------------------
# check title
#
# @return the MD.
# -----------------------------------------------------------------------------
def check_key_name(key, git_folder, ref_folder):
    logging.info("Check the name for %s", key)
    key_data = ref.get_info_ref(key, lang=None)
    logging.info("path=%s", key_data['path'])

    dest = {}
    dest['folder'] = common.set_correct_path(
        os.path.join(ref_folder, key_data['path']))
    common.check_create_folder(dest['folder'])

    re_path = r"(?P<category>[a-zA-Z0-9-]+)[/|\\]" \
        r"(?P<domain>[a-zA-Z0-9-]+)[/|\\]" \
        r"(?P<key>[A-Z]+[0-9]+)-(?P<name>[a-zA-Z0-9-]+)"

    dest_match = re.search(re_path, key_data['path'])

    if not dest_match:
        logging.info("    CAN NOT FIND THE DOMAIN/NAME")
        return

    dest['category'] = dest_match.group('category')
    dest['domain'] = dest_match.group('domain')
    dest['name'] = dest_match.group('name')

    git_entries = find_key_from_git(key, git_folder,
                                    dest['category'],
                                    dest['domain'],
                                    dest['name'])

    if git_entries['folder'] is None:
        logging.info("            --> End")
        return

    logging.info("From '%s' To '%s'", git_entries['folder'], key_data['path'])

    # move git files
    repo = git.Repo(git_folder)
    for entrie in git_entries['files']:
        match = re.search(re_path, entrie[0])
        from_filename = common.set_correct_path(
            os.path.join(repo.working_tree_dir, entrie[0]))
        to_filename = os.path.split(entrie[0])[1].replace(
            match.group('name'), dest['name'])
        to_filename = os.path.join(ref_folder, key_data['path'],
                                   to_filename)
        logging.info("Move from %s ---> %s", from_filename, to_filename)
        result = repo.index.move((from_filename, to_filename))
        # logging.info("Move from %s ---> %s", result[0], result[1])

    # copy other files
    common.copytree(git_entries['folder'], dest['folder'])
    shutil.rmtree(git_entries['folder'])

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
# -----------------------------------------------------------------------------
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result
