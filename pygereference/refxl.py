#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Le référentiel d'information de Guichet Entreprises est mis à disposition
# selon les termes de la licence Creative Commons Attribution - Pas de
# Modification 4.0 International.

# Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
# suivante :
# http://creativecommons.org/licenses/by-nd/4.0/
# ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
# Mountain View, California, 94041, USA.
# -----------------------------------------------------------------------------

import logging
import sys
import os
import openpyxl

from pymdtools import common
from . import ref

# -----------------------------------------------------------------------------
# Find a column with the name
#
# @param column_name name of the column
# @param wsheet the worksheet
# @return the coordinate (raw, col)
# -----------------------------------------------------------------------------
def find_column(column_name, wsheet):
    col_found = -1
    raw_found = -1
    raw_max = 5
    col_max = 50
    for raw in range(0, raw_max):
        if raw_found == -1:
            for col in range(0, col_max):
                tested_value = wsheet[xlcoord(raw, col)].value
                if tested_value is not None \
                        and tested_value.lower() == column_name.lower():
                    (raw_found, col_found) = (raw, col)

    if raw_found == -1:
        return None

    return (raw_found, col_found)

# -----------------------------------------------------------------------------
# Get the worksheet from the excel workbook
#
# @param ws_name name of the worksheet
# @param wb the workbook
# @return the ws found
# -----------------------------------------------------------------------------
def ws_from_wb(ws_name, wbook):
    if ws_name not in wbook.get_sheet_names():
        logging.error('%s is not a worksheet of the referential', ws_name)
        raise RuntimeError(
            '%s is not a worksheet of the referential' % ws_name)
    return wbook[ws_name]

# -----------------------------------------------------------------------------
# Get the worksheet position from position row_num,col_num
#
# @param row_num the position number start at 0
# @param col_num the position number start at 0
# @return the coordinate start at "A1"
# -----------------------------------------------------------------------------
def xlcoord(row_num, col_num):
    row_num += 1
    col_num += 1

    col_str = ''

    while col_num:
        remainder = col_num % 26

        if remainder == 0:
            remainder = 26

        # Convert the remainder to a character.
        col_letter = chr(ord('A') + remainder - 1)

        # Accumulate the column letters, right to left.
        col_str = col_letter + col_str

        # Get the next order of magnitude.
        col_num = int((col_num - 1) / 26)

    return col_str + str(row_num)


# -----------------------------------------------------------------------------
# update the list of columns
#
# @param ws_ref the excel worksheet
# @param cols dict for title -> col
# @param title_raw the the raw for the titles
# @return the cols updated
# -----------------------------------------------------------------------------
def find_all_cols(ws_ref, cols, title_raw):
    current_col = 0
    while ws_ref[xlcoord(title_raw, current_col)].value is not None:
        key = ws_ref[xlcoord(title_raw, current_col)].value.lower()
        cols[key] = current_col
        current_col += 1

    return cols


# -----------------------------------------------------------------------------
# update the title raw
#
# @param titles the  list of the new title
# @param cols dict for title -> col
# @param title_raw the the raw for the titles
# @param ws_ref the excel worksheet
# @return the cols updated
# -----------------------------------------------------------------------------
def update_title_raw(ws_ref, titles, cols, title_raw):
    current_col = max(cols.values()) + 1
    for title in titles:
        if title not in cols:
            col = find_column(title, ws_ref)
            if col is None:
                col = current_col
                current_col += 1
            ws_ref[xlcoord(title_raw, col)].value = title
            cols[title] = col
    return cols


# -----------------------------------------------------------------------------
# Add table to the worksheet
#
# @param  ws the worksheet
# @param  table_name the table name
# @param  ref_table the table coordonate (in the excel format)
# @return the worksheet
# -----------------------------------------------------------------------------
def add_table_to_ws(wsheet, table_name, ref_table):
    for table in wsheet._tables:
        if table.displayName == table_name:
            logging.info('Table %s adjust', table_name)
            table.ref = ref_table
            return wsheet

    logging.info('Table %s create', table_name)
    tab = openpyxl.worksheet.table.Table(displayName=table_name, ref=ref_table)
    style = openpyxl.worksheet.table.TableStyleInfo(name="TableStyleMedium9",
                                                    showFirstColumn=False,
                                                    showLastColumn=False,
                                                    showRowStripes=True,
                                                    showColumnStripes=True)
    tab.tableStyleInfo = style
    wsheet.add_table(tab)
    return wsheet


# -----------------------------------------------------------------------------
# fill the worksheet with the data
#
# @param ws_ref the excel worksheet
# @param ref_info the info referential
# -----------------------------------------------------------------------------
def fill_data(ws_ref, ref_info):
    logging.info('Put data in worksapce')
    rows = {}
    cols = {}
    cols_name = ["key", "lang"]
    hidden_cols = ["filename"]

    logging.info('Find cols')
    for name in cols_name:
        (rows[name], cols[name]) = find_column(name, ws_ref)
        logging.info('%10s= (raw %02d, col %02d)',
                     name, rows[name], cols[name])
    title_row = set(rows.values())
    if len(title_row) != 1:
        logging.error('Too many rows for column title')
        raise RuntimeError('Too many rows for column title')
    title_row = title_row.pop()

    cols = find_all_cols(ws_ref, cols, title_row)

    current_row = title_row + 1

    for key in ref_info:
        for lang in ref_info[key]:
            data = ref_info[key][lang]
            for col_name in hidden_cols:
                if col_name in data:
                    del data[col_name]
            cols = update_title_raw(ws_ref, data.keys(), cols, title_row)

            for title, value in data.items():
                ws_ref[xlcoord(current_row, cols[title])].value = value

            current_row += 1

    # define table
    ref_table = "%s:%s" % (xlcoord(title_row, min(cols.values())),
                           xlcoord(current_row - 1, max(cols.values())))
    add_table_to_ws(ws_ref, table_name="reference", ref_table=ref_table)

    return ws_ref


# -----------------------------------------------------------------------------
# Create an excel file from all reference files
#
# @param xl_filename the excel output filename
# @param ref_info the info referential
# -----------------------------------------------------------------------------
def create(xl_filename, ref_info=None,
           overwrite=True, backup=True, refresh=False):
    xl_filename = common.set_correct_path(xl_filename)
    if os.path.isfile(xl_filename):
        if not overwrite:
            logging.error(
                '%s can not be overwritten (it is a file)', xl_filename)
            raise RuntimeError(
                '%s can not be overwritten (it is a file)' % xl_filename)
        if backup:
            common.create_backup(xl_filename)
        os.remove(xl_filename)

    if ref_info is None:
        ref_info = ref.get_ref(refresh=refresh)

    logging.debug('Load the template')
    wb_template = openpyxl.load_workbook(
        filename=os.path.join(__get_this_folder(), "templates", "ref.xlsx"))
    ws_ref = ws_from_wb("Ref", wb_template)

    # all the job is here
    fill_data(ws_ref, ref_info)

    common.check_create_folder(os.path.dirname(xl_filename))
    # finally saved
    wb_template.save(xl_filename)

# -----------------------------------------------------------------------------
# Check value from excel
# @param value the value
# @return the value corrected
# -----------------------------------------------------------------------------
def check_val(value):
    if value is None:
        return value

    result = value.strip()

    # result = result.replace(u'\u2019', "'")  # none breakable space
    # result = result.replace(u'\u2013', "-")  # long dash
    # result = result.replace(u'\u2026', "...")  # compressed ...

    return result

# -----------------------------------------------------------------------------
# Read an excel file to get all reference
#
# @param xl_filename the excel  filename
# @return ref_info the info referential
# -----------------------------------------------------------------------------
def read_ref_from(xl_filename):
    result = {}
    xl_filename = common.check_is_file_and_correct_path(xl_filename)

    logging.info("Read the excel file %s", xl_filename)
    wbook = openpyxl.load_workbook(filename=xl_filename)
    wsheet = ws_from_wb("Ref", wbook)

    title_col = {}
    (row_title, col_key) = find_column("key", wsheet)

    for i in range(col_key, 100):
        title = check_val(wsheet[xlcoord(row_title, i)].value)
        if (title is not None) and (len(title) > 1):
            title_col[title.lower()] = i

    current_row = row_title + 1
    while wsheet[xlcoord(current_row, title_col['key'])].value is not None:
        value = {}
        for title in title_col:
            value[title] = wsheet[xlcoord(current_row, title_col[title])].value
            value[title] = check_val(value[title])
            if value[title] is None:
                value[title] = ''

        value['key'] = value['key'].upper()
        value['lang'] = value['lang'].lower()

        if value['key'] not in result:
            result[value['key']] = {}
        if value['lang'] not in result[value['key']]:
            result[value['key']][value['lang']] = {}

        for key, val in value.items():
            result[value['key']][value['lang']][key] = val

        current_row += 1

    return result

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
# -----------------------------------------------------------------------------
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]
