﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Le référentiel d'information de Guichet Entreprises est mis à disposition
# selon les termes de la licence Creative Commons Attribution - Pas de
# Modification 4.0 International.

# Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
# suivante :
# http://creativecommons.org/licenses/by-nd/4.0/
# ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
# Mountain View, California, 94041, USA.
# -----------------------------------------------------------------------------

import logging
import sys
import os
import pickle

from pymdtools import common
from pymdtools import mdfile


__folder_name__ = ["%(category-filename-short-slug)s",
                   "%(domain-filename-short-slug)s",
                   "%(key)s-%(name-filename-short-slug)s"]

__filename_ref__ = "%(key)s-%(name-filename-short-slug)s"


# -----------------------------------------------------------------------------
# A decorator to cache the result of a function to the disk
# the function must be of the following type :
#    def the_function(filename)
#
# Could be usefull for an heavy initialisation from a file
# -----------------------------------------------------------------------------
@common.simple_decorator
def cache_on_disk(function_to_decorated):
    # helper
    def call_func_and_save_cache(filename, cache_filename):
        result = function_to_decorated(filename)
        file = open(cache_filename, "wb")
        logging.debug("Create cache %s", cache_filename)
        pickle.dump(result, file)
        file.close()
        return result

    # helper
    def read_cache(cache_filename):
        file = open(cache_filename, "rb")
        logging.debug("Read cache %s", cache_filename)
        result = pickle.load(file)
        file.close()
        return result

    # the return function
    def cached_init_from_file(filename, refresh=False):
        the_filename = common.set_correct_path(filename)
        if os.path.isdir(the_filename):
            the_filename = the_filename + os.sep
        cache_filename = the_filename + \
            "." + function_to_decorated.__name__ + \
            ".cache"

        if not os.path.isfile(cache_filename) or refresh:
            return call_func_and_save_cache(filename, cache_filename)

        # last modification time
        mtime_file = os.path.getmtime(the_filename)
        mtime_cache = os.path.getmtime(cache_filename)

        if mtime_file > mtime_cache:
            return call_func_and_save_cache(filename, cache_filename)
        return read_cache(cache_filename)

    return cached_init_from_file


# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
# -----------------------------------------------------------------------------
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]

# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result


# -----------------------------------------------------------------------------
__REF_FOLDER__ = {}
# -----------------------------------------------------------------------------
def get_ref_folder():
    if 'path' not in __REF_FOLDER__:
        # make a guess
        ref_folder = os.path.join(__get_this_folder(), "..", "content/")
        __REF_FOLDER__['path'] = ref_folder

    return __REF_FOLDER__['path']

# -----------------------------------------------------------------------------
def set_ref_folder(the_new_path):
    __REF_FOLDER__['path'] = common.check_folder(the_new_path)
    return __REF_FOLDER__['path']

# -----------------------------------------------------------------------------
def add_data_to_reference(ref_base, data):
    if 'key' not in data.keys() or 'lang' not in data.keys():
        return ref_base

    key = data['key'].upper()
    lang = data['lang'].lower()
    if key not in ref_base:
        ref_base[key] = {}
    if lang in ref_base[key]:
        raise Exception("Two file for the key %s lang %s" % (key, lang))

    ref_base[key][lang] = {}
    for name, value in data.items():
        ref_base[key][lang][name] = value

    logging.info("Add key=%s lang=%s", key, lang)
    return ref_base

# -----------------------------------------------------------------------------
@cache_on_disk
def create_ref_from_files(folder):
    result = {}

    def collect(filename):
        markd = mdfile.MarkdownContent(filename)
        logging.debug("collect %s", markd.full_filename)
        markd['filename'] = markd.full_filename
        markd['content_path'] = os.path.relpath(markd.full_filename, folder)

        add_data_to_reference(result, markd)

    common.apply_function_in_folder(folder, collect, filename_ext=".md")

    return result

# -----------------------------------------------------------------------------
def list_description_files(folder=None):
    result = []
    if folder is None:
        folder = get_ref_folder()

    def collect(filename):
        logging.debug("analyse %s", filename)
        mdown = mdfile.MarkdownContent(filename)
        if 'type' not in mdown.keys() \
                or mdown['type'].lower() != "description":
            return
        result.append(mdown.full_filename)
        logging.info("add description %s", mdown.full_filename)

    common.apply_function_in_folder(folder, collect, filename_ext=".md")

    return result

# -----------------------------------------------------------------------------
# Function to get the information referential
#
# @param key the key to find the info data (if key is None, return the dict)
# @return the info referential
# -----------------------------------------------------------------------------
@common.static(__info_ref_dict__=None)
def get_ref(key=None, lang=None, refresh=False):
    if get_ref.__info_ref_dict__ is None:
        if get_ref_folder() is None:
            raise Exception("You must set the folder for reference")
        get_ref.__info_ref_dict__ = \
            create_ref_from_files(get_ref_folder(), refresh=refresh)

    if key is not None:
        key = key.upper()
        if lang is not None:
            return get_ref.__info_ref_dict__[key][lang]
        return get_ref.__info_ref_dict__[key]

    return get_ref.__info_ref_dict__

# -----------------------------------------------------------------------------
# Function to get the information referential
#
# @param key the key to find the info data (if key is None, return the dict)
# @return the info referential
# -----------------------------------------------------------------------------
def get_ref_langs():
    refs = get_ref()
    result = []
    for key in refs:
        for lang in refs[key]:
            if lang not in result:
                result.append(lang)
    return result

# -----------------------------------------------------------------------------
# Function to get the information referential
#
# @param key the key to find the info data (if key is None, return the dict)
# @return the info referential
# -----------------------------------------------------------------------------
def get_ref_organised(lang, keys_order, refresh=False):
    refs = get_ref(refresh=refresh)
    temp = {}
    for key in refs:
        if lang not in refs[key]:
            continue
        temp[key] = {
            'filename': refs[key][lang]['filename'],
            'title': refs[key][lang]['title'],
            'key': key,
        }
        for level in keys_order:
            temp[key][level] = refs[key][lang][level]

    result = {}
    for key in temp:
        inter = result
        for level in keys_order:
            if temp[key][level] not in inter:
                inter[temp[key][level]] = {}
            inter = inter[temp[key][level]]
        inter[key] = {
            'filename': temp[key]['filename'],
            'title': temp[key]['title'],
            'key': key,
        }

    return result
