﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Le référentiel d'information de Guichet Entreprises est mis à disposition
# selon les termes de la licence Creative Commons Attribution - Pas de
# Modification 4.0 International.

# Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
# suivante :
# http://creativecommons.org/licenses/by-nd/4.0/
# ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
# Mountain View, California, 94041, USA.
# -----------------------------------------------------------------------------
from .version import __version_info__
from .version import __release_date__

__module_name__ = "pygereference"
__package_name__ = "reference"
__version__ = __version_info__
__author__ = "Florent Tournois"
__copyright__ = "Copyright 2018, Guichet Entreprises"

__credits__ = ["Arthur Viller"]
__license__ = "CC BY ND"
__maintainer__ = "Florent Tournois"
__email__ = "florent.tournois@gmail.fr"
__status__ = "Production"
__url__ = "https://gitlab.com/guichet-entreprises.fr/information/reference"

__gitlab_project_id__ = "13561843"
__gitlab_url__ = "https://gitlab.com/"
