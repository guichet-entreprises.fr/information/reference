﻿## Définition de l’activité

### Définition
### Centre de formalités des entreprises (CFE) compétent
### Code(s) APE


## Conditions d'installation

### Qualifications professionnelles
### Qualifications professionnelles - Ressortissants européens
### Condition d'honorabilité
### Quelques particularités de la réglementation de l’activité


## Démarches et formalités d'installation

### Formalités de déclaration de l’entreprise
### Le cas échéant, enregistrer les statuts de la société
### Autorisation(s) post-immatriculation