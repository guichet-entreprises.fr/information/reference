@ECHO off
REM # -----------------------------------------------------------------------------
REM # 
REM # Le référentiel d'information de Guichet Entreprises est mis à disposition
REM # selon les termes de la licence Creative Commons Attribution - Pas de
REM # Modification 4.0 International.
REM #
REM # Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
REM # suivante :
REM # http://creativecommons.org/licenses/by-nd/4.0/
REM # ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
REM # Mountain View, California, 94041, USA.
REM # 
REM # -----------------------------------------------------------------------------
SET MYPATH=%~dp0
CD %MYPATH%
SET ARGUMENT=%1
SET MODULE=pygpfr
SET FUN=scripts/common.bat
IF EXIST %MODULE%\version.bat (
    CALL %MODULE%\version.bat
) ELSE (
    SET VERSION=Not found
)
REM -------------------------------------------------------------------------------
:STARTAGAIN
CALL %FUN% :CONFIGURE_DISPLAY
CALL %FUN% :CLEAR_SCREEN
CALL %FUN% :PRINT_LINE "    VERSION=%VERSION%" 
CALL %FUN% :PRINT_LINE "    MYPATH=%MYPATH%" 
CALL %FUN% :LINE_BREAK

TITLE [ge.fr][%MODULE%] MAKE %ARGUMENT%
IF /I "%ARGUMENT%" == "install_requirements" ( CALL %FUN% :INSTALL_REQUIREMENTS "requirements.txt"      ) ELSE ^
IF /I "%ARGUMENT%" == "install_editable"     ( CALL %FUN% :INSTALL_EDITABLE                             ) ELSE ^
CALL %FUN% :PYTHON_SETUP "%ARGUMENT%"

REM -------------------------------------------------------------------------------
CALL %FUN% :LINE_BREAK
CALL %FUN% :PRINT_LINE "   End of the make execution"
CALL %FUN% :LINE_BREAK
REM -------------------------------------------------------------------------------
CHOICE /C:YN /M "Do it again ? (Y/N)"
IF "%ERRORLEVEL%" EQU "1" GOTO :STARTAGAIN
IF "%ERRORLEVEL%" EQU "2" GOTO :EOF
REM -------------------------------------------------------------------------------
:EOF
