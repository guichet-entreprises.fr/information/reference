﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="da" -->

Bemærkning om kvaliteten af ​​maskinoversættelse
================================================

<!-- begin-ref(disclaimer-trans-da) -->

**Bemærkning om kvaliteten af ​​maskinoversættelse**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Denne side er blevet oversat ved hjælp af en maskinoversættelse værktøj og kan indeholde fejl. Brugere rådes til at kontrollere rigtigheden af ​​oplysningerne på denne side før den træffer foranstaltninger.

Enterprise Bank Tjenesten kan ikke holdes ansvarlig for driften af ​​oplysninger, der vil være unøjagtige på grund af en ikke-automatisk oversættelse tro mod originalen.<!-- alert-end:warning -->

<!-- end-ref -->