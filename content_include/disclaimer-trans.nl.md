﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="nl" -->

Mededeling over de kwaliteit van automatische vertaling
=======================================================

<!-- begin-ref(disclaimer-trans-nl) -->

**Mededeling over de kwaliteit van automatische vertaling**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Deze pagina is vertaald met behulp van een automatische vertaling instrument en kunnen fouten bevatten. Gebruikers wordt geadviseerd om de juistheid van de informatie op deze pagina informatie te controleren alvorens actie te ondernemen.

De Enterprise Bank Service kan niet verantwoordelijk voor de werking van de informatie die onjuist als gevolg van een niet-automatische vertaling trouw aan het origineel zal worden gehouden.<!-- alert-end:warning -->

<!-- end-ref -->