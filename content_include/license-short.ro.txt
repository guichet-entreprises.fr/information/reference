﻿Banca de afaceri de depozit de informații sunt puse la dispoziție
în conformitate cu termenii licenței Creative Commons Attribution - nr
Schimbarea International 4.0.

Pentru a accesa o copie a acestei licențe, vă mulțumesc pentru a merge la adresa
următor:
http://creativecommons.org/licenses/by-nd/4.0/
sau trimite o scrisoare la Creative Commons, 444 Castro Street, Suite 900,
Mountain View, California, 94041, Statele Unite ale Americii.