﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="ga" -->

Fógra faoi chaighdeán an aistriúcháin
=====================================

<!-- begin-ref(disclaimer-eu-ga) -->

**Fógra faoi chaighdeán an aistriúcháin**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

lch seo aistrithe ag gairmiúla is féidir go mbeadh earráidí. Úsáideoirí Moltar do sheiceáil ar chruinneas na faisnéise a thaispeántar ar an leathanach seo sula gcuireann siad aon ghníomh.
Ní féidir leis an tSeirbhís Banc Fiontair a bheith freagrach as oibriú na faisnéise a bheidh dlite míchruinn ar aistriúchán neamh-dílis de na bunaidh.<!-- alert-end:warning -->

<!-- end-ref -->