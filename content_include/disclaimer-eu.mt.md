﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="mt" -->

Avviż dwar il-kwalità tat-traduzzjoni
=====================================

<!-- begin-ref(disclaimer-eu-mt) -->

**Avviż dwar il-kwalità tat-traduzzjoni**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Din il-paġna ġiet tradotta mill professjonali iżda jista 'jkun fihom żbalji. Utenti huma avżati biex jiċċekkjaw l-eżattezza tal-informazzjoni pprovduta f'din il-paġna qabel ma jkun assunt kull azzjoni.
Il-Bank Servizz Intrapriża ma tistax tinżamm responsabbli għall-operat ta 'informazzjoni li tkun dovuta eżatt li traduzzjoni mhux fidila tal-oriġinal.<!-- alert-end:warning -->

<!-- end-ref -->