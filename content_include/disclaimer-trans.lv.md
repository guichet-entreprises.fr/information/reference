﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="lv" -->

Paziņojums par mašīntulkojuma kvalitāti
=======================================

<!-- begin-ref(disclaimer-trans-lv) -->

**Paziņojums par mašīntulkojuma kvalitāti**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Šī lapa ir tulkota, izmantojot automātiskās tulkošanas rīku un var saturēt kļūdas. Lietotājiem tiek ieteikts pārbaudīt precizitāti šajā lapā sniegtā informācija Pirms uzsākt kādu darbību.

Enterprise Banka dienests nevar saukt pie atbildības par darbību informāciju, kas būs neprecīzi, jo nav automātiskā tulkošana uzticīgs oriģinālam.<!-- alert-end:warning -->

<!-- end-ref -->