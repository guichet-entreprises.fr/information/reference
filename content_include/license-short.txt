Le référentiel d'information de Guichet Entreprises est mis à disposition
selon les termes de la licence Creative Commons Attribution - Pas de
Modification 4.0 International.

Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
suivante :
http://creativecommons.org/licenses/by-nd/4.0/
ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
Mountain View, California, 94041, USA.