﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="ro" -->

Notă cu privire la calitatea de traducere automată
==================================================

<!-- begin-ref(disclaimer-trans-ro) -->

**Notă cu privire la calitatea de traducere automată**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

Această pagină a fost tradusă folosind un instrument de traducere automată și poate conține erori. Utilizatorii sunt sfătuiți să verifice acuratețea informațiilor furnizate pe această pagină înainte de a întreprinde orice acțiune.

Banca Serviciul Enterprise nu poate fi considerat responsabil pentru exploatarea informațiilor care vor fi din cauza inexacte la o traducere neautomat fidelă originalului.<!-- alert-end:warning -->

<!-- end-ref -->