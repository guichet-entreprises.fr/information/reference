﻿Business Pankki tietovarasto on saatavilla
ehtojen mukaisesti Creative Commons Nimeä License - Ei
Muuttuvassa kansainvälisessä 4.0.

Pääset kopion tämän lisenssin, kiitos mennä osoitteeseen
seuraava:
http://creativecommons.org/licenses/by-nd/4.0/
tai lähettää kirjeen Creative Commons, 444 Castro Street, Suite 900,
Mountain View, California, 94041, USA.