﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS113" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Réalisation de projets de plan pluriannuel de travaux" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="realisation-de-projets-de-ppt" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/realisation-de-projets-de-ppt.html" -->
<!-- var(last-update)="2022-06" -->
<!-- var(url-name)="realisation-de-projets-de-ppt" -->
<!-- var(translation)="None" -->

# Réalisation de projets de plan pluriannuel de travaux

Dernière mise à jour : <!-- begin-var(last-update) -->Juin 2022<!-- end-var -->

## Définition de l'activité

La personne qui réalise un projet de plan pluriannuel de travaux (PPT) procède à une analyse du bâti et des équipements de l'immeuble pour lequel son expertise a été sollicitée par un syndic mandaté par un syndicat des copropriétaires.  

À cet effet elle dresse la liste des travaux nécessaires à la sauvegarde de l'immeuble, à la préservation de la santé et de la sécurité des occupants, à la réalisation d'économies d'énergie et à la réduction des émissions de gaz à effet de serre. Elle évalue sommairement leur coût et les hiérarchise.

Ce professionnel estime le niveau de performance que les travaux mentionnés ci-dessus permettent d'atteindre.  
 
Ce professionnel propose également au syndicat des copropriétaires un échéancier des travaux dont la réalisation apparaît nécessaire dans les dix prochaines années.


## Qualifications professionnelles requises en France

Pour réaliser un projet de PPT, la personne doit, dans le domaine des techniques du bâtiment, justifier soit :

- d’un diplôme de l’enseignement supérieur, d’une durée minimale de trois ans ou d’une durée équivalente à temps partiel, délivré par une autorité compétente d’un État de l’Union européenne ou partie à l’accord sur l’Espace économique européen ;
- d’un titre professionnel de niveau équivalent ;
- d’une certification de qualifications professionnelles de niveau équivalent ;
- d’une attestation d’inscription au tableau d’un ordre professionnel reconnu dans le domaine de l’immobilier.

Ces diplômes, titres, certificats ou attestations d’inscription à un ordre professionnel lui permettent  de justifier de ses connaissances dans le domaine des techniques de bâtiment, notamment les  modes constructifs, les produits et matériaux de construction, les pathologies, la thermique, les possibilités d'amélioration énergétique et de réhabilitation thermique, l’évaluation des émissions de gaz à effet de serre et les possibilités de réduction de celles-ci, la terminologie technique et juridique, les textes législatifs et réglementaires relatifs aux normes sanitaires et de sécurité afférentes à l’habitat.

Cette activité pourra être exercée à compter du :

- 1er janvier 2023, pour les syndicats des copropriétaires de plus de 200 lots ;
- 1er janvier 2024, pour les syndicats des copropriétaires comprenant entre 51 et 200 lots ;
- 1er janvier 2025, pour les syndicats des copropriétaires comprenant au plus 50 lots.

## Particularités de la réglementation de l’activité

### Déontologie (indépendance, impartialité)

Nul ne peut réaliser un projet de plan pluriannuel de travaux (PPT) s’il ne peut attester de son impartialité et de son indépendance vis-à-vis :

- du syndic qui assure la gestion de l’immeuble objet du projet de PPT, sauf dérogation prévue au II de l’article 18-1-A de la loi n°65-557 du 10 juillet 1965 ;
- des fournisseurs d’énergies et des entreprises intervenant sur l’immeuble et ses équipements et sur lequel porte le projet.

### Interdictions

La personne qui se propose de réaliser le projet de PPT ne peut accorder au syndicat des copropriétaires concernés aucun avantage ni rétribution. 

Elle ne peut également recevoir aucun avantage ou rétribution de la part des entreprises pouvant réaliser des travaux sur l’immeuble objet du projet. 

### Assurances

Si la personne réalisant un projet de PPT a souscrit une assurance de responsabilité civile professionnelle, elle remet au syndic de l’immeuble concerné une attestation précisant les compétences couvertes.

### Obligation d’information

La personne qui réalise un projet de PPT et qui a souscrit une assurance de responsabilité civile professionnelle remet au syndic de l’immeuble concerné une attestation précisant les compétences couvertes.

Ce professionnel doit également produire une copie de diplôme, titre professionnel, certification de qualification professionnelle dans les domaines des techniques du bâtiment ou une attestation d’inscription au tableau d’un ordre professionnel reconnu dans le domaine l’immobilier.

## Démarches pré-immatriculation

Aucune procédure n’est à réaliser auprès d’une autorité compétente pour exercer l’activité de service. Il appartient au syndic de copropriété de vérifier au préalable les compétences et garanties de l’entreprise qui réalisera le projet de plan pluriannuel de travaux.

## Liens utiles

### Textes de référence

- Article 14-2, loi n° 65-557 du 10 juillet 1965 fixant la copropriété des immeubles bâtis ;
- Article 171, loi n° 2021-1104 du 22 août 2021 portant lutte contre le dérèglement climatique et renforcement de la résilience face à ses effets ;
- Décret n° 2022-663 du 25 avril 2022 fixant les compétences et les garanties exigées pour les personnes établissant le projet de plan pluriannuel de travaux des immeubles soumis au statut de la copropriété.