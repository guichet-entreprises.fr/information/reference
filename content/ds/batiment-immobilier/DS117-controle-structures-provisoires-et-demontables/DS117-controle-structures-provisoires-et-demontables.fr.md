﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS117" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Contrôle, vérification et inspection des structures provisoires et démontables" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="controle-structures-provisoires-et-demontables" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/controle-structures-provisoires-et-demontables.html" -->
<!-- var(last-update)="2022-09" -->
<!-- var(url-name)="controle-structures-provisoires-et-demontables" -->
<!-- var(translation)="None" -->

# Contrôle, vérification et inspection des structures provisoires et démontables

Dernière mise à jour : <!-- begin-var(last-update) -->Septembre 2022<!-- end-var -->

## Définition de l'activité

Les structures provisoires et démontables sont soumises à des règles relatives à leur implantation, leur solidité, leur aménagement, leur exploitation et leur vérification. Le fabricant, l'installateur et l'organisateur sont tenus, chacun en ce qui le concerne, de s'assurer que l'ensemble démontable soit conçu, installé et entretenu en conformité avec les dispositions de l'arrêté fixant les règles de sécurité et les dispositions techniques applicables aux structures provisoires et démontables. A cet effet, ces vérifications doivent être effectuées par un organisme agréé par le ministère en charge de la construction ou par un organisme accrédité ou par un technicien compétent conformément aux dispositions de l'arrêté.

La fiche précise les conditions permettant à une personne physique ou morale d'exercer l'activité de contrôle de la conception des structures provisoires et démontables ou l'activité de vérification de leur montage et la réalisation des inspections durant leur exploitation.

## Qualifications professionnelles requises en France

### Activité pouvant être exercée par des personnes ayant des qualifications professionnelles

L'exercice de l'activité de contrôle de la conception des structures provisoires et démontables ou l'activité de vérification de leur montage et la réalisation des inspections durant leur exploitation ne nécessite d'observer aucune exigence supplémentaire que les règles d'accès propres à la profession de [Contrôleur technique](https://www.guichet-qualifications.fr/fr/dqp/batiment/controleur-technique-de-la-construction.html).

### Activité pouvant être exercée par des personnes n'ayant pas de qualifications professionnelles

L'exercice de l'activité de contrôle de la conception des structures provisoires et démontables ou l'activité de vérification de leur montage et la réalisation des inspections durant leur exploitation peut être réalisée par :

- une personne physique ou morale agréée par le ministère en charge de la construction ;
- une personne physique ou morale accréditée par le  Comité français d'accréditation (Cofrac) ou par tout autre organisme d'accréditation équivalent signataire de l'accord multilatéral pris dans le cadre de la coordination européenne des organismes d'accréditation.

## Particularités de la réglementation de l'activité

Cette rubrique ne vaut que pour l'exercice de l'activité par une personne physique ou morale agréée par le ministère en charge de la construction, par une personne physique ou morale accrédité par le  Comité français d'accréditation (Cofrac) ou par tout autre organisme d'accréditation équivalent signataire de l'accord multilatéral pris dans le cadre de la coordination européenne des organismes d'accréditation.

Dans l'hypothèse où l'une des activités sera exercée par un contrôleur technique, merci de vous référer à la fiche [Contrôleur technique](https://www.guichet-qualifications.fr/fr/dqp/batiment/controleur-technique-de-la-construction.html).

### Incompatibilités

Une personne physique ou morale peut exercer :

- soit l'activité de contrôle de la conception des structures provisoires et démontables ;
- soit l'activité de vérification du montage et les inspections durant l'exploitation.

L'activité de contrôle de la conception des structures provisoires et démontables est incompatible avec l'activité de vérification du montage et de réalisation des inspections durant l'exploitation.

Le contrôle de la conception est réalisé par un organisme d'inspection tierce partie indépendant des parties engagées. En particulier il ne doit jouer aucun rôle dans les domaines suivants : conception, fabrication, fourniture, installation, acquisition, possession, utilisation ou maintenance des structures inspectées. De plus, l'organisme ne doit ni faire partie, ni être lié à une entité juridique agissant dans les domaines cités précédemment (organisme de type A selon la norme NF EN ISO/IEC 17020).

La vérification du montage et l'inspection en exploitation de l'ensemble démontable sont réalisées soit par un organisme tierce partie indépendante, ou par un organisme qui constitue une partie identifiable mais pas nécessairement une partie distincte de l'organisation impliquée dans les domaines cités précédemment (par exemple, une société de prestations événementielles peut disposer d'un service de vérification et d'inspection. Dans ce cas, les personnes accréditées de ce service ne sont pas autorisées à vérifier et à inspecter les structures installées par leur société) et qui fournit des prestations d'inspection sous réserve qu'il n'intervient pas dans le montage de l'ensemble démontable vérifié (organisme de type C selon la norme NF EN ISO/IEC 17020)

### Assurance

L'organisme de contrôle technique doit justifier d'une assurance de responsabilité adaptée à la prestation envisagée (article R. 125-6 du code de la construction et de l'habitation).

## Démarches d'obtention de l'agrément ou de l'accréditation

### Autorité compétente

Le ministre en charge de la construction est l'autorité compétente pour délivrer l'agrément. Contact par courriel à l'adresse suivante : [nicolas.darrius@developpement-durable.gouv.fr](mailto:nicolas.darrius@developpement-durable.gouv.fr)

Le [Cofrac](https://www.Cofrac.fr/faq-contact/) est compétent pour délivrer les accréditations.


### Procédure

#### Agrément par le ministre en charge de la construction

La procédure est décrite dans l'arrêté du 26 novembre 2009 fixant les modalités pratiques d'accès à l'exercice de l'activité de contrôleur technique.

#### Accréditation

La procédure est décrite sur le [site du Cofrac](https://www.Cofrac.fr/comment-se-faire-accrediter/la-route-vers-laccreditation/vous-preparer-a-laccreditation/).

### Délais de réponse

Concernant l'agrément délivré par le ministre en charge de la construction, le délai est de de 6 mois à compter de la réception du dossier jugé recevable. L'absence de décision vaut refus de la demande.

Le document Cofrac INS REF 05 « Règlement d'accréditation » accessible sur le site du Cofrac décrit la procédure d'accréditation des organismes d'inspection. A titre indicatif le délai d'obtention d'une accréditation initiale est de 6 à 9 mois en fonction de la maturité de l'entreprise candidate.  Ce délai comprend :

- la recevabilité administrative de la demande (1 à 2 mois) ;
- la recevabilité opérationnelle de la demande (15 jours à 2 mois selon réactivité de l'organisme à répondre aux éventuelles remarques). Il s'agit de déterminer l'aptitude de l'entreprise à recevoir l'équipe d'évaluation ;
- sous réserve que les prérequis soient satisfait (réalisation de missions d'inspection à blanc ou réelle selon la situation, application complète des dispositions du système de management de l'organisme, dont en particulier la surveillance des inspecteurs, l'audit interne et la revue de direction), dans les 2 à 3 mois qui suivent la recevabilité opérationnelle, l'évaluation initiale (ou d'extension) est déclenchée. Au terme des travaux d'investigation (minimum 1,5 jour et adapté en fonction notamment de la taille de l'organisme) et dans un délai maximal d'un mois, le responsable de l'équipe d'évaluation délivre Cofrac un rapport ;
- le Cofrac rend sa décision dans un délai d'environ un mois.

Lorsque l'accréditation initiale est prononcée, un premier cycle de surveillance de 4 ans débute comprenant des évaluations de surveillance annuelles jusqu'au renouvellement.

A l'issue, les autres cycles de surveillance ont une durée de 5 ans.

## Liens utiles

### Textes de référence

- Article L. 131-1 du Code de la construction et de l'habitation : « Tout bâtiment est implanté, conçu et dimensionné de sorte qu'il résiste durablement dans son ensemble et dans chacun de ses éléments à l'effet combiné de son propre poids, des charges climatiques extrêmes et des surcharges d'exploitation correspondant à son usage normal. Il en est de même pour les structures provisoires et démontables pour toute la durée de leur utilisation. »
- Article L. 134-12 du Code de la construction et de l'habitation : « Les bâtiments sont conçus et construits de manière à éviter les chutes accidentelles de hauteur des personnes, dans le cadre d'un usage normal. Il en va de même pour les structures provisoires et démontables pendant toute la durée de leur utilisation. »
- Arrêté du 26 novembre 2009 fixant les modalités pratiques d'accès à l'exercice de l'activité de contrôleur technique

### Autres liens utiles

- [Rubrique consacrée à la préparation à l'accréditation](https://www.Cofrac.fr/comment-se-faire-accrediter/la-route-vers-laccreditation/vous-preparer-a-laccreditation/) du site du Cofrac