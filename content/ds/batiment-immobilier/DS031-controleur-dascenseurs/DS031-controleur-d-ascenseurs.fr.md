﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS031" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Contrôleur d'ascenseurs" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="controleur-d-ascenseurs" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/controleur-d-ascenseurs.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="controleur-d-ascenseurs" -->
<!-- var(translation)="None" -->

# Contrôleur d'ascenseurs

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le contrôleur d'ascenseurs est un professionnel dont la mission consiste à vérifier périodiquement le bon état de fonctionnement des ascenseurs et la sécurité des personnes les utilisant.

Le professionnel a notamment pour mission de :

- vérifier la conformité des ascenseurs aux obligations de sécurité ;
- relever les défauts présentant un danger pour la sécurité des personnes et/ou portant atteinte au bon fonctionnement de l'appareil.

*Pour aller plus loin* : article L. 125-2-3 du Code de la construction et de l'habitation ; article R. 125-2-4 du Code de la construction et de l'habitation ; arrêté du 7 août 2012 relatif aux contrôles techniques à réaliser dans les installations d'ascenseurs.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de l'activité exercée :

- pour une activité libérale, le CFE compétent est l'Urssaf ;
- pour les sociétés commerciales, il s'agit de la chambre de commerce et d'industrie (CCI) ;
- pour les sociétés civiles, il s'agit du greffe du tribunal de commerce, ou du greffe du tribunal d'instance pour les départements du Bas-Rhin, du Haut-Rhin et de la Moselle.

**Bon à savoir**

L'activité est considérée comme commerciale dès lors que l'entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l'activité demeure artisanale quel que soit le nombre de salariés, à la condition qu'elle n'utilise pas de procédé industriel). En revanche, si l'entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d'achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le professionnel souhaitant exercer l'activité de contrôleur d'ascenseurs dispose de 3 possibilités:

- cas 1 : être un [contrôleur technique de la construction](https://www.guichet-qualifications.fr/fr/professions-reglementees/controleur-technique-de-la-construction/) bénéficiant d'un agrément l'habilitant à intervenir sur les ascenseurs ;
- cas 2 : être un organisme habilité dans un des États membres de l'Union européenne ou dans l'un des autres États parties à l'accord sur l'Espace économique européen, chargé d'effectuer l'évaluation de la conformité d'ascenseurs soumis au marquage CE (dans ce cas il n'y a pas de qualifications professionelles requises pour exercer l'activité) ;
- cas 3 : être certifié par un organisme accrédité par le Comité français d'accréditation ou par un organisme signataire de l'accord européen multilatéral pris dans le cadre de la coordination européenne des organismes d'accréditation (dans ce cas il n'y a pas de qualifications professionelles requises pour exercer l'activité) ;

*Pour aller plus loin* : articles R. 125-2-5 du Code de la construction et de l'habitation.

### b. Qualifications professionnelles - Ressortissants européens (LPS ou LE)

#### Pour une Libre Prestation de Services (LPS)

Le professionnel ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) peut exercer en France, à titre temporaire et occasionnel, l'activité de contrôleur d'ascenseurs, dès lors qu'il est légalement établi dans cet État pour y exercer la même activité.

L'intéressé devra effectuer des procédures différentes suivant les diverses possibilités décrites en 2°. a. :

- cas 1 : appliquer la procédure décrite dans la fiche « [Contrôleur technique de la construction](https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/controleur-technique-de-la-construction.html) » ;
- pour les cas 2 et 3, aucune qualification professionelle requise.

#### Pour un Libre Établissement (LE)

Le ressortissant d'un État membre de l'UE ou l'EEE souhaitant exercer l'activité de contrôleur d'ascenseurs en France à titre permanent doit appliquer des procédures différentes suivant les diverses possibilités décrites en 2°. a. :

- cas 1: se reporter à la fiche « [Contrôleur technique de la construction](https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/controleur-technique-de-la-construction.html) » ;
- pour les cas 2 et 3, aucune qualification professionelle requise.

### c. Conditions d’honorabilité

Le professionnel exerçant l'activité de contrôleur d’ascenseurs doit agir en respectant des principes :

- d'impartialité ;
- d'indépendance avec le propriétaire qui fait appel à lui, ou avec une entreprise susceptible d'effectuer des travaux sur un ascenseur ou son entretien.

### d. Quelques particularités de la réglementation de l’activité

**Assurance**

Le contrôleur d'ascenseurs est tenu, en sa qualité de professionnel, de souscrire une assurance de responsabilité professionnelle.

**Rapports dressés par le contrôleur technique**

Le professionnel chargé du contrôle des ascenseurs est tenu de remettre au propriétaire :

- un document attestant sur l'honneur qu'il :
  - dispose des qualifications professionnelles nécessaires à l'exercice de son activité,
  - a souscrit une assurance de responsabilité professionnelle,
  - exerce son activité en toute impartialité et indépendance ;
- un rapport, dans le mois suivant son intervention, contenant l'ensemble des opérations réalisées et des défauts constatés.

En outre, le professionnel doit remettre un rapport au ministre chargé de la construction s'il constate que l'ascenseur contrôlé ne respecte pas les exigences essentielles mentionnées à l'article R. 125-2-13.

Enfin, avant le 1er mars de chaque année, le contrôleur d'ascenseurs dresse un bilan des contrôles techniques effectués au cours de l'année civile précédente et remet ce bilan au ministre chargé du logement.

*Pour aller plus loin* : article R. 125-2-6 du Code de la construction et de l'habitation ; arrêté du 7 août 2012 précité.

## 3°. Démarches et formalités d’installation

### a. Cas 1

Se reporter à la fiche « [Contrôleur technique de la construction](https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/controleur-technique-de-la-construction.html) ».

Point de contact: [cact@developpement-durable.gouv.fr](mailto:cact@developpement-durable.gouv.fr).

*Pour aller plus loin* : arrêté du 26 novembre 2009 fixant les modalités pratiques d'accès à l'exercice de l'activité de contrôleur technique.

### b. Cas 2

Point de contact pour les demandes de notification adressées à la France : [ascenseurs@developpement-durable.gouv.fr](mailto:ascenseurs@developpement-durable.gouv.fr)

*Pour aller plus loin* : chapitre IV intitulé notification des organismes d'évaluation de la conformité de la directive 2014/33/UE dite [« directive ascenseur »](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014L0033) la directive 2014 ; articles R. 125-2-28 à R. 125-2-38 du Code de la construction et de l'habitation.

### b. Cas 3

Se reporter à la [page consacrée au contrôle technique d'ascenseurs du site Internet de l'organisme certificateur dénommé SGS](https://www.sgsgroup.fr/fr-fr/construction/services-related-to-machinery-and-equipment/equipment-certification-and-calibration/technical-controller-of-elevators-certification).

Point de contact: [fr.cdp.ascenseurs@sgs.com](mailto:fr.cdp.ascenseurs@sgs.com).

*Pour aller plus loin* : arrêté du 13 décembre 2004 relatif aux critères de compétence des personnes réalisant des contrôles techniques dans les installations d'ascenseurs et arrêté du 15 juin 2005 modifiant l'arrêté précité.

### d. Formalités de déclaration de l’entreprise

**Autorité compétente**

Le contrôleur d'ascenseurs doit procéder à la déclaration de son entreprise, et doit pour cela effectuer une déclaration auprès de la CCI.

**Pièces justificatives**

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

**Délais**

Le centre de formalités des entreprises de la CCI adresse le jour même un récépissé au professionnel dressant, le cas échéant, les pièces manquantes au dossier. Le professionnel dispose alors d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

**Voies de recours**

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le CFE refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

**Coût**

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### d. Le cas échéant, enregistrer les statuts de la société

Le contrôleur d'ascenseurs doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- la forme même de l'acte l'exige.

**Autorité compétente**

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

**Pièces justificatives**

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.