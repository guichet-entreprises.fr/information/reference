﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS031" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Lift inspector" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="lift-inspector" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/lift-inspector.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="lift-inspector" -->
<!-- var(translation)="Auto" -->

Lift inspector
===================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

The elevator controller is a professional whose mission is to periodically check the proper functioning of elevators and the safety of the people using them.

The professional's mission is to:

- Verify elevators' compliance with safety obligations
- identify defects that pose a danger to the safety of the persons and/or affect the proper operation of the device.

*To go further* : Section L. 125-2-3 of the Building and Housing Code ; Article R. 125-2-4 of the Building and Housing Code ; 7 August 2012 order on technical checks to be carried out in elevator facilities.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the activity:

- for a liberal activity, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, it is the registry of the Commercial Court, or the registry of the district court for the departments of lower Rhine, Upper Rhine and Moselle.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees, provided that it does not use a process industrial sector). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional wishing to carry out the activity of elevator controller has 3 possibilities:

- Case 1: Being a[technical controller of construction](https://www.guichet-qualifications.fr/fr/professions-reglementees/controleur-technique-de-la-construction/) with an accreditation that allows him to intervene on elevators;
- Case 2: to be an authorised body in one of the Member States of the European Union or in one of the other States parties to the European Economic Area Agreement, responsible for assessing the compliance of lifts subject to CE marking (in this case it there are no professional qualifications required to carry out the activity);
- case 3: be certified by an accredited body by the French Accreditation Committee or by an organization that is a signatory to the European multilateral agreement taken as part of the European coordination of accreditation bodies (in this case there is no Professional qualifications required to carry out the activity);

*To go further* : Articles R. 125-2-5 of the Building and Housing Code.

### b. Professional qualifications - European nationals (LPS or LE)

#### For Freedom to provide services

The professional who is a member of the European Union (EU) or a party to the Agreement on the European Economic Area (EEA) may work in France, on a temporary and occasional basis, as an elevator controller, as long as he is established in that state to carry out the same activity.

The person concerned will have to carry out different procedures according to the various possibilities described in 2 degrees. Has.:

- Case 1: Apply the procedure described in the sheet[Technical controller of construction](https://www.guichet-qualifications.fr/fr/professions-reglementees/controleur-technique-de-la-construction/)
- cases 2 and 3, no professional qualifications required.
For a Freedom of establishment

The national of an EU Member State or the EEA wishing to carry out the activity of elevator controller in France on a permanent basis must apply different procedures according to the various possibilities described in 2 degrees. Has.:

- Case 1: Refer to the plug[Technical controller of construction](https://www.guichet-qualifications.fr/fr/professions-reglementees/controleur-technique-de-la-construction/) ;
- cases 2 and 3, no professional qualifications required.

### c. Conditions of honorability

The professional operating the activity of elevator controller must act in accordance with the principles:

- impartiality;
- independence with the owner who calls on him, or with a company likely to carry out work on an elevator or its maintenance.

### d. Some peculiarities of the regulation of the activity

**Insurance**

The elevator controller is required, as a professional, to take out professional liability insurance.

**Reports prepared by the technical controller**

The elevator control professional is required to provide the owner with:

- a document attesting to the honour that it:- has the professional qualifications to carry out its activity,
  - has taken out professional liability insurance,
  - operates impartially and independently;
- a report, within one month of his intervention, containing all the operations carried out and the defects found.

In addition, the professional must submit a report to the Minister responsible for construction if he finds that the controlled elevator does not meet the essential requirements mentioned in the section R. 125-2-13.

Finally, before March 1 of each year, the elevator controller takes stock of the technical checks carried out during the previous calendar year and gives this assessment to the Minister responsible for housing.

*To go further* : Article R. 125-2-6 of the Building and Housing Code ; 7 August 2012 above.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Case 1

See the plug[Technical controller of construction](https://www.guichet-qualifications.fr/fr/professions-reglementees/controleur-technique-de-la-construction/).

Contact point:[cact@developpement-durable.gouv.fr](mailto:cact@developpement-durable.gouv.fr).

*To go further* : Order of 26 November 2009 setting out the practical terms of access to the exercise of the technical controller activity.

### Case 2

Contact point for notification requests to France:[ascenseurs@developpement-durable.gouv.fr](mailto:ascenseurs@developpement-durable.gouv.fr)

*To go further* Chapter IV entitled Notification of Compliance Assessment Bodies of directive 2014/33/EU["elevator directive"](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014L0033 la directive 2014/) ; Articles R. 125-2-28 to R. 125-2-38 of the Building and Housing Code.

### Case 3

Refer to the[page dedicated to the technical control of elevators on the website of the certifying body called SGS](https://www.sgsgroup.fr/fr-fr/construction/services-related-to-machinery-and-equipment/equipment-certification-and-calibration/technical-controller-of-elevators-certification).

Contact point:[fr.cdp.ascenseurs@sgs.com](mailto:fr.cdp.ascenseurs@sgs.com).

*To go further* : decreed from 13 December 2004 competency criteria for people carrying out technical checks in elevators and elevators decreed on 15 June 2005 amending the aforementioned decree.

### d. Company reporting formalities

**Competent authority**

The elevator controller must report his company, and to do so must make a declaration with the ICC.

**Supporting documents**

The person concerned must provide the supporting documents depending on the nature of its activity.

**Timeframe**

The CCI's business formalities centre sends a receipt to the professional on the same day, drawing up the missing documents on file. The professional then has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the CFE refuses to receive the file, the applicant has an appeal before the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*To go further* Section 635 of the General Tax Code.

### d. If necessary, register the company's statutes

Once the company's status has been dated and signed, the elevator controller must register them with the[Corporate Tax Department]((http://www2.impots.gouv.fr/sie/ifu.htm)) (SIE) if:

- The act involves a particular transaction subject to registration;
- the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*To go further* Section 635 of the General Tax Code.

