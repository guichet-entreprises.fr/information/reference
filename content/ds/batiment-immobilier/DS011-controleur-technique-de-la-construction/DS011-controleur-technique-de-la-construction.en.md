﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS011" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Construction inspector" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="construction-inspector" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/construction-inspector.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="construction-inspector" -->
<!-- var(translation)="Auto" -->


Construction inspector
====================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The technical controller's mission is to prevent any technical hazards that might occur during the construction of a work.

It will ensure the soundness of the structures, the safety of the people who will occupy it, compliance with regulations relating to people with reduced mobility or energy performance.

*To go further* Article L. 111-23 of the Building and Housing Code;[Decree 99-443 of May 28, 1999](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000561661&dateTexte=20180416) general technical clauses applicable to public contracts for technical control.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out.

- For a commercial activity, the relevant CFE will be the Chamber of Commerce and Industry (CCI);
- If an individual business is set up, it is the Urssaf;
- for civil societies, the competent CFE is the registry of the Commercial Court or the registry of the district court in the departments of Lower Rhine, Upper Rhine and Moselle.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal, regardless of the number of employees of the company on the condition that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The profession of technical controller is reserved for holders with ministerial accreditation (see infra "3 degrees). a. Seek accreditation") and the following professional qualifications:

- for operational staff and engineers:- either have a post-secondary degree in building or civil engineering that warrants at least four years of study, and have at least three years of practical experience in the design, implementation, technical control or expertise of Buildings
  - or have six years of practical experience in the field;
- for mission execution staff:- either hold a high school certificate in the intended field of activity, and a practice of at least three years in design, construction, technical control or construction expertise,
  - or have six years of practical experience in the field.

*To go further* Article R. 111-32-2 of the Building and Housing Code.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment Establishment (LE))

**For temporary and casual exercise (LPS)**

Any national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA) working as a technical controller may carry out the same activity in France on a temporary and casual basis.

In order to do so, the person must make a prior declaration to the Minister responsible for construction prior to his first performance (see infra "3o). b. Pre-declaration for EU or EEA nationals for temporary and casual exercise").

In addition, where neither access to the activity nor its exercise is regulated in that state, the national must justify having engaged in this activity for at least two years in the ten years preceding his first benefit.

*To go further* Article L. 111-25 of the Building and Housing Code.

**For a permanent exercise (LE)**

Any legally established national practising as a technical controller in an EU or EEA state may carry out the same activity on a permanent basis in France. To do this, the professional must apply for approval from the Minister responsible for construction, as well as the French national (see infra "3°. a. Seek approval").

In the event of substantial differences between his professional qualifications and those required in France, the national will be auditioned by the[Accreditation Commission](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=67FE5CB5D0712DFD0DD7565EC7F5A972.tplgfr36s_1?idArticle=LEGIARTI000020740035&cidTexte=LEGITEXT000006074096&dateTexte=20091007) and will have to demonstrate his skills and knowledge in the construction field.

*To go further* Article L. 111-25 of the Building and Habitat Code.

### c. Conditions of honorability

Technical controller activity is incompatible with the design, execution or work expertise activities. In addition, the technical controller must act impartially and not interfere with the independence of those carrying out the above activities.

*To go further* Article R. 111-31 of the Building and Habitat Code.

### d. Some peculiarities of the regulation of the activity

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*To go further* : order of June 25, 1980 approving the general provisions of the fire and panic safety regulations in public institutions.

It is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

**Subscribe to 10-year liability insurance**

As part of his mission, the technical controller will have to take out 10-year liability insurance. He will be held responsible for damage that will compromise the strength of the structure or render it unsuitable for its destination.

*To go further* Article L. 111-24 of the Building and Housing Code.

**Activity report**

The technical controller must submit an activity report to the minister responsible for construction every March 31 of each year. The report should include:

- The controller's overall annual activity
- Its workforce;
- the indication of the improvements that the controller believes he has made during the exercise of his activity;
- a description of the claims and malfeasance he found;
- transactions for which he used a subcontractor.

*To go further* :[Article 3](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0DC55679BA8BBCEF1289B99A1B3777B3.tplgfr24s_1?idArticle=LEGIARTI000021355456&cidTexte=LEGITEXT000021355429&dateTexte=20180416) of the order of 26 November 2009 setting out the practical terms of access to the exercise of the technical controller activity.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Seek approval

**Competent authority**

The Minister responsible for construction is responsible for issuing the licence, which has a renewable validity of five years.

**Supporting documents**

The application for approval is made by filing a file that includes:

- The applicant's marital status and the address of his or her home;
- the justification of the theoretical competence and practical experience of the management staff, the internal organisation of the technical management, the rules for assistance to operational services effectively responsible for monitoring and the criteria Hiring or assigning officers
- The commitment that the comptroller will act impartially and independently;
- The comptroller's commitment to the attention of the administration will be made to any changes to the information he has given in order to obtain accreditation;
- If so, all the approvals it has obtained previously in the construction sector;
- the extent of the approval required according to the building category or categories of the structure, and the nature or extent of the vagaries that may result.

**Procedure**

The file will be submitted to the Accreditation Commission, which will ensure compliance with the exhibits. She will hear from the applicant before deliberating on what to do with her application. When it gives a favourable opinion, it will notify the Minister who will issue the approval within six months of receiving the full file. The silence kept within this time will be worth refusing the application.

*To go further* Articles R.111-29 and R.111-32 of the Building and Housing Code; Appendix III of the[decreed november 26, 2009](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021344640&dateTexte=20180416) setting out the practical ways in which the technical controller activity can be accessed;[link to the secretariat of the accreditation commission](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### b. Pre-declaration for EU or EEA national for temporary and casual exercise (LPS)

**Competent authority**

The Minister responsible for construction is responsible for deciding on the prior declaration of activity.

**Supporting documents**

This request can be sent electronically or by mail and takes the form of a folder with the following supporting documents:

- The marital status of the national and the address of his or her home;
- a certificate justifying that it is legally established in an EU or EEA state and that it does not incur a ban on practising;
- any document justifying his professional qualifications;
- any document justifying that he has been engaged in this activity for at least two years in the last ten years as long as the EU or EEA State in which it is established does not regulate this activity;
- The commitment that the national will act impartially and independently;
- The nature of the performance he plans to perform and its start and end date;
- a certificate of liability insurance tailored to the benefit he plans to make.

**Procedure**

The Minister responsible for construction will have one month to verify the file and authorize the delivery, after favourable advice from the Accreditation Commission. In the event of substantial differences between the national's professional qualifications and those required in France, the Minister may ask the national to appear before the commission and demonstrate his knowledge and skills construction.

The silence of the competent authority after receipt of the file, within one month, will be worth accepting the application.

*To go further* Articles R. 111-29-1 and R. 111-32-1 of the Building and Housing Code;[link to the secretariat of the accreditation commission](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### c. Company reporting formalities

The professional is required to register with the Trade and Companies Register (RCS) with the ICC in the event of the creation of a commercial company or to make a declaration of activity with the Urssaf in case of creation of an individual company.

It is advisable to refer to the "Registration of a commercial individual business at the RCS" for more information.

4°. Reference texts
---------------------------------------

- Articles L. 111-23 to L. 111-26 of the Building and Housing Code (Building and Housing Code);
- Articles R. 111-29 to R. 111-42 of the Building and Housing Code;
- Order of 26 November 2009 setting out the practical terms of access to the exercise of the technical controller activity;
- Decree 99-443 of 28 May 1999 relating to the general technical clauses applicable to public technical control contracts;
- NF 03-100.

