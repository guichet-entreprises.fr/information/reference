﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS011" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Contrôleur technique de la construction" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="controleur-technique-de-la-construction" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/controleur-technique-de-la-construction.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="controleur-technique-de-la-construction" -->
<!-- var(translation)="None" -->

# Contrôleur technique de la construction

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le contrôleur technique a pour mission de prévenir tous les aléas techniques qui pourraient intervenir lors de la réalisation d'un ouvrage.

Il veillera à la solidité des ouvrages, à la sécurité des personnes qui l'occuperont, au respect de la réglementation relative aux personnes à mobilité réduite ou aux performances énergétiques.

*Pour aller plus loin* : article L. 111-23 du Code de la construction et de l'habitation ; décret n° 99-443 du 28 mai 1999 relatif au cahier des clauses techniques générales applicables aux marchés publics de contrôle technique.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée.

- pour une activité commerciale, le CFE compétent sera la chambre de commerce et d'industrie (CCI) ;
- en cas de création d'une entreprise individuelle, il s'agit de l'Urssaf ;
- pour les sociétés civiles, le CFE compétent est le greffe du tribunal de commerce ou le greffe du tribunal d'instance dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale, quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

La profession de contrôleur technique est réservée aux titulaires disposant d'un agrément ministériel (cf. infra « 3°. a. Solliciter un agrément ») et des qualifications professionnelles suivantes :

- pour le personnel d'encadrement opérationnel et les ingénieurs :
  - soit être titulaire d'un diplôme de niveau postsecondaire en bâtiment ou génie civil justifiant au moins quatre années d'études, et avoir une expérience pratique d'au moins trois ans dans la conception, la réalisation, le contrôle technique ou l’expertise de constructions,
  - soit avoir une expérience pratique de six ans dans le domaine ;
- pour le personnel d'exécution des missions :
  - soit être titulaire d'un certificat d'études secondaires dans le domaine d'activité envisagé, et une pratique d'au moins trois ans dans la conception, la réalisation, le contrôle technique ou l’expertise de construction,
  - soit avoir une expérience pratique de six ans dans le domaine.

*Pour aller plus loin* : article R. 111-32-2 du Code de la construction et de l'habitation.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services (LPS) ou Libre ÉtablisseÉtablissement (LE))

#### En vue d'un exercice temporaire et occasionnel (LPS)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) exerçant l'activité de contrôleur technique peut exercer à titre temporaire et occasionnel, la même activité en France.

Pour cela, l'intéressé doit effectuer, avant sa première prestation, une déclaration préalable auprès du ministre chargé de la construction (cf. infra « 3°. b. Déclaration préalable pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel »).

En outre, lorsque ni l'accès à l'activité ni son exercice ne sont réglementés dans cet État, le ressortissant doit justifier avoir exercé cette activité pendant au moins deux ans au cours des dix années précédant sa première prestation.

*Pour aller plus loin* : article L. 111-25 du Code de la construction et de l'habitation.

#### En vue d'un exercice permanent (LE)

Tout ressortissant légalement établi et exerçant l'activité de contrôleur technique dans un État de l'UE ou de l'EEE peut exercer à titre permanent la même activité en France. Pour cela, le professionnel doit solliciter l'agrément auprès du ministre chargé de la construction, au même titre que le ressortissant français (cf. infra « 3°. a. Solliciter un agrément »).

En cas de différences substantielles entre ses qualifications professionnelles et celles requises en France, le ressortissant sera auditionné par la commission d'agrément et devra démontrer ses compétences et ses connaissances dans le domaine de la construction.

*Pour aller plus loin* : article L. 111-25 du Code de la construction et de l'habitat.

### c. Conditions d’honorabilité

L'activité de contrôleur technique est incompatible avec les activités de conception, d'exécution ou d'expertise d'ouvrage. En outre, le contrôleur technique devra agir avec impartialité et ne pas porter atteinte à l'indépendance des personnes exerçant les activités citées ci-avant.

*Pour aller plus loin* : article R. 111-31 du Code de la construction et de l'habitat.

### d. Quelques particularités de la réglementation de l’activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

#### Souscrire à une assurance de responsabilité décennale

Dans le cadre de sa mission, le contrôleur technique devra souscrire une assurance de responsabilité décennale. Il sera tenu pour responsable des dommages qui compromettront la solidité de l'ouvrage ou le rendront impropre à sa destination.

*Pour aller plus loin* : article L. 111-24 du Code de la construction et de l'habitation.

#### Rapport d'activités

Le contrôleur technique doit transmettre un rapport d'activité aux services du ministre chargé de la construction tous les 31 mars de chaque année. Le rapport devra comporter les renseignements suivants :

- l'activité annuelle globale du contrôleur ;
- ses effectifs ;
- l'indication des améliorations que le contrôleur estime avoir apporté lors de l'exercice de son activité ;
- une description des sinistres et des malfaçons qu'il a relevés ;
- les opérations pour lesquelles il a eu recours à un sous-traitant.

*Pour aller plus loin* : article 3 de l'arrêté du 26 novembre 2009 fixant les modalités pratiques d'accès à l'exercice de l'activité de contrôleur technique.

## 3°. Démarches et formalités d’installation
 
### a. Solliciter un agrément

#### Autorité compétente

Le ministre chargé de la construction est compétent pour délivrer l'agrément dont la durée de validité est de cinq ans renouvelable.

#### Pièces justificatives

La demande d'agrément se fait par le dépôt d'un dossier comprenant les pièces suivantes :

- l'état civil du demandeur ainsi que l'adresse de son domicile ;
- la justification de la compétence théorique et de l'expérience pratique du personnel de direction, l'organisation interne de la direction technique, les règles d'assistance aux services opérationnels chargés effectivement du contrôle et les critères d'embauche ou d'affectation des agents ;
- l'engagement que le contrôleur agira avec impartialité et indépendance ;
- l'engagement que le contrôleur portera à l'attention de l'administration toute modification des renseignements qu'il a donné pour l'obtention de l'agrément ;
- le cas échéant, tous les agréments qu'il a obtenu précédemment dans le domaine de la construction ;
- l'étendue de l'agrément demandé selon la ou les catégories de construction d'ouvrage, et la nature ou l'importance des aléas qui pourront en découler.

#### Procédure

Le dossier sera soumis à la commission d'agrément qui s'assurera de la conformité des pièces. Elle auditionnera le demandeur avant de délibérer sur les suites à donner à sa demande. Lorsqu'elle rend un avis favorable, elle en informera le ministre qui délivrera l'agrément dans un délai de six mois à compter de la réception du dossier complet. Le silence gardé dans ce délai vaudra refus de la demande.

*Pour aller plus loin* : articles R.111-29 et R.111-32 du Code de la construction et de l'habitation ; annexe III de l'arrêté du 26 novembre 2009 fixant les modalités pratiques d'accès à l'exercice de l'activité de contrôleur technique ; [lien vers le secrétariat de la commission d'agrément](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### b. Déclaration préalable pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ministre chargé de la construction est compétent pour se prononcer sur la déclaration préalable d'activité.

#### Pièces justificatives

Cette demande peut être envoyée sous format électronique ou par courrier et prend la forme d'un dossier comportant les pièces justificatives suivantes :

- l'état civil du ressortissant ainsi que l'adresse de son domicile ;
- une attestation justifiant qu'il est légalement établi dans un État de l'UE ou de l'EEE et qu'il n'encourt pas d'interdiction d'exercer ;
- tout document justifiant ses qualifications professionnelles ;
- tout document justifiant qu'il a exercé cette activité pendant au moins deux ans au cours des dix dernières années dès lors que l’État de l'UE ou de l'EEE dans lequel il est établi ne réglemente pas cette activité ;
- l'engagement que le ressortissant agira avec impartialité et indépendance ;
- la nature de la prestation qu'il envisage de faire et sa date de début et de fin ;
- une attestation d'assurance de responsabilité adaptée à la prestation qu'il envisage de faire.

#### Procédure

Le ministre chargé de la construction disposera d'un délai d'un mois pour vérifier le dossier et autoriser la prestation, après avis favorable de la commission d'agrément. En cas de différences substantielles entre les qualifications professionnelles du ressortissant et celles exigées en France, le ministre pourra demander au ressortissant de se présenter devant la commission et démontrer ses connaissances et ses compétences dans le domaine de la construction.

Le silence de l'autorité compétente après réception du dossier, dans un délai d'un mois, vaudra acceptation de la demande.

*Pour aller plus loin* : articles R. 111-29-1 et R. 111-32-1 du Code de la construction et de l'habitation ; [lien vers le secrétariat de la commission d'agrément](https://www.cohesion-territoires.gouv.fr/exercer-le-metier-de-controleur-technique-de-la-construction).

### c. Formalités de déclaration de l’entreprise

Le professionnel est tenu de s'immatriculer au registre du commerce et des sociétés (RCS) auprès de la CCI en cas de création d'une société commerciale ou effectuer une déclaration d'activité auprès de l'Urssaf en cas de création d'une entreprise individuelle.

## 4°. Textes de référence

- Articles L. 111-23 à L. 111-26 du code de la construction et de l'habitation (Code de la construction et de l’habitation) ;
- Articles R. 111-29 à R. 111-42 du Code de la construction et de l’habitation ;
- Arrêté du 26 novembre 2009 fixant les modalités pratiques d'accès à l'exercice de l'activité de contrôleur technique ;
- Décret n° 99-443 du 28 mai 1999 relatif au cahier des clauses techniques générales applicables aux marchés public de contrôle technique ;
- NF P 03-100.