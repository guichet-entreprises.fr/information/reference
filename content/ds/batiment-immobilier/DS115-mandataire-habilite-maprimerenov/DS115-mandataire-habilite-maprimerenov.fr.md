﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS115" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Mandataire habilité MaPrimeRénov'" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="mandataire-habilite-maprimerenov" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/mandataire-habilite-maprimerenov.html" -->
<!-- var(last-update)="2022-08" -->
<!-- var(url-name)="mandataire-habilite-maprimerenov" -->
<!-- var(translation)="None" -->

# Mandataire habilité « MaPrimeRénov' »

Dernière mise à jour : <!-- begin-var(last-update) -->Août 2022<!-- end-var -->

## Définition de l'activité

Lancée le 1er janvier 2020, MaPrimeRénov’ est une aide de l’État pour les travaux de rénovation énergétique, accessible à tous les propriétaires, qu’ils occupent ou louent leur logement. MaPrimeRénov’ est accessible à l’ensemble des propriétaires qu’ils soient propriétaires occupants, propriétaires bailleurs ou copropriétaires. 

Les demandeurs de MaPrimeRénov’ souhaitant déléguer la gestion de leurs démarches peuvent faire appel à un tiers en recourant à un mandataire. En effet, pour faciliter le dépôt de dossiers auprès de l'Agence nationale de l'habitat (ci-après l'ANAH), la France a ouvert la possibilité à des entreprises d'être mandatées par les particuliers pour réaliser les démarches administratives auprès de cette agence. Lorsqu’il se voit confier un mandat par un particulier éligible à la prime, le mandataire est chargé à titre gratuit de réaliser pour ce dernier les démarches administratives auprès de l’ANAH, c’est-à-dire de constituer son dossier, demander la prime et, le cas échéant, percevoir les fonds pour son compte. Le mandataire peut être une personne physique ou morale. Cette activité n’est nullement exclusive d’une autre activité. 

Pour alléger les charges administratives pesant sur les entreprises déposant de nombreux dossiers chaque année, la France a adopté plusieurs textes permettant à un mandataire habilité d'avoir un accès simplifié au dispositif d'instruction des dossiers MaPrimeRénov’ de l'ANAH. L’exercice de cette activité suppose, en plus d’être préalablement mandataire MaPrimeRénov’, une  habilitation par l’ANAH. L’article 4 du décret n° 2021-344 du 29 mars 2021 relatif à l'habilitation de mandataires dans le cadre de la prime de transition énergétique liste les objectifs et les obligations à la charge du mandataire habilité, il permet ainsi de proposer au demandeur un accès simplifié à la prime.

## Qualifications professionnelles requises en France

Aucun diplôme spécifique n'est requis pour être mandataire habilité « MaPrimeRénov' » auprès de l’ANAH. Cependant, le demandeur de l’habilitation doit justifier de sa capacité à :

- déposer des dossiers dont il s'assure qu'ils sont conformes à la réglementation en vigueur, cette conformité étant analysée dans le cadre du plan de contrôle de l'Agence nationale de l'habitat visé à l'article 7 du décret n° 2021-344 du 29 mars 2021 relatif à l'habilitation de mandataires dans le cadre de la prime de transition énergétique ; 
- traiter, avant sa demande d'habilitation, un volume minimum de dossiers déposés en tant que mandataire dans les quatre derniers mois et ayant abouti à l'octroi d'une prime de transition énergétique, ce volume minimum étant fixé par arrêté conjoint des ministres chargés du logement, de l'énergie, de l'économie et du budget ;
- apporter une information et un accompagnement sécurisé au demandeur conformément à l'article 3 du présent décret, par le biais d'un traitement sécurisé des données conformément à la réglementation relative à la protection des données. 

Ainsi, le demandeur justifie au dépôt de son dossier du nombre et de la compétence de ses collaborateurs intervenant sur le dispositif et, chaque année, des moyens humains et financiers mis en œuvre.

## Particularités de la réglementation de l’activité

### Déontologie

Le demandeur, personne physique, ou le représentant légal du demandeur, personne morale, doit justifier n’avoir pas fait l’objet au cours des cinq années précédant la demande :

- de l'une des condamnations définitives mentionnées à l'article L. 2141-1 du Code de la commande publique ;
- d'une condamnation définitive pour avoir commis l'infraction prévue à l'article L. 132-14 du Code de la consommation ;
- d'une condamnation définitive pour avoir commis l'infraction prévue à l'article 1741 du Code général des impôts ;
- d'une interdiction d'exercer une activité professionnelle ou commerciale prononcée en application des dispositions du Code de commerce ;
- de la sanction pécuniaire prévue au 1° de l'article L. 222-2 du Code de l'énergie, lorsqu'elle est supérieure ou égale à 3 % du chiffres d'affaire hors taxes du dernier exercice clos ;
- de la sanction visée au 2° de l'article L. 222-2 du Code de l'énergie ;
- de la sanction visée au 4° de l'article L. 222-2 du Code de l'énergie, lorsque la durée de la suspension est supérieure ou égale à douze mois et qu'elle porte sur des opérations de rénovation énergétique de bâtiments résidentiels au bénéfice de personnes physiques. Le directeur général de l'Agence nationale de l'habitat peut décider de déroger à la prise en compte de cette sanction dans le processus d'habilitation. Le projet de décision est alors notifié préalablement aux ministres chargés de l'énergie, du logement, de l'économie et du budget, qui peuvent s'y opposer dans un délai de quinze jours.

### Conditions d’honorabilité

Pour exercer son activité le mandataire habilité « MaPrimeRénov' », s’il s’agit d’un professionnel, ne doit pas présenter de retard de paiement relatif à la part patronale des cotisations sociales.

Le demandeur verse au dossier de demande ou de renouvellement d’habilitation les éléments démontrant qu’il est en capacité :

- d’assurer une chaîne commerciale, contractuelle et assurantielle respectueuse des droits du consommateur, en excluant notamment tout démarchage abusif ;
- de contrôler les chantiers financés ainsi que les professionnels réalisant les travaux ; 
- d’apporter une information sur l'ensemble des engagements et porter à la connaissance du demandeur la réglementation relative à la prime, notamment les règles de commencement et d'achèvement des travaux et les conditions générales d'utilisation de la plateforme de dépôt de la demande. Le cas échéant, le demandeur porte à la connaissance de l'Agence nationale de l'habitat les sanctions et condamnations listées à l'article 1er du susmentionné décret dont il a fait l'objet. 

Par ailleurs, le mandataire s'engage auprès de l'Agence nationale de l'habitat, à : 

- lui reverser le montant des primes indûment perçues pour le compte du mandant à l'issue d'une décision de retrait ou de reversement prononcé par l'Agence, et du fait d'un manquement imputable au mandataire ou à l'un de ses sous-traitants lorsque ceux-ci assurent l'accompagnement administratif du demandeur ; 
- l'informer des modifications de statut et d'organisation, ou de celles de ses sous-traitants, dès lors que ces changements sont susceptibles d'avoir une incidence sur l'habilitation ainsi que sur le respect des engagements prévus au présent article ; 
- appliquer la réglementation relative à la lutte contre la corruption, la prévention des risques d'atteinte à la probité et à la protection des données ; 
- déclarer les éventuels recours à une ou plusieurs entreprise(s) dans le cadre d'un partenariat ou contrat de sous-traitance régi par la loi du 31 décembre 1975 ; 
- ne pas demander l'avance visée à l'article 6 de l'arrêté du 14 janvier 2020 relatif à la prime de transition énergétique.

### Interdictions

L'accès simplifié à la prime est proposé à titre gratuit au demandeur de cette prime. Le demandeur, personne physique, ou le représentant légal du demandeur, personne morale, doit justifier n’avoir pas fait l’objet au cours des cinq années précédant la demande :

- de l'une des condamnations définitives mentionnées à l'article L. 2141-1 du Code de la commande publique ;
- d'une condamnation définitive pour avoir commis l'infraction prévue à l'article L. 132-14 du Code de la consommation ;
- d'une condamnation définitive pour avoir commis l'infraction prévue à l'article 1741 du Code général des impôts ;
- d'une interdiction d'exercer une activité professionnelle ou commerciale prononcée en application des dispositions du Code de commerce ;
- de la sanction pécuniaire prévue au 1° de l'article L. 222-2 du Code de l'énergie, lorsqu'elle est supérieure ou égale à 3 % du chiffres d'affaire hors taxes du dernier exercice clos ;
- de la sanction visée au 2° de l'article L. 222-2 du Code de l'énergie ;
- de la sanction visée au 4° de l'article L. 222-2 du Code de l'énergie, lorsque la durée de la suspension est supérieure ou égale à douze mois et qu'elle porte sur des opérations de rénovation énergétique de bâtiments résidentiels au bénéfice de personnes physiques. Le directeur général de l'Agence nationale de l'habitat peut décider de déroger à la prise en compte de cette sanction dans le processus d'habilitation. Le projet de décision est alors notifié préalablement aux ministres chargés de l'énergie, du logement, de l'économie et du budget, qui peuvent s'y opposer dans un délai de quinze jours. 

### Assurance

Le professionnel exerçant l'activité de mandataire habilité « MaPrimeRénov' » est tenu de souscrire une assurance responsabilité civile professionnelle.

### Transmission d’informations à l’Administration

Le mandataire habilité « MaPrimeRénov' » doit :

- informer l’ANAH des modifications de statut et d'organisation, ou de celles de ses sous-traitants, dès lors que ces changements sont susceptibles d'avoir une incidence sur l'habilitation ainsi que sur le respect des engagements prévus à l’article 4 du décret n° 2021-344 du 29 mars 2021 relatif à l'habilitation de mandataires dans le cadre de la prime de transition énergétique ;
- déclarer auprès de l’ANAH les éventuels recours à une ou plusieurs entreprises dans le cadre d'un partenariat ou contrat de sous-traitance régi par la loi du 31 décembre 1975 ;
- transmettre chaque année un bilan annuel de cette activité et le descriptif des moyens humains et financiers mis en œuvre.

## Démarche pour exercer l’activité de mandataire habilité « MaPrimeRénov' »

Pour obtenir l'accès simplifié, le demandeur de l’habilitation doit être enregistré en tant que mandataire auprès de l’ANAH. Les mandataires doivent présenter lors de leur demande d’habilitation des garanties suffisantes et cumulatives attestant de leur expérience, de leurs capacités professionnelles, de leur déontologie et leur honorabilité. L’habilitation est accordée pour une durée de 3 ans à compter de sa notification et est valable sur l’ensemble du territoire national.

### Autorité compétente

La demande d’habilitation doit être adressée à l’ANAH. Pour pouvoir devenir mandataire habilité, le candidat doit demander la création de son compte auprès de l’ANAH et obtenir un numéro d’immatriculation. Pour cela, il devra au préalable envoyer un formulaire de demande de création de compte et les pièces justificatives nécessaires, par courrier électronique à l’adresse [maprimerenov.mandataire@anah.fr](mailto:maprimerenov.mandataire@anah.fr). Le site [maprimerenov.gouv.fr](https://www.maprimerenov.gouv.fr/prweb/PRAuth/app/AIDES_/BPNVwCpLW8TKW49zoQZpAw*/!STANDARD) centralise les informations nécessaires au demandeur.

### Procédure

Seule une personne ayant déjà obtenu le statut de mandataire « MaPrimeRénov' » auprès de l’ANAH peut obtenir le statut de mandataire habilité. Pour obtenir, conserver ou renouveler son habilitation auprès de l’ANAH, le mandataire dépose un dossier qui :

- démontre le respect de certaines conditions, de déontologie et d’honorabilité notamment. Dans le cas d'une demande d'habilitation présentée par une personne morale, ces conditions s'appliquent au représentant légal ;
- justifie de sa capacité à traiter avec succès un volume important de dossiers (1 500) de demande « MaPrimeRénov' » en tant que mandataire. 

Lorsque le candidat est établi dans un autre État membre de l'Union européenne, il transmet à l'appui de sa demande les justificatifs équivalents à ceux disponibles dans ledit État. Les documents et informations sont traduits en langue française.

### Pièces justificatives

Pour bénéficier de l’habilitation, outre les pièces et informations mentionnées à l'annexe 4 de l'arrêté du 14 janvier 2020 relatif à la prime de transition écologique, le demandeur de l’habilitation doit fournir à l’ANAH les documents et informations listés au sein de l’article 1 de l’arrêté du 29 mars 2021 relatif à l'habilitation de mandataires dans le cadre de la prime de transition énergétique. Ces éléments sont relatifs à l’identité du demandeur de l’habilitation et à sa capacité à exercer l’activité de mandataire habilité « MaPrimeRénov' ».

### Délais de réponse

Après le dépôt de la demande, l’ANAH dispose d’un délai de deux mois après réception du dossier pour l’instruire et décider d’habiliter ou non le mandataire. Le refus doit être justifié et, en l'absence de réponse dans le délai des deux mois, l'autorisation est implicitement accordée. En cas de rejet de la demande d'habilitation ou de retrait de celle-ci, le mandataire ne peut déposer de nouvelle demande d'habilitation avant un délai d'un an.

### Coût

La déclaration est gratuite.

### Voies de recours

L'introduction d'un recours contentieux afférent aux décisions relatives à l'habilitation des mandataires est subordonnée à l'exercice préalable d'un recours administratif auprès du directeur général de l’ANAH. Ce recours administratif est régi par les dispositions des chapitres Ier et II du titre Ier du livre IV du Code des relations entre le public et l'administration.

## Démarche de contrôle par l’ANAH de l’activité de mandataire habilité « MaPrimeRénov' » et de retrait éventuel de l’habilitation

### Autorité compétente

L’ANAH peut contrôler ou faire contrôler, sur pièces et sur place, le bénéficiaire de l'habilitation en vue de s'assurer d'une part de l'authenticité des documents et informations transmis à l'appui de la demande d'habilitation ou de renouvellement, et d'autre part du respect des engagements et obligations qui s'imposent au bénéficiaire de l'habilitation. 

En cas de manœuvres frauduleuses, ou de non-respect des engagements mentionnés à l'article 4 du décret n° 2021-344 du 29 mars 2021 relatif à l'habilitation de mandataires dans le cadre de la prime de transition énergétique, le directeur général de l’ANAH peut procéder au retrait de l'habilitation, après avoir invité le bénéficiaire à présenter ses observations dans le cadre d'une procédure contradictoire.

### Procédure

La date du contrôle du mandataire habilité par l’ANAH est notifiée par lettre recommandée avec accusé de réception au mandataire au moins vingt-quatre heures avant celui-ci. L'entrave à la réalisation des contrôles peut entraîner le retrait de l'habilitation. 

Un rapport de contrôle est communiqué au mandataire, qui peut présenter ses observations écrites dans un délai d'un mois. 

Une procédure contradictoire est mise en place avec le mandataire habilité dans le cas où le retrait de son habilitation est envisagé sur la base des motifs exposés par le directeur général de l’ANAH auprès de l’intéressé.

L'introduction d'un recours contentieux afférent aux décisions relatives à l'habilitation des mandataires est subordonnée à l'exercice préalable d'un recours administratif auprès du directeur général de l'Agence nationale de l'habitat. Ce recours administratif est régi par les dispositions des chapitres Ier et II du titre Ier du livre IV du Code des relations entre le public et l'administration. 

### Voies de recours

Un rapport de contrôle est communiqué au mandataire, qui peut présenter ses observations écrites dans un délai d'un mois. 

Une procédure contradictoire est mise en place avec le mandataire habilité dans le cas où le retrait de son habilitation est envisagé sur la base des motifs exposés par le directeur général de l’ANAH auprès de l’intéressé.

L'introduction d'un recours contentieux afférent aux décisions relatives à l'habilitation des mandataires est subordonnée à l'exercice préalable d'un recours administratif auprès du directeur général de l'Agence nationale de l'habitat. Ce recours administratif est régi par les dispositions des chapitres Ier et II du titre Ier du livre IV du Code des relations entre le public et l'administration. 

## Liens utiles

### Textes de référence

- Loi n° 2019-1479 du 28 décembre 2019 de finances pour 2020, notamment son article 15 ;
- Décret n° 2020-26 du 14 janvier 2020 relatif à la prime de transition énergétique ;
- Arrêté du 14 janvier 2020 relatif à la prime de transition énergétique ;
- Décret n° 2021-344 du 29 mars 2021 relatif à l'habilitation de mandataires dans le cadre de la prime de transition énergétique ;
- Arrêté du 29 mars 2021 relatif à l'habilitation de mandataires dans le cadre de la prime de transition énergétique. 

### Autres liens utiles

- [Site de l’ANAH](https://www.maprimerenov.gouv.fr/prweb/PRAuth/app/AIDES_/BPNVwCpLW8TKW49zoQZpAw*/!STANDARD), à la rubrique permettant le dépôt du dossier pour devenir mandataire habilité


