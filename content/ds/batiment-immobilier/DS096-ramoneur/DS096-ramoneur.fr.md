﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS096" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Ramoneur" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="ramoneur" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/ramoneur.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="ramoneur" -->
<!-- var(translation)="None" -->

# Ramoneur

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Professionnel chargé d’assurer l’entretien des conduits de cheminées et le nettoyage des âtres, fourneaux, incinérateurs, chaudières, gaines de ventilation et dispositifs d’évacuation des fumées. Le ramonage concerne des conduits d’évacuation variés : gaz, bois, fioul, gaines grasses, dispositifs de ventilation mécanique contrôlée (VMC), etc.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- en cas de création d’une entreprise individuelle, le CFE compétent est l’Urssaf ;
- en cas d’activité artisanale, le CFE compétent est la chambre de métiers et de l’artisanat (CMA) ;
- en cas création d’une société commerciale, le CFE compétent est la chambre de commerce et de l’industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

L’intéressé souhaitant exercer l’activité de ramoneur doit disposer d’une qualification professionnelle ou exercer sous le contrôle effectif et permanent d’une personne disposant de cette qualification.

Cette qualification professionnelle consiste, par exemple, en :

- un certificat d’aptitude professionnelle (CAP) ;
- un brevet d’études professionnelles (BEP) ;
- un diplôme ou un titre égal ou supérieur homologué ou enregistré lors de sa délivrance au [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Le [site de la commission nationale de la certification professionnelle (CNCP)](http://www.cncp.gouv.fr/) propose une liste de l’ensemble des qualifications professionnelles envisageables.

À défaut de l’un de ces diplômes ou titres, l’intéressé doit justifier d’une expérience professionnelle de trois années effectives sur le territoire de l’Union européenne (UE) ou de l’Espace économique européen (EEE) acquise en qualité de dirigeant d’entreprise, de travailleur indépendant ou de salarié dans l’exercice du métier de ramoneur. Dans ce cas, il est conseillé à l’intéressé de s’adresser à la chambre des métiers et de l’artisanat (CMA) pour demander une attestation de reconnaissance de qualification professionnelle.

*Pour aller plus loin* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l’artisanat ; décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l’exercice des activités prévues à l’article 16 de la loi n° 96-603 du 5 juillet 1996 précitée.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### Pour une Libre Prestation de Services (LPS)

Le professionnel ressortissant de l’UE ou de l’EEE peut exercer en France, à titre temporaire et occasionnel, le contrôle effectif et permanent de l’activité de ramoneur à la condition d’être légalement établi dans un de ces États pour y exercer la même activité.

Si ni l’activité ni la formation y conduisant n’est réglementée dans l’État d’établissement, l’intéressé doit en outre prouver qu’il a exercé l’activité de ramoneur dans cet État pendant au moins l’équivalent de deux ans à temps plein au cours des dix dernières années précédant la prestation qu’il veut effectuer en France.

*Pour aller plus loin* : article 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée.

#### Pour un Libre Établissement (LE)

Pour exercer en France, à titre permanent, le contrôle effectif et permanent de l’activité de ramoneur, le professionnel ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- être titulaire d’une attestation de compétence ou d’un titre de formation requis pour l’exercice de l’activité de ramoneur dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’une attestation de compétence ou d’un titre de formation qui certifie sa préparation à l’exercice de l’activité de ramoneur lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de cette activité ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité de ramoneur dans l’État qui a admis l’équivalence.

**À noter**

Le ressortissant d’un État de l’UE ou de l’EEE qui remplit l’une des conditions précitées peut solliciter une attestation de reconnaissance de qualification professionnelle (cf. infra « 3°. b. Le cas échéant, demander une attestation de qualification professionnelle ».)

Si l’intéressé ne remplit aucune des conditions précitées, la chambre de métiers et de l’artisanat (CMA) saisie peut lui demander d’accomplir une mesure de compensation dans les cas suivants :

- si la durée de la formation attestée est inférieure d’au moins un an à celle exigée pour obtenir l’une des qualifications professionnelles requises en France pour exercer l’activité de ramoneur ;
- si la formation reçue porte sur des matières substantiellement différentes de celles couvertes par l’un des titres ou diplômes requis pour exercer en France l’activité de ramoneur ;
- si le contrôle effectif et permanent de l’activité de ramoneur nécessite, pour l’exercice de certaines de ses attributions, une formation spécifique qui n’est pas prévue dans l’État membre d’origine et porte sur des matières substantiellement différentes de celles couvertes par l'attestation de compétences ou le titre de formation dont le demandeur fait état.

*Pour aller plus loin* : articles 17 et 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée ; articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 précité.

#### Bon à savoir : mesures de compensation

La CMA, saisie d’une demande de délivrance d’une attestation de reconnaissance de qualification professionnelle, notifie au demandeur sa décision tendant à lui faire accomplir l’une des mesures de compensation. Cette décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est impérative pour exercer en France.

Le demandeur doit alors choisir entre un stage d’adaptation d’une durée maximale de trois ans ou une épreuve d’aptitude.

L’épreuve d’aptitude prend la forme d’un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA de la décision du demandeur d’opter pour cette épreuve. À défaut, la qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

À la fin du stage d’adaptation, le demandeur adresse à la CMA une attestation certifiant qu’il a valablement accompli ce stage, accompagnée d’une évaluation de l’organisme qui l’a encadré. La CMA délivre, sur la base de cette attestation, une attestation de qualification professionnelle dans un délai d’un mois.

La décision de recourir à une mesure de compensation peut être contestée par l’intéressé qui doit former un recours administratif auprès du préfet dans un délai de deux mois à compter de la notification de la décision. En cas de rejet de son recours, il peut alors initier un recours contentieux.

*Pour aller plus loin* : articles 3 et 3-2 du décret n° 98-246 du 2 avril 1998 précité ; article 6-1 du décret n° 83-517 du 24 juin 1983 fixant les conditions d’application de la loi 82-1091 du 23 décembre 1982 relative à la formation professionnelle des artisans.

### c. Conditions d’honorabilité

Nul ne peut exercer la profession s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévu au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 III de la loi n° 96-603 du 5 juillet 1996 précitée.

### d. Assurance de responsabilité civile obligatoire

Le professionnel doit souscrire une assurance responsabilité civile professionnelle. Elle lui permet d’être couvert pour les dommages causés à autrui qu’il en soit directement à l’origine ou qu’ils soient le fait de ses salariés, de ses locaux ou de son matériel.

Les artisans intervenant dans les travaux de gros œuvre et de construction sont soumis à l’obligation de souscrire une assistance de responsabilité civile décennale.

Les références du contrat d’assurance doivent apparaître sur les devis et factures de l’artisan.

*Pour aller plus loin* : article L. 241-1 du Code des assurances et article 22-2 de la loi n° 96-603 du 5 juillet 1996 précitée.

### e. Quelques particularités de la réglementation de l’activité

#### Respecter les règles d’accessibilité et de sécurité

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) ».

#### Information du consommateur

Préalablement à tous travaux, l’entreprise doit faire connaître au consommateur les informations suivantes :

- les taux horaires de main-d’œuvre TTC ;
- les modalités de décompte du temps passé ;
- les prix TTC des différentes prestations forfaitaires proposées ;
- les frais de déplacement, le cas échéant ;
- le caractère gratuit ou payant du devis et son coût, le cas échéant ;
- toute autre condition de rémunération.

Ces informations doivent être affichées de façon visible et lisible dans les locaux de l’entreprise si elle reçoit la clientèle. Si la prestation est offerte sur le lieu de l’intervention, ces informations doivent être présentées au client préalablement à tout travail dans un document écrit.

*Pour aller plus loin* : article 2 de l’arrêté du 2 mars 1990 relatif à la publicité des prix des prestations de dépannage, de réparation et d’entretien dans le secteur du bâtiment et de l’équipement de la maison.

#### Publicité

Toute publicité écrite doit comporter les mentions suivantes :

- le nom, la raison sociale et l’adresse de l’entreprise ;
- son numéro d’inscription au registre du commerce et des sociétés (RCS) ou au répertoire des métiers ;
- les taux horaires de main-d’œuvre TTC pratiqués pour chaque catégorie de prestations concernée ou les prix unitaires ;
- les frais de déplacement si l’entreprise se rend au domicile du consommateur ;
- le caractère payant ou non du devis ;
- toute autre condition de rémunération, le cas échéant.

*Pour aller plus loin* : article 3 de l’arrêté du 2 mars 1990 précité.

#### Obligation d’établir une note

Le professionnel doit adresser au consommateur une note une fois la prestation effectuée et avant le paiement du prix. De plus, il fait signer au consommateur une décharge pour les pièces, éléments ou appareils remplacés dont ce dernier a refusé la conservation.

*Pour aller plus loin* : article 5 de l’arrêté du 2 mars 1990 précité et arrêté n° 83-50/A du 3 octobre 1983 relatif à la publicité des prix de tous les services.

#### Conduite d’engins automoteurs et d’appareils de levage

Les salariés de l’entreprise ne peuvent conduire des engins automoteurs et appareils de levage (grues, chariots autoporteurs, plateformes élévatrices, etc.) que s’ils ont reçu une autorisation de conduite délivrée par l’employeur.

Pour délivrer cette autorisation, l’employeur procède à une évaluation du travailleur prenant en compte :

- un examen d’aptitude à la conduite réalisé par le médecin du travail ;
- un contrôle des connaissances et du savoir-faire de l’opérateur pour la conduite en sécurité de l’équipement de travail. L’obtention du certificat d’aptitude à la conduite en sécurité (Caces) permet d’attester du contrôle des connaissances et du savoir-faire du salarié. Ce certificat doit être délivré par des organismes testeurs certifiés ;
- un contrôle des connaissances des lieux et des instructions à respecter sur le ou les sites d’utilisation.

*Pour aller plus loin* : article R. 4323-56 du Code du travail et arrêté du 2 décembre 1998 relatif à la formation à la conduite des équipements de travail mobiles autoporteurs et des appareils de levage de charges ou de personnes.

## 3°. Démarches et formalités d’installation

### a. Le cas échéant, demander une attestation de reconnaissance de qualification professionnelle

L’intéressé souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de reconnaissance de qualification professionnelle.

#### Autorité compétente

La demande doit être adressée à la CMA territorialement compétente.

#### Procédure

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les 15 jours du dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

#### Pièces justificatives

Le dossier doit contenir :

- la demande d’attestation de qualification professionnelle ;
- le justificatif de la qualification professionnelle : une attestation de compétences ou le diplôme ou titre de formation professionnelle ;
- la preuve de la nationalité du demandeur ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE : une attestation portant sur la nature et de la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France : les justificatifs de l’exercice de l’activité pendant trois années.

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou l’expérience professionnelle du demandeur pour déterminer s’il existe d’éventuelles différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du centre international d’études pédagogiques (Ciep) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme ou d’un certificat ou titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français par un traducteur agréé.

#### Délai de réponse

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA reconnaît la qualification professionnelle et délivre l’attestation de qualification professionnelle ;

- décide de soumettre le demandeur à une mesure de compensation et lui notifie cette décision ;
- refuse de délivrer l’attestation de qualification professionnelle.

En l’absence de décision dans le délai de quatre mois, la demande d’attestation de reconnaissance de qualification professionnelle est réputée acquise.

#### Voies de recours

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier un recours contentieux devant le tribunal administratif compétent dans les deux mois suivant la notification du refus de la CMA. De même, si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours gracieux auprès du préfet du département dans lequel la CMA a son siège, dans les deux mois suivant la notification de la décision de la CMA. S’il n’obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 précité ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.

### b. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### c. Effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité ponctuelle (Libre Prestation de Services)

Les ressortissants de l’UE ou de l’EEE souhaitant exercer en France de manière occasionnelle et ponctuelle sont soumis à une déclaration préalable à sa première prestation sur le sol français.

Cette déclaration préalable d’activité doit être renouvelée tous les ans si l’intéressé entend exercer de nouveau en France.

#### Autorité compétente

La déclaration préalable d’activité est adressée à la CMA dans le ressort de laquelle le déclarant envisage de réaliser sa prestation.

#### Récépissé

La CMA délivre au déclarant un récépissé mentionnant la date à laquelle elle a reçu le dossier complet de déclaration préalable d’activité. Si le dossier est incomplet, la CMA notifie dans les quinze jours la liste des pièces manquantes au déclarant. Elle délivre le récépissé dès que le dossier est complet.

#### Pièces justificatives

La déclaration préalable d’activités doit comporter :

- la déclaration datée et signée, mentionnant les informations relatives aux assurances civiles professionnelles obligatoires ;
- la preuve de la nationalité du déclarant ;
- la preuve des qualifications professionnelles : titre de formation, attestation de compétence délivrée par l’autorité compétente dans l’État d’établissement, tout document attestant d’une expérience professionnelle indiquant sa nature et sa durée, etc ;
- le cas échéant, la preuve que le déclarant a exercé pendant au moins l'équivalent de deux ans à temps complet l’activité de ramoneur au cours des dix dernières années ;
- une attestation d’établissement dans un État de l’UE ou de l’EEE pour exercer l’activité de ramoneur ;
- une attestation de non-condamnation à une interdiction, même temporaire, d’exercer.

Tous les documents doivent être traduits en français (par un traducteur certifié) s’ils ne sont pas établis en français.

#### Délai

Dans un délai d’un mois à compter de la réception du dossier complet de déclaration préalable d’activité, la CMA délivre au déclarant une attestation de qualification professionnelle ou notifie la nécessité de procéder à un examen complémentaire. Dans ce dernier cas, la CMA notifie sa décision finale dans les deux mois à compter de la réception du dossier complet de déclaration préalable d’activité. À défaut de notification dans ce délai, la déclaration préalable est réputée acquise et la prestation de services peut donc débuter.

Pour appuyer sa décision, la CMA peut s’adresser à l’autorité compétente de l’État d’établissement du déclarant pour obtenir toute information concernant la légalité de l’établissement et son absence de sanction disciplinaire ou pénale à caractère professionnel.

**Bon à savoir**

Si la CMA relève une différence substantielle entre la qualification professionnelle requise pour exercer en France et celle déclarée par le prestataire et que cette différence est de nature à nuire à la santé ou à la sécurité du bénéficiaire du service, le déclarant est invité à se soumettre à une épreuve d’aptitude. S’il s’y refuse, la prestation de services ne peut être réalisée. L’épreuve d’aptitude doit être organisée dans un délai de trois mois à compter du dépôt du dossier complet de déclaration préalable d’activité. Si ce délai n’est pas respecté, la reconnaissance de qualification professionnelle est réputée acquise et la prestation de services peut débuter. Pour plus d’informations sur l’épreuve d’aptitude, se reporter supra à « Bon à savoir : les mesures de compensation ».

#### Recours

Toute décision de la CMA de soumettre le déclarant à une épreuve d’aptitude peut faire l’objet d’un recours administratif auprès du préfet du département où la CMA a son siège dans les trois mois suivant la notification de la décision de la CMA. Si le recours est infructueux, le déclarant peut alors initier un recours contentieux.

#### Coût

La déclaration préalable est gratuite. Cependant, si le déclarant participe à une épreuve d’aptitude, il pourra devoir participer aux frais d’organisation de cette mesure.

*Pour aller plus loin* : article 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée ; articles 3 et suivants du décret n° 98-246 du 2 avril 1998 précité ; articles 1er et 6 de l’arrêté du 28 octobre 2009 précité.