﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS017" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Plombier" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="plombier" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/plombier.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="plombier" -->
<!-- var(translation)="None" -->

# Plombier

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le plombier est un professionnel chargé de l’installation et de la réparation des canalisations d’eau et de gaz. Il prépare notamment la pose et la mise en service d’une installation sanitaire neuve (salle de bain, évier, piscine, etc.). Le plombier effectue également des remises en état sur des installations sanitaires anciennes et leurs équipements.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour une activité commerciale, le CFE compétent est la chambre de commerce et d’industrie (CCI).

Si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Seule une personne qualifiée professionnellement ou placée sous le contrôle effectif et permanent d’une personne qualifiée peut exercer l'activité de plombier.

La personne qui exerce ou qui en contrôle l’exercice, doit être titulaire au choix :

- d’un certificat d’aptitude professionnelle (CAP) ;
- d’un brevet d’études professionnelles (BEP) ;
- d’un diplôme ou d’un titre de niveau égal ou supérieur homologué ou enregistré lors de sa délivrance au Répertoire national des certifications professionnelles (RNCP).

À défaut de l’un de ces diplômes, l’intéressé devra justifier d’une expérience professionnelle de trois années effectives, dans un État de l’Union européenne (UE) ou partie à l’accord sur l’Espace économique européen (EEE), acquise en qualité de dirigeant d’entreprise, de travailleur indépendant ou de salarié dans l’exercice du métier de plombier. Dans ce cas, l’intéressé pourra effectuer une demande d’attestation de reconnaissance de qualification professionnelle auprès de la CMA compétente.

*Pour aller plus* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l’artisanat ; article 1 du décret n° 98-246 du 2 avril 1998 ; décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l’exercice des activités prévues à l’article 16 de la loi n° 96-603 du 5 juillet 1996.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### Pour une Libre Prestation de Services (LPS)

Le professionnel ressortissant de l’UE ou de l’EEE peut exercer en France, à titre temporaire et occasionnel, l'activité de plombier à la condition d’être légalement établi dans un de ces États pour y exercer la même activité.

Il devra au préalable en faire la demande par déclaration écrite adressée à la CMA compétente (cf. infra « 3°. b. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS) »).

Si ni l’activité ni la formation y conduisant n’est réglementée dans l’État dans lequel le professionnel est légalement établi, l’intéressé doit en outre prouver qu’il a exercé l’activité dans cet État pendant au moins un an à temps plein ou à temps partiel au cours des dix dernières années précédant la prestation qu’il veut effectuer en France.

**À noter**

Le professionnel qui répond à ces conditions est dispensé des exigences relatives à l’immatriculation au répertoire des métiers (RM) ou au registre des entreprises.

*Pour aller plus* : article 17-1 de la loi n° 96-603 du 5 juillet 1996.

#### Pour un Libre Établissement (LE)

Pour exercer en France, à titre permanent, l'activité de plombier, le professionnel ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- être titulaire d’une attestation de compétences ou d’un titre de formation requis pour l’exercice de l’activité dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’une attestation de compétences ou d’un titre de formation qui certifie sa préparation à l’exercice de l’activité lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès, ni l’exercice de cette activité ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité dans l’État qui a admis l’équivalence.

**À noter**

Le ressortissant d’un État de l’UE ou de l’EEE qui remplit l’une des conditions précitées peut solliciter une attestation de reconnaissance de qualification professionnelle (cf. infra « 3°. c. Le cas échéant, demander une attestation de qualification professionnelle ».)

Si l’intéressé ne remplit aucune des conditions précitées, la CMA saisie peut lui demander d’accomplir une mesure de compensation dans les cas suivants :

- si la durée de la formation attestée est inférieure d’au moins un an à celle exigée pour obtenir l’une des qualifications professionnelles requises en France pour exercer l’activité ;
- si la formation reçue porte sur des matières substantiellement différentes de celles couvertes par l’un des titres ou diplômes requis pour exercer en France l’activité ;
- si le contrôle effectif et permanent de l’activité nécessite, pour l’exercice de certaines de ses attributions, une formation spécifique qui n’est pas prévue dans l’État membre d’origine et porte sur des matières substantiellement différentes de celles couvertes par l’attestation de compétences ou le titre de formation dont le demandeur fait état.

*Pour aller plus* : articles 17 et 17-1 de la loi n° 96-603 du 5 juillet 1996 ; articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998.

**Bon à savoir : mesures de compensation**

La CMA, saisie d’une demande de délivrance d’une attestation de reconnaissance de qualification professionnelle, notifie au demandeur sa décision tendant à lui faire accomplir l'une des mesures de compensation. Cette décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est impérative pour exercer en France.

Le demandeur doit alors choisir entre un stage d’adaptation d’une durée maximale de trois ans ou une épreuve d’aptitude.

L’épreuve d’aptitude prend la forme d’un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA, de la décision du demandeur d’opter pour cette épreuve. À défaut, la qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

À l'issue du stage d’adaptation, le demandeur adresse à la CMA une attestation certifiant qu’il a valablement accompli ce stage, accompagnée d’une évaluation de l’organisme qui l’a encadré. La CMA délivre, sur la base de cette attestation, une attestation de qualification professionnelle dans un délai d’un mois.

La décision de recourir à une mesure de compensation peut être contestée par l’intéressé qui doit former un recours administratif auprès du préfet dans un délai de deux mois à compter de la notification de la décision. En cas de rejet de son recours, il peut alors initier un recours contentieux.

*Pour aller plus* : articles 3 et 3-2 du décret n° 98-246 du 2 avril 1998 ; article 6-1 du décret n° 83-517 du 24 juin 1983 fixant les conditions d’application de la loi 82-1091 du 23 décembre 1982 relative à la formation professionnelle des artisans.

### c. Conditions d'honorabilité et incompatibilités

Nul ne peut exercer la profession de plombier s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévue au 11° de l’article 131-6 du Code pénal.

*Pour aller plus* : article 19 de la loi n° 96-603 du 5 juillet 1996.

### d. Quelques particularités de la réglementation de l'activité

#### Réglementation relative à la qualité d’artisan et aux titres de maître artisan et de meilleur ouvrier de France

**La qualité d’artisan**

Pour se prévaloir de la qualité d’artisan, la personne doit justifier soit :

- d’un CAP, d’un BEP ou d’un titre homologué ou enregistré lors de sa délivrance au RNCP d’un niveau au moins équivalent (cf. supra « 2°. a. Qualifications professionnelles ») ;
- d’une expérience professionnelle dans ce métier de trois ans au moins.

*Pour aller plus* : article 1 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

**Le titre de maître artisan**

Ce titre est attribué aux personnes physiques, y compris les dirigeants sociaux des personnes morales :

- immatriculées au répertoire des métiers ;
- titulaires du brevet de maîtrise dans le métier exercé ;
- justifiant d’au moins deux ans de pratique professionnelle.

**À noter**

Les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan à la commission régionale des qualifications dans deux hypothèses :

- lorsqu’elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de niveau de formation au moins équivalent au brevet de maîtrise, qu’elles justifient de connaissances en gestion et en psychopédagogie équivalentes à celles des unités de valeur correspondantes du brevet de maîtrise et qu’elles ont deux ans de pratique professionnelle ;
- lorsqu’elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles disposent d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

*Pour aller plus* : article 3 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

**Le titre de meilleur ouvrier de France (MOF)**

Le diplôme professionnel « un des meilleurs ouvriers de France » est un diplôme d’État qui atteste l’acquisition d’une haute qualification dans l’exercice d’une activité professionnelle dans le domaine artisanal, commercial, industriel ou agricole.

Le diplôme est classé au niveau III de la nomenclature interministérielle des niveaux de formation. Il est délivré à l’issue d’un examen dénommé « concours un des meilleurs ouvriers de France » au titre d’une profession dénommée « classe », rattachée à un groupe de métiers.

Pour plus d’informations, il est recommandé de consulter le [site officiel du concours « un des meilleurs ouvriers de France »](http://www.meilleursouvriersdefrance.info/).

*Pour aller plus* : article D. 338-9 du Code de l’éducation.

#### Respecter la réglementation relative à la publicité des prix (ERP)

Le plombier doit rendre facilement accessible au public les informations relatives :

- au taux horaire de main-d'œuvre toutes taxes comprises (TTC) ;
- aux modalités de décompte du temps estimé ;
- le cas échéant, aux prix TTC des différentes prestations forfaitaires proposées ;
- le cas échéant, aux frais de déplacement ;
- au caractère payant ou gratuit du devis et, le cas échéant, au coût d'établissement du devis ;
- le cas échéant, à toute autre condition de rémunération.

*Pour aller plus* : arrêté du 24 janvier 2017 relatif à la publicité des prix des prestations de dépannage, de réparation et d'entretien dans le secteur du bâtiment et de l'équipement de la maison

## 3°. Démarches et formalités d’installation

### a. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

**Autorité compétente**

La CMA du lieu dans lequel le ressortissant souhaite réaliser la prestation, est compétente pour délivrer la déclaration préalable d'activité.

**Pièces justificatives**

La demande de déclaration préalable d'activité est accompagnée d'un dossier complet comprenant les pièces justificatives suivantes :

- une photocopie d'une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE ;
- un document justifiant la qualification professionnelle du ressortissant qui peut être, au choix :
  - une copie d'un diplôme, titre ou certificat,
  - une attestation de compétences,
  - tout document attestant de l'expérience professionnelle du ressortissant.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

**À noter**

Lorsque le dossier est incomplet, la CMA dispose d'un délai de quinze jours pour en informer le ressortissant et demander l'ensemble des pièces manquantes.

**Issue de la procédure**

À réception de l'ensemble des pièces du dossier, la CMA dispose d'un délai d'un mois pour décider :

- soit d'autoriser la prestation lorsque le ressortissant justifie d'une expérience professionnelle de trois ans dans un État de l'UE ou de l'EEE, et de joindre à cette décision une attestation de qualification professionnelle ;
- soit d'autoriser la prestation lorsque les qualifications professionnelles du ressortissant sont jugées suffisantes ;
- soit de lui imposer une épreuve d'aptitude lorsqu'il existe des différences substantielles entre les qualifications professionnelles du ressortissant et celles exigées en France. En cas de refus d'accomplir cette mesure de compensation ou en cas d'échec dans son exécution, le ressortissant ne pourra pas effectuer la prestation de services en France.

Le silence gardé de l'autorité compétente dans ce délai vaut autorisation de débuter la prestation de services.

*Pour aller plus* : article 2 du décret du 2 avril 1998 ; article 2 de l'arrêté du 17 octobre 2017 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre Ier du décret n° 98-247 du 2 avril 1998.

### b. Le cas échéant, demander une attestation de reconnaissance de qualification professionnelle

L’intéressé souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de reconnaissance de qualification professionnelle.

**Autorité compétente**

La demande doit être adressée à la CMA territorialement compétente.

**Procédure**

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les quinze jours du dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

**Pièces justificatives**

Le dossier doit contenir les pièces suivantes :

- la demande d’attestation de qualification professionnelle ;
- une attestation de compétences ou le diplôme ou titre de formation professionnelle ;
- la preuve de la nationalité du demandeur ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE, une attestation portant sur la nature et de la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France, les justificatifs de l’exercice de l’activité pendant trois années.

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du Centre international d’études pédagogiques (CIEP) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme ou d’un certificat ou d’un titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français (traduction agréée).

**Délai de réponse**

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut :

- reconnaître la qualification professionnelle et délivrer l’attestation de qualification professionnelle ;
- décider de soumettre le demandeur à une mesure de compensation et lui notifier cette décision ;
- refuser de délivrer l’attestation de qualification professionnelle.

En l’absence de décision dans le délai de quatre mois, la demande d’attestation de qualification professionnelle est réputée acquise.

**Voies de recours**

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier, dans les deux mois suivant la notification du refus de la CMA, un recours contentieux devant le tribunal administratif compétent. De même, si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours gracieux auprès du préfet du département dans lequel la CMA a son siège, dans les deux mois suivant la notification de la décision de la CMA. S’il n’obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.

### c. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).