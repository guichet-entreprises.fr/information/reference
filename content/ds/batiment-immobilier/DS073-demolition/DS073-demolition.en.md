﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS073" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Demolition" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="demolition" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/demolition.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="demolition" -->
<!-- var(translation)="Auto" -->

Demolition
==========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Demolition is an activity specializing in the deconstruction and destruction of buildings.

Several techniques are used, such as blasting, wrecking ball, thermal spear and human force through mass among others.

Deconstruction will often be preferred to destruction because it allows the building's construction elements to be dismantled, materials recycled more easily and nuisances (dust, noise, clutter, etc.) can be reduced.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

Only a qualified person who is professionally qualified or placed under the effective and permanent control of a qualified person can carry out the demolition activity.

The person who practices or controls the exercise must be the holder of the exercise:

- a Certificate of Professional Qualification (CAP) or a Bachelor of Professional Degree in the construction, public works or environmental sectors;
- A Professional Studies Patent (BEP);
- a diploma or a degree of equal or higher level approved or registered when it was issued to the National Directory of Professional Certifications (RNCP).

In the absence of one of these diplomas, the person concerned will have to justify an effective three years of professional experience in a European Union (EU) state or party to the agreement on the European Economic Area (EEA) acquired as a leader self-employed or employee in demolition. In this case, the person concerned will be able to apply to the relevant CMA for a certificate of professional qualification.

In the case of the use of explosive devices, the person in charge of the demolition (also known as the fire extinguisher) will have to justify:

- Holding a shooting attendant certificate
- to have acquired sufficient practice in the implementation of explosives.

*For further information*: Decree 87-231 of 27 March 1987 concerning special protection requirements relating to the use of explosives in construction, public works and agricultural works; order of 10 July 1987 relating to the conditions for issuing the firearms licence provided by Decree 87-231 of 27 March 1987 concerning special protection requirements relating to the use of explosives in construction, public works and agricultural work; Decree 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act 96-603 of 5 July 1996.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

**For a temporary and casual exercise (LPS)**

Any national of an EU or EEA Member State who is established and legally performs demolition activity in that state may carry out the same activity in France on a temporary and occasional basis.

He must first make the request by declaration to the CMA of the place in which he wishes to carry out the service.

In the event that the profession is not regulated, either in the course of the activity or in the context of training, in the country in which the professional is legally established, he must have carried out this activity for at least one year, in the course of the ten years before the benefit, in one or more EU Member States.

Where there are substantial differences between the professional qualification of the national and the training required in France, the competent CMA may require that the person concerned submit to an aptitude test.

*For further information*: Article 17-1 of the Act of 5 July 1996; Article 2 of the decree of 2 April 1998 amended by the decree of 4 May 2017.

**For a permanent exercise**

In order to carry out the demolition activity in France on a permanent basis, the EU or EEA national must meet one of the following conditions:

- have the same professional qualifications as a Frenchman;
- hold a certificate of competency or training document required for the exercise of demolition activity in an EU or EEA state when that state regulates access or exercise of this activity on its territory;
- have a certificate of competency or a training certificate that certifies its readiness to carry out the demolition activity when this certificate or title has been obtained in an EU or EEA state which does not regulate access or exercise of This activity
- have a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has carried out the demolition activity in the state which has admitted equivalence.

If the eu's qualifications are met, a national of an EU or EEA state will be able to apply for a certificate of recognition of professional qualification.

Where there are substantial differences between the professional qualification of the national and the training required in France, the competent CMA may require that the person concerned submit to compensation measures.

*For further information*: Articles 17 and 17-1 of Law 96-603 of 5 July 1996; Articles 3 to 3-2 of the decree of 2 April 1998 amended by the decree of 4 May 2017.

### c. Conditions of honorability and incompatibility

No one may carry out the demolition activity if it is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 III of Act 96-603 of July 5, 1996.

### d. Obligation to take out professional liability insurance

The professional wishing to carry out a demolition activity must take out professional liability insurance. On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

**Please note**

The use of motorized land-based vehicles (construction equipment or trucks) is subject to the obligation to take out car liability insurance.

*For further information*: Article L. 211-1 of the Insurance Code.

### e. Some peculiarities of the regulation of the activity

#### Regulations on the quality of craftsman and master craftsman

**Craftsmanship**

To claim the status of craftsman, the person must justify either:

- a CAP, a BEP or a certified or registered designation when it was issued to the RNCP at least an equivalent level (see supra "2." a. Professional qualifications");
- professional experience in this occupation for at least three years.

*For further information*: Article 1 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of master craftsman**

This title is given to individuals, including the social leaders of legal entities:

- Registered in the trades directory;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

**Please note**

Individuals who do not hold the master's degree can apply to the Regional Qualifications Commission for the title of Master Craftsman under two assumptions:

- when they are registered in the trades directory, have a degree at least equivalent to the master's degree, and justify management and psycho-pedagogical knowledge equivalent to those of the corresponding value units of the master's degree and that they have two years of professional practice;
- when they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

#### Permit to demolish

Any operation to demolish a building is subject to an application for a building permit.

The application is a file addressed to the town hall of the municipality on which the building is located, in four copies.

*For further information*: Articles R. 423-1 and R. 423-2 of the Planning Code.

#### Professional identification card

Building and public works employees must have a professional identification card to present to screening officers.

The card application is made by the employer or by the temporary work companies.

It is valid for the duration of the employment contract and for a period of five years for temporary workers.

**Cost**

The demand for a card, which is[online](https://www.cartebtp.fr/), is accompanied by the payment of a fee set at 10.80 euros.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Artisanal Company Reporting Formalities," "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Follow the installation preparation course (SPI)

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including a degree in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who are:

- have been engaged in a professional activity requiring a level of knowledge equivalent to that provided by the internship for at least three years;
- have knowledge acquired in an EU or EEA state or a third country during a professional experience that can fully or partially cover the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France to run a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree No.83-517 of 24 June 1983.

### c. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)

**Competent authority**

The CMA of the place in which the national wishes to carry out the benefit is competent to issue the prior declaration of activity.

**Supporting documents**

The request for a pre-report of activity is accompanied by a complete file containing the following supporting documents:

- A photocopy of a valid ID
- a certificate justifying that the national is legally established in an EU or EEA state;
- a document justifying the professional qualification of the national who may be, at your choice:- A copy of a diploma, title or certificate,
  - A certificate of competency,
  - any documentation attesting to the national's professional experience.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Please note**

When the file is incomplete, the CMA has a period of fifteen days to inform the national and request all the missing documents.

**Outcome of the procedure**

Upon receipt of all the documents in the file, the CMA has one month to decide:

- either to authorise the benefit where the national justifies three years of work experience in an EU or EEA state, and to attach to that decision a certificate of professional qualification;
- or to authorize the provision when the national's professional qualifications are deemed sufficient;
- or to impose an aptitude test on him when there are substantial differences between the professional qualifications of the national and those required in France. In the event of a refusal to carry out this compensation measure or in the event of failure in its execution, the national will not be able to carry out the provision of services in France.

The silence kept by the competent authority within this period is worth authorisation to begin the provision of service.

*For further information*: Article 2 of the decree of 2 April 1998; Article 2 of the decree of 17 October 2017 relating to the submission of the declaration and requests provided for by Decree 98-246 of 2 April 1998 and Title I of Decree 98-247 of 2 April 1998.

### d. If necessary, apply for a certificate of recognition of professional qualification

The person concerned wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of recognition of professional qualification.

**Competent authority**

The request must be addressed to the territorially competent CMA.

**Procedure**

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the person concerned to complete it within a fortnight of filing the file. A receipt is issued as soon as the file is complete.

**Supporting documents**

The folder should contain the following parts:

- Applying for a certificate of professional qualification
- A certificate of competency or diploma or vocational training designation;
- Proof of the applicant's nationality
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, the proofs of the exercise of the activity for three years.

The CMA may request further information about its training or professional experience to determine the possible existence of substantial differences with the professional qualification required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (CIEP) to obtain additional information on the level of training of a diploma, certificate or foreign designation, the applicant will have to pay a fee Additional.

**What to know**

If necessary, all supporting documents must be translated into French .

**Response time**

Within three months of the receipt, the CMA may:

- Recognise professional qualification and issue certification of professional qualification;
- decide to subject the applicant to a compensation measure and notify him of that decision;
- refuse to issue the certificate of professional qualification.

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

**Remedies**

If the CMA refuses to issue the recognition of professional qualification, the applicant may initiate, within two months of notification of the refusal of the CMA, a legal challenge before the competent administrative court. Similarly, if the person concerned wishes to challenge the CMA's decision to submit it to a compensation measure, he must first initiate a graceful appeal with the prefect of the department in which the CMA is based, within two months of notification of the decision. CMA. If he does not succeed, he may opt for a litigation before the relevant administrative tribunal.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the Community or another state party to the European Economic Area agreement.

### e. Post-registration authorisation

#### Obtaining authorization for the acquisition of explosive devices

Any demolition professional who uses explosives must obtain an authorization in the form of:

- a one-year renewable acquisition certificate requested from the prefect of the department, as long as the person concerned wishes to acquire more than 500 detonators;
- a three-month order order requested from the gendarmerie unit or the police department of the place of use of explosives, for the acquisition of 500 explosive products up to 25 kg.

*For further information*: Article R. 2352-74 of the Defence Code; order of 3 March 1982 relating to the acquisition of explosive products.

#### Formalities related to the storage and transport of explosive devices

Many formalities fall to the professional who uses explosive devices.

In particular, it will have to obtain:

- **technical approval for storage.** The request is addressed to the prefect of the installation site who will request an opinion from the arms inspection for the powders and explosives at the Direccte as well as the police and gendarmerie services. It must include:- a safety study with a list of the recommended safety measures,
  - A facility compliance notice

**What to know**

The administration's silence for two months is worth rejecting the application.

*For further information*: Articles R. 2352-97 to R. 2352-102 of the Defence Code.

- **an individual operating permission for storage.** The authorization is issued to the operator himself who requests it from the prefect of the department of the place of installation.

The application is filed in the form of a file that includes:

- If it is a natural person, a copy of a complete and valid official identification document, and the indication of the applicant's occupation, residence and nationality, as well as an shipment of the number two criminal record bulletin;
- If it is a foreign national, an equivalent court document, translated into French by a certified translator;
- If it is a company, an extract from the statutes including the indication of the form of the company and the social purpose as well as the address of the head office and the same information as above relating to the agents of the company performing a function of direction for the operation of the depot, debit or mobile installation of explosive products;
- the justification for the company's inclusion in the trade register and its identification number assigned by the National Institute of Statistics and Economic Studies;

**What to know**

The administration's silence for two months is worth rejecting the application.

*For further information*: Articles R. 2352-110 to R. 2352-117 of the Defence Code; order of 12 March 1993 for the application of articles R. 2352-110 to R.2352-121 of the Defence Code.

- **authorisation for the transport of explosives.** Any professional who carries explosives is required to apply to the prefect. The authorization is valid for five renewable years.

#### If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)

As long as the storage of explosive products is necessary, the regulations on ICPE apply.

Depending on the amount of explosives, the formalities can be either:

- authorisation for between 500 kg and 10 tonnes;
- a registration for between 100 kg and 500 kg;
- prior declaration with periodic check for between 30 kg and 100 kg.

*For further information*: Articles R. 51-1, R. 512-46-1 to R. 512-46-7, and R. 512-46-19 to R. 512-46-23 of the Environment Code.