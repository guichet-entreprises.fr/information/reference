﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS073" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Démolition" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="demolition" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/demolition.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="demolition" -->
<!-- var(translation)="None" -->

# Démolition

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

La démolition est une activité spécialisée dans la déconstruction et la destruction de bâtiments.

Plusieurs techniques sont employées, comme par exemple le dynamitage, la boule de démolition, la lance thermique et la force humaine par le biais de masse entre autres.

La déconstruction sera souvent privilégiée à la destruction car elle permet de démonter les éléments d'ouvrage du bâtiment, recycler plus facilement les matériaux et réduire les nuisances (poussières, bruit, encombrement, etc.).

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Seule une personne qualifiée professionnellement ou placée sous le contrôle effectif et permanent d’une personne qualifiée peut exercer l'activité de démolition.

La personne qui exerce ou qui en contrôle l’exercice, doit être titulaire au choix :

- d’un certificat d’aptitude professionnelle (CAP) ou d'un baccalauréat professionnel dans les secteurs du bâtiment, des travaux publics ou de l'environnement ;
- d’un brevet d’études professionnelles (BEP) ;
- d’un diplôme ou d’un titre de niveau égal ou supérieur homologué ou enregistré lors de sa délivrance au Répertoire national des certifications professionnelles (RNCP).

À défaut de l’un de ces diplômes, l’intéressé devra justifier d’une expérience professionnelle de trois années effectives dans un État de l’Union européenne (UE) ou partie à l’accord sur l’Espace économique européen (EEE) acquise en qualité de dirigeant d’entreprise, de travailleur indépendant ou de salarié dans la démolition. Dans ce cas, l’intéressé pourra effectuer une demande d’attestation de reconnaissance de qualification professionnelle auprès de la CMA compétente.

En cas d'utilisation d'engins explosifs, la personne en charge de la démolition (aussi appelée boutefeu) devra justifier :

- d'être titulaire d'un certificat de préposé au tir ;
- d'avoir acquis une pratique suffisante dans la mise en œuvre des explosifs.

*Pour aller plus loin* : décret n° 87-231 du 27 mars 1987 concernant les prescriptions particulières de protection relatives à l'emploi des explosifs dans les travaux du bâtiment, les travaux publics et les travaux agricoles ; arrêté du 10 juillet 1987 relatif aux conditions de délivrance du permis de tir prévu par le décret n° 87-231 du 27 mars 1987 concernant les prescriptions particulières de protection relatives à l'emploi des explosifs dans les travaux du bâtiment, les travaux publics et les travaux agricoles ; décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l’exercice des activités prévues à l’article 16 de la loi n° 96-603 du 5 juillet 1996.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

**Pour un exercice temporaire et occasionnel (LPS)**

Tout ressortissant d'un État membre de l'UE ou de l'EEE qui est établi et exerce légalement l'activité de démolition dans cet État peut exercer en France, de manière temporaire et occasionnelle, la même activité.

Il doit au préalable en faire la demande par déclaration à la CMA du lieu dans lequel il souhaite réaliser la prestation.

Dans le cas où la profession n'est pas réglementée, soit dans le cadre de l'activité, soit dans le cadre de la formation, dans le pays dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité pendant au moins un an, au cours des dix dernières années précédant la prestation, dans un ou plusieurs États membres de l'UE.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, la CMA compétente peut exiger que l'intéressé se soumette à une épreuve d'aptitude.

*Pour aller plus loin* : article 17-1 de la loi du 5 juillet 1996 ; article 2 du décret du 2 avril 1998 modifié par le décret du 4 mai 2017.

**Pour un exercice permanent**

Pour exercer l’activité de démolition en France à titre permanent, le ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français ;
- être titulaire d’une attestation de compétences ou d’un titre de formation requis pour l’exercice de l’activité de démolition dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’une attestation de compétences ou d’un titre de formation qui certifie sa préparation à l’exercice de l’activité de démolition lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de cette activité ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité de démolition dans l’État qui a admis l’équivalence.

Dès lors qu'il remplit l'une des conditions précitées, le ressortissant d’un État de l’UE ou de l’EEE pourra demander une attestation de reconnaissance de qualification professionnelle.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, la CMA compétente peut exiger que l'intéressé se soumette à des mesures de compensation.

*Pour aller plus loin* : articles 17 et 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée ; articles 3 à 3-2 du décret du 2 avril 1998 modifié par le décret du 4 mai 2017.

### c. Conditions d'honorabilité et incompatibilités

Nul ne peut exercer l'activité de démolition s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévus au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 III de la loi n° 96-603 du 5 juillet 1996 précitée.

### d. Obligation de souscrire une assurance de responsabilité civile professionnelle

Le professionnel souhaitant exercer une activité de démolition doit souscrire une assurance de responsabilité civile professionnelle. En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. Dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

**À noter**

L'utilisation de véhicules terrestres à moteur (engins de chantier ou camions) est soumise à l'obligation de souscrire une assurance de responsabilité civile automobile.

*Pour aller plus loin* : article L. 211-1 du Code des assurances.

### e. Quelques particularités de la réglementation de l'activité

#### Réglementation relative à la qualité d'artisan et de maître artisan

**La qualité d'artisan**

Pour se prévaloir de la qualité d’artisan, la personne doit justifier soit :

- d’un CAP, d’un BEP ou d’un titre homologué ou enregistré lors de sa délivrance au RNCP d’un niveau au moins équivalent (cf. supra « 2°. a. Qualifications professionnelles ») ;
- d’une expérience professionnelle dans ce métier d'au moins trois ans.

*Pour aller plus loin* : article 1 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

**Le titre de maître artisan**

Ce titre est attribué aux personnes physiques, y compris les dirigeants sociaux des personnes morales :

- immatriculées au répertoire des métiers ;
- titulaires du brevet de maîtrise dans le métier exercé ;
- justifiant d’au moins deux ans de pratique professionnelle.

**À noter**

Les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan à la commission régionale des qualifications dans deux hypothèses :

- lorsqu’elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de niveau de formation au moins équivalent au brevet de maîtrise, qu’elles justifient de connaissances en gestion et en psychopédagogie équivalentes à celles des unités de valeur correspondantes du brevet de maîtrise et qu’elles ont deux ans de pratique professionnelle ;
- lorsqu’elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles disposent d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

*Pour aller plus loin* : article 3 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

#### Permis de démolir

Toute opération de démolition d'un bâtiment est soumise à une demande de permis de construire.

La demande est un dossier adressé à la mairie de la commune sur laquelle se trouve le bâtiment, en quatre exemplaires.

*Pour aller plus loin* : articles R. 423-1 et R. 423-2 du Code de l'urbanisme.

#### Carte d'identification professionnelle

Les salariés du bâtiment et des travaux publics doivent posséder une carte d'identification professionnelle à présenter aux agents de contrôle.

La demande de carte est faite par l'employeur ou par les entreprises de travail temporaire.

Elle est valable pour toute la durée du contrat de travail et pour une durée de cinq ans pour les intérimaires.

**Coût**

La demande de carte, qui se fait [en ligne](https://www.cartebtp.fr/), s'accompagne de l'acquittement d'une redevance fixée à 10,80 euros.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

**Autorité compétente**

La CMA du lieu dans lequel le ressortissant souhaite réaliser la prestation est compétente pour délivrer la déclaration préalable d'activité.

**Pièces justificatives**

La demande de déclaration préalable d'activité est accompagnée d'un dossier complet comprenant les pièces justificatives suivantes :

- une photocopie d'une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE ;
- un document justifiant la qualification professionnelle du ressortissant qui peut être, au choix :
  - une copie d'un diplôme, titre ou certificat,
  - une attestation de compétences,
  - tout document attestant de l'expérience professionnelle du ressortissant.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

**À noter**

Lorsque le dossier est incomplet, la CMA dispose d'un délai de quinze jours pour en informer le ressortissant et demander l'ensemble des pièces manquantes.

**Issue de la procédure**

À réception de l'ensemble des pièces du dossier, la CMA dispose d'un délai d'un mois pour décider :

- soit d'autoriser la prestation lorsque le ressortissant justifie d'une expérience professionnelle de trois ans dans un État de l'UE ou de l'EEE, et de joindre à cette décision une attestation de qualification professionnelle ;
- soit d'autoriser la prestation lorsque les qualifications professionnelles du ressortissant sont jugées suffisantes ;
- soit de lui imposer une épreuve d'aptitude lorsqu'il existe des différences substantielles entre les qualifications professionnelles du ressortissant et celles exigées en France. En cas de refus d'accomplir cette mesure de compensation ou en cas d'échec dans son exécution, le ressortissant ne pourra pas effectuer la prestation de services en France.

Le silence gardé de l'autorité compétente dans ce délai vaut autorisation de débuter la prestation de service.

*Pour aller plus loin* : article 2 du décret du 2 avril 1998 ; article 2 de l'arrêté du 17 octobre 2017 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre Ier du décret n° 98-247 du 2 avril 1998.

### c. Le cas échéant, demander une attestation de reconnaissance de qualification professionnelle

L’intéressé souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de reconnaissance de qualification professionnelle.

**Autorité compétente**

La demande doit être adressée à la CMA territorialement compétente.

**Procédure**

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les quinze jours du dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

**Pièces justificatives**

Le dossier doit contenir les pièces suivantes :

- la demande d’attestation de qualification professionnelle ;
- une attestation de compétences ou le diplôme ou titre de formation professionnelle ;
- la preuve de la nationalité du demandeur ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE, une attestation portant sur la nature et la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France, les justificatifs de l’exercice de l’activité pendant trois années.

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du Centre international d’études pédagogiques (CIEP) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme, d’un certificat ou d’un titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français (traduction agrée).

**Délai de réponse**

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut :

- reconnaître la qualification professionnelle et délivrer l’attestation de qualification professionnelle ;
- décider de soumettre le demandeur à une mesure de compensation et lui notifier cette décision ;
- refuser de délivrer l’attestation de qualification professionnelle.

En l’absence de décision dans le délai de quatre mois, la demande d’attestation de qualification professionnelle est réputée acquise.

**Voies de recours**

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier, dans les deux mois suivant la notification du refus de la CMA un recours contentieux devant le tribunal administratif compétent. De même, si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours gracieux auprès du préfet du département dans lequel la CMA a son siège, dans les deux mois suivant la notification de la décision de la CMA. S’il n’obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.

### d. Autorisation(s) post-immatriculation

#### Obtenir une autorisation pour l'acquisition d'engins explosifs

Tout professionnel de la démolition qui utilise des produits explosifs doit obtenir une autorisation sous la forme soit :

- d'un certificat d'acquisition d'une durée d'un an renouvelable demandé au préfet de département, dès lors que l'intéressé souhaite acquérir plus de 500 détonateurs ;
- d'un bon de commande valable trois mois demandé à l'unité de gendarmerie ou le service de police du lieu d'emploi des explosifs, pour l'acquisition de 500 produits explosifs maximum allant jusqu'à 25 kg.

*Pour aller plus loin* : article R. 2352-74 du Code de la défense ; arrêté du 3 mars 1982 relatif à l’acquisition des produits explosifs.

#### Formalités liées au stockage et au transport d'engins explosifs

De nombreuses formalités incombent au professionnel qui emploie des engins explosifs.

Il devra notamment obtenir :

- **un agrément technique pour le stockage.** La demande est adressée au préfet du lieu d'installation qui demandera un avis à l'inspection des armements pour les poudres et explosifs à la Direccte ainsi qu'aux services de police et de gendarmerie. Elle doit comporter :
  - une étude de sûreté avec la liste des mesures de sûreté préconisées,
  - une notice relative à la conformité de l'installation ;

**À savoir**

Le silence gardé de l'administration pendant deux mois vaut rejet de la demande.

*Pour aller plus loin* : articles R. 2352-97 à R. 2352-102 du Code de la défense.

- **une autorisation individuelle d'exploitation pour le stockage.** L'autorisation est délivrée à l'exploitant lui-même qui en fait la demande au préfet du département du lieu d'installation.

La demande est déposée sous forme d'un dossier comprenant :

- s'il s'agit d'une personne physique, la copie d'une pièce d'identité officielle complète et en cours de validité, et l'indication de la profession, du domicile et de la nationalité du demandeur ainsi qu'une expédition du bulletin n° 2 du casier judiciaire ;
- s'il s'agit d'un ressortissant étranger, un document judiciaire équivalent, traduit en français par un traducteur agréé ;
- s'il s'agit d'une société, un extrait des statuts comportant notamment l'indication de la forme de la société et de l'objet social ainsi que l'adresse du siège social et les mêmes renseignements que ci-dessus relatifs aux agents de la société exerçant une fonction de direction pour l'exploitation du dépôt, du débit ou de l'installation mobile de produits explosifs ;
- la justification de l'inscription de l'entreprise au registre du commerce et son numéro d'identification attribué par l'Institut national de la statistique et des études économiques ;

**À savoir**

Le silence gardé de l'administration pendant deux mois vaut rejet de la demande.

*Pour aller plus loin* : articles R. 2352-110 à R. 2352-117 du Code de la défense ; arrêté du 12 mars 1993 pris pour l'application des articles R. 2352-110 à R.2352-121 du Code de la défense.

- **une autorisation pour le transport de produits explosifs.** Tout professionnel qui transporte des produits explosifs est tenu d'en faire la demande auprès du préfet. L'autorisation est valable pendant cinq ans renouvelables.

#### Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE)

Dès lors que le stockage de produits explosifs est nécessaire, la réglementation relative aux ICPE s'applique.

Selon la quantité de produits explosifs, les formalités peuvent être soit :

- une autorisation pour une quantité comprise entre 500 kg et 10 tonnes ;
- un enregistrement pour une quantité comprise entre 100 kg et 500 kg ;
- une déclaration préalable avec contrôle périodique pour une quantité comprise entre 30 kg et 100 kg.

*Pour aller plus loin* : articles R. 51-1, R. 512-46-1 à R. 512-46-7, et R. 512-46-19 à R. 512-46-23 du Code de l'environnement.