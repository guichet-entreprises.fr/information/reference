﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS116" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Réalisation d'attestations de bonne gestion des pollutions des sols pour les projets de construction ou pour les cessations d'activité des ICPE" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="attestation-gestion-pollution-sols" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/attestation-gestion-pollution-sols.html" -->
<!-- var(last-update)="2022-09" -->
<!-- var(url-name)="attestation-gestion-pollution-sols" -->
<!-- var(translation)="None" -->

# Réalisation d'attestations de bonne gestion des pollutions des sols pour les projets de construction ou pour les cessations d'activité des ICPE

Dernière mise à jour : <!-- begin-var(last-update) -->Septembre 2022<!-- end-var -->

## Définition de l'activité

Suite aux lois dites ALUR (2014) et ASAP (2020), la conformité de certaines études réalisées dans le domaine de la gestion des pollutions des sols pour les projets de construction ou pour les cessations d'activité d'une installation classée pour la protection de l'environnement (ICPE) doit faire l'objet d'une attestation. Le Code de l'environnement exige que ces attestations soient délivrées par une entreprise certifiée selon des normes relatives aux sites et sols pollués, ou bénéficiant d'un dispositif équivalent. La présente fiche liste les conditions permettant d'accéder à l'activité de délivrance de ces attestations par une entreprise certifiée. Le public visé est principalement constitué de bureaux d'étude spécialisé en sites et sols pollués, de sociétés d'ingénierie, d'entreprises de contrôle généraliste et plus rarement d'entreprises de travaux de dépollution.

## Qualifications professionnelles requises en France

La délivrance d'attestation nécessite une certification, qui n'exige pas de diplômes dans une discipline spécifique bien qu'un diplôme de niveau master ou une importante expérience professionnelle pour certaines fonctions soient attendus. 

Les exigences de la certification sont définies par l'[arrêté ministériel du 9 février 2022](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000045220761?init=true&page=1&query=TREP2133425A&searchField=ALL&tab_selection=all). Il existe un référentiel de certification pour chaque attestation, constitué d'une partie commune et d'une partie spécifique. La norme NFX 31-620-1 constitue le tronc commun général. Celle-ci est accessible gratuitement en français sur le site de la [boutique de l'AFNOR](https://www.boutique.afnor.org/fr-fr/norme/nf-x316201/qualite-du-sol-prestations-de-services-relatives-aux-sites-et-sols-pollues-/fa203736/317393), après inscription. La partie spécifique du référentiel est composé de l'une des annexes IV à VIII de l'arrêté ministériel, qui définissent les prestations de délivrance de chacune des attestations, et éventuellement des normes NFX 31-620-2 et NFX 31-620-3 relatives aux prestations d'études et d'ingénierie dans le domaine des sites et sols pollués.

A titre d'exemple, afin de délivrer une attestation de mise en sécurité, voici quelques attendus pour chacune des fonctions évoquées au sein des référentiels de certification :

<table class="table table-striped table-bordered">
<thead style="text-align: center;border-collapse: collapse">
    <tr>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Rôle</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Niveau d'études</th>
<th style="border: 1px solid #AAA;padding: 4px;text-align: center;">Expérience professionnelle pour les prestations de l'Annexe V</th>
    </tr>
</thead>
<tbody style="text-align: center;border-collapse: collapse">
    <tr>
        <td style="border: 1px solid #AAA;padding: 4px">Superviseur</td>
        <td style="border: 1px solid #AAA;padding: 4px">Bac +5 ou diplôme équivalent, de préférence en génie de l'environnement, géologie, hydrogéologie, chimie, géochimie, agronomie, pédologie, génie civil, risques technologiques, géotechnique, génie des procédés</td>
        <td style="border: 1px solid #AAA;padding: 4px">3 ans comme chef de projet réalisant des prestations des annexes II ou V du présent arrêté</td>
    </tr>
    <tr>
        <td style="border: 1px solid #AAA;padding: 4px">Superviseur</td>
        <td style="border: 1px solid #AAA;padding: 4px">Sinon se référer à l'expérience professionnelle</td>
        <td style="border: 1px solid #AAA;padding: 4px">3 ans comme chef de projet réalisant des prestations des annexes II ou V du présent arrêté</td>
    </tr>
    <tr>
        <td style="border: 1px solid #AAA;padding: 4px">Chef de projet</td>
        <td style="border: 1px solid #AAA;padding: 4px">Bac +5 ou diplôme équivalent, de préférence en génie de l'environnement, géologie, hydrogéologie, chimie, géochimie, agronomie, pédologie, génie civil, risques technologiques, géotechnique, génie des procédés</td>
        <td style="border: 1px solid #AAA;padding: 4px">3 ans comme chef de projet réalisant des prestations des annexes II ou V du présent arrêté<br>ou<br>1 an comme ingénieur réalisant des prestations des annexes II ou V du présent arrêté et 3 ans d'expérience comme chef de projet réalisant des prestations des annexes II ou III du présent arrêté</td>
    </tr>
    <tr>
        <td style="border: 1px solid #AAA;padding: 4px">Chef de projet</td>
        <td style="border: 1px solid #AAA;padding: 4px">Sinon se référer à l'expérience professionnelle</td>
        <td style="border: 1px solid #AAA;padding: 4px">3 ans comme chef de projet réalisant des prestations des annexes II ou V du présent arrêté</td>
    </tr>
    <tr>
        <td style="border: 1px solid #AAA;padding: 4px">Ingénieur</td>
        <td style="border: 1px solid #AAA;padding: 4px">Bac +5 ou diplôme équivalent, de préférence </td>
        <td style="border: 1px solid #AAA;padding: 4px">Sans objet</td>
    </tr>
    <tr>
        <td style="border: 1px solid #AAA;padding: 4px">Ingénieur</td>
        <td style="border: 1px solid #AAA;padding: 4px">Bac +2 ou Bac +3</td>
        <td style="border: 1px solid #AAA;padding: 4px">5 ans</td>
    </tr>
    <tr>
        <td style="border: 1px solid #AAA;padding: 4px">Ingénieur</td>
        <td style="border: 1px solid #AAA;padding: 4px">Sinon se référer à l'expérience professionnelle</td>
        <td style="border: 1px solid #AAA;padding: 4px">8 ans</td>
    </tr>
</table>

## Particularités de la réglementation de l'activité

### Déontologie

Les référentiels de certification incluent notamment l'engagement de respecter le devoir d'information et de conseil envers le donneur d'ordre, qu'il s'agisse d'un exploitant industriel ou d'un maître d'ouvrage, mais également la maîtrise des risques de conflit d'intérêt.

### Assurance

L'entreprise certifiée doit disposer d'une attestation responsabilité civile professionnelle et d'exploitation incluant spécifiquement les risques d'atteintes à l'environnement.

### Tenue de registre

L'entreprise certifiée est tenue de conserver pendant dix ans les attestations qu'elles délivrent, et de les tenir à la disposition de l'organisme certificateur et de l'administration.

## Démarches de certification

L'obtention de la certification auprès d'un organisme certificateur est un préalable à l'exercice de l'activité de délivrance d'attestation. 

### Certification

#### Autorité compétente

La certification réglementaire est délivrée par un organisme certificateur accrédité par le COFRAC. Si l'entreprise souhaite faire valoir une équivalence à cette certification, elle doit faire part de sa demande à la direction générale de la prévention des risques du ministère en charge de l'environnement. Une équivalence est possible si l'entreprise dispose d'une autre certification comparable, ou bien si elle bénéficie d'un agrément ministériel d'un autre Etat dans le domaine des sites et sols pollués.

#### Procédure

Afin d'être certifiée, l'entreprise doit déposer un dossier auprès d'un organisme certificateur puis être auditée par ce dernier. La liste des organismes certificateurs est disponible sur le [site du ministère chargé de l'environnement](https://www.ecologie.gouv.fr/sites-et-sols-pollues). Le contenu du dossier et les modalités d'audit sont définies par l'[arrêté ministériel du 9 février 2022](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000045220761).

Si elle souhaite plutôt faire valoir une équivalence à la certification, elle doit contacter directement la direction générale de la prévention des risques. Une première prise de contact par courriel à l'adresse suivante est recommandée avec le dépôt de la demande : [bsss.dgpr@developpement-durable.gouv.fr](mailto:bsss.dgpr@developpement-durable.gouv.fr).

#### Pièces justificatives

Outre les différents éléments permettant de caractériser l'entreprise (SIRET, effectifs, identification du responsable...), celle-ci doit démontrer son respect des référentiels de certification (matrice de compétence, liste d'équipements) et communiquer une liste de dossiers de prestations réalisées conformément aux référentiels. La liste complète figure à l'adresse suivante : [arrêté ministériel du 9 février 2022](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000045220761).

#### Délais de réponse

L'organisme certificateur dispose d'un mois pour indiquer si le dossier de l'entreprise souhaitant être certifiée est complet auquel il faut ajouter un délai variable concernant l'évaluation de conformité via les audits dits de phase initiale. Si l'entreprise a été créée il y a moins d'un an, elle bénéficie d'un régime spécifique lui permettant de ne pas fournir trois études de référence réalisée conformément au référentiel de certification. En contrepartie, la certification est délivrée à titre provisoire pour 24 mois, à l'issue desquels l'entreprise doit pouvoir présenter ces études de référence. 

La direction générale de la prévention des risques doit statuer sur une demande d'équivalence dans un délai de deux mois.

#### Coût

Le coût de la certification peut varier selon l'organisme certificateur, le nombre d'établissements concernés par la certification et les référentiels selon lesquels l'entreprise souhaite être certifiée. Pour une certification sur cinq ans incluant plusieurs audits, des montants de l'ordre de 12 000 € sont constatés sur le marché en 2022. Il n'existe pas encore à ce jour de retour d'expérience pour le coût supporté par une entreprise entrant sur le marché et souhaitant bénéficier de la certification à titre provisoire. Celui-ci sera cependant inférieur au coût précité.

La demande d'équivalence est gratuite.

### Reconduction de la certification

#### Autorité compétente

La reconduction de la certification est faite par un organisme certificateur, tout comme la certification initiale. Toutefois, une entreprise a la possibilité de changer d'organisme certificateur si elle le souhaite.

#### Procédure

La démarche de reconduction de la certification est similaire à la certification initiale, bien que les durées d'audit soient plus courtes.

#### Pièces justificatives

Tout comme lors de la certification initiale, l'entreprise doit démontrer son respect des référentiels de certification (matrice de compétence, liste d'équipements) et communiquer une liste de dossiers de prestations réalisées conformément aux référentiels.

#### Délais de réponse

Tout comme pour la certification initiale, l'organisme certificateur dispose d'un mois pour indiquer si le dossier de l'entreprise souhaitant être certifiée est complet, mais il n'existe aucun délai réglementaire concernant l'évaluation de conformité via les audits.

#### Coût

Tout comme pour la certification initiale, le coût du renouvellement peut varier selon l'organisme certificateur, le nombre d'établissements concernés par la certification et les référentiels selon lesquels l'entreprise souhaite être certifiée. A périmètre constant, ce coût est a priori inférieur car les durées d'audit sont plus courtes.

## Liens utiles

### Textes de référence

- Articles L. 512-6-1, L. 512-7-6, L. 512-12-1, L. 556-1 et L. 556-2 du Code de l'environnement
- Articles R. 512-39-1 à R. 512-39-3, R. 512-46-25 à R. 512-46-27, R. 512-66-1, R. 512-66-3, R. 512-106 et R. 556-3 du Code de l'environnement
- Arrêté ministériel du 9 février 2022