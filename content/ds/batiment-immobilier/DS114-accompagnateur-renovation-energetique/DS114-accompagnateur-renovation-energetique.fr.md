﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS114" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Accompagnateur rénovation énergétique" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="accompagnateur-renovation-energetique" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/accompagnateur-renovation-energetique.html" -->
<!-- var(last-update)="2022-07" -->
<!-- var(url-name)="accompagnateur-renovation-energetique" -->
<!-- var(translation)="None" -->

# Accompagnateur rénovation énergétique

Dernière mise à jour : <!-- begin-var(last-update) -->Juillet 2022<!-- end-var -->

## Définition de l'activité

L'agrément « Mon Accompagnateur Rénov' » délivré par l'Agence nationale de l'habitat est mis en place en application de l'article 164 de la loi n° 2021-1104 du 22 août 2021 portant lutte contre le dérèglement climatique et renforcement de la résilience face à ses effets.

Cet agrément sert à identifier les acteurs de l'accompagnement à la rénovation énergétique dans le cadre de la mise en œuvre du service public de la performance énergétique de l'habitat. Il ne constitue pas une obligation générale pour les professionnels souhaitant exercer une activité d'accompagnement à la rénovation énergétique sur le territoire.
Le recours à un professionnel agréé « Mon Accompagnateur Rénov' » est uniquement obligatoire lorsque le particulier souhaite bénéficier de certaines aides de l'État à la rénovation énergétique. 

L'activité d'accompagnement réalisée par « Mon Accompagnateur Rénov' » a pour objectif d'assister les particuliers à mettre en œuvre un projet de rénovation énergétique et de traiter des difficultés rencontrées d'un point de vue administratif, financier et technique (difficultés à s'informer, à choisir des professionnels de confiance, crainte de pratiques frauduleuses, etc.).

L'opérateur agréé s'engage à respecter un cahier des charges concernant les prestations d'accompagnement réalisées sur chaque intervention, et à réaliser :

- une évaluation de la situation du logement et du ménage en amont de la réalisation de travaux et comprenant :
  - une visite sur site et des informations aux ménages sur le processus d'accompagnement ;
  - un diagnostic économique (évaluation de la situation économique du ménage qui permet de lui exposer son éventuelle éligibilité à des aides financières de l'État) ;
  - un diagnostic social (une évaluation de l'éligibilité du ménage à des soutiens sociaux en cas de précarité énergétique, de logement indécent, de situation de handicap).
- un accompagnement durant la phase de projet des travaux et comprenant :
  - un audit énergétique réglementaire permettant de définir un projet de travaux ;
  - la fourniture d'une liste de professionnels susceptibles de réaliser les travaux à la suite de l'audit énergétique et au regard de la solution retenue par le ménage ;
  - une aide à la réalisation d'un plan de financement et un appui au montage des dossiers de demandes d'aide ;
  - des conseils sur le suivi d'un chantier, et aide à la résolution des difficultés éventuelles ;
  - l'accompagnement du ménage à la réception des travaux et l'aide à la prise en main des nouveaux dispositifs installés.

L'accompagnement pourra également comprendre des prestations complémentaires de nature sociales telle que la proposition d'un scénario de travaux adapté aux problématiques de décence, d'insalubrité et de perte d'autonomie dans le logement. 

## Qualifications professionnelles requises en France

### Activité pouvant être exercée par des personnes ayant des qualifications professionnelles :

Pour les professions réglementées visées ci-dessous, l'exercice de l'activité de accompagnateur rénovation énergétique nécessite d'observer : 

- les exigences applicables à la profession d'[architecte](https://www.guichet-qualifications.fr/fr/dqp/batiment/architecte.html) ;
- les exigences supplémentaires indiquées dans les sections « particularités de la règlementation de l'activité » et « démarches de pré-immatriculation » (cf. infra).

### Activité pouvant être exercée par des personnes n'ayant pas de qualifications professionnelles :

**Pour les activités réglementées listées ci-dessous, l'exercice de l'activité de accompagnateur rénovation énergétique nécessite d'observer :**

- les exigences propres à l'activité  d'auditeur énergétique (fiche d'information à paraître prochainement) ;
- les exigences propres à l'activité d'opérateur ayant la qualité « RGE offre globale » (fiche d'information à paraître prochainement).

Les auditeurs énergétique et les opérateurs ayant la qualité « RGE offre globale » prévus à l'alinéa précédent devront également respecter : 

- les exigences supplémentaires indiquées dans les sections « particularités de la règlementation de l'activité » et « démarches de pré-immatriculation » (cf. infra).

**En dehors des cas prévus ci-dessus, l'exercice de l'activité d'accompagnateur rénovation énergétique peut être réalisée par des opérateurs qui devront :**

- avoir la qualité de structure ayant contractualisé avec une collectivité territoriale ou son groupement pour assurer le rôle de guichet d'information, de conseil et d'accompagnement au sens du I de l'article L. 232-2 du Code de l'énergie ;
- avoir la qualité de société de tiers-financement au sens du 8 de l'article L. 511-6 du Code monétaire et financier ;
- être titulaire de l'agrément délivré au titre de l'article L. 365-3 du Code de la construction et de l'habitation ;
- avoir la qualité de structure concourant à la mise en œuvre d'une opération programmée d'amélioration de l'habitat au sens de l'article L. 303-1 du Code de la construction et de l'habitation ou d'un programme d'intérêt général d'amélioration de l'habitat au sens de l'article R. 327-1 du même code, sous réserve qu'elle soit en cours de conventionnement valide avec une collectivité territoriale ou son groupement.

Les opérateurs prévus à l'alinéa précédent devront également respecter les exigences supplémentaires indiquées dans les sections « particularités de la règlementation de l'activité » et « démarches de pré-immatriculation ».

## Particularités de la réglementation de l'activité

### Déontologie (indépendance et impartialité)

Nul ne peut exercer l'activité d'accompagnement à la rénovation énergétique s'il exerce directement une activité d'exécution d'ouvrage.

L'accompagnateur est tenu au respect d'une stricte neutralité, à performance égale, vis-à-vis des équipements, solutions technologiques et scénarios de travaux proposés ainsi qu'une stricte neutralité, à qualité égale, vis-à-vis des entreprises de travaux recommandées.

### Conditions d'honorabilité 

Pour exercer son activité le professionnel ne doit pas être :

- placées en état de redressement ou de liquidation judiciaire ;
- définitivement condamnées pour un fait énoncé au 3° du II de l'article L. 123-11-3 du Code de commerce ;
- exclus de la procédure de passation des marchés au sens de l'article L. 2141-2 du Code de la commande publique.

### Interdictions

La sous-traitance des prestations d'accompagnement est interdite, à l'exception de l'audit énergétique et des prestations facultatives sociales.

Les prestations d'accompagnement sociales peuvent seulement être sous-traitées à l'un des opérateurs suivants :

- opérateurs de l'Agence nationale de l'habitat agréé au titre de l'article L. 365-3 du Code de la construction et de l'habitation ;
- structures concourant à la mise en œuvre d'une opération programmée d'amélioration de l'habitat au sens de l'article L. 303-1 du Code de la construction et de l'habitation ou d'un programme d'intérêt général d'amélioration de l'habitat au sens de l'article R. 327-1 du même code, sous réserve qu'elle soit en cours de contractualisation avec une collectivité territoriale ou son groupement ;
- structures de maîtrise d'ouvrage d'insertion au sens de l'article L.365-2 du Code de la construction et de l'habitation ;
- les collectivités territoriales ou leurs groupements.

### Incompatibilités

Nul ne peut exercer l'activité d'accompagnement à la rénovation énergétique s'il exerce directement une activité d'exécution d'ouvrage.

### Garantie financière

L'accompagnateur doit être en capacité d'exercer ses activités sur le plan financier. Il devra fournir lors du dépôt de sa demande d'agrément les comptes financiers des 3 années écoulées ainsi que le budget prévisionnel de l'année en cours.

### Transmission d'information à l'Administration

L'opérateur transmet ou fait transmettre sur le système d'information de l'Agence nationale de l'habitat prévu à cet effet un rapport d'accompagnement pour chaque prestation effectuée.

L'opérateur s'engage à transmettre, à chaque date anniversaire de la décision d'agrément, un rapport d'indépendance qui atteste de l'absence de lien direct avec une activité d'exécution d'ouvrage.

### Facturation

La prestation fait l'objet d'un contrat conclu entre le bénéficiaire et le prestataire, dans lequel est précisé le coût complet de l'accompagnement ainsi que la liste des missions prévues, comprenant à minima les prestations mentionnées dans le décret d'application du dispositif.

## Démarches pré-immatriculation

### Demande d'agrément

Tout opérateur qui souhaite devenir accompagnateur doit être agréé par l'Agence nationale de l'habitat.

Pour obtenir ou renouveler leur agrément, les candidats à l'agrément déposent un dossier auprès de l'Agence nationale de l'habitat, dont le contenu est précisé dans le décret d'application du texte.

#### Autorité compétente

Le dossier de demande d'agrément initial, ou le dossier de renouvellement d'agrément sont transmis via une adresse électronique communiquée par l'Agence nationale de l'habitat.

L'adresse électronique sera accessible sur le [site internet France Rénov'](https://france-renov.gouv.fr/) dans un onglet spécifique dédié à la procédure d'agrément.

#### Procédure

L'Agence nationale de l'habitat examine le dossier de candidature complet déposé par l'opérateur et se prononce sur l'octroi ou le renouvellement de l'agrément dans un délai de 3 mois. Les agréments sont délivrés au fil de l'eau, par décision expresse de l'Agence nationale de l'habitat et sont communiqués à la structure candidate.  En cas de dossier incomplet, l'Agence nationale de l'habitat adresse à l'intéressé une demande de pièces complémentaires. Le délai d'instruction de l'agrément est suspendu jusqu'à réception des compléments demandés. La demande d'agrément peut être rejetée dans le cas où un dossier demeure incomplet après une première demande de compléments.

Le dossier de demande d'agrément devra être déposé directement en ligne sur le [site France Rénov'](https://france-renov.gouv.fr/).

#### Pièces justificatives

Le dossier de demande d'agrément comprend les pièces suivantes :

- un document attestant qu'elles remplissent l'une des conditions suivantes :
  - être architecte au sens de l'article 2 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture,
  - être titulaire du signe de qualité mentionné au II de l'article 1er du décret n°2014-812 du 16 juillet 2014 pris pour l'application du second alinéa du 2 de l'article 200 quater du code général des impôts et du dernier alinéa du 2 du I de l'article 244 quater U du même code, pour la catégorie de travaux visée au 17° du I de l'article 1er du décret précité,
  - être titulaire du signe de qualité visé au b du 2° du II de l'article 1er du décret n° 2018-416 du 30 mai 2018,
  - avoir la qualité de structure ayant contractualisé avec une collectivité territoriale ou son groupement pour assurer le rôle de guichet d'information, de conseil et d'accompagnement au sens du I de l'article L. 232-2 du Code de l'énergie,
  - avoir la qualité de société de tiers-financement au sens du 8 de l'article L. 511-6 du Code monétaire et financier,
  - être titulaire de l'agrément délivré au titre de l'article L. 365-3 du Code de la construction et de l'habitation,
  - avoir la qualité de structure concourant à la mise en œuvre d'une opération programmée d'amélioration de l'habitat au sens de l'article L. 303-1 du code de la construction de l'habitation ou d'un programme d'intérêt général d'amélioration de l'habitat au sens de l'article R. 327-1 du même code, sous réserve qu'elle soit en cours de conventionnement valide avec une collectivité territoriale ou son groupement ;
- un justificatif attestant la compétence de la personne chargée de réaliser l'accompagnement du ménage. Ce justificatif précise les formations déjà suivies et celles appelées à l'être pendant la période d'agrément ;
- un justificatif attestant un niveau d'activité régulier ou, à défaut, un engagement relatif au niveau d'activité régulier comprenant une cible d'activité ;
- une déclaration relative au périmètre d'intervention infra-départemental, départemental, régional, ou national, ce périmètre pouvant comprendre plusieurs parties de départements contigües ;
- un justificatif, le cas échéant, de la capacité à intervenir au niveau interdépartemental, régional, ou national ;
- un justificatif établissant la condition d'indépendance par rapport aux activités d'exécution d'ouvrage ; 
- un justificatif attestant la capacité financière de l'accompagnateur à exercer ses activités, comprenant notamment les comptes financiers de l'année écoulée et le budget prévisionnel de l'année en cours ;
- une preuve que le candidat à l'obtention ou au renouvellement de l'agrément n'a pas fait l'objet d'une des condamnations, interdictions ou sanctions visées au 3° du II de l'article L. 123-11-3 du Code de commerce ou au L. 2141-2 du Code de la commande publique.

#### Délai de réponse

L'agrément ou son renouvellement sont accordés par décision expresse de l'Agence nationale de l'habitat pour une durée maximum de cinq ans renouvelable, dans un délai de 3 mois à compter du dépôt d'un dossier. Le silence gardé par l'Agence au terme de ce délai vaut décision implicite de rejet.

En cas de dossier incomplet, l'Agence nationale de l'habitat adresse à l'intéressé une demande de pièces complémentaires. Le délai d'instruction de l'agrément est suspendu jusqu'à réception des compléments demandés.

#### Coût

Le dépôt du dossier de demande d'agrément et son instruction sont gratuits.

#### Voies de recours

Les agréments sont délivrés au fil de l'eau, par décision expresse de l'Agence nationale de l'habitat et sont communiqués à la structure candidate. Les décisions défavorables sont motivées et mentionnent les délais et voies de recours devant le tribunal administratif.

## Liens utiles

### Textes de référence

- Articles L. 232-1 à L. 232-3 du Code de l'énergie ;
- Articles L. 242 1 à L. 242-4 du Code des relations entre le public et l'Administration ;
- Articles L. 511-6 du Code monétaire et financier ;
- Articles L. 173-1-1, L. 303 1, L. 365-3, R. 321-2, R. 321-5, R. 321-7, R. 321-12, R. 321-16, R. 321-17, R. 327-1 et R. 362-1 du Code de la construction et de l'habitation ;
- Décret n° 2014-812 du 16 juillet 2014 pris pour l'application du second alinéa du 2 de l'article 200 quater du Code général des impôts et du dernier alinéa du 2 du I de l'article 244 quater U du Code général des impôts ;
- Décret n° 2018-416 du 30 mai 2018 relatif aux conditions de qualification des auditeurs réalisant l'audit énergétique éligible au crédit d'impôt sur le revenu pour la transition énergétique prévues au dernier alinéa du 2 de l'article 200 quater du Code général des impôts ;
- Décret NOR : TRER2208329D pris pour application de l'article 164 de la loi n° 2021-1104 du 22 août 2021 portant lutte contre le dérèglement climatique et renforcement de la résilience face à ses effets.

### Autres liens utiles

- [Services en ligne](https://france-renov.gouv.fr/) du service public de la performance énergétique de l'habitat.