﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->



<!-- var(key)="DS106" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Asbestos removal (removal or encapsulation)" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="asbestos-removal-or-encapsulation" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/asbestos-removal-or-encapsulation.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="asbestos-removal-or-encapsulation" -->
<!-- var(translation)="Auto" -->







Asbestos removal (removal or encapsulating asbestos)
====================================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition - nature of the work

Asbestos removal or encapsulating work (commonly referred to as asbestos removal or asbestos treatment) is work that can cause the emission of asbestos fibres and subsequently treat asbestos or asbestos asbestos-containing material (MCA), i.e. asbestos management, as defined by the Public Health Code, whether by storage in an adapted facility, by vitrification or by total and watertight overlay. The concept of withdrawal must thus be interpreted, not in the physical or literal sense of the term but in the legal sense of the action of processing the material, from its management until its final elimination.

The asbestos removal or encapsulating company is therefore a company whose business consists of removing or encapsulating asbestos and materials, equipment and materials or items containing, including in cases of demolition (Article R. 4412-94/1) of the Labour Code).

To do this, these companies must:

- train their workers by a certified training organization (Article R. 4412-141 of the Labour Code), according to the terms defined by the decree of 23 February 2012 defining the modalities of training workers in the prevention of Asbestos-related risks
- be certified by an accredited body, according to the terms defined by Articles R. 4412-129 to 131 of the Labour Code and the provisions of the amended December 14, 2012 order setting out the certification requirements for companies carrying out work of removal or encapsulating asbestos, materials, equipment or items containing it.

The contractor of a asbestos removal operation must first ensure the competence of the company to which he plans to entrust this work (Article R. 4412-129 of the Labour Code), which must be justified by the production of the certificate. issued by the certifying body.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out.

Asbestos removal activity is commercial in nature, so the relevant CFE is the Chamber of Commerce and Industry (CCI).

As part of the certification process, asbestos treatment companies justify the legality of their existence by producing the Kbis extract linked to their registration in the Register of Trade and Companies (RCS).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal.

2°. Conditions for the exercise of asbestos removal or encapsulating activity
--------------------------------------------------------------------------------------

In order to carry out the asbestos treatment activity, the company must meet, in accordance with the provisions of Article 1 of the amended order of 14 December 2012, the requirements set out in the NF X 46-010 standard: August 2012 "Processing work of Asbestos - Technical repository for business certification - General requirements."

This mandatory standard, available free of charge on the AFNOR website, sets out the associated requirements and evaluation criteria:

- administrative, legal, and economic;
- Organizational
- Technical
- staff assigned to asbestos treatment activities;
- asbestos treatment operations.

### a. Professional qualifications

There is no requirement for a professional degree, title or experience to carry out the asbestos de asbestos work.

However, the company justifies in the process of certifying its activity, a sufficient number of people, aged over 18 years and holders of an indeterminate contract, allowing it to insure by its own means the asbestos treatment operations depending on the size or nature of the construction sites. These staff have the following skills: technical supervision, construction management or construction operator.

In order to be assigned to asbestos treatment activities, the worker must:

- have the skills to carry out your job (technical supervision, construction management, construction operator);
- be trained in the prevention of asbestos-related risks in accord with the terms defined by the decree of 23 February 2012, by a training organisation (OF) certified by a certifying body (OC) itself accredited by the accreditation body.

As of October 1, 2019, 31 OF were certified as such by the 3 OCs:[Certibat](https://www.certibat.fr/entreprises/),[I.Cert](https://www.icert.fr/liste-des-certifies/?recherche=&dyn_certification=632&departement=tous) And[GLOBAL Certification](https://www.global-certification.fr/images/CERTIFICATION/AMIANTE/OF-AMIANTE/of-amiante-liste-clients-maj-06-08-2018.pdf).

### b. Professional qualifications - European and non-EU nationals

Any European or non-EU company, as long as it intervenes in France, must apply French regulations on the health and safety of workers in accordance with Article L. 1262-4 9o of the Labour Code.

As a result, a company based in another Member State or outside the European Union wishing to carry out work on French territory to remove or encapsulate asbestos materials must comply with French regulations and French standards.

This company, which can also hold a certification in its country of origin, must provide proof of the equivalence to the French system of its training measures, prevention, its documentary obligations and the procedure certification of his country of origin (Articles R. 4412-132 and R. 4412-141 of the Labour Code).

These companies established outside France may detach staff in order to carry out a temporary service. The use of service provision must then comply with the labour code's secondment rules set out in Sections L. 1262-1 and the following of the Labour Code: prior declaration, temporary benefit, pre-existing link between the employee and his or her non-permanent activity in France, respect for the core of labour regulations, etc.

Employers detaching employees in France are required to make a prior declaration of secondment to the labour inspectorate of the place where the benefit begins. Tele-reporting,[available on the SIPSI website](https://www.sipsi.travail.gouv.fr/), has been mandatory since October 2016.

*To go further* Article R. 1263-4-1 and Article R. 1263-5 of the Labour Code.

An employer may not avail itself of the provisions applicable to the secondment of employees when it performs, in the State in which it is established, activities solely related to internal or administrative management, or when its activity is carried out on national territory in a normal, stable and continuous manner.

As long as the activity on French territory is permanent, the foreign company must create a secondary school, which is subject to French regulations as a matter of law.

*To go further* Articles L. 1262-1 and L. 1262-3 of the Labour Code

#### Conditions of honourability and criminal sanctions

The professional is bound by the rules of ethics specific to each certifying body. For more information, it is advisable to visit the website of these organizations:

- [AFNOR Certification](https://certification.afnor.org/gestion-des-risques-sst/traitement-de-l-amiante) ;
- [GLOBAL Certification](https://www.global-certification.fr/certification/amiante/amiante-entreprise) ;
- [Qualibat](https://www.qualibat.com/resultat-de-la-recherche/).

#### Control and sanctions

The asbestos removal or encapsulating company, like any employer or its delegate who has committed violations of health and safety rules, incurs criminal proceedings punishable by a fine of 10,000 euros applicable as many times as there are company workers involved.

*To go further* Article L. 4741-1 of the Labour Code.

On the other hand, when a labour inspection control officer finds a serious and imminent danger situation resulting from the absence of protective devices to avoid the risks associated with asbestos treatment work, he or she may issue a temporary stop-work order to remove exposed workers.

*To go further:* Article L. 4731-1 of the Labour Code.

### c. Professional liability insurance

As part of the process of certifying its asbestos treatment business, the company justifies that it takes appropriate measures to cover its civil liability for the treatment of asbestos through insurance. 4.2 of the above-mentioned NF X 46-010 standard.

### d. Some peculiarities of the regulation of the activity

Asbestos treatment companies must be certified by an organization itself accredited by the Cofrac (national accreditation body), after its assessment of its compliance with the requirements set by NF EN ISO/CIS 17065 - Compliance Assessment - Requirements for organizations certifying products, processes and services.

The certifying bodies (OCs) accredited to date by the Cofrac are AFNOR certification, GLOBAL certification and Qualibat. They assess the ability of companies, which are candidates for certification or already holders of the latter, to carry out asbestos treatment work in accordance with the requirements set out in Article 1 of the order of 14 December 2012, I.e:

- those set by the August 2012 NF X 46-010 standard ("Asbestos Processing Works - Technical Reference for Business Certification - General Requirements");
- those set by the December 2014 NF X 46-011 ("Asbestos Processing Works - How Companies' Certificates Are Awarded and Tracked");
- regulations in the Labour Code and governing asbestos operations.

For more information, visit the AFNOR website:

- [standard NF X 46-010](https://www.boutique.afnor.org/norme/nf-x46-010/travaux-de-traitement-de-l-amiante-referentiel-technique-pour-la-certification-des-entreprises-exigences-generales/article/798037/fa170060) ;
- [standard NF X 46-011](https://www.boutique.afnor.org/norme/nf-x46-011/travaux-de-traitement-de-l-amiante-modalites-d-attribution-et-de-suivi-des-certificats-des-entreprises/article/821097/fa059103).

3°.Procedures and formalities of carrying out the activity
----------------------------------------------------------------------

After assessing the company's ability to perform work in accordance with the requirements of the above-mentioned NF X 46-010 standard, the certifying body issues, when satisfied, a certificate under the conditions set out in NF X 46-011: December 2014 "Asbestos Processing Works - How Companies' Certificates Are Awarded and Tracked."

The certification process takes place in four stages:

- Step 0: Receivability (after passing the documentary exam);
- Step 1: Pre-certification (after successfully passing the seat audit. The transition to pre-certification is confirmed by the OC's decision-making body);
- Step 2: Probation Certification: two incompressible years with annual follow-up (after successfully passing the first site audit. The transition to pre-certification is confirmed by the OC's decision-making body. Its maintenance during the validity period is conditioned by the successful passage of surveillance operations);
- Step 3: Certification: five years with annual follow-up (successfully passed the probationary stage and the decision-making body's assessment of three reference files selected by the OC. The transition to certification is confirmed by the OC's decision-making body. Its maintenance during the validity period is conditioned by the successful passage of surveillance operations);
- Step 4: Renewal of certification: five years renewable with annual follow-up (successfully completed renewal operations that include a site audit, a headquarters audit and a document review. The renewal is confirmed by the OC's decision-making body).

The OC can make different types of decisions in the face of company deviations, including:

- suspension or withdrawal
- alert or emergency in the event of a serious asbestos hazard.

The OC also handles the company's appeals of decisions sanctioning it, complaints or claims made by third parties.

A certificate is issued by the OC that mentions the certification stage and the activities in which (which) the company conducts its asbestos treatment business.

The directory of certified companies, available to the public on the OC sites, also contains this information.

### 4°. Reference texts

In view of the health and legal risks resulting from exposure to asbestos fibres, asbestos removal activity is highly regulated by the following provisions:

- Articles R. 4412-94 to R. 4412-148 of the Labour Code and their enforcement orders:- Decree 23 February 2012 setting out the modalities of training workers to prevent asbestos-related risks,
  - Order 14 August 2012 amended relating to the conditions for measuring dust levels, the conditions for monitoring compliance with the occupational exposure limit for asbestos fibres and the conditions for accreditation of the bodies measuring these measurements,
  - Decree 14 December 2012 amended setting the conditions for certification of companies carrying out work to remove or encapsulate asbestos, materials, equipment or items containing them,
  - Order 7 March 2013 relating to the selection, maintenance and verification of personal protective equipment used in operations with a risk of asbestos exposure,
  - Decree 8 April 2013 on technical rules, preventive measures and collective protection measures to be implemented by companies in operations with a risk of asbestos exposure;
- standards made mandatory by the amended December 14, 2012 order:- NF X 46-010: August 2012 "Asbestos Treatment Work - Technical Reference for Business Certification - General Requirements,"
  - NF X 46-011: December 2014 "Asbestos Processing Works - How Companies' Certificates Are Awarded and Tracked."

*To go further* The Ministry of Labour's website is updated[regulations to prevent occupational risks associated with asbestos](https://travail-emploi.gouv.fr/sante-au-travail/prevention-des-risques-pour-la-sante-au-travail/article/amiante). The website of the Direccte Pays-de-la-Loire offers[many tools to support asbestos treatment companies](http://pays-de-la-loire.direccte.gouv.fr/amiante,3968), including a[guide to helping asbestos removal companies with certification](http://pays-de-la-loire.direccte.gouv.fr/La-certification-pour-les-travaux-des-traitements-de-l-amiante).

