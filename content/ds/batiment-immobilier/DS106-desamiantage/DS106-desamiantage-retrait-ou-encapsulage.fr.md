﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS106" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Désamiantage (retrait ou encapsulage d’amiante)" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="desamiantage-retrait-ou-encapsulage" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/desamiantage-retrait-ou-encapsulage-d-amiante.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="desamiantage-retrait-ou-encapsulage-d-amiante" -->
<!-- var(translation)="None" -->

# Désamiantage (retrait ou encapsulage d’amiante)

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition - nature des travaux

Les travaux de retrait ou d’encapsulage d’amiante (dénommés couramment travaux de désamiantage ou de traitement de l’amiante) sont des travaux susceptibles de provoquer l’émission de fibres d’amiante et qui permettent par la suite de traiter l’amiante ou le matériau contenant de l’amiante (MCA), c’est-à-dire de gérer l’amiante, au sens où l’entend le Code de la santé publique, que ce soit par stockage dans une installation adaptée, par vitrification ou par recouvrement total et étanche. La notion de retrait doit ainsi être interprétée, non au sens physique ou littéral du terme mais au sens juridique de l’action de traitement du matériau, de sa gestion jusqu’à son élimination finale.

L’entreprise de retrait ou d’encapsulage de l’amiante est en conséquence une entreprise dont l’activité consiste à procéder au retrait ou à l’encapsulage d’amiante et de matériaux, d’équipements et de matériels ou d’articles en contenant, y compris dans les cas de démolition (article R. 4412-94/1° du Code du travail).

Pour exercer cette activité, ces entreprises doivent :

- faire former leurs travailleurs par un organisme de formation lui-même certifié (article R. 4412-141 du Code du travail), selon les modalités définies par l’arrêté du 23 février 2012 définissant les modalités de la formation des travailleurs à la prévention des risques liés à l’amiante ;
- être certifiées par un organisme accrédité, selon les modalités définies par les articles R. 4412-129 à 131 du Code du travail et les dispositions de l’arrêté du 14 décembre 2012 modifié fixant les conditions de certification des entreprises réalisant des travaux de retrait ou d’encapsulage d’amiante, de matériaux, d’équipements ou d’articles en contenant.

Le donneur d’ordre d’une opération de désamiantage doit s’assurer au préalable de la compétence de l’entreprise à laquelle il envisage de confier ces travaux (article R. 4412-129 du Code du travail), celle-ci devant en justifier par la production du certificat délivré par l’organisme certificateur.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée.

L’activité de désamiantage est de nature commerciale, le CFE compétent est donc la chambre de commerce et d’industrie (CCI). 

Dans le cadre de la procédure de certification, les entreprises de traitement de l’amiante justifient de la légalité de leur existence par la production de l'extrait Kbis liée à leur immatriculation au registre du commerce et des sociétés (RCS).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale.

## 2°. Conditions d'exercice de l’activité de retrait ou d’encapsulage d’amiante

Pour exercer l’activité de traitement de l’amiante, l’entreprise doit satisfaire, conformément aux dispositions de l’article 1er de l’arrêté du 14 décembre 2012 modifié, aux exigences définies par la norme NF X 46-010 : août 2012 « Travaux de traitement de l’amiante - Référentiel technique pour la certification des entreprises – Exigences générales ».

Cette norme obligatoire, consultable gratuitement sur le site de l’AFNOR, fixe notamment les exigences et les critères d’évaluation associés :

- d’ordre administratif, juridique, et économique ;
- organisationnels ;
- techniques ;
- concernant le personnel affecté aux activités de traitement de l’amiante ;
- concernant les opérations de traitement de l’amiante.

### a. Qualifications professionnelles

Il n’y a pas d’exigence de diplôme, titre ou expérience professionnels afin d’exercer l’activité de désamianteur.

Toutefois, l’entreprise justifie dans le cadre de la démarche de certification de son activité, d’un nombre suffisant de personnes, âgées de plus de 18 ans et titulaires d’un contrat à durée indéterminée, lui permettant d’assurer par ses propres moyens les opérations de traitement de l’amiante selon l’importance ou la nature des chantiers. Ce personnel possède les compétences suivantes : encadrement technique, encadrement de chantier ou opérateur de chantier.

Afin de pouvoir être affecté aux activités de traitement de l’amiante, le travailleur doit :

- détenir les compétences nécessaire à l’exercice de sa fonction (encadrement technique, encadrement de chantier, opérateur de chantier) ;
- être formé à la prévention des risques liés à l’amiante selon les modalités définies par l’arrêté du 23 février 2012, par un organisme de formation (OF) certifié par un organisme certificateur (OC) lui-même accrédité par l’instance d’accréditation. 

Au 1er octobre 2019, 31 OF étaient certifiés à ce titre par les 3 OC : [Certibat](https://www.certibat.fr/entreprises/), [I.Cert](https://www.icert.fr/liste-des-certifies/?recherche=&dyn_certification=632&departement=tous) et [GLOBAL Certification](https://www.global-certification.fr/certification/amiante/amiante-entreprise).

### b. Qualifications professionnelles - Ressortissants européens et extra-communautaires

Toute entreprise européenne ou extra-communautaire, dès lors qu’elle intervient en France, doit appliquer la réglementation française en matière de santé et de sécurité des travailleurs conformément à ce que prévoit l’article L. 1262-4 9° du Code du travail. 

En conséquence, l’entreprise installée dans un autre État membre ou hors de l'Union européenne et souhaitant effectuer sur le territoire français des travaux de retrait ou d’encapsulage de matériaux amiantés doit se conformer à la réglementation française et aux normes françaises y afférentes. 

Cette entreprise, qui peut par ailleurs détenir une certification dans son pays d’origine, doit apporter la preuve de l’équivalence au dispositif français de ses mesures de formation, de prévention, de ses obligations documentaires et de la procédure de certification de son pays d’origine (articles R. 4412-132 et R. 4412-141 du Code du travail).

Ces entreprises établies hors de France peuvent y détacher du personnel afin d’exercer une prestation temporaire. Le recours à la prestation de services doit alors respecter les règles du Code du travail en matière de détachement prévues aux articles L. 1262-1 et suivants du Code du travail : déclaration préalable, prestation temporaire, lien préexistant entre le salarié et son employeur, activité non permanente en France, respect du noyau dur de la réglementation du travail, etc.

Les employeurs détachant des salariés en France sont tenus d’effectuer une déclaration préalable de détachement auprès de l'inspection du travail du lieu où débute la prestation. La télédéclaration, [disponible sur le site internet SIPSI](https://www.sipsi.travail.gouv.fr/), est obligatoire depuis octobre 2016.

*Pour aller plus loin* : article R. 1263-4-1 et article R. 1263-5 du Code du travail.

Un employeur ne peut se prévaloir des dispositions applicables au détachement de salariés lorsqu’il exerce, dans l’État dans lequel il est établi, des activités relevant uniquement de la gestion interne ou administrative, ou lorsque son activité est réalisée sur le territoire national de façon habituelle, stable et continue.

Dès lors que l’activité sur le territoire français est permanente, l’entreprise étrangère doit créer un établissement secondaire, lequel est assujetti de plein droit à la réglementation française.

*Pour aller plus loin* : articles L. 1262-1 et L. 1262-3 du Code du travail

#### Conditions d’honorabilité et sanctions pénales

Le professionnel est tenu au respect de règles déontologiques propres à chaque organisme certificateur. Pour plus d'informations, il est conseillé de consulter le site internet de ces organismes :

- [AFNOR Certification](https://certification.afnor.org/gestion-des-risques-sst/traitement-de-l-amiante) ;
- [GLOBAL Certification](https://www.global-certification.fr/certification/amiante/amiante-entreprise) ;
- [Qualibat](https://www.qualibat.com/resultat-de-la-recherche/).

#### Contrôle et sanctions 

L’entreprise de retrait ou d’encapsulage d’amiante encourt, comme tout employeur ou son délégataire ayant commis des infractions aux règles de santé et de sécurité, des poursuites pénales passibles d’une amende de 10 000 euros applicable autant de fois qu’il y a de travailleurs de l’entreprise concernés.

*Pour aller plus loin* : article L. 4741-1 du Code du travail.

Par ailleurs, lorsqu’un agent de de contrôle de l’inspection du travail constate une situation de danger grave et imminent résultants de l’absence de dispositifs de protection de nature à éviter les risques liés aux travaux de traitement de l’amiante, il peut prononcer une mesure d’arrêt temporaire de travaux afin d’y soustraire les travailleurs exposés.

*Pour aller plus loin* : article L. 4731-1 du Code du travail.

### c. Assurance de responsabilité civile professionnelle

Dans le cadre de la démarche de certification de son activité de traitement de l’amiante, l’entreprise justifie de ce qu’elle prend des dispositions appropriées pour couvrir sa responsabilité civile en matière de traitement de l’amiante par une assurance. (§ 4.2 de la norme NF X 46-010 précitée).

### d. Quelques particularités de la réglementation de l'activité

Les entreprises de traitement de l’amiante doivent être certifiées par un organisme lui-même accrédité par le Cofrac (instance nationale d'accréditation), après évaluation par ce dernier de son respect des exigences fixées par la norme NF EN ISO/CEI 17065 - Evaluation de la conformité - Exigences pour les organismes certifiant les produits, les procédés et les services.

Les organismes certificateurs (OC) accrédités à ce jour par le Cofrac sont AFNOR certification, GLOBAL certification et Qualibat. Ces derniers évaluent les capacités des entreprises, candidates à la certification ou déjà titulaires de cette dernière, à réaliser les travaux de traitement de l'amiante conformément aux exigences définies à l'article 1er de l'arrêté du 14 décembre 2012, c'est-à-dire : 

- celles fixées par la norme NF X 46-010 d'août 2012 (« Travaux de traitement de l'amiante - Référentiel technique pour la certification des entreprises - Exigences générales ») ;
- celles fixées par la norme NF X 46-011 de décembre 2014 (« Travaux de traitement de l'amiante - Modalité d'attribution et de suivi des certificats des entreprises ») ;
- les prescriptions de la réglementation inscrites au Code du travail et encadrant les opérations portant sur l'amiante.

Pour plus d’informations, consulter le site de l’AFNOR :

- [norme NF X 46-010](https://www.boutique.afnor.org/norme/nf-x46-010/travaux-de-traitement-de-l-amiante-referentiel-technique-pour-la-certification-des-entreprises-exigences-generales/article/798037/fa170060) ;
- [norme NF X 46-011](https://www.boutique.afnor.org/norme/nf-x46-011/travaux-de-traitement-de-l-amiante-modalites-d-attribution-et-de-suivi-des-certificats-des-entreprises/article/821097/fa059103).

## 3°. Démarches et formalités d'exercice de l'activité

Après avoir évalué la capacité de l’entreprise à réaliser des travaux conformément aux exigences de la norme NF X 46-010 précitée, l’organisme certificateur délivre, lorsqu’elle y satisfait, un certificat dans les conditions fixées par la norme NF X 46-011 : décembre 2014 «  Travaux de traitement de l’amiante – Modalités d’attribution et de suivi des certificats des entreprises ». 

La démarche de certification se déroule en 4 étapes :

- Etape 0 : Recevabilité (après avoir passé l’examen documentaire) ;
- Etape 1 : Pré-certification (après avoir passé avec succès l’audit siège. Le passage en pré-certification est confirmé par l’instance de décision de l’OC) ;
- Etape 2 : Certification probatoire : deux années incompressibles avec suivi annuel (après avoir passé avec succès l’audit de premier chantier. Le passage en pré-certification est confirmé par l’instance de décision de l’OC. Son maintien pendant la durée de validité est conditionné par le passage avec succès des opérations de surveillance) ;
- Etape 3 : Certification : cinq années avec suivi annuel (avoir passé avec succès l’étape probatoire et l’évaluation par l’instance de décision de trois dossiers de référence choisis par l’OC. Le passage à la certification est confirmé par l’instance de décision de l’OC. Son maintien pendant la durée de validité est conditionné par le passage avec succès des opérations de surveillance) ;
- Etape 4 : Renouvellement de la certification : cinq années renouvelables avec suivi annuel (avoir passé avec succès les opérations de renouvellement qui comprennent un audit chantier, un audit siège et un examen documentaire. Le renouvellement est confirmé par l’instance de décision de l’OC).

L’OC peut prendre différents types de décision face aux écarts de l’entreprise et engager notamment une procédure :

- de suspension ou de retrait ;
- d’alerte ou d’urgence, en cas de danger grave constaté relatif à l’amiante.

L’OC traite également les appels interjetés par l’entreprise à l’égard des décisions la sanctionnant, des plaintes ou réclamations formulées par des tiers.

Un certificat est délivré par l’OC qui mentionne l’étape de la certification ainsi que les activités dans laquelle (lesquelles) l’entreprise exerce son activité de traitement de l’amiante.

Le répertoire des entreprises certifiées, accessible au public sur les sites des OC contient également ces informations.

### 4°. Textes de référence

Au regard des risques sanitaires et juridiques qui résultent de l’exposition aux fibres d’amiante, l’activité de désamiantage est très encadrée par les dispositions suivantes :

- les articles R. 4412-94 à R. 4412-148 du Code du travail et leurs arrêtés d’application :
  - arrêté 23 février 2012 définissant les modalités de la formation des travailleurs à la prévention des risques liés à l’amiante,
  - arrêté 14 août 2012 modifié relatif aux conditions de mesurage des niveaux d’empoussièrement, aux conditions de contrôle du respect de la valeur limite d’exposition professionnelle aux fibres d’amiante et aux conditions d’accréditation des organismes procédant à ces mesurages,
  - arrêté 14 décembre 2012 modifié fixant les conditions de certification des entreprises réalisant des travaux de retrait ou d’encapsulage d’amiante, de matériaux, d’équipements ou d’articles en contenant,
  - arrêté 7 mars 2013 relatif au choix, à l’entretien et à la vérification des équipements de protection individuelle utilisés lors d’opération comportant un risque d’exposition à l’amiante,
  - arrêté 8 avril 2013 relatif aux règles techniques, aux mesures de prévention et aux moyens de protection collective à mettre en œuvre par les entreprises lors d’opération comportant un risque d’exposition à l’amiante ;
- des normes rendues obligatoires par l’arrêté 14 décembre 2012 modifié :
  - NF X 46-010 : août 2012 « Travaux de traitement de l’amiante - Référentiel technique pour la certification des entreprises – Exigences générales »,
  - NF X 46-011 : décembre 2014 « Travaux de traitement de l’amiante – Modalités d’attribution et de suivi des certificats des entreprises ».

*Pour aller plus loin* : le site du ministère du Travail présente de manière actualisée [la réglementation en matière de prévention des risques professionnels liés à l’amiante](https://travail-emploi.gouv.fr/sante-au-travail/prevention-des-risques-pour-la-sante-au-travail/article/amiante). Le site de la Direccte Pays-de-la-Loire propose [de nombreux outils d’appui aux entreprises de traitement de l’amiante](http://pays-de-la-loire.direccte.gouv.fr/amiante,3968), et notamment un [guide d’aide à la certification des entreprises de désamiantage](http://pays-de-la-loire.direccte.gouv.fr/La-certification-pour-les-travaux-des-traitements-de-l-amiante).