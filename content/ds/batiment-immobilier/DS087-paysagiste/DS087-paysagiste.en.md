﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS087" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Landscaper" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="landscaper" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/landscaper.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="landscaper" -->
<!-- var(translation)="Auto" -->


Landscaper
==========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

The landscaper is a professional in green spaces. Its activity consists of creating, developing and maintaining public natural spaces (gardens, parks, traffic lanes, etc.) or private ones.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- For a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI);
- for a liberal activity, the competent CFE is the Urssaf;
- for an agricultural activity, the competent CFE is the chamber of agriculture.

**Please note**

If the activity is agricultural and commercial in nature, the relevant CFE will be the ICC.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To practice the profession of landscaper, the professional must justify professional qualifications that vary according to the nature of his duties.

**Designer landscaper**

The professional must hold a diploma sanctioning cultural, scientific and technical training in landscape design.

*For further information*: Article 174 of Law No. 2016-1087 of August 8, 2016 for the recapture of biodiversity, nature and landscapes; Decree No. 2017-673 of April 28, 2017 relating to the use of the title of designer landscaper.

**Landscaper in charge of carrying out small works of construction of landscaped structures**

The professional must be professionally qualified or exercise under the effective and permanent control of a professional.

To be recognized as a professionally qualified, the professional must hold one of the following diplomas or training titles:

- a professional agricultural patent "Landscaping Works" (BPA);
- a certificate of agricultural professional aptitude "Landscape Gardener" (CAPA);
- a professional certificate, a professional bachelor's degree, or a senior agricultural technician's (BTSA) "Landscaping" certificate;
- a professional license "Specialty Landscape Project Coordinator: Natural Heritage and Coastal Landscapes" or "Humanities and Social Sciences mention landscaping: design, management, maintenance";
- a state diploma (DE) as a landscaper.

In the absence of one of these diplomas or titles, the person concerned must justify an effective three years of professional experience in the territory of the European Union (EU) or the European Economic Area (EEA) acquired as a business leader, self-employed or salaried in the practice of landscape work. In this case, the person concerned is advised to apply to the CMA for a certificate of professional qualification.

*For further information*: Article 16 Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Decree 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act 96-603 of 5 July 1996.

**Landscaper responsible for the maintenance of green spaces and the use of phytopharmaceuticals and/or biocides**

The professional must hold an individual certificate for the activity of "Sale, sale of phytopharmaceuticals" which is subject to the following conditions:

- have followed and validated either:- training tailored to this activity,
  - training and a knowledge-testing test,
  - A one-hour test
- having obtained, in the last five years, one of the diplomas listed in Annex of the order of 29 August 2016 establishing and setting out the terms of obtaining the individual certificate for the activity "sale, sale of phytopharmaceuticals".

For the activity related to the use of biocides, the professional must hold an individual certificate which he can apply electronically with the Ministry of the Environment, which will issue him the individual certificate within two months.

*For further information*: Article R. 254-1 of the Rural Code and Marine Fisheries; ordered from 29 August 2016 above; decree from 9 October 2013 conditions for the practice of professional user and distributor activity of certain types of biocides.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

**For the purpose of Freedom to provide services**

The professional who is a member state of the EU or the EEA who is legally established and practising as a landscaper may carry out the same activity in France on a temporary and casual basis. For this the national must:

- Hold a diploma, title or training certificate necessary to practice one's speciality and issued in the Member State of origin;
- request a pre-report of its activity prior to its first service delivery in France (see below. "2) Request for pre-reporting for the provision of free services");
- where the profession is not regulated, either in the course of the activity or in the context of training, in the country in which the professional is legally established, he must have carried out this activity for at least one year in the last ten years years before the benefit in one or more EU member states.

**Please note**

An EU national who wishes to practise as a designer landscaper is exempt from the pre-reporting requirement and may carry out his or her temporary and occasional activity in France under the professional title acquired in that state.

*For further information*: Articles R. 254-9 and the following of the Rural Code and Marine Fisheries; Article 17-1 of the Act of 5 July 1996; Article 2 of the decree of April 2, 1998 modified by the decree of May 4, 2017 ; Article 7 of Decree No. 2017-673 of April 28, 2017 relating to the use of the design landscaper title.

**In view of a Freedom of establishment**

The professional who is a member state of the EU or the EEA must, in order to carry out his work as a landscaper in France:

- Hold a diploma, title or training certificate issued in the Member State of origin necessary to exercise its specialty;
- apply for qualification recognition (see below. "2. d. Application for qualification for a Freedom of establishment");
- submit to a compensation measure if the competent authority considers that the applicant's training relates to subjects substantially different from the training required to carry out the activity in France. If necessary, the competent authority may require the applicant to submit to an aptitude test or an adjustment course of up to three years.

*For further information*: Article R. 204-1 of the Rural Code and Marine Fisheries; Articles 4 to 6 of the decree of 28 April 2017 above; 17 and 17-1 of Act 96-603 of 5 July 1996.

### c. Conditions of honorability

No one may practise as a landscaper if he or she is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 of Act 96-603 of July 5, 1996.

### d. Procedures and formalities for EU nationals

#### Request for pre-reporting for free service provision

The nature of the pre-declaration depends on the professional activity carried out by the EU national.

**Professional landscaper: maintenance of green spaces and use of phytopharmaceuticals and/or biocides**

**Competent authority**

A national who has an individual certificate of activity must apply to the regional director of food, agriculture and forestry of the place where he wishes to perform his service.

**Procedure and supporting documents**

The applicant addresses his application by any means accompanied by the following supporting documents:

- Proof of nationality
- a certificate certifying that it is legally established in an EU or EEA state and that there is no prohibition on practising;
- proof that he has been in this activity for at least one year in the last ten years when access to the profession is not regulated in the Member State.

**Please note**

In the event of a change in employment status, the application must be renewed.

*For further information*: Article R. 204-1 of the Rural Code and Marine Fisheries.

**Landscaper in charge of carrying out small works of construction of landscaped structures**

**Competent authority**

The national must apply to the CMA of the place where he wishes to carry out his service.

**Supporting documents**

The request for a pre-report of activity is accompanied by a complete file containing the following supporting documents:

- A photocopy of a valid ID
- a certificate justifying that the national is legally established in an EU or EEA state;
- a document justifying the professional qualification of the national who may be, at your choice:- A copy of a diploma, title or certificate,
  - A certificate of competency,
  - any documentation attesting to the national's professional experience.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Please note**

When the file is incomplete, the CMA has a period of fifteen days to inform the national and request all the missing documents.

**Outcome of the procedure**

Upon receipt of all the documents in the file, the CMA has one month to decide:

- either to authorise the benefit where the national justifies three years of work experience in an EU or EEA state, and to attach to that decision a certificate of professional qualification;
- or to authorize the provision when the national's professional qualifications are deemed sufficient;
- either to impose an aptitude test or an adjustment course when there are substantial differences between the professional qualifications of the national and those required in France. In the event of a refusal to carry out this compensation measure or in the event of failure in its execution, the national will not be able to carry out the provision of services in France.

The silence kept by the competent authority in these times is worth authorisation to begin the provision of services.

*For further information*: Article 2 of the decree of 2 April 1998; Article 2 of the October 17, 2017 regarding the submission of the declaration and the requests provided for by Decree 98-246 of 2 April 1998 and Title 1 of Decree 98-247 of 2 April 1998.

#### Application for qualification for a Freedom of establishment

A professional working in a craft activity wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of recognition of professional qualification.

**Competent authority**

The request must be addressed to the territorially competent CMA.

**Procedure**

A receipt is given to the applicant within one month of receiving the application from the CMA. If the file is incomplete, the authority informs the person concerned within a fortnight of filing.

**Supporting documents**

The folder should contain the following parts:

- Applying for a certificate of professional qualification
- A certificate of competency or the applicant's diploma or vocational training certificate;
- proof of nationality
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, a proof of exercise of this activity for three years.

The CMA may request further information about its training or professional experience to determine the possible existence of substantial differences with the professional qualification required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (CIEP) to obtain additional information on the level of training of a diploma or certificate or a foreign designation, the applicant will have to pay a fee Additional.

**What to know**

If necessary, all supporting documents must be translated into French.

**Response time**

Within three months of the receipt being issued, the CMA may decide either:

- Recognise professional qualification and issue certification of professional qualification;
- submit the applicant to a compensation measure and notify him of that decision;
- refuse to issue the certificate of professional qualification.

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

**Remedies**

If the CMA refuses to issue the recognition of professional qualification, the applicant may initiate, within two months of notification of the refusal of the CMA, a legal challenge before the relevant administrative court.

Similarly, if the person concerned wishes to challenge the CMA's decision to submit it to a compensation measure, he must first initiate a graceful appeal with the prefect of the department in which the CMA is based, within two months of notification of the decision. CMA. If he does not succeed, he may opt for a litigation before the relevant administrative tribunal.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the Community or another state party to the European Economic Area agreement.

### e. Some peculiarities of the regulation of the activity

#### Specific provisions for the professional responsible for the maintenance of green spaces and the use of phytopharmaceuticals and/or biocides

**Health information**

The professional working as a landscaper in charge of the maintenance of green spaces is subject to compliance with the provisions relating to surveillance, prevention and the fight against health hazards.

*For further information*: Articles L. 201-1 and the following of the Rural Code and Marine Fisheries.

**Copy of the insurance certificate**

The professional performing this activity must provide the regional prefect with a copy of his certificate of professional liability insurance each year.

*For further information*: Articles R. 254-19 and the following of the Rural Code and Marine Fisheries.

#### Common provisions

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

**Case of young workers**

The professional must comply with the provisions relating to young workers, including the prohibition of entrusting them with:

- temporary work at heights:- where the prevention of the risk of falls is not ensured by collective protection measures,
  - dealing with trees and other woody and semi-woody species;
- the installation and dismantling of scaffolding;
- handling, monitoring or controlling pressurized devices, missions in confined locations or with molten materials.

*For further information*: Articles D. 4153-30 and the following articles of the Labour Code.

**Insurance**

In addition to compulsory liability insurance, the professional engaged in the construction of landscape works may be required to take out insurance covering the ten-year and biennial guarantees.

*For further information*: Articles 1779 and the following of the Civil Code.

**Human services**

A professional who does gardening work as part of a human services activity can report his activity to the regional directorate of business, competition, consumption, work and employment (Direccte).

3°. Installation procedures and formalities
------------------------------------------------------

### a. Follow the installation preparation course (SPI)

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including an education in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who are:

- have been engaged in a professional activity requiring a level of knowledge equivalent to that provided by the internship for at least three years;
- have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, in full or in part, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of their professional qualifications shows substantial differences with those required in France to run a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree 83-517 of June 24, 1983.

### b. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the "Formality of Reporting of a Commercial Company," "Individual Business Registration in the Register of Trade and Companies" and "Corporate Reporting Formalities. artisanal work."

### c. If necessary, register the company's statutes

Once the company's statutes have been dated and signed, the landscaper must register them with the Corporate Tax Office (SIE) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building when the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

### d. Application for approval for companies, in case of use of phytopharmaceuticals

**Pre-approval certification**

The professional responsible for the maintenance of green spaces and the use of phytopharmaceuticals must, prior to his application for approval, obtain a company certification.

The issuance of this certification is following an audit carried out by a certifying body in charge of verifying that the conditions of ownership of the individual certificate are properly met.

**Please note**

This prior certification will be passed on.

*For further information*: Article R. 254-3 of the Rural Code and Marine Fisheries; Order of 25 November 2011 relating to the certification framework provided for in Article R. 254-3 of the Rural Code and Maritime Fisheries "general organisation".

**Formalities for applying for accreditation**

To carry out this activity, the professional must obtain an exercise certification. To do this, he must:

- be the holder of the prior certification (see above "3.00). d. Pre-certification of the application for accreditation");
- Have taken out professional liability insurance
- Be recognized by a third-party organization as operating with respect for the protection of public health and the environment and the good information of the user;
- have entered into a follow-up contract necessary to maintain certification.

**Competent authority**

The prefect of the region in which the company's head office is located is competent to issue this accreditation.

**Timeframe**

The lack of response from the regional prefect is the case that the application for accreditation is rejected.

*For further information*: Article L. 254-1; Articles R. 254-15 to R. 254-19 of R. 254-3 of the Rural Code and Marine Fisheries; Decree No. 2011-1325 of October 18, 2011 setting out the conditions for issuing, renewing, suspending and withdrawing business approvals and individual certificates for sale, free distribution, application and advice on the use of phytopharmaceuticals.