﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS087" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Paysagiste" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="paysagiste" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/paysagiste.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="paysagiste" -->
<!-- var(translation)="None" -->

# Paysagiste

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le paysagiste est un professionnel des espaces verts. Son activité consiste à créer, aménager et entretenir des espaces naturels publics (jardins, parcs, voies de circulation, etc.) ou privés.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

- pour une activité artisanale, le CFE compétent est la chambre des métiers et de l'artisanat (CMA) ;
- pour une activité commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI) ;
- pour une activité libérale, le CFE compétent est l'Urssaf ;
- pour une activité agricole, le CFE compétent est la chambre d'agriculture.

**À noter**

Si l'activité est de nature agricole et commerciale, le CFE compétent sera la CCI.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer la profession de paysagiste, le professionnel doit justifier de qualifications professionnelles qui varient selon la nature de ses fonctions.

#### Paysagiste concepteur

Le professionnel doit être titulaire d'un diplôme sanctionnant une formation de caractère culturel, scientifique et technique à la conception paysagère.

*Pour aller plus loin* : article 174 de la loi n° 2016-1087 du 8 août 2016 pour la reconquête de la biodiversité, de la nature et des paysages ; décret n° 2017-673 du 28 avril 2017 relatif à l'utilisation du titre de paysagiste concepteur.

#### Paysagiste chargé de la réalisation de petits travaux de construction d'ouvrages paysagers

Le professionnel doit être qualifié professionnellement ou exercer sous le contrôle effectif et permanent d'un professionnel.

Pour être reconnu comme qualifié professionnellement, le professionnel doit être titulaire de l'un des diplômes ou titres de formation suivants :

- un brevet professionnel agricole « Travaux des aménagements paysagers » (BPA) ;
- un certificat d'aptitude professionnelle agricole « Jardinier paysagiste » (CAPA) ;
- un brevet professionnel, un baccalauréat professionnel, ou un brevet de technicien supérieur agricole (BTSA) « Aménagements paysagers » ;
- une licence professionnelle « Aménagement du paysage spécialité coordonnateur de projet : patrimoine naturel et paysages littoraux » ou « Sciences humaines et sociales mention aménagement paysager : conception, gestion, entretien » ;
- un diplôme d’État (DE) de paysagiste.

À défaut de l'un de ces diplômes ou titres, l'intéressé doit justifier d'une expérience professionnelle de trois années effectives sur le territoire de l'Union européenne (UE) ou de l'Espace économique européen (EEE) acquise en qualité de dirigeant d'entreprise, de travailleur indépendant ou de salarié dans l'exercice du métier de paysagiste. Dans ce cas, il est conseillé à l'intéressé de s'adresser à la CMA pour demander une attestation de qualification professionnelle.

*Pour aller plus loin* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l’artisanat ; décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l’exercice des activités prévues à l’article 16 de la loi n° 96-603 du 5 juillet 1996 précitée.

#### Paysagiste chargé de l'entretien d'espaces verts et de l'utilisation de produits phytopharmaceutiques et/ou biocides

Le professionnel doit être titulaire d'un certificat individuel pour l'activité de « Mise en vente, vente des produits phytopharmaceutiques » dont l'obtention est soumise aux conditions suivantes :

- avoir suivi et validé soit :
  - une formation adaptée à cette activité,
  - une formation et un test de vérification de connaissances,
  - un test d'une heure ;
- avoir obtenu, au cours des cinq dernières années, l'un des diplômes figurant en annexe de l'arrêté du 29 août 2016 portant création et fixant les modalités d'obtention du certificat individuel pour l'activité « mise en vente, vente des produits phytopharmaceutiques ».

Pour l'activité relative à l'utilisation de produits biocides, le professionnel doit être titulaire d'un certificat individuel dont il pourra faire la demande par voie électronique auprès du ministère en charge de l'environnement, qui lui délivrera le certificat individuel dans un délai de deux mois.

*Pour aller plus loin* : article R. 254-1 du Code rural et de la pêche maritime ; arrêté du 29 août 2016 précité ; arrêté du 9 octobre 2013 relatif aux conditions d'exercice de l'activité d'utilisateur professionnel et de distributeur de certains types de produits biocides.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services (LPS) ou Libre Établissement (LE))

#### En vue d'une Libre Prestation de Services

Le professionnel ressortissant d'un État membre de l'UE ou de l'EEE légalement établi et exerçant l'activité de paysagiste peut exercer en France à titre temporaire et occasionnel la même activité. Pour cela le ressortissant doit :

- être titulaire d'un diplôme, titre ou certificat de formation nécessaire à l'exercice de sa spécialité et délivré dans l’État membre d'origine ;
- effectuer une demande de déclaration préalable relative à son activité avant sa première prestation de services en France (cf. infra. « 2° d. Demande de déclaration préalable en vue d'une Libre Prestation de Services ») ;
- lorsque la profession n'est pas réglementée, soit dans le cadre de l'activité, soit dans le cadre de la formation, dans le pays dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité pendant au moins un an au cours des dix dernières années précédant la prestation dans un ou plusieurs État(s) membre(s) de l'UE.

**À noter**

Le ressortissant de l'UE qui souhaite exercer la profession de paysagiste concepteur est dispensé de l'obligation de déclaration préalable et peut exercer son activité à titre temporaire et occasionnel en France sous le titre professionnel acquis dans cet État.

*Pour aller plus loin* : articles R. 254-9 et suivants du Code rural et de la pêche maritime ; article 17-1 de la loi du 5 juillet 1996 ; article 2 du décret du 2 avril 1998 modifié par le décret du 4 mai 2017 ; article 7 du décret n° 2017-673 du 28 avril 2017 relatif à l'utilisation du titre de paysagiste concepteur.

#### En vue d'un Libre Établissement

Le professionnel ressortissant d'un État membre de l'UE ou de l'EEE doit, pour exercer son activité de paysagiste en France :

- être titulaire d'un diplôme, titre ou certificat de formation délivré dans l’État membre d'origine nécessaire à l'exercice de sa spécialité ;
- effectuer une demande de reconnaissance de qualification (cf. infra. « 2° d. Demande de reconnaissance de qualification en vue d'un Libre Établissement ») ;
- se soumettre à une mesure de compensation dès lors que l'autorité compétente estime que la formation du demandeur porte sur des matières substantiellement différentes de la formation requise pour exercer l'activité en France. Le cas échéant, l'autorité compétente pourra exiger que le demandeur se soumette à une épreuve d'aptitude ou un stage d'adaptation d'une durée maximale de trois ans.

*Pour aller plus loin* : article R. 204-1 du Code rural et de la pêche maritime ; articles 4 à 6 du décret du 28 avril 2017 susvisé ; articles 17 et 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée.

### c. Conditions d’honorabilité

Nul ne peut exercer la profession de paysagiste s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévus au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 de la loi n° 96-603 du 5 juillet 1996.

### d. Démarches et formalités pour le ressortissant de l'UE

#### Demande de déclaration préalable en vue d'une Libre Prestation de Services

La nature de la déclaration préalable dépend de l'activité professionnelle exercée par le ressortissant de l'UE.

##### Professionnel exerçant l'activité de paysagiste : entretien d'espaces verts et utilisation de produits phytopharmaceutiques et/ou biocides

###### Autorité compétente

Le ressortissant qui dispose d'un certificat individuel d'exercice de son activité doit adresser une demande au directeur régional de l'alimentation, de l'agriculture et de la forêt du lieu où il souhaite exercer sa prestation.

###### Procédure et pièces justificatives

Le demandeur adresse sa demande par tout moyen accompagnée des pièces justificatives suivantes :

- un justificatif de nationalité ;
- une attestation certifiant qu'il est légalement établi dans un État de l'UE ou de l'EEE et qu'il ne fait l'objet d'aucune interdiction d'exercer ;
- une preuve qu'il a exercé cette activité pendant au moins un an au cours des dix dernières années lorsque l'accès à la profession n'est pas réglementé dans l’État membre.

**À noter**

En cas de changement de situation professionnelle, la demande doit être renouvelée.

*Pour aller plus loin* : article R. 204-1 du Code rural et de la pêche maritime.

##### Paysagiste chargé de la réalisation de petits travaux de construction d'ouvrages paysagers**

###### Autorité compétente

Le ressortissant doit adresser une demande auprès de la CMA du lieu dans lequel il souhaite réaliser sa prestation.

###### Pièces justificatives

La demande de déclaration préalable d'activité est accompagnée d'un dossier complet comprenant les pièces justificatives suivantes :

- une photocopie d'une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE ;
- un document justifiant la qualification professionnelle du ressortissant qui peut être, au choix :
  - une copie d'un diplôme, titre ou certificat,
  - une attestation de compétence,
  - tout document attestant de l'expérience professionnelle du ressortissant.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

**À noter**

Lorsque le dossier est incomplet, la CMA dispose d'un délai de quinze jours pour en informer le ressortissant et demander l'ensemble des pièces manquantes.

###### Issue de la procédure

Dès réception de l'ensemble des pièces du dossier, la CMA dispose d'un délai d'un mois pour décider :

- soit d'autoriser la prestation lorsque le ressortissant justifie d'une expérience professionnelle de trois ans dans un État de l'UE ou de l'EEE, et de joindre à cette décision une attestation de qualification professionnelle ;
- soit d'autoriser la prestation lorsque les qualifications professionnelles du ressortissant sont jugées suffisantes ;
- soit de lui imposer une épreuve d'aptitude ou un stage d'adaptation lorsqu'il existe des différences substantielles entre les qualifications professionnelles du ressortissant et celles exigées en France. En cas de refus d'accomplir cette mesure de compensation ou en cas d'échec dans son exécution, le ressortissant ne pourra pas effectuer la prestation de services en France.

Le silence gardé de l'autorité compétente dans ces délais vaut autorisation de débuter la prestation de services.

*Pour aller plus loin* : article 2 du décret du 2 avril 1998 ; article 2 de l'arrêté du 17 octobre 2017 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre 1er du décret n° 98-247 du 2 avril 1998.

#### Demande de reconnaissance de qualification en vue d'un Libre Établissement

Le professionnel exerçant une activité artisanale souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de reconnaissance de qualification professionnelle.

##### Autorité compétente

La demande doit être adressée à la CMA territorialement compétente.

##### Procédure

Un récépissé est remis au demandeur dans un délai d’un mois suivant la réception de sa demande par la CMA. Si le dossier est incomplet, l'autorité en informe l’intéressé dans les quinze jours suivant son dépôt.

##### Pièces justificatives

Le dossier doit contenir les pièces suivantes :

- la demande d’attestation de qualification professionnelle ;
- une attestation de compétences ou le diplôme ou titre de formation professionnelle du demandeur ;
- un justificatif de sa nationalité ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE, une attestation portant sur la nature et la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France, un justificatif d'exercice de cette activité pendant trois années.

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du Centre international d’études pédagogiques (CIEP) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme ou d’un certificat ou d’un titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français.

##### Délai de réponse

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut décider soit :

- de reconnaître la qualification professionnelle et délivrer l’attestation de qualification professionnelle ;
- de soumettre le demandeur à une mesure de compensation et lui notifier cette décision ;
- de refuser de délivrer l’attestation de qualification professionnelle.

En l’absence de décision dans un délai de quatre mois, la demande d’attestation de qualification professionnelle est réputée acquise.

##### Voies de recours

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier, dans les deux mois suivant la notification du refus de la CMA, un recours contentieux devant le tribunal administratif compétent.

De même, si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours gracieux auprès du préfet du département dans lequel la CMA a son siège, dans les deux mois suivant la notification de la décision de la CMA. S’il n’obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.

### e. Quelques particularités de la réglementation de l’activité

#### Dispositions spécifiques au professionnel chargé de l'entretien d'espaces verts et de l'utilisation de produits phytopharmaceutiques et/ou biocides

##### Informations sanitaires

Le professionnel exerçant l'activité de paysagiste chargé de l'entretien d'espaces verts est soumis au respect des dispositions relatives à la surveillance, la prévention et la lutte contre les dangers sanitaires.

*Pour aller plus loin* : articles L. 201-1 et suivants du Code rural et de la pêche maritime.

##### Copie de l'attestation d'assurance

Le professionnel exerçant cette activité doit chaque année fournir au préfet de région une copie de son attestation d'assurance de responsabilité civile professionnelle.

*Pour aller plus loin* : articles R. 254-19 et suivants du Code rural et de la pêche maritime.

#### Dispositions communes

##### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lequel le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

##### Cas des jeunes travailleurs

Le professionnel doit respecter les dispositions relatives aux jeunes travailleurs et notamment l'interdiction de leur confier :

- la réalisation de travaux temporaires en hauteur :
  - lorsque la prévention du risque de chute n'est pas assurée par des mesures de protection collective,
  - portant sur les arbres et autres essences ligneuses et semi-ligneuses ;
- la réalisation du montage et démontage d'échafaudages ;
- la réalisation de manipulation, surveillance ou contrôle d'appareils sous pression, de missions dans des lieux confinés ou avec des matériaux en fusion.

*Pour aller plus loin* : articles D. 4153-30 et suivants du Code du travail.

##### Assurance

En plus de l'assurance de responsabilité civile obligatoire, le professionnel exerçant l'activité de construction d'ouvrages paysagers peut être tenu de souscrire une assurance couvrant les garanties décennales et biennales.

*Pour aller plus loin* : articles 1779 et suivants du Code civil.

##### Services à la personne

Le professionnel qui réalise des travaux de jardinage dans le cadre d'une activité de services à la personne peut déclarer son activité auprès de la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (Direccte).

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Le cas échéant, enregistrer les statuts de la société

Le paysagiste doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.

### c. Demande d'agrément pour les entreprises, en cas d'utilisation de produits phytopharmaceutiques

#### Certification préalable à la demande d'agrément

Le professionnel chargé de l'entretien d'espaces verts et de l'utilisation de produits phytopharmaceutiques doit, préalablement à sa demande d'agrément, obtenir une certification d'entreprise.

La délivrance de cette certification se fait à la suite d'un audit réalisé par un organisme certificateur en charge de vérifier que les conditions de détention du certificat individuel sont bien remplies.

**À noter**

Cette certification préalable sera transmise.

*Pour aller plus loin* : article R. 254-3 du Code rural et de la pêche maritime ; arrêté du 25 novembre 2011 relatif au référentiel de certification prévu à l'article R. 254-3 du Code rural et de la pêche maritime « organisation générale ».

#### Formalités de demande d'agrément

Le professionnel doit, pour exercer cette activité, obtenir un agrément d'exercice. Pour cela, il doit :

- être titulaire de la certification préalable (cf. supra « 3°. d. Certification préalable à la demande d'agrément ») ;
- avoir souscrit une assurance de responsabilité civile professionnelle ;
- être reconnu par un organisme tiers comme exerçant son activité dans le respect de la protection de la santé publique et de l'environnement et de la bonne information de l'utilisateur ;
- avoir conclu un contrat de suivi nécessaire au maintien de la certification.

#### Autorité compétente

Le préfet de la région dans laquelle se trouve le siège social de l'entreprise est compétent pour délivrer cet agrément.

#### Délai

L'absence de réponse de la part du préfet de région vaut rejet de la demande d'agrément.

*Pour aller plus loin* : article L. 254-1 ; articles R. 254-15 à R. 254-19 du R. 254-3 du Code rural et de la pêche maritime ; décret n° 2011-1325 du 18 octobre 2011 fixant les conditions de délivrance, de renouvellement, de suspension et de retrait des agréments des entreprises et des certificats individuels pour la mise en vente, la distribution à titre gratuit, l'application et le conseil à l'utilisation de produits phytopharmaceutiques.