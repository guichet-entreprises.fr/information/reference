﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS001" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Estate agent - Commonhold association - Property manager" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="real-estate-agent-commonhold-association-property-manager" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/real-estate-agent-commonhold-association-property-manager.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="real-estate-agent-commonhold-association-property-manager" -->
<!-- var(translation)="Auto" -->


Estate agent - Commonhold association - Property manager
================================================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The real estate agent, the property administrator and the condominium trustee (known as "real estate professionals") are professionals whose missions are to participate in the operations on the property of others and related to:

- buying, selling, researching, exchanging, renting or subletting, seasonally or not, in nude or furnished, of built or unded buildings;
- Buying, selling or leasing commercial funds;
- the sale of a herd (cattle fund) dead or alive;
- underwriting, buying, selling shares or shares in real estate companies or housing companies;
- The purchase, sale of non-negotiable shares where the social asset includes a building or a commercial fund;
- Property management
- the sale of lists or files relating to the purchase, sale, rental or subletting, nude or furnished of buildings built or not, or the sale of commercial funds (excluding publications by press);
- the conclusion of any timeshare building enjoyment contract;
- performing the duties of condominium trustee.

*For further information*: Articles 1 and following of Act 70-9 of January 2, 1970 regulating the conditions for the conduct of activities relating to certain transactions involving real estate and commercial funds.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- In case of artisanal activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for the liberal professions, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

In order to carry out this activity, the person concerned must obtain a business card (see infra "2. d. Application for a business card for a permanent exercise (LE)).

For this, the real estate professional must either:

- hold one of the following degrees:- a bachelor's degree (B.A. 3) sanctioning legal, economic or business studies,
  - a diploma or title registered in the national directory of professional certifications of an equivalent level (B.A. 3) and sanctioning studies of the same nature,
  - The Senior Technician's Patent (BTS) "Real Estate Professions"
  - The diploma of the Institute for Economic and Legal Studies applied to construction and housing;
- hold a bachelor's degree or a diploma or a title in the national directory of professional certifications of an equivalent level (level IV) and sanctioning legal, economic or commercial studies and have 3 years of experience professional in real estate;
- have 10 years of professional experience for non-executives or four years for real estate executives.

*For further information*: Articles 11 to 14 of Decree 72-678 of July 20, 1972.

### b. Professional qualifications - European nationals (free provision of services or Freedom of establishment)

#### In case of free provision of services (LPS)

A real estate professional who is a member of a European Union (EU) state or a state party to the European Economic Area (EEA) agreement may, provided he is legally established, carry out his activity on a temporary and casual basis.

In order to do so, he will have to make a request for a prior declaration with the president of the Chamber of Commerce and Territorial Industry or the departmental chamber of Ile-de-France.

If neither professional activity nor training is regulated in the EU or EEA State, the person concerned may work in France on a temporary and occasional basis if he justifies having carried out such activity for at least one year during the 10 years prior to the benefit.

**Please note**

In addition, the national practising as a real estate agent will be able to apply for the issuance of a European business card (see infra "2." d. European Professional Card (CPE) for nationals working as a real estate agent").

*For further information*: Article 8-1 of the Act of January 2, 1970.

#### In case of Freedom of establishment

In order to work as a real estate professional in France on a permanent basis, the EU or EEA national will have to meet one of the following conditions:

- Hold a certificate of competency or training certificate issued by a competent authority of the Member State of origin;
- where the EU or EEA State does not regulate this occupation, having been active for at least one year in the previous ten years.

Once the national fulfils one of these conditions, he or she will be able to apply for a business card (see below "3 degrees). d. Application for a business card for a permanent exercise (Freedom of establishment)").

**Please note**

The national must also have sufficient knowledge of the French language.

### c. Conditions of honorability and incompatibility

The national who has been the subject of a final conviction for less than ten years cannot practise as a real estate professional:

- for felony;
- a sentence of at least three months' imprisonment without a conditional sentence;
- and an dismissal from the functions of public or ministerial officer.

In addition, the professional must not have been subjected to:

- bankruptcy or no-practice measure;
- a deletion of the functions of administrator or judicial agent;
- prohibited from practising as a professional for at least six months.

*For further information*: Article 9 of the Act of January 2, 1970.

### d. Formalities and procedures for the EU national

#### Request for pre-reporting for a temporary and casual exercise (LPS)

The person concerned must submit an application by recommended letter or electronically to the president of the Chamber of Commerce and Territorial Industry or the Departmental Chamber of Ile-de-France in the jurisdiction of which he plans to carry out his first performance.

This application must be accompanied by a file that includes:

- a certificate justifying that it is legally established in an EU or EEA Member State and that it is not prohibited from practising;
- Where the Member State does not regulate the activity, any document justifying work experience for at least one year in the ten years preceding the benefit;
- A certificate of nationality
- A financial guarantee certificate
- a certificate of professional liability insurance;
- if so, a statement on the honour that he does not hold any funds, effect, or value, other than those representative of his remuneration.

*For further information*: Article 16-6 of the decree of 20 July 1972.

#### Application for a business card for a permanent exercise (LE)

The national of an EU or EEA member state must apply to the President of the Chamber of Commerce and Territorial Industry or the Departmental Chamber of Ile-de-France.

This application must be accompanied by a file with the following supporting documents:

- The form[Cerfa 15312*01](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15312.do) completed and signed;
- Proof of the applicant's professional fitness
- A financial guarantee certificate
- a certificate of professional liability insurance;
- an excerpt from the Register of Trade and Companies (RCS) less than one month old if the person is registered, or double the demand;
- depending on the case:- certificate with the account number issued by the credit institution that opened the account,
  - or a certificate of openness on behalf of each principal of bank accounts;
- if so, the declaration of honour that the applicant does not hold any other funds, directly or indirectly, than those representing his remuneration;
- Bulletin 2 with a national criminal record or an equivalent for nationals of another EU Member State;
- [investigation file fees](https://www.entreprises.cci-paris-idf.fr/web/formalites/demande-carte-professionnelle-immobilier) 120 euros to be paid online.

##### Time

If the file is incomplete, the competent authority sends the professional a list of missing documents within a fortnight of receiving it. If his file is not completed within two months, his application for a card will be rejected.

*For further information*: Articles 2 and following of the decree of July 20, 1972.

#### European Professional Card (CPE) for nationals working as a real estate agent

A national of an EU or EEA Member State working as a real estate agent may apply for the issuance of a CPE allowing him to have his professional qualifications recognised in another EU state.

**Please note**

The CPE procedure can be used when the national wishes to operate in another EU state on a permanent basis as a temporary and occasional person.

The CPE is valid:

- indefinitely in the case of a long-term settlement (Freedom of establishment);
- 18 months for the provision of services on a temporary basis.

##### Request for CPE

To apply for a CPE, the national must:

- Create a user account on the[European Commission authentication service](https://webgate.ec.europa.eu/cas) ;
- then fill out his CPE profile (identity, contact information...).

**Please note**

It is also possible to create a CPE application by downloading the scanned supporting documents.

##### Cost

For each CPE application, the authorities of the host country and the country of origin may charge a file review fee, the amount of which varies depending on the situation.

##### Time to apply for a CPE for a temporary and casual exercise (LPS)

Within a week, the country of origin authority acknowledges receipt of CPE's request, reports if documents are missing and informs of any costs. Then, the host country's authorities check the case.

If no verification is required with the host country, the country of origin authority reviews the application and makes a final decision within three weeks.

If verifications are required within the host country, the country of origin authority has one month to review the application and forward it to the host country. The host country then makes a final decision within three months.

##### Deadlines for a CPE application for a permanent activity (LE)

Within a week, the country of origin authority acknowledges receipt of CPE's request, reports if documents are missing and informs of any costs. The country of origin then has one month to review the application and forward it to the host country. The latter makes the final decision within three months.

If the host country authorities believe that the level of education or training or work experience is below the standards required in that country, they may ask the applicant to take an aptitude test or to complete an internship adaptation.

##### Issue of CPE application

If the application for CPE is granted, then it is possible to obtain a CPE certificate online.

If the host country's authorities do not make a decision within the allotted time, qualifications are tacitly recognised and a CPE is issued. It is then possible to obtain a CPE certificate from your online account.

If the application for CPE is rejected, the decision to refuse must be justified and bring the remedies to challenge that refusal.

*For further information*: Articles 16-8 and following of the decree of July 20, 1972.

### e. Some peculiarities of the regulation of the activity

#### Compulsory continuing education

The real estate professional must complete a continuous training of 14 hours per year or 42 hours during three consecutive years of practice.

The renewal of his business card is conditional on compliance with this requirement.

*For further information*: Articles 1 and following of Decree No. 2016-173 of February 18, 2016 relating to the continuing education of real estate professionals.

#### Occupational civil insurance

The real estate professional must take out professional liability insurance in order to be covered against financial risks in the course of his activity.

This guarantee must include the contact details of the real estate professional as well as those of his insurer. The limit of this professional civil insurance cannot be less than 76,224.51 euros per year for the same insured.

*For further information*: Article 49 of the decree of 20 July 1972; decree of 1 July 2005 setting out the conditions of insurance and the form of the supporting document provided by the decree of 20 July 1972.

#### Financial guarantee

The real estate professional, who holds the professional card or who requests it (see infra "3.00). a. Application for a business card for a permanent year"), must take out a financial guarantee of at least the maximum amount of funds it plans to hold. This guarantee is intended to cover receivables that have been designed during its activity.

The cardholder agrees to receive payments or rebates only within the amount of that guarantee.

Once he has taken out this guarantee, the deposit and deposit fund issues him a certificate of guarantee.

*For further information*: Articles 27 and following of the decree of July 20, 1972.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

#### Competent authority

The real estate professional must proceed with the declaration of his company. To do so, it must file a declaration with the Chamber of Commerce and Industry (CCI).

#### Supporting documents

The person concerned must provide the[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

#### Time

The ICC's Business Formalities Centre sends a receipt to the professional on the same day and, if necessary, indicates the missing documents on file. The person then has a period of 15 days to complete it. Once the file is complete, the ICC will tell the applicant which agencies their file has been forwarded to.

#### Remedies

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the CFE refuses to receive the file, the applicant has an appeal to the administrative court.

#### Cost

The cost of this declaration will depend on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

### b. If necessary, registration of the company's statutes

The real estate professional must, once the company's statutes have been dated and signed, register them with the corporate tax office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

#### Competent authority

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

#### Supporting documents

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

### c. Pre-declaration of activity for a secondary school

The real estate professional must also make a prior declaration of activity for each establishment or branch.

#### Competent authority

The prior declaration must be addressed by the applicant to the territorial ICC or to the departmental chamber of Ile-de-France of the place of situation of the secondary establishment or branch.

#### Supporting documents

The advance statement must state:

- When the request is made by a natural person, the identity and personal home of the registrant;
- when the application is made on behalf of a corporation:- The name, legal form, seat and purpose of the corporation,
  - marital status, residence, occupation and the quality of the legal or statutory representative.

Once the declaration is made, the professional receives a receipt for a return.

**Please note**

When changes are made, either in the address of the institution or in the person in charge, they must be reported by the professional to the competent authority mentioned above.

*For further information*: Article 3 of the Act of January 2, 1970 and Articles 2 and 8 of the Decree of July 20, 1972.

