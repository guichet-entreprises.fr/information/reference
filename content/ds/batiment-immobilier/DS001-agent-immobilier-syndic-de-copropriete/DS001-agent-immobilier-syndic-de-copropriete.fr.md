﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS001" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Agent immobilier - Syndic de copropriété - Administrateur de biens" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="agent-immobilier-syndic-de-copropriete" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/agent-immobilier-syndic-de-copropriete-administrateur-biens.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agent-immobilier-syndic-de-copropriete-administrateur-biens" -->
<!-- var(translation)="None" -->


# Agent immobilier - Syndic de copropriété - Administrateur de biens

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’agent immobilier, l’administrateur de biens et le syndic de copropriété (nommés ci-après « professionnels de l’immobilier ») sont des professionnels dont les missions consistent à participer aux opérations sur les biens d’autrui et relatives à :

- l’achat, la vente, la recherche, l’échange, la location ou sous-location, saisonnière ou non, en nu ou en meublé, d’immeubles bâtis ou non bâtis ;
- l’achat, la vente ou la location-gérance de fonds de commerce ;
- la cession d’un cheptel (fonds de bétail) mort ou vif ;
- la souscription, l’achat, la vente d’actions ou de parts de sociétés immobilières ou de sociétés d’habitat ;
- l’achat, la vente de parts sociales non négociables lorsque l’actif social comprend un immeuble ou un fonds de commerce ;
- la gestion immobilière ;
- la vente de listes ou de fichiers relatifs à l’achat, la vente, la location ou sous-location, en nu ou en meublé d’immeubles bâtis ou non, ou à la vente de fonds de commerce (à l’exception des publications par voie de presse) ;
- la conclusion de tout contrat de jouissance d’immeuble à temps partagé ;
- l’exercice des fonctions de syndic de copropriété.

*Pour aller plus loin* : articles 1er et suivants de la loi n° 70-9 du 2 janvier 1970 réglementant les conditions d’exercice des activités relatives à certaines opérations portant sur les immeubles et les fonds de commerce.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- en cas d’activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour les professions libérales, le CFE compétent est l’Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer cette activité, l’intéressé doit obtenir une carte professionnelle (cf. infra « 2°. d. Demande de carte professionnelle en vue d'un exercice permanent (LE) »).

Pour cela, le professionnel de l'immobilier doit soit :

- être titulaire de l'un des diplômes suivants :
  - un diplôme d’études supérieures de niveau licence (bac +3) sanctionnant des études juridiques, économiques ou commerciales,
  - un diplôme ou titre inscrit au Répertoire national des certifications professionnelles d’un niveau équivalent (bac +3) et sanctionnant des études de même nature,
  - le brevet de technicien supérieur (BTS) « Professions immobilières »,
  - le diplôme de l’Institut d’études économiques et juridiques appliquées à la construction et à l’habitation ;
- être titulaire du baccalauréat ou d'un diplôme ou d'un titre inscrit au Répertoire national des certifications professionnelles d'un niveau équivalent (niveau IV) et sanctionnant des études juridiques, économiques ou commerciales et détenir 3 ans d'expérience professionnelle dans l'immobilier ;
- avoir acquis une expérience professionnelle de dix ans pour les non-cadres ou quatre ans pour les cadres dans l'immobilier.

*Pour aller plus loin* : articles 11 à 14 du décret n° 72-678 du 20 juillet 1972.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement)

#### En cas de Libre Prestation de Services (LPS)

Le professionnel de l'immobilier ressortissant d'un État de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) peut, à condition d'être légalement établi, exercer son activité à titre temporaire et occasionnel.

Pour cela, il devra effectuer une demande de déclaration préalable auprès du président de la chambre de commerce et d'industrie territoriale ou de la chambre départementale d’Île-de-France.

Si ni l'activité professionnelle ni la formation ne sont réglementées dans l’État de l’UE ou de l’EEE, l’intéressé pourra exercer en France de manière temporaire et occasionnelle dès lors qu’il justifie avoir exercé une telle activité pendant au moins un an au cours des dix années précédant la prestation.

**À noter**

De plus, le ressortissant exerçant l'activité d'agent immobilier pourra demander la délivrance d’une carte professionnelle européenne (cf. infra « 2°. d. Carte professionnelle européenne (CPE) pour le ressortissant exerçant l'activité d'agent immobilier »).

*Pour aller plus loin* : article 8-1 de la loi du 2 janvier 1970.

#### En cas de Libre Établissement (LE)

Pour exercer l'activité de professionnel de l’immobilier en France à titre permanent, le ressortissant de l’UE ou de l’EEE devra remplir l’une des conditions suivantes :

- être titulaire d’une attestation de compétences ou d'un titre de formation délivré par une autorité compétente de l’État membre d’origine ;
- lorsque l'État de l’UE ou de l’EEE ne réglemente pas cette profession, avoir exercé l’activité pendant au moins un an au cours des dix années précédentes.

Dès lors que le ressortissant remplit l’une de ces conditions, il pourra solliciter la délivrance d’une carte professionnelle (cf. infra « 3°. d. Demande de carte professionnelle en vue d'un exercice permanent (libre établissement) »).

**À noter**

Le ressortissant doit également avoir une connaissance suffisante de la langue française.

### c. Conditions d'honorabilité et incompatibilités

Ne peut exercer en qualité de professionnel de l'immobilier le ressortissant ayant fait l'objet, depuis moins de dix ans, d'une condamnation définitive :

- pour crime ;
- à une peine d'au moins trois mois d'emprisonnement sans sursis ;
- et d'une destitution des fonctions d'officier public ou ministériel.

En outre, le professionnel ne doit pas avoir fait l'objet :

- d'une faillite ou d'une mesure d'interdiction d'exercice ;
- d’une radiation des fonctions d'administrateur ou de mandataire judiciaire ;
- d'une interdiction d'exercer une profession libérale pendant au moins six mois.

*Pour aller plus loin* : article 9 de la loi du 2 janvier 1970.

### d. Formalités et démarches pour le ressortissant UE

#### Demande de déclaration préalable en vue d'un exercice temporaire et occasionnel (LPS)

L'intéressé doit adresser une demande par lettre recommandée ou par voie électronique au président de la chambre de commerce et d’industrie territoriale ou de la chambre départementale d’Île-de-France dans le ressort de laquelle il envisage d'effectuer sa première prestation.

Cette demande doit être accompagnée d'un dossier comprenant :

- une attestation justifiant qu'il est légalement établi dans un État membre de l’UE ou de l’EEE et qu’il n’encourt aucune interdiction d’exercer ;
- lorsque l’État membre ne réglemente pas l’activité, tout document justifiant d’une expérience professionnelle pendant au moins un an au cours des dix années précédant la prestation ;
- un certificat de nationalité ;
- une attestation de garantie financière ;
- une attestation d’assurance de responsabilité civile professionnelle ;
- le cas échéant, une déclaration sur l’honneur qu’il ne détient aucun fonds, effet, ou valeur, autre que ceux représentatifs de sa rémunération.

*Pour aller plus loin* : article 16-6 du décret du 20 juillet 1972.

#### Demande de carte professionnelle en vue d'un exercice permanent (LE)

Le ressortissant d'un État membre de l'UE ou de l'EEE doit adresser une demande au président de la chambre de commerce et d’industrie territoriale ou de la chambre départementale d’Île-de-France.

Cette demande doit être accompagnée d'un dossier comportant les pièces justificatives suivantes :

- le formulaire [Cerfa n° 15312](https://www.service-public.fr/professionnels-entreprises/vosdroits/R13999) rempli et signé ;
- un justificatif d’aptitude professionnelle du demandeur ;
- une attestation de garantie financière ;
- une attestation d’assurance de responsabilité civile professionnelle ;
- un extrait du registre du commerce et des sociétés (RCS) datant de moins d’un mois si la personne est immatriculée, ou du double de la demande ;
- suivant le cas :
  - soit une attestation comportant le numéro de compte délivrée par l’établissement de crédit qui a ouvert le compte,
  - soit une attestation d’ouverture au nom de chaque mandant des comptes bancaires ;
- le cas échéant, la déclaration sur l’honneur que le demandeur ne détient aucun autre fonds, directement ou indirectement, que ceux représentant sa rémunération ;
- le bulletin n° 2 au casier judiciaire national ou un équivalent pour les ressortissants d’un autre État membre de l’UE ;
- [des frais de dossier d'instruction](https://www.entreprises.cci-paris-idf.fr/web/formalites/demande-carte-professionnelle-immobilier) d’un montant de 120 euros à régler en ligne.

##### Délais

Si le dossier est incomplet, l’autorité compétente adresse au professionnel la liste des pièces manquantes dans un délai de quinze jours à compter de sa réception. Si son dossier n’est pas complété dans les deux mois suivants, sa demande de carte sera rejetée.

*Pour aller plus loin* : articles 2 et suivants du décret du 20 juillet 1972.

#### Carte professionnelle européenne (CPE) pour le ressortissant exerçant l'activité d'agent immobilier

Le ressortissant d'un État membre de l'UE ou de l'EEE exerçant l'activité d'agent immobilier peut solliciter la délivrance d'une CPE lui permettant de faire reconnaître ses qualifications professionnelles dans un autre État de l'UE.

**À noter**

La procédure CPE peut être utilisée lorsque le ressortissant souhaite exercer son activité dans un autre État de l’UE à titre permanent comme à titre temporaire et occasionnel.

La CPE est valide :

- indéfiniment en cas d’établissement à long terme (libre établissement) ;
- dix-huit mois pour la prestation de services à titre temporaire.

##### Demande de CPE

Pour demander une CPE, le ressortissant doit :

- créer un compte utilisateur sur le [service d’authentification de la Commission européenne](https://webgate.ec.europa.eu/cas) ;
- remplir ensuite son profil CPE (identité, coordonnées…).

**À noter**

Il est également possible de créer une demande de CPE en téléchargeant les pièces justificatives scannées.

##### Coût

Pour chaque demande de CPE, les autorités du pays d’accueil et du pays d’origine peuvent prélever des frais d’examen de dossier dont le montant varie selon les situations.

##### Délais pour une demande de CPE en vue d'un exercice temporaire et occasionnel (LPS)

Dans un délai d’une semaine, l’autorité du pays d’origine accuse réception de la demande de CPE, signale s’il manque des documents et informe des frais éventuels. Puis, les autorités du pays d’accueil contrôlent le dossier.

Si aucune vérification n’est nécessaire auprès du pays d’accueil, l’autorité du pays d’origine examine la demande et prend une décision finale dans un délai de trois semaines.

Si des vérifications sont nécessaires au sein du pays d’accueil, l’autorité du pays d’origine a un mois pour examiner la demande et la transmettre au pays d’accueil. Le pays d’accueil prend alors une décision finale dans un délai de trois mois.

##### Délais pour une demande de CPE pour une activité permanente (LE)

Dans un délai d’une semaine, l’autorité du pays d’origine accuse réception de la demande de CPE, signale s’il manque des documents et informe des frais éventuels. Le pays d’origine dispose ensuite d’un délai d’un mois pour examiner la demande et la transmettre au pays d’accueil. Ce dernier prend la décision finale dans un délai de trois mois.

Si les autorités du pays d’accueil estiment que le niveau d’éducation ou de formation ou que l’expérience professionnelle sont inférieurs aux normes exigées dans ce pays, elles peuvent demander au demandeur de passer une épreuve d’aptitude ou d’effectuer un stage d’adaptation.

##### Issue de la demande de CPE

Si la demande d’obtention de CPE est accordée, il est alors possible d’obtenir un certificat de CPE en ligne.

Si les autorités du pays d’accueil ne prennent pas de décision dans les délais impartis, les qualifications sont tacitement reconnues et une CPE est délivrée. Il est alors possible d’obtenir un certificat de CPE à partir de son compte en ligne.

Si la demande d’obtention de CPE est rejetée, la décision de refus doit être motivée et présenter les voies de recours pour contester ce refus.

*Pour aller plus loin* : articles 16-8 et suivants du décret du 20 juillet 1972.

### e. Quelques particularités de la réglementation de l'activité

#### Formation continue obligatoire

Le professionnel de l'immobilier doit suivre une formation continue de 14 heures par an ou de 42 heures au cours de trois années consécutives d’exercice.

Le renouvellement de sa carte professionnelle est subordonné au respect de cette exigence.

*Pour aller plus loin* : articles 1er et suivants du décret n° 2016-173 du 18 février 2016 relatif à la formation continue des professionnels de l’immobilier.

#### Assurance civile professionnelle

Le professionnel de l'immobilier doit souscrire une assurance de responsabilité civile professionnelle en vue d'être couvert contre les risques financiers dans le cadre de son activité.

Cette garantie doit comporter les coordonnées du professionnel de l'immobilier ainsi que celles de son assureur. La limite de cette assurance civile professionnelle ne peut être inférieure à 76 224,51 euros par an pour un même assuré.

*Pour aller plus loin* : article 49 du décret du 20 juillet 1972 ; arrêté du 1er juillet 2005 fixant les conditions d'assurance et la forme du document justificatif prévu par le décret du 20 juillet 1972.

#### Garantie financière

Le professionnel de l’immobilier, titulaire de la carte professionnelle ou qui en fait la demande (cf. infra « 3°. a. Demande de carte professionnelle en vue d'un exercice permanent »), doit souscrire une garantie financière d’un montant au moins égal au montant maximal des fonds qu’il envisage de détenir. Cette garantie vise à couvrir les créances nées au cours de son activité.

Le titulaire de la carte s'engage à ne recevoir de versements ou de remise que dans la limite du montant de cette garantie.

Dès lors qu’il a souscrit cette garantie, la caisse des dépôts et consignations lui délivre une attestation de garantie.

*Pour aller plus loin* : articles 27 et suivants du décret du 20 juillet 1972.

## 3°. Démarches et formalités d'installation

### a. Formalités de déclaration de l'entreprise

#### Autorité compétente

Le professionnel de l'immobilier doit procéder à la déclaration de son entreprise. Pour cela, il doit, effectuer une déclaration auprès de la chambre de commerce et d'industrie (CCI).

#### Pièces justificatives

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI adresse le jour même un récépissé au professionnel et lui indique, le cas échéant, les pièces manquantes au dossier. L’intéressé dispose alors d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier a été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Si le CFE refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépendra de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### b. Le cas échéant, enregistrement des statuts de la société

Le professionnel de l'immobilier doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.

### c. Déclaration préalable d'activité pour un établissement secondaire

Le professionnel de l'immobilier doit en outre effectuer une déclaration préalable d'activité pour chaque établissement ou succursale.

#### Autorité compétente

La déclaration préalable doit être adressée par le demandeur à la CCI territoriale ou à la chambre départementale d’Île-de-France du lieu de situation de l'établissement secondaire ou de la succursale.

#### Pièces justificatives

La déclaration préalable doit indiquer :

- lorsque la demande est faite par une personne physique, l'identité et le domicile personnel du déclarant ;
- lorsque la demande est faite au nom d'une personne morale :
  - la dénomination, la forme juridique, le siège et l'objet de la personne morale,
  - l'état civil, le domicile, la profession et la qualité du ou des représentants légaux ou statutaires.

Une fois la déclaration effectuée, le professionnel reçoit un récépissé de déclaration.

**À noter**

Lorsque des changements sont apportés, soit quant à l'adresse de l'établissement, soit quant à la personne qui le dirige, ceux-ci doivent être déclarés par le professionnel à l'autorité compétente citée ci-dessus.

*Pour aller plus loin* : article 3 de la loi du 2 janvier 1970 et articles 2 et 8 du décret du 20 juillet 1972.