﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS002" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Architect" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="architect" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/architect.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="architect" -->
<!-- var(translation)="Auto" -->


Architect
=========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The architect is responsible for the various phases of the design and construction of the work of a work.

First, it is required to carry out a feasibility survey of the land before drawing the first plans of the future building. He will obtain the building permit and negotiate prices with the various contractors who will work on the site. Throughout the project, it will have to take into account planning regulations, legal and technical constraints, as well as the client's budget and deadlines requirements.

Once the plans have been designed, the architect coordinates the teams responsible for the construction of the construction site until the delivery of the work.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for a liberal profession, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The practice of the profession of architect is reserved for those registered in the regional table of architects. Registration is open to holders of the State Architect Diploma who have completed an additional year of training leading to the certification to practice the master's degree in its own name (HMONP).

Architectural training is delivered at 20 national higher schools of architecture throughout France, at the National Higher Institute of Applied Sciences (INSA) in Strasbourg, and at the Special School of Architecture (ESA) in Paris.

It is accessible after graduation and lasts five years. Access is made after reviewing the candidate's file, complete with an interview that may include tests.

The studies consist of two cycles:

- a three-year undergraduate degree that leads to the Architecture Studies Diploma (DEEA);
- a second cycle in two years (master level) that leads to the State Diploma of Architect (DEA).

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

#### For a temporary and casual exercise (LPS)

A national of a European Union (EU) or European Economic Area (EEA) state legally practising as an architect in one of these states may use his or her professional title in France on a temporary or casual basis.

He will have to make the request, before his performance, the request by written declaration to the regional council of the Order of Architects (see infra "3." b. Make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)").

In the event of substantial differences between his professional qualifications and the training required in France, the council may submit him to an aptitude test which consists of a thirty-minute oral test. It will cover all or part of the subjects covered by Article 9 of the December 17, 2009 decree on the recognition of professional qualifications for the practice of the architectural profession. Depending on the subject, an architectural project may be asked of the national to assess his skills.

*To go further* Article 10-1 of Law 77-2 of January 3, 1977 on architecture; Articles 10 to 14 of Decree No. 2009-1490 of December 2, 2009 on the recognition of professional qualifications for the practice of the profession of architect.

#### For a permanent exercise (LE)

The national of an EU or EEA state may settle in France to practice permanently if he or she holds:

- the state diploma of architect or another French diploma of architect recognized by that state, and the HMONP;
- a diploma, certificate or other foreign title recognized by that state allowing the practice of the profession of architect and recognized by the state.

In both cases, he will be able to apply directly for his registration in the regional list of architects with the regional council of the Order of Architects (see infra "3°. d. Request registration on the regional architect's list").

Can also settle in France permanently, the EU or EEA national who:

- holds a training certificate issued by a third state but recognised by an EU or EEA state and has allowed the profession of architect to be practised for at least three years;
- was recognized as qualified by the Minister for Culture, after a review of all his knowledge, qualifications and professional experience;
- was recognized as qualified by the Minister for Culture after presenting professional references that attest that he distinguished himself by the quality of his architectural achievements, after advice from a national commission.

Once the national fulfils one of these conditions, he or she will be able to apply for recognition of his professional qualifications before applying for registration on the board (see below "3o). c. If necessary, request prior recognition of his professional qualifications for the EU or EEA national for inclusion in the regional architect's list").

If there are substantial differences between his training and the professional qualifications required in France, the national may be subjected to an aptitude test organised within six months of his decision.

*To go further* Article 10 of Law 77-2 of January 3, 1977 on architecture; Articles 1 to 9 of Decree No. 2009-1490 of December 2, 2009 on the recognition of professional qualifications for the practice of the architectural profession.

### c. Some peculiarities of the regulation of the activity

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

As the premises are open to the public, the professional must comply with the rules relating to public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

#### Legal obligations

The architect must declare to the regional council any links he may have with a natural or legal person whose activity benefits from the construction.

It shall not engage in commercial activities that are not distinct, independent or public knowledge.

He is also required to take out professional liability insurance, when he exercises in a liberal capacity, covering the acts performed during his mission.

#### Compliance with the code of ethics

The provisions of the Code of Ethics are imposed on all architects practising in France.

As such, the architect must respect the principles of respect for professional confidentiality, competition and conflicts of interest. For more information, please visit the Order of Architects website.

*To go further* Sections 18 and 19 of Law 77-2 of January 3, 1977 on architecture.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the contractor must register with the Register of Trade and Companies (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Make a pre-declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)

**Competent authority**

The Regional Council of the Order of Architects of the delivery site is responsible for deciding on the application for prior declaration.

**Supporting documents**

The request for prior declaration of activity is made by filing a file that includes:

- a statement mentioning the national's intention to provide services in France;
- A certificate of professional civil insurance of the national less than three months old;
- A copy of the training titles
- A copy of the certificate certifying that the national is established in an EU or EEA state and is not prohibited from practising;
- a copy of a valid ID.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

The competent authority has one month from the receipt of the declaration to decide on the request for declaration. When the applicant is required to pass an aptitude test, the regional council will have an additional one month to decide.

*To go further* Articles 14 to 17 of the December 17, 2009 decree on the recognition of professional qualifications for the practice of the architectural profession.

### c. If necessary, request prior recognition of his professional qualifications for the EU or EEA national in view of his registration in the regional architect's list

**Competent authority**

The Minister for Culture is responsible for deciding on applications for recognition of the national's professional qualifications.

**Supporting documents**

The application for recognition must be addressed to the competent authority and submitted by file, in two copies, containing the following documents:

- A copy of a valid ID
- A copy of the training titles
- If applicable, a copy of the detailed description of the curriculum and its hourly volume, as well as a copy of the authorisation to carry the title of architect;
- If applicable, a copy of the training title issued by a third state but recognised by an EU or EEA state, as well as a copy:- either the certification issued by the EU State or the EEA attesting to the practice of the architectural profession for at least three years in that state,
  - either any document issued by the EU state or the EEA attesting to the practice of the architectural profession;
- a description of the training and work experience relevant to the exercise of the activity.

In the case of the national who would have distinguished himself by the quality of his achievements in the field of architecture, the file will include:

- A resume
- A letter outlining his motivations
- a collection presenting a diverse selection of studies and projects carried out showing their exterior appearance, interior design and insertion into the site, illustrated with photographs, plans and cuts as well as sketches and drawings;
- A list of references to the work and studies carried out;
- A copy of articles and publications devoted to the achievements presented or their author;
- Certificates and recommendations from employers and customers
- a certificate issued by the EU or EEA State certifying that the applicant's activities are architectural.

**Procedure**

The competent authority will acknowledge receipt of the file within one month. Upon receipt of the full file, the Minister responsible for culture will have four months to make his decision, after advice from the National Council of the Order of Architects.

In the event of substantial differences between the training and the qualifications required in France, he may decide to subject the national to the same aptitude test as that requested in the case of free provision of services.

*To go further* Articles 3 to 7 of Decree No. 2009-1490 of 2 December 2009 on the recognition of professional qualifications for the practice of the architectural profession; Articles 2 to 13 of the Decree of 17 December 2009 on the modalities of recognition of professional qualifications for the practice of the profession of architect.

### d. Request registration on the regional architect's list

**Competent authority**

The Regional Council of the College of Architects is responsible for deciding on applications to register on the Order's board.

**Supporting documents**

The request is made by sending a file in two copies with the following supporting documents:

- A copy of a valid ID
- A copy of the training title
- a certificate of professional civil insurance of the person concerned dates back less than three months;
- an extract from the criminal record.

**Procedure**

The regional council will acknowledge receipt of the file within one month. The council's silence within two months will be worth the decision to reject the application.

*To go further* Article 1 of the Decree of 17 December 2009 on the terms of recognition of professional qualifications for the practice of the architectural profession.

