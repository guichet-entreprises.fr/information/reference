﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS002" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Société d'architecture" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="societe-darchitecture" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/societe-darchitecture.html" -->
<!-- var(last-update)="2021-01" -->
<!-- var(url-name)="societe-darchitecture" -->
<!-- var(translation)="None" -->


# Société d'architecture

Dernière mise à jour : <!-- begin-var(last-update) -->2021-01<!-- end-var -->

## 1°. Définition de l'activité

Les sociétés immatriculées au registre des commerces et sociétés et inscrites au tableau de l’Ordre des architectes peuvent seules porter le titre de société d’architecture. Cette inscription permet à ces sociétés d’exercer la profession d’architecte en France. 

L’exercice de cette activité se fait en conformité avec le Code de déontologie des architectes. 

Toute société d'architecture doit communiquer ses statuts, la liste de ses associés ainsi que toute modification statutaire éventuelle au conseil régional de l'Ordre des architectes sur le tableau duquel elle a demandé son inscription.

## 2°. La constitution d’une société d’architecture

### Les personnes pouvant créer une société d’architecture

Les sociétés d’architecture ne peuvent être constituées que par une personne physique qui est soit un architecte inscrit au tableau de l’Ordre des architectes en France, soit une personne physique établie dans un autre État membre de l’Union européenne ou dans un autre État partie à l’accord sur l’Espace économique européen et exerçant légalement la profession d’architecte dans les conditions définies aux 1° à 4° de l’article 10 ou à l’article 10-1 de la loi n° 77-2 du 3 janvier 1977 sur l’architecture. Ces personnes peuvent s’associer avec des personnes physiques ou morales. Ces sociétés peuvent grouper des architectes ou des sociétés d'architecture inscrits à différents tableaux régionaux. 

### Le capital d’une société d’architecture

Les actions de la société d’architecture doivent revêtir la forme nominative. Plus de la moitié du capital social et des droits de vote doivent être détenus par :

- un ou plusieurs architectes ou une ou plusieurs personnes physiques établies dans un autre État membre de l’Union ou partie à l’EEE et exerçant légalement la profession d’architecte dans les conditions définies aux 1° à 4° de l'article 10 ou à l'article 10-1 de la loi du 3 janvier 1977 sur l’architecture ;
- des sociétés d’architecture ou des personnes morales établies en France ou dans un autre État membre de l’Union européenne ou partie à l’accord sur l’Espace économique européen dont plus de la moitié du capital et des droits de vote est détenue par des architectes. 

Les personnes morales associées qui ne sont pas des sociétés d’architecture ne peuvent pas détenir plus de 25 % du capital social et des droits de vote des sociétés d’architecture. Ce pourcentage est de 49 % dans les sociétés d’architecture d’exercice libérale (SEL).

### Les membres d’une société d’architecture

L’adhésion d’un nouvel associé dans une société d’architecture, excepté l’entreprise unipersonnelle à responsabilité limitée (EIRL), est subordonnée à l’agrément préalable de l’assemblée générale statuant à la majorité des deux tiers. 

Le président du conseil d’administration, le directeur général s’il est unique, la moitié au moins des directeurs généraux, des membres du directoire et des gérants, ainsi que la majorité au moins des membres du conseil d’administration et du conseil de surveillance doivent être des architectes ou sociétés d’architecture établies dans un État membre de l’Union européenne ou partie à l’accord sur l’Espace économique européen.

### L’assurance professionnelle

Toute société d’architecture doit transmettre chaque année une assurance professionnelle au tableau de l’Ordre des architectes. Les sociétés d’architecture sous la forme d'une société à responsabilité limitée (SARL) ou d'une société anonyme (SA) doivent souscrire une assurance professionnelle pour les architectes intervenant en qualité d'agent public, en qualité de salarié ou en qualité d’associé dans leurs entreprises.

Quelle que soit la forme sociale adoptée, toute société d'architecture est solidairement responsable des actes professionnels accomplis pour son compte par des architectes. 

## 3°. Les différentes formes des sociétés d’architecture

Les formes de sociétés d’architectures auxquelles il peut être recouru sont les suivantes :

- société à responsabilité limitée d'architecture (SARL d’architecture) ;
- entreprise unipersonnelle à responsabilité limitée d’architecture (EURL d’architecture) ;
- société par actions simplifiée d'architecture (SAS d’architecture) ;
- société par actions simplifiée unipersonnelle (SASU d’architecture) ;
- société anonyme d'architecture (SA d’architecture) ;
- société coopérative de production sous forme de SARL à capital variable (SCOP SARL d’architecture) ;
- société d’exercice libéral à responsabilité limitée d'architecture (SELARL d’architecture) ;
- société d’exercice libéral sous forme de société anonyme (SELAFA d’architecture) ;
- société d’exercice libéral sous forme de société par actions simplifiée (SELAS d’architecture) ;
- société civile professionnelle d'architecture (SCP d’architecture) ;
- société de participation financière de profession libérale d’architectes (SFFPL d’architecture).

## 4°. La création d’une succursale en France par une société d’architecture située dans un État membre de l’Union européenne ou dans un autre État partie à l’accord sur l’Espace économique européen

Les sociétés d'architecture ou des personnes morales établies dans un autre État membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen dont plus de la moitié du capital et des droits de vote est détenue par des personnes qualifiées, au sens des articles 10 ou 10-1 de la loi du 3 janvier 1977 sur l’architecture, et exerçant légalement la profession d'architecte peuvent ouvrir une succursale en France, à condition qu’elle soit inscrite au tableau de l’Ordre des architectes.

La succursale dispose d’une autonomie de gestion et est pourvue d’une direction qui lui est propre. Elle doit être déclarée au registre du commerce et des sociétés français et inscrite sur le registre des succursales du tableau de l’Ordre des architectes, respecter les dispositions législatives et réglementaire relatives à l’exercice de la profession d’architecte, au Code de déontologie, transmettre au tableau de l’Ordre des architectes une assurance professionnelle et payer la cotisation ordinale.

La succursale doit adresser une fois par an au tableau de l’Ordre des architectes, un état de la composition du capital social de la société mère.

## 5°. Démarches et formalités d’inscription d’une société ou d’une succursale au tableau de l’Ordre des architectes

### Autorité compétente

Le conseil régional de l'Ordre des architectes est compétent pour se prononcer sur les demandes d'inscription au tableau de l’Ordre des architectes.

### Pièces justificatives pour l’inscription d’une société d’architecture

La demande d'inscription est accompagnée d'un dossier comprenant les pièces justifiant que la société remplit les conditions fixées par la loi. Les pièces nécessaires pour l’inscription au tableau de l’Ordre des architectes sont les suivantes :

- un exemplaire des statuts signés ;
- l’attestation d'inscription individuelle au tableau de l’Ordre des architectes ou à son annexe des architectes associés ou leur demande d’inscription individuelle ;
- la requête individuelle de chaque architecte associé, datée et signé ;
- le paiement du droit d’inscription requis pour l’instruction du dossier.

### Pièces justificatives pour l’inscription d’une succursale

La demande d'inscription est accompagnée d'un dossier comprenant les pièces justifiant que la succursale remplit les conditions fixées par la loi. Les pièces nécessaires pour l’inscription au tableau de l’Ordre des architectes sont les suivantes :

- une copie des statuts à jour de la société mère ;
- une copie de la demande d’immatriculation de la succursale au registre du commerce et des sociétés ;
- une copie du diplôme, certificat ou autre titre permettant l’exercice de la profession d’architecte et reconnu par l’État, de toutes les personnes physiques associées majoritaires de la société mère et de la personne physique représentant la société mère dans la succursale ;
- une copie de l’acte de nomination du responsable de la succursale ;
- une copie du justificatif de jouissance des locaux où est installée la succursale ;
- le paiement du droit d’inscription requis pour l’instruction du dossier.

**À savoir**

Les pièces doivent être traduites en français par un traducteur agréé.

### Procédure

La demande d’inscription est déposée ou adressée par lettre recommandée avec demande d’avis de réception au siège du conseil régional de l’Ordre des architectes du lieu dans lequel la société ou la succursale souhaite s’installer. 

La décision d’inscription ou de refus d’inscription du conseil régional de l’Ordre des architectes est motivée. Elle est notifiée immédiatement à l’intéressé par lettre recommandée avec demande d’avis de réception. S’il s’agit d’un refus, elle précise le délai et les modalités du recours.

### Délais

Le conseil régional en accuse réception par écrit. Le silence gardé par le conseil régional pendant un délai de deux mois sur la demande d’inscription vaut décision de rejet. 

### Recours

Le recours devant le ministre chargé de la culture est un préalable obligatoire au recours contentieux.

Le silence gardé par le conseil régional de l’Ordre des architectes pendant deux mois sur une demande d’inscription au tableau de l’Ordre des architectes vaut décision de rejet. Ainsi, en cas de silence du conseil régional de l’Ordre des architectes ou de décision défavorable, le demandeur peut former un recours dans un délai de deux mois auprès du ministre chargé de la culture.

Le ministre chargé de la culture a un délai de deux mois pour se prononcer sur le recours. Le silence gardé par le ministre chargé de la culture pendant deux mois vaut décision de rejet. Ainsi, en cas de silence de celui-ci ou de décision défavorable, le demandeur peut former un recours contentieux dans un délai de deux mois auprès du tribunal administratif de Paris. 

*Pour aller plus loin :* articles 9, 12, 13, 14 et 16 de la loi n° 77-2 du 3 janvier 1977 sur l’architecture ; articles 17 à 23 du décret n° 77-1481 du 28 décembre 1977 sur l'organisation de la profession d'architecte.