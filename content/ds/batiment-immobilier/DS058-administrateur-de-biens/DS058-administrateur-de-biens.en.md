﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS058" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Property manager" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="property-manager" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/property-manager.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="property-manager" -->
<!-- var(translation)="Auto" -->

Property manager
======================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The property administrator is a mandated professional whose activity consists of managing real estate on behalf of either an individual, a syndicate of co-owners or a civil real estate company on a daily basis.

As such, he may be responsible for the day-to-day management of assets (payment of taxes, invoices, maintenance), the resolution of various disputes concerning them, or their rental management (finding tenants and managing the rental).

*For further information*: Article 1 of Act 70-9 of January 2, 1970 regulating the conditions for the conduct of activities relating to certain transactions involving real estate and commercial funds.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out.

- For a commercial activity, the relevant CFE will be the Chamber of Commerce and Industry (CCI);
- If an individual business is set up, it is the Urssaf;
- for civil societies, the competent CFE is the registry of the Commercial Court or the registry of the district court in the departments of Lower Rhine, Upper Rhine and Moselle.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal, regardless of the number of employees of the company on the condition that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To be a property administrator, the professional must hold a business card and must:

- Be professionally qualified
- justify a financial guarantee
- have taken out professional liability insurance.

To be recognized as a professionally qualified person, the person must:

- or hold one of the following degrees:- a degree at a Bac-3 level that punishes legal, economic or commercial studies,
  - a diploma or a title in the national directory of professional certifications ([RNCP](http://www.rncp.cncp.gouv.fr/grand-public/recherche)),
  - a Senior Technician's Certificate (BTS) marked "Real Estate Professions"
  - a diploma from the Institute for Economic and Legal Studies applied to construction and housing (ICH);
- or justify at least ten years of professional experience (four years for executives) in the relevant activity.

Once they meet these conditions, they can apply for a business card (see infra "3 degrees). a. Request for a business card").

*For further information*: Article 3 of the aforementioned 2 January 1970 law; Articles 12-15 of Decree 72-678 of 20 July 1972 setting out the conditions for the application of Act No. 70-9 of 2 January 1970 regulating the conditions of activities relating to certain transactions relating to buildings and commercial funds.

**Continuous training**

Continuous training of 14 hours per year or 42 hours during three consecutive years of practice is mandatory for any real estate professional.

**Please note**

The renewal of his business card is conditional on compliance with this requirement.

*For further information*: Articles 1 and following of Decree No. 2016-173 of February 18, 2016 on the continuing education of real estate professionals.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

**For temporary and casual exercise (LPS)**

Any national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA), acting as a property administrator, may carry out the same activity in France on a temporary and casual basis.

In order to do so, the person concerned must make a prior declaration before his first performance with the President of the Territorial ICC, or with the departmental chamber of Ile-de-France (see infra "3°. b. Pre-declaration for the EU national for a temporary and occasional exercise").

Moreover, where neither access to the activity nor its exercise is regulated in that Member State, the national must justify having carried out this activity for at least one year in the ten years preceding his first benefit.

*For further information*: Article 8-1 of the Act of January 2, 1970 and Articles 16-6 and 16-7 of the Decree of July 20, 1972.

**For a permanent exercise (LE)**

Any legally established EU national who acts as a property administrator may carry out the same activity in France on a permanent basis. For this, the professional must:

- Hold a certificate of competence or a training certificate issued by the competent authority of the Member State regulating the activity;
- Where the activity is not regulated by the Member State, justify having been a property administrator for at least one year in the last ten years;
- be in possession of the language skills necessary to carry out their activity in France.

Once the professional meets these requirements, he or she can apply for a business card (see infra "3°. a. Request for a business card").

*For further information*: Article 16-1 of the decree of 20 July 1972.

### c. Conditions of honorability

The property administrator, as a real estate professional, must carry out his activity with all probity and morality.

In addition, it must not have been the subject of:

- a conviction for less than ten years for a crime;
- a sentence of at least three months' imprisonment without a conditional sentence, including:- Scam
  - breach of trust,
  - Receiving
  - Money laundering
  - active or passive corruption, influence peddling, subtraction or embezzlement of property,
  - falsification of securities or fiduciary values, falsification of the authority's trademarks,
  - participation in an association of criminals,
  - drug trafficking,
  - pimping or prostitution,
  - bankruptcy, loan sharking practice, tax evasion, bank canvassing.

**Please note**

The professional who is the subject of such an incapacity during the course of his activity, incurs a sentence of five years' imprisonment and a fine of 375,000 euros for the offence of fraud and must cease to practise within one month of the decision Final.

*For further information*: Article 9 and following of the Act of January 2, 1970.

### d. Some peculiarities of the regulation of the activity

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of June 25, 1980 approving the general provisions of the fire and panic safety regulations in public institutions.

It is advisable to refer to the listing " [Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

**Obligation to justify a financial guarantee**

The property administrator, who holds or is applying for the business card, must apply for a financial guarantee of at least the maximum amount of funds he or she plans to hold. This guarantee is intended to cover receivables that have been designed during its activity. In addition, the cardholder will only be able to receive payments or rebates within the amount of that guarantee.

Once he has taken out this guarantee, the deposit and deposit fund issues him a certificate of guarantee.

*For further information*: Article 27 and following of the decree of 20 July 1972.

**Take out professional liability insurance**

The property administrator, as a real estate professional, must take out insurance in order to cover financial risks in the course of his activity.

This insurance policy must include its contact information as well as those of the insurance agency. The limit of the guarantee cannot be less than 76,224.51 euros per year for the same insured.

*For further information*: Article 49 of the decree of 20 July 1972, decree of 1 July 2005 setting out the conditions of insurance and the form of the supporting document provided for by the decree of 20 July 1972.

**Obligation to be in possession of a warrant**

The administrator of property engaged in property management or condominium trustee activities is required to:

- to be in possession of a signed written warrant, mentioning the extent of its powers to receive property, monies or securities in the administration of the assets in his charge;
- keep a record of the warrants received Model 15 September 1972.

**Please note**

The professional performing one of these activities may receive sums other than those resulting from the administration of the assets for which he is responsible but only on a temporary basis from then on:

- he has been managing the property, which has been the subject of the contract for more than three years;
- that these sums are included in the amount of the financial guarantee;
- that he has been given a special mandate to hold this office;
- that the risks incurred during this activity are covered by insurance specific to property management activities or condominium trustees or by special or supplementary insurance taken out with an insurance company.

*For further information*: Article 6 of the Act of 2 January 1970; Articles 64 to 71 of the decree of 29 July 1972.

**Posting requirement**

The professional holding the professional card is required to put up a poster in all the premises where he or she carries out his activity stating:

- The number of his card
- The amount of his guarantee underwritten
- if necessary, the name and address of the manager.

In addition, all documents issued must include the information:

- business card (number and place of issue);
- The nature of its business
- The name and name of his company
- If so, the name and address of the guarantor
- if so, the statement that he should not receive or hold funds other than those intended for the exercise of his activity.

*For further information*: Articles 93 to 95 of the decree of 20 July 1972.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Request for a business card

**Competent authority**

The professional must apply to the President of the Territorial ICC or the Departmental Chamber of Ile-de-France in the jurisdiction of which is either his seat if the applicant is a legal person or his principal institution if the applicant is a natural person.

**Supporting documents**

His request should mention:

- The nature of the proposed transactions;
- if the applicant is a natural person:- his marital status,
  - his profession,
  - His home and professional address
- if the applicant is a legal entity:- its name, its legal form and the purpose of its activity,
  - its head office,
  - information about its legal or statutory representatives (civil status, occupation, residence).

In addition, his application must be accompanied by the following documents:

- The form Cerfa 15312*01 completed and signed;
- Proof of the applicant's professional fitness
- A financial guarantee certificate
- a certificate of professional liability insurance;
- an extract from the Register of Trade and Companies (RCS) less than one month old if the person is registered, or double the demand;
- depending on the case:- certificate with the account number, issued by the credit institution that opened the account,
  - or a certificate of openness on behalf of each principal of bank accounts;
- if applicable, the declaration of honour that the applicant does not hold any other funds, directly or indirectly than those representing his remuneration;
- bulletin 2 to the national criminal record or an equivalent for nationals of another EU Member State.

**Cost**

The application must be accompanied by the application fee of an amount of[120 euros](https://www.entreprises.cci-paris-idf.fr/web/formalites/demande-carte-professionnelle-immobilier).

**Timeframe**

If the file is incomplete, the competent authority sends the professional a list of missing documents within a fortnight of receiving it. If his file is not completed within two months, his application for a card will be rejected.

*For further information*: Articles 2 and 3 of the decree of 20 July 1972 above.

### b. Pre-declaration for EU national for temporary and casual exercise (LPS)

**Competent authority**

The national must send his application, by letter recommended with notice of receipt, or electronically, to the president of the territorial ICC or the departmental chamber of Ile-de-France in the jurisdiction of which he wishes to perform his service services.

**Supporting documents**

His application must include:

- a certificate certifying that it is legally established in an EU or EEA Member State and that it is not prohibited from practising the activity of property administrator;
- a document justifying that he has been engaged in this activity for at least one year in the last ten years if the Member State in which it is established does not regulate this activity;
- proof of nationality
- a document attesting that he has the financial guarantee required to exercise (see above "2." d. Obligation to justify a financial guarantee");
- A proof that he has professional liability insurance;
- a statement of honour that he does not hold, nor has he directly or indirectly received any other funds, effects or values other than those included in his remuneration or fees.

*For further information*: Article 16-6 of the decree of 20 July 1972.

### c. Company reporting formalities

The professional is required to register with the Trade and Companies Register (RCS) with the ICC in the event of the creation of a commercial company or to make a declaration of activity with the Urssaf in case of creation of an individual company.

It is advisable to refer to the "Registration of a commercial individual business at the RCS" for more information.

### d. Requirement to declare a secondary school

If a secondary or branch is opened, the professional must first declare them.

**Competent authority**

The professional must apply to the ICC of the location of the secondary school.

**Supporting documents**

His application must contain the form Cerfa 15312*01 identical to that required for the issuance of the business card and the supporting documents required for this procedure.

**Delays and procedures**

The ICC issues a return receipt as soon as the application is received.

*For further information*: Article 3 of the Act of 2 January 1970; Article 8 of the decree of 20 July 1972

