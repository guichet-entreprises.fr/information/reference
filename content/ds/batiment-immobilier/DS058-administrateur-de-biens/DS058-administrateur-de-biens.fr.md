﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS058" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Administrateur de biens" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="administrateur-de-biens" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/administrateur-de-biens.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="administrateur-de-biens" -->
<!-- var(translation)="None" -->

# Administrateur de biens

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'administrateur de biens est un professionnel mandaté dont l'activité consiste à gérer au quotidien, des biens immobiliers pour le compte, soit d'un particulier, d'un syndicat de copropriétaires ou d'une société civile immobilière.

À ce titre, il peut être chargé de la gestion courante des biens (paiement des impôts, des factures, entretien), du règlement des différents litiges les concernant, ou encore de leur gestion locative (de trouver des locataires et gérer la location).

*Pour aller plus loin* : article 1er de la loi n°70-9 du 2 janvier 1970 réglementant les conditions d'exercice des activités relatives à certaines opérations portant sur les immeubles et les fonds de commerce.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée.

- pour une activité commerciale, le CFE compétent sera la chambre de commerce et d'industrie (CCI) ;
- en cas de création d'une entreprise individuelle, il s'agit de l'Urssaf ;
- pour les sociétés civiles, le CFE compétent est le greffe du tribunal de commerce ou le greffe du tribunal d'instance dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale, quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité d'administrateur de biens, le professionnel doit être titulaire d'une carte professionnelle et doit pour cela :

- être qualifié professionnellement ;
- justifier d'une garantie financière ;
- avoir souscrit une assurance de responsabilité civile professionnelle.

Pour être reconnu comme étant qualifié professionnellement, l'intéressé doit :

- soit être titulaire de l'un des diplômes suivants :
  - un diplôme d'un niveau Bac+3 sanctionnant des études juridiques, économiques ou commerciales,
  - un diplôme ou un titre inscrit au Répertoire national des certifications professionnelles ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/),
  - un brevet de technicien supérieur (BTS) mention « Professions immobilières »,
  - un diplôme de l'Institut d'études économiques et juridiques appliquées à la construction et à l'habitation (ICH) ;
- soit justifier d'une expérience professionnelle d'au moins dix ans (quatre ans pour les cadres) au sein de l'activité concernée.

Dès lors qu'il remplit ces conditions, il peut effectuer une demande de carte professionnelle (cf. infra « 3°. a. Demande en vue d'obtenir une carte professionnelle »).

*Pour aller plus loin* : article 3 de la loi du 2 janvier 1970 susvisée ; articles 12 à 15 du décret n° 72-678 du 20 juillet 1972 fixant les conditions d'application de la loi n°70-9 du 2 janvier 1970 réglementant les conditions d'exercice des activités relatives à certaines opérations portant sur les immeubles et fonds de commerce.

#### Formation continue

Une formation continue de 14 heures par an ou de 42 heures au cours de trois années consécutives d’exercice est obligatoire pour tout professionnel de l’immobilier.

**À noter**

Le renouvellement de sa carte professionnelle est subordonné au respect de cette exigence.

*Pour aller plus loin* : articles 1er et suivants du décret n°2016-173 du 18 février 2016 relatif à la formation continue des professionnels de l’immobilier.

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services (LPS) ou Libre établissement (LE))

#### En vue d'un exercice temporaire et occasionnel (LPS)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE), exerçant l'activité d'administrateur de biens, peut exercer à titre temporaire et occasionnel, la même activité en France.

Pour cela, l'intéressé doit effectuer avant sa première prestation, une déclaration préalable auprès du président de la CCI territoriale, ou auprès de la chambre départementale d'Île-de-France (cf. infra « 3°. b. Déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel »).

En outre, lorsque ni l'accès à l'activité, ni son exercice ne sont réglementés dans cet État membre, le ressortissant doit justifier avoir exercé cette activité pendant au moins un an au cours des dix années précédant sa première prestation.

*Pour aller plus loin* : article 8-1 de la loi du 2 janvier 1970 et articles 16-6 et 16-7 du décret du 20 juillet 1972.

#### En vue d'un exercice permanent (LE)

Tout ressortissant de l'UE légalement établi et exerçant l'activité d'administrateur de biens, peut exercer à titre permanent, la même activité en France. Pour cela, le professionnel doit :

- être titulaire d'une attestation de compétence ou d'un titre de formation délivré par l'autorité compétente de l’État membre réglementant l'activité ;
- lorsque l'activité n'est pas réglementée par l’État membre, justifier avoir exercé l'activité d'administrateur de biens pendant au moins un an au cours des dix dernières années ;
- être en possession des connaissances linguistiques nécessaires à l'exercice de son activité en France.

Dès lors qu'il remplit ces conditions, le professionnel peut effectuer une demande de carte professionnelle (cf. infra « 3°. a. Demande en vue d'obtenir une carte professionnelle »).

*Pour aller plus loin* : article 16-1 du décret du 20 juillet 1972.

### c. Conditions d’honorabilité

L'administrateur de biens, en tant que professionnel de l'immobilier doit exercer son activité en toute probité et moralité.

En outre, il ne doit pas avoir fait l'objet :

- d'une condamnation depuis moins de dix ans pour crime ;
- d'une peine d'au moins trois mois d'emprisonnement sans sursis, notamment pour :
  - escroquerie,
  - abus de confiance,
  - recel,
  - blanchiment,
  - corruption active ou passive, trafic d'influence, soustraction ou détournement de biens, 
  - faux, falsification de titres ou valeurs fiduciaires, falsification des marques de l'autorité, 
  - participation à une association de malfaiteurs, 
  - trafic de stupéfiants,
  - proxénétisme ou prostitution,
  - banqueroute, pratique de prêt usuraire, fraude fiscale, démarchage bancaire.

**À noter**

Le professionnel qui fait l'objet d'une telle incapacité en cours d'activité, encourt une peine de cinq ans d'emprisonnement et de 375 000 euros d'amende prévue pour le délit d'escroquerie et doit cesser d'exercer dans un délai d'un mois à compter de la décision définitive.

*Pour aller plus loin* : article 9 et suivants de la loi du 2 janvier 1970.

### d. Quelques particularités de la réglementation de l’activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

#### Obligation de justifier d'une garantie financière

L'administrateur de biens, titulaire de la carte professionnelle ou qui en fait la demande, doit solliciter une garantie financière d’un montant au moins égal au montant maximal des fonds qu’il envisage de détenir. Cette garantie vise à couvrir les créances nées au cours de son activité. De plus, le titulaire de la carte ne pourra recevoir de versements ou de remise que dans la limite du montant de cette garantie.

Dès lors qu’il a souscrit cette garantie, la caisse des dépôts et consignations lui délivre une attestation de garantie.

*Pour aller plus loin* : article 27 et suivants du décret du 20 juillet 1972.

#### Souscrire une assurance de responsabilité civile professionnelle

L'administrateur de biens en qualité de professionnel de l’immobilier, doit souscrire une assurance afin de le couvrir des risques financiers dans le cadre de son activité.

Ce contrat d'assurance doit comporter ses coordonnées ainsi que celles de l’organisme d’assurance. La limite de la garantie ne peut être inférieure à 76 224,51 euros par an pour un même assuré.

*Pour aller plus loin* : article 49 du décret du 20 juillet 1972, arrêté du 1er juillet 2005 fixant les conditions d’assurance et la forme du document justificatif prévu par le décret du 20 juillet 1972.

#### Obligation d'être en possession d'un mandat

L'administrateur de biens exerçant les activités de gestion immobilière ou de syndic de copropriété est tenu :

- d'être en possession d'un mandat écrit signé, mentionnant l'étendue de ses pouvoirs en vue de recevoir des biens, des sommes ou des valeurs à l'occasion de l'administration des biens dont il a la charge ;
- tenir un registre des mandats reçus dont le modèle est fixé par l'arrêté du 15 septembre 1972.

**À noter**

Le professionnel exerçant l'une de ces activités, peut recevoir des sommes autre que celles résultants de l'administration des biens dont il a la charge mais uniquement à titre temporaire dès lors :

- qu'il gère le bien, objet du contrat depuis plus de trois ans ;
- que ces sommes sont comprises dans le montant de la garantie financière ;
- qu'il a reçu mandat spécial pour exercer cette fonction ;
- que les risques encourus au cours de cette activité sont couverts par une assurance spécifique aux activités de gestion immobilière ou de syndic de copropriété ou par une assurance spéciale ou complémentaire souscrite auprès d'une entreprise d'assurance.

*Pour aller plus loin* : article 6 de la loi du 2 janvier 1970 ; articles 64 à 71 du décret du 29 juillet 1972 précité.

#### Obligation d'affichage

Le professionnel titulaire de la carte professionnelle est tenu d'apposer une affiche dans l'ensemble des locaux où il exerce son activité mentionnant :

- le numéro de sa carte ;
- le montant de sa garantie souscrite ;
- le cas échéant, la dénomination et l'adresse du gérant.

En outre, tous les documents émis doivent comporter les informations  :

- relatives à sa carte professionnelle (numéro et lieu de délivrance) ;
- la nature de son activité ;
- le nom et la raison sociale de son entreprise ;
- le cas échéant, le nom et l'adresse du garant ;
- le cas échéant, la mention qu'il ne doit recevoir ni détenir de fonds autres que ceux prévus pour l'exercice de son activité.

*Pour aller plus loin* : articles 93 à 95 du décret du 20 juillet 1972.

## 3°. Démarches et formalités d’installation
 
### a. Demande en vue d'obtenir une carte professionnelle

#### Autorité compétente

Le professionnel doit adresser sa demande au président de la CCI territoriale ou de la chambre départementale d’Île-de-France dans le ressort de laquelle se trouve soit son siège si le demandeur est une personne morale, soit son principal établissement si le demandeur est une personne physique.

#### Pièces justificatives

Sa demande doit mentionner :

- la nature des opérations envisagées ;
- si le demandeur est une personne physique :
  - son état civil,
  - sa profession,
  - son domicile et son adresse professionnelle ;
- si le demandeur est une personne morale :
  - sa dénomination, sa forme juridique et l'objet de son activité,
  - son siège social,
  - les informations relatives à ses représentants légaux ou statutaires (état civil, profession, domicile).

En outre, sa demande doit être accompagnée des documents suivants :

- le formulaire Cerfa n° 15312*01 rempli et signé ;
- un justificatif d’aptitude professionnelle du demandeur ;
- une attestation de garantie financière ;
- une attestation d’assurance de responsabilité civile professionnelle ;
- d’un extrait du registre du commerce et des sociétés (RCS) datant de moins d’un mois si la personne est immatriculée, ou du double de la demande ;
- suivant le cas :
  - soit une attestation comportant le numéro de compte, délivrée par l’établissement de crédit qui a ouvert le compte,
  - soit une attestation d’ouverture au nom de chaque mandant des comptes bancaires ;
- le cas échéant, la déclaration sur l’honneur que le demandeur ne détient aucun autres fonds, directement ou indirectement que ceux représentant sa rémunération ;
- le bulletin n° 2 au casier judiciaire national ou un équivalent pour les ressortissants d’un autre État membre de l’UE.

#### Coût

La demande doit être accompagnée des frais de dossier d’instruction d’un montant de [120 euros]( https://www.entreprises.cci-paris-idf.fr/web/formalites/demande-carte-professionnelle-immobilier).

#### Délais

Si le dossier est incomplet, l’autorité compétente adresse au professionnel la liste des pièces manquantes dans un délai de quinze jours à compter de sa réception. Si son dossier n’est pas complété dans les deux mois suivants, sa demande de carte sera rejetée.

*Pour aller plus loin* : articles 2 et 3 du décret du 20 juillet 1972 précité.

### b. Déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa demande, par lettre recommandée avec avis de réception, ou par voie électronique, au président de la CCI territoriale ou de la chambre départementale d'Île-de-France dans le ressort de laquelle il souhaite effectuer sa prestation de services.

#### Pièces justificatives

Sa demande doit contenir :

- une attestation certifiant qu'il est légalement établi dans un État membre de l'UE ou de l'EEE et qu'il n'encourt aucune interdiction d'exercer l'activité d'administrateur de biens ;
- un document justifiant qu'il a exercé cette activité pendant au moins un an au cours des dix dernières années dès lors que l’État membre dans lequel il est établi ne réglemente pas cette activité ;
- un justificatif de sa nationalité ;
- un document attestant qu'il dispose de la garantie financière exigée pour exercer (cf. supra « 2°. d. Obligation de justifier d'une garantie financière ») ;
- un justificatif certifiant qu'il a souscrit une assurance de responsabilité civile professionnelle ;
- une déclaration sur l'honneur qu'il ne détient, ni n'a reçu directement ou indirectement d'autres fonds, effets ou valeurs que ceux compris dans sa rémunération ou ses honoraires.

*Pour aller plus loin* : article 16-6 du décret du 20 juillet 1972 précité.

### c. Formalités de déclaration de l’entreprise

Le professionnel est tenu de s'immatriculer au registre du commerce et des sociétés (RCS) auprès de la CCI en cas de création d'une société commerciale ou effectuer une déclaration d'activité auprès de l'Urssaf en cas de création d'une entreprise individuelle.

### d. Obligation de déclarer un établissement secondaire

En cas d'ouverture d'un établissement secondaire ou de succursale, le professionnel doit au préalable les déclarer.

#### Autorité compétente

Le professionnel doit adresser sa demande à la CCI du lieu où se situe l'établissement secondaire.

#### Pièces justificatives

Sa demande doit contenir le formulaire Cerfa n° 15312*01 identique à celui requis pour la délivrance de la carte professionnelle ainsi que les pièces justificatives requises pour cette procédure.

#### Délais et procédure

La CCI remet un récépissé de déclaration dès la réception de la demande.

*Pour aller plus loin* : article 3 de la loi du 2 janvier 1970 ; article 8 du décret du 20 juillet 1972.