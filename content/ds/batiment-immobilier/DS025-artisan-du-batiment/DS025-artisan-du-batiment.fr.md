﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS025" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Artisan du bâtiment" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="artisan-du-batiment" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/artisan-du-batiment.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="artisan-du-batiment" -->
<!-- var(translation)="None" -->

# Artisan du bâtiment

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Les artisans du bâtiment effectuent des travaux de revêtement des sols et des murs. Ils forment un groupe de métiers très divers possédant chacun des savoir-faire manuels et techniques spécifiques. Parmi les artisans du bâtiment, on peut citer : le carreleur, le menuisier-charpentier, le plâtrier-plaquiste, le couvreur, le peintre, etc. 

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- en cas de création d’une entreprise individuelle, le CFE compétent est l’Urssaf ;
- en cas d’activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- en cas création d’une société commerciale, le CFE compétent est la chambre de commerce et d’industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

L’intéressé souhaitant exercer l’activité d’artisan du bâtiment doit disposer d’une qualification professionnelle ou exercer sous le contrôle effectif et permanent d’une personne disposant d’une telle qualification.

Cette qualification professionnelle peut être :

- un certificat d’aptitude professionnelle (CAP) ;
- un brevet d’études professionnelles (BEP) ;
- un diplôme ou un titre égal ou supérieur homologué ou enregistré lors de sa délivrance au [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Le [site de la Commission nationale de la certification professionnelle](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (CNCP) propose une liste de l’ensemble de ces qualifications professionnelles.

À défaut de l’un de ces diplômes ou titres, l’intéressé doit justifier d’une expérience professionnelle de trois années effectives sur le territoire de l’Union européenne (UE) ou de l’Espace économique européen (EEE) acquise en qualité de dirigeant d’entreprise, de travailleur indépendant ou de salarié dans l’exercice du métier d’artisan du bâtiment. Dans ce cas, il est conseillé à l’intéressé de s’adresser à la chambre des métiers et de l’artisanat (CMA) pour demander une attestation de reconnaissance de qualification professionnelle.

*Pour aller plus loin* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l’artisanat ; décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l’exercice des activités prévues à l’article 16 de la loi n° 96-603 du 5 juillet 1996 précitée.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### Pour une Libre Prestation de Services (LPS)

Le professionnel ressortissant de l’UE ou de l’EEE peut exercer en France, à titre temporaire et occasionnel, le contrôle effectif et permanent de l’activité d’artisan du bâtiment à la condition d’être légalement établi dans un de ces États pour y exercer la même activité.

Si ni l’activité ni la formation y conduisant ne sont réglementées dans l’État d’établissement, l’intéressé doit en outre prouver qu’il a exercé l’activité d’artisan du bâtiment dans cet État pendant au moins l’équivalent de deux ans à temps complet au cours des dix dernières années précédant la prestation qu’il veut effectuer en France.

*Pour aller plus loin* : article 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée.

#### Pour un Libre Établissement (LE)

Pour exercer en France, à titre permanent, le contrôle effectif et permanent de l’activité d’artisan du bâtiment, le professionnel ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- être titulaire d’une attestation de compétence ou d’un titre de formation requis pour l’exercice de l’activité d’artisan du bâtiment dans un État de l’UE ou de l’EEE lorsqu’il réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’une attestation de compétence ou d’un titre de formation qui certifie sa préparation à l’exercice de l’activité d’artisan du bâtiment lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente pas cette activité ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE, autre que la France, à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité d’artisan du bâtiment dans l’État qui a admis l’équivalence.

**À noter**

Le ressortissant d’un État de l’UE ou de l’EEE qui remplit l’une des conditions précitées peut solliciter une attestation de reconnaissance de qualification professionnelle pour exercer le contrôle effectif et permanent de l’activité d’artisan du bâtiment (cf. infra « 3°. b. Le cas échéant, demander une attestation de qualification professionnelle »).

Si l’intéressé ne remplit aucune des conditions précitées, la CMA saisie peut lui demander d’accomplir une mesure de compensation dans les cas suivants :

- si la durée de la formation attestée est inférieure d’au moins un an à celle exigée pour obtenir l’une des qualifications professionnelles requises en France pour exercer l’activité d’artisan du bâtiment ;
- si la formation reçue porte sur des matières substantiellement différentes de celles couvertes par l’un des titres ou diplômes requis pour exercer en France l’activité d’artisan du bâtiment ;
- si le contrôle effectif et permanent de l’activité d’artisan du bâtiment nécessite, pour l’exercice de certaines de ses attributions, une formation spécifique qui n’est pas prévue dans l’État membre d’origine et porte sur des matières substantiellement différentes de celles couvertes par l'attestation de compétence ou le titre de formation dont le demandeur fait état.

*Pour aller plus loin* : articles 17 et 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée ; articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 précité.

**Bon à savoir : mesures de compensation**

La CMA, saisie d’une demande de délivrance d’une attestation de reconnaissance de qualification professionnelle, notifie au demandeur sa décision tendant à lui faire accomplir l’une des mesures de compensation. Cette décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est impérative pour exercer en France.

Le demandeur doit alors choisir entre un stage d’adaptation d’une durée maximale de trois ans et une épreuve d’aptitude.

L’épreuve d’aptitude prend la forme d’un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA de la décision du demandeur d’opter pour cette épreuve. À défaut, la qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

À la fin du stage d’adaptation, le demandeur adresse à la CMA une attestation certifiant qu’il a valablement accompli ce stage, accompagnée d’une évaluation de l’organisme qui l’a encadré. La CMA délivre, sur la base de cette attestation, une attestation de qualification professionnelle dans un délai d’un mois.

La décision de recourir à une mesure de compensation peut être contestée par l’intéressé qui doit former un recours administratif auprès du préfet dans un délai de deux mois à compter de la notification de la décision. En cas de rejet de son recours, il peut alors initier un recours contentieux.

*Pour aller plus loin* : articles 3 et 3-2 du décret n° 98-246 du 2 avril 1998 précité ; article 6-1 du décret n° 83-517 du 24 juin 1983 fixant les conditions d’application de la loi 82-1091 du 23 décembre 1982 relative à la formation professionnelle des artisans.

### c. Conditions d’honorabilité, incompatibilités

Nul ne peut exercer la profession s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévue au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 III de la loi n° 96-603 du 5 juillet 1996 précitée.

### d. Assurance de responsabilité civile

Le professionnel doit souscrire une assurance responsabilité civile professionnelle. Elle lui permet d’être couvert pour les dommages causés à autrui qu’il en soit directement à l’origine ou qu’ils soient le fait de ses salariés, de ses locaux ou de son matériel.

De plus, les artisans du bâtiment intervenant dans les travaux de gros œuvre et de construction sont soumis à l’obligation de souscrire une assistance de responsabilité civile décennale.

Les références du contrat d’assurance doivent apparaître sur les devis et factures de l’artisan concerné.

*Pour aller plus loin* : article L. 241-1 du Code des assurances et article 22-2 de la loi n° 96-603 du 5 juillet 1996 précitée.

### e. Quelques particularités de la réglementation de l’activité

#### Réglementation concernant la qualité d’artisan et les titres de maître artisan ou de meilleur ouvrier de France

**La qualité d’artisan**

Seules les personnes justifiant d’une qualification professionnelle déterminée peuvent se prévaloir de la qualité d’artisan (cf. supra « 2°. a. Qualification professionnelle »).

*Pour aller plus loin* : article 1 du décret n° 98-247 du 2 avril 1998.

**Le titre de maître artisan**

Pour obtenir le titre de maître artisan, l’intéressé (personne physique ou dirigeant d’une société artisanale) doit :

- être immatriculé au répertoire des métiers ;
- être titulaire du brevet de maîtrise ;
- justifier d’au moins deux ans de pratique professionnelle.

La demande doit être adressée au président de la CMA compétente.

**À savoir**

Les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan dans deux situations distinctes :

- si elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de formation équivalente au brevet de maîtrise, qu’elles disposent de connaissances en gestion et psychopédagogie équivalentes aux unités de valeur du brevet de maîtrise et qu’elles justifient de plus de deux ans de pratique professionnelle ;
- si elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles disposent d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

Dans ces deux cas, le titre de maître artisan peut être accordé par la commission régionale des qualifications.

Pour plus de précisions, il est conseillé de se rapprocher de la CMA considérée.

*Pour aller plus loin* : décret n° 98-247 du 2 avril 1998 précité.

**Le titre de meilleur ouvrier de France**

Le titre de meilleur ouvrier de France (MOF) est réservé aux personnes ayant réussi l’examen « concours un des meilleurs ouvriers de France ». Il s’agit d’un diplôme d’État qui atteste de l’acquisition d’une haute qualification dans l’exercice d’une activité professionnelle.

*Pour aller plus loin* : [site officiel du concours « un des meilleurs ouvriers de France »](http://www.meilleursouvriersdefrance.org/) ; articles D. 338-9 et suivants du Code de l'éducation et arrêté du 27 décembre 2012.

#### Réglementation concernant l’établissement

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

#### Information du consommateur

Préalablement à tous travaux, l’artisan du bâtiment informer le consommateur des mentions suivantes :

- les taux horaires de main-d’œuvre TTC ;
- les modalités de décompte du temps passé ;
- les prix TTC des différentes prestations forfaitaires proposées ;
- les frais de déplacement, le cas échéant ;
- le caractère gratuit ou payant du devis et son coût le cas échéant ;
- toute autre condition de rémunération.

Ces informations doivent être affichées de façon visible et lisible dans les locaux de l’entreprise si elle reçoit la clientèle. Si la prestation est offerte sur le lieu de l’intervention, ces informations doivent être présentées au client préalablement à tout travail dans un document écrit.

*Pour aller plus loin* : article 2 de l’arrêté du 2 mars 1990 relatif à la publicité des prix des prestations de dépannage, de réparation et d’entretien dans le secteur du bâtiment et de l’équipement de la maison.

#### Obligation d’établir une note

Le professionnel doit adresser au consommateur une note une fois la prestation effectuée et avant le paiement du prix. De plus, il fait signer au consommateur une décharge pour les pièces, éléments ou appareils remplacés dont ce dernier a refusé la conservation.

*Pour aller plus loin* : article 5 de l’arrêté du 2 mars 1990 précité et arrêté n° 83-50/A du 3 octobre 1983 relatif à la publicité des prix de tous les services.

#### Conduite d’engins automoteurs et d’appareils de levage

Les salariés de l’entreprise ne peuvent conduire des engins automoteurs et appareils de levage (grues, chariots autoporteurs, plateformes élévatrices, etc.) qu’à la condition d’avoir reçu une autorisation de conduite délivrée par l’employeur.

Pour délivrer cette autorisation, l’employeur procède à une évaluation du travailleur prenant en compte :

- un examen d’aptitude à la conduite réalisé par le médecin du travail ;
- un contrôle des connaissances et du savoir-faire de l’opérateur pour la conduite en sécurité de l’équipement de travail. L’obtention du certificat d’aptitude à la conduite en sécurité (CACES) permet d’attester du contrôle des connaissances et du savoir-faire du salarié. Ce certificat est délivré par des organismes testeurs certifiés ;
- un contrôle des connaissances des lieux et des instructions à respecter sur le ou les sites d’utilisation.

*Pour aller plus loin* : article R. 4323-56 du Code du travail et arrêté du 2 décembre 1998 relatif à la formation à la conduite des équipements de travail mobiles autoporteurs et des appareils de levage de charges ou de personnes.

#### Habilitation à effectuer des opérations sur installation électrique

Le professionnel doit être habilité à intervenir sur des installations électriques ou dans leur voisinage, qu’il effectue des opérations d’ordre électrique ou non. L’habilitation doit respecter les dispositions prévues dans la norme NF C 18-510.

**Pour le salarié**, c’est l’employeur qui délivre au salarié l’habilitation à effectuer certaines opérations sur installation électrique à la double condition suivante :

- le salarié doit avoir reçu une formation théorique et pratique sur les risques liés à l’électricité et les mesures à prendre pour intervenir en sécurité ;
- le salarié doit avoir été déclaré apte par le médecin du travail.

Si les conditions sont réunies, l’employeur délivre, maintient ou renouvelle l’habilitation et remet à chaque employé un carnet de prescriptions délimitant les opérations qu’il est habilité à mener et précisant les instructions de sécurité particulières au travail effectué, le cas échéant.

**Pour l’employeur ou le travailleur indépendant**, s’il exerce une activité sur les installations électriques ou dans leur voisinage, il doit disposer d’un niveau de connaissance des risques liés à l’électricité et des mesures à prendre pour intervenir en sécurité équivalent à celui des salariés auxquels sont confiées ces activités. Les travailleurs indépendants et employeurs ne peuvent pas s’auto-habiliter mais doivent respecter les prescriptions de sécurité de la norme NF C 18-510.

**Bon à savoir**

Lorsqu’une entreprise étrangère intervient en France, elle doit se conformer à la réglementation nationale. Elle est donc tenue d’habiliter ses travailleurs conformément aux dispositions du Code du travail, selon la norme NF C 18-510.

*Pour aller plus loin* : articles R. 4544-3, R. 4544-9 à R. 4544-12 du Code du travail et la [brochure de l’Institut national de recherche et de sécurité pour la prévention des accidents du travail et des maladies professionnelles (INRS) relative à l’habilitation électrique](http://www.inrs.fr/media.html?refINRS=ED%206127).

## 3°. Démarches et formalités d’installation

### a. Le cas échéant, demander une attestation de reconnaissance de qualification professionnelle

L’intéressé souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de qualification professionnelle.

**Autorité compétente**

La demande doit être adressée à la CMA territorialement compétente.

**Procédure**

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les 15 jours du dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

**Pièces justificatives**

Le dossier de demande d’attestation de reconnaissance de qualification professionnelle doit contenir :

- la demande d’attestation de reconnaissance de qualification professionnelle ;
- le justificatif de la qualification professionnelle : une attestation de compétence ou le diplôme ou titre de formation professionnelle ;
- la preuve de la nationalité du demandeur ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE, une attestation portant sur la nature et de la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France, les justificatifs de l’exercice de l’activité pendant trois années.

La CMA peut demander la communication d’informations complémentaires concernant la formation ou l’expérience professionnelle du demandeur pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du Centre international d’études pédagogiques (Ciep) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme ou d’un certificat ou titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français par un traducteur agréé.

**Délai de réponse**

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut, au choix :

- reconnaître la qualification professionnelle et délivrer l’attestation de qualification professionnelle ;
- décider de soumettre le demandeur à une mesure de compensation et lui notifier cette décision ;
- refuser de délivrer l’attestation de qualification professionnelle.

En l’absence de décision dans le délai de quatre mois, la demande d’attestation de reconnaissance de qualification professionnelle est réputée acquise.

**Voies de recours**

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier un recours contentieux devant le tribunal administratif dans les deux mois suivant la notification du refus de la CMA. De même, si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours gracieux auprès du préfet du département où la CMA a son siège dans les deux mois suivant la notification de la décision. S’il n’obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 précité ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997et n° 98-246 du 2 avril 1998 relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.

### b. Le cas échéant, effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité ponctuelle (Libre Prestation de Services)

Les professionnels qui œuvrent pour la construction, l'entretien et la réparation des bâtiments ne sont pas tenus de déclarer préalablement leur activité ponctuelle et temporaire.

En revanche, certains professionnels sont concernés par cette déclaration. Il s’agit en effet de tous les professionnels intervenant dans la mise en place, l'entretien et la réparation des réseaux et des équipements utilisant les fluides, ainsi que des matériels et équipements destinés à l'alimentation en gaz, au chauffage des immeubles et aux installations électriques.

Pour ces professionnels, la procédure est la suivante : les ressortissants de l’UE ou de l’EEE souhaitant exercer en France de manière occasionnelle et ponctuelle sont soumis à une déclaration préalable à sa première prestation sur le sol français. Cette déclaration préalable d’activité doit être renouvelée tous les ans si l’intéressé souhaite exercer de nouveau en France.

**Autorité compétente**

La déclaration préalable d’activité est adressée à la CMA dans le ressort de laquelle le déclarant envisage de réaliser sa prestation.

**Récépissé**

La CMA délivre au déclarant un récépissé mentionnant la date à laquelle elle a reçu le dossier complet de déclaration préalable d’activité. Si le dossier est incomplet, la CMA notifie dans les 15 jours la liste des pièces manquantes au déclarant. Elle délivre le récépissé dès que le dossier est complet.

**Pièces justificatives**

La déclaration préalable d’activité doit comporter :

- la déclaration remplie, datée et signée, mentionnant les informations relatives aux assurances civiles professionnelles obligatoires ;
- la preuve de la nationalité du déclarant ;
- la preuve des qualifications professionnelles : titre de formation, attestation de compétence délivrée par l’autorité compétente dans l’État d’établissement, tout document attestant d’une expérience professionnelle indiquant sa nature et sa durée, etc. ;
- le cas échéant, la preuve que le déclarant a exercé pendant au moins l’équivalent de deux ans à temps complet son activité au cours des dix dernières années ;
- une attestation d’établissement dans un État de l’UE ou de l’EEE ;
- une attestation de non-condamnation à une interdiction, même temporaire, d’exercer délivrée par l’autorité compétente de l’État d’établissement ;

Tous les documents doivent être traduits en français (par un traducteur certifié) s’ils ne sont pas établis en français.

**Délai**

Dans un délai d’un mois à compter de la réception du dossier complet de déclaration préalable d’activité, la CMA délivre au déclarant une attestation de reconnaissance de qualification professionnelle ou lui notifie la nécessité de procéder à un examen complémentaire. Dans ce dernier cas, la CMA notifie sa décision finale dans les deux mois à compter de la réception du dossier complet de déclaration préalable d’activité. À défaut de notification dans ce délai, la déclaration préalable est réputée acquise et la prestation de services peut donc débuter.

Pour appuyer sa décision, la CMA peut s’adresser à l’autorité compétente de l’État d’établissement du déclarant pour obtenir toute information concernant la légalité de l’établissement et son absence de sanction disciplinaire ou pénale à caractère professionnel.

**À savoir**

Si la CMA relève une différence substantielle entre la qualification professionnelle requise pour exercer en France et celle déclarée par le prestataire et que cette différence est de nature à nuire à la santé ou à la sécurité du bénéficiaire du service, le déclarant est invité à se soumettre à une épreuve d’aptitude. S’il s’y refuse, la prestation de services ne peut être réalisée. L’épreuve d’aptitude doit être organisée dans un délai de trois mois à compter du dépôt du dossier complet de déclaration préalable d’activité. Si ce délai n’est pas respecté, la reconnaissance de qualification professionnelle est réputée acquise et la prestation de services peut débuter (cf. supra « Bon à savoir : les mesures de compensation »).

**Recours**

Toute décision de la CMA de soumettre le déclarant à une épreuve d’aptitude peut faire l’objet d’un recours administratif auprès du préfet du département dans lequel la CMA a son siège, dans les trois mois suivant la notification de la décision de la CMA. Si le recours est infructueux, le déclarant peut alors initier un recours contentieux auprès du tribunal administratif compétent.

**Coût**

La déclaration préalable est gratuite. Cependant, si le déclarant participe à une épreuve d’aptitude, il pourra devoir participer aux frais d’organisation de cette mesure.

*Pour aller plus loin* : article 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée ; articles 3 et suivants du décret n° 98-246 du 2 avril 1998 précité ; articles 1er et 6 de l’arrêté du 28 octobre 2009 précité.