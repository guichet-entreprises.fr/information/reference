﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS025" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Construction tradesman" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="construction-tradesman" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/construction-tradesman.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="construction-tradesman" -->
<!-- var(translation)="Auto" -->


Construction tradesman
==================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The craftsmen of the building carry out the work of covering the floors and walls. They form a very diverse group of trades, each with specific manual and technical know-how. Among the craftsmen of the building are: the tiler, the carpenter-carpenter, the plasterer-plaquist, the roofer, the painter, etc.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- In the case of the creation of an individual company, the competent CFE is the Urssaf;
- In case of artisanal activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- In the event of the creation of a commercial company, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The person concerned wishing to work as a building craftsman must have a professional qualification or exercise under the effective and permanent control of a person with such a qualification.

This professional qualification can be:

- A Certificate of Professional Qualification (CAP)
- A Professional Studies Patent (BEP);
- an equal or higher degree or designation approved or registered at the time of its issuance at the [national directory of professional certifications](http://www.rncp.cncp.gouv.fr/).

The [National Professional Certification Commission website](http://www.rncp.cncp.gouv.fr/) (CNCP) offers a list of all of these professional qualifications.

In the absence of one of these diplomas or titles, the person concerned must justify an effective three years of professional experience in the territory of the European Union (EU) or the European Economic Area (EEA) acquired as a business leader, self-employed or salaried in the trade of building worker. In this case, the person concerned is advised to contact the Chamber of Trades and Crafts (CMA) to request a certificate of recognition of professional qualification.

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Decree 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act 96-603 of 5 July 1996.

### b. Professional Qualifications - European Nationals (LPS or LE)

#### For Freedom to provide services

The professional national of the EU or the EEA may exercise in France, on a temporary and occasional basis, the effective and permanent control of the activity of the building craftsman provided that he is legally established in one of these states to carry out the same Activity.

If neither the activity nor the training leading to it is regulated in the State of establishment, the person concerned must also prove that he has been a construction worker in that state for at least the equivalent of two full-time years in the ten years before the performance he wants to perform in France.

*For further information*: Article 17-1 of Act 96-603 of July 5, 1996.

#### For a Freedom of establishment

In order to carry out on a permanent basis in France the effective and permanent control of the activity of building craftsmen, the professional national of the EU or the EEA must fulfil one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above "2 degrees). a. Professional qualifications");
- hold a certificate of competency or training degree required for the practice of buildingwork in an EU or EEA state when regulating access or exercise of this activity on its territory;
- have a certificate of competency or a training certificate that certifies its preparation for the activity of buildingwork when this certificate or title has been obtained in an EU or EEA state that does not regulate this activity;
- be the holder of a diploma, title or certificate acquired in a third state and admitted in equivalence by an EU or EEA state, other than France, on the additional condition that the person concerned has worked for three years as a buildingman in state that has admitted equivalence.

**Please note**

A national of an EU or EEA state that fulfils one of the above conditions may apply for a certificate of recognition of professional qualification to exercise effective and permanent control of the activity of buildingman (cf. infra "3 degrees. b. If necessary, apply for a certificate of professional qualification").

If the individual does not meet any of the above conditions, the CMA may ask him to perform a compensation measure in the following cases:

- if the duration of the certified training is at least one year less than that required to obtain one of the professional qualifications required in France to carry out the activity of building craftsman;
- If the training received covers subjects substantially different from those covered by one of the titles or diplomas required to carry out the activity of building craftsmen in France;
- If the effective and permanent control of the building craftsman's activity requires, for the exercise of some of its remits, specific training which is not provided in the Member State of origin and deals with materials substantially different from those covered by the certificate of competency or training designation that the applicant refers to.

*For further information*: Articles 17 and 17-1 of Law 96-603 of 5 July 1996; Articles 3 to 3-2 of Decree 98-246 of 2 April 1998.

**Good to know: compensation measures**

The CMA, which is applying for a certificate of recognition of professional qualification, notifies the applicant of his decision to have him perform one of the compensation measures. This decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose between an adjustment course of up to three years and an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organised within six months of the CMA's receipt of the applicant's decision to opt for the event. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the adjustment course, the applicant sends the CMA a certificate certifying that he has validly completed this internship, accompanied by an evaluation of the organization that supervised him. The CMA issues, on the basis of this certificate, a certificate of professional qualification within one month.

The decision to use a compensation measure may be challenged by the person concerned who must file an administrative appeal with the prefect within two months of notification of the decision. If his appeal is dismissed, he can then initiate a legal challenge.

*For further information*: Articles 3 and 3-2 of Decree 98-246 of 2 April 1998; Article 6-1 of Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

### c. Conditions of honorability, incompatibility

No one may practise the profession if he is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 III of Act 96-603 of July 5, 1996.

### d. Liability insurance

The professional must take out professional liability insurance. It allows it to be covered for damage caused to others, whether directly caused or by its employees, premises or equipment.

In addition, construction workers involved in large-scale construction and construction work are required to take out 10-year civil liability assistance.

References to the insurance contract must appear on the quotes and invoices of the craftsman concerned.

*For further information*: Article L. 241-1 of the Insurance Code and Section 22-2 of Act 96-603 of July 5, 1996.

### e. Some peculiarities of the regulation of the activity

#### Regulation concerning the quality of craftsman and the titles of master craftsman or best worker in France

**Craftsmanship**

Only those who have a specific professional qualification can claim the status of craftsman (see supra "2." a. Professional qualification").

*For further information*: Article 1 of Decree 98-247 of 2 April 1998.

**The title of master craftsman**

To obtain the title of master craftsman, the person concerned (a natural person or a manager of a craft company) must:

- Be registered in the trades directory
- Hold a master's degree
- justify at least two years of professional practice.

The request must be addressed to the president of the relevant CMA.

**What to know**

Individuals who do not hold the master's degree can apply for the title of master craftsman in two distinct situations:

- if they are registered in the trades directory, have a training degree equivalent to the master's degree, and have management and psycho-pedagogical knowledge equivalent to the units of value of the master's degree and that they warrant more than two years of professional practice;
- if they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

In both cases, the title of master craftsman may be granted by the Regional Qualifications Commission.

For more details, it is advisable to get closer to the CMA under consideration.

*For further information*: Decree 98-247 of 2 April 1998.

**The title of best worker in France**

The title of best worker in France (MOF) is reserved for those who have passed the exam "one of the best workers in France". It is a state diploma that attests to the acquisition of a high qualification in the course of a professional activity.

*For further information*: [official website of the competition "one of the best workers in France"](http://www.meilleursouvriersdefrance.org/) ; Articles D. 338-9 and the following of the Education Code and arrested on December 27, 2012.

#### Settlement regulations

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, it is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

#### Consumer information

Before any work is done, the building's craftsman will inform the consumer of the following mentions:

- TTC hourly labour rates;
- How to count the time spent
- TTC prices for the various flat-rate services offered;
- Travel expenses, if any;
- The free or paid nature of the quote and its cost if applicable;
- any other compensation condition.

This information must be displayed in a visible and readable way on the company's premises if it receives customers. If the service is offered at the venue of the intervention, this information must be presented to the client prior to any work in a written document.

*For further information*: Article 2 of the March 2, 1990 Order on the Advertising of The Prices of Troubleshooting, Repair and Maintenance Services in the Building and Home Equipment Sector.

#### Obligation to make a note

The professional must send the consumer a note after the service has been completed and before the payment of the price. In addition, it has the consumer sign a discharge for the replaced parts, items or appliances that the consumer has refused to keep.

*For further information*: Article 5 of the order of 2 March 1990, and Order 83-50/A of 3 October 1983 relating to the price advertising of all services.

#### Driving self-propelled and lifting devices

The company's employees may only drive self-propelled vehicles and lifting equipment (grues, free-standing trolleys, forklifts, etc.) provided they have received a driving authorization issued by the employer.

To issue this authorization, the employer conducts an assessment of the worker taking into account:

- A driving fitness test performed by the occupational physician
- a control of the operator's knowledge and know-how for the safe operation of work equipment. Obtaining the Certificate of Fitness to Drive safely (CACES) allows to attest to the control of the knowledge and know-how of the employee. This certificate is issued by certified testing bodies;
- a control of the knowledge of the place and the instructions to be followed on the site or sites of use.

*For further information*: Article R. 4323-56 of the Labour Code and order of 2 December 1998 relating to the training in the conduct of mobile self-driving equipment and load or lifting devices.

#### The ability to perform operations on an electrical installation

The professional must be empowered to intervene on electrical installations or in their vicinity, whether he performs electrical operations or not. The clearance must comply with the provisions of NF C 18-510.

**For the employee**, it is the employer who gives the employee the authorization to perform certain operations on electrical installation on the following two conditions:

- The employee must have received theoretical and practical training on electricity risks and measures to intervene safely;
- the employee must have been declared fit by the occupational doctor.

If the conditions are met, the employer issues, maintains or renews the authorization and gives each employee a prescription book delimiting the transactions he is entitled to carry out and specifying the specific safety instructions to the work done, if any.

**For the employer or the self-employed**, if it carries out an activity on electrical installations or in their vicinity, it must have a level of knowledge of the risks associated with electricity and the measures to be taken to intervene safely equivalent to that of the employees to whom are entrusted with these activities. Self-employed workers and employers cannot self-empower but must comply with the safety requirements of NF C 18-510.

**Good to know**

When a foreign company intervenes in France, it must comply with national regulations. It is therefore required to empower its workers in accordance with the provisions of the Labour Code, according to the NF C 18-510 standard.

*For further information*: Articles R. 4544-3, R. 4544-9 to R. 4544-12 of the Labour Code and [National Institute for Research and Safety for the Prevention of Workplace Accidents and Occupational Diseases (INRS) brochure on electrical clearance](http://www.inrs.fr/media.html?refINRS=ED%206127).

3°. Installation procedures and formalities
------------------------------------------------------

### a. Follow the installation preparation course (SPI)

The SPI is a mandatory requirement for anyone applying for registration in the trades directory.

**Terms of the internship**

- Registration is done upon presentation of a piece of identification with the territorially competent CMA.
- It has a minimum duration of 30 hours.
- It comes in the form of courses and practical work.
- Its objective is to acquire the basic knowledge essential in the legal, tax, social and accounting fields essential to the creation of a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

At the end of the internship, the participant receives a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training costs about 236 euros in 2016.

**Cases of exemption from the internship**

The person concerned may be excused from performing the SPI in two situations:

- if he has already received a level III-certified degree or diploma, including an education in economics and business management, or if he has obtained a master's degree from a CMA;
- if he has been engaged in a professional activity requiring a level of knowledge equivalent to that provided by the SPI for at least three years.

**Internship exemption for EU or EEA nationals**

A qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the SPI.

The qualification in business management is recognized as equivalent to that provided by the SPI for people who:

- have either engaged in a professional activity requiring a level of knowledge equivalent to that provided by the SPI for at least three years;
- either who have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, fully or partially, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France for the management of a craft company.

**Terms of the SPI exemption**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship. He must accompany his mail with supporting documents (copy of the diploma approved at level III, copy of the master's certificate, proof of a professional activity requiring an equivalent level of knowledge) and pay a fee (25 euros in 2016, as an indication). Failure to respond within one month of receiving the application is worth accepting the application for an SPI waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree 83-517 of 24 June 1983.

### b. If necessary, apply for a certificate of recognition of professional qualification

The person concerned wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of professional qualification.

**Competent authority**

The request must be addressed to the territorially competent CMA.

**Procedure**

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the applicant to complete it within 15 days of filing the file. A receipt is issued as soon as the file is complete.

**Supporting documents**

The application for certification of professional qualification must contain:

- Applying for a certificate of recognition of professional qualification;
- proof of professional qualification: a certificate of competency or a diploma or vocational training certificate;
- Proof of the applicant's nationality
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, the proofs of the exercise of the activity for three years.

The CMA may request further information regarding the applicant's training or work experience to determine the possible existence of substantial differences with the required professional qualification France. In addition, if the CMA is to approach the International Centre for Educational Studies (Ciep) to obtain additional information on the level of training of a foreign diploma or certificate or certificate or designation, the applicant will have to pay a fee Additional.

**What to know**

If necessary, all supporting documents must be translated into French by a certified translator.

**Response time**

Within three months of the receipt, the CMA may, at its choice:

- Recognise professional qualification and issue certification of professional qualification;
- decide to subject the applicant to a compensation measure and notify him of that decision;
- refuse to issue the certificate of professional qualification.

In the absence of a decision within four months, the application for certification of professional qualification is deemed to have been acquired.

**Remedies**

If the CMA refuses to issue the recognition of professional qualification, the applicant can initiate a legal action before the administrative court within two months of notification of the refusal of the CMA. Similarly, if the person concerned wishes to challenge the CMA's decision to submit it to a compensation measure, he must first initiate a graceful appeal with the prefect of the department where the CMA is based within two months of notification of the decision. If he does not succeed, he may opt for a litigation before the relevant administrative tribunal.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the European Community or another state party to the European Economic Area agreement.

### c. Company reporting formalities

In the process of being drafted.

### b. Post-registration authorisation

### c. If necessary, make a prior declaration of activity for EU nationals engaged in a one-off activity (Freedom to provide services)

Professionals working in the construction, maintenance and repair of buildings are not required to declare their occasional and temporary activity in advance.

On the other hand, some professionals are concerned by this statement. They are all professionals involved in the installation, maintenance and repair of fluid-using networks and equipment, as well as equipment and equipment for gas supply, building heating and heating. electrical installations.

For these professionals, the procedure is as follows: EU or EEA nationals wishing to practice in France on an occasional and ad hoc basis are subject to a declaration prior to their first performance on French soil. This prior declaration of activity must be renewed every year if the person wishes to practice again in France.

**Competent authority**

The prior declaration of activity is addressed to the CMA in the jurisdiction from which the registrant plans to carry out his performance.

**Receipt**

The CMA issues a receipt to the registrant stating the date on which it received the full pre-report of activity file. If the file is incomplete, the CMA notifies the list of missing documents to the registrant within 15 days. It issues the receipt as soon as the file is complete.

**Supporting documents**

The prior declaration of activity must include:

- The completed, dated and signed declaration mentioning information on compulsory professional civil insurance;
- Proof of the nationality of the registrant;
- proof of professional qualifications: training certificate, certificate of competence issued by the competent authority in the State of establishment, any document attesting to professional experience indicating its nature and duration, etc.;
- If applicable, evidence that the registrant has been working for at least two years in full-time for the past ten years;
- A certificate of establishment in an EU or EEA state;
- a certificate of non-condemnation to a ban, even temporary, from practising issued by the competent authority of the State of Establishment;

All documents must be translated into French (by a certified translator) if they are not drawn up in French.

**Timeframe**

Within one month of receiving the full pre-report of activity file, the CMA issues a certificate of recognition of professional qualification to the declarant or advises the declarate of the need to carry out a review Complementary. In the latter case, the CMA notifies its final decision within two months of receipt of the full pre-report of activity file. In the absence of notification within this time frame, the pre-declaration is deemed to have been acquired and the service delivery may therefore begin.

In support of its decision, the CMA may contact the competent authority of the registrant's state of settlement for any information regarding the legality of the institution and its absence of disciplinary or criminal sanction of a nature. Professional.

**What to know**

If the CMA finds a substantial difference between the professional qualification required to practise in France and that declared by the claimant and this difference is likely to adversely affect the health or safety of the beneficiary of the service, the registrant is invited to submit to an aptitude test. If it refuses to do so, the provision of services cannot be carried out. The aptitude test must be held within three months of filing the full pre-report activity file. If this deadline is not met, recognition of professional qualification is deemed to have been acquired and service delivery may begin (see above :"Good to know: compensation measures").

**Use**

Any decision by the CMA to subject the registrant to an aptitude test may be the subject of an administrative appeal to the prefect of the department in which the CMA is based, within three months of notification of the CMA's decision. If the appeal is unsuccessful, the registrant can then initiate a legal challenge with the relevant administrative court.

**Cost**

Pre-reporting is free of charge. However, if the registrant participates in an aptitude test, he may have to contribute to the costs of organizing this measure.

*For further information*: Article 17-1 of Law 96-603 of 5 July 1996; Articles 3 and following of Decree 98-246 of 2 April 1998 above; Articles 1 and 6 of the order of 28 October 2009 mentioned above.

