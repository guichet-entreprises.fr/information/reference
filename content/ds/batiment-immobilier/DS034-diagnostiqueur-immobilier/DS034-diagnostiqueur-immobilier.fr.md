﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS034" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Diagnostiqueur immobilier" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="diagnostiqueur-immobilier" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/diagnostiqueur-immobilier.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="diagnostiqueur-immobilier" -->
<!-- var(translation)="None" -->

# Diagnostiqueur immobilier

Dernière mise à jour : <!-- begin-var(last-update) -->2020-12<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Des diagnostics immobiliers sont obligatoires dans le cadre d’une transaction immobilière ou d’une location d’un bien. Six domaines sont concernés :

- 1° le constat de risque d'exposition au plomb (diagnostic plomb) ;
- 2° l'état mentionnant la présence ou l'absence de matériaux ou produits contenant de l'amiante (diagnostic amiante) ;
- 3° l'état relatif à la présence de termites dans le bâtiment (diagnostic termite). Ce diagnostic est uniquement obligatoire pour les transactions ;
- 4° l'état de l'installation intérieure de gaz (diagnostic gaz) ;
- 5° le diagnostic de performance énergétique (DPE) :
- 6° l'état de l'installation intérieure d'électricité (diagnostic électricité).

Le diagnostiqueur immobilier est la personne physique qui établit l’un de ces documents. Les compétences de cette personne doivent être certifiées par un organisme accrédité. Cette certification des compétences, d’une durée de sept ans, est délivrée par un organisme accrédité par un organisme signataire de l'accord européen multilatéral pris dans le cadre de la coordination européenne des organismes d'accréditation.

Chaque domaine de diagnostic est concerné par au moins une certification. Pour trois de ces domaines de diagnostics : diagnostic plomb, DPE et diagnostic amiante, deux certifications sont concernées : certifications « avec mention » et « sans mention ». Cette distinction se justifie par une intervention des diagnostiqueurs plus de compétences dans l’analyse technique de certains biens plus complexes. 

*Pour aller plus loin :* articles L. 271-4 et R. 271-3 du Code de la construction et de l'habitation et arrêté du 2 juillet 2018 définissant les critères de certification des opérateurs de diagnostic technique et des organismes de formation et d'accréditation des organismes de certification.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée.

Pour les activités artisanales, le CFE compétent est la chambre de métiers et de l'artisanat (CMA).

Pour les activités commerciales, il s'agit de la chambre de commerce et d'industrie (CCI).

Pour l'exercice d'une activité libérale, le CFE compétent est l'Urssaf.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le diagnostiqueur immobilier qui établit un des six diagnostics immobiliers obligatoires doit être en possession d’une certification de compétence en cours de validité pour exercer son activité. 

Pour la certification sans mention du diagnostic amiante, la certification sans mention du diagnostic plomb, le diagnostic électricité, le diagnostic gaz et le diagnostic termite, aucun prérequis relatif à la détention de qualifications professionnelles des candidats à une de ces certifications n'est exigé. 

Pour la certification avec mention du diagnostic amiante, la certification avec mention du diagnostic plomb, et les certifications avec et sans mentions du DPE, aucun prérequis relatif uniquement à la détention d’un diplôme spécifique des candidats à une de ces certifications n'est exigé. Néanmoins, l'intéressé devra fournir : 

- soit la preuve par tous moyens d'une expérience professionnelle de trois ans en tant que technicien ou agent de maîtrise du bâtiment ou dans des fonctions d'un niveau professionnel équivalent dans le domaine des techniques du bâtiment ;
- soit un diplôme sanctionnant une formation du niveau de l'enseignement post-secondaire d'une durée minimale de deux ans à temps plein ou d'une durée équivalente à temps partiel dans le domaine des techniques du bâtiment, dispensée dans une université ou un établissement d'enseignement supérieur ou dans un autre établissement de niveau équivalent, ou un titre professionnel équivalent ; 
- soit la preuve par tous moyens des compétences exigées par un État de l'Union européenne (UE) ou d'un autre État partie à l'accord sur l'Espace économique européen (EEE) pour une activité de diagnostic comparable, ces preuves ayant été obtenues dans un de ces États ; 
- soit toute preuve de la détention de connaissances équivalentes en lien avec les techniques du bâtiment.

#### Formation

La personne candidate à une certification, pour une première demande de certificat, apporte la preuve qu'elle a suivi le module de formation initiale, d'une durée de cinq jours pour un certificat avec mention et de trois jours pour tous les autres certificats demandés. 

La personne certifiée apporte la preuve, entre le début de la deuxième année et la fin de la troisième année, et au cours de la dernière année du cycle de certification, qu’elle a suivi les modules de formation continue, d’une durée de deux jours pour les certificats avec mention et d’un jour pour les autres certificats demandés.  

#### Examen pour la première obtention d’une certification ou son renouvellement

L'organisme de certification vérifie que le candidat dispose des compétences requises au travers : 

- d'un examen théorique et d'un examen pratique (mise en situation de diagnostic) pour une première demande d’un certificat ;
- d’un examen pratique (mise en situation de diagnostic) et d’un examen documentaire pour un renouvellement d’un certificat. L’examen documentaire consiste à contrôler la conformité aux dispositions réglementaires, normatives ou bonnes pratiques professionnelles en vigueur d'un échantillon d'au moins cinq rapports établis par la personne certifiée depuis le début du cycle de certification. Cet échantillon est sélectionné par l'organisme de certification.

*Pour aller plus loin :* définition du programme des examens théorique et pratique à l’annexe 3 « compétence des personnes physiques exigées dans le cadre de la certification » de l’arrêté du 2 juillet 2018.

#### Surveillance pendant le cycle de certification 

Le professionnel fait l’objet d’une surveillance pour chaque certificat par l’organisme de certification. Elle permet de contrôler la conformité aux dispositions réglementaires, normatives ou aux bonnes pratiques professionnelles.

Pendant le cycle de certification, au moment de la première année (uniquement pour le premier certificat) et entre la deuxième et la sixième année (pour le premier certificat et les suivants) l’organisme de certification vérifie notamment que la personne certifiée : 

- se tient à jour des évolutions techniques, législatives et réglementaires dans le domaine concerné, notamment en s’assurant qu’elle a suivi la formation ;
- exerce réellement l’activité pour laquelle elle a obtenu une certification, en effectuant un contrôle documentaire via au moins cinq rapports (échantillon réalisé par l’organisme de certification) établis par la personne certifiée ;
- est dûment assurée.

Cette surveillance est complétée par un contrôle in situ des activités du diagnostiqueur par l’organisme de certification pendant le cycle de certification.  

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement)

Une personne légalement établie dans un État de l'UE ou d'un État partie à l'accord sur l'EEE pour y exercer une activité de diagnostic peut, après vérification de sa compétence technique et de sa bonne pratique de la langue française par un organisme de certification, (organisme de certification légalement établi dans un État de l'UE ou d’un État partie à l’accord sur l’EEE autre que la France) en collaboration avec les services français des ministres en charge de la santé et de la construction, exercer en France, à titre salarié ou à titre indépendant.

L'organisme de certification délivre une attestation d'équivalence de certification après vérification de la compétence technique au regard des informations fournies par la personne au moyen d'une déclaration et transmise à l'organisme de certification. L’'organisme de certification, informe, le candidat de sa décision :

a) de permettre la prestation de services en lui accordant une certification sans vérification complémentaire ;
b) de soumettre le prestataire aux examens, ou parties d'examen, nécessaires.

La compétence technique du déclarant est appréciée par référence aux exigences fixées par l’arrêté du 2 juillet 2018. 

Lorsque l'organisme de certification a autorisé l‘exercice de la profession de diagnostiqueur, la personne est soumise à la surveillance (cf. supra « 2°. a. qualifications professionnelles »), et en particulier le contrôle in situ qui devra être réalisé dans les deux ans.

*Pour aller plus loin :* paragraphe 5 « reconnaissance mutuelle » de l’annexe 1 de l’arrêté du 2 juillet 2018 susmentionné.

### c. Conditions d’honorabilité et sanctions pénales

Le professionnel ne doit avoir aucun lien de nature à porter atteinte à son impartialité et à son indépendance ni avec le propriétaire ou le mandataire qui fait appel à lui, ni avec une entreprise pouvant réaliser des travaux sur les ouvrages, installations ou équipements pour lesquels il lui est demandé d'établir l'un des six diagnostics obligatoires. 

**À noter**

Le professionnel peut être tenu au respect de règles déontologiques propres à chaque organisme certificateur. Pour plus d'informations, il est conseillé de consulter le site internet de ces organismes.

### d. Quelques particularités de la réglementation de l’activité

#### Assurances

Le professionnel est tenu de souscrire une assurance permettant de couvrir les conséquences d'un engagement de sa responsabilité en raison de ses interventions. Il souscrit une assurance dont le montant de la garantie ne peut être inférieur à 300 000 euros par sinistre et 500 000 euros par année d'assurance.

**À noter**

Si le professionnel est un salarié, c'est à son employeur de souscrire une telle assurance pour les actes effectués à l'occasion de son activité professionnelle.

*Pour aller plus loin :* articles L. 271-6 et R. 271-2 du Code de la construction et de l'habitation.

#### Obligations et méthodologie propres à chaque diagnostic

Le professionnel auquel il est fait appel pour l’établissement d’un des six diagnostics obligatoires remet préalablement à son client un document par lequel il atteste sur l’honneur qu’il est en situation régulière au regard des obligations réglementaires. 

Les diagnostics obligatoires établis sous couvert de la certification comportent la mention suivante : « Le présent rapport est établi par une personne dont les compétences sont certifiées par [...] », complétée par le nom et l'adresse postale de l'organisme certificateur concerné. 

Il est tenu de suivre des règles méthodologiques en fonction de la nature du diagnostic effectué.

Pour information, voici une la liste non exhaustive des textes réglementaires définissant le modèle et la méthode de réalisation des diagnostics :

- arrêté du 19 août 2011 relatif au constat de risque d'exposition au plomb ;
- arrêté du 29 mars 2007 définissant le modèle et la méthode de réalisation de l'état du bâtiment relatif à la présence de termites ;
- arrêtés issus des articles R. 1334-14 à 1334-29-9 du Code de la santé relatifs à la prévention des risques liés à l'amiante dans les immeubles bâtis, et l’arrêté du 16 juillet 2019 relatif au repérage de l'amiante avant certaines opérations réalisées dans les immeubles bâtis ;
- arrêté du 6 avril 2007 définissant le modèle et la méthode de réalisation de l'état de l'installation intérieure de gaz ;
- arrêté du 28 septembre 2017 définissant le modèle et la méthode de réalisation de l'état de l'installation intérieure d'électricité dans les immeubles à usage d'habitation ;
- arrêté du 15 septembre 2006 relatif au diagnostic de performance énergétique pour les bâtiments existants proposés à la vente en France métropolitaine, arrêté du 3 mai 2007 relatif au diagnostic de performance énergétique pour les bâtiments existants à usage principal d'habitation proposés à la location en France métropolitaine, arrêté du 18 avril 2012 relatif au diagnostic de performance énergétique pour les centres commerciaux existants proposés à la vente ou à la location en France métropolitaine.

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin :* arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

#### Autorité compétente

Le diagnostiqueur immobilier doit procéder à la déclaration de son entreprise. Il doit pour cela effectuer une déclaration auprès de la chambre de commerce et d'industrie (CCI).

#### Pièces justificatives

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre de formalités des entreprises de la CCI adresse le jour même un récépissé au professionnel indiquant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter.

Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier a été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais indiqués ci-dessus.

Si le CFE refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin :* article 635 du Code général des impôts.

### b. Le cas échéant, enregistrement des statuts de la société

Le diagnostiqueur immobilier doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ; 
- si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin :* article 635 du Code général des impôts.