﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS034" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Home inspector" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="home-inspector" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/home-inspector.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="home-inspector" -->
<!-- var(translation)="Auto" -->

Home inspector
========================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

The real estate diagnostician is a professional whose activity consists, on the occasion of a real estate transaction, of establishing a technical balance sheet of the building.

The Technical Diagnostics (DDT) file may include:

- Energy performance (energy performance diagnosis (EDP)) of the building for the issuance of the building's energy label;
- The safety of its gas facilities;
- Electricity facilities
- the presence of asbestos, lead or any other cause of unsanitary conditions.

**Please note**

The documents established in connection with his activity include the mention of the professional's certification.

*For further information*: Articles L. 271-4 and R. 271-3 of the Building and Housing Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out.

The activity of real estate diagnostics is commercial in nature, so the competent CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To carry out the activity of real estate diagnostic, the professional must have one of the following diplomas:

- a professional license mentioning "BTP trades: energy and environmental performance of buildings" or "Safety of goods and people";
- a diploma in building and public works research research by the National Conservatory of Arts and Crafts (Cnam);
- a real estate diagnostics technician's degree from the Technical Gas and Air Institute (ITGA);
- a real estate diagnostic diploma from a higher education institution.

These diplomas are available to candidates from a training course under student or student status, apprenticeship contract, after continuing education, professionalisation contract, individual application or through the process of validation of experience (VAE). For more information, please refer to the official website of the[Vae](http://www.vae.gouv.fr/).

In the absence of any of the above degrees, the professional must have three years of professional experience as a technician or master's officer in the building field, or any other equivalent function.

*For further information*: National directory of professional certifications ([RNCP](http://www.rncp.cncp.gouv.fr)) ; Appendix 2 of the decree of 16 October 2006 setting out the criteria for certifying the skills of individuals making the diagnosis of energy performance or the certificate of consideration of thermal regulations and criteria accreditation of certification bodies.

Once the professional fulfils these conditions, he must carry out the procedure of certifying his skills (see infra "3." a. Certification of skills").

*For further information*: Article R. 271-1 of the Building and Housing Code.

### b. Professional Qualifications - European Nationals (Free Service Or Freedom of establishment)

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement may practice on a temporary and casual basis or the same activity in France.

For this reason, the national is subject to the same requirements as the French national (see above "2." a. Professional qualifications") and must thus justify a diploma of a level equivalent to that required for a French national.

In addition, it must obtain certification from a European body that is a signatory to the multilateral European agreement taken as part of the European coordination of accreditation bodies.

*For further information*: Appendix 2 of the order of 16 October 2006 above.

### c. Conditions of honourability and criminal penalties

The professional must carry out his activity impartially and independently and must, as such, have no connection to interfere with these obligations.

**Please note**

The professional may be required to comply with the ethical rules specific to each certifying body. For more information, it is advisable to visit the website of these organizations.

*For further information*: Articles L. 271-6 and R. 271-3 of the Building and Housing Code.

**Criminal sanctions**

The real estate diagnosticor faces a fine of up to 1,500 euros (or 3,000 euros in the event of a repeat offence) if he:

- makes a diagnosis without being professionally qualified, without having taken out insurance (see infra "2. (d. Insurance) or in a lack of knowledge of the obligations of impartiality and independence;
- is certified by an uncredited body to issue certification.

*For further information*: Article R. 271-4 of the Building and Housing Code.

### d. Some peculiarities of the regulation of the activity

**Insurance**

The real estate diagnosticor is required to take out insurance to discover the risks incurred during his activity. The amount of this guarantee cannot be less than 300,000 euros per claim and 500,000 euros per year of insurance.

**Please note**

If the professional is an employee, it is up to his employer to take out such insurance for the acts performed during his professional activity.

*For further information*: Articles L. 271-6 and R. 271-2 of the Building and Housing Code.

**Obligations and methodology specific to each diagnosis**

Before each technical diagnosis, the professional gives the client a document attesting to the honour that he is in a regular situation and that he has the necessary means to carry out his performance. In addition, it is required to follow methodological rules and to submit a report to the competent authority depending on the nature of the diagnosis made.

Thus, in a non-exhaustive way:

- all EPDs must be forwarded to the Environment and Energy Management Agency ([Ademe](http://www.ademe.fr/)) for statistical approach, evaluation and methodological improvement (see Article L. 134-4-2 of the Building and Housing Code);
- after a diagnosis of the gas installation, the professional must prepare a visit report according to the model set out in Schedule 1 of the Stopped April 6, 2007 defining the model and method of achieving the state of the indoor gas facility;
- in the event of a diagnosis of electrical installations, it must prepare a report in accordance with the model set at the Stopped September 28, 2017 defining the model and method of achieving the state of indoor electricity installation in residential buildings;
- After a diagnosis of the presence of thermites in the building concerned, the professional must prepare a report in accordance with the provisions of the Stopped March 29, 2007 defining the model and method of achieving the condition of the building for the presence of termites.

**Monitoring**

The certified professional is monitored by the certifying body and must provide:

- The status of claims and complaints against him during his activity;
- all the reports and information about the mission carried out (date, premises, type of mission, energy class of premises).

**Please note**

This information may be requested during the certification renewal process.

*For further information*: Article 2-1 of the order of 16 October 2006 above; Section R. 271-1 of the Housing and Construction Code.

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

It is advisable to refer to the "Establishment receiving the public" sheet for more information.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Skills certification

The candidate for certification must submit an application file to the certifying body which will judge its admissibility. Once eligible, the candidate must be the subject of:

- theoretical examination to verify that the person has the knowledge of:- generalities about the building,
  - The thermal of the building,
  - The envelope of the building,
  - different systems (heating technologies, the implementation of renewable energy, etc.),
  - Regulatory texts
- a practical examination of a candidate's situational analysis to verify that he or she is capable of assessing a building's energy consumption and developing a diagnosis of energy performance taking into account the specifics of the case Treaty.

The certification awarded to the professional is valid for five years. At the end of this period it must proceed with its renewal.

**Please note**

A professional may only hold one certification and must provide a declaration of honour attesting that he has no other certification.

*For further information*: Article R. 134-4 of the Building and Housing Code; October 16, 2006.

### b. Company reporting formalities

**Competent authority**

The real estate diagnosticor must proceed with the declaration of his company. To do so, it must make a declaration to the Chamber of Commerce and Industry (CCI).

**Supporting documents**

The person concerned must provide the supporting documents depending on the nature of its activity.

**Timeframe**

The CCI's Business Formalities Centre sends a receipt to the professional on the same day indicating the missing documents on file. If necessary, the professional has a period of fifteen days to complete it.

Once the file is complete, the ICC will tell the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the time frames mentioned above.

If the CFE refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

#### c. If necessary, registration of the company's statutes

The real estate diagnosticor must, once the company's statutes are dated and signed, register them with the Corporate Tax Office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building when the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.