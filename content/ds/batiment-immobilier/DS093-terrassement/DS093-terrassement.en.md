﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS093" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Construction and real estate" -->
<!-- var(title)="Earthwork" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="construction-real-estate" -->
<!-- var(title-short)="earthworks" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/construction-real-estate/earthworks.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="earthworks" -->
<!-- var(translation)="Auto" -->


Earthwork
=====

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The earthworks activity consists for the professional (the terrassier) to modify the natural terrain of a land and to prepare the soil for future construction or modification of the structure.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To carry out the terrasser's activity, the professional must:

- depending on the case, be professionally qualified;
- perform an installation preparation course (SPI) (see infra "3. a. Do a pre-installation internship");
- depending on the nature of its activity, register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS).

It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

#### Training

It is possible to carry out this activity without professional qualifications as long as no construction of work is carried out during the work.

On the other hand, as long as such construction is planned, the professional must be qualified or placed under the effective and permanent control of a qualified person.

The person who practices or controls the exercise must be the holder, of the choice:

- A Certificate of Professional Qualification (CAP) "Engine Drivers: Public Works and Careers";
- a Professional Studies Patent (BEP) "Public Works";
- a professional patent (BP) "Driver of public works equipment";
- a University Degree in Technology (DUT) "Civil Engineering Option Public Works and Planning (TPA)";
- a professional title (TP) "Driver of public works road, pipeline, earthworks";
- a professional license in one of the following specialties:- "Public Works,"
  - "Construction trades: public works,"
  - "Specialty construction materials industrial activities in the concrete sector"
  - "Civil engineering and construction";
- an engineer specializing in public works, civil engineering, geology or electrical mechanics;
- a diploma or a degree of equal or higher level approved or registered when it was issued to the National Directory of Professional Certifications (RNCP).

In the absence of one of these diplomas, the person concerned will have to justify an effective three years' professional experience, in a European Union (EU) state or party to the agreement on the European Economic Area (EEA), acquired as a leader self-employed or salaried in the job of a terrassier. In this case, the person concerned will be able to apply to the relevant CMA for a certificate of professional qualification.

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 1 of Decree 98-246 of 2 April 1998; Decree No. 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act 96-603 of 5 July 1996; The official website of the[RNCP](http://www.rncp.cncp.gouv.fr).

### b. Professional Qualifications - European Nationals (Free Service Or Freedom of establishment)

#### For Freedom to provide services

The professional who is a member of the EU or the EEA may carry out the earthmoving activity in France on a temporary and casual basis if he is legally established in one of these states and carries out the same activity there.

He will first have to request it by written declaration to the relevant CMA (see infra "3°. b. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)).

If neither the activity nor the training leading to it is regulated in the state in which the professional is legally established, the person must also prove that he or she has been active in that state for at least one year full-time or part-time during the ten years before the performance he wants to perform in France.

**Please note**

The professional who meets these conditions is exempt from the registration requirements of the trades directory (RM) or the register of companies.

*For further information*: Section 17-1 of Act 96-603 of July 5, 1996.

#### For a Freedom of establishment

In order to carry out the earthmoving activity on a permanent basis in France, the professional national of the EU or the EEA must meet one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above: "2. a. Professional qualifications");
- Hold a certificate of competency or training degree required for the exercise of the activity in an EU or EEA state when that state regulates access or exercise of this activity on its territory;
- have a certificate of competency or a training document that certifies its preparation for the exercise of the activity when this certificate or title has been obtained in an EU or EEA state that does not regulate access or the exercise of this activity;
- be a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has been active for three years in the state that has admitted equivalence.

**Please note**

A national of an EU or EEA state that meets one of the above conditions may apply for a certificate of recognition of professional qualification (see below: "3. c. If necessary, request a certificate of recognition of professional qualification.")

If the individual does not meet any of the above conditions, the CMA may ask him to perform a compensation measure in the following cases:

- if the duration of the certified training is at least one year less than that required to obtain one of the professional qualifications required in France to carry out the activity;
- If the training received covers subjects substantially different from those covered by one of the qualifications required to carry out the activity in France;
- If the effective and permanent control of the activity requires, for the exercise of some of its remits, specific training which is not provided in the Member State of origin and covers subjects substantially different from those covered by the certificate of competency or training designation referred to by the applicant.

*For further information*: Articles 17 and 17-1 of Act 96-603 of July 5, 1996; Articles 3 to 3-2 of Decree 98-246 of 2 April 1998.

**Good to know: compensation measures**

The CMA, which is applying for a certificate of recognition of professional qualification, notifies the applicant of his decision to have him perform one of the compensation measures. This decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose between an adjustment course of up to three years or an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organised within six months of the CMA's receipt of the applicant's decision to opt for the event. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the adaptation course, the applicant sends the CMA a certificate certifying that he has validly completed this internship, accompanied by an evaluation of the organization that supervised him. The CMA issues, on the basis of this certificate, a certificate of professional qualification within one month.

The decision to use a compensation measure may be challenged by the person concerned who must file an administrative appeal with the prefect within two months of notification of the decision. If his appeal is dismissed, he can then initiate a legal challenge.

*For further information*: Articles 3 and 3-2 of Decree 98-246 of 2 April 1998; Article 6-1 of Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

### c. Conditions of honorability

No one may practise as a terrassier if he is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 of Act 96-603 of July 5, 1996.

### d. Some peculiarities of the regulation of the activity

#### Regulations on the quality of craftsman and the titles of master craftsman and best worker in France

**Craftsmanship**

To claim the status of craftsman, the person must justify either:

- a CAP, a BEP or a certified or registered title when it was issued to the RNCP at least equivalent (see above: "2. a. Professional qualifications");
- professional experience in this trade for at least three years.

*For further information*: Article 1 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of master craftsman**

This title is given to individuals, including the social leaders of legal entities:

- Registered in the trades directory;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

###### Note that

Individuals who do not hold the master's degree can apply to the Regional Qualifications Commission for the title of Master Craftsman under two assumptions:

- when they are registered in the trades directory, have a degree at least equivalent to the master's degree, and justify management and psycho-pedagogical knowledge equivalent to those of the corresponding value units of the master's degree, and that they justify two years of professional practice;
- when they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of best worker in France (MOF)**

The professional diploma "one of the best workers in France" is a state diploma that attests to the acquisition of a high qualification in the exercise of a professional activity in the artisanal, commercial, industrial or agricultural fields.

The diploma is classified at level III of the interdepartmental nomenclature of training levels. It is issued after an examination called "one of the best workers in France" under a profession called "class" attached to a group of trades.

For more information, it is recommended that you consult[official website](http://www.meilleursouvriersdefrance.info/) "one of the best workers in France."

*For further information*: Article D. 338-9 of the Education Code.

#### Compliance with safety and accessibility standards

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the fire and panic safety regulations in the ERPs.

It is advisable to refer to the "Establishment receiving the public" sheet for more information.

#### Compliance with the provisions for young workers

Some work cannot be carried out by miners, including:

- likely to expose them to hazards (radiation, metals, hazardous chemical or biological agents, etc.);
- temporary high-rise work;
- exposing them to extreme temperatures.

*For further information*: Articles D. 4153-15 and the following articles of the Labour Code.

#### Worker safety

The professional is required to ensure the safety and health of the people working on the site. To do this, he must:

- Assess risks and avoid them as much as possible
- adapt equipment and working methods to its employees;
- take collective protection measures and give appropriate instructions to workers.

*For further information*: Articles L. 4121-1 to L. 4121-5 of the Labour Code.

#### Occupational insurance

Any builder of a work can be held liable as soon as damage occurs and affects its use and solidity. This responsibility is said to be ten years old and runs for a period of ten years from the time the work is received.

As such, in order to be covered against this damage, the professional must take out insurance before the start of the construction or, in the context of a public contract, at the time of filing the application.

In addition, when the professional enters into a subcontract, he or she is bound by the same insurance obligations as the prime contractor who has the work done on behalf of others.

**Please note**

Professional liability insurance is optional but nevertheless recommended, especially for risky activities.

*For further information*: Articles L. 241-1 and L. 241-2 of the Insurance Code.

**Authorization to drive self-propelled vehicles and lifting devices**

Any professional who uses load lifting equipment (grues, construction equipment, lift platforms, etc.) during his activity must have a driving authorization issued by the head of the equipment. establishment. In order to do so, the person concerned must have been trained.

The duration and content of this training vary depending on the nature of the equipment involved. For more information, it is advisable to get closer to the organizations providing the training or the institution in which the professional works.

*For further information*: Articles R. 4323-55 to R. 4353-57 of the Labour Code; order of 2 December 1998 relating to the training in the operation of self-propelled mobile work equipment and load or lifting devices.

**Asbestos removal**

The company employing workers who may be exposed to asbestos must put in place protective measures adapted to the nature of the work carried out, including:

- To ensure the protection of exposed professionals;
- to limit the spread of asbestos fibres.

In addition, if the level of dusting is too high, the professional must take the necessary measures to reduce the level of the level and must inform the contractor, the labour inspector and the agency of prevention services social security agencies.

*For further information*: Articles R. 4412-107 to R. 4412-115 of the Labour Code.

**Technical rules for new equipment**

When the professional uses new or new equipment, he must comply with the technical rules set out in the Appendix 1 Article R. 4312-1 of the Labour Code.

**Technical rules of protection**

The professional carrying out earthworks must take all the general safety measures, including to inform himself before the start of the work with the road service:

- the existence of reported land;
- the presence of cables or pipes in the work area;
- risks of impregnating the basement with fumes or harmful products.

*For further information*: Articles R. 4534-1 and the following articles of the Labour Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Complete an installation preparation course (SPI)

Before registration, the professional must complete an installation preparation course (SPI). The latter is a mandatory requirement for anyone applying for registration in the trades and crafts repertoire.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary for the creation of a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including an education in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional from a Member State of the European Union (EU) or another State party to the Agreement on the European Economic Area (EEA) is exempt from the SPI if he justifies with the CMA a qualification in management confers on him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who:

- have either worked for at least three years requiring a level of knowledge equivalent to that provided by the internship;
- either have knowledge acquired in an EU or EEA state or a third country during a professional experience that can fully or partially cover the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of their professional qualifications shows substantial differences with those required. France for the management of a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982, Article 6-1 of Decree 83-517 of 24 June 1983.

### b. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)

**Competent authority**

The CMA of the place in which the national wishes to carry out the benefit is competent to issue the prior declaration of activity.

**Supporting documents**

The request for a pre-report of activity is accompanied by a complete file containing the following supporting documents:

- A photocopy of a valid ID
- a certificate justifying that the national is legally established in an EU or EEA state;
- a document justifying the professional qualification of the national who may be, at your choice:- A copy of a diploma, title or certificate,
  - A certificate of competency,
  - any documentation attesting to the national's professional experience.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Please note**

When the file is incomplete, the CMA has a period of fifteen days to inform the national and request all the missing documents.

**Outcome of the procedure**

Upon receipt of all the documents in the file, the CMA has one month to decide:

- either to authorise the benefit where the national justifies three years of work experience in an EU or EEA state, and to attach to that decision a certificate of professional qualification;
- or to authorize the provision when the national's professional qualifications are deemed sufficient;
- or to impose an aptitude test on him when there are substantial differences between the professional qualifications of the national and those required in France. In the event of a refusal to carry out this compensation measure or in the event of failure in its execution, the national will not be able to carry out the provision of services in France.

The silence kept by the competent authority within this period is worth authorisation to begin the provision of services.

*For further information*: Article 2 of the decree of 2 April 1998; Article 2 of the decree of 17 October 2017 relating to the submission of the declaration and requests provided for by Decree 98-246 of 2 April 1998 and Title 1 of Decree 98-247 of 2 April 1998.

### c. If necessary, request a certificate of recognition of professional qualification

The person concerned wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of recognition of professional qualification.

**Competent authority**

The request must be addressed to the territorially competent CMA.

**Procedure**

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the person concerned to complete it within a fortnight of filing the file. A receipt is issued as soon as the file is complete.

**Supporting documents**

The folder should contain the following parts:

- Applying for a certificate of professional qualification
- A certificate of competency or diploma or vocational training designation;
- Proof of the applicant's nationality
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, the proofs of the exercise of the activity for three years.

The CMA may request further information about its training or professional experience from the individual to determine the possible existence of substantial differences with professional qualification. required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (CIEP) to obtain additional information on the level of training of a diploma or certificate or a foreign designation, the applicant will have to pay a fee Additional.

**What to know**

If necessary, all supporting documents must be translated into French.

**Response time**

Within three months of the receipt, the CMA may:

- Recognise professional qualification and issue certification of professional qualification;
- decide to subject the applicant to a compensation measure and notify him of that decision;
- refuse to issue the certificate of professional qualification.

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

**Remedies**

If the CMA refuses to issue the recognition of professional qualification, the applicant may initiate, within two months of notification of the refusal of the CMA, a legal challenge before the relevant administrative court. Similarly, if the person concerned wishes to challenge the CMA's decision to submit it to a compensation measure, he must first initiate a graceful appeal with the prefect of the department in which the CMA is based within two months of notification of the decision. CMA. If he does not succeed, he may opt for a litigation before the relevant administrative tribunal.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the Community or another state party to the European Economic Area agreement.

### d. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Artisanal Company Reporting Formalities" for more information.

