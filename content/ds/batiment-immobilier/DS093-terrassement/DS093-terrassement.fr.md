﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS093" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Bâtiment – Immobilier" -->
<!-- var(title)="Terrassement" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="batiment-immobilier" -->
<!-- var(title-short)="terrassement" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/batiment-immobilier/terrassement.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="terrassement" -->
<!-- var(translation)="None" -->

# Terrassement

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité de terrassement consiste pour le professionnel (le terrassier) à modifier le relief naturel d'un terrain et à préparer le sol pour de futurs travaux de construction ou de modification d'ouvrage.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour une activité commerciale, le CFE compétent est la chambre de commerce et d’industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité de terrassier, le professionnel doit :

- selon le cas, être qualifié professionnellement ;
- effectuer un stage de préparation à l'installation (SPI) (cf. infra « 3. a. Effectuer un stage de préparation à l'installation ») ;
- selon la nature de son activité, s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

#### Formation

Il est possible d'exercer cette activité sans qualification professionnelle dès lors qu'aucune construction d'ouvrage n'est réalisée lors des travaux.

En revanche, dès lors qu'une telle construction est prévue, le professionnel doit être qualifié ou placé sous le contrôle effectif et permanent d’une personne qualifiée.

La personne qui exerce ou qui en contrôle l’exercice doit être titulaire, au choix :

- d'un certificat d'aptitude professionnelle (CAP) « Conducteurs d'engins : travaux publics et carrières » ;
- d'un brevet d'étude professionnelle (BEP) « Travaux publics » ;
- d'un brevet professionnel (BP) « Conducteur d'engins de chantier de travaux publics » ;
- d'un diplôme universitaire de technologie (DUT) « Génie civil option travaux publics et aménagement (TPA) » ;
- d'un titre professionnel (TP) « Conducteur de travaux publics route, canalisation, terrassement » ;
- d'une licence professionnelle dans l'une des spécialités suivantes :
  - « Travaux publics »,
  - « Métiers du BTP : travaux publics »,
  - « Matériaux de construction spécialité activités industrielles de la filière béton »,
  - « Génie civil et construction » ;
- d'un titre d'ingénieur spécialisé dans les travaux publics, le génie civil, la géologie ou la mécanique-électrique ;
- d’un diplôme ou d’un titre de niveau égal ou supérieur homologué ou enregistré lors de sa délivrance au Répertoire national des certifications professionnelles (RNCP).

À défaut de l’un de ces diplômes, l’intéressé devra justifier d’une expérience professionnelle de trois années effectives, dans un État de l’Union européenne (UE) ou partie à l’accord sur l’Espace économique européen (EEE), acquise en qualité de dirigeant d’entreprise, de travailleur indépendant ou de salarié dans l’exercice du métier de terrassier. Dans ce cas, l’intéressé pourra effectuer une demande d’attestation de reconnaissance de qualification professionnelle auprès de la CMA compétente.

*Pour aller plus loin* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l’artisanat ; article 1er du décret n° 98-246 du 2 avril 1998 ; décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l’exercice des activités prévues à l’article 16 de la loi n° 96-603 du 5 juillet 1996 ; site officiel du [RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement)

#### Pour une Libre Prestation de Services (LPS)

Le professionnel ressortissant de l’UE ou de l’EEE peut exercer en France à titre temporaire et occasionnel l'activité de terrassement s'il est légalement établi dans un de ces États et y exerce la même activité.

Il devra au préalable en faire la demande par déclaration écrite adressée à la CMA compétente (cf. infra « 3°. b. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS) »).

Si ni l’activité ni la formation y conduisant ne sont réglementées dans l’État dans lequel le professionnel est légalement établi, l’intéressé doit en outre prouver qu’il a exercé l’activité dans cet État pendant au moins un an à temps plein ou à temps partiel au cours des dix années précédant la prestation qu’il veut effectuer en France.

**À noter**

Le professionnel qui répond à ces conditions est dispensé des exigences relatives à l’immatriculation au répertoire des métiers (RM) ou au registre des entreprises.

*Pour aller plus loin* : article 17-1 de la loi n° 96-603 du 5 juillet 1996.

#### Pour un Libre Établissement (LE)

Pour exercer l'activité de terrassement à titre permanent en France, le professionnel ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- être titulaire d’une attestation de compétence ou d’un titre de formation requis pour l’exercice de l’activité dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’une attestation de compétence ou d’un titre de formation qui certifie sa préparation à l’exercice de l’activité lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de cette activité ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité dans l’État qui a admis l’équivalence.

**À noter**

Le ressortissant d’un État de l’UE ou de l’EEE qui remplit l’une des conditions précitées peut solliciter une attestation de reconnaissance de qualification professionnelle (cf. infra « 3°. c. Le cas échéant, demander une attestation de reconnaissance de qualification professionnelle ».)

Si l’intéressé ne remplit aucune des conditions précitées, la CMA saisie peut lui demander d’accomplir une mesure de compensation dans les cas suivants :

- si la durée de la formation attestée est inférieure d’au moins un an à celle exigée pour obtenir l’une des qualifications professionnelles requises en France pour exercer l’activité ;
- si la formation reçue porte sur des matières substantiellement différentes de celles couvertes par l’un des titres ou diplômes requis pour exercer en France l’activité ;
- si le contrôle effectif et permanent de l’activité nécessite, pour l’exercice de certaines de ses attributions, une formation spécifique qui n’est pas prévue dans l’État membre d’origine et porte sur des matières substantiellement différentes de celles couvertes par l’attestation de compétence ou le titre de formation dont le demandeur fait état.

*Pour aller plus loin* : articles 17 et 17-1 de la loi n° 96-603 du 5 juillet 1996 ; articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998.

#### Bon à savoir : mesures de compensation

La CMA saisie d’une demande de délivrance d’une attestation de reconnaissance de qualification professionnelle notifie au demandeur sa décision tendant à lui faire accomplir l'une des mesures de compensation. Cette décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est impérative pour exercer en France.

Le demandeur doit alors choisir entre un stage d’adaptation d’une durée maximale de trois ans ou une épreuve d’aptitude.

L’épreuve d’aptitude prend la forme d’un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA de la décision du demandeur d’opter pour cette épreuve. À défaut, la qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

À l'issue du stage d’adaptation, le demandeur adresse à la CMA une attestation certifiant qu’il a valablement accompli ce stage, accompagnée d’une évaluation de l’organisme qui l’a encadré. La CMA délivre, sur la base de cette attestation, une attestation de qualification professionnelle dans un délai d’un mois.

La décision de recourir à une mesure de compensation peut être contestée par l’intéressé qui doit former un recours administratif auprès du préfet dans un délai de deux mois à compter de la notification de la décision. En cas de rejet de son recours, il peut alors initier un recours contentieux.

*Pour aller plus loin* : articles 3 et 3-2 du décret n° 98-246 du 2 avril 1998 ; article 6-1 du décret n° 83-517 du 24 juin 1983 fixant les conditions d’application de la loi 82-1091 du 23 décembre 1982 relative à la formation professionnelle des artisans.

### c. Conditions d’honorabilité

Nul ne peut exercer la profession de terrassier s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévue au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 de la loi n° 96-603 du 5 juillet 1996.

### d. Quelques particularités de la réglementation de l’activité

#### Réglementation relative à la qualité d’artisan et aux titres de maître artisan et de meilleur ouvrier de France

##### La qualité d’artisan

Pour se prévaloir de la qualité d’artisan, la personne doit justifier soit :

- d’un CAP, d’un BEP ou d’un titre homologué ou enregistré lors de sa délivrance au RNCP d’un niveau au moins équivalent (cf. supra « 2°. a. Qualifications professionnelles ») ;
- d’une expérience professionnelle dans ce métier de trois ans au moins.

*Pour aller plus loin* : article 1er du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

##### Le titre de maître artisan

Ce titre est attribué aux personnes physiques, y compris les dirigeants sociaux des personnes morales :

- immatriculées au répertoire des métiers ;
- titulaires du brevet de maîtrise dans le métier exercé ;
- justifiant d’au moins deux ans de pratique professionnelle.

**À noter**

Les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan auprès de la commission régionale des qualifications dans deux hypothèses :

- lorsqu’elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de niveau de formation au moins équivalent au brevet de maîtrise, qu’elles justifient de connaissances en gestion et en psychopédagogie équivalentes à celles des unités de valeur correspondantes du brevet de maîtrise, et qu’elles justifient de deux ans de pratique professionnelle ;
- lorsqu’elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles disposent d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

*Pour aller plus loin* : article 3 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

##### Le titre de meilleur ouvrier de France (MOF)

Le diplôme professionnel « un des meilleurs ouvriers de France » est un diplôme d’État qui atteste l’acquisition d’une haute qualification dans l’exercice d’une activité professionnelle dans les domaines artisanal, commercial, industriel ou agricole.

Le diplôme est classé au niveau III de la nomenclature interministérielle des niveaux de formation. Il est délivré à l’issue d’un examen dénommé concours « un des meilleurs ouvriers de France » au titre d’une profession dénommée « classe », rattachée à un groupe de métiers.

Pour plus d’informations, il est recommandé de consulter le [site officiel](http://www.meilleursouvriersdefrance.info/) du concours « un des meilleurs ouvriers de France ».

*Pour aller plus loin* : article D. 338-9 du Code de l’éducation.

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les ERP.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

#### Respect des dispositions en matière de jeunes travailleurs

Certains travaux ne peuvent être confiés à des mineurs, notamment les travaux :

- susceptibles de les exposer à des dangers (rayonnements, métaux, agents chimiques ou biologiques dangereux, etc.) ;
- temporaires effectués en hauteur ;
- les exposant à des températures extrêmes.

*Pour aller plus loin* : articles D. 4153-15 et suivants du Code du travail.

#### Sécurité des travailleurs 

Le professionnel est tenu d'assurer la sécurité et de protéger la santé physique et mentale des personnes travaillant sur le chantier. Pour cela, il doit :

- évaluer les risques et les éviter autant que possible ;
- adapter les équipements et les méthodes de travail à ses salariés ;
- prendre des mesures de protection collective et donner des instructions appropriées aux travailleurs.

*Pour aller plus loin* : articles L. 4121-1 à L. 4121-5 du Code du travail.

#### Assurances professionnelles

Tout constructeur d'un ouvrage peut voir sa responsabilité engagée dès lors que des dommages surviennent et affectent son usage et sa solidité. Cette responsabilité est dite décennale et court pendant une durée de dix ans à compter de la réception de l'ouvrage.

À ce titre, afin d'être couvert contre ces dommages, le professionnel doit souscrire une assurance avant le début du chantier ou, dans le cadre d'un marché public, au moment du dépôt de la candidature.

En outre, lorsque le professionnel a conclu un contrat de sous-traitance, il est tenu aux mêmes obligations d'assurance que l'entrepreneur principal qui fait réaliser les travaux pour le compte d'autrui.

**À noter**

L'assurance de responsabilité civile professionnelle est facultative mais néanmoins conseillée, notamment pour les activités à risques.

*Pour aller plus loin* : articles L. 241-1 et L. 241-2 du Code des assurances.

#### Autorisation de conduite des engins automoteurs et des appareils de levage

Tout professionnel qui utilise au cours de son activité des équipements servant au levage de charges (grues, engins de chantier, plates-formes élévatrices, etc.) doit être titulaire d'une autorisation de conduite de ces équipements délivrée par le chef de l'établissement. Pour cela, l'intéressé doit avoir suivi une formation.

La durée et le contenu de cette formation varient selon la nature de l'équipement concerné. Pour plus d'informations, il est conseillé de se rapprocher des organismes dispensant la formation ou de l'établissement au sein duquel exerce le professionnel.

*Pour aller plus loin* : articles R. 4323-55 à R. 4353-57 du Code du travail ; arrêté du 2 décembre 1998 relatif à la formation à la conduite des équipements de travail mobiles automoteurs et des appareils de levage de charges ou de personnes.

#### Désamiantage

L'entreprise employant des travailleurs susceptibles d'être exposés à de l'amiante doit mettre en place des moyens de protection adaptés à la nature des travaux effectués, en vue notamment :

- d'assurer la protection des professionnels exposés ;
- de limiter la propagation des fibres d'amiante.

En outre, dès lors que le niveau d'empoussièrement est trop important, le professionnel doit prendre les mesures nécessaires pour en réduire le niveau et doit en informer le donneur d'ordre, l'inspecteur du travail et l'agence des services de prévention des organismes de sécurité sociale.

*Pour aller plus loin* : articles R. 4412-107 à R. 4412-115 du Code du travail.

#### Règles techniques relatives aux équipements neufs

Lorsque le professionnel utilise des équipements neufs ou considérés comme neufs, il doit respecter les règles techniques fixées à l'annexe 1 de l'article R. 4312-1 du Code du travail.

#### Règles techniques de protection

Le professionnel effectuant des travaux de terrassement doit prendre l'ensemble des mesures générales de sécurité, et notamment s'informer avant le début des travaux auprès du service de voirie :

- de l'existence de terres rapportées ;
- de la présence de câbles ou canalisations présents dans la zone de travaux ;
- des risques d'imprégnation du sous-sol par des émanations ou des produits nocifs.

*Pour aller plus loin* : articles R. 4534-1 et suivants du Code du travail.

## 3°. Démarches et formalités d’installation

### a. Effectuer un stage de préparation à l'installation (SPI) 

Avant son immatriculation, le professionnel doit effectuer un stage de préparation à l'installation (SPI). Ce dernier est un prérequis obligatoire pour toute personne sollicitant son immatriculation au répertoire des métiers et de l'artisanat.

#### Modalités du stage

L’inscription se fait sur présentation d’une pièce d’identité auprès de la CMA territorialement compétente. Le stage a une durée minimale de 30 heures et se présente sous la forme de cours et de travaux pratiques. Il a pour objectif de faire acquérir les connaissances essentielles dans les domaines juridique, fiscal, social et comptable, nécessaires à la création d’une entreprise artisanale.

#### Report exceptionnel du début du stage

En cas de force majeure, l’intéressé est autorisé à effectuer le SPI dans un délai d’un an à compter de l’immatriculation de son entreprise au répertoire des métiers. Il ne s’agit pas d’une dispense mais simplement d’un report du stage qui demeure obligatoire.

#### Issue du stage

Le participant recevra une attestation de suivi de stage qu’il devra obligatoirement joindre à son dossier de déclaration d’entreprise.

#### Coût

Le stage est payant. À titre indicatif, la formation coûtait environ 260 euros en 2017.

#### Cas de dispense de stage

L’intéressé peut être dispensé d’effectuer le stage dans deux situations :

- s’il a déjà bénéficié d’une formation sanctionnée par un titre ou un diplôme homologué au niveau III (bac +2) comportant un enseignement en économie et gestion d’entreprise, ou le brevet de maîtrise délivré par une CMA ;
- s’il a exercé pendant au moins trois ans une activité professionnelle requérant un niveau de connaissance équivalent à celui fourni par le stage.

#### Dispense de stage pour les ressortissants de l’UE ou de l’EEE

Par principe, un professionnel qualifié ressortissant d'un État membre de l'Union européenne (UE) ou d'un autre État partie à l'accord sur l'Espace économique européen (EEE) est dispensé du SPI s’il justifie auprès de la CMA d’une qualification en gestion d’entreprise lui conférant un niveau de connaissance équivalent à celui fourni par le stage.

La qualification en gestion d’entreprise est reconnue comme équivalente à celle fournie par le stage pour les personnes qui :

- ont soit exercé, pendant au moins trois ans, une activité professionnelle requérant un niveau de connaissance équivalent à celui fourni par le stage ;
- soit disposent de connaissances acquises dans un État de l’UE ou de l’EEE ou un État tiers au cours d’une expérience professionnelle de nature à couvrir totalement ou partiellement la différence substantielle en matière de contenu.

Pour les personnes ne répondant pas à ces conditions, la chambre consulaire peut exiger qu’elles se soumettent à une mesure de compensation si l’examen de leurs qualifications professionnelles fait apparaître des différences substantielles avec celles requises en France pour la direction d’une entreprise artisanale.

#### Modalités de la dispense de stage

Pour être dispensé de SPI, l’intéressé (français ou ressortissant de l’UE ou de l’EEE) doit adresser au président de la CMA concernée une demande de dispense de stage.

Il doit accompagner son courrier des pièces justificatives suivantes :

- copie du diplôme homologué au niveau III ;
- copie du brevet de maîtrise ;
- justificatifs d’exercice d’une activité professionnelle requérant un niveau de connaissance équivalent ;
- l'acquittement de frais variables.

L’absence de réponse dans un délai d’un mois suivant la réception de la demande vaut acceptation de la demande de dispense de stage.

*Pour aller plus loin* : article 2 de la loi n° 82-1091 du 23 décembre 1982, article 6-1 du décret n° 83-517 du 24 juin 1983.

### b. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

La CMA du lieu dans lequel le ressortissant souhaite réaliser la prestation est compétente pour délivrer la déclaration préalable d'activité.

#### Pièces justificatives

La demande de déclaration préalable d'activité est accompagnée d'un dossier complet comprenant les pièces justificatives suivantes :

- une photocopie d'une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE ;
- un document justifiant la qualification professionnelle du ressortissant qui peut être, au choix :
  - une copie d'un diplôme, titre ou certificat,
  - une attestation de compétence,
  - tout document attestant de l'expérience professionnelle du ressortissant.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

**À noter**

Lorsque le dossier est incomplet, la CMA dispose d'un délai de quinze jours pour en informer le ressortissant et demander l'ensemble des pièces manquantes.

#### Issue de la procédure

À réception de l'ensemble des pièces du dossier, la CMA dispose d'un délai d'un mois pour décider :

- soit d'autoriser la prestation lorsque le ressortissant justifie d'une expérience professionnelle de trois ans dans un État de l'UE ou de l'EEE, et de joindre à cette décision une attestation de qualification professionnelle ;
- soit d'autoriser la prestation lorsque les qualifications professionnelles du ressortissant sont jugées suffisantes ;
- soit de lui imposer une épreuve d'aptitude lorsqu'il existe des différences substantielles entre les qualifications professionnelles du ressortissant et celles exigées en France. En cas de refus d'accomplir cette mesure de compensation ou en cas d'échec dans son exécution, le ressortissant ne pourra pas effectuer la prestation de services en France.

Le silence gardé de l'autorité compétente dans ce délai vaut autorisation de débuter la prestation de services.

*Pour aller plus loin* : article 2 du décret du 2 avril 1998 ; article 2 de l'arrêté du 17 octobre 2017 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre 1er du décret n° 98-247 du 2 avril 1998.

### c. Le cas échéant, demander une attestation de reconnaissance de qualification professionnelle

L’intéressé souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de reconnaissance de qualification professionnelle.

#### Autorité compétente

La demande doit être adressée à la CMA territorialement compétente.

#### Procédure

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les quinze jours suivant le dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

#### Pièces justificatives

Le dossier doit contenir les pièces suivantes :

- la demande d’attestation de qualification professionnelle ;
- une attestation de compétence ou le diplôme ou titre de formation professionnelle ;
- la preuve de la nationalité du demandeur ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE, une attestation portant sur la nature et la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France, les justificatifs de l’exercice de l’activité pendant trois années.

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou son expérience professionnelle à l'intéressé pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du Centre international d’études pédagogiques (CIEP) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme ou d’un certificat ou d’un titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français.

#### Délai de réponse

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut :

- reconnaître la qualification professionnelle et délivrer l’attestation de qualification professionnelle ;
- décider de soumettre le demandeur à une mesure de compensation et lui notifier cette décision ;
- refuser de délivrer l’attestation de qualification professionnelle.

En l’absence de décision dans le délai de quatre mois, la demande d’attestation de qualification professionnelle est réputée acquise.

#### Voies de recours

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier, dans les deux mois suivant la notification du refus de la CMA, un recours contentieux devant le tribunal administratif compétent. De même, si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours gracieux auprès du préfet du département dans lequel la CMA a son siège dans les deux mois suivant la notification de la décision de la CMA. S’il n’obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.

### d. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).