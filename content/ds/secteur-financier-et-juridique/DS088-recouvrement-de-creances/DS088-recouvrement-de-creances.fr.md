﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS088" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Secteur financier et juridique" -->
<!-- var(title)="Recouvrement de créances" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="secteur-financier-et-juridique" -->
<!-- var(title-short)="recouvrement-de-creances" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/secteur-financier-et-juridique/recouvrement-de-creances.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="recouvrement-de-creances" -->
<!-- var(translation)="None" -->

# Recouvrement de créances

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le recouvrement de créances intervient lorsqu'un professionnel agit pour le compte d'un créancier auprès d'un débiteur qui n'aurait pas payé, à la date d'exigibilité, la ou les sommes qu'il doit.

Il relancera autant de fois que possible le débiteur au moyen de lettres de recouvrement, de relances téléphoniques ou, le cas échéant, de visites au domicile du débiteur avant d'éventuelles mises en demeure. Une fois les sommes dues par le débiteur récupérées, le professionnel les reversera directement au créancier.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- en cas d’activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour les professions libérales, le CFE compétent est l’Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le professionnel qui est amené à fournir une consultation juridique ou à rédiger des actes sous seing privé doit être titulaire d’une licence de droit (bac +3) ou, à défaut, de justifier d'une compétence juridique appropriée à la consultation et à la rédaction d'actes en matière juridique.

*Pour aller plus loin* : article 54 de la loi n° 71-1130 du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un État membre de l’Union européenne (UE) ou partie de l’Espace économique européen (EEE) souhaitant exercer dans le recouvrement de créances en France à titre temporaire et occasionnel ou à titre permanent.

Dès lors, seules les mesures prises pour le ressortissant français trouveront à s'appliquer (cf. infra « 3°. Démarches et formalités d’installation »).

### c. Conditions d'honorabilité et incompatibilités

Le professionnel en charge du recouvrement des créances est tenu au secret professionnel des informations qu'il serait amené à connaître de chacune des parties.

Le professionnel chargé du recouvrement des créances doit respecter certaines formalités et notamment celles visées ci-après (cf. infra « 2°. d. Quelques particularités de la réglementation de l'activité »).

En cas de manquement à ces obligations, il se verrait puni d'une amende de 1 500 euros, et jusqu'à 3 000 euros en cas de récidive.

*Pour aller plus loin* : article R. 124-7 du Code des procédures civiles d'exécution.

### d. Quelques particularités de la réglementation de l'activité

#### Obligation de souscrire une assurance de responsabilité civile professionnelle

Le professionnel chargé du recouvrement de créances est tenu de souscrire une assurance de responsabilité civile professionnelle le couvrant pour les actes effectués à l’occasion de son activité professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. Dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance.

*Pour aller plus loin* : article R. 124-2 du Code des procédures civiles d'exécution.

#### Obligation d'ouvrir un compte réservé à la réception des fonds

Le professionnel est tenu d'ouvrir un compte dans un établissement de crédit qui sera affecté uniquement à la réception des fonds provenant du recouvrement des créances.

Avant tout exercice de l'activité, le professionnel devra transmettre une attestation justifiant de l'ouverture de ce compte au procureur de la République du tribunal de grande instance du lieu d'installation.

*Pour aller plus loin* : article R. 124-2 du Code des procédures civiles d'exécution.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l'entreprise.

### b. Effectuer une déclaration d'activité de recouvrement de créances

#### Autorité compétente

Le professionnel qui souhaite exercer une activité de recouvrement de créances doit au préalable déclarer cette activité auprès du procureur de la République du tribunal de grande instance du lieu d'installation.

#### Pièces justificatives

La déclaration est effectuée par le dépôt d'un dossier comportant les pièces suivantes :

- une attestation d'assurance de responsabilité civile professionnelle ;
- un document justifiant de l'ouverture d'un compte bancaire affecté aux fonds perçus dans le recouvrement des créances ;
- le cas échéant, une copie des statuts de la société ;
- un extrait Kbis de la société.

#### Procédure

Le procureur de la République accusera réception du dossier et adressera un récépissé à l'intéressé avec un numéro d'enregistrement.

#### Coût

Gratuit.

### c. Autorisation(s) post-immatriculation

#### Obligation de conclure une convention avec le créancier

Le professionnel a l'obligation de conclure une convention dans laquelle il lui est donné pouvoir de recevoir des fonds pour le compte du créancier.

La convention doit comporter l'ensemble des informations suivantes :

- le fondement et le montant des sommes dues, avec l'indication distincte des différents éléments de la ou des créances à recouvrer sur le débiteur ;
- les conditions et les modalités de la garantie donnée au créancier contre les conséquences pécuniaires de la responsabilité civile encourue en raison de l'activité de recouvrement des créances ;
- les conditions de détermination de la rémunération à la charge du créancier ;
- les conditions de reversement des fonds encaissés pour le compte du créancier.

**À savoir**

Le professionnel a l'obligation d'informer le créancier de toutes les sommes qu'il percevrait du débiteur ou de toute proposition faite par le débiteur pour s'acquitter de la dette par un autre moyen que le paiement immédiat.

Une fois que les fonds ont été récupérés et encaissés, le professionnel chargé du recouvrement des créances devra les reverser dans un délai d'un mois au créancier.

*Pour aller plus loin* : articles R. 124-3, R. 124-5 et R. 124-6 du Code des procédures civiles d'exécution.

#### Obligation d'informer le débiteur de l'existence d'une procédure de recouvrement de créances

Le professionnel doit informer le débiteur qu'une procédure de recouvrement de créances court à son encontre. Il lui adressera une lettre comprenant les mentions suivantes :

- son nom ou sa dénomination sociale, son adresse ou son siège social, l'indication qu'il exerce une activité de recouvrement amiable ;
- le nom ou la dénomination sociale du créancier, son adresse ou son siège social ;
- le fondement et le montant de la somme due en principal, intérêts et autres accessoires, en distinguant les différents éléments de la dette, à l'exclusion des frais qui restent à la charge du créancier ;
- l'indication d'avoir à payer la somme due et les modalités de paiement de la dette ;
- la reproduction des troisième et quatrième alinéas de l'article L. 118-1 du Code des procédures civiles d'exécution : « *Les frais de recouvrement entrepris sans titre exécutoire restent à la charge du créancier, sauf s'ils concernent un acte dont l'accomplissement est prescrit par la loi au créancier. Toute stipulation contraire est réputée non écrite, sauf disposition législative contraire.* »

À réception des fonds par le débiteur, le professionnel lui remettra une quittance justifiant le règlement du ou des sommes dues.

*Pour aller plus loin* : article R. 124-4 du Code des procédures civiles d'exécution.