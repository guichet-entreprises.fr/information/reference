﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS088" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Financial and legal sector" -->
<!-- var(title)="Debt recovery" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="financial-and-legal-sector" -->
<!-- var(title-short)="debt-recovery" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/financial-and-legal-sector/debt-recovery.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="debt-recovery" -->
<!-- var(translation)="Auto" -->


Debt recovery
=============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Debt collection occurs when a professional acts on behalf of a creditor with a debtor who has not paid, on the due date, the amount or amounts he owes.

It will revive the debtor as many times as possible through collection letters, telephone raises or, if necessary, visits to the debtor's home before possible notices. Once the sums due by the debtor have been recovered, the professional will return them directly to the creditor.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- In case of artisanal activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for the liberal professions, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional who is required to provide legal advice or to draft acts under private seing must hold a law degree (B.A. 3) or, failing that, to justify appropriate legal jurisdiction to consult and draft deeds. legal matters.

*To go further* Article 54 of Law 71-1130 of 31 December 1971 on the reform of certain judicial and legal professions.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

There are no provisions for a national of a Member State of the European Union (EU) or part of the European Economic Area (EEA) wishing to exercise in the collection of debts in France on a temporary and casual basis or at permanent title.

Therefore, only the measures taken for the French national will find to apply (see infra "3°. Installation procedures and formalities").

### c. Conditions of honorability and incompatibility

The professional in charge of debt collection is bound by the professional secrecy of the information he would have to know from each of the parties.

The professional in charge of debt collection must respect certain formalities, including those referred to below (see below "2. d. Some peculiarities of the regulation of the activity").

If he breaches these obligations, he would be fined 1,500 euros, and up to 3,000 euros in the event of a repeat offence.

*To go further* Article R. 124-7 of the Code of Civil Enforcement Procedures.

### d. Some peculiarities of the regulation of the activity

#### Obligation to take out professional liability insurance

The professional in charge of debt collection is required to take out professional liability insurance covering him for acts carried out during his professional activity.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees.

*To go further* Article R. 124-2 of the Code of Civil Enforcement Procedures.

#### Obligation to open an account reserved for receiving funds

The professional is required to open an account in a credit institution that will be used only to receive funds from debt collection.

Before any exercise of the activity, the professional will have to pass on a certificate justifying the opening of this account to the prosecutor of the Republic of the high court of the place of installation.

*To go further* Article R. 124-2 of the Code of Civil Enforcement Procedures.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the "Formality of Reporting of a Commercial Company" and "Registration of a Commercial Individual Company at the RCS" cards.

### b. Make a declaration of debt collection activity

**Competent authority**

The professional who wishes to carry out a debt collection activity must first declare this activity with the prosecutor of the Republic of the high court of the place of installation.

**Supporting documents**

The declaration is made by filing a file with the following documents:

- a certificate of professional liability insurance;
- A document justifying the opening of a bank account assigned to funds collected in debt collection;
- If so, a copy of the company's statutes
- a Kbis extract from the company.

**Procedure**

The public prosecutor will acknowledge receipt of the file and send a receipt to the person concerned with a registration number.

**Cost**

Free.

### c. Post-registration authorisation

#### Obligation to enter into an agreement with the creditor

The professional has an obligation to enter into an agreement in which he is given the power to receive funds on behalf of the creditor.

The convention must include all the following information:

- the basis and amount of the sums due, with the separate indication of the various elements of the receivable or receivables to be recovered on the debtor;
- the terms and conditions of the guarantee given to the creditor against the monetary consequences of the civil liability incurred as a result of the debt collection activity;
- The conditions for determining the remuneration paid by the creditor;
- conditions for the remittance of funds received on behalf of the creditor.

**What to know**

The professional has an obligation to inform the creditor of all the sums he would receive from the debtor or of any proposal made by the debtor to pay the debt by any means other than the immediate payment.

Once the funds have been recovered and cashed in, the debt collection professional will have to return them within one month to the creditor.

*To go further* Articles R. 124-3, R. 124-5 and R. 124-6 of the Code of Civil Enforcement Procedures.

#### Obligation to inform the debtor of the existence of a debt collection procedure

The professional must inform the debtor that a debt collection procedure is against him. He will send her a letter with the following words:

- his name or name, address or head office, indication that he is engaged in an amicable collection activity;
- The creditor's name or name, address or head office;
- the basis and amount of the sum owed in principal, interest and other accessories, distinguishing the different elements of the debt, excluding the costs that remain at the creditor's expense;
- the indication of having to pay the amount owed and the terms of payment of the debt;
- reproduction of the third and fourth paragraphs of Article L. 118-1 of the Code of Civil Enforcement Procedures: " *Recovery costs undertaken without an enforceable title remain at the creditor's expense, unless they relate to an act which is required by law to the creditor. Any stipulation to the contrary is deemed unwritten, unless there is any other statutory provision to the contrary.* »
Upon receipt of the funds by the debtor, the professional will give him a discharge justifying the payment of the amounts earned.

*To go further* Article R. 124-4 of the Code of Civil Enforcement Procedures.

