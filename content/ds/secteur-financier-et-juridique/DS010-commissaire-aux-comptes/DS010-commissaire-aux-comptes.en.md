﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS010" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Financial and legal sector" -->
<!-- var(title)="Statutory auditor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="financial-and-legal-sector" -->
<!-- var(title-short)="statutory-auditor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/financial-and-legal-sector/statutory-auditor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="statutory-auditor" -->
<!-- var(translation)="Auto" -->


Statutory auditor
=======

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The Auditor ("CAC") is a professional whose main mission is to certify that the annual accounts are regular and sincere and give an accurate picture of the results of the operations of the past year as well as the financial situation and company's assets at the end of this fiscal year. He intervenes in an entity to which he has been appointed voluntarily or obligatory. During his mission, he will be required to disclose to the public prosecutor any wrongdoing he may have known about.

The ACC is also required to report on major events in the life of the company such as a capital increase, a transformation or the dividend payment.

The ACC is in principle appointed for six exercises. Under certain conditions, such as in a group or in the case of voluntary certification of a company located below the thresholds of the small business, it can be designated for three exercises. Provisions limit the possibility of renewing the mandate of the ACC in some sectors and impose a rotation of signatories (EIP and subsidiaries of EIP, non-commercial private legal entities with economic activity and exceeding certain associations receiving grants above a threshold: see articles L. 822-14 and L.823-3-1 of the Code of Commerce).

### b. competent CFE

The relevant business formalities centre (CFE) differs depending on the legal form chosen by the person concerned to settle:

- If the activity is carried out in an individual company, Urssaf is competent;
- If the activity is carried out in the form of a trading company, the Chamber of Commerce and Industry (CCI) is competent;
- if the activity is carried out in the form of a civil society or a liberal practising company, the registry of the Commercial Court, or the registry of the district court in the departments of Lower Rhine, Upper Rhine and Moselle, is competent.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To practice its profession, the ACC must be included on the list drawn up by the High Council of the Office of the Commissioner of Auditors.

To do so, he will have to meet the following conditions:

- be a French national or be a national of a Member State of the European Union (EU) or a party to the agreement on the European Economic Area (EEA) or another foreign state when it admits French nationals to exercise legal control of the Accounts
- not having been criminally convicted of acts contrary to honour or probity, or having received disciplinary sanction leading to a disbarment for similar acts;
- not to have been subjected to personal bankruptcy or any of the prohibition or forfeiture measures in Book VI of the Code of Commerce.

Registration on the ACC list is open to the professional who justifies:

- or hold the Certificate of Ability to Act as An Auditor (CAFCAC);
- or hold the Diploma of Accounting (DEC);
- either to have passed an aptitude test for the national approved by the authorities of his EU state or the EEA to exercise legal control of the accounts or, although not approved, meeting the conditions of title, diploma and practical training to obtain such accreditation in accordance with the provisions of the 2006/43/EC Directive;
- or have passed an aptitude test for the national of a non-EU state admitting French nationals to exercise legal control of the accounts, holding a diploma or title of the same level as the CAFCAC or the DEC and justifying an experience three-year professional in the field of legal auditing of accounts,
- have completed a professional internship of a period set by regulation with an auditor or a person approved by a Member State of the European Union to exercise legal control of accounts.

To attend the CAFCAC, he must be the holder:

- a master's degree and have passed the preparatory certificate tests for CAC functions whose program, such as that of the CAFCAC, is specified in a[decree of March 5, 2013](https://www.legifrance.gouv.fr/affichTexte.docidTexte=JORFTEXT000027143986&categorieLien=id) ;
- or a graduate degree in accounting studies governed by Decree 81-537 of May 12, 1981 relating to the graduate degree in accounting or the graduate degree of accounting and financial, or having validated at least four of the seven tests required to obtain a graduate degree in accounting and management (DSCG);
- have a degree deemed equivalent to the graduate degree in accounting or the graduate degree in accounting and financial studies.

In addition, it is imperative that he justify holding a certificate of completion of a professional internship, the duration of the latter must be three years. A full or partial exemption may be granted by decision of the Seal Guard, Minister of Justice, if the candidate justifies at least fifteen years of experience in the financial, accounting and legal field (see Articles R.822-3 and following of the Code of Commerce for the content of this internship and the exemption).

In order to attend the DEC, the person concerned will have to justify:

- Hold the DSCG or Graduate Accounting Diploma (DECS);
- have completed an internship that is in principle three years, in an accounting firm or in similar structures.

*For further information*: Articles 63 and following of Decree No. 2012-432 of March 30, 2012.

The CAFCAC includes an eligibility test and an admission test, the terms and conditions of which are provided for at the[Article A. 822-1](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=58CC1389C7F69724803027999EB5D10B.tplgfr33s_1idArticle=LEGIARTI000027146372&cidTexte=LEGITEXT000005634379&dateTexte=20171207) Code of Commerce.

*For further information*: Articles L. 822-1, L. 822-1-1, R. 822-2 and the following of the Code of Commerce; decree of 5 March 2013 setting out the programme of the certificate of fitness for the functions of auditor and the certificate preparatory to the functions of auditor.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

**In case of Freedom to provide services**

There are no regulations for a national of a Member State of the European Union (EU) or part of the European Economic Area (EEA) wishing to practise as a CAC in France on a temporary and casual basis.

Therefore, only the measures taken for the Freedom of establishment of EU or EEA nationals will apply.

**In case of Freedom of establishment**

A national of an EU or EEA state who legally operates as a CAC in that state may settle in France to carry out the same activity on a permanent basis.

He will have to apply for registration on the LIST of CAC with the High Council of the Office of the Commissioner of Auditors, subject to having passed a test of aptitude beforehand.

*For further information*: Articles L. 822-1-2 and R. 822-6 of the Code of Commerce.

### c. Conditions of honorability and incompatibility

A set of ethical rules must be followed, including:

- The professional secrecy of the data it processes;
- Integrity, including the justification for not being the perpetrator of incriminating or disciplinary acts;
- independence and impartiality, in order to avoid any conflict of interest between its supervisory and advisory mission and any situation of self-revision;
- exercise incompatibility with:- an activity or act that infringes on its independence,
  - an employee's job. However, an auditor may provide instruction related to the practice of his profession or hold a paid job with an auditor or an accountant
  - a commercial activity, with the exception, on the one hand, of business activities incidental to the profession of accountant, and, on the other hand, of ancillary business activities carried out by the multi-professional practising company

**Please note**

Criminal sanctions may be applied against the ACC which would operate without being on the list of the High Council of the Auditor or would not comply with the rules of incompatibility referred to above. In both cases, he faces a one-year prison sentence and a fine of 15,000 euros.

Criminal penalties are also provided in the situations set out in Articles L.820-6 and L. 820-7 of the Code of Commerce, particularly when the professional refrains from disclosing the criminal facts that he is aware of from time to time.

*For further information*: Articles L. 820-5, L. 820-7, L. 822-10 and following, Articles R. 822-20 and beyond, and Appendix 8-1 of the Code of Commerce.

### d. Some peculiarities of the regulation of the activity

**Obligation to take out liability insurance** Professional**

The Liberal ACC has an obligation to purchase professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

In any event, the ACC must provide a financial guarantee of more than 76,224.51 euros per year per claim.

*For further information*: Articles L. 822-17, R. 822-36 and A. 822-31 of the Code of Commerce.

**Obligation to undergo continuing vocational training**

In order to maintain and improve their knowledge in the areas of accounting, finance and law, ACCs must undergo a continuous professional training course of one hundred and twenty hours over three consecutive years. At least 20 hours are completed in a single year (see A. 822-28-2).

*For further information*: Articles L. 822-4, R. 822-22, A. 822-28-1 and the following Code of Commerce.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the "Formality of Reporting of a Commercial Company," "Individual Business Registration in the Register of Trade and Companies" and "Corporate Reporting Formalities. artisanal work."

### b. Ask for inclusion on the CAC list

**Pass a pre-suitability test for the EU or EEA or other foreign state national when the latter admits French nationals to exercise legal control of the accounts**

In this case, the national will have to request it by file forwarded to the Minister responsible for justice. The file must include the following supporting documents translated into French by a certified translator:

- A valid piece of identification
- any diploma, certificate or title that attests to the national's professional qualifications;
- any certificate of internship or training to assess the content and level of post-secondary education.

Upon receipt of the file, the Minister of Justice will issue a receipt to the national and will have four months to decide on the application for the aptitude test. The silence kept within this time will be worth accepting of it.

Once allowed to take the test, the national must pass a written and oral exam in the French language, with an average score of 10 or higher.

**Mandatory registration on the CAC list**

The person concerned (French or national) wishing to practise in the profession of CAC must apply for registration on a list maintained by the High Council of the Office of the Commissioner of Auditors.

**Competent authority**

The application for final inclusion on the list of CAC is made with the High Council of the Office of the Commissioner of Auditors.

**Supporting documents**

The application can be made by post or directly online on the[Site](https://www.cncc.fr/liste-cac.html) National Company of Auditors. In addition to his or her reasoned request, the professional will be required to submit all of the following supporting information:

- A valid piece of identification
- A resume
- a copy of one of the diplomas referred to in paragraph "2." a. Professional qualifications";
- Certificate of completion of internship;
- If so, a certificate of success in the qualification test for the national;
- a certificate of non-incompatibility with the practice of the profession;
- a certificate of honour justifying that he is not subject to personal bankruptcy or the perpetrator of facts that have resulted in a criminal conviction.

**Outcome of the procedure**

Upon receipt of the coins, the High Council will issue a receipt to the person concerned. If, within four months of receipt, the competent authority has not decided on the application, registration is deemed to be granted.

He will thus be able to legally exercise the function of CAC after being sworn in before the first president of the territorially competent Court of Appeal.

*For further information*: Articles L. 822-1, L. 822-1-1, L.822-1-2, R.822-9, R. 822-12 of the Code of Commerce.

**If necessary, the company's inclusion on the list of the High Council**

Provided that the statutes of the CAC company have been filed with the registry of the territorially competent Commercial Court, its legal representative must apply for the company to be included in the list of ACCs held by the High Council.

In this case, the application file must include all of the following supporting documents:

- A copy of the statutes
- A request from each partner asks for registration;
- A list of shareholders or partners including their civil registration information, the number of voting rights in the company and the proof of their application for listing on the LIST of CSCs;
- a certificate of the filing of the company's statutes to the registry of the competent commercial court.

The registration of the CAC company will be done under the same procedural and time-limits as those applicable to the ACC which exercises as an individual.

