﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS010" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Secteur financier et juridique" -->
<!-- var(title)="Commissaire aux comptes" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="secteur-financier-et-juridique" -->
<!-- var(title-short)="commissaire-aux-comptes" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/secteur-financier-et-juridique/commissaire-aux-comptes.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="commissaire-aux-comptes" -->
<!-- var(translation)="None" -->

# Commissaire aux comptes

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le commissaire aux comptes (« CAC ») est un professionnel dont la mission principale est de certifier que les comptes annuels sont réguliers et sincères et donnent une image fidèle du résultat des opérations de l'exercice écoulé ainsi que de la situation financière et du patrimoine de la société à la fin de cet exercice. Il intervient dans une entité auprès de laquelle il a été nommé volontairement ou obligatoirement. Au cours de sa mission, il sera tenu de révéler au procureur de la République tous faits délictueux dont il aurait eu connaissance.

Le CAC est par ailleurs amené à établir des rapports en cas d'événements majeurs dans la vie de l'entreprise tels qu'une augmentation de capital, une transformation ou encore le paiement de dividende.

Le CAC est en principe nommé pour six exercices. Dans certaines conditions, notamment dans un groupe ou en cas de certification volontaire d’une société située sous les seuils de la petite entreprise, il peut être désigné pour trois exercices. Des dispositions limitent dans certains secteurs la possibilité de renouveler le mandat du CAC et imposent une rotation des signataires (EIP et filiales d'EIP, personnes morales de droit privé non commerçantes ayant une activité économique et dépassant certains seuils, associations recevant des subventions dépassant un seuil : voir les articles L. 822-14 et L.823-3-1 du Code de commerce).

### b. CFE compétent

Le centre des formalités de l’entreprise (CFE) compétent diffère selon la forme juridique choisie par l’intéressé pour s’installer :

- si l'activité est exercée en entreprise individuelle, l'Urssaf est compétente ;
- si l’activité est exercée sous la forme d’une société commerciale, la chambre de commerce et d’industrie (CCI) est compétente ;
- si l’activité est exercée sous la forme d’une société civile ou d'une société d'exercice libéral, le greffe du tribunal de commerce, ou le greffe du tribunal d’instance dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle, est compétent.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer sa profession, le CAC doit être inscrit sur la liste établie par le Haut Conseil du commissariat aux comptes.

Pour cela, il devra remplir les conditions suivantes :

- être de nationalité française ou être ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) ou d'un autre Etat étranger lorsque celui-ci admet les nationaux français à exercer le contrôle légal des comptes ;
- ne pas avoir été condamné pénalement pour des faits contraires à l'honneur ou à la probité ni n'avoir reçu de sanction disciplinaire menant à une radiation pour des faits de même nature ;
- ne pas avoir été frappé de faillite personnelle ou de l'une des mesures d'interdiction ou de déchéance prévues au livre VI du Code de commerce.

L'inscription sur la liste des CAC est ouverte au professionnel qui justifie :

- soit être titulaire du certificat d'aptitude aux fonctions de commissaire aux comptes (CAFCAC) ;
- soit être titulaire du diplôme d'expertise comptable (DEC) ;
- soit avoir réussi une épreuve d'aptitude pour le ressortissant agréé par les autorités de son État de l'UE ou de l'EEE à exercer le contrôle légal des comptes ou, bien que non agréé, réunissant les conditions de titre, de diplôme et de formation pratique permettant d'obtenir un tel agrément conformément aux dispositions de la directive 2006/43/CE ;
- soit avoir réussi une épreuve d'aptitude pour le ressortissant d'un Etat non membre de l'UE admettant les nationaux français à exercer le contrôle légal des comptes, titulaire d'un diplôme ou titre de même niveau que le CAFCAC ou le DEC et justifiant d'une expérience professionnelle de trois ans dans le domaine du contrôle légal des comptes,
- avoir accompli un stage professionnel d'une durée fixée par voie réglementaire chez un commissaire aux comptes ou une personne agréée par un État membre de l'Union européenne pour exercer le contrôle légal des comptes.

Pour se présenter au CAFCAC, il devra être titulaire :

- soit d'un diplôme de niveau master et avoir réussi les épreuves du certificat préparatoire aux fonctions de CAC dont le programme, comme celui du CAFCAC, est précisé dans un arrêté du 5 mars 2013 ;
- soit d'un diplôme d'études comptables supérieures régi par le décret n° 81-537 du 12 mai 1981 relatif au diplôme d'études comptables supérieures ou du diplôme d'études supérieures comptables et financières ou avoir validé au moins quatre des sept épreuves obligatoires permettant d'obtenir le diplôme supérieur de comptabilité et gestion (DSCG) ;
- être titulaire d’un diplôme jugé de niveau équivalent au diplôme d’études comptables supérieures ou au diplôme d’études supérieures comptables et financières.

En outre, il devra impérativement justifier être titulaire d'une attestation de fin de stage professionnel, la durée de ce dernier devant être de trois ans. Une dispense totale ou partielle peut être octroyée par décision du garde des Sceaux, ministre chargé de la justice, si le candidat justifie d'une expérience d'au moins quinze ans dans le domaine financier, comptable et juridique (cf. articles R.822-3 et suivants du Code de commerce pour le contenu de ce stage et la dispense).

Pour se présenter au DEC, l'intéressé devra justifier :

- être titulaire du DSCG ou du diplôme d'études comptables supérieures (DECS) ;
- avoir effectué un stage dont la durée est en principe de trois ans, en cabinet d'expertise comptable ou dans des structures assimilées.

*Pour aller plus loin* : articles 63 et suivants du décret n° 2012-432 du 30 mars 2012.

Le CAFCAC comprend une épreuve d'admissibilité et une épreuve d'admission dont les modalités sont prévues à l'article A. 822-1 du Code de commerce.

*Pour aller plus loin* : articles L. 822-1, L. 822-1-1, R. 822-2 et suivants du Code de commerce ; arrêté du 5 mars 2013 fixant le programme du certificat d'aptitude aux fonctions de commissaire aux comptes et du certificat préparatoire aux fonctions de commissaire aux comptes.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### En cas de Libre Prestation de Services (LPS)

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un État membre de l’Union européenne (UE) ou partie de l’Espace économique européen (EEE) souhaitant exercer la profession de CAC en France à titre temporaire et occasionnel.

Dès lors, seules les mesures prises pour le libre établissement des ressortissants de l'UE ou de l'EEE trouveront à s'appliquer.

#### En cas de Libre Établissement (LE)

Le ressortissant d'un État de l'UE ou de l'EEE qui exerce légalement l'activité de CAC dans cet État peut s'établir en France pour y exercer cette même activité de manière permanente.

Il devra demander son inscription sur la liste des CAC auprès du Haut Conseil du commissariat aux comptes, sous réserve d'avoir réussi au préalable une épreuve d'aptitude.

*Pour aller plus loin* : articles L. 822-1-2 et R. 822-6 du Code de commerce.

### c. Conditions d'honorabilité et incompatibilités

Un ensemble de règles déontologiques est à respecter, et notamment :

- le secret professionnel des données qu'il traite ;
- l'intégrité, en justifiant notamment ne pas être l'auteur de faits incriminants ou donnant lieu à une sanction disciplinaire ;
- l'indépendance et l'impartialité, afin d'éviter tout conflit d'intérêts entre sa mission de contrôle et celle de conseil et toute situation d'auto-révision ;
- l'incompatibilité d'exercice avec :
  - une activité ou un acte portant atteinte à son indépendance,
  - un emploi de salarié. Toutefois, un commissaire aux comptes peut dispenser un enseignement se rattachant à l'exercice de sa profession ou occuper un emploi rémunéré chez un commissaire aux comptes ou chez un expert-comptable
  - une activité commerciale, à l'exception, d'une part, des activités commerciales accessoires à la profession d'expert-comptable, et, d'autre part, des activités commerciales accessoires exercées par la société pluri-professionnelle d'exercice

**À noter**

Des sanctions pénales peuvent être appliquées à l'encontre du CAC qui exercerait son activité sans être inscrit sur la liste du Haut Conseil du commissaire aux comptes ou ne respecterait pas les règles d'incompatibilité visées ci-dessus. Dans ces deux cas, il encourt une peine d'un an de prison et 15 000 euros d'amende.

Des sanctions pénales sont également prévues dans les situations énoncées aux articles L.820-6 et L. 820-7 du Code de commerce, notamment lorsque le professionnel s'abstient de révéler les faits délictueux dont il a connaissance à l'occasion.

*Pour aller plus loin* : articles L. 820-5, L. 820-7, L. 822-10 et suivants, articles R. 822-20 et suivants, et annexe 8-1 du Code de commerce.

### d. Quelques particularités de la réglementation de l'activité

#### Obligation de souscrire une assurance de responsabilité civile professionnelle

Le CAC exerçant à titre libéral a l'obligation de souscrire une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans ce cas, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

En tout état de cause, le CAC devra impérativement présenter une garantie financière supérieure à 76 224,51 € par année et par sinistre.

*Pour aller plus loin* : articles L. 822-17, R. 822-36 et A. 822-31 du Code de commerce.

#### Obligation de suivre une formation professionnelle continue

Afin d'entretenir et de perfectionner leurs connaissances dans les domaines liés à la comptabilité, la finance et le droit, les CAC doivent suivre une formation professionnelle continue de cent vingt heures au cours de trois années consécutives. Vingt heures au moins sont accomplies au cours d'une même année (voir A. 822-28-2).

*Pour aller plus loin* : articles L. 822-4, R. 822-22, A. 822-28-1 et suivants du Code de commerce.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Demander son inscription sur la liste des CAC

#### Réussir une épreuve préalable d'aptitude pour le ressortissant de l'UE ou de l'EEE ou d'un autre État étranger lorsque celui-ci admet les nationaux français à exercer le contrôle légal des comptes

Dans ce cas, le ressortissant devra en faire la demande par dossier transmis au ministre chargé de la justice. Le dossier doit comporter les pièces justificatives suivantes traduites en français par un traducteur agréé :

- une pièce d'identité en cours de validité ;
- tout diplôme, certificat ou titre qui attestent des qualifications professionnelles du ressortissant ;
- toute attestation de stage ou de formation suivie permettant d'apprécier le contenu et le niveau d'études postsecondaire.

À réception du dossier, le ministre chargé de la justice délivrera un récépissé au ressortissant et disposera d'un délai de quatre mois pour se prononcer sur la demande d'inscription à l'épreuve d'aptitude. Le silence gardé dans ce délai vaudra acceptation de celle-ci.

Une fois autorisé à se présenter à l'épreuve, le ressortissant devra réussir avec succès un examen écrit et oral en langue française, en obtenant une note moyenne égale ou supérieure à 10.

#### Inscription obligatoire sur la liste des CAC

L'intéressé (français ou ressortissant) souhaitant exercer la profession de CAC doit demander son inscription sur une liste tenue par le Haut Conseil du commissariat aux comptes.

#### Autorité compétente

La demande d'inscription définitive sur la liste des CAC se fait auprès du Haut Conseil du commissariat aux comptes.

#### Pièces justificatives

La demande d'inscription peut se faire par voie postale ou directement en ligne sur le [site](https://www.cncc.fr/liste-cac.html) de la Compagnie nationale des commissaires aux comptes. Le professionnel devra transmettre, en plus de sa demande motivée, l'ensemble des éléments justificatifs suivants :

- une pièce d'identité en cours de validité ;
- un curriculum vitae ;
- une copie de l'un des diplômes visés au paragraphe « 2°. a. Qualifications professionnelles » ;
- l'attestation de fin de stage ;
- le cas échéant, une attestation de réussite à l'épreuve d'aptitude pour le ressortissant ;
- une attestation de non-incompatibilité avec l'exercice de la profession ;
- une attestation sur l'honneur justifiant qu'il n'est ni frappé de faillite personnelle ni l'auteur de faits ayant donné lieu à une condamnation pénale.

#### Issue de la procédure

À réception des pièces, le Haut Conseil délivrera un récépissé à l'intéressé. Si, dans un délai de quatre mois à compter de la réception de ce récépissé, l'autorité compétente ne s'est pas prononcée sur la demande, l'inscription est réputée accordée.

Il pourra ainsi exercer légalement la fonction de CAC après avoir prêté serment devant le premier président de la cour d'appel territorialement compétente.

*Pour aller plus loin* : articles L. 822-1, L. 822-1-1, L.822-1-2, R.822-9, R. 822-12 du Code de commerce.

#### Le cas échéant, inscription de la société sur la liste du Haut Conseil

Sous réserve que les statuts de la société de CAC aient été déposés au greffe du tribunal de commerce territorialement compétent, son représentant légal doit demander l'inscription de la société à la liste des CAC tenue par le Haut Conseil.

Dans ce cas, le dossier de demande d'inscription devra comporter l'ensemble des pièces justificatives suivantes :

- un exemplaire des statuts ;
- une requête de chaque associé demande l'inscription ;
- une liste des actionnaires ou des associés comprenant leurs informations d'état civil, le nombre de droits de vote dans la société ainsi que le justificatif de leur demande d'inscription à la liste des CAC ;
- une attestation du dépôt des statuts de la société au greffe du tribunal de commerce compétent.

L'inscription de la société de CAC se fera dans les mêmes conditions de procédure et de délai que celles applicables au CAC qui exerce à titre individuel.