﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS013" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Financial and legal sector" -->
<!-- var(title)="Certified accountant" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="financial-and-legal-sector" -->
<!-- var(title-short)="certified-accountant" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/financial-and-legal-sector/certified-accountant.html" -->
<!-- var(last-update)="2021-07" -->
<!-- var(url-name)="certified-accountant" -->
<!-- var(translation)="Auto" -->


Certified accountant
==========

Latest update: <!-- begin-var(last-update) -->July 2021<!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

The accountant is a professional whose mission is to review and appreciate the accounts, attest to the regularity and sincerity of the results accounts, and also hold, centralize, open, stop, monitor, straighten and consolidate accounting for companies and organizations.

Ancillary activities directly related to accounting work are also permitted for accounting professionals (consultations, carry out all studies and work of a statistical, economic, administrative nature, legal, social or fiscal and provide notice to any public or private authority or body that authorizes them) but without being able to be the main focus of their activity and only if they are companies in which missions are carried out accounting of a permanent or usual nature or to the extent that such consultations, studies, works or opinions are directly related to the accounting work for which they are responsible.

*To go further* Articles 2 and 22 of Ordinance 45-2138 of September 19, 1945, establishing the College of Chartered Accountants and regulating the title and profession of public accountant.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for a liberal profession, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

#### National legislation

The profession of accountant is reserved for anyone on the Order of Accountants' Table, which justifies:

- enjoying his civil rights;
- not having been sentenced to a criminal or correctional sentence;
- that it presents the guarantees of morality deemed necessary by the Council of the Order;
- adtively:- have a French degree in accounting (DEC),
  - or be forty years old and justify fifteen years of activity in the execution of accounting organizing or revision work, including at least 5 years in functions or missions involving the exercise of important responsibilities of order administrative, financial and accounting
  - or, if it is a national of a Member State of the European Union (EU) or a party to the Agreement on the European Economic Area (EEA), justify a certificate of competence or a training certificate recognised as equivalent and issued in such a state and, the if they have completed an aptitude test (see point b), or, if not a national, justify a recognized diploma equivalent to the Accounting Diploma (CED), and have successfully completed an aptitude test.

*To go further* Articles 3, 7*Bis*, 26 and 27 of Ordinance 45-2138 of September 19, 1945; Decree No. 2012-432 of March 30, 2012 relating to accounting.

When it comes to the exercise of a corporate accounting activity, professionals may constitute:

- Accounting companies: entities with legal personality, with the exception of legal forms that confer on their partners the status of a trader. Accountants must, directly or indirectly by a company registered in the Order, hold more than half the capital and more than two-thirds of the voting rights. No person or group of interests, outside the Order, shall hold, directly or by person interposed, a part of the capital or voting rights likely to jeopardize the exercise of the profession, the independence of the partners accountants or their compliance with the rules inherent in their status and ethics. The public offering of financial securities is only permitted for securities excluding access, even deferred or conditional, to capital. The managers, the chairman of the simplified company, the chairman of the board of directors or the members of the board of directors must be accountants, members of the company. The society that is a member of the Order communicates annually to the councils of the Order, from which it reports the list of its associates and any changes made to this list;
- Accounting holding companies: entities whose main purpose is the holding of securities of chartered accountants as well as participation in any grouping under foreign law having as its object the exercise of the profession of chartered accountant. These companies, listed on the order board, may have ancillary activities directly related to their object and intended exclusively for companies or groups in which they hold participations;
- Management and accounting associations: entities created on the initiative of territorial chambers of commerce and industry, chambers of trades or chambers of agriculture, or professional organizations of industrialists, traders, craftsmen, farmers or professionals. No association can be registered on the board if it has less than three hundred members at the time of the application. The directors and directors of these associations must justify having met their tax and social obligations;
- Accounting branches: individuals nationals of other EU Member States or other States parties to the European Economic Area Agreement as well as legal entities incorporated in accordance with legislation of one of these states, which has its statutory headquarters, headquarters or principal institution in one of these states, which legally practise the profession of accounting, is entitled to constitute, for the exercise of branches that do not have legal personality. Branches are not members of the College of Chartered Accountants. They are on the board. Their work is under the responsibility of an accountant practising within the branch and ordinal representative specifically appointed in this capacity to the regional council of the College of Public Accountants by those admitted to the constitute;
- Multi-professional practising societies: the aim of the joint practice of several of the professions of lawyer, lawyer in the Council of State and the Court of Cassation, judicial auctioneer, judicial officer, notary, judicial administrator, judicial agent, industrial property consultant, auditor and accountant. The multi-professional practice society must include at least one member of each of the professions it practises. The company can take any social form, except those that give their partners the status of a trader. It is governed by the rules specific to the chosen social form. Whatever form of social action chosen by the multi-professional practising society, and even when it has not been incorporated as a liberal exercise company, certain provisions of Title I of Act 90-1258 of 31 December 1990 are Applicable. All of the capital and voting rights are held by the following individuals:- (1) any natural person practising, within or outside society, one of the professions mentioned in Article 31-3 of Act 90-1258 of 31 December 1990 and practised jointly within society,
  - (2) any legal person whose entire capital and voting rights is directly or indirectly owned by one or more of the persons mentioned in the 1st,
  - (3) any individual or legal person, legally established in another Member State of the European Union or party to the agreement on the European Economic Area or in the Swiss Confederation, which actually carries out, in one of these states, an activity subject to a legislative or regulatory status or subordinate to the possession of a recognized national or international qualification, whose exercise in France falls under one of the professions mentioned in Article 31-3 and which is exercised jointly within society ; for legal entities, all the capital and voting rights are held under the conditions provided for in the 1st or 2nd;
- Financial holding companies of the liberal professions: incorporated between individuals or corporations practising one or more professional professions subject to legislative or regulatory status or whose title is protected or persons 6th of The 6th of Article 5 of Act 90-1258 of December 31, 1990, financial equity companies for the holding of shares or shares of companies mentioned in the first paragraph of Article 1 of Act 90-1258 of 31 December 1990 with the purpose of the exercise of the same profession as well as participation in any foreign law group with the purpose of the exercise of the same profession. These companies may engage in any other activity subject to being intended exclusively for the companies or groups of which they hold interests. These companies may be incorporated in the form of limited liability companies, limited partnerships, corporations, simplified shares or limited partnerships governed by Book II of the Code of Commerce. More than half of the capital and voting rights must be held by persons in the same profession as the companies held by the shares or shares. Managers, the president, the executives, the chairman of the board of directors, the members of the board of directors, the chairman of the supervisory board and the directors, as well as at least two-thirds of the members of the board of directors or the supervisory board of the simplified corporation, must be chosen from among those who are allowed to form these companies.

*To go further* Articles 7, 7*Ter*, 7*D* 45-2138 of September 19, 1945 and articles 31-1 to 31-2, and articles 31-3 and articles of Act 90-1258 of December 31, 1990 relating to the exercise in the form of professional corporations subject to legislative or regulatory status or protected and financial holding companies of the professions.

#### Training

To apply to the College as an accountant, the candidate, who does not use the procedures mentioned in Articles 7 *Bis*, 26 or 27 of Ordinance 45-2138 of September 19, 1945, must have successively validated the following diplomas except exemptions granted to holders of certain titles and diplomas:

- a bachelor's degree in accounting and management (DCG) (B.A.3);
- The graduate degree in accounting and management (DSCG) at the master's level (B.A. 5);
- the diploma of accounting (DEC) at the bac level 8, after a three-year internship in an accounting firm. For this diploma, the candidate must pass a written test on the legal and contractual revision of the accounts, an oral test before a jury, and support his thesis at the end of his internship.

These diplomas can also be obtained through the Experience Validation Procedure (VAE).

*To go further* Article 3 of Ordinance 45-2138 of September 19, 1945, Articles 45 and following of Decree No. 2012-432 of March 30, 2012 and decree of February 13, 2019 setting the provisions relating to obtaining the DEC by the VAE.

### Related costs

There is a fixed fee per degree and per event. In 2018, for the DCG, the price per event is set at 22 euros at the rate of 14 events. As for the DSCG, it is 30 euros per event at the rate of 8 events. As far as the DEC is concerned, it is 50 euros at the rate of 3 events. In addition, some courses preparing for these diplomas are paid for. For more information, it is advisable to get closer to the organizations that provide them.

The procedure for recognising the professional qualifications of EU or EEA nationals is free of charge.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

#### EU nationals: for temporary and casual exercise (LPS)

The profession of accountant or part of the accounting activities may be carried out in France on a temporary and occasional basis by any national of an EU Member State or party to the EEA agreement, subject to:

(1) to be legally established, on a permanent basis, in one of the states mentioned to carry out all or part of the activity of accountant;
(2) where this occupation or the training conducting it is not regulated in the State of establishment, to have further practiced this occupation in one or more of the States mentioned for at least one year in the ten years preceding the performance he intends to achieve in France.

The provision of accounting is carried out under the professional title of the State of Establishment where such a title exists in that state. This title is indicated in the official language of the State of Establishment. In cases where this professional title does not exist in the State of the establishment, the claimant mentions his diploma or training title in the official language of that state. The performance of this accounting benefit is subject to a written statement to the Higher Council of the College of Chartered Accountants prior to the first benefit. The written statement specifies insurance coverage or other means of personal or collective protection regarding the professional liability of this provider. The declaration provided for the purpose of carrying out the first benefit in France on a temporary and occasional basis is repeated in the event of a material change in the elements of the declaration and renewed each year if the claimant plans to carry out this activity during the year in question. Upon receipt of this declaration, the Higher Council of the College of Chartered Accountants sends a copy to the regional council of the College of Chartered Accountants in which the provision of accounting expertise must be carried out. Upon receipt of this transmission, the regional council registers the registrant for the year considered on the Order's board.

*To go further* Section 26-1 of Ordinance 45-2138 of September 19, 1945.

#### EU nationals: for a permanent exercise (LE)

Any national of an EU member state or party to the EEA agreement that meets one of two conditions may be included in the Order's table as an accountant, without holding a degree in accounting, any national of an EU Member State or party to the EEA agreement that meets one of two conditions:

- (1) to hold a certificate of competency or training qualifications covered by Article 11 of the 2005/36/CE amended directive of the European Parliament and the Council of 7 September 2005 relating to the recognition of professional qualifications allowing the practice of the profession in an EU Member State or party to the EEA agreement. This certificate or title is issued either by the competent authority of that state and punishes training acquired in the EU or the EEA, either by a third country, provided that a certificate from the competent authority of the EU Member State is provided or party to the EEA agreement which has recognised the diploma, certificate or other title, certifying that the holder has, in that state, at least three years of work full-time or of an equivalent period of time part-time during the ten Years. Certificates of competency or training documents are issued by the state of origin, designated in accordance with the legislative, regulatory or administrative provisions of that state;
- (2) having worked full-time as an accountant for a year or part-time for a total period equivalent to at least in the previous ten years in an EU Member State or a party to the EEA agreement which does not regulate the and who have one or more certificates of competency or proof of training credentials issued by another state that does not regulate this profession. These certificates of competency or training credentials meet the following conditions:


  - (a) be issued by the competent authority of the state mentioned in the first paragraph, designated in accordance with the legislative, regulatory or administrative provisions of that state,
  - b) certify the owner's preparation for the practice of the profession concerned.

However, the condition of a one-year work experience mentioned is not required when the applicant's title or training documents sanction regulated training directly geared towards the practice of the profession. Accounting.

Unless the knowledge, skills and skills acquired during one's work experience, either full-time or part-time, or lifelong learning, and which has been validated by a competent authority for this purpose in an EU Member State or party to the European EEA agreement or in a third country designated in accordance with the legislative, regulatory or administrative provisions of that state, are likely to render this verification unnecessary, the person concerned must be submit to an aptitude test:

- (1) where the training it justifies covers subjects substantially different from those on the French accounting diploma programme;
- (2) when the State in which it has obtained a certificate of competence or a training document covered by Article 11 of the 2005/36/CEamended by the European Parliament and the Council on 7 September 2005 relating to the recognition of the professional qualifications he uses or the state in which he practised the profession does not regulate this profession or regulates it in a way substantially different from French regulations.

*To go further* Article 26 of Ordinance 45-2138 of September 19, 1945 and Articles 97 to 99 and 103 of Decree No. 2012-432 of March 30, 2012.

Entering an application to this effect, whether for an establishment or for a temporary and occasional provision of services in France, the competent authority grants partial access to the activities of accounting when all conditions are filled:

- (1) the professional is fully qualified to carry out in an EU Member State or party to the EEA agreement the professional activity for which partial access is requested;
- (2) the differences between professional activity legally carried out in the EU Member State or party to the EEA agreement and the profession of accountant in France are so important that the application of compensation measures would amount to imposing applicant to follow the full education and training programme required in France to have full access to the profession in France;
- (3) the work requested can objectively be separated from other activities of the accounting profession in France, as it can be carried out autonomously in the Member State of origin.

Partial access may be denied if this refusal is justified on compelling grounds of general interest, if it is suitable to guarantee the achievement of the objective pursued and if it does not go beyond what is necessary to achieve this objective.

The professional activity is carried out under the professional title of the State of origin when partial access has been granted. Professionals who have partial access clearly indicate to the recipients of the services the scope of their professional activities.

The provisions of this I do not apply to professionals who receive automatic recognition of their professional qualifications in accordance with Articles 49*Bis* and 49*Ter* 2005/36/CEamended by the European Parliament and the Council of 7 September 2005 on the recognition of professional qualifications.

Applications for partial access to an institution are reviewed under the same procedure as applications under section 26 of Order 45-2138 of September 19, 1945.

Professionals who have been authorized to partially engage in the accounting activity are not members of the College of Chartered Accountants. They are placed on the Order's board according to the conditions set out in Section II of Ordinance 45-2138 of September 19, 1945. They are subject to the legislative and regulatory provisions relating to the profession of public accountant. They pay dues on the same basis and under the same conditions as the members of the Order.

*To go further* Article 26-0 of Ordinance 45-2138 of September 19, 1945 and Articles 97 to 99-1 and 103 of Decree No. 2012-432 of March 30, 2012.

### c. Condition of honorability

Accounting professionals must comply with the legislative and regulatory provisions governing their profession and the College's internal regulations, which are established by decision of the Higher Council. In all cases, accounting professionals take responsibility for their work and activities. The own liability of the Order's member companies and the management and accounting associations leaves the personal responsibility of each accountant or professional who has been authorized to partially carry out the activity accounting expertise because of the work it does on behalf of these companies, branches or associations. The work and activities must be accompanied by the personal signature of the accountant, the employee or professional who has been authorized to partially carry out the accounting activity as well as the visa or social signature. Members of the the Order, which, being partners or shareholders of a company recognized by it, carry out their activity in that company, as well as the members of the Order salaried by a colleague or company listed on the board, of a multi-professional company of practice, a branch or a management and accounting association, and professionals who have been authorized to partially engage in the accounting activity may carry out their duties or mandates on their own behalf and on their own behalf. directly entrusted by customers or members. They exercise this right under the conditions of the conventions that eventually bind them to those companies or their employers.

Accounting professionals are bound by professional secrecy in the course of their duties. However, they are untied in cases of information opened against them or of prosecutions brought against them by the public authorities or in actions brought before the disciplinary chambers of the Order.

They are also subject to rules of exercise incompatibility. As such, they cannot:

- be employed, except for another accountant or a company that is legally practising public accounting, a member of the National Company of Auditors, an accounting branch or a management and management association. Accounting;
- carry out another commercial activity or an act of intermediary, except if it is carried out on an ancillary basis and is not likely to endanger the exercise of the profession or the independence of the chartered accountants as well as the compliance by the latter with the rules inherent in their status and their ethics. The conditions and limits for the exercise of these activities for the performance of these acts are set by a professional standard approved by decree of March 12, 2021 of the Minister for the Economy;
- have a mandate to receive, retain or issue funds or securities, or to give up. However, as an accessory, accountants, accounting firms, branches, management and accounting associations, employees referred to in articles 83 ter and 83 quater, and multi-professional companies order's bank account may, through the bank account of their client or member, proceed to the amicable collection of their debts and the payment of their debts, for which a mandate has been entrusted to them, under conditions set by decree. The issuance of funds can be made when it corresponds to the payment of tax or social debts for which a mandate has been entrusted to the professional;
- act as a business agent, take on a mission of representation before the courts of the judicial or administrative order, perform accounting, accounting or accounting work for the companies in which they are directly or indirectly have substantial interests.

Finally, accounting professionals are subject to a Code of Ethics. This Code of Ethics contains rules related to general duties, duties to clients or members, duties of fraternity and duties to the College.

*To go further* Articles 12, 21, 22 of Ordinance 45-2138 of September 19, 1945, Articles 141 and following of Decree No. 2012-432 of March 30, 2012 and ordered on November 25, 2020 regarding the internal regulations of the College of Chartered Accountants.

### d. Financial guarantee

No financial guarantee is required.

### e. Professional liability insurance

Accounting professionals are required, if established in France, to justify an insurance contract to guarantee civil liability incurred because of all their work and activities. The amount of insurance guarantees taken out cannot be less than, per insured, 500,000 euros per claim and 1,000,000 euros per year of insurance. Parties may agree on more favourable provisions.

When the pecuniary consequences of civil liability are not covered by such a contract, they are guaranteed by an insurance contract underwritten by the Higher Council of the Order for the benefit of whom it will be owned. Accounting professionals are involved in the payment of premiums for this contract.

*To go further* Article 17 of Ordinance 45-2138 of September 19, 1945 and Articles 134 and articles of Decree No. 2012-432 of March 30, 2012.

### f. Some peculiarities of the regulation of the activity

#### Tax visa

The accounting professional may issue the tax visa to clients or members so that their professional income subject to income tax under a real plan is not increased. To do so, it must have:

- Received an authorization from the Government Commissioner to the Regional Council of the College of Accountants;
- signed a three-year agreement with the tax authorities.

*To go further* (7) Of Article 158 and Article 1649*Quarter* L of the General Tax Code.

#### Trusted third-party mission

As part of their annual income tax return, the individual can ask an accountant:

- Collect supporting documents that will be used to justify a deduction of the overall income or a tax credit;
- certify the execution of these operations;
- teletransmit the return to the tax authorities.

This mission is only possible if the accountant has an agreement with the administration and a contract with his client that frames his scope of intervention.

*To go further* Article 170*Ter* General Tax Code and Articles 95 ZA and articles from Schedule II to the General Tax Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The declaration of the creation of a business is a unique and mandatory formality for any new business. This approach allows:

- automatic registration in the national directory of companies and establishments (Sirene)
- receipt of an identification number (Siret number) and, where applicable, a Community VAT number.

The General Directorate of Public Finances (DGFIP) has set up a space entirely dedicated to business creators on the [impots.gouv.fr](https://www.impots.gouv.fr/portail/) website. The useful documentation is accessible from the link "Create my company" (Home/Professional/Create my company).

The creation of a file is done online via [guichet-entreprises.fr](https://www.guichet-entreprises.fr). The completed file will automatically be sent to the competent body for processing. In France, the business formalities centers (CFE) are in charge of collecting your file. The CFEs are the first interlocutors. The CFE makes it possible to accomplish in the same place or in a dematerialized way the procedures of creation, modification relating to the operation or termination of your business. Each center is responsible for companies whose head office, main establishment or secondary establishment is located within its territorial jurisdiction. The CFE depends on the nature of the structure in which the activity is carried out:

- for an independent liberal profession, the competent CFE is that of URSSAF (subscription of a P0PL form);
- for a commercial company, the competent CFE is that of the chamber of commerce and industry (subscription of an M0 form);
- for a civil company, the competent CFE is that of the registry of the commercial court (subscription of an M0 form);
- for a civil company from the departments of Bah-Rhin, Haut-Rhin and Moselle, the competent CFE is that of the district court (subscription of an M0 form).

**Situation of EU or EEA nationals liable for withholding tax (PAS) because exercising a temporary and occasional activity (LPS) employing at least one person affiliated to a social system and / or liable to tax on income in France and / or liable for VAT or other tax in France:**

Subscription of a Form 15928*02 EEO:

- either from one of the competent CFEs:
  - Urssaf for a liberal profession,
  - the Chamber of Commerce and Industry (CCI) for commercial companies,
  - the registry of the commercial court for civil companies,
  - the registry of the district court for civil companies in the departments of Bas-Rhin, Haut-Rhin and Moselle;
- either directly from the competent recipient body:
  - the National Center for Foreign Firms (CNFE), if the company employs at least one person affiliated to a social security scheme in France,
  - the non-resident tax department (DINR), if the company does not need to appoint a tax representative.

**Good to know**

The business tax service on which the tax representative depends remains competent when the business must appoint a tax representative and it is not liable for PAS.

### b. Post-registration authorisation

Accounting professionals are required to register on the College of Chartered Accountants.

*To go further* Articles 3, 7, 7*Bis*, 7 *Ter*, 7 *D*, 26, 26-1 and 27 of Ordinance 45-2138 of September 19, 1945 and Sections 31-1 to 31-2, and Articles 31-3 and following of Act No.90-1258 of December 31, 1990.

#### Contacts and information available

The Higher Council of the College of Chartered Accountants and the Regional Councils of the College of Chartered Accountants are the competent authorities for the inclusion of the College of Chartered Accountants and the recognition of qualifications.

      Conseil supérieur de l’Ordre des expert-comptables

      Immeuble Le Jour

      200-2016, rue Raymond Losserand 75680 Paris cedex 14

[Website of the Higher Council of the Order of Chartered Accountants](https://www.experts-comptables.fr/page-d-accueil)

In terms of initial training, the CSOEC has several dedicated pages on its website:

- [Curriculum](https://www.experts-comptables.fr/devenir-expert-comptable/le-cursus) ;
- [The internship](https://www.experts-comptables.fr/devenir-expert-comptable/le-stage).

In terms of the recognition of professional qualifications, the CSOEC has on its website a dedicated page, including a[fact sheet](https://www.experts-comptables.fr/devenir-expert-comptable/la-reconnaissance-des-qualifications) in this domain.

The steps and more information can be obtained by contacting the training department of the CSOEC:

- Email:[communication@cs.experts-comptables.org](mailto:communication@cs.experts-comptables.org) ;
- or Dominique Nechelis:[dnechelis@cs.experts-comptables.org](mailto:dnechelis@cs.experts-comptables.org) ;
- or Sophie Parisot : [sparisot@cs.experts-comptables.org](mailto:sparisot@cs.experts-comptables.org).

Finally, a[contact form](https://www.experts-comptables.fr/devenir-expert-comptable/contact-formation) is also available on the website of the Higher Council of the College of Chartered Accountants.

### c. Make a prior declaration of activity for EU nationals engaged in a one-off activity

The profession of accountant or part of the accounting activities may be carried out in France on a temporary and occasional basis by any national of an EU Member State or party to the EEA agreement, subject to:

- (1) to be legally established, on a permanent basis, in one of the states mentioned to carry out all or part of the activity of accountant;
- (2) where this occupation or the training conducting it is not regulated in the State of establishment, to have further practiced this occupation in one or more of the States mentioned for at least one year in the ten years preceding the performance he intends to achieve in France.

The provision of accounting is carried out under the professional title of the State of Establishment where such a title exists in that state. This title is indicated in the official language of the State of Establishment. In cases where this professional title does not exist in the State of the establishment, the claimant mentions his diploma or training title in the official language of that state. The performance of this accounting benefit is subject to a written statement to the Higher Council of the College of Chartered Accountants prior to the first benefit. The written statement specifies insurance coverage or other means of personal or collective protection regarding the professional liability of this provider. The declaration provided for the purpose of carrying out the first benefit in France on a temporary and occasional basis is repeated in the event of a material change in the elements of the declaration and renewed each year if the claimant plans to carry out this activity during the year in question. Upon receipt of this declaration, the Higher Council of the College of Chartered Accountants sends a copy to the regional council of the College of Chartered Accountants in which the provision of accounting expertise must be carried out. Upon receipt of this transmission, the regional council registers the registrant for the year considered on the Order's board.

*To go further* Section 26-1 of Ordinance 45-2138 of September 19, 1945.

### d. Make a pre-report of activity for a secondary school

In France, when a professional or company has one or more offices whose management is carried out on site and permanently by a member of the Order practising as an employee or partner of a company recognized by the Order, he or she is registered also due to this or those offices in the corresponding constituency table. This or these offices are marked separately.

The same is true when a management and accounting association has one or more offices permanently open to its members in which the supervision of the work is carried out on a regular and effective basis by an employee who is a member of the Order, a employee authorized to practise as a registered accountant as a result of the same constituency table or of employees previously designated as responsible for the accounting services of a licensed management centre and authorised in connection with the section 1649*Quarter* D of the General Tax Code, effectively and regularly practising within that association. This registration must be requested by the management and accounting association at the National Registration Commission. On notification from the National Registration Commission, the regional council of the district in the jurisdiction of the office or offices immediately proceeds to register the management and accounting association on the board this Riding. This or these offices are marked separately. Where the management and accounting association is already registered, the registration of a newly created secondary office must be requested from the regional council on which it depends. In case of difficulty, the application for registration is submitted to the National Registration Commission.

*To go further* Article 118 of Decree No. 2012-432 of March 30, 2012.

Individuals who are nationals of other EU Member States or other states parties to the EEA agreement, as well as legal entities incorporated in accordance with the legislation of one of these states and having their statutory headquarters, their headquarters or their principal institution in one of these states, which are legally practising the profession of public accounting, are entitled to set up branches for the exercise of their profession that do not have the legal personality. These branches are the only ones entitled to use the term "accounting branch." Branches are not members of the College of Chartered Accountants. They are on the board. Their work is under the responsibility of an accountant practising within the branch and ordinal representative specifically appointed to the regional council of the College of Chartered Accountants. Branches are subject to legislation and regulations relating to the profession of public accountant.

*To go further* Article 7 *D* 45-2138 of September 19, 1945.