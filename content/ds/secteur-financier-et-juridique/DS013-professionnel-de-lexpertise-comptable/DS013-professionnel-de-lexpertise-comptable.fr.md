﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS013" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Secteur financier et juridique" -->
<!-- var(title)="Professionnel de l'expertise comptable" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="secteur-financier-et-juridique" -->
<!-- var(title-short)="professionnel-de-lexpertise-comptable" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/secteur-financier-et-juridique/professionnel-de-lexpertise-comptable.html" -->
<!-- var(last-update)="2021-07" -->
<!-- var(url-name)="professionnel-de-lexpertise-comptable" -->
<!-- var(translation)="None" -->

# Professionnel de l'expertise comptable

Dernière mise à jour : <!-- begin-var(last-update) -->Juillet 2021<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’expert-comptable est un professionnel dont la mission est de réviser et apprécier les comptabilités, attester de la régularité et de la sincérité des comptes de résultats, et également tenir, centraliser, ouvrir, arrêter, surveiller, redresser et consolider les comptabilités des entreprises et organismes.

Des activités accessoires directement liées aux travaux comptables sont également autorisées pour les professionnels de l'expertise comptable (consultations, effectuer toutes études et tous travaux d'ordre statistique, économique, administratif, juridique, social ou fiscal et apporter un avis devant toute autorité ou organisme public ou privé qui les y autorise) mais sans pouvoir en faire l'objet principal de leur activité et seulement s'il s'agit d'entreprises dans lesquelles sont assurées des missions d'ordre comptable de caractère permanent ou habituel ou dans la mesure où lesdites consultations, études, travaux ou avis sont directement liés aux travaux comptables dont ils sont chargés.

*Pour aller plus loin* : articles 2 et 22 de l'ordonnance n° 45-2138 du 19 septembre 1945 portant institution de l'Ordre des experts-comptables et réglementant le titre et la profession d'expert-comptable.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une profession libérale, le CFE compétent est l'Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d'industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

#### Législation nationale

La profession d'expert-comptable est réservée à toute personne inscrite au tableau de l'Ordre des experts-comptables, qui justifie :

- jouir de ses droits civils ;
- ne pas avoir été condamnée à une peine d'ordre criminel ou correctionnel ;
- qu’elle présente les garanties de moralité jugées nécessaires par le Conseil de l’Ordre ;
- aternativement :
  - être titulaire d'un diplôme français d'expertise comptable,
  - ou être âgé de quarante ans révolus et justifier de quinze ans d’activité dans l’exécution de travaux d’organisation ou de révision de comptabilité, dont 5 ans au moins dans des fonctions ou missions comportant l’exercice de responsabilités importantes d’ordre administratif, financier et comptable,
  - ou, si elle est ressortissante d’un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE), justifier d'une attestation de compétences ou d'un titre de formation reconnu comme équivalent et délivré dans un tel État et, le cas échéant, avoir satisfait à une épreuve d’aptitude (cf. point b), ou, si elle n'en est pas ressortissante, justifier d'un diplôme reconnu de niveau équivalent au diplôme d’expertise comptable (DEC), et avoir subi avec succès un examen d’aptitude.

*Pour aller plus loin* : articles 3, 7 *bis*, 26 et 27 de l’ordonnance n° 45-2138 du 19 septembre 1945 ; décret n° 2012-432 du 30 mars 2012 relatif à l’expertise comptable.

S’agissant de l’exercice de l’activité d’expertise comptable par une personne morale, les professionnels peuvent constituer :

- des sociétés d’expertise comptable : entités dotées de la personnalité morale, à l'exception des formes juridiques qui confèrent à leurs associés la qualité de commerçant. Les experts‑comptables doivent, directement ou indirectement par une société inscrite à l'Ordre, détenir plus de la moitié du capital et plus des deux tiers des droits de vote. Aucune personne ou groupement d'intérêts, extérieur à l'Ordre, ne doit détenir, directement ou par personne interposée, une partie du capital ou des droits de vote de nature à mettre en péril l'exercice de la profession, l'indépendance des associés experts-comptables ou le respect par ces derniers des règles inhérentes à leur statut et à leur déontologie. L'offre au public de titres financiers n'est autorisée que pour des titres excluant l'accès, même différé ou conditionnel, au capital. Les gérants, le président de la société par actions simplifiée, le président du conseil d'administration ou les membres du directoire doivent être des experts-comptables, membres de la société. La société membre de l'Ordre communique annuellement aux conseils de l'Ordre dont elle relève la liste de ses associés ainsi que toute modification apportée à cette liste ;
- des sociétés de participations d'expertise comptable : entités ayant pour objet principal la détention de titres des sociétés d’expertise comptable ainsi que la participation à tout groupement de droit étranger ayant pour objet l’exercice de la profession d’expert-comptable. Ces sociétés, inscrites au tableau de l’ordre, peuvent avoir des activités accessoires en relation directe avec leur objet et destinées exclusivement aux sociétés ou aux groupements dont elles détiennent des participations ;
- des associations de gestion et de comptabilité : entités créées à l'initiative de chambres de commerce et d'industrie territoriales, de chambres de métiers ou de chambres d'agriculture, ou d'organisations professionnelles d'industriels, de commerçants, d'artisans, d'agriculteurs ou de professions libérales. Aucune association ne peut être inscrite au tableau si elle a moins de trois cents adhérents lors de la demande d'inscription. Les dirigeants et les administrateurs de ces associations doivent justifier avoir satisfait à leurs obligations fiscales et sociales ;
- des succursales d’expertise comptable : les personnes physiques ressortissantes des autres États membres de l'Union européenne ou d'autres États parties à l'accord sur l'Espace économique européen ainsi que les personnes morales constituées en conformité avec la législation de l'un de ces États et ayant leur siège statutaire, leur administration centrale ou leur principal établissement dans l'un de ces États, qui y exercent légalement la profession d'expertise comptable, sont admises à constituer, pour l'exercice de leur profession, des succursales qui ne sont pas dotées de la personnalité juridique. Les succursales ne sont pas membres de l'Ordre des experts-comptables. Elles sont inscrites au tableau. Leurs travaux sont placés sous la responsabilité d'un expert-comptable exerçant au sein de la succursale et représentant ordinal spécifiquement désigné à ce titre auprès du conseil régional de l'Ordre des experts-comptables par les personnes admises à les constituer ;
-  des sociétés pluri-professionnelles d'exercice : entités ayant pour objet l'exercice en commun de plusieurs des professions d'avocat, d'avocat au Conseil d'État et à la Cour de cassation, de commissaire-priseur judiciaire, d'huissier de justice, de notaire, d'administrateur judiciaire, de mandataire judiciaire, de conseil en propriété industrielle, de commissaire aux comptes et d'expert-comptable. La société pluri-professionnelle d'exercice doit comprendre, parmi ses associés, au moins un membre de chacune des professions qu'elle exerce. La société peut revêtir toute forme sociale, à l'exception de celles qui confèrent à leurs associés la qualité de commerçant. Elle est régie par les règles particulières à la forme sociale choisie. Quelle que soit la forme sociale choisie par la société pluri-professionnelle d'exercice, et y compris lorsqu'elle n'a pas été constituée sous forme de société d'exercice libéral, certaines dispositions du titre Ier de la loi n° 90-1258 du 31 décembre 1990 lui sont applicables. La totalité du capital et des droits de vote est détenue par les personnes suivantes :
  - 1° toute personne physique exerçant, au sein de la société ou en dehors, l'une des professions mentionnées à l'article 31-3 de la loi n° 90-1258 du 31 décembre 1990  et exercées en commun au sein de la société,
  - 2° toute personne morale dont la totalité du capital et des droits de vote est détenue directement ou indirectement par une ou des personnes mentionnées au 1°,
  - 3° toute personne physique ou morale, légalement établie dans un autre État membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen ou dans la Confédération suisse, qui exerce effectivement, dans l'un de ces États, une activité soumise à un statut législatif ou réglementaire ou subordonnée à la possession d'une qualification nationale ou internationale reconnue, dont l'exercice relève en France de l'une des professions mentionnées à l'article 31-3 et qui est exercée en commun au sein de la société ; pour les personnes morales, la totalité du capital et des droits de vote est détenue dans les conditions prévues aux 1° ou 2° ;
- des sociétés de participations financières de professions libérales : entitées constituées entre personnes physiques ou morales exerçant une ou plusieurs professions libérales soumises à un statut législatif ou réglementaire ou dont le titre est protégé ou des personnes mentionnées au 6° du B du I de l'article 5 de la loi n° 90-1258 du 31 décembre 1990, les sociétés de participations financières ayant pour objet la détention des parts ou d'actions de sociétés mentionnées au premier alinéa de l'article 1er de la loi n° 90-1258 du 31 décembre 1990 ayant pour objet l'exercice de cette même profession ainsi que la participation à tout groupement de droit étranger ayant pour objet l'exercice de la même profession. Ces sociétés peuvent exercer toute autre activité sous réserve d'être destinée exclusivement aux sociétés ou aux groupements dont elles détiennent des participations. Ces sociétés peuvent être constituées sous la forme de sociétés à responsabilité limitée, de sociétés anonymes, de sociétés par actions simplifiées ou de sociétés en commandite par actions régies par le livre II du Code de commerce. Plus de la moitié du capital et des droits de vote doit être détenue par des personnes exerçant la même profession que celle exercée par les sociétés faisant l'objet de la détention des parts ou actions. Les gérants, le président, les dirigeants, le président du conseil d'administration, les membres du directoire, le président du conseil de surveillance et les directeurs généraux, ainsi que les deux tiers au moins des membres du conseil d'administration ou du conseil de surveillance de la société par actions simplifiée, doivent être choisis parmi les personnes admises à constituer ces sociétés.

*Pour aller plus loin* : articles 7, 7 *ter*, 7 *quinquies* de l'ordonnance n° 45-2138 du 19 septembre 1945 et articles 31-1 à 31-2, et articles 31-3 et suivants de la loi n° 90-1258 du 31 décembre 1990 relative à l'exercice sous forme de sociétés des professions libérales soumises à un statut législatif ou réglementaire ou dont le titre est protégé et aux sociétés de participations financières de professions libérales.

#### Formation

Pour demander son inscription à l'Ordre en tant qu'expert-comptable, le candidat, qui ne recourt pas aux procédures mentionnées aux articles 7 *bis*, 26 ou 27 de l'ordonnance n° 45‑2138 du 19 septembre 1945, devra au préalable avoir validé successivement les diplômes suivants sauf dispenses accordées aux titulaires de certains titres et diplômes :

- le diplôme de comptabilité et de gestion (DCG) de niveau licence (bac +3) ;
- le diplôme supérieur de comptabilité et de gestion (DSCG) de niveau master (bac +5) ;
- le diplôme d'expertise comptable (DEC) de niveau bac +8, à l'issue d'un stage de trois ans réalisé dans un cabinet d'expertise comptable. Pour ce diplôme, le candidat devra réussir une épreuve écrite portant sur la révision légale et contractuelle des comptes, une épreuve orale devant jury, et soutenir son mémoire de fin de stage.

Ces diplômes peuvent être également obtenu par le biais de la procédure de validation des acquis de l'expérience (VAE).

*Pour aller plus loin* : article 3 de l’ordonnance n° 45-2138 du 19 septembre 1945, articles 45 et suivants du décret n° 2012-432 du 30 mars 2012 et arrêté du 13 février 2019 fixant les dispositions relatives à l’obtention du DEC par la VAE.

#### Coûts associés

Des frais fixes existent par diplôme et par épreuve. En 2018, pour le DCG, le prix par épreuve est fixé à 22 euros à raison de 14 épreuves. Quant au DSCG, il est de 30 euros par épreuve à raison de 8 épreuves. S’agissant du DEC, il est de 50 euros à raison de 3 épreuves. En outre, certaines formations préparant à ces diplômes sont payantes. Pour plus d'informations, il est conseillé de se rapprocher des organismes qui les dispensent.

La procédure de reconnaissance des qualifications professionnelles des ressortissants de l’UE ou de l’EEE est gratuite.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### Ressortissants UE : en vue d’un exercice temporaire et occasionnel (LPS)

La profession d'expert-comptable ou une partie des activités d'expertise comptable peuvent être exercées en France de façon temporaire et occasionnelle par tout ressortissant d'un État membre de l'UE ou partie à l'accord sur l'EEE, sous réserve :

- 1° d'être légalement établi, à titre permanent, dans l'un des États mentionnés pour exercer tout ou partie de l'activité d'expert-comptable ;
- 2° lorsque cette profession ou la formation y conduisant ne sont pas réglementées dans l'État d'établissement, d'avoir en outre exercé cette profession dans un ou plusieurs des États mentionnés pendant au moins une année au cours des dix années qui précèdent la prestation qu'il entend réaliser en France.

La prestation d'expertise comptable est effectuée sous le titre professionnel de l'État d'établissement lorsqu'un tel titre existe dans cet État. Ce titre est indiqué dans la langue officielle de l'État d'établissement. Dans les cas où ce titre professionnel n'existe pas dans l'État d'établissement, le prestataire fait mention de son diplôme ou titre de formation dans la langue officielle de cet État. L'exécution de cette prestation d'expertise comptable est subordonnée à une déclaration écrite auprès du Conseil supérieur de l'Ordre des experts-comptables préalable à la première prestation. La déclaration écrite précise les couvertures d'assurance ou autres moyens de protection personnelle ou collective concernant la responsabilité professionnelle de ce prestataire. La déclaration prévue aux fins de réaliser la première prestation en France de façon temporaire et occasionnelle est réitérée en cas de changement matériel dans les éléments de la déclaration et renouvelée chaque année si le prestataire envisage d'exercer cette activité au cours de l'année concernée. Dès réception de cette déclaration, le Conseil supérieur de l'Ordre des experts-comptables en adresse copie au conseil régional de l'Ordre des experts-comptables dans le ressort duquel la prestation d'expertise comptable doit être réalisée. Dès réception de cette transmission, le conseil régional procède à l'inscription du déclarant pour l'année considérée au tableau de l'Ordre.

*Pour aller plus loin* : article 26-1 de l’ordonnance n° 45-2138 du 19 septembre 1945.

#### Ressortissants UE : en vue d’un exercice permanent (LE)

Peut être inscrit au tableau de l'Ordre en qualité d'expert-comptable, sans être titulaire du diplôme d’expertise comptable, tout ressortissant d'un État membre de l'UE ou partie à l'accord sur l'EEE qui satisfait à l'une des deux conditions suivantes :

- 1° être titulaire d'une attestation de compétences ou d'un titre de formation visé à l'article 11 de la directive 2005/36/CE modifiée du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles permettant l'exercice de la profession dans un État membre de l'UE ou partie à l'accord sur l'EEE. Cette attestation ou ce titre sont délivrés, soit par l'autorité compétente de cet Etat et sanctionnent une formation acquise dans l'UE ou dans l'EEE, soit par un pays tiers, à condition que soit fournie une attestation émanant de l'autorité compétente de l'État membre de l'UE ou partie à l'accord sur l'EEE qui a reconnu le diplôme, certificat ou autre titre, certifiant que son titulaire a, dans cet État, une expérience professionnelle de trois ans au moins à temps plein ou d'une durée équivalente à temps partiel au cours des dix dernières années. Les attestations de compétences ou les titres de formation sont délivrés par l'autorité compétente de l'État d’origine, désignée conformément aux dispositions législatives, réglementaires ou administratives de cet État ;
- 2° avoir exercé à plein temps la profession d'expert-comptable pendant une année ou à temps partiel pendant une durée totale équivalente au moins au cours des dix années précédentes dans un État membre de l'UE ou partie à l'accord sur l'EEE qui ne réglemente pas la profession et qui possèdent une ou plusieurs attestations de compétences ou preuves de titres de formation délivrés par un autre État qui ne réglemente pas cette profession. Ces attestations de compétences ou ces titres de formation remplissent les conditions suivantes :
  - a) être délivrés par l'autorité compétente de l'État mentionné au premier alinéa, désignée conformément aux dispositions législatives, réglementaires ou administratives de cet État,
  - b) attester la préparation du titulaire à l'exercice de la profession concernée.

Toutefois, la condition d'une expérience professionnelle d'un an mentionnée n'est pas exigée lorsque le titre ou les titres de formation détenus par le demandeur sanctionnent une formation réglementée directement orientée vers l'exercice de la profession comptable.

Sauf si les connaissances, aptitudes et compétences acquises au cours de son expérience professionnelle, à temps plein ou à temps partiel, ou de l'apprentissage tout au long de la vie, et ayant fait l'objet, à cette fin, d'une validation par une autorité compétente dans un État membre de l'UE ou partie à l'accord sur l'EEE européen ou dans un pays tiers désigné conformément aux dispositions législatives, réglementaires ou administrative de cet État, sont de nature à rendre cette vérification inutile, l'intéressé doit se soumettre à une épreuve d'aptitude :

- 1° lorsque la formation dont il justifie porte sur des matières substantiellement différentes de celles qui figurent au programme du diplôme français d'expertise comptable ;
- 2° lorsque l'État dans lequel il a obtenu une attestation de compétences ou un titre de formation visé à l'article 11 de la directive 2005/36/CE modifiée du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles dont il se prévaut ou l’état dans lequel il a exercé la profession ne réglemente pas cette profession ou la réglemente d'une manière substantiellement différente de la réglementation française. 

*Pour aller plus loin* : article 26 de l’ordonnance n° 45-2138 du 19 septembre 1945 et articles 97 à 99 et 103 du décret n° 2012-432 du 30 mars 2012.

Saisie d'une demande en ce sens, que ce soit pour un établissement ou pour une prestation temporaire et occasionnelle de services en France, l'autorité compétente accorde un accès partiel aux activités d'expertise comptable lorsque toutes les conditions suivantes sont remplies :

- 1° le professionnel est pleinement qualifié pour exercer dans un État membre de l'UE ou partie à l'accord sur l'EEE l'activité professionnelle pour laquelle un accès partiel est sollicité ;
- 2° les différences entre l'activité professionnelle légalement exercée dans l'État membre de l'UE ou partie à l'accord sur l'EEE et la profession d'expert‑comptable en France sont si importantes que l'application de mesures de compensation reviendrait à imposer au demandeur de suivre le programme complet d'enseignement et de formation requis en France pour avoir pleinement accès à la profession en France ;
- 3° l'activité professionnelle demandée peut objectivement être séparée d'autres activités relevant de la profession d'expert‑comptable en France, dans la mesure où elle peut être exercée de manière autonome dans l'État membre d'origine.

L'accès partiel peut être refusé si ce refus est justifié par des raisons impérieuses d'intérêt général, s'il est propre à garantir la réalisation de l'objectif poursuivi et s'il ne va pas au-delà de ce qui est nécessaire pour atteindre cet objectif.

L'activité professionnelle est exercée sous le titre professionnel de l'État d'origine lorsque l'accès partiel a été accordé. Les professionnels qui bénéficient d'un accès partiel indiquent clairement aux destinataires des services le champ de leurs activités professionnelles.

Les dispositions du présent I ne s'appliquent pas aux professionnels qui bénéficient de la reconnaissance automatique de leurs qualifications professionnelles conformément aux articles 49 *bis* et 49 *ter* de la directive 2005/36/CE modifiée du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles.

Les demandes aux fins d'accès partiel pour un établissement sont examinées selon la même procédure que les demandes présentées dans le cadre de l'article 26 de l’ordonnance n° 45-2138 du 19 septembre 1945.

Les professionnels ayant été autorisés à exercer partiellement l'activité d'expertise comptable ne sont pas membres de l'Ordre des experts-comptables. Ils sont inscrits au tableau de l'Ordre suivant les conditions énoncées au II de l'article 3 de l’ordonnance n° 45-2138 du 19 septembre 1945. Ils sont soumis aux dispositions législatives et réglementaires relatives à la profession d'expert-comptable. Ils acquittent des cotisations au même titre et dans les mêmes conditions que les membres de l'Ordre.

*Pour aller plus loin* : article 26-0 de l’ordonnance n° 45-2138 du 19 septembre 1945 et articles 97 à 99-1 et 103 du décret n° 2012-432 du 30 mars 2012.

### c. Condition d'honorabilité

Les professionnels de l’expertise comptable doivent observer les dispositions législatives et réglementaires régissant leur profession ainsi que le règlement intérieur de l'Ordre qui est établi par décision du Conseil supérieur. Les professionnels de l’expertise comptable assument dans tous les cas la responsabilité de leurs travaux et activités. La responsabilité propre des sociétés membres de l'Ordre et des associations de gestion et de comptabilité laisse subsister la responsabilité personnelle de chaque expert-comptable ou professionnel ayant été autorisé à exercer partiellement l'activité d'expertise comptable à raison des travaux qu'il exécute lui-même pour le compte de ces sociétés, succursales ou associations. Les travaux et activités doivent être assortis de la signature personnelle de l'expert-comptable, du salarié ou du professionnel ayant été autorisé à exercer partiellement l'activité d'expertise comptable ainsi que du visa ou de la signature sociale. Les membres de l'Ordre qui, étant associés ou actionnaires d'une société reconnue par lui, exercent leur activité dans cette société, ainsi que les membres de l'Ordre salariés d'un confrère ou d'une société inscrite au tableau, d'une société pluri-professionnelle d'exercice, d'une succursale ou d'une association de gestion et de comptabilité, et les professionnels ayant été autorisés à exercer partiellement l'activité d'expertise comptable peuvent exécuter en leur nom et pour leur propre compte les missions ou mandats qui leur sont directement confiés par des clients ou adhérents. Ils exercent ce droit dans les conditions prévues par les conventions qui les lient éventuellement auxdites sociétés ou à leurs employeurs.

Les professionnels de l’expertise comptable sont tenus au secret professionnel dans le cadre de leurs fonctions. Ils en sont toutefois déliées dans les cas d'information ouverte contre eux ou de poursuites engagées à leur encontre par les pouvoirs publics ou dans les actions intentées devant les chambres de discipline de l'Ordre.

Ils sont également soumis à des règles d'incompatibilité d'exercice. À ce titre, ils ne peuvent pas :

- être salarié, sauf d’un autre expert-comptable ou d’une société exerçant légalement l’expertise comptable, d'un membre de la Compagnie nationale des commissaires aux comptes, d'une succursale d’expertise comptable ou d'une association de gestion et de comptabilité ;
- réaliser une autre activité commerciale ou un acte d'intermédiaire, sauf s’il est réalisé à titre accessoire et n’est pas de nature à mettre en péril l’exercice de la profession ou l’indépendance des associés experts-comptables ainsi que le respect par ces derniers des règles inhérentes à leur statut et à leur déontologie. Les conditions et les limites à l’exercice de ces activités à la réalisation de ces actes sont fixées par une norme professionnelle agréée par arrêté du 12 mars 2021 du ministre chargé de l’économie ;
- avoir un mandat de recevoir, conserver ou délivrer des fonds ou valeurs, ou de donner quittance. Toutefois, à titre accessoire, les experts-comptables, les sociétés d'expertise comptable, les succursales, les associations de gestion et de comptabilité, les salariés mentionnés aux articles 83 ter et 83 quater et les sociétés pluri-professionnelles d'exercice inscrites au tableau de l'Ordre peuvent, par le compte bancaire de leur client ou adhérent, procéder au recouvrement amiable de leurs créances et au paiement de leurs dettes, pour lesquels un mandat leur a été confié, dans des conditions fixées par décret. La délivrance de fonds peut être effectuée lorsqu'elle correspond au paiement de dettes fiscales ou sociales pour lequel un mandat a été confié au professionnel ;
- agir en tant qu'agent d'affaires, assumer une mission de représentation devant les tribunaux de l'ordre judiciaire ou administratif, effectuer des travaux d'expertise comptable, de révision comptable ou de comptabilité pour les entreprises dans lesquelles ils possèdent directement ou indirectement des intérêts substantiels.

Enfin, les professionnels de l’expertise comptable sont soumis à un Code déontologie. Ce Code de déontologie contient des règles liées aux devoirs généraux, aux devoirs envers les clients ou adhérents, aux devoirs de confraternité et aux devoirs envers l’Ordre.

*Pour aller plus loin* : articles 12, 21, 22 de l’ordonnance n° 45-2138 du 19 septembre 1945, articles 141 et suivants du décret n° 2012-432 du 30 mars 2012 et arrêté du 25 novembre 2020 portant règlement intérieur de l’ordre des experts-comptables.

### d. Garantie financière

Aucune garantie financière n’est exigée.

### e. Assurance de responsabilité civile professionnelle

Les professionnels de l’expertise comptable sont tenus, s'ils sont établis en France, de justifier d'un contrat d'assurance pour garantir la responsabilité civile encourue en raison de l'ensemble de leurs travaux et activités. Le montant des garanties d'assurances souscrites ne peut être inférieur, par assuré, à 500 000 euros par sinistre et 1 000 000 d'euros par année d'assurance. Les parties peuvent convenir de dispositions plus favorables.

Lorsque les conséquences pécuniaires de la responsabilité civile ne sont pas couvertes par un tel contrat, elles sont garanties par un contrat d'assurance souscrit par le Conseil supérieur de l'Ordre au profit de qui il appartiendra. Les professionnels de l’expertise comptable participent au paiement des primes afférentes à ce contrat.

*Pour aller plus loin* : article 17 de l’ordonnance n° 45-2138 du 19 septembre 1945 et articles 134 et suivants du décret n° 2012-432 du 30 mars 2012.

### f. Quelques particularités de la réglementation de l’activité 

#### Visa fiscal

Le professionnel de l’expertise comptable peut délivrer le visa fiscal à ses clients ou adhérents afin que leurs revenus professionnels soumis à l’impôt sur le revenu selon un régime réel ne soient pas majorés. Pour ce faire, il doit avoir :

- reçu une autorisation délivrée par le commissaire du gouvernement auprès du conseil régional de l’Ordre des experts-comptables ;
- signé une convention de trois ans avec l'administration fiscale.

*Pour aller plus loin* : 7° de l’article 158 et l’article 1649 *quater* L du Code général des impôts (CGI).

#### Mission de tiers de confiance

Dans le cadre de sa déclaration annuelle d'impôt sur le revenu, le particulier peut demander à un expert-comptable :

- de collecter les pièces justificatives qui serviront à justifier une déduction du revenu global ou un crédit d'impôt ;
- d'attester l'exécution de ces opérations ;
- de procéder à la télétransmission de la déclaration à l'administration fiscale.

Cette mission n'est possible que si l'expert-comptable a conclu une convention avec l'administration et un contrat avec son client qui encadre son périmètre d'intervention.

*Pour aller plus loin* : article 170 *ter* du Code général des impôts et articles 95 ZA et suivants de l’annexe II au Code général des impôts.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

La déclaration de création d’une entreprise est une formalité unique et obligatoire pour toute nouvelle entreprise. Cette démarche permet :

- une inscription automatique au Répertoire national des entreprises et des établissements (Sirene)
- la réception d’un numéro d’identification (n° Siret) et, le cas échéant, un numéro de TVA communautaire.

La direction générale des Finances publiques (DGFIP) a mis en place sur le site [impots.gouv.fr](https://www.impots.gouv.fr/portail/) un espace entièrement dédié aux créateurs d’entreprise. La documentation utile est accessible à partir du lien « Créer mon entreprise » (Accueil/Professionnel/Créer mon entreprise).

La constitution d’un dossier s’effectue en ligne via [guichet-entreprises.fr](https://www.guichet-entreprises.fr). Le dossier complété sera automatiquement transmis à l'organisme compétent pour traitement. En France, les centres de formalités des entreprises (CFE) sont en charge de recueillir votre dossier. Les CFE sont les premiers interlocuteurs. Le CFE permet d'accomplir en un même lieu ou de manière dématérialisée les démarches de création, modification relative à l'exploitation ou cessation de votre entreprise. Chaque centre est compétent à l’égard des entreprises dont le siège, l’établissement principal ou l’établissement secondaire est situé dans son ressort territorial. Le CFE dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une profession libérale indépendante, le CFE compétent est celui de l’URSSAF (souscription d’un formulaire P0PL) ;
- pour une société commerciale, le CFE compétent est celui de la chambre de commerce et de l’industrie (souscription d’un formulaire M0) ;
- pour une société civile, le CFE compétent est celui du greffe du tribunal de commerce (souscription d’un formulaire M0) ;
- pour une société civile des départements du Bah-Rhin, du Haut-Rhin et de la Moselle, le CFE compétent est celui du tribunal d’instance (souscription d’un formulaire M0).

**Situation des ressortissants de l'UE ou de l'EEE redevable du prélèvement à la source (PAS) car exerçant une activité temporaire et occasionnelle (LPS) employant au moins une personne affiliée à un régime social et/ou redevable d'impôt sur le revenu en France et/ou redevable de la TVA ou d'un autre impôt en France :**

Souscription d'un formulaire 15928*02 EEO :

- soit auprès d'un des CFE compétents  :
  - l'Urssaf pour une profession libérale,
  - la chambre de commerce et d'industrie (CCI) pour les sociétés commerciales,
  - le greffe du tribunal de commerce pour les sociétés civiles,
  - le greffe du tribunal d’instance pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle ;
- soit directement auprès de l’organisme destinataire compétent :
  - le Centre national des firmes étrangères (CNFE), si l’entreprise emploie au moins une personne affiliée à un régime social en France,
  - la direction des impôts des non résidents (DINR), si l'entreprise n'a pas besoin de désigner un représentant fiscal.

**Bon à savoir**

Le service des impôts des entreprises dont dépend le représentant fiscal demeure compétent lorsque l'entreprise doit désigner un représentant fiscal et qu’elle n'est pas redevable de PAS.

### b. Autorisation(s) post-immatriculation

Les professionnels de l’expertise comptable sont soumis à une obligation d’inscription au tableau de l’Ordre des experts-comptables.

*Pour aller plus loin* : articles 3, 7, 7 *bis*, 7 *ter*, 7 *quinquies*, 26, 26-1 et 27 de l’ordonnance n° 45-2138 du 19 septembre 1945 et articles 31-1 à 31-2, et articles 31-3 et suivants de la loi n° 90-1258 du 31 décembre 1990.

#### Contacts et informations disponibles

Le Conseil supérieur de l’Ordre des experts-comptables et les conseils régionaux de l’Ordre des experts-comptables constituent les autorités compétentes en matière d’inscription au tableau de l’Ordre des experts-comptables et de reconnaissance des qualifications professionnelles.

      Conseil supérieur de l’Ordre des expert-comptables

      Immeuble Le Jour

      200-2016, rue Raymond Losserand 75680 Paris cedex 14

[Site internet du Conseil supérieur de l'Ordre des experts-comptables](https://www.experts-comptables.fr/page-d-accueil)

En matière de formation initiale le CSOEC dispose sur son site internet de plusieurs pages dédiées :

- [le cursus](https://www.experts-comptables.fr/devenir-expert-comptable/le-cursus) ;
- [le stage](https://www.experts-comptables.fr/devenir-expert-comptable/le-stage).

En matière de reconnaissance des qualifications professionnelles, le CSOEC dispose sur son site internet d’une page dédiée et contenant notamment une [fiche d’information](https://www.experts-comptables.fr/devenir-expert-comptable/la-reconnaissance-des-qualifications) en la matière.

Les démarches et de plus amples informations peuvent être obtenues en contactant le service formation du CSOEC :

- courriel : [communication@cs.experts-comptables.org](mailto:communication@cs.experts-comptables.org) ;
- ou Dominique Nechelis : [dnechelis@cs.experts-comptables.org](mailto:dnechelis@cs.experts-comptables.org) ;
- ou Sophie Parisot : [sparisot@cs.experts-comptables.org](mailto:sparisot@cs.experts-comptables.org).

Enfin, un [formulaire de contact](https://www.experts-comptables.fr/devenir-expert-comptable/contact-formation) est également disponible sur le site du Conseil supérieur de l’Ordre des experts-comptables.

### c. Effectuer une déclaration préalable d'activité pour les ressortissants européens exerçant une activité ponctuelle

La profession d'expert-comptable ou une partie des activités d'expertise comptable peuvent être exercées en France de façon temporaire et occasionnelle par tout ressortissant d'un État membre de l'UE ou partie à l'accord sur l'EEE, sous réserve :

- 1° d'être légalement établi, à titre permanent, dans l'un des États mentionnés pour exercer tout ou partie de l'activité d'expert-comptable ;
- 2° lorsque cette profession ou la formation y conduisant ne sont pas réglementées dans l'État d'établissement, d'avoir en outre exercé cette profession dans un ou plusieurs des États mentionnés pendant au moins une année au cours des dix années qui précèdent la prestation qu'il entend réaliser en France. 

La prestation d'expertise comptable est effectuée sous le titre professionnel de l'État d'établissement lorsqu'un tel titre existe dans cet État. Ce titre est indiqué dans la langue officielle de l'État d'établissement. Dans les cas où ce titre professionnel n'existe pas dans l'État d'établissement, le prestataire fait mention de son diplôme ou titre de formation dans la langue officielle de cet État. L'exécution de cette prestation d'expertise comptable est subordonnée à une déclaration écrite auprès du Conseil supérieur de l'Ordre des experts-comptables préalable à la première prestation. La déclaration écrite précise les couvertures d'assurance ou autres moyens de protection personnelle ou collective concernant la responsabilité professionnelle de ce prestataire. La déclaration prévue aux fins de réaliser la première prestation en France de façon temporaire et occasionnelle est réitérée en cas de changement matériel dans les éléments de la déclaration et renouvelée chaque année si le prestataire envisage d'exercer cette activité au cours de l'année concernée. Dès réception de cette déclaration, le Conseil supérieur de l'Ordre des experts-comptables en adresse copie au conseil régional de l'Ordre des experts-comptables dans le ressort duquel la prestation d'expertise comptable doit être réalisée. Dès réception de cette transmission, le conseil régional procède à l'inscription du déclarant pour l'année considérée au tableau de l'Ordre.

*Pour aller plus loin* : article 26-1 de l’ordonnance n° 45-2138 du 19 septembre 1945.

### d. Effectuer une déclaration préalable d'activité pour un établissement secondaire

En France, lorsqu'un professionnel ou une société possède un ou plusieurs bureaux dont la direction est assurée sur place et en permanence par un membre de l'Ordre exerçant en qualité de salarié ou d'associé d'une société reconnue par l'Ordre, il ou elle est inscrit également à raison de ce ou ces bureau(x) au tableau des circonscriptions correspondantes. Ce ou ces bureau(x) font l'objet d'une mention distincte.

Il en est de même lorsqu'une association de gestion et de comptabilité possède un ou plusieurs bureaux ouverts en permanence à ses adhérents dans lesquels l'encadrement des travaux est assuré de manière régulière et effective par un salarié membre de l'Ordre, un salarié autorisé à exercer la profession d'expert-comptable inscrit à la suite du tableau de la même circonscription ou de salariés antérieurement désignés en qualité de responsables des services comptables d'un centre de gestion agréé et habilité dans le cadre de l'habilitation prévue à l'article 1649 *quater* D du Code général des impôts, exerçant de manière effective et régulière au sein de ladite association. Cette inscription doit être demandée par l'association de gestion et de comptabilité à la Commission nationale d'inscription. Sur notification de la Commission nationale d'inscription, le conseil régional de la circonscription dans le ressort de laquelle se trouvent le ou les bureaux procède sans délai à l'inscription de l'association de gestion et de comptabilité au tableau cette circonscription. Ce ou ces bureaux font l'objet d'une mention distincte. Lorsque l'association de gestion et de comptabilité est déjà inscrite, l'inscription d'un bureau secondaire nouvellement créé doit être demandée au conseil régional dont il dépend. En cas de difficulté, la demande d'inscription est soumise à la Commission nationale d'inscription.

*Pour aller plus loin* : article 118 du décret n° 2012-432 du 30 mars 2012.

Les personnes physiques ressortissantes des autres États membres de l'UE ou d'autres États parties à l'accord sur l'EEE ainsi que les personnes morales constituées en conformité avec la législation de l'un de ces États et ayant leur siège statutaire, leur administration centrale ou leur principal établissement dans l'un de ces États, qui y exercent légalement la profession d'expertise comptable, sont admises à constituer, pour l'exercice de leur profession, des succursales qui ne sont pas dotées de la personnalité juridique. Ces succursales sont seules habilitées à utiliser l'appellation de « succursale d'expertise comptable ». Les succursales ne sont pas membres de l'Ordre des experts-comptables. Elles sont inscrites au tableau. Leurs travaux sont placés sous la responsabilité d'un expert-comptable exerçant au sein de la succursale et représentant ordinal spécifiquement désigné à ce titre auprès du conseil régional de l'Ordre des experts-comptables. Les succursales sont soumises aux dispositions législatives et réglementaires relatives à la profession d'expert-comptable.

*Pour aller plus loin* : article 7 *quinquies* de l’ordonnance n° 45-2138 du 19 septembre 1945.