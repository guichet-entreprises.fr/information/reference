﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS019" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Non-industrial laundry" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="non-industrial-laundry" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/non-industrial-laundry.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="non-industrial-laundry" -->
<!-- var(translation)="Auto" -->


Non-industrial laundry
=====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Non-industrial dyeing is an activity whose mission is to wash and dye fabrics. This activity is often associated with laundry, laundry and ironing customers' laundry.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For craft activities, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial companies, it is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional who wishes to open a non-industrial dyeing establishment must:

- Take an installation preparation course (SPI)
- depending on the nature of its activity, register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

#### Preparation for installation

Before registration, the professional must complete an installation preparation course (SPI). The SPI is a mandatory requirement for anyone applying for registration in the trades and crafts repertoire.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary for the creation of a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including an education in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional from a Member State of the European Union (EU) or another State party to the Agreement on the European Economic Area (EEA) is exempt from the SPI if he justifies with the CMA a qualification in management confers on him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who:

- have either worked for at least three years requiring a level of knowledge equivalent to that provided by the internship;
- either have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, fully or partially, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France for the management of a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree 83-517 of June 24, 1983.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There is no provision for the national of an EU or EEA Member State to carry out non-industrial dyeing on a temporary and casual or permanent basis in France.

As such, the professional EU national is subject to the same professional requirements as the French national (see above "3o. Installation procedures and formalities").

### c. Conditions of honorability

No one may engage in the dying business if it has been the subject of:

- a criminal conviction or a prohibition on running, managing or administering a commercial or craft business or a corporation;
- additional penalty for prohibiting the use of professional activity.

*For further information*: Article 131-6 of the Penal Code.

### d. Some peculiarities of the regulation of the activity

#### Regulations on the quality of craftsman and the titles of master craftsman and best worker in France

The professional of the dyeing factory can avail himself of the quality of craftsman, the title of master craftsman or the title of best worker of France (MOF).

**Craftsmanship**

To claim the status of craftsman, the person must justify either:

- a CAP, a BEP or a certified or registered title when it was issued to the National Directory of Professional Certifications (RNCP) of at least an equivalent level (see supra "2." a. Professional qualifications");
- professional experience in this trade for at least three years.

*For further information*: Article 1 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of master craftsman**

This title is given to individuals, including the social leaders of legal entities:

- Registered in the trades directory;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

**Please note**

Individuals who do not hold the master's degree can apply to the Regional Qualifications Commission for the title of Master Craftsman under two assumptions:

- when they are registered in the trades directory, have a degree at least equivalent to the master's degree, and justify management and psycho-pedagogical knowledge equivalent to those of the corresponding value units of the master's degree and that they have two years of professional practice;
- when they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of best worker in France (MOF)**

The professional diploma "one of the best workers in France" is a state diploma that attests to the acquisition of a high qualification in the exercise of a professional activity in the artisanal, commercial, industrial or agricultural field.

The diploma is classified at level III of the interdepartmental nomenclature of training levels. It is issued after an examination called "one of the best workers in France" under a profession called "class" attached to a group of trades.

For more information, it is recommended that you consult[official website](http://www.meilleursouvriersdefrance.info/) "one of the best workers in France."

*For further information*: Article D. 338-9 of the Education Code.

#### Information and price of services

The professional engaged in the non-industrial dyeing activity must inform consumers of the prices charged for each offered service. Thus, it must make visible and readable to its customers, in the shop window or failing at the entrance of the establishment as well as outside, the rates charged (TTC) as well as the nature of the following services:

- laundry:- white sheet,
  - color sheet,
  - men's shirt,
  - washed, undried weight, per minimum of 4 kg per kilogram;
- for dry cleaning:- men's and lady's pants,
  - Jacket
  - Skirt
  - Dress
  - coat or raincoat.

*For further information*: order of March 27, 1987 relating to the advertising of prices for services in the laundry and dry cleaning sector.

#### Risk regulation and environmental protection

Non-industrial dyeing activity can be considered a classified facility for environmental protection (ICPE) and as such, fall under the specific regulations of these facilities, due to the dangers or inconveniences to the neighbourhood, environment or public order.

Thus, the professional will have to check whether his activity falls under classified facilities by consulting the nomenclature of icPEs available on the[Aida website](https://aida.ineris.fr/liste_documents/1/18028/1) and, if necessary, proceed to:

- in case of laundry activity, laundry laundromat excluding dry cleaning:- a record if the washing capacity is more than five tons per day,
  - a declaration if the laundry capacity is greater than 500 kg per day but less than or equal to five tons per day;
- If solvents are used:- a request for authorisation if the capacity of the machines is greater than 50 kg,
  - a periodic check-up if the capacity of the machines is greater than 0.5 kg and less than or equal to 50 kg;
- If the activity is based on the benefits of dyes, prints, primers, coatings, bleaching and washing of textile materials, the professional will have to apply for authorisation if the amount of fiber and fabric that can be processed is more than one tonne per day and a declaration if it is greater than 50 kg per day but less than or equal to one tonne per day.

*For further information*: Article R. 511-9 of the Environmental Code and its[Annexes](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=241B87CE2555E6349D1C8DC16D6ECBDB.tplgfr25s_1?idArticle=LEGIARTI000036078650&cidTexte=LEGITEXT000006074220&dateTexte=20180215&categorieLien=id&oldAction=) ; decree of 21 November 2017 amending the nomenclature of facilities classified for environmental protection.

#### Compliance with obligations for institutions receiving the public (ERP)

As the premises are open to the public, the professional must comply with the rules relating to public institutions (ERP):

- Fire
- accessibility.

For more information, it is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

**Clothing retention requirement**

Any garment entrusted to the dry cleaner that has not been removed beyond one year in the dyeing plant will be considered abandoned. If necessary, the professional may be authorized by the judge of the district court or the president of the high court to resell it at public auctions.

*For further information*: Article 1 of the Act of 31 December 1903 relating to the sale of certain abandoned items.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

**Competent authority**

The professional engaged in a non-industrial dyeing activity must report his company, and for this, must make a declaration with the ICC.

**Supporting documents**

The person concerned must provide the[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sends a receipt to the professional on the same day mentioning the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the CFE refuses to receive the file, the applicant has an appeal before the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

### b. If necessary, register the company's statutes

The professional carrying out the non-industrial dyeing activity must, once the company's statutes are dated and signed, register them with the corporate tax department ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building when the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

