﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS019" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Teinturerie non industrielle" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="teinturerie-non-industrielle" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/teinturerie-non-industrielle.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="teinturerie-non-industrielle" -->
<!-- var(translation)="None" -->

# Teinturerie non industrielle

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

La teinturerie non industrielle est une activité dont la mission consiste à laver et teindre les tissus. Cette activité est souvent associée à celle de blanchisserie qui consiste à laver, blanchir et repasser le linge des clients.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les activités artisanales, le CFE compétent est la chambre de métiers et de l'artisanat (CMA) ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le professionnel qui souhaite ouvrir un établissement de teinturerie non industrielle doit :

- suivre un stage de préparation à l'installation (SPI) ;
- suivant la nature de son activité, s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

#### Stage de préparation à l'installation

Avant son immatriculation le professionnel doit effectuer un stage de préparation à l'installation (SPI). Le SPI est un prérequis obligatoire pour toute personne sollicitant son immatriculation au répertoire des métiers et de l'artisanat.

**Modalités du stage**

L’inscription se fait sur présentation d’une pièce d’identité auprès de la CMA territorialement compétente. Le stage a une durée minimale de 30 heures et se présente sous la forme de cours et de travaux pratiques. Il a pour objectif de faire acquérir les connaissances essentielles dans les domaines juridique, fiscal, social et comptable, nécessaires à la création d’une entreprise artisanale.

**Report exceptionnel du début du stage**

En cas de force majeure, l’intéressé est autorisé à effectuer le SPI dans un délai d’un an à compter de l’immatriculation de son entreprise au répertoire des métiers. Il ne s’agit pas d’une dispense mais simplement d’un report du stage qui demeure obligatoire.

**Issue du stage**

Le participant recevra une attestation de suivi de stage qu’il devra obligatoirement joindre à son dossier de déclaration d’entreprise.

**Coût**

Le stage est payant. À titre indicatif, la formation coûtait environ 260 euros en 2017.

**Cas de dispense de stage**

L’intéressé peut être dispensé d’effectuer le stage dans deux situations :

- s’il a déjà bénéficié d’une formation sanctionnée par un titre ou un diplôme homologué au niveau III (bac +2) comportant un enseignement en économie et gestion d’entreprise, ou le brevet de maîtrise délivré par une CMA ;
- s’il a exercé pendant au moins trois ans une activité professionnelle requérant un niveau de connaissance équivalent à celui fourni par le stage.

**Dispense de stage pour les ressortissants de l’UE ou de l’EEE**

Par principe, un professionnel qualifié ressortissant d'un État membre de l'Union européenne (UE) ou d'un autre État partie à l'accord sur l'Espace économique européen (EEE) est dispensé du SPI s’il justifie auprès de la CMA d’une qualification en gestion d’entreprise lui conférant un niveau de connaissance équivalent à celui fourni par le stage.

La qualification en gestion d’entreprise est reconnue comme équivalente à celle fournie par le stage pour les personnes qui :

- ont soit exercé pendant au moins trois ans une activité professionnelle requérant un niveau de connaissance équivalent à celui fourni par le stage ;
- soit disposent de connaissances acquises dans un État de l’UE ou de l’EEE ou un État tiers au cours d’une expérience professionnelle de nature à couvrir, totalement ou partiellement la différence substantielle en matière de contenus.

Pour les personnes ne répondant pas à ces conditions, la chambre consulaire peut exiger qu’elles se soumettent à une mesure de compensation si l’examen de ses qualifications professionnelles fait apparaître des différences substantielles avec celles requises en France pour la direction d’une entreprise artisanale.

**Modalités de la dispense de stage**

Pour être dispensé de SPI, l’intéressé (français ou ressortissant de l’UE ou de l’EEE) doit adresser au président de la CMA concernée une demande de dispense de stage.

Il doit accompagner son courrier des pièces justificatives suivantes :

- copie du diplôme homologué au niveau III ;
- copie du brevet de maîtrise ;
- justificatifs d’exercice d’une activité professionnelle requérant un niveau de connaissance équivalent ;
- l'acquittement de frais variables.

L’absence de réponse dans un délai d’un mois suivant la réception de la demande vaut acceptation de la demande de dispense de stage.

*Pour aller plus loin* : article 2 de la loi n° 82-1091 du 23 décembre 1982 ; article 6-1 du décret n° 83-517 du 24 juin 1983.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services (LPS) ou Libre Établissement (LE))

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'UE ou de l'EEE en vue d'exercer l'activité de teinturerie non industrielle à titre temporaire et occasionnel ou permanent en France.

A ce titre, le professionnel ressortissant UE est soumis aux mêmes exigences professionnelles que le ressortissant français (cf. supra « 3°. Démarches et formalités d'installation »).

### c. Conditions d’honorabilité

Nul ne peut exercer l'activité de teinturier s'il a fait l'objet :

- d'une condamnation pénale ou d'une interdiction de diriger, gérer ou administrer une entreprise commerciale ou artisanale ou une personne morale ;
- d'une peine complémentaire d'interdiction d'exercer une activité professionnelle.

*Pour aller plus loin* : article 131-6 du Code pénal.

### d. Quelques particularités de la réglementation de l’activité

#### Réglementation relative à la qualité d’artisan et aux titres de maître artisan et de meilleur ouvrier de France

Le professionnel de la teinturerie peut se prévaloir de la qualité d'artisan, du titre de maître artisan ou du titre de meilleur ouvrier de France (MOF).

**La qualité d’artisan**

Pour se prévaloir de la qualité d’artisan, la personne doit justifier soit :

- d’un CAP, d’un BEP ou d’un titre homologué ou enregistré lors de sa délivrance au Répertoire national des certifications professionnelles (RNCP) d’un niveau au moins équivalent (cf. supra « 2°. a. Qualifications professionnelles ») ;
- d’une expérience professionnelle dans ce métier de trois ans au moins.

*Pour aller plus loin* : article 1 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

**Le titre de maître artisan**

Ce titre est attribué aux personnes physiques, y compris les dirigeants sociaux des personnes morales :

- immatriculées au répertoire des métiers ;
- titulaires du brevet de maîtrise dans le métier exercé ;
- justifiant d’au moins deux ans de pratique professionnelle.

**À noter**

Les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan à la commission régionale des qualifications dans deux hypothèses :

- lorsqu’elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de niveau de formation au moins équivalent au brevet de maîtrise, qu’elles justifient de connaissances en gestion et en psychopédagogie équivalentes à celles des unités de valeur correspondantes du brevet de maîtrise et qu’elles ont deux ans de pratique professionnelle ;
- lorsqu’elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles disposent d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

*Pour aller plus loin* : article 3 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

**Le titre de meilleur ouvrier de France (MOF)**

Le diplôme professionnel « un des meilleurs ouvriers de France » est un diplôme d’État qui atteste l’acquisition d’une haute qualification dans l’exercice d’une activité professionnelle dans le domaine artisanal, commercial, industriel ou agricole.

Le diplôme est classé au niveau III de la nomenclature interministérielle des niveaux de formation. Il est délivré à l’issue d’un examen dénommé « concours un des meilleurs ouvriers de France » au titre d’une profession dénommée « classe », rattachée à un groupe de métiers.

Pour plus d’informations, il est recommandé de consulter le [site officiel](http://www.meilleursouvriersdefrance.info/) du concours « un des meilleurs ouvriers de France ».

*Pour aller plus loin* : article D. 338-9 du Code de l’éducation.

#### Informations et prix des prestations

Le professionnel exerçant l'activité de teinturerie non industrielle doit informer les consommateurs des prix pratiqués pour chaque prestation proposée. Ainsi, il doit rendre visibles et lisibles à sa clientèle, en vitrine ou à défaut à l'entrée de l'établissement ainsi qu'à l'extérieur, les tarifs pratiqués (TTC) ainsi que la nature des prestations suivantes :

- en blanchisserie :
  - drap blanc,
  - drap couleur,
  - chemise homme,
  - linge au poids lavé, non séché, par 4 kg minimum, le kilogramme ;
- pour le nettoyage à sec :
  - pantalon homme et dame,
  - veste,
  - jupe,
  - robe,
  - manteau ou imperméable.

*Pour aller plus loin* : arrêté du 27 mars 1987 relatif à la publicité des prix des prestations de services dans le secteur de la blanchisserie et du nettoyage à sec.

#### Réglementation en matière de risques et protection de l'environnement

L'activité de teinturerie non industrielle peut être considérée comme une installation classée pour la protection de l'environnement (ICPE) et à ce titre, relever de la réglementation spécifique à ces installations, en raison des dangers ou inconvénients pour le voisinage, l'environnement ou l'ordre public.

Ainsi, le professionnel devra vérifier si son activité relève des installations classées en consultant la nomenclature des ICPE disponible sur le [site de l'Aida](https://aida.ineris.fr/liste_documents/1/18028/1) et le cas échéant, procéder à :

- en cas d'activité de blanchisserie, laverie de linge à l'exclusion du nettoyage à sec :
  - un enregistrement si la capacité de lavage est supérieure à cinq tonnes par jour,
  - une déclaration si la capacité de lavage de linge est supérieure à 500 kg par jour mais inférieure ou égale à cinq tonnes par jour ;
- en cas d'utilisation de solvants :
  - une demande d'autorisation si la capacité des machines est supérieure à 50 kg,
  - une déclaration avec contrôle périodique si la capacité des machines est supérieure à 0,5 kg et inférieure ou égale à 50 kg ;
- si l'activité relève des prestations de teintures, impressions, apprêt, enduction, blanchiment et délavage de matières textiles, le professionnel devra effectuer une demande d'autorisation si la quantité de fibres et de tissus susceptible d'être traitée est supérieure à une tonne par jour et une déclaration si elle est supérieure à 50 kg par jour mais inférieure ou égale à une tonne par jour.

*Pour aller plus loin* : article R. 511-9 du Code de l'environnement et ses annexes ; décret du 21 novembre 2017 modifiant la nomenclature des installations classées pour la protection de l'environnement.

#### Respect des obligations pour les établissements recevant du public (ERP)

Les locaux étant ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

**Obligation de conservation des vêtements**

Tout vêtement confié au teinturier qui n'aurait pas été retiré au delà d'un délai d'un an au sein de la teinturerie sera considéré comme abandonné. Le cas échéant, le professionnel pourra être autorisé par le juge du tribunal d'instance ou le président du tribunal de grande instance à le revendre lors de ventes aux enchères publiques.

*Pour aller plus loin* : article 1er de la loi du 31 décembre 1903 relative à la vente de certains objets abandonnés.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

**Autorité compétente**

Le professionnel exerçant une activité de teinturerie non industrielle doit procéder à la déclaration de son entreprise, et pour cela, doit, effectuer une déclaration auprès de la CCI.

**Pièces justificatives**

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

**Délais**

Le centre des formalités des entreprises de la CCI adresse le jour même un récépissé au professionnel mentionnant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

**Voies de recours**

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le CFE refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

**Coût**

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### b. Le cas échéant, enregistrer les statuts de la société

Le professionnel exerçant l'activité de teinturerie non industrielle doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

**Autorité compétente**

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

**Pièces justificatives**

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.