﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS059" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Agence de mannequins" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="agence-de-mannequins" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/agence-de-mannequins.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agence-de-mannequins" -->
<!-- var(translation)="None" -->

# Agence de mannequins

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'exploitant d'une agence de mannequins représente des modèles qui prêtent leur physique pour la promotion de produits, services ou messages publicitaires et les met à disposition auprès de clients (magazines de mode, maison de hautes coutures, agences de publicité, etc.).

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

L'activité visant à placer des mannequins est réservée au titulaire d'une licence d'agence de mannequins.

Aucune formation spécifique n'est requise pour l'exploitant d'une agence, toutefois une bonne connaissance du milieu de la mode notamment, et des compétentes en matières sociale et comptable sont nécessaires.

*Pour aller plus loin* : article L. 7123-11 du Code du travail.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### Pour un exercice temporaire et occasionnel (LPS)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord de l'Espace économique européen (EEE) qui est établi et exerce légalement l'activité visant à placer des mannequins dans cet État peut exercer en France, de manière temporaire et occasionnelle, la même activité.

Il devra, préalablement à la première prestation, en faire la demande par déclaration auprès de la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (Direccte) (cf. infra « 3°. b. Effectuer une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice ponctuel et occasionnel (LPS) »).

*Pour aller plus loin* : article L. 7123-11 du Code du travail.

#### Pour un exercice permanent (LE)

La réglementation relative au professionnel français s'applique de la même façon pour le ressortissant de l'UE ou de l'EEE qui souhaite s'établir en France de façon permanente. Dès lors, le ressortissant devra obtenir la licence d'agence de mannequins auprès du préfet de Paris pour ouvrir son agence (cf. infra « 3°. c. Obtenir une licence d'agence de mannequins »).

*Pour aller plus loin* : articles L. 7123-11 et R. 7123-10-2 du Code du travail.

### c. Conditions d'honorabilité et incompatibilités

#### Dispositions pénales

L'exploitant d'une agence de mannequins doit conclure un contrat de travail avec chaque mannequin qu'il emploie. Le non-respect de cette obligation lui vaudra une peine d'emprisonnement de six mois et 75 000 euros d'amende.

Les mêmes peines peuvent s'appliquer pour l'exploitant qui ne serait pas titulaire de la licence telle que visée ci-après.

*Pour aller plus loin* : articles L. 7123-25 à L. 7123-32 du Code du travail.

#### Prévenir les conflits d'intérêts

L'activité d'agence de mannequin peut entraîner des situations de conflits d'intérêts avec les activités suivantes :

- production ou réalisation d'œuvres cinématographiques ou audiovisuelles ;
- distribution ou sélection pour l'adaptation d'une production ;
- organisation de cours ou de stages de formation payants pour mannequins ou comédiens ;
- agence de publicité ;
- organisation de défilés de mode ;
- photographie.

Pour prévenir de tels conflits, les mannequins employés par l'agence, ses clients et la Direccte territorialement compétente doivent être informés :

- des modalités de facturations des prestations exécutées par les mannequins ;
- du détail des mandats sociaux exercés par les dirigeants, associés et salariés.

*Pour aller plus loin* : articles R. 7123-15 à R. 7123-17-1 du Code du travail.

### d. Quelques particularités de la réglementation de l'activité

#### Obligation de justifier d'une garantie financière

Toute agence de mannequins doit justifier d'une garantie financière auprès de ses utilisateurs. Cette garantie a pour objet d'assurer le paiement des mannequins et celui des organismes de sécurité sociale.

Son montant ne doit pas être inférieur à 6 % de la masse salariale, ni à un minimum de 15 200 euros.

Le non-respect de cette obligation est puni de six mois de prison et d'une amende de 75 000 euros.

*Pour aller plus loin* : articles L. 7123-19 à L. 7123-22, et R. 7123-20 à R. 7123-22 du Code du travail.

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Effectuer une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice ponctuel et occasionnel (LPS)

#### Autorité compétente

La Direccte du lieu d'exécution de la prestation est compétente pour délivrer la déclaration préalable d'activité.

#### Pièces justificatives

La demande de déclaration préalable d'activité est accompagnée d'un dossier comprenant les pièces justificatives suivantes :

- les références de l'immatriculation de l'agence à un registre professionnel de son pays d'origine ;
- le nom ou la raison sociale et l'adresse du lieu d'établissement de l'agence de mannequins ;
- les nom, prénoms et adresse du domicile des dirigeants de l'agence ;
- la désignation du ou des organismes auxquels l'agence de mannequins verse les cotisations de sécurité sociale ;
- la preuve de l'obtention d'une garantie financière ;
- le nom ou la raison sociale ainsi que l'adresse de l'utilisateur ;
- les lieux, les dates, la durée et, le cas échéant, les heures d'exécution de la prestation ;
- le cas échéant, l'agrément pour l'emploi de mineurs de moins de 16 ans.

*Pour aller plus loin* : articles L. 7123-11 et R. 7123-12 du Code du travail.

### c. Obtenir une licence d'agence de mannequins

#### Autorité compétente

Le préfet de Paris est compétent pour délivrer la licence, après instruction du dossier par la Direccte d'Île-de-France et avis du directeur régional des affaires culturelles d’Île-de-France.

#### Pièces justificatives

La demande de licence se fait par l'envoi d'un dossier adressé au préfet par lettre recommandée avec avis de réception comportant l'ensemble des pièces suivantes, accompagné de leur traduction en français, le cas échéant :

- un extrait du Kbis de l'agence accompagné de ses statuts ;
- un curriculum vitae indiquant, notamment, l'expérience professionnelle du demandeur à la date de la demande ;
- la liste des collaborateurs permanents, des délégataires de l'agence et des personnes habilitées à représenter l'agence pour tout ou partie de ses activités au siège de l'agence ou dans les succursales, avec l'indication, pour chacune d'elles, des nom, prénoms, nationalité, date et lieu de naissance, adresse personnelle, expérience professionnelle (curriculum vitae) ainsi que des fonctions exercées au sein de l'agence ;
- une copie de l'attestation de la garantie financière ;
- un extrait de bulletin de casier judiciaire n° 2 ou tout document équivalent du demandeur de la licence, des dirigeants sociaux et des gérants de l'agence ;
- une note sur les conditions dans lesquelles l'agence exercera son activité, notamment au plan géographique, et comportant l'identification des succursales et les secteurs professionnels concernés ;
- une déclaration indiquant, le cas échéant, les autres activités ou professions exercées et les mandats sociaux détenus par chaque dirigeant, mandataire social, associé, délégataire et salarié.

#### Procédure

En cas de pièces justificatives manquantes, le préfet en informera le demandeur dans un délai d'un mois.

Le silence gardé du préfet dans un délai de deux mois vaut acceptation de la demande de licence.

**À savoir**

La licence est accordée pour une période de trois ans renouvelable. Tout changement de lieu du siège social de l'agence ou de ses succursales, modification de ses statuts ou changement de situation doit être notifié au préfet par déclaration.

*Pour aller plus loin* : articles L. 7123-11, R. 7123-10-1 et R. 7123-10-2 du Code du travail.

### d. Autorisation(s) post-immatriculation

#### Le cas échéant, obtenir un agrément pour l'emploi de mannequin de moins de 16 ans

Dès lors qu'une agence de mannequins souhaite employer des mineurs de moins de 16 ans, elle devra en faire la demande auprès de la Direccte et obtenir un agrément.

#### Pièces justificatives

Le dossier d'agrément doit comprendre les pièces suivantes :

- un extrait d'acte de naissance des dirigeants, associés et gérants de l'agence ;
- une attestation de versement des cotisations aux organismes de sécurité sociale pour les agences en activité au moment du dépôt de la demande d'agrément ;
- une attestation par laquelle l'agence s'engage à faire passer au mineur un examen médical aux frais de l'agence ;
- un exemplaire d'une notice précisant le fonctionnement de l'agence, le contrôle médical ; la procédure de sélection par les utilisateurs, les conditions de mise à disposition de l'utilisateur avec la durée de ses déplacements, les durées maximales d'emploi et les conditions de rémunération ;
- tous les justificatifs permettant d'apprécier :
  - la moralité, la compétence et l'expérience professionnelle en matière d'emploi d'enfants mannequins des dirigeants, associés et gérants de l'agence de mannequins,
  - la situation financière de l'agence,
  - les conditions de fonctionnement de l'agence,
  - les conditions dans lesquelles elle exercera son activité avec des enfants.

#### Délai

L'agrément est accordé pour une durée d'un an renouvelable, après avis d'une commission consultative. Le silence gardé dans un délai de deux mois à réception du dossier vaut acceptation de la demande d'agrément.

L'agence de mannequin devra consigner, entre autres informations, l'identité des mineurs employés, les horaires de travail, les mises à disposition du client pour chaque mineur, dans un registre spécial.

*Pour aller plus loin* : articles L. 7124-4, L. 7124-5 et R. 7124-8 à R. 7124-14 du Code du travail.