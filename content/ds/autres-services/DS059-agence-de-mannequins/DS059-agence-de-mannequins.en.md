﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS059" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Modelling agency" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="modelling-agency" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/modelling-agency.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="modelling-agency" -->
<!-- var(translation)="Auto" -->

Modelling agency
================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

The operator of a modeling agency represents models who lend their looks to promote products, services or advertising messages and makes them available to clients (fashion magazines, fashion houses, advertising, etc.).

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The activity to place mannequins is reserved for the holder of a modeling agency license.

No specific training is required for the operator of an agency, however a good knowledge of the fashion world in particular, and competence in social and accounting matters are necessary.

*To go further* Article L. 7123-11 of the Labour Code.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

#### For a temporary and casual exercise (LPS)

Any national of a Member State of the European Union (EU) or party to the Agreement of the European Economic Area (EEA) which is established and legally carries out the activity to place mannequins in that state may exercise in France, temporarily and occasional, the same activity.

It must, prior to the first benefit, make a declaration request to the regional directorate of companies, competition, consumption, labour and employment (Direccte) (see infra "3o). b. Make a pre-declaration of activity for the EU or EEA national for a one-time and casual exercise (LPS)).

*To go further* Article L. 7123-11 of the Labour Code.

#### For a permanent exercise (LE)

The French professional regulation applies in the same way to the EU or EEA national who wishes to settle in France permanently. Therefore, the national will have to obtain the license of a modeling agency from the prefect of Paris to open his agency (see infra "3. c. Obtain a modeling agency license").

*To go further* Articles L. 7123-11 and R. 7123-10-2 of the Labour Code.

### c. Conditions of honorability and incompatibility

#### Criminal provisions

The operator of a modeling agency must enter into an employment contract with each model he employs. Failure to do so will result in a six-month prison sentence and a fine of 75,000 euros.

The same penalties may apply to the operator who does not hold the licence as referred to below.

*To go further* Articles L. 7123-25 to L. 7123-32 of the Labour Code.

#### Preventing conflicts of interest

Modeling agency activity can lead to conflicts of interest with the following activities:

- production or production of film or audiovisual works;
- distribution or selection for the adaptation of a production;
- organising paid courses or training courses for models or actors;
- Advertising agency;
- organizing fashion shows;
- Photography.

To prevent such conflicts, the models employed by the agency, its clients and the territorially competent Direccte must be informed:

- How dummies are billed for the services performed by the models;
- details of the social mandates exercised by managers, associates and employees.

*To go further* Articles R. 7123-15 to R. 7123-17-1 of the Labour Code.

### d. Some peculiarities of the regulation of the activity

#### Obligation to justify a financial guarantee

Any modeling agency must justify a financial guarantee to its users. The purpose of this guarantee is to ensure the payment of mannequins and social security agencies.

Its amount must not be less than 6% of the wage bill, nor at a minimum of 15,200 euros.

Failure to comply with this obligation is punishable by six months in prison and a fine of 75,000 euros.

*To go further* Articles L. 7123-19 at L. 7123-22, and R. 7123-20 at R. 7123-22 of the Labour Code.

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Make a pre-declaration of activity for the EU or EEA national for a one-time and casual exercise (LPS)

**Competent authority**

The Direccte of the place of execution of the benefit is competent to issue the prior declaration of activity.

**Supporting documents**

The request for a pre-report of activity is accompanied by a file containing the following supporting documents:

- References to the agency's registration to a professional register in its country of origin;
- The name or name and address of the modeling agency's establishment;
- Names, first names and addresses of the home of the agency's leaders;
- The designation of the agency or agencies to which the modelling agency pays social security contributions;
- Proof of obtaining a financial guarantee
- The user's name or name and address
- Locations, dates, duration and, if applicable, the hours of delivery;
- approval for the employment of minors under the age of 16.

*To go further* Articles L. 7123-11 and R. 7123-12 of the Labour Code.

### c. Obtain a modeling agency license

**Competent authority**

The prefect of Paris is competent to issue the license, after instruction of the file by the Direccte d'Ile-de-France and notice of the regional director of cultural affairs of Ile-de-France.

**Supporting documents**

The application for a licence is made by sending a file addressed to the prefect by letter recommended with notice of receipt containing all the following documents, accompanied by their translation into French, if necessary:

- an excerpt from the agency's Kbis accompanied by its statutes;
- A resume showing, among other things, the applicant's work experience on the date of application;
- the list of permanent employees, delegates of the agency and persons entitled to represent the agency for all or part of its activities at the agency's headquarters or in the branches, with the indication, for each of them, of the names, first names, nationality, date and place of birth, personal address, professional experience (curriculum vitae) as well as functions performed within the agency;
- A copy of the financial guarantee certificate
- an extract from criminal record slip 2 or any equivalent document of the licence applicant, social leaders and agency managers;
- A note on the conditions under which the agency will operate, particularly geographically, and including the identification of branches and relevant professional sectors;
- a statement indicating, if necessary, the other activities or professions carried out and the social mandates held by each officer, social agent, partner, delegate and employee.

**Procedure**

In the event of missing supporting documents, the prefect will notify the applicant within one month.

The prefect's silence within two months is worth accepting the application for a licence.

**What to know**

The licence is granted for a renewable three-year period. Any change of location of the head office of the agency or its branches, changes in its statutes or change of situation must be notified to the prefect by declaration.

*To go further* Articles L. 7123-11, R. 7123-10-1 and R. 7123-10-2 of the Labour Code.

### d. Post-registration authorisation

#### If necessary, obtain a licence for the job of model under the age of 16

If a modeling agency wishes to employ minors under the age of 16, it will have to apply to the Direccte and obtain accreditation.

**Supporting documents**

The accreditation file must include:

- an extract from the birth certificates of the agency's officers, associates and managers;
- a certificate of payment of contributions to social security agencies for agencies operating at the time of filing the application for accreditation;
- a certificate by which the agency agrees to give the minor a medical examination at the agency's expense;
- A copy of a notice detailing the operation of the agency, the medical check-up; The user selection procedure, the conditions for making the user available with the duration of their travels, the maximum durations of employment and the conditions of remuneration;
- all the proofs to appreciate:- the morality, competence and professional experience of employing model children of the directors, associates and managers of the modelling agency,
  - The agency's financial situation
  - The agency's operating conditions,
  - conditions in which she will work with children.

**Timeframe**

Accreditation is granted for a renewable period of one year, after notice of a Advisory Committee. The silence kept within two months of receiving the file is worth accepting the application for approval.

The modeling agency will have to record, among other information, the identity of the minors employed, the working hours, the availability of the client for each minor, in a special register.

*To go further* Articles L. 7124-4, L. 7124-5 and R. 7124-8 to R. 7124-14 of the Labour Code.