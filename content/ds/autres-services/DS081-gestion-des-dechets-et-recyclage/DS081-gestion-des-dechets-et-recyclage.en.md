﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS081" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Waste management and recycling" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="waste-management-and-recycling" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/waste-management-and-recycling.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="waste-management-and-recycling" -->
<!-- var(translation)="Auto" -->


Waste management and recycling
==============================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Waste management and recycling is an activity that consists for the professional to ensure the management of waste from their production to their treatment. This activity includes collection, transportation, recovery operation (allowing waste to be used for useful purposes as a substitute for other substances), or waste disposal.

Recycling, on the other hand, is a recycling operation that consists of treating the waste for reuse either for its original purpose or for another purpose.

*For further information*: Article L. 541-1-1 of the Environment Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. As the activity is commercial in nature, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional carries out a buying and resale activity his activity will be both commercial and artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

No professional qualifications are required for the professional wishing to carry out the waste management and recycling activity.

However, when its activity falls under the classified facilities for the protection of the environment (ICPE), it will be required to carry out specific formalities (see infra "3." a. If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)).

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There are no specific provisions for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement to carry out waste management and recycling activities on a temporary basis. casual or permanent in France.

As such, the national is bound to the same requirements as the French national (see infra "3°. Installation procedures and formalities").

### c. Responsibility and sanctions

**Responsibility**

Any professional who produces or holds waste is responsible for its management until it is disposed of or recycled, even if treated by a third party.

*For further information*: Article L. 541-2-1 of the Environment Code.

**Sanctions**

In the event of a breach of specific obligations in the management and recycling of waste, the professional faces a two-year prison sentence and a fine of 75,000 euros.

*For further information*: Article L. 541-46 of the Environment Code.

### d. Some peculiarities of the regulation of the activity

**Waste nomenclature**

The professional is required to determine the nature of the waste he manages. It must also include on all traceability documents the Code of this waste, established according to the nomenclature set out in the annex of the[Decision 2000/532/EC](https://Aida.ineris.fr/consultation_document/33804) 03/05/00 replacing decision 94/3/EC establishing a waste list under Article 1, (a) Council's Directive 75/442/EEC on Waste and the Council's Decision 94/904/EC establishing a list of hazardous wastes under Article 1, paragraph 4, directive 91/689/EEC of the Hazardous Waste Council.

*For further information*: Article R. 541-7 of the Environment Code.

**Compliance with the national waste prevention and management policy**

The professional working in waste management and recycling is bound by the national waste prevention and management policy. This policy is aimed at promoting the so-called circular economy.

As such, the professional is required to:

- Prevent and minimize the production and harmfulness of waste;
- Respect the hierarchy of waste treatment methods
- Ensure that waste management does not harm human health or the environment;
- respect the principle of proximity by ensuring that the transport of waste is limited in distance and volume;
- ensure that resource use efficiency is improved and that resources are depleted.

*For further information*: Article L. 541-1 of the Environment Code.

**Obligation to keep a waste treatment register**

The professional who manages waste and recycles waste must maintain a chronological record of waste in and out of the facility, stating to them:

- Nature
- treatment
- Shipping.

In addition, each year, the professional is required to declare annually the nature and quantities of this waste.

The professional must keep this registry for at least five years.

*For further information*: Article L. 541-7 of the Environment Code; Articles 1 and 2 of the February 29, 2012 order setting out the contents of the records referred to in articles R. 541-43 and R. 541-46 of the Environment Code.

**Transportation of waste**

The professional who, during his activity, ensures the transport of waste is required to declare his company, as long as the amount of waste transported:

- is more than 0.1 tonnes per load of hazardous waste;
- 0.5 tonnes per load of non-hazardous waste.

Once the professional fulfils these conditions, he must send a declaration file to the prefect of the department where his head office is located (see infra "3°. b. Make a declaration for a waste transport activity").

**Please note**

Some professionals are not required to comply with this requirement, including ICPE operations under section 2790 of the ICPE (Dangerous Waste Treatment) nomenclature.

*For further information*: Article R. 541-50 and the following of the Environment Code.

**Subscribe to financial guarantees**

The professional is obliged to take out financial guarantees as long as his activity falls under the classified facilities for the protection of the environment subject to authorisation (see infra "3." b. If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)).

These financial guarantees may result in:

- a written commitment from a credit, insurance, financing or mutual bond institution;
- A guarantee fund managed by Ademe;
- a deposit in the hands of the Caisse des dépôts et consignments;
- a written commitment with a stand-alone guarantee from a person holding more than half of the operator's capital.

These financial guarantees must be provided for a minimum of two years and must be renewed at least three months before their maturity.

These financial guarantees are intended to cover the risks associated with expenses to:

- The safety of the site of the professional's operation;
- Interventions in the event of an accident or pollution;
- re-conditioned after its closure.

*For further information*: Article R. 516-1 and the following of the Environment Code; decree of 31 May 2012 setting out the list of classified facilities subject to the obligation to establish financial guarantees under article R. 516-1 of the Environment Code.

**Hazardous Waste Tracking Border (BSDD)**

The professional who manages and recycles products that are classified as hazardous according to the waste nomenclature (see supra "2." d. Waste Nomenclature") is required to fill out a waste tracking slip in accordance with the Cerfa form No.12571*01.

*For further information*: Article R. 541-45 of the Environment Code; order of 29 July 2005 setting out the hazardous waste tracking slip form mentioned in Article 4 of Decree No. 2005-635 of 30 May 2005.

3°. Installation procedures and formalities
------------------------------------------------------

### a. If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)

Depending on the type and amount of waste to be treated, waste management and recycling activity may fall under the regulations of classified environmental protection facilities (ICPE).

Thus, the professional will be required to determine the heading to which his activity belongs and to proceed if necessary, either to:

- A request for permission
- Pre-declaration
- registration of its activity in the prefecture.

It is advisable to refer to the nomenclature of the ICPE, available on the[Aida](https://aida.ineris.fr/) for more information.

### b. Make a declaration for a waste transport activity

**Competent authority**

The professional must send his file to the prefect of the department in which his head office is located or, failing that, his home.

**Supporting documents**

Its reporting file must include:

- a statement in accordance with the model set out in the Appendix I the order of 12 August 1998 relating to the composition of the declaration file and the receipt of declaration for the exercise of the waste transport activity;
- an extract from its registration in the Register of Trade and Companies (RCS) or, failing that, in the directory of trades, dating back less than three months.

**Time and procedure**

Upon receipt of the full file, the prefect sends the applicant a receipt in accordance with the model set out in the Appendix II 12 August 1998 above. The prefect issues a copy number equal to the number of vehicles assigned to transport waste.

**Please note**

This declaration must be renewed every five years.

### c. Company reporting formalities

In the event of the creation of a commercial company, the professional is required to register in the Register of Trade and Companies (RCS).

**Competent authority**

The professional must make a declaration to the Chamber of Commerce and Industry (CCI).

**Supporting documents**

In order to do so, it must provide supporting documents depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sent him a receipt on the same day indicating the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the time frames mentioned above.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

