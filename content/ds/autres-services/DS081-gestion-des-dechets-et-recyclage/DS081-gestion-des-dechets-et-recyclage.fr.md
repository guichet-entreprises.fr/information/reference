﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS081" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Gestion des déchets et recyclage" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="gestion-des-dechets-et-recyclage" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/gestion-des-dechets-et-recyclage.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="gestion-des-dechets-et-recyclage" -->
<!-- var(translation)="None" -->

# Gestion des déchets et recyclage

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

La gestion des déchets et recyclage est une activité qui consiste pour le professionnel à assurer la prise en charge des déchets de leur production à leur traitement. Cette activité comprend, la collecte, le transport, l'opération de valorisation (permettant aux déchets de servir à des fins utiles en remplacement d'autres substances), ou encore, l'élimination des déchets.

Le recyclage quant à lui, est une opération de valorisation qui consiste à traiter les déchets en vue de leur réutilisation soit à leur fin initialement prévue, soit pour une autre finalité.

*Pour aller plus loin* : article L. 541-1-1 du Code de l'environnement.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée. L'activité étant de nature commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

**Bon à savoir** 

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exerce une activité d'achat-revente son activité sera à la fois commerciale et artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucune qualification professionnelle n'est requise pour le professionnel souhaitant exercer l'activité de gestion des déchets et leur recyclage.

Cependant, lorsque son activité relève des installations classées pour la protection de l'environnement (ICPE), il sera tenu de procéder à des formalités spécifiques (cf. infra « 3°. a. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE) »).

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services (LPS) ou Libre établissement (LE))

Aucune disposition spécifique n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer l'activité de gestion des déchets et recyclage, à titre temporaire et occasionnel ou permanent, en France.

À ce titre, le ressortissant est tenu aux mêmes exigences que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation »).

### c. Responsabilité et sanctions

#### Responsabilité

Tout professionnel qui produit ou détient des déchets est responsable de leur gestion jusqu'à leur élimination ou leur valorisation et ce même en cas de traitement par un tiers.

*Pour aller plus loin* : article L. 541-2-1 du Code de l'environnement.

#### Sanctions

En cas de manquement aux obligations spécifiques en matière de gestion et de recyclage des déchets, le professionnel encourt une peine de deux ans d'emprisonnement et une amende de 75 000 euros.

*Pour aller plus loin* : article L. 541-46 du Code de l'environnement.

### d. Quelques particularités de la réglementation de l’activité

#### Nomenclature des déchets

Le professionnel est tenu de déterminer la nature des déchets dont il assure la gestion. Il doit également, faire figurer sur l'ensemble des documents de traçabilité le Code de ces déchets, établi selon la nomenclature fixée à l'annexe de la [décision n° 2000/532/CE](https://Aida.ineris.fr/consultation_document/33804) du 03/05/00 remplaçant la décision 94/3/CE établissant une liste de déchets en application de l'article 1er, point a), de la directive 75/442/CEE du Conseil relative aux déchets et la décision 94/904/CE du Conseil établissant une liste de déchets dangereux en application de l'article 1er, paragraphe 4, de la directive 91/689/CEE du Conseil relative aux déchets dangereux.

*Pour aller plus loin* : article R. 541-7 du Code de l'environnement.

#### Respect de la politique nationale de prévention et de gestion des déchets

Le professionnel exerçant l'activité de gestion des déchets et de recyclage est tenu au respect de la politique nationale de prévention et de gestion des déchets. Cette politique vise à favoriser l'économie dite circulaire.

À ce titre, le professionnel est notamment tenu de :

- prévenir et de réduire au maximum la production et la nocivité des déchets ;
- respecter la hiérarchie des modes de traitement des déchets ;
- veiller à ce que la gestion des déchets ne nuise ni à la santé humaine, ni à l'environnement ;
- respecter le principe de proximité en veillant à limiter en distance et en volume le transport des déchets ;
- veiller à améliorer l'efficacité de l'utilisation des ressources et d'économiser les ressources épuisables.

*Pour aller plus loin* : article L. 541-1 du Code de l'environnement.

#### Obligation de tenir un registre de traitement des déchets

Le professionnel qui assure la gestion des déchets et leur recyclage doit, en vue d'assurer leur traçabilité, tenir à jour un registre chronologique des déchets entrants et sortants de son installation, mentionnant leur :

- nature ;
- traitement ;
- expédition.

En outre, chaque année, le professionnel est tenu de déclarer annuellement nature et les quantités de ces déchets.

Le professionnel doit conserver ce registre pendant au moins cinq ans.

*Pour aller plus loin* : article L. 541-7 du Code de l'environnement ; articles 1 et 2 de l'arrêté du 29 février 2012 fixant le contenu des registres mentionnés aux articles R. 541-43 et R. 541-46 du Code de l'environnement.

#### Transport des déchets

Le professionnel qui, au cours de son activité, assure le transport de déchet est tenu de déclarer son entreprise, dès lors que la quantité de déchets transportés :

- est supérieure à 0,1 tonne par chargement de déchets dangereux ;
- est supérieure à 0,5 tonne par chargement de déchets non dangereux.

Dès lors qu'il remplit ces conditions, le professionnel doit adresser un dossier de déclaration auprès du préfet du département où se trouve son siège social (cf. infra « 3°. b. Effectuer une déclaration pour une activité de transport de déchets »).

**À noter**

Certains professionnels ne sont pas tenu au respect de cette exigence et notamment les exploitations d'ICPE relevant de la rubrique 2790 de la nomenclature des ICPE (Traitement des déchets dangereux).

*Pour aller plus loin* : article R. 541-50 et suivants du Code de l'environnement.

#### Souscrire à des garanties financières

Le professionnel est tenu de souscrire des garanties financières dès lors que son activité relève des installations classées pour la protection de l'environnement soumises à autorisation (cf. infra « 3°. b. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE) »).

Ces garanties financières peuvent résulter au choix :

- d'un engagement écrit d'un établissement de crédit, d'assurance, de financement ou de caution mutuelle ;
- d'un fonds de garantie géré par l'Ademe ;
- d'une consignation entre les mains de la Caisse des dépôts et consignations ;
- un engagement écrit portant garantie autonome d'une personne titulaire de plus de la moitié du capital de l'exploitant.

Ces garanties financières doivent être constituées pour une durée minimale de deux ans et doivent être renouvelées au moins trois mois avant leur échéance.

Ces garanties financières visent à couvrir les risques liés aux dépenses pour :

- la mise en sécurité du site de l'exploitation du professionnel ;
- les interventions en cas d'accident ou de pollution ;
- la remise en l'état du site après sa fermeture.

*Pour aller plus loin* : article R. 516-1 et suivants du Code de l'environnement ; arrêté du 31 mai 2012 fixant la liste des installations classées soumises à l'obligation de constitution de garanties financières en application du 5° de l'article R. 516-1 du Code de l'environnement.

#### Bordereau de suivi des déchets dangereux (BSDD)

Le professionnel qui assure la gestion et le recyclage de produits qualifiés de dangereux selon la nomenclature des déchets (cf. supra « 2°. d. Nomenclature des déchets ») est tenu de remplir un bordereau de suivi des déchets conformément au formulaire Cerfa n°12571*01.

*Pour aller plus loin* : article R. 541-45 du Code de l'environnement ; arrêté du 29 juillet 2005 fixant le formulaire de bordereau de suivi des déchets dangereux mentionné à l'article 4 du décret n°2005-635 du 30 mai 2005.

## 3°. Démarches et formalités d’installation
 
### a. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE)

Selon le type et la quantité de déchets à traiter, l'activité de gestion des déchets et de recyclage est susceptible de relever de la réglementation en matière d'installations classées pour la protection de l'environnement (ICPE).

Ainsi, le professionnel sera tenu de déterminer la rubrique à laquelle appartient son activité et de procéder le cas échéant, soit à :

- une demande d'autorisation ;
- une déclaration préalable ;
- l'enregistrement de son activité en préfecture.

Il est conseillé de se reporter à la nomenclature des ICPE, disponible sur le site de l'[Aida](https://aida.ineris.fr/) pour de plus amples informations.

### b. Effectuer une déclaration pour une activité de transport de déchets

#### Autorité compétente

Le professionnel doit adresser son dossier au préfet du département au sein duquel se trouve son siège social ou à défaut son domicile.

#### Pièces justificatives

Son dossier de déclaration doit comporter :

- une déclaration conformément au modèle fixé à l'annexe I de l'arrêté du 12 août 1998 relatif à la composition du dossier de déclaration et au récépissé de déclaration pour l'exercice de l'activité de transport de déchets ;
- un extrait de son inscription au registre du commerce et des sociétés (RCS) ou à défaut, au répertoire des métiers, datant de moins de trois mois.

#### Délai et procédure

Dès réception du dossier complet, le préfet adresse au demandeur, un récépissé conformément au modèle fixé à l'annexe II de l'arrêté du 12 août 1998 susvisé. Le préfet lui délivre un nombre de copie égal au nombre de véhicules affectés au transport des déchets.

**À noter**

Cette déclaration doit faire l'objet d'un renouvellement tous les cinq ans.

### c. Formalités de déclaration de l’entreprise

En cas de création d'une société commerciale, le professionnel est tenu de s'immatriculer au registre du commerce et des sociétés (RCS).

#### Autorité compétente

Le professionnel doit effectuer une déclaration auprès de la chambre de commerce et d'industrie (CCI).

#### Pièces justificatives

Pour cela, il doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI lui adresse le jour même, un récépissé indiquant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais indiqués ci-dessus.

Si le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.