﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS103" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Collecte des déchets : pneus" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="collecte-des-dechets-pneus" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/collecte-des-dechets-pneus.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="collecte-des-dechets-pneus" -->
<!-- var(translation)="None" -->

# Collecte des déchets : pneus

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité de collecte des déchets de pneumatiques consiste pour le professionnel (le collecteur) à assurer l'enlèvement des déchets auprès des producteurs ou des distributeurs, et à prendre en charge leur regroupement ainsi que leur transport vers des installations de traitement.

*Pour aller plus loin* : article R. 543-138 du Code de l'environnement.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée. L'activité étant de nature commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exerce une activité d'achat-revente son activité sera à la fois commerciale et artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité de collecte de pneus, le professionnel doit :

- être titulaire d'un agrément ;
- s'inscrire au sein du registre « Système déclaration des filières Responsabilité élargie du producteur (REP) » tenu par l'Agence de l'environnement et de la maîtrise de l'énergie (Ademe), (cf. infra « 3°. b. Procéder à son inscription au sein du registre de l'Ademe ») ;
- déclarer son activité auprès du préfet du département au sein duquel il souhaite exercer (cf. infra « 3°. c. Demande de déclaration d'activité »).

*Pour aller plus loin* : article R. 543-145 du Code de l'environnement.

#### Agrément

En vue d'obtenir son agrément, le professionnel doit :

- posséder les capacités financières et techniques suffisantes pour exercer son activité ;
- être titulaire d'un contrat ou d'une promesse de contrat avec au moins un producteur de pneumatiques ayant mis en place un système individuel de collecte ou contribuant à un éco-organisme prévu à cet effet ;
- s'engager à respecter l'ensemble des obligations prévues au sein du cahier des charges (cf. infra « 2°. c. Obligations professionnelles »).

Dès lors qu'il remplit ces conditions, le professionnel doit adresser une demande auprès du préfet du département au sein duquel se trouve son installation (cf. infra « 3°. a. Demande en vue d'obtenir un agrément »).

*Pour aller plus loin* : article R. 543-143 du Code de l'environnement.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services (LPS) ou Libre Établissement (LE))

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer l'activité de collecte de déchets de pneumatiques, à titre temporaire et occasionnel ou permanent, en France.

À ce titre, le professionnel est soumis aux mêmes exigences que le ressortissant français (cf. infra. « 3°. Démarches et formalités d'installation  »).

### c. Obligations professionnelles

Le professionnel titulaire d'un agrément est tenu de respecter l'ensemble des dispositions inscrites au sein du cahier des charges. Ainsi, il doit :

- procéder à la collecte des déchets de pneumatiques que les producteurs mettent à sa disposition dans chaque département où il est agréé ;
- transmettre au préfet, dans un délai de deux mois à compter de la délivrance de son agrément, l'ensemble des contrats conclus avec les producteurs ;
- procéder à la collecte des lots de déchets d'un poids égal ou supérieur à une tonne dans un délai de quinze jours suivant leur dépôt ;
- ramasser gratuitement les déchets déposés par les producteurs ayant mis en place un système individuel ou un éco-organisme de collecte pour les détenteurs ou les distributeurs dans les conditions de l'article R. 543-144 du Code de l'environnement ;
- remettre les déchets de pneumatiques uniquement aux personnes qui exploitent des installations de regroupement agréées ;
- communiquer à l'Agence de l'environnement et de la maîtrise de l'énergie (Ademe), au plus tard le 31 mars de l'année en cours, les quantités de déchets de pneumatiques collectés au cours de l'année précédente, ainsi que leur destination précise.

**À noter**

En cas de manquement à ces obligations, le préfet peut mettre en demeure le professionnel de présenter ses observations et, le cas échéant, procéder au retrait de son agrément.

*Pour aller plus loin* : article R. 543-146 du Code de l'environnement ; annexe de l'arrêté du 15 décembre 2015 relatif à la collecte des déchets de pneumatiques.

### d. Quelques particularités de la réglementation de l’activité

#### Hiérarchie de traitement des déchets

Le professionnel assurant la collecte des déchets de pneumatiques est tenu au respect de la politique nationale de prévention et de gestion des déchets. Cette politique prévoit une hiérarchie des modes de traitement des déchets en vue de favoriser l'économie dite circulaire.

À ce titre, les déchets de pneumatiques doivent faire l'objet :

- d'une préparation en vue de leur réutilisation ;
- d'un recyclage ;
- de modes de valorisation telle que la valorisation énergétique.

*Pour aller plus loin* : articles L. 541-1 et R. 543-140 du Code de l'environnement.

#### Déclaration annuelle auprès de l'Ademe

Le professionnel est tenu de déclarer par voie électronique à l'Ademe, chaque année avant le 31 mars, les quantités de déchets de pneumatiques collectées par catégorie, par type de détenteurs et par département.

Le modèle de cette déclaration est fixé à l'annexe 3 de l'arrêté du 30 décembre 2016 relatif à la communication d'informations relatives à la gestion des déchets de pneumatiques.

*Pour aller plus loin* : article R. 543-150 du Code de l'environnement.

#### Tenir un registre chronologique des déchets transportés ou collectés

Le professionnel doit tenir à jour un registre chronologique mentionnant pour chacun des déchets collectés :

- la date de son enlèvement et de son déchargement ;
- sa nature et sa quantité ;
- le numéro d'immatriculation du ou des véhicule(s) ayant assuré le transport ;
- le cas échéant, le numéro du bordereau de suivi et le numéro de notification ;
- le nom et l'adresse de la personne ayant procédé à sa remise au collecteur ;
- le nom et l'adresse de l'installation vers laquelle il a été expédié.

Ce registre doit être conservé pendant au moins trois ans.

*Pour aller plus loin* : article 3 de l'arrêté du 29 février 2012 fixant le contenu des registres mentionnés aux articles R. 541-43 et R. 541-46 du Code de l'environnement.

#### Garanties financières

Dès lors que son activité collecte un volume de déchets supérieur ou égal à 1 000 m³ et relève des installations classées pour la protection de l'environnement, le professionnel est tenu de souscrire des garanties financières.

Ces garanties peuvent résulter au choix :

- d'un engagement écrit d'un établissement de crédit, d'assurance, de financement ou de caution mutuelle ;
- d'un fonds de garantie géré par l'Ademe ;
- d'une consignation entre les mains de la Caisse des dépôts et consignations ;
- d'un engagement écrit portant garantie autonome d'une personne titulaire de plus de la moitié du capital de l'exploitant.

Ces garanties financières doivent être constituées pour une durée minimale de deux ans et doivent être renouvelées au moins trois mois avant leur échéance.

Ces garanties financières visent à couvrir les risques liés aux dépenses pour :

- la mise en sécurité du site de l'exploitation du professionnel ;
- les interventions en cas d'accident ou de pollution ;
- la remise en l'état du site après sa fermeture.

*Pour aller plus loin* : articles R. 516-1 et suivants du Code de l'environnement.

## 3°. Démarches et formalités d’installation

### a. Demande en vue d'obtenir un agrément 

#### Autorité compétente

Le professionnel doit adresser sa demande auprès du préfet du département au sein duquel il souhaite exercer. Cette demande doit être transmise en autant d'exemplaires que de départements concernés.

#### Pièces justificatives

Sa demande doit contenir :

- son identité en cas de demandeur personne physique ou la raison sociale, la forme juridique, l'adresse du siège social et la qualité du signataire de la demande en cas de demandeur personne morale ;
- un contrat ou une promesse d'engagement d'un ou plusieurs producteur(s) pour le compte duquel le professionnel souhaite collecter les déchets ainsi que les informations suivantes :
  - la liste des départements au sein desquels il souhaite exercer,
  - la description des moyens humains et matériels dont il dispose,
  - son engagement à respecter l'ensemble des obligations fixées au sein du cahier des charges ;
- en vue d'une activité de ramassage :
  - une copie du récépissé de la déclaration d'activité de transport par route de déchets,
  - les coordonnées des installations de regroupement agréées auprès desquelles le professionnel déposera les déchets après leur ramassage ;
- en vue d'une activité de regroupement :
  - une copie de l'autorisation d'exploiter ou un récépissé de déclaration d'activité,
  - la capacité de stockage maximale des déchets de pneumatiques.

#### Délais et procédure

Lors de la réception de la demande, le préfet transmet, pour avis, le dossier à la direction régionale de l'environnement, de l'aménagement et du logement ainsi que, pour information, aux préfets des départements concernés. Le préfet dispose d'un délai de six mois pour statuer sur la demande à compter de sa réception. Cet agrément est délivré pour une durée maximale de cinq ans.

**À noter**

Six mois avant son expiration, le professionnel doit adresser au préfet une nouvelle demande d'agrément.

*Pour aller plus loin* : articles 4 à 6 de l'arrêté du 15 décembre 2015 relatif à la collecte des déchets de pneumatiques.

### b. Procéder à son inscription au sein du registre de l'Ademe

#### Autorité compétente

Le professionnel doit procéder à son inscription au plus tard un mois avant sa première déclaration annuelle à l'Ademe.

#### Pièces justificatives

L'inscription se fait par voie électronique sur le [site du système déclaratif des filières REP](https://www.syderep.ademe.fr/).

*Pour aller plus loin* : article 1er de l'arrêté du 30 décembre 2016 relatif à la communication d'informations relatives à la gestion des déchets de pneumatiques.

### c. Demande de déclaration d'activité 

#### Autorité compétente

Le professionnel doit déposer une demande auprès du préfet du département au sein duquel se trouve son siège social ou à défaut, son domicile.

#### Pièces justificatives

Sa demande doit comprendre :

- une déclaration conforme au [modèle](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C7A4C308C6097CFE7090975B4CC9DA46.tplgfr22s_1?idArticle=LEGIARTI000006893211&cidTexte=LEGITEXT000005626655&dateTexte=20180424) fixé à l'annexe I de l'arrêté du 12 août 1998 relatif à la composition du dossier de déclaration et au récépissé de déclaration pour l'exercice de l'activité de transport de déchets ;
- un extrait de son inscription au registre du commerce et des sociétés (RCS) ou, à défaut, au répertoire des métiers, datant de moins de trois mois.

#### Délai et procédure

Dès réception du dossier complet de déclaration le préfet lui délivre :

- un récépissé conforme au [modèle](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C7A4C308C6097CFE7090975B4CC9DA46.tplgfr22s_1?idArticle=LEGIARTI000006893212&cidTexte=LEGITEXT000005626655&dateTexte=20180424) fixé à l'annexe II de l'arrêt du 12 août 1998 susvisé ;
- un nombre de copies conformes et numérotées de ce récépissé égal au nombre de véhicules affectés à la collecte des déchets. Le professionnel est tenu de conserver ces copies à bord de chacun de ses véhicules de collecte.

La déclaration doit faire l'objet d'un renouvellement tous les cinq ans.

*Pour aller plus loin* : articles R. 541-50 et suivants du Code de l'environnement.

### d. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE)

L'activité de collecte de déchets de pneumatiques peut être soumise à la réglementation relative aux installations classées pour la protection de l'environnement (ICPE).

L'activité est susceptible de relever de la rubrique 2714 : transit, regroupement ou tri de déchets non dangereux de papiers/cartons, plastiques, caoutchouc, textiles, bois.

Ainsi, selon le volume collecté de déchets, le professionnel sera tenu d'effectuer :

- une demande d'autorisation en cas de volume supérieur ou égal à 1 000 m³ ;
- une déclaration si ce volume est supérieur ou égal à 100 m³ mais inférieur à 1 000 m³.

Il est conseillé de se reporter à la nomenclature des ICPE, disponible sur le site de l'[AIDA](https://aida.ineris.fr/) pour de plus amples informations.

### e. Formalités de déclaration de l’entreprise

En cas de création d'une société commerciale, le professionnel est tenu de s'immatriculer au registre du commerce et des sociétés (RCS).

#### Autorité compétente

Le professionnel doit effectuer une déclaration auprès de la chambre de commerce et d'industrie (CCI).

#### Pièces justificatives

Pour cela, il doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI lui adresse le jour même un récépissé indiquant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais indiqués ci-dessus.

Si le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.