﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS103" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Waste tyre collection" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="waste-tyre-collection" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/waste-tyre-collection.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="waste-tyre-collection" -->
<!-- var(translation)="Auto" -->

Waste tyre collection
=======================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The activity of collecting tyre waste consists for the professional (the collector) to ensure the removal of waste from producers or distributors, and to take charge of their grouping and transport to processing facilities.

*For further information*: Article R. 543-138 of the Environment Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. As the activity is commercial in nature, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional carries out a buying and resale activity his activity will be both commercial and artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To carry out the tire collection activity, the professional must:

- Be licensed
- register in the register "System Declaration of The Extended Responsibility of the Producer (REP)" held by the Environment and Energy Management Agency (Ademe), (see infra "3." b. Proceed to register in the Ademe register");
- declare his activity with the prefect of the department in which he wishes to practice (see infra "3°. c. Request for declaration of activity").

*For further information*: Article R. 543-145 of the Environment Code.

**Approval**

In order to obtain accreditation, the professional must:

- Have the financial and technical capacity to carry out its business;
- Hold a contract or contract promise with at least one tyre producer who has set up an individual collection system or contributes to an eco-organism provided for this purpose;
- commit to complying with all of the obligations set out in the specifications (see infra "2°. c. Professional obligations").

Once the professional fulfils these conditions, he must apply to the prefect of the department in which his installation is located (see infra "3o). a. Application for approval").

*For further information*: Article R. 543-143 of the Environment Code.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There is no provision for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement to carry out the tyre waste collection activity, on a temporary and temporary basis. casual or permanent in France.

As such, the professional is subject to the same requirements as the French national (see below. "3°. Installation procedures and formalities").

### c. Professional obligations

The certified professional is required to comply with all the provisions in the specifications. Thus, he must:

- collecting tyre waste that producers make available to it in each department where it is approved;
- submit to the prefect, within two months of the issuance of his approval, all contracts with the producers;
- collect lots of waste weighing one tonne or more within a fortnight of deposit;
- collect waste deposited free of charge by producers who have set up an individual system or eco-collection agency for holders or distributors under the terms of Article R. 543-144 of the Environment Code;
- hand over tyre waste only to those operating licensed pooling facilities;
- communicate to the Environment and Energy Management Agency (Ademe) by 31 March of the current year the quantities of tyre waste collected in the previous year, as well as their precise destination.

**Please note**

In the event of a breach of these obligations, the prefect may order the professional to make his observations and, if necessary, withdraw his accreditation.

*For further information*: Article R. 543-146 of the Environment Code; annex to the 15 December 2015 order on the collection of tyre waste.

### d. Some peculiarities of the regulation of the activity

**Waste treatment hierarchy**

The professional responsible for the collection of tyre waste is bound by the national waste prevention and management policy. This policy provides for a hierarchy of waste treatment methods in order to promote the so-called circular economy.

As such, tyre waste must be subject to:

- Preparation for reuse
- Recycling
- development methods such as energy recovery.

*For further information*: Articles L. 541-1 and R. 543-140 of the Environment Code.

**Annual statement to Ademe**

The professional is required to report electronically to Ademe, each year before 31 March, the quantities of tyre waste collected by category, type of holders and by department.

The model of this statement is set at the[Appendix 3](http://www.bulletin-officiel.developpement-durable.gouv.fr/fiches/BO20171/bo20171.pdf) 30 December 2016 on the disclosure of information relating to the management of tyre waste.

*For further information*: Article R. 543-150 of the Environment Code.

**Keep a chronological record of waste transported or collected**

The professional must maintain a chronological record stating for each of the collected waste:

- The date of his abduction and unloading;
- its nature and quantity;
- The registration number of the vehicle or vehicles that provided the transport;
- If so, the number of the follow-up slip and the notification number;
- The name and address of the person who handed it over to the collector;
- the name and address of the facility to which it was shipped.

This register must be kept for at least three years.

*For further information*: Article 3 of the February 29, 2012 order setting out the contents of the records referred to in articles R. 541-43 and R. 541-46 of the Environment Code.

**Financial guarantees**

As long as its activity collects a volume of waste of 1,000 m3 or more and falls under facilities classified for the protection of the environment, the professional is obliged to take out financial guarantees.

These guarantees may result in:

- a written commitment from a credit, insurance, financing or mutual bond institution;
- A guarantee fund managed by Ademe;
- a deposit in the hands of the Caisse des dépôts et consignments;
- a written commitment with a stand-alone guarantee from a person holding more than half of the operator's capital.

These financial guarantees must be provided for a minimum of two years and must be renewed at least three months before their maturity.

These financial guarantees are intended to cover the risks associated with expenses to:

- The safety of the site of the professional's operation;
- Interventions in the event of an accident or pollution;
- re-conditioned after its closure.

*For further information*: Articles R. 516-1 and the following of the Environment Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Request for approval

**Competent authority**

The professional must apply to the prefect of the department in which he wishes to practice. This request must be sent in as many copies as from the departments concerned.

**Supporting documents**

His application must include:

- its identity in the case of a natural applicant or the name, the legal form, the address of the head office and the quality of the signatory of the application in the case of a legal applicant;
- a contract or promise of commitment from one or more producers for which the professional wishes to collect the waste and the following information:


  - the list of departments in which he wishes to practice,
  - the description of the human and material resources at its disposal,
  - its commitment to comply with all the obligations set out in the specifications;
- for a pickup activity:


  - A copy of the receipt for the declaration of road-to-road waste transport activity,
  - The contact information for the approved collection facilities from which the professional will deposit the waste after it has been collected;
- for a grouping activity:


  - A copy of the operating authorization or a receipt for a declaration of activity,
  - Maximum storage capacity for tyre waste.

**Delays and procedures**

Upon receipt of the application, the prefect sends the file to the regional directorate of environment, planning and housing for advice, as well as, for information, to the prefects of the departments concerned. The prefect has six months to decide on the application from the moment it is received. This accreditation is issued for a maximum of five years.

**Please note**

Six months before its expiry, the professional must submit a new application for accreditation to the prefect.

*For further information*: Articles 4 to 6 of the 15 December 2015 order on the collection of tyre waste.

### b. Proceed to register in the Ademe register

**Competent authority**

The professional must register no later than one month before his first annual declaration to Ademe.

**Supporting documents**

Registration is done electronically on the[site of the reporting system of the REP streams](https://www.syderep.ademe.fr/).

*For further information*: Article 1 of the 30 December 2016 decree on the disclosure of information relating to the management of tyre waste.

### c. Request for a declaration of activity

**Competent authority**

The professional must file an application with the prefect of the department where his head office is located or, failing that, his home.

**Supporting documents**

His request must include:

- a statement consistent with the [Model](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C7A4C308C6097CFE7090975B4CC9DA46.tplgfr22s_1?idArticle=LEGIARTI000006893211&cidTexte=LEGITEXT000005626655&dateTexte=20180424) ScheduleD for Schedule I of the 12 August 1998 order on the composition of the declaration file and the receipt of declaration for the exercise of the waste transport activity;
- an extract from its registration in the Register of Trade and Companies (RCS) or, failing that, in the directory of trades, dating back less than three months.

**Time and procedure**

Upon receipt of the full declaration file, the prefect issues:

- a receipt in accordance with the [Model](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=C7A4C308C6097CFE7090975B4CC9DA46.tplgfr22s_1?idArticle=LEGIARTI000006893212&cidTexte=LEGITEXT000005626655&dateTexte=20180424) Schedule II of the August 12, 1998 judgment above;
- a number of compliant and numbered copies of this receipt equal to the number of vehicles assigned to collect waste. The professional is required to keep these copies on board each of his collection vehicles.

The declaration must be renewed every five years.

*For further information*: Articles R. 541-50 and the following of the Environment Code.

### d. If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)

The tyre waste collection activity may be subject to the Regulations on Classified Facilities for environmental protection (ICPE).

The activity is likely to fall under the heading 2714: transit, grouping or sorting of non-hazardous waste of paper/cardboard, plastics, rubber, textiles, wood.

Thus, depending on the volume collected, the professional will be required to perform:

- a request for permission in case of volume greater than or equal to 1,000 m3;
- a declaration if this volume is greater than or equal to 100 m3 but less than 1,000 m3.

It is advisable to refer to the nomenclature of the ICPE, available on the[Aida](https://aida.ineris.fr/) for more information.

### e. Company reporting formalities

In the event of the creation of a commercial company, the professional is required to register in the Register of Trade and Companies (RCS).

**Competent authority**

The professional must make a declaration to the Chamber of Commerce and Industry (CCI).

**Supporting documents**

In order to do so, it must provide supporting documents depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sent him a receipt on the same day indicating the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the time frames mentioned above.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

