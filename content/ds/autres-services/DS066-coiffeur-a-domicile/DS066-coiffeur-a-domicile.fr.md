﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS066" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Coiffeur à domicile" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="coiffeur-a-domicile" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/coiffeur-a-domicile.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="coiffeur-a-domicile" -->
<!-- var(translation)="None" -->

# Coiffeur à domicile

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le coiffeur à domicile est un professionnel qui propose des soins esthétiques et hygiéniques de la chevelure, naturelle ou artificielle, sans installation fixe d'un matériel lourd et sophistiqué. Il se déplace au domicile des particuliers, dans les établissements collectifs (maisons de cure) ou encore sur leur lieu de travail et de villégiature.

Le coiffeur à domicile propose une gamme variée de prestations : coupe, défrisage, mise en plis, coloration, traitement du cuir chevelu, soins des cheveux, etc. Il peut également proposer la vente de produits, notamment cosmétiques, à sa clientèle.

*Pour aller plus loin* : loi n° 46-1173 du 23 mai 1946 portant réglementation des conditions d'accès à la profession de coiffeur. 

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour une activité commerciale, le CFE compétent est la chambre de commerce et d’industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Seule une personne qualifiée professionnellement peut exercer l’activité de coiffeur à domicile.

Sont considérées comme qualifiées professionnellement les personnes titulaires :

- du certificat d'aptitude professionnelle (CAP) « coiffure » ;
- ou d'un diplôme ou d'un titre de niveau égal ou supérieur homologué ou enregistré lors de sa délivrance au [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/). Il peut s’agir par exemple du brevet professionnel (BP) « coiffure ».

Une personne non qualifiée professionnellement peut créer une entreprise de coiffure à domicile mais ne peut pas exercer elle-même l’activité. Elle doit en déléguer l’exercice à une personne qualifiée (salarié, conjoint collaborateur ou associé).

**À noter**

Les coiffeurs pour hommes n’exerçant la profession que de manière accessoire ou en complément d’une autre profession ne sont pas soumis à l’obligation de qualification professionnelle pour exercer à la condition qu’ils interviennent dans les communes de moins de 2 000 habitants.

*Pour aller plus loin* : article 3 de la loi n° 46-1173 du 23 mai 1946 portant réglementation des conditions d'accès à la profession de coiffeur et décret n° 97-558 du 29 mai 1997 relatif aux conditions d’accès à la profession de coiffeur.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### Pour une Libre Prestation de Services (LPS)

Le professionnel ressortissant d’un État de l’Union européenne (UE) ou d’un État partie à l’accord sur l’Espace économique européen (EEE) peut exercer en France, à titre temporaire et occasionnel, l’activité de coiffeur à domicile à condition d’être légalement établi dans l’un de ces États pour y exercer cette activité.

Si ni l’activité ni la formation y conduisant ne sont réglementées dans l’État d’établissement, l’intéressé doit en outre prouver qu’il a exercé l’activité de coiffeur à domicile dans l’État où il est établi pendant au moins l’équivalent de deux ans à temps plein au cours des dix dernières années précédant la prestation qu’il veut réaliser en France.

Le professionnel souhaitant exercer de manière ponctuelle et occasionnelle en France est dispensé de l’obligation d'immatriculation au répertoire des métiers ou au registre des entreprises.

*Pour aller plus loin* : article 3-1 de la loi du 23 mai 1946 précitée. 

#### Pour un Libre Établissement (LE)

Pour exercer en France, à titre permanent, l’activité de coiffeur à domicile, le professionnel ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- avoir légalement exercé l’activité dans un État de l’UE ou de l’EEE pendant six années consécutives à titre indépendant ou en qualité de dirigeant. Dans ce cas, l’exercice de l’activité ne doit pas avoir pris fin depuis plus de dix ans au moment où l’intéressé sollicite auprès de la CMA la délivrance d’une attestation de qualification professionnelle (cf. infra « 3°. b. Le cas échéant, demander une attestation de qualification professionnelle » ) ;
- avoir légalement exercé l’activité dans un État de l’UE ou de l’EEE, pendant trois années consécutives, à titre indépendant ou en qualité de dirigeant d’entreprise après avoir reçu une formation préalable d’une durée minimale de trois ans sanctionnée par un certificat reconnu par l’État (ou jugé valable par un organisme professionnel compétent en vertu d’une délégation de l’État). La période d’exercice est portée à quatre années si la formation suivie n’a duré que deux ans ;
- avoir légalement exercé l’activité dans un État de l’UE ou de l’EEE, pendant trois années consécutives à titre indépendant et s’il a exercé l’activité en question à titre salarié, pendant au moins cinq ans. Dans ce cas, l’exercice de l’activité ne doit pas avoir pris fin depuis plus de dix ans au moment où l’intéressé sollicite de la CMA la délivrance d’une attestation de qualification professionnelle (cf. infra « 3°. b. Le cas échéant, demander une attestation de qualification professionnelle ») ;
- disposer d’une attestation de compétences ou d’un titre de formation requis par un État de l’UE ou de l’EEE qui réglemente l’accès ou l’exercice de l’activité de coiffure sur son territoire. Cette attestation, qui certifie un niveau de qualification professionnelle équivalent ou immédiatement inférieur à celle exigée pour les ressortissants français (cf. supra « 2°. a. Qualifications professionnelles »), est délivrée sur la base soit d’une formation ne donnant pas lieu à la délivrance d’un diplôme ou d’un certificat soit d’un examen sans formation préalable ou de l’exercice de la profession dans l’un des ces États pendant trois années ;
- disposer d’une attestation de compétences ou d’un titre de formation certifiant sa préparation à l’exercice de l’activité de coiffure, obtenu dans un État de l’UE ou de l’EEE qui ne réglemente pas l’accès ou l’exercice cette activité. Cette attestation, qui certifie un niveau de qualification professionnelle équivalent ou immédiatement inférieur à celle exigée pour les ressortissants français (cf. supra « 2°. a. Qualifications professionnelles »), est délivrée sur la base soit d’une formation ne donnant pas lieu à la délivrance d’un diplôme ou d’un certificat soit d’un examen sans formation préalable ou de l’exercice de la profession dans l’un des ces États pendant trois années ;
- être titulaire d’un diplôme, titre ou certificat permettant l’exercice de l’activité de coiffure acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE et avoir exercé cette activité pendant trois années dans l’État ayant admis l’équivalence.

Le professionnel qui remplit l’une des conditions précitées est considéré comme qualifié professionnellement pour s’établir en France. Il peut solliciter de la CMA l’obtention de l’attestation de qualification professionnelle pour exercer l’activité de coiffeur à domicile (cf. infra « 3°. b. Le cas échéant, demander une attestation de qualification professionnelle »).

De plus, le ressortissant doit disposer de connaissances suffisantes de la langue française pour pouvoir exercer la profession de coiffeur à domicile.

**À noter**

En cas de contrôle, le professionnel qualifié dispose d’un délai de quatre mois pour produire l'attestation de qualification professionnelle.

*Pour aller plus loin* : articles 5, 6 et 10 du décret du 29 mai 1997 précité. 

### c. Conditions d’honorabilité et incompatibilités

Nul ne peut exercer la profession s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévue au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 III de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat ; article 131-6 du Code pénal et article L. 653-8 du Code de commerce.

### d. Quelques particularités de la réglementation de l’activité

#### Titres professionnels

##### Le titre d’artisan

Les dirigeants d’entreprise artisanale, leur conjoint collaborateur ou associé et leurs associés prenant part personnellement et habituellement à l’activité de l’entreprise peuvent demander au président de la CMA l’obtention de la qualité d’artisan.

Ces personnes doivent justifier, au choix :

- d’un diplôme de niveau V dans le métier (CAP, BEP, etc.) ;
- d’un titre homologué de niveau V dans le métier exercé ou un métier connexe ;
- d’une expérience de trois années d’expérience dans le métier.

Pour plus d’informations, il est conseillé de se rapprocher de la CMA territorialement compétente.

##### Le titre de maître artisan

Pour obtenir le titre de maître artisan, l’intéressé (personne physique ou dirigeant d’une société artisanale) doit :

- être immatriculé au répertoire des métiers ;
- être titulaire du brevet de maîtrise (BM) ;
- justifier d’au moins deux ans de pratique professionnelle.

La demande doit être adressée au président de la CMA compétente.

**À savoir**

Les personnes qui ne sont pas titulaires du BM peuvent solliciter l’obtention du titre de maître artisan dans deux situations distinctes :

- si elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de formation équivalente au brevet de maîtrise, qu’elles disposent de connaissances en gestion et psychopédagogie équivalentes aux unités de valeur du brevet de maîtrise et qu’elles justifient de plus de deux ans de pratique professionnelle ;
- si elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles disposent d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

Dans ces deux cas, le titre de maître artisan peut être accordé par la commission régionale des qualifications.

Pour plus de précisions, il est conseillé de se rapprocher de la CMA territorialement compétente.

*Pour aller plus loin* : décret n° 98-247 du 2 avril 1998 précité.

##### Le titre de meilleur ouvrier de France

Le titre de meilleur ouvrier de France (MOF) est réservé aux personnes ayant réussi l’examen dénommé « concours un des meilleurs ouvriers de France ». Il s’agit d’un diplôme d’État qui atteste de l’acquisition d’une haute qualification dans l’exercice d’une activité professionnelle.

*Pour aller plus loin* : [site officiel du concours « un des meilleurs ouvriers de France »](http://www.meilleursouvriersdefrance.org/) ; articles D. 338-9 et suivants du Code de l'éducation et arrêté du 27 décembre 2012.

#### Publicité des tarifs

Le coiffeur à domicile a l’obligation d’informer le client, par voie de marquage, d'étiquetage, d'affichage ou par tout autre procédé approprié, des prix et des conditions particulières de la vente et de l'exécution de ses services. Pour l’ensemble des prestations fournies, la liste des prix hors taxe et toutes taxes comprises (TTC) doit être communiquée.

*Pour aller plus loin* : article L. 112-1 du Code de la consommation.

#### Obligation de délivrer une facture au client

Dans le cas où les prestations de coiffure et d'esthétique sont effectuées au domicile (ou dans tout autre lieu d'hébergement du client), le professionnel doit établir une fiche en triple exemplaire.

La facture doit notamment mentionner :

- l’identification du client, de l’entreprise et du salarié en cause ;
- la ou les prestation(s) effectuée(s) ;
- le mode de paiement ;
- le détail du montant de la prestation ;
- la signature du client et du professionnel.

*Pour aller plus loin* : article 12-3 de la convention collective nationale de la coiffure et des professions connexes du 10 juillet 2006.

#### Utilisation de produits nocifs

Les produits renfermant de l'acide thioglycolique, ses sels ou ses esters, d'une concentration comprise entre 8 % et 11 %, doivent être maniés avec précaution lors de leur usage, notamment lorsqu’il s’agit de les utiliser pour friser, défriser ou onduler les cheveux. Seules les personnes titulaires d’un brevet professionnel, d’un brevet de maîtrise, d’un titre équivalent ou dont la capacité professionnelle a été validée par la commission nationale de la coiffure sont autorisés à utiliser ces produits.

*Pour aller plus loin* : décret n° 98-848 du 21 septembre 1998 fixant les conditions d'usage professionnel de produits renfermant de l'acide thioglycolique, ses sels ou ses esters.

#### Réglementation d’une activité ambulante

En tant que professionnel n’exerçant pas de manière sédentaire, le coiffeur à domicile est soumis à des conditions d’exercice supplémentaires. Il doit par exemple obtenir la carte d’artisan ambulant.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Artisan-Commerçant ambulant](https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/commercant-artisan-ambulant.html) ».

## 3°. Démarches et formalités d’installation

### a. Le cas échéant, demander une attestation de qualification professionnelle

L’intéressé souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de reconnaissance de qualification professionnelle.

#### Autorité compétente

La demande doit être adressée à la CMA territorialement compétente, c’est-à-dire dans le ressort de laquelle l’intéressé souhaite exercer.

#### Procédure

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les 15 jours du dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

#### Pièces justificatives

Le dossier doit contenir :

- la demande d’attestation de qualification professionnelle ;
- le justificatif de la qualification professionnelle : une attestation de compétences ou le diplôme ou titre de formation professionnelle ;
- la preuve de la nationalité du demandeur ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE, une attestation portant sur la nature et de la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France, les justificatifs de l’exercice de l’activité pendant trois années.

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du centre international d’études pédagogiques (Ciep) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme ou d’un certificat ou d'un titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français par un traducteur agréé.

#### Délai de réponse

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA :

- reconnaît la qualification professionnelle et délivre l’attestation de qualification professionnelle ;
- décide de soumettre le demandeur à une mesure de compensation et lui notifie cette décision ;
- refuse de délivrer l’attestation de qualification professionnelle.

En l’absence de décision dans le délai de quatre mois, la demande d’attestation de qualification professionnelle est réputée acquise.

#### Mesures de compensation

La CMA peut soumettre le demandeur à une mesure de compensation consistant en un stage d’adaptation ou une épreuve d’aptitude. La mesure de compensation est exigée dans l’une des situations suivantes :

- si la durée de formation attestée est inférieure d’au moins un an à celle requise pour obtenir l’un des diplômes ou titres exigés pour exercer l’activité de coiffeur à domicile (cf. infra « 2°. a. Qualifications professionnelles ») ;
- si la formation reçue porte sur des matières substantiellement différentes de celles couvertes par l’un des diplômes ou titres exigés pour exercer l’activité de coiffeur à domicile (cf. infra « 2°. a. Qualifications professionnelles ») ;
- si l’exercice de l’activité de coiffeur à domicile (ou le contrôle effectif et permanent de cette activité) nécessite, pour l’exercice de certaines de ses attributions, une formation spécifique qui n’est pas prévue dans l’État membre d’origine et qui porte sur des matières substantiellement différentes de celles couvertes par l’attestation de compétences ou le titre de formation dont le demandeur fait état.

Avant de demander une mesure de compensation, la CMA vérifie si les connaissances acquises par le demandeur au cours de son expérience professionnelle dans un État de l’UE, de l’EEE ou un État tiers sont de nature à couvrir, totalement ou partiellement, la différence substantielle en termes de durée ou de contenu.

#### Coût

Gratuit. Cependant, si le ressortissant doit se soumettre à une mesure de compensation, il devra s’acquitter de frais d’instruction du dossier.

#### Voies de recours

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier, dans les deux mois suivant la notification du refus de la CMA, un recours contentieux devant le tribunal administratif compétent. De même, si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours gracieux auprès du préfet du département dans lequel la CMA a son siège, dans les deux mois suivant la notification de la décision de la CMA. S’il n’obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 précité ; arrêté du 28 octobre 2009 pris en application des décrets du 29 mai 1997 et du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.