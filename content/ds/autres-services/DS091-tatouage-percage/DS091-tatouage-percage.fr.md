﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS091" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Tatouage-perçage" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="tatouage-percage" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/tatouage-percage.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="tatouage-percage" -->
<!-- var(translation)="None" -->

# Tatouage-perçage

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le tatoueur-perceur est un professionnel qui met en œuvre des techniques de tatouage par effraction cutanée, y compris la technique du maquillage permanent et du perçage corporel, à l'exception du perçage du pavillon de l'oreille et de l'aile du nez quand il est réalisé par la technique du pistolet perce-oreille.

*Pour aller plus loin* : article R. 1311-1 du Code de la santé publique.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- en cas de création d’une entreprise individuelle, le CFE compétent est l’Urssaf ;
- en cas d’activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- en cas de création d’une société commerciale, le CFE compétent est la chambre de commerce et de l’industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

#### Obligation de suivre une formation relative aux conditions d’hygiène et de salubrité

Les personnes qui pratiquent les techniques de tatouage, de maquillage permanent et de perçage corporel doivent obligatoirement avoir suivi une formation relative aux conditions d'hygiène et de salubrité. Cette formation permet d’obtenir les connaissances obligatoires sur la stérilité du matériel utilisé, sur les règles d'hygiène à respecter lors du tatouage et sur l’hygiène du lieu d'exercice où sont réalisées ces pratiques.

##### Modalités de la formation

La formation dure au moins 21 heures réparties sur trois jours consécutifs.

##### Autorité compétente

La formation ne peut être dispensée que par l’un des organismes de formation agréés par l’agence régionale de santé (ARS) territorialement compétente, dont la liste figure sur [le site du ministère chargé de la santé](%5b*http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing*%5d(http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing)). À l’issue de la procédure, une attestation de formation est remise au participant.

**À noter**

Le professionnel doit afficher l’attestation de formation dans le lieu de réception de la clientèle.

##### Dispense de formation

Sont dispensées de la formation les personnes :

- titulaires d'un diplôme d’État de docteur en médecine ou d'un diplôme universitaire de spécialité hygiène hospitalière ;
- titulaires d’un « titre de formation » équivalent à l’un de ces titres.

*Pour aller plus loin* : articles R. 1311-3 et suivants du Code de la santé publique et arrêté du 12 décembre 2008 pris pour l'application de l'article R. 1311-3 du Code de la santé publique et relatif à la formation des personnes qui mettent en œuvre les techniques de tatouage par effraction cutanée et de perçage corporel.

#### L’activité de maquillage permanent ou semi-permanent

Pour pratiquer les techniques de maquillage permanent ou semi-permanent, l’intéressé doit disposer de qualifications professionnelles spécifiques, ou être placé sous le contrôle effectif et permanent d’une personne justifiant de telles qualifications.

Sont considérées comme qualifiées professionnellement les personnes qui, au choix :

- sont titulaires du certificat d’aptitude professionnelle (CAP) d'esthétique, du brevet d’études professionnelles (BEP) d'esthétique, d’un diplôme ou d’un titre de niveau égal ou supérieur homologué ou enregistré lors de sa délivrance au Répertoire national des certifications professionnelles ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) et délivré pour l’exercice du métier d'esthéticien ;
- justifient d’une expérience professionnelle de trois années effectives dans un État de l’Union européenne (UE) ou dans un État partie à l’accord sur l’Espace économique européen (EEE), acquise en qualité de dirigeant d’entreprise, de travailleur indépendant ou de salarié dans l’exercice du métier. Dans ce cas, il est conseillé à l’intéressé de s’adresser à la chambre des métiers et de l’artisanat (CMA) pour demander une attestation de reconnaissance de qualification professionnelle.

*Pour aller plus loin* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l’artisanat ; décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l’exercice des activités prévues à l’article 16 de la loi n° 96-603 du 5 juillet 1996 précitée.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### En cas de Libre Prestation de Services

Le ressortissant d'un État de l’UE ou de l’EEE souhaitant exercer cette activité de manière occasionnelle et temporaire en France est soumis à une obligation de formation et de déclaration, dont les modalités peuvent différer de celles applicables aux ressortissants français.

Concernant la formation, les ressortissants peuvent choisir entre :

- suivre la formation dite « classique », c’est-à-dire la même formation que celle à laquelle sont soumis les ressortissants français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- ou participer à une formation « spécifique », préalable à la manifestation à laquelle le ressortissant veut participer en France. Dans ce cas, cette formation est conduite sous la responsabilité de l'organisateur de l'événement.

##### Modalités de la formation « spécifique »

La formation dure au moins sept heures et n'est valable que pour la manifestation ou l’événement pour lequel elle a été délivrée.

##### Autorité compétente

La formation « spécifique » est délivrée par l’un des organismes de formation agréés par l’agence régionale de santé territorialement compétente, dont la liste figure sur [le site du ministère chargé de la santé](%5b*http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing#nb2-18*%5D(http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing#nb2-18)).

*Pour aller plus loin* : arrêté du 23 décembre 2008 fixant les modalités de déclaration des activités de tatouage par effraction cutanée, y compris de maquillage permanent, et de perçage corporel. 

#### En cas de Libre Établissement

Le ressortissant d’un État de l’UE ou de l’EEE souhaitant s’établir en France pour y exercer l’activité de tatoueur-perceur de façon permanente doit répondre aux mêmes conditions de formation que les nationaux (c’est-à-dire se soumettre à une obligation de formation et de déclaration préalable d’activité).

Sous certaines conditions, le ressortissant peut obtenir une dispense de formation. Pour plus de précisions, il est conseillé de se reporter à l’article 8 de l’arrêté du 12 décembre 2008.

*Pour aller plus loin* : article 8 de l’arrêté du 12 décembre 2008 pris pour l’application de l’article R. 1311-3 du Code de la santé publique et relatif à la formation des personnes qui mettent en œuvre les techniques de tatouage par effraction cutanée et de perçage corporel.

### c. Quelques particularités de la réglementation de l’activité

#### Règles d’accessibilité et de sécurité

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

#### Règles relatives à l’hygiène et à la salubrité

Les professionnels exécutant des techniques de tatouage-perçage doivent respecter les règles suivantes :

- le matériel pénétrant la barrière cutanée ou entrant en contact avec la peau ou la muqueuse du client et les supports directs de ce matériel doit être soit à usage unique et stérile, soit stérilisé avant chaque utilisation ;
- les locaux doivent comprendre une salle exclusivement réservée à la réalisation de ces techniques. Cette salle doit être nettoyée tous les jours et les surfaces utilisées doivent être désinfectées entre chaque client ;
- le professionnel doit retirer ses bijoux et porter des gants stériles, changés pour chaque client. Les gants sont également changés, pour un même client, après tout geste septique en cours d'acte et en cas de perçages successifs sur des zones corporelles différentes. Il doit préparer la zone à percer ou à tatouer en utilisant un antiseptique ;
- le professionnel doit utiliser des pinces ou aiguilles stérilisées et à usage unique. La table de travail doit être désinfectée, équipée d'un champ stérile à usage unique.

*Pour aller plus loin* : article R. 1311-4 et suivants du Code de la santé publique ; arrêté du 11 mars 2009 relatif aux bonnes pratiques d'hygiène et de salubrité pour la mise en œuvre des techniques de tatouage par effraction cutanée, y compris de maquillage permanent et de perçage corporel, à l'exception de la technique du pistolet perce-oreille ; arrêté du 6 mars 2013 fixant la liste des substances qui ne peuvent pas entrer dans la composition des produits de tatouage.

#### Règles relatives à l’élimination des déchets

Les déchets produits sont assimilés aux déchets d'activités de soins à risques infectieux. Par conséquent, leur élimination est soumise à des dispositions législatives strictes (séparation, tri, collecte, conditionnement, étiquetage, procédure spécifique d’élimination ou désinfection, etc.). Pour plus de précisions, il est conseillé de se reporter aux articles R. 1335-5 à 1335-8 et R. 1335-13 à 1335-14 du Code de la santé publique.

*Pour aller plus loin* : article R. 1311-5 du Code de la santé publique.

#### Règles relatives aux produits utilisés pour le tatouage-perçage

Les tiges utilisées lors d'un perçage initial jusqu'à cicatrisation et les tiges utilisées après cicatrisation doivent être conformes à certaines dispositions, notamment celles sur le nickel.

Les produits utilisés pour réaliser un tatouage doivent être conformes aux dispositions des articles L. 513-10-1 et suivants du Code de la santé publique et correspondre à la définition suivante : « toute substance ou préparation colorante destinée, par effraction cutanée, à créer une marque sur les parties superficielles du corps humain à l'exception des produits qui sont des dispositifs médicaux ». Le professionnel doit donc veiller, par exemple, à n’utiliser que des pigments autorisés dans les produits de tatouage qu’il utilise.

*Pour aller plus loin* : articles L. 513-10-1, L513-10-2 et R. 1311-10 du Code de la santé publique et arrêté du 6 mars 2013 fixant la liste des substances qui ne peuvent pas entrer dans la composition des produits de tatouage.

#### Règles relatives à l’information du client avant toute prestation concernant les risques auxquels il s’expose : information orale et affichage

Les personnes qui mettent en œuvre les techniques de tatouage et de piercing, y compris par pistolet perce-oreille, doivent informer leurs clients, avant qu’ils ne se soumettent à ces techniques, des risques auxquels ils s’exposent et, après la réalisation des gestes, des précautions à respecter.

##### Obligation d’affichage visible

Le professionnel doit afficher, dans les lieux dans lesquels il effectue des interventions, une information sur les risques encourus par le client et sur les précautions à respecter après l’intervention.

##### Obligation d’information orale du client

Le professionnel doit informer oralement le client, selon la technique pratiquée, des éléments suivants :

- le caractère irréversible des tatouages impliquant une modification corporelle définitive ;
- le caractère éventuellement douloureux des actes ;
- les risques d’infection ;
- les risques allergiques, notamment liés aux encres de tatouage et aux bijoux de piercing ;
- les recherches de contre-indications au geste liées au terrain ou aux traitements que suit le client ;
- le temps de cicatrisation adapté à la technique qui a été mise en œuvre et les risques cicatriciels ;
- les précautions à respecter après la réalisation des techniques, notamment pour permettre une cicatrisation rapide.

##### Obligation d’information écrite du client

L’ensemble des informations transmises oralement au client doit également lui être remis par écrit. Ce texte est complété, le cas échéant, par des indications sur les soins après la réalisation du geste.

*Pour aller plus loin* : article R. 1311-12 du Code de la santé publique et arrêté du 3 décembre 2008 précité.

#### Clients mineurs

Il est interdit de pratiquer une intervention de tatouage, maquillage permanent ou de perçage corporel sur une personne mineure sans avoir recueilli préalablement le consentement écrit d’une personne titulaire de l’autorité parentale ou de son tuteur. Le professionnel doit conserver la preuve de ce consentement pendant trois ans.

*Pour aller plus loin* : article R. 1311-11 du Code de la santé publique.

#### Dispositions relatives au perçage de l’aile du nez et du pavillon de l’oreille

Les techniques de perçage de l’aile du nez et du pavillon de l’oreille ne peuvent être mises en œuvre que par les professionnels qui, au choix :

- ont effectué la déclaration d’activité de tatoueur-perceur (cf. infra « 3°. b. Effectuer une déclaration d’activité ») ;
- ont une activité principale relevant du code NAF 47.77Z : « Commerce de détail d’articles d’horlogerie et de bijouterie en magasin spécialisé » ou 32.12Z « Fabrication d’articles de joaillerie et bijouterie » ;
- relèvent des conventions collectives suivantes :
  - convention collective nationale du commerce de détail de l’horlogerie-bijouterie,
  - convention collective nationale de la bijouterie, joaillerie, orfèvrerie et activités s’y rapportant.

*Pour aller plus loin* : article R. 1311-7 du Code de la santé publique ; arrêté du 29 octobre 2008 pris pour l’application de l’article R. 1311-7 du Code de la santé publique et relatif au perçage par la technique du pistolet perce-oreille et arrêté du 11 mars 2009 relatif aux bonnes pratiques d’hygiène et de salubrité pour la mise en œuvre du perçage du pavillon de l’oreille et de l’aile du nez par la technique du pistolet perce-oreille.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Effectuer une déclaration d’activité

Le professionnel souhaitant mettre en œuvre une ou plusieurs technique(s) de tatouage, de maquillage permanent ou de perçage corporel doit en faire une déclaration préalable.

#### Autorité compétente

La déclaration préalable doit être adressée au directeur général de l'agence régionale de santé (ARS) du lieu d’exercice de l’activité, préalablement au démarrage de l’activité.

#### Délais

Toute demande de déclaration complète donne lieu à l’envoi d’un récépissé au déclarant. En revanche, si la déclaration est irrégulière ou incomplète, le déclarant est invité à régulariser ou compléter sa déclaration.

#### Pièces justificatives

La déclaration s'effectue sur papier libre et doit mentionner :

- les nom et prénom du déclarant ;
- l'adresse du ou des lieu(x) d'exercice de l'activité ;
- la nature de la ou des technique(s) mise(s) en œuvre ;
- l'attestation de formation ou le titre accepté en équivalence.

#### Coût

Gratuit.

**Bon à savoir**

Le processus de déclaration d’activité ne concerne pas les personnes qui mettent en œuvre le perçage par pistolet perce-oreille (du pavillon de l’oreille et de l’aile du nez).

**À noter**

Même lorsque le professionnel n’exerce pas plus de 5 jours ouvrés par an sur un lieu, il doit déclarer cette activité. Pour plus de précisions, sur la procédure de déclaration, il est conseillé de se rapporter aux articles 5 et 6 de l’arrêté du 23 décembre 2008.

*Pour aller plus loin* : article R. 1311-2 du Code de la santé publique et arrêté du 23 décembre 2008 fixant les modalités de déclaration des activités de tatouage par effraction cutanée, y compris de maquillage permanent, et de perçage corporel.