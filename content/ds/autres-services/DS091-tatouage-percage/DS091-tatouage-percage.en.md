﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS091" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Tattoo-piercing" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="tattoo-piercing" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/tattoo-piercing.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="tattoo-piercing" -->
<!-- var(translation)="Auto" -->


Tattoo-piercing
===============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1. Definition of activity0
--------------------------

### a. Definition

The tattoo artist is a professional who implements skin break-in tattoo techniques, including permanent makeup and body piercing, with the exception of the piercing of the ear pavilion and nose wing when it is made by the technique of the ear-piercing pistol.

*For further information*: Article R. 1311-1 of the Public Health Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- In the case of the creation of an individual company, the competent CFE is the Urssaf;
- In case of artisanal activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- In the event of the creation of a commercial company, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

#### Obligation to undergo hygiene and safety training

People who practice tattooing, permanent make-up and body piercing techniques must have undergon training in hygiene and safety conditions. This training provides the necessary knowledge on the infertility of the equipment used, on the hygiene rules to be respected during tattooing and on the hygiene of the place of practice where these practices are carried out.

**Training terms**

The training lasts at least 21 hours spread over three consecutive days.

**Competent authority**

Training can only be provided by one of the training bodies accredited by the territorially competent regional health agency (ARS), whose list is listed on [Ministry of Health's website](%5b*http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing*%5d(http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing)). At the end of the procedure, a certificate of training is given to the participant.

###### Note that

The professional must display the training certificate in the customer reception area.

**Training exemption**

Training includes:

- holders of a state medical degree or a university degree in hospital hygiene;
- holders of a "training title" equivalent to one of these titles.

*For further information*: Articles R. 1311-3 and the following of the Public Health Code and arrested on December 12, 2008 for the application of Article R. 1311-3 of the Public Health Code and relating to the training of persons who implement burglary techniques skin and body piercing.

#### Permanent or semi-permanent make-up activity

To practice permanent or semi-permanent make-up techniques, the person concerned must have specific professional qualifications, or be placed under the effective and permanent control of a person justifying such qualifications.

Professionally qualified individuals who, at the choice:

- hold the Certificate of Professional Fitness (CAP) of Aesthetics, the Certificate of Professional Studies (BEP) of Aesthetics, a diploma or a title of equal or higher level approved or registered at the time of its issuance to the national directory professional certifications ([RNCP](%5b*http://www.rncp.cncp.gouv.fr/*%5d(http://www.rncp.cncp.gouv.fr/))) and issued for the practice of the work of beautician;
- justify an effective three-year work experience in a European Union (EU) state or in a state party to the European Economic Area (EEA) agreement, acquired as a business manager, self-employed or self-employed employee in the line of duty. In this case, the person concerned is advised to contact the Chamber of Trades and Crafts (CMA) to request a certificate of recognition of professional qualification.

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Decree 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act 96-603 of 5 July 1996.

### b. Professional Qualifications - European Nationals (LPS or LE)

#### In case of Freedom to provide services

A national of an EU or EEA state wishing to carry out this activity on an occasional and temporary basis in France is subject to a training and reporting obligation, the terms of which may differ from those applicable to French nationals.

When it comes to training, nationals can choose between:

- follow the so-called "classic" training, that is, the same training to which French nationals are subjected (see above: "2. a. Professional qualifications");
- or participate in a "specific" training, prior to the event in which the national wants to participate in France. In this case, this training is conducted under the responsibility of the event organizer.

**Terms of "specific" training**

The training lasts at least seven hours and is only valid for the event or event for which it was issued.

**Competent authority**

The "specific" training is delivered by one of the training bodies accredited by the territorially competent regional health agency, whose list is listed on [Ministry of Health's website](%5b*http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing#nb2-18*%5D(http://social-sante.gouv.fr/soins-et-maladies/qualite-des-soins-et-pratiques/securite/article/tatouage-et-piercing#nb2-18)).

*For further information*: Order of December 23, 2008 setting out the reporting of skin break-in activities, including permanent make-up, and body piercing.

#### In case of Freedom of establishment

The national of an EU or EEA state wishing to settle in France to carry out the activity of tattoo-piercer must meet the same training conditions as nationals (i.e. submit to a duty to training and pre-reporting of activity).

Under certain conditions, the national may obtain a training exemption. For further clarification, it is advisable to refer to Article 8 of the decree of 12 December 2008.

*For further information*: Article 8 of the December 12, 2008 order for the application of Section R. 1311-3 of the Public Health Code and relating to the training of persons who implement skin-breaking tattooing and body piercing techniques.

### c. Some peculiarities of the regulation of the activity

#### Accessibility and safety rules

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

#### Hygiene and safety rules

Professionals performing tattoo-piercing techniques must follow the following rules:

- Material penetrating the skin barrier or coming into contact with the customer's skin or mucous membrane and the direct supports of this material must be either single-use and sterile or sterilized before each use;
- the premises must include a room exclusively reserved for the realization of these techniques. This room must be cleaned daily and the surfaces used must be disinfected between each customer;
- the professional must remove his jewelry and wear sterile gloves, changed for each client. The gloves are also changed, for the same client, after any septic gesture during the act and in case of successive piercings on different bodily areas. He must prepare the area to pierce or tattoo using an antiseptic;
- the professional must use sterilized, single-use tongs or needles. The work table must be disinfected, equipped with a sterile single-use field.

*For further information*: Article R. 1311-4 and the following of the Public Health Code; Order of 11 March 2009 on good hygiene and safety practices for the implementation of skin break-in techniques, including permanent make-up and body piercing, with the exception of the ear-piercing gun technique; decree of 6 March 2013 setting out the list of substances that cannot be included in the composition of tattoo products.

#### Waste disposal rules

The waste produced is equated with waste from infectious risk care activities. Therefore, their disposal is subject to strict legislation (separation, sorting, collection, packaging, labelling, specific disposal or disinfection procedure, etc.). For more information, please refer to sections R. 1335-5 at 1335-8 and R. 1335-13 to 1335-14 of the Public Health Code.

*For further information*: Article R. 1311-5 of the Public Health Code.

#### Rules for products used for tattoo-piercing

The stems used during an initial drilling until healing and the stems used after healing must comply with certain provisions, including those on nickel.

Products used to perform a tattoo must comply with the provisions of Sections L. 513-10-1 and the following of the Public Health Code and comply with the following definition: "any substance or coloring preparation intended, by break and enter to create a mark on the superficial parts of the human body with the exception of products that are medical devices." The professional must therefore be careful, for example, to use only authorized pigments in the tattoo products he uses.

*For further information*: Articles L. 513-10-1, L513-10-2 and R. 1311-10 of the Public Health Code and ordered from March 6, 2013 to list substances that cannot be included in the composition of tattoo products.

#### Rules for informing the client before any benefit regarding the risks to which he or she is exposed: oral information and display

People who implement tattoo and piercing techniques, including ear-piercing pistols, should inform their clients, before they submit to these techniques, of the risks to which they are exposed and, after the completion of the precautions to be taken.

**Visible display requirement**

The professional must post information about the risks to the client and the precautions to be taken after the procedure in the places in which he performs interventions.

**Customer's duty to provide oral information**

The professional must inform the client orally, depending on the technique used, of the following:

- The irreversible nature of tattoos involving permanent body modification;
- The potentially painful nature of the acts;
- The risk of infection
- allergic risks, including tattoo inks and piercing jewelry;
- Searching for contraindications to the terrain or treatments the client is undergoing;
- The healing time adapted to the technique that has been implemented and the scarring risks;
- precautions to be followed after the techniques have been carried out, in particular to allow rapid healing.

**Customer's written information obligation**

All information transmitted orally to the client must also be given to the client in writing. This text is supplemented, if necessary, by indications on the care after the realization of the gesture.

*For further information*: Article R. 1311-12 of the Public Health Code and arrested on December 3, 2008.

#### Minor customers

It is forbidden to perform a tattoo, permanent make-up or body piercing procedure on a minor without first obtaining the written consent of a person with parental authority or his guardian. The professional must keep proof of this consent for three years.

*For further information*: Article R. 1311-11 of the Public Health Code.

#### Provisions for the piercing of the nose wing and ear pavilion

The drilling techniques of the nose wing and the ear pavilion can only be implemented by professionals who, at their choice:

- carried out the tattoo-piercer activity statement (see below: "3. b. Make a declaration of activity");
- have a core activity under the code NAF 47.77Z: "Retail trade in watch and jewellery in specialty stores" or 32.12Z "Manufacturing jewellery and jewellery items";
- collective agreements:


  - national collective agreement for the retail trade in watch and jewellery,
  - national collective agreement for jewellery, jewellery, silverware and related activities.

*For further information*: Article R. 1311-7 of the Public Health Code; order of 29 October 2008 for the application of Article R. 1311-7 of the Code of Public Health and relating to the piercing by the machine gun technique and stopped on 11 March 2009 relating to good hygiene and safety practices for the implementation of the piercing the ear pavilion and nose wing by the ear-piercing gun technique.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the "Formality of Reporting of a Commercial Company," "Individual Business Registration in the Register of Trade and Companies" and "Corporate Reporting Formalities. artisanal work."

### b. Make a declaration of activity

The professional wishing to implement one or more tattooing, permanent make-up or body piercing techniques must make a prior declaration.

**Competent authority**

The prior declaration must be addressed to the Director General of the Regional Health Agency (ARS) of the place where the activity takes place, prior to the start of the activity.

**Timeframe**

Any request for a full declaration results in a receipt being sent to the registrant. On the other hand, if the declaration is irregular or incomplete, the registrant is invited to regularize or complete his declaration.

**Supporting documents**

The declaration is made on free paper and must mention:

- The name and surname of the registrant;
- The address of the place or place where the activity takes place;
- The nature of the technique or technique implemented;
- certificate of training or the title accepted in equivalence.

**Cost**

Free.

**Good to know**

The activity reporting process does not involve those who implement ear-piercing (ear and nose wing) gun piercing.

**Please note**

Even when the professional does not work more than 5 working days a year in a place, he must declare this activity. For more details, on the reporting procedure, it is advisable to refer to Articles 5 and 6 of the order of 23 December 2008.

*For further information*: Article R. 1311-2 of the Public Health Code and ordered on December 23, 2008 setting out the reporting of skin break-in activities, including permanent makeup, and body piercing.

