﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS104" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Traitement des déchets : huiles" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="traitement-des-dechets-huiles" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/traitement-des-dechets-huiles.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="traitement-des-dechets-huiles" -->
<!-- var(translation)="None" -->

# Traitement des déchets : huiles

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité de traitement des déchets d'huiles usagées consiste pour le professionnel, à valoriser ou, en dernier recours, éliminer les huiles devenues impropres à l'usage pour lequel elles étaient destinées.

Sont considérées comme des huiles usagées :

- les huiles des moteurs à combustion et des systèmes de transmission ;
- les huiles lubrifiantes ;
- les huiles pour turbines ;
- les huiles pour systèmes hydrauliques.

L'opération de valorisation vise à permettre aux déchets de servir des fins utiles, et à les préparer en vue de leur réemploi.

*Pour aller plus loin* : article R. 543-3 du Code de l'environnement ; article 3 de la directive 2008/98/CE du Parlement européen et du Conseil du 19 novembre 2008 relative aux déchets et abrogeant certaines directives.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

L'activité étant de nature commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exerce une activité d'achat-revente son activité sera à la fois commerciale et artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le professionnel qui souhaite exploiter une installation de traitement des huiles usagées doit obtenir un agrément, dont les modalités de délivrance varient selon la nature de l'activité exercée.

Ainsi, lorsque l'activité du professionnel relève du régime des installations classées pour la protection de l'environnement (ICPE), son agrément lui est délivré dans les conditions suivantes :

- en même temps que l'arrêté d'autorisation ou d'enregistrement ;
- dès lors que cet arrêté d'autorisation ou d'enregistrement mentionne la nature, l'origine, les quantités maximales admises et les conditions de traitement des déchets.

**À noter**

Lorsque l'installation est déjà autorisée ou soumise à déclaration, le professionnel est réputé agréé dès lors que les mentions susvisées sont énoncées au sein de l'arrêté d'autorisation, d'enregistrement ou de la déclaration.

À défaut, le professionnel est tenu d'adresser à l'autorité compétente, une déclaration complémentaire comportant ces informations.

En outre, pour exploiter une installation d'élimination de ces huiles, le professionnel doit obtenir un agrément spécifique (cf. infra « 3°. a. Demande d'agrément en vue de l'exploitation d'une installation d'élimination »).

*Pour aller plus loin* : articles L. 541-22 ; R. 543-13 et R. 515-37 du Code de l'environnement.

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services (LPS) ou Libre établissement (LE))

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'un exercice temporaire et occasionnel ou permanent en France.

À ce titre, le ressortissant légalement établi dans un État membre et exerçant l'activité de traitement des déchets d'huiles, est soumis aux mêmes exigences que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation »).

### c. Règles professionnelles

#### Respect du cahier des charges

Le professionnel titulaire d'un agrément en vue de l'élimination des déchets est tenu au respect d'un cahier des charges et doit, à ce titre :

- tenir une comptabilité mentionnant :
  - les quantités et les dates de réception des huiles usagées,
  - les caractéristiques physico-chimiques des produits issus de la régénération ou du recyclage,
  - les destinataires, 
  - le cas échéant, les tonnages éliminés des produits incinérés ;
- délivrer au ramasseur agréé un bordereau de prise en charge des produits comprenant leur tonnage et leur qualité ;
- disposer d'une capacité minimale de stockage égale au douzième de la capacité annuelle d'élimination de l'installation ;
- le cas échéant, prendre l'ensemble des dispositions permettant d'assurer le stockage transitoire des huiles en cas de suspension ou de cessation d'activité ;
- transmettre tous les mois à l'Agence de l'environnement et de la maîtrise de l'énergie (Ademe) l'ensemble des statistiques techniques et économiques relatives à son activité (le tonnage réceptionné et traité, les prix de reprise) ;
- afficher les prix de reprise pratiqués.

*Pour aller plus loin* : annexe de l'arrêté du 28 janvier 1999 relatif aux conditions d'élimination des huiles usagées.

#### Sanctions

Le professionnel qui exerce l'activité de traitement des déchets encourt une amende de 750 euros dès lors qu'il :

- ne tient aucun registre de son activité ;
- refuse de remettre ce registre aux agents en cas de contrôle ;
- omet de transmettre la déclaration annuelle à l'Ademe en cas d'exploitation d'une ICPE ;

*Pour aller plus loin* : R. 541-78 du Code de l'environnement.

### d. Quelques particularités de la réglementation de l’activité

#### Garanties financières

Le professionnel est tenu de souscrire des garanties financières dès lors que son activité relève des installations classées pour la protection de l'environnement soumises à autorisation (cf. infra « 3°. b. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE) »).

Ces garanties financières peuvent résulter au choix :

- d'un engagement écrit d'un établissement de crédit, d'assurance, de financement ou de caution mutuelle ;
- d'un fonds de garantie géré par l'Ademe ;
- d'une consignation entre les mains de la Caisse des dépôts et consignations ;
- d'un engagement écrit portant garantie autonome d'une personne titulaire de plus de la moitié du capital de l'exploitant.

Ces garanties financières doivent être constituées pour une durée minimale de deux ans et doivent être renouvelées au moins trois mois avant leur échéance.

Ces garanties financières visent à couvrir les risques liés aux dépenses pour :

- la mise en sécurité du site de l'exploitation du professionnel ;
- les interventions en cas d'accident ou de pollution ;
- la remise en l'état du site après sa fermeture.

*Pour aller plus loin* : article R. 516-1 et suivants du Code de l'environnement ; arrêté du 31 mai 2012 fixant la liste des installations classées soumises à l'obligation de constitution de garanties financières en application du 5° de l'article R. 516-1 du Code de l'environnement.

#### Hiérarchie des modes de traitement des déchets

Le traitement des huiles usagées doit s'effectuer dans le respect de la politique nationale de prévention et de gestion des déchets.

À ce titre, le traitement des huiles usagées doit privilégier :

- la préparation en vue de la réutilisation ;
- le recyclage ;
- la valorisation énergétique ;
- l'élimination.

*Pour aller plus loin* : article L. 541-1 et suivants du Code de l'environnement.

#### Tenir un registre chronologique de l'activité

Le professionnel doit tenir à jour un registre chronologique de son activité de traitement des déchets.

Ce registre doit mentionner pour l'ensemble des déchets :

- leur désignation et leur code selon la classification des déchets dangereux fixée à l'annexe du décret n° 2002540 du 18 avril 2002 relatif à la classification des déchets ;
- leur tonnage ;
- leur date de réception ;
- le numéro des bordereaux de suivi ;
- l'identité et l'adresse de l'expéditeur initial et, le cas échéant son numéro SIRET ou, si le déchet à fait l'objet d'un traitement ou d'une transformation ne permettant plus d'identifier sa provenance, l'identité et le numéro SIRET de l'exploitant de l'installation ayant effectué cette opération ;
- le cas échéant, l'identification des installations au sein desquelles les déchets ont été préalablement entreposés, reconditionnés, transformés ou traités ;
- l'identité du transporteur, son numéro SIREN et son numéro de récépissé ;
- la désignation des modes de traitement ou de leur transformation ;
- la date du reconditionnement, de la transformation ou du reconditionnement des déchets ;
- le cas échéant, la date et le motif de refus de prise en charge des déchets.

Ce registre doit être conservé pendant au moins trois ans.

**À noter**

Le professionnel exploitant une ICPE doit également tenir un registre de son activité et fournir à l'Ademe une déclaration annuelle sur la nature et les quantités d'huiles usagées qui quittent son installation.

*Pour aller plus loin* : articles R. 541-43 et R. 541-46 du Code de l'environnement ; article 4 de l'arrêté du 7 juillet 2005 fixant le contenu des registres mentionnés à l'article 2 du décret n° 2005-635 du 30 mai 2005 relatif au contrôle des circuits de traitement des déchets et concernant les déchets dangereux et les déchets autres que dangereux ou radioactifs.

## 3°. Démarches et formalités d’installation

### a. Demande d'agrément en vue de l'exploitation d'une installation d'élimination

#### Autorité compétente

Le professionnel doit adresser sa demande en trois exemplaires, auprès du préfet du département au sein duquel se trouve son installation.

#### Pièces justificatives

Sa demande doit comporter :

- une description technique de l'installation, comprenant :
  - les procédés et les capacités de recyclage, de régénération, d'incinération et de co-incinération des huiles usagées,
  - les capacités de stockage des huiles, 
  - les modalités d'élimination des déchets,
  - les dispositions relatives aux vérifications des caractéristiques des huiles usagées ;
- les moyens humains et matériels permettant de procéder aux contrôles et vérifications.

#### Procédure

La demande est transmise pour avis, au conseil départemental de l'environnement et des risques sanitaires et technologiques. Le professionnel peut se faire entendre par ce conseil et le cas échéant, le préfet doit l'informer au moins huit jours à l'avance de la date et du lieu de la réunion.

*Pour aller plus loin* : arrêté du 28 janvier 1999 relatif aux conditions d'élimination des huiles usagées.

### b. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE)

L'activité de traitement des huiles usagées, en raison du caractère polluant et dangereux des déchets, peut être soumise à la réglementation relative aux installations classées pour la protection de l'environnement.

Le professionnel est alors tenu d'effectuer une demande d'autorisation dès lors que son activité :

- entre dans le champ de la rubrique 2790 : Traitement des déchets dangereux ;
- entre dans le champ de la rubrique 2792 : Traitement des déchets contenant des polychlorobiphényles (PCB) / polychloroterphényles (PCT) à une concentration supérieure à 50 partie par million (ppm).

La demande d'autorisation doit être adressée au préfet du département au sein duquel il souhaite exercer et, doit comprendre le formulaire Cerfa 15293*01 en cas d'installations énergétiques ou le formulaire Cerfa 15294*01 en cas d'installations non énergétiques.

Il est conseillé de se reporter à la nomenclature des ICPE, disponible sur le site de l'[Aida](https://aida.ineris.fr/) pour de plus amples informations.

### c. Formalités de déclaration de l’entreprise

En cas de création d'une société commerciale, le professionnel est tenu de s'immatriculer au registre du commerce et des sociétés (RCS).

#### Autorité compétente

Le professionnel doit effectuer une déclaration auprès de la chambre de commerce et d'industrie (CCI).

#### Pièces justificatives

Pour cela, il doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI lui adresse le jour même, un récépissé indiquant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter.

Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais indiqués ci-dessus.

Si le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.