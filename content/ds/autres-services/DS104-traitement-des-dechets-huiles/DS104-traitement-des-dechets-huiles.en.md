﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS104" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Waste oil management" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="waste-oil-management" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/waste-oil-management.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="waste-oil-management" -->
<!-- var(translation)="Auto" -->

Waste oil management
=====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

The activity of processing waste of used oils consists for the professional, to develop or, as a last resort, eliminate oils that have become unsuitable for the use for which they were intended.

Used oils are considered:

- oils from combustion engines and transmission systems;
- lubricating oils
- turbine oils;
- oils for hydraulic systems.

The purpose of the reclamation operation is to enable waste to be used for useful purposes, and to prepare it for reuse.

*For further information*: Article R. 543-3 of the Environment Code; Article 3 of the European Parliament and Council 2008/EC Directive on Waste on 19 November 2008 and repealing certain directives.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

As the activity is commercial in nature, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional carries out a buying and resale activity his activity will be both commercial and artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional who wishes to operate a used oil processing facility must obtain an approval, the terms of which vary depending on the nature of the activity carried out.

Thus, when the professional's activity falls under the system of classified facilities for the protection of the environment (ICPE), his approval is granted under the following conditions:

- at the same time as the authorization or registration order;
- as long as this authorisation or registration order mentions the nature, origin, maximum quantities allowed and waste treatment conditions.

**Please note**

Where the installation is already authorized or subject to declaration, the professional is deemed to be approved as soon as the above mentions are stated within the authorization, registration or declaration order.

Failing that, the professional is required to send a supplementary statement with this information to the relevant authority.

In addition, in order to operate a facility to dispose of these oils, the professional must obtain a specific approval (see infra "3." a. Application for approval to operate a disposal facility").

*For further information*: Articles L. 541-22; R. 543-13 and R. 515-37 of the Environment Code.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There is no provision for the national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area (EEA) for a temporary and occasional or permanent exercise in France.

As such, a national legally established in a Member State and carrying out the activity of processing oil waste, is subject to the same requirements as the French national (see infra "3°. Installation procedures and formalities").

### c. Professional rules

**Compliance with the specifications**

The professional who holds a licence for waste disposal is bound by a specification and must, as such:

- keep an accounting that says:


  - quantities and dates of receipt of used oils,
  - The physical and chemical characteristics of products derived from regeneration or recycling,
  - The recipients,
  - If necessary, tonnages removed from incinerated products;
- Issue the approved picker with a product support slip, including their tonnage and quality;
- Have a minimum storage capacity equal to one-twelfth of the facility's annual disposal capacity;
- If necessary, take all the provisions to ensure the transitional storage of oils in the event of suspension or cessation of activity;
- submit monthly to the Environment and Energy Management Agency (Ademe) all technical and economic statistics relating to its activity (the tonnage received and processed, the recovery prices);
- show the recovery prices charged.

*For further information*: annex to the January 28, 1999 ordinance on the disposal conditions for used oils.

**Sanctions**

The professional who carries out the waste treatment activity is fined 750 euros if he:

- keeps no record of its activity;
- refuses to hand over this register to the officers in the event of a check;
- fails to submit the annual declaration to Ademe in the event of the operation of an ICPE;

*For further information*: R. 541-78 of the Environment Code.

### d. Some peculiarities of the regulation of the activity

**Financial guarantees**

The professional is obliged to take out financial guarantees as long as his activity falls under the classified facilities for the protection of the environment subject to authorisation (see infra "3." b. If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)).

These financial guarantees may result in:

- a written commitment from a credit, insurance, financing or mutual bond institution;
- A guarantee fund managed by Ademe;
- a deposit in the hands of the Caisse des dépôts et consignments;
- a written commitment with a stand-alone guarantee from a person holding more than half of the operator's capital.

These financial guarantees must be provided for a minimum of two years and must be renewed at least three months before their maturity.

These financial guarantees are intended to cover the risks associated with expenses to:

- The safety of the site of the professional's operation;
- Interventions in the event of an accident or pollution;
- re-conditioned after its closure.

*For further information*: Article R. 516-1 and the following of the Environment Code; decree of 31 May 2012 setting out the list of classified facilities subject to the obligation to establish financial guarantees under article R. 516-1 of the Environment Code.

**Hierarchy of waste treatment methods**

The treatment of used oils must be carried out in accordance with the national policy of prevention and waste management.

As such, the treatment of used oils should focus on:

- Preparation for reuse
- Recycling
- Energy recovery
- elimination.

*For further information*: Article L. 541-1 and the following of the Environment Code.

**Keep a chronological record of the activity**

The professional must maintain a chronological record of his waste treatment activity.

This register should mention for all waste:

- their designation and code according to the classification of hazardous waste set out in the Annex Decree No. 2002540 of 18 April 2002 on the classification of waste;
- their tonnage;
- Their date of receipt
- The number of the follow-up slips;
- the identity and address of the original sender and, if so, the SIRET number or, if the waste has been processed or processed, no longer identifying the origin, identity and SIRET number of the The installation that performed this operation;
- If necessary, identification of facilities in which waste has been previously stored, reconditioned, processed or treated;
- The carrier's identity, SIREN number and receipt number;
- The designation of treatment methods or their transformation;
- The date of repackaging, processing or reconditioning of waste;
- if so, the date and reason for refusing to take care of the waste.

This register must be kept for at least three years.

**Please note**

The professional operating an ICPE must also keep a record of his activity and provide Ademe with an annual declaration on the nature and quantities of used oils that leave its facility.

*For further information*: Articles R. 541-43 and R. 541-46 of the Environment Code; Article 4 of the 7 July 2005 decree setting out the contents of the registers mentioned in Article 2 of Decree No. 2005-635 of 30 May 2005 relating to the control of waste treatment circuits and regarding hazardous waste and non-hazardous or hazardous waste Radioactive.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Application for approval to operate a disposal facility

**Competent authority**

The professional must submit his application in three copies, to the prefect of the department in which his installation is located.

**Supporting documents**

His application must include:

- a technical description of the installation, including:
  - processes and capacity for recycling, regenerating, incineration and co-incineration of used oils,
  - Oil storage capabilities,
  - How waste is disposed of
  - provisions for checking the characteristics of used oils;
- human and material means to carry out checks and checks.

**Procedure**

The request is forwarded for advice to the Departmental Council for the Environment and Health and Technology Risks. The professional may be heard by this council and, if necessary, the prefect must inform him at least eight days in advance of the date and location of the meeting.

*For further information*: decree of 28 January 1999 relating to the conditions for the disposal of used oils.

### b. If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)

The processing of used oils, due to the polluting and dangerous nature of the waste, may be subject to regulations relating to facilities classified for the protection of the environment.

The professional is then required to apply for permission as long as his activity:

- 2790: Hazardous Waste Treatment;
- 2792: Treatment of polychlorobiphenyl (PCB) / polychloroterpphenyls (PCT) at a concentration greater than 50 part per million (ppm).

The request for authorisation must be addressed to the prefect of the department in which he wishes to practice and must include the Form Cerfa 15293*01 in the event of an energy plant or Form Cerfa 15294*01 non-energy installations.

It is advisable to refer to the nomenclature of the ICPE, available on the[Aida](https://aida.ineris.fr/) for more information.

### c. Company reporting formalities

In the event of the creation of a commercial company, the professional is required to register in the Register of Trade and Companies (RCS).

**Competent authority**

The professional must make a declaration to the Chamber of Commerce and Industry (CCI).

**Supporting documents**

In order to do so, it must provide supporting documents depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sent him a receipt on the same day indicating the missing documents on file. If necessary, the professional has a period of fifteen days to complete it.

Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the time frames mentioned above.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.