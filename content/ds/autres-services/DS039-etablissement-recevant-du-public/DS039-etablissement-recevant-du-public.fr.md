﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS039" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Établissement recevant du public" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="etablissement-recevant-du-public" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="etablissement-recevant-du-public" -->
<!-- var(translation)="None" -->

# Établissement recevant du public

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Un établissement recevant du public (ERP) est un bâtiment, un local ou une enceinte permettant d'accueillir des personnes extérieures. Le public y est admis soit gratuitement soit moyennant une redevance, et ce de manière ouverte ou sur invitation.

*Pour aller plus loin* : article R. 123-2 du Code de la construction et de l'habitation.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée. Cette dernière peut varier d'un établissement à l'autre.

Aussi, pour plus d'informations, il convient de se reporter à la fiche de l'activité concernée.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exploiter un établissement recevant du public, aucune qualification professionnelle particulière n'est requise. Néanmoins, une telle exigence peut être requise selon la nature de l'activité du professionnel.

Toutefois, l'exploitant est tenu au respect des obligations suivantes :

- effectuer une demande d'autorisation de construire, aménager ou modifier un ERP ;
- effectuer une demande d'ouverture de l'ERP (cf. « 3°. b. Demande d'autorisation d'ouverture d'un ERP ») ;
- veiller à ce que celui-ci respecte :
  - les exigences d'accessibilité pour les personnes à mobilité réduite (PMR) (cf. infra « 2°. d. Exigence d'accessibilité de l'établissement aux PMR »),
  - les normes d'incendie et de sécurité (cf. infra « 2°. d. Mise en place de dispositifs de sécurité et de protection contre les risques d'incendie et de panique ») ;
- lorsque des travaux d'aménagement sont envisagés au sein de l'établissement, il doit délivrer à la mairie une attestation d'achèvement des travaux (cf. infra « 3°. c. Le cas échéant, fournir une attestation après l'achèvement des travaux »).

#### Demande d'autorisation de construire, aménager ou modifier un ERP

Dès lors que le professionnel envisage de construire un nouvel établissement, ou d'aménager ou modifier un établissement existant, il doit obtenir une autorisation préalable délivrée au nom de l’État.

Pour cela il doit effectuer une demande d'autorisation préalable (cf. infra « 3°. a. Demande d'autorisation de construire, aménager ou modifier un ERP »).

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services (LPS) ou Libre Établissement (LE))

Aucune disposition spécifique n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exploiter un ERP à titre temporaire et occasionnel ou à titre permanent en France.

À ce titre, le ressortissant est soumis aux mêmes exigences que le ressortissant français (cf. supra « 2°. Conditions d'installation »).

### c. Sanctions pénales

L'exploitant d'un ERP encourt une amende de 1 500 euros dès lors qu'il :

- ne respecte pas les exigences en matière de sécurité et d'accessibilité des ERP ;
- ne procède pas aux formalités de contrôle des aménagements et des locaux ;
- procède à l'ouverture de son établissement sans posséder d'autorisation d'ouverture ;
- ne permet pas à la commission de sécurité d'effectuer les droits de visite au cours de la construction ou de l'aménagement de l'établissement.

**À noter**

L'amende sera appliquée autant de fois que de jours d'ouverture de l'établissement sans autorisation préalable.

*Pour aller plus loin* : articles R. 152-6 à R. 152-7 du Code de la construction et de l'habitation.

### d. Quelques particularités de la réglementation de l’activité

#### Exigence d'accessibilité de l'établissement aux PMR

Le professionnel doit veiller à ce que son établissement soit accessible aux PMR dans les mêmes conditions que les personnes valides ou, à défaut, à présenter une qualité d'usage équivalente.

Pour cela, lors de sa construction, il doit s'assurer que les locaux sont aménagés à l'extérieur (places de stationnement) comme à l'intérieur (ascenseurs, équipements, etc.) afin que les PMR puissent en toute autonomie circuler, accéder aux locaux et aux équipements, se repérer et bénéficier de l'ensemble des prestations proposées.

L'ensemble des aménagements à effectuer pour s'assurer de la conformité de l'établissement en construction sont fixés par l'arrêté du 20 avril 2017 relatif à l'accessibilité aux personnes handicapées des établissements recevant du public lors de leur construction et des installations ouvertes au public lors de leur aménagement.

*Pour aller plus loin* : articles L. 111-7 et R. 111-19 à R. 111-19-5 du Code de la construction et de l'habitation.

Lorsque le professionnel envisage des travaux ou aménagements dans un ERP déjà construit, il doit veiller à ce que les travaux d'aménagement permettent de maintenir les conditions d'accessibilité existantes.

*Pour aller plus loin* : articles R. 111-19-7 à R. 111-19-12 du Code de la construction et de l'habitation ; arrêté du 8 décembre 2014 fixant les dispositions prises pour l'application des articles R. 111-19-7 à R. 111-19-11 du Code de la construction et de l'habitation et de l'article 14 du décret n° 2006-555 relatives à l'accessibilité aux personnes handicapées des établissements recevant du public situés dans un cadre bâti existant et des installations existantes ouvertes au public.

#### Mise en place de dispositifs de sécurité et de protection contre les risques d'incendie et de panique

Le professionnel qui souhaite effectuer des travaux de construction ou d'aménagement d'un ERP doit respecter l'ensemble des exigences en matière de sécurité et de protection des risques d'incendie.

L'établissement doit notamment comporter :

- des aménagements en vue d'assurer l'évacuation des occupants des locaux ;
- des équipements de lutte contre les incendies ;
- une ou plusieurs façades en bordure d'espaces libres permettant une évacuation rapide du public ;
- des matériaux permettant de résister au feu ;
- des sorties et des espaces d'attente sécurisés en vue des risques potentiellement encourus ;
- des dispositifs d'éclairage de sécurité électriques ;
- des équipements (ascenseurs, monte-charge, chauffage, climatisation, ventilation, etc.) sécurisés.

L'ensemble de ces exigences fait l'objet d'un règlement de sécurité fixé par l'arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les ERP.

*Pour aller plus loin* : articles L. 123-1 et R. 123-1 et suivants du Code de la construction et de l'habitation.

#### Classement des établissements

Les ERP sont classés selon la nature de leur activité (le type d'activité) et leur capacité d'accueil (effectif). Ce classement donne lieu à un numéro d'identification qui déterminera les exigences en matière de sécurité et de protection contre les incendies.

**À noter**

Lors de chaque construction d'un nouvel ERP, le maître de l'ouvrage propose un classement qui sera alors validé ou non par la commission départementale de sécurité.

*Pour aller plus loin* : article R. 123-18 du Code de la construction et de l'habitation.

## 3°. Démarches et formalités d’installation

### a. Demande d'autorisation de construire, aménager ou modifier un ERP

#### Autorité compétente

L'exploitant doit adresser sa demande en quatre exemplaires au préfet et au maire.

#### Pièces justificatives

Sa demande doit contenir le formulaire Cerfa n° 13824 complété et signé, ainsi que l'ensemble des pièces justificatives exigées.

En outre, le demandeur doit joindre à sa demande un dossier permettant au maire de vérifier la conformité de l'établissement avec les règles de sécurité.

Ce dossier doit comporter les éléments suivants :

- une notice descriptive précisant l'ensemble des matériaux utilisés au sein du bâtiment (intérieur, extérieur, gros œuvre, décoration, etc.) ;
- un ou plusieurs plans précisant la largeur des passages, des sorties, des dégagements, des escaliers ou plus largement tous les modes d'évacuation et de circulation du bâtiment ;
- le cas échéant, le certificat de vérification de la mise en place des mesures de protection d'une canalisation de transport de gaz naturel, d'hydrocarbures ou de produits chimiques.

#### Procédure

La demande fait l'objet d'une instruction dans les quatre mois suivant le dépôt du dossier, soit, le cas échéant, par le service chargé de l'instruction du permis de construire, soit par le maire dans tous les autres cas. Cette autorité transmet la demande en vue de vérifier la conformité du projet aux règles d'accessibilité des PMR à la commission consultative départementale de sécurité et d'accessibilité (ou la commission départementale de sécurité dans les départements de Paris, des Hauts-de-Seine, de la Seine-Saint-Denis et du Val-de-Marne).

#### Délai

En l'absence de réponse dans un délai de quatre mois, l'autorisation est réputée accordée.

*Pour aller plus loin* : articles R. 111-19-13 à R. 111-19-30 du Code de la construction et de l'habitation.

### b. Demande d'autorisation d'ouverture d'un ERP

#### Autorité compétente

L'autorisation d'ouverture est adressée au maire.

#### Pièces justificatives

L'exploitant doit fournir, en vue de l'examen de sa demande, l'attestation de contrôle par un contrôleur technique agréé de la conformité des locaux avec les normes d'accessibilité.

#### Procédure

L'autorité doit veiller à ce que les locaux ainsi que les équipements intérieurs et extérieurs soient accessibles au public et aux PMR. Après avoir recueilli l'avis favorable de l'autorité de contrôle et d'accessibilité, l'autorisation d'ouverture est adressée à l'exploitant de l'établissement par lettre recommandée avec avis de réception.

**À noter**

Lorsque l'autorisation d'ouverture est délivrée par le maire, il doit en remettre une copie au préfet.

*Pour aller plus loin* : articles L. 111-7, L. 111-8-3 et R. 111-19-29 du Code de la construction et de l'habitation.

### c. Le cas échéant, fournir une attestation après l'achèvement des travaux

#### Autorité compétente

Une fois les travaux réalisés, l'exploitant doit adresser à la mairie, par pli recommandé avec avis de réception, une déclaration attestant l'achèvement et la conformité des travaux (DAACT).

#### Pièces justificatives

Cette déclaration doit comprendre le formulaire Cerfa n° 13408*04 complété et signé ainsi que l'ensemble des pièces justificatives exigées.

#### Procédure

Cette déclaration établie par un contrôleur technique agréé doit permettre à l'exploitant d'attester que les travaux ont été réalisés conformément aux exigences en matière de sécurité et d'accessibilité.

**À noter**

L'exploitant encourt une amende de 1 500 euros ou de 3 000 euros en cas de récidive dès lors que l'attestation n'a pas été effectuée par un contrôleur agréé.

*Pour aller plus loin* : articles L. 111-7-4 et R. 111-19-27 à R. 111-19-28 du Code de la construction et de l'habitation ; article R. 462-1 du Code de l'urbanisme.