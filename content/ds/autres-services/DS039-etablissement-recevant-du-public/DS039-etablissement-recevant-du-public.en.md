﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS039" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Establishment open to the public" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="establishment-open-to-the-public" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/establishment-open-to-the-public.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="establishment-open-to-the-public" -->
<!-- var(translation)="Auto" -->

Establishment open to the public
==================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

A public-receiving facility (ERP) is a building, space or precinct for outsiders. The public is admitted either free of charge or for a fee, in an open or invitational manner.

*For further information*: Article R. 123-2 of the Building and Housing Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. The latter may vary from institution to institution.

Also, for more information, it is necessary to refer to the fact sheet of the activity concerned.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To operate a public-receiving institution, no special professional qualifications are required. However, such a requirement may be required depending on the nature of the professional's activity.

However, the operator is required to meet the following obligations:

- Apply for planning permission, develop or modify an ERP
- request to open the ERP (see "3 degrees). b. Request for permission to open an ERP");
- ensure that it respects:- accessibility requirements for people with reduced mobility (PMR) (see infra "2 degrees). d. Requirement for accessibility of the institution to PMRs"),
  - fire and safety standards (see infra "2." d. Implementation of safety and protection against fire and panic risks);
- when development work is envisaged within the institution, it must issue the town hall with a certificate of completion of the work (see infra "3°. c. If necessary, provide a certificate after the completion of the work").

**Request for permission to build, develop or modify an ERP**

Once the professional is considering building a new facility, or developing or modifying an existing facility, he must obtain prior authorization issued on behalf of the state.

In order to do so, he must apply for prior authorisation (see infra "3°. a. Request for permission to build, develop or modify an ERP").

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There is no specific provision for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement to operate an ERP on a temporary and casual basis or on a permanent basis in France.

As such, the national is subject to the same requirements as the French national (see above "2." Conditions of installation").

### c. Criminal sanctions

The operator of an ERP is fined 1,500 euros if he:

- Does not meet the safety and accessibility requirements of ERPs;
- Does not carry out the formalities of checking the layouts and premises;
- opens its establishment without having an opening permit;
- does not allow the safety commission to carry out visitation rights during the construction or development of the facility.

**Please note**

The fine will be applied as many times as days of opening without prior authorization.

*For further information*: Articles R. 152-6 to R. 152-7 of the Building and Housing Code.

### d. Some peculiarities of the regulation of the activity

**Requirement for accessibility to PMRs**

The professional must ensure that his establishment is accessible to THE PMRs under the same conditions as able-bodied persons or, failing that, to present an equivalent quality of use.

For this, during its construction, it must ensure that the premises are arranged outside (parking spaces) as well as inside (elevators, equipment, etc.) so that the PMRs can move independently, access the premises and the equipment, locate and benefit from all the services offered.

All the adjustments to be made to ensure compliance of the building under construction are determined by the Stopped April 20, 2017 on accessibility to people with disabilities in facilities that receive public services during their construction and facilities open to the public during their development.

*For further information*: Articles L. 111-7 and R. 111-19 to R. 111-19-5 of the Building and Housing Code.

When considering work or development in an existing ERP, they must ensure that the development work maintains the existing accessibility conditions.

*For further information*: Articles R. 111-19-7 to R. 111-19-12 of the Building and Housing Code; December 8, 2014 setting out the provisions for the application of Articles R. 111-19-7 to R. 111-19-11 of the Building and Housing Code and Article 14 of Decree No. 2006-555 relating to accessibility to persons with disabilities in the public facilities located in an existing built environment and existing facilities open to the public.

**Putting in place safety and protection against fire and panic risks**

Professionals who wish to carry out construction or development of an ERP must meet all safety and fire risk protection requirements.

The establishment must include:

- Accommodation to ensure the evacuation of the occupants of the premises;
- Firefighting equipment
- one or more facades bordering open spaces for rapid evacuation of the public;
- Materials to withstand fire
- Safe exits and waiting areas for potential risks
- Electrical safety lighting devices
- equipment (elevators, lifts, heating, air conditioning, ventilation, etc.) that are secure.

All of these requirements are subject to a safety regulation set out in the June 25, 1980 order approving the general provisions of the Fire and Panic Safety Regulations in the ERPs.

*For further information*: Articles L. 123-1 and R. 123-1 and following of the Building and Housing Code.

**Ranking of establishments**

ERPs are classified according to the nature of their activity (the type of activity) and their capacity (staff). This classification results in an identification number that will determine safety and fire protection requirements.

**Please note**

During each construction of a new ERP, the owner proposes a classification that will then be validated or not by the departmental safety commission.

*For further information*: Article R. 123-18 of the Building and Housing Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Request for permission to build, develop or modify an ERP

**Competent authority**

The operator must submit his application in four copies to the prefect and the mayor.

**Supporting documents**

Its application must contain the Form Cerfa No. 13824 completed and signed, as well as all the required supporting documents.

In addition, the applicant must attach to his application a file allowing the mayor to verify the compliance of the establishment with the safety rules.

This file should include:

- A descriptive notice detailing all the materials used within the building (indoors, exteriors, large works, decoration, etc.);
- one or more plans specifying the width of the passages, exits, clearances, stairs or more broadly all modes of evacuation and circulation of the building;
- if necessary, the certificate of verification of the implementation of measures to protect a pipeline transporting natural gas, hydrocarbons or chemicals.

**Procedure**

The application is investigated within four months of filing the file, either by the building permit department or by the mayor in all other cases. This authority submits the request to verify the project's compliance with the rules on the accessibility of the PMRs to the Departmental Advisory Committee on Safety and Accessibility (or the Departmental Safety Commission in the Departments of Paris, Hauts-de-Seine, Seine-Saint-Denis and Val-de-Marne).

**Timeframe**

If no response is available within four months, the authorization is deemed to be granted.

*For further information*: Articles R. 111-19-13 to R. 111-19-30 of the Building and Housing Code.

### b. Request for permission to open an ERP

**Competent authority**

The opening authorization is addressed to the mayor.

**Supporting documents**

The operator must provide, for the purpose of reviewing his application, the certificate of control by an approved technical controller of the compliance of the premises with accessibility standards.

**Procedure**

The authority must ensure that the premises and indoor and outdoor facilities are accessible to the public and the PMRs. After receiving a favourable opinion from the supervisory and accessibility authority, the opening authorization is addressed to the operator of the establishment by recommended letter with notice of receipt.

**Please note**

When the opening authorization is issued by the mayor, he must give a copy to the prefect.

*For further information*: Articles L. 111-7, L. 111-8-3 and R. 111-19-29 of the Building and Housing Code.

### c. If necessary, provide a certificate after the completion of the work

**Competent authority**

Once the work has been completed, the operator must send a declaration of completion and compliance (DAACT) to the town hall, by a recommended fold with notice of receipt.

**Supporting documents**

This statement must include the Form Cerfa 13408Completed and signed as well as all the required supporting documents.

**Procedure**

This statement by an approved technical controller must allow the operator to certify that the work was carried out in accordance with safety and accessibility requirements.

**Please note**

The operator faces a fine of 1,500 euros or 3,000 euros in the event of a repeat offence if the certificate has not been carried out by an approved controller.

*For further information*: Articles L. 111-7-4 and R. 111-19-27 to R. 111-19-28 of the Building and Housing Code; Article R. 462-1 of the Planning Code.

