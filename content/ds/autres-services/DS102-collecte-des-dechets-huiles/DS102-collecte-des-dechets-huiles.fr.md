﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS102" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Collecte des déchets : huiles" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="collecte-des-dechets-huiles" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/collecte-des-dechets-huiles.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="collecte-des-dechets-huiles" -->
<!-- var(translation)="None" -->

# Collecte des déchets : huiles

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité de collecte d'huiles consiste, pour le professionnel (aussi appelé ramasseur), à assurer la collecte et le transport, jusqu'au point de traitement, d'huiles devenues impropres à l'usage pour lequel elles étaient destinées.

Sont considérées comme des huiles usagées :

- les huiles des moteurs à combustion et des systèmes de transmission ;
- les huiles lubrifiantes ;
- les huiles pour turbines ;
- les huiles pour systèmes hydrauliques.

*Pour aller plus loin* : article R. 543-3 du Code de l'environnement.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée. L'activité étant de nature commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exerce une activité d'achat-revente son activité sera à la fois commerciale et artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité de collecte d'huiles usagées, le professionnel doit être titulaire d'un agrément valable pour une durée maximale de cinq ans au sein d'une zone géographique déterminée (un département).

Pour cela, il doit effectuer une demande en vue d'obtenir cet agrément (cf. infra « 3°. a. Demande en vue d'obtenir un agrément »).

*Pour aller plus loin* : articles R. 543-6 et suivants du Code de l'environnement.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services (LPS) ou Libre Établissement (LE))

#### En cas d'exercice temporaire et occasionnel (LPS)

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) exerçant l'activité de collecte d'huiles en vue d'un exercice temporaire et occasionnel en France. À ce titre, le ressortissant est soumis aux mêmes exigences que le ressortissant français (cf. infra « Démarches et formalités d'installation »).

#### En cas d'exercice permanent (LE)

Tout ressortissant de l'UE ou de l'EEE légalement établi dans un État membre et exerçant l'activité de collecte d'huiles usagées peut exercer en France, à titre permanent, la même activité.

Il doit pour cela :

- être titulaire d'une autorisation délivrée par cet État membre présentant des garanties équivalentes à celles requises pour exercer en France ;
- effectuer une déclaration préalable auprès du préfet du département au sein duquel il souhaite exercer. Il est conseillé de se rapprocher de l'autorité compétente pour plus de renseignements.

*Pour aller plus loin* : article R. 543-6 du Code de l'environnement.

### c. Obligations professionnelles

Le professionnel, lors de toute collecte, est tenu au respect d'obligations rassemblées au sein d'un cahier des charges. À ce titre il doit :

- procéder à la collecte au sein du département pour lequel il a reçu un agrément ;
- afficher et tenir à jour un registre des tarifs pratiqués pour la reprise des déchets. Ces tarifs doivent tenir compte des différentes qualités d'huile ;
- procéder à la collecte des lots d'huiles usagées supérieurs à 600 litres dans un délai de quinze jours. Ce délai pourra toutefois être prolongé par décision du préfet du département ;
- délivrer un bon d'enlèvement au détenteur des lots d'huiles collectées mentionnant :
  - la quantité et la qualité des huiles,
  - le prix de reprise ;
- réaliser un double échantillonnage des huiles avant tout mélange avec un autre lot afin de détecter la présence des polychlorobiphényles (PCB), classés dangereux pour l'environnement. Le professionnel est tenu de conserver l'un de ces échantillons jusqu'au traitement des déchets et de remettre le second à son détenteur qui devra alors parapher le bon d'enlèvement et indiquer qu'un échantillon lui a bien été remis ;
- disposer d'une capacité de stockage minimale et conforme à la réglementation relative aux installations classées pour l'environnement (ICPE), à savoir :
  - au moins 1/12 du tonnage collecté annuellement,
  - cinquante mètres cubes permettant la séparation entre les huiles et les autres déchets collectés ainsi qu'entre les différentes qualités d'huiles collectées ;
- livrer les huiles collectées à des éliminateurs agréés ;
- adresser tous les mois à l'Agence de l'environnement et de la maîtrise de l'énergie ([Ademe](http://www.ademe.fr/)) l'ensemble des informations concernant son activité :
  - les tonnages collectés et les détenteurs concernés,
  - les prix de reprise,
  - les tonnages livrés aux éliminateurs ou aux acheteurs d'huiles claires en vue de leur réutilisation.

**À noter**

La reprise d'huiles « moteur » (ne contenant pas plus de 5% d'eau) est réalisée à titre gratuit par les ramasseurs implantés dans les départements et les collectivités d'outre-mer dès lors qu'ils bénéficient d'un régime d'aide.

*Pour aller plus loin* : article R. 543-11 du Code de l'environnement ; titre II de l'annexe de l'arrêté du 28 janvier 1999 relatif aux conditions de ramassage des huiles usagées.

### d. Quelques particularités de la réglementation de l’activité

#### Garanties financières

Dès lors que son activité relève des installations classées pour la protection de l'environnement soumises à autorisation (cf. infra « 3°. b. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE) »), le professionnel est tenu de souscrire des garanties financières.

Ces garanties financières peuvent résulter au choix :

- d'un engagement écrit d'un établissement de crédit, d'assurance, de financement ou de caution mutuelle ;
- d'un fonds de garantie géré par l'Ademe ;
- d'une consignation entre les mains de la Caisse des dépôts et consignations ;
- d'un engagement écrit portant garantie autonome d'une personne titulaire de plus de la moitié du capital de l'exploitant.

Ces garanties financières doivent être constituées pour une durée minimale de deux ans et doivent être renouvelées au moins trois mois avant leur échéance.

Ces garanties financières visent à couvrir les risques liés aux dépenses pour :

- la mise en sécurité du site de l'exploitation du professionnel ;
- les interventions en cas d'accident ou de pollution ;
- la remise en l'état du site après sa fermeture.

*Pour aller plus loin* : articles R. 516-1 et suivants du Code de l'environnement.

#### Hiérarchie des modes de traitement des déchets

La collecte et le traitement des huiles usagées doivent s'effectuer dans le respect de la politique nationale de prévention et de gestion des déchets.

À ce titre, le traitement des huiles usagées doit privilégier :

- la préparation en vue de la réutilisation ;
- le recyclage ;
- la valorisation énergétique ;
- l'élimination.

*Pour aller plus loin* : articles L. 541-1 et suivants du Code de l'environnement.

## 3°. Démarches et formalités d’installation

### a. Demande en vue d'obtenir un agrément

#### Autorité compétente

Le professionnel doit adresser sa demande en trois exemplaires au préfet de département.

#### Pièces justificatives

Sa demande doit comprendre :

- son engagement à respecter l'ensemble des obligations prévues au sein du cahier des charges ;
- une fiche de renseignement de l'entreprise mentionnant :
  - sa structure juridique et financière,
  - ses activités antérieures,
  - les tonnages collectés et livrés aux éliminateurs agréés,
  - la ou les zone(s) de ramassage,
  - le chiffre d'affaires des deux dernières années,
  - les autres activités dans le domaine des déchets ;
- une fiche de renseignement sur les moyens mis en œuvre pour le ramassage et le stockage des huiles usagées mentionnant notamment les informations relatives aux :
  - personnel chargé de la collecte,
  - véhicules de collecte,
  - installations de stockage, 
  - fichier de sa clientèle (existante ou envisagée) ainsi qu'aux moyens de prospection ;
- une fiche de prévision d'exploitation quantitative et économique pour les cinq prochaines années.

#### Délais et procédure

Le préfet examine la demande du professionnel et délivre l'agrément après avoir consulté l'Agence de l'environnement et de la maîtrise de l'énergie. L'arrêté de délivrance est publié au recueil des actes administratifs au sein de la préfecture et doit être mentionné dans au moins deux journaux de la presse locale ou régionale (dont les frais de publication sont à la charge du professionnel).

*Pour aller plus loin* : titre Ier de l'annexe de l'arrêté du 28 janvier 1999 relatif aux conditions de ramassage des huiles usagées.

### b. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE)

La collecte d'huiles usagées peut être soumise à la réglementation relative aux installations classées pour la protection de l'environnement selon le volume de déchets collectés.

L'activité du professionnel entre dans la rubrique 2718 : installation de transit, regroupement ou tri de déchets dangereux ou de déchets contenant les substances dangereuses ou préparations dangereuses mentionnées à l'article R. 511-10 du Code de l'environnement à l'exclusion des installations visées aux rubriques 1313, 2710, 2711, 2712, 2717 et 2719.

Selon le volume de déchets collectés le professionnel sera tenu d'effectuer :

- une demande d'autorisation, en cas de volume de collecte supérieur ou égal à une tonne ;
- une déclaration avec contrôle périodique si ce volume est inférieur à une tonne.

Il est conseillé de se reporter à la nomenclature des ICPE disponible sur le site de l'[AIDA](https://aida.ineris.fr/) pour de plus amples informations.

### c. Formalités de déclaration de l’entreprise

En cas de création d'une société commerciale, le professionnel est tenu de s'immatriculer au registre du commerce et des sociétés (RCS).

#### Autorité compétente

Le professionnel doit effectuer une déclaration auprès de la chambre de commerce et d'industrie (CCI).

#### Pièces justificatives

Pour cela, il doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI lui adresse le jour même un récépissé indiquant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais indiqués ci-dessus.

Si le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.