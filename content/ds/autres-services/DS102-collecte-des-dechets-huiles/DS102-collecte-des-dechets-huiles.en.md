﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS102" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Waste oil collection" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="waste-oil-collection" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/waste-oil-collection.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="waste-oil-collection" -->
<!-- var(translation)="Auto" -->

Waste oil collection
======================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The activity of collecting oils consists, for the professional (also called collector), to ensure the collection and transport, to the point of treatment, of oils that have become unsuitable for the use for which they were intended.

Used oils are considered:

- oils from combustion engines and transmission systems;
- lubricating oils
- turbine oils;
- oils for hydraulic systems.

*For further information*: Article R. 543-3 of the Environment Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. As the activity is commercial in nature, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional carries out a buying and resale activity his activity will be both commercial and artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To carry out the activity of collecting used oils, the professional must hold an accreditation valid for up to five years within a specific geographical area (a department).

In order to do so, he must apply for this approval (see infra "3°. a. Application for approval").

*For further information*: Articles R. 543-6 and the following of the Environment Code.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

**In case of temporary and occasional exercise (LPS)**

There is no provision for the national of a Member State of the European Union (EU) carrying out the oil collection activity for a temporary and occasional exercise in France. As such, the national is subject to the same requirements as the French national (see infra "Procedures and formalities of installation").

**In case of permanent exercise (LE)**

Any EU or EEA national legally established in a Member State and engaged in the collection of used oils may carry out the same activity in France on a permanent basis.

For this he must:

- Be the holder of an authorisation issued by that Member State with guarantees equivalent to those required to practise in France;
- to make a prior declaration to the prefect of the department in which he wishes to practice. It is advisable to get closer to the relevant authority for more information.

*For further information*: Article R. 543-6 of the Environment Code.

### c. Professional obligations

The professional, during any collection, is bound to respect obligations gathered within a specification. As such, he must:

- collecting within the department for which he has received accreditation;
- maintain a record of waste recovery rates. These tariffs must take into account the different qualities of oil;
- collect lots of used oils greater than 600 litres within a fortnight. However, this period may be extended by decision of the prefect of the department;
- issue a pick-up voucher to the holder of the lots of oils collected stating:- The quantity and quality of oils,
  - The recovery price;
- double sampling of oils before mixing with another batch to detect the presence of polychlorobiphenyls (PCBs), which are classified as harmful to the environment. The professional is obliged to keep one of these samples until the waste is treated and to hand the second one to the holder who will then have to sign the removal voucher and indicate that a sample has been given to him;
- have minimal storage capacity and compliant with the Regulations for Classified Facilities for the Environment (ICPE), namely:- at least 1/12 of the tonnage collected annually,
  - fifty cubic metres allowing the separation between oils and other waste collected as well as between the different qualities of oils collected;
- Deliver collected oils to approved eliminators;
- refer monthly to the Environment and Energy Management Agency ([Ademe](http://www.ademe.fr/)) all the information about its business:- The tonnages collected and the holders involved,
  - recovery prices,
  - tonnages delivered to removers or buyers of clear oils for reuse.

**Please note**

The resumption of "motor" oils (containing no more than 5% water) is carried out free of charge by collectors in overseas departments and communities as long as they benefit from an aid scheme.

*For further information*: Article R. 543-11 of the Environment Code; Appendix II of the January 28, 1999 order on the collection conditions of used oils.

### d. Some peculiarities of the regulation of the activity

**Financial guarantees**

As long as its activity falls under the classified environmental protection facilities subject to authorisation (see infra "3." b. If necessary, to proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)"), the professional is required to take out financial guarantees.

These financial guarantees may result in:

- a written commitment from a credit, insurance, financing or mutual bond institution;
- A guarantee fund managed by Ademe;
- a deposit in the hands of the Caisse des dépôts et consignments;
- a written commitment with a stand-alone guarantee from a person holding more than half of the operator's capital.

These financial guarantees must be provided for a minimum of two years and must be renewed at least three months before their maturity.

These financial guarantees are intended to cover the risks associated with expenses to:

- The safety of the site of the professional's operation;
- Interventions in the event of an accident or pollution;
- re-conditioned after its closure.

*For further information*: Articles R. 516-1 and the following of the Environment Code.

**Hierarchy of waste treatment methods**

The collection and treatment of used oils must be carried out in accordance with the national waste prevention and management policy.

As such, the treatment of used oils should focus on:

- Preparation for reuse
- Recycling
- Energy recovery
- elimination.

*For further information*: Articles L. 541-1 and the following from the Environment Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Request for approval

**Competent authority**

The professional must send his application in three copies to the prefect of department.

**Supporting documents**

His request must include:

- its commitment to meet all the obligations in the specifications;
- a company fact sheet that states:- its legal and financial structure,
  - its previous activities,
  - Tonnages collected and delivered to approved eliminators,
  - the pick-up area or areas
  - turnover over the past two years,
  - Other waste activities;
- an information sheet on the means used to collect and store used oils, including information on:- collection staff,
  - collection vehicles,
  - storage facilities,
  - file of its clientele (existing or envisaged) as well as the means of prospecting;
- a quantitative and economic operating forecast sheet for the next five years.

**Delays and procedures**

The prefect reviews the professional's application and issues the accreditation after consulting with the Environment and Energy Management Agency. The decree of issuance is published in the collection of administrative acts within the prefecture and must be mentioned in at least two newspapers of the local or regional press (whose publication costs are the responsibility of the professional).

*For further information*: Title I of the annex of the January 28, 1999 order on the collection conditions of used oils.

### b. If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)

The collection of used oils may be subject to regulations relating to facilities classified for environmental protection according to the volume of waste collected.

The professional's activity is included in section 2718: transit facility, collection or sorting of hazardous waste or waste containing hazardous substances or preparations referred to in Article R. 511-10 of the Code of excluding facilities under 1313, 2710, 2711, 2712, 2717 and 2719.

Depending on the volume of waste collected, the professional will be required to perform:

- a request for permission, in case of collection volume greater than or equal to one tonne;
- periodic control if this volume is less than one tonne.

It is advisable to refer to the nomenclature of the ICPE available on the website of the[Aida](https://aida.ineris.fr/) for more information.

### c. Company reporting formalities

In the event of the creation of a commercial company, the professional is required to register in the Register of Trade and Companies (RCS).

**Competent authority**

The professional must make a declaration to the Chamber of Commerce and Industry (CCI).

**Supporting documents**

In order to do so, it must provide[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sent him a receipt on the same day indicating the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the time frames mentioned above.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

