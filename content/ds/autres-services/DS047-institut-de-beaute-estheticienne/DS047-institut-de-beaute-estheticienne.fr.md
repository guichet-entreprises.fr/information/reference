﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS047" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Institut de beauté-esthéticien" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="institut-de-beaute-estheticien" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/institut-de-beaute-estheticien.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="institut-de-beaute-estheticienne" -->
<!-- var(translation)="None" -->

# Institut de beauté-esthéticien

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'esthéticien est un professionnel du soin et de la mise en beauté. Le professionnel réalise des soins esthétiques et des modelages de confort. Ces actes ne sont de nature ni médicale ni paramédicale.

*Pour aller plus loin* : loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une profession artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d'industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

L'intéressé qui souhaite exercer l'activité d'esthéticien doit disposer d'une qualification professionnelle ou exercer sous le contrôle effectif et permanent d'une personne ayant cette qualification.

Pour être considéré comme qualifiée professionnellement, la personne doit être titulaire de l'un des diplômes ou titres suivants :

- certificat d'aptitude professionnelle (CAP) « Esthétique cosmétique parfumerie » ;
- brevet professionnel « Esthétique cosmétique parfumerie » ;
- baccalauréat professionnel (bac pro) « Esthétique cosmétique parfumerie » ;
- brevet de technicien supérieur (BTS) « Esthétique cosmétique » ;
- brevet de maîtrise « Esthéticien(ne) cosméticien(ne) » délivré par l'Assemblée permanente des chambres de métiers et de l'artisanat (APCMA).

À défaut de l’un de ces diplômes ou titres, l’intéressé doit justifier d’une expérience professionnelle de trois années effectives sur le territoire de l’Union européenne (UE) ou de l’Espace économique européen (EEE), acquise en qualité de dirigeant d’entreprise, de travailleur indépendant ou de salarié dans l’exercice du métier d'esthéticien/ne. Dans ce cas, il est conseillé à l’intéressé de s’adresser à la chambre des métiers et de l’artisanat (CMA) pour demander une attestation de qualification professionnelle.

*Pour aller plus loin* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l’artisanat ; décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l’exercice des activités prévues à l’article 16 de la loi n° 96-603 du 5 juillet 1996 précitée.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Service ou Libre Établissement)

#### Pour un exercice temporaire et occasionnel (LPS)

Tout ressortissant d'un État membre de l'UE ou de l'EEE, qui est établi et exerce légalement l'activité d'esthéticien dans cet État, peut exercer en France, de manière temporaire et occasionnelle, la même activité.

Il doit au préalable en faire la demande par déclaration à la CMA du lieu dans lequel il souhaite réaliser la prestation.

Dans le cas où la profession n'est pas réglementée, soit dans le cadre de l'activité, soit dans le cadre de la formation, dans le pays dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité pendant au moins un an, au cours des dix dernières années précédant la prestation, dans un ou plusieurs États membres de l'UE.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, la CMA compétente peut exiger que l'intéressé se soumette à une épreuve d'aptitude.

*Pour aller plus loin* : article 17-1 de la loi du 5 juillet 1996 ; article 2 du décret du 2 avril 1998 modifié par le décret du 4 mai 2017.

#### Pour un exercice permanent (LE)

Pour exercer l’activité d'esthéticien en France à titre permanent, le ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français ;
- avoir exercé l'activité d'esthéticien pendant deux ans consécutifs, à titre indépendant ou en qualité de dirigeant d'entreprise, dès lors qu'il est titulaire d'un certificat de formation reconnu par un État de l'UE ou de l'EEE ;
- être titulaire d’une attestation de compétence ou d’un titre de formation requis pour l’exercice de l’activité d'esthéticien/ne dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’une attestation de compétence ou d’un titre de formation qui certifie sa préparation à l’exercice de l’activité d'esthéticien/ne lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès, ni l’exercice de cette activité ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité d'esthéticien dans l’État qui a admis l’équivalence.

Dès lors qu'il remplit l'une des conditions précitées, le ressortissant d’un État de l’UE ou de l’EEE pourra demander une attestation de reconnaissance de qualification professionnelle.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation exigée en France, la CMA compétente peut exiger que l'intéressé se soumette à des mesures de compensation.

*Pour aller plus loin* : articles 17 et 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée ; articles 3 à 3-2 du décret du 2 avril 1998 modifié par le décret du 4 mai 2017.

### c. Honorabilité

Nul ne peut exercer la profession d'esthéticien s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévue au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 III de la loi n° 96-603 du 5 juillet 1996 précitée.

### d. Quelques particularités de la réglementation de l'activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public.

#### Réglementation relative à la qualité d’artisan et au titre de meilleur ouvrier de France

##### La qualité d’artisan

Pour se prévaloir de la qualité d’artisan, la personne doit justifier soit :

- d’un CAP, d’un BEP ou d’un titre homologué ou enregistré lors de sa délivrance au RNCP d’un niveau au moins équivalent (cf. supra « 2°. a. Qualifications professionnelles ») ;
- d’une expérience professionnelle dans ce métier de trois ans au moins.

*Pour aller plus loin* : article 1 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

##### Le titre de meilleur ouvrier de France (MOF)

Le diplôme professionnel « un des meilleurs ouvriers de France » est un diplôme d’État qui atteste l’acquisition d’une haute qualification dans l’exercice d’une activité professionnelle dans le domaine artisanal, commercial, industriel ou agricole.

Le diplôme est classé au niveau III de la nomenclature interministérielle des niveaux de formation. Il est délivré à l’issue d’un examen dénommé « concours un des meilleurs ouvriers de France » au titre d’une profession dénommée « classe », rattachée à un groupe de métiers.

Pour plus d’informations, il est recommandé de consulter le [site officiel du concours « un des meilleurs ouvriers de France »](http://www.meilleursouvriersdefrance.info/).

*Pour aller plus loin* : article D. 338-9 du Code de l’éducation.

#### Réglementation concernant les prestations spécifiques

L'esthéticien ne peut pas pratiquer des activités de massages sur ses clients, qui sont réservées aux masseurs-kinésithérapeutes, mais seulement des activités dites de modelages. Il s'agit de manœuvre superficielle externe réalisée sur la peau du visage et du corps humain dans un but exclusivement esthétique et de confort, à l’exclusion de toute finalité médicale et thérapeutique.

De même concernant la partie épilation, il ne pourra pas utiliser que des techniques d'épilation à la pince ou à la cire, les autres opérations étant réservées aux médecins.

**À noter**

La réglementation concernant l'épilation à la lumière pulsée en institut n'a pas encore été fixée. Un avis de l'agence national de sécurité sanitaire précise que l'esthéticien qui pratique cette activité devrait être titulaire d'un diplôme spécialisé. Des discussions sont toujours en cours.

#### Affichage des prix

L'esthéticien doit afficher à l'extérieur de son institut les tarifs toutes taxes comprises (TTC) qu'il pratique. À l'intérieur, il devra offrir une plaquette complète des prestations réalisées ainsi que leurs prix TTC.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur devra s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

La CMA du lieu dans lequel le ressortissant souhaite réaliser la prestation, est compétente pour délivrer la déclaration préalable d'activité.

#### Pièces justificatives

La demande de déclaration préalable d'activité est accompagnée d'un dossier complet comprenant les pièces justificatives suivantes :

- une photocopie d'une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE ;
- un document justifiant la qualification professionnelle du ressortissant qui peut être, au choix :
  - une copie d'un diplôme, titre ou certificat,
  - une attestation de compétence,
  - tout document attestant de l'expérience professionnelle du ressortissant.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

**À noter** 

Lorsque le dossier est incomplet, la CMA dispose d'un délai de quinze jours pour en informer le ressortissant et demander l'ensemble des pièces manquantes.

#### Issue de la procédure

A réception de l'ensemble des pièces du dossier, la CMA dispose d'un délai d'un mois pour décider :

- soit d'autoriser la prestation lorsque le ressortissant justifie d'une expérience professionnelle de trois ans dans un État de l'UE ou de l'EEE, et de joindre à cette décision une attestation de qualification professionnelle ;
- soit d'autoriser la prestation lorsque les qualifications professionnelles du ressortissant sont jugées suffisantes ;
- soit de lui imposer une épreuve d'aptitude lorsqu'il existe des différences substantielles entre les qualifications professionnelles du ressortissant et celles exigées en France. En cas de refus d'accomplir cette mesure de compensation ou en cas d'échec dans son exécution, le ressortissant ne pourra pas effectuer la prestation de service en France.

Le silence gardé de l'autorité compétente dans ce délai vaut autorisation de débuter la prestation de service.

*Pour aller plus loin* : article 2 du décret du 2 avril 1998 ; article 2 de l'arrêté du 17 octobre 2017 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre Ier du décret n° 98-247 du 2 avril 1998.

### c. Le cas échéant, demander une attestation de reconnaissance de qualification professionnelle

L’intéressé souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de reconnaissance de qualification professionnelle.

#### Autorité compétente

La demande doit être adressée à la CMA territorialement compétente.

#### Procédure

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les quinze jours du dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

#### Pièces justificatives

Le dossier doit contenir les pièces suivantes :

- la demande d’attestation de qualification professionnelle ;
- une attestation de compétences ou le diplôme ou titre de formation professionnelle ;
- la preuve de la nationalité du demandeur ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE, une attestation portant sur la nature et de la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France, les justificatifs de l’exercice de l’activité pendant trois années.

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du centre international d’études pédagogiques (CIEP) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme ou d’un certificat ou d’un titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français (traduction agrée).

#### Délai de réponse

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut :

- reconnaître la qualification professionnelle et délivrer l’attestation de qualification professionnelle ;
- décider de soumettre le demandeur à une mesure de compensation et lui notifier cette décision ;
- refuser de délivrer l’attestation de qualification professionnelle.

En l’absence de décision dans le délai de quatre mois, la demande d’attestation de qualification professionnelle est réputée acquise.

#### Voies de recours

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier, dans les deux mois suivant la notification du refus de la CMA, un recours contentieux devant le tribunal administratif compétent. De même, si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours gracieux auprès du préfet du département dans lequel la CMA a son siège, dans les deux mois suivant la notification de la décision de la CMA. S’il n’obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.