﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS085" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Farrier" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="farrier" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/farrier.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="farrier" -->
<!-- var(translation)="Auto" -->

Farrier
==========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The farrier is a professional whose main mission is to make and lay irons on the hooves of horses and other equines.

He takes care of the parage of the hoof (give the foot of the horse its optimal shape and length) before shaping the iron that will adapt exactly to the appearance of the horse. In some cases, in agreement with the veterinarian, he will have to make orthopedic or therapeutic fittings.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial companies, it is the Chamber of Commerce and Industry (CCI).

If the professional has a buying and resale activity, his activity is both artisanal and commercial.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Professional qualifications
----------------------------------------

### a. Professional qualifications

The person concerned wishing to work as a farrier must have a professional qualification or exercise under the effective and permanent control of a person with this qualification.

To be considered professionally qualified, the person must hold one of the following diplomas or titles:

- Certificate of Professional Skills (CAP) agricultural farrier;
- technical patent of the trades (BTM) farrier.

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts, Decree 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act No. 96-603 of July 5, 1996.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

#### For a temporary and casual exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or party to the agreement of the European Economic Area (EEA), which is established and legally practises the activity of farrier in that state, may exercise in France, temporarily and occasional, the same activity.

He must first make the request by declaration to the CMA of the place in which he wishes to carry out the service.

In the event that the profession is not regulated, either in the course of the activity or in the context of training, in the country in which the professional is legally established, he must have carried out this activity for at least one year, in the course of the ten years before the benefit, in one or more EU Member States.

Where there are substantial differences between the professional qualification of the national and the training required in France, the competent CMA may require that the person concerned submit to an aptitude test.

*For further information*: Article 17-1 of the Act of 5 July 1996; Article 2 of the decree of April 2, 1998 modified by the decree of May 4, 2017.

### For a permanent exercise (Freedom of establishment)

In order to carry out the activity of farrier in France on a permanent basis, the EU or EEA national must meet one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above: "2. a. Professional qualifications");
- hold a certificate of competency or training certificate required for the exercise of farrier activity in an EU or EEA state when that state regulates access or exercise of this activity on its territory;
- have a certificate of competency or a training certificate that certifies its preparation for the exercise of farrier activity when this certificate or title has been obtained in an EU or EEA state which does not regulate access or The exercise of this activity
- be the holder of a diploma, title or certificate acquired in a third state and admitted in equivalence by an EU or EEA state on the additional condition that the person concerned has been a farrier for three years in the State which has admitted equivalence.

Once the national of an EU or EEA state fulfils one of the above conditions, he or she will be able to apply for a certificate of recognition of professional qualification (see below: "5. b. Request a certificate of professional qualification for the EU or EEA national for a permanent exercise (LE))

Where there are substantial differences between the professional qualification of the national and the training required in France, the competent CMA may require that the person concerned submit to compensation measures (see infra "5°. a. Good to know: compensation measures").

*For further information*: Articles 17 and 17-1 of Law 96-603 of 5 July 1996; Articles 3 to 3-2 of the decree of 2 April 1998 amended by the decree of 4 May 2017.

### c. Conditions of honorability, ethical rules, ethics

No one may practise as a farrier if he is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 III of Act 96-603 of July 5, 1996.

### d. Some peculiarities of the regulation of the activity

#### Regulations on the quality of craftsman and the titles of master craftsman and best worker in France

**Craftsmanship**

To claim the status of craftsman, the person must justify either:

- a CAP, a BEP or a certified or registered title when it was issued to the RNCP at least equivalent (see above: "2. a. Professional qualifications");
- professional experience in this trade for at least three years.

*For further information*: Article 1 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of master craftsman**

This title is given to individuals, including the social leaders of legal entities:

- Registered in the trades directory;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

**Please note**

Individuals who do not hold the master's degree can apply to the Regional Qualifications Commission for the title of Master Craftsman under two assumptions:

- when they are registered in the trades directory, have a degree at least equivalent to the master's degree, and justify management and psycho-pedagogical knowledge equivalent to those of the corresponding value units of the master's degree and that they have two years of professional practice;
- when they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of best worker in France (MOF)**

The professional diploma "one of the best workers in France" is a state diploma that attests to the acquisition of a high qualification in the exercise of a professional activity in the artisanal, commercial, industrial or agricultural field.

The diploma is classified at level III of the interdepartmental nomenclature of training levels. It is issued after an examination called "one of the best workers in France" under a profession called "class" attached to a group of trades.

For more information, it is recommended to consult the official website of the competition "one of the best workers in France".

*For further information*: Article D. 338-9 of the Education Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Follow the installation preparation course (SPI)

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including a degree in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who are:

- have been engaged in a professional activity requiring a level of knowledge equivalent to that provided by the internship for at least three years;
- have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, fully or partially, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France to run a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982, Article 6-1 of Decree 83-517 of 24 June 1983.

### b. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)

**Competent authority**

The CMA of the place in which the national wishes to carry out the benefit, is competent to issue the prior declaration of activity.

**Supporting documents**

The request for a pre-report of activity is accompanied by a file containing the following supporting documents:

- A photocopy of a valid ID
- a certificate justifying that the national is legally established in an EU or EEA state;
- a document justifying the professional qualification of the national who may be, at your choice:- A copy of a diploma, title or certificate,
  - a certificate of competency.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Please note**

When the file is incomplete, the CMA has a period of fifteen days to inform the national and request all the missing documents.

**Outcome of the procedure**

Upon receipt of all the documents in the file, the CMA has one month to decide:

- either to authorise the benefit where the national justifies three years of work experience in an EU or EEA state, and to attach to that decision a certificate of professional qualification;
- or to authorize the provision when the national's professional qualifications are deemed sufficient;
- or to impose an aptitude test on him when there are substantial differences between the professional qualifications of the national and those required in France. In the event of a refusal to perform this compensation measure or if it fails to perform, the national will not be able to perform the service in France.

The silence kept by the competent authority in these times is worth authorisation to begin the delivery of service.

*For further information*: Article 2 of the decree of 2 April 1998; Article 2 of the October 17, 2017 regarding the submission of the declaration and the requests provided for by Decree 98-246 of 2 April 1998 and Title I of Decree 98-247 of 2 April 1998.

### c. Request a certificate of recognition of professional qualification for the EU or EEA national in the event of a permanent exercise (LE)

The person concerned wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of recognition of professional qualification.

**Competent authority**

The request must be addressed to the appropriate CMA of the place in which the person wishes to settle.

**Procedure**

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the person concerned to complete it within a fortnight of filing the file. A receipt is issued as soon as it is complete.

**Supporting documents**

The application for certification of professional qualification is a file containing the documents
supporting:

- An application for a certificate of professional qualification
- a proof of professional qualification in the form of a certificate of competency or a diploma or a vocational training certificate;
- A photocopy of the applicant's valid ID
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, the proofs of the exercise of the activity for three years.

**What to know**

If necessary, all supporting documents must be translated into French by a certified translator.

The CMA may request further information about its training or professional experience to determine the possible existence of substantial differences with the professional qualification required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (CIEP) to obtain additional information on the level of training of a diploma or certificate or a foreign designation, the applicant will have to pay a fee Additional.

**Timeframe**

Within three months of the receipt, the CMA may decide to:

- Recognize and issue the certificate of professional qualification;
- subject the national to a compensation measure and notifies him of this decision;
- refuse to issue the certificate of professional qualification.

**What to know**

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

**Remedies**

If the CMA refuses the CMA's application for professional qualification, the applicant may challenge the decision. It can thus, within two months of notification of the CMA's refusal, form:

- a graceful appeal to the prefect of the relevant CMA department;
- a legal challenge before the relevant administrative court.

**Cost**

Free.

**Good to know: compensation measures**

The CMA notifies the applicant of his decision to have him perform one of the compensation measures. This decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose between an adjustment course of up to three years and an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organised within six months of the CMA's receipt of the applicant's decision to opt for the event. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the adjustment course, the applicant sends the CMA a certificate certifying that he has validly completed this internship, accompanied by an evaluation of the organization that supervised him. On this basis, the CMA issues a certificate of professional qualification within one month.

The decision to use a compensation measure may be challenged by the person concerned who must file an administrative appeal with the prefect within two months of notification of the decision. If his appeal is dismissed, he can then initiate a legal challenge.

**Cost**

A fixed fee covering the investigation of the case may be charged.

For more information, it is advisable to get closer to the relevant CMA.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998, order of 28 October 2009 taken in according decrees 97-558 of 29 May 1997 and 98-246 of 2 April 1998 and relating to the procedure for recognising professional qualifications of a a member state of the European Community or another state party to the European Economic Area agreement.

### d. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Artisanal Company Reporting Formalities" for more information.

