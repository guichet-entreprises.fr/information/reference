﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS008" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Hairdresser in a salon" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="hairdresser-in-a-salon" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/hairdresser-in-a-salon.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="hairdresser-in-a-salon" -->
<!-- var(translation)="Auto" -->


Hairdresser in salon
====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
========================

### a. Definition

The hairdresser in the salon is a professional who provides, within an institution, all the aesthetic and hygienic treatments of the hair (natural or artificial). It offers a wide range of services: cutting, hair straightening, folding, coloring, scalp treatment, hair care, etc. It can also sell products, especially cosmetics, to its customers.

*For further information*: Law 46-1173 of May 23, 1946 regulating the conditions of access to the profession of hairdresser.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The person concerned wishing to work as a hairdresser in the salon must justify a professional qualification or exercise under the effective and permanent control of a person with such a qualification.

Holders are considered professionally qualified:

- Professional Hairdressing Patent (BP)
- Master's degree (BM) as a hairdresser;
- an equal or higher degree or designation approved or registered at the time of its issuance at the[national directory of professional certifications](http://www.rncp.cncp.gouv.fr/) (RNCP).

**Please note**

Hairdressers for men practising the profession only incidentally or in addition to another profession are not subject to the obligation of professional qualification to practise on the condition that they intervene in communes less than 2 000 inhabitants.

*For further information*: Decree 97-558 of 29 May 1997 on conditions of access to the profession of hairdresser; Article 7 quater of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades

### b. Professional Qualifications - European Nationals (LPS or LE)

#### For Freedom to provide services

The professional who is a member of a European Union (EU) state or a state party to the Agreement on the European Economic Area (EEA) may work in France, on a temporary and casual basis, as a hairdresser on the condition that he is legally established in one of these states to carry out this activity.

If neither the activity nor the training conducting it is regulated in the State of the Establishment, the person concerned must also prove that he or she has worked as a hairdresser in the salon in the state where he is established for at least the equivalent of two years full-time during the of the last ten years before the performance he wants to perform in France.

The professional wishing to practice on an ad hoc and occasional basis in France is exempt from the registration requirement in the trades directory or in the register of companies. Therefore, it is also exempt from attending the installation preparation course (SPI).

*For further information*: Article 3-1 of Law 46-1173 of May 23, 1946 regulating the conditions of access to the profession of hairdresser.

#### For a Freedom of establishment

In order to carry out a permanent salon in France, the professional who is a national of the EU or the EEA must meet one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above "2 degrees). a. Professional qualifications");
- have legally operated in an EU or EEA state for six consecutive years independently or as a leader. In this case, the exercise of the activity must not have been terminated for more than ten years at the time when the person requested a certificate of professional qualification from the CMA (see infra "3o). b. If necessary, apply for a certificate of professional qualification");
- have legally operated in an EU or EEA state for three consecutive years, either independently or as a business leader after receiving prior training of at least three years sanctioned by a certificate recognized by the state (or deemed valid by a competent professional body under a state delegation). The exercise period is increased to four years if the training has lasted only two years;
- have legally operated in an EU or EEA state for three consecutive years on an independent basis and has been in the job as an employee for at least five years. In this case, the exercise of the activity must not have ended for more than ten years when the person concerned requested a certificate of professional qualification from the CMA (see infra "3o). b. If necessary, apply for a certificate of professional qualification");
- have a certificate of competency or training document required by an EU or EEA state that regulates access or the exercise of hairdressing activity on its territory. This certification, which certifies a level of professional qualification equivalent to or immediately lower than that required for French nationals (see supra "2." a. Professional Qualifications"), is issued on the basis of either non-graduation or certificate training or examination without prior training or occupation in one of these states for three Years
- have a certificate of competency or a training certificate certifying its preparation for the performance of the hairdressing activity, obtained in an EU or EEA state that does not regulate access or exercise of this activity. This certification, which certifies a level of professional qualification equivalent to or immediately lower than that required for French nationals (see supra "2." a. Professional Qualifications"), is issued on the basis of either non-graduation or certificate training or examination without prior training or occupation in one of these states for three Years
- have a diploma, title or certificate allowing the exercise of hairdressing activity acquired in a third state and admitted in equivalence by an EU or EEA state and have carried out this activity for three years in the state that has admitted equivalence.

The professional who fulfils one of the aforementioned conditions is considered professionally qualified to settle in France. He can apply from the CMA for the certification of professional qualification to carry out the activity of hairdresser at home (see infra "3. b. If necessary, apply for a certificate of professional qualification").

In addition, the national must have sufficient knowledge of the French language to be able to practise as a hairdresser

**Please note**

In the event of a check-up, the qualified professional has four months to produce the certificate of professional qualification.

*For further information*: Articles 5, 6 and 10 of the decree of 29 May 1997 above.

### c. Conditions of honorability and incompatibility

No one may practise the profession if he is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 III of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 131-6 of the Penal Code and Article L. 653-8 of the Code of Commerce.

### d. Some peculiarities of the regulation of the activity

#### Professional titles

**The title of craftsman**

Craft business leaders, their spouses or associates and their associates who are personally and usually involved in the company's activity can apply to the president of the CMA for the quality of craftsmanship.

These people must have, at their choice:

- A V-level diploma in the trade (CAP, BEP, etc.);
- A V-level certified designation in the trade or related occupation;
- three years of registration experience in the trade.

For more information, it is advisable to get closer to the territorially competent CMA.

**The title of master craftsman**

To obtain the title of master craftsman, the person concerned (a natural person or a manager of a craft company) must:

- Be registered in the trades directory
- Hold a master's degree (BM);
- justify at least two years of professional practice.

The request must be addressed to the president of the relevant CMA.

**What to know**

Individuals who do not hold the WB can apply for the title of Master Craftsman in two distinct situations:

- if they are registered in the trades directory, have a training degree equivalent to the master's degree, and have management and psycho-pedagogical knowledge equivalent to the units of value of the master's degree and that they warrant more than two years of professional practice;
- if they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

In both cases, the title of master craftsman may be granted by the Regional Qualifications Commission.

For more details, it is advisable to get closer to the territorially competent CMA.

*For further information*: Decree 98-247 of 2 April 1998.

**The title of best worker in France**

The title of best worker in France (MOF) is reserved for those who have passed the exam called "one of the best workers in France". It is a state diploma that attests to the acquisition of a high qualification in the course of a professional activity.

*For further information*: [official website of the competition "one of the best workers in France"](http://www.meilleursouvriersdefrance.org/) ; Articles D. 338-9 and the following of the Education Code and arrested on December 27, 2012.

#### Respect accessibility and safety rules

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, it is advisable to refer to the activity sheet[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

#### Advertising tariffs

The hairdresser has an obligation to inform the customer, by way of displaying in a display, in a visible and readable way, the prices of the most common services.

Hair salons for men or women must display at least ten all-tax (TTC) prices and mixed hair salons must display at least ten TTC awards for men and ten TTC awards for women.

Packages, comprising at least two services, must show the details of the services that make up them.

In addition, rates must also be displayed inside the lounge, visible and readable by customers, instead of payment.

Hairdresser operators will be required to keep a card at the checkout in view of customers with a complete list of TTC prices for all services offered at the facility. Copies of this card will be made available to customers in the lounge.

The ability to view the benefit card should be mentioned on the price scoreboard, both outside and inside the facility.

*For further information*: Articles 1 and 2 of the March 27, 1987 order on the advertising of hairdressing rates.

#### Obligation to issue an invoice to the customer

A note must be given to the client, if the benefit issued by the hairdresser was made before the payment of the price and if the latter is greater than or equal to 25 euros TTC. In the case, where the price is less than 25 euros TTC, the issuance of the note is optional but remains mandatory, if the customer requests it.

The note must include:

- The date it was written
- The provider's name and address
- The name of the client, unless the customer is opposed;
- The date and place of execution of the service
- the detailed count, in quantity and price, of each service and product supplied or sold. The detailed count is optional when the service delivery has resulted, prior to its execution, in the establishment of a descriptive and detailed estimate, accepted by the client and in accordance with the work performed;
- the total amount to be paid excluding taxes (HT) and TTC.

Finally, the score must be set in double copy:

- The original is given to the customer.
- double is kept by the hairdresser, for a period of two years and ranked by date.

*For further information*: ( Order 83-50/A of 3 October 1983 relating to the price advertising of all services.

#### Use of harmful products

Products containing thioglycolic acid, its salts or esters, with a concentration of between 8% and 11%, should be handled with care when using them, especially when using them to curl, relax or undulate hair. . Only persons holding a professional certificate, a master's certificate, an equivalent title or whose professional capacity has been validated by the National Hairdressing Commission are allowed to use these products.

*For further information*: Decree 98-848 of 21 September 1998 setting out the conditions for the professional use of products containing thioglycolic acid, its salts or esters.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Follow the installation preparation course (SPI)

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

- Registration is done upon presentation of a piece of identification with the territorially competent CMA.
- It has a minimum duration of 30 hours.
- It comes in the form of courses and practical work.
- Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

At the end of the internship, the participant receives a certificate of follow-up internship which must be attached to his business declaration file.

**Cost**

The internship pays off. As an indication, the training costs about 236 euros in 2016.

**Cases of exemption from the internship**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including a degree in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who:

- have either worked for at least three years requiring a level of knowledge equivalent to that provided by the internship;
- either have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, fully or partially, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France for the management of a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship. He must accompany his mail with the following supporting documents: copy of the diploma approved at level III, copy of the master's certificate, proof of a professional activity requiring an equivalent level of knowledge. In addition, the person concerned must pay a variable fee (25 euros in 2016). Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982 and Article 6-1 of Decree 83-517 of 24 June 1983.

### b. If necessary, apply for a certificate of professional qualification, if necessary

The person concerned wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of recognition of professional qualification.

**Competent authority**

The request must be addressed to the territorially competent CMA, that is, within which the person concerned wishes to exercise.

**Procedure**

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the applicant to complete it within 15 days of filing the file. A receipt is issued as soon as the file is complete.

**Supporting documents**

The folder should contain:

- Applying for a certificate of professional qualification
- Proof of professional qualification: a certificate of competency or a diploma or vocational training certificate;
- Proof of the applicant's nationality
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, the proofs of the exercise of the activity for three years.

The CMA may request further information about its training or professional experience to determine the possible existence of substantial differences with the professional qualification required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (CIEP) to obtain additional information on the level of training of a diploma or certificate or a foreign designation, the applicant will have to pay a fee Additional.

**What to know**

If necessary, all supporting documents must be translated into French by a certified translator.

**Response time**

Within three months of the receipt, the CMA:

- recognises professional qualification and issues certification of professional qualification;
- decides to subject the applicant to a compensation measure and notifies him of that decision;
- refuses to issue the certificate of professional qualification.

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

**Compensation measures**

The CMA may subject the applicant to a compensation measure consisting of an accommodation or aptitude test. The compensation measure is required in one of the following situations:

- if the length of training is at least one year less than that required to obtain one of the diplomas or titles required to work as a hairdresser at home (see below "2. a. Professional qualifications");
- if the training received covers subjects substantially different from those covered by one of the diplomas or titles required to work as a hairdresser at home (see infra "2°. a. Professional qualifications");
- If the exercise of the activity of hairdresser at home (or the effective and permanent control of this activity) requires, for the exercise of some of its responsibilities, specific training which is not provided in the Member State of origin and which deals with subjects substantially different from those covered by the certificate of competency or training designation referred to by the applicant.

Before requesting compensation, the CMA verifies whether the knowledge acquired by the applicant during his professional experience in an EU, EEA or third country state is likely to cover, in full or in part, the difference substantial in terms of duration or content.

**Cost**

Free. However, if the national is to submit to a compensation measure, he will have to pay a fee for the investigation of the case.

**Remedies**

If the CMA refuses to issue the recognition of professional qualification, the applicant may initiate, within two months of notification of the refusal of the CMA, a legal challenge before the relevant administrative court. Similarly, if the person concerned wishes to challenge the CMA's decision to submit it to a compensation measure, he must first initiate a graceful appeal with the prefect of the department in which the CMA is based, within two months of notification of the decision. CMA. If he does not succeed, he may opt for a litigation before the relevant administrative tribunal.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under the decrees of 29 May 1997 and 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the European Community or another State party to the Agreement on the European Economic Area.

### c. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, it is advisable to refer to the activity sheets "Formality of reporting a commercial company" and "Formalities of declaration of artisanal enterprise".

### d. Post-registration authorisation

#### Ask for permission to stream music

The hair salon wishing to broadcast sound music (in the background) must, ask the society of composers and music publishers (Sacem) before the opening of the establishment.

A contract was then concluded between the Sacem and the hair salon providing, in exchange for the payment of a flat fee, the authorisation to broadcast the works of the French and international repertoire managed by the Sacem. The amount of the fee varies depending on the number of employees in contact with customers, the number of sound sources used, etc. For more details, it is advisable to refer to the [Sacem's official website](https://www.sacem.fr/).

**What to know**

Sacem is also responsible for collecting, on behalf of the Fair Compensation Collection Society (SPRE), fair remuneration owed to performers and producers for the use of recorded music. The hair salon will therefore also have to pay this fee to the Sacem.

*For further information*: decision of December 8, 2010 of the commission under Article L. 214-4 of the Intellectual Property Code amending the Decision of January 5, 2010.

