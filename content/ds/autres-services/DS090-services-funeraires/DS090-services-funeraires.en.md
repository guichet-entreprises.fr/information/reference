﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS090" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Funeral services" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="funeral-services" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/funeral-services.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="funeral-services" -->
<!-- var(translation)="Auto" -->


Funeral services
================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Funeral services serve a public service mission that includes:

- Transporting bodies before and after beer;
- The organization of the funeral;
- thanatopraxia (which deals with the care of the deceased's conservation);
- providing covers, coffins and their interior and exterior accessories as well as cinerary urns;
- The management and use of burial chambers;
- the provision of hearses and mourning cars;
- the provision of personnel and the objects and services necessary for funerals, burials, exhumations and cremations, with the exception of funeral plaques, religious emblems, flowers, various printing works and funeral marble.

These services can be provided by any company that has received a prefectural clearance.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for a craft activity (thanatopraxia, body presentation and marblery), the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

Anyone wishing to practise as a master of ceremonies, thanatopracteur or funeral or assimilated counsellor, must hold a national diploma.

The training leading to the profession of master of ceremonies is set at 70 hours and 140 hours for the training leading to the profession of funeral or assimilated counselor.

To work as a thanatopracteur, the person must obtain his national diploma after completing a theoretical training of 195 hours and a practical training of twelve months.

*To go further* Articles D. 2223-132, D.2223-55-2 and following of the General Code of Local Authorities.

### b. Professional Qualifications-EU or EEA Nationals (Free Service Or Freedom of establishment)

**For a permanent exercise (LE)**

Any national of a European Union (EU) or European Economic Area (EEA) state may settle in France to practice permanently if he or she justifies:

- professional experience as a manager:- three years in a row,
  - two consecutive years accompanied by prior training sanctioned by a state-recognized certificate,
  - two consecutive years with three years of work experience as an employee;
- three consecutive years of professional experience as an employee and a certificate justifying prior training in funeral services.

If the national does not meet these conditions, he or she will have to justify:

- a diploma, certificate or title in thanatopraxia issued by a state that regulates the profession, or the practice of that profession for one year in the last ten years, when neither its training nor its activity is regulated in that state;
- certificate of competency or training certificate sanctioning an activity other than than thanatopracteur, or the exercise of such an activity for one year in the last ten years, when neither his training nor his activity is regulated in that state.

The prefect of the national's place of installation will be competent to decide on the application for recognition of qualifications of his competence.

*To go further* Articles L. 2223-48 and L. 2223-49 of the General Code of Local Government.

**For a temporary and casual exercise (LPS)**

Any national of an EU or EEA state may perform a temporary and occasional funeral service activity in France, as long as he fulfils one of the following conditions:

- Be legally established in that state to carry out the same activity;
- have been in this activity for one year in the last ten years preceding the delivery, when neither training nor activity is regulated in that state;
- have received a clearance by the competent prefecture (see infra "3.3. c. Request a prefectural clearance").

*To go further* Articles L. 2223-23 and L. 2223-47 of the General Code of Local Government.

### c. Conditions of honorability and incompatibility

In order to be able to perform the duties of the head of a funeral service, the person concerned must not:

- be hit with personal bankruptcy
- have been sentenced to prison for one of the following crimes or misdemeanours:- illegally carrying out a regulated professional activity,
  - active or passive corruption or influence peddling,
  - intimidation of a person in a public service,
  - Scam
  - breach of trust,
  - violation of burial or violation of respect due to the dead,
  - Flight
  - moral assault or sexual assault,
  - Receiving
  - Wilful assaults;
- have been the subject of a conviction handed down by a foreign court and recognized as such by French law.

*To go further* Article L. 2223-24 of the General Code of Local Authorities.

### d. Some peculiarities of the regulation of the activity

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

#### Information to share with families

Documents and quotes provided to the families of deceased persons must include the name of the institution, its legal representative, the operator's address and, if applicable, its registration number in the trade register or trades directory, as well as an indication of its legal form, the authorisation it holds and, if necessary, the amount of its capital.

Quotes must distinguish between the amounts allocated to supplies and services and those paid to third parties in benefit compensation.

*To go further* Articles R. 2223-24 to R. 2223-32-1 of the General Code of Local Authorities.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Artisanal Company Reporting Formalities," "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Follow the installation preparation course (SPI) for the activity of thanatopracteur and marbrier

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including a degree in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who are:

- have been engaged in a professional activity requiring a level of knowledge equivalent to that provided by the internship for at least three years;
- have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, fully or partially, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France to run a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*To go further* Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree No.83-517 of 24 June 1983.

### c. Post-registration authorisation

#### Request a prefectural clearance

The establishments providing one of the funeral services referred to in the "1o. a. Definition" must have been granted clearance by the prefect of the site. The prefect of Paris will also be responsible for deciding on the applications of foreign nationals who wish to settle in France.

The application for clearance is made by submitting a file that includes:

- a statement indicating the name of the institution, its legal form, information on the civil status of the legal representative and, if necessary, an extract from the register of trade and companies or the directory of trades;
- A list of activities that will be carried out;
- justifications for the institution's taxation and social security contributions;
- Certificates justifying the professional capacity of officers and agents;
- The up-to-date status of staff employed
- if necessary, a certificate of compliance of all vehicles used to transport bodies before and after beer (see infra "3.0). d. If necessary, obtain a certificate of compliance with the vehicles used for beer);
- if necessary, the certificate of compliance of the burial chamber (see infra "3.3. d. If necessary, obtain permission to manage a burial chamber");
- if applicable, any certification that its staff holds diplomas relating to the professions of thanatopracteur, master of ceremonies or funeral or assimilated counselor;
- if applicable, certification of crematorium compliance (see below "If applicable, obtain a certification of crematorium compliance").

The authorization is granted for a period of six years. In the event of a change, the head of the school will have to notify the prefect within two months.

*To go further* Articles L. 2223-23 and R. 2223-56 to R. 2223-65 of the General Code of Local Government.

#### If necessary, obtain permission to manage a burial chamber

Institutions or associations wishing to manage or use a burial chamber must obtain an authorization and produce a certificate of compliance. The request must first be addressed to the territorially competent prefect in the form of a file including:

- An explanatory note
- A situation plan
- a draft public opinion that will also be published in two local or regional newspapers.

The prefect will have two months to obtain the opinion of the city council and an additional two months to decide on the application. The silence kept will be worth permission.

Once the authorization has been granted, the company or association will have to ensure that the funeral board complies with the French COFRAC Accreditation Committee) or by another accreditation body that is a signatory to the recognition agreement. Multilateral.

*To go further* Articles R. 2223-59, R. 2223-74 and D. 2223-87 of the General Code of Local Government.

#### If necessary, obtain a certificate of compliance from the crematorium

Any institution wishing to have a crematorium must first obtain an authorisation from the administration and a certificate of compliance issued by COFRAC or by another accreditation body that is a signatory to the multilateral recognition. The administration's silence on the creation of a crematorium will be earned the rejection of the application for authorisation.

*To go further* Articles R. 2223-99 to R. 2223-109 of the General Code of Local Authorities.

#### If necessary, obtain a certificate of compliance with the vehicles used for beer

The seller of a transport vehicle used for beering people must have obtained a certificate of compliance issued by COFRAC or another accreditation body that is a signatory to the multilateral recognition agreement.

Transportation vehicles are subject to a compliance visit every three years or within six months of the renewal of the clearance application.

*To go further* Articles D. 2223-110 to D. 2223-115 of the General Code of Local Government.

