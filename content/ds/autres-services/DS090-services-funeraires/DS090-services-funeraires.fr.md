﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS090" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Services funéraires" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="services-funeraires" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/services-funeraires.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="services-funeraires" -->
<!-- var(translation)="None" -->

# Services funéraires

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Les services funéraires desservent une mission de service publique comprenant :

- le transport des corps avant et après mise en bière ;
- l'organisation des obsèques ;
- la thanatopraxie (qui porte sur les soins de conservation du défunt) ;
- la fourniture des housses, des cercueils et de leurs accessoires intérieurs et extérieurs ainsi que des urnes cinéraires ;
- la gestion et l'utilisation des chambres funéraires ;
- la fourniture des corbillards et des voitures de deuil ;
- la fourniture de personnel et des objets et prestations nécessaires aux obsèques, inhumations, exhumations et crémations, à l'exception des plaques funéraires, emblèmes religieux, fleurs, travaux divers d'imprimerie et de la marbrerie funéraire.

Ces services peuvent être assurés par toute entreprise ayant reçu une habilitation préfectorale.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une activité artisanale (thanatopraxie, présentation du corps et marbrerie), le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Toute personne souhaitant exercer la profession de maître de cérémonie, thanatopracteur ou conseiller funéraire ou assimilé, doit être titulaire d'un diplôme national.

La formation menant à la profession de maître de cérémonie est fixée à 70 heures et à 140 heures pour celle menant à la profession de conseiller funéraire ou assimilé.

Pour exercer le métier de thanatopracteur, l'intéressé devra obtenir son diplôme national après avoir suivi une formation théorique de 195 heures et une formation pratique de douze mois.

*Pour aller plus loin* : articles D. 2223-132, D.2223-55-2 et suivants du Code général des collectivités territoriales.

### b. Qualifications professionnelles–Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### Pour un exercice permanent (LE)

Tout ressortissant d'un État de l'Union européenne (UE) ou de l'Espace économique européen (EEE) peut s'établir en France pour y exercer de façon permanente s'il justifie :

- d'une expérience professionnelle en qualité de dirigeant soit :
  - de trois années consécutives,
  - de deux années consécutives accompagnée d'une formation préalable sanctionnée par une attestation reconnue par l’État,
  - de deux années consécutives accompagnée d'une expérience professionnelle de trois années en tant que salarié ;
- d'une expérience professionnelle de trois années consécutives en qualité de salarié et d'une attestation justifiant une formation préalable dans les services funéraires.

Lorsqu'il ne remplit pas ces conditions, le ressortissant devra justifier, le cas échéant :

- d'un diplôme, certificat ou titre en thanatopraxie délivré par un État qui réglemente la profession, ou de l'exercice de cette profession pendant un an au cours des dix dernières années, lorsque ni sa formation, ni son activité ne sont réglementées dans cet État ;
- d'une attestation de compétence ou d'un titre de formation sanctionnant une activité autre que celle de thanatopracteur, ou de l'exercice d'une telle activité pendant un an au cours des dix dernières années, lorsque ni sa formation, ni son activité ne sont réglementées dans cet État.

Le préfet du lieu d'installation du ressortissant sera compétent pour se prononcer sur la demande de reconnaissance de qualifications de ses compétences.

*Pour aller plus loin* : articles L. 2223-48 et L. 2223-49 du Code général des collectivités territoriales.

#### Pour un exercice temporaire et occasionnel (LPS)

Tout ressortissant d'un État de l'UE ou de l'EEE, peut exercer en France une activité de service funéraire, de manière temporaire et occasionnelle, dès lors qu'il remplit l'une des conditions suivantes :

- être légalement établi dans cet État pour y exercer la même activité ;
- avoir exercé cette activité pendant un an au cours des dix dernières années qui précèdent la prestation, lorsque ni la formation, ni l'activité ne sont réglementées dans cet État ;
- avoir reçu une habilitation par la préfecture compétente (cf. infra « 3°. c. Demander une habilitation préfectorale »).

*Pour aller plus loin* : articles L. 2223-23 et L. 2223-47 du Code général des collectivités territoriales.

### c. Conditions d'honorabilité et incompatibilités

Pour pouvoir exercer les fonctions de dirigeant d'un service funéraire, l'intéressé ne doit pas :

- être frappé de faillite personnelle ;
- avoir fait l'objet d'une condamnation à une peine de prison pour l'un des crimes ou des délits suivants :
  - exercice illégal d'une activité professionnelle réglementée,
  - corruption active ou passive ou trafic d'influence,
  - acte d'intimidation contre une personne exerçant une fonction publique,
  - escroquerie,
  - abus de confiance,
  - violation de sépulture ou atteinte au respect dû aux morts,
  - vol,
  - attentat aux mœurs ou agression sexuelle,
  - recel,
  - coups et blessures volontaires ;
- avoir fait l'objet d'une condamnation prononcée par une juridiction étrangère et reconnue comme telle par la loi française.

*Pour aller plus loin* : article L. 2223-24 du Code général des collectivités territoriales.

### d. Quelques particularités de la réglementation de l'activité

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

#### Information à communiquer aux familles

Les documents et devis fournis aux familles des personnes décédés doivent comporter le nom de l'établissement, de son représentant légal, de l'adresse de l'opérateur et, le cas échéant, son numéro d'inscription au registre du commerce ou au répertoire des métiers, ainsi que l'indication de sa forme juridique, de l'habilitation dont il est titulaire et, le cas échéant, du montant de son capital.

Les devis doivent distinguer les sommes affectées aux fournitures et aux services de celles versées à des tiers en rémunération de prestations.

*Pour aller plus loin* : articles R. 2223-24 à R. 2223-32-1 du Code général des collectivités territoriales.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Autorisation(s) post-immatriculation

#### Demander une habilitation préfectorale

Les établissements délivrant l'un des services funéraires visées au « 1°. a. Définition » doivent avoir obtenu une habilitation par le préfet du lieu d'implantation. Le préfet de Paris sera également compétent pour se prononcer sur les demandes des ressortissants étrangers qui souhaitent s'installer en France.

La demande d'habilitation se fait par la remise d'un dossier comprenant :

- une déclaration indiquant la dénomination de l'établissement, sa forme juridique, les informations sur l'état civil du représentant légal et, le cas échéant, un extrait du registre du commerce et des sociétés ou du répertoire des métiers ;
- la liste des activités qui seront exercées ;
- les justifications relatives à l'imposition et aux cotisations sociales de l'établissement ;
- les attestations justifiant de la capacité professionnelle des dirigeants et des agents ;
- l'état à jour du personnel employé ;
- le cas échéant, une attestation de conformité de tous les véhicules utilisés pour le transport des corps avant et après mise en bière (cf. infra « 3°. d. Le cas échéant, obtenir une attestation de conformité des véhicules utilisés pour la mise en bière ») ;
- le cas échéant, l'attestation de conformité de la chambre funéraire (cf. infra « 3°. d. Le cas échéant, obtenir une autorisation pour gérer une chambre funéraire ») ;
- le cas échéant, toute attestation que son personnel est titulaire des diplômes relatifs aux professions de thanatopracteur, maître de cérémonie ou bien conseiller funéraire ou assimilé ;
- le cas échéant, l'attestation de la conformité du crématorium (cf. infra « Le cas échéant, obtenir une attestation de conformité du crématorium »).

L'habilitation est accordée pour une durée de six ans. En cas de changement, le dirigeant de l'établissement devra le notifier au préfet dans les deux mois.

*Pour aller plus loin* : articles L. 2223-23 et R. 2223-56 à R. 2223-65 du Code général des collectivités territoriales.

#### Le cas échéant, obtenir une autorisation pour gérer une chambre funéraire

Les établissements ou associations qui souhaitent gérer ou utiliser une chambre funéraire doivent obtenir une autorisation et produire une attestation de conformité. La demande doit au préalable être adressée au préfet territorialement compétent sous la forme d'un dossier comprenant :

- une notice explicative ;
- un plan de situation ;
- un projet d'avis au public qui sera également publié dans deux journaux de la presse locale ou régionale.

Le préfet disposera d'un délai de deux mois pour obtenir l'avis du conseil municipal puis de deux mois supplémentaires pour se prononcer sur la demande. Le silence gardé vaudra autorisation.

Une fois l'autorisation accordée, l'entreprise ou l'association devra s'assurer de la conformité de la chambre funéraire auprès du Comité français d'accréditation (Cofrac) ou par un autre organisme d'accréditation signataire de l'accord de reconnaissance multilatérale.

*Pour aller plus loin* : articles R. 2223-59, R. 2223-74 et D. 2223-87 du Code général des collectivités territoriales.

#### Le cas échéant, obtenir une attestation de conformité du crématorium

Tout établissement souhaitant disposer d'un crématorium doit, au préalable, obtenir une autorisation de l’administration et d’une attestation de conformité délivrée par le Cofrac ou par un autre organisme d'accréditation signataire de l'accord de reconnaissance multilatérale. Le silence gardé de l'administration sur la création d'un crématorium vaudra rejet de la demande d’autorisation.

*Pour aller plus loin* : articles R. 2223-99 à R. 2223-109 du Code général des collectivités territoriales.

#### Le cas échéant, obtenir une attestation de conformité des véhicules utilisés pour la mise en bière

Le vendeur d'un véhicule de transport utilisé pour la mise en bière de personnes doit avoir obtenu une attestation de conformité délivrée par le Cofrac ou par un autre organisme d'accréditation signataire de l'accord de reconnaissance multilatérale.

Les véhicules de transport font l'objet d'une visite de conformité tous les trois ans ou dans les six mois précédent le renouvellement de la demande d'habilitation.

*Pour aller plus loin* : articles D. 2223-110 à D. 2223-115 du Code général des collectivités territoriales.