﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS105" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Traitement des déchets : pneus" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="traitement-des-dechets-pneus" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/traitement-des-dechets-pneus.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="traitement-des-dechets-pneus" -->
<!-- var(translation)="None" -->

# Traitement des déchets : pneus

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité de traitement des déchets de pneumatiques consiste à réaliser l'ensemble des opérations de réutilisation et de valorisation de ces déchets.

L'opération de valorisation vise à permettre aux déchets de servir des fins utiles, et à les préparer en vue de leur réemploi.

*Pour aller plus loin* : article R. 543-138 du Code de l'environnement.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée : L'activité étant de nature commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exerce une activité d'achat-revente son activité sera à la fois commerciale et artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucune qualification professionnelle n'est requise pour le professionnel souhaitant exercer l'activité de traitement des déchets de pneumatiques.

Toutefois, lorsque son activité relève des installations classées pour la protection de l'environnement, le professionnel sera tenu de souscrire des garanties financières et de procéder à des formalités spécifiques (cf. infra « 2°. Garanties financières » et « 3°. Démarches et formalités d'installation »).

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services (LPS) ou Libre établissement (LE))

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer l'activité de traitement des déchets de pneumatiques, à titre temporaire et occasionnel ou permanent, en France.

À ce titre, l'intéressé est soumis aux mêmes exigences que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation »).

### c. Quelques particularités de la réglementation de l’activité

#### Hiérarchie de traitement des déchets

Le professionnel assurant le traitement des déchets de pneumatiques est tenu au respect de la politique nationale de prévention et de gestion des déchets. Cette politique prévoit une hiérarchie des modes de traitement des déchets en vue de favoriser l'économie dite circulaire.

À ce titre, les déchets de pneumatiques doivent faire l'objet :

- d'une préparation en vue de leur réutilisation ;
- d'un recyclage ;
- de modes de valorisation telle que la valorisation énergétique.

*Pour aller plus loin* : articles L. 541-1 et R. 543-140 du Code de l'environnement.

#### Garanties financières

Le professionnel est tenu de souscrire des garanties financières dès lors que son activité relève des installations classées pour la protection de l'environnement soumises à autorisation (cf. infra « 3°. b. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE) »).

Ces garanties financières peuvent résulter au choix :

- d'un engagement écrit d'un établissement de crédit, d'assurance, de financement ou de caution mutuelle ;
- d'un fonds de garantie géré par l'Ademe ;
- d'une consignation entre les mains de la Caisse des dépôts et consignations ;
- d'un engagement écrit portant garantie autonome d'une personne titulaire de plus de la moitié du capital de l'exploitant.

Ces garanties financières doivent être constituées pour une durée minimale de deux ans et doivent être renouvelées au moins trois mois avant leur échéance.

Ces garanties financières visent à couvrir les risques liés aux dépenses pour :

- la mise en sécurité du site de l'exploitation du professionnel ;
- les interventions en cas d'accident ou de pollution ;
- la remise en l'état du site après sa fermeture.

*Pour aller plus loin* : article R. 516-1 et suivants du Code de l'environnement ; arrêté du 31 mai 2012 fixant la liste des installations classées soumises à l'obligation de constitution de garanties financières en application du 5° de l'article R. 516-1 du Code de l'environnement.

## 3°. Démarches et formalités d’installation

### a. Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE)

L'activité de traitement des déchets pneumatiques est susceptible de relever de la réglementation en matière d'installations classées pour la protection de l'environnement (ICPE).

L'activité entre dans le champ de la rubrique 2791 : Installation de traitement de déchets non dangereux, à l'exclusion des installations visées aux rubriques 2720, 2760, 2771, 2780, 2781, 2782 et 2971.

Le professionnel est tenu d'effectuer :

- une demande d'autorisation dès lors que la quantité de déchets traités est supérieure ou égale à 10 tonnes par jour (t/j) ;
- une déclaration préalable si cette quantité est inférieure à 10t/j.

Il est conseillé de se reporter à la nomenclature des ICPE, disponible sur le site de l'[Aida](https://aida.ineris.fr/) pour de plus amples informations.

### b. Formalités de déclaration de l’entreprise

En cas de création d'une société commerciale, le professionnel est tenu de s'immatriculer au registre du commerce et des sociétés (RCS).

#### Autorité compétente

Le professionnel doit effectuer une déclaration auprès de la chambre de commerce et d'industrie (CCI).

#### Pièces justificatives

Pour cela, il doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI lui adresse le jour même, un récépissé indiquant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais indiqués ci-dessus.

Si le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.