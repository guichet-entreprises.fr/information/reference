﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS105" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Waste tyre management" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="waste-tyre-management" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/waste-tyre-management.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="waste-tyre-management" -->
<!-- var(translation)="Auto" -->

Waste tyre management
======================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The activity of processing tyre waste consists of carrying out all the operations of reuse and recovery of this waste.

The purpose of the reclamation operation is to enable waste to be used for useful purposes, and to prepare it for reuse.

*To go further* Article R. 543-138 of the Environment Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out: The activity being commercial in nature, the competent CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional carries out a buying and resale activity his activity will be both commercial and artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

No professional qualifications are required for the professional wishing to carry out the tyre waste treatment activity.

However, when its activity falls under facilities classified for the protection of the environment, the professional will be required to take out financial guarantees and to carry out specific formalities (see infra "2." Financial guarantees" and "3 degrees. Installation procedures and formalities").

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There is no provision for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement to carry out the tyre waste treatment activity, on a temporary and temporary basis. casual or permanent in France.

As such, the person concerned is subject to the same requirements as the French national (see infra "3°. Installation procedures and formalities").

### c. Some peculiarities of the regulation of the activity

**Waste treatment hierarchy**

The professional responsible for the treatment of tyre waste is bound by the national waste prevention and management policy. This policy provides for a hierarchy of waste treatment methods in order to promote the so-called circular economy.

As such, tyre waste must be subject to:

- Preparation for reuse
- Recycling
- development methods such as energy recovery.

*To go further* Articles L. 541-1 and R. 543-140 of the Environment Code.

**Financial guarantees**

The professional is obliged to take out financial guarantees as long as his activity falls under the classified facilities for the protection of the environment subject to authorisation (see infra "3." b. If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)).

These financial guarantees may result in:

- a written commitment from a credit, insurance, financing or mutual bond institution;
- A guarantee fund managed by Ademe;
- a deposit in the hands of the Caisse des dépôts et consignments;
- a written commitment with a stand-alone guarantee from a person holding more than half of the operator's capital.

These financial guarantees must be provided for a minimum of two years and must be renewed at least three months before their maturity.

These financial guarantees are intended to cover the risks associated with expenses to:

- The safety of the site of the professional's operation;
- Interventions in the event of an accident or pollution;
- re-conditioned after its closure.

*To go further* Article R. 516-1 and the following of the Environment Code; decree of 31 May 2012 setting out the list of classified facilities subject to the obligation to establish financial guarantees under article R. 516-1 of the Environment Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)

The activity of pneumatic waste is likely to fall under the regulations of classified facilities for the protection of the environment (ICPE).

Activity is within the scope of heading 2791: Non-hazardous waste treatment facility, excluding facilities under 2720, 2760, 2771, 2780, 2781, 2782 and 2971.

The professional is required to perform:

- a request for authorisation if the amount of waste treated is greater than or equal to 10 tonnes per day (t/d);
- pre-reporting if this amount is less than 10t/d.

It is advisable to refer to the nomenclature of the ICPE, available on the[Aida](https://aida.ineris.fr/) for more information.

### b. Company reporting formalities

In the event of the creation of a commercial company, the professional is required to register in the Register of Trade and Companies (RCS).

**Competent authority**

The professional must make a declaration to the Chamber of Commerce and Industry (CCI).

**Supporting documents**

In order to do so, it must provide[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sent him a receipt on the same day indicating the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the time frames mentioned above.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*To go further* Section 635 of the General Tax Code.

