﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS056" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Personal services" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="personal-services" -->
<!-- var(title-short)="human-services" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/personal-services.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="personal-services" -->
<!-- var(translation)="Auto" -->


Personal services
==============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The professional engaged in a human services activity (SAP) helps to improve the daily lives of individuals. These services of a diverse nature are carried out in the homes of the beneficiaries of the benefit or from their homes.

SAP's activity includes a set of activities that may be subject to an optional declaration and some of which are subject to approval.

Activities subject to approval include:

# home care for children under the age of 3 and under 18 with disabilities;
# accompanying children under the age of 3 and under the age of 18 with disabilities in their travels outside their homes (walks, transport, everyday acts);
# assistance in the daily activities of life or assistance to social integration to the elderly and persons with disabilities or those with chronic conditions who need such services at home, excluding acts of care related to acts of care related to acts of care Medical
# driving the personal vehicle of the elderly, disabled or with chronic conditions from home to work, at the place of vacation, for administrative procedures;
# support for the elderly, people with disabilities or chronic conditions, in their travels outside their homes (walks, mobility and transport assistance, everyday acts).

Activities that are not subject to approval are:

# housekeeping and housework;
# small gardening work, including clearing work;
# small diy jobs called "man all hands";
# child care at home above an age set by joint decree of the Minister for the Economy and the Minister for the Family;
# home school support or home schooling
# home-based cosmetics for dependents;
# preparing meals at home, including time spent at the races;
# home meal delivery;
# collection and home delivery of ironed linen;
# home grocery delivery;
# Home-based IT support
# care and walks of pets, with the exception of veterinary care and grooming, for dependents;
# Temporary maintenance, maintenance and vigilance, at home, of the main and secondary residence;
# administrative assistance at home;
# accompanying children over the age of three in their travels outside their homes (walks, transport, everyday acts);
# tele-assistance and video assistance;
# Sign language interpreter, written technician and spoken language coder completed;
# the personal vehicle of the persons mentioned in the 20th of the II of this article, from home to work, at the place of vacation, for administrative procedures;
# Temporaryly dependent support in their travels outside their homes (walks, mobility and transport assistance, everyday acts);
# assistance to people temporarily dependent, at home, excluding medical care;
# coordination and delivery of the 20 services previously listed.

All SAP activities, whether or not they fall under the accreditation, are subject, on an optional basis, to be reported to qualify for the tax and social benefits of AMPs.

*For further information*: Articles L.7231-1, L.7233-2 and D.7231-1 of the Labour Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For craft activities, the competent CFE is the Chamber of Trades and Crafts (CMA);
- For commercial activities, it is the Chamber of Commerce and Industry (CCI);
- for the exercise of a liberal activity, the competent CFE is the Urssaf.

All procedures can be carried out online directly on guichet-entreprises.fr, which transmits to the relevant CFE.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal.

2°. Installation conditions
------------------------------------

### a. Conditions for obtaining accreditation

For approval:

# the professional must have, within or within the network of which he is a part, the human, material and financial means to carry out the SAP services for which accreditation is sought;
# it must commit to complying with the specifications set out in the decree of 1 October 2018 under Article R.7232-6 of the Labour Code;
# Executives must not have been the subject of a criminal conviction or a civil, commercial or administrative sanction such as to prohibit them from managing, administering or directing a corporation or from carrying out a commercial activity;
# where SAP's activity is related to minors, leaders, supervisors and stakeholders should not be registered in the national automated judicial file of perpetrators of sexual offences, or equivalent, for nationals of a Member State European Union or another state party to the European Economic Area agreement.

Once the professional meets these conditions, he must apply for certification in order to practice his profession.

*For further information*: Article R. R7232-6 of the Labour Code.

### b. Professional qualifications

In order to carry out SAP's approved activities, the professional must justify professional qualifications specified in the decree of 1 October 2018 setting out the specifications set out in Article R. 7232-6 of the Labour Code.

### c. Professional Qualifications - European Nationals (Free Service Or Freedom of establishment)

A national of a Member State of the European Union (EU) or party to the Agreement on the European Economic Area (EEA) who wishes to carry out an SAP activity in France on a temporary and occasional basis (as part of a free provision of services (LPS ) or on a permanent basis (as part of a Freedom of establishment) is subject to the professional qualification requirements specified in the order of 1 October 2018 setting out the specifications set out in Article R. 7232-6 of the Labour Code.

### d. Some peculiarities of the regulation of the activity

#### Tax measures

As long as the professional carries out his SAP activity exclusively (without any other activity within the same structure), he can benefit from tax advantages (see Articles 279 and 199 sexdecies of the General Tax Code). To do this, the professional must make a declaration of activity (see infra "3 degrees. Installation procedures and formalities for approved activities").

For some activities, the professional can only benefit from these benefits if the benefit is provided as part of a set of activities carried out at home (see III Article D. 7231-1 of the Labour Code to find out what activities are involved).

*For further information*: In: circular of April 11, 2019 (NOR: ECOI1907576C).

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company registration

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Application for accreditation

#### Competent authority

The application for approval is addressed to the prefect of department electronically or by letter recommended with notice of receipt.

#### Supporting documents

The application for accreditation states:

# The organization's name
# The address of the organization and its institutions
# The nature of the services performed and the public or customers concerned;
# The conditions of employment of staff;
# the means of exploitation implemented.

A file is attached to the application for approval, including:

# an extract from the register of trades and companies or the directory of trades or a copy of the statutes of the legal person, or, if necessary, for nationals of a Member State of the European Union or another State party to the Agreement on the Area European economics, an equivalent document;
# Elements to assess the level of quality of the services implemented;
# A document model that provides information for clients and users on tax matters and administrative services in statistical matters;
# The list of subcontractors;

*For further information*: the application for approval is made under the conditions set out in Articles R.7232-1 to R.7232-3 of the Labour Code and by points 42 and 67 of the specifications of 1 October 2018.

**Please note**

Professionals legally based in another EU Member State or party to the European Economic Area Agreement attach to their file any information and documents relating to their situation in relation to the implementation of the obligations, if any, by applicable legislation in the state in which they are based, for the purpose of reviewing their application for accreditation.

#### Outcome of the procedure

If the file is incomplete, the prefect informs the professional and invites him to produce the missing documents or information. The silence kept per the prefect for more than three months from the date of receipt of a full application for accreditation carries decision of acceptance.

*For further information*: Article R7232-4 of the Labour Code

**Please note**

If the professional plans to carry out the activity subject to accreditation in several departments, the prefect of the department of the place of establishment of the main professional establishment gathers the opinion of the presidents of the departmental council of the departments, through the territorially competent prefects.

#### Validity and renewal

Accreditation is valid for five years and any renewal application must be sent to department prefect no later than three months before the end of the previous accreditation.

*For further information*: Articles R. 7232-7 and the following articles of the Labour Code.

#### Business report

The certified professional produces at least a quarterly state of activity and each year a qualitative and quantitative assessment of the activity carried out for the past year as well as an annual statistical table. These documents are sent electronically to the prefect, who makes them available to the Minister responsible for the economy. When the professional has several establishments, the statistical statements and the annual balance sheet distinguish the activity carried out by each institution.

*For further information*: Articles R. 7232-9 and the following articles of the Labour Code.

4°. Installation procedures and formalities for reporting activities
----------------------------------------------------------------------------------------

### a. Company registration

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Tax utility of the return

The declaration allows professionals to be entitled to the tax benefits provided for in Article L. 7233-2 of the Labour Code (tax credit provided by Article 199 sexdecies of the General Tax Code and, as appropriate, reduced VAT rate provided for in the i of Article 279 or D of Article 278-0 bis of the same C ode for some of their approved activities), as well as the exemptions from social contributions referred to in Article L. 241-10 of the Social Security Code for Personal Service Activities section D. 7231-1 of the Labour Code.

The declaration covers both activities requiring prior approval or authorization as well as activities that can be conducted freely.

The professional undertakes to carry out only the activities specified in his declaration, among the activities of SAP listed in Article D. 7231-1 of the Labour Code and subject to having obtained accreditation for the activities under its jurisdiction.

*For further information*: In: circular of April 11, 2019 (NOR: ECOI1907576C).

### c. Request to register the return

#### Competent authority

The declaration is addressed to the territorially competent Dirrecte electronically or by recommended letter with notice of receipt. The request is made via the NOVA (www.entreprises.gouv.fr/services-a-la-personne) application or failing that on a standard form.

#### Content of the declaration file

The reporting file includes:

# The professional's name and address
# The address of the professional's main institution and the address of his secondary schools;
# Mention of SAP's activities
# for professionals subject to the condition of exclusive activity, the commitment of the legal representative to exercise exclusively the services to the person subject to the declaration, excluding any other service or provision of goods (Article L. 7232-1-1 of the Labour Code);
# for professionals exempted from the condition of exclusive activity, the legal representative's commitment to set up a separate accounting to account for expenses and products related to their personal services activities alone ( Article L. 7232-1-2 of the Labour Code);
# for the activities mentioned in the 2nd, 4th, 5th of the I and the 8th, 9th, 10th, 15th, 18th and 19th of the II of Article D. 7231-1 of the Labour Code, the professional undertakes to associate the delivery and transport services with one or more personal services activities. (a condition known as a global offer provided for in the III period of Article D. 7231-1).

This list is exhaustive, no other documents can be requested by the registrant by the Dirrecte.

*For further information*: Articles R.7232-16 to R.7232-22 of the Labour Code and Circular of April 11, 2019 (NOR: ECOI1907576C).

#### Outcome of the procedure

Upon receipt of the file, its completeness is verified, including the inscription in the Sirene directory or the national directory of associations.

When the declaration is complete the Dirrecte registers it within eight days and issues a receipt electronically or postally to the applicant.

When the file is incomplete, a letter is sent to the applicant to inform the applicant of the missing information.

*For further information*: Article R7232-18 of the Labour Code and Circular of 11 April 2019 (NOR: ECOI1907576C).

**Please note**

The professional who has made a declaration undertakes to affix on all his commercial media the logotype identifying the human services sector. It is made available free of charge to corporations and individual contractors by downloading to NOVA, after acceptance of the specific conditions of the license of the tracker services to the person.

#### Validity and renewal

The declaration takes effect on the day of filing when the file is complete and meets all regulatory requirements. It is not time-limited, and has a national reach.

*For further information*: In: circular of April 11, 2019 (NOR: ECOI1907576C).

#### Business report

The professional who has made a return produces at least a quarterly state of activity and each year a qualitative and quantitative assessment of the activity carried out for the past year as well as an annual statistical table. These documents are sent electronically to the prefect, who makes them available to the Minister responsible for the economy. When the professional has several establishments, the statistical statements and the annual balance sheet distinguish the activity carried out by each institution.

*For further information*: Article R.7232-19 of the Labour Code.

5°. Billing SAP
-------------------------

### a. The bill

SAP providers must file an invoice showing:

# The professional's name and address
# The number and registration date of the return if requested, as well as the number and date of issuance of the authorization when the activities fall under Article L. 7232-1;
# The name and address of the recipient of the service
# The exact nature of the services provided
# The amount of money actually paid for the service;
# A registration number of the intervener allowing his identification in the registers of employees of the company or the association providing;
# hourly labour rates all taxes included or, if applicable, the flat rate of the benefit;
# Counting the time spent
# the price of the various services and when the benefits are financially supported directly by the funder to the service, the price remains at the expense of the beneficiary of the benefit;
# If so, travel expenses
# If applicable, the name and registration number of the subcontractor who performed the service;
# where the professional is certified under Section L. 7231-1 but is not declared under Section L. 7232-1-1, the quotes, invoices and business documents indicate that the benefits provided do not qualify for the tax benefits provided by Article L. 7233-2.

Similarly, when SAP activity is offered to the user by a third party in the form of a card, voucher or gift pass, quotes, invoices and business documents state that the services provided do not qualify for tax benefits. Article L. 7233-2.

When SAP benefits are taxable on VAT, rates, prices and travel expenses include this tax.

*For further information*: Article D.7233-1, D.7233-2 of the Labour Code and Circular of April 11, 2019 (NOR:ECOI1907576C).

### b. Tax certificate

The declared professional must provide each of his clients with an annual tax certificate by 31 March of year 1, in order to enable them to benefit from the tax benefit defined in Section 199 sexdecies of the General Tax Code under the imposition of the N-year.

This certificate should include:

# The professional's name, address and identification number
# The number and date of registration of the return
# The name and address of the person who benefited from the service, the number of his account debited if applicable, the amount actually paid;
# a summary of the interventions taken (name and number of the speaker's identification code, date and duration of the intervention).

In cases where benefits are paid in pre-funded CESU, the certificate must indicate to the client that he is obliged to clearly identify with the tax authorities, on his annual tax return, the amount of the CESUs he has personally funded, this amount alone giving rise to tax benefits.

Cash payments do not qualify for a tax certificate.

*For further information*: Article D. 7233-3, Article D. 7233-4 of the Labour Code and Circular of April 11, 2019 (NOR:ECOI1907576C).

#### Remedies

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the CFE refuses to receive the file, the applicant has an appeal before the administrative court.

#### Cost

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

