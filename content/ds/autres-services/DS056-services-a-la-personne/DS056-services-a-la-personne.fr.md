﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS056" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Services à la personne" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="services-a-la-personne" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/services-a-la-personne.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="services-a-la-personne" -->
<!-- var(translation)="None" -->

# Services à la personne

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le professionnel exerçant une activité de services à la personne (SAP) contribue à améliorer le quotidien des personnes physiques. Ces services de nature diverse s'exercent au domicile des bénéficiaires de la prestation ou à partir de leur domicile.

L'activité de SAP comprend un ensemble d'activités qui peuvent être soumises à titre facultatif à une déclaration et dont certaines sont soumises à un agrément.

Les activités soumises à agrément sont les suivantes :

1. garde d'enfants à domicile de moins de 3 ans et de moins de 18 ans en situation de handicap ;
2. accompagnement des enfants de moins de 3 ans et de moins de 18 ans en situation de handicap dans leurs déplacements en dehors de leur domicile (promenades, transport, actes de la vie courante) ;
3. assistance dans les actes quotidiens de la vie ou aide à l'insertion sociale aux personnes âgées et aux personnes handicapées ou atteintes de pathologies chroniques qui ont besoin de telles prestations à domicile, à l'exclusion d'actes de soins relevant d'actes médicaux ;
4. prestation de conduite du véhicule personnel des personnes âgées, des personnes handicapées ou atteintes de pathologies chroniques du domicile au travail, sur le lieu de vacances, pour les démarches administratives ;
5. accompagnement des personnes âgées, des personnes handicapées ou atteintes de pathologies chroniques, dans leurs déplacements en dehors de leur domicile (promenades, aide à la mobilité et au transport, actes de la vie courante).

Les activités qui ne sont pas soumises à agrément sont :

1. entretien de la maison et travaux ménagers ;
2. petits travaux de jardinage, y compris les travaux de débroussaillage ;
3. travaux de petit bricolage dits « homme toutes mains » ;
4. garde d'enfants à domicile au-dessus d'un âge fixé par arrêté conjoint du ministre chargé de l'économie et du ministre chargé de la famille ;
5. soutien scolaire à domicile ou cours à domicile ;
6. soins d'esthétique à domicile pour les personnes dépendantes ;
7. préparation de repas à domicile, y compris le temps passé aux courses ;
8. livraison de repas à domicile ;
9. collecte et livraison à domicile de linge repassé ;
10. livraison de courses à domicile ;
11. assistance informatique à domicile ;
12. soins et promenades d'animaux de compagnie, à l'exception des soins vétérinaires et du toilettage, pour les personnes dépendantes ;
13. maintenance, entretien et vigilance temporaires, à domicile, de la résidence principale et secondaire ;
14. assistance administrative à domicile ;
15. accompagnement des enfants de plus de trois ans dans leurs déplacements en dehors de leur domicile (promenades, transport, actes de la vie courante) ; 
16. téléassistance et visio assistance ;
17. interprète en langue des signes, technicien de l'écrit et codeur en langage parlé complété ;
18. prestation de conduite du véhicule personnel des personnes mentionnées au 20° du II du présent article, du domicile au travail, sur le lieu de vacances, pour les démarches administratives ;
19. accompagnement temporairement dépendantes dans leurs déplacements en dehors de leur domicile (promenades, aide à la mobilité et au transport, actes de la vie courante) ;
20. assistance aux personnes temporairement dépendantes, à leur domicile, à l'exclusion des soins relevant d'actes médicaux ;
21. coordination et délivrance des 20 services précédemment listés.

Toutes les activités de SAP qu'elles relèvent ou non de l'agrément, sont soumises, à titre facultatif, à déclaration pour ouvrir droit aux avantages fiscaux et sociaux des SAP.

*Pour aller plus loin* : articles L.7231-1, L.7233-2 et D.7231-1 du Code du travail.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

- pour les activités artisanales, le CFE compétent est la chambre des métiers et de l'artisanat (CMA) ;
- pour les activités commerciales, il s'agit de la chambre de commerce et d'industrie (CCI) ;
- pour l'exercice d'une activité libérale, le CFE compétent est l'Urssaf.

L'ensemble des démarches peut être effectué en ligne directement sur guichet-entreprises.fr, qui transmet au CFE compétent. 

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale.

## 2°. Conditions d’installation

### a. Conditions d'obtention de l'agrément

Pour obtenir un agrément :

1. le professionnel doit disposer, en propre ou au sein du réseau dont il fait partie, des moyens humains, matériels et financiers permettant de réaliser les prestations de SAP pour lesquelles l’agrément est sollicité ;
2. il doit s’engager à respecter le cahier des charges fixé par l’arrêté du 1er octobre 2018 pris en application de l’article R.7232-6 du Code du travail ;
3. les dirigeants ne doivent pas avoir fait l'objet d'une condamnation pénale ni d'une sanction civile, commerciale ou administrative de nature à leur interdire de gérer, administrer ou diriger une personne morale ou d'exercer une activité commerciale ;
4. lorsque l'activité de SAP est en lien avec les mineurs, les dirigeant, encadrants et intervenants ne doivent pas être inscrits au fichier judiciaire national automatisé des auteurs d'infractions sexuelles, ou équivalent, pour les ressortissants d'un État membre de l'Union européenne ou d'un autre État partie à l'accord sur l'Espace économique européen.

Dès lors qu'il remplit ces conditions, le professionnel doit effectuer une demande d'agrément en vue d'exercer sa profession.

*Pour aller plus loin* : article R. R7232-6 du Code du travail.

### b. Qualifications professionnelles

Pour exercer les activités de SAP soumises à agrément, le professionnel doit justifier des qualifications professionnelles précisées dans l'arrêté du 1er octobre 2018 fixant le cahier des charges prévu à l’article R. 7232-6 du Code du travail.

### c. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) qui souhaite exercer une activité de SAP en France de manière temporaire et occasionnelle (dans le cadre d'une libre prestation de services (LPS)) ou à titre permanent (dans le cadre d'un libre établissement (LE)) est soumis aux exigences de qualifications professionnelles précisées dans l'arrêté du 1er octobre 2018 fixant le cahier des charges prévu à l’article R. 7232-6 du Code du travail.

### d. Quelques particularités de la réglementation de l’activité

#### Mesures fiscales

Dès lors que le professionnel exerce son activité de SAP à titre exclusif (sans aucune autre activité au sein de la même structure), il peut bénéficier d'avantages fiscaux (cf. articles 279 et 199 sexdecies du Code général des impôts). Pour cela, le professionnel doit effectuer une déclaration d'activité (cf. infra « 3°. Démarches et formalités d’installation pour les activités soumises à agrément »).

Pour certaines activités, le professionnel ne peut bénéficier de ces avantages que si la prestation est fournie dans le cadre d'un ensemble d'activités effectuées à domicile (cf. III article D. 7231-1 du Code du travail pour connaître les activités concernées).

*Pour aller plus loin* : circulaire du 11 avril 2019 (NOR : ECOI1907576C).

## 3°. Démarches et formalités d’installation

### a. Immatriculation de l'entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Demande d'agrément

#### Autorité compétente

La demande d’agrément est adressée au préfet de département par voie électronique ou par lettre recommandée avec avis de réception.

#### Pièces justificatives

La demande d'agrément mentionne :

1. la raison sociale de l'organisme ;
2. l'adresse de l'organisme et de ses établissements ;
3. la nature des prestations effectuées et des publics ou clients concernés ;
4. les conditions d'emploi du personnel ;
5. les moyens d'exploitation mis en œuvre.

À la demande d'agrément est joint un dossier comprenant :

1. un extrait du registre du commerce et des sociétés ou du répertoire des métiers ou une copie des statuts de la personne morale, ou, le cas échéant, pour les ressortissants d'un État membre de l'Union européenne ou d'un autre État partie à l'accord sur l'Espace économique européen, un document équivalent ;
2. les éléments permettant d'apprécier le niveau de qualité des services mis en œuvre ;
3. un modèle de document prévoyant une information des clients et des usagers en matière fiscale et des services administratifs en matière statistique ;
4. la liste des sous-traitants.

*Pour aller plus loin* : la demande d’agrément est formulée dans les conditions fixées par les articles R.7232-1 à R.7232-3 du Code du travail et par les points 42 et 67 du cahier des charges du 1er octobre 2018.

**À noter**

Les professionnels légalement établis dans un autre État membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen joignent à leur dossier toute information et tout document relatifs à leur situation au regard de la mise en œuvre des obligations prévues, le cas échéant, par la législation applicable dans l'Etat où ils sont établis, en vue de l'examen de leur demande d'agrément.

#### Issue de la procédure

Si le dossier est incomplet, le préfet en informe le professionnel et l’invite à produire les pièces ou informations manquantes. Le silence gardé per le préfet pendant plus de trois mois à compter de la date de réception d’un dossier complet de demande d’agrément emporte décision d’acceptation.

*Pour aller plus loin* : article R7232-4 du Code du travail

**À noter**

Si le professionnel projette d'exercer l'activité soumise à agrément dans plusieurs départements, le préfet du département du lieu d'implantation du principal établissement du professionnel recueille l'avis des présidents de conseil départemental des départements intéressés, par l'intermédiaire des préfets territorialement compétents. 

#### Durée de validité et renouvellement

L'agrément est valable cinq ans et toute demande de renouvellement doit être adressée préfet de département au plus tard trois mois avant le terme de l'agrément précédent.

*Pour aller plus loin* : articles R. 7232-7 et suivants du Code du travail.

#### Bilan d'activité

Le professionnel agréé produit au moins chaque trimestre un état d'activité et chaque année un bilan qualitatif et quantitatif de l'activité exercée au titre de l'année écoulée ainsi qu'un tableau statistique annuel. Ces documents sont adressés par voie électronique au préfet, qui les rend accessibles au ministre chargé de l'économie. Lorsque le professionnel dispose de plusieurs établissements, les états statistiques et le bilan annuel distinguent l'activité exercée par chaque établissement.

*Pour aller plus loin* : articles R. 7232-9 et suivants du Code du travail.

## 4°. Démarches et formalités d’installation pour les activités soumises à déclaration

### a. Immatriculation de l'entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Utilité fiscale de la déclaration

La déclaration permet aux professionnels d’ouvrir droit aux avantages fiscaux prévus aux 1° et 2° de l’article L. 7233-2 du Code du travail (crédit d’impôt prévu par l’article 199 sexdecies du Code général des impôts et, selon les cas, taux réduit de TVA prévu au i de l’article 279 ou au D de l’article 278-0 bis du même C ode pour certaines de leurs activités agréées), ainsi qu’aux exonérations de cotisations sociales mentionnées à l’article L. 241-10 du Code de la sécurité sociale pour les activités de service à la personne mentionnées à l’article D. 7231-1 du Code du travail.

La déclaration concerne aussi bien les activités nécessitant un agrément préalable ou une autorisation que les activités qui peuvent s’exercer librement. 

Le professionnel s’engage à n’exercer que les activités précisées dans sa déclaration, parmi les activités de SAP listés à l’article D. 7231-1 du Code du travail et sous réserve d’avoir obtenu l’agrément pour les activités qui en relèvent.

*Pour aller plus loin* : circulaire du 11 avril 2019 (NOR : ECOI1907576C).

### c. Demande d’enregistrement de la déclaration

#### Autorité compétente

La déclaration est adressée à la Dirrecte territorialement compétente par voie électronique ou par lettre recommandée avec avis de réception. La demande est effectuée via l’application NOVA (www.entreprises.gouv.fr/services-a-la-personne) ou à défaut sur un formulaire type.

#### Contenu du dossier de déclaration

Le dossier de déclaration comprend :

1. la raison sociale du professionnel et son adresse ;
2. l’adresse du principal établissement du professionnel ainsi que l’adresse de ses établissements secondaires ;
3. la mention des activités de SAP ;
4. pour les professionnels soumis à la condition d’activité exclusive, l’engagement du représentant légal à exercer exclusivement les services à la personne objets de la déclaration, à l’exclusion de tout autre service ou de toute fourniture de biens (article L. 7232-1-1 du Code du travail) ;
5. pour les professionnels dispensés de la condition d’activité exclusive, l’engagement du représentant légal à mettre en place une comptabilité séparée permettant de rendre compte des charges et produits liés à leurs seules activités de services à la personne (article L. 7232-1-2 du Code du travail) ;
6. pour les activités mentionnées aux 2°, 4°, 5° du I et aux 8°, 9°, 10 , 15°, 18° et 19° du II de l’article D. 7231-1 du Code du travail, le professionnel s’engage à associer les prestations de livraison et transport à une ou plusieurs activités de services à la personne réalisées à domicile (condition dite d’offre globale prévue au III de l’article D. 7231-1).

Cette liste est exhaustive, aucun autre document ne peut être demandé au déclarant par la Dirrecte.

*Pour aller plus loin* : articles R.7232-16 à R.7232-22 du Code du travail et circulaire du 11 avril 2019 (NOR : ECOI1907576C).

#### Issue de la procédure

Dès réception du dossier, sa complétude est vérifiée, notamment l’inscription au répertoire Sirene ou du Répertoire national des associations.

Lorsque la déclaration est complète la Dirrecte l’enregistre dans les huit jours et délivre un récépissé par voie électronique ou postale au demandeur.

Lorsque le dossier est incomplet, un courrier est adressé au demandeur pour lui indiquer les informations manquantes.

*Pour aller plus loin* : article R7232-18 du Code du travail et circulaire du 11 avril 2019 (NOR : ECOI1907576C).

**À noter**

Le professionnel qui a effectué une déclaration s’engage à apposer sur tous ses supports commerciaux le logotype identifiant le secteur des services à la personne. Il est mis gratuitement à la disposition des personnes morales et des entrepreneurs individuels par téléchargement sur NOVA, après acceptation des conditions particulières de la licence du traceur services à la personne. 

#### Durée de validité et renouvellement

La déclaration prend effet au jour de son dépôt lorsque le dossier est complet et répond à toutes les exigences de la réglementation. Elle n’est pas limitée dans le temps, et a une portée nationale.

*Pour aller plus loin* : circulaire du 11 avril 2019 (NOR : ECOI1907576C).

#### Bilan d'activité

Le professionnel qui a effectué une déclaration produit au moins chaque trimestre un état d'activité et chaque année un bilan qualitatif et quantitatif de l'activité exercée au titre de l'année écoulée ainsi qu'un tableau statistique annuel. Ces documents sont adressés par voie électronique au préfet, qui les rend accessibles au ministre chargé de l'économie. Lorsque le professionnel dispose de plusieurs établissements, les états statistiques et le bilan annuel distinguent l'activité exercée par chaque établissement. 

*Pour aller plus loin* : article R.7232-19 du Code du travail.

## 5°. Facturation des SAP

### a. La facture

Les fournisseurs de SAP doivent produire une facture faisant apparaître :

1. le nom et l’adresse du professionnel ;
2. le numéro et la date d'enregistrement de la déclaration si celle-ci a été demandée, ainsi que le numéro et la date de délivrance de l'agrément lorsque les activités relèvent de l’article L. 7232-1 ;
3. le nom et l'adresse du bénéficiaire de la prestation de service ;
4. la nature exacte des services fournis ;
5. le montant des sommes effectivement acquittées au titre de la prestation de service ;
6. un numéro d’immatriculation de l’intervenant permettant son identification dans les registres des salariés de l’entreprise ou de l’association prestataire ;
7. les taux horaires de main-d’oeuvre toutes taxes comprises ou, le cas échéant, le prix forfaitaire de la prestation ;
8. le décompte du temps passé ;
9. le prix des différentes prestations et lorsque les prestations font l’objet d’une prise en charge financière directement versée par son financeur au service, le prix restant à la charge du bénéficiaire de la prestation ;
10. le cas échéant, les frais de déplacement ;
11. le cas échéant, le nom et le numéro d’agrément du sous-traitant ayant effectué la prestation ;
12. lorsque le professionnel est agréé en application de l’article L. 7231-1 mais non déclaré au titre de l'article L. 7232-1-1, les devis, factures et documents commerciaux indiquent que les prestations fournies n'ouvrent pas droit aux avantages fiscaux prévus par l'article L. 7233-2.

De la même manière, lorsque l’activité de SAP est offerte à l’utilisateur par une tierce personne sous forme de carte, bon ou pass-cadeau, les devis, factures et documents commerciaux précisent que les prestations fournies n'ouvrent pas droit aux avantages fiscaux prévus par l'article L. 7233-2.

Lorsque les prestations de SAP sont imposables à la TVA, les taux, prix et frais de déplacement comprennent cette taxe.

*Pour aller plus loin* : article D.7233-1, D.7233-2 du Code du travail et circulaire du 11 avril 2019 (NOR :ECOI1907576C).

### b. Attestation fiscale

Le professionnel déclaré doit communiquer avant le 31 mars de l’année N+1 à chacun de ses clients une attestation fiscale annuelle, afin de leur permettre de bénéficier de l’avantage fiscal défini à l’article 199 sexdecies du Code général des impôts au titre de l’imposition de l’année N. 

Cette attestation doit mentionner :

1. le nom, l’adresse et le numéro d’identification du professionnel ;
2. le numéro et la date d’enregistrement de la déclaration ;
3. le nom et l’adresse de la personne ayant bénéficié du service, le numéro de son compte débité le cas échéant, le montant effectivement acquitté ;
4. un récapitulatif des interventions effectuées (nom et numéro du code d’identification de l’intervenant, date et durée de l’intervention).

Dans les cas où des prestations sont acquittées en CESU préfinancé, l’attestation doit indiquer au client qu’il lui est fait obligation d’identifier clairement auprès des services des impôts, lors de sa déclaration fiscale annuelle, le montant des CESU qu’il a personnellement financé, ce montant seul donnant lieu à avantage fiscal.

Les paiements effectués en numéraire n’ouvrent pas droit à l’établissement d’une attestation fiscale.

*Pour aller plus loin* : article D. 7233-3, article D. 7233-4 du Code du travail et circulaire du 11 avril 2019 (NOR :ECOI1907576C).

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le CFE refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.