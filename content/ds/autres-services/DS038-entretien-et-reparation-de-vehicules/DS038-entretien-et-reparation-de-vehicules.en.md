﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS038" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Other services" -->
<!-- var(title)="Vehicle maintenance and repair" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="other-services" -->
<!-- var(title-short)="vehicle-maintenance-and-repair" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/other-services/vehicle-maintenance-and-repair.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="vehicle-maintenance-and-repair" -->
<!-- var(translation)="Auto" -->


Vehicle maintenance and repair
==============================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The maintenance and repair activity of vehicles consists for the professional, to carry out in a workshop (garage) the checks, checks, maintenance and cleaning of motor vehicles as well as possible repairs Necessary.

The professional may thus be required to perform:

- Draining and overhauling the vehicle
- all settings and replacement of used or defective parts (electronic, mechanical);
- Repairing the bodywork and sheet metal;
- Painting
- towing the vehicle.

It can also offer for sale:

- Fuels and lubricants
- automotive accessories
- new or used vehicles.

**Please note**

Technical control activity can only be carried out by state-approved establishments exclusively engaged in this activity. Thus, an establishment carrying out the maintenance and repair activity of the vehicles cannot exercise that of technical control.

*For further information*: Article R. 323-8 of the Highway Traffic Act.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For craft activities, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial activities, it is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use no industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

In order to carry out the maintenance and repair activity of vehicles the professional must:

- be professionally qualified or placed under the effective and permanent control of a professionally qualified person;
- have completed an installation preparation course (see below "Doing an installation preparation course (SPI) );
- be registered in the trades directory (RM) or in the register of companies in case of installation in the departments of lower Rhine, Upper Rhine or Moselle (see below. "Request for registration in the trades directory").

#### Training

To be recognized as a professionally qualified, the professional who performs the vehicle maintenance and repair activity or who controls the exercise of the vehicle, must be the holder of the choice:

- A Certificate of Professional Fitness (CAP) for motor vehicle maintenance;
- a master's degree (BM) in repairman - manager in automotive maintenance;
- a diploma or title of equal or higher level approved or registered in the National Register of Professional Certifications[RNCP](http://www.rncp.cncp.gouv.fr/).

In the absence of one of these diplomas, the person concerned will have to justify an effective three years' professional experience, in a European Union (EU) state or party to the agreement on the European Economic Area (EEA), acquired as a leader self-employed or employee in the maintenance and repair of vehicles. In this case, the person concerned will be able to apply to the relevant CMA for a certificate of professional qualification.

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 1 of Decree 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act 96-603 of 5 July 1996 relating to the development and promotion of trade and crafts.

**CAP vehicle maintenance**

This Level V diploma (corresponding to a general and technological graduate before the senior year) is available to candidates after a training course as a student, on an apprenticeship contract, after a course of continuing education, in a professionalisation contract, by individual application or through the validation of experience (VAE) acquired process. For more information, you can see[Site](http://www.vae.gouv.fr/) VAE's official.

The training normally lasts two years and takes place in a vocational high school.

**BM repairman - car maintenance manager**

The master's degree is a Level III approved designation (B.A. 2) issued by the CMA, whose two-year training is done through continuing education, a professionalization contract or validation of experience.

*For further information*: Articles D. 337-1 and the following articles of the Education Code.

#### Complete an installation preparation course (SPI)

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship is of a minimum of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including a degree in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who are:

- have been engaged in a professional activity requiring a level of knowledge equivalent to that provided by the internship for at least three years;
- have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, fully or partially, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of their professional qualifications shows substantial differences with those required in France to run a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- A copy of the Level III certified diploma;
- A copy of the master's degree
- a proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree 83-517 of June 24, 1983.

#### Application for registration in the trades directory

**Competent authority**

The professional must apply to the CMA for registration at his head office within one month of starting his activity. However, the request can be made no later than one month after the start of the activity as soon as the professional has notified the president of the CMA, the start date of the CMA, no later than the day before by letter recommended with notice of receipt.

**Supporting documents**

The list of supporting documents to be sent to the CMA depends on the nature of the activity carried out.

**Timeframe**

As soon as the CMA files its complete file, it sends a receipt for the application of a business creation file to the applicant. Failure to respond within two months is worth registration.

*For further information*: Articles 9 and following of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

**For Freedom to provide services**

The professional who is a member of the EU or the EEA may carry out the maintenance and repair of vehicles in France on a temporary and casual basis, provided that he is legally established in one of these states to carry out the same activity.

He will first have to request it by written declaration to the relevant CMA (see infra "3°. a. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)).

If neither the activity nor the training conducting there is regulated in the state in which the professional is legally established, the person must also prove that he or she has been active in that state for at least one year full-time or part-time during the of the last ten years before the performance he wants to perform in France.

**Please note**

The professional who meets these conditions is exempt from the registration requirements of the trades directory (RM) or the register of companies.

*For further information*: Section 17-1 of Act 96-603 of July 5, 1996.

**In view of a Freedom of establishment**

In order to carry out the maintenance and repair of vehicles in France on a permanent basis, the professional who is an EU or EEA national must meet one of the following conditions:

- have the same professional qualifications as those required for a French national (see above: "2. a. Professional qualifications");
- Hold a certificate of competency or training degree required for the exercise of the activity in an EU or EEA state when that state regulates access or exercise of this activity on its territory;
- have a certificate of competency or a training certificate that certifies its readiness to carry out the activity when this certificate or title has been obtained in an EU or EEA state which does not regulate access or the exercise of this activity ;
- be a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has been active for three years in the state that has admitted equivalence.

**Please note**

A national of an EU or EEA state that meets one of the above conditions may apply for a certificate of recognition of professional qualification (see below: "3. b. Request a certificate of professional qualification for a permanent exercise (LE)")

If the individual does not meet any of the above conditions, the CMA may ask him to perform a compensation measure in the following cases:

- if the duration of the certified training is at least one year less than that required to obtain one of the professional qualifications required in France to carry out the activity;
- If the training received covers subjects substantially different from those covered by one of the qualifications required to carry out the activity in France;
- If the effective and permanent control of the activity requires, for the exercise of some of its remits, specific training which is not provided in the Member State of origin and covers subjects substantially different from those covered by the certificate of competency or training designation referred to by the applicant.

*For further information*: Articles 17 and 17-1 of Act 96-603 of July 5, 1996; Articles 3 to 3-2 of Decree 98-246 of 2 April 1998.

### c. Conditions of honorability

No one may practice the profession of repair and maintenance of vehicles if it is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 of Act 96-603 of July 5, 1996.

### d. Some peculiarities of the regulation of the activity

**Safety and accessibility rules**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERPs) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

It is advisable to refer to the "Establishment receiving the public" sheet for more information.

**Posting requirement**

The professional who carries out the maintenance and repair of the vehicles, must display the prices charged all taxes included (T.T.C.) at the entrance of the establishment and the reception, in a way readable and visible to the customers.

In addition, if the professional is engaged in towing or troubleshooting, he must display the T.T.C. rates of these operations.

*For further information*: order of 27 March 1987 relating to the rules of price advertising for maintenance or repair services, technical control, troubleshooting or towing as well as vehicle garage.

**Billing**

The professional is subject to compliance with the billing obligations and must issue a note to the customer, in case of a benefit of an amount greater than or equal to 25 euros. This note should include the following information:

- Its date of writing;
- The provider's name and address
- The customer's name (if the customer does not object to it);
- The date and place of execution of the service
- the details of each benefit as well as the total amount payable (all taxes included and excluding taxes).

The professional must keep a double of this grade, rank them chronologically and keep them for two years.

*For further information*: ( Order 83-50/A of 3 October 1983 relating to the price advertising of all services.

**Vehicle towing and troubleshooting regulations**

The professional in the case of troubleshooting and towing of a vehicle on motorway or express road must display the rates charged in accordance with the order of August 10, 2017 relating to the rate of repair of light vehicles on highways and express roads.

**Requirement to offer used parts for vehicle repairs**

The professional who performs the maintenance or repair activity of vehicles is required to offer consumers used spare parts for the repair of their vehicle.

However, this obligation does not apply when:

- Maintenance or repair was carried out free of charge or under contractual warranty;
- Spare parts are not available during the vehicle's immobilization period.
- the professional believes that spare parts pose a significant risk to the environment, public health or road safety.

*For further information*: Articles L. 224-67, R. 224-22 and following of the Consumer Code.

**Regulations on the quality of craftsman and the titles of master craftsman and best worker in France**

**Craftsmanship**

To claim the status of craftsman, the person must justify either:

- a CAP, a BEP or a certified or registered title when it was issued to the RNCP at least equivalent (see above: "2. a. Professional qualifications");
- professional experience in this trade for at least three years.

*For further information*: Article 1 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of master craftsman**

This title is given to individuals, including the social leaders of legal entities:

- Registered in the trades directory;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

**Please note**

Individuals who do not hold the master's degree may apply for the title of Master Craftsman at the Regional Qualifications Commission in two hypotheses:

- when they are registered in the trades directory, have a degree at least equivalent to the master's degree, and justify management and psycho-pedagogical knowledge equivalent to those of the corresponding value units of the master's degree and that they have two years of professional practice;
- when they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of best worker in France (MOF)**

The professional diploma "one of the best workers in France" is a state diploma that attests to the acquisition of a high qualification in the exercise of a professional activity in the artisanal, commercial, industrial or agricultural field.

The diploma is classified at level III of the interdepartmental nomenclature of training levels. It is issued after an examination called "one of the best workers in France" under a profession called "class" attached to a group of trades.

For more information, it is recommended that you consult[official website](http://www.meilleursouvriersdefrance.info/) "one of the best workers in France."

*For further information*: Article D. 338-9 of the Education Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)

**Competent authority**

The CMA of the place in which the national wishes to carry out the benefit, is competent to issue the prior declaration of activity.

**Supporting documents**

The request for a pre-report of activity is accompanied by a complete file containing the following supporting documents:

- A photocopy of a valid ID
- a certificate justifying that the national is legally established in an EU or EEA state;
- a document justifying the professional qualification of the national who may be, at your choice:- A copy of a diploma, title or certificate,
  - A certificate of competency,
  - any documentation attesting to the national's professional experience.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Please note**

When the file is incomplete, the CMA has a period of fifteen days to inform the national and request all the missing documents.

**Outcome of the procedure**

Upon receipt of all the documents in the file, the CMA has one month to decide:

- either to authorise the benefit where the national justifies three years of work experience in an EU or EEA state, and to attach to that decision a certificate of professional qualification;
- or to authorize the provision when the national's professional qualifications are deemed sufficient;
- or to impose an aptitude test on him when there are substantial differences between the professional qualifications of the national and those required in France. In the event of a refusal to perform this compensation measure or if it fails to perform, the national will not be able to perform the service in France.

The silence kept by the competent authority within this period is worth authorisation to begin the provision of services.

*For further information*: Article 2 of the decree of 2 April 1998; Article 2 of the decree of 17 October 2017 relating to the submission of the declaration and requests provided for by Decree 98-246 of 2 April 1998 and Title I of Decree 98-247 of 2 April 1998.

### b. Request a certificate of professional qualification for a permanent exercise (LE)

The person concerned wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of recognition of professional qualification.

**Competent authority**

The request must be addressed to the territorially competent CMA.

**Procedure**

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the person concerned to complete it within a fortnight of filing the file. A receipt is issued as soon as the file is complete.

**Supporting documents**

The folder should contain the following parts:

- Applying for a certificate of professional qualification
- A certificate of competency or diploma or vocational training designation;
- Proof of the applicant's nationality
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, the proofs of the exercise of the activity for three years.

The CMA may request further information about its training or professional experience to determine the possible existence of substantial differences with the professional qualification required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (CIEP) to obtain additional information on the level of training of a diploma or certificate or a foreign designation, the applicant will have to pay a fee Additional.

**What to know**

If necessary, all supporting documents must be translated into French .

**Response time**

Within three months of the receipt, the CMA may:

- Recognise professional qualification and issue certification of professional qualification;
- decide to subject the applicant to a compensation measure and notify him of that decision;
- refuse to issue the certificate of professional qualification.

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

**Remedies**

If the CMA refuses to issue the recognition of professional qualification, the applicant may initiate, within two months of notification of the refusal of the CMA, a legal challenge before the relevant administrative court. Similarly, if the person concerned wishes to challenge the CMA's decision to submit it to a compensation measure, he must first initiate a graceful appeal with the prefect of the department in which the CMA is based, within two months of notification of the decision. CMA. If he does not succeed, he may opt for a litigation before the relevant administrative tribunal.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998, order of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 and relating to the procedure for recognising the professional qualifications of a professional a member state of the European Community or another state party to the European Economic Area agreement.

**Good to know: compensation measures**

The CMA, which is applying for a certificate of recognition of professional qualification, notifies the applicant of his decision to have him perform one of the compensation measures. This decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose between an adjustment course of up to three years or an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organised within six months of the receipt by the CMA of the applicant's decision to opt for this test. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the adaptation course, the applicant sends the CMA a certificate certifying that he has validly completed this internship, accompanied by an evaluation of the organization that supervised him. The CMA issues, on the basis of this certificate, a certificate of professional qualification within one month.

The decision to use a compensation measure may be challenged by the person concerned who must file an administrative appeal with the prefect within two months of notification of the decision. If his appeal is dismissed, he can then initiate a legal challenge.

*For further information*: Articles 3 and 3-2 of Decree 98-246 of 2 April 1998, Article 6-1 of Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

### c. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Artisanal Company Reporting Formalities," "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### d. Post-registration authorisation

#### Classified Facilities for Environmental Protection (ICPE)

Where the professional's activity poses risks to the environment or health, health and public safety, the professional's establishment will be subject to the issuance of a registration or reporting procedure according to the nature of its business.

As such, the professional will first have to verify that his activity is within the scope of one of the[Nomenclature](https://aida.ineris.fr/liste_documents/1/18298/1) ICPE is available on the website of the Ministry of Ecological and Solidarity Transition.

The professional's facilities for vehicle maintenance and repair can be used in two different areas:

- 2930 "Vehicle and Motor Vehicle Repair and Maintenance Workshops";
- 2560 "Mechanical Work of Metals and Alloys."

Depending on the nature of the activity, the professional will have to make the declaration, registration or request for prior authorization.

**Facilities under 2930**

For the repair and maintenance activity of vehicles and motor vehicles the professional will have to carry out:

- a request for permission if the area of his workshop is more than 5,000 square metres;
- a declaration with periodic control, if the area of his workshop is greater than 2000 square meters but less or equal to 5000 square meters.

For the activity of varnish, paint or primer, the professional will have to perform:

- a request for permission if the maximum amount of products that can be used is more than 100 kilos per day;
- a periodic check-up if this amount is greater than 10 kilos per day or if the annual amount of solvents contained in the products is greater than O.5 tons and the maximum amount of products exceeds 100 kilos per day.

**Facilities under section 2560**

The procedure depends on the maximum power of the machines:

- If this power is greater than 1,000 kilowatt (kW) the professional will have to register his installations:
- if this power is greater than 150 kW but less than 1,000 kW or less it will have to report with periodic control.

It is advisable to consult the[Aida site - Ineris](https://aida.ineris.fr/liste_documents/1/18016/1) to find out the terms of the different procedures.

*For further information*: Article L. 511-1 of the Environment Code; Order of 4 June 2004 relating to general requirements for classified facilities subject to declaration under heading 2930 relating to repair and maintenance shops for vehicles and motor vehicles, including bodywork and sheet metal.

#### In case of activity related to refrigerants

The professional who uses refrigerant fluids (particularly on equipment such as heat pump or vehicle air conditioning) must obtain a certificate of capacity.

**Competent authority**

The professional must submit his application to an organization approved by the French Accreditation Committee ([Cofrac](https://www.cofrac.fr/)).

**Supporting documents**

The application file must include:

- The identity of the applicant and his address, or if so the name, legal form, SIRET number and address of the head office of the corporation;
- A list of the categories of activities the professional intends to carry out;
- The list of stakeholders involved in these activities;
- The types and quantities of tools used to exercise;
- the professional's commitment to give each year to the operator who issued the certificate, a declaration no later than 31 December of the previous calendar year, specifying the quantities of fluids acquired, loaded, destroyed, divested, regenerated and stored under the conditions set out in the 30 June 2008 order on the issuance of capacity certificates to operators under Article R. 543-99 of the Environment Code.

**Delays and procedures**

The organization issues the certificate of capacity within two months of receiving the application. This certificate of capacity is issued for a period of five years.

*For further information*: Articles R. 543-99 and R. 543-108 and following of the Environment Code.

