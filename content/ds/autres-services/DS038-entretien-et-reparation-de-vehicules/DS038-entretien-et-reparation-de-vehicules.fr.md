﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS038" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Autres services" -->
<!-- var(title)="Entretien et réparation de véhicules" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="autres-services" -->
<!-- var(title-short)="entretien-et-reparation-de-vehicules" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/autres-services/entretien-et-reparation-de-vehicules.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="entretien-et-reparation-de-vehicules" -->
<!-- var(translation)="None" -->

# Entretien et réparation de véhicules

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité d'entretien et de réparation de véhicules consiste pour le professionnel, à effectuer au sein d'un atelier (garage) les contrôles, les vérifications, l'entretien et le nettoyage des véhicules automobiles ainsi que les éventuelles réparations nécessaires.

Le professionnel peut ainsi être amené à effectuer :

- la vidange et la révision du véhicule ;
- l'ensemble des réglages et le remplacement des pièces usagées ou défaillantes (électroniques, mécaniques) ;
- la réparation de la carrosserie et de la tôlerie ;
- la peinture ;
- le dépannage et le remorquage du véhicule.

Il peut également proposer à la vente :

- des carburants et des lubrifiants ;
- des accessoires automobiles ;
- des véhicules neufs ou d'occasion.

**À noter**

L'activité de contrôle technique ne peut être effectuée que par des établissements agréés par l’État et exerçant exclusivement cette activité. Ainsi, un établissement exerçant l'activité d'entretien et réparation des véhicules ne peut exercer celle de contrôle technique.

*Pour aller plus loin* : article R. 323-8 du Code de la route.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

- pour les activités artisanales, le CFE compétent est la chambre des métiers et de l'artisanat (CMA) ;
- pour les activités commerciales, il s'agit de la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité d'entretien et de réparation de véhicules le professionnel doit :

- être qualifié professionnellement ou placé sous le contrôle effectif et permanent d'une personne qualifiée professionnellement ;
- être immatriculé au répertoire des métiers (RM) ou au registre des entreprises en cas d'installation dans les départements du Bas-Rhin, du Haut-Rhin ou de la Moselle (cf. infra. « Demande d'immatriculation au répertoire des métiers »).

#### Formation

Pour être reconnu comme étant qualifié professionnellement, le professionnel qui exerce l'activité d'entretien et de réparation de véhicules ou qui en contrôle l'exercice, doit être titulaire au choix :

- d'un certificat d'aptitude professionnelle (CAP) de maintenance des véhicules automobiles ;
- d'un brevet de maîtrise (BM) de réparateur - gestionnaire en maintenance automobile ;
- d'un diplôme ou titre de niveau égal ou supérieur homologué ou enregistré au Répertoire national des certifications professionnelles [RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

À défaut de l’un de ces diplômes, l’intéressé devra justifier d’une expérience professionnelle de trois années effectives, dans un État de l’Union européenne (UE) ou partie à l’accord sur l’Espace économique européen (EEE), acquise en qualité de dirigeant d’entreprise, de travailleur indépendant ou de salarié dans l’exercice de l'activité d'entretien et réparation de véhicules. Dans ce cas, l’intéressé pourra effectuer une demande d’attestation de reconnaissance de qualification professionnelle auprès de la CMA compétente.

*Pour aller plus loin* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat ; article 1er du décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l'exercice des activités prévues à l'article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat.

**CAP maintenance des véhicules**

Ce diplôme de niveau V (correspondant à une sortie de second cycle général et technologique avant l'année de terminale) est accessible aux candidats après un parcours de formation en tant qu'élève ou étudiant, en contrat d'apprentissage, après un parcours de formation continue, en contrat de professionnalisation, par candidature individuelle ou via la procédure de validation des acquis par l'expérience (VAE). Pour plus d’informations, vous pouvez consulter le [site](http://www.vae.gouv.fr/) officiel de la VAE.

La formation dure en principe deux ans et s’effectue dans un lycée professionnel.

**BM réparateur - gestionnaire en maintenance automobile**

Le brevet de maîtrise est un titre homologué de niveau III (bac +2) délivré par la CMA dont la formation d'une durée de deux ans se fait par la voie de la formation continue, d'un contrat de professionnalisation ou par validation des acquis de l'expérience.

*Pour aller plus loin* : articles D. 337-1 et suivants du Code de l’éducation.

#### Demande d'immatriculation au répertoire des métiers

**Autorité compétente**

Le professionnel doit adresser sa demande d'immatriculation à la CMA de son siège social dans un délai d'un mois avant le début de son activité. Toutefois, la demande peut être adressée au plus tard un mois après le début de l'activité dès lors que le professionnel à notifié au président de la CMA, la date de début de celle-ci, au plus tard la veille par lettre recommandée avec avis de réception.

**Pièces justificatives**

La liste des pièces justificatives à adresser à la CMA dépend de la nature de l'activité exercée.

**Délai**

Dès le dépôt de son dossier complet, la CMA adresse un récépissé de dépôt de dossier de création d'entreprise au demandeur. L'absence de réponse dans un délai de deux mois vaut immatriculation.

*Pour aller plus loin* : articles 9 et suivants du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement)

#### En vue d'une Libre Prestation de Services (LPS)

Le professionnel ressortissant de l’UE ou de l’EEE peut exercer en France, à titre temporaire et occasionnel, l'activité d'entretien et réparation de véhicules à condition d’être légalement établi dans un de ces États pour y exercer la même activité.

Il devra au préalable en faire la demande par déclaration écrite adressée à la CMA compétente (cf. infra « 3°. a. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS) »).

Si ni l’activité, ni la formation y conduisant ne sont réglementées dans l’État dans lequel le professionnel est légalement établi, l’intéressé doit en outre prouver qu’il a exercé l’activité dans cet État pendant au moins un an à temps plein ou à temps partiel au cours des dix dernières années précédant la prestation qu’il veut effectuer en France.

**À noter**

Le professionnel qui répond à ces conditions est dispensé des exigences relatives à l’immatriculation au répertoire des métiers (RM) ou au registre des entreprises.

*Pour aller plus loin* : article 17-1 de la loi n° 96-603 du 5 juillet 1996.

#### En vue d'un Libre Établissement (LE)

Pour exercer en France, à titre permanent, l'activité d'entretien et de réparation de véhicules, le professionnel ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un ressortissant français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- être titulaire d’une attestation de compétence ou d’un titre de formation requis pour l’exercice de l’activité dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’une attestation de compétences ou d’un titre de formation qui certifie sa préparation à l’exercice de l’activité lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès, ni l’exercice de cette activité ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité dans l’État qui a admis l’équivalence.

**À noter**

Le ressortissant d’un État de l’UE ou de l’EEE qui remplit l’une des conditions précitées peut solliciter une attestation de reconnaissance de qualification professionnelle (cf. infra « 3°. b. Demander une attestation de qualification professionnelle en vue d'un exercice permanent (LE) ».)

Si l’intéressé ne remplit aucune des conditions précitées, la CMA saisie peut lui demander d’accomplir une mesure de compensation dans les cas suivants :

- si la durée de la formation attestée est inférieure d’au moins un an à celle exigée pour obtenir l’une des qualifications professionnelles requises en France pour exercer l’activité ;
- si la formation reçue porte sur des matières substantiellement différentes de celles couvertes par l’un des titres ou diplômes requis pour exercer en France l’activité ;
- si le contrôle effectif et permanent de l’activité nécessite, pour l’exercice de certaines de ses attributions, une formation spécifique qui n’est pas prévue dans l’État membre d’origine et porte sur des matières substantiellement différentes de celles couvertes par l’attestation de compétences ou le titre de formation dont le demandeur fait état.

*Pour aller plus loin* : articles 17 et 17-1 de la loi n° 96-603 du 5 juillet 1996 ; articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998.

### c. Conditions d’honorabilité

Nul ne peut exercer la profession de réparation et entretien de véhicules s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévue au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 de la loi n° 96-603 du 5 juillet 1996.

### d. Quelques particularités de la réglementation de l’activité

#### Règles de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des Établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

#### Obligation d'affichage

Le professionnel qui exerce l'activité d'entretien et réparation des véhicules, doit procéder à l'affichage des prix pratiqués toutes taxes comprises (T.T.C.) à l'entrée de l'établissement et à l'accueil, de manière lisible et visible pour la clientèle.

En outre, si le professionnel exerce l'activité de remorquage ou de dépannage, il doit afficher les tarifs T.T.C. de ces opérations.

*Pour aller plus loin* : arrêté du 27 mars 1987 relatif aux règles de publicité des prix pour les prestations d'entretien ou de réparation, de contrôle technique, de dépannage ou de remorquage ainsi que de garage des véhicules.

#### Facturation

Le professionnel est soumis au respect des obligations en matière de facturation et doit délivrer une note au client, en cas de prestation d'un montant supérieur ou égal à 25 euros. Cette note doit comporter les informations suivantes :

- sa date de rédaction ;
- le nom et l'adresse du prestataire ;
- le nom du client (si celui-ci ne s'y oppose pas) ;
- la date et le lieu d'exécution de la prestation ;
- le détail de chaque prestation ainsi que le montant total à payer (toutes taxes comprises et hors taxes).

Le professionnel doit conserver un double de cette note, les classer chronologiquement et les conserver pendant deux ans.

*Pour aller plus loin* : arrêté n° 83-50/A du 3 octobre 1983 relatif à la publicité des prix de tous les services.

#### Réglementation en matière de remorquage et dépannage des véhicules

Le professionnel en cas de dépannage et de remorquage d'un véhicules sur autoroute ou route express doit afficher les tarifs pratiqués conformément à l'arrêté du 10 août 2017 relatif au tarif de dépannage des véhicules légers sur autoroutes et routes express.

#### Obligation de proposer des pièces d'occasions pour la réparation de véhicules

Le professionnel qui exerce l'activité d'entretien ou de réparation de véhicules à l'obligation de proposer aux consommateurs des pièces de rechange d'occasion pour la réparation de leur véhicule.

Cette obligation ne s'applique toutefois pas lorsque :

- l'entretien ou la réparation a été réalisée à titre gratuit ou sous garantie contractuelle ;
- les pièces de rechange ne sont pas disponibles durant le délai d'immobilisation du véhicule ;
- le professionnel estime que les pièces de rechange présentent un risque important pour l'environnement, la santé publique ou la sécurité routière.

*Pour aller plus loin* : articles L. 224-67, R. 224-22 et suivants du Code de la consommation.

#### Réglementation relative à la qualité d’artisan et aux titres de maître artisan et de meilleur ouvrier de France

##### La qualité d’artisan

Pour se prévaloir de la qualité d’artisan, la personne doit justifier soit :

- d’un CAP, d’un BEP ou d’un titre homologué ou enregistré lors de sa délivrance au RNCP d’un niveau au moins équivalent (cf. supra « 2°. a. Qualifications professionnelles ») ;
- d’une expérience professionnelle dans ce métier de trois ans au moins.

*Pour aller plus loin* : article 1 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

##### Le titre de maître artisan

Ce titre est attribué aux personnes physiques, y compris les dirigeants sociaux des personnes morales :

- immatriculées au répertoire des métiers ;
- titulaires du brevet de maîtrise dans le métier exercé ;
- justifiant d’au moins deux ans de pratique professionnelle.

**À noter**

Ldes personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan à la commission régionale des qualifications dans deux hypothèses :

- lorsqu’elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de niveau de formation au moins équivalent au brevet de maîtrise, qu’elles justifient de connaissances en gestion et en psychopédagogie équivalentes à celles des unités de valeur correspondantes du brevet de maîtrise et qu’elles ont deux ans de pratique professionnelle ;
- lorsqu’elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles disposent d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

*Pour aller plus loin* : article 3 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

##### Le titre de meilleur ouvrier de France (MOF)

Le diplôme professionnel « un des meilleurs ouvriers de France » est un diplôme d’État qui atteste l’acquisition d’une haute qualification dans l’exercice d’une activité professionnelle dans le domaine artisanal, commercial, industriel ou agricole.

Le diplôme est classé au niveau III de la nomenclature interministérielle des niveaux de formation. Il est délivré à l’issue d’un examen dénommé « concours un des meilleurs ouvriers de France » au titre d’une profession dénommée « classe », rattachée à un groupe de métiers.

Pour plus d’informations, il est recommandé de consulter le [site officiel](http://www.meilleursouvriersdefrance.info/) du concours « un des meilleurs ouvriers de France ».

*Pour aller plus loin* : article D. 338-9 du Code de l’éducation.

## 3°. Démarches et formalités d’installation

### a. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

La CMA du lieu dans lequel le ressortissant souhaite réaliser la prestation, est compétente pour délivrer la déclaration préalable d'activité.

#### Pièces justificatives

La demande de déclaration préalable d'activité est accompagnée d'un dossier complet comprenant les pièces justificatives suivantes :

- une photocopie d'une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE ;
- un document justifiant la qualification professionnelle du ressortissant qui peut être, au choix :
  - une copie d'un diplôme, titre ou certificat,
  - une attestation de compétences,
  - tout document attestant de l'expérience professionnelle du ressortissant.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

**À noter**

Lorsque le dossier est incomplet, la CMA dispose d'un délai de quinze jours pour en informer le ressortissant et demander l'ensemble des pièces manquantes.

#### Issue de la procédure

À réception de l'ensemble des pièces du dossier, la CMA dispose d'un délai d'un mois pour décider :

- soit d'autoriser la prestation lorsque le ressortissant justifie d'une expérience professionnelle de trois ans dans un État de l'UE ou de l'EEE, et de joindre à cette décision une attestation de qualification professionnelle ;
- soit d'autoriser la prestation lorsque les qualifications professionnelles du ressortissant sont jugées suffisantes ;
- soit de lui imposer une épreuve d'aptitude lorsqu'il existe des différences substantielles entre les qualifications professionnelles du ressortissant et celles exigées en France. En cas de refus d'accomplir cette mesure de compensation ou en cas d'échec dans son exécution, le ressortissant ne pourra pas effectuer la prestation de service en France.

Le silence gardé de l'autorité compétente dans ce délai vaut autorisation de débuter la prestation de services.

*Pour aller plus loin* : article 2 du décret du 2 avril 1998 ; article 2 de l'arrêté du 17 octobre 2017 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre Ier du décret n° 98-247 du 2 avril 1998.

### b. Demander une attestation de qualification professionnelle en vue d'un exercice permanent (LE)

L’intéressé souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de reconnaissance de qualification professionnelle.

#### Autorité compétente

La demande doit être adressée à la CMA territorialement compétente.

#### Procédure

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les quinze jours du dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

#### Pièces justificatives

Le dossier doit contenir les pièces suivantes :

- la demande d’attestation de qualification professionnelle ;
- une attestation de compétences ou le diplôme ou titre de formation professionnelle ;
- la preuve de la nationalité du demandeur ;
- si l’expérience professionnelle a été acquise sur le territoire d’un État de l’UE ou de l’EEE, une attestation portant sur la nature et de la durée de l’activité délivrée par l’autorité compétente dans l’État membre d’origine ;
- si l’expérience professionnelle a été acquise en France, les justificatifs de l’exercice de l’activité pendant trois années.

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du centre international d’études pédagogiques (CIEP) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme ou d’un certificat ou d’un titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français (traduction agrée).

#### Délai de réponse

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut :

- reconnaître la qualification professionnelle et délivrer l’attestation de qualification professionnelle ;
- décider de soumettre le demandeur à une mesure de compensation et lui notifier cette décision ;
- refuser de délivrer l’attestation de qualification professionnelle.

En l’absence de décision dans le délai de quatre mois, la demande d’attestation de qualification professionnelle est réputée acquise.

#### Voies de recours

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier, dans les deux mois suivant la notification du refus de la CMA, un recours contentieux devant le tribunal administratif compétent. De même, si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours gracieux auprès du préfet du département dans lequel la CMA a son siège, dans les deux mois suivant la notification de la décision de la CMA. S’il n’obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998, arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.

#### Bon à savoir :mesures de compensation

La CMA saisie d’une demande de délivrance d’une attestation de reconnaissance de qualification professionnelle, notifie au demandeur sa décision tendant à lui faire accomplir l'une des mesures de compensation. Cette décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est impérative pour exercer en France.

Le demandeur doit alors choisir entre un stage d’adaptation d’une durée maximale de trois ans ou une épreuve d’aptitude.

L’épreuve d’aptitude prend la forme d’un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA, de la décision du demandeur d’opter pour cette épreuve. À défaut, la qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

À l'issue du stage d’adaptation, le demandeur adresse à la CMA une attestation certifiant qu’il a valablement accompli ce stage, accompagnée d’une évaluation de l’organisme qui l’a encadré. La CMA délivre, sur la base de cette attestation, une attestation de qualification professionnelle dans un délai d’un mois.

La décision de recourir à une mesure de compensation peut être contestée par l’intéressé qui doit former un recours administratif auprès du préfet dans un délai de deux mois à compter de la notification de la décision. En cas de rejet de son recours, il peut alors initier un recours contentieux.

*Pour aller plus loin* : articles 3 et 3-2 du décret n° 98-246 du 2 avril 1998, article 6-1 du décret n° 83-517 du 24 juin 1983 fixant les conditions d’application de la loi 82-1091 du 23 décembre 1982 relative à la formation professionnelle des artisans.

### c. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### d. Autorisation(s) post-immatriculation

#### Installations classées pour la protection de l'environnement (ICPE)

Lorsque l'activité du professionnel présente des risques pour l'environnement ou la santé, la salubrité et la sécurité publique, son établissement sera soumis à la délivrance d'une autorisation ou à une procédure d'enregistrement ou de déclaration selon la nature de son activité.

À ce titre, le professionnel devra au préalable vérifier que son activité entre dans le champ de l'une des rubriques de la [nomenclature](https://aida.ineris.fr/liste_documents/1/18298/1) ICPE disponible sur le site du Ministère de la transition écologique et solidaire.

Les installations du professionnel exerçant l'activité d'entretien et de réparation de véhicules peut ainsi relever de deux rubriques différentes :

- la rubrique 2930 « Ateliers de réparation et d'entretien de véhicules et engins à moteur » ;
- la rubrique 2560 « Travail mécanique des métaux et alliages ».

Ainsi selon la nature de son activité le professionnel devra effectuer la déclaration, l'enregistrement ou la demande d'autorisation préalable.

#### Installations relevant de la rubrique 2930

Pour l'activité de réparation et entretien de véhicules et engins à moteur le professionnel devra effectuer :

- une demande d'autorisation si la surface de son atelier est supérieure à 5 000 mètres carrés ;
- une déclaration avec contrôle périodique, si la surface de son atelier est supérieure à 2000 mètres carrés mais inférieure ou égale à 5000 mètres carrés.

Pour l'activité de vernis, peinture ou apprêt, le professionnel devra effectuer :

- une demande d'autorisation si la quantité maximale de produits susceptible d'être utilisée est supérieure à 100 kilos par jour ;
- une déclaration avec contrôle périodique si cette quantité est supérieure à 10 kilos par jour ou si la quantité annuelle de solvants contenus dans les produits est supérieure à O,5 tonne et que la quantité maximale de produits dépasse 100 kilos par jour.

#### Installations relevant de la rubrique 2560

La procédure dépend de la puissance maximale des machines :

- si cette puissance est supérieure à 1 000 kilowatt (kW) le professionnel devra procéder à un enregistrement de ses installations :
- si cette puissance est supérieure à 150 kW mais inférieure ou égale à 1 000 kW il devra effectuer une déclaration avec contrôle périodique.

Il est conseillé de consulter le [site de l'Aida - Ineris](https://aida.ineris.fr/liste_documents/1/18016/1) pour connaître les modalités des différentes procédures.

*Pour aller plus loin* : article L. 511-1 du Code de l'environnement ; arrêté du 4 juin 2004 relatif aux prescriptions générales applicables aux installations classées soumises à déclaration sous la rubrique 2930 relative aux ateliers de réparation et d'entretien de véhicules et engins à moteur, y compris les activités de carrosserie et de tôlerie.

#### En cas d'activité liée aux fluides frigorigènes

Le professionnel qui utilise des fluides réfrigérants (notamment sur des équipements de type pompe à chaleur ou climatisation des véhicules) doit obtenir une attestation de capacité.

##### Autorité compétente

Le professionnel doit remettre sa demande à un organisme agréé par le Comité français d'accréditation ([Cofrac](https://www.cofrac.fr/)).

##### Pièces justificatives

Le dossier de demande doit comporter les éléments suivants :

- l'identité du demandeur et son adresse, ou le cas échéant la raison sociale, la forme juridique, le numéro SIRET et l'adresse du siège social de la personne morale ;
- la liste des catégories d'activités que le professionnel compte exercer ;
- la liste des intervenants amenés à exercer ces activités ;
- les types et quantités d'outillages utilisés pour exercer ;
- l'engagement du professionnel de remettre chaque année à l'opérateur qui lui a délivré l'attestation, une déclaration au plus tard le 31 décembre de l'année civile précédente, précisant les quantités des fluides acquises, chargées, détruites, cédées, régénérées et stockées dans les conditions fixées par l'arrêté du 30 juin 2008 relatif à la délivrance des attestations de capacité aux opérateurs prévues à l'article R. 543-99 du Code de l'environnement.

##### Délais et procédure

L'organisme délivre l'attestation de capacité dans un délai de deux mois suivants la réception de la demande. Cette attestation de capacité est délivrée pour une durée de cinq ans.

*Pour aller plus loin* : articles R. 543-99 et R. 543-108 et suivants du Code de l'environnement.