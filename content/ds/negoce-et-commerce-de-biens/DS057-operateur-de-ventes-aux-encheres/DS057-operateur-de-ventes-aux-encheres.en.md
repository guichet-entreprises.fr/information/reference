﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS057" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Auctioneer" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="auctioneer" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/auctioneer.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="auctioneer" -->
<!-- var(translation)="Auto" -->


Auctioneer
================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The voluntary sales operator is a professional whose activity consists of organizing and carrying out the sale of new or used goods during a public auction, and ensuring that the transaction with the bidder runs smoothly.

**Please note**

The professional natural person performing this activity during an auction takes the title of auctioneer of voluntary sales excluding any other type of sale.

*For further information*: Articles L. 321-1 to L. 321-3 of the Code of Commerce.

### b. Competent Business Formality Centre

The relevant Business Formalities Centre (CFE) depends on the nature of the structure in which the activity is carried out:

- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court or the registry of the district court in the departments of Lower Rhine, Upper Rhine and Moselle;
- in the case of the creation of an individual company, the competent CFE is the Urssaf.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

**In the case of a natural person operator**

To practice, the voluntary sales operator must:

- Be French or a national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement;
- have not been the author of facts that have resulted in a definitive criminal conviction for acts contrary to honour, probity or good morals or similar acts that have given rise to a disciplinary or administrative sanction of impeachment, delisting, revocation, withdrawal of accreditation or authorization in the profession he had previously practiced;
- have the necessary qualifications to lead a sale, i.e. have passed the examination of access to the internship and have completed a two-year internship, including at least one year in France, which includes theoretical and practical training;
- having made a prior declaration of activity with the[Council for Voluntary Furniture Auctions at Public Auctions](https://www.conseildesventes.fr/) (Voluntary Sales Council) (see infra "3. a. Pre-declaration of activity").

**In the case of a corporate operator**

If so, it must:

- Be legally incorporated and have its head office, administration or principal establishment in France or within a Member State;
- Have at least one establishment or branch in France
- Have at least one of the executives professionally qualified to carry out the activity of voluntary sales operator;
- provide proof that its leaders have not been definitively convicted of acts contrary to honour, probity or morals, or any disciplinary sanction;
- make a prior declaration of activity under the same conditions as the natural person operator (see infra "3 degrees). a. Declaration of activity").

*For further information*: Articles L. 321-4 and R. 321-18 of the Code of Commerce.

**Please note**

Voluntary sales may be made by notaries and judicial officers in municipalities where there is no judicial auctioneer's office, subject to training (see Article R. 321-18-1 of the Code of Commerce). They then carry out their activity as an ancillary (see Article L. 321-2 of the Code of Commerce).

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

**For temporary or casual exercise (LPS)**

Any national of an EU or EEA Member State who is legally established and who acts as a voluntary sales commissioner in that state may carry out the same activity in France on a temporary and occasional basis.

To do so, it must make a prior declaration to the[Voluntary Sales Council](https://www.conseildesventes.fr/) (see infra "5. b. Pre-declaration for EU or EEA nationals for temporary and casual exercise").

Where neither activity nor training is regulated in the Member State in which the professional is legally established, he must have carried out this activity in one or more EU Member States for at least one year in the ten years preceding the Benefit.

*For further information*: Articles L. 321-4 and the following of the Code of Commerce.

**For a permanent exercise (LE)**

Any EU national legally established in a Member State and operating as a voluntary sales operator may carry out this activity on a permanent basis. For this it must:

- have the same professional qualifications as those required for a Frenchman (see above "2 degrees). a. Professional qualifications");
- hold a certificate of competency or training certificate required for the exercise of the activity of auctioneer in an EU or EEA state when that state regulates access or the exercise of this activity on its territory;
- have a training certificate that attests to its preparation for the exercise of the activity of auctioneer when this certificate or title has been obtained in an EU or EEA state which does not regulate the access or exercise of this activity;
- have a certificate of competency or a training document that certifys the preparation for the exercise of the activity of a voluntary sales auctioneer and further justify, in a member state or a state party to the agreement on the Space which does not regulate access to or exercise of this occupation, a full-time exercise of the profession for at least one year in the previous ten years or for an equivalent period in the case of part-time exercise, under the that this exercise should be certified by the competent authority of the state.

Where the training received by the professional covers subjects substantially different from those required for the practice of the profession in France, the[Voluntary Sales Council](https://www.conseildesventes.fr/) may require him to submit to a compensation measure (see infra "3°. c. Good to know: compensation measures").

As soon as the national meets these conditions, he must apply to the Voluntary Sales Council for recognition of his qualifications (see infra "3°. c. If necessary, apply for recognition for the national for a permanent exercise (LE) ").

*For further information*: Articles R. 321-65 and the following of the Code of Commerce.

### c. Conditions of honourability and criminal penalties

**Professional rules**

The voluntary sales operator is bound by the following professional rules:

- Probity
- good manners;
- Honor.

Any breach or action contrary to these rules may be the subject of a conviction or a ban on practising.

*For further information*: Article L. 321-4 of the Code of Commerce.

**Incompatibilities**

The professional who auctions property is deemed to act as an agent of the owner of these propertys. Also, he cannot make the purchase on his own account or the sale of the owner's property.

*For further information*: Article L. 321-5 of the Code of Commerce.

**Criminal sanctions**

The professional faces a two-year prison sentence and a fine of 375,000 euros if he proceeds or has one or more voluntary sales of furniture at public auctions:

- without having made the prior declaration of activity (see infra "3.3. a. Pre-declaration of activity");
- without applying for qualification when the professional is an EU national (see below "3 degrees). c. If necessary, apply for recognition for the national for a permanent exercise (LE);
- even though it does not have the necessary qualifications to organize or carry out voluntary sales at public auctions or is prohibited from holding such a sale.

*For further information*: Article L. 321-15 of the Code of Commerce.

### d. Insurance

The professional who acts as a voluntary sales operator must:

- Open an account in a credit institution to receive funds held on behalf of others during its activity;
- Take out professional liability insurance
- purchase insurance or a bond to guarantee the funds received during its activity. These guarantees can only be granted by a credit institution or an authorized financing company, or an insurance company or a mutual surety company.

**Please note**

EU or EEA nationals are subject to the same requirements.

*For further information*: Articles L. 321-6, R. 321-10 to R. 321-17 and R. 321-56 of the Code of Commerce.

### e. Some peculiarities of the regulation of the activity

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP). It is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

**Advertising measure**

Within eight days of each voluntary sale of furniture at public auction, the professional must advertise with the Voluntary Sales Council.

**Supporting documents**

The professional must send him a document mentioning all the information relating to the organization of the sale, his technical and financial means as well as the following information:

- the date, location, as well as the identity of the professional who will proceed with the sale and the date of his declaration to the[Voluntary Sales Council](https://www.conseildesventes.fr/) ;
- The seller's quality as a trader or craftsman when the seller sells new products;
- The new character of the property;
- If applicable, the ownership of the property put up for sale when the owner is an organizing voluntary sales operator or his employee, manager or partner;
- The intervention of an expert in the organization of the sale;
- mention of the statute of limitations for civil liability actions incurred during a voluntary sale, i.e. a five-year period from the auction or prize.

**Please note**

The professional must keep an electronic sales register and a directory containing the minutes of the sales made every day.

*For further information*: Articles L. 321-7 and R. 321-32 to R. 321-35 of the Code of Commerce.

**Obligation to keep a record for precious metals**

If during the course of the exercise of the professional is in possession of gold, silver, platinum or alloys of these metals, he must keep a register called "police book" containing all the information relating to the sale and the property sold.

*For further information*: Sections 537 and 538 of the General Tax Code.

**Anti-money laundering and anti-terrorism obligations**

The auction operator must, during its activity, implement all the obligations relating to the fight against money laundering and terrorism.

*For further information*: Article L. 561-2 of the Monetary and Financial Code.

**Daily record of sales**

The professional must keep a daily record of the characteristics, provenance and method of settlement of the property sold.

In the event of a breach of these obligations or in the case of inaccurate mentions on the register, the professional faces a sentence of six months' imprisonment and a fine of 30,000 euros.

*For further information*: Article L. 321-10 of the Code of Commerce; 321-7 and 321-8 of the Penal Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Pre-declaration of activity

The professional must make a statement by recommended letter with notice of receipt or by any other means to the[Council for Voluntary Furniture Auctions at Public Auctions](https://www.conseildesventes.fr/).

Its statement should include:

- Proof of identity
- a certificate certifying that he is not the subject of any criminal conviction, disciplinary sanction or ban on practising;
- A document justifying that the persons responsible for directing the sales are professionally qualified;
- A copy of the lease or title to the premises where the activity takes place, as well as the latest balance sheet or forecast balance sheet;
- A document justifying the opening of an account in a credit institution to receive funds held on behalf of others;
- A document justifying that the professional has taken out professional liability insurance;
- a document justifying that the professional has taken out insurance or a bond guaranteeing the representation of funds held on behalf of others.

*For further information*: Articles R. 321-1 to R. 321-4, and Section R. 321-56 of the Code of Commerce.

### b. Statement of activity for EU national for temporary and casual exercise (LPS)

The EU national must make a declaration of activity under the same conditions as the French national (see above "3o. a. Pre-declaration of activity").

However, his application must be accompanied by the following additional documents, if any, with their translation:

- a proof of identity and nationality, or, in the case of a legal person, a copy of its statutes and a document justifying its registration in a public register;
- A document justifying that the professional is legally established in a Member State;
- proof that the professional has been an auction operator for at least one year in the last ten years in a Member State when that State does not regulate access to the activity or its exercise;
- a declaration issued by the Member State less than three months old or, failing that, a certificate on the honour that it does not incur any even temporary ban on carrying out this activity;
- A document mentioning the date, location, identity and qualification of the person in charge of the proposed sale;
- evidence less than three months ago that the professional has professional liability insurance and insurance or bond guaranteeing the representation of funds held on behalf of others.

*For further information*: Articles R. 321-56 to R. 321-61 of the Code of Commerce.

### c. If necessary, apply for recognition for the national for a permanent exercise (LE)

**Competent authority**

The national must apply for qualification by letter with notice of receipt to the[Voluntary Sales Council](https://www.conseildesventes.fr/).

**Supporting documents**

His application must be accompanied by the following documents, if any translated into French:

- proof of the applicant's identity, nationality and residence;
- A copy of the applicant's certificate of competency or training designation giving him access to the activity of a voluntary sales auctioneer;
- If the national holds a diploma or training document issued by a third country and recognised by an EU member country or the EEA, a certificate issued by the competent authority of that Member State certifying the duration of the professional exercise of the applicant and exercise dates;
- a proof of the exercise of the activity of voluntary auctioneer in the last ten years since neither training nor activity is regulated in the Member State;
- proof of his professional qualification
- a document justifying that he is not the subject of any criminal conviction, disciplinary sanction or prohibition of the exercise of the activity of a volunteer sales commissioner.

**Time and remedies**

The Commission acknowledges receipt of the application within one month and informs the applicant if there is a missing piece. The Commission's decision is sent by recommended letter to the applicant within three months of receipt of the full file. The Council's decision is subject to appeal by letter recommended with notice of receipt to the registry of the Court of Appeal of Paris.

*For further information*: Article A. 321-27 of the Code of Commerce.

**Good to know: compensation measures**

The Voluntary Sales Council may require the professional to submit to a selection test before the professional jury in charge of the examination of access to the training course or an adjustment course of up to three years.

*For further information*: Article R. 321-67 of the Code of Commerce.

### b. Company reporting formalities

Depending on the nature of his activity, the professional must register with the Register of Trade and Companies (RSC) or register with the Urssaf.

It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

