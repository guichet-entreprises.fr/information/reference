﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS057" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Opérateur de ventes aux enchères" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="operateur-de-ventes-aux-encheres" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/operateur-de-ventes-aux-encheres.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="operateur-de-ventes-aux-encheres" -->
<!-- var(translation)="None" -->

# Opérateur de ventes aux enchères

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'opérateur de ventes volontaires est un professionnel dont l'activité consiste à organiser et réaliser la vente de biens neufs ou d'occasion au cours d'une vente publique aux enchères, et à s'assurer du bon déroulement de la transaction avec l'enchérisseur.

**À noter**

Le professionnel personne physique exerçant cette activité au cours d'une vente aux enchères prend le titre de commissaire-priseur de ventes volontaires à l'exclusion de tout autre type de vente.

*Pour aller plus loin* : articles L. 321-1 à L. 321-3 du Code de commerce.

### b. Centre de formalités des entreprises compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les sociétés commerciales, il s’agit de la chambre de commerce et d'industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ou le greffe du tribunal d’instance dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle ;
- en cas de création d'une entreprise individuelle, le CFE compétent est l'Urssaf.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

#### En cas d'opérateur personne physique

Pour exercer, l'opérateur de ventes volontaires doit :

- être français ou ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) ;
- ne pas avoir été l'auteur de faits ayant donné lieu à une condamnation pénale définitive pour des agissements contraires à l'honneur, à la probité ou aux bonnes mœurs ou de faits de même nature ayant donné lieu à une sanction disciplinaire ou administrative de destitution, radiation, révocation, de retrait d'agrément ou d'autorisation dans la profession qu'il exerçait antérieurement ;
- avoir les qualifications requises pour diriger une vente, c'est-à-dire avoir passé avec succès l'examen d'accès au stage et avoir accompli un stage de deux ans, dont un an au moins en France, qui comprend une formation théorique et pratique ;
- avoir effectué une déclaration préalable d'activité auprès du [Conseil des ventes volontaires de meubles aux enchères publiques](https://www.conseildesventes.fr/) (Conseil des ventes volontaires) (cf. infra « 3°. a. Déclaration préalable d'activité »).

#### En cas d'opérateur personne morale

Le cas échéant, elle doit :

- être légalement constituée et avoir son siège social, son administration ou son principal établissement en France ou au sein d'un État membre ;
- avoir au moins un établissement ou succursale en France ;
- avoir au moins l'un des dirigeants qualifié professionnellement pour exercer l'activité d'opérateur de ventes volontaires ;
- fournir la preuve que ses dirigeants n'ont fait l'objet d'aucune condamnation définitive pour des faits contraires à l'honneur, à la probité ou aux bonnes mœurs, ni d'aucune sanction disciplinaire ;
- effectuer une déclaration préalable d'activité dans les mêmes conditions que l'opérateur personne physique (cf. infra « 3°. a. Déclaration d'activité »).

*Pour aller plus loin* : articles L. 321-4 et R. 321-18 du Code de commerce.

**À noter**

Peuvent réaliser les ventes volontaires les notaires et huissiers de justice dans les communes où il n'y a pas d'office de commissaire priseur judiciaire, sous réserve d'avoir suivi une formation (cf. article R. 321-18-1 du Code de commerce). Ils exercent alors leur activité à titre accessoire (cf. article L. 321-2 du Code de commerce).

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de Services (LPS) ou Libre Etablissement (LE))

#### En vue d'un exercice temporaire ou occasionnel (LPS)

Tout ressortissant d'un État membre de l'UE ou de l'EEE qui est légalement établi et qui exerce l'activité de commissaire-priseur de ventes volontaires dans cet État peut exercer en France, de manière temporaire et occasionnelle, la même activité.

Il doit pour cela effectuer une déclaration préalable auprès du [Conseil des ventes volontaires](https://www.conseildesventes.fr/) (cf. infra « 5°. b. Déclaration préalable pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel »).

Lorsque ni l'activité ni la formation ne sont réglementées dans l'État membre dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité dans un ou plusieurs États membres de l'UE pendant au moins un an au cours des dix années précédant la prestation.

*Pour aller plus loin* : articles L. 321-4 et suivants du Code de commerce.

#### En vue d'un exercice permanent (LE)

Tout ressortissant de l'UE légalement établi dans un État membre et exerçant l'activité d'opérateur de ventes volontaires peut exercer cette activité à titre permanent. Pour cela il doit :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- être titulaire d’une attestation de compétences ou d’un titre de formation requis pour l’exercice de l’activité de commissaire-priseur dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’un titre de formation qui atteste sa préparation à l’exercice de l’activité de commissaire-priseur lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de cette activité ;
- disposer d'une attestation de compétence ou d'un titre de formation qui certifient la préparation à l'exercice de l'activité de commissaire-priseur de ventes volontaires et justifier en outre, dans un État member ou un État partie à l'accord sur l'Espace économique européen qui ne réglemente pas l'accès à cette profession ou son exercice, d'un exercice à plein temps de la profession pendant une année au moins au cours des dix années précédentes ou pendant une période équivalente en cas d'exercice à temps partiel, sous réserve que cet exercice soit attesté par l'autorité compétente de l'État.

Lorsque la formation reçue par le professionnel porte sur des matières substantiellement différentes de celles requises pour l'exercice de la profession en France, le [Conseil des ventes volontaires](https://www.conseildesventes.fr/) peut exiger qu'il se soumette à une mesure de compensation (cf. infra « 3°. c. Bon à savoir : mesures de compensation »).

Dès lors qu'il remplit ces conditions, le ressortissant doit adresser au Conseil des ventes volontaires une demande de reconnaissance de ses qualifications (cf. infra « 3°. c. Le cas échéant, effectuer une demande de reconnaissance pour le ressortissant en vue d'un exercice permanent (LE) »).

*Pour aller plus loin* : articles R. 321-65 et suivants du Code de commerce.

### c. Conditions d’honorabilité et sanctions pénales

#### Règles professionnelles

L'opérateur de ventes volontaires est tenu au respect des règles professionnelles suivantes :

- la probité ;
- les bonnes mœurs ;
- l'honneur.

Tout manquement ou agissement contraire à ces règles pourra faire l'objet d'une condamnation ou d'une interdiction d'exercice.

*Pour aller plus loin* : article L. 321-4 du Code de commerce.

#### Incompatibilités

Le professionnel qui procède à la vente aux enchères de biens est réputé agir comme un mandataire du propriétaire de ces biens. Aussi, il ne peut procéder à l'achat pour son propre compte ou à la vente des biens du propriétaire.

*Pour aller plus loin* : article L. 321-5 du Code de commerce.

#### Sanctions pénales

Le professionnel encourt une peine de deux ans d'emprisonnement et de 375 000 euros d'amende dès lors qu'il procède ou fait procéder à une ou plusieurs ventes volontaires de meubles aux enchères publiques :

- sans avoir effectué la déclaration préalable d'activité (cf. infra « 3°. a. Déclaration préalable d'activité ») ;
- sans avoir effectué une demande de reconnaissance de qualification lorsque le professionnel est un ressortissant de l'UE (cf. infra « 3°. c. Le cas échéant, effectuer une demande de reconnaissance pour le ressortissant en vue d'un exercice permanent (LE) ») ;
- alors même qu'il ne dispose pas des qualifications requises pour organiser ou réaliser des ventes volontaires aux enchères publiques ou fait l'objet d'une interdiction d'exercer une telle vente.

*Pour aller plus loin* : article L. 321-15 du Code de commerce.

### d. Assurances

Le professionnel exerçant l'activité d'opérateur de ventes volontaires doit :

- ouvrir un compte dans un établissement de crédit en vue de recevoir les fonds détenus pour le compte d'autrui au cours de son activité ;
- souscrire une assurance de responsabilité professionnelle ;
- souscrire une assurance ou un cautionnement garantissant les fonds reçus au cours de son activité. Ces garanties ne peuvent être consenties que par un établissement de crédit ou une société de financement habilitée, ou une société d'assurance ou une société de caution mutuelle.

**À noter**

Les ressortissants de l'UE ou de l'EEE sont soumis aux mêmes exigences.

*Pour aller plus loin* : articles L. 321-6, R. 321-10 à R. 321-17 et R. 321-56 du Code de commerce.

### e. Quelques particularités de la réglementation de l’activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP). Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) » pour de plus amples informations.

#### Mesure de publicité

Dans un délai de huit jours avant chaque vente volontaire de meubles aux enchères publiques, le professionnel doit procéder à des mesures de publicité auprès du Conseil des ventes volontaires.

#### Pièces justificatives

Le professionnel doit lui adresser un document mentionnant toutes les informations relatives à l'organisation de la vente, ses moyens techniques et financiers ainsi que les informations suivantes :

- la date, le lieu, ainsi que l'identité du professionnel qui procédera à la vente ainsi que la date de sa déclaration auprès du [Conseil des ventes volontaires](https://www.conseildesventes.fr/) ;
- la qualité de commerçant ou artisan du vendeur lorsque celui-ci met en vente des produits neufs ;
- le caractère neuf du bien ;
- le cas échéant, la qualité de propriétaire du bien mis en vente lorsque le propriétaire est un opérateur de ventes volontaires organisateur ou son salarié, dirigeant ou associé ;
- l'intervention d'un expert dans l'organisation de la vente ;
- la mention du délai de prescription des actions en responsabilité civile engagées au cours d'une vente volontaire, à savoir un délai de cinq ans à compter de l'adjudication ou de la prisée.

**À noter**

Le professionnel doit tenir chaque jour, sous forme électronique, un registre des ventes ainsi qu'un répertoire contenant les procès-verbaux des ventes réalisées.

*Pour aller plus loin* : articles L. 321-7 et R. 321-32 à R. 321-35 du Code de commerce.

#### Obligation de tenir un registre pour les métaux précieux

Si au cours de l'exercice de son activité le professionnel est en possession d'or, d'argent, de platine ou d'alliages de ces métaux, il doit tenir un registre appelé « livre de police » comportant l'ensemble des informations relatives à la vente et au bien vendu.

*Pour aller plus loin* : articles 537 et 538 du Code général des impôts.

#### Obligations relatives à la lutte contre le blanchiment et lutte contre le terrorisme

L'opérateur de ventes aux enchères doit, au cours de son activité, mettre en œuvre l'ensemble des obligations relatives à la lutte contre le blanchiment et contre le terrorisme.

*Pour aller plus loin* : 14° de l'article L. 561-2 du Code monétaire et financier.

#### Tenue quotidienne d'un registre de vente

Le professionnel doit tenir chaque jour un registre mentionnant les caractéristiques, la provenance et le mode de règlement du bien vendu.

En cas de manquement à ces obligations ou en cas de mentions inexactes inscrites sur le registre, le professionnel encourt une peine de six mois d'emprisonnement et de 30 000 euros d'amende.

*Pour aller plus loin* : article L. 321-10 du Code de commerce ; articles 321-7 et 321-8 du Code pénal.

## 3°. Démarches et formalités d’installation

### a. Déclaration préalable d'activité

Le professionnel doit adresser une déclaration par lettre recommandée avec avis de réception ou par tout autre moyen au [Conseil des ventes volontaires de meubles aux enchères publiques](https://www.conseildesventes.fr/).

Sa déclaration doit comporter les éléments suivants :

- un justificatif d'identité ;
- une attestation certifiant qu'il ne fait l'objet d'aucune condamnation pénale, sanction disciplinaire ou interdiction d'exercice ;
- un document justifiant que les personnes chargées de diriger les ventes sont qualifiées professionnellement ;
- une copie du bail ou du titre de propriété du local où s'exerce l'activité ainsi que le dernier bilan ou le bilan prévisionnel ;
- un document justifiant l'ouverture dans un établissement de crédit d'un compte destiné à recevoir les fonds détenus pour le compte d'autrui ;
- un document justifiant que le professionnel a souscrit une assurance de responsabilité professionnelle ;
- un document justifiant que le professionnel a souscrit une assurance ou un cautionnement garantissant la représentation des fonds détenus pour le compte d'autrui.

*Pour aller plus loin* : articles R. 321-1 à R. 321-4, et article R. 321-56 du Code de commerce.

### b. Déclaration d'activité pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS)

Le ressortissant UE doit effectuer une déclaration d'activité dans les mêmes conditions que le ressortissant français (cf. supra « 3°. a. Déclaration préalable d'activité »).

Toutefois, sa demande doit être accompagnée des pièces complémentaires suivantes, le cas échéant assorties de leur traduction :

- un justificatif d'identité et de nationalité, ou, en cas de personne morale, une copie de ses statuts et un document justifiant son immatriculation à un registre public ;
- un document justifiant que le professionnel est légalement établi dans un État membre ;
- une preuve justifiant que le professionnel a exercé l'activité d'opérateur de ventes aux enchères pendant au moins un an au cours des dix dernières années dans un État membre lorsque cet État ne réglemente ni l'accès à l'activité ni son exercice ;
- une déclaration délivrée par l’État membre datant de moins de trois mois ou, à défaut, une attestation sur l'honneur qu'il n'encourt aucune interdiction même temporaire d'exercer cette activité ;
- un document mentionnant la date, le lieu de réalisation, l'identité et la qualification de la personne en charge de la vente envisagée ;
- une preuve datant de moins de trois mois attestant que le professionnel a souscrit une assurance de responsabilité civile professionnelle ainsi qu'une assurance ou un cautionnement garantissant la représentation des fonds détenus pour le compte d'autrui.

*Pour aller plus loin* : articles R. 321-56 à R. 321-61 du Code de commerce.

### c. Le cas échéant, effectuer une demande de reconnaissance pour le ressortissant en vue d'un exercice permanent (LE)

#### Autorité compétente

Le ressortissant doit adresser une demande de reconnaissance de qualification par lettre recommandée avec avis de réception au [Conseil des ventes volontaires](https://www.conseildesventes.fr/).

#### Pièces justificatives

Sa demande doit être accompagnée des documents suivants, le cas échéant traduits en français :

- un justificatif d'identité, de nationalité et de domicile du demandeur ;
- une copie de l'attestation de compétences ou du titre de formation du demandeur lui donnant accès à l'activité de commissaire-priseur de ventes volontaires ;
- si le ressortissant est titulaire d'un diplôme ou titre de formation délivré par un pays tiers et reconnu par un pays membre de l'UE ou l'EEE, une attestation délivrée par l'autorité compétente de cet État membre certifiant la durée de l'exercice professionnel du demandeur ainsi que les dates d'exercice ;
- un justificatif d'exercice de l'activité de commissaire-priseur de ventes volontaires au cours des dix dernières années dès lors que ni la formation ni l'activité ne sont réglementées dans l’État membre ;
- une preuve de sa qualification professionnelle ;
- un document justifiant qu'il ne fait l'objet d'aucune condamnation pénale, sanction disciplinaire ou interdiction d'exercice de l'activité de commissaire-priseur de ventes volontaires.

#### Délai et voies de recours

Le Conseil accuse réception de la demande dans un délai d'un mois et informe le demandeur en cas de pièce manquante. La décision du Conseil est transmise par lettre recommandée au demandeur dans un délai de trois mois à compter de la réception du dossier complet. La décision du Conseil est susceptible de recours adressé par lettre recommandée avec avis de réception au greffe de la cour d'appel de Paris.

*Pour aller plus loin* : article A. 321-27 du Code de commerce.

#### Bon à savoir : mesures de compensation

Le Conseil des ventes volontaires peut exiger que le professionnel se soumette, au choix, à une épreuve d'aptitude devant le jury professionnel chargé de l'examen d'accès au stage de formation ou à un stage d'adaptation d'une durée maximale de trois ans.

*Pour aller plus loin* : article R. 321-67 du Code de commerce.

### b. Formalités de déclaration de l’entreprise

Selon la nature de son activité, le professionnel doit s'immatriculer au registre du commerce et des sociétés (RSC) ou s'immatriculer auprès de l'Urssaf.