﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS074" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Consignment store" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="consignment-store" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/consignment-store.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="consignment-store" -->
<!-- var(translation)="Auto" -->


Consignment store
============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Deposit-sale allows individuals (depositors) to market second-hand furniture through merchants (custodians) in exhibition shops.

The depositor and custodian will agree to a sale price on which the custodian will charge a commission to pay himself once the property is sold.

### b. Competent Business Formality Centre

The relevant Business Formalities Centre (CFE) depends on the nature of the structure in which the activity is carried out:

- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

No diploma or certification is required to practice in a depot-sale. However, it is recommended to have accounting qualities to manage inventory.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

A national of a Member State of the European Union (EU) or a party to the European Economic Area (EEA) is not subject to any qualification or certification requirements, as are the French.

### c. Conditions of honorability and incompatibility

Any custodian who does not keep the register of furniture objects on a daily basis (see infra "2°. d. Obligation to keep a register of furniture (ROM)) is punishable by six months in prison and a fine of 30,000 euros.

*To go further* Article 321-7 of the Penal Code.

### d. Some peculiarities of the regulation of the activity

#### Obligation to keep a register of furniture objects (ROM)

Any professional who wishes to sell used or acquired furniture from people who have neither manufactured nor marketed them is required to fill out a paper register listed and signed (also called "brocante register") containing:

- The characteristics and provenance of the objects
- The purchase price and how each item is settled
- for objects priced less than 60.98 euros and have no artistic and historical interest, a common mention and description are sufficient.

It is also possible to use an electronic registry that will require automated processing to ensure the integrity, intangibility and security of the recorded data. The shelf life of this data is ten years.

*To go further* Articles R. 321-3 and the following of the Penal Code.

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, it is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Make a pre-report

The custodian is required to make a prior declaration worth applying for registration in the register of resellers of objects of furniture. The request is addressed to the prefect of the place of installation of the furniture dealer or to the prefect of Paris (especially for the national of an EU or EEA state) and must include the following pieces:

- a Cerfa form 1173301 filled, dated and signed;
- A photocopy of the identity document of the French professional or national of an EU or EEA state;
- a certificate of registration in the register of trades and companies or a certificate of registration in the directory of trades.

*To go further* Article R. 32161 of the Penal Code.

### c. If necessary, make a declaration as a holder of precious metals

As long as the custodian holds gold, silver or platinum objects, he must make a prior declaration to the warranty office of the regional directorate of customs and indirect duties.

The declaration must contain the name of the custodian or company, the designation of its activity, the place of practice and the address of the head office.

Upon receipt of the file, the warranty office will issue a declaration of existence.

*To go further* Section 534 of the General Tax Code.

