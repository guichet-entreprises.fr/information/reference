﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS074" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Dépôt-vente" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="depot-vente" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/depot-vente.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="depot-vente" -->
<!-- var(translation)="None" -->

# Dépôt-vente

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le dépôt-vente permet à des particuliers (déposants) de commercialiser des biens mobiliers d'occasion par l'intermédiaire de commerçants (dépositaires) dans des boutiques d'exposition.

Le déposant et le dépositaire conviendront d'un prix de vente sur lequel le dépositaire prélèvera une commission pour se rémunérer une fois le bien vendu.

### b. Centre de formalités des entreprises compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucun diplôme ni certification ne sont requis pour exercer dans un dépôt-vente. Cependant, il est recommandé d'avoir des qualités comptables pour gérer les stocks.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE) n'est soumis à aucune condition de diplôme ou de certification, au même titre que les Français.

### c. Conditions d'honorabilité et incompatibilités

Tout dépositaire qui ne tiendrait pas quotidiennement le registre d'objets mobiliers (cf. infra « 2°. d. Obligation de tenir un registre d'objets mobiliers (ROM) ») est puni de six mois de prison et de 30 000 euros d'amende.

*Pour aller plus loin* : article 321-7 du Code pénal.

### d. Quelques particularités de la réglementation de l'activité

#### Obligation de tenir un registre d'objets mobiliers (ROM)

Tout professionnel qui souhaite vendre des objets mobiliers usagés ou acquis auprès de personnes qui ne les ont ni fabriqués ni commercialisés est tenu de remplir chaque jour un registre papier coté et paraphé (encore appelé « registre de brocante ») contenant :

- les caractéristiques et la provenance des objets ;
- le prix d'achat et le mode de règlement de chaque objet ;
- pour les objets dont le prix est inférieur à 60,98 euros et n'ont pas d'intérêt artistique et historique, une mention et une description communes suffisent.

Il est également possible de recourir à un registre électronique qui devra faire l'objet d'un traitement automatisé garantissant l'intégrité, l'intangibilité et la sécurité des données enregistrées. La durée de conservation de ces données est de dix ans.

*Pour aller plus loin* : articles R. 321-3 et suivants du Code pénal.

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Effectuer une déclaration préalable

Le dépositaire est tenu de faire une déclaration préalable valant demande d'inscription au registre des revendeurs d'objets mobiliers. La demande est adressée au préfet du lieu d'installation du revendeur d'objets mobiliers ou au préfet de Paris (notamment pour le ressortissant d'un État de l'UE ou de l'EEE) et doit comporter les pièces suivantes :

- un formulaire Cerfa n° 11733*01 rempli, daté et signé ;
- une photocopie de la pièce d'identité du professionnel français ou ressortissant d'un État de l'UE ou de l'EEE ;
- une attestation d'immatriculation au registre du commerce et des sociétés ou une attestation d'inscription au répertoire des métiers.

*Pour aller plus loin* : article R. 32161 du Code pénal.

### c. Le cas échéant, effectuer une déclaration en tant que détenteur de métaux précieux

Dès lors que le dépositaire détient des objets en or, argent ou platine, il doit en faire une déclaration préalable auprès du bureau de garantie de la direction régionale des douanes et droits indirects.

La déclaration doit contenir le nom du dépositaire ou de la société, la désignation de son activité, le lieu d'exercice et l'adresse du siège sociale.

À réception du dossier, le bureau de garantie délivrera une déclaration d'existence.

*Pour aller plus loin* : article 534 du Code général des impôts.