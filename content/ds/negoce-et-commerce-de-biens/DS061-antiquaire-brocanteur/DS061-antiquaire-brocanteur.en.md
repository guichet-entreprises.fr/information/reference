﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS061" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Antique dealer/Junk shop owner" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="antique-dealer-junk-shop-owner" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/antique-dealer-junk-shop-owner.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="antique-dealer-junk-shop-owner" -->
<!-- var(translation)="Auto" -->


Antique dealer/Junk shop owner
===============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The antique dealer and the dealer are professionals whose mission is to buy and resell antiques.

These professionals will search (search) all types of everyday occasion objects (speciality of the dealer), or exceptional pieces (officially referenced) to assess them and, if necessary, restore them (speciality of antique dealer) for resale.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity
is exercised:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

No diploma or certification is required to carry out the activity of dealer-antique dealer. However, it is recommended to have professional experience in the trade or sale of art or used objects. Training in art history or a state diploma as a negotiator in art and decoration objects are nevertheless recommended.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

A national of a Member State of the European Union (EU) or a party to the European Economic Area (EEA) is not subject to any qualification or certification requirements, as are the French.

### c. Some peculiarities of the regulation of the activity

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

As the premises are open to the public, the professional must comply with the rules relating to public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

#### b. Register in the Register of Furniture Objects (ROM)

Any professional who wishes to sell used or acquired furniture from people who have neither manufactured nor marketed them is required to fill out a paper register listed and signed (also called "brocante register") containing:

- The characteristics and provenance of the objects
- The purchase price and how each item is settled
- for objects priced less than 60.98 euros and have no artistic and historical interest, a common mention and description are sufficient.

It is also possible to use an electronic registry that will require automated processing to ensure the integrity, intangibility and security of the recorded data. The shelf life of this data is ten years.

The antique dealer must send a file to the prefecture of the place of establishment including the following supporting documents:

- The form[Cerfa 117303*01](https://www.service-public.fr/professionnels-entreprises/vosdroits/R14001) completed, dated and signed;
- A valid piece of identification
- a certificate of registration with the RCS or a certificate of registration with the RMA or the receipt of declaration of activity.

**Cost**

Free.

*To go further* Articles R. 321-3 and the following of the Penal Code.

### c. If necessary, make a declaration as a holder of precious metals

As long as the dealer-antique dealer holds gold, silver or platinum objects, he must make a prior declaration to the warranty office of the regional directorate of customs and indirect duties.

The declaration is made by sending a file containing the following supporting documents:

- The declaration containing the name of the custodian or company, the designation of its activity, the place of practice and the address of the head office;
- A valid piece of identification
- a certificate of registration with the RCS or a certificate of registration with the RMA or the receipt of declaration of activity.

Upon receipt of the file, the warranty office will issue a declaration of existence.

*To go further* Section 534 of the General Tax Code.

