﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS009" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Commerçant, artisan ambulant" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="commercant-artisan-ambulant" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/commercant-artisan-ambulant.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="commercant-artisan-ambulant" -->
<!-- var(translation)="None" -->

# Commerçant, artisan ambulant

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le commerçant ou l’artisan ambulant exerce son activité de manière non sédentaire, sur la voie publique, notamment sur les halles, marchés, et foires. Ce régime s’applique dans deux hypothèses :

- quand le professionnel exerce son activité hors de la commune où est situé son habitation ou son principal établissement ;
- quand il n'a ni domicile ni résidence fixe de plus de six mois.

*Pour aller plus loin* : articles L. 123-29 et R. 123-208-1 du Code de commerce.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de l’activité exercée, de la forme juridique de l’entreprise et du nombre de salariés employés :

- pour les artisans et pour les sociétés commerciales exerçant une activité artisanale, à condition qu’ils n’emploient pas plus de dix salariés, le CFE compétent est la chambre de métiers et de l’artisanat (CMA) ;
- pour les artisans et pour les sociétés commerciales qui emploient plus de dix salariés, il s’agit de la chambre de commerce et de l’industrie (CCI) ;
- dans les départements du Haut-Rhin et du Bas-Rhin, la chambre des métiers d’Alsace est compétente

**Bon à savoir**

Dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle, l’activité reste artisanale, quel que soit le nombre de salariés employés, dès lors que l’entreprise n’utilise pas de procédé industriel. Le CFE compétent est donc la CMA ou la chambre des métiers d’Alsace. Pour plus d’informations, il est conseillé de se reporter au site officiel de la chambre des métiers d'Alsace.

Pour plus d’informations, il est conseillé de consulter [le site de la CCI de Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites/competence-cfe).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Les qualifications professionnelles exigées dépendent de la profession ambulante exercée. Il n’y a pas de condition de qualification professionnelle supplémentaire spécifique à l’exercice d’une activité ambulante. Pour plus d’informations, il est conseillé de se reporter aux fiches activités correspondant à l’activité exercée.

### b. Conditions d’honorabilité et incompatibilités

Il n’existe pas de conditions d’honorabilité spécifique à l’exercice d’une activité ambulante. En revanche, le commerçant ou l’artisan ambulant doit respecter les conditions et incompatibilités propres à la profession exercée. Pour plus d’informations, il est conseillé de se reporter aux fiches activités concernées.

### c. Garantie financière

Lorsque le commerçant ou l’artisan ambulant exerce son activité sur la voie publique ou dans un lieu public sans avoir de domicile ou de résidence fixe de plus de six mois en France, il doit se faire connaître à l’administration fiscale et déposer une somme en garantie du recouvrement des impôts et taxes dont il est redevable. Ce dépôt doit être effectué avant tout exercice de son activité.

**À noter**

Dans cette hypothèse, les obligations fiscales sont accomplies auprès du service des impôts dont relève la commune à laquelle ces personnes sont rattachées.

Pour une activité de vente de marchandises ou de prestations de services exercées sans véhicule, cette somme est de 150 €. Elle est majorée de 76 € pour l’utilisation d’un véhicule, de 150 € pour deux véhicules et de 300 € pour plus de deux véhicules.

Ce dépôt est restituable aux personnes qui justifient du paiement, de l'exonération ou de la non-exigibilité, pour la totalité de la période de validité du récépissé, de la taxe professionnelle, de la taxe sur la valeur ajoutée et des taxes assimilées et de l'impôt sur le revenu.

Il est délivré un récépissé en contrepartie du paiement de cette consignation. Le récépissé mentionne l'identité du déposant, sa commune de rattachement, la nature de l'activité qu'il exerce, les moyens d'exploitation ainsi que les identités et domiciles de ses préposés. En outre, il est délivré à chaque préposé une attestation spéciale.

Le récépissé est valable trois mois à compter de son établissement. Il ne peut être utilisé que par son titulaire, et pour les conditions d'exploitation qui y sont mentionnées. Le renouvellement du récépissé donne lieu à dépôt d'une nouvelle consignation.

Pour obtenir la délivrance du récépissé de consignation, le commerçant ou l’artisan ambulant doit fournir :

- une photographie récente de lui-même et de ses préposés ;
- le justificatif de son identité et du lieu de sa commune de rattachement ;
- le justificatif de l’identité et du domicile ou du lieu de la commune de rattachement de ses préposés ;
- lorsqu'il est ressortissant étranger, un titre de séjour valable sur le territoire national.

Ce récépissé doit être produit à toute réquisition des officiers et agents de police judiciaire, des agents de la répression des fraudes et des juges des tribunaux d’instance.

*Pour aller plus loin* : articles 302 octies, 111 quaterdecies et 50 quindecies annexe 4 du Code général des impôts.

### d. Assurance de responsabilité civile

L’assurance de responsabilité civile est obligatoire pour certains professionnels. Pour plus d’informations, il est conseillé de se reporter à la fiche activité relative à la profession exercée.

### e. Quelques particularités de la réglementation de l’activité

#### Carte permettant l’exercice d’une activité ambulante

##### Effectuer une déclaration préalable

L’exercice d’une activité commerciale ou artisanale ambulante suppose une déclaration préalable qui permet d’obtenir la carte nécessaire à l’exercice de l’activité ambulante.

Sont soumises à cette formalité :

- les personnes physiques et morales exerçant leur activité hors du territoire de la commune où est situé leur habitation ou leur principal établissement ;
- les personnes n’ayant ni domicile ni résidence fixe de plus de six mois dans un État membre de l’Union européenne (UE).

**À noter**

La personne qui n’a ni domicile ni résidence fixe est celle qui n'a pas d'établissement principal ou qui ne séjourne pas dans un État de l’UE depuis six mois au moins à titre de propriétaire ou de locataire d'un logement garni de meubles lui appartenant.

En revanche, cette formalité ne s’applique pas :

- aux agents commerciaux ;
- aux vendeurs-colporteurs de presse ;
- aux exploitants de taxi ;
- aux VRP, vendeurs à domicile indépendants et démarcheurs bancaires et financiers ;
- aux professionnels effectuant à titre accessoire dans une ou plusieurs communes limitrophes des tournées de vente de leurs produits ou de prestations de services à partir d’établissements fixes.

*Pour aller plus loin* : articles L. 123-29 et R. 123-208-1 du Code de commerce.

**Bon à savoir**

En cas de contrôle, le commerçant ou l’artisan ambulant présente cette carte. De la même manière, le salarié ou le conjoint collaborateur présentent une copie de la carte de la personne pour laquelle il travaille, un document établissant un lien avec le titulaire de la carte et un document justifiant de son identité.

*Pour aller plus loin* : articles R. 123-208-4 du Code de commerce.

##### Autorité compétente

La déclaration doit être faite auprès du CFE de la CCI ou de la CMA, en fonction de la nature de l’activité. Cette autorité est celle qui délivre ensuite la carte.

*Pour aller plus loin* : article R. 123-208-2 du Code de commerce.

##### Modalités d’obtention

La déclaration peut être remise sur place contre récépissé, par correspondance, par lettre recommandée avec avis de réception ou en ligne pour certains CFE.

La carte est valable quatre ans. Elle est renouvelable. Dans ce cas, l'ancienne carte qui a expiré doit être remise contre la nouvelle carte qui la remplace.

En cas de changement affectant son activité, le titulaire de la carte doit adresser une déclaration modificative aux fins de mise à jour ou de retrait.

Pour permettre l'exercice immédiat de l'activité ambulante, un certificat provisoire, valable un mois, peut être délivré par le CFE, à la demande de l'entrepreneur, en attendant l'obtention de la carte définitive.

*Pour aller plus loin* : article R. 123-208-3 du Code de commerce.

##### Délai de réponse

La carte permettant l’exercice d’une activité commerciale ou artisanale ambulante est délivrée dans un délai maximum d’un mois, à compter de la réception du dossier de déclaration. Dans l’hypothèse d’une déclaration concomitante au dépôt d’une demande de création d’entreprise, le délai d’un mois court à compter de l’inscription au registre de publicité légale.

Durant ce délai et jusqu’à réception de la carte définitive, le déclarant peut présenter en cas de contrôle un certificat provisoire délivré, à sa demande, par la CCI ou la CAM.

En cas de renouvellement de la carte, le délai de délivrance de la nouvelle carte est porté à 15 jours à compter de la réception du dossier complet de déclaration.

##### Pièces justificatives

Le dossier de déclaration doit comporter :

- le formulaire Cerfa 14022*02 daté et signé ;
- un extrait datant de moins de trois mois du RCS ou du RM ou, pour les personnes physiques dispensées d’ immatriculation, le certificat d’inscription au répertoire des entreprises et des établissements (Sirene) et, pour les associations exerçant une activité commerciale, le Sirene et la copie de leur statut ;
- pour les ressortissants de l’UE ne disposant pas d’établissement en France mais ayant déclaré leur activité commerciale ou artisanale dans un autre État de l’UE, la preuve de cette déclaration ;
- une copie de la pièce d’identité ou, le cas échéant, du titre de circulation ou de séjour ;
- deux photographies d’identité récentes.

Lorsque la déclaration est effectuée concomitamment à une déclaration de création d’entreprise soumise à une inscription à un registre de publicité légale, la déclaration d’entreprise vaut remise de la déclaration préalable d’une activité commerciale ou artisanale ambulante. Dans cette hypothèse, le demandeur produit seulement deux photographies d’identité récentes.

Le dossier de renouvellement s’effectue dans les mêmes conditions que la demande initiale.

*Pour aller plus loin* : articles R. 123-208-2 et suivants, A. 123-80-1 et suivants du Code de commerce.

##### Coût

La carte est délivrée contre paiement d'une redevance de 15 euros.

*Pour aller plus loin* : articles R. 123-208-3 et A. 123-80-5 du Code de commerce.

#### Livret spécial de circulation en cas d’absence de résidence fixe

Les commerçants et artisans ambulants qui n’ont ni domicile ni résidence fixe de plus de six mois dans un État de l’UE doivent être munis d'un livret spécial de circulation délivré par les autorités administratives. Il en est de même pour les personnes qui les accompagnent et pour leurs préposés s’ils sont âgés de plus de seize ans et n'ont en France ni domicile, ni résidence fixe depuis plus de six mois.

**À noter**

Les employeurs doivent s'assurer que leurs préposés sont effectivement munis de ce document, lorsqu'ils y sont tenus.

Le livret spécial de circulation est valable cinq ans. Pour obtenir la prorogation de validité, son titulaire doit présenter personnellement sa requête au commissaire de la République ou au commissaire-adjoint de la République soit de l'arrondissement de sa commune de rattachement, soit du lieu le plus proche de son lieu de séjour.

Un récépissé de dépôt lui est aussitôt remis, qui vaut titre de circulation pour une durée de trois mois. Lors du dépôt, le requérant doit indiquer la préfecture ou la sous-préfecture auprès de laquelle il désire retirer son titre prolongé.

**À savoir**

Le défaut de livret de circulation constitue une contravention de 5e classe.

##### Autorité compétente

Pour demander le livret spécial de circulation, le commerçant ou l’artisan ambulant doit se présenter personnellement au préfet ou au sous-préfet de l'arrondissement où est située la commune à laquelle il désire être rattaché. À Paris, la demande doit être effectuée au préfet de police.

##### Pièces justificatives

À l’appui de sa demande, le commerçant ou l’artisan ambulant doit :

- produire les justificatifs de son identité et de sa nationalité et sa photographie d'identité en trois exemplaires ;
- indiquer simultanément la commune à laquelle il désire être rattaché et le motif du choix de la commune ;
- s’il n'est pas de nationalité française, fournir le document en cours de validité ayant permis l'entrée en France et, le cas échéant, son titre de séjour et sa carte de travailleur ou de commerçant étranger.

##### Délai de réponse

En l’absence de réponse du préfet dans un délai de deux mois, la demande est rejetée.

Il est délivré une attestation valant titre provisoire de circulation, valable pour une durée maximale d'un mois.

*Pour aller plus loin* : décret n° 2014-1292.

##### Coût

Le livret de circulation n’est soumis à aucun visa.

*Pour aller plus loin* : loi n° 69-3 article 2 ; décret n° 70-708.

#### Restrictions au commerce ambulant

##### Vente de boissons alcoolisées

Il est interdit aux marchands ambulants de vendre au détail, soit pour consommer sur place, soit pour emporter, des boissons alcoolisées des quatrième et cinquième groupes, tels que le rhum ou les alcools provenant de la distillation des vins, cidres.

Pour plus d’informations, il est conseillé de se reporter à la fiche activités « [Débit de boissons](https://www.guichet-entreprises.fr/fr/ds/alimentation/debitant-de-boissons.html) ».

*Pour aller plus loin* : articles L. 3321-1 et L. 3322-6 du Code de la santé publique.

##### Commerce d’animaux de compagnie

La vente des chiens, des chats et des autres animaux de compagnie est interdite dans les foires, marchés, brocantes, salons, expositions ou autres manifestations non spécifiquement consacrés aux animaux, sauf dérogation préfectorale.

*Pour aller plus loin* : article L. 214-7 du Code rural et de la pêche maritime.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Autorisation(s) post-immatriculation

#### Demander une autorisation d’installation sur la voie publique

Le commerçant ou l’artisan ambulant doit solliciter une autorisation d’installation sur le domaine public. Cette autorisation varie selon le type de lieu occupé :

- dans les cas des halles, des marchés et des foires, il s’agit d’une demande d’emplacement sur un marché à adresser à la mairie, au placier municipal ou à l’organisateur de l’événement ;
- l’occupation sous emprise concerne les terrasses ouvertes, les étalages, les stationnements de camionnette, elle nécessite un permis de stationnement à demander à l’autorité administrative chargée de la police de la circulation ;
- dans le cas d’une occupation privative avec emprise (terrasses fermées, kiosques fixés au sol), une permission de voirie doit être demandée à l’autorité administrative chargée de la gestion du domaine.