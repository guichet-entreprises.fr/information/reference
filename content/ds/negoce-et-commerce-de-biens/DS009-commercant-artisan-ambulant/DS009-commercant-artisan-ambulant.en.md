﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS009" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Itinerant trader or artisan" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="itinerant-trader-or-artisan" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/itinerant-trader-or-artisan.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="itinerant-trader-or-artisan" -->
<!-- var(translation)="Auto" -->


Itinerant trader or artisan
===============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The merchant or the travelling craftsman carries out his activity in a non-sedentary manner, on public roads, especially in halls, markets, and fairs. This regime applies in two assumptions:

- When the professional operates outside the commune where his dwelling or main establishment is located;
- when he has no home or fixed residence for more than six months.

*For further information*: Articles L. 123-29 and R. 123-208-1 of the Code of Commerce.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the activity carried out, the legal form of the company and the number of employees employed:

- for craftsmen and commercial companies engaged in artisanal activity, provided they do not employ more than ten employees, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for craftsmen and commercial companies that employ more than ten employees, this is the Chamber of Commerce and Industry (CCI);
- in the departments of the Upper and Lower Rhine, the Alsace Chamber of Trades is competent

**Good to know**

In the departments of Lower Rhine, Upper Rhine and Moselle, the activity remains artisanal, regardless of the number of employees employed, as long as the company does not use an industrial process. The competent CFE is therefore the CMA or the Chamber of Trades of Alsace. For more information, it is advisable to refer to the<span id=«c.-codes-ape-à-titre-indicatif" class="anchor»> id="«c.-codes-ape-à-titre-indicatif"" class=""></span id=«c.-codes-ape-à-titre-indicatif" class="anchor»>>official website of the Alsace Chamber of Trades.

For more information, it is advisable to consult[the SITE of the CCI in Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites/competence-cfe).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional qualifications required depend on the travelling profession. There is no additional qualification requirement specific to the exercise of a walking activity. For more information, it is advisable to refer to the activity sheets corresponding to the activity carried out.

### b. Conditions of honorability and incompatibility

There are no conditions of honorability specific to the exercise of a walking activity. On the other hand, the merchant or the travelling craftsman must respect the conditions and incompatibilities specific to the profession exercised. For more information, it is advisable to refer to the relevant activity sheets.

### c. Financial guarantee

When the merchant or travelling craftsman carries out his activity on public roads or in a public place without having a residence or fixed residence for more than six months in France, he must make himself known to the tax authorities and deposit a sum as a guarantee of the collection of taxes owed to him. This deposit must be made before any exercise of its activity.

**Please note**

In this case, the tax obligations are fulfilled with the tax department to which the municipality to which these persons are attached is responsible.

For an activity selling goods or services carried out without a vehicle, this amount is 150 euros. It is increased by 76 euros for the use of a vehicle, 150 euros for two vehicles and 300 euros for more than two vehicles.

This deposit is refundable to those who justify payment, exemption or non-due, for the entire period of validity of the receipt, the professional tax, the value-added tax and the related taxes and the tax on income.

A receipt is issued in return for payment of this deposit. The receipt mentions the identity of the applicant, his commune of attachment, the nature of the activity he carries out, the means of exploitation as well as the identities and homes of his agents. In addition, each attendant is issued a special certificate.

The receipt is valid for three months from its establishment. It can only be used by its owner, and for the operating conditions mentioned in it. The renewal of the receipt results in the filing of a new deposit.

In order to obtain the receipt, the merchant or street craftsman must provide:

- a recent photograph of himself and his attendants;
- proof of his identity and the place of his commune of attachment;
- proof of the identity and the residence or the place of the commune of attachment of its attendants;
- a valid residence permit on the national territory.

This receipt must be produced at any request from judicial police officers, fraud enforcement officers and district court judges.

*For further information*: Articles 302 octies, 111 quaterdecies and 50 quindecies Annex 4 of the General Tax Code.

### d. Liability insurance

Liability insurance is mandatory for some professionals. For more information, it is advisable to refer to the activity sheet relating to the profession exercised.

### e. Some peculiarities of the regulation of the activity

#### Map allowing the exercise of a walking activity

**Make a pre-report**

The exercise of a travelling commercial or artisanal activity requires a prior declaration that allows to obtain the card necessary for the exercise of the walking activity.

Subject to this formality:

- individuals and corporations operating outside the territory of the commune where their dwelling or main establishment is located;
- persons who have no residence or fixed residence for more than six months in a Member State of the European Union (EU).

**Please note**

The person who does not have a home or a fixed residence is one who does not have a principal establishment or who has not been in an EU state for at least six months as the owner or tenant of a dwelling furnished with furniture belonging to him.

However, this formality does not apply:

- Commercial agents;
- press sellers-peddlers;
- Taxi operators;
- VRPs, independent home sellers and bank and financial canvassers;
- professionals performing incidental tours in one or more neighbouring municipalities to sell their products or to provide services from fixed establishments.

*For further information*: Articles L. 123-29 and R. 123-208-1 of the Code of Commerce.

**Good to know**

In case of control, the shopkeeper or the street craftsman presents this card. Similarly, the employee or co-worker spouse presents a copy of the card of the person for whom he works, a document establishing a link with the cardholder and a document justifying his identity.

*For further information*: Articles R. 123-208-4 of the Code of Commerce.

**Competent authority**

The declaration must be made to the ICC OR CMA's CFE, depending on the nature of the activity. This authority is the one that then issues the card.

*For further information*: Article R. 123-208-2 of the Code of Commerce.

**How to get it**

The declaration may be submitted on-site for receipt, by mail, by recommended letter with notice of receipt or online for certain CFEs.

The card is valid for four years. It's renewable. In this case, the old card that has expired must be given against the new card that replaces it.

In the event of a change affecting their business, the cardholder must submit an amending statement for update or withdrawal.

To enable the immediate exercise of the walking activity, a provisional certificate, valid for one month, can be issued by the CFE, at the request of the contractor, pending the obtaining of the final card.

*For further information*: Article R. 123-208-3 of the Code of Commerce.

**Response time**

The card allowing the exercise of a travelling commercial or artisanal activity is issued within a maximum of one month, from the receipt of the declaration file. In the event of a concurrent declaration of the filing of an application to start a business, the period of one month runs from registration to the register of legal advertising.

During this period and until the final card is received, the registrant may present a provisional certificate issued, at his request, by the ICC or the CAM.

If the card is renewed, the deadline for issuing the new card is increased to 15 days from the time the full return file is received.

**Supporting documents**

The reporting file must include:

- Form Cerfa 1402202 dated and signed;
- an excerpt less than three months old from the RCS or the RM or, for individuals exempt from registration, the certificate of registration in the directory of companies and establishments (Sirene) and, for associations carrying out an activity Sirene and copying their status;
- for EU nationals who do not have an establishment in France but have declared their business or artisanal activity in another EU state, proof of this declaration;
- A copy of the ID or, if necessary, the traffic or residence permit;
- two recent identity photographs.

When the declaration is made in conjunction with a declaration of business creation subject to registration in a legal advertising register, the company declaration is worth submitting the prior declaration of a business activity or itinerant crafts. In this case, the applicant produces only two recent identity photographs.

The renewal file is carried out under the same conditions as the original application.

*For further information*: Articles R. 123-208-2 and following, A. 123-80-1 and beyond the Code of Commerce.

**Cost**

The card is issued for payment of a fee of 15 euros.

*For further information*: Articles R. 123-208-3 and A. 123-80-5 of the Code of Commerce.

#### Special traffic booklet in case of absence of fixed residence

Travellers and craftsmen who do not have a home or fixed residence for more than six months in an EU state must have a special traffic booklet issued by the administrative authorities. The same applies to the people accompanying them and their attendants if they are over the age of sixteen and have no home or fixed residence in France for more than six months.

**Please note**

Employers must ensure that their employees are in fact provided with this document, when they are required to do so.

The special circulation booklet is valid for five years. In order to obtain the extension of validity, the holder must personally submit his request to the Commissioner of the Republic or to the Deputy Commissioner of the Republic either from the borough of his commune of connection or from the place closest to his place of residence.

A deposit receipt is immediately given to him, which is worth a circulation title for a period of three months. At the time of filing, the applicant must indicate the prefecture or sub-prefecture from which he wishes to withdraw his extended title.

**What to know**

The lack of a traffic booklet is a 5th class ticket.

**Competent authority**

In order to apply for the special traffic booklet, the merchant or the travelling craftsman must personally present himself to the prefect or the sub-prefect of the borough where the commune to which he wishes to be attached is located. In Paris, the request must be made to the prefect of police.

**Supporting documents**

In support of his request, the merchant or the travelling craftsman must:

- produce proof of identity and nationality and identity photography in three copies;
- simultaneously indicate the commune to which it wishes to be attached and the reason for the choice of commune;
- If he is not a French national, provide the valid document that allowed entry to France and, if necessary, his residence permit and his foreign worker or merchant card.

**Response time**

If the prefect does not respond within two months, the application is rejected.

It is issued a certificate valid provisional traffic title, valid for a maximum of one month.

*For further information*: Decree No. 2014-1292.

**Cost**

The traffic booklet is not subject to any visa.

*For further information*: Law 69-3 Article 2; Decree 70-708.

#### Restrictions on street trade

**Sale of alcoholic beverages**

Street vendors are prohibited from retailing, either for on-site consumption or to take away, alcoholic beverages from the fourth and fifth groups, such as rum or spirits from the distillation of wines, ciders.

For more information, it is advisable to refer to the activity sheet[Beverage flow](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*For further information*: Articles L. 3321-1 and L. 3322-6 of the Public Health Code.

**Pet trade**

The sale of dogs, cats and other pets is prohibited at fairs, markets, flea markets, trade shows, exhibitions or other events not specifically dedicated to animals, except for prefectural exemption.

*For further information*: Article L. 214-7 of the Rural Code and Marine Fisheries.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, it is recommended to refer to the activity sheets "Declaration of a Commercial Company" and "Registration of an Individual Company in the Register of Trade and Society."

### b. Post-registration authorisation

#### Request permission to install on public roads

The merchant or the travelling craftsman must apply for a permit to install on the public domain. This permission varies depending on the type of place occupied:

- In the case of halls, markets and fairs, this is a request for a location in a market to be addressed to the town hall, the municipal placier or the event organizer;
- occupancy under the influence concerns open terraces, displays, van parking lots, it requires a parking permit to be requested by the administrative authority responsible for the traffic police;
- in the case of a private occupation with right-of-way (closed terraces, kiosks fixed to the ground), road permission must be requested from the administrative authority responsible for the management of the domain.

