﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS069" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Commerce de gros, commerce interentreprises" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="commerce-de-gros-commerce-interentreprises" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/commerce-de-gros-commerce-interentreprises.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="commerce-de-gros-commerce-interentreprises" -->
<!-- var(translation)="None" -->

# Commerce de gros, commerce interentreprises

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité de commerce de gros (commerce interentreprises) consiste pour le professionnel à acheter et revendre des marchandises à une clientèle principalement professionnelle (grossistes, intermédiaires) et ce, quelle que soit la quantité vendue.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

L'activité étant de nature commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exerce une activité d'achat-revente son activité sera à la fois commerciale et artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité de commerce de gros ou de commerce interentreprises aucune qualification professionnelle n'est requise. Toutefois, le professionnel qui souhaite rejoindre un marché d'intérêt national (MIN) doit effectuer une demande d'autorisation.

*Pour aller plus loin* : article L. 761-1 et suivants du Code de commerce.

#### Installation au sein d'un marché d'intérêt national (MIF)

Les MIF sont des plate-formes de service public réservées aux professionnels et permettant d'assurer l'organisation et la sécurisation des circuits de distribution des produits agricoles et alimentaires. L'accès à ces MIF est réglementé et restreint aux commerçants et producteurs autorisés.

Ainsi, pour en faire partie, le professionnel exploitant un commerce de gros doit effectuer une demande d'installation (cf. infra « 3°. a. Demande d'autorisation d'installation au sein d'un MIF »).

Sont soumis à cette demande d'autorisation, les projets d'implantation ou d'extension des locaux destinés à recevoir des ventes en gros sur une surface de plus de 1000 mètres carrés.

*Pour aller plus loin* : R. 761-12-1 du Code de commerce.

#### Commerce de gros de denrées alimentaires d'origine animale

Pour exercer une activité de préparation, transformation, manipulation ou entreposage de denrées alimentaires d'origine animale, le professionnel doit soit :

- obtenir un agrément sanitaire dès lors qu'il exerce son activité sur un marché sans remise directe au consommateur ;
- effectuer une déclaration de manipulation des denrées dès lors qu'il est en contact avec le consommateur.

##### Demande d'agrément sanitaire en vue de la vente en gros de denrées alimentaires d'origine animale

Dès lors que l'activité du commerçant a trait à des produits d'origine animale et destinés à la consommation, il doit au préalable obtenir une autorisation d'exercice.

**À noter**

Le cas échéant l'établissement doit comprendre au moins une personne ayant suivi une formation spécifique aux règles d'hygiène alimentaire ou d'une expérience professionnelle d'au moins trois ans au sein d'un établissement du secteur alimentaire en tant que gestionnaire ou exploitant.

##### Autorité compétente*

Avant le début de son activité, le professionnel doit adresser sa demande en ligne ou par voie postale à la direction départementale de la cohésion sociale et de la protection des populations (DDCSPP) où il souhaite s'implanter.

##### Pièces justificatives

Le professionnel doit remplir le formulaire Cerfa 13983 *02 disponible sur le site du Ministère chargé de l'agriculture.

Sa demande doit également contenir :

- les éléments descriptifs de l'établissement du demandeur ;
- le plan de maîtrise sanitaire ;
- l'ensemble des éléments fixés à l'annexe II de l'arrêté du 8 juin 2006 relatif à l'agrément sanitaire des établissements mettant sur le marché des produits d'origine animale ou des denrées contenant des produits d'origine animale.

##### Procédure

Dès lors que le dossier est reçu complet, l'autorité compétente délivre un agrément conditionnel pour une durée de trois mois. À l'issue de ce délai un contrôle des équipements et installations est effectué et en cas de conformité, l'établissement est agréé et reçoit un numéro d'agrément. En l'absence de réponse au delà de deux mois, la demande d'agrément est réputée rejetée.

##### Coût

Gratuit.

*Pour aller plus loin* : article L. 233-2 du Code rural et de la pêche maritime ; arrêté du 8 juin 2006 susvisé.

#### Dispense d'agrément sanitaire

Certains établissements sont dispensés d'obtenir un tel agrément dès lors qu'ils remplissent les trois conditions suivantes :

- la quantité maximale de produits qu'ils cèdent ne dépasse pas le seuil fixé pour chaque produit au sein de la deuxième colonne de l'annexe III de l'arrêté susvisé ;
- cette quantité par catégorie de produits, représente au maximum 30 % de la production totale de l'établissement pour ce produit ;
- la distance entre l'établissement et les établissements livrés ne dépasse pas un rayon de quatre vingt kilomètres.

Le cas échéant, il doit adresser une déclaration préalable au préfet du lieu d'implantation de son établissement en ligne via le formulaire Cerfa 13982*05.

Sa déclaration préalable doit également contenir :

- la liste détaillée des produits cédés ainsi que la liste des établissements destinataires (activité, adresse et la distance par rapport à l'établissement du demandeur) ;
- la quantité hebdomadaire des produits cédés et produite pour chaque produit.

*Pour aller plus loin* : titre III de l'arrêté du 8 juin 2006, rubrique mes démarches du [site officiel](http://agriculture.gouv.fr/) du ministère de l'agriculture et de l'alimentation.

#### Le cas échéant, effectuer une déclaration d'exploitation d'un commerce de gros de denrées d'origine animale non soumis à agrément

Dès lors que le professionnel exploite un établissement non soumis à un agrément sanitaire, il doit effectuer une déclaration d'exploitation.

##### Autorité compétente

Le professionnel doit transmettre sa déclaration au préfet du lieu où il souhaite implanter son établissement.

**Pièces justificatives** 

Le professionnel doit remplir le formulaire Cerfa 13984*03 en ligne sur le site du ministère chargé de l'agriculture.

##### Coût

Gratuit.

*Pour aller plus loin* : articles L. 233-2 R. 233-4 du Code rural et de la pêche maritime 

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement)

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union Européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer l'activité de commerce de gros à titre temporaire et occasionnel en France.

A ce titre, le professionnel ressortissant UE est soumis aux mêmes exigences professionnelles que le ressortissant français (cf. supra « 2°. a.Qualifications professionnelles »).

### c. Quelques particularités de la réglementation de l’activité

**Respect des normes de sécurité et d'accessibilité**

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des Établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

## 3°. Démarches et formalités d’installation

### a. Demande d'autorisation d'installation au sein d'un MIF

#### Autorité compétente

Le professionnel doit adresser sa demande en deux exemplaires par pli recommandé avec avis de réception au préfet ou par voie électronique.

#### Pièces justificatives

Sa demande se décompose en deux parties :

La première partie doit mentionner :

- l'identité du demandeur et son numéro unique d'identification ;
- un extrait d'immatriculation au registre du commerce et des sociétés (RCS), d'une inscription équivalente pour le ressortissant étranger ou les statuts enregistrés auprès des services fiscaux de la société si celle-ci est en cours de constitution ;
- les produits envisagés au cours de la vente en gros ;
- les surfaces prévues par le projet à savoir celles :
  - de vente globale,
  - de vente dédiée aux produits,
  - de réserve,
  - de stationnement des clients, des fournisseurs et celle nécessaire aux manœuvres de livraison ;
- les contraintes techniques spécifiques du projet.

La seconde partie doit préciser les conditions de réalisation et d'exploitation des locaux et mentionner :

- les moyens de transports nécessaires à l'activité et leur compatibilité avec ceux existants ;
- les capacités d'accueil pour le chargement et le déchargement des marchandises ;
- la consommation énergétique et l'impact du projet sur l'environnement.

Cette partie doit également comprendre un plan de la surface de vente (exploitée et projetée).

#### Procédure

Lorsque la première partie du dossier est complète, le préfet en informe le gestionnaire du MIF et lui transmet une copie de la première partie de la déclaration. Le gestionnaire doit alors informer le préfet de la disponibilité de la surface demandée par l'exploitant. Dès lors qu'il possède l'ensemble des surfaces, le gestionnaire du MIF transmet une proposition d'installation au préfet comprenant :

- un plan de localisation des surfaces au sein du marché ;
- les caractéristiques des terrains et installations qu'il peut mettre à sa disposition ;
- le règlement intérieur du marché ;
- les conditions de mise à disposition des fluides et traitement des déchets et eaux usagées ;
- les conditions financières de cette mise à disposition des locaux.

#### Coût

Gratuit.

*Pour aller plus loin* : articles A. 761-11 à A. 761-14 du Code de commerce.

### b. Formalités de déclaration de l’entreprise

#### Autorité compétente

Le commerçant doit procéder à la déclaration de son entreprise, et pour cela, doit, effectuer une déclaration auprès de la chambre de commerce et d'industrie (CCI).

#### Pièces justificatives

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI adresse le jour même, un récépissé au professionnel les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Si le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### c. Le cas échéant, enregistrement des statuts de la société

Le professionnel doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du Service des Impôts des Entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.