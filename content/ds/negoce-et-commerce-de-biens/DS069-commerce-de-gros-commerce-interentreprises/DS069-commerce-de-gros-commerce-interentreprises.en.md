﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS069" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Wholesale trade - business-to-business trade" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="wholesale-trade-business-to-business-trade" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/trade/wholesale-trade-business-to-business-trade.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="wholesale-trade-business-to-business-trade" -->
<!-- var(translation)="Auto" -->

Wholesale trade - business-to-business trade
===========================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

Wholesale trade (business-to-business) consists of the professional buying and reselling goods to a predominantly professional clientele (wholesalers, intermediaries) regardless of the quantity sold.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

As the activity is commercial in nature, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional carries out a buying and resale activity his activity will be both commercial and artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To carry out the business of wholesale or business-to-business no professional qualifications are required. However, a professional who wishes to join a market of national interest (MIN) must apply for permission.

*For further information*: Article L. 761-1 and the following Code of Commerce.

#### Installation in a National Interest Market (MIF)

MIFIs are public service platforms reserved for professionals and to ensure the organization and security of distribution channels for agricultural and food products. Access to these MIFMs is regulated and restricted to licensed traders and producers.

Thus, in order to be part of it, the professional operating a wholesale business must make a request for installation (see infra "3." a. Application for permission to install within an MIF").

This application for authorisation includes projects to locate or expand premises to receive wholesale sales over an area of more than 1000 square metres.

*For further information*: In: R. 761-12-1 of the Code of Commerce.

#### Wholesale trade in animal foodstuffs

In order to carry out activity of preparation, processing, handling or storage of animal foodstuffs, the professional must either:

- obtain health approval if it operates in a market without direct discounting to the consumer;
- make a declaration of handling of the food as long as it is in contact with the consumer.

**Application for health approval for wholesale animal food**

As long as the trader's activity relates to animal products intended for consumption, he must first obtain an authorization to practice.

**Please note**

If necessary, the facility must include at least one person who has undergon specific training in food hygiene rules or has at least three years of professional experience in a food sector establishment as a manager or operator.

**Competent authority**

Before starting his activity, the professional must send his application online or by post to the Departmental Directorate of Social Cohesion and Population Protection (DDCSPP) where he wishes to establish himself.

**Supporting documents**

The professional must complete Cerfa form 13983 available on the website of the Ministry of Agriculture.

Its application must also include:

- The descriptive elements of the applicant's establishment;
- The health control plan
- all the elements attached to the Appendix II of the decree of 8 June 2006 on the health accreditation of establishments that market animal products or foodstuffs containing animal products.

**Procedure**

Once the file is received completely, the competent authority issues a conditional approval for a period of three months. At the end of this period, a check of the equipment and facilities is carried out and in the event of compliance, the establishment is approved and receives an accreditation number. In the absence of a response beyond two months, the application for accreditation is deemed to be rejected.

**Cost**

Free.

*For further information*: Article L. 233-2 of the Rural Code and Marine Fisheries; June 8, 2006.

**Health accreditation expense**

Some institutions are exempt from obtaining such accreditation if they meet the following three conditions:

- the maximum amount of products they sell does not exceed the threshold set for each product within the second column of the Appendix III of the aforementioned decree;
- This quantity by product category represents a maximum of 30% of the establishment's total production for this product;
- the distance between the establishment and the establishments delivered does not exceed a radius of eighty kilometres.

If necessary, he must send a prior statement to the prefect of the location of his online establishment via the[Form](https://agriculture-portail.6tzen.fr/default/requests/cerfa13982/) Cerfa 13982*05.

Its pre-declaration must also include:

- A detailed list of the products sold as well as the list of receiving establishments (activity, address and distance from the applicant's establishment);
- the weekly quantity of products sold and produced for each product.

*For further information*: Title III of the order of June 8, 2006, under my steps of the[official website](http://agriculture.gouv.fr/) Ministry of Agriculture and Food.

**If necessary, make a declaration of exploitation of a wholesale trade in animal products not subject to approval**

As long as the professional operates an establishment not subject to health approval, he must make a declaration of operation.

**Competent authority**

The professional must submit his statement to the prefect of the place where he wishes to set up his establishment.

**Supporting documents**

The professional must complete the form[Cerfa](https://agriculture-portail.6tzen.fr/default/requests/cerfa13984/) 1398403 online on the website of the Ministry of Agriculture.

**Cost**

Free.

*For further information*: Articles L. 233-2 R. 233-4 of the Rural Code and Marine Fisheries

### b. Professional Qualifications - European Nationals (Free Service Or Freedom of establishment)

There is no provision for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement to carry out wholesale trade on a temporary and casual basis in France.

As such, the eu national professional is subject to the same professional requirements as the French national (see above "2o. a.Professional qualifications").

### c. Some peculiarities of the regulation of the activity

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERPs) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

It is advisable to refer to the "Establishment receiving the public" sheet for more information.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Application for installation authorization within a MIF

**Competent authority**

The professional must submit his application in two copies by recommended fold with notice of receipt to the prefect or electronically.

**Supporting documents**

His application is divided into two parts:

The first part should mention:

- The applicant's identity and unique identification number
- an extract of registration in the Register of Trade and Companies (RCS), of equivalent registration for the foreign national or the statutes registered with the tax services of the company if it is in the process of being established;
- Products envisaged during the wholesale sale;
- the areas planned by the project, namely:- global sales,
  - dedicated to products,
  - reserve,
  - parking of customers, suppliers and the parking required for delivery manoeuvres;
- the specific technical constraints of the project.

The second part should specify the conditions for the construction and operation of the premises and mention:

- The means of transport necessary for the activity and their compatibility with existing ones;
- Reception capacity for loading and unloading goods;
- energy consumption and the impact of the project on the environment.

This part must also include a plan of the sales area (exploited and projected).

**Procedure**

When the first part of the file is complete, the prefect informs the MIF manager and sends him a copy of the first part of the statement. The manager must then inform the prefect of the availability of the surface requested by the operator. As long as he owns all the surfaces, the manager of the MIF sends a proposal for installation to the prefect including:

- A map of the location of surfaces within the market;
- The characteristics of the land and facilities it can make available to it;
- Internal market regulation;
- conditions for the provision of fluids and treatment of waste and waste water;
- financial conditions for making the premises available.

**Cost**

Free.

*For further information*: Articles A. 761-11 to A. 761-14 of the Code of Commerce.

### b. Company reporting formalities

**Competent authority**

The trader must report his business, and for this, must make a declaration with the Chamber of Commerce and Industry (CCI).

**Supporting documents**

The person concerned must provide the supporting documents depending on the nature of its activity.

**Timeframe**

The CCI's business formalities centre sends the missing documents to the professional on the same day. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

#### c. If necessary, registration of the company's statutes

The professional must, once the company's statutes have been dated and signed, register them with the Corporate Tax Office (SIE) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

