﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS068" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Non-food retailer" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="non-food-retailer" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/trade/non-food-retailer.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="non-food-retailer" -->
<!-- var(translation)="Auto" -->


Non-food retailer
===============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Non-food retailing consists of the purchase of new or used products for sale to individuals. This activity is done in stores, in markets or also via the internet.

Products are not processed until they are resold.

### b. Competent Business Formality Centre

The relevant Business Formalities Centre (CFE) depends on the nature of the structure in which the activity is carried out:

- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Professional qualifications
----------------------------------------

### a. Professional qualifications

No specific diploma is required for the operation of a non-food retail business. However, the individual is advised to have knowledge of accounting and management.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

A national of a Member State of the European Union (EU) or party to the Agreement on the European Economic Area (EEA) is not subject to any qualification or certification requirements, as are the French.

### c. Some peculiarities of the regulation of the activity

#### Price display

The operator of a non-food retail business must inform the public of the prices charged, using the route of marking, labelling, display or any other appropriate process.

Each product must have a unit price and a selling price.

**Please note**

For certain products (shoes, leather products, furniture, etc.), labelling must include information on the materials used, on the manufacturer's identification or the implementation process.

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

If the premises are open to the public, the professional must comply with the ERP rules:

- Fire
- accessibility.

For more information, it is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

#### Video surveillance

Non-food retail outlets can be equipped with systems for video surveillance of customers. The operator will therefore have to indicate their presence in order not to infringe on their privacy.

The operator will also have to declare his installation with the prefecture of the establishment and complete the form[Cerfa 13806*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13806.do), accompanied by any document detailing the system used.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The contractor must register with the Trade and Corporate Register (SCN). It is advisable to refer to the "Artisanal Company Reporting Formalities" for more information.

### b. If necessary, seek an operating authorization

The person must apply for permission to operate his non-food retail business and, if necessary, before the building permit is granted in the following cases:

- Creation of a retail store with a retail area of more than 1,000 square metres resulting either from a new construction or from the transformation of an existing building;
- Expanding the retail space of a retail store that has already reached the 1,000 square metre threshold or is expected to exceed it by completing the project;
- any change in the business line of a business with a sales area of more than 1,000 square metres when the store's new business is food;
- Creating a commercial complex with a total sales area of more than 1,000 square metres;
- extending the sales area of a commercial complex that has already reached the threshold of 1,000 square metres or is expected to exceed it by the project' completion;
- re-opening to the public on the same site of a retail store with a retail area of more than 1,000 square metres whose premises have ceased to operate for three years;
- creation or extension of a permanent point of withdrawal by customers of telematics-controlled retail purchases, organized for automobile access.

The request is sent in twelve copies to the Departmental Commission for Commercial Planning (CDAC) of the prefecture of the department where the trade is located, which has two months to authorize the operation.

The application is accompanied by all the following supporting documents:

- an indicative plan showing the sales area of the shops;
- Information:- delineation of the project's catchment area,
  - public transport and pedestrian and cycling access that are served around the trade;
- a study with the elements to assess the effects of the project on:- Accessibility of the commercial offer,
  - the flow of passenger cars and delivery vehicles as well as secure access to public roads,
  - Space management,
  - energy consumption and pollution,
  - landscapes and ecosystems.

If refused, the decision may be appealed to the National Commercial Planning Commission, which will rule within four months.

*For further information*: Articles L. 752-1 and the following of the Code of Commerce.

