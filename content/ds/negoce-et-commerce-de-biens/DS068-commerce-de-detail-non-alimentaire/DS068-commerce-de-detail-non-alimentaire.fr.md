﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS068" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Commerce de détail non alimentaire" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="commerce-de-detail-non-alimentaire" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/commerce-de-detail-non-alimentaire.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="commerce-de-detail-non-alimentaire" -->
<!-- var(translation)="None" -->

# Commerce de détail non alimentaire

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l'activité

### a. Définition

Le commerce de détail non alimentaire consiste en l'achat de produits neufs ou d'occasion destinés à être vendus à des particuliers. Cette activité se fait en magasin, sur des marchés ou également par internet .

Les produits ne sont pas transformés avant d'être revendus.

### b. Centre de formalités des entreprises compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Qualifications professionnelles

### a. Qualifications professionnelles

Aucun diplôme spécifique n'est requis pour l'exploitation d'un commerce de détail non alimentaire. Toutefois, il est conseillé à l'intéressé d'avoir des connaissances en matière comptable et en gestion.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) n'est soumis à aucune condition de diplôme ou de certification, au même titre que les Français.

### c. Quelques particularités de la réglementation de l'activité

#### Affichage des prix

L'exploitant d'un commerce de détail non alimentaire doit informer le public des prix pratiqués, en utilisant la voie du marquage, de l'étiquetage, de l'affichage ou par tout autre procédé approprié.

Chaque produit doit comporter un prix à l'unité de mesure et un prix de vente.

**À noter**

Pour certains produits (chaussures, produits en cuir, meubles, etc.), l'étiquetage doit faire apparaître des informations sur les matériaux employés, sur l'identification du fabricant ou encore le procédé de mise en œuvre.

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux ERP :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) ».

#### Vidéosurveillance

Les commerces de détail non alimentaire peuvent être équipés de systèmes permettant la vidéosurveillance des clients. L'exploitant devra dès lors indiquer leur présence afin de ne pas porter atteinte à la vie privée de ces derniers.

L'exploitant devra également déclarer son installation auprès de la préfecture du lieu de l'établissement et remplir le formulaire [Cerfa n° 13806](https://www.service-public.fr/particuliers/vosdroits/R13984), accompagné de tout document détaillant le système utilisé.

## 3°. Démarches et formalités d'installation

### a. Formalités de déclaration de l’entreprise

L’entrepreneur doit s’immatriculer au registre du commerce et des sociétés (RCS).

### b. Le cas échéant, demander une autorisation d'exploitation

L'intéressé doit demander une autorisation pour exploiter son commerce de détail non alimentaire et, le cas échéant, avant l'octroi du permis de construire dans les cas suivants :

- création d'un magasin de commerce de détail d'une surface de vente supérieure à 1 000 mètres carrés résultant soit d'une construction nouvelle, soit de la transformation d'un immeuble existant ;
- extension de la surface de vente d'un magasin de commerce de détail ayant déjà atteint le seuil des 1 000 mètres carrés ou devant le dépasser par la réalisation du projet ;
- tout changement de secteur d'activité d'un commerce d'une surface de vente supérieure à 1 000 mètres carrés lorsque l'activité nouvelle du magasin est alimentaire ;
- création d'un ensemble commercial et dont la surface de vente totale est supérieure à 1 000 mètres carrés ;
- extension de la surface de vente d'un ensemble commercial ayant déjà atteint le seuil des 1 000 mètres carrés ou devant le dépasser par la réalisation du projet ;
- réouverture au public, sur le même emplacement, d'un magasin de commerce de détail d'une surface de vente supérieure à 1 000 mètres carrés dont les locaux ont cessé d'être exploités pendant trois ans ;
- création ou extension d'un point permanent de retrait par la clientèle d'achats au détail commandés par voie télématique, organisé pour l'accès en automobile.

La demande est adressée en douze exemplaires à la commission départementale d'aménagement commercial (CDAC) de la préfecture du département où se situe le commerce qui dispose d'un délai de deux mois pour autoriser l'exploitation.

La demande est accompagnée de l'ensemble des pièces justificatives suivantes :

- un plan indicatif faisant apparaître la surface de vente des commerces ;
- les renseignements suivants :
  - la délimitation de la zone de chalandise du projet,
  - les transports collectifs et accès pédestres et cyclistes qui sont desservis autour du commerce ;
- une étude comportant les éléments permettant d'apprécier les effets du projet sur :
  - l'accessibilité de l'offre commerciale,
  - les flux de voitures particulières et de véhicules de livraison ainsi que sur les accès sécurisés à la voie publique,
  - la gestion de l'espace,
  - les consommations énergétiques et la pollution,
  - les paysages et les écosystèmes.

En cas de refus, la décision peut faire l'objet d'un recours devant la commission nationale d'aménagement commercial qui se prononcera dans un délai de quatre mois.

*Pour aller plus loin* : articles L. 752-1 et suivants du Code de commerce.