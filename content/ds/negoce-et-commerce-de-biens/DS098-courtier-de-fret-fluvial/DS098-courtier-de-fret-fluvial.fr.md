﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS098" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Courtier de fret fluvial" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="courtier-de-fret-fluvial" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/courtier-de-fret-fluvial.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="courtier-de-fret-fluvial" -->
<!-- var(translation)="None" -->

# Courtier de fret fluvial

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le courtier de fret fluvial est un professionnel, personne physique ou morale, mandaté pour mettre en rapport des donneurs d'ordre et des transporteurs publics de marchandises par bateau, en vue de la conclusion entre eux d'un contrat de transport. Le courtier de fret fluvial peut également être amené à surveiller la bonne exécution dudit contrat.

*Pour aller plus loin* : article L. 4441-1 du Code des transports.

### b. Centre de formalités des entreprises compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de la structure dans laquelle l’activité est exercée. Dans le cas du courtier de fret fluvial, il s’agit d’une activité commerciale. Le CFE compétent est donc la chambre de commerce et d’industrie (CCI).

**Bon à savoir**

Un commerçant est une personne qui accomplit des actes de commerce à titre habituel. Sont considérés comme des actes de commerce l’achat de biens en vue de leur revente, la location de meubles, les opérations de manufacture ou de transport, les activités d’intermédiaires de commerce (courtage et agence notamment), etc.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer en qualité de courtier de fret fluvial, l’intéressé doit être titulaire d’une attestation de capacité professionnelle. Cette attestation de capacité professionnelle permet l’inscription sur le registre des courtiers de fret fluvial.

L’attestation de capacité professionnelle est délivrée aux personnes qui, au choix :

- possèdent un diplôme de l’enseignement supérieur sanctionnant une formation juridique, économique, comptable, commerciale ou technique préparant à la gestion d'une entreprise ;
- possèdent un diplôme de l’enseignement supérieur sanctionnant une formation préparant aux activités de transport ;
- justifient de l’exercice, pendant au moins trois années consécutives, de fonctions de direction ou d’encadrement, à condition que ces fonctions n'aient pas pris fin depuis plus de trois ans à la date de la demande, au sein :
  - d’une entreprise de courtage de fret fluvial,
  - d’une entreprise de transport fluvial de marchandises,
  - d'une autre entreprise si l’activité exercée relève du domaine des transports.

Pour plus d’informations concernant l’obtention de l’attestation de capacité professionnelle, il est conseillé de se reporter infra. « 3°. b. Autorisations post-immatriculation ».

*Pour aller plus loin* : articles R. 4441-3 et R. 4441-4 du Code des transports.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### En cas de Libre Prestation de Services

Les ressortissants de l’Union européenne (UE) ou de l’Espace économique européen (EEE) légalement établis dans l’un de ces États et qui souhaitent exercer en France à titre temporaire et occasionnel sont dispensés de l'obligation d'inscription au registre des courtiers de fret fluvial.

*Pour aller plus loin* : article R. 4441-2 du Code des transports.

#### En cas de Libre Établissement

Les ressortissants de l’UE ou de l’EEE souhaitant exercer à titre permanent en France doivent répondre aux mêmes conditions que celles applicables aux Français. Ils doivent notamment disposer d’une attestation de capacité professionnelle et s’inscrire au registre des courtiers de fret fluvial (cf. infra « 2°. Conditions d’installation »).

*Pour aller plus loin* : article R. 4441-2 du Code des transports.

### c. Conditions d’honorabilité

Seules les personnes qui ne sont pas frappées d’une interdiction d’exercer une profession industrielle ou commerciale et qui ne sont pas inscrites sur le fichier national des interdits de gérer peuvent exercer légalement la profession de courtier de fret fluvial en France.

*Pour aller plus loin* : articles L. 4441-2 et R. 4441-5 du Code des transports.

### d. Quelques particularités de la réglementation de l’activité

#### Réglementation concernant l’établissement

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Le professionnel doit être immatriculé au registre du commerce et des sociétés (RCS).

### b. Autorisation(s) post-immatriculation

#### Demander l’attestation de capacité à l’exercice de la profession

##### Autorité compétente

La demande d’attestation est adressée à la direction régionale de l'environnement, de l'aménagement et du logement (Dreal) des Hauts-de-France.

##### Pièces justificatives obligatoires

Pour obtenir l’attestation de capacité professionnelle, l’intéressé doit composer un dossier composé des pièces suivantes

- la demande d’attestation de capacité professionnelle, rédigée sur papier libre (si l’intéressé se prévaut de son expérience à des fonctions de direction ou d’encadrement, il décrit de façon détaillée la nature et la durée d’exercice de ces fonctions ainsi que les motivations de sa demande) ;
- le formulaire Cerfa 10429*01 rempli, daté et signé ;
- une fiche individuelle d’état civil ;
- une enveloppe, format 26x32 cm, affranchie au tarif postal en vigueur (50 g) pour les envois en recommandé avec avis de réception ;
- un imprimé de dépôt de lettre recommandée avec avis de réception.

##### Pièces justificatives supplémentaires spécifiques aux personnes se prévalant d’un diplôme

Le dossier doit contenir :

- la photocopie certifiée conforme du diplôme ;
- un certificat de l’établissement scolaire ayant délivré le diplôme, attestant du suivi de 200 heures d’enseignement de la gestion dans la préparation du diplôme de l’enseignement supérieur.

##### Pièces justificatives supplémentaires spécifiques aux personnes se prévalant d’une expérience professionnelle

Le dossier doit contenir :

- si le demandeur est un salarié, les photocopies certifiées conformes du contrat de travail et des bulletins de salaire (bulletin des mois de janvier et de décembre de chacune des trois années de l’expérience acquise) permettant de déterminer la nature des fonctions et la durée pendant laquelle elles ont été exercées ;
- le cas échéant, un extrait de l’inscription au registre du commerce et des sociétés (extrait Kbis) de moins de trois mois ou le certificat d’inscription au registre tenu par la Chambre nationale de la batellerie artisanale (CNBA) ;
- le certificat d’affiliation à une caisse de retraite (pour les cadres ou pour les travailleurs non-salariés, selon la situation du demandeur) précisant la date de début d’affiliation ;
- le cas échéant, les photocopies de l’attestation des pouvoirs bancaires délivrée par la banque et des délégations de signature dont a pu disposer le demandeur, pour toute la durée de ses fonctions.

*Pour aller plus loin* : articles R. 4441-3 et R. 4441-4 du Code des transports.

#### Demander l’inscription au registre des courtiers de fret fluvial

Pour exercer légalement l’activité de courtier de fret fluvial en France, l’intéressé doit être inscrit au registre des courtiers de fret fluvial. Ce registre mentionne les nom et fonctions de l’intéressé.

##### Autorité compétente

La demande d’inscription doit être adressée au service « Transport et véhicule » de la Dreal des Hauts-de-France.

##### Délai de réponse

Après avoir adressé le dossier de demande d’inscription complet à la Dreal, celle-ci délivre un accusé de réception à l’intéressé et débute l’instruction de la demande. Une fois l’instruction terminée, la Dreal délivre, le cas échéant, une attestation de conformité à la réglementation de l’activité de courtier de fret fluvial. L’absence de réponse de la Dreal dans le délai de deux mois à compter de la délivrance de l'accusé de réception du dossier complet vaut accord.

##### Procédure

L'inscription au registre donne lieu à la délivrance d'un certificat d'inscription personnel et incessible qui permet au courtier d’effectuer toute opération de courtage sur le territoire métropolitain.

##### Pièces justificatives obligatoires

Pour obtenir son inscription au registre des courtiers de fret fluvial, le demandeur doit joindre à sa demande un dossier composé des éléments suivants :

- un formulaire Cerfa 10428 complété, daté et signé ;
- une copie d'une pièce d'identité de l'attestataire de capacité et du responsable légal de l'entreprise (carte nationale d'identité, passeport ou tout autre document équivalent pour les ressortissants de l'UE) ;
- une copie de l’attestation de capacité à l’exercice de la profession de courtier de fret fluvial de la personne qui assure la direction effective et permanente de l’entreprise ou, au sein de celle-ci, l’activité de courtier de fret fluvial.

De plus, dès la réception de l'attestation de conformité à la réglementation de l'activité de courtier de fret fluvial délivrée à l'issue de l'instruction du dossier par l'autorité compétente, l’intéressé doit :

- effectuer les formalités de déclaration d'entreprise auprès du CFE ;
- fournir l'original de l'extrait du registre du commerce et des sociétés (extrait Kbis) comportant la mention d’activité de courtier de fret fluvial ;
- fournir un relevé d’identification de l’entreprise délivré par l’Insee ;

Le certificat d'inscription sera délivré une fois ces formalités effectuées, sur présentation de l'extrait du registre.

##### Pièces justificatives supplémentaires spécifiques aux sociétés souhaitant s’inscrire au registre

Le dossier doit contenir :

- l'original de l'extrait du registre du commerce et des sociétés comportant la mention de l'activité de courtier de fret fluvial (peut être provisoirement remplacé par la fourniture du récépissé de la demande d'inscription au registre du commerce et des sociétés) ;
- une copie des attestations de l’Urssaf, des impôts directs, des impôts indirects constatant la situation de l’entreprise vis-à-vis de ces organismes ;
- l'original du certificat de non-faillite, liquidation ou règlement judiciaire s’il s’agit d’une entreprise déjà inscrite au registre du commerce pour une autre activité ;
- une copie des statuts, datés et signés par les associés, mentionnant dans la rubrique objet l'activité de courtier de fret fluvial et la nomination du ou des responsables légaux ou la copie du procès-verbal de l'assemblée ayant décidé de l'adjonction d'activité et/ou de la nomination des responsables légaux ;
- une copie de l'insertion de l’acte de constitution de la société dans le journal d'annonce légale.

##### Pièces justificatives supplémentaires spécifiques aux personnes titulaires de l’attestation de capacité professionnelle qui ne sont pas le responsable légal de l’entreprise

Le dossier doit contenir :

- une copie de l'attestation d'adhésion à une caisse de retraite des cadres au nom de l'attestataire et de l'entreprise ;
- une copie du contrat de travail signé par le responsable légal de l'entreprise et par l’attestataire, comportant une clause de rémunération, spécifiant que l’attestataire assurera la direction effective et permanente de l’activité de courtier de fret fluvial au sein de l’entreprise ;
- une copie du procès-verbal de l'assemblée générale le nommant.

**Bon à savoir**

L’administration en charge du dossier de demande d’inscription au registre des courtiers de fret fluvial porte au dossier un extrait du casier judiciaire du demandeur (bulletin n° 2), établi depuis moins de trois mois.

##### Coût

Gratuit.

##### Voie de recours

Deux types de recours sont possibles :

- un recours contentieux : ce recours est exercé devant le tribunal administratif territorialement compétent dans les deux mois à compter de la notification de la décision contestée ou dans les deux mois suivant la réponse expresse à un recours administratif préalable ;
- un recours administratif : ce recours s'adresse à l’autorité qui a pris la décision contestée (recours administratif gracieux) ou au supérieur hiérarchique de l'auteur de cette décision (recours administratif hiérarchique). Il doit être exercé dans le délai de deux mois suivant le recours contentieux qui se trouve alors prorogé de deux mois. Il est possible d’exercer un recours hiérarchique sans avoir préalablement engagé de recours gracieux ou sans attendre d'avoir reçu la réponse au recours gracieux.

*Pour aller plus loin* : arrêté du 25 mars 1997 relatif à la composition du dossier de demande d'inscription au registre des courtiers de fret fluvial ; articles R. 4441-6 et R. 4441-7 du Code des transports ; notice pour l’inscription au registre des courtiers de fret fluvial (Cerfa 50191*02).