﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS098" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Shipbroker (Inland waterways)" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="shipbroker-inland-waterways" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/shipbroker-inland-waterways.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="shipbroker-inland-waterways" -->
<!-- var(translation)="Auto" -->


Shipbroker (Inland waterways)
====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The river freight broker is a professional, individual or legal person, mandated to connect contractors and public carriers of goods by boat, with a view to concluding a transport contract between them. The river freight broker may also be required to monitor the smooth performance of the contract.

*To go further* Article L. 4441-1 of the Transportation Code.

### b. Competent Business Formality Centre

The relevant business formalities centre (CFE) depends on the nature of the structure in which the activity is carried out. In the case of the river freight broker, it is a commercial activity. The relevant CFE is therefore the Chamber of Commerce and Industry (CCI).

**Good to know**

A trader is a person who performs acts of commerce as usual. These are considered acts of trade to purchase goods for resale, furniture rentals, manufacturing or transport operations, the activities of intermediaries (including brokerage and agency), etc.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To practice as a river freight broker, the person must have a certificate of professional capacity. This certification of professional capacity allows registration on the register of river freight brokers.

The certificate of professional ability is issued to those who, at the choice:

- have a degree in higher education with legal, economic, accounting, commercial or technical training to run a business;
- have a higher education degree sanctioning training for transport activities;
- justify the exercise of management or management duties for at least three consecutive years, provided that these duties have not been terminated for more than three years at the date of the application, within:- a river freight brokerage company,
  - a river freight transport company,
  - another company if the activity is in the transport sector.

For more information regarding obtaining the certificate of professional ability, it is advisable to refer to below. "3°.b. Post-registration authorizations."

*To go further* Articles R. 4441-3 and R. 4441-4 of the Transportation Code.

### b. Professional Qualifications - European Nationals (LPS or LE)

#### In the case of free provision of services

Nationals of the European Union (EU) or the European Economic Area (EEA) legally established in one of these states who wish to practise in France on a temporary and casual basis are exempt from the registration requirement in the river freight brokers.

*To go further* Article R. 4441-2 of the Transportation Code.

#### In case of Freedom of establishment

EU or EEA nationals wishing to work permanently in France must meet the same conditions as those applicable to the French. In particular, they must have a certificate of professional capacity and register with the register of river freight brokers (see below: "2. Conditions of installation").

*To go further* Article R. 4441-2 of the Transportation Code.

### c. Conditions of honorability

Only persons who are not prohibited from practising an industrial or commercial profession and who are not registered on the national prohibited to manage file can legally practice the profession of river freight broker in France.

*To go further* Articles L. 4441-2 and R. 4441-5 of the Transportation Code.

### d. Some peculiarities of the regulation of the activity

#### Settlement regulations

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, it is advisable to refer to the listing " [Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The professional must be registered in the Register of Trade and Companies (RCS). For more information, please refer to the "Formality of Reporting of a Commercial Company" and "Registration of a Commercial Individual Company at the RCS" cards.

### b. Post-registration authorisation

#### Request certification of ability to practice the profession

**Competent authority**

The application for certification is addressed to the regional directorate of environment, development and housing (Dreal) of Hauts-de-France.

**Mandatory supporting documents**

In order to obtain the certificate of professional capacity, the person must compose a file consisting of the following documents

- The application for certification of professional capacity, written on free paper (if the person relies on his experience in management or management positions, he describes in detail the nature and duration of these duties as well as the motivations for his request);
- Form Cerfa 1042901 filled, dated and signed;
- An individual civil registration card
- an envelope, 26x32 cm in size, stamped at the current postal rate (50 g) for shipments in recommended with notice of receipt;
- a recommended letter filing with notice of receipt.

**Additional supporting documents specific to people taking a degree**

The folder should contain:

- Certified photocopying of the diploma
- a certificate from the school that issued the diploma, attesting to the follow-up of 200 hours of management instruction in the preparation of the higher education diploma.

**Additional supporting documents specific to people with work experience**

The folder should contain:

- If the applicant is an employee, the photocopies certified as compliant with the employment contract and the pay slips (January and December of each of the three years of the experience acquired) to determine the nature of the duties and the length of time they were exercised;
- if applicable, an extract from the registration in the register of trade and companies (extracted Kbis) of less than three months or the certificate of registration in the register held by the National Chamber of Craft Boating (CNBA);
- The certificate of affiliation with a pension fund (for executives or for non-salary workers, depending on the applicant's situation) specifying the start date of affiliation;
- if necessary, photocopies of the bank's certificate of banking powers and the signature delegations available to the applicant for the duration of his duties.

*To go further* Articles R. 4441-3 and R. 4441-4 of the Transportation Code.

#### Request registration in the river freight brokers' register

In order to legally operate as a river freight broker in France, the person concerned must be registered in the register of river freight brokers. This register mentions the name and functions of the person concerned.

**Competent authority**

The application must be sent to the "Transport and Vehicle" service of the Dreal des Hauts-de-France.

**Response time**

After having sent the application file to the Dreal, the Dreal issues an acknowledgement to the person concerned and begins the investigation of the application. Once the investigation is complete, Dreal issues, if necessary, a certificate of compliance with the regulations of the river freight broker business. The Dreal's failure to respond within two months of the issuance of the full record is worth agreement.

**Procedure**

Registration on the register results in the issuance of a personal and non-transferable registration certificate that allows the broker to carry out any brokerage transaction in the metropolitan area.

**Mandatory supporting documents**

To obtain registration in the river freight broker registry, applicants must attach a file consisting of:

- a Deer 10428 form completed, dated and signed;
- A copy of a piece of identification from the capacity certifiee and the company's legal officer (national identity card, passport or other equivalent document for EU nationals);
- a copy of the certificate of capacity to practice the profession of river freight broker of the person who provides the effective and permanent management of the company or, within it, the activity of river freight broker.

In addition, upon receipt of the certificate of compliance with the regulations of the river freight broker activity issued after the relevant authority has investigated the file, the person concerned must:

- Complete the formalities of reporting a business to the CFE;
- Provide the original extract from the trade and company register (extract Kbis) with the mention of river freight broker activity;
- Provide a company identification statement issued by INSEE;
The registration certificate will be issued once these formalities have been completed, upon presentation of the extract from the register.

**Additional supporting documents specific to companies wishing to register**

The folder should contain:

- the original extract from the trade and company register containing the reference to the activity of river freight broker (may be temporarily replaced by the provision of the receipt of the application for registration in the register of trade and companies);
- a copy of Urssaf's certificates, direct taxes, indirect taxes noting the company's situation vis-à-vis these organizations;
- The original certificate of non-bankruptcy, liquidation or legal settlement if it is a company already registered in the register of commerce for another activity;
- a copy of the statutes, dated and signed by the partners, mentioning in the subject section the activity of river freight broker and the appointment of the legal official or copy of the minutes of the meeting that decided on the addition of activity and/or the appointment of legal officials;
- a copy of the insertion of the company's incorporation into the legal advertising newspaper.

**Additional supporting documents specific to persons holding the certificate of professional capacity who are not the legal manager of the company**

The folder should contain:

- A copy of the certificate of membership in an executive pension fund on behalf of the certifiee and the company;
- a copy of the employment contract signed by the legal manager of the company and by the certifiee, including a compensation clause, specifying that the certifier will ensure the effective and permanent direction of the activity of river freight broker in the Within the company;
- a copy of the minutes of the general meeting naming him.

**Good to know**

The administration in charge of the application file for registration in the register of river freight brokers carries to the file an extract from the applicant's criminal record (bulletin 2), established less than three months ago.

**Cost**

Free.

**Remedy**

There are two types of remedies:

- Litigation: this appeal is exercised before the administrative court jurisdictionally within two months of notification of the contested decision or within two months of the express response to an administrative appeal Pre-pre-precondition
- administrative recourse: this appeal is directed at the authority that made the contested decision (ex gratia administrative recourse) or to the supervisor of the author of that decision (hierarchical administrative recourse). It must be exercised within two months of the litigation, which is then extended by two months. It is possible to exercise a hierarchical remedy without first initiating a graceful appeal or without waiting for the response to the graceful appeal.

*To go further* : decree of 25 March 1997 relating to the composition of the application file for registration in the register of river freight brokers; Articles R. 4441-6 and R. 4441-7 of the Transportation Code; registration in the register of river freight brokers (Cerfa 50191*02).

