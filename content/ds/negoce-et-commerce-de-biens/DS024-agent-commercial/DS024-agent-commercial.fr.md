﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS024" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Agent commercial" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="agent-commercial" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/agent-commercial.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agent-commercial" -->
<!-- var(translation)="None" -->

# Agent commercial

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'agent commercial est un professionnel mandaté et indépendant dont l'activité consiste à négocier et conclure des contrats pour le compte d'autrui (des entreprises, des commerçants, d'autres agents commerciaux etc.).

Ces contrats peuvent notamment porter sur :

- des prestations de services ;
- des ventes ;
- des achats ou locations.

*Pour aller plus loin* : article L. 134-1 du Code de commerce.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de l'activité exercée.

Quelle que soit la nature juridique de l'activité, le CFE compétent est le greffe du tribunal de commerce dans le ressort duquel le professionnel est domicilié ou le greffe du tribunal de grande instance pour les départements du Bas-Rhin, du Haut-Rhin et de la Moselle.

Toutefois, si le professionnel exerce une activité commerciale, le CFE compétent sera la chambre de commerce et d'industrie (CCI).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le professionnel souhaitant exercer l'activité d'agent commercial n'est soumis à aucune exigence de diplôme ou titre de formation.

Toutefois, avant d'exercer, il doit effectuer une demande d'immatriculation au registre spécial des agents commerciaux (RSAC) (cf. infra « 3°. b. Immatriculation au registre spécial des agents commerciaux »).

*Pour aller plus loin* : article L. 134-6 du Code de commerce.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement)

**En vue d'une Libre Prestation de Services (LPS)**

Le ressortissant d'un pays membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) légalement établi qui exerce l'activité d'agent commercial peut exercer de manière temporaire et occasionnelle en France la même activité.

Le ressortissant est soumis aux mêmes exigences que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation »). Cependant, l'intéressé est dispensé de l'obligation d'immatriculation au RSAC.

*Pour aller plus loin* : alinéa 8 de l'article R. 134-6 du Code de commerce.

**En vue d'un Libre Établissement (LE)**

Aucune disposition réglementaire n'est prévue pour le ressortissant de l'UE en vue d'exercer en France l'activité d'agent commercial à titre permanent.

À ce titre, le ressortissant est soumis aux mêmes exigences que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation »).

### c. Conditions d’honorabilité et sanctions pénales

**Conditions d'honorabilité**

Le professionnel qui souhaite exercer l'activité d'agent commercial ne doit avoir fait l'objet :

- ni d'une condamnation pénale ou interdiction de diriger, gérer ou administrer une entreprise commerciale ou artisanale ou une personne morale ;
- ni d'une peine complémentaire d'interdiction d'exercer une activité professionnelle.

*Pour aller plus loin* : article 131-6 du Code pénal.

**Obligations**

Le professionnel est soumis à une obligation de loyauté et au devoir d'information envers le mandant.

À ce titre, il doit lui communiquer toutes les informations nécessaires à l'exécution du contrat. En outre, le mandant est tenu aux mêmes obligations et doit remettre à l'agent commercial toute la documentation portant sur les éléments du contrat.

*Pour aller plus loin* : articles L. 134-4, R. 134-1 et suivants du Code de commerce.

**Sanctions pénales**

Le professionnel encourt une amende d'un montant maximal de 3 000 euros dès lors qu'il exerce son activité d'agent commercial sans avoir :

- procédé à son immatriculation au RSAC ;
- informé l'autorité de tout changement des informations figurant sur la déclaration d'immatriculation ;
- demandé sa radiation dès lors qu'il n'exerce plus son activité ou ne remplit plus les conditions nécessaires à l'exercice de la profession.

Il encourt également une amende dès lors que les informations fournies en vue de son immatriculation sont inexactes ou incomplètes.

*Pour aller plus loin* : articles R. 134-14 et suivants du Code de commerce.

### d. Quelques particularités de la réglementation de l’activité

**Statut de l'agent commercial**

L'agent commercial exerce son activité à titre indépendant et peut accepter de représenter d'autres mandants. Toutefois, il est soumis à une obligation de non-concurrence et doit obtenir l'autorisation du mandant avant de représenter une entreprise concurrente.

*Pour aller plus loin* : article L. 134-3 du Code de commerce.

**Contrat d'agent commercial**

Le contrat conclu entre l'agent commercial et le mandant peut être à durée déterminée ou indéterminée. Si l'exécution du contrat se poursuit après l'arrivée de son terme, le contrat à durée déterminée est réputé être un contrat à durée indéterminée.

En outre, tout contrat à durée indéterminée peut être rompu par les parties dès lors qu'elles respectent un préavis :

- d'un mois pour la première année du contrat ;
- de deux mois pour la deuxième année du contrat ;
- de trois mois à compter de la troisième année du contrat.

**À noter**

Les parties peuvent prévoir des délais de préavis plus longs uniquement si le délai prévu pour le mandant n'est pas plus court que celui de l'agent commercial.

De plus, le mandant doit remettre à l'agent commercial un relevé mentionnant l'ensemble des commissions dues ainsi que la base ayant permis de calculer leur montant.

*Pour aller plus loin* : articles L. 134-11 et R. 134-3 du Code de commerce.

## 3°. Démarches et formalités d’installation

### a. Immatriculation au registre du commerce et des sociétés (RCS)

Le professionnel souhaitant créer une société commerciale doit procéder à son immatriculation au RCS dont les formalités dépendent du CFE compétent :

- lorsque le CFE compétent est la chambre de commerce et d'industrie, une fois l'immatriculation au RCS effectuée, le professionnel doit effectuer une demande d'immatriculation au registre spécial des agents commerciaux (cf. infra « 3°. b. Immatriculation au registre spécial des agents commerciaux (RSAC) ») ;
- lorsque le CFE compétent est le greffe du tribunal de commerce, ce dernier procédera à l'immatriculation au RSAC et au RCS.

### b. Immatriculation au registre spécial des agents commerciaux (RSAC)

**Autorité compétente**

En vue de son immatriculation au RSAC, l'agent commercial doit déposer une déclaration en double exemplaire auprès du greffier du tribunal de commerce.

**Pièces justificatives**

La déclaration doit contenir :

- un exemplaire du contrat signé avec le mandant mentionnant le contenu du contrat d'agence ou, à défaut, un justificatif de l'existence d'un tel contrat traduit en français ;
- un justificatif d'identité de la personne physique en cours de validité ou, le cas échéant, un justificatif d'identification ainsi qu'un extrait du RCS de la personne morale immatriculée ;
- un bulletin ou une demande d'affiliation à une caisse d'assurance vieillesse de non-salariés et un bulletin ou une demande d'affiliation à une caisse d'allocations familiales ;
- si le demandeur est une personne physique :
  - une déclaration d'insaisissabilité (cf. article L. 526-1 du Code de commerce),
  - tout document justifiant que le conjoint est informé des conséquences des éventuelles dettes sur leurs biens communs,
  - le cas échéant, l'identité du conjoint dès lors qu'il est collaborateur, associé ou salarié.

**Issue de la procédure**

Dès le dépôt de la demande, le greffier en informe le juge en charge du RCS afin qu'il demande le bulletin n° 2 du casier judiciaire. Dès réception de ce bulletin, un numéro d'immatriculation sera attribué au demandeur ainsi qu'un exemplaire de sa déclaration.

**À noter**

Cette démarche peut être accomplie par voie électronique.

*Pour aller plus loin* : articles A. 134-1 et suivants du Code de commerce ; annexe 1.1 du Code de commerce.

### c. Le cas échéant, enregistrer les statuts de la société

Une fois les statuts de la société datés et signés, le professionnel doit procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

**Autorité compétente**

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

**Pièces justificatives**

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.