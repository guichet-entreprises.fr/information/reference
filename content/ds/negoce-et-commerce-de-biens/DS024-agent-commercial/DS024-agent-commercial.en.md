﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS024" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Commercial agent" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="commercial-agent" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/commercial-agent.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="commercial-agent" -->
<!-- var(translation)="Auto" -->


Commercial agent
===========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The sales agent is a mandated and independent professional whose business consists of negotiating and entering into contracts on behalf of others (companies, traders, other commercial agents, etc.).

These contracts may include:

- Service delivery
- Sales
- purchases or rentals.

*For further information*: Article L. 134-1 of the Code of Commerce.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the activity.

Whatever the legal nature of the activity, the competent CFE is the registry of the commercial court in which the professional is domiciled or the registry of the high court for the departments of Lower Rhine, Upper Rhine and Moselle.

However, if the professional is engaged in commercial activity, the relevant CFE will be the Chamber of Commerce and Industry (CCI).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional wishing to work as a sales agent is not subject to any diploma requirement or training title.

However, before practising, he must apply for registration in the Special Register of Commercial Agents (RSAC) (see infra "3 degrees). b. Registration in the Special Register of Commercial Agents").

*For further information*: Article L. 134-6 of the Code of Commerce.

### b. Professional Qualifications - European Nationals (Free Service Or Freedom of establishment)

**For a Freedom to provide services**

A national of a Member Of The European Union (EU) or a party to the legally established European Economic Area (EEA) agreement who acts as a commercial agent may perform the same activity temporarily and occasionally in France.

The national is subject to the same requirements as the French national (see infra "3°. Installation procedures and formalities"). However, the person concerned is exempt from the registration requirement at the RSAC.

*For further information*: paragraph 8 of Article R. 134-6 of the Code of Commerce.

**In View to a Freedom of establishment**

There are no regulations for the EU national to carry out the activity of a permanent sales agent in France.

As such, the national is subject to the same requirements as the French national (see infra "3°. Installation procedures and formalities").

### c. Conditions of honourability and criminal penalties

**Conditions of honorability**

The professional who wishes to work as a sales agent must not have been subject to:

- nor a criminal conviction or prohibition on running, managing or administering a commercial or craft business or a legal person;
- or a supplementary penalty for prohibiting the exercise of any professional activity.

*For further information*: Article 131-6 of the Penal Code.

**Obligations**

The professional is subject to a duty of loyalty and a duty of information to the principal.

As such, he must provide him with all the information necessary to carry out the contract. In addition, the principal is bound by the same obligations and must provide the sales agent with all documentation relating to the elements of the contract.

*For further information*: Articles L. 134-4, R. 134-1 and the following of the Code of Commerce.

**Criminal sanctions**

The professional faces a fine of up to 3,000 euros if he works as a sales agent without having:

- registered with the RSAC;
- informed the authority of any changes to the information on the registration declaration;
- requested that he be removed if he no longer practises his activity or fulfils the necessary conditions for the practice of the profession.

He also faces a fine if the information provided for his registration is inaccurate or incomplete.

*For further information*: Articles R. 134-14 and the following of the Code of Commerce.

### d. Some peculiarities of the regulation of the activity

**Status of sales agent**

The sales agent operates independently and may agree to represent other constituents. However, it is subject to a non-competition obligation and must obtain the principal's authorization before representing a competing company.

*For further information*: Article L. 134-3 of the Code of Commerce.

**Commercial agent contract**

The contract between the sales agent and the principal may be fixed-term or indeterminate. If the contract continues to be executed after the end of its term, the fixed-term contract is deemed to be an indeterminate contract.

In addition, any indeterminate contract may be terminated by the parties if they comply with notice:

- one month for the first year of the contract
- two months for the second year of the contract;
- three months from the third year of the contract.

**Please note**

Parties may provide longer notice periods only if the time limit for the principal is not shorter than that of the sales agent.

In addition, the principal must provide the sales agent with a statement that mentions all the commissions owed and the basis for calculating their amount.

*For further information*: Articles L. 134-11 and R. 134-3 of the Code of Commerce.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Register of Trade and Companies (RCS)

The professional wishing to set up a commercial company must register with the RCS, the formalities of which depend on the relevant CFE:

- where the relevant CFE is the Chamber of Commerce and Industry, once registration with the RCS has been completed, the professional must apply for registration in the Special Register of Commercial Agents (see infra "3°. b. Registration in the Special Register of Commercial Agents (RSAC);
- where the relevant CFE is the registry of the Commercial Court, the commercial court will register with the RSAC and the RCS.

For more information, please refer to the "Company Reporting Formalities" sheet.

### b. Registration in the Special Register of Commercial Agents (RSAC)

**Competent authority**

For registration to the RSAC, the sales officer must file a duplicate statement with the Clerk of the Commercial Court.

**Supporting documents**

The statement should include:

- A copy of the contract signed with the principal mentioning the contents of the agency contract or, failing that, proof of the existence of such a contract translated into French;
- a valid proof of identity of the natural person or, if necessary, a proof of identification as well as an extract from the registered corporation's RCS;
- a newsletter or application for membership in a non-salary old-age insurance fund and a newsletter or application for affiliation with a family allowance fund;
- if the applicant is a natural person:- a declaration of inseability (see Article L. 526-1 of the Code of Commerce),
  - any document justifying that the spouse is informed of the consequences of any debts on their common property,
  - if necessary, the identity of the spouse as long as he is an employee, partner or employee.

**Outcome of the procedure**

Upon filing the application, the Clerk informs the judge in charge of the RCS to request bulletin 2 of the criminal record. Upon receipt of this ballot, the applicant will be assigned a registration number and a copy of his or her return.

**Please note**

This can be done electronically.

*For further information*: Articles A. 134-1 and the following of the Code of Commerce; Appendix 1.1 of the Code of Commerce.

### c. If necessary, register the company's statutes

Once the company's statutes have been dated and signed, the professional must register them with the Corporate Tax Office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

