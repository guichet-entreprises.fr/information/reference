﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS071" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Wine and spirit broker" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="wine-and-spirit-broker" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/wine-and-spirit-broker.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="wine-and-spirit-broker" -->
<!-- var(translation)="Auto" -->


Wine and spirit broker
=======================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The wine and spirits broker known as the "campaign broker" is a professional intermediary whose activity consists of connecting the various players in wine and wine production.

In this capacity, it is responsible for researching, informing and ensuring the proper execution of the transaction between producers or sellers of grapes, musts, wines and spirits and operators (traders, winegrowers and cooperatives).

*For further information*: Articles 1 and 5 of Act 49-1652 of December 31, 1949 regulating the profession of wine broker known as a "campaign broker."

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. As the activity of a wine and spirits broker is commercial in nature, the competent CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To be a wine and spirits broker, the professional must:

- enjoying his civil rights;
- Be a French national or a regular on French territory;
- hold a business card.

*For further information*: Article 2 of the aforementioned Act of 31 December 1949.

**Good to know**

Ordinance No. 2015-1682 of 17 December 2015 simplifying certain pre-authorization and reporting regimes for companies and professionals, which came into force on 1 January 2016, envisaged replacing the business card with a registered in the National Register of Wine Brokers. However, the implementing texts relating to the implementation of this declaration are currently under discussion.

*For further information*: Article 3 of Ordinance No. 2015-1682 of December 17, 2015 simplifying certain pre-authorization and reporting regimes for companies and professionals; Article 3 of the aforementioned Act of 31 December 1949.

#### Do an internship with a wine broker

Prior to applying for a business card, the applicant must do an internship with a professional.

This six-month internship aims to enable the professional to acquire the knowledge of:

- aspects of the practice of the profession of wine and spirits broker;
- general concepts of the wine and wine industry, as well as the inter-professional agreements in force in the region in which the professional is doing his internship.

*For further information*: Article 2 of Decree No. 2007-222 of February 19, 2007 relating to the practice of the profession of wine and spirits broker.

#### Get a business card

In order to practice, the person concerned must obtain a business card and must:

- request (see infra "3 degrees). a. Request for the business card");
- submit to a panel of judges presided over by a consular judge and composed of:- an winemaker,
  - a representative of the wine and spirits broker profession,
  - president of the territorial Chamber of Commerce and Industry (CCI).

The purpose of this review is to assess the candidate's knowledge and professional experience in order to practice as a wine and spirits broker, and focuses on:

- assessing his oenological knowledge
- his ability to taste;
- his knowledge of commercial law and brokerage contracts.

When the applicant has passed this examination, the ICC President issues his professional card within one month of the review board's decision.

*For further information*: Article 2 of Decree 51-372 of March 27, 1951 regulating public administration for the application of Law 49-1652 of December 31, 1949 regulating the profession of wine broker known as a "campaign broker."

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

**In case of temporary and occasional exercise (LPS)**

There is no provision for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement for a temporary and occasional exercise in France.

As such, the national is subject to the same requirements as the French national (see infra "3°. Installation procedures and formalities").

**In case of permanent exercise (LE)**

Any legally established EU or EEA national who operates as a wine and spirits broker may carry out the same activity in France on a permanent basis.

In order to do so, the person concerned must:

- meet the following conditions:- not be prohibited from practising a commercial or industrial profession,
  - not engage in an activity incompatible with the practice of the wine and spirits broker,
  - Be legally established within the Member State
- justify either:- to have held the business for two years in a row,
  - to hold a certificate of competency or a training certificate issued by the Member State that regulates access or practice to the profession,
  - Where the state does not regulate the activity, to hold a certificate of competency or a training document justifying that it has been prepared for the exercise of this activity,
  - to hold a diploma, title or certificate to carry out the activity of wine and spirits broker and issued by a third state and recognized by a Member State, and have carried out this activity for at least three years in that state Member.

Once the national fulfils these conditions, he must apply for the issuance of a business card (see infra "3°. a. Request for the business card").

*For further information*: Article 2-2 of Decree 51-372 of March 27, 1951, regulating public administration for the application of Law 49-1652 of December 31, 1949 regulating the profession of wine broker known as a "campaign broker."

### c. Conditions of honorability

To practice, the professional does not have to:

- prohibited from practising a commercial or industrial profession or directing, administering, managing or controlling a commercial or industrial enterprise;
- purchase or sell wines and spirits on his own account, except for family or wine needs from his property;
- have a licence as a wholesale or retail wine and spirits dealer.

*For further information*: Article 2 of Act 49-1652 of December 31, 1949 regulating the profession of wine broker known as a "campaign broker."

**Incompatibilities**

Not to be a wine and spirits broker:

- civil servants and agents of the state or local authorities;
- employees of social security funds and family allowances;
- board members, directors, managers and employees of cooperative winery or cooperative winery unions or winery unions;
- Members of the boards of directors of agricultural credit unions;
- employees of wine merchants;
- winemakers or chemists;
- forwarders, stockers or acconiers;
- beverages, restaurateurs and hoteliers;
- directors, employees or employees of newspapers relating to viticulture and the wine and spirits trade.

*For further information*: Article 1 of Decree 51-372 of March 27, 1951 regulating public administration for the application of Law 49-1652 of December 31, 1949 regulating the profession of wine broker known as a "campaign broker."

### d. Some peculiarities of the regulation of the activity

**Mandatory mentions in the event of a sales agreement**

Once the transaction is made between the producer and the trader, the broker is required to place the following statements on the sale confirmation:

- identity (birth name, first name, if any, use name);
- The address of his home
- The order number of his business card
- the address of the company's head office on behalf of which it operates.

**Please note**

Brokerage compensation will be due from this agreement.

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all[public-receiving establishments](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

3°. Installation procedures and formalities
------------------------------------------------------

### a. Request for business card

**Competent authority**

The professional must submit his request to the President of the ICC of the region in which he wishes to practice.

**Supporting documents**

His application must include:

- if it is registered in the Register of Trade and Companies (RCS) or if it is mentioned as an officer or partner of a company, a registration extract dating back less than three months or, failing that, a document attesting to the honour that it is not the subject of no ban on practising a commercial profession;
- a certificate justifying that he has completed an internship with a professional broker (see supra "2.0). Internship with a wine broker"). This document is not required for the EU national;
- for the EU national, a document justifying that he has been a wine and spirits broker for two consecutive years;
- Failing that, a certificate of competency or a training certificate issued by a Member State justifying its preparation for the practice of the profession;
- A copy of his ID card
- two recent identity photographs.

**Time and procedure**

Once the aptitude exam has been validated, the ICC President issues his professional card. The professional is then placed on the list of wine and spirits brokers established by the CCI network.

**Please note**

If the applicant is an EU national, his card is issued by the ICC President within two months of receiving his full application.

This card signed by the President of the ICC mentions:

- The identity of the professional, his date and place of birth, his address;
- if applicable, its registration number to the RCS and, in the case of registration as a legal person, its name and the address of the company's headquarters;
- The identification of the ICC that issued its card;
- Order number and date of issue.

*For further information*: Decree No. 2007-222 of February 19, 2007 relating to the practice of the profession of wine and spirits broker; Order of 19 February 2007 relating to the practice of the profession of wine and spirits broker; Articles 1 and 2 of the Order of 19 February 2007 relating to the practice of the profession of wine and spirits broker.

### b. Business registration in the Register of Trade and Companies (RCS)

**Competent authority**

The trader must report his business, and for this must, make a declaration with the Chamber of Commerce and Industry (CCI).

**Supporting documents**

The person concerned must provide the[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Timeframe**

The CCI's Business Formalities Centre sends a receipt to the professional on the same day listing the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Article L. 123-1 of the Code of Commerce; Section 635 of the General Tax Code.

