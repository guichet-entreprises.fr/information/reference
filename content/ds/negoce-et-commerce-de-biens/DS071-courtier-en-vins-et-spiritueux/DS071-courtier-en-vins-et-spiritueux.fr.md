﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS071" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Courtier en vins et spiritueux" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="courtier-en-vins-et-spiritueux" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/courtier-en-vins-et-spiritueux.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="courtier-en-vins-et-spiritueux" -->
<!-- var(translation)="None" -->

# Courtier en vins et spiritueux

Dernière mise à jour : <!-- begin-var(last-update) -->2020-12<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le courtier en vins et spiritueux dit courtier « de campagne » est un intermédiaire professionnel dont l’activité consiste à mettre en relation, les différents acteurs de la production viticole et vinicole.

À ce titre, il est chargé de rechercher, d’informer et de veiller à la bonne exécution de la transaction entre d’une part, les producteurs ou les vendeurs de raisins, de moûts, de vins et de spiritueux, et d’autre part, les opérateurs (négociants, viticulteurs et coopératives).

*Pour aller plus loin* : articles 1 et 5 de la loi n° 49-1652 du 31 décembre 1949 réglementant la profession de courtier en vins dite de « courtier de campagne ». 

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée. L’activité de courtier en vins et spiritueux étant de nature commerciale, le CFE compétent est la chambre de commerce et d’industrie (CCI). 

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). 

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l’activité de courtier en vins et spiritueux, le professionnel doit :

- jouir de ses droits civils ;
- être de nationalité française ou en situation régulière sur le territoire français ;
- être titulaire d’une carte professionnelle ;
- satisfaire à au moins une des trois conditions suivantes :
  - avoir obtenu un diplôme sanctionnant une formation dans la filière viti-vinicole,
  - avoir  accompli un stage d’au moins six mois auprès d’un professionnel de la filière viti-vinicole (ensemble d’activités comprenant la culture de la vigne et l’élaboration des vins et spiritueux),
  - justifier d’une expérience professionnelle d’au moins six mois sur le territoire d’un État membre de l’Union européenne ou d’un État partie à l’accord sur l’Espace économique européen en qualité de travailleur indépendant ou de salarié dans la filière viti-vinicole.

*Pour aller plus loin* : article 1er du décret n° 2020-1254 du 13 octobre 2020 relatif à l’accès et à l’exercice de la profession de courtier en vins et spiritueux.

#### Valider un examen permettant au jury d’apprécier les connaissances et aptitudes professionnelles des candidats pour exercer la profession de courtier en vins et spiritueux

Cet examen a pour objectif d’évaluer les connaissances et l’expérience professionnelle du candidat en vue d’exercer la profession de courtier en vins et spiritueux, et porte sur l’ensemble des connaissances nécessaires à l’exercice de l’activité. Les domaines de connaissance dans lesquels le candidat peut être interrogé sont détaillés à l’annexe du décret n° 2020-1254 du 13 octobre 2020. Ces connaissances sont notamment administratives et juridiques, commerciales et de négociation et relatives à la viticulture et à la connaissance du vin

L’examen comprend un exposé oral au cours duquel le candidat présente ses connaissances ou expériences professionnelles et un entretien conduit par les membres du jury portant sur les connaissances du candidat.

*Pour aller plus loin* : article 2 de la loi du 31 décembre 1949 ; article 1er et annexe du décret n° 2020-1254 du 13 octobre 2020 relatif à l’accès et à l’exercice de la profession de courtier en vins et spiritueux.

#### Être inscrit au Registre national des courtiers en vin

En application de la loi n° 49-1652 du 31 décembre 1949 modifiée réglementant la profession de courtiers en vins dits « courtiers de campagne » et du décret n° 2020-1253 du 13 octobre 2020, les courtiers en vin doivent obligatoirement être inscrits au Registre national des courtiers en vins et spiritueux dont la gestion est confiée à CCI-France.

L’inscription est de droit dès lors que les conditions posées à l’article 2 de la loi du 31 décembre 1949 et présentées ci-dessus sont remplies. Elle s’effectue au moyen d’une déclaration à adresser (par lettre recommandée ou par voie électronique) au président de CCI-France.

### b. Qualifications professionnelles – Ressortissants européens (Libre Prestation de Services (LPS) ou Libre Établissement (LE))

#### En cas d’exercice temporaire et occasionnel (LPS)

Les personnes légalement établies dans un autre État membre de l’UE ou de l’EEE qui exercent l’activité de courtier en vin peuvent exercer cette activité de façon temporaire et occasionnelle en France sans avoir à demander au préalable leur inscription au Registre national des courtiers en vins. Leurs prestations sont, dans ce cas, délivrées sous la dénomination habituellement utilisée dans l’État d’établissement. 

*Pour aller plus loin* : article 1er du décret n° 2020-1254 du 13 octobre 2020 relatif à l’accès et à l’exercice de la profession de courtier en vins et spiritueux..

#### En cas d’exercice permanent (LE)

Tout ressortissant de l’UE ou de l’EEE légalement établi et exerçant l’activité de courtier en vins et spiritueux peut exercer en France, à titre permanent, la même activité.

Pour cela, l’intéressé doit remplir les conditions suivantes :

- ne faire l’objet d’aucune interdiction d’exercer une profession commerciale ou industrielle ;
- ne pas exercer une activité incompatible avec l’exercice de la profession de courtier en vins et spiritueux ;
- être légalement établi au sein de l’État membre ;
- satisfaire aux mêmes conditions que celles qui sont exposées plus haut, à la rubrique « 2°. Conditions d’installation ».

Toutefois, les ressortissants d’un État membre de l’Union européenne ou de l’Espace économique européen peuvent être dispensés de l’examen sous réserve de satisfaire aux exigences permettant d’exercer l’activité de courtier en vins et spiritueux dans des conditions équivalentes à celles requises en France. La demande de dispense doit être adressée au président de la chambre de commerce et d’industrie dans le ressort territorial de laquelle le professionnel souhaite exercer.

Dès lors qu’il remplit ces conditions, le ressortissant doit adresser (par lettre recommandée ou par voie électronique) une déclaration au président de CCI-France pour être inscrit au Registre national des courtiers en vins et régler une redevance représentative des frais de traitement du dossier (300 euros). 

*Pour aller plus loin* : articles 1 et 2 du décret n° 2020-1253 du 13 octobre 2020 relatif au Registre national des courtiers en vins et spiritueux.

### c. Conditions d’honorabilité

Pour exercer, le professionnel ne doit pas :

- faire l’objet d’une interdiction d’exercer une profession commerciale ou industrielle ou de diriger, d’administrer, de gérer ou de contrôler une entreprise commerciale ou industrielle ;
- effectuer d’achat ou de vente de vins et spiritueux à son compte, excepté pour des besoins familiaux ou de vins provenant de sa propriété ;
- être titulaire d’une licence de marchand de vins et spiritueux en gros ou de détail.

*Pour aller plus loin* : article 2 de la loi n° 49-1652 du 31 décembre 1949 réglementant la profession de courtier en vins dite de « courtier de campagne ».

#### Incompatibilités

Ne peuvent exercer l’activité de courtier en vins et spiritueux :

- les professionnels de l’achat ou vente de vin ou spiritueux en gros ou en détail,
- les négociants en vin et leurs employés,
- les dirigeants et les salariés des caves coopératives, 
- les vinificateurs et œnologues prestataires de services,
- les transitaires, transporteurs et leurs manutentionnaires,
- les dirigeants et les salariés d’organismes dont l’activité est principalement consacrée à l’examen des questions relatives à la viticulture et au commerce des vins et spiritueux.

*Pour aller plus loin* : article 5 du décret n° 2020-1253 du 13 octobre 2020 relatif au Registre national des courtiers en vins et spiritueux.

### d. Quelques particularités de la réglementation de l’activité

#### Mentions obligatoires en cas d’accord de vente

Dès lors que la transaction est effectuée entre le producteur et le négociant, le courtier est tenu d’apposer sur la confirmation de vente les mentions suivantes :

- son identité (nom de naissance, prénom(s), le cas échéant son nom d’usage) ;
- l’adresse de son domicile ;
- le numéro d’ordre de sa carte professionnelle ;
- l’adresse du siège social de la société pour le compte de laquelle il exerce. 

**À noter**

La rémunération de courtage sera due à compter de cet accord.

#### Respect des normes de sécurité et d’accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s’assurer du respect des normes de sécurité et d’accessibilité applicables à l’ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d’incendie et d’accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d’incendie et de panique dans les établissements recevant du public (ERP).

## 3°. Démarches et formalités d’installation

### a. Demande en vue d’obtenir l’inscription au Registre national des courtiers en vins et spiritueux

#### Autorité compétente

Le professionnel doit adresser la déclaration au président de CCI-France. 

#### Pièces justificatives

Le dossier de déclaration doit comporter :

- une photo recto-verso de la carte d’identité ;
- une attestation d’employeur datant de moins de trois mois pour les salariés ;
- la déclaration dont le modèle figure à  l’annexe de l’arrêté du 13 octobre 2020 fixant les modalités d’inscription au Registre national des courtiers en vins et spiritueux. Cette déclaration contient les informations suivantes :
  - nom et prénom de l’intéressé ainsi que sa nationalité,
  - date et lieu de naissance,
  - adresse postale, adresse électronique et téléphone,
  - nom commercial et forme juridique de l’entreprise,
  - adresse de l’entreprise.

Les titulaires d’une carte professionnelle délivrée avant l’entrée en vigueur de l’ordonnance du 17 décembre 2015 susvisée joignent à leur demande leur carte professionnelle, ou la preuve par tout moyen qu’ils en sont bien titulaires.

Les documents précités sont rédigés en langue française ou accompagnés de leur traduction en français établie par un traducteur assermenté.

L’envoi du dossier est accompagné du paiement de la redevance afférente à l’inscription (300 euros). 

#### Délai et procédure

Une fois l’examen d’aptitude validé, le président de la CCI inscrit le professionnel au Registre national des courtiers en vins et spiritueux. 

*Pour aller plus loin* : décret n° 2020-1253 du 13 octobre 2020 relatif au Registre national des courtiers en vins et spiritueux ; arrêté du 13 octobre 2020 fixant les modalités d’inscription au Registre national des courtiers en vins et spiritueux. 

### b. Immatriculation de l’entreprise au registre du commerce et des sociétés (RCS)

#### Autorité compétente

Le commerçant doit procéder à la déclaration de son entreprise. Il doit pour cela effectuer une déclaration auprès de la chambre de commerce et d’industrie (CCI).

#### Pièces justificatives

L’intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI adresse le jour même un récépissé au professionnel listant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d’un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu’il n’a pas été transmis durant les délais ci-dessus.

Si le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d’un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article L. 123-1 du Code de commerce ; article 635 du Code général des impôts.