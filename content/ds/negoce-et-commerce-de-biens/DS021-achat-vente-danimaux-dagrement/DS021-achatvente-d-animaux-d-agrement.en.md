﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS021" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Sale and purchase of pets and domesticated animals" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="sale-and-purchase-of-pets-and-domesticated-animals" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/sale-and-purchase-of-pets-and-domesticated-animals.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="sale-and-purchase-of-pets-and-domesticated-animals" -->
<!-- var(translation)="Auto" -->


Sale and purchase of pets and domesticated animals
=================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The business of buying or selling pets is for the professional, to buy and offer to individuals or professionals, the sale of domestic and non-domestic animals.

Pets are considered pets, those intended to be held by man for his enjoyment. By definition, non-domestic animals are those that do not fall into this category.

*For further information*: Article L. 214-6 of the Rural and Marine Fisheries Code and L. 413-2 of the Environment Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. As the activity is commercial in nature, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional carries out a buying and resale activity his activity will be both commercial and artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To engage in the business of buying or selling pets or amenity, the professional must:

- registered with the Register of Trade and Companies (RCS) (see infra "3°. a. REGISTRATION to the RCS");
- Be professionally qualified
- operate in premises in accordance with the health provisions (see infra "2. (d.)
- In case of pet-related activity, make a declaration to the prefect (see below "3 degrees). b. Declaration of activity in relation to pets");
- In case of activity related to non-domestic animals, ask for permission to open its establishment (see infra "3. c. Request for permission to open a non-domestic animal facility").

*For further information*: Article L. 214-6 of the Rural Code and Marine Fisheries; Section L. 413-2 of the Environment Code.

**If you buy/sell with pets**

In order to engage in a business of buying or selling pets of domestic species, the professional must be the holder of either:

- one of the professional certifications mentioned in the[Appendix II](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E4347A807573D4D41A0A98B2B4ED58E7.tplgfr30s_1?idArticle=LEGIARTI000032095076&cidTexte=LEGITEXT000032095054&dateTexte=20180301) (4 February 2016) order on training and updating the necessary knowledge of persons engaged in activities related to domestic pets and the authorisation of training bodies;
- a certificate of knowledge relating to biological, physiological, behavioural and pet maintenance needs issued by an institution approved by the Minister for Agriculture;
- a Certificate of Capacity of Domestic Species Animals (CCAD) issued before January 1, 2016.

To find out how to access these courses, it is advisable to refer to the "Pet Maintenance of Domestic Species" sheet.

*For further information*: Article L. 214-6 of the Rural Code and Marine Fisheries.

**In case of purchase/sale of non-domestic animals**

To carry out this activity, the professional must hold a certificate of ability.

To do so, he must apply to the prefect of the department of his home (see infra "3°. d. Application for a certificate of capacity for non-domestic animals").

*For further information*: Article L. 413-2 of the Environment Code.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

**For temporary and casual exercise (LPS)**

Any national of a Member State of the European Union (EU) or a State party to the agreement on the European Economic Area (EEA) legally established and engaged in activity of buying or selling pets or pleasure, may, as a matter of temporary and casual activities in France.

In order to do so, it must make a prior declaration, the terms of which vary depending on the nature of its activity.

To find out the terms of this declaration, it is advisable to refer to the "Pet Maintenance of Domestic Species" and "Responsible for the establishment of non-domestic, sale or rental animals, presentation to the public of living specimens of French or foreign wildlife."

**For a permanent exercise (LE)**

There is no provision for EU nationals wishing to engage in the purchase or sale of pets or amenities.

As such, the national is subject to the same provisions as the French national (see infra "3°. Installation procedures and formalities").

### c. Prohibitions

For the professional, it is forbidden to sell a pet:

- sick or injured during an event for the presentation and sale of pets;
- a minor under the age of 16 without the consent of his or her parents;
- for a fee or not, in fairs, flea markets, trade shows, exhibitions or other events that are not specifically dedicated to animals.

*For further information*: Articles L. 214-7, R. 214-20 and R. 214-31-1 of the Rural Code and Marine Fisheries.

### d. Some peculiarities of the regulation of the activity

**Terms and conditions for premises**

The premises in which the professional practises must comply with the rules of health and the protection of animals.

**Please note**

The professional who wishes to organize an exhibition or event related to the sale of pets, must first make a declaration with the prefect of the department and must ensure that the facilities comply with the rules. health care and animal protection.

*For further information*: Article R. 214-29 of the Rural Code and Marine Fisheries; decree of 3 April 2014 setting out the sanitary and animal protection rules to which activities related to domestic pets under Article L. 214-6 of the Rural Code and Maritime Fisheries must be met).

**Customer disclosure obligations**

When selling a pet, the professional is required to provide the buyer with:

- a certificate of transfer, or an invoice in case of sale between professionals;
- A backgrounder on all the characteristics and needs of the animal sold;
- in case of the sale of dogs and cats, a veterinary certificate.

*For further information*: Article L. 214-8 of the Rural Code and Marine Fisheries.

**Obligation to establish a health regulation with a veterinarian**

The professional is required, with the help of a health veterinarian, to draw up a health regulation specifying the conditions of his activity. This regulation must ensure that the needs of the animals are respected as well as the hygiene and health of the staff.

This regulation must include at least the following information:

- A plan to clean and disinfect premises and equipment;
- All of the establishment's hygiene rules
- Animal maintenance procedures
- the length of time the animals are isolated when they arrive at the facility.

**Please note**

Twice a year, the professional must ensure that a health veterinarian visits the premises to verify the compliance of the facilities with all of these requirements.

*For further information*: Article R. 214-30 of the Rural Code and Marine Fisheries; Chapter III of the[Annex](http://agriculture.gouv.fr/file/annexesarreteanimauxdecompagniebo-maafcle83fb2bpdf) I of the order of April 3, 2014 aforementioned.

**Obligation to keep different records**

During the course of his activity, the professional must keep up to date with:

- A register of animals entering and exiting the names and addresses of their owners;
- a register listing all information on health monitoring and animal health.

**Please note**

These registers must be presented to the relevant authority at each health check.

*For further information*: Article R. 214-30-3 of the Rural Code and Marine Fisheries.

**Appoint a veterinarian**

The professional engaged in the purchase or sale of pets must appoint a health veterinarian to monitor compliance with health rules and carry out animal protection checks. To do so, he must inform the prefect of the department where the animals are located.

*For further information*: Articles L. 203-1, R. 203-1 and R. 203-2 of the Rural and Marine Fisheries Code.

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERPs) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of June 25, 1980 approving the general provisions of the fire and panic safety regulations in public institutions.

It is advisable to refer to the listing " [Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Registration with the RCS for the purchase or sale of pets

**Competent authority**

The trader must report his business and, in order to do so, make a declaration with the Chamber of Commerce and Industry (CCI).

**Supporting documents**

The person concerned must provide the[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sends a receipt to the professional on the same day mentioning the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Articles L. 214-6-3 of the Rural and Marine Fisheries Code and L. 123-1 of the Code of Commerce; Section 635 of the General Tax Code.

### b. Statement of activity in relation to pets

**Competent authority**

The professional must address his statement to the Departmental Directorate of Population Protection (DDPP) or the Departmental Directorate of Social Cohesion and Population Protection (DDCSPP) of the department where he wishes to settle.

**Supporting documents**

It must send the relevant body to the form[Cerfa 15045*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15045.do) completed and signed. This procedure can also be done online on the[Department of Agriculture services website](https://agriculture-portail.6tzen.fr/default/requests/Cerfa15045/).

**Procedure**

Once the declaration has been received, the prefect sends the applicant a receipt of a declaration.

*For further information*: Article L. 214-6-1 of the Rural Code and Marine Fisheries.

### c. Request for permission to open a non-domestic animal facility

Institutions are categorized into two categories:

- the first includes establishments that pose serious dangers or disadvantages to wildlife and natural environments and the safety of people;
- the second includes those who do not present the above dangers but who must comply with provisions to ensure the protection of wildlife and natural environments and the safety of persons.

**Competent authority**

The professional must submit a seven-copy application to the prefect of the department in which the establishment is located (or from his home as long as the establishment is mobile).

**Supporting documents**

The request should include the following information:

- The applicant's identity, address and, if the applicant is a legal entity, its name and name;
- The nature of the activities the professional wishes to carry out;
- The list of the facility's facilities and the facility plan;
- The list of species and the number of animals of each species held by the institution and their distribution in the facility;
- A notice on how the establishment works
- The facility manager's certificate of capacity.

**Please note**

The name of the institution should not include the following terms, due to the specific provisions that govern them:

- National Park;
- Nature Reserve;
- Conservatory.

In addition, where the facility includes pre-approved facilities classified for environmental protection, the facility is subject to opening permission.

**Delays and outcome of the procedure**

The prefect receives the opinion of the Departmental Commission on the Nature of Landscapes and Sites, which may question the applicant. If necessary, he will be informed by the prefect eight days before his presentation.

The prefect has five months from the filing of the application to authorize the opening of the establishment. In the event of a favourable opinion, it decides the opening authorization setting out the list of species that the establishment may hold and the activities that may be carried out.

**Please note**

For establishments in the first category, the prefect must seek the opinion of the local authorities, which have a 45-day decision period. In the absence of an answer, the opinion is deemed favourable.

On the other hand, for establishments in the second category, the prefect examines the compliance of the application with the requirements for species protection and the quality of non-domestic animal care facilities.

*For further information*: Articles R. 413-10 to R. R. 413-14 of the Environment Code; decree of 10 August 2004 setting out the general rules for the operation of breeding facilities for the breeding of animals of non-domestic species.

### d. Application for a certificate of capacity for non-domestic animals

**Competent authority**

The professional must apply to the prefect of the department of his home or to the prefect of police in Paris if he is not domiciled in a French department or in Saint-Pierre-et-Miquelon.

**Supporting documents**

His request should contain the following information:

- identity, address and type of general or special qualification requested
- all diplomas, certificates or any documents justifying his professional experience;
- any documentation to justify the applicant's competence to engage in a professional activity in connection with pets and the development of an establishment that would house them.

**Delays and outcome of the procedure**

Upon receipt of the full request from the professional, the prefect issues him the certificate of capacity. This certificate may be granted for a limited or unlimited period of time and mentions the species or activity for which it was granted and, as an optional option, the number of animals authorized for maintenance.

**Please note**

For the breeding of certain categories of animals, consultation with the Departmental Commission on Nature, Landscapes and Sites is not mandatory as long as the professional meets qualification requirements. The terms of this procedure are set by the[decreed from 2 July 2009](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020887078&dateTexte=20180406) quoted below.

*For further information*: R. 413-3 to R. 413-7 of the Environment Code; order of 2 July 2009 setting out the simplified conditions under which the certificate of capacity for the maintenance of animals of non-domestic species can be issued.

### e. Post-registration authorisation

**If necessary, proceed with the formalities related to the facilities classified for the protection of the environment (ICPE)**

Depending on the nature of the business, the professional may be required to declare his facilities.

Only the breeding, sale and transit activities of dogs[2120](https://aida.ineris.fr/consultation_document/10535) and furry predatory animals[2113](https://aida.ineris.fr/consultation_document/10535) are affected by this regulation.

As such, the professional must:

- seek prefectural authorization if it holds more than 50 dogs or more than 2000 furry predators;
- make a declaration to the prefect if he holds between 10 and 50 dogs or between 100 and 200 furry carnivores.

*For further information*: Articles L. 181-1, L. 413-3 and L. 511-1 and following of the Environment Code.

To learn more about the reporting and authorisation arrangements, it is advisable to visit the[site dedicated to classified facilities](http://www.installationsclassees.developpement-durable.gouv.fr/).

