﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS021" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Achat/Vente d’animaux d’agrément ou de compagnie" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="achatvente-d-animaux-d-agrement" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/achatvente-d-animaux-d-agrement-ou-de-compagnie.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="achatvente-d-animaux-d-agrement-ou-de-compagnie" -->
<!-- var(translation)="None" -->

# Achat/Vente d’animaux d’agrément ou de compagnie

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité d'achat ou de vente d'animaux d'agrément de compagnie consiste pour le professionnel, à acheter et à proposer à des particuliers ou des professionnels, la vente d'animaux domestiques et non domestiques.

Sont considérés comme des animaux domestiques, ceux destinés à être détenus par l'homme pour son agrément. Par définition, les animaux non domestiques sont ceux ne rentrant pas dans cette catégorie.

*Pour aller plus loin* : article L. 214-6 du Code rural et de la pêche maritime et L. 413-2 du Code de l'environnement.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée. L'activité étant de nature commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exerce une activité d'achat-revente son activité sera à la fois commerciale et artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité d'achat ou de vente d'animaux de compagnie ou d'agrément, le professionnel doit :

- procéder à son immatriculation au Registre du commerce et des sociétés (RCS) (cf. infra « 3°. a. Immatriculation au RCS ») ;
- être qualifié professionnellement ;
- exercer son activité dans des locaux conformes aux dispositions sanitaires (cf. infra « 2°. d. Conditions relatives aux locaux ») ;
- en cas d'activité liée aux animaux domestiques, effectuer une déclaration au préfet (cf. infra « 3°. b. Déclaration d'activité en lien avec les animaux domestiques ») ;
- en cas d'activité liée aux animaux non domestiques, demander une autorisation d'ouverture de son établissement (cf. infra « 3°. c. Demande d'autorisation d'ouverture d'un établissement d'accueil des animaux non domestiques »).

*Pour aller plus loin* : article L. 214-6 du Code rural et de la pêche maritime ; article L. 413-2 du Code de l'environnement.

**En cas d'achat/vente avec des animaux domestiques**

Pour exercer une activité d'achat ou de vente d'animaux d'espèces domestiques, le professionnel doit être titulaire soit :

- de l'une des certifications professionnelles mentionnées à l'annexe II de l'arrêté du 4 février 2016 relatif à l'action de formation et à l'actualisation des connaissances nécessaires aux personnes exerçant des activités liées aux animaux de compagnie d'espèces domestiques et à l'habilitation des organismes de formation ;
- d'une attestation de connaissance relative aux besoins biologiques, physiologiques, comportementaux et à l'entretien des animaux de compagnie délivrée par un établissement agréé par le ministre chargé de l'agriculture ;
- d'un certificat de capacité des animaux d'espèces domestiques (CCAD) délivré avant le 1er janvier 2016.

Pour connaître les modalités d'accès à ces formations, il est conseillé de se reporter à la fiche « [Entretien des animaux de compagnie d'espèces domestiques](https://www.guichet-qualifications.fr/fr/dqp/metiers-animaliers/entretien-des-animaux-de-compagnie-d-especes-domestiques.html) ».

*Pour aller plus loin* : article L. 214-6 du Code rural et de la pêche maritime.

**En cas d'achat/vente d'animaux non domestiques**

Pour exercer cette activité, le professionnel doit être titulaire d'un certificat de capacité.

Pour cela, il doit adresser une demande au préfet du département de son domicile (cf. infra « 3°. d. Demande en vue d'obtenir un certificat de capacité pour les animaux non domestiques »).

*Pour aller plus loin* : article L. 413-2 du Code de l'environnement.

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services (LPS) ou Libre établissement (LE))

**En vue d'un exercice temporaire et occasionnel (LPS)**

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant une activité d'achat ou de vente d'animaux de compagnie ou d'agrément, peut exercer, à titre temporaire et occasionnel, en France, la même activité.

Pour cela, il doit effectuer une déclaration préalable dont les modalités varient selon la nature de son activité.

Pour connaître les modalités de cette déclaration, il est conseillé de se reporter aux fiches « Entretien des animaux de compagnie d'espèces domestiques » et « Responsable d'établissement d'élevage d'animaux d'espèce non domestique, de vente ou de location, de transit, de présentation au public de spécimens vivants de la faune française ou étrangère ».

**En vue d'un exercice permanent (LE)**

Aucune disposition n'est prévue pour le ressortissant de l'UE souhaitant exercer l'activité d'achat ou de vente d'animaux de compagnie ou d'agrément.

À ce titre, le ressortissant est soumis aux mêmes dispositions que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation »).

### c. Interdictions

Est interdit pour le professionnel, le fait de vendre un animal de compagnie :

- malade ou blessé, au cours d'un événement destiné à la présentation et la vente d'animaux de compagnie ;
- à un mineur de moins de 16 ans sans le consentement de ses parents ;
- à titre onéreux ou non, au sein de foires, de brocantes, de salons, d'expositions ou d'autres manifestations qui ne sont pas spécialement consacrées aux animaux.

*Pour aller plus loin* : articles L. 214-7, R. 214-20 et R. 214-31-1 du Code rural et de la pêche maritime.

### d. Quelques particularités de la réglementation de l’activité

**Conditions relatives aux locaux**

Les locaux au sein desquels exerce le professionnel, doivent être conformes aux règles sanitaires et relatives à la protection des animaux.

**À noter**

Le professionnel qui souhaite organiser une exposition ou une manifestation liée à la vente d'animaux de compagnie, doit au préalable, effectuer une déclaration auprès du préfet du département et doit s'assurer de la conformité des installations aux règles sanitaires et de la protection animale.

*Pour aller plus loin* : article R. 214-29 du Code rural et de la pêche maritime ; arrêté du 3 avril 2014 fixant les règles sanitaires et de protection animale auxquelles doivent satisfaire les activités liées aux animaux de compagnie d'espèces domestiques relevant du IV de l'article L. 214-6 du Code rural et de la pêche maritime).

**Obligations d'information au client**

Lors de la vente d'un animal de compagnie, le professionnel est tenu de fournir à l'acheteur :

- une attestation de cession, ou une facture en cas de vente entre professionnels ;
- un document d'information sur l'ensemble des caractéristiques et des besoins de l'animal vendu ;
- en cas de vente de chiens et chats, d'un certificat vétérinaire.

*Pour aller plus loin* : article L. 214-8 du Code rural et de la pêche maritime.

**Obligation d'établir un règlement sanitaire avec un vétérinaire**

Le professionnel est tenu, avec l'aide d'un vétérinaire sanitaire, de dresser un règlement sanitaire précisant les conditions d'exercice de son activité. Ce règlement doit permettre de s'assurer du respect des besoins des animaux ainsi que de l'hygiène et de la santé du personnel.

Ce règlement doit comprendre a minima les informations suivantes :

- un plan de nettoyage et de désinfection des locaux et du matériel ;
- l'ensemble des règles d'hygiène de l'établissement ;
- les procédures d'entretien des animaux ;
- les durées d'isolement des animaux à leur arrivée au sein de l'établissement.

**À noter**

Le professionnel doit, deux fois par an, s'assurer de la visite des locaux par un vétérinaire sanitaire pour vérifier la conformité des installations avec l'ensemble de ces exigences.

*Pour aller plus loin* : article R. 214-30 du Code rural et de la pêche maritime ; chapitre III de l'[annexe](http://agriculture.gouv.fr/file/annexesarreteanimauxdecompagniebo-maafcle83fb2bpdf) I de l'arrêté du 3 avril 2014 précité.

**Obligation de tenir différents registres**

Au cours de son activité, le professionnel doit tenir à jour :

- un registre d'entrée et de sortie des animaux mentionnant les noms et adresses de leurs propriétaires ;
- un registre mentionnant l'ensemble des informations sur le suivi sanitaire et la santé des animaux.

**À noter**

Ces registres doivent être présentés à l'autorité compétente lors de chaque contrôle sanitaire.

*Pour aller plus loin* : article R. 214-30-3 du Code rural et de la pêche maritime.

**Procéder à la désignation d'un vétérinaire**

Le professionnel exerçant l'activité d'achat ou de vente d'animaux de compagnie, doit désigner un vétérinaire sanitaire en vue de contrôler le respect des règles sanitaires et d'effectuer les contrôles en matière de protection animale. Pour cela, il doit en informer le préfet du département où se situent les animaux.

*Pour aller plus loin* : articles L. 203-1, R. 203-1 et R. 203-2 du Code rural et de la pêche maritime.

**Respect des normes de sécurité et d'accessibilité**

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des Établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

## 3°. Démarches et formalités d’installation

### a. Immatriculation au RCS en vue de l'achat ou de la vente d'animaux de compagnie

**Autorité compétente**

Le commerçant doit procéder à la déclaration de son entreprise et doit, pour cela, effectuer une déclaration auprès de la chambre de commerce et d'industrie (CCI).

**Pièces justificatives**

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

**Délais**

Le centre des formalités des entreprises de la CCI adresse le jour même, un récépissé au professionnel mentionnant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

**Voies de recours**

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Si le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

**Coût**

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : articles L. 214-6-3 du Code rural et de la pêche maritime et L. 123-1 du Code de commerce ; article 635 du Code général des impôts.

### b. Déclaration d'activité en lien avec des animaux domestiques

**Autorité compétente**

Le professionnel doit adresser sa déclaration à la direction départementale de la protection des populations (DDPP) ou de la direction départementale de la cohésion sociale et de la protection des populations (DDCSPP) du département où il souhaite s'établir.

**Pièces justificatives**

Il doit adresser à l'autorité compétente le formulaire [Cerfa 15045](https://www.service-public.fr/professionnels-entreprises/vosdroits/R36866) complété et signé. Cette procédure peut également se faire en ligne sur le [site des services du ministère de l'Agriculture](https://agriculture-portail.6tzen.fr/default/requests/Cerfa15045/).

**Procédure**

Une fois la déclaration reçue complétée, le préfet adresse au demandeur un récépissé de déclaration.

*Pour aller plus loin* : article L. 214-6-1 du Code rural et de la pêche maritime.

### c. Demande d'autorisation d'ouverture d'un établissement d'accueil des animaux non domestiques

Les établissements sont classés en deux catégories :

- la première regroupe les établissements présentant des dangers ou inconvénients graves pour les espèces sauvages et les milieux naturels et la sécurité des personnes ;
- la seconde regroupe ceux qui ne présentent pas les dangers susmentionnés mais qui doivent respecter les dispositions visant à assurer la protection des espèces sauvages et des milieux naturels et la sécurité des personnes.

**Autorité compétente**

Le professionnel doit adresser une demande en sept exemplaires au préfet du département dans lequel est situé l'établissement (ou de son domicile dès lors que l'établissement est mobile).

**Pièces justificatives**

La demande doit mentionner les informations suivantes :

- l'identité du demandeur, son adresse et, si le demandeur est une personne morale, sa dénomination et sa raison sociale ;
- la nature des activités que le professionnel souhaite exercer ;
- la liste des équipements de l'établissement et le plan des installations ;
- la liste des espèces et le nombre d'animaux de chaque espèces détenus par l'établissement et leur répartition dans l'établissement ;
- une notice relative au fonctionnement de l'établissement ;
- le certificat de capacité du responsable de l'établissement.

**À noter**

La dénomination de l'établissement ne doit pas comporter les termes suivants, en raison des dispositions spécifiques qui les régissent :

- parc national ;
- réserve naturelle ;
- conservatoire.

De plus, lorsque l'établissement comprend des installations classées pour la protection de l'environnement soumises à autorisation préalable, cette dernière vaut autorisation d'ouverture.

**Délais et issue de la procédure**

Le préfet recueille l'avis de la commission départementale de la nature des paysages et des sites qui peut interroger le demandeur. Le cas échéant, il sera informé par le préfet huit jours avant sa présentation.

Le préfet dispose d'un délai de cinq mois à compter du dépôt de la demande pour autoriser l'ouverture de l'établissement. En cas d'avis favorable il arrête l'autorisation d'ouverture fixant la liste des espèces que l'établissement peut détenir et les activités susceptibles d'être pratiquées.

**À noter**

Pour les établissements de la première catégorie, le préfet doit recueillir l'avis des collectivités territoriales qui disposent d'un délai de quarante-cinq jours pour statuer. En l'absence de réponse l'avis est réputé favorable.

En revanche pour les établissements de la seconde catégorie, le préfet examine la conformité de la demande avec les exigences en matière d'impératifs de protection des espèces et de qualité des équipements d'accueil des animaux non domestiques.

*Pour aller plus loin* : articles R. 413-10 à R. R. 413-14 du Code de l'environnement ; arrêté du 10 août 2004 fixant les règles générales de fonctionnement des installations d'élevage d'agrément d'animaux d'espèces non domestiques.

### d. Demande en vue d'obtenir un certificat de capacité pour les animaux non domestiques

**Autorité compétente**

Le professionnel doit adresser une demande au préfet du département de son domicile ou au préfet de police de Paris s'il n'est pas domicilié dans un département français ni à Saint-Pierre-et-Miquelon.

**Pièces justificatives**

Sa demande doit contenir les informations suivantes :

- son identité, son adresse et le type de qualification générale ou spécialisée demandée ;
- l'ensemble des diplômes, certificats ou tout document justifiant de son expérience professionnelle ;
- tout document permettant de justifier de la compétence du demandeur à exercer une activité professionnelle en lien avec des animaux domestiques et l'aménagement d'un établissement les accueillant.

**Délais et issue de la procédure**

Dès réception de la demande complète de la part du professionnel, le préfet lui délivre le certificat de capacité. Ce certificat peut être accordé pour une durée limitée ou illimitée et mentionne les espèces ou l'activité pour lesquelles il a été accordé et à titre facultatif le nombre d'animaux dont l'entretien est autorisé.

**À noter**

Pour l'élevage de certaines catégories d'animaux, la consultation de la commission départementale de la nature, des paysages et des sites n'est pas obligatoire dès lors que le professionnel satisfait à des exigences de qualification. Les modalités de cette procédure sont fixées par l'arrêté du 2 juillet 2009 cité ci-après.

*Pour aller plus loin* : R. 413-3 à R. 413-7 du Code de l'environnement ; arrêté du 2 juillet 2009 fixant les conditions simplifiées dans lesquelles le certificat de capacité pour l'entretien des animaux d'espèces non domestiques peut être délivré.

### e. Autorisation post-immatriculation

**Le cas échéant, procéder aux formalités liées aux installations classées pour la protection de l'environnement (ICPE)**

Selon la nature de son activité, le professionnel peut être tenu de déclarer ses installations.

Seules les activités d'élevage, de vente et de transit de chiens [rubrique 2120](https://aida.ineris.fr/consultation_document/10535) et d'animaux carnassiers à fourrure [rubrique 2113](https://aida.ineris.fr/consultation_document/10535) sont concernés par cette réglementation.

À ce titre, le professionnel doit :

- solliciter une autorisation préfectorale dès lors qu'il détient plus de 50 chiens ou plus de 2000 animaux carnassiers à fourrure ;
- procéder à une déclaration auprès du préfet dès lors qu'il détient entre 10 et 50 chiens ou entre 100 et 200 animaux carnassiers à fourrure.

*Pour aller plus loin* : articles L. 181-1, L. 413-3 et L. 511-1 et suivants du Code de l'environnement.

Pour en savoir plus sur les modalités de déclaration et d'autorisation, il est conseillé de se rendre sur le [site dédié aux installations classées](https://aida.ineris.fr).