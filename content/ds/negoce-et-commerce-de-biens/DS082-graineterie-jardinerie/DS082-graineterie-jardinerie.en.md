﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS082" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Seed shop – Garden centre" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="seed-shop-garden-centre" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/seed-shop-garden-centre.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="seed-shop-garden-centre" -->
<!-- var(translation)="Auto" -->


Seed shop – Garden centre
====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The seeding and gardening activity consists of the professional selling plants and supplies for the garden and the environment. During its activity, it is also required to produce and market seed seeds and plants.

The seeding or gardening can also involve a pet shop. If so, it is advisable to refer to the listing[Purchase/Sale of Pleasure or Pets](https://www.guichet-entreprises.fr/fr/activites-reglementees/negoce-et-commerce-de-biens/achat-vente-danimaux-dagrement-ou-de-compagnie/).

**Good to know**

The practice of this activity is regulated as long as the professional proceeds to the sale of phytopharmaceuticals or treated seeds.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. The activity is commercial in nature, however, in case of preparation of plants and floral compositions the activity will be commercial and artisanal.

- For craft activities, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional operator has a fast food activity associated with the manufacture of handicrafts, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

No professional qualifications are required to operate a seed and gardening business. However, the professional is required to:

- take an installation preparation course (SPI) for registration in the trades directory;
- If phytopharmaceuticals are put up for sale, be the holder:- a specific individual certificate called "Certiphyto"
  - approval for the practice of a distributor of phytopharmaceuticals (see infra "3°. b. Application for approval").*For further information*: Article L. 254-1 of the Rural Code and Marine Fisheries.

**Installation Preparation Course (SPI)**

The professional engaged in a craft activity must, in order to register him in the trades directory, carry out an SPI composed of two parts:

- the first part aims to train the professional in the management of his business by introducing him, in particular, to the general accounting and the economic, legal and social environment of a craft company;
- the second part of the internship is to accompany the professional after his registration in the trades directory.

However, the professional may be excused from performing an SPI as soon as he or she:

- is prevented by a case of force majeure. If necessary, he will have to complete this internship within one year of his registration;
- completed a management training course of at least thirty hours and organized by the Chamber of Trades and Crafts regional, departmental or interdepartmental;
- received support for the creation of a business lasting a minimum of thirty hours delivered by a network of help to set up a business. As such, the professional must:- have received management training,
  - and be registered in the national directory of professional certifications;
- has been engaged in a professional activity requiring at least the same level as that provided by the SPI for at least three years.

*For further information*: Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen; Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

**Individual certificate for the activity of "sale, sale of phytopharmaceuticals"**

The individual certificate is issued to the professional who:

- (a) has either undergoed training that includes a one-hour knowledge check test;
- or undergone a one-and-a-half hour test, consisting of thirty questions about the training programme set out in Schedule II of the order of 29 August 2016;
- or holds one of the diplomas on the schedule I list of the August 29, 2016 order, obtained in the five years prior to the application date.

Once the professional fulfils one of these conditions, he must apply for the certificate (see infra "3°. a. Request for an individual certificate of practice").

*For further information*: decree of August 29, 2016 establishing and setting out the terms of obtaining the individual certificate for the activity "sale, sale of phytopharmaceuticals".

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

**In preparation for an LPS**

Any national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement, legally established and engaged in a commercial or sale of phytopharmaceutical products, may exercise as a temporary and casual, the same activity in France.

Where neither access to the profession nor its exercise is regulated in that Member State, the national must justify having carried out this activity for at least one year in the last ten years.

Once it fulfils these conditions, it must, prior to its first provision of services, make a declaration to the Regional Director of Food, Agriculture and Forestry (DRAAF) (see below "3. c. Pre-declaration for the EU national for a temporary and occasional exercise").

*For further information*: Articles R. 254-9 and L. 204-1 of the Rural code and marine fisheries.

**In preparation for an LE**

Any national of an EU or EEA Member State, engaged in the sale or sale of phytopharmaceuticals, may carry out the same activity on a permanent basis in France.

As long as he holds an individual certificate of exercise issued by the competent authority of a Member State, he is deemed to hold the certificate required to practise in France.

*For further information*: II of Article R. 254-9 of the Rural Code and Marine Fisheries.

### c. Insurance

The professional is required, in order to obtain his approval, to take out insurance covering his professional civil liability.

*For further information*: Article L. 254-2 of the Rural Code and Marine Fisheries.

### d. Some peculiarities of the regulation of the activity

**Health information obligation**

The professional engaged in activity related to plants and/or animals must, if he detects or suspects a health hazard, inform the prefect immediately.

It must also carry out or carry out, during its activity, all measures aimed at preventing and combating possible health hazards.

*For further information*: Article L. 201-7 and the following of the Rural Code and Marine Fisheries.

**Obligations for the sale of phytopharmaceuticals to non-professional users**

The professional working as a seed gardener may only offer for sale to non-professional persons products with the words "authorized employment in gardens".

However, exemptions are possible for people acting on behalf of professionals. If so, these users must justify being in possession of a contract or a certificate of delegation to a third party or a proof of the professional quality of that third party.

In addition, in the event of a sale of products that do not carry this mention, the professional is obliged to ensure beforehand the professional quality of the future buyer. To do so, it must verify that the latter has an individual certificate of practice in one of the following categories:

- "Decision-maker in work and services";
- "Decision-maker in agricultural work";
- "Territorial Community Applicator."

At each sale, the professional is required to:

- separate products labelled "authorized use in gardens" and those that do not, using an explicit signage process;
- provide all information about the use of products and the risks they pose to health and the environment, as well as safety guidelines to prevent risks.

*For further information*: Articles R. 254-20; R. 254-22 of the Rural Code and Marine Fisheries; January 6, 2016 order on the documentation required for the purchase of phytopharmaceuticals from the range of "professional" uses.

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of June 25, 1980 approving the general provisions of the fire and panic safety regulations in public institutions.

It is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

**Activity register**

The professional must maintain a chronological record of sales of phytopharmaceuticals, stating:

- Product information (trade name, authorization number, quantity sold, royalty amount);
- The number and billing date
- The end-user's postcode of the product
- Documents justifying the user's professional status
- all information about the treated seed (the plant species concerned, the number and date of the billing).

Each year, the professional must take stock of the previous calendar year, including all the information mentioned above during the sale of each phytopharmaceutical product.

In addition, each year before 1 April, it must transmit this information to the water agency and the water board electronically, as well as the diffuse pollution charge.

*For further information*: Article R. 254-23 and the following of the Rural code and marine fisheries.

**Specific provisions for the marketing of seeds and plants**

Only seeds and plants can be marketed in France, referenced in an official catalogue of cultivated plant species and varieties when such referencing is provided for the species concerned.

The French and European catalogues of varieties are available on the website of the National Interprofessional Seed and Plant Group ([GNIS](http://www.gnis.fr/catalogue-varietes/)).

*For further information*: Decree 81-605 of 18 May 1981 for the implementation of the law of 1 August 1905 on the suppression of fraud in relation to the trade in seeds and plants.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Request for the individual certificate of exercise

**Competent authority**

The professional must submit his request to the Regional Directorate of Food, Agriculture and Forestry (DRAAF) of the place of his residence, or if necessary, the place of the head office of the organization where his training was carried out.

**Supporting documents**

Applicants are required to create an online account on the website[service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) to access the certificate application service.

His application must include, depending on the case:

- A proof of follow-up training and, if necessary, a pass to the test;
- copy of his diploma or training title.

**Time and procedure**

The certificate is issued within two months of filing his application. In the absence of the issuance of the certificate after this period, and unless notification of refusal, the above supporting documents are worth individual certificate for up to two months.

This certificate is valid for five years and renewable under the same conditions.

*For further information*: Article R. 254-11 of the Rural Code and Marine Fisheries.

### b. Application for approval

**Competent authority**

The professional must submit his request to the prefect of the region in which the company's head office is located. If the professional has his head office in a Member State other than France, he will have to submit his application to the prefect of the region in which he will carry out his first activity.

**Supporting documents**

His application must include the[Application form](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14581.do) completed and signed as well as the following documents:

- A certificate of professional liability insurance policy;
- a certificate issued by a third-party organization justifying that it performs this activity in accordance with the conditions guaranteeing the protection of public health and the environment and the good information of users. This company certification is obtained following an audit carried out by a certifying body on the basis of a repository set for the order of 27 April 2017;
- copying a contract with a third-party organization with a contract providing the necessary follow-up to maintain certification.

**Please note**

Micro-distributors, on the other hand, must submit the following documents to THE DRAAF:

- a certificate of professional liability insurance;
- proof of the holding of the Certiphyto certificate by all its employees;
- A document justifying that it is subject to the micro-enterprise tax system;
- If so, the list of sites where the professional is likely to carry out his activity.

**Time and procedure**

The professional who starts his activity must apply for an interim approval which will be granted for a period of six months non-renewable. Following this, he will be able to apply for final approval.

The silence kept by the prefect of the region beyond a period of two months, is worth decision of rejection.

In addition, the professional is required to provide the prefect with a copy of his insurance certificate each year.

*For further information*: Article R. 254-15-1 to R. 254-17 of the Rural Code and Marine Fisheries; order of 27 April 2017 amending the order of 25 November 2011 relating to the certification framework provided for in Article R. 254-3 of the Rural and Maritime Fisheries Code for the activity "distribution of phytopharmaceuticals to non-users professionals."

**Please note**

Even if you apply for several activities, the accreditation obtained will be unique.

### c. Pre-declaration for EU national for temporary and casual exercise (LPS)

**Competent authority**

The national must apply by any means to the DRAAF of the place where his first service is delivered.

**Supporting documents**

Its application must include the individual certificate of exercise issued by the Member State and, if necessary, with its translation into French by a certified translator.

**Please note**

This declaration must be renewed every year and in the event of a change in employment status.

*For further information*: III of Article R. 254-9 of the Rural Code and Marine Fisheries.

### d. Company reporting formalities

Depending on the nature of his activity, the professional will have to register with the Trades and Crafts Register (RMA) or the Register of Trade and Companies (RCS). It is advisable to refer to the "Artisanal Company Reporting Formalities" "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### e. Register with GNIS

**Competent authority**

The professional must apply to the GNIS for registration.

**Supporting documents**

His application must include the[Questionnaire](https://media.afecreation.fr/file/30/4/questionnaire_en_vue_de_delivrer_l%27attestation_d%27enregistrement_au_gnis.56304.pdf) planned for this purpose, completed and signed.

**Time and procedure**

Once the request is submitted, the GNIS sends the professional a certificate of registration.

**Please note**

In the event of a change in the volume or nature of the activity, the professional is required to report it to the GNIS.

*For further information*: decree of 7 November 1996 on the terms of registration in the professional categories of the Plants section;[GNIS official website](http://www.gnis.fr/).

