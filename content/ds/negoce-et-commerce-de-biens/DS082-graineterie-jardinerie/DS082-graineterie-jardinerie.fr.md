﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS082" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Graineterie – Jardinerie" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="graineterie-jardinerie" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/graineterie-jardinerie.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="graineterie-jardinerie" -->
<!-- var(translation)="None" -->

# Graineterie – Jardinerie

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité de graineterie et de jardinerie consiste pour le professionnel à vendre des végétaux et des fournitures pour le jardin et l'environnement. Au cours de son activité, il est également amené à produire et commercialiser de graines de semences et des plants.

La graineterie ou la jardinerie peut également comporter une activité d'animalerie. Le cas échéant, il est conseillé de se reporter à la fiche « [Achat/Vente d'animaux d'agrément ou de compagnie](https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/achatvente-d-animaux-d-agrement-ou-de-compagnie.html) ».

**Bon à savoir**

L'exercice de cette activité est réglementé dès lors que le professionnel procède à la mise en vente de produits phytopharmaceutiques ou de semences traitées.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée. L'activité est de nature commerciale, toutefois, en cas de préparation de plantes et de compositions florales l'activité sera de nature commerciale et artisanale.

- pour les activités artisanales, le CFE compétent est la chambre de métiers et de l'artisanat (CMA) ;
- pour les sociétés commerciales, il s’agit de la chambre du commerce et de l’industrie (CCI) ;

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exploitant a une activité de restauration rapide associée à la fabrication de produits artisanaux, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucune qualification professionnelle n'est requise pour exploiter une entreprise spécialisée en graineterie et jardinerie. Toutefois, le professionnel est tenu de :

- suivre un stage de préparation à l'installation (SPI) en vue de son immatriculation au répertoire des métiers ;
- en cas de mise en vente des produits phytopharmaceutiques, être titulaire :
  - d'un certificat individuel spécifique appelé « Certiphyto »,
  - d'un agrément pour l'exercice de l'activité de distributeur de produits phytopharmaceutiques (cf. infra « 3°. b. Demande en vue d'obtenir un agrément »).

*Pour aller plus loin* : article L. 254-1 du Code rural et de la pêche maritime.

#### Stage de préparation à l'installation (SPI)

Le professionnel exerçant une activité artisanale doit, en vue de son immatriculation au répertoire des métiers, effectuer un SPI composé de deux parties :

- la première partie a pour but de former le professionnel à la gestion de son entreprise en l'initiant, notamment, à la comptabilité générale et l'environnement économique, juridique et social d'une entreprise artisanale ;
- la seconde partie du stage consiste à accompagner le professionnel après son immatriculation au répertoire des métiers.

Toutefois, le professionnel peut être dispensé d'effectuer un SPI dès lors qu'il :

- en est empêché par un cas de force majeure. Le cas échéant, il devra effectuer ce stage dans un délai d'un an suivant son immatriculation ;
- a suivi une formation en gestion d'une durée minimale de trente heures et organisé par la chambre de métiers et de l'artisanat régionale, départementale ou interdépartementale ;
- a bénéficié d'un accompagnement à la création d'entreprise d'une durée minimale de trente heures délivré par un réseau d'aide à la création d'entreprise. A ce titre le professionnel doit :
  - avoir reçu une formation en gestion,
  - et être inscrit au Répertoire national des certifications professionnelles ;
- a exercé pendant au moins trois ans une activité professionnelle nécessitant un niveau au moins égal à celui fourni par le SPI.

*Pour aller plus loin* : loi n° 82-1091 du 23 décembre 1982 relative à la formation professionnelle des artisans ; décret n° 83-517 du 24 juin 1983 fixant les conditions d'application de la loi 82-1091 du 23 décembre 1982 relative à la formation professionnelle des artisans.

#### Certificat individuel pour l'activité de « mise en vente, vente des produits phytopharmaceutiques »

Le certificat individuel est délivré au professionnel qui :

- a soit suivi une formation comprenant une épreuve d'une heure de contrôle de connaissances ;
- soit subi un test d'une heure trente, composé de trente questions portant sur le programme de formation fixé à l'annexe II de l'arrêté du 29 août 2016 ;
- soit est titulaire de l'un des diplômes inscrits sur la liste fixée à l'annexe I de l'arrêté du 29 août 2016, obtenu au cours des cinq ans précédant la date de la demande.

Dès lors qu'il remplit l'une de ces conditions, le professionnel doit adresser une demande en vue d'obtenir le certificat (cf. infra « 3°. a. Demande en vue d'obtenir un certificat individuel d'exercice »).

*Pour aller plus loin* : arrêté du 29 août 2016 portant création et fixant les modalités d'obtention du certificat individuel pour l'activité « mise en vente, vente des produits phytopharmaceutiques ».

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services (LPS) ou Libre établissement (LE))

#### En vue d'une LPS

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE), légalement établi et exerçant une activité de mise en vente ou de vente des produit phytopharmaceutiques, peut exercer à titre temporaire et occasionnel, la même activité en France.

Lorsque ni l'accès à la profession ni son exercice ne sont réglementés dans cet État membre, le ressortissant doit justifier avoir exercé cette activité pendant au moins un an au cours des dix dernières années.

Dès lors qu'il remplit ces conditions, il doit, préalablement à sa première prestation de services, effectuer une déclaration auprès du directeur régional de l'alimentation, de l'agriculture et de la forêt (DRAAF) (cf. infra « 3. c. Déclaration préalable pour le ressortissant de l'UE en vue d'un exercice temporaire et occasionnel »).

*Pour aller plus loin* : articles R. 254-9 et L. 204-1 du Code rural et de la pêche maritime.

#### En vue d'un LE

Tout ressortissant d'un État membre de l'UE ou de l'EEE, exerçant l'activité de mise en vente ou de vente des produits phytopharmaceutiques, peut exercer à titre permanent, la même activité en France.

Dès lors qu'il est titulaire d'un certificat individuel d'exercice délivré par l'autorité compétente d'un État membre, il est réputé être titulaire du certificat exigé pour exercer en France.

*Pour aller plus loin* : II de l'article R. 254-9 du Code rural et de la pêche maritime.

### c. Assurance

Le professionnel est tenu, pour obtenir son agrément, de souscrire une assurance couvrant sa responsabilité civile professionnelle.

*Pour aller plus loin* : article L. 254-2 du Code rural et de la pêche maritime.

### d. Quelques particularités de la réglementation de l’activité

#### Obligation d'information sanitaire

Le professionnel exerçant une activité liée aux végétaux et/ou aux animaux doit, dès lors qu'il détecte ou suspecte un danger sanitaire, en informer immédiatement le préfet.

Il doit en outre réaliser ou faire réaliser, au cours de son activité, l'ensemble des mesures visant à prévenir et à lutter contre les éventuels dangers sanitaires.

*Pour aller plus loin* : article L. 201-7 et suivants du Code rural et de la pêche maritime.

#### Obligations en cas de vente de produits phytopharmaceutiques à des utilisateurs non professionnels

Le professionnel exerçant une activité de grainetier - jardinier ne peut proposer à la vente aux personnes non professionnelles, que des produits comportant la mention « emploi autorisé dans les jardins ».

Toutefois, des dispenses sont possibles pour les personnes agissant pour le compte de professionnels. Le cas échéant, ces utilisateurs doivent justifier être en possession d'un contrat ou d'une attestation de délégation à un tiers ou un justificatif de la qualité professionnelle de ce tiers.

En outre, en cas de vente de produits ne portant pas cette mention, le professionnel est tenu de s'assurer au préalable, de la qualité de professionnel du futur acheteur. Pour cela, il doit vérifier que ce dernier dispose d'un certificat individuel d'exercice dans l'une des catégories suivantes :

- « Décideur en travaux et services » ;
- « Décideur en travaux agricoles » ;
- « Applicateur en collectivité territoriale ».

Lors de chaque vente, le professionnel est tenu :

- de séparer les produits portant cette mention « emploi autorisé dans les jardins » et ceux ne la comportant pas, grâce à un procédé signalétique explicite ;
- de fournir l'ensemble des informations relatives à l'utilisation des produits et des risques qu'ils présentent pour la santé et l'environnement, ainsi que les consignes de sécurité en vue de prévenir les risques.

*Pour aller plus loin* : articles R. 254-20 ; R. 254-22 du Code rural et de la pêche maritime ; arrêté du 6 janvier 2016 relatif aux justificatifs requis pour l'achat de produits phytopharmaceutiques de la gamme d'usages « professionnels ».

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

#### Registre d'activité

Le professionnel doit tenir à jour un registre chronologique, des ventes de produits phytopharmaceutiques effectuées, mentionnant :

- les informations relatives au produit (nom commercial, numéro d'autorisation, quantité vendue, montant de la redevance) ;
- le numéro et la date de facturation ;
- le code postal de l'utilisateur final du produit ;
- les documents justifiants la qualité de professionnel de l'utilisateur ;
- l'ensemble des informations relatives à la semence traitée (espèce végétale concernée, numéro et date de facturation).

Chaque année, le professionnel doit dresser un bilan de l'année civile précédente, comportant l'ensemble des informations citées ci-dessus lors de la vente de chaque produit phytopharmaceutique.

De plus, il doit, chaque année avant le 1er avril, transmettre ces informations à l'agence de l'eau et l'office des eaux par voie électronique, ainsi que la redevance pour pollution diffuse.

*Pour aller plus loin* : article R. 254-23 et suivants du Code rural et de la pêche maritime.

#### Dispositions spécifiques à la commercialisation de semences et de plants

Seuls peuvent être commercialisés en France, les semences et plants référencés au sein d'un catalogue officiel des espèces et variétés de plantes cultivées lorsqu'un tel référencement est prévu pour l'espèce concernée.

Les catalogues français et européen des variétés sont disponibles sur le site du Groupement national interprofessionnel des semences et des plants ([GNIS](http://www.gnis.fr/catalogue-varietes/)).

*Pour aller plus loin* : décret n° 81-605 du 18 mai 1981 pris pour l'application de la loi du 1er août 1905 sur la répression des fraudes en ce qui concerne le commerce des semences et plants.

## 3°. Démarches et formalités d’installation

### a. Demande en vue d'obtenir le certificat individuel d'exercice

#### Autorité compétente

Le professionnel doit adresser sa demande à la direction régionale de l'alimentation, de l'agriculture et de la forêt (DRAAF) du lieu de sa résidence, ou le cas échéant, du lieu du siège social de l'organisme où ont été réalisées ses formations.

#### Pièces justificatives

Le demandeur est tenu de créer un compte en ligne sur le site [service-public.fr](https://www.service-public.fr/compte/se-connecter?targetUrl=/loginSuccessFromSp&typeCompte=particulier) pour accéder au service de la demande de certificat.

Sa demande doit comprendre, selon le cas :

- un justificatif attestant du suivi de la formation et le cas échéant de la réussite au test ;
- la copie de son diplôme ou titre de formation.

#### Délai et procédure

Le certificat lui est délivré dans un délai de deux mois suivant le dépôt de sa demande. En l'absence de délivrance du certificat après ce délai, et sauf notification de refus, les pièces justificatives précitées valent certificat individuel pour une durée maximale de deux mois.

Ce certificat est valable pendant cinq ans et renouvelable dans les mêmes conditions.

*Pour aller plus loin* : article R. 254-11 du Code rural et de la pêche maritime.
 
### b. Demande en vue d'obtenir un agrément

#### Autorité compétente

Le professionnel doit adresser sa demande au préfet de la région au sein de laquelle se trouve le siège social de l'entreprise. Si le professionnel a son siège social dans un autre État membre que la France, il devra adresser sa demande au préfet de la région dans laquelle il effectuera sa première prestation d'activité.

#### Pièces justificatives

Sa demande doit comporter le [formulaire de demande d'agrément](https://www.service-public.fr/professionnels-entreprises/vosdroits/R22206) complété et signé ainsi que les documents suivants :

- un certificat de police d'assurance de responsabilité civile professionnelle ;
- un certificat délivré par un organisme tiers justifiant qu'il exerce cette activité dans le respect des conditions garantissant la protection de la santé publique et de l'environnement et de la bonne information des utilisateurs. Cette certification d'entreprise est obtenue suite à un audit réalisé par un organisme certificateur sur la base d'un référentiel fixé à l'arrêté du 27 avril 2017 ;
- la copie d'un contrat conclu avec un organisme tiers un contrat prévoyant le suivi nécessaire au maintien de la certification.

**À noter**

Les micro-distributeurs quant à eux, doivent transmettre à la DRAAF les documents suivants :

- une attestation d'assurance de responsabilité civile professionnelle ;
- un justificatif de la détention du certificat Certiphyto par l'ensemble de ses salariés ;
- un document justifiant qu'il est soumis au régime fiscal des micro-entreprises ;
- le cas échéant, la liste des sites où le professionnel est susceptible d'exercer son activité.

#### Délai et procédure

Le professionnel qui débute son activité doit solliciter un agrément provisoire qui lui sera délivré pour une durée de six mois non renouvelable. Suite à cela il pourra effectuer sa demande d'agrément définitif.

Le silence gardé par le préfet de région au delà d'un délai de deux mois, vaut décision de rejet.

En outre, le professionnel est tenu de fournir, chaque année, au préfet, une copie de son attestation d'assurance.

*Pour aller plus loin* : article R. 254-15-1 à R. 254-17 du Code rural et de la pêche maritime ; arrêté du 27 avril 2017 modifiant l'arrêté du 25 novembre 2011 relatif au référentiel de certification prévu à l'article R. 254-3 du Code rural et de la pêche maritime pour l'activité « distribution de produits phytopharmaceutiques à des utilisateurs non professionnels ».

**À noter**

Même en cas de demande portant sur plusieurs activités, l'agrément obtenu sera unique.

### c. Déclaration préalable pour le ressortissant de l'UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa demande par tout moyen, à la DRAAF du lieu d'exercice de sa première prestation de services.

#### Pièces justificatives

Sa demande doit comporter le certificat individuel d'exercice délivré par l’État membre et, le cas échéant, assorti de sa traduction en français par un traducteur agréé.

**À noter**

Cette déclaration doit être renouvelée chaque année et en cas de changement de situation professionnelle.

*Pour aller plus loin* : III de l'article R. 254-9 du Code rural et de la pêche maritime.
 
### d. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, le professionnel devra s'immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### e. Procéder à son enregistrement auprès du GNIS

#### Autorité compétente

Le professionnel doit adresser sa demande auprès du GNIS en vue de son inscription.

#### Pièces justificatives

Sa demande doit comporter le questionnaire prévu à cet effet, complété et signé.

#### Délai et procédure

Une fois la demande transmise, le GNIS adresse au professionnel une attestation d'enregistrement.

**À noter**

En cas de changement relatif au volume ou à la nature de l'activité, le professionnel est tenu de le déclarer au GNIS.

*Pour aller plus loin* : arrêté du 7 novembre 1996 portant sur les modalités d'inscription dans les catégories professionnelles de la section Plantes ; [site officiel du GNIS](http://www.gnis.fr/).