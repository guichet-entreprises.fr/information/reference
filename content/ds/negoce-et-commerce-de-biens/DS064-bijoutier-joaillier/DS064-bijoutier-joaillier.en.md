﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS064" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Sale and purchase of goods" -->
<!-- var(title)="Jeweller" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="sale-and-purchase-of-goods" -->
<!-- var(title-short)="jeweller" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/sale-and-purchase-of-goods/jeweller.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="jeweller" -->
<!-- var(translation)="Auto" -->


Jeweller
===============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The jeweler is a professional whose main mission is to manufacture and repair jewelry either in metals (gold, silver or platinum), a specialty of the jeweler, or composed of stones (precious or fine), a specialty of the jeweler.

He creates, transforms and restores rings, bracelets, necklaces and brooches.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI).

If the professional has a buying and resale activity, his activity is both artisanal and commercial.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Professional qualifications
----------------------------------------

### a. Professional qualifications

The person concerned who wishes to become a jeweller must have a minimum V-level diploma or training qualification that may be:

- a certificate of professional aptitude (CAP) "art and techniques of jewellery," "art of jewellery and jewel" or "goldsmith option fitter";
- a craft patent (BMA) "jewellery-jewellery option";
- a diploma in crafts (DMA) "art of jewel and jewel".

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

There is no provision for the national of a Member State of the European Union (EU) or party to the agreement on the European Economic Area (EEA) to carry out the activity of jeweller-jeweller on a temporary and casual or permanent basis in France.

As such, the national is subject to the same professional requirements as the French professional (see above "3o. Installation procedures and formalities").

### c. Conditions of honorability and incompatibility

No one may engage in jewellery or jewellery if it is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 of Act 96-603 of July 5, 1996.

### d. Some peculiarities of the regulation of the activity

#### Compliance with safety and accessibility standards

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

It is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

#### Obligation to keep a police book

As long as the professional holds gold, silver and platinum, he must list in a register their purchase, receipt and resale.

The police book takes the form of a register (paper or dematerialized) which must be kept for six years and which must mention each operation carried out by the jeweller.

#### Punching

To ensure the quality and provenance of a jewel, the jeweller uses the technique of punching which consists of marking the object with a punch. Depending on the shape of the punch (square, diamond, eagle, minerva, shell, etc.), the jeweler will determine the purity and value of the jewel.

#### Ear and nose wing piercing requirements

Drilling techniques for the nose wing and ear pavilion can only be implemented by professionals who:

- have a core business under the NAF 47.77Z code "Retail trade in watch and jewellery in specialty stores" or 32.12Z "Manufacturing jewellery and jewellery items";
- collective agreements:- national collective agreement for the retail trade in watch and jewellery,
  - national collective agreement for jewellery, jewellery, silverware and related activities.

*For further information*: Article R. 1311-7 of the Public Health Code; order of 29 October 2008 for the application of Article R. 1311-7 of the Code of Public Health and relating to piercing by the technique of the ear-gun; order of 11 March 2009 relating to good hygiene and safety practices for the implementation of the piercing of the ear pavilion and nose wing by the technique of the ear-piercing gun.

#### The title of best worker in France (MOF)

The professional diploma "one of the best workers in France" is a state diploma that attests to the acquisition of a high qualification in the exercise of a professional activity in the artisanal, commercial, industrial or agricultural fields.

The diploma is classified at level III of the interdepartmental nomenclature of training levels. It is issued after an examination called "one of the best workers in France" under a profession called "class" attached to a group of trades.

For more information, it is recommended that you consult[official website of the competition "one of the best workers in France"](https://www.meilleursouvriersdefrance.org/).

*For further information*: Article D. 338-9 of the Education Code.

#### The title of master craftsman

This title is given to individuals, including the social leaders of legal entities:

- Registered in the trades directory;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

**Please note**

Individuals who do not hold the master's degree can apply to the Regional Qualifications Commission for the title of Master Craftsman under two assumptions:

- when they are registered in the trades directory, have a degree at least equivalent to the master's degree, and justify management and psycho-pedagogical knowledge equivalent to those of the corresponding value units of the master's degree and that they have two years of professional practice;
- when they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Formalities of Declaration of Craft Enterprise.

### b. Follow the installation preparation course (SPI)

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received training sanctioned by a degree approved at Level III (B.A. 2), including an education in economics and business management, or by the master's degree granted by a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who:

- have either worked for at least three years requiring a level of knowledge equivalent to that provided by the internship;
- either have knowledge acquired in an EU or EEA state or a third country during a professional experience that can fully or partially cover the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of their professional qualifications shows substantial differences with those required in France to run a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree 83-517 of June 24, 1983.

### c. If necessary, make a declaration as a holder of precious metals

As long as the jeweller holds gold, silver or platinum objects, he must make a prior declaration to the warranty office of the regional directorate of customs and indirect duties.

The declaration is made by sending a file containing the following supporting documents:

- The declaration containing the name of the custodian or company, the designation of its activity, the place of practice and the address of the head office;
- A valid piece of identification
- a certificate of registration with the RCS or a certificate of registration with the RMA, or the receipt of declaration of activity.

Upon receipt of the file, the warranty office will issue a declaration of existence.

*For further information*: Section 534 of the General Tax Code.

