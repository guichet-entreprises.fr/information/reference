﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS064" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Négoce et Commerce de biens" -->
<!-- var(title)="Bijoutier-joaillier" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="negoce-et-commerce-de-biens" -->
<!-- var(title-short)="bijoutier-joaillier" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/negoce-et-commerce-de-biens/bijoutier-joaillier.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="bijoutier-joaillier" -->
<!-- var(translation)="None" -->

# Bijoutier-joaillier

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l'activité

### a. Définition

Le bijoutier-joaillier est un professionnel dont les missions principales sont de fabriquer et de réparer des bijoux soit en métaux (or, argent ou platine), spécialité du bijoutier, soit composés de pierres (précieuses ou fines), spécialité du joaillier.

Il crée, transforme et restaure des bagues, des bracelets, des colliers ou encore des broches.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour une activité commerciale, le CFE compétent est la chambre de commerce et d’industrie (CCI).

Si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Qualifications professionnelles

### a. Qualifications professionnelles

L'intéressé qui souhaite devenir bijoutier-joaillier doit être titulaire d'un diplôme ou d'un titre de formation de niveau V minimum qui peut être :

- un certificat d’aptitude professionnelle (CAP) « art et techniques de la bijouterie-joaillerie », « art du bijou et du joyau » ou encore « orfèvre option monteur » ;
- un brevet des métiers d'art (BMA) « bijou option bijouterie-joaillerie » ;
- un diplôme des métiers d'art (DMA) « art du bijou et du joyau ».

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer l'activité de bijoutier-joaillier à titre temporaire et occasionnel ou permanent en France.

À ce titre, le ressortissant est soumis aux mêmes exigences professionnelles que le professionnel français (cf. supra « 3°. Démarches et formalités d'installation »).

### c. Conditions d'honorabilité et incompatibilités

Nul ne peut exercer l'activité de bijouterie-joaillerie s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévue au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 de la loi n° 96-603 du 5 juillet 1996.

### d. Quelques particularités de la réglementation de l'activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

#### Obligation de tenir un livre de police

Dès lors que le professionnel détient de l'or, de l'argent et du platine, il doit répertorier dans un registre leur achat, leur réception et leur revente.

Le livre de police prend la forme d'un registre (papier ou dématérialisé) qui doit être conservé pendant six ans et qui doit mentionner chaque opération réalisée par le bijoutier-joaillier.

#### Le poinçonnage

Pour garantir la qualité et la provenance d'un bijou, le bijoutier-joaillier utilise la technique du poinçonnage qui consiste à marquer l'objet d'un poinçon. En fonction de la forme du poinçon (carré, losange, aigle, minerve, coquille, etc.), le bijoutier-joaillier déterminera la pureté et la valeur du bijou.

#### Obligations relatives au perçage du pavillon de l'oreille et de l'aile du nez

Les techniques de perçage de l’aile du nez et du pavillon de l’oreille ne peuvent être mises en œuvre que par les professionnels qui :

- ont une activité principale relevant du code NAF 47.77Z « Commerce de détail d’articles d’horlogerie et de bijouterie en magasin spécialisé » ou 32.12Z « Fabrication d’articles de joaillerie et bijouterie » ;
- relèvent des conventions collectives suivantes :
  - convention collective nationale du commerce de détail de l’horlogerie-bijouterie,
  - convention collective nationale de la bijouterie, joaillerie, orfèvrerie et activités s’y rapportant.

*Pour aller plus loin* : article R. 1311-7 du Code de la santé publique ; arrêté du 29 octobre 2008 pris pour l’application de l’article R. 1311-7 du Code de la santé publique et relatif au perçage par la technique du pistolet perce-oreille ; arrêté du 11 mars 2009 relatif aux bonnes pratiques d’hygiène et de salubrité pour la mise en œuvre du perçage du pavillon de l’oreille et de l’aile du nez par la technique du pistolet perce-oreille.

#### Le titre de meilleur ouvrier de France (MOF)

Le diplôme professionnel « un des meilleurs ouvriers de France » est un diplôme d’État qui atteste de l’acquisition d’une haute qualification dans l’exercice d’une activité professionnelle dans les domaines artisanal, commercial, industriel ou agricole.

Le diplôme est classé au niveau III de la nomenclature interministérielle des niveaux de formation. Il est délivré à l’issue d’un examen dénommé « concours un des meilleurs ouvriers de France » au titre d’une profession dénommée « classe », rattachée à un groupe de métiers.

Pour plus d’informations, il est recommandé de consulter le [site officiel du concours « un des meilleurs ouvriers de France »](https://www.meilleursouvriersdefrance.org/).

*Pour aller plus loin* : article D. 338-9 du Code de l’éducation.

#### Le titre de maître artisan

Ce titre est attribué aux personnes physiques, y compris les dirigeants sociaux des personnes morales :

- immatriculées au répertoire des métiers ;
- titulaires du brevet de maîtrise dans le métier exercé ;
- justifiant d’au moins deux ans de pratique professionnelle.

**À noter**

Les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan à la commission régionale des qualifications dans deux hypothèses :

- lorsqu’elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de niveau de formation au moins équivalent au brevet de maîtrise, qu’elles justifient de connaissances en gestion et en psychopédagogie équivalentes à celles des unités de valeur correspondantes du brevet de maîtrise et qu’elles ont deux ans de pratique professionnelle ;
- lorsqu’elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles disposent d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

*Pour aller plus loin* : article 3 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

## 3°. Démarches et formalités d'installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Le cas échéant, effectuer une déclaration en tant que détenteur de métaux précieux

Dès lors que le bijoutier-joaillier détient des objets en or, argent ou platine, il doit en faire une déclaration préalable auprès du bureau de garantie de la direction régionale des douanes et droits indirects.

La déclaration se fait par l'envoi d'un dossier comprenant les pièces justificatives suivantes :

- la déclaration comportant le nom du dépositaire ou de la société, la désignation de son activité, le lieu d'exercice et l'adresse du siège sociale ;
- une pièce d'identité en cours de validité ;
- une attestation d’immatriculation au RCS ou une attestation d’inscription au RMA, ou le récépissé de déclaration d’activité.

À réception du dossier, le bureau de garantie délivrera une déclaration d'existence.

*Pour aller plus loin* : article 534 du Code général des impôts.