﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS041" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="Expert foncier et agricole" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="expert-foncier-et-agricole" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/expertise/expert-foncier-et-agricole.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="expert-foncier-et-agricole" -->
<!-- var(translation)="None" -->


# Expert foncier et agricole

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l'activité

### a. Définition

L'expert foncier et agricole est un professionnel qui réalise des missions d'expertise en matière foncière et agricole portant sur des biens meubles et immeubles.

Il intervient notamment dans les domaines suivants :

- estimation immobilière et rurale ;
- rédaction d’actes et conseils juridiques ;
- aménagement du territoire et gestion durable de l’environnement (conseil, appui) ;
- agriculture–productions animales ou végétales ;
- estimation de l’exploitation agricole et de ses composantes ;
- estimation des dommages.

*Pour aller plus loin* : article L. 171-1 du Code rural et de la pêche maritime.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une profession libérale, le CFE compétent est l'Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d'installation

### a. Qualifications professionnelles

L'exercice de la profession d'expert foncier et agricole est réservé aux professionnels inscrits sur la liste nationale des experts fonciers et agricoles.

L'inscription est ouverte à l'intéressé qui remplit les conditions suivantes :

- justifier d'une pratique professionnelle :
  - soit d'au moins sept ans,
  - soit d'au moins trois ans, pour les titulaires d'un titre ou d'un diplôme de niveau au moins égale à quatre années d'études postsecondaires, dans les disciplines agricoles, agronomiques, environnementales, forestières, juridiques ou économiques ;
- ne pas avoir fait l'objet d'une condamnation pour des faits contraires à l'honneur, à la probité ou aux bonnes mœurs, au cours des cinq dernières années ;
- ne pas avoir fait l'objet d'une sanction disciplinaire ou administrative de destitution, radiation, révocation, de retrait d'agrément ou d'autorisation ;
- ne pas avoir été frappé de faillite personnelle.

En cas d’exercice de l’activité en société civile professionnelle (SCP), tous les associés doivent avoir le titre d’expert forestier.

En cas d’exercice de l’activité en société d'exercice libéral (SEL), plus de 50% du capital doivent être détenus par des experts forestiers en exercice.

En cas d’exercice de l’activité en société commerciale, au moins 50% du capital doivent être détenus par des experts forestiers ou des anciens experts sous réserve que ceux-ci n’aient pas été radiés.

*Pour aller plus loin* : articles L. 171-1, R. 171-10 et R. 173-58 du Code rural et de la pêche maritime.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### Pour un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d’un État de l’Union Européenne (UE) ou de l’Espace économique européen (EEE) peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel, sous réserve :

- d'être légalement établi dans l'un de ces États pour y exercer la profession d'expert foncier et agricole ;
- lorsque ni la profession, ni la formation ne sont réglementées dans cet État, d'avoir exercé la profession d'expert foncier et agricole dans cet État pendant au moins un an au cours des dix dernières années ;
- d'être assuré contre les conséquences pécuniaires de sa responsabilité civile professionnelle.

Pour cela, le ressortissant devra en faire la demande, préalablement à sa première prestation, par déclaration adressée au Conseil national de l'expertise foncière, agricole et forestière (CNEFAF) (cf. infra « 3°. c. Le cas, échéant, effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

*Pour aller plus loin* : article L. 171-2 du Code rural et de la pêche maritime.

#### Pour un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE, qui est établi et exerce légalement l'activité d'expert foncier et agricole dans cet État, peut exercer la même activité en France de manière permanente sous réserve de remplir les mêmes conditions que le professionnel français (cf. supra « 2°. a. Qualifications professionnelles »).

Il devra demander son inscription sur la liste nationale des experts fonciers et agricoles auprès du comité du CNEFAF (cf. infra « 3°. b. Demander son inscription sur la liste des experts fonciers et agricoles »).

Si des différences substantielles existent entre la formation et l'expérience professionnelle du ressortissant et celles exigées en France, le comité pourra décider de la soumettre à un mesure de compensation.

*Pour aller plus loin* : articles L. 171-3 et R. 717-10 du Code rural et de la pêche maritime.

### c. Quelques particularités de la réglementation de l'activité

#### Respect du Code de déontologie des experts fonciers et agricoles

Les dispositions du Code de déontologie s'imposent à tous les experts fonciers et agricoles exerçant en France.

À ce titre, l'expert foncier et agricole s'engage à :

- respecter l'indépendance nécessaire à l'exercice de sa profession ;
- se prononcer en toute impartialité ;
- respecter le secret professionnel ;
- s'abstenir de toute pratique déloyale à l'égard de ses confrères.

*Pour aller plus loin* : articles R. 172-1 à R. 172-10 du Code rural et de la pêche maritime.

#### Incompatibilités

La profession d'expert foncier et agricole est incompatible :

- avec les charges d'officiers publics et ministériels ;
- avec toutes fonctions susceptibles de porter atteinte à son indépendance, en particulier, celle consistant à acquérir de façon habituelle des biens mobiliers ou immobiliers en vue de leur revente (ex : négoce, marchand de biens, etc.).

*Pour aller plus loin* : article L. 171-1 du Code rural et de la pêche maritime.

#### Assurance

L'expert foncier et agricole exerçant à titre libéral doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. En effet, dans ce cas, c'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.

*Pour aller plus loin* : article L. 171-1 alinéa 8 du Code rural et de la pêche maritime.

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Les locaux étant ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre du commerce et des sociétés (RCS), effectuer une déclaration auprès de l'Urssaf ou auprès du Greffe du tribunal de commerce. Pour de plus amples informations, il est conseillé de se renseigner auprès du CFE compétent.

### b. Demander son inscription sur la liste des experts fonciers et agricoles

#### Autorité compétente

Le comité de la CNEFAF est compétent pour se prononcer sur la demande d'inscription sur la liste des experts fonciers et agricoles.

#### Pièces justificatives

La demande se fait par l'envoi d'un dossier par courrier recommandé avec accusé de réception, comprenant les pièces justificatives suivantes :

- une pièce d'identité en cour de validité ;
- une copie du titre de formation ou du diplôme permettant au demandeur d'exercer son activité ;
- toute attestation justifiant de la pratique de la profession en France ou, le cas échéant, dans l’État de l'UE ou de l'EEE ;
- un curriculum vitae dans lequel sont indiquées les activités professionnelles que le candidat a exercées antérieurement avec l'indication des dates et lieux d'exercice ;
- une justification ou, à défaut, un engagement de souscription d'une police d'assurance de responsabilité civile professionnelle ;
- un extrait de casier judiciaire n° 3 datant de moins de trois mois ou tout document équivalent délivré par l'autorité compétente de l’État de l'UE ou de l'EEE de moins de trois mois ;
- une déclaration sur l'honneur ou tout autre moyen de preuve attestant que l'intéressé remplit les conditions d'honorabilité ;
- le cas échéant, une déclaration de l'activité envisagée sous forme sociétaire.

#### Procédure

À réception du dossier, le comité dispose de trois mois pour informer le demandeur de sa décision de l'inscrire sur la liste ou non.

Toutefois, lorsque le demandeur est un ressortissant de l'UE ou de l'EEE et qu'il existe des différences substantielles entre sa formation et son expérience professionnelles et celles exigées en France, le comité pourra le soumettre à la mesure de compensation de son choix, soit à un stage d'adaptation, soit à une épreuve d'aptitude.

En tout état de cause, le silence gardé pendant un délai de trois mois vaut acceptation de la décision.

Le renouvellement de la demande est soumis à la production de l'attestation de l'assurance de responsabilité civile professionnelle.

#### Le cas des personnes morales

Les SCP et les SEL de l'expertise foncière et agricole doivent être inscrites sur la liste nationale. La demande d'inscription doit comprendre les pièces justificatives suivantes :

- un exemplaire des statuts et, le cas échéant, le règlement intérieur ;
- les documents d'état civil des associés ;
- pour les SEL, la justification que les experts fonciers et agricoles travaillant au sein de la société remplissent les conditions d’exercice de la profession, ainsi que la répartition du capital entre les associés ;
- pour les SCP, les documents nécessaires à la demande d’inscription des associés non encore inscrits sur la liste national.

**À savoir**

Leur inscription doit être effectuée avant d'accomplir les formalités auprès du CFE compétent.

*Pour aller plus loin* : articles R. 171-10 à R. 171-13 du Code rural et de la pêche maritime.

### c. Le cas échéant, effectuer une déclaration préalable d’activité pour le ressortissant de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le CNEFAF est compétent pour délivrer la déclaration préalable d'activité.

#### Pièces justificatives

La demande se fait par l'envoi d'un dossier à l'autorité compétente, comprenant les pièces justificatives suivantes :

- une pièce d'identité ;
- une attestation justifiant qu'il est légalement établi dans un État de l'UE ou de l'EEE ;
- une preuve que le ressortissant a exercé les activités d'expertise foncière et agricole pendant au moins un an au cours des dix années précédentes ;
- une attestation d'assurance de responsabilité civile.

**À savoir**

Les pièces justificatives doivent être rédigées en langue française ou traduites par un traducteur agréé.

*Pour aller plus loin* : articles R. 171-17- à R. 171-17-3, et R 173-57 du Code rural et de la pêche maritime.