﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS041" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="Agricultural and real estate appraiser" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="agricultural-and-real-estate-appraiser" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/expertise/agricultural-and-real-estate-appraiser.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="agricultural-and-real-estate-appraiser" -->
<!-- var(translation)="Auto" -->


Agricultural and real estate appraiser
============================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The land and agricultural expert is a professional who carries out missions of expertise in land and agriculture dealing with property and buildings.

It is particularly active in the following areas:

- real estate and rural estimate;
- drafting acts and legal advice;
- land use and sustainable environmental management (advice, support);
- agriculture-animal or plant production;
- Estimating the farm and its components;
- estimate of the damage.

*To go further* Article L. 171-1 of the Rural Code and Marine Fisheries.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for a liberal profession, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The practice of the profession of land and agricultural expert is reserved for professionals on the national list of land and agricultural experts.

Registration is open to the individual who meets the following conditions:

- justify a professional practice:- at least seven years,
  - or at least three years, for holders of a degree at least four years of post-secondary education, in the agricultural, agronomic, environmental, forestry, legal or economic disciplines;
- not having been convicted for facts contrary to honour, probity or good morals in the past five years;
- not disciplinary or administrative sanction for dismissal, dismissal, dismissal, withdrawal of accreditation or authorization;
- have not been hit with personal bankruptcy.

In the event of a professional civil society (PCS) activity, all partners must have the title of forest expert.

In the event of the exercise of the activity in a liberal holding company (SEL), more than 50% of the capital must be held by practising forest experts.

In the event of a commercial company activity, at least 50% of the capital must be held by forest experts or former experts provided that they have not been written off.

*To go further* Articles L. 171-1, R. 171-10 and R. 173-58 of the Rural Code and Marine Fisheries.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

#### For a temporary and casual exercise (Freedom to provide services)

The national of a European Union (EU) or European Economic Area (EEA) state may use his or her professional title in France, either temporarily or occasionally, subject to:

- to be legally established in one of these states to practise as a land and agricultural expert;
- where neither the profession nor training is regulated in that state to have practised as a land and agricultural expert in that state for at least one year in the last ten years;
- to be insured against the pecuniary consequences of his professional civil liability.

In order to do so, the national will have to apply, prior to his first benefit, by declaration addressed to the National Council of Land, Agricultural and Forestry Expertise (CNEFAF) (see infra "3°. c. If necessary, make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)").

*To go further* Article L. 171-2 of the Rural Code and Marine Fisheries.

#### For a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state, who is established and legally practises as a land and agricultural expert in that state, may carry out the same activity in France on a permanent basis subject to meeting the same conditions as the French professional (see supra "2.00. a. Professional qualifications").

He will have to apply to the CNEFAF committee for his inclusion on the national list of land and agricultural experts (see infra "3o). b. Ask for inclusion on the list of land and agricultural experts").

If there are substantial differences between the training and professional experience of the national and those required in France, the committee may decide to subject it to a compensation measure.

*To go further* Articles L. 171-3 and R. 717-10 of the Rural Code and Marine Fisheries.

### c. Some peculiarities of the regulation of the activity

#### Compliance with the Code of Ethics for Land and Agricultural Experts

The provisions of the Code of Ethics are imposed on all land and agricultural experts practising in France.

As such, the land and agricultural expert is committed to:

- Respect the independence necessary to carry out one's profession;
- make an impartial statement;
- Respect professional secrecy
- refrain from any unfair practice towards his colleagues.

*To go further* Articles R. 172-1 to R. 172-10 of the Rural Code and Marine Fisheries.

#### Incompatibilities

The profession of land and agricultural expert is incompatible:

- with the offices of public and ministerial officers;
- with all functions that could impair its independence, in particular, that of acquiring personal or real estate in a usual way for resale (e.g. trading, merchant of goods, etc.).

*To go further* Article L. 171-1 of the Rural Code and Marine Fisheries.

#### Insurance

The liberal land and agricultural expert must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

*To go further* Article L. 171-1 paragraph 8 of the Rural Code and Marine Fisheries.

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

As the premises are open to the public, the professional must comply with the rules relating to public institutions (ERP):

- Fire
- accessibility.

It is advisable to refer to the "Establishment receiving the public" sheet for more information.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of his activity, the contractor must register with the Register of Trade and Companies (RCS), make a declaration with the Urssaf or with the Registry of the Commercial Court. For more information, it is advisable to check with the relevant CFE and in case of the creation of a commercial company, refer to the sheets "Formality of reporting a commercial company" and "Registration of a company individual commercial at the RCS."

### b. Ask for inclusion on the list of land and agricultural experts

**Competent authority**

The CNEFAF committee is responsible for deciding on the application for inclusion on the list of land and agricultural experts.

**Supporting documents**

The request is made by sending a file by registered mail with acknowledgement, including the following supporting documents:

- A valid piece of identification
- A copy of the training title or diploma allowing the applicant to carry out his activity;
- any evidence justifying the practice of the profession in France or, if necessary, in the EU or EEA State;
- A resume that indicates the candidate's previous professional activities with the dates and locations of the exercise;
- a justification or, failing that, a commitment to underseed a professional liability insurance policy;
- a criminal record extract no. 3 less than three months old or any equivalent document issued by the competent authority of the EU State or the EEA of less than three months;
- a statement of honour or any other evidence that the person is meeting the conditions of honour;
- if necessary, a declaration of the activity envisaged in company form.

**Procedure**

Upon receipt of the file, the committee has three months to inform the applicant of his decision whether or not to list him or her.

However, where the applicant is an EU or EEA national and there are substantial differences between his professional training and experience and those required in France, the committee may subject him to the choice, either for an adjustment course or for an aptitude test.

In any event, the silence kept for a period of three months is worth accepting the decision.

The renewal of the application is subject to the production of the certificate of professional liability insurance.

**The case of legal persons**

CPS and SELs of land and agricultural expertise must be included on the national list. The application must include the following supporting documents:

- A copy of the statutes and, if necessary, the internal regulations;
- The associates' civil registration documents;
- for THE SEL, the justification that the land and agricultural experts working in the company meet the conditions of practice of the profession, as well as the distribution of capital between the partners;
- for CPS, the documents necessary to apply for the registration of partners not yet on the national list.

**What to know**

They must be registered before completing the formalities with the relevant CFE.

*To go further* Articles R. 171-10 to R. 171-13 of the Rural and Marine Fisheries Code.

### c. If necessary, make a prior declaration of activity for the EU or EEA national engaged in temporary and occasional activity (LPS)

**Competent authority**

The CNEFAF is responsible for issuing the prior declaration of activity.

**Supporting documents**

The request is made by sending a file to the appropriate authority, including the following supporting documents:

- ID
- a certificate justifying that it is legally established in an EU or EEA state;
- proof that the national has carried out land and agricultural expertise activities for at least one year in the previous ten years;
- a certificate of liability insurance.

**What to know**

Supporting documents must be written in French or translated by a certified translator.

*To go further* Articles R. 171-17- to R. 171-17-3, and R 173-57 of the Rural Code and Marine Fisheries.

