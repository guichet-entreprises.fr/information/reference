﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS065" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="Vehicle inspection centre" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="vehicle-inspection-centre" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/expertise/vehicle-inspection-centre.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="vehicle-inspection-centre" -->
<!-- var(translation)="Auto" -->


Vehicle inspection centre
===================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The Automotive Technical Control Centre is an establishment responsible for checking the working condition of light (less than 3.5 tons) and heavy vehicles (total load weight greater than 3.5 tonnes).

The center can be integrated into a control network or not.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a craft profession, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

In order to operate an automotive technical control centre, the person must either have completed a 35-hour training course in a training organization, or must have an internship certificate justifying the completion of one of the training to enable access to the profession of automotive technical controller.

To be a technical controller of light vehicles, the individual must have a Certificate of Professional Qualification (CQP) or a professional designation as a technical controller of light vehicles or meeting the following requirements:

#### Qualifications acquired in France 

The person concerned must justify one of the following qualifications:

- a Level IV diploma from the Ministry of Education (Professional Vehicle Maintenance Bachelor option private cars or bachelor's degree in professional vehicle maintenance option industrial vehicles or vehicles road transport) or a diploma equivalent to the National Directory of Professional Certifications (RNCP);
- a Level III diploma from the Department of Education (automotive expert diploma or graduate car technician certificate option "special vehicles" option or superior technician's patent after-sales auto option " (industrial vehicles")," or an equivalent degree to the National Directory of Professional Certifications (RNCP). ;
- and initial training on the technical control of light vehicles, consisting of a theoretical part in a training centre of at least 245 hours and a practical part in a specialized centre of at least 70 hours.

#### Qualifications acquired in another EU member state or in a state party to the European Economic Area agreement 

The person concerned must justify prior training sanctioned by a certificate recognized by the State of origin or deemed fully valid by a competent professional body and an experience of three years, in the previous ten years, in technical controller of light vehicles.

*To be a technical controller of heavy vehicles*, the individual must justify a Certificate of Professional Qualification (CQP) or a professional designation as a heavy vehicle technical controller or meet the following requirements:

#### Qualifications acquired in France 

The person concerned must justify one of the following qualifications:

- a Level IV diploma from the Ministry of National Education (professional baccalaureate "vehicle maintenance" options "private cars" or "industrial vehicles") or a diploma equivalent to the national directory of certifications (RNCP);
- a Level III diploma from the Ministry of National Education (automotive expert diploma, senior technician certificate "automotive after-sales" options "special vehicles" or "industrial vehicles" or senior technician's certificate " vehicle maintenance" options "private cars" or "road transport vehicles") or an equivalent diploma to the National Directory of Professional Certifications (RNCP);
- and initial training on the technical control of heavy vehicles, consisting of a theoretical part in a training centre of at least 245 hours and a practical part in a technical control centre for heavy vehicles of at least 105 hours.

#### Qualifications acquired in another EU Member State or in a State party to the European Economic Area Agreement 

The person concerned must justify prior training sanctioned by a certificate recognized by the State of origin or deemed fully valid by a competent professional body and an experience of three consecutive years, during the ten years as a technical controller for heavy vehicles.

*To go further* Appendix IV of the 18 June 1991 decree on the establishment and organisation of the technical control of vehicles weighing no more than 3.5 tonnes; Appendix IV of the 27 July 2004 order on the technical control of heavy vehicles.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

#### For a temporary and casual exercise (LPS)

The national of an EU or EEA state, legally established in one of these states, may use his title of technical controller in France, either on a temporary or casual basis.

He will have to report it to the prefect of the department in which he plans to carry out his activity (see infra "3o). b. Make a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)).

Where neither the profession nor the training is regulated in that state, he must have been engaged in this activity for at least one year in the last ten years.

*To go further* Article L. 323-1 of the Highway Traffic Act.

#### For a permanent exercise (LE)

The national of an EU or EEA state, who is established and legally practises as a technical controller in that state, may carry out the same activity in France on a permanent basis.

The formalities and procedures applicable to the EU or EEA national are the same as for the French national.

### c. Conditions of honorability

The automotive technical controller must not have been the subject of a conviction on the second ballot of the criminal record.

In addition, the controller must not engage in repair or automotive trade, either as an independent or an employee.

The activity of the technical control centre must also not be accompanied by a car vehicle repair or sale activity.

*To go further* Articles L. 323-1 and R. 323-13 of the Highway Traffic Act.

### d. Some peculiarities of the regulation of the activity

#### Compliance with safety and accessibility standards

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*To go further* : order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

It is advisable to refer to the "Establishment receiving the public" sheet for more information.

#### Posting requirement

Operators of technical control centres are required to display the sign attesting to prefectural accreditation in plain sight.

*To go further* Appendix 1 of Appendix V of the 18 June 1991 decree on the establishment and organisation of technical control of vehicles weighing no more than 3.5 tonnes; Appendix V of the Order of 27 July 2004.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The contractor must register with the Trade and Corporate Register (SCN). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Make a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)

**Competent authority**

The prefect of the department in which the national wishes to carry out a technical check is competent to decide on the request for a prior declaration of activity.

**Supporting documents**

The application is made by filing a file that includes the following documents:

- A photocopy of a valid ID
- A certificate certifying that the national is legally established in the EU or EEA State;
- proof of his professional qualifications
- proof by any means that the national has been a technical controller for at least 1 year in the last ten years;
- a copy of the employment contract or a letter of commitment from the employer control centre.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

The prefect of the department will have one month to send a receipt of the declaration to the national. If the prefect refuses, the national will be able to file a legal challenge before the administrative court within two months of notification of the decision.

### c. Follow the installation preparation course (SPI)

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including a degree in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

Internship exemption for EU or EEA nationals: as a matter of principle, a qualified professional who is eu's or EEA national is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who are:

- have been engaged in a professional activity requiring a level of knowledge equivalent to that provided by the internship for at least three years;
- have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, fully or partially, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France to run a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*To go further* Article 2 of Act 82-1091 of 23 December 1982, Article 6-1 of Decree 83-517 of 24 June 1983.

### d. Seek prefectural approval for the technical controller

#### For light vehicles not exceeding 3.5 tons

**Competent authority**

The prefect of the department is competent to decide on the application for accreditation.

**Supporting documents**

The application for prefectural approval is made by filing a double copy file including the following supporting documents:

- an application for certification as a controller, indicating the control centre (as well as the approved control network to which it is eventually attached) in which the applicant intends to carry out his activity in the main capacity, and specifying in what capacity ( operator or employee);
- Bulletin 2 of his criminal record showing that the applicant was not convicted (document directly requested by the prefect from the national criminal record);
- A valid copy of a document to justify the identity of the controller;
- the supporting documents for the qualification required to carry out the controller activity (see Appendix IV) accompanied by a summary sheet consistent with the appendix 1 model of this appendix. If a foreign national is a foreign national, he or she must provide an equivalent document prepared less than three months ago on the date of the application for accreditation and written in French or accompanied by an official translation;
- the opinion of the approved control network on which the applicant depends, or in the case of a controller not attached to a network, the opinion of the central technical body, following the appendix 2 model of this appendix;
- If the controller is an employee, a copy of the employment contract or a letter of commitment from the employer control centre;
- a statement of honour, following the appendix 3 model of this appendix, certifying the accuracy of the information provided, certifying that it is not subject to a suspension or withdrawal of accreditation, and undertaking not to exercise, during the duration of the certification, any activity in the repair or auto trade and not to use the results of the checks for any purpose other than those provided for by the regulations.

**Procedure**

The prefect's silence within four months is worth accepting the application for accreditation.

#### For heavy vehicles

**Competent authority**

The prefect of the department is competent to decide on the application for accreditation.

**Supporting documents**

The application for prefectural approval is made by filing a file in two copies including the following supporting documents:

- Applying for certification as a controller
- A copy of a valid ID
- A copy of bulletin 2 of the criminal record;
- A summary sheet of the controller's experience and qualification;
- A statement of honour certifying the accuracy of the information provided;
- the opinion of the approved control network on which the applicant depends or in the case of a controller not attached to a network, the opinion of the central technical body.

### e. Seek prefectural approval for the technical control centre

#### For heavy vehicles

**Competent authority**

The prefect of the department of the place of establishment of the center is competent to decide on the application for accreditation.

**Supporting documents**

The application for accreditation must be submitted in two copies for a control centre attached to a network, and three copies for a non-networked centre.

For the control centre attached to a network, the parts are:

- Application for approval on letterhead;
- a certification of the control network, following the appendix 5 model of this appendix, certifying that the facilities have been subject to a favourable initial audit (with indication of the date and reference of the report) and that the file is in accordance with the requirements in this chapter, and a copy of the original audit report;
- the specifications referred to in Article R. 323-14 of the Highway Traffic Act include:- A description of the organization and material means, following the appendix 7 model of this appendix and the list of related controllers,
  - A situation plan to identify the real estate right-of-way and the control zone in relation to the environment,
  - a 1/100-scale mass plan showing all covered surfaces and indicating the location of the control equipment,
  - the applicant's commitment, following the appendix 6 model of this appendix,
  - The model of the minutes that will be used in the control centre;
- Copying the network approval notification for the technical control of heavy vehicles;
- the proof that the facility is part of the network's accreditation perimeter in accordance with Articles 22 and 32 of this order or a receipt issued by the accrediting body indicating that the application for integration of the facility within the network's accreditation perimeter has been filed;
- applicant's commitment to comply with the above specifications.

For an independent control centre, the parts are:

- An application for approval on letterhead
- a proof relating to registration in the trades directory or the register of trades and companies dating back less than three months, on which is identified the establishment corresponding to the control centre;
- a copy of the certificate of accreditation of the operating individual or corporation or a receipt issued by the accrediting body as stipulated in Article 22 of this order certifying that the centre has filed, for its certification, its system full quality and in accordance with the standard NF EN ISO/CIS 17020: 2012;
- the specifications referred to in Article R. 323-14 of the Highway Traffic Act include:- A description of the organization and material means, following the appendix 7 model of this appendix and the list of related controllers,
  - A situation plan to identify the real estate right-of-way and the control zone in relation to the environment,
  - a 1/100-scale mass plan showing all covered surfaces and indicating the location of the control equipment,
  - the applicant's commitment, following the appendix 6 model of this appendix,
  - procedures in paragraph 1.2 of Appendix V of this order,
  - The model of the minutes that will be used in the control centre;
- Technical references to assess the applicant's experience in technical control;
- The favourable initial audit report prepared by an agency approved by the Minister for Transport;
- the opinion of the central technical body following the appendix 8 model of this appendix (notice directly requested by the prefect from the central technical body receiving the application for accreditation);
- The certification of compliance with the computer tool issued by the CTA under the provisions of Article 37 of this order;
- applicant's commitment to comply with the above specifications.

*To go further* : order of 27 July 2004 relating to the technical control of heavy vehicles.

#### For light vehicles not exceeding 3.5 tons

**Competent authority**

The prefect of the department of the place of establishment of the center is competent to decide on the application for accreditation.

**Supporting documents**

The application for accreditation must be submitted in three copies and must include the following supporting documents.

For a network control centre, the parts are:

- An application for approval on letterhead
- a proof relating to registration in the trades directory or the register of trades and companies dating back less than three months, on which is identified the establishment corresponding to the control centre;
- A certification of membership in an approved control network, following the appendix 5 model of this appendix;
- a certification from the control network certifying that the control centre has been the subject of a favourable initial audit (with indication of the date and reference of the report) and that the file complies with the requirements of this chapter, and a copy of the Report of the initial audit;
- the specifications referred to in the 2nd paragraph of Article R. 323-14 of the Highway Traffic Act include:- A description of the organization and material means, following the appendix 6 model of this appendix and the list of related controllers,
  - A situation plan to identify the real estate right-of-way and the control zone in relation to the environment,
  - a 1/100th scale mass plan showing all covered surfaces and indicating the location of the control equipment,
  - the applicant's commitment, following the appendix 4 model of this appendix,
  - The model of the minutes that will be used in the control centre,
  - applicant's commitment to comply with the above specifications.

For an independent control centre:

- An application for approval on letterhead
- a proof relating to registration in the trades directory or the register of trades and companies dating back less than three months, on which is identified the establishment corresponding to the control centre;
- A favourable initial audit report prepared by an accredited body;
- the opinion of the central technical body, following the model of Appendix 7 of this appendix (notice directly requested by the prefect to the central technical body receiving the application for accreditation);
- the specifications referred to in the 2nd paragraph of Article R. 323-14 of the Highway Traffic Act include:- A description of the organization and material means, following the appendix 6 model of this appendix and the list of related controllers,
  - A situation plan to identify the real estate right-of-way and the control zone in relation to the environment,
  - A 1/100th scale mass plan showing all covered surfaces and indicating the location of control equipment;
- applicant's commitment, following the appendix 4 model of this appendix:- establish all documents, relating to his activity, prescribed by the Minister responsible for transport,
  - facilitate the mission of the officers appointed by the latter to monitor the proper functioning of the control centres,
  - to sign the technical assistance agreement provided for at article 29 of this order;
- the internal procedures of the control centre to ensure compliance with the requirements of Article R. 323-14 of the Aforementioned Highway Traffic Act, as well as paragraph 1 of Chapter II of Title II of this order, including:- approval and authorisation of a technical controller,
  - organisation of training and qualification of technical controllers,
  - mastery of technical control software,
  - integrity, security and maintenance of the computer system,
  - management, maintenance and maintenance of control equipment,
  - transmission of data on technical controls carried out,
  - exploitation of the indicators provided by the central technical body,
  - audit of control facilities and controllers,
  - management and archiving of technical control minutes,
  - organisation and conduct of technical checks,
  - alternative methods of testing in the event of an impossibility of control,
  - treatment of amicable remedies available to the public,
  - management of the documentary basis of the regulatory texts and their evolution,
  - management of gas-specific tools for the centres concerned;
- The certification of compliance with the computer tool issued by the CTA under the provisions of Article 29 of this order;
- The model of the minutes that will be used in the control centre;
- applicant's commitment to comply with the above specifications.

*To go further* : order of 18 June 1991 relating to the establishment and organisation of the technical control of vehicles weighing no more than 3.5 tonnes.

