﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS065" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="Centre de contrôle technique automobile" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="centre-de-controle-technique-automobile" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/expertise/centre-de-controle-technique-automobile.html" -->
<!-- var(last-update)="2018-07" -->
<!-- var(url-name)="centre-de-controle-technique-automobile" -->
<!-- var(translation)="None" -->

# Centre de contrôle technique automobile

Dernière mise à jour : <!-- begin-var(last-update) -->2018-07<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le centre de contrôle technique automobile est un établissement chargé de vérifier l'état de marche de véhicules légers (moins de 3,5 tonnes) et lourds (poids total en charge supérieur à 3,5 tonnes).

Le centre peut être intégré à un réseau de contrôle ou non.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une profession artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d'industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour être exploitant d'un centre de contrôle technique automobile, l'intéressé doit soit avoir suivi une formation d'une durée de 35 heures dans un organisme de formation, soit être titulaire d'une attestation de stage justifiant la réalisation d’une des formations permettant l’accès à la profession de contrôleur technique automobile.

Pour être contrôleur technique de véhicules légers, l'intéressé doit justifier d'un certificat de qualification professionnelle (CQP) ou d'un titre professionnel de contrôleur technique de véhicules légers ou répondant aux exigences suivantes :

#### Qualifications acquises en France 

L'intéressé doit justifier d'une des qualifications visées ci-dessous :

- un diplôme de niveau IV du ministère de l'Éducation nationale (baccalauréat professionnel maintenance des véhicules option voitures particulières ou baccalauréat professionnel maintenance des véhicules option véhicules industriels ou véhicules de transport routier) ou un diplôme équivalent au regard du Répertoire national des certifications professionnelles (RNCP) ;
- un diplôme de niveau III du ministère de l’Éducation nationale (diplôme d’expert en automobile ou brevet de technicien supérieur après-vente automobile option « véhicules particuliers » ou brevet de technicien supérieur après-vente automobile option « véhicules industriels ») ou un diplôme équivalent vis-à-vis du Répertoire national des certifications professionnelles (RNCP). ;
- et d’une formation initiale relative au contrôle technique des véhicules légers, constituée d’une partie théorique en centre de formation d’au minimum 245 heures et d’une partie pratique en centre spécialisé d’au minimum 70 heures.

#### Qualifications acquises dans un autre Etat membre de l'Union européenne ou dans un État partie à l'accord sur l'Espace économique européen 

L’intéressé doit justifier d’une formation préalable sanctionnée par un certificat reconnu par l’État d’origine ou jugée pleinement valable par un organisme professionnel compétent et d’une expérience de trois années, au cours des dix années précédentes, en tant que contrôleur technique des véhicules légers.

*Pour être contrôleur technique de véhicules lourds*, l’intéressé doit justifier d'un certificat de qualification professionnelle (CQP) ou d'un titre professionnel de contrôleur technique de véhicules lourds ou répondre aux exigences suivantes :

#### Qualifications acquises en France 

L’intéressé doit justifier d'une des qualifications visées ci-dessous :

- un diplôme de niveau IV du ministère de l’éducation nationale (baccalauréat professionnel « maintenance des véhicules » options « voitures particulières » ou « véhicules industriels ») ou un diplôme équivalent au regard du Répertoire national des certifications professionnelles (RNCP) ;
- un diplôme de niveau III du ministère de l’éducation nationale (diplôme d’expert en automobile, brevet de technicien supérieur « après-vente automobile » options « véhicules particuliers » ou « véhicules industriels » ou brevet de technicien supérieur « maintenance des véhicules » options « voitures particulières » ou « véhicules de transport routier ») ou un diplôme équivalent vis-à-vis du Répertoire national des certifications professionnelles (RNCP) ;
- et d'une formation initiale relative au contrôle technique des véhicules lourds, constituée d'une partie théorique en centre de formation d'au minimum 245 heures et d'une partie pratique en centre de contrôle technique de véhicules lourds d'au minimum 105 heures.

#### Qualifications acquises dans un autre État membre de l'Union européenne ou dans un État partie à l'accord sur l'Espace économique européen 

L’intéressé doit justifier d'une formation préalable sanctionnée par un certificat reconnu par l'Etat d'origine ou jugée pleinement valable par un organisme professionnel compétent et d'une expérience de trois années consécutives, au cours des dix années précédentes, en tant que contrôleur technique des véhicules lourds.

*Pour aller plus loin* : annexe IV de l'arrêté du 18 juin 1991 relatif à la mise en place et à l'organisation du contrôle technique des véhicules dont le poids n'excède pas 3,5 tonnes ; annexe IV de l'arrêté du 27 juillet 2004 relatif au contrôle technique des véhicules lourds.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### Pour un exercice temporaire et occasionnel (LPS)

Le ressortissant d'un État de l'UE ou de l'EEE, légalement établi dans l'un de ces États, peut faire usage de son titre de contrôleur technique en France, à titre temporaire ou occasionnel.

Il devra, préalablement à sa première prestation, en faire la déclaration auprès du préfet du département dans lequel il envisage d'exercer son activité (cf. infra « 3°. b. Effectuer une déclaration préalable d’activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS) »).

Lorsque ni la profession, ni la formation ne sont réglementées dans cet État, il devra avoir exercé cette activité pendant au moins un an au cours des dix dernières années.

*Pour aller plus loin* : article L. 323-1 du Code de la route.

#### Pour un exercice permanent (LE)

Le ressortissant d'un État de l'UE ou de l'EEE, qui est établi et exerce légalement l'activité de contrôleur technique dans cet État, peut exercer la même activité en France de manière permanente.

Les formalités et démarches applicables au ressortissant de l'UE ou de l'EEE sont les mêmes que pour le ressortissant français.

### c. Conditions d'honorabilité

Le contrôleur technique automobile ne doit pas avoir fait l'objet d'une condamnation inscrite au bulletin n° 2 du casier judiciaire.

De plus, le contrôleur ne doit pas exercer une activité de réparateur ou de commerce automobile, que ce soit à titre indépendant ou salarié.

L'activité du centre de contrôle technique ne doit pas non plus être accompagnée d'une activité de réparation ou de vente de véhicule automobile.

*Pour aller plus loin* : articles L. 323-1 et R. 323-13 du Code de la route.

### d. Quelques particularités de la réglementation de l'activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

#### Obligation d'affichage

Les exploitants des centres de contrôle technique ont l'obligation d'afficher à la vue du public le panneau attestant de l'agrément préfectoral.

*Pour aller plus loin* : appendice 1 de l'annexe V de l'arrêté du 18 juin 1991 relatif à la mise en place et à l'organisation du contrôle technique des véhicules dont le poids n'excède pas 3,5 tonnes ; annexe V de l'arrêté du 27 juillet 2004.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

L’entrepreneur doit s’immatriculer au registre du commerce et des sociétés (RCS).

### b. Effectuer une déclaration préalable d’activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le préfet du département dans lequel le ressortissant souhaite effectuer un contrôle technique est compétent pour se prononcer sur la demande de déclaration préalable d'activité.

#### Pièces justificatives

La demande s'effectue par le dépôt d'un dossier comprenant les documents suivants :

- une photocopie d'une pièce d'identité en cours de validité ;
- une attestation certifiant que le ressortissant est légalement établi dans l'État de l'UE ou de l'EEE ;
- une preuve de ses qualifications professionnelles ;
- la preuve par tout moyen que le ressortissant a exercé l'activité de contrôleur technique pendant au moins 1 ans au cours des dix dernières années ;
- une copie du contrat de travail ou une lettre d'engagement du centre de contrôle employeur.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

Le préfet du département aura un mois pour adresser un récépissé de la déclaration au ressortissant. En cas de refus du préfet, le ressortissant pourra former un recours contentieux devant le tribunal administratif dans un délai de deux mois suivant la notification de la décision.

### c. Solliciter un agrément préfectoral pour le contrôleur technique

#### Pour les véhicules légers n'excédant pas 3,5 tonnes

##### Autorité compétente

Le préfet de département est compétent pour se prononcer sur la demande d'agrément.

##### Pièces justificatives

La demande d'agrément préfectoral se fait par le dépôt d'un dossier en double exemplaire comprenant les pièces justificatives suivantes :

- une demande d’agrément en tant que contrôleur, indiquant le centre de contrôle (ainsi que le réseau de contrôle agréé auquel il est éventuellement rattaché) dans lequel le demandeur compte exercer son activité à titre principal, et précisant en quelle qualité (exploitant ou salarié) ;
- le bulletin n° 2 de son casier judiciaire faisant apparaître que le demandeur n’a fait l’objet d’aucune condamnation (document directement requis par le préfet auprès du casier judiciaire national) ;
- la copie d’un document, en cours de validité, permettant de justifier de l’identité du contrôleur ;
- les pièces justificatives de la qualification requise pour exercer l’activité de contrôleur (cf. annexe IV) accompagnées d’une fiche récapitulative conforme au modèle de l’appendice 1 de la présente annexe. S’il s’agit d’un ressortissant étranger, celui-ci doit fournir un document équivalent établi depuis moins de trois mois à la date de la demande d’agrément et rédigé en français ou accompagné d’une traduction officielle ;
- l’avis du réseau de contrôle agréé dont le demandeur dépend, ou dans le cas d’un contrôleur non rattaché à un réseau, l’avis de l’organisme technique central, suivant le modèle de l’appendice 2 de la présente annexe ;
- si le contrôleur est salarié, une copie du contrat de travail ou bien une lettre d’engagement du centre de contrôle employeur ;
- une déclaration sur l’honneur, suivant le modèle de l’appendice 3 de la présente annexe, certifiant l’exactitude des renseignements fournis, attestant ne pas être sous le coup d’une mesure de suspension ou de retrait d’agrément, s’engageant à ne pas exercer, pendant la durée de l’agrément, une quelconque activité dans la réparation ou le commerce automobile et à ne pas utiliser les résultats des contrôles à d’autres fins que celles prévues par la réglementation.

##### Procédure

Le silence du préfet dans un délai de quatre mois vaut acceptation de la demande d'agrément.

#### Pour les véhicules lourds

##### Autorité compétente

Le préfet de département est compétent pour se prononcer sur la demande d'agrément.

##### Pièces justificatives

La demande d'agrément préfectoral se fait par le dépôt d'un dossier en deux exemplaires comprenant les pièces justificatives suivantes :

- une demande d'agrément en tant que contrôleur ;
- une copie d'une pièce d'identité en cours de validité ;
- une copie du bulletin n° 2 du casier judiciaire ;
- une fiche récapitulative de l'expérience et de la qualification du contrôleur ;
- une déclaration sur l'honneur certifiant l'exactitude des renseignements fournis ;
- l'avis du réseau de contrôle agréé dont le demandeur dépend ou dans le cas d'un contrôleur non rattaché à un réseau, l'avis de l'organisme technique central.

### d. Solliciter un agrément préfectoral pour le centre de contrôle technique

#### Pour les véhicules lourds

##### Autorité compétente

Le préfet de département du lieu d'implantation du centre est compétent pour se prononcer sur la demande d'agrément.

##### Pièces justificatives

Le dossier de demande d'agrément doit être transmis en deux exemplaires pour un centre de contrôle rattaché à un réseau, et en trois exemplaires pour un centre non rattaché à un réseau.

Pour le centre de contrôle rattaché à un réseau, les pièces sont les suivantes :

- la demande d'agrément sur papier en-tête ;
- une attestation du réseau de contrôle, suivant le modèle de l'appendice 5 de la présente annexe, certifiant que les installations ont fait l'objet d'un audit initial favorable (avec indication de la date et de la référence du rapport) et que le dossier est conforme aux prescriptions du présent chapitre, et une copie du rapport de l'audit initial ;
- le cahier des charges visé au I de l'article R. 323-14 du Code de la route comprenant notamment :
  - une description de l'organisation et des moyens matériels, suivant le modèle de l'appendice 7 de la présente annexe ainsi que la liste des contrôleurs rattachés,
  - un plan de situation permettant d'identifier l'emprise immobilière et la zone de contrôle par rapport à l'environnement,
  - un plan de masse à l'échelle 1/100 faisant apparaître l'ensemble des surfaces couvertes et indiquant l'emplacement des matériels de contrôle,
  - l'engagement du demandeur, suivant le modèle de l'appendice 6 de la présente annexe,
  - le modèle du procès-verbal qui sera utilisé dans le centre de contrôle ;
- la copie de la notification d'agrément du réseau pour le contrôle technique des véhicules lourds ;
- le justificatif indiquant que l'installation fait partie du périmètre d'accréditation du réseau conformément aux articles 22 et 32 du présent arrêté ou un récépissé délivré par l'organisme accréditeur indiquant que la demande d'intégration de l'installation dans le périmètre d'accréditation du réseau a été déposée ;
- l'engagement du demandeur à respecter le cahier des charges susvisé.

Pour un centre de contrôle indépendant, les pièces sont les suivantes :

- une demande d'agrément sur papier à en-tête ;
- un justificatif relatif à l’immatriculation au répertoire des métiers ou au registre du commerce et des sociétés datant de moins de trois mois, sur lequel est identifié l’établissement correspondant au centre de contrôle ;
- la copie du certificat d'accréditation de la personne physique ou morale exploitante ou un récépissé délivré par l'organisme accréditeur tel que prévu à l'article 22 du présent arrêté attestant que le centre a déposé, en vue de son accréditation, son système qualité complet et conforme à la norme NF EN ISO/CEI 17020 : 2012 ;
- le cahier des charges visé au I de l'article R. 323-14 du Code de la route comprenant notamment :
  - une description de l'organisation et des moyens matériels, suivant le modèle de l'appendice 7 de la présente annexe ainsi que la liste des contrôleurs rattachés,
  - un plan de situation permettant d'identifier l'emprise immobilière et la zone de contrôle par rapport à l'environnement,
  - un plan de masse à l'échelle 1/100 faisant apparaître l'ensemble des surfaces couvertes et indiquant l'emplacement des matériels de contrôle,
  - l’engagement du demandeur, suivant le modèle de l’appendice 6 de la présente annexe,
  - les procédures prévues au paragraphe 1.2 de l'annexe V du présent arrêté,
  - le modèle du procès-verbal qui sera utilisé dans le centre de contrôle ;
- les références techniques permettant d'apprécier l'expérience du demandeur dans le domaine du contrôle technique ;
- le rapport d'audit initial favorable établi par un organisme agréé par le ministre chargé des transports ;
- l'avis de l'organisme technique central suivant le modèle de l'appendice 8 de la présente annexe (avis directement demandé par le préfet à l'organisme technique central à réception du dossier de demande d'agrément) ;
- l'attestation de conformité de l'outil informatique délivrée par l'OTC en application des dispositions de l'article 37 du présent arrêté ;
- l'engagement du demandeur à respecter le cahier des charges susvisé.

*Pour aller plus loin* : arrêté du 27 juillet 2004 relatif au contrôle technique des véhicules lourds.

#### Pour les véhicules légers n'excédant pas 3,5 tonnes

##### Autorité compétente

Le préfet de département du lieu d'implantation du centre est compétent pour se prononcer sur la demande d'agrément.

##### Pièces justificatives

Le dossier de demande d'agrément doit être transmis en trois exemplaires et doit comporter les pièces justificatives suivantes.

Pour un centre de contrôle en réseau, les pièces sont les suivantes :

- une demande d’agrément sur papier à en-tête ;
- un justificatif relatif à l’immatriculation au répertoire des métiers ou au registre du commerce et des sociétés datant de moins de trois mois, sur lequel est identifié l’établissement correspondant au centre de contrôle ;
- une attestation de l’affiliation à un réseau de contrôle agréé, suivant le modèle de l’appendice 5 de la présente annexe ;
- une attestation du réseau de contrôle certifiant que le centre de contrôle a fait l’objet d’un audit initial favorable (avec indication de la date et de la référence du rapport) et que le dossier est conforme aux prescriptions du présent chapitre, et une copie du rapport de l’audit initial ;
- le cahier des charges visé au deuxième alinéa du I de l’article R. 323-14 du Code de la route comprenant notamment :
  - une description de l’organisation et des moyens matériels, suivant le modèle de l’appendice 6 de la présente annexe ainsi que la liste des contrôleurs rattachés,
  - un plan de situation permettant d’identifier l’emprise immobilière et la zone de contrôle par rapport à l’environnement,
  - un plan de masse à l’échelle 1/100e faisant apparaître l’ensemble des surfaces couvertes et indiquant l’emplacement des matériels de contrôle,
  - l’engagement du demandeur, suivant le modèle de l’appendice 4 de la présente annexe,
  - le modèle du procès-verbal qui sera utilisé dans le centre de contrôle,
  - l’engagement du demandeur à respecter le cahier des charges susvisé.

Pour un centre de contrôle indépendant :

- une demande d’agrément sur papier à en-tête ;
- un justificatif relatif à l’immatriculation au répertoire des métiers ou au registre du commerce et des sociétés datant de moins de trois mois, sur lequel est identifié l’établissement correspondant au centre de contrôle ;
- un rapport d’audit initial favorable établi par un organisme agréé ;
- l’avis de l’organisme technique central, suivant le modèle de l’appendice 7 de la présente annexe (avis directement demandé par le préfet à l’organisme technique central à réception du dossier de demande d’agrément) ;
- le cahier des charges visé au 2e alinéa du I de l’article R. 323-14 du Code de la route comprenant notamment :
  - une description de l’organisation et des moyens matériels, suivant le modèle de l’appendice 6 de la présente annexe ainsi que la liste des contrôleurs rattachés,
  - un plan de situation permettant d’identifier l’emprise immobilière et la zone de contrôle par rapport à l’environnement,
  - un plan de masse à l’échelle 1/100e faisant apparaître l’ensemble des surfaces couvertes et indiquant l’emplacement des matériels de contrôle ;
- l’engagement du demandeur, suivant le modèle de l’appendice 4 de la présente annexe :
  - établir tous les documents, se rapportant à son activité, prescrits par le ministre chargé des transports,
  - faciliter la mission des agents désignés par celui-ci pour effectuer la surveillance du bon fonctionnement des centres de contrôle,
  - de signer la convention d’assistance technique prévue au point d de l’article 29 du présent arrêté ;
- les procédures internes du centre de contrôle permettant de s’assurer du respect des prescriptions du I de l’article R. 323-14 du Code de la route susvisé, ainsi que du paragraphe 1er du chapitre II du titre II du présent arrêté, et notamment :
  - agrément et habilitation d’un contrôleur technique,
  - organisation de la formation et qualification des contrôleurs techniques,
  - maîtrise du logiciel de contrôle technique,
  - intégrité, sécurité et maintenance du système informatique,
  - gestion, entretien et maintenance du matériel de contrôle,
  - transmission des données relatives aux contrôles techniques effectués,
  - exploitation des indicateurs fournis par l’organisme technique central,
  - audit des installations de contrôle et des contrôleurs,
  - gestion et archivage des procès-verbaux de contrôle technique,
  - organisation et déroulement des contrôles techniques,
  - méthodes alternatives d’essais en cas d’impossibilité de contrôle,
  - traitement des voies de recours amiables offertes au public,
  - gestion de la base documentaire des textes réglementaires et de leurs évolutions,
  - gestion de l’outillage spécifique gaz, pour les centres concernés ;
- l’attestation de conformité de l’outil informatique délivrée par l’OTC en application des dispositions de l’article 29 du présent arrêté ;
- le modèle du procès-verbal qui sera utilisé dans le centre de contrôle ;
- l’engagement du demandeur à respecter le cahier des charges susvisé.

*Pour aller plus loin* : arrêté du 18 juin 1991 relatif à la mise en place et à l'organisation du contrôle technique des véhicules dont le poids n'excède pas 3,5 tonnes.