﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS099" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="Expert forestier" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="expert-forestier" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/expertise/expert-forestier.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="expert-forestier" -->
<!-- var(translation)="None" -->

# Expert forestier

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'expert forestier est un professionnel qui réalise des missions d'expertise en matière forestière portant sur des biens meubles et immeubles.

Ses missions peuvent porter sur :

- le conseil, l'expertise ou l'évaluation du patrimoine forestier en cas d'achat, de vente, de succession ou de litige ;
- les études d'impacts (environnementales et paysagères) ;
- les diagnostics sanitaires d'arbres et ornements ;
- l'audit en environnement ;
- la gestion des chasses et étangs.

*Pour aller plus loin* :article L. 171-1 du Code rural et de la pêche maritime.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une profession libérale, le CFE compétent est l'Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité d'expert forestier le professionnel doit être inscrit sur la liste des experts forestiers établie par le comité du Conseil national de l'expertise foncière agricole et forestière (Cnefaf).

Pour cela, l'intéressé doit remplir les conditions suivantes :

- justifier d'une pratique professionnelle à titre personnel ou sous la responsabilité d'un maître de stage :
  - soit d'au moins sept ans en tant qu'expert forestier,
  - soit d'au moins trois ans si le professionnel est titulaire d'un titre ou diplôme sanctionnant au moins quatre années d'études post-secondaires dans les disciplines agricole, agronomique, environnementale, forestière, juridique ou économique ;
- ne pas avoir fait l'objet d'une condamnation pour des faits contraires à l'honneur, la probité ou aux bonnes mœurs au cours des cinq dernières années ;
- ne pas avoir fait l'objet d'une sanction disciplinaire ou administrative de destitution, radiation, ou de retrait d'agrément ou d'autorisation ;
- ne pas avoir été frappé de faillite personnelle.

*Pour aller plus loin* : article R. 171-10 du Code rural et de la pêche maritime.

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services (LPS) ou Libre Établissement (LE))

#### En vue d'un exercice temporaire et occasionnel (LPS)

Le ressortissant d’un État de l’Union européenne (UE) ou de l’Espace économique européen (EEE) peut faire usage de son titre professionnel en France à titre temporaire et occasionnel sans être inscrit sur la liste des experts forestiers, sous réserve :

- d'être légalement établi dans l'un de ces États pour y exercer la profession d'expert forestier ;
- lorsque ni la profession ni la formation ne sont réglementées dans cet État, d'avoir exercé la profession d'expert forestier dans cet État pendant au moins un an au cours des dix dernières années ;
- d'être assuré contre les conséquences pécuniaires de sa responsabilité civile professionnelle.

Pour cela, le ressortissant devra en faire la demande, préalablement à sa première prestation par déclaration adressée au Cnefaf (cf. infra « 3°. a. Déclaration préalable d'activité pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS) »).

*Pour aller plus loin* : article L. 171-2 du Code rural et de la pêche maritime.

#### En vue d'un exercice permanent (LE)

Tout ressortissant d'un État de l'UE ou de l'EEE qui est établi et exerce légalement l'activité d'expert forestier dans cet État peut exercer la même activité en France de manière permanente sous réserve de remplir les mêmes conditions que le professionnel français (cf. supra « 2°. a. Qualifications professionnelles »).

Il devra demander son inscription sur la liste nationale des experts fonciers et agricoles auprès du comité du Cnefaf (cf. infra « 3°. b. Demande d'inscription sur la liste des experts forestiers en vue d'un exercice permanent (LE) »).

Si des différences substantielles existent entre la formation et l'expérience professionnelle du ressortissant et celles exigées en France, le comité pourra décider de le soumettre à une mesure de compensation.

*Pour aller plus loin* : articles L. 171-3 et R. 717-10 du Code rural et de la pêche maritime.

### c. Conditions d’honorabilité et incompatibilités

#### Règles et devoirs professionnels

L'expert forestier est tenu au respect de règles et devoirs professionnels durant l'exercice de son activité, et s'engage à :

- respecter l'indépendance nécessaire à l'exercice de sa profession ;
- se prononcer en toute impartialité ;
- respecter le secret professionnel ;
- s'abstenir de toute pratique déloyale à l'égard de ses confrères.

*Pour aller plus loin* : article L. 171-1 et article R. 172-1 et suivants du Code rural et de la pêche maritime.

#### Incompatibilités

L'exercice de la profession d'expert forestier est incompatible avec :

- une charge d'officier public et ministériel ;
- toutes fonctions susceptibles de porter atteinte à son indépendance, en particulier celles consistant à acquérir de façon habituelle des biens mobiliers ou immobiliers en vue de leur revente.

*Pour aller plus loin* : article L. 171-1 du Code rural et de la pêche maritime.

### d. Quelques particularités de la réglementation de l’activité

#### Obligation d'assurance

L'expert forestier est tenu de fournir chaque année au comité du Cnefaf un justificatif de souscription d'une police d'assurance de responsabilité civile professionnelle.

*Pour aller plus loin* : article L. 171-1 alinéa 8 du Code rural et de la pêche maritime.

#### Formation continue

L'expert forestier doit suivre une formation continue en vue de son maintien sur la liste du Comité du Cnefaf. Il est tenu de produire chaque année l'attestation de suivi de cette formation continue.

*Pour aller plus loin* : article R. 171-16 du Code rural et de la pêche maritime ; règlement intérieur du [Cnefaf](http://www.cnefaf.fr).

#### Sanctions disciplinaires

Au cours de son activité, l'expert forestier fait l'objet d'une procédure de surveillance et de contrôle par le Comité du Cnefaf.

À ce titre, en cas de manquement aux règles professionnelles, il encourt les sanctions disciplinaires suivantes :

- un blâme ;
- un avertissement ;
- une suspension temporaire de trois mois à trois ans ;
- une radiation en cas de faute grave.

*Pour aller plus loin* : article L. 171-1 alinéa 7 du Code rural et de la pêche maritime.

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

## 3°. Démarches et formalités d’installation

### a. Déclaration préalable d'activité pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

L'expert forestier doit adresser une demande au Cnefaf.

#### Pièces justificatives

Le dossier de sa demande doit comporter :

- une pièce d'identité ;
- une attestation justifiant qu'il est légalement établi dans un État de l'UE ou de l'EEE ;
- une preuve que le ressortissant a exercé les activités d'expertise foncière et agricole pendant au moins un an au cours des dix dernières années ;
- une attestation d'assurance de responsabilité civile.

**À savoir**

Les pièces justificatives doivent être rédigées en français ou traduites par un traducteur agréé.

*Pour aller plus loin* : articles R. 171-17- à R. 171-17-3 du Code rural et de la pêche maritime.

### b. Demande d'inscription sur la liste des experts forestiers en vue d'un exercice permanent (LE)

#### Autorité compétente

Le professionnel qui sollicite sont inscription à la liste des experts forestiers doit adresser sa demande au Cnefaf par lettre recommandée.

#### Pièces justificatives

La demande doit contenir les éléments suivants, le cas échéant accompagnés de leur traduction en français :

- l'ensemble des éléments justifiant de l'état civil du demandeur ;
- une copie de ses titres ou diplômes ;
- un justificatif de son expérience professionnelle ;
- un curriculum vitae précisant les activités professionnelles antérieures du professionnel (date et lieu d'exercice) ;
- un justificatif ou à défaut, un engagement de souscription à l'assurance de responsabilité civile professionnelle ;
- un extrait de casier judiciaire n° 3 datant de moins de trois mois ou tout document équivalent délivré par l'autorité compétente de l’État de l'UE ou de l'EEE de moins de trois mois ;
- une déclaration sur l'honneur ou tout autre moyen de preuve attestant que l'intéressé remplit les conditions d'honorabilité ;
- le cas échéant, une déclaration de l'activité envisagée sous forme sociétaire.

#### Procédure

À réception du dossier, le comité dispose de trois mois pour informer le ressortissant de sa décision de l'inscrire sur la liste ou non.

Toutefois, en cas de différences substantielles entre la formation et l'expérience professionnelles du ressortissant et celles exigées en France, le comité pourra le soumettre à la mesure de compensation de son choix, qui peut-être soit un stage d'adaptation, soit une épreuve d'aptitude.

Le silence gardé pendant un délai de trois mois vaut acceptation de la décision.

Le renouvellement de la demande est soumis à la production de l'attestation de l'assurance de responsabilité civile professionnelle.

*Pour aller plus loin* : article R. 171-10 à R. 171-13 du Code rural et de la pêche maritime.

### d. Formalités de déclaration de l’entreprise

#### Autorité compétente

L'expert forestier doit procéder à la déclaration de son entreprise, et pour cela doit effectuer une déclaration auprès de la CCI.

#### Pièces justificatives

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI adresse le jour même un récépissé au professionnel mentionnant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter.

Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### e. Le cas échéant, enregistrer les statuts de la société

L'expert forestier doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.