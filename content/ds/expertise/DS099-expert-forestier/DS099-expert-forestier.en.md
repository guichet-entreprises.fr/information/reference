﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS099" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="Forestry expert" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="forestry-expert" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/expertise/forestry-expert.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="forestry-expert" -->
<!-- var(translation)="Auto" -->


Forestry expert
=============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The forest expert is a professional who carries out expert missions in forestry dealing with soft property and buildings.

Its missions may include:

- Advice, expertise or assessment of forest assets in the event of a purchase, sale, estate or dispute;
- impact studies (environmental and landscape)
- health diagnostics of trees and ornaments;
- Environmental auditing
- management of hunts and ponds.

*To go further* Article L. 171-1 of the Rural Code and Marine Fisheries.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for a liberal profession, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To carry out the activity of forest expert the professional must be registered on the list of forest experts established by the committee of the National Council of Agricultural and Forestry Land Expertise (CNEFAF).

To do so, the person must meet the following conditions:

- justify a professional practice in a personal capacity or under the responsibility of a trainee:- at least seven years as a forest expert,
  - or at least three years if the professional holds a title or diploma sanctioning at least four years of post-secondary education in the agricultural, agronomic, environmental, forestry, legal or economic disciplines;
- not having been convicted for facts contrary to honour, probity or morals in the past five years;
- not disciplinary or administrative sanction for dismissal, delisting, or withdrawal of accreditation or authorization;
- have not been hit with personal bankruptcy.

*To go further* Article R. 171-10 of the Rural Code and Marine Fisheries.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

**For temporary and casual exercise (LPS)**

The national of a European Union (EU) or The European Economic Area (EEA) can use his professional title in France on a temporary and casual basis without being on the list of forest experts, subject to:

- to be legally established in one of these states to practise as a forest expert;
- where neither the profession nor training is regulated in that state, having practised as a forest expert in that state for at least one year in the last ten years;
- to be insured against the pecuniary consequences of his professional civil liability.

In order to do so, the national will have to apply for it, prior to his first benefit by declaration addressed to the CNEFAF (see infra "3°. a. Pre-declaration of activity for the EU national for a temporary and casual exercise (LPS)").

*To go further* Article L. 171-2 of the Rural Code and Marine Fisheries.

**For a permanent exercise (LE)**

Any national of an EU or EEA state who is established and legally practises the activity of forest expert in that state may carry out the same activity in France on a permanent basis subject to meeting the same conditions as the French professional ( c. supra "2." a. Professional qualifications").

He will have to apply to the CNEFAF committee for his inclusion on the national list of land and agricultural experts (see infra "3o). b. Application for inclusion on the list of forest experts for a permanent exercise (LE)).

If there are substantial differences between the training and professional experience of the national and those required in France, the committee may decide to subject him to a compensation measure.

*To go further* Articles L. 171-3 and R. 717-10 of the Rural Code and Marine Fisheries.

### c. Conditions of honorability and incompatibility

**Professional rules and duties**

The forest expert is bound by professional rules and duties during the exercise of his activity, and is committed to:

- Respect the independence necessary to carry out one's profession;
- make an impartial statement;
- Respect professional secrecy
- refrain from any unfair practice towards his colleagues.

*To go further* Article L. 171-1 and Section R. 172-1 and following of the Rural code and marine fisheries.

**Incompatibilities**

The practice of the profession of forest expert is incompatible with:

- a public and ministerial officer office;
- any functions that could impair its independence, particularly those of acquiring personal or real estate in a common way for resale.

*To go further* Article L. 171-1 of the Rural Code and Marine Fisheries.

### d. Some peculiarities of the regulation of the activity

**Insurance obligation**

The forest expert is required to provide the CNEFAF committee with an annual proof of underwriting a professional liability insurance policy.

*To go further* Article L. 171-1 paragraph 8 of the Rural Code and Marine Fisheries.

**Continuous training**

The forest expert must undergo ongoing training in order to remain on the CNEFAF Committee list. It is required to produce the certificate of follow-up of this continuing education each year.

*To go further* Article R. 171-16 of the Rural Code and Marine Fisheries; the internal regulation of the[CNEFAF](http://www.cnefaf.fr).

**Disciplinary sanctions**

During the course of his activity, the forest expert is subject to a monitoring and control procedure by the CNEFAF Committee.

As such, if he breaches professional rules, he faces the following disciplinary sanctions:

- Blame
- A warning
- a temporary suspension from three months to three years;
- write-off in the event of gross misconduct.

*To go further* Article L. 171-1 paragraph 7 of the Rural Code and Marine Fisheries.

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*To go further* : order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

3°. Installation procedures and formalities
------------------------------------------------------

### a. Advance declaration of activity for EU national for temporary and casual exercise (LPS)

**Competent authority**

The forest expert must submit an application to the CNEFAF.

**Supporting documents**

The application file must include:

- ID
- a certificate justifying that it is legally established in an EU or EEA state;
- proof that the national has been engaged in land and agricultural expertise for at least one year in the past ten years;
- a certificate of liability insurance.

**What to know**

Supporting documents must be written in French or translated by a certified translator.

*To go further* Articles R. 171-17- to R. 171-17-3 of the Rural Code and Marine Fisheries.

### b. Application for inclusion on the list of forest experts for a permanent exercise (LE)

**Competent authority**

The professional who applies for registration to the list of forest experts must submit his application to the CNEFAF by recommended letter.

**Supporting documents**

The application must contain the following, if any, accompanied by their translation into French:

- All the evidence justifying the applicant's marital status;
- A copy of his titles or diplomas
- proof of work experience
- A resume detailing the professional's previous professional activities (date and place of practice);
- a proof or default, a commitment to underwrrite the professional liability insurance;
- a criminal record extract no. 3 less than three months old or any equivalent document issued by the competent authority of the EU State or the EEA of less than three months;
- a statement of honour or any other evidence that the person is meeting the conditions of honour;
- if necessary, a declaration of the activity envisaged in company form.

**Procedure**

Upon receipt of the file, the committee has three months to inform the national of his decision to list or not.

However, in the event of substantial differences between the national's professional training and experience and those required in France, the committee may submit him to the compensation measure of his choice, which may be an adaptation course, be an aptitude test.

The silence kept for a period of three months is worth accepting the decision.

The renewal of the application is subject to the production of the certificate of professional liability insurance.

*To go further* Article R. 171-10 to R. 171-13 of the Rural and Marine Fisheries Code.

### d. Company reporting formalities

**Competent authority**

The forest expert must make the declaration of his company, and for this must make a declaration with the ICC.

**Supporting documents**

The person concerned must provide the[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sends a receipt to the professional on the same day mentioning the missing documents on file. If necessary, the professional has a period of fifteen days to complete it.

Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*To go further* Section 635 of the General Tax Code.

### e. If necessary, register the company's statutes

The forest expert must, once the company's statutes have been dated and signed, register them with the corporate tax office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*To go further* Section 635 of the General Tax Code.

