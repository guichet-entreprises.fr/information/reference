﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS014" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="Géomètre-expert" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="geometre-expert" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/expertise/geometre-expert.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="geometre-expert" -->
<!-- var(translation)="None" -->

# Géomètre-expert

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le géomètre-expert est un professionnel dont la mission est :

- de réaliser les études et les travaux topographiques fixant les limites des biens fonciers dans le cadre de missions d’aménagement du territoire ;
- de procéder à toutes opérations techniques ou études sur l’évaluation, la gestion ou l’aménagement des biens fonciers.

*Pour aller plus loin* : article 1er de la loi n° 46-942 du 7 mai 1946 instituant l’Ordre des géomètres-experts.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la structure dans laquelle l'activité est exercée :

- pour une profession libérale, le CFE compétent est l'Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ou du greffe du tribunal d'instance. Pour les départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d'installation

### a. Condition d'âge

Pour exercer l'activité de géomètre-expert, le professionnel doit être âgé de 25 ans révolus.

*Pour aller plus loin* : article 3 alinéa 3 de la loi n° 46-942 du 7 mai 1946 instituant l'Ordre des géomètres-experts.

### b. Qualifications professionnelles

Seule une personne qualifiée professionnellement et inscrite au tableau de l'Ordre des géomètres-experts peut exercer l'activité de géomètre-expert. Pour cela, l'intéressé doit :

- être titulaire :
  - soit du diplôme d'ingénieur géomètre délivré par :
    - l'École supérieure des géomètres et topographes (l'ESGT) au Mans,
    - l'École spéciale des travaux publics (L’ESTP) à Paris,
    - l'Institut national des sciences appliquées (L’INSA) à Strasbourg,
  - soit du diplôme de géomètre-expert foncier délivré par le gouvernement (DPLG),
  - soit ayant fait l'objet de la procédure de validation des acquis de l'expérience (VAE) ;
- avoir exercé un stage professionnel de deux ans.

*Pour aller plus loin* : article 16 du décret n° 96-478 du 31 mai 1996 portant règlement de la profession de géomètre-expert et Code des devoirs professionnels ; et article 6 du décret n° 2010-1406 du 12 novembre 2010 relatif au diplôme de géomètre-expert foncier délivré par le gouvernement.

### c. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement )

#### Pour une Libre Prestation de Services (LPS)

Le professionnel ressortissant d’un État membre de l’Union européenne (UE) ou partie à l'accord sur l’Espace économique européen (EEE) peut exercer en France à titre temporaire et occasionnel l'activité de géomètre-expert, à condition d'être légalement établi dans un de ces États pour y exercer la même activité.

Il devra adresser, préalablement à l’exécution de la prestation de services, une demande auprès du conseil régional dans le ressort duquel la prestation doit être réalisée.

Le candidat doit également avoir les connaissances linguistiques nécessaires à l’exercice de la profession en France.

*Pour aller plus loin* : article 2-1 de la loi du 7 mai 1946 ; article 39 du décret n° 96-478 du 31 mai 1996 portant règlement de la profession de géomètre-expert et Code des devoirs professionnels.

#### Pour un Libre Établissement (LE)

Pour exercer à titre permanent sur le territoire français, le ressortissant d’un État membre de l’UE ou de l’EEE doit être en possession :

- soit d’une attestation de compétence ou d’un titre de formation délivré par l’autorité compétente d’un État qui réglemente l’exercice de la profession ;
- soit d’une attestation indiquant qu’il a exercé l’activité pendant un an au cours des dix dernières années précédant la demande dans un État qui ne réglemente pas la profession.

Dès lors qu’il remplit l’une de ces conditions, il pourra demander une reconnaissance de qualification auprès du ministre chargé de l’urbanisme.

**À noter**

Le géomètre-expert qui a effectué une demande de reconnaissance peut également solliciter son inscription au tableau de l’Ordre des géomètres-experts (cf. infra « 3°. b. Demande d'inscription à l’Ordre des géomètres-experts »).

*Pour aller plus loin* : article 7-1 du décret du 31 mai 1996.

### d. Conditions d'honorabilité et incompatibilités

#### Honorabilité

Pour exercer son activité le professionnel ne doit pas :

- avoir fait l'objet d'une procédure de redressement, de liquidation judiciaire ou de faillite ;
- être fonctionnaire ni être révoqué pour des agissements contraires à l'honneur ou à la probité ;
- avoir été l'auteur de faits contraires à l'honneur pénalement répréhensibles ;
- faire l'objet d'une interdiction ou d’une suspension d'exercice.

**À noter**

Les ressortissants d'un État membre de l'UE ou de l'EEE ne doivent pas avoir fait l'objet de ces mêmes sanctions.

*Pour aller plus loin* : article 3 alinéa 2 de la loi du 7 mai 1946.

#### Incompatibilités

L'exercice de la profession de géomètre-expert doit être effectué de manière indépendante, aussi il est incompatible avec :

- une charge d'officier public ou ministériel ;
- un mandat commercial ou tout emploi rémunéré par traitement ou salaire.

#### Conditions d'exercice des activités d'entremise et de gestion immobilières

Le professionnel qui exerce des activités d'entremise et de gestion immobilières doit respecter certaines obligations, et notamment :

- obtenir une autorisation du conseil régional de l'Ordre des géomètres-experts ;
- ne pas exercer simultanément l'activité d'entremise immobilière et celle de géomètre-expert lors de la même mission ;
- souscrire une assurance de responsabilité civile personnelle pour ces activités ;
- tenir une comptabilité distincte de ses activités de géomètre-expert ;
- avoir un mandat écrit.

*Pour aller plus loin* : article 8 de la loi du 7 mai 1946.

### e. Quelques particularités de la réglementation de l'activité

#### Obligation de respecter les règles déontologiques de la profession

Le géomètre-expert est soumis, durant toute la durée de sa prestation, au respect des règles disciplinaires. Le conseil régional de l’Ordre dans la circonscription duquel il sera inscrit assure le respect des règles suivantes :

- la garantie d’indépendance lors de chacune de ses missions. À noter que la qualité de membre de l’Ordre est incompatible avec une charge d’officier public ou ministériel ;
- une obligation de conseil et de transparence envers ses clients ;
- une obligation de dater et signer l’ensemble des plans et documents établis ainsi que d’apposer son cachet ;
- le devoir de mémoire (conserver et tenir à jour les documents et archives réalisés) ;
- le secret professionnel ;
- un recours à la publicité possible uniquement à titre informatif.

**À savoir**

Toute publicité de la part du géomètre-expert sera soumise au conseil régional de l’Ordre avant sa diffusion.

*Pour aller plus loin* : article 8 de la loi du 7 mai 1946.

#### Obligation de souscrire une assurance de responsabilité civile

En qualité de professionnel, le géomètre-expert doit souscrire une assurance de responsabilité professionnelle. Dès lors que le professionnel exerce en tant que salarié, cette obligation incombe à son employeur.

*Pour aller plus loin* : articles L. 241-1 et suivants du Code des assurances.

## 3°. Démarches et formalités d’installation

### a. Demande de reconnaissance de qualification

Tout ressortissant d'un État de l'UE ou de l'EEE qui souhaite s'installer en France et être reconnu en tant que géomètre-expert doit effectuer une demande de reconnaissance de qualification.

#### Autorité compétente

Le professionnel doit adresser une demande de reconnaissance de qualification au ministre chargé de l'urbanisme.

#### Pièces justificatives

Cette demande doit être transmise en double exemplaire et comporter l'ensemble des pièces justificatives suivantes :

- une fiche d’état civil et une pièce d’identité datant de moins de trois mois ;
- une copie des diplômes, certificats ou titres attestant la formation reçue, ainsi qu’un descriptif du contenu des études poursuivies, des stages effectués et le nombre d’heures correspondant ;
- une attestation de compétence ou un titre de formation ;
- tout document justifiant que le géomètre-expert a bien exercé pendant un an au cours des dix dernières années dans un État membre de l’UE ou de l’EEE.

#### Délai

Le ministre chargé de l'urbanisme disposera de trois mois à compter de la réception du dossier complet pour se prononcer sur la demande. Il pourra décider soit de reconnaître les qualifications professionnelles du ressortissant, et dans ce cas, lui permettre d'effectuer l'activité de géomètre-expert en France, soit de le soumettre à une mesure de compensation.

**À noter**

L’absence de réponse dans un délai de trois mois équivaut au rejet de la demande.

#### Bon à savoir : Mesures de compensation

Le ministre chargé de l’urbanisme peut décider que le ressortissant accomplira un stage d’adaptation pendant trois ans maximum ou se soumettra à une épreuve d’aptitude. Cette dernière consiste en la présentation et la discussion d’un dossier portant sur l’expérience professionnelle et les connaissances du candidat pour l’exercice de la profession. Le dossier est remis à l’intéressé avant sa présentation devant un jury, qui fera ensuite connaître les résultats de l’épreuve au ministre chargé de l’urbanisme.

Une fois la qualification obtenue, le demandeur pourra solliciter son inscription au tableau de l’Ordre des géomètres-experts (cf. ci-après « 3°. b. Demande d’inscription à l’Ordre des géomètres-experts »).

*Pour aller plus loin* : article 2-1 de la loi du 7 mai 1946 ; articles 7 et suivants du décret du 31 mai 1996.

### b. Demande d'inscription à l'Ordre des géomètres-experts

#### Autorité compétente

Le ressortissant qui souhaite exercer l'activité de géomètre-expert doit adresser, par lettre recommandée au président du conseil régional de l'Ordre où il souhaite s'établir, une demande d'inscription à l'Ordre de la profession comprenant :

- un formulaire de renseignements remis par le conseil régional en trois exemplaires ;
- une copie d'un diplôme délivrant le titre de géomètre-expert ou d’une décision ministérielle portant reconnaissance de qualification ou, à défaut, l’accusé de réception du dossier complet de la demande de reconnaissance par le ministre chargé de l’urbanisme ;
- trois photographies d'identité ;
- une attestation de versement de l'indemnité pour frais de dossier ;
- une fiche d'état civil et une pièce d'identité en cours de validité ;
- un extrait du casier judiciaire ou d'un document équivalent ;
- pour le ressortissant, un diplôme d'études en langue française ou un diplôme approfondi de langue française, ou, le cas échéant, une attestation de possession des compétences linguistiques à l'exercice de la profession en France établie par le géomètre-expert chez qui le demandeur a effectué un stage.

À compter de la réception complète de la demande, le président du conseil régional en accuse réception à l’intéressé dans un délai d’un mois.

#### Coûts

Le montant de l’indemnité pour frais de dossier est fixé chaque année par le Conseil supérieur après approbation du commissaire du gouvernement (à titre indicatif, elle était de 200 euros en 2017).

#### Voie de recours

Le demandeur peut former, dans les deux mois de la notification de la décision, un recours auprès du Conseil supérieur de l’Ordre qui aura alors quatre mois pour statuer.

Une fois inscrit, le géomètre-expert prête serment devant le conseil régional et reçoit un numéro d’inscription à l’Ordre délivré par le Conseil supérieur ainsi qu’une carte professionnelle.

*Pour aller plus loin* : arrêté du 12 novembre 2009 relatif à l’inscription au tableau de l’Ordre des géomètres-experts.

### c. Formalités de déclaration de l'entreprise

#### Autorité compétente

Le géomètre-expert doit procéder à la déclaration de son entreprise, et pour cela, il doit effectuer une déclaration auprès de la chambre de commerce et d'industrie (CCI).

#### Pièces justificatives

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le CFE de la chambre de commerce et d'industrie adresse le jour même un récépissé au professionnel listant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis dans les délais indiqués ci-dessus.

Dès lors que le CFE refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

#### Le cas échéant, enregistrement des statuts de la société

Le géomètre-expert doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- la forme même de l'acte l'exige.

##### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

##### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.

#### Déclaration préalable d'activité pour un établissement secondaire

Le géomètre-expert peut exercer son activité au sein d'un cabinet de géomètre-expert, composé alors :

- d'un bureau principal installé dans la circonscription du conseil régional auprès de laquelle le professionnel est inscrit au tableau de l'Ordre des géomètres-experts ;
- de bureaux secondaires, permanences ou bureaux de chantiers installés dans n'importe quelle circonscription régionale. La création de bureaux secondaires est soumise à une déclaration préalable.

##### Autorité compétente

La déclaration préalable d'ouverture doit être adressée par le professionnel au président du conseil régional compétent.

##### Pièces justificatives

La déclaration préalable doit indiquer :

- les éléments suivants relatifs au professionnel :
  - son identité,
  - son adresse professionnelle,
  - son numéro d'inscription à l'Ordre ;
- les éléments relatifs à la société :
  - sa raison sociale et son adresse,
  - son numéro d'inscription à l'Ordre,
  - l'identité des gémètres-experts associés,
  - le nom et la qualité du géomètre-expert responsable du bureau secondaire.

**À noter**

Le changement d'adresse d'un établissement secondaire fait l'objet d'une information préalable par le géomètre-expert responsable du bureau au conseil régional.

*Pour aller plus loin* : article 30 du décret du 31 mai 1996 ; règlement intérieur de l'Ordre des géomètres-experts.