﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS014" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="Land surveyor" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="land-surveyor" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/expertise/land-surveyor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="land-surveyor" -->
<!-- var(translation)="Auto" -->


Land surveyor
===============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The surveyor-expert is a professional whose mission is:

- carry out topographical studies and work setting the boundaries of land in land use of land-use planning missions;
- conduct any technical operations or studies on the assessment, management or management of land assets.

*For further information*: Article 1 of Law 46-942 of May 7, 1946 establishing the Order of Expert Surveyors.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the structure in which the activity is carried out:

- for a liberal profession, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, it is the registry of the commercial court or the registry of the district court. For the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Age requirement

To be an expert surveyor, the professional must be 25 years old.

*For further information*: Article 3 paragraph 3 of Law 46-942 of May 7, 1946 establishing the Order of Expert Surveyors.

### b. Professional qualifications

Only a professionally qualified person on the Order of Expert Surveyors can work as an expert surveyor. In order to do so, the person concerned must:

- Be a holder:- or the surveyor engineering degree issued by:- Higher School of Surveyors and Topographers (ESGT) in Le Mans,
    * Special School of Public Works (ESTP) in Paris,
    * Institute of Applied Sciences (INSA) in Strasbourg,
  - or government-issued land surveyor-expert diploma (DPLG),
  - have been the subject of the experience validation procedure (VAE);
- have worked a two-year professional internship.

*For further information*: Article 16 of Decree 96-478 of 31 May 1996 on the regulation of the profession of surveyor-expert and code of professional duties; and Article 6 of Decree No. 2010-1406 of November 12, 2010 relating to the government's land surveyor-expert diploma.

### c. Professional Qualifications - European Nationals (Free Service Or Freedom of establishment)

#### For Freedom to provide services

The professional national of a Member State of the European Union (EU) or party to the agreement on the European Economic Area (EEA) may practice in France on a temporary and occasional basis the activity of surveyor-expert, provided that he is legally established states to carry out the same activity.

It will have to apply to the regional council in the run-up to the delivery of the services.

The candidate must also have the necessary language skills to practice the profession in France.

*For further information*: Article 2-1 of the Law of 7 May 1946; Article 39 of Decree 96-478 of 31 May 1996 on the regulation of the profession of surveyor-expert and code of professional duties.

#### For a Freedom of establishment

To exercise permanently on French territory, the national of an EU or EEA member state must be in possession of:

- either a certificate of competency or a training certificate issued by the competent authority of a state that regulates the practice of the profession;
- or a certificate indicating that he has been in the business for one year in the last ten years prior to the application in a state that does not regulate the profession.

Once he fulfils one of these conditions, he can apply to the Minister for Planning for qualification.

**Please note**

An expert surveyor who has applied for recognition may also apply for inclusion in the Order of Expert Surveyors (see below "3 degrees). b. Application for registration with the Order of Surveyors-Experts").

*For further information*: Article 7-1 of the May 31, 1996 decree.

### d. Conditions of honorability and incompatibility

#### Integrity

In order to carry out his activity the professional does not have to:

- have been the subject of a redress, liquidation or bankruptcy procedure;
- be a public servant or be dismissed for acts contrary to honour or probity;
- having been the author of criminally reprehensible acts contrary to honour;
- prohibited or suspended from practising.

**Please note**

Nationals of an EU or EEA member state must not have been subject to the same sanctions.

*For further information*: Article 3, paragraph 2 of the Act of May 7, 1946.

#### Incompatibilities

The practice of the profession of surveyor-expert must be carried out independently, so it is incompatible with:

- a public or ministerial officer office;
- a commercial mandate or any paid employment by salary or salary.

**Terms of practice for intermediaries and property management**

Professionals who engage in real estate management and intermediaries must meet certain obligations, including:

- obtain approval from the regional council of the College of Surveyors Experts;
- Do not simultaneously engage in real estate and surveyor-expert activities during the same mission;
- Take out personal liability insurance for these activities;
- Keep a separate accounting of his activities as an expert surveyor;
- have a written warrant.

*For further information*: Article 8 of the Act of May 7, 1946.

### e. Some peculiarities of the regulation of the activity

**Obligation to respect the ethical rules of the profession**

The surveyor-expert is subject, throughout his performance, to compliance with disciplinary rules. The Order's regional council in the riding from which it will be registered ensures compliance with the following rules:

- guarantee of independence in each of its missions. It should be noted that membership of the Order is incompatible with a public or ministerial officer office;
- an obligation to advise and be transparent to its clients;
- an obligation to date and sign all the plans and documents drawn up as well as to stamp it;
- The duty of remembrance (keeping and maintaining documents and archives made);
- Professional secrecy
- possible use of advertising only for informational purposes.

**What to know**

Any advertising by the surveyor-expert will be submitted to the Order's regional council before it is broadcast.

*For further information*: Article 8 of the Act of May 7, 1946.

**Obligation to take out liability insurance**

As a professional, the surveyor-expert must take out professional liability insurance. As long as the professional practises as an employee, this obligation rests with his employer.

*For further information*: Articles L. 241-1 and the following from the Insurance Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Application for qualification recognition

Any national of an EU or EEA state who wishes to settle in France and be recognised as an expert surveyor must apply for qualification recognition.

**Competent authority**

The professional must apply for qualification recognition to the Minister responsible for urban planning.

**Supporting documents**

This request must be submitted in duplicate and include all the following supporting documents:

- A civil registration card and identification less than three months old;
- A copy of diplomas, certificates or titles attesting to the training received, as well as a description of the content of the studies carried out, the internships carried out and the corresponding number of hours;
- A certificate of competency or a training certificate;
- any document justifying that the expert surveyor has exercised well for a year in the last ten years in an EU or EEA member state.

**Timeframe**

The Minister responsible for urban planning will have three months from receipt of the full file to decide on the application. He may decide either to recognize the professional qualifications of the national, and in this case, allow him to carry out the activity of surveyor-expert in France, or to submit him to a compensation measure.

**Please note**

Failure to respond within three months is tantamount to the rejection of the application.

**Good to know: Compensation measures**

The Minister for Planning may decide that the national will complete an adjustment course for up to three years or submit to an aptitude test. The latter consists of the presentation and discussion of a file on the candidate's professional experience and knowledge for the practice of the profession. The file is given to the person concerned before it is presented to a jury, who will then make the results of the test known to the Minister responsible for urban planning.

Once qualified, the applicant will be able to apply for registration in the Order of Expert Surveyors (see "3o. b. Application for registration with the Order of Surveyors-Experts").

*For further information*: Article 2-1 of the Law of 7 May 1946; Articles 7 and following of the decree of 31 May 1996.

### b. Application for registration with the College of Surveyors-Experts

**Competent authority**

A national wishing to carry out the activity of surveyor-expert must submit, by letter recommended to the president of the regional council of the Order where he wishes to settle, an application for registration to the Order of the Profession including:

- An information form provided by the regional council in three copies;
- A copy of a diploma issuing the title of surveyor-expert or a ministerial decision recognizing qualification or, failing that, the acknowledgement of receipt of the full file of the application for recognition by the Minister responsible for planning;
- three identity photographs;
- A certificate of payment of the claim fee;
- A valid civil registration card and identification;
- an extract from the criminal record or an equivalent document;
- for the national, a French-language degree or a deep French-language diploma, or, if necessary, a certificate of possession of language skills in the practice of the profession in France established by the surveyor-expert applicant has completed an internship.

Once the application is fully received, the president of the regional council acknowledges the request within one month.

**Costs**

The amount of the compensation for file costs is set each year by the Higher Council after approval by the Government Commissioner (as an indication, it was 200 euros in 2017).

**Remedy**

The applicant may file an appeal with the Higher Council of the Order within two months of notification of the decision, which will then have four months to decide.

Once registered, the expert surveyor takes an oath before the regional council and receives a registration number to the Order issued by the Higher Council as well as a professional card.

*For further information*: order of November 12, 2009 relating to the inclusion on the board of the Order of Surveyors-experts.

### c. Company reporting formalities

**Competent authority**

The surveyor-expert must report his company, and for this, he must make a declaration with the Chamber of Commerce and Industry (CCI).

**Supporting documents**

The person concerned must provide the[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Timeframe**

The Chamber of Commerce and Industry's CFE sends a receipt to the professional on the same day listing the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted within the time frame specified above.

If the CFE refuses to receive the file, the applicant has an appeal before the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

#### If necessary, register the company's statutes

The surveyor-expert must, once the company's statutes have been dated and signed, register them with the Corporate Tax Office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

#### Pre-declaration of activity for a secondary school

The surveyor-expert can work in a surveyor-expert firm, which is then composed of:

- a main office in the regional council district where the professional is listed on the Order of Expert Surveyors;
- secondary offices, permanent offices or construction sites located in any regional constituency. The creation of secondary offices is subject to prior declaration.

**Competent authority**

The prior opening statement must be addressed by the professional to the president of the relevant regional council.

**Supporting documents**

The advance statement must state:

- The following about the professional:- its identity,
  - his business address,
  - His registration number to the Order;
- Company-related elements:- its name and address,
  - his registration number to the Order,
  - The identity of the associated expert-gestreters,
  - the name and quality of the surveyor-expert in charge of the secondary office.

**Please note**

The change of address of a secondary school is the subject of prior information by the surveyor-expert responsible for the office at the regional council.

*For further information*: Article 30 of the decree of 31 May 1996; Order of Expert Surveyors.

