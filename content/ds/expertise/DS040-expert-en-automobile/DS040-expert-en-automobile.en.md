﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS040" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="Road traffic accident expert" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="road-traffic-accident-expert" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/expertise/road-traffic-accident-expert.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="road-traffic-accident-expert" -->
<!-- var(translation)="Auto" -->


Road traffic accident expert
=================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The automotive expert is a professional who carries out a road safety mission. It is responsible for writing third-party reports on any damage to motor vehicles and cycles and their derivatives. In particular, the automotive expert determines the origin, consistency and value of this damage and its repair. It is also responsible for determining the value of any vehicle.

In accordance with the provisions of Section D. 114-12 of the Public Relations Code and the Administration, any user may obtain an information certificate on the standards applicable to the automotive profession. To do so, you should send your application to the following address: devenir-expert-automobile@interieur.gouv.fr.

*For further information*: Article L. 326-4 of the Highway Traffic Act.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- In the case of the creation of an individual company, the competent CFE is the Urssaf;
- In case of artisanal activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- In the event of the creation of a commercial company, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

For more information, it is advisable to visit the Urssaf's websites,[ICC In Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites),[Strasbourg ICC](http://strasbourg.cci.fr/cfe/competences),[CFE-CMA](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023) [And Public Service](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

#### Mandatory qualification

To practice as an automotive expert, the person must be a holder, at the right:

- the professional certificate of automotive expert or the recognition of this quality or the state diploma of automotive expert or the transcript of the state diploma of automotive expert issued by the academy rector;
- a title issued by another Member State of the European Union (EU) or party to the agreement on the European Economic Area (EEA), or a title recognised by one of these states as equivalent to the titles mentioned above;
- either from professional experience in automotive expertise in another EU or EEA state.

*For further information*: Decree 95-493 of 25 April 1995 establishing and regulating the general diploma of automotive expert.

#### Mandatory additional qualification to control damaged vehicles

If the professional wishes to control damaged vehicles, he must, in addition to the compulsory qualification, obtain a specific additional professional qualification.

Obtaining this additional qualification is conditional on the follow-up of specific training to update and update the legal and technical knowledge necessary to conduct vehicle procedures. Damaged. The follow-up of this training, which lasts 8 hours over a day, gives rise to the delivery of a certificate to the participant.

Only three agencies are approved by the Ministry of Transport to provide this training:

- [BCA University](http://www.bca.fr/nos-offres-pro/les-services-specifiques) ;
- Institute for Motor Vehicle Training ([IFOR2A](%5b*http://anea.fr/ifor2a/*%5d(http://anea.fr/ifor2a/))) ;
- National Institute for Road Safety and Research ([Inserr](%5b*http://www.inserr.fr/formation-categorie/experts-vehicules-endommages*%5d(http://www.inserr.fr/formation-categorie/experts-vehicules-endommages))).

**What to know**

The qualification for the control of damaged vehicles is valid until 31 December of the year following the training. Thus, in order to maintain this qualification, the person concerned must take an annual refresher course.

The national list of automotive experts specifies for each expert, if any, that he or she is trained to carry out checks on damaged vehicles. This training is deemed to have been acquired for experts who have had a car expert degree for less than a year.

Some professionals may carry out activity of controlling damaged vehicles without having undergoed specific training. Indeed, this qualification is also granted, under certain conditions specified by the decree of 26 July 2011, to automotive experts who, at their choice:

- have received training in another EU or equivalent EEA state;
- establish themselves to have professional experience in the control of damaged vehicles acquired in another EU or EEA state.

*For further information*: Article R. 326-11 of the Highway Traffic Act; order of 26 July 2011 relating to the obtaining and maintenance of the qualification for the control of damaged vehicles for automotive experts.

### b. Professional Qualifications - European Nationals (LPS or LE)

#### In case of Freedom to provide services

The professional who is a national of the EU or the EEA, who is legally established in one of these states to work as an automotive expert, is deemed to have the professional qualification to practise in France, on an occasional and temporary basis, same activity.

However, if neither the activity nor the training conducting there is regulated in the State of the Establishment, the person must prove that he or she has been an automotive expert for the equivalent of one full-time year in the ten years preceding the performance he wants to perform in France.

In all cases, the expert is placed on the national list of automotive experts on a temporary basis.

**Qualification required for the control of damaged vehicles**

If the EXPERT who is a national of the EU or the EEA wishes to carry out the activity of monitoring damaged vehicles in France on a temporary and occasional basis, he must provide proof, by any means, that he has exercised and has a practice of monitoring repairs and re-circulation of vehicles under the same conditions as those applicable to those permanently operating in France.

If neither the activity nor the training leading to it is regulated in the state of the institution, the expert must have worked full-time for at least two years in the ten years prior to the benefit.

If a review of the exhibits presented to justify the expert's qualification for the control of damaged vehicles reveals a substantial difference between the reality of the applicant's qualifications and those required to carry out the activity in France, and to the extent that this difference is likely to affect the safety of persons, the qualification "damaged vehicles" (VE) can only be recognised and granted after the expert has completed the continuous training in vehicle control. Damaged.

After checking his qualification for the control of damaged vehicles, the claimant is placed on the list for a period of one year with mention of his "EV" qualification.

*For further information*: Article L. 326-4 of the Highway Traffic Act and Section 2-II and 2-III of the order of July 26, 2011 mentioned above.

#### In case of Freedom of establishment

In order to practise in France, on a permanent basis, the person must submit to the same qualification requirements as those applicable to French nationals (see below: "2. a. Professional qualifications").

**Good to know**

Registration on the national list of automotive experts is mandatory for European nationals wishing to practice this profession in France on a permanent basis.

**Qualification required for the control of damaged vehicles**

If the applicant's state of establishment regulates access to the profession, its exercise or training leading to the exercise of the damaged vehicle control activity, the national must justify his or her competence or training or recognized in one of these states recognizing it as qualified to carry out these operations. Specifically, these operations must enable the expert to issue, at the conclusion of the procedure for the control of damaged vehicles, a certificate of compliance guaranteeing that the vehicle is in any way compliant with its receipt and that it can circulate under normal safety conditions.

If the applicant's state of establishment does not regulate this occupation, the national must prove by any means that he has worked for two full-time years in the previous ten years and possess at least one certificate of competency or a training document attesting that it has been prepared for the practice of this profession. However, two years of professional experience are not due when the detainee training designation or designations sanction regulated training.

If a review of the exhibits presented to justify the expert's qualification for the control of damaged vehicles reveals a substantial difference between the reality of the applicant's qualifications and those required to carry out the activity in France, and to the extent that this difference is likely to affect the safety of persons, the "EV" qualification can only be recognised and granted after the expert has completed continuous training in the control of damaged vehicles.

*For further information*: Article L. 326-4 of the Highway Traffic Act and Section 2-I and 2-III of the July 26, 2011 Order.

### c. Conditions of honorability and incompatibility

#### Conditions of honorability

No person may engage as an automotive expert if he has been convicted of theft, fraud, receiving, breach of trust, sexual assault, subtraction by a custodian of public authority, false testimony, corruption or influence peddling, forgery, or for a crime punishable by theft, fraud or breach of trust.

*For further information*: Article L. 326-2 of the Highway Traffic Act.

**Good to know**

In the event of a conviction for facts constituting a breach of honour or probity, the court may, as a supplementary sentence, prohibit, temporarily or permanently, the exercise of the activity.

*For further information*: Article L. 326-9 of the Highway Traffic Act.

#### Incompatibilities

The practice of being an automotive expert is incompatible with:

- Holding a public or ministerial officer office;
- activities related to the production, sale, leasing, repair and representation of motor vehicles and accessory parts;
- the profession of insurer.

**Good to know**

The conditions under which an automotive expert practises his profession must not impair his independence.

*For further information*: Article L. 326-6 of the Highway Traffic Act.

### d. Liability insurance

The automotive expert must take out an insurance contract guaranteeing the civil liability that he may incur as a result of his professional activities.

For further details on the minimum conditions of the compulsory insurance contract, it is advisable to refer to the decree of 5 February 2002 amending the order of 13 August 1974 relating to the minimum conditions of the liability that automotive experts must take out.

*For further information*: Article L. 326-7 of the Highway Traffic Act and stopped on February 5, 2002.

### e. Some peculiarities of the regulation of the activity

#### Wage-earning practice of the automotive expert profession

It is possible to practice the profession of automotive expert independently or in an employee.

Persons practising in an employee's way are subject to the same obligations as those of the self-employed: to fulfil the condition of professional qualification, the conditions of honourability, to respect the rules of incompatibility and to subscribe professional liability insurance (see below: "2. Conditions of installation").

In addition, salaried automotive experts must provide the Ministry of Transportation with a copy of their employment contract or an employee certificate.

#### Accessibility and safety rules

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the activity sheets "Reporting formalities of a commercial company," "Registration of an individual business in the register of trade and companies" or "Reporting formalities" artisanal enterprise."

### b. Ask for inclusion on the national list of automotive experts

In order to legally practise as an automotive expert, the person concerned must be registered on the national list of automotive experts, available on the [Road Safety website](http://www.securite-routiere.gouv.fr/connaitre-les-regles/le-vehicule/la-liste-nationale-des-experts-automobile). The expert who practises without being registered is subject to administrative, criminal and civil sanctions.

**Competent authority**

The application must be sent to the Ministry of Transport.

**Response time**

The Minister responsible for transport acknowledges receipt of the file within one month of receipt and informs the applicant, if necessary, of any missing documents. The Minister then has three months from the date of receipt of the full file to decide on the application for registration. This decision must be justified. The silence kept at the end of this period is worth accepting the application for registration.

**Supporting documents**

The application must be accompanied by the following documents:

- the completed, dated, and signed registration form, the model of which is available on the [Road Safety website](http://www.securite-routiere.gouv.fr/connaitre-les-regles/le-vehicule/les-experts-en-automobile) ;
- A valid document establishing the person's marital status (identity card, passport, etc.);
- copy of one of the following documents:- the professional certificate of automotive expert or the recognition of the quality of automotive expert or the diploma of automotive expert or the transcript of the automotive expert diploma issued by the academy rector,
  - the title issued by another EU or EEA Member State or a title recognised by one of these states equivalent to the above-mentioned securities,
  - any piece that would establish the person's professional experience in automotive expertise in another EU or EEA state;
- a certificate of insurance stating that the person is covered by liability insurance for the exercise of his profession as an "automotive expert" dating back less than three months;
- an excerpt from the 3rd bulletin of the criminal record (or an equivalent document for foreign nationals), less than three months old;
- A Kbis extract or the registration certificate issued by INSEE;
- a statement on the honour attesting that the expert:- does not hold a public or ministerial officer's office or engage in any activity incompatible with the status of automotive expert,
  - was not the subject of a sentence under section 326-2 of the Highway Traffic Act,
  - is not subject to a conviction for facts that constitute a breach of honour or probity;
- one of the documents informing the conditions of professional practice of the activity (employees, liberals or business leaders);
- If the expert wishes to carry out the assessments on "severely damaged vehicle" or "economically irreparable vehicle" (VGE/VEI), provide a copy of the certificate of follow-up to the corresponding training.

**What to know**

If necessary, the documents must be translated into French by a certified translator.

**Cost**

Free.

*For further information*: Article L. 326-4, and following and articles R. 326-5 and following of the Highway Traffic Act.

### c. Make a prior declaration of activity for EU nationals engaged in a one-off activity (LPS)

Nationals living in an EU or EEA state who wish to carry out the activity of automotive expert in France on an ad hoc and occasional basis must make a prior declaration.

This written statement results in an audit of the claimant's qualifications to ensure that his or her performance will not affect the safety or health of the service recipient, due to the lack of professional qualifications of the Provider.

The declaration results in the inclusion on the national list of automotive experts for one year. This list is available on the [Road Safety website](http://www.securite-routiere.gouv.fr/connaitre-les-regles/le-vehicule/la-liste-nationale-des-experts-automobile).

**Competent authority**

The statement must be addressed to the Ministry of Transport.

**Response time**

Within one month of receiving the request and the required supporting documents, the Minister responsible for transport informs the claimant of his decision. The Minister may decide to subject the applicant to a professional interview or professional internship if a review of the documents provided reveals a substantial difference between his professional qualifications and those required to practice in France, if this difference is likely to harm the safety of people.

The absence of a request for further information or notification of the result of the qualifications audit is worth accepting the exercise of the activity described in the declaration.

**Supporting documents**

The application must be accompanied by the following documents:

- The [registration form](http://media.afecreation.fr/file/08/2/ficheinscription_2015.90082.pdf) completed, dated and signed (the "SIRET" field is not to be completed);
- proof of the claimant's identity and nationality;
- a certificate certifying that the claimant is legally established in another EU or EEA state to practise as an automotive expert, and that, when the certificate is issued, there is no prohibition, even temporary, Exercise
- proof of the applicant's professional qualifications
- where the profession of automotive expert is not regulated in the state of the establishment, evidence by any means that the claimant has practiced this occupation for at least the equivalent of two full-time years in the previous ten years;
- proof that the claimant is covered by an insurance policy guaranteeing the liability he may incur;
- if the expert wishes to carry out the VGE/VEI assessments, proof that he has exercised and that he has a practice of monitoring repairs and re-circulating vehicles under the same conditions as for those wishing to practice permanently in France.

**What to know**

If necessary, all documents must be translated into French by a certified translator.

**Cost**

Free.

**Please note**

If necessary, the Minister may ask the claimant to justify that he has the language skills necessary to carry out the duties of automotive expert in France.

*For further information*: Articles R. 326-4 and the following of the Highway Traffic Act.

**Application for renewal of registration**

If, at the end of the first year, the claimant wishes to carry out his activity again on a temporary and occasional basis in France, he sends, by any means, to the Minister responsible for transport an application for renewal of his registration on the list for a one-year period. This renewal application is accompanied by evidence that the claimant is covered by an insurance contract guaranteeing the civil liability that he may incur as a result of the activities of an automotive expert.

