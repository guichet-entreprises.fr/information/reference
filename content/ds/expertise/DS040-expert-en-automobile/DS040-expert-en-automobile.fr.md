﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS040" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="Expert en automobile" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="expert-en-automobile" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/expertise/expert-en-automobile.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="expert-en-automobile" -->
<!-- var(translation)="None" -->

# Expert en automobile

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’expert en automobile est un professionnel qui assure une mission de sécurité routière. Il est chargé de rédiger des rapports, destinés à des tiers, relatifs à tous dommages causés aux véhicules à moteur ainsi qu’aux cycles et à leurs dérivés. L’expert en automobile détermine notamment l’origine, la consistance et la valeur de ces dommages et de leur réparation. Il est chargé, par ailleurs, de déterminer la valeur de tout véhicule.

Conformément aux dispositions de l'article D. 114-12 du Code des relations entre le public et l'Administration, tout usager peut obtenir un certificat d'information sur les normes applicables à la profession d'expert en automobile. Pour ce faire, il convient d'adresser votre demande à l'adresse suivante : devenir-expert-automobile@interieur.gouv.fr.

*Pour aller plus loin* : article L. 326-4 du Code de la route.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- en cas de création d’une entreprise individuelle, le CFE compétent est l’Urssaf ;
- en cas d’activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- en cas de création d’une société commerciale, le CFE compétent est la chambre de commerce et d’industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

Pour plus d’informations, il est conseillé de consulter les sites de l'Urssaf, [de la CCI de Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites), [de la CCI de Strasbourg](http://strasbourg.cci.fr/cfe/competences), [du CFE-CMA](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023) [et du Service public](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

#### Qualification obligatoire

Pour exercer en qualité d’expert en automobile, l’intéressé doit être titulaire, au choix :

- du brevet professionnel d'expert en automobile ou de la reconnaissance de cette qualité ou du diplôme d'État d'expert en automobile ou du relevé de notes du diplôme d'État d'expert en automobile délivré par le recteur d'académie ;
- d'un titre délivré par un autre État membre de l'Union européenne (UE) ou partie à l’accord sur l’Espace économique européen (EEE), ou d'un titre reconnu par l'un de ces États comme étant équivalent aux titres mentionnés précédemment ;
- soit d'une expérience professionnelle acquise en matière d'expertise automobile dans un autre État de l’UE ou de l’EEE.

*Pour aller plus loin* : décret n° 95-493 du 25 avril 1995 portant création et règlement général du diplôme d'expert en automobile. 

#### Qualification supplémentaire obligatoire pour contrôler les véhicules endommagés

Si le professionnel souhaite contrôler des véhicules endommagés, il doit, outre la qualification obligatoire, obtenir une qualification professionnelle supplémentaire spécifique.

L’obtention de cette qualification supplémentaire est subordonnée au suivi d’une formation spécifique destinée à l’actualisation et à la mise à jour des connaissances juridiques et techniques nécessaires à la conduite des procédures relatives aux véhicules endommagés. Le suivi de cette formation, d’une durée de 8 heures réparties sur une journée, donne lieu à la remise d’une attestation au participant.

Seuls trois organismes sont agréés par le ministère chargé des transports pour dispenser cette formation :

- [BCA Université](http://www.bca.fr/nos-offres-pro/les-services-specifiques) ;
- l’Institut de formation associée à l’automobile ([IFOR2A](%5b*http://anea.fr/ifor2a/*%5d(http://anea.fr/ifor2a/))) ;
- l’Institut national de sécurité routière et de recherches ([Inserr](%5b*http://www.inserr.fr/formation-categorie/experts-vehicules-endommages*%5d(http://www.inserr.fr/formation-categorie/experts-vehicules-endommages))).

**À savoir**

La qualification pour le contrôle des véhicules endommagés est valable jusqu’au 31 décembre de l’année suivant celle de la formation. Ainsi, pour conserver cette qualification, l’intéressé doit suivre un stage de recyclage annuel.

La liste nationale des experts en automobile précise pour chaque expert, le cas échéant, qu’il est titulaire de la formation permettant d’effectuer des contrôles de véhicules endommagés. Cette formation est réputée acquise pour les experts titulaires du diplôme d'expert en automobile depuis moins d'un an.

Certains professionnels peuvent exercer une activité de contrôle de véhicules endommagés sans avoir suivi la formation spécifique. En effet, cette qualification est également accordée, sous certaines conditions précisées par l’arrêté du 26 juillet 2011, aux experts en automobile qui, au choix :

- ont reçu une formation dispensée dans un autre État de l'UE ou de l’EEE équivalente ;
- établissent avoir une expérience professionnelle en matière de contrôle des véhicules endommagés acquise dans un autre État de l’UE ou de l’EEE.

*Pour aller plus loin* : article R. 326-11 du Code de la route ; arrêté du 26 juillet 2011 relatif à l'obtention et au maintien de la qualification pour le contrôle des véhicules endommagés pour les experts en automobile.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### En cas de Libre Prestation de Services (LPS)

Le professionnel ressortissant de l’UE ou de l’EEE, légalement établi dans l’un de ces États pour y exercer l’activité d’expert en automobile, est réputé disposer de la qualification professionnelle pour exercer en France, de façon occasionnelle et temporaire la même activité.

Cependant, si ni l’activité, ni la formation y conduisant n’est réglementée dans l’État d’établissement, l’intéressé doit prouver y avoir exercé l’activité d’expert en automobile pendant l’équivalent d’une année à temps complet au cours des dix années précédant la prestation qu’il veut effectuer en France.

Dans tous les cas, l’expert est inscrit à titre temporaire sur la liste nationale des experts en automobile.

##### Qualification requise pour le contrôle des véhicules endommagés

Si l'expert ressortissant de l’UE ou de l’EEE désire exercer en France l'activité de contrôle des véhicules endommagés de façon temporaire et occasionnelle, il doit apporter la preuve, par tout moyen, qu'il a exercé et détient une pratique des opérations de suivi des réparations et de remise en circulation des véhicules dans les mêmes conditions que celles applicables à ceux exerçant de manière permanente en France.

Si ni l'activité ni la formation y conduisant n'est réglementée dans l'État d'établissement, l'expert doit avoir exercé l'activité à temps plein pendant au moins deux ans au cours des dix années précédant la prestation. 

Si l'examen des pièces présentées pour justifier de la qualification de l'expert au contrôle des véhicules endommagés laisse apparaître une différence substantielle entre la réalité des qualifications du demandeur et celles requises pour exercer l'activité en France, et dans la mesure où cette différence est de nature à nuire à la sécurité des personnes, la qualification « véhicules endommagés » (VE) ne pourra être reconnue et accordée qu'après que l'expert ait suivi la formation continue au contrôle des véhicules endommagés.

Après vérification de sa qualification au contrôle des véhicules endommagés, le prestataire est inscrit sur la liste pour une durée d'un an avec mention de sa qualification « VE ».

*Pour aller plus loin* : article L. 326-4 du Code de la route et article 2-II et 2-III de l’arrêté du 26 juillet 2011 précité.

#### En cas de Libre Établissement (LE)

Pour exercer en France, à titre permanent, l’intéressé doit se soumettre aux mêmes conditions de qualifications que celles applicables aux ressortissants français (cf. infra « 2°. a. Qualifications professionnelles »).

**Bon à savoir**

L’inscription sur la liste nationale des experts en automobile est obligatoire pour les ressortissants européens souhaitant exercer cette profession en France de manière permanente.

##### Qualification requise pour le contrôle des véhicules endommagés

Si l'État d'établissement du demandeur réglemente l’accès à la profession, son exercice ou la formation conduisant à l’exercice de l’activité de contrôle des véhicules endommagés, le ressortissant doit justifier de sa compétence ou d’une formation dispensée ou reconnue dans l’un de ces États lui reconnaissant la qualification pour exercer ces opérations. Plus précisément, ces opérations doivent permettre à l'expert de délivrer, en conclusion de la procédure de contrôle des véhicules endommagés, un certificat de conformité garantissant que le véhicule est en tout point conforme à sa réception et qu'il peut circuler dans des conditions normales de sécurité. 

Si l'État d'établissement du demandeur ne réglemente pas cette profession, le ressortissant doit prouver par tout moyen avoir exercé pendant deux années à temps complet au cours des dix années précédentes cette activité d'expertise et posséder au moins une attestation de compétences ou un titre de formation attestant qu'il a été préparé à l'exercice de cette profession. Les deux années d'expérience professionnelle ne sont toutefois pas exigibles lorsque le ou les titre(s) de formation détenus sanctionnent une formation réglementée.

Si l'examen des pièces présentées pour justifier de la qualification de l'expert au contrôle des véhicules endommagés laisse apparaître une différence substantielle entre la réalité des qualifications du demandeur et celles requises pour exercer l'activité en France, et dans la mesure où cette différence est de nature à nuire à la sécurité des personnes, la qualification « VE » ne pourra être reconnue et accordée qu'après que l'expert ait suivi la formation continue au contrôle des véhicules endommagés.

*Pour aller plus loin* : article L. 326-4 du Code de la route et article 2-I et 2-III de l’arrêté du 26 juillet 2011.

### c. Conditions d’honorabilité et incompatibilités

#### Conditions d’honorabilité

Nul ne peut exercer l’activité d’expert en automobile s'il a fait l'objet d'une condamnation pour vol, escroquerie, recel, abus de confiance, agressions sexuelles, soustraction commise par un dépositaire de l'autorité publique, faux témoignage, corruption ou trafic d'influence, faux, ou pour un délit puni des peines du vol, de l'escroquerie ou de l'abus de confiance.

*Pour aller plus loin* : article L. 326-2 du Code de la route.

**Bon à savoir**

En cas de condamnation pour des faits constituant un manquement à l'honneur ou à la probité, le tribunal peut, à titre de peine complémentaire, interdire, temporairement ou définitivement, l'exercice de l’activité.

*Pour aller plus loin* : article L. 326-9 du Code de la route.

#### Incompatibilités

L’exercice de la profession d’expert en automobile est incompatible avec :

- la détention d'une charge d'officier public ou ministériel ;
- l'exercice d'activités touchant à la production, la vente, la location, la réparation et la représentation de véhicules à moteur et des pièces accessoires ;
- l'exercice de la profession d'assureur.

**Bon à savoir**

Les conditions dans lesquelles un expert en automobile exerce sa profession ne doivent pas porter atteinte à son indépendance.

*Pour aller plus loin* : article L. 326-6 du Code de la route.

### d. Assurance de responsabilité civile

L’expert en automobile doit obligatoirement souscrire un contrat d'assurance garantissant la responsabilité civile qu'il peut encourir en raison de ses activités professionnelles.

Pour plus de précisions concernant les conditions minimales du contrat d’assurance obligatoire, il est conseillé de se reporter à l’arrêté du 5 février 2002 modifiant l’arrêté du 13 août 1974 relatif aux conditions minimales du contrat d’assurance de la responsabilité civile professionnelle que doivent souscrire les experts en automobile.

*Pour aller plus loin* : article L. 326-7 du Code de la route et arrêté du 5 février 2002 précité.

### e. Quelques particularités de la réglementation de l’activité

#### Exercice salarié de la profession d’expert en automobile

Il est possible d’exercer la profession d’expert en automobile de façon indépendante ou salariée.

Les personnes exerçant de manière salariée sont soumises aux mêmes obligations que celles incombant aux indépendants : remplir la condition de qualification professionnelle, les conditions d’honorabilité, respecter les règles d’incompatibilité et souscrire une assurance de responsabilité civile professionnelle (cf. infra « 2°. Conditions d’installation »).

De plus, les experts en automobile salariés doivent fournir au ministère chargé des transports une copie de leur contrat de travail ou une attestation d'employé.

#### Règles d’accessibilité et de sécurité

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Demander son inscription sur la liste nationale des experts en automobile

Pour légalement exercer l’activité d’expert en automobile, l’intéressé doit être inscrit sur la liste nationale des experts en automobile, consultable sur le [site de la Sécurité routière](http://www.securite-routiere.gouv.fr/connaitre-les-regles/le-vehicule/la-liste-nationale-des-experts-automobile). L’expert qui exerce sans être inscrit s’expose à des sanctions administratives, pénales et civiles.

#### Autorité compétente

La demande d’inscription doit être adressée au ministère chargé des transports.

#### Délai de réponse

Le ministre chargé des transports accuse réception du dossier dans un délai d'un mois à compter de sa réception et informe le demandeur, le cas échéant, de tout document manquant. Le ministre dispose alors d’un délai de trois mois à compter de la date de réception du dossier complet pour statuer sur la demande d’inscription. Cette décision doit être motivée. Le silence gardé à l’expiration de ce délai vaut acceptation de la demande d’inscription.

#### Pièces justificatives

La demande d’inscription doit être accompagnée des pièces suivantes :

- le formulaire d’inscription complété, daté, et signé dont le modèle est disponible sur le [site de la Sécurité routière](http://www.securite-routiere.gouv.fr/connaitre-les-regles/le-vehicule/les-experts-en-automobile) ;
- un document en cours de validité établissant l’état civil de l’intéressé (carte d’identité, passeport, etc.) ;
- la copie de l’un des documents suivants :
  - le brevet professionnel d’expert en automobile ou la reconnaissance de la qualité d’expert en automobile ou du diplôme d’expert en automobile ou le relevé de notes du diplôme d’expert en automobile délivré par le recteur d’académie,
  - le titre délivré par un autre État membre de l’UE ou de l’EEE ou d’un titre reconnu par l’un de ces États équivalent aux titres mentionnés précédemment,
  - toute pièce de nature à établir l'expérience professionnelle acquise par l'intéressé en matière d'expertise automobile dans un autre État de l’UE ou de l’EEE ;
- une attestation d’assurance précisant que l’intéressé est couvert par une assurance de responsabilité civile pour l’exercice de sa profession « d’expert en automobile », datant de moins de trois mois ;
- un extrait du bulletin n° 3 du casier judiciaire (ou un document équivalent pour les ressortissants étrangers), datant de moins de trois mois ;
- un extrait Kbis ou le certificat d’inscription délivré par l’Insee ;
- une déclaration sur l’honneur attestant que l’expert :
  - ne détient pas de charge d’officier public ou ministériel ni n’exerce une activité incompatible avec la qualité d’expert en automobile,
  - n’a pas fait l'objet d’une condamnation visée à l’article 326-2 du Code de la route,
  - n’est pas sous le coup d’une condamnation pour des faits constituant un manquement à l’honneur ou à la probité ;
- un des documents informant des conditions d'exercice professionnel de l'activité (salariés, libéraux ou chefs d’entreprise) ;
- si l’expert souhaite effectuer les expertises sur « véhicule gravement endommagé » ou sur « véhicule économiquement irréparable » (VGE/VEI), fournir copie de l’attestation de suivi de la formation correspondante.

**À savoir**

Le cas échéant, les documents doivent être traduits en français par un traducteur agréé.

#### Coût

Gratuit.

*Pour aller plus loin* : article L. 326-4, et suivants et articles R. 326-5 et suivants du Code de la route.

### c. Effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité ponctuelle (LPS)

Les ressortissants établis dans un État de l’UE ou de l’EEE et souhaitant exercer, de manière ponctuelle et occasionnelle, l’activité d’expert en automobile en France doivent effectuer une déclaration préalable.

Cette déclaration écrite donne lieu à la vérification des qualifications du prestataire afin de s’assurer que sa prestation ne portera pas atteinte à la sécurité ou à la santé du bénéficiaire du service, du fait du manque de qualification professionnelle du prestataire.

La déclaration entraîne l’inscription sur la liste nationale des experts en automobile pour une année. Cette liste est disponible sur le [site de la Sécurité routière](http://www.securite-routiere.gouv.fr/connaitre-les-regles/le-vehicule/la-liste-nationale-des-experts-automobile).

#### Autorité compétente

La déclaration doit être adressée au ministère chargé des transports.

#### Délai de réponse

Dans le mois suivant la réception de la demande et des pièces justificatives requises, le ministre chargé des transports fait part de sa décision au prestataire. Le ministre peut décider de soumettre le demandeur à un entretien professionnel ou un stage professionnel si l’examen des documents fournis fait apparaître une différence substantielle entre ses qualifications professionnelles et celles requises pour exercer en France, si cette différence est de nature à nuire à la sécurité des personnes.

L'absence de demande de complément d'information ou de notification du résultat de la vérification des qualifications vaut acceptation de l’exercice de l’activité décrite dans la déclaration.

#### Pièces justificatives

La demande d’inscription doit être accompagnée des pièces suivantes :

- le formulaire d'inscription complété, daté et signé (le champ «SIRET» n'est pas à compléter) ;  
- une preuve de l'identité et de la nationalité du prestataire ;
- une attestation certifiant que le prestataire est légalement établi dans un autre État de l’UE ou de l’EEE pour y exercer la profession d'expert en automobile, et qu'il n'encourt, lorsque l'attestation est délivrée, aucune interdiction, même temporaire, d'exercer ;
- une preuve des qualifications professionnelles du demandeur ;
- lorsque la profession d'expert en automobile n'est pas réglementée dans l'État d'établissement, la preuve par tout moyen que le prestataire a exercé cette profession pendant au moins l’équivalent de deux années à temps plein au cours des dix années précédentes ;
- une preuve que le prestataire est couvert par un contrat d'assurance garantissant la responsabilité civile qu'il peut encourir ;
- si l'expert souhaite effectuer les expertises VGE/VEI, une preuve qu'il a exercé et qu’il détient une pratique des opérations de suivi des réparations et de remise en circulation des véhicules dans les mêmes conditions que pour ceux voulant exercer de façon permanente en France.

**À savoir**

Tous les documents doivent, le cas échéant, être traduits en français par un traducteur agréé.

#### Coût

Gratuit.

**À noter**

Le cas échéant, le ministre peut demander au prestataire de justifier qu'il possède les connaissances linguistiques nécessaires à l'exercice des fonctions d'expert en automobile en France.

*Pour aller plus loin* : articles R. 326-4 et suivants du Code de la route.

#### Demande de renouvellement d’inscription

Si, à l’issue de la première année, le prestataire souhaite de nouveau exercer son activité de manière temporaire et occasionnelle en France, il adresse, par tout moyen, au ministre chargé des transports une demande de renouvellement de son inscription sur la liste pour une durée d’un an. Cette demande de renouvellement est accompagnée de la preuve que le prestataire est couvert par un contrat d'assurance garantissant la responsabilité civile qu'il peut encourir en raison des activités d’expert en automobile.