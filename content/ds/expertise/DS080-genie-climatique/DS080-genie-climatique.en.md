﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS080" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Expertise" -->
<!-- var(title)="HVAC engineering" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="expertise" -->
<!-- var(title-short)="hvac-engineering" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/expertise/hvac-engineering.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="hvac-engineering" -->
<!-- var(translation)="Auto" -->

HVAC engineering
===================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Climate engineering combines all the heating, ventilation and air conditioning techniques that allow thermal comfort by controlling the energy performance of a building.

A climate engineering professional is anyone who performs one or all of the following activities:

- Commissioning refrigeration and air conditioning systems and facilities, including heat pumps and vehicle air conditioning, containing refrigerants, alone or in mixture;
- maintenance and repair of these equipment, as long as these operations require intervention on the circuit containing refrigerant fluids;
- controlling their waterproofing
- dismantling them;
- the recovery and loading of refrigerants in these devices;
- any other operation performed on equipment requiring the handling of refrigerant fluids.

*For further information*: Article R. 543-76 of the Environment Code.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI).

If the professional has a buying and resale activity, his activity is both artisanal and commercial.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

Only a qualified person who is professionally qualified or placed under the effective and permanent control of a qualified person can carry out one of the activities of climate engineering.

The person who performs one of the activities covered in paragraph "1." a. Definition" or who controls the exercise, must be the holder of the choice:

- A Certificate of Professional Qualification (CAP);
- A Professional Studies Patent (BEP);
- a diploma or a degree of equal or higher level approved or registered when it was issued to the national directory of professional certifications.

In the absence of one of these diplomas, the person concerned will have to justify an effective three years' professional experience, in a European Union (EU) state or party to the agreement on the European Economic Area (EEA), acquired as a leader self-employed or self-employed in a climate engineering trade. In this case, the person concerned will be able to apply to the relevant CMA for a certificate of professional qualification.

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 1 of Decree 98-246 of 2 April 1998; Decree 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act 96-603 of 5 July 1996.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

#### For Freedom to provide services

The eu's or EEA-married professional may engage in climate engineering in France on a temporary and casual basis, provided that he or she is legally established in one of these states to carry out the same activity.

He will first have to request it by written declaration to the relevant CMA (see infra "3°. b. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)).

If neither the activity nor the training leading to it is regulated in the State of The Institution, the person must also prove that he or she has been active in that state for at least one year full-time or part-time in the last ten years prior to the which he wants to perform in France.

**Please note**

The professional who meets these conditions is exempt from the registration requirements of the trades directory (RM) or the register of companies.

*For further information*: Section 17-1 of Act 96-603 of July 5, 1996.

#### For a Freedom of establishment

In order to carry out a climate engineering activity in France on a permanent basis, the professional national of the EU or the EEA must meet one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above: "2. a. Professional qualifications");
- Hold a certificate of competency or training degree required for the exercise of the activity in an EU or EEA state when that state regulates access or exercise of this activity on its territory;
- have a certificate of competency or a training document that certifies its preparation for the exercise of the activity when this certificate or title has been obtained in an EU or EEA state that does not regulate access or the exercise of this activity;
- be a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has been active for three years in the state that has admitted equivalence.

**Please note**

A national of an EU or EEA state that meets one of the above conditions may apply for a certificate of recognition of professional qualification (see below: "3. c. If necessary, apply for a certificate of professional qualification").

If the individual does not meet any of the above conditions, the CMA may ask him to perform a compensation measure in the following cases:

- if the duration of the certified training is at least one year less than that required to obtain one of the professional qualifications required in France to carry out the activity;
- If the training received covers subjects substantially different from those covered by one of the qualifications required to carry out the activity in France;
- If the effective and permanent control of the activity requires, for the exercise of some of its remits, specific training which is not provided in the Member State of origin and covers subjects substantially different from those covered by the certificate of competency or training designation referred to by the applicant.

*For further information*: Articles 17 and 17-1 of Act 96-603 of July 5, 1996; Articles 3 to 3-2 of Decree 98-246 of 2 April 1998.

**Good to know: compensation measures**

The CMA, which is applying for a certificate of recognition of professional qualification, notifies the applicant of his decision to have him perform one of the compensation measures. This decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose between an adjustment course of up to three years or an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organised within six months of the CMA's receipt of the applicant's decision to opt for the event. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the adjustment course, the applicant sends the CMA a certificate certifying that he has validly completed this internship, accompanied by an evaluation of the organization that supervised him. The CMA issues, on the basis of this certificate, a certificate of professional qualification within one month.

The decision to use a compensation measure may be challenged by the person concerned who must file an administrative appeal with the prefect within two months of notification of the decision. If his appeal is dismissed, he can then initiate a legal challenge.

*For further information*: Articles 3 and 3-2 of Decree 98-246 of 2 April 1998; Article 6-1 of Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

### c. Conditions of honorability and incompatibility

No one may practice a profession of climate engineering if he is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 of Act 96-603 of July 5, 1996.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Follow the installation preparation course (SPI)

The installation preparation course is a mandatory prerequisite for anyone applying for registration in the trades directory.

#### Terms of the internship

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

#### Exceptional postponement of the start of the internship

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

#### The result of the internship

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

#### Cost

The internship pays off. As an indication, the training cost about 260 euros in 2017.

#### Case of internship waiver

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including a degree in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

#### Internship exemption for EU or EEA nationals

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who:

- have been engaged in a professional activity requiring a level of knowledge equivalent to that provided by the internship for at least three years;
- or who have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, fully or partially, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France for the management of a craft company.

#### Terms of the internship waiver

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship. He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree 83-517 of June 24, 1983.

### b. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)

#### Competent authority

The CMA of the place in which the national wishes to carry out the benefit, is competent to issue the prior declaration of activity.

#### Supporting documents

The request for a pre-report of activity is accompanied by a complete file containing the following supporting documents:

- A photocopy of a valid ID
- a certificate justifying that the national is legally established in an EU or EEA state;
- a document justifying the professional qualification of the national who may be, at your choice:- A copy of a diploma, title or certificate,
  - A certificate of competency,
  - any documentation attesting to the national's professional experience.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Please note**

When the file is incomplete, the CMA has a period of fifteen days to inform the national and request all the missing documents.

#### Outcome of the procedure

Upon receipt of all the documents in the file, the CMA has one month to decide:

- either to authorise the benefit where the national justifies three years of work experience in an EU or EEA state, and to attach to that decision a certificate of professional qualification;
- or to authorize the provision when the national's professional qualifications are deemed sufficient;
- or to impose an aptitude test on him when there are substantial differences between the professional qualifications of the national and those required in France. In the event of a refusal to perform this compensation measure or if it fails to perform, the national will not be able to perform the service in France.

The silence kept by the competent authority in these times is worth authorisation to begin the provision of services.

*For further information*: Article 2 of the decree of 2 April 1998; Article 2 of the decree of 17 October 2017 relating to the submission of the declaration and requests provided for by Decree 98-246 of 2 April 1998 and Title I of Decree 98-247 of 2 April 1998.

### c. If necessary, request a certificate of recognition of professional qualification

The person concerned wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of recognition of professional qualification.

#### Competent authority

The request must be addressed to the territorially competent CMA.

#### Procedure

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the person concerned to complete it within a fortnight of filing the file. A receipt is issued as soon as the file is complete.

#### Supporting documents

The folder should contain the following parts:

- Applying for a certificate of professional qualification
- A certificate of competency or diploma or vocational training designation;
- Proof of the applicant's nationality
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, the proofs of the exercise of the activity for three years.

The CMA may request further information about its training or professional experience to determine the possible existence of substantial differences with the professional qualification required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (Ciep) to obtain additional information on the level of training of a diploma or certificate or a foreign designation, the applicant will have to pay a fee Additional.

**What to know**

If necessary, all supporting documents must be translated into French.

#### Response time

Within three months of the receipt, the CMA may:

- Recognise professional qualification and issue certificate of professional qualification;
- decide to subject the applicant to a compensation measure and notify him of that decision;
- refuse to issue the certificate of professional qualification.

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

#### Remedies

If the CMA refuses to issue the recognition of professional qualification, the applicant may initiate, within two months of notification of the refusal of the CMA, a legal challenge before the relevant administrative court. Similarly, if the person concerned wishes to challenge the CMA's decision to submit it to a compensation measure, he must first initiate a graceful appeal with the prefect of the department in which the CMA is based, within two months of notification of the decision. CMA. If he does not succeed, he may opt for a litigation before the relevant administrative tribunal.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the Community or another state party to the European Economic Area agreement.

### d. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Formalities of Declaration of Craft Enterprise.

### e. Special case of hydrofluorocarbon manipulation (HFC)

The national greenhouse gas regulation aims to define the concrete modalities for the implementation of the regulation (EU) 517/2014.

It is essentially contained in Articles R. 543-75 at R. 543-123 of the Environment Code and in the orders of February 29, 2016.

#### Fluoridated greenhouse gases: the role of operators

Operators handling fluids and equipment must:

- Obtain a "capacity certificate" for their business from an accredited body;
- ensure that their fluid handlers obtain a certificate of fitness or a certificate (in a personal capacity). The certificate of aptitude is required for the handling of fluorinated greenhouse gases for cold production and air conditioning. The certificate is required for the handling of fluorinated greenhouse gases for use in extinction, dielectric or solvent. The certificate of aptitude can be obtained either through the initial training followed by the person, or through recognised professional training, or from a certified assessing body (see below: "Fluorinated greenhouse gases: organisms certified evaluators"). Notices to the official newspaper specify the initial and professional trainings recognized. These reviews are updated regularly, they can be viewed by search on the site [legifrance.gouv.fr](http://www.legifrance.gouv.fr). Certificates can be obtained from accredited bodies;
- complete, after each intervention, an intervention sheet and a (dangerous) waste tracking slip for fluid waste. These two documents are merged when the waste is disposed of without being mixed with other waste;
- report to Ademe the amount of fluids handled and recovered;
- Apply a coloured macaroon to the equipment after a waterproofing check to determine if the equipment is sealed and allows the equipment to restart.

#### Fluoridated greenhouse gases: the role of accredited organisms

Approved organizations issue:

- capacity certifications for companies handling fluoridated greenhouse refrigerants, regardless of the use of these fluids. These certificates are valid for a period of 5 years. During this time, organizations must conduct one or more on-site audits;
- certificates for employees of these companies when the use of fluorinated greenhouse gases is extinction, dielectric or solvents. In other cases (cold use and air conditioning), it is a certificate of suitability that is required (see above "Fluorinated greenhouse gas: role of operators"). It can be obtained from certified evaluators (see infra "Fluorinated Greenhouse Gas: Certified Evaluators").

For the use of fluorinated greenhouse gases for cold, the following six organizations are approved for the issuance of capacity certificates, as of January 1, 2018:

| Approved body                           | Business category   |
|-----------------------------------------|---------------------|
| Afnor Certification                     | V                   |
| Bureau Veritas Certification            | I, II, III IV and V |
| Cemafroid                               | I, II, III IV and V |
| Dekra certification                     | V                   |
| Qualiclimafroid                         | I, II, III and IV   |
| SGS International Certification Service | I, II, III IV and V |
| Socotec Qualification Australia         | I, II, III IV and V |

#### Fluorinated greenhouse gases: certified evaluators

The evaluators issue individual certificates of fitness to employees working in a company that handle fluoridated greenhouse gases for cold use and air conditioning. The company itself must have a certificate of capacity, which it obtains from an accredited body.

The evaluators duly certified to issue the certificates of aptitude are mentioned on the[list of evaluators compiled by the Ministry of Ecological and Solidarity Transition](http://www.ecologique-solidaire.gouv.fr/sites/default/files/Fluides%20frigorig%C3%A8nes%20-%20liste%20des%20organismes%20%C3%A9valuateurs-1.pdf).

*For further information*: :[Ministry of Ecological and Solidarity Transition website](https://www.ecologique-solidaire.gouv.fr/substances-impact-climatique-fluides-frigorigenes#e3).

