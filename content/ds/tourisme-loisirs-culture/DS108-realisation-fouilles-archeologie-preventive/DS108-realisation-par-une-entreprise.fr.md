﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS108" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Réalisation par une entreprise de fouilles d’archéologie préventive" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="realisation-par-une-entreprise" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/realisation-par-une-entreprise-de-fouilles-d-archeologie-preventive.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="realisation-par-une-entreprise-de-fouilles-d-archeologie-preventive" -->
<!-- var(translation)="None" -->

# Réalisation par une entreprise de fouilles d’archéologie préventive

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’archéologie préventive, qui relève de missions de service public selon l'article L. 521-1 du Code du patrimoine, vise à assurer la conservation ou la sauvegarde par l'étude scientifique des éléments du patrimoine archéologique lorsqu'ils sont menacés par des travaux, publics ou privés. 

Lorsque des personnes (les aménageurs) entendent réaliser des travaux et aménagements sur le territoire, l’État peut être amené à prescrire un diagnostic afin de vérifier si lesdits travaux sont susceptibles de porter atteinte ou non à des éléments du patrimoine archéologique. Si le diagnostic archéologique met en évidence la présence d’un site archéologique dont les caractères et l'état de conservation le justifient, la réalisation d'une fouille permettant de sauvegarder l'information patrimoniale et scientifique que recèlent les vestiges est imposée par l'État.

Les fouilles sont réalisées sous la maîtrise d'ouvrage de l'aménageur qui peut choisir de faire appel soit à l’établissement public à caractère administratif chargé de réaliser les diagnostics d’archéologie préventive ([Inrap](https://www.inrap.fr/)), soit à un service archéologique territorial, soit à une personne de droit public ou privé titulaire d’un agrément délivré par l'État. 

## 2°. Conditions d'installation

### a. Pour les professionnels établis en France

Une personne de droit public ou privé peut réaliser des fouilles d’archéologie préventive prescrites par l’État, sous réserve que cette personne soit titulaire d’un agrément délivré par l’État attestant de sa compétence scientifique. 

Pour cela, la personne doit adresser un dossier de demande d’agrément aux services de l’État compétents. L’agrément permet la réalisation de fouilles préventives sur l'ensemble du territoire national. Il peut être limité à certains domaines ou périodes de la recherche archéologique. La demande d'agrément précise éventuellement les périodes ou les domaines souhaités.

Le dossier de demande doit comporter les qualifications, les spécialités et l'expérience professionnelle, dans le domaine de la recherche archéologique et de la conservation du patrimoine, des personnels employés par l'organisme dont l'agrément est demandé. Le dossier doit également établir la capacité financière du demandeur et son respect d'exigences en matière sociale, financière et comptable.

Il convient de noter que, lorsque la personne projetant d'exécuter les travaux est une personne privée, l'opérateur de fouilles ne peut être contrôlé, directement ou indirectement, par cette dernière ou l’un de ses actionnaires.

### b. Pour les ressortissants européens (LPS ou LE)

Elles sont appréciées dans les mêmes conditions que pour les ressortissants français.

### c. Condition d'honorabilité

Le dossier de demande d’agrément doit comprendre une attestation sur l’honneur que le demandeur n’entre dans aucun des cas mentionnés à l'article 48 du décret n° 2016-360 du 25 mars 2016 relatif aux marchés publics.

### d. Garantie financière

Le dossier de demande d’agrément doit comprendre l'ensemble des documents permettant d'établir la capacité financière du demandeur et notamment les comptes certifiés et la justification de leur dépôt auprès du tribunal de commerce.

### e. Quelques particularités de la réglementation de l’activité

L’activité de réalisation des opérations de fouilles d’archéologie préventive est régie par des considérations d’abord scientifiques avant d’être économiques. Elle suppose la rencontre d’une demande (celle des aménageurs titulaires d’une prescription de fouille de l’État) et d’une offre (notamment celle d’un opérateur agréé) sous le contrôle scientifique et technique de l’État. 

## 3°. Démarches et formalités d'installation

### a. Effectuer une demande d’agrément

La demande d'agrément doit être adressée au ministre chargé de la culture, via le [téléservice en ligne sur le site du Ministère de la Culture](https://mesdemarches.culture.gouv.fr/loc_re/mcc/requests/ARCHE_PREVE_agrement_01/?__CSRFTOKEN__=c29f1117-9058-4a4d-a539-a342ef0db0f1).

Le dossier de demande d’agrément doit comporter les pièces suivantes :

- la présentation de l'organisme et des personnels scientifiques justifiant l'agrément pour les périodes et domaines sollicités ;
- les qualifications, les spécialités et l'expérience professionnelle, dans le domaine de la recherche archéologique et de la conservation du patrimoine, des personnels employés par l'organisme dont l'agrément est demandé ;
- une attestation précisant la nature du contrat de travail ou justifiant une promesse d'embauche des personnels ;
- le projet scientifique que l'organisme se propose de développer ou de mettre en œuvre pour la durée de l'agrément ;
- la présentation des moyens techniques et opérationnels dont dispose l'organisme pour réaliser des fouilles préventives ;
- l'ensemble des documents permettant d'établir la capacité financière de l'organisme et notamment les comptes certifiés et la justification de leur dépôt auprès du tribunal de commerce ;
- le document d'évaluation des risques pour la santé et la sécurité des travailleurs prévu à l'article R. 4121-1 du Code du travail ;
- la déclaration sur l'honneur prévue à l'article 48 du décret n° 2016-360 du 25 mars 2016 relatif aux marchés publics ;
- lorsque l'agrément est sollicité par une association :
  - un exemplaire ou une copie du Journal officiel de la République française contenant l'insertion mentionnée à l'article 5 de la loi du 1er juillet 1901 relative au contrat d'association, ou, dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle, une copie de la décision du tribunal judiciaire ou de la juridiction supérieure inscrivant l'association,
  - à la place des comptes certifiés, le rapport moral et le rapport financier approuvés lors de la dernière assemblée générale ;
- en cas de demande de renouvellement de l’agrément, le dossier doit comporter un bilan scientifique de l'activité réalisée pendant la durée de l'agrément précédent. Ce bilan présente par périodes ou domaines les résultats scientifiques des opérations réalisées par l'opérateur dans le cadre de son agrément ainsi que les perspectives scientifiques qu'il entend développer.

Le ministre chargé de la culture et le ministre chargé de la recherche se prononcent, après consultation du Conseil national de la recherche archéologique, dans un délai de six mois à compter de la réception du dossier complet. L'absence de décision expresse à l'expiration de ce délai vaut agrément. L’agrément est valable cinq ans. 

La personne agréée doit transmettre chaque année à l'autorité compétente de l'État un bilan scientifique, administratif, social, technique et financier de son activité en matière d'archéologie préventive. Ce bilan doit comporter notamment : 

- une présentation des opérations archéologiques achevées et en cours, accompagnée, pour ces dernières, d'un état des travaux et études à réaliser, des dates prévisionnelles de rendu de rapport d'opération et d'un décompte des charges prévisionnelles nécessaires à leur réalisation ainsi que des pièces justifiant de la capacité financière de la personne agréée à achever ces opérations ;
- les comptes certifiés de l'année écoulée ;
- un bilan social ;
- un programme annuel de prévention des risques professionnels et d'amélioration des conditions de travail, au sens de l'article L. 4612-16 du Code du travail ;
- un organigramme et un état des effectifs actualisés.

### b. Effectuer une déclaration préalable d'activité pour les ressortissants européens exerçant une activité ponctuelle

Les ressortissants européens sont soumis aux mêmes exigences que les ressortissants français et que ceux légalement établis en France. Si un régime d’agrément similaire est déjà prévu par leur État d’origine, les demandeurs étrangers peuvent s’en prévaloir, sous réserve de l’appréciation de l’équivalence de cet agrément à celui prévu par la réglementation française.

### c. Effectuer une déclaration préalable d'activité pour un établissement secondaire

Un établissement secondaire disposant d’une autonomie juridique doit disposer d’un agrément s’il souhaite réaliser des fouilles archéologiques préventives.

## 4°. Textes de référence

- Article L. 522-1 et L.522-8 du Code du patrimoine ;
- Articles R. 522-8 à R. 522-13 du Code du patrimoine.