﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS108" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Archaeological rescue excavation operated by a business" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="archaeological-rescue-excavation-operated-by-a-business" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/archaeological-rescue-excavation-operated-by-a-business.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="archaeological-rescue-excavation-operated-by-a-business" -->
<!-- var(translation)="Auto" -->




Archaeological rescue excavation operated by a business
==============================================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Preventive archaeology, which is a matter of public service missions according to Article L. 521-1 of the Heritage Code, aims to ensure the conservation or safeguarding by scientific study of elements of archaeological heritage when threatened by public or private works.

When persons (developers) intend to carry out work and development on the territory, the State may be required to prescribe a diagnosis in order to ascertain whether or not such work is likely to affect elements of the heritage. Archaeological. If the archaeological diagnosis highlights the presence of an archaeological site whose characteristics and state of conservation justify it, the completion of an excavation to safeguard the heritage and scientific information contained by the remains is imposed by the state.

The excavations are carried out under the supervision of the developer who can choose to use either the public institution of administrative character responsible for making the diagnoses of preventive archaeology ([Inrap](https://www.inrap.fr/)), either to a territorial archaeological service or to a person of public or private law who holds a state-issued accreditation.

### b. competent CFE

2°. Installation conditions
------------------------------------

### a. For professionals based in France

A person under public or private law may carry out state-mandated preventive archaeology excavations, provided that the person holds a state-issued certification of his or her scientific competence.

To do so, the person must submit an application for accreditation to the relevant government services. Accreditation allows preventive excavations to be carried out throughout the country. It may be limited to certain areas or periods of archaeological research. The application for approval may specify the periods or areas desired.

The application file must include the qualifications, specialties and professional experience, in the field of archaeological research and heritage conservation, of staff employed by the organization whose accreditation is requested. The file must also establish the applicant's financial capacity and compliance with social, financial and accounting requirements.

It should be noted that where the person planning to carry out the work is a private person, the search operator cannot be controlled, directly or indirectly, by the latter or one of its shareholders.

### b. For European nationals (LPS or LE)

They are valued under the same conditions as for French nationals.

### c. Condition of honorability

The application for certification must include a certificate of honour that the applicant does not enter into any of the cases mentioned in section 48 of Decree No. 2016-360 of March 25, 2016 relating to public procurement.

### d. Financial guarantee

The application for certification must include all documents to establish the applicant's financial capacity, including certified accounts and the rationale for filing them with the Commercial Court.

### e. Some peculiarities of the regulation of the activity

The activity of carrying out excavation operations of preventive archaeology is governed by scientific considerations first before being economic. It involves meeting a request (that of the developers holding a state search order) and an offer (including that of an approved operator) under the scientific and technical control of the State.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Apply for accreditation

The application for accreditation must be addressed to the Minister for Culture, via the[online teleservice on the website of the Ministry of Culture](https://mesdemarches.culture.gouv.fr/loc_re/mcc/requests/ARCHE_PREVE_agrement_01/?__CSRFTOKEN__=c29f1117-9058-4a4d-a539-a342ef0db0f1).

The application for accreditation must include:

- The presentation of the organization and scientific staff justifying accreditation for the periods and fields requested;
- qualifications, specialties and professional experience, in the field of archaeological research and heritage conservation, of staff employed by the organization whose accreditation is required;
- A certificate specifying the nature of the employment contract or justifying a promise to hire staff;
- The scientific project that the organization intends to develop or implement for the duration of the accreditation;
- The presentation of the technical and operational means available to the organization to carry out preventive searches;
- all documents to establish the financial capacity of the organization, including certified accounts and the justification for filing them with the Commercial Court;
- The workers' health and safety risk assessment document in Section R. 4121-1 of the Labour Code;
- The declaration on honour in Article 48 of Decree No. 2016-360 of 25 March 2016 on public procurement;
- when approval is sought by an association:- a copy or copy of the Official Journal of the French Republic containing the insertion mentioned in Article 5 of the law of 1 July 1901 relating to the association contract, or, in the departments of lower Rhine, Upper Rhine and Moselle, a copy of The decision of the court or the higher court registering the association,
  - In place of the certified accounts, the moral report and the financial report approved at the last general meeting;
- In the event of an application to renew the accreditation, the file must include a scientific assessment of the activity carried out during the duration of the previous accreditation. This review presents in periods or fields the scientific results of the operations carried out by the operator in the context of its accreditation as well as the scientific perspectives it intends to develop.

The Minister for Culture and the Minister for Research, after consultation with the National Archaeological Research Council, will vote within six months of receiving the full file. The absence of an express decision at the end of this period is worth approval. The accreditation is valid for five years.

Each year, the approved person must submit to the competent authority of the State a scientific, administrative, social, technical and financial assessment of his activity in preventive archaeology. This assessment should include:

- a presentation of the completed and ongoing archaeological operations, accompanied by a report of the work and studies to be carried out, the forecast dates of the transaction report and a count of the necessary forecasting expenses. to carry them out as well as documents justifying the financial capacity of the approved person to complete these transactions;
- Certified accounts for the past year
- A social balance sheet
- an annual programme to prevent occupational hazards and improve working conditions, within the meaning of Article L. 4612-16 of the Labour Code;
- an organisational chart and an updated workforce statement.

### b. Make a prior declaration of activity for EU nationals engaged in a one-off activity

European nationals are subject to the same requirements as French nationals and those legally established in France. If a similar accreditation scheme is already provided for by their State of origin, foreign applicants can take advantage of it, subject to the assessment of the equivalence of this approval to that provided by French regulations.

### c. Make a pre-report of activity for a secondary school

A secondary school with legal autonomy must have a licence if it wishes to carry out preventive archaeological excavations.

4°. Reference texts
---------------------------------------

- Article L. 522-1 and L.522-8 of the Heritage Code;
- Articles R. 522-8 to R. 522-13 of the Heritage Code.

