﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS006" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Centre équestre et assimilé" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="centre-equestre-et-assimile" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/centre-equestre-et-assimile.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="centre-equestre-et-assimile" -->
<!-- var(translation)="None" -->

# Centre équestre et assimilé

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le centre équestre, et assimilés, est un établissement ouvert au public au sein duquel le professionnel propose la pratique de l'équitation et, à titre optionnel, les activités suivantes :

- l'enseignement de l'équitation ;
- le dressage, l'entraînement et l'ensemble des soins apportés aux équidés ;
- la vente des chevaux exploités dans le centre ;
- l'organisation de concours et compétitions ;
- La randonnée équestre.

Le centre peut également proposer les activités commerciales suivantes :

- la location d'équidés au public pour des activités de loisirs (promenade et tourisme) ;
- le transport de chevaux ;
- la location de boxes à des propriétaires ;
- la vente de produits alimentaires ou tout objet nécessaire à la pratique de l'équitation ;
- l'élevage d'équidés.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

- quelle que soit la forme juridique retenue, le CFE compétent sera la chambre d'agriculture ;
- en cas d'activité commerciale, le CFE compétent sera la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le professionnel qui souhaite exercer au sein d'un centre équestre doit avoir la qualité d'éducateur sportif et être titulaire d'une carte professionnelle (cf. infra « 3°. a. Déclaration d'éducateur sportif en vue d'obtenir une carte professionnelle »). Toutefois, cette condition de qualification n'est requise que si le centre équestre propose une activité d'enseignement, d'animation ou d'encadrement de l'équitation.

Le cas échéant, l’intéressé doit être titulaire d’un diplôme, d’un titre à finalité professionnelle ou d’un certificat de qualification :

- garantissant sa compétence en matière de sécurité des pratiquants et des tiers dans l’activité physique ou sportive considérée ;
- enregistré au [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Parmi les diplômes, titres ou certificats de qualification qui remplissent ces deux critères, on peut citer pour exemple :

- le certificat de qualification professionnelle (CQP) ;
- le brevet professionnel de la jeunesse, de l’éducation populaire et du sport (BPJEPS) ;
- le diplôme d’État de la jeunesse, de l’éducation populaire et du sport (DEJEPS).

Chacun d’entre eux est décerné pour une discipline sportive déterminée (escrime, judo-jujitsu, golf, etc.) ou un groupe d’activités (activités équestres, loisirs tous publics, sport automobile, etc.).

La liste des diplômes, titres ou certificats permettant d’exercer en tant qu’éducateur sportif en France est établie à l’annexe II-1 de l’article A212-1 du Code du sport. Cette liste précise également les conditions d’exercice attachées à chaque diplôme (les limites géographiques d’exercice, la catégorie de public que peut encadrer l’intéressé, la nature exacte de l’activité sportive concernée, la durée annuelle maximale d’exercice, etc.).

**À noter**

Ne peuvent exercer que sous la responsabilité d'une personne titulaire d'un BPJEPS les professionnels titulaires des diplômes suivants :

- un brevet d'animateur Poney (BAP) ;
- un certificat de qualification professionnelle animateur soigneur assistant (CQP ASA) ;
- un brevet d'aptitude professionnelle d'assistant animateur technicien (BAPAAT) ;
- un diplôme d'accompagnateur de tourisme équestre (ATE) ou de guide de tourisme équestre (GTE) décerné par la Fédération française d'équitation (FFE) pour l'accompagnement de promenade ou de randonnée. Ces diplômes ne permettent pas d'enseigner l'équitation.

#### Bon à savoir : l’éducateur sportif stagiaire

Les personnes qui sont en cours de formation menant à l’obtention d’un diplôme, titre ou certificat de qualification peuvent exercer sous l’appellation d’éducateur sportif stagiaire et créer leur entreprise. Cependant, dans ce cas, elles doivent respecter la réglementation attachée à ces diplômes, titres ou certificats de qualification, être placées sous l’autorité d’un tuteur et avoir satisfait aux exigences préalables à leur mise en situation pédagogique.

*Pour aller plus loin* : articles L. 212-1 et suivants du Code du sport ; annexe II-1 (article A212-1) du Code du sport.

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de Services (LPS) ou Libre Établissement (LE))

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi dans cet état et exerçant l'activité d'éducateur sportif au sein d'un centre équestre peut exercer, à titre temporaire et occasionnel, ou permanent, la même activité en France.

#### En vue d'un exercice temporaire et occasionnel (LPS)

Le ressortissant UE peut exercer l'activité d'éducateur sportif au sein d'un centre équestre dès lors qu'il justifie :

- avoir exercé l'activité dans un ou plusieurs État(s) membre(s) pendant au moins un an au cours des dix dernières années lorsque ni l'accès à la profession ni la formation ne sont réglementées dans cet État membre ;
- avoir les connaissances linguistiques nécessaires à l'exercice de sa profession en France.

Dès lors qu'il remplit ces conditions, le ressortissant doit, avant toute prestation, adresser une déclaration d'activité au préfet du département au sein duquel il souhaite exercer (cf. infra « 3°. b. Effectuer une déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS) »).

*Pour aller plus loin* : article L. 212-7 du Code du sport.

#### En vue d'un exercice permanent (LE)

Le ressortissant UE peut exercer de façon permanente l'activité d'éducateur sportif dès lors qu'il remplit l'une des conditions suivantes :

- si l’État membre d’origine réglemente l’accès ou l’exercice de l’activité :
  - être titulaire d’une attestation de compétences ou d’un titre de formation délivré par l’autorité compétente d’un État de l’UE ou de l’EEE qui atteste d’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France,
  - être titulaire d’un titre acquis dans un État tiers et admis en équivalence dans un État de l’UE ou de l’EEE et justifier avoir exercé cette activité pendant au moins deux ans à temps complet dans cet État ;
- si l’État membre d’origine ne réglemente ni l’accès ni l’exercice de l’activité :
  - avoir exercé cette activité dans un État de l’UE ou de l’EEE, à temps complet pendant au moins un an au cours des dix dernières années et être titulaire d’une attestation de compétences ou d’un titre de formation délivré par l’autorité compétente d’un de ces États qui atteste d’une préparation à l’exercice de l’activité, ainsi qu’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France,
  - être titulaire d’un titre attestant d’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France, délivré par l’autorité compétente d’un État de l’UE ou de l’EEE et sanctionnant une formation réglementée visant spécifiquement l’exercice de tout ou partie des activités mentionnées à l’article L. 212-1 du Code du sport et consistant en un cycle d’études complété, le cas échéant, par une formation professionnelle, un stage ou une pratique professionnelle.

Dès lors que le ressortissant remplit l’une des quatre conditions précitées, le ressortissant est réputé qualifié professionnellement et doit effectuer une déclaration préalable d'activité auprès de l'autorité compétente (cf. infra « 3°. c. Effectuer une déclaration préalable pour le ressortissant UE en vue d'un exercice permanent (LE) »).

Néanmoins, si les qualifications professionnelles du ressortissant présentent une différence substantielle avec les qualifications requises en France qui ne permettrait pas de garantir la sécurité des pratiquants et des tiers, il peut être amené à se soumettre à une épreuve d’aptitude ou accomplir un stage d’adaptation (cf. infra « 3°. c. Bon à savoir : mesures de compensation »).

Le ressortissant doit posséder la connaissance de la langue française nécessaire à l’exercice de son activité en France, en particulier afin de garantir la sécurité des activités physiques et sportives et sa capacité à alerter les secours.

*Pour aller plus loin* : articles L. 212-7, et articles R. 212-88 à R. 212-90 du Code du sport.

### c. Conditions d’honorabilité et sanctions pénales

#### Honorabilité

Le professionnel ne peut exercer en qualité d'éducateur sportif en France s'il a fait l’objet d’une condamnation pour crime ou pour l’un des délits suivants :

- homicide volontaire ;
- torture ou actes de barbarie ;
- violences ou menaces ;
- agressions sexuelles ;
- trafic ou usage illicite de stupéfiants ;
- mise en danger d’autrui ;
- proxénétisme et infractions qui en résultent ;
- mise en péril des mineurs ;
- infractions prévues aux articles L. 232-25 à L. 232-27 du Code du sport ;
- à titre de peine complémentaire à une infraction en matière fiscale : condamnation à une interdiction temporaire d’exercer, directement ou par personne interposée, pour son compte ou le compte d’autrui, toute profession industrielle, commerciale ou libérale (article 1750 du Code général des impôts).

De plus, nul ne peut enseigner, animer ou encadrer une activité physique ou sportive auprès de mineurs s’il a fait l’objet d’une mesure administrative d’interdiction de participer, à quelque titre que ce soit, à la direction et à l’encadrement d’institutions et d’organismes soumis aux dispositions législatives ou réglementaires relatives à la protection des mineurs accueillis en centre de vacances et de loisirs, ainsi que de groupements de jeunesse ou s’il a fait l’objet d’une mesure administrative de suspension de ces mêmes fonctions.

*Pour aller plus loin* : article L. 212-9 du Code du sport.

#### Sanctions pénales

Le professionnel encourt une peine d'un an d'emprisonnement et de 15 000 euros d'amende dès lors qu'il :

- exerce l'activité de professeur, de moniteur, d'éducateur, d'entraîneur ou d'animateur d'activité physique ou sportive au sein d'un centre équestre sans être qualifié professionnellement ;
- emploie un ressortissant français ou de l'UE qui n'est pas qualifié professionnellement.

*Pour aller plus loin* : article L. 212-8 du Code du sport.

### d. Quelques particularités de la réglementation de l’activité

#### Déclaration de détention d'équidés

Tout centre équestre propriétaire d'équidés est tenu de se déclarer auprès de l'Institut français du cheval et de l'équitation (IFCE) avant l'arrivée du premier animal.

Cette déclaration dont le modèle est fixé à l'annexe I de l'arrêté du 25 juin 2018 précisant les modalités de déclaration des détenteurs d'équidés et des lieux de stationnement doit comporter :

- l'identité et l'adresse du détenteur des animaux ;
- l'adresse des lieux de leur stationnement si celle-ci est différente de celle du détenteur.

Cette identification donne lieu à l'attribution d'un numéro national unique.

L'arrêté du 25 juin 2018 précise en annexe I la liste des organismes tiers auprès desquels le détenteur peut confier le soin de réaliser cette déclaration auprès de l'IFCE. Ainsi, la FFE transmet périodiquement la liste des adhérents à l'IFCE dès lors qu'ils ont déclaré être lieu de détention d'équidés.

*Pour aller plus loin* : article L. 212-19, et articles D. 212-47 et suivants du Code rural et de la pêche maritime.

#### Demande d'autorisation d'exploiter

Le centre équestre doit effectuer une déclaration auprès de la préfecture dès lors qu'il :

- effectue une reprise des terres agricoles, et ce, en l'absence de diplôme agricole ;
- exerce une activité d'élevage d'équidés. Le cas échéant, le professionnel devra justifier de diplômes agricoles ou d'une expérience de cinq ans au cours des quinze dernières années, en qualité d'exploitant, salarié exploitant, aide familiale ou collaborateur d'exploitation.

Cette déclaration doit être adressée sur papier libre par lettre recommandée avec avis de réception au préfet de la région sur le territoire de laquelle est situé le centre ou bien le lieu où se trouve le siège de l'exploitation du déclarant.

*Pour aller plus loin* : articles R. 331-1 et suivants du Code rural et de la pêche maritime.

#### Déclaration de recensement d'équipement sportif

Le centre équestre propriétaire d'un équipement sportif doit effectuer une déclaration en vue d'identifier les caractéristiques des équipements sportifs. Celle-ci doit être adressée dans les trois mois suivants l'ouverture du centre au préfet du département dans lequel il est situé.

Cette déclaration peut être effectuée directement en ligne via un formulaire de déclaration sur le [site du ministère chargé des sports](http://www.res.sports.gouv.fr/Declaration_en_Ligne.aspx).

*Pour aller plus loin* : article L. 312-2 du Code du sport.

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

Il et conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

#### Assurance obligatoire

Le professionnel qui souhaite ouvrir un centre équestre doit souscrire une assurance de responsabilité civile les couvrant ainsi que leurs salariés contre les risques liés à leur activité professionnelle.

En cas de manquement à cette obligation, le professionnel encourt une peine de six mois de prison et une amende de 7 500 euros.

En outre, le professionnel doit informer les adhérents d'une association ou fédération sportive de l'intérêt de souscrire une assurance en vue de couvrir les risques liés à la pratique sportive.

*Pour aller plus loin* : articles L. 321-1 et L. 321-4 du Code du sport.

#### Obligation d'information

Le professionnel doit procéder à l'affichage au sein du centre équestre des informations suivantes :

- les diplômes ou titres professionnels de l'ensemble des personnes exerçant au sein du centre, ainsi que leurs carte professionnelle ou les attestations de stagiaire ;
- l'ensemble des textes relatifs aux mesures d'hygiène et de sécurité et des normes techniques applicables à l'encadrement des activités physiques et sportives ;
- l'attestation d'assurance du centre ;
- les numéros d'urgence.

*Pour aller plus loin* : article R. 322-5 du Code du sport.

#### Autres dispositions

Il existe par ailleurs de nombreuses réglementations propres à la détention d'équidés notamment relatives :

- à la présence d'une trousse de secours déstinée à apporter les premiers soins et un moyen de communication pour alerter les services de secours ;
- aux règles d'hygiène et de sécurité liées à l'utilisation d'équidés ;
- à l'obligation de faire appel à un vétérinaire ;
- à l'organisation de compétitions (se renseigner auprès de la Fédération Française d’Équitation ([FFE](https://www.ffe.com/))) ;
- à la circulation à cheval le long de cours d'eau, du littoral, en forêt ou sur la voie publique ;
- au transport des équidés.

Pour plus d'informations il est conseillé de se renseigner auprès de l'[IFCE](http://www.ifce.fr/).

## 3°. Démarches et formalités d’installation

### a. Déclaration d'éducateur sportif en vue d'obtenir une carte professionnelle

Toute personne souhaitant exercer l’une des professions régies par l’article L. 212-1 du Code du sport doit déclarer son activité au préfet du département du lieu où elle compte exercer son activité à titre principal. Cette déclaration déclenche l’obtention d’une carte professionnelle.

La déclaration doit être renouvelée tous les cinq ans.

#### Autorité compétente

La déclaration doit être adressée à la direction départementale de la cohésion sociale (DDCS) ou direction départementale de la cohésion sociale et de la protection des populations (DDCSPP) du département d’exercice ou du principal lieu d'exercice, ou directement en ligne sur le [site officiel du ministère chargé des sports](https://eaps.sports.gouv.fr).

#### Délai

Dans le mois suivant le dépôt du dossier de déclaration, la préfecture envoie un accusé de réception au déclarant. La carte professionnelle, valable cinq ans, est ensuite adressée au déclarant.

#### Pièces justificatives

La demande doit comporter :

- un formulaire de déclaration [Cerfa n° 12699](https://www.service-public.fr/particuliers/vosdroits/R32050) ;
- les pièces justificatives suivantes :
  - une copie d’une pièce d’identité en cours de validité,
  - une photo d’identité,
  - une déclaration sur l’honneur attestant de l’exactitude des informations figurant dans le formulaire,
  - une copie de chacun des diplômes, titres ou certificats invoqués,
  - une copie de l’autorisation d’exercice, de l’équivalence de diplôme, le cas échéant,
  - un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées, datant de moins d’un an.

En cas de renouvellement de la déclaration, il faut joindre :

- le formulaire Cerfa n° 12699*02 ;
- une photo d’identité ;
- une copie de l’attestation de révision en cours de validité pour les qualifications soumises à l’obligation de recyclage ;
- un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées datant de moins d’un an.

La préfecture demandera elle-même aux services compétents la communication d’un extrait de moins de trois mois du casier judiciaire du déclarant pour vérifier l’absence d’incapacité ou d’interdiction d’exercer.

#### Coût

Gratuit

*Pour aller plus loin* : articles L. 212-11, R. 212-85, et articles A. 212-176 à A. 212-178 du Code du sport.

### b. Effectuer une déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser une demande au préfet du département où il souhaite exercer la majeure partie de son activité.

#### Pièces justificatives

La demande du ressortissant doit comporter :

- le formulaire type fixé à l'annexe II-12-3 du Code du sport complété et signé ;
- les pièces justificatives suivantes, le cas échéant assorties de leur traduction en français :
  - une photographie d'identité du demandeur,
  - une copie de sa pièce d'identité,
  - une copie de son attestation de compétences ou de son titre de formation,
  - une copie des documents justifiant que le ressortissant est légalement établi dans l’État membre et qu'il n'encourt aucune interdiction d'exercer son activité,
  - une copie de tout document justifiant qu'il a exercé l'activité d'éducateur sportif pendant au moins un an au cours des dix dernières années si ni l'activité ni la formation ne sont réglementées dans l’État membre d'établissement ;
- une déclaration sur l'honneur attestant de l'exactitude des informations fournies.

#### Délais

Dans le mois suivant la réception du dossier de déclaration, le préfet notifie au prestataire soit :

- une demande d’informations complémentaires (dans ce cas, le préfet dispose de deux mois pour donner sa réponse) ;
- un récépissé de déclaration de prestation de services s’il ne procède pas à la vérification des qualifications. Dans ce cas, la prestation de services peut débuter ;
- qu’il procède à la vérification des qualifications. Dans ce cas, le préfet délivre ensuite au prestataire un récépissé lui permettant de débuter sa prestation ou, si la vérification des qualifications fait apparaître des différences substantielles avec les qualifications professionnelles requises en France, le préfet soumet le prestataire à une épreuve d’aptitude ou un stage d'adaptation.

Dans tous les cas, en l’absence de réponse dans les délais précités, le prestataire est réputé exercer légalement son activité en France.

*Pour aller plus loin* : articles R. 212-92 à R. 212-93 et article A. 212-182-2 du Code du sport.

### c. Effectuer une déclaration préalable pour le ressortissant UE en vue d'une exercice permanent (LE)

Tout ressortissant de l’UE ou de l’EEE qualifié pour y exercer tout ou partie des activités mentionnées à l’article L. 212-1 du Code du sport et souhaitant s’établir en France doit en faire préalablement la déclaration au préfet du département dans lequel il compte exercer à titre principal.

Cette déclaration permet au déclarant d’obtenir une carte professionnelle et ainsi d’exercer en toute légalité en France dans les mêmes conditions que les ressortissants français.

La déclaration doit être renouvelée tous les cinq ans.

#### Autorité compétente

La déclaration doit être adressée à la direction départementale en charge de la cohésion sociale (DDCS) ou à la direction départementale en charge de la cohésion sociale et de la protection des populations (DDCSPP).

#### Pièces justificatives pour la première déclaration d’activité

Le dossier de déclaration d’activité doit contenir :

- le formulaire de déclaration complété et signé, dont le modèle est fixé à l’annexe II-12-2-a du Code du sport ;
- les pièces justificatives suivantes, le cas échéant assorties de leur traduction en français :
  - une photo d’identité,
  - une copie d’une pièce d’identité en cours de validité,
  - un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives (de moins d’un an),
  - une copie de l’attestation de compétences ou du titre de formation, accompagnée de documents décrivant le cursus de formation (programme, volume horaire, nature et durée des stages effectués),
  - le cas échéant, une copie de toute pièce justifiant de son expérience professionnelle,
  - si le titre de formation a été obtenu dans un État tiers, les copies des pièces attestant que ce titre a été admis en équivalence dans un État de l’UE ou de l’EEE qui réglemente l’activité,
  - les documents attestant que le déclarant n’a pas fait l’objet, dans l’État membre d’origine, d’une des condamnations ou mesures mentionnées aux articles L. 212-9 et L. 212-13 du Code du sport.

#### Pièces justificatives pour un renouvellement de déclaration d’activité

Le dossier de renouvellement de déclaration d’activité doit contenir :

- un exemplaire du formulaire de renouvellement de déclaration dont le modèle est fourni à l’annexe II-12-2-b du Code du sport ;
- une photo d’identité ;
- un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives datant de moins d’un an.

#### Délais et issue de la procédure

La décision du préfet de délivrer la carte professionnelle intervient dans un délai de trois mois à compter de la présentation du dossier complet par le déclarant. Ce délai peut être prorogé d’un mois sur décision motivée. Si le préfet décide de ne pas délivrer la carte professionnelle ou de soumettre le déclarant à une mesure de compensation (épreuve d’aptitude ou stage), sa décision doit être motivée (cf. infra « Bon à savoir : mesure de compensation »).

**À noter**

La demande doit être renouvelée tous les cinq ans.

#### Coût

Gratuit.

#### Voies de recours

Tout recours contentieux doit être exercé dans les deux mois de la notification de la décision auprès du tribunal administratif compétent.

*Pour aller plus loin* : articles R. 212-88 à R. 212-91, article A. 212-182 et les annexes II-12-2-a et II-12-b du Code du sport.

#### Bon à savoir : mesures de compensation

S’il existe une différence substantielle entre la qualification du requérant et celle requise en France pour exercer la même activité, le préfet saisit la commission de reconnaissance des qualifications placée auprès du ministre chargé des sports. Cette commission, après examen et instruction du dossier, émet, dans le mois de sa saisine, un avis qu’elle adresse au préfet. 

Dans son avis, la commission peut :

- estimer qu’il existe effectivement une différence substantielle entre la qualification du déclarant et celle requise en France. Dans ce cas, la commission propose de soumettre le déclarant à une épreuve d’aptitude ou à un stage d’adaptation. Elle définit la nature et les modalités précises de ces mesures de compensation (nature des épreuves, modalités de leur organisation et de leur évaluation, période d’organisation, contenu et durée du stage, types de structures pouvant accueillir le stagiaire, etc.) ;
- estimer qu’il n’y a pas de différence substantielle entre la qualification du déclarant et celle requise en France.

À réception de l’avis de la commission, le préfet notifie sa décision motivée au déclarant (il n’est pas obligé de suivre l’avis de la commission) :

- s’il exige qu’une mesure de compensation soit effectuée, le déclarant dispose d’un délai d’un mois pour choisir entre la ou les épreuve(s) d’aptitude et le stage d’adaptation. Le préfet délivre ensuite une carte professionnelle au déclarant qui a satisfait aux mesures de compensation. En revanche, si le stage ou l’épreuve d’aptitude ne sont pas satisfaisants, le préfet notifie sa décision motivée de refus de délivrance de la carte professionnelle à l’intéressé ;
- s’il n’exige pas de mesure de compensation, le préfet délivre une carte professionnelle à l’intéressé.

*Pour aller plus loin* : articles R. 212-84 et D. 212-84-1 du Code du sport.

### d. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit déclarer son activité auprès de la chambre de l'agriculture ou s’immatriculer au registre du commerce et des sociétés (RCS).

### e. Le cas échéant, enregistrer les statuts de la société

L'exploitant d'un centre équestre doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.