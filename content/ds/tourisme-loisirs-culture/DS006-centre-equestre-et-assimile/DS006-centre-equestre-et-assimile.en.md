﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS006" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Equestrian centre and similar establishment" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="equestrian-centre-and-similar-establishment" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/equestrian-centre-and-similar-establishmenthtml" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="equestrian-centre-and-similar-establishment" -->
<!-- var(translation)="Auto" -->


Equestrian centre and similar establishment
=================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The equestrian centre, and assimilated, is an establishment open to the public in which the professional offers the practice of horse riding and, as an option, the following activities:

- teaching riding;
- training, training and all the care given to equines;
- the sale of horses operated in the centre;
- Organising competitions and competitions;
- The equestrian ride.

The centre can also offer the following business activities:

- Renting equines to the public for leisure activities (walking and tourism);
- Transporting horses
- Renting boxes to owners;
- selling food or anything necessary for horse riding;
- equine farming.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- whatever legal form is chosen, the competent CFE will be the chamber of agriculture;
- In the event of commercial activity, the relevant CFE will be the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional who wishes to practice in an equestrian centre must have the status of a sports educator and hold a professional card (see infra "3°. a. Statement of sports educator in order to obtain a professional card"). However, this qualification requirement is only required if the equestrian centre offers a teaching, animation or equestrian coaching activity.

If so, the person must have a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](http://www.rncp.cncp.gouv.fr/).

Among the diplomas, titles or certificates of qualification that meet these two criteria are:

- Certificate of Professional Qualification (CQP);
- The Professional Youth, Popular Education and Sport Certificate (BPJEPS);
- State Diploma of Youth, Popular Education and Sport (DEJEPS).

Each of them is awarded for a specific sports discipline (fencing, judo-jujitsu, golf, etc.) or a group of activities (equestrian activities, all public leisure, motorsport, etc.).

The list of diplomas, titles or certificates to practise as a sports educator in France is drawn up at the[Appendix II-1 of Article A212-1 of the Code of Sport](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000018752146&dateTexte=&categorieLien=cid). This list also specifies the conditions of practice attached to each degree (geographic limits of exercise, the category of public that the person concerned can supervise, the exact nature of the sports activity concerned, the maximum annual duration exercise, etc.).

**Please note**

Professionals with the following degrees may only be practised under the responsibility of a person holding a PJEPS.

- A Pony Animator Patent (BAP);
- a certificate of professional qualification as an assistant healer (CQP ASA);
- A certificate of professional aptitude as a technical assistant animator (BAPAAT);
- a diploma of equestrian tourism accompanist (ATE) or equestrian tourism guide (GTE) awarded by the French Riding Federation (FFE) for walking or hiking accompaniment. These diplomas do not allow you to teach horseback riding.

**Good to know: the trainee sports educator**

Individuals who are in the process of training leading to a diploma, title or certificate of qualification may practice as a trainee sports educator and start a business. However, in this case, they must comply with the regulations attached to these diplomas, titles or certificates of qualification, be placed under the authority of a tutor and have met the requirements prior to their pedagogical situation.

*For further information*: Articles L. 212-1 and the following articles of the Code of Sport; Appendix II-1 (Article A212-1) of the Code of Sport.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

Any national of a Member State of the European Union (EU) or a State party to the agreement on the European Economic Area (EEA) legally established in that state and practising the activity of sports educator within an equestrian centre may exercise, as a temporary and casual, or permanent, the same activity in France.

**For temporary and casual exercise (LPS)**

The EU national may work as a sports educator in an equestrian centre if he justifies:

- have been active in one or more member states for at least one year in the last ten years when neither access to the profession nor training is regulated in that Member State;
- have the language skills necessary to practice your profession in France.

As soon as the national fulfils these conditions, he must, before any performance, send a declaration of activity to the prefect of the department in which he wishes to practice (see infra "3°. b. Make a pre-declaration for the EU national for a temporary and casual exercise (LPS)").

*For further information*: Article L. 212-7 of the Code of Sport.

**For a permanent exercise (LE)**

The EU national may be a permanent sports educator if he fulfils one of the following conditions:

- if the Member State of origin regulates access or the exercise of the activity:- have a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France,
  - holding a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having held that activity for at least two years full-time in that state;
- If the Member State of origin does not regulate access or the exercise of the activity:- have carried out this activity in an EU or EEA state, on a full-time basis for at least one year in the last ten years and have held a certificate of competency or a training certificate issued by the competent authority of one of these states which attests to a preparation for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France,
  - be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

Once the national fulfils one of the four conditions mentioned above, the national is deemed to be professionally qualified and must make a prior declaration of activity with the competent authority (see infra "3°. c. Make a pre-declaration for the EU national for a permanent exercise (LE)).

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or complete an adjustment course (see infra "3 degrees). c. Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*For further information*: Articles L. 212-7, and Articles R. 212-88 to R. 212-90 of the Code of Sport.

### c. Conditions of honourability and criminal penalties

**Integrity**

The professional cannot practise as a sports educator in France if he has been convicted of a crime or for one of the following offences:

- Voluntary manslaughter;
- torture or acts of barbarism;
- violence or threats;
- Sexual assaults;
- trafficking or illicit use of narcotics;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- violations of Articles L. 232-25 to L. 232-27 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups or if it has been the subject of an administrative measure to suspend these same functions.

*For further information*: Article L. 212-9 of the Code of Sport.

**Criminal sanctions**

The professional faces a one-year prison sentence and a fine of 15,000 euros if he:

- practises as a teacher, instructor, educator, coach or physical activity or sports facilitator in an equestrian centre without being professionally qualified;
- employs a French or EU national who is not professionally qualified.

*For further information*: Article L. 212-8 of the Code of Sport.

### d. Some peculiarities of the regulation of the activity

**Statement of detention of equines**

Any equestrian centre that owns equines is required to report to the French Institute of Horse and Riding (IFCE) before the arrival of the first animal.

This statement, the model of which is set at the[Appendix I of the June 25, 2018 order](https://www.legifrance.gouv.fr/eli/arrete/2018/6/25/AGRG1802984A/jo/texte#JORFSCTA000037131512) specifying the reporting procedures for equine holders and parking spaces must include:

- The identity and address of the animal owner
- the address of the parking spaces if it is different from that of the holder.

This identification results in the allocation of a single national number.

The order of 25 June 2018 specifies in Appendix I the list of third-party organizations from which the holder can entrust the responsibility of carrying out this declaration with the IFCE. Thus, the FFE periodically sends the list of ifCE members once they have declared their place of detention of equines.

*For further information*: Article L. 212-19, and articles D. 212-47 and the following of the Rural Code and Marine Fisheries.

**Application for permission to operate**

The equestrian centre must make a declaration to the prefecture as soon as it:

- takes over farmland in the absence of an agricultural degree;
- is engaged in equine farming. If necessary, the professional will have to justify agricultural diplomas or five years of experience in the last fifteen years, as an operator, an employee, a domestic helper or a farm worker.

This declaration must be addressed on free paper by letter recommended with notice of receipt to the prefect of the region on the territory of which the centre is located or the place where the headquarters of the registrant's operation is located.

*For further information*: Articles R. 331-1 and the following of the Rural Code and Marine Fisheries.

**Sports equipment census statement**

The equestrian centre that owns sports equipment must make a declaration to identify the characteristics of sports equipment. It must be addressed within three months of the opening of the centre to the prefect of the department in which it is located.

This statement can be made directly online via a declaration form on the[Ministry of Sport website](http://www.res.sports.gouv.fr/Declaration_en_Ligne.aspx).

*For further information*: Article L. 312-2 of the Code of Sport.

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

He and advised to refer to the "Establishment receiving the public" sheet for more information.

**Compulsory insurance**

The professional who wishes to open an equestrian centre must take out liability insurance covering them and their employees against the risks associated with their professional activity.

If the duty is breached, the professional faces a six-month prison sentence and a fine of 7,500 euros.

In addition, the professional must inform the members of a sports association or federation of the interest of purchasing insurance in order to cover the risks associated with sports practice.

*For further information*: Articles L. 321-1 and L. 321-4 of the Code of Sport.

**Disclosure requirement**

The professional must display the following information within the equestrian centre:

- the diplomas or professional titles of all those practising in the centre, as well as their professional cards or trainee certificates;
- all the texts relating to hygiene and safety measures and the technical standards applicable to the supervision of physical and sports activities;
- The centre's insurance certificate
- emergency numbers.

*For further information*: Article R. 322-5 of the Code of Sport.

**Other provisions**

There are also many regulations specific to the possession of equines, including:

- the presence of an emergency kit deed to provide first aid and a means of communication to alert emergency services;
- hygiene and safety rules related to the use of equines;
- the obligation to call in a veterinarian
- competitions (inquire with the French Riding Federation ([Ffe](https://www.ffe.com/))) ;
- horseback riding along rivers, coastlines, forests or on public roads;
- transporting equines.

For more information it is advisable to check with the[IFCE](http://www.ifce.fr/).

3°. Installation procedures and formalities
------------------------------------------------------

### a. Statement by sports educator to obtain a professional card

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to carry out his activity as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the exercise department or the main place of practice, or directly online on the[official website of the Ministry of Sport](https://eaps.sports.gouv.fr).

**Timeframe**

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

**Supporting documents**

The application must include:

- a declaration form[Cerfa 12699*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_12699.do) ;
- Supporting documents:- A copy of a valid ID,
  - A photo ID,
  - A statement on the honour attesting to the accuracy of the information on the form,
  - A copy of each of the diplomas, titles or certificates invoked,
  - A copy of the authorization to exercise, the equivalency of a diploma, if any,
  - a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If the return is renewed, you should attach:

- Form Cerfa No. 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned dating back less than one year.

The prefecture will itself request the communication of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

**Cost**

Free

*For further information*: Articles L. 212-11, R. 212-85, and Articles A. 212-176 to A. 212-178 of the Code of Sport.

### b. Make a pre-declaration for the EU national for a temporary and casual exercise (LPS)

**Competent authority**

The national must apply to the prefect of the department where he wishes to carry out most of his activity.

**Supporting documents**

The national's request must include:

- the standard form attached to the[Appendix II-12-3](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000021257586&dateTexte=&categorieLien=cid) The Code of Sport completed and signed;
- the following supporting documents, if any, with their translation into French:- A photograph of the applicant's identity,
  - A copy of his ID,
  - A copy of his certificate of competency or his training certificate,
  - A copy of the documents justifying that the national is legally established in the Member State and that he is not prohibited from practising his or her activities,
  - a copy of any document justifying that he has been a sports educator for at least one year in the last ten years if neither activity nor training is regulated in the establishment Member State;
- a statement of honour attesting to the accuracy of the information provided.

**Timeframe**

Within one month of receiving the declaration file, the prefect notifies the claimant:

- a request for further information (in this case, the prefect has two months to give his answer);
- a service delivery receipt if it does not conduct a qualifications check. In this case, service delivery may begin;
- that it conducts the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test or an adjustment course.

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

*For further information*: Articles R. 212-92 to R. 212-93 and Section A. 212-182-2 of the Code of Sport.

### c. Make a pre-declaration for the EU national for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport and wishing to settle in France must make a declaration to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

**Supporting documents for the first declaration of activity**

The activity report file should include:

- completed and signed declaration form, the model of which is set at the[Appendix II-12-2-a](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006071318&idArticle=LEGIARTI000021257548&dateTexte=&categorieLien=cid) Code of Sport;
- the following supporting documents, if any, with their translation into French:- A photo ID,
  - A copy of a valid ID,
  - a medical certificate of non-contradiction to the practice and supervision of physical or sporting activities (less than one year old),
  - a copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out),
  - If so, a copy of any evidence justifying his professional experience,
  - If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalent in an EU or EEA state that regulates the activity,
  - documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport in the Member State of origin.

**Evidence for a renewal of activity declaration**

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities dating back less than one year.

**Delays and outcome of the procedure**

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated (see infra "Good to know: compensation measure").

**Please note**

The application must be renewed every five years.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*For further information*: Articles R. 212-88 to R. 212-91, Section A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

**Good to know: compensation measures**

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications placed with the Minister responsible for sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*For further information*: Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### d. Company reporting formalities

Depending on the nature of the business, the contractor must report his activity to the Chamber of Agriculture or register in the Register of Trade and Companies (RCS).

It is advisable to refer to the "Formalities of Reporting a Commercial Company" and "Registration of a Commercial Individual Company to the SCN" and to the[Chamber of Agriculture](http://www.chambres-agriculture.fr/) for more information.

### e. If necessary, register the company's statutes

The operator of an equestrian centre must, once the company's statutes have been dated and signed, register them with the corporate tax office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

