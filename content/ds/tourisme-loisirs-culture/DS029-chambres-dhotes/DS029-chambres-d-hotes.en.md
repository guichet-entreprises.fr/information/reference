﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS029" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Bed and breakfast" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="bed-and-breakfast" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/bed-and-breakfast.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="bed-and-breakfast" -->
<!-- var(translation)="Auto" -->




Bed and breakfast
===========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The activity of guest rooms consists, for the professional, to welcome tourists to his home for a fee, for one or more nights.

The provision of services includes the reception of tourists, in premises equipped with a bathroom and a toilet, the provision of linen as well as breakfast.

**Please note**

The professional cannot rent more than five rooms per dwelling, nor accommodate more than 15 people at the same time.

*For further information*: Articles L. 324-3 and D. 324-13 to D. 324-14 of the Tourism Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI);
- in the case of agricultural activity, it is the chamber of agriculture.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal, regardless of the number of employees of the company on the condition that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To carry out the activity of guest rooms, training must be followed in case of drinking flow (for cases where the guest room also makes guest table). It is necessary to make a declaration of the activity with the town hall of the municipality where the dwelling for rent is located.

**Please note**

The professional who runs a guesthouse establishment without reporting his activity is liable to a fine of 450 euros.

*For further information*: Articles L. 324-4 and R. 324-16 of the Tourism Code.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There is no provision for the national of a European Union (EU) state or a state party to the European Economic Area (EEA) agreement to operate as a temporary and casual guest room (LPS) or as a member of the European Economic Area (EEA) (LE).

As such, the professional is subject to the same requirements as the French national (see infra "3°. Installation procedures and formalities").

### c. Some peculiarities of the regulation of the activity

**Price advertising**

**Price display inside the property**

The professional is obliged to inform his clients of the price of the services offered within his establishment. This price must:

- Understand all the taxes you plan and the services that are inseparable from booking
- "Up-to-date rate."

In addition, the final price to be paid for all the services provided must be available at all times to the consumer and indicate to him:

- the service, or not, of breakfast;
- Whether or not you have access to an internet connection from the rooms
- information on all services available within the institution.

**Duty to display outside the property**

The professional is required to display in a clear and readable way, at the main entrance of his establishment and at the place of reception of his clientele:

- the price charged for the next night in a double room or the maximum price charged for a night in a double room for a certain period including the next night. However, if the establishment does not offer these services, the professional must display the price corresponding to the most practiced accommodation service as well as its duration;
- whether or not to access the internet from the rooms, whether or not to provide breakfast and, if so, whether these services are included in the price of the service or not;
- how to access information about the prices and services offered by the establishment.

In addition to the above information, the professional must, at the reception point of his clientele, indicate the possible arrival and departure times and, if necessary, the supplements in case of late departure.

**Requirement to display in each room**

In each room of his establishment, the professional must make available all the information relating to the price of the services provided as well as the procedures for consulting this information.

*For further information*: decree of 18 December 2015 relating to the advertising of the prices of tourist accommodations non-tourist tourist accommodations other than tourist accommodation and outdoor hotels.

**Billing**

The professional is subject to compliance with the billing obligations and must issue a note to the customer in case of a benefit of an amount greater than or equal to 25 euros. This note should include the following information:

- Its date of writing;
- The provider's name and address
- The customer's name (if the customer does not object to it);
- The date and place of execution of the service:
- the details of each benefit as well as the total amount payable (all taxes included and excluding taxes).

The professional must keep a double of this grade, rank them chronologically and keep them for two years.

*For further information*: ( Order 83-50/A of 3 October 1983 relating to the price advertising of all services.

**Individual font sheet**

As soon as the professional welcomes tourists of foreign nationality into his establishment, he must have them fill out and sign an individual police card whose model is set in annex to the decree of 1 October 2015 taken in according Article R. 611-42 of the Code of Entry and Residence of Foreigners and the Right of Asylum.

This fact sheet to prevent disturbances to public order and for judicial investigation and research purposes should mention:

- The identity of the foreign national;
- date, place of birth and nationality;
- The address of his usual home
- His phone number and email address
- The date of his arrival at the establishment and the expected date of his departure;
- As an option, children under the age of fifteen may be listed on an adult's card accompanying them.

This police record must be kept by the professional for six months and handed over, if requested, to the police and gendarmerie units.

*For further information*: Article R. 611-42 of the Code of Entry and Residence of Foreigners and the Right of Asylum.

**Compliance with hygiene, safety and safety requirements**

The professional is required to comply with all safety, hygiene and safety requirements. For more information, it is advisable to contact the Departmental Directorate of Population Protection ([DDPP](https://www.economie.gouv.fr/dgccrf/coordonnees-des-DDPP-et-DDCSPP)).

If the property has a swimming area or swimming pool, the professional will be required to comply with the health and safety rules against drowning.

It is advisable to refer to the listing[Pool operator - swimming spot](https://www.guichet-entreprises.fr/fr/activites-reglementees/tourisme-loisirs-culture/exploitant-piscine-lieu-de-baignade/) for more information.

*For further information*: Article D. 324-14 of the Tourism Code.

**Music broadcast**

The professional who wishes to broadcast music within his institution must apply online for prior authorization to the Society of Composers and Music Publishers (Sacem) and pay the amount of the royalty. The professional must register and complete the online authorisation form on the[Sacem website](https://clients.sacem.fr/autorisations).

*For further information*: Article D. 324-14 of the Tourism Code.

**Residence tax**

When the establishment of the professional is located in a municipality that has instituted a residence tax, he is obliged to collect this tax and to pay it to the local authority.

*For further information*: Articles L. 2333-26 and R. 2333-44 of the General Code of Local Government.

**Statement in case of food sale**

When the professional proposes the sale of food at his establishment, he is required to make a declaration to the DDPP of the department where his establishment is located.

In order to do so, it must address the[Form Cerfa 13984*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13984.do) completed and signed.

*For further information*: Article R. 233-4 of the Rural Code and Marine Fisheries.

**If necessary, obtain a liquor licence**

In the event of the sale of alcoholic beverages to be consumed on site, the professional is required to apply for a liquor licence.

Drinks are divided into four groups based on their alcohol content. The license issued varies depending on the group to which they belong.

It is advisable to refer to the listing[Drinking](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/), for more information.

In addition, if alcoholic beverages are sold only at mealtime, the professional is required to apply for either:

- a "small restaurant license" to offer on-site and during meals, third group drinks (wine, beer, cider, pear, mead, sweet wines, cream and fruit juice between 1.2 and 3 degrees of alcohol and liqueurs drawing 18 degrees maximum pure alcohol);
- a "restaurant license" to sell all permitted beverages for on-site consumption of meals.

In all cases, the professional is required to undergo specific training on the rights and obligations relating to the sale of alcoholic beverages adapted to the nature of his activity.

For more information, please refer to the listing[Traditional catering](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/restaurant-traditionnel/).

*For further information*: Articles L. 3321-1 and L. 3331-2 of the Public Health Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Statement of activity

**Competent authority**

The professional must send his declaration electronically, by recommended letter or in physical deposit, to the town hall of the municipality where the establishment concerned is located.

**Supporting documents**

The declaration must include the[Form Cerfa 13566*03](https://www.formulaires.modernisation.gouv.fr/gf/showFormulaireSignaletiqueConsulter.do?numCerfa=13566) completed and signed. His statement should mention:

- The identity of the professional
- The address of his home
- The number of rooms available for rent;
- The maximum number of people who can be accommodated
- forecast periods of lease.

**Procedure**

The town hall sends a receipt for filing the declaration to the professional, the model of which is set out in the appendix of the aforementioned form.

**Cost**

Free.

*For further information*: Article D. 324-15 of the Tourism Code.

### b. Company reporting formalities

Depending on the nature of the business, the contractor must report his activity to the Chamber of Agriculture or register in the Register of Trade and Companies (RCS).

It is advisable to refer to the "Formalities of Reporting a Commercial Company" and "Registration of a Commercial Individual Company to the SCN" and to the[Chamber of Agriculture](http://www.chambres-agriculture.fr/) for more information.

### c. If necessary, register the company's statutes

The professional operating a guesthouse establishment must, once the company's statutes have been dated and signed, register them with the corporate tax office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

