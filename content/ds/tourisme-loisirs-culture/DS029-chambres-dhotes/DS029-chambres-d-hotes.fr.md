﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS029" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Chambres d’hôtes" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="chambres-d-hotes" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/chambres-d-hotes.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="chambres-d-hotes" -->
<!-- var(translation)="None" -->

# Chambres d’hôtes

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'activité de chambres d'hôtes consiste, pour le professionnel, à accueillir des touristes chez lui à titre onéreux, pour une ou plusieurs nuitées.

La prestation de services comprend l'accueil des touristes, dans des locaux équipés d'une salle d'eau et d'un WC, la fourniture du linge de maison ainsi que le petit déjeuner.

**À noter**

Le professionnel ne peut louer plus de cinq chambres par habitation, ni accueillir plus de 15 personnes en même temps.

*Pour aller plus loin* : articles L. 324-3 et D. 324-13 à D. 324-14 du Code du tourisme.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

- pour une activité commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI) ;
- en cas d'activité agricole, il s'agit de la chambre d'agriculture.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale, quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité de chambres d'hôtes, une formation doit être suivie en cas de débit de boissons (pour les cas où la chambre d’hôte fait aussi table d’hôte). Il est nécessaire d'effectuer une déclaration de l'activité auprès de la mairie de la commune où se trouve l'habitation destinée à la location.

**À noter**

Le professionnel qui tient un établissement de chambres d'hôtes sans procéder à la déclaration de son activité encourt une amende de 450 euros.

*Pour aller plus loin* : articles L. 324-4 et R. 324-16 du Code du tourisme.

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services (LPS) ou Libre établissement (LE))

Aucune disposition n'est prévue pour le ressortissant d'un État de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer l'activité de chambres d'hôtes à titre temporaire et occasionnel (LPS) ou à titre permanent (LE).

À ce titre, le professionnel est soumis aux mêmes exigences que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation  »).

### c. Quelques particularités de la réglementation de l’activité

**Publicité des prix**

**Affichage du prix à l'intérieur de l'établissement**

Le professionnel est tenu d'informer ses clients du prix des prestations proposées au sein de son établissement. Ce prix affiché doit :

- comprendre l'ensemble des taxes prévues et les prestations indissociables de la réservation ;
- préciser la mention « Tarif à jour ».

En outre, le prix définitif à payer pour l'ensemble des prestations fournies doit être disponible à tout moment pour le consommateur et lui indiquer :

- le service, ou non, du petit déjeuner ;
- l'accès, ou non, à une connexion internet depuis les chambres ;
- le cas échéant, les informations relatives à toutes les prestations accessibles au sein de l'établissement.

**Obligation d'affichage à l'extérieur de l'établissement**

Le professionnel est tenu d'afficher de manière claire et lisible, à l'entrée principale de son établissement et au lieu de réception de sa clientèle :

- le prix pratiqué pour la prochaine nuitée en chambre double ou le prix maximum pratiqué pour une nuitée en chambre double pendant une période donnée incluant la prochaine nuitée. Toutefois si l'établissement ne propose pas ces prestations, le professionnel doit afficher le prix correspondant à la prestation d'hébergement la plus pratiquée ainsi que sa durée ;
- la possibilité d'accéder ou non à internet depuis les chambres, la fourniture ou non du petit déjeuner et, le cas échéant, si ces prestations sont comprises dans le prix de la prestation ou non ;
- les modalités d'accès aux informations concernant les prix et les prestations proposées par l'établissement.

En plus des informations susvisées, le professionnel doit, sur le lieu de réception de sa clientèle, indiquer les heures d'arrivée et de départ possibles et, le cas échéant, les suppléments en cas de départ tardif.

**Obligation d'affichage dans chaque chambre**

Au sein de chaque chambre de son établissement, le professionnel doit rendre accessible l'ensemble des informations relatives au prix des prestations fournies ainsi que les modalités de consultation de ces informations.

*Pour aller plus loin* : arrêté du 18 décembre 2015 relatif à la publicité des prix des hébergements touristiques marchands autres que les meublés de tourisme et les établissements hôteliers de plein air.

**Facturation**

Le professionnel est soumis au respect des obligations en matière de facturation et doit délivrer une note au client en cas de prestation d'un montant supérieur ou égal à 25 euros. Cette note doit comporter les informations suivantes :

- sa date de rédaction ;
- le nom et l'adresse du prestataire ;
- le nom du client (si celui-ci ne s'y oppose pas) ;
- la date et le lieu d'exécution de la prestation ;
- le détail de chaque prestation ainsi que le montant total à payer (toutes taxes comprises et hors taxes).

Le professionnel doit conserver un double de cette note, les classer chronologiquement et les conserver pendant deux ans.

*Pour aller plus loin* : arrêté n° 83-50/A du 3 octobre 1983 relatif à la publicité des prix de tous les services.

**Fiche individuelle de police**

Dès lors que le professionnel accueille au sein de son établissement des touristes de nationalité étrangère, il doit leur faire remplir et signer une fiche individuelle de police dont le modèle est fixé en annexe de l'arrêté du 1er octobre 2015 pris en application de l'article R. 611-42 du Code de l'entrée et du séjour des étrangers et du droit d'asile.

Cette fiche destinée à prévenir les troubles à l'ordre public et à des fins d'enquête judiciaire et de recherche doit mentionner :

- l'identité du ressortissant étranger ;
- sa date, son lieu de naissance et sa nationalité ;
- l'adresse de son domicile habituel ;
- son numéro de téléphone et son adresse électronique ;
- la date de son arrivée au sein de l'établissement et la date prévue de son départ ;
- à titre facultatif, les enfants de moins de quinze ans peuvent figurer sur la fiche d'un adulte qui les accompagne.

Cette fiche de police doit être conservée par le professionnel pendant six mois et être remis, en cas de demande, aux services de police et aux unités de gendarmerie.

*Pour aller plus loin* : article R. 611-42 du Code de l'entrée et du séjour des étrangers et du droit d'asile.

**Respect des exigences en matière d'hygiène, de sécurité et de salubrité**

Le professionnel est tenu de respecter l'ensemble des exigences en matière de sécurité, d'hygiène et de salubrité. Pour de plus amples informations, il est conseillé de contacter la direction départementale de la protection des populations ([DDPP](https://www.economie.gouv.fr/dgccrf/coordonnees-des-DDPP-et-DDCSPP)).

Si l'établissement comporte un lieu de baignade ou une piscine, le professionnel sera tenu au respect des règles sanitaires et de sécurité contre les noyades.

Il est conseillé de se reporter à la fiche « [Exploitant piscine - lieu de baignade](https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/exploitant-piscine-lieu-de-baignade.html) » pour de plus amples informations.

*Pour aller plus loin* : article D. 324-14 du Code du tourisme.

**Diffusion de musique**

Le professionnel qui souhaite diffuser de la musique au sein de son établissement, doit adresser en ligne, une demande d'autorisation préalable à la Société des auteurs compositeurs et éditeurs de musique (Sacem) et s'acquitter du montant de la redevance. Le professionnel doit s'inscrire et remplir le formulaire d'autorisation en ligne sur le [site de la Sacem](https://clients.sacem.fr/autorisations).

*Pour aller plus loin* : article D. 324-14 du Code du tourisme.

**Taxe de séjour**

Lorsque l'établissement du professionnel se situe dans une commune ayant institué une taxe de séjour, il est tenu de percevoir cette taxe et de la reverser à la collectivité territoriale.

*Pour aller plus loin* : articles L. 2333-26 et R. 2333-44 du Code général des collectivités territoriales.

**Déclaration en cas de vente de denrée alimentaires**

Lorsque le professionnel propose au sein de son établissement, la vente de denrées alimentaires, il est tenu d'effectuer une déclaration auprès de la DDPP du département où est situé son établissement.

Pour cela, il doit adresser le [formulaire Cerfa 13984](https://www.service-public.fr/professionnels-entreprises/vosdroits/R17520) complété et signé.

*Pour aller plus loin* : article R. 233-4 du Code rural et de la pêche maritime.

**Le cas échéant, obtenir une licence de débit de boissons**

En cas de vente de boissons alcoolisées à consommer sur place, le professionnel est tenu d'effectuer une demande de licence de débit de boissons.

Les boissons sont classées en quatre groupes selon leur teneur en alcool. La licence délivrée varie selon le groupe auquel elles appartiennent.

Il est conseillé de se reporter à la fiche [Débitant de boissons](https://www.guichet-entreprises.fr/fr/ds/alimentation/debitant-de-boissons.html), pour de plus amples informations.

En outre, en cas de vente de boissons alcoolisées uniquement au moment des repas, le professionnel est tenu d'effectuer une demande en vue d'obtenir soit :

- une « petite licence restaurant » permettant de proposer à la vente sur place et au cours des repas , des boissons du troisième groupe (vin, bière, cidre, poiré, hydromel, vins doux, crème et jus de fruits entre 1,2 et 3 degrés d'alcool et liqueurs tirant 18 degrés d'alcool pur maximum) ;
- une « licence restaurant » permettant de vendre pour consommer sur place à l'occasion des repas toutes les boissons autorisées.

Dans tous les cas, le professionnel est tenu de suivre une formation spécifique sur les droits et obligations relatifs à la vente de boissons alcoolisées adaptée à la nature de son activité.

Pour plus de renseignements, vous pouvez vous reporter à la fiche « [Restauration traditionnelle](https://www.guichet-entreprises.fr/fr/ds/alimentation/restauration-traditionnelle.html) ».

*Pour aller plus loin* : articles L. 3321-1 et L. 3331-2 du Code de la santé publique.

## 3°. Démarches et formalités d’installation

### a. Déclaration d'activité

**Autorité compétente**

Le professionnel doit adresser sa déclaration par voie électronique, par lettre recommandée ou en dépôt physique, à la mairie de la commune où se trouve l'établissement concerné.

**Pièces justificatives**

La déclaration doit comprendre le [formulaire Cerfa 13566](https://www.formulaires.service-public.fr/gf/showFormulaireSignaletiqueConsulter.do?numCerfaAndExtension=13566*03) complété et signé. Sa déclaration doit mentionner :

- l'identité du professionnel ;
- l'adresse de son domicile ;
- le nombre de chambres proposées à la location ;
- le nombre maximal de personnes pouvant être accueillies ;
- la ou les période(s) prévisionnelle(s) de location.

**Procédure**

La mairie adresse un récépissé de dépôt de la déclaration au professionnel dont le modèle est fixé en annexe du formulaire précité.

**Coût**

Gratuit.

*Pour aller plus loin* : article D. 324-15 du Code du tourisme.
 
### b. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit déclarer son activité auprès de la chambre de l'agriculture ou s’immatriculer au registre du commerce et des sociétés (RCS).

Il est conseillé de se reporter au site des [chambres d'agriculture](http://www.chambres-agriculture.fr/) pour de plus amples informations.

### c. Le cas échéant, enregistrer les statuts de la société

Le professionnel exploitant un établissement de chambres d'hôtes doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- la forme même de l'acte l'exige.

**Autorité compétente**

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

**Pièces justificatives**

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.