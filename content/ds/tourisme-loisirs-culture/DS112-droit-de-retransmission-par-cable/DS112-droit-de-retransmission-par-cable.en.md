<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS112" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Collection and distribution of royalties deriving from the broadcasting by cable, simultaneously and in full,throughout France of a work broadcast from an EU Member State" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="royalties-deriving-from-the-broadcasting-by-cable" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/droit-de-retransmission-par-cable-dune-oeuvre-telediffusee-a-partir-dun-autre-etat-membre.html" -->
<!-- var(last-update)="2021-01" -->
<!-- var(url-name)="royalties-deriving-from-the-broadcasting-by-cable-of-a-work-broadcast-from-EU-member-state" -->
<!-- var(translation)="None" -->

# Collection and distribution of royalties deriving from the broadcasting by cable, simultaneously and in full,throughout France of a work broadcast from an EU Member State

Latest update: <!-- begin-var(last-update) -->2021-01<!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

## 1 °. Definition of the activity

### at. Definition

Article 9 of Directive 93/83 of September 27, 1993 on the coordination of certain rules applicable to satellite broadcasting and cable retransmission creates an obligation of collective management for the right of simultaneous, complete and unchanged retransmission. by cable of a work broadcast from a Member State on national territory.

The activity of collective management of cable retransmission rights is subject to approval by the Ministry of Culture.

## 2 °. Installation conditions

To hold an authorization from the Ministry of Culture to exercise the activity of collective management of the right of simultaneous, complete and unchanged retransmission by cable of a work broadcast from a Member State on national territory , it is necessary to demonstrate (article L. 132-20-1 of the Intellectual Property Code):

- the professional qualification of the heads of the organizations. The concept of "professional qualification" should be understood in the sense of "professional competence": no requirement of level of study or particular qualification is required;
- the means that they can implement to ensure the recovery of rights and the exploitation of their repertoire;
- the importance of their repertoire.

## 3 °. Installation procedures and formalities

### at. Application for approval

#### Competent authority

The organization wishing to exercise the activity of collective management of the right of simultaneous, full and unchanged retransmission by cable of a work broadcast from a Member State on national territory must send its request for approval to the Ministry of Culture.

#### Vouchers

This request must be accompanied by the following elements (article R. 323-1 of the Intellectual Property Code):

- any proof of the effective management of the right to authorize retransmission by cable, at the rate of the number of rights holders and the economic importance expressed in income or in turnover;
- all documents to justify the qualification of its managers and corporate officers assessed in terms of:
  - either the nature and level of their diplomas,
  - or their experience in the management of professional organizations;
- all information relating to the administrative organization and the conditions of installation and equipment;
- all information relating to the collections received or expected during the cable retransmission, simultaneous, complete and without change, on the national territory, from a Member State of the European Union and to the data necessary for their distribution ;
- copy of the agreements made with third parties relating to cable retransmission, simultaneous, complete and without change, on national territory, from a Member State of the European Community;
- where applicable, a copy of the agreements made with foreign professional organizations responsible for collecting and distributing rights.

#### Procedure

The interested party must send his request for approval by registered letter with acknowledgment of receipt to the Minister responsible for culture, who issues a receipt.

The request can also be made [online](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/PROPR_DEMAN_agrement_04/?__CSRFTOKEN__=643c9f26-9b48-4aba-ab02-25e89d798e5c).

When the file is not complete, the Minister in charge of culture requests by registered letter with acknowledgment of receipt an additional file which must be submitted in the same form within one month of receipt of this letter.

Approval is issued by order of the Minister responsible for culture, published in the Official Journal of the French Republic. It is granted for five years and is renewable under the same conditions as the initial approval.

#### Changes in the situation

If the organization ceases to meet one of the conditions set out in Article R. 323-1 of the Intellectual Property Code, the administration sends it a formal notice by registered letter with acknowledgment of receipt. The beneficiary of the authorization has a period of one month to present his observations. In the absence of regularization of the situation, the approval may be withdrawn by order of the Minister responsible for culture, published in the Official Journal of the French Republic.

Any change of statute or general regulations, any termination of function of a member of the governing and deliberative bodies of an approved body shall be communicated to the Minister responsible for culture within fifteen days of the corresponding decision. Failure to declare may result in withdrawal of approval

## 4 °. Reference texts

- Intellectual Property Code, legislative part, articles L.132-20-1 and L. 217-2;
- Intellectual Property Code, regulatory part, articles R.323-1 to R. 323-5.