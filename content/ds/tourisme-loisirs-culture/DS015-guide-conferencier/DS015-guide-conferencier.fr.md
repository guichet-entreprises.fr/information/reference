﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS015" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Guide-conférencier" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="guide-conferencier" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/guide-conferencier.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="guide-conferencier" -->
<!-- var(translation)="None" -->

# Guide-conférencier

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le guide-conférencier est un professionnel dont la mission est d'assurer des visites guidées en français ou dans une langue étrangère, dans les musées de France et les monuments historiques. Son rôle est de valoriser le patrimoine en concevant des actions de médiation culturelle à destination des publics dans les territoires et lieux patrimoniaux.

Hormis l’application de la disposition législative de l’article L. 221-1 du Code du tourisme , les métiers de guide touristique, guide-accompagnateur ou accompagnateur de tourisme sont libres d’accès en France et peuvent s’exercer sans condition de diplôme ou de titre professionnel pour les ressortissants européens

*Pour aller plus loin* : article L. 221-1 du Code du tourisme.

### b. CFE compétent

Le centre des formalités de l’entreprise (CFE) compétent diffère de l’activité exercée par l’intéressé pour s’installer :

- si l'activité est exercée en entreprise individuelle : compétence de l'Urssaf ;
- si l’activité est exercée sous la forme d’une société commerciale : compétence de la chambre de commerce et d’industrie (CCI) ;
- si l’activité est exercée sous la forme d’une société civile ou d'une société d'exercice libéral : compétence du greffe du tribunal de commerce, ou greffe du tribunal d’instance dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité de guide-conférencier, l'intéressé doit justifier d'une carte professionnelle délivrée aux titulaires d'une certification inscrite au Répertoire national des certifications professionnelles (RNCP).

La carte est remise au titulaire :

- de la licence professionnelle de guide-conférencier ;
- d'un master comportant les unités d'enseignement suivantes :
  - compétences des guides-conférenciers,
  - mise en situation et pratique professionnelle,
  - langue vivante (autre que le français) ;

**À noter**

Ces unités d'enseignements doivent impérativement avoir été validées par l'intéressé. La preuve de leur validation peut prendre la forme d'une attestation délivrée par l'établissement d'enseignement ou d'une annexe descriptive jointe au diplôme.

- d'un master et justifiant :
  - d'au moins une année cumulée d'expérience professionnelle dans la médiation orale des patrimoines au cours des cinq dernières années,
  - d'un niveau C1 au cadre européen commun de référence pour les langues ([CECRL](https://www.coe.int/T/DG4/Linguistic/Source/Framework_FR.pdf)) dans une langue vivante étrangère, une langue régionale de France ou la langue des signes française.

*Pour aller plus loin* : article 1 et annexe de l'arrêté du 9 novembre 2011 relatif aux compétences requises en vue de la délivrance de la carte professionnelle de guide-conférencier aux titulaires de licence professionnelle ou de diplôme conférant le grade de master.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

**En cas de Libre Prestation de Services (LPS)**

Tout ressortissant d'un État membre de l'UE ou de l'EEE qui est établi et exerce légalement l'activité de guide-conférencier dans cet État peut exercer la même activité en France, de manière temporaire et occasionnelle.

Le ressortissant devra simplement indiquer la mention du titre professionnel ou du titre de formation qu'il détient dans cet État sur les documents qu'il présentera en France aux personnes liées à l'accueil touristique, ainsi qu'aux responsables de musée ou de monument historique visité.

Dans le cas où la profession n'est pas réglementée, soit dans le cadre de l'activité, soit dans le cadre de la formation, dans l'État dans lequel le professionnel est légalement établi, il devra avoir exercé cette activité pendant au moins un an au cours des dix années précédant la prestation dans un ou plusieurs États de l'UE ou de l'EEE.

*Pour aller plus loin* : articles L. 211-1, L. 221-3, L. 221-4 et R. 221-14 du Code du tourisme.

**En cas de Libre Établissement (LE)**

Le ressortissant d'un État de l'UE ou de l'EEE établi et exerçant légalement l'activité de guide-conférencier dans l'un de ces États peut exercer la même activité en France de manière permanente.

Il pourra demander la carte professionnelle auprès du préfet, dès lors qu'il justifie :

- être titulaire d'un diplôme, certificat ou autre titre permettant l'exercice de l'activité à titre professionnel dans un État de l'UE ou de l'EEE et délivré par :
  - l'autorité compétente de cet État,
  - un État tiers, fourni avec attestation d'un État de l'UE ou de l'EEE reconnaissant le titre et certifiant qu'il a exercé l'activité pendant trois années minimum ;
- être titulaire d'un titre de formation acquis dans son État d'origine, visant spécifiquement l'exercice de la profession ;
- de l'exercice de l'activité pendant au moins un an, à temps plein ou à temps partiel, au cours des dix dernières années dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de cette activité.

Lorsqu'il existe des différences substantielles entre la qualification professionnelle du ressortissant et la formation en France, le préfet compétent peut exiger que l'intéressé se soumette à une mesure de compensation.

*Pour aller plus loin* : articles L.221-2, R. 221-12 et R. 221-13 du Code du tourisme.

### c. Conditions d'honorabilité et incompatibilités

Toute personne exerçant l'activité de guide-conférencier sans être titulaire de la carte professionnelle encourt une amende de troisième classe allant jusqu'à 450 euros.

*Pour aller plus loin* : article R. 221-3 du Code du tourisme et article 131-13 du Code pénal.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Demander une carte professionnelle

L'intéressé souhaitant exercer la profession de guide-conférencier doit demander une carte professionnelle.

**Autorité compétente**

Le préfet du département dans lequel le professionnel souhaite s'établir est compétent pour délivrer la carte professionnelle de guide-conférencier. Le ressortissant d'un État de l'UE ou de l'EEE devra s'adresser au préfet de Paris pour en faire la demande.

**Pièces justificatives**

À l'appui de sa demande, le professionnel devra transmettre au préfet compétent un dossier comportant les pièces justificatives suivantes :

- le formulaire de demande de carte, complété et signé ;
- un document énonçant les mentions particulières, soit linguistiques, soit scientifiques et culturelles, devant figurer sur la carte ;
- une photocopie d'une pièce d'identité en cours de validité ;
- une ou plusieurs photos d'identité ;
- une copie du diplôme, certificat ou titre obtenu.

**Issue de la procédure**

Le préfet disposera d'un délai d'un mois à compter de la réception des éléments justificatifs pour en accuser réception ou demander l'envoi de pièces manquantes. La décision de délivrer la carte professionnelle interviendra ensuite dans un délai de deux mois.

Le silence gardé à l'expiration de ce délai vaudra décision d'octroi de la carte professionnelle.

**Voies de recours**

En cas de décision négative du préfet, l'intéressé, s'il estime que cette décision est contestable peut :

- soit former dans un délai de deux mois à compter de la réception de la notification de la décision un recours gracieux auprès du préfet ;
- soit former, dans les mêmes délais, un recours hiérarchique auprès du ministre chargé de l'économie et des finances ;
- soit former directement dans un délai de deux mois à compter de la réception de la notification de la décision du préfet un recours contentieux devant le tribunal administratif territorialement compétent.

**Coût**

Gratuit.