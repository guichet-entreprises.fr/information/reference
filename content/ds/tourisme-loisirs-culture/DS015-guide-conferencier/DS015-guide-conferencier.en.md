﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS015" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Tourist guide" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="tourist-guide" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/tourist-guide.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="tourist-guide" -->
<!-- var(translation)="Auto" -->




Tourist guide
=============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The guide-lecturer is a professional whose mission is to ensure guided tours in French or in a foreign language, in museums in France and in historical monuments. Its role is to enhance heritage by designing cultural mediation actions aimed at the public in heritage territories and places.

Apart from the application of the legislative provision of Article L. 221-1 of the Tourism Code, the trades of tour guide, guide-guide or tour operator are free to access in France and can be exercised without conditions of diploma or professional title for EU nationals

*For further information*: Article L. 221-1 of the Tourism Code.

### b. competent CFE

The relevant business formalities centre (CFE) differs from the activity carried out by the person concerned to set up shop:

- If the activity is carried out in an individual undertaking: the competence of the Urssaf;
- If the activity is carried out in the form of a trading company: jurisdiction of the Chamber of Commerce and Industry (CCI);
- if the activity is carried out in the form of a civil society or a liberal practising company: the jurisdiction of the registry of the commercial court, or the registry of the district court in the departments of the Lower Rhine, Upper Rhine and Moselle.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

In order to carry out the activity of guide-lecturer, the person concerned must justify a professional card issued to holders of a certification registered in the National Directory of Professional Certifications (RNCP).

The card is given to the holder:

- Professional license as a guide-lecturer;
- master's degree with the following teaching units:- skills of the guide-lecturers,
  - situation and professional practice,
  - living language (other than French);

**Please note**

These teaching units must have been validated by the person concerned. Proof of their validation may take the form of a certificate issued by the educational institution or a descriptive appendix attached to the diploma.

- master's degree and justifying:- at least one year of cumulative professional experience in oral inheritance mediation over the past five years,
  - level C1 to the common European framework of reference for languages ([CECRL](https://www.coe.int/T/DG4/Linguistic/Source/Framework_FR.pdf)) in a foreign living language, a regional language in France or French sign language.

*For further information*: Article 1 and appendix to the[ordered from November 9, 2011](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024813610) relating to the skills required to issue the professional guide-speaker card to holders of a professional license or diploma conferring the master's degree.

### b. Professional qualifications - EU or EEA nationals (free provision of services or Freedom of establishment)

**In case of free provision of services (LPS)**

Any national of an EU or EEA Member State who is established and legally practises the activity of guide-speaker in that state may carry out the same activity in France, on a temporary and occasional basis.

The national will simply have to indicate the mention of the professional title or the training title he holds in that state on the documents he will present in France to persons related to the tourist reception, as well as to museum or museum officials. historic monument visited.

In the event that the profession is not regulated, either in the course of the activity or in the context of training, in the state in which the professional is legally established, he must have carried out this activity for at least one year during the ten years before the benefit in one or more EU or EEA states.

*For further information*: Articles L. 211-1, L. 221-3, L. 221-4 and R. 221-14 of the Tourism Code.

**In case of Freedom of establishment**

A national of an EU or EEA state established and legally practising as a guide-speaker in one of these states may carry out the same activity in France on a permanent basis.

He will be able to apply for the professional card from the prefect, as long as he justifies:

- hold a diploma, certificate or other title allowing the exercise of the activity in a professional capacity in an EU or EEA state and issued by:- the competent authority of that state,
  - a third state, provided with certification from an EU or EEA state recognising the title and certifying that it has been in business for a minimum of three years;
- Hold a training degree acquired in your home state, specifically aimed at the practice of the profession;
- for at least one year, full-time or part-time, in the last ten years in an EU or EEA state that does not regulate access or the exercise of this activity.

Where there are substantial differences between the professional qualification of the national and the training in France, the competent prefect may require that the person concerned submit to a compensation measure.

*For further information*: Articles L.221-2, R. 221-12 and R. 221-13 of the Tourism Code.

### c. Conditions of honorability and incompatibility

Anyone who works as a guide-lecturer without holding a professional card is fined up to 450 euros.

*For further information*: Article R. 221-3 of the Tourism Code and Article 131-13 of the Penal Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the "Formality of Reporting of a Commercial Company," "Individual Business Registration in the Register of Trade and Companies" and "Corporate Reporting Formalities. artisanal work."

### b. Ask for a business card

The person concerned wishing to practise as a guide-speaker must apply for a professional card.

**Competent authority**

The prefect of the department in which the professional wishes to establish himself is competent to issue the professional guide-speaker card. The national of an EU or EEA state will have to contact the prefect of Paris to apply.

**Supporting documents**

In support of his application, the professional will have to submit to the appropriate prefect a file containing the following supporting documents:

- The card application form, completed and signed;
- A document setting out the specific mentions, either linguistic or scientific or cultural, to be included on the map;
- A photocopy of a valid ID
- One or more photo IDs
- a copy of the diploma, certificate or title obtained.

**Outcome of the procedure**

The prefect will have one month from the receipt of the supporting evidence to acknowledge it or request the sending of missing documents. The decision to issue the business card will then take place within two months.

The silence kept at the end of this period will be worth the decision to grant the professional card.

**Remedies**

In the event of a negative decision by the prefect, the person concerned, if he considers that this decision is questionable, may:

- or file a graceful appeal with the prefect within two months of receiving notification of the decision;
- or form, within the same time frame, a hierarchical appeal to the Minister responsible for the economy and finance;
- or form a legal challenge directly within two months of receiving notification of the prefect's decision before the territorially competent administrative court.

**Cost**

Free.

