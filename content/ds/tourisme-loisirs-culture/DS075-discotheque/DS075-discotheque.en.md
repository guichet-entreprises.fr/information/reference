﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS075" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Nightclub" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="nightclub" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/nightclub.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="nightclub" -->
<!-- var(translation)="Auto" -->


Nightclub
=====

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

A nightclub, more commonly known as a nightclub, is an establishment open to a major public, with a dance floor and a cab housing a disc jockey.

The operator of a nightclub must include:

- Coordinating teams
- Control the maintenance of equipment and facilities
- Conduct accounting and administrative management of the structure
- to deal with the communication of upcoming events at his institution.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

No specific diploma is required for the profession of nightclub operator. However, the individual is advised to have a solid knowledge of the operation of leisure structures, accounting and communication.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

A national of a Member State of the European Union (EU) or a party to the European Economic Area (EEA) is not subject to any qualification or certification requirements, as are the French.

### c. Honorability and incompatibility

The professional, who wishes to open a disco, is required to respect a number of rules including:

- prohibition on the sale of alcohol to persons under the age of 18. It is also forbidden to receive minors under the age of 16 if they are not accompanied by a parent, their legal guardian or a person over the age of 18 with his or her care or supervision;
- The[smoking ban](http://solidarites-sante.gouv.fr/prevention-en-sante/addictions/article/l-interdiction-de-fumer-dans-les-lieux-publics) in enclosed and covered areas.

*For further information*: Article L. 3342-3 of the Public Health Code.

### d. Some peculiarities of the regulation of the activity

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

As the premises are open to the public, the professional must comply with the rules relating to public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

#### Respect for operating schedules

The operator of a nightclub is required to close the premises at 7 a.m.

**Please note**

Exemptions may be granted by the municipality of these establishments.

*For further information*: Article D. 314-1 of the Tourism Code.

#### Availability of ethyllotests

The operator must make available to the public breathalysers that have been checked periodically. They should be marked with posters and accompanied by an explanatory note.

*For further information*: Article L. 3341-4 of the Public Health Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Requirement to hold a licence for the flow of beverages

The operator of a nightclub that sells alcoholic beverages is licensed.

**What to know**

Licensing is only possible if the operator:

- is an emancipated minor (or minor);
- is not under guardianship;
- was not sentenced for criminal offences, pimping, theft, fraud or breach of trust.

Prior to obtaining the licence, the operator must have a[operating permits](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14407.do) delivered after receiving specific training on the prevention and control of alcoholism, the protection of minors, drug legislation and noise control.

In parallel with the operator's application for an operating licence, the operator must submit a[pre-reporting](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_11542.do) at the town hall of the place of installation, which will later issue a receipt justifying the possession of the license.

### c. Post-installation authorisation

#### Obligation to enter into a performance contract with the Society of Composers and Music Publishers (SACEM)

As soon as a disco broadcasts music, its leader is obliged to apply for permission from SACEM, by signing a performance contract.

Prior to its conclusion, a first[Declaration](https://clients.sacem.fr/docs/autorisations/Diffusions_musicales_attractives.pdf), completed, dated and signed, must be forwarded to SACEM who will acknowledge this by returning the contract of representation to be signed.

Once SACEM has granted permission to broadcast, the operator of the disco will have to pay him a copyright based on the turnover he makes.

*For further information*: Articles L. 132-18 to L. 132-20-2 of the Intellectual Property Code.

#### Requirement to report its activity to the Corporation for the collection of fair compensation for the communication of commercial phonograms (SPRE) to the public

Unlike SACEM, which pays authors, composers and music publishers, the SPRE pays the broadcasting rights (called[Fair compensation](https://www.spre.fr/index.php?page_id=47)) to performers and phonogram producers.

The amount of fair compensation will take into account the turnover achieved.

The operator will be required to report his activity annually to the SPRE and send the following supporting documents:

- The[declaration slip](https://www.spre.fr/document/bordereau_declaration_annuelle.pdf) completed, dated and signed;
- A certified copy of the result account or the class 7 account list
- for micro-enterprises, a certified copy of the extract from Form 2042, under BIC.

**Please note**

The operator who does not accompany his declaration of the required proof would automatically trigger an invoice of 580 euros per month minimum.

*For further information*: Articles L. 214-1 and the following of the Intellectual Property Code.

#### Obligation to carry out a study of the impact of noise pollution

The operator of a disco must establish a study of the impact of noise disturbances that could affect the tranquillity or health of the neighbourhood.

It should include:

- acoustic diagnosis to estimate acoustic pressure levels inside and outside the premises;
- a description of the arrangements taken to limit the noise level (sound insulation work, installation of an acoustic pressure limiter, etc.).

In the event of significant disturbances related to the broadcasting of music, criminal penalties (5th class fine) may be applied to the operator.

**Please note**

This study should be updated with each change in venue layout or activity.

*For further information*: Articles R. 571-25 to R. 571-28 of the Environment Code.

