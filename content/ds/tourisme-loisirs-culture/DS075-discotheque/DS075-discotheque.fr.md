﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS075" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Discothèque" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="discotheque" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/discotheque.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="discotheque" -->
<!-- var(translation)="None" -->

# Discothèque

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Une discothèque, plus communément appelée boîte de nuit, est un établissement ouvert à un public majeur, dans lequel se situe une piste de danse et une cabine accueillant un disc-jockey.

L’exploitant d’une discothèque doit notamment :

- coordonner les équipes ;
- contrôler l’entretien des équipements et des installations ;
- effectuer la gestion comptable et administrative de la structure ;
- s’occuper de la communication des événements à venir dans son établissement.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucun diplôme spécifique n'est requis pour l'exercice de la profession d'exploitant de discothèque. Toutefois, il est conseillé à l'intéressé d'avoir de solides connaissances dans l’exploitation de structures de loisirs, en comptabilité et en communication.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Service ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'Espace économique européen (EEE) n'est soumis à aucune condition de diplôme ou de certification, au même titre que les Français.

### c. Honorabilité et incompatibilité

Le professionnel, qui souhaite ouvrir une discothèque, est tenu de respecter un certain nombre de règles dont :

- l'interdiction de vendre de l'alcool aux personnes de moins de 18 ans. Il est également interdit de recevoir des mineurs de moins de 16 ans s'ils ne sont pas accompagnés d'un de leur parent, de leur tuteur légal ou d'une personne de plus de 18 ans ayant sa charge ou sa surveillance ;
- l'[interdiction de fumer](http://solidarites-sante.gouv.fr/prevention-en-sante/addictions/article/l-interdiction-de-fumer-dans-les-lieux-publics) dans les lieux fermés et couverts.

*Pour aller plus loin* : article L. 3342-3 du Code de la santé publique.

### d. Quelques particularités de la réglementation de l'activité

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Les locaux étant ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

#### Respect des horaires d'exploitation

L'exploitant d'une discothèque est tenu de fermer les lieux à 7 heures du matin.

**À noter**

Des dérogations peuvent être accordées par la commune de ces établissements.

*Pour aller plus loin* : article D. 314-1 du Code du tourisme.

#### Mise à disposition d'éthylotests

L'exploitant doit mettre à disposition du public des éthylotests ayant fait l'objet de vérification périodique. Ils devront être signalés par des affichettes et accompagnés d'une notice explicative.

*Pour aller plus loin* : article L. 3341-4 du Code de la santé publique.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Obligation d'être titulaire d'une licence pour le débit de boisson

L'exploitant d'une discothèque qui vend des boissons alcoolisées est soumis à licence.

**À savoir**

La délivrance de la licence n'est possible que si l'exploitant :

- est majeur (ou mineur émancipé) ;
- n'est pas sous tutelle ;
- n'a pas été condamné à des peines pour infraction pénale, proxénétisme, vol, escroquerie ou abus de confiance.

Préalablement à l'obtention de la licence, l'exploitant doit être titulaire d'un [permis d'exploitation](https://www.service-public.fr/professionnels-entreprises/vosdroits/R22375) délivré après avoir suivi une formation spécifique portant sur la prévention et la lutte contre l'alcoolisme, la protection des mineurs, la législation des stupéfiants ou encore la lutte contre le bruit.

En parallèle de sa demande de permis d'exploitation, l'exploitant doit transmettre une [déclaration préalable](https://www.service-public.fr/professionnels-entreprises/vosdroits/R15902) à la mairie du lieu d’installation, qui lui délivrera par la suite un récépissé justifiant la possession de la licence.

### c. Autorisation post-installation

#### Obligation de conclure un contrat de représentation avec la Société des auteurs compositeurs et éditeurs de musiques (SACEM)

Dès lors qu'une discothèque diffuse de la musique, son dirigeant est tenu d'en demander l'autorisation auprès de la SACEM, en signant un contrat de représentation.

Préalablement à sa conclusion, une première [déclaration](https://clients.sacem.fr/docs/autorisations/Diffusions_musicales_attractives.pdf), remplie, datée et signée, doit être transmise à la SACEM qui en accusera réception en lui retournant le contrat de représentation à signer.

Une fois l'autorisation de diffuser accordée par la SACEM, l'exploitant de la discothèque devra lui payer des droits d'auteurs déterminés selon le chiffre d'affaires qu'il réalise.

*Pour aller plus loin* : articles L. 132-18 à L. 132-20-2 du Code de la propriété intellectuelle.

#### Obligation de déclarer son activité à la Société pour la perception de la rémunération équitable de la communication au public des phonogrammes du commerce (SPRE)

Contrairement à la SACEM qui rémunère les auteurs, compositeurs et éditeurs de musique, la SPRE verse les droits de diffusion (appelés [Rémunération équitable](https://www.spre.fr/index.php?page_id=47)) aux artistes interprètes et aux producteurs de phonogrammes.

Le montant de la rémunération équitable tiendra compte du chiffre d'affaires réalisé.

L'exploitant devra déclarer annuellement son activité auprès de la SPRE et envoyer les documents justificatifs suivants :

- le [bordereau de déclaration](https://www.spre.fr/document/bordereau_declaration_annuelle.pdf) rempli, daté et signé ;
- une copie certifiée du compte de résultat ou la liste des comptes de classe 7 ;
- pour les micro-entreprises, une copie certifiée de l'extrait du formulaire 2042, rubrique BIC.

**À noter**

L'exploitant qui n'accompagnerait pas sa déclaration des justificatifs requis verrait se déclencher automatique une facture de 580 euros par mois minimum.

*Pour aller plus loin* : articles L. 214-1 et suivants du Code de la propriété intellectuelle.

#### Obligation de réaliser une étude de l'impact des nuisances sonores

L'exploitant d'une discothèque doit établir une étude de l'impact des nuisances sonores qui pourraient porter atteinte à la tranquillité ou à la santé du voisinage.

Elle devra comporter :

- un diagnostic acoustique permettant d'estimer les niveaux de pression acoustique à l'intérieur et à l'extérieur des locaux ;
- une description des dispositions prises pour limiter le niveau sonore (travaux d'isolation phonique, installation d'un limiteur de pression acoustique, etc.).

En cas de nuisances importantes liées à la diffusion de musique, des sanctions pénales (amende de 5e classe) pourront être appliquées à l'exploitant.

**À noter**

Cette étude doit être mise à jour à chaque modification de l'aménagement des lieux ou de l'activité.

*Pour aller plus loin* : articles R. 571-25 à R. 571-28 du Code de l'environnement.