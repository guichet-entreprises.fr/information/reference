<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS110" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Droit de prêt en bibliothèque : activité de perception et de répartition" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="droit-de-pret-en-bibliotheque" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/droit-de-pret-en-bibliotheque-activite-de-perception-et-de-repartition.html" -->
<!-- var(last-update)="2021-01" -->
<!-- var(url-name)="droit-de-pret-en-bibliotheque-activite-de-perception-et-de-repartition" -->
<!-- var(translation)="None" -->

# Droit de prêt en bibliothèque : activité de perception et de répartition

Dernière mise à jour : <!-- begin-var(last-update) -->2021-01<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition 

Sur le fondement de l’article 6-1 de la directive 2006/115/CE du 12 décembre 2006, le Code de propriété intellectuelle prévoit que, lorsqu'une œuvre a fait l'objet d'un contrat d'édition en vue de sa publication et de sa diffusion sous forme de livre, l'auteur ne peut s'opposer au prêt d'exemplaires de cette édition par une bibliothèque accueillant du public. 

Ce prêt ouvre droit à rémunération au profit de l'auteur. Le Code de propriété intellectuelle prévoit que la perception de cette rémunération est assurée par un ou plusieurs organismes de gestion collective agréés à cet effet par le ministère de la Culture.

## 2°. Conditions d'installation 

Pour être titulaire d’un agrément du ministère de la Culture permettant de gérer la rémunération au titre du prêt en bibliothèque, il est nécessaire que l’organisme demandant l’agrément démontre (article L. 133-2 du Code de la propriété intellectuelle) :

- la diversité de ses associés au sein des instances dirigeantes et délibérante de l’organisme de gestion ;
- la qualification professionnelle des dirigeants. Il n’y a pas d’exigence de niveau d'étude ou encore de qualification particulière, cela est entendu au sens de « compétence professionnelle » ;
- les moyens que l’organisme propose de mettre en œuvre pour assurer la perception et la répartition de la rémunération au titre du prêt en bibliothèque ;
- la représentation équitable des auteurs et des éditeurs parmi ses associés et au sein de ses organes dirigeants.

## 3°. Démarches et formalités d'installation

### a. Demande d'agrément

#### Autorité compétente

L’organisme qui souhaite exercer l’activité de gestion collective des droits de prêt en bibliothèque doit adresser sa demande d'agrément au ministère de la Culture. 

#### Pièces justificatives

Cette demande doit être accompagnée des éléments suivants (article R. 325-1 du Code de la propriété intellectuelle) : 

- toute preuve permettant d’établir, par la composition de ses organes délibérants et dirigeants, la diversité de ses associés à raison des catégories et du nombre des ayants droit, de l'importance économique exprimée en revenu ou en chiffre d'affaires et de la diversité des genres éditoriaux ; 
- toute preuve permettant d’établir la représentation équitable des auteurs et des éditeurs parmi ses associés et au sein de ses organes dirigeants ; 
- tout pièce permettant de justifier de la qualification professionnelle de ses gérants et mandataires sociaux en raison de leur qualité d'auteur, de la nature et du niveau de leurs diplômes ou de leur expérience dans le secteur de l'édition ou de la gestion d'organismes professionnels ; 
- toutes informations nécessaires relatives à l'organisation administrative et aux conditions d'installation et d’équipement 
- toutes informations nécessaires relatives aux moyens mis en œuvre pour la collecte des données statistiques sur les acquisitions d'ouvrages par les bibliothèques ; 
- toutes informations nécessaires relatives aux moyens mis en œuvre pour la perception des rémunérations et le traitement des données nécessaires à la répartition de ces rémunérations ; 
- toutes informations nécessaires relatives au plan de financement et au budget prévisionnel des trois exercices suivant la demande d'agrément ; 
- toutes informations nécessaires relatives aux dispositions qu'elle a prises ou qu'elle entend prendre pour garantir le respect des règles de répartition des rémunérations entre les auteurs et les éditeurs, ainsi que le caractère équitable de la répartition au sein de chacune de ces catégories.

#### Procédure

L’intéressé doit adresser sa demande d'agrément par lettre recommandée avec avis de réception au ministre chargé de la culture qui en délivre récépissé. 

La demande peut également être effectuée [en ligne](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/PROPR_DEMAN_agrement_03/?__CSRFTOKEN__=643c9f26-9b48-4aba-ab02-25e89d798e5c).

Lorsque le dossier n'est pas complet, le ministre chargé de la culture demande par lettre recommandée avec avis de réception un dossier complémentaire qui doit être remis dans la même forme dans un délai d'un mois à compter de la réception de cette lettre.

L'agrément est délivré par arrêté du ministre chargé de la culture, publié au Journal officiel de la République française. Il est accordé pour cinq années et est renouvelable dans les mêmes conditions que l'agrément initial.

#### Changements de situation

Si l'organisme cesse de remplir l'une des conditions fixées à l'article R. 325-1 du Code de la propriété intellectuelle, l'administration lui adresse une mise en demeure par lettre recommandée avec avis de réception. Le bénéficiaire de l'agrément dispose d'un délai d'un mois pour présenter ses observations. Faute de régularisation de la situation, l'agrément peut être retiré par arrêté du ministre chargé de la culture, publié au Journal officiel de la République française.

Tout changement de statut ou de règlement général, toute cessation de fonction d'un membre des organes dirigeants et délibérants d'un organisme agréé sont communiqués au ministre chargé de la culture dans un délai de quinze jours à compter de la décision correspondante. Le défaut de déclaration peut entraîner retrait de l'agrément.

## 4°. Textes de référence

- Code de la propriété intellectuelle, partie législative, article L.133-2 ;
- Code de la propriété intellectuelle, partie réglementaire, articles R.325-1 et suivants.