﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS046" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Hotel" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="hotel" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/hotel.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="hotel" -->
<!-- var(translation)="Auto" -->


Hotel
=====

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

A hotel is a merchant tourist accommodation. It is a commercial establishment providing short-term accommodation (day, week or month) to a visiting clientele who do not choose to live there. The professional who operates a hotel is responsible for renting the rooms or furnished apartments of his establishment, and the issuance of all the services made available to his clientele. The rental is for a package and includes daily maintenance of the room and bed as well as the provision of towels. The property can also offer other services (laundry, catering, swimming pool, wellness, seminars, etc.). Each of these complementary activities is subject to its own regulations.

**Please note**

The hotel's activity can be carried out all year round or only on a seasonal basis.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. As the business is commercial in nature, the CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

If the operator chooses the status of limited liability contractor (EIRL), he makes a declaration of professional assets assigned to this activity with the CFE who will forward his file to the contents of the Register of Trade and Companies (RCS). If it operates in the form of a commercial company, it must be registered with the RCS after registering its statutes with the Corporate Tax Service (SIE) of the company's head office.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

No qualification requirement is required for the professional wishing to operate a hotel.

However, depending on the case, the professional will have to make a declaration in case of preparation or handling of animal or animal products (see infra "3°. d. Statement in case of preparation or sale of animal or animal products").

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There is no provision for the national of a Member State of the European Union (EU) or a State party to the Agreement on the European Economic Area, with a view to practising in France the activity of hotelier, on a temporary and casual basis (LPS) or (LE).

As such, the professional is subject to the same requirements as the French national (see infra "3°. Installation procedures and formalities").

### c. Responsibility

The operator of a hotel may be liable for theft or damage committed within his establishment. This responsibility includes theft and damage caused by all staff and third parties coming and going within the hotel.

*For further information*: Article L. 311-9 of the Tourism Code and Articles 1952 to 1954 of the Civil Code.

### d. Some peculiarities of the regulation of the activity

**Beverage flow provisions**

In the event of the sale of alcoholic beverages to be consumed on site, the professional is required to apply for a liquor licence.

Drinks are divided into four groups based on their alcohol content. The license issued varies depending on the group to which they belong.

It is advisable to refer to the listing[Drinking](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/), for more information.

In addition, if alcoholic beverages are sold only at mealtime, the professional is required to apply for either:

- a "small restaurant license" to offer on-site and during meals, third group drinks (wine, beer, cider, pear, mead, sweet wines, cream and fruit juice between 1.2 and 3 degrees of alcohol and liqueurs drawing 18 degrees maximum pure alcohol);
- a "restaurant license" to sell all permitted beverages for on-site consumption of meals.

In all cases, the professional is required to undergo specific training on the rights and obligations relating to the sale of alcoholic beverages adapted to the nature of his activity.

For more information, please refer to the listing[Traditional catering](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/restaurant-traditionnel/).

*For further information*: Articles L. 3321-1 and L. 3331-2 of the Public Health Code; Article L. 313-1 of the Tourism Code.

**Compliance with fire safety and accessibility standards**

As a hotel is a publicly-received establishment (ERP), the professional is required to ensure compliance with fire safety standards, as well as accessibility standards applicable to the category of Type O establishments according to the nomenclature of the Code of construction and housing (Article R. 123-48). For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: amended order of June 25, 1980 approving the general provisions of the Fire and Panic Safety Regulations in the ERPs. It is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) and the website of the Ministerial Accessibility Delegation for more information.

**Music broadcast**

The professional who wishes to broadcast music within his hotel must apply online for prior authorization to the Society of Composers and Music Publishers (Sacem) and pay the amount of the royalty. The professional must register and complete the online authorisation form on the[Sacem](https://clients.sacem.fr/autorisations), which is also responsible for collecting fair compensation for the[Spre](https://www.spre.fr/).

**Residence tax**

The residence tax or the flat-rate residence tax can be instituted by a municipality or a grouping of municipalities. Depending on the method of collection, the operation pays the real tax to the local authority from the customers or directly the amount of the flat-rate tax.

*For further information*: Articles L. 2333-26, R. 2333-43 and R. 2333-44 of the General Code of Local Government.

**Contribution to public broadcasting**

The professional is required to pay a fee as long as he makes available to his clients, a television receiver. This fee varies depending on the number of devices it has (as an indication, the price is 138 euros in the metropolis and 89 euros for the overseas departments).

**Please note**

A deduction is applied from three devices. In addition, if the hotel's annual activity does not exceed nine months, the professional can benefit from a 25% reduction in the contribution.

*For further information*: Section 1605 and the following of the General Tax Code.

### Price advertising

The professional is required to display in a clear and legible manner, outside the hotel and in the place of reception of his clientele:

- the price of all taxes and services included in euros for the next night in a double room or the maximum price charged for a night in a double room for a given period including the next night. However, if the establishment does not offer these services, the professional displays the price corresponding to the most practiced accommodation service as well as its duration;
- whether or not you can access the internet from the rooms, whether or not to provide breakfast, and if so, whether these services are included in the price of the service or not;
- how to access information about the prices and services offered by the establishment.

In addition to the above information, the professional indicates on the customer's reception location the possible arrival and departure times and, if necessary, the supplements in case of late departure.

**Requirement to display in each room**

In each room of his hotel, the professional makes available all the information relating to the price of the services provided incidentally to the nights or stays as well as the terms of consultation of this information.

*For further information*: decree of 18 December 2015 relating to the advertising of the prices of tourist accommodations non-tourist tourist accommodations other than tourist accommodation and outdoor hotels.

**Billing**

The professional issues a note to the client, in case of a benefit of an amount greater than or equal to 25 euros. This note should include the following information:

- Its date of writing;
- The provider's name and address
- The customer's name (if the customer does not object to it);
- The date and place of execution of the service
- the details of each benefit as well as the total amount payable (all taxes included and excluding taxes).

The professional keeps double this grade, ranks them chronologically and keeps them for two years.

*For further information*: ( Order 83-50/A of 3 October 1983 relating to the price advertising of all services.

**Individual font sheet**

As soon as the professional welcomes tourists of foreign nationality to his establishment, he has the tourist fill out and sign an individual police card whose model is set in annex to the decree of 1 October 2015 taken into application Article R. 611-42 of the Code of Entry and Residence of Foreigners and the Right of Asylum.

This fact sheet to prevent disturbances to public order and for forensic investigations and research should mention:

- The identity of the foreign national;
- date, place of birth and nationality;
- The address of his usual home
- His phone number and email address
- The date of his arrival at the establishment and the expected date of his departure;
- As an option, children under the age of fifteen may be listed on an adult's card accompanying them.

This police record is kept for six months and handed over, in case of request, to the police and gendarmerie units.

*For further information*: Article R. 611-42 of the Code of Entry and Residence of Foreigners and the Right of Asylum.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

**Competent authority**

The professional who operates a hotel maintains reports his business to the Chamber of Commerce and Industry (CCI).

**Supporting documents**

The person concerned provides the[supporting documents](http://media.afecreation.fr/file/74/2/pj-activites-commerciales-sans_ae_02.2014.68742.pdf) depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sends a receipt to the professional on the same day mentioning the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the CFE refuses to receive the file, the applicant has an appeal before the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

### b. Request for classification in tourist hotel

An operator may, regardless of the number of rooms, request the official classification in a tourist hotel to improve its offer.

**Competent authority**

The ranking is based on criteria corresponding to an increasing number of stars (1 to 5). It is set by a ranking table approved by the Minister responsible for tourism.

**Time and procedure**

The individual decision on classification is made, on the delegation of the State, by the tourism development agency[Trump France](http://www.atout-france.fr/), within a month of receiving the full file. The lack of response within this time frame is worth acceptance. The ranking is valid for five years.

To obtain this, the operator first contacts a compliance assessment agency accredited by the[French Accreditation Committee](https://www.cofrac.fr/) (Cofrac). A visit certificate is issued by the evaluator within a fortnight of the visit. It is only valid if it was carried out in the three months prior to the transmission of the complete file to Atout France.

The classification decision may be amended or repealed if, after an adversarial procedure, the operator has not corrected the compliance discrepancies found as a result of a customer's claim (Article D. 311-10 of the Tourism Code).

**Please note**

The professional whose establishment is classified is required to affix on the façade a sign in accordance with the provisions of the decree of February 19, 2010 relating to the sign of tourist hotels.

*For further information*: Article L. 311-6, and Articles D. 311-4 and the following of the Tourism Code; 23 December 2009 setting out the standards and procedure for classifying tourist hotels.

### c. Request for the "Palace" award

This device is intended to distinguish 5-star tourist hotel "with exceptional characteristics", including historical, aesthetic or heritage interest, and meeting special conditions (including eligibility bedroom surfaces and services).

**Procedure**

The award is carried out in two stages: a first stage of review of the compliance of the file with eligibility conditions by Atout France, and a second stage of analysis of the file by an attribution commission that issues a notice. This distinction, valid for five years, is awarded by the Minister responsible for tourism.

**Please note**

Failure to respond within four months is a refusal to award the award.

*For further information*: order of 3 October 2014 relating to the "Palace Distinction".

### d. Statement in case of preparation or sale of animal or animal products

As soon as the hotel prepares, processes, processes, handles or stores food, its owner is required to make a declaration to the prefect of the place where his establishment is located.

In order to do this, the professional will have to address the[Form Cerfa 13984*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13984.do) completed and signed to the Departmental Directorate of Population Protection (DDPP) of the department where its establishment is located.

*For further information*: Articles L. 233-2, R. 233-4 of the Rural Code and Marine Fisheries.

### e. If necessary, register the company's statutes

The professional operating a hotel must, once the company's statutes have been dated and signed, register them with the[Corporate Tax Department](http://www2.impots.gouv.fr/sie/ifu.htm) (SIE) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

