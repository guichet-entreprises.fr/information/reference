﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS046" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Hôtel" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="hotel" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/hotel.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="hotel" -->
<!-- var(translation)="None" -->

# Hôtel

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Un hôtel est un hébergement touristique marchand. C'est un établissement commercial fournissant une prestation d'hébergement de courte durée (à la journée, à la semaine ou encore au mois) à une clientèle de passage qui n'y élit pas domicile. Le professionnel qui exploite un hôtel est chargé d'assurer la location des chambres ou des appartements meublés de son établissement, et la délivrance de l'ensemble des prestations mises à disposition de sa clientèle. La location s'effectue au forfait et comprend l'entretien quotidien de la chambre et du lit ainsi que la fourniture du linge de toilette. L’établissement peut également offrir d’autres services (blanchisserie, restauration, piscine, bien-être, séminaires, etc.). Chacune de ces activités complémentaires est soumise à sa propre réglementation.

**À noter**

L'activité de l'hôtel peut être assurée toute l'année ou seulement de manière saisonnière.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée. L'activité étant de nature commerciale, le CFE est la chambre du commerce et de l'industrie (CCI).

**Bon à savoir**

Si l’exploitant choisit le statut d’entrepreneur à responsabilité limitée (EIRL), il effectue une déclaration de biens professionnels affectés à cette activité auprès du CFE qui transmettra son dossier au teneur du registre du commerce et des sociétés (RCS). S’il exploite sous la forme d’une société commerciale, il devra être immatriculé au RCS après avoir fait enregistrer ses statuts auprès du service des impôts des entreprises (SIE) du siège de la société.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Aucune exigence de qualification n'est requise pour le professionnel souhaitant exploiter un établissement hôtelier.

Toutefois, selon le cas, le professionnel devra effectuer une déclaration en cas de préparation ou de manipulation de denrée animale ou d'originale animale (cf. infra « 3°. d. Déclaration en cas de préparation ou de vente de denrées animales ou d'origine animale »).

### b. Qualifications professionnelles - Ressortissants européens (Libre prestation de services (LPS) ou Libre établissement (LE))

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'espace économique européen, en vue d'exercer en France, l'activité d'hôtelier, à titre temporaire et occasionnel (LPS) ou permanent (LE).

À ce titre, le professionnel est soumis aux mêmes exigences que le ressortissant français (cf. infra « 3°. Démarches et formalités d'installation »).

### c. Responsabilité

L'exploitant d'un hôtel est susceptible d'engager sa responsabilité en cas de vols ou dommages commis au sein de son établissement. Cette responsabilité s'entend du vol et des dommages causés par l'ensemble du personnel et des tiers allant et venant au sein de l'hôtel.

*Pour aller plus loin* : article L. 311-9 du Code du tourisme et articles 1952 à 1954 du Code civil.

### d. Quelques particularités de la réglementation de l’activité

#### Dispositions relatives au débit de boissons

En cas de vente de boissons alcoolisées à consommer sur place, le professionnel est tenu d'effectuer une demande de licence de débit de boissons.

Les boissons sont classées en quatre groupes selon leur teneur en alcool. La licence délivrée varie selon le groupe auquel elles appartiennent.

Il est conseillé de se reporter à la fiche « [Débitant de boissons](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) », pour de plus amples informations.

En outre, en cas de vente de boissons alcoolisées uniquement au moment des repas, le professionnel est tenu d'effectuer une demande en vue d'obtenir soit :

- une « petite licence restaurant » permettant de proposer à la vente sur place et au cours des repas , des boissons du troisième groupe (vin, bière, cidre, poiré, hydromel, vins doux, crème et jus de fruits entre 1,2 et 3 degrés d'alcool et liqueurs tirant 18 degrés d'alcool pur maximum) ;
- une « licence restaurant » permettant de vendre pour consommer sur place à l'occasion des repas toutes les boissons autorisées.

Dans tous les cas, le professionnel est tenu de suivre une formation spécifique sur les droits et obligations relatifs à la vente de boissons alcoolisées adaptée à la nature de son activité.

Pour plus de renseignements, vous pouvez vous reporter à la fiche « [Restauration traditionnelle](https://www.guichet-entreprises.fr/fr/ds/alimentation/restauration-traditionnelle.html) ».

*Pour aller plus loin* : articles L. 3321-1 et L. 3331-2 du Code de la santé publique ; article L. 313-1 du Code du tourisme.

#### Respect des normes de sécurité incendie et d'accessibilité

Un hôtel étant un établissement recevant du public (ERP), le professionnel est tenu de s'assurer du respect des normes de sécurité incendie, ainsi que des normes d'accessibilité applicables à la catégorie des établissements de type O selon la nomenclature du Code de la construction et de l'habitation (article R. 123-48). Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté modifié du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les ERP. Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » ainsi qu'au site de la délégation ministérielle à l'accessibilité pour de plus amples informations.

#### Diffusion de musique

Le professionnel qui souhaite diffuser de la musique au sein de son hôtel, doit adresser en ligne, une demande d'autorisation préalable à la Société des auteurs compositeurs et éditeurs de musique (Sacem) et s'acquitter du montant de la redevance. Le professionnel doit s'inscrire et remplir le formulaire d'autorisation en ligne sur le site de la [Sacem](https://clients.sacem.fr/autorisations), qui est également chargée de collecter la rémunération équitable de la [SPRE](https://www.spre.fr/).

#### Taxe de séjour

La taxe de séjour ou la taxe de séjour forfaitaire peut être instituée par une commune ou un groupement de communes. Selon le mode de perception, l’exploitation reverse à la collectivité territoriale la taxe perçue au réel auprès des clients ou directement le montant de la taxe au forfait.

*Pour aller plus loin* : articles L. 2333-26, R. 2333-43 et R. 2333-44 du Code général des collectivités territoriales.

#### Contribution à l'audiovisuel public

Le professionnel est tenu de verser une taxe dès lors qu'il met à disposition de ses clients, un récepteur de télévision. Cette redevance varie selon le nombre d'appareils dont il dispose (à titre indicatif, le tarif est de 138 euros en métropole et de 89 euros pour les départements d'outre-mer).

**À noter**

Un abattement est appliqué à partir de trois appareils. En outre, dès lors que l'activité annuelle de l'hôtel n'excède pas neuf mois, le professionnel peut bénéficier d'une minoration de 25 % sur la contribution.

*Pour aller plus loin* : article 1605 et suivants du Code général des impôts.

#### Publicité des prix

Le professionnel est tenu d'afficher de manière claire et lisible, à l'extérieur de l'hôtel et au lieu de réception de sa clientèle :

- le prix pratiqué toutes taxes et services compris en euros pour la prochaine nuitée en chambre double ou le prix maximum pratiqué pour une nuitée en chambre double pendant une période donnée incluant la prochaine nuitée. Toutefois si l'établissement ne propose pas ces prestations, le professionnel affiche le prix correspondant à la prestation d'hébergement la plus pratiquée ainsi que sa durée ;
- la possibilité d'accéder ou non à internet depuis les chambres, la fourniture ou non du petit déjeuner et le cas échéant, si ces prestations sont comprises dans le prix de la prestation ou non ;
- les modalités d'accès aux informations concernant les prix et les prestations proposées par l'établissement.

En plus des informations susvisées, le professionnel indique sur le lieu de réception de la clientèle les heures d'arrivée et de départ possibles et, le cas échéant, les suppléments en cas de départ tardif.

#### Obligation d'affichage dans chaque chambre

Dans chaque chambre de son hôtel, le professionnel rend accessible l'ensemble des informations relatives au prix des prestations fournies accessoirement aux nuitées ou séjours ainsi que les modalités de consultation de ces informations.

*Pour aller plus loin* : arrêté du 18 décembre 2015 relatif à la publicité des prix des hébergements touristiques marchands autres que les meublés de tourisme et les établissements hôteliers de plein air.

#### Facturation

Le professionnel délivre une note au client, en cas de prestation d'un montant supérieur ou égal à 25 euros. Cette note doit comporter les informations suivantes :

- sa date de rédaction ;
- le nom et l'adresse du prestataire ;
- le nom du client (si celui-ci ne s'y oppose pas) ;
- la date et le lieu d'exécution de la prestation ;
- le détail de chaque prestation ainsi que le montant total à payer (toutes taxes comprises et hors taxes).

Le professionnel conserve un double de cette note, les classer chronologiquement et les conserver pendant deux ans.

*Pour aller plus loin* : arrêté n° 83-50/A du 3 octobre 1983 relatif à la publicité des prix de tous les services.

#### Fiche individuelle de police

Dès lors que le professionnel accueille au sein de son établissement des touristes de nationalité étrangère, il fait remplir par le touriste et signer une fiche individuelle de police dont le modèle est fixé en annexe de l'arrêté du 1er octobre 2015 pris en application de l'article R. 611-42 du Code de l'entrée et du séjour des étrangers et du droit d'asile.

Cette fiche destinée à prévenir les troubles à l'ordre public et à des fins d'enquêtes judiciaires et de recherche doit mentionner :

- l'identité du ressortissant étranger ;
- sa date, son lieu de naissance et sa nationalité ;
- l'adresse de son domicile habituel ;
- son numéro de téléphone et son adresse électronique ;
- la date de son arrivée au sein de l'établissement et la date prévue de son départ ;
- à titre facultatif, les enfants de moins de quinze ans peuvent figurer sur la fiche d'un adulte qui les accompagne.

Cette fiche de police est conservée pendant six mois et remise, en cas de demande, aux services de police et aux unités de gendarmerie.

*Pour aller plus loin* : article R. 611-42 du Code de l'entrée et du séjour des étrangers et du droit d'asile.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

#### Autorité compétente

Le professionnel qui exploite un établissement hôtelier procède à la déclaration de son entreprise auprès de la chambre de commerce et d'industrie (CCI).

#### Pièces justificatives

L'intéressé fournit les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI adresse le jour même, un récépissé au professionnel mentionnant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le CFE refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### b. Demande de classement en hôtel de tourisme

Un exploitant peut, quel que soit le nombre de ses chambres, demander le classement officiel en hôtel de tourisme pour améliorer son offre.

#### Autorité compétente

Le classement s’effectue en fonction de critères correspondant à un nombre croissant d’étoiles (1 à 5). Il est fixé par un tableau de classement homologué par le ministre chargé du tourisme.

#### Délai et procédure

La décision individuelle de classement est prononcée, sur délégation de l’État, par l’agence de développement touristique [Atout France](http://www.atout-france.fr/), dans le mois qui suit la réception du dossier complet. L’absence de réponse dans ce délai vaut acceptation. Le classement est valable pendant cinq ans.

Pour l’obtenir, l’exploitant s’adresse au préalable à un organisme évaluateur de la conformité accrédité par le [Comité français d'accréditation](https://www.cofrac.fr/) (Cofrac). Un certificat de visite est remis par l’organisme évaluateur dans les quinze jours de la visite. Elle n’est valable que si elle a été réalisée dans les trois mois précédant la transmission du dossier complet à Atout France.

La décision de classement peut être modifiée ou abrogée si, après une procédure contradictoire, l’exploitant n’a pas rectifié les écarts de conformité constatés à la suite de la réclamation d’un client (article D. 311-10 du Code du tourisme).

**À noter**

Le professionnel dont l'établissement est classé est tenu d'apposer sur la façade un panonceau conformément aux dispositions de l'arrêté du 19 février 2010 relatif au panonceau des hôtels de tourisme.

*Pour aller plus loin* : article L. 311-6, et articles D. 311-4 et suivants du Code du tourisme ; arrêté modifié du 23 décembre 2009 fixant les normes et la procédure de classement des hôtels de tourisme.

### c. Demande en vue d'obtenir la distinction « Palace »

Ce dispositif est destiné à distinguer hôtel de tourisme classé 5 étoiles « présentant des caractéristiques exceptionnelles », et notamment un intérêt historique, esthétique ou patrimonial, et répondant à des conditions particulières (notamment d’éligibilité, de surfaces des chambres et de services).

#### Procédure

L'attribution s'effectue en deux temps : une première étape d’examen de la conformité du dossier aux conditions d'éligibilité par Atout France, et une seconde étape d’analyse du dossier par une commission d'attribution qui émet un avis . Cette distinction, valable cinq ans, est attribuée par le ministre chargé du tourisme.

**À noter**

L'absence de réponse dans un délai de quatre mois vaut refus de l'octroi de la distinction.

*Pour aller plus loin* : arrêté du 3 octobre 2014 relatif à la « distinction Palace ».

### d. Déclaration en cas de préparation ou de vente de denrées animales ou d'origine animale

Dès lors que l'hôtel prépare, traite, transforme, manipule ou entrepose des denrées alimentaires, son propriétaire est tenu d'effectuer une déclaration auprès du préfet du lieu où se situe son établissement.

Pour cela, le professionnel devra adresser le [formulaire Cerfa 13984](https://www.service-public.fr/professionnels-entreprises/vosdroits/R17520) complété et signé à la direction départementale de la protection des populations (DDPP) du département où est situé son établissement.

*Pour aller plus loin* : articles L. 233-2, R. 233-4 du Code rural et de la pêche maritime.

### e. Le cas échéant, enregistrer les statuts de la société

Le professionnel exploitant un hôtel doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.