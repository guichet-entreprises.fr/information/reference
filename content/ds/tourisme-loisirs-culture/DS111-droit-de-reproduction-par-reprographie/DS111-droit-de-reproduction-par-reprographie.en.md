<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS111" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Collection and distribution of reprographic reproduction rights" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="collection-and-distribution-of-reprographic-reproduction-rights" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/collection-and-distribution-of-reprographic-reproduction-rights.html" -->
<!-- var(last-update)="2021-01" -->
<!-- var(url-name)="collection-and-distribution-of-reprographic-reproduction-rights" -->
<!-- var(translation)="None" -->

# Collection and distribution of reprographic reproduction rights

Latest update: <!-- begin-var(last-update) -->2021-01<!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

## 1°. Definition of the activity

### a. Definition

The Intellectual Property Code provides that the publication of a work entails the transfer of the reproduction right by reprography to a collective management organization approved by the Ministry of Culture.

These organizations alone are capable of concluding any agreement with users for the purposes of managing reproduction rights by reprography, subject to the stipulations authorizing copies for purposes of sale, rental, advertising or promotion, of the agreement of the author or his successors in title.

In the absence of designation by the author or his successor in title on the date of publication of the work, one of the approved organizations is deemed to be the assignee of this right.

## 2°. Installation conditions

To hold an authorization from the Ministry of Culture allowing the activity of collective management of reproduction rights by reprography, it is necessary for the organization requesting the authorization to demonstrate (article L. 122-10 of the Code of intellectual property):

- the diversity of its associates. The purpose of this rule of constitution and operation of the company is to establish a balanced representation of the beneficiaries within the deliberative and executive bodies of the management body;
- the professional qualification of managers. There is no requirement for a level of study or even a specific qualification, this is understood in the sense of "professional competence";
- the means that the organization proposes to implement to ensure the management of the reproduction right by reprography;
- the equitable nature of the methods for distributing royalties between authors and publishers.

## 3°. Installation procedures and formalities

### a. Application for approval

#### Competent authority

The organization wishing to exercise the activity of collective management of reproduction rights by reprography must submit its request for approval to the Ministry of Culture.

#### Vouchers

This request must be accompanied by the following elements (article R. 322-1 of the Intellectual Property Code):

- any proof allowing to establish, by the composition of its deliberative and executive bodies, the diversity of its associates by reason of the categories and the number of the beneficiaries, of the economic importance expressed in income or in turnover and of the diversity of editorial genres;
- any document making it possible to justify the professional qualification of its managers and corporate officers by reason of their authorship, the nature and level of their diplomas or their experience in the publishing or management sector. professional organizations;
- all necessary information relating to the administrative organization and the conditions of installation and equipment. This information must relate to the collection of data on the practice of reprography, the collection of remuneration, the processing of the data necessary for the distribution of the remuneration received, the financing plan and the estimated budget for the three years following the request for authorization;
- proof that it is provided in its statutes, its general regulations and the standard acts of engagement of each member, of the rules guaranteeing the fairness of the modalities provided for the distribution of the remuneration received by the authors and the publishers.

#### Procedure

The interested party must send his request for approval by registered letter with acknowledgment of receipt to the Minister responsible for culture, who issues a receipt.

The request can also be made [online](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/PROPR_DEMAN_agrement_06/?__CSRFTOKEN__=643c9f26-9b48-4aba-ab02-25e89d798e5c).

When the file is not complete, the Minister in charge of culture requests by registered letter with acknowledgment of receipt an additional file which must be submitted in the same form within one month of receipt of this letter.

Approval is issued by order of the Minister responsible for culture, published in the Official Journal of the French Republic. It is granted for five years and is renewable under the same conditions as the initial approval.

#### Changes in the situation

If the organization ceases to meet one of the conditions set out in Article R. 322-1 of the Intellectual Property Code, the administration sends it a formal notice. The beneficiary of the authorization has a period of one month to present his observations. In the absence of regularization of the situation, the approval may be withdrawn by order of the Minister responsible for culture, published in the Official Journal of the French Republic.

Any change of statute or general regulations, any termination of function of a member of the governing and deliberative bodies of an approved body shall be communicated to the Minister responsible for culture within fifteen days of the corresponding decision. Failure to declare may result in withdrawal of approval

## 4 °. Reference texts

- Intellectual property code, legislative part, articles L.122-10 and L. 122-12;
- Intellectual property code, regulatory part, articles R. 322-1 et seq.