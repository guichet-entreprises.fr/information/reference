<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS111" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Droit de reproduction par reprographie : activité de perception et de répartition" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="droit-de-reproduction-par-reprographie" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/droit-de-reproduction-par-reprographie-activite-de-perception-et-de-repartition.html" -->
<!-- var(last-update)="2021-01" -->
<!-- var(url-name)="droit-de-reproduction-par-reprographie-activite-de-perception-et-de-repartition" -->
<!-- var(translation)="None" -->

# Droit de reproduction par reprographie : activité de perception et de répartition

Dernière mise à jour : <!-- begin-var(last-update) -->2021-01<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition 

Le Code de propriété intellectuelle prévoit que la publication d'une œuvre entraîne la cession du droit de reproduction par reprographie à un organisme de gestion collective agréé par le ministère de la Culture.

Ces organismes sont seuls en capacité de conclure toute convention avec les utilisateurs aux fins de gestion des droits de reproduction par reprographie, sous réserve, pour les stipulations autorisant les copies aux fins de vente, de location, de publicité ou de promotion, de l'accord de l'auteur ou de ses ayants droit.

À défaut de désignation par l'auteur ou son ayant droit à la date de la publication de l'œuvre, un des organismes agréés est réputé cessionnaire de ce droit.

## 2°. Conditions d'installation 

Pour être titulaire d’un agrément du ministère de la Culture permettant d’exercer l’activité de gestion collective des droits de reproduction par reprographie, il est nécessaire que l’organisme demandant l’agrément démontre (article L. 122-10 du Code de la propriété intellectuelle) :

- la diversité de ses associés. Cette règle de constitution et de fonctionnement de la société a pour objectif d'instituer une représentation équilibrée des ayants droit au sein des instances délibérantes et dirigeantes de l’organisme de gestion ;
- la qualification professionnelle des dirigeants. Il n’y a pas d’exigence de niveau d'étude ou encore de qualification particulière, cela est entendu au sens de « compétence professionnelle » ;
- les moyens que l’organisme propose de mettre en œuvre pour assurer la gestion du droit de reproduction par reprographie ;
- le caractère équitable des modalités de répartition des redevances entre auteurs et éditeurs.

## 3°. Démarches et formalités d'installation 

### a. Demande d'agrément

#### Autorité compétente

L’organisme qui souhaite exercer l’activité de gestion collective des droits de reproduction par reprographie doit adresser sa demande d'agrément au ministère de la Culture. 

#### Pièces justificatives

Cette demande doit être accompagnée des éléments suivants (article R. 322-1 du Code de la propriété intellectuelle) : 

- toute preuve permettant d’établir, par la composition de ses organes délibérants et dirigeants, la diversité de ses associés à raison des catégories et du nombre des ayants droit, de l'importance économique exprimée en revenu ou en chiffre d'affaires et de la diversité des genres éditoriaux ;
- tout pièce permettant de justifier de la qualification professionnelle de ses gérants et mandataires sociaux en raison de leur qualité d'auteur, de la nature et du niveau de leurs diplômes ou de leur expérience dans le secteur de l'édition ou de la gestion d'organismes professionnels ;
- toutes informations nécessaires relatives à l'organisation administrative et aux conditions d'installation et d’équipement. Ces informations doivent concerner la collecte des données sur la pratique de la reprographie, la perception des rémunérations, le traitement des données nécessaires pour la répartition des rémunérations perçues, le plan de financement et le budget prévisionnel des trois exercices suivant la demande d'agrément ;
- la preuve qu’il est prévu dans ses statuts, son règlement général et les actes types d'engagement de chacun des membres, des règles garantissant le caractère équitable des modalités prévues pour la répartition des rémunérations perçues par les auteurs et les éditeurs.

#### Procédure

L’intéressé doit adresser sa demande d'agrément par lettre recommandée avec avis de réception au ministre chargé de la culture qui en délivre récépissé. 

La demande peut également être effectuée [en ligne](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/PROPR_DEMAN_agrement_06/?__CSRFTOKEN__=643c9f26-9b48-4aba-ab02-25e89d798e5c).

Lorsque le dossier n'est pas complet, le ministre chargé de la culture demande par lettre recommandée avec avis de réception un dossier complémentaire qui doit être remis dans la même forme dans un délai d'un mois à compter de la réception de cette lettre.

L'agrément est délivré par arrêté du ministre chargé de la culture, publié au Journal officiel de la République française. Il est accordé pour cinq années et est renouvelable dans les mêmes conditions que l'agrément initial.

#### Changements de situation

Si l'organisme cesse de remplir l'une des conditions fixées à l'article R. 322-1 du Code de la propriété intellectuelle, l'administration lui adresse une mise en demeure. Le bénéficiaire de l'agrément dispose d'un délai d'un mois pour présenter ses observations. Faute de régularisation de la situation, l'agrément peut être retiré par arrêté du ministre chargé de la culture, publié au Journal officiel de la République française.

Tout changement de statut ou de règlement général, toute cessation de fonction d'un membre des organes dirigeants et délibérants d'un organisme agréé sont communiqués au ministre chargé de la culture dans un délai de quinze jours à compter de la décision correspondante. Le défaut de déclaration peut entraîner retrait de l'agrément

## 4°. Textes de référence 

- Code de la propriété intellectuelle, partie législative, articles L.122-10 et L. 122-12 ;
- Code de la propriété intellectuelle, partie réglementaire, articles R. 322-1 et suivants.