﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS083" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Alpine montain guide" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="alpine-mountain-guide" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/alpine-mountain-guide.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="alpine-mountain-guide" -->
<!-- var(translation)="Auto" -->


Alpine montain guide
===================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The mountain guide is a professional who conducts and educates and educates all types of mountaineering and related activities in safety and autonomy. He can practice as an accompanist, teacher and coach for an educational purpose, leisure or sports performance. He may also act as a technical advisor and consultant in mountain activities or perform mountain rescue duties.

The mountain guide performs the following activities:

- driving and accompanying people on excursions or ascents from mountain to rock, snow, ice and mixed terrain;
- driving and accompanying people on ski touring, ski mountaineering and off-piste skiing trips;
- teaching mountaineering, climbing and ski touring, ski mountaineering and off-piste skiing;
- training in competition practices in the aforementioned disciplines.

The job of mountain guide can be practiced full-time or as a seasonal occupation.

*To go further* Article 1 and Appendix V of the decree of 16 June 2014 relating to the specific training of the State Diploma of Mountaineering - High Mountain Guide.

### b. Competent Business Formalities Centre (CFE)

The relevant business formalities centre (CFE) depends on the nature of the activity and the legal form of the business:

- for the liberal professions, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

For more information, it is advisable to visit the websites[Urssaf](https://www.cfe.urssaf.fr/saisiepl/),[ICC In Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites),[Strasbourg ICC](http://strasbourg.cci.fr/cfe/competences) And[Public Service](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The person who wishes to practice the profession of mountain guide must have the status of sports educator and hold the state diploma of mountaineering - mountain guide.

The National Ski and Mountaineering School (Ensa) provides training for this diploma. For more information, it is advisable to consult[Ensa's website](http://www.Ensa.sports.gouv.fr/index.php?option=com_content&view=article&id=516&Itemid=772).

*To go further* Articles L. 212-1 and D. 212-68 and the following articles of the Code of Sport; decree of 16 June 2014 relating to the specific training of the State Diploma of Mountaineering - a mountain guide.

### b. Professional Qualifications - European Nationals (LPS or LE)

#### For the Free Service

Nationals of the European Union (EU) or the European Economic Area (EEA) legally established in one of these states may carry out the same activity in France on a temporary and occasional basis provided they have referred to the prefect of the department Isère a prior declaration of activity.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity there for at least the equivalent of a full-time year in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and their ability to alert emergency services.

*To go further* Articles L. 212-7, R. 212-92 to R. 212-94 and A. 212-221 of the Code of Sport.

**Good to know: the substantial difference**

For the supervision of mountaineering by a mountain guide, the substantial difference, likely to exist between the professional qualification of the registrant and the professional qualification required on the national territory, is appreciated in reference to the formation of the mountain guide diploma of the state mountaineering certificate for:

- Theoretical and practical safety knowledge
- technical security skills.

Under the SPA, where the prefect believes that there is a substantial difference, he or she may decide to subject the declarant to all or part of the aptitude test or an adjustment course. This decision is taken after the advice of the permanent mountaineering section of the Training and Employment Committee of the Higher Council of Mountain Sports forwarded to the National Pole of Ski And Mountaineering Trades.

*To go further* Articles A. 212-222, A. 212-224 and A. 212-228 of the Code of Sport.

#### For the Freedom of establishment

A national of an EU or EEA state may settle in France to practice permanently if he fulfils one of the following four conditions.

##### If the State of origin regulates access or the exercise of the activity:

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state and which certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

##### If the Member State of origin does not regulate access or the exercise of the activity:

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate competency or training certificate issued by the competent authority of one of these States, which attests to a readiness to carry out the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualification of the national differs substantially from the qualification required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to a aptitude test or performing an adjustment course (see infra. "compensation measures").

On the other hand, if the prefect believes that there is no substantial difference or where a substantial difference has been identified and the registrant has met the aptitude test, the prefect issues a certificate of freeness to the declarant. establishment and a professional sports educator card.

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safe exercise of physical and sports activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

**Good to know: the substantial difference**

For the supervision of mountaineering by a mountain guide, the substantial difference, likely to exist between the professional qualification of the registrant and the professional qualification required on the national territory, is appreciated in reference to the formation of the mountain guide diploma of the State Mountaineering Certificate as it incorporates:

- Theoretical and practical safety knowledge
- technical security skills.

In the context of freedom of establishment, where the prefect considers that there is a substantial difference, he refers the matter to the Qualifications Recognition Commission, attaching the opinion of the permanent section to the file. This decision is taken after the opinion of the permanent mountaineering section of the Training and Employment Committee of the Higher Council of Mountain Sports, forwarded to the National Trades Centre for Ski and Mountaineering,

After ruling on the existence of a substantial difference, the Qualifications Recognition Commission proposes, if necessary, to the prefect to submit the declarant to an aptitude test or an adjustment course.

When the registrant has passed the aptitude test, the prefect issues him a certificate of Freedom of establishment and a professional card of a sports educator.

*To go further* Articles A. 212-222, A. 212-223 and A. 212-228, R. 212-88 to R. 212-91 of the Code of Sport.

### c. Conditions of honorability and incompatibility

It is forbidden to practice as a mountain guide in France for persons who have been convicted of any crime or for one of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substances or plants classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

### d. Some of the peculiarities of regulation

#### The retraining course

Holders of the State Diploma of Mountaineering - Mountain Guide are subjected every six years to a refresher course in order to update their professional skills. Recycling is a condition of the practice of the profession.

It is organized by the Ensa.

*To go further* : decree of 11 March 2015 relating to the content and organisation of the recycling of holders of mountain guide diplomas.

#### The specific environment

The practice of a mountain guide is an activity that takes place in a specific environment. It involves compliance with special security measures. Therefore, only organizations under the tutelage of sports can train future professionals.

*To go further* Articles L. 212-2 and R. 212-7 of the Code of Sport.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the activity sheets "Formality of Reporting a Commercial Company" or "Registration of an Individual Company in the Register of Trade and Companies."

### b. Reporting requirement (for the purpose of obtaining the professional sports educator card)

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Timeframe**

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

**Supporting documents**

The statement should include:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

**Cost**

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 and following of the Code of Sport.

### c. Make a prior declaration of activity for EU nationals engaged in one-off activity (Freedom to provide services) 

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

**Competent authority**

The prior declaration of activity must be addressed to the prefect of the Department of Isère.

**Timeframe**

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "3 degrees). e. CompEnsation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

**Supporting documents**

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and subsequent articles A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### d. Make a prior declaration of activity for EU nationals engaged in permanent activity (Freedom of establishment)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a prior declaration of activity.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. It may also decide to subject the national to an aptitude test or an adjustment course (see below: "3 e. CompEnsation Measures").

**Competent authority**

The prior declaration of activity must be addressed to the prefect of the Department of Isère.

**Timeframe**

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a measure of compEnsation (fitness test or internship), his decision must be motivated.

**Supporting documents for the first declaration of activity**

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or the training document, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

**Evidence for a renewal of activity declaration**

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

#### e. CompEnsation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission refers the permanent section of alpine skiing to the Training and Employment Committee of the Higher Council of Mountain Sports after review and investigation of the case. In the month of her referral, she issued a notice to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these measures of compEnsation (nature of the tests, the modalities of their organisation and evaluation, the period of organization, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires a compEnsation measure to be carried out, the registrant has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the outstanding measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- If he does not require a measure of compEnsation, the prefect issues a professional card to the person concerned.

*To go further* Articles R. 212-90-1 and R. 212-92 of the Code of Sport.

