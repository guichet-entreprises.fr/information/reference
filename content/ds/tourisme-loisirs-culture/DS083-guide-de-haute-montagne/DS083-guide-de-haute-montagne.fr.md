﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS083" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Guide de haute montagne" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="guide-de-haute-montagne" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/guide-de-haute-montagne.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="guide-de-haute-montagne" -->
<!-- var(translation)="None" -->

# Guide de haute montagne

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le guide de haute montagne est un professionnel qui conduit et instruit en sécurité et en autonomie tous types de publics pratiquant l’alpinisme et ses activités assimilées. Il peut exercer comme accompagnant, enseignant et entraîneur dans une finalité éducative, de loisir ou de performance sportive. Il peut également intervenir comme conseiller technique et consultant dans les activités de montagne ou exercer des fonctions liées au secours en montagne.

Le guide de haute montagne exerce les activités suivantes :

- conduite et accompagnement de personnes dans des excursions ou des ascensions de montagne en rocher, neige, glace et terrain mixte ;
- conduite et accompagnement de personnes dans des excursions de ski de randonnée, ski alpinisme et ski hors-pistes ;
- enseignement des techniques d’alpinisme, d’escalade et de ski de randonnée, ski alpinisme et ski hors-pistes ;
- entraînement aux pratiques de compétition dans les disciplines précitées.

Le métier de guide de haute montagne peut s’exercer à temps plein ou à titre d’occupation saisonnière.

*Pour aller plus loin* : article 1 et annexe V de l’arrêté du 16 juin 2014 relatif à la formation spécifique du diplôme d’État d’alpinisme – guide de haute montagne.

### b. Centre de formalités des entreprises (CFE) compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de l’activité exercée et de la forme juridique de l’entreprise :

- pour les professions libérales, le CFE compétent est l’Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre du commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

Pour plus d’informations, il est conseillé de consulter les sites [de l'Urssaf](https://www.cfe.urssaf.fr/saisiepl/), [de la CCI de Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites), [de la CCI de Strasbourg](http://strasbourg.cci.fr/cfe/competences) et [du Service public](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24023).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

La personne qui souhaite exercer la profession de guide de haute montagne doit avoir la qualité d’éducateur sportif et être titulaire du diplôme d’État d’alpinisme - guide de haute montagne.

L’École nationale de ski et d’alpinisme (Ensa) assure la formation à ce diplôme. Pour plus d’informations, il est conseillé de consulter [le site de l'Ensa](http://www.Ensa.sports.gouv.fr/index.php?option=com_content&view=article&id=516&Itemid=772).

*Pour aller plus loin* : articles L. 212-1 et D. 212-68 et suivants du Code du sport ; arrêté du 16 juin 2014 relatif à la formation spécifique du diplôme d’État d’alpinisme – guide de haute montagne.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### Pour une Libre Prestation de Services

Les ressortissants de l’Union européenne (UE) ou de l’Espace économique européen (EEE) légalement établis dans un de ces États peuvent exercer la même activité en France de manière temporaire et occasionnelle à la condition d’avoir adressé au préfet du département de l’Isère une déclaration préalable d’activité.

Si l’activité ou la formation y conduisant n’est pas réglementée dans l’État membre d’origine ou l’État du lieu d’établissement, le ressortissant doit également justifier y avoir exercé cette activité pendant au moins l’équivalent d’une année à temps complet au cours des dix dernières années précédant la prestation.

Les ressortissants européens désireux d’exercer en France de manière temporaire ou occasionnelle doivent posséder les connaissances linguistiques nécessaires à l’exercice de l’activité en France, en particulier afin de garantir la sécurité des activités physiques et sportives et leur capacité à alerter les secours.

*Pour aller plus loin* : articles L. 212-7, R. 212-92 à R. 212-94 et A. 212-221 du Code du sport.

#### Bon à savoir : la différence substantielle

Pour l'encadrement de l'alpinisme par un guide de haute montagne, la différence substantielle, susceptible d'exister entre la qualification professionnelle du déclarant et la qualification professionnelle requise sur le territoire national, est appréciée en référence à la formation du diplôme de guide de haute montagne du brevet d'État d'alpinisme concernant :

- les connaissances théoriques et pratiques en matière de sécurité ; 
- les compétences techniques de sécurité.

Dans le cadre de la LPS, lorsque le préfet estime qu'il existe une différence substantielle, il peut décider de soumettre le déclarant à tout ou partie de l'épreuve d'aptitude ou d’un stage d’adaptation. Cette décision est prise après avis de la section permanente de l'alpinisme de la commission de la formation et de l'emploi du Conseil supérieur des sports de montagne transmis au Pôle national des métiers de l'encadrement du ski et de l'alpinisme.

*Pour aller plus loin* : articles A. 212-222, A. 212-224 et A. 212-228 du Code du sport.

#### Pour une Libre Établissement

Le ressortissant d’un État de l’UE ou de l’EEE peut s’établir en France pour y exercer de façon permanente s’il remplit l’une des quatre conditions suivantes.

##### Si l’État d’origine réglemente l’accès ou l’exercice de l’activité :

- être titulaire d’une attestation de compétence ou d’un titre de formation délivré par l’autorité compétente d’un État de l’UE ou de l’EEE et qui atteste d’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France ;
- être titulaire d’un titre acquis dans un État tiers et admis en équivalence dans un État de l’UE ou de l’EEE et justifier avoir exercé cette activité pendant au moins deux ans à temps complet dans cet État.

##### Si l’État membre d’origine ne réglemente ni l’accès, ni l’exercice de l’activité :

- justifier avoir exercé l’activité dans un État de l’UE ou de l’EEE, à temps complet pendant deux ans au moins au cours des dix dernières années, ou, en cas d’exercice à temps partiel, justifier d’une activité d’une durée équivalente et être titulaire d’une attestation de compétence ou d’un titre de formation délivré par l’autorité compétente d’un de ces États, qui atteste d’une préparation à l’exercice de l’activité, ainsi qu’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France ;
- être titulaire d’un titre attestant d’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France, délivré par l’autorité compétente d’un État de l’UE ou de l’EEE et sanctionnant une formation réglementée visant spécifiquement l’exercice de tout ou partie des activités mentionnées à l’article L. 212-1 du Code du sport et consistant en un cycle d’études complété, le cas échéant, par une formation professionnelle, un stage ou une pratique professionnelle.

Si le ressortissant remplit l’une des quatre conditions précitées, l’obligation de qualification requise pour exercer est réputée satisfaite.

Néanmoins, si la qualification professionnelle du ressortissant présente une différence substantielle avec la qualification requise en France qui ne permettrait pas de garantir la sécurité des pratiquants et des tiers, il peut être amené à se soumettre à une épreuve d’aptitude ou accomplir un stage d’adaptation (cf. infra « e. mesures de compensation »).

En revanche, dans le cas où le préfet estime qu'il n'existe pas de différence substantielle ou lorsqu'une différence substantielle a été identifiée et que le déclarant a satisfait à l'épreuve d'aptitude, le préfet délivre au déclarant une attestation de libre établissement et une carte professionnelle d'éducateur sportif.

Le ressortissant doit posséder la connaissance de la langue française nécessaire à l’exercice de son activité en France, en particulier afin de garantir l'exercice en sécurité des activités physiques et sportives et sa capacité à alerter les secours.

*Pour aller plus loin* : articles L. 212-7 et R. 212-88 à R. 212-90 du Code du sport.

#### Bon à savoir : la différence substantielle

Pour l'encadrement de l'alpinisme par un guide de haute montagne, la différence substantielle, susceptible d'exister entre la qualification professionnelle du déclarant et la qualification professionnelle requise sur le territoire national, est appréciée en référence à la formation du diplôme de guide de haute montagne du brevet d'État d'alpinisme en tant qu'elle intègre :

- les connaissances théoriques et pratiques en matière de sécurité ; 
- les compétences techniques de sécurité.

Dans le cadre de la liberté d'établissement, lorsque le préfet estime qu'il existe une différence substantielle, il saisit la commission de reconnaissance des qualifications, en joignant au dossier l'avis de la section permanente. Cette décision est prise après avis de la section permanente de l'alpinisme de la commission de la formation et de l'emploi du Conseil supérieur des sports de montagne, transmis au Pôle national des métiers de l'encadrement du ski et de l'alpinisme,

Après s'être prononcée sur l'existence d'une différence substantielle, la commission de reconnaissance des qualifications propose, le cas échéant, au préfet de soumettre le déclarant à une épreuve d'aptitude ou à un stage d’adaptation.

Lorsque le déclarant a satisfait à l'épreuve d'aptitude, le préfet lui délivre une attestation de libre établissement et une carte professionnelle d'éducateur sportif.

*Pour aller plus loin* : articles A. 212-222, A. 212-223 et A. 212-228, R. 212-88 à R. 212-91 du Code du sport.

### c. Conditions d’honorabilité et incompatibilités

Il est interdit d’exercer en tant que guide de haute montagne en France pour les personnes ayant fait l’objet d’une condamnation pour tout crime ou pour l’un des délits suivants :

- torture et actes de barbarie ;
- agressions sexuelles ;
- trafic de stupéfiants ;
- mise en danger d’autrui ;
- proxénétisme et infractions qui en résultent ;
- mise en péril des mineurs ;
- usage illicite de substances ou plantes classées comme stupéfiants ou provocation à l’usage illicite de stupéfiants ;
- infractions prévues aux articles L. 235-25 à L. 235-28 du Code du sport ;
- à titre de peine complémentaire à une infraction en matière fiscale : condamnation à une interdiction temporaire d’exercer, directement ou par personne interposée, pour son compte ou le compte d’autrui, toute profession industrielle, commerciale ou libérale (article 1750 du Code général des impôts).

De plus, nul ne peut enseigner, animer ou encadrer une activité physique ou sportive auprès de mineurs, s’il a fait l’objet d’une mesure administrative d’interdiction de participer, à quelque titre que ce soit, à la direction et à l’encadrement d’institutions et d’organismes soumis aux dispositions législatives ou réglementaires relatives à la protection des mineurs accueillis en centre de vacances et de loisirs, ainsi que de groupements de jeunesse, ou s’il a fait l’objet d’une mesure administrative de suspension de ces mêmes fonctions.

*Pour aller plus loin* : article L. 212-9 du Code du sport.

### d. Quelques particularités de la réglementation

#### Le stage de recyclage

Les titulaires du diplôme d’État d’alpinisme – guide de haute montagne sont soumis tous les six ans à un stage de recyclage afin d’actualiser leurs compétences professionnelles. Le recyclage conditionne l’exercice de la profession.

Il est organisé par l’Ensa.

*Pour aller plus loin* : arrêté du 11 mars 2015 relatif au contenu et aux modalités d'organisation du recyclage des titulaires des diplômes de guide de haute montagne.

#### L’environnement spécifique

La pratique de guide de haute montagne, constitue une activité s’exerçant dans un environnement spécifique. Elle implique le respect de mesures de sécurité particulières. Par conséquent, seuls les organismes sous la tutelle des sports peuvent former les futurs professionnels.

*Pour aller plus loin* : articles L. 212-2 et R. 212-7 du Code du sport. 

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Obligation de déclaration (en vue de l’obtention de la carte professionnelle d’éducateur sportif)

Toute personne souhaitant exercer l’une des professions régies par l’article L. 212-1 du Code du sport, doit déclarer son activité au préfet du département du lieu où elle compte exercer à titre principal. Cette déclaration déclenche l’obtention d’une carte professionnelle.

La déclaration doit être renouvelée tous les cinq ans.

#### Autorité compétente

La déclaration doit être adressée à la direction départementale de la cohésion sociale (DDCS) ou direction départementale de la cohésion sociale et de la protection des populations (DDCSPP) du département d’exercice ou du principal exercice, ou directement en ligne sur le [site officiel](https://eaps.sports.gouv.fr).

#### Délais

Dans le mois suivant le dépôt du dossier de déclaration, la préfecture envoie un accusé de réception au déclarant. La carte professionnelle, valable cinq ans, est ensuite adressée au déclarant.

#### Pièces justificatives

La déclaration doit contenir :

- formulaire de déclaration Cerfa 12699*02 ;
- une copie d’une pièce d’identité en cours de validité ;
- une photo d’identité ;
- une déclaration sur l’honneur attestant de l’exactitude des informations figurant dans le formulaire ;
- une copie de chacun des diplômes, titres, certificats invoqués ;
- une copie de l’autorisation d’exercice, ou, le cas échéant, de l’équivalence de diplôme ;
- un certificat médical de non contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées, datant de moins d’un an.

En cas de renouvellement de déclaration, il faut joindre :

- le formulaire Cerfa 12699*02 ;
- une photo d’identité ;
- une copie de l’attestation de révision en cours de validité pour les qualifications soumises à l’obligation de recyclage ;
- un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées, datant de moins d’un an.

De plus, dans tous les cas, la préfecture demandera elle-même la communication d’un extrait de moins de trois mois du casier judiciaire du déclarant pour vérifier l’absence d’incapacité ou d’interdiction d’exercer.

#### Coût

Gratuit.

*Pour aller plus loin* : articles L. 212-11, R. 212-85 et A. 212-176 et suivants du Code du sport.

### c. Effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité ponctuelle (Libre Prestation de Services) 

Les ressortissants de l’UE ou de l’EEE légalement établis dans l’un de ces États et souhaitant exercer en France de manière temporaire ou occasionnelle doivent effectuer une déclaration préalable d’activité, avant la première prestation de services.

Si le prestataire souhaite effectuer une nouvelle prestation en France, cette déclaration préalable doit être renouvelée.

Afin d’éviter des dommages graves pour la sécurité des bénéficiaires, le préfet peut, lors de la première prestation, procéder à une vérification préalable des qualifications professionnelles du prestataire.

#### Autorité compétente

La déclaration préalable d’activité doit être adressée au préfet du département de l’Isère.

#### Délais

Dans le mois suivant la réception du dossier de déclaration, le préfet notifie au prestataire :

- soit une demande d’informations complémentaires (dans ce cas, le préfet dispose de deux mois pour donner sa réponse) ;
- soit un récépissé de déclaration de prestation de services s’il ne procède pas à la vérification des qualifications. Dans ce cas, la prestation de services peut débuter ;
- soit qu’il procède à la vérification des qualifications. Dans ce cas, le préfet délivre ensuite au prestataire un récépissé lui permettant de débuter sa prestation ou, si la vérification des qualifications fait apparaître des différences substantielles avec les qualifications professionnelles requises en France, le préfet soumet le prestataire à une épreuve d’aptitude (cf. infra « 3°. e. mesures de compEnsation »).

Dans tous les cas, en l’absence de réponse dans les délais précités, le prestataire est réputé exercer légalement son activité en France.

#### Pièces justificatives

Le dossier de déclaration préalable d’activité doit contenir :

- un exemplaire du formulaire de déclaration dont le modèle est fourni à l’annexe II-12-3 du Code du sport ;
- une photo d’identité ;
- une copie d’une pièce d’identité ;
- une copie de l’attestation de compétence ou du titre de formation ;
- une copie des documents attestant que le déclarant est légalement établi dans l’État membre d’établissement et qu’il n’encourt aucune interdiction, même temporaire, d’exercer (traduits en français par un traducteur agréé) ;
- dans le cas où ni l’activité, ni la formation, conduisant à cette activité n’est réglementée dans l’État membre d’établissement, une copie de toutes pièces justifiant que le déclarant a exercé cette activité dans cet État pendant au moins l’équivalent de deux ans à temps complet au cours des dix dernières années (traduites en français par un traducteur agréé) ;
- l’un des trois documents au choix (à défaut, un entretien sera organisé):
  - une copie d’une attestation de qualification délivrée à l’issue d’une formation en français,
  - une copie d’une attestation de niveau en français délivré par une institution spécialisée,
  - une copie d’un document attestant d’une expérience professionnelle acquise en France.

#### Coût

Gratuit.

#### Voies de recours

Tout recours contentieux doit être exercé dans les deux mois de la notification de la décision auprès du tribunal administratif compétent.

*Pour aller plus loin* : articles R. 212-92 et suivants, articles A. 212-182-2 et suivants et annexe II-12-3 du Code du sport.

### d. Effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité permanente (Libre Établissement)

Tout ressortissant de l’UE ou de l’EEE qualifié pour y exercer tout ou partie des activités mentionnées à l’article L. 212-1 du Code du sport, et souhaitant s’établir en France, doit effectuer une déclaration préalable d’activité.

Cette déclaration permet au déclarant d’obtenir une carte professionnelle et ainsi d’exercer en toute légalité en France dans les mêmes conditions que les ressortissants français.

La déclaration doit être renouvelée tous les cinq ans.

En cas de différence substantielle avec la qualification requise en France, le préfet peut saisir, pour avis, la commission de reconnaissance des qualifications placée auprès du ministre chargé des sports. Il peut aussi décider de soumettre le ressortissant à une épreuve d’aptitude ou à un stage d’adaptation (cf. infra « 3° e. mesures de compEnsation »).

#### Autorité compétente

La déclaration préalable d’activité doit être adressée au préfet du département de l’Isère.

#### Délais

La décision du préfet de délivrer la carte professionnelle intervient dans un délai de trois mois à compter de la présentation du dossier complet par le déclarant. Ce délai peut être prorogé d’un mois sur décision motivée. Si le préfet décide de ne pas délivrer la carte professionnelle ou de soumettre le déclarant à une mesure de compEnsation (épreuve d’aptitude ou stage), sa décision doit être motivée.

#### Pièces justificatives pour la première déclaration d’activité

Le dossier de déclaration d’activité doit contenir :

- un exemplaire du formulaire de déclaration dont le modèle est fourni à l’annexe II-12-2-a du Code du sport ;
- une photo d’identité ;
- une copie d’une pièce d’identité en cours de validité ;
- un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives, datant de moins d’un an (traduit par un traducteur agréé) ;
- une copie de l’attestation de compétence ou du titre de formation, accompagnée de documents décrivant le cursus de formation (programme, volume horaire, nature et durée des stages effectués), traduit en français par un traducteur agréé ;
- le cas échéant, une copie de toutes pièces justifiant de l’expérience professionnelle (traduites en français par un traducteur agréé) ;
- si le titre de formation a été obtenu dans un État tiers, les copies des pièces attestant que ce titre a été admis en équivalence dans un État de l’UE ou de l’EEE qui réglemente l’activité ;
- l’un des trois documents au choix (à défaut, un entretien sera organisé) :
  - une copie d’une attestation de qualification délivrée à l’issue d’une formation en français,
  - une copie d’une attestation de niveau en français délivrée par une institution spécialisée,
  - une copie d’un document attestant d’une expérience professionnelle acquise en France ;
- les documents attestant que le déclarant n’a pas fait l’objet, dans l’État membre d’origine, d’une des condamnations ou mesures mentionnées aux articles L. 212-9 et L. 212-13 du Code du sport (traduits en français par un traducteur agréé).

#### Pièces justificatives pour un renouvellement de déclaration d’activité

Le dossier de renouvellement de déclaration d’activité doit contenir :

- un exemplaire du formulaire de renouvellement de déclaration dont le modèle est fourni à l’annexe II-12-2-b du Code du sport ;
- une photo d’identité ;
- un certificat médical de non contre-indication à la pratique et à l’encadrement des activités physiques ou sportives, datant de moins d’un an.

#### Coût

Gratuit.

#### Voies de recours

Tout recours contentieux doit être exercé dans les deux mois de la notification de la décision auprès du tribunal administratif compétent.

*Pour aller plus loin* : articles R. 212-88 à R. 212-91, A. 212-182 et les annexes II-12-2-a et II-12-b du Code du sport.

### e. Mesures de compensation

S’il existe une différence substantielle entre la qualification du requérant et celle requise en France pour exercer la même activité, le préfet saisit la commission de reconnaissance des qualifications, placée auprès du ministre chargé des sports. Cette commission saisit la section permanente du ski alpin de la commission de la formation et de l’emploi du Conseil supérieur des sports de montagne après examen et instruction du dossier. Elle émet, dans le mois de sa saisine, un avis qu’elle adresse au préfet. 

Dans son avis, la commission peut :

- estimer qu’il existe effectivement une différence substantielle entre la qualification du déclarant et celle requise en France. Dans ce cas, la commission propose de soumettre le déclarant à une épreuve d’aptitude ou un stage d’adaptation. Elle définit la nature et les modalités précises de ces mesures de compEnsation (nature des épreuves, modalités de leur organisation et de leur évaluation, période d’organisation, contenu et durée du stage, types de structures pouvant accueillir le stagiaire, etc.) ;
- estimer qu’il n’y a pas de différence substantielle entre la qualification du déclarant et celle requise en France.

À réception de l’avis de la commission, le préfet notifie sa décision motivée au déclarant (il n’est pas obligé de suivre l’avis de la commission) :

- s’il exige qu’une mesure de compEnsation soit effectuée, le déclarant dispose d’un délai d’un mois pour choisir entre la ou les épreuve(s) d’aptitude et le stage d’adaptation. Le préfet délivre ensuite une carte professionnelle au déclarant qui a satisfait aux mesures de compEnsation. En revanche, si le stage ou l’épreuve d’aptitude ne sont pas satisfaisants, le préfet notifie sa décision motivée de refus de délivrance de la carte professionnelle à l’intéressé ;
- s’il n’exige pas de mesure de compEnsation, le préfet délivre une carte professionnelle à l’intéressé.

*Pour aller plus loin* : articles R. 212-90-1 et R. 212-92 du Code du sport.