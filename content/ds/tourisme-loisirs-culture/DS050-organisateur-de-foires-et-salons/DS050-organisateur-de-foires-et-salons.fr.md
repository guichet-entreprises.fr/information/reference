﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS050" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Organisateur de foires et salons" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="organisateur-de-foires-et-salons" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/organisateur-de-foires-et-salons.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="organisateur-de-foires-et-salons" -->
<!-- var(translation)="None" -->

# Organisateur de foires et salons

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'organisateur de foires et salons est un professionnel dont l'activité consiste à assurer la promotion et la gestion d'une manifestation commerciale. Il est également chargé de fournir des emplacements aux professionnels qui envisagent d'exposer et de proposer leurs produits et services au cours de l'événement.

Ces manifestations commerciales de nature collective et temporaire peuvent être :

- des salons dits professionnels, consacrés à une branche professionnelle spécifique, dont les exposants peuvent vendre sur place leurs produits en vue de l'usage personnel de l'acquéreur ;
- des salons ouverts au public au sein desquels les professionnels proposent leurs biens en vente directe ;
- des foires au sein desquelles les exposants proposent leurs biens en vente directe avec retrait de la marchandise.

**À noter**

Ces activités peuvent s'exercer dans des parcs d'exposition équipés d'installations permanentes et accueillant des exposants toute l'année. Toutefois, ces parcs d'exposition sont soumis à une réglementation spécifique qui ne sera pas traitée dans cette fiche.

*Pour aller plus loin* : article L. 762-2 du Code de commerce.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

- pour les entreprises individuelles, le CFE compétent est l'Urssaf ;
- pour les activités commerciales, il s'agit de la chambre de commerce et d'industrie (CCI) ;
- en cas de création d'une société civile, le CFE compétent est le greffe du tribunal de commerce, ou le greffe du tribunal d'instance dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer l'activité d'organisateur de foires et salons, le professionnel n'est soumis à aucune exigence de diplôme. Toutefois, il doit effectuer en amont de chaque manifestation commerciale une déclaration préalable dont la nature dépend de la structure au sein de laquelle a lieu l'opération concernée.

#### En cas de manifestation commerciale au sein d'un parc d'exposition enregistré

Lorsque la manifestation commerciale est organisée au sein de l'un de ces parcs, l'organisateur de foires et salons doit fournir à l'exploitant des lieux les informations nécessaires à l'inscription de la manifestation au sein de la déclaration du programme du programme annuel du parc, qui doit obligatoirement intervenir avant le début du dernier trimestre de l'année précédente. À défaut, il est tenu directement la déclaration (cf. infra « 3°. a. Déclaration préalable »).

À ce titre il doit fournir :

- les caractéristiques chiffrées de la manifestation (à titre d'estimation lorsqu'il s'agit de la première session) :
  - la surface en mètres carrés,
  - le nombre d'exposants et de visites,
  - le nombre de visiteurs pour les salons professionnels, dont, à titre facultatif, le nombre de visiteurs étrangers,
  - à titre facultatif, le nombre et la surface nette occupée par les exposants étrangers ;
- les éléments permettant de l'identifier (nom, adresse, numéro Siret, site internet, etc.).

Lorsque la manifestation a déjà fait l'objet d'une précédente session, le professionnel doit fournir les chiffres contrôlés de cette dernière session.

L'ensemble de ces données chiffrées doit faire l'objet d'un contrôle par un organisme accrédité par le Comité français d'accréditation [Cofrac](https://www.cofrac.fr/). Toutefois, lorsque la surface nette de la manifestation est inférieure à 1 000 m², ce contrôle peut être effectué par l'exploitant du parc lui-même.

*Pour aller plus loin* : articles R. 762-4 et 762-5 du Code de commerce ; article A. 762-3 du Code de commerce ; annexe II de l'annexe 7-10 du Code de commerce.

#### En cas de manifestation commerciale en dehors d'un parc d'exposition

Lorsque la manifestation commerciale de type salon professionnel a lieu en dehors de tout parc d'exposition, son organisateur doit effectuer directement la déclaration préalable (cf. infra « 3°. a. Déclaration préalable »).

*Pour aller plus loin* : article R. 762-10 du Code de commerce.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre établissement)

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer l'activité d'organisateur de foires et salons à titre temporaire et occasionnel et permanent en France.

A ce titre, le professionnel ressortissant UE est soumis aux mêmes exigences professionnelles que le ressortissant français (cf. supra « 2°. a. Qualifications professionnelles »).

### c. Quelques particularités de la réglementation de l’activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public.

#### Manifestation relative à la vente d'animaux de compagnie

L'organisateur d'une manifestation commerciale consacrée à la vente d'animaux de compagnie doit effectuer une déclaration préalable auprès du préfet du département où aura lieu l'événement. À ce titre, il devra également veiller au respect des dispositions en matière sanitaire et à la protection animale.

**À noter**

La vente ou la cession à titre gratuit, d'animaux de compagnie dans les manifestations commerciales n'est autorisée que si ces manifestations sont consacrées exclusivement aux animaux.

*Pour aller plus loin* : articles L. 214-7 et R. 214-31 et R. 214-31-1 du Code rural et de la pêche maritime.

#### Dispositions applicables en cas d'ouverture de débit de boissons temporaire

Le professionnel qui souhaite ouvrir un débit de boissons temporaire au cours de la foire ou du salon doit effectuer une demande d'autorisation auprès de la mairie du lieu de l’événement (ou à la préfecture de police s'il a lieu à Paris).

**À noter**

Seule la vente des boissons des groupes un et trois de la classification des boissons sera autorisée (cf. article L. 3321-1 du Code de la santé publique).

*Pour aller plus loin* : L. 3334-2 du Code de la santé publique.

#### Diffusion de musique

L'organisateur qui souhaite diffuser de la musique au cours de son évènement, doit adresser en ligne une demande d'autorisation préalable à la Société des auteurs compositeurs et éditeurs de musique (Sacem) et s'acquitter du montant de la redevance. Le professionnel doit s'inscrire et remplir le formulaire d'autorisation en ligne sur le site de la [Sacem]((%3Chttps://clients.sacem.fr/autorisations%3E)).

#### Prix maximum des marchandises vendues lors d'un salon professionnel

Le professionnel organisant un salon professionnel doit veiller à ce que la valeur des marchandises proposées à la vente sur place n'excède pas 80 euros toutes taxes comprises (TTC).

Dès lors que ce montant est dépassé, la manifestation sera requalifiée en « salon ouvert au public » ou en « vente au déballage ».

*Pour aller plus loin* : article D. 762-13 du Code de commerce.

#### Dispositions spécifiques en cas de vente d'objets mobiliers usagés

En cas de vente de tels objets au cours d'une manifestation commerciale, le professionnel doit obligatoirement tenir à jour un registre permettant d'identifier les vendeurs.

Ce registre doit mentionner :

- l'identité de chaque vendeur professionnel ;
- en cas de vendeur non professionnel, un document attestant sur l'honneur qu'ils n'ont pas participé à deux autres manifestations commerciales au cours de l'année ;
- lorsqu'il s'agit d'une personne morale, les éléments permettant de l'identifier ainsi que l'identité et la qualité de son représentant.

Ce registre doit être à la disposition des services de police, gendarmerie, douanes et des services de la concurrence, de la consommation et de la répression des fraudes pendant toute la durée de la manifestation.

**À noter**

En cas de non-respect de cette obligation ou en cas de mentions inexactes sur le registre, le professionnel encourt une peine de six mois d'emprisonnement et 30 000 euros d'amende.

Un modèle de ce registre est fixé à l'annexe II de l'arrêté du 21 juillet 1992 fixant les modèles de registres prévus par le décret n° 8861040 du 14 novembre 1988 relatif à la vente ou à l'échange de certains objets mobiliers.

*Pour aller plus loin* : articles R. 321-9 à R. 321-12 du Code pénal.

## 3°. Démarches et formalités d’installation

### a. Déclaration préalable (hors programme annuel de parc d'exposition)

#### Autorité compétente

L'organisateur de foires et salons doit adresser sa déclaration par voie électronique au préfet du département dans lequel doit se dérouler la manifestation au moins deux mois avant son début, au moyen du [service de télédéclaration](https://www.foiresetsalons.entreprises.gouv.fr/teleprocedure.php).

*Pour aller plus loin* : article du site service-public.fr sur l'[organisation de foires et salons](https://www.service-public.fr/professionnels-entreprises/vosdroits/F22688).

#### Issue de la procédure

Dans un délai de quinze jours à compter de la réception de la demande, le préfet doit soit :

- informer le demandeur que son dossier est incomplet. Ce dernier dispose alors d'un délai de dix jours pour le compléter ;
- lui délivrer, par voie dématérialisée, un récépissé en cas de dossier reçu complet.

En cas de modification de l'un des éléments de la déclaration, le professionnel doit adresser une déclaration modificative au préfet par voie électronique au moyen du [service de télédéclaration](https://www.foiresetsalons.entreprises.gouv.fr/teleprocedure.php).

*Pour aller plus loin* : articles A. 762-4, A. 762-8 et annexes IV, V, et XI de l'annexe 7-10 du Code de commerce.

### b. Formalités de déclaration de l’entreprise

#### Autorité compétente

L'organisateur de foires et salons doit procéder à la déclaration de son entreprise, et doit pour cela, effectuer une déclaration auprès de la CCI.

#### Pièces justificatives

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la CCI adresse le jour même, un récépissé au professionnel mentionnant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la CCI indique au demandeur les organismes auxquels son dossier à été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts.

### c. Le cas échéant, enregistrer les statuts de la société

L'organisateur de foires et salons doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.