﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS050" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Organiser of fairs and exhibitions" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="organiser-of-fairs-and-exhibitions" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/organizer-of-fairs-and-exhibitions.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="organizer-of-fairs-and-exhibitions" -->
<!-- var(translation)="Auto" -->

Organiser of fairs and exhibitions
============================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

The organizer of fairs and fairs is a professional whose activity is to ensure the promotion and management of a commercial event. It is also responsible for providing locations for professionals who plan to exhibit and offer their products and services during the event.

These collective and temporary trade events may be:

- so-called trade shows, dedicated to a specific professional branch, whose exhibitors can sell their products on site for the personal use of the purchaser;
- fairs open to the public where professionals offer their goods for direct sale;
- fairs in which exhibitors offer their goods for direct sale with the withdrawal of the goods.

**Please note**

These activities can be performed in exhibition parks equipped with permanent facilities and hosting exhibitors all year round. However, these exhibition parks are subject to specific regulations that will not be addressed in this card.

*For further information*: Article L. 762-2 of the Code of Commerce.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for individual companies, the relevant CFE is the Urssaf;
- For commercial activities, it is the Chamber of Commerce and Industry (CCI);
- in the case of the creation of a civil society, the competent CFE is the registry of the Commercial Court, or the registry of the district court in the departments of lower Rhine, Upper Rhine and Moselle.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

In order to carry out the activity of organising fairs and fairs, the professional is not subject to any diploma requirement. However, it must make a prior declaration ahead of each commercial event, the nature of which depends on the structure within which the transaction takes place.

#### In the event of a commercial event within a registered exhibition park

When the commercial event is held within one of these parks, the organizer of fairs and fairs must provide the venue operator with the information necessary to register the event in the declaration of the annual park program, which must take place before the beginning of the last quarter of the previous year. Failing that, the declaration is held directly (see infra "3." a. Pre-declaration").

As such, it must provide:

- The numerical characteristics of the event (as an estimate for the first session):- The square footage,
  - The number of exhibitors and visits
  - The number of visitors to trade shows, including, on an optional basis, the number of foreign visitors,
  - optional, the number and net space occupied by foreign exhibitors;
- identifying elements (name, address, Siret number, website, etc.).

When the event has already been the subject of a previous session, the professional must provide the controlled figures for that last session.

All of these encrypted data must be monitored by an agency accredited by the French Accreditation Committee[Cofrac](https://www.cofrac.fr/). However, where the net area of the event is less than 1,000 m2, this control can be carried out by the operator of the park itself.

*For further information*: Articles R. 762-4 and 762-5 of the Code of Commerce; Article A. 762-3 of the Code of Commerce; Appendix II of Appendix 7-10 of the Code of Commerce.

#### In the event of a commercial event outside an exhibition park

When the trade show-type event takes place outside of any exhibition park, its organizer must make the pre-declaration directly (see infra "3." a. Pre-declaration").

*For further information*: Article R. 762-10 of the Code of Commerce.

### b. Professional Qualifications - European Nationals (Free Service Or Freedom of establishment)

There is no provision for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement to carry out the activity of organising fairs and fairs on a temporary basis and casual and permanent in France.

As such, the eu national professional is subject to the same professional requirements as the French national (see above "2o. a. Professional qualifications").

### c. Some peculiarities of the regulation of the activity

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

It is advisable to refer to the "Establishment receiving the public" sheet for more information.

*For further information*: order of June 25, 1980 approving the general provisions of the fire and panic safety regulations in public institutions.

**Pet sale protest**

The organizer of a commercial event dedicated to the sale of pets must make a prior declaration to the prefect of the department where the event will take place. As such, it will also have to ensure compliance with health and animal protection provisions.

**Please note**

The sale or sale of pets free of charge in commercial events is only permitted if these events are dedicated exclusively to animals.

*For further information*: Articles L. 214-7 and R. 214-31 and R. 214-31-1 of the Rural Code and Marine Fisheries.

**Provisions for temporary beverage outlets**

The professional who wishes to open a temporary drinks outlet during the fair or trade show must apply for permission from the town hall of the event venue (or to the police prefecture if it takes place in Paris).

**Please note**

Only the sale of beverages from groups one and three of the beverage classification will be permitted (see Section L. 3321-1 of the Public Health Code).

*For further information*: L. 3334-2 of the Public Health Code.

**Music broadcast**

The organizer who wishes to broadcast music during his event, must apply online for prior authorization to the Society of Composers and Music Publishers (Sacem) and pay the amount of the royalty. The professional must register and complete the online authorisation form on the[Sacem]((%3Chttps://clients.sacem.fr/autorisations%3E)).

**Maximum price of goods sold at a trade show**

The professional organising a trade show must ensure that the value of the goods offered for sale on site does not exceed 80 euros all taxes included (TTC).

As soon as this amount is exceeded, the event will be reclassified as a "public lounge" or "unpacking sale".

*For further information*: Article D. 762-13 of the Code of Commerce.

**Specific provisions for the sale of used furniture**

In the event of the sale of such items during a commercial event, the professional must maintain a register to identify the sellers.

This register should mention:

- The identity of each professional seller
- In the case of a non-professional seller, a document attesting to the honour that they did not participate in two other commercial events during the year;
- when it comes to a legal person, the elements to identify it and the identity and quality of its representative.

This register must be available to the police, gendarmerie, customs and competition, consumer and fraud enforcement services for the duration of the demonstration.

**Please note**

If this obligation is not met or if there are inaccurate mentions on the register, the professional faces a six-month prison sentence and a fine of 30,000 euros.

A model of this register is attached to the Appendix II of the decree of 21 July 1992 establishing the models of registers provided for by Decree No. 8861040 of 14 November 1988 relating to the sale or exchange of certain objects.

*For further information*: Articles R. 321-9 to R. 321-12 of the Penal Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Pre-declaration (excluding annual exhibition park program)

**Competent authority**

The organiser of fairs and fairs must send his declaration electronically to the prefect of the department in which the event is to take place at least two months before its start, using the[tele-declaration service](https://www.foiresetsalons.entreprises.gouv.fr/teleprocedure.php).

*For further information*: service-public.fr website article on the[organising fairs and fairs](https://www.service-public.fr/professionnels-entreprises/vosdroits/F22688).

**Outcome of the procedure**

Within a fortnight of receiving the application, the prefect must either:

- inform the applicant that his file is incomplete. The latter then has ten days to complete it;
- to provide him, by means of dematerialization, a receipt in case of a complete receipt.

If any of the elements of the declaration are changed, the professional must send a amending statement to the prefect electronically through the[tele-declaration service](https://www.foiresetsalons.entreprises.gouv.fr/teleprocedure.php).

*For further information*: Articles A. 762-4, A. 762-8 and Appendixes IV, V, and XI of Schedule 7-10 of the Code of Commerce.

### b. Company reporting formalities

**Competent authority**

The organizer of fairs and fairs must make the declaration of his company, and for this, must make a declaration to the ICC.

**Supporting documents**

The person concerned must provide the supporting documents depending on the nature of its activity.

**Timeframe**

The ICC's Business Formalities Centre sends a receipt to the professional on the same day mentioning the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the ICC tells the applicant which agencies their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code.

### c. If necessary, register the company's statutes

The organiser of fairs and fairs must, once the company's statutes have been dated and signed, register them with the corporate tax office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.