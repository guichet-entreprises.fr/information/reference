﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS045" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Gîte ou meublé de tourisme" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="gite-ou-meuble-de-tourisme" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/gite-ou-meuble-de-tourisme.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="gite-ou-meuble-de-tourisme" -->
<!-- var(translation)="None" -->

# Gîte ou meublé de tourisme

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Les gîtes sont définis comme des meublés de tourisme. Ce sont des villas, appartements, ou studios meublés, à l'usage exclusif du locataire, offerts en location à une clientèle de passage qui y effectue un séjour caractérisé par une location à la journée, à la semaine ou au mois, et qui n'y élit pas domicile.

La location doit être conclue pour une durée maximale et non renouvelable de 90 jours consécutifs à la même personne.

*Pour aller plus loin* : I de l'article L. 324-1-1 du Code du tourisme ; article 1-1 de la loi du 2 janvier 1970 réglementant les conditions d'exercice des activités relatives à certaines opérations portant sur les immeubles et les fonds de commerce.

**Bon à savoir**

Lorsque les meublés de tourisme sont situés en zone rurale, on emploie l’appellation « gîte rural ».

### b. Centre de formalités des entreprises (CFE) compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de l’activité exercée :

- si l’activité est accompagnée de prestations complémentaires de service d’hôtellerie (par exemple, le nettoyage du gîte) ou de loisirs (par exemple, la location de VTT), il s’agit d’une activité commerciale. Le CFE compétent est donc la chambre de commerce et d’industrie ;
- si la location de gîte est liée à une exploitation agricole ou si elle en constitue un prolongement, l’activité doit être déclarée auprès de la chambre d’agriculture ;
- si la location de gîte est le prolongement d’une exploitation agricole mais n’est pas gérée par l’exploitant agricole, le CFE compétent est le greffe du tribunal de commerce ;
- si l’activité n’est ni agricole ni commerciale, le CFE compétent est le greffe du tribunal de commerce.

**À noter**

Il s’agit du greffe du tribunal d’instance dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle.

*Pour aller plus loin* : article R. 123-3 du Code du commerce et article L. 311-2-1 du Code rural et de la pêche maritime.

## 2°. Conditions d’installation

### a. Condition de nationalité

Il n’y a pas de condition de nationalité pour ouvrir un gîte.

Cependant, dès lors que le loueur sert des boissons alcoolisées, il doit justifier qu'il est soit :

- français ;
- ressortissant d'un État de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) ;
- ou ressortissant d’un État ayant conclu un traité de réciprocité avec la France comme l’Algérie ou le Canada par exemple.

Pour plus d’informations, il est conseillé de se référer à la fiche « [Débit de boissons](https://www.guichet-entreprises.fr/fr/ds/alimentation/debitant-de-boissons.html) ».

*Pour aller plus loin* : article L. 3332-3 du Code de la santé publique.

### b. Conditions d’honorabilité et incompatibilités liées à l’exploitation du débit de boissons

Il n’y a pas de condition d’honorabilité ni d’incompatibilité pour ouvrir un gîte.

En revanche, dès lors que le restaurateur sert des boissons alcoolisées, il doit respecter les conditions d’honorabilité et les incompatibilités relatives à la profession de débitant de boissons. Pour plus d’informations, il est conseillé de se reporter à la fiche « [Débit de boissons](https://www.guichet-entreprises.fr/fr/ds/alimentation/debitant-de-boissons.html) ».

### c. Le cas échéant, obtenir un permis d’exploitation et une licence pour l’exploitation du débit de boissons

Lorsque le propriétaire souhaite vendre de l’alcool dans son gîte, il doit obtenir un permis d’exploitation et une licence.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Débitant de boissons](https://www.guichet-entreprises.fr/fr/ds/alimentation/debitant-de-boissons.html) ».

### d. Quelques particularités de la réglementation de l’activité

#### Déclarer sur l’honneur ses obligations

Toute personne qui se livre ou prête son concours contre rémunération ou à titre gratuit, par une activité d'entremise ou de négociation ou par la mise à disposition d'une plateforme numérique, à la mise en location d'un meublé de tourisme soumis à l'article L. 324-1-1 du Code du tourisme et aux articles L. 631-7 et suivants du Code de la construction et de l'habitation informe le loueur des obligations de déclaration ou d'autorisation préalables prévues par ces articles et obtient de lui, préalablement à la publication ou à la mise en ligne de l'annonce de location, une déclaration sur l'honneur attestant du respect de ces obligations, indiquant si le logement constitue ou non sa résidence principale, ainsi que, le cas échéant, le numéro de déclaration, obtenu en application du III de l'article L. 324-1-1 du Code du tourisme. Elle publie, dans toute annonce relative à ce meublé, son numéro de déclaration.

Dans les communes ayant mis en œuvre la procédure d'enregistrement mentionnée au III de l'article L. 324-1-1, la commune peut, jusqu'au 31 décembre de l'année suivant celle au cours de laquelle un meublé de tourisme a été mis en location, demander à un intermédiaire, de lui transmettre le nombre de jours au cours desquels ce meublé de tourisme a fait l'objet d'une location par son intermédiaire. Il transmet ces informations dans un délai d'un mois, en rappelant l'adresse du meublé et son numéro de déclaration. La commune peut demander un décompte individualisé pour une liste de meublés de tourisme dans un périmètre donné. Dans ces mêmes communes, l'intermédiaire n'offre plus à la location un meublé de tourisme déclaré comme résidence principale du loueur lorsqu'elle a connaissance, notamment lorsqu'elle met à disposition une plateforme numérique de nature à lui conférer la connaissance ou le contrôle des données stockées, que ce meublé a été loué, par son intermédiaire, plus de cent vingt jours au cours d'une même année civile. Elle remplit ses obligations sur la base de la déclaration sur l'honneur mentionnée au même I. Le dispositif de retrait des offres peut être mutualisé par plusieurs personnes mentionnées au même I. Le cas échéant, ce dispositif mutualisé est certifié chaque année avant le 31 décembre par un tiers indépendant.

*Pour aller plus loin* : article L. 324-2-1 du Code du tourisme.

#### Afficher le classement du meublé

Le loueur du meublé ou son mandataire peut signaler le classement de son meublé (cf. « d. Le cas échéant, demander le classement du meublé ») par l'affichage d'un panonceau. Il doit afficher, de manière visible à l'intérieur du meublé, la décision de classement.

Le nombre d'étoiles figurant sur le panonceau correspond au nombre d'étoiles attribué par la décision de classement.

*Pour aller plus loin* : article D. 324-2 et suivants Code du tourisme ; article 1er de l’arrêté du 22 décembre 2010 relatif aux panonceaux des hébergements de tourisme.

#### Remettre un contrat de location saisonnière avec un état descriptif des lieux loués au locataire 

Toute offre ou contrat de location saisonnière doit revêtir la forme écrite et contenir l'indication du prix demandé ainsi qu'un état descriptif des lieux.

Bon à savoir : sera punie d'une amende de 3 750 euros, toute personne qui, à l'occasion d'une location saisonnière ou d'une offre de location saisonnière d'un local meublé, en vue de l’habitation, aura fourni des renseignements manifestement inexacts sur la situation de l'immeuble, la consistance et l'état des lieux, les éléments de confort ou l'ameublement. En cas de récidive, l'amende pourra être portée à 7 500 euros.

*Pour aller plus loin* : article L. 324-2 du Code du tourisme ; décret n° 67-128 du 14 février 1967 réprimant la production de renseignements inexacts en cas d'offre ou de contrat de location saisonnière en meublé.

#### Respecter la réglementation relative aux établissements recevant du public (ERP) 

Le loueur doit alors se conformer aux règles de sécurité et d’accessibilité relatives aux ERP lorsque la capacité maximale d'un hébergement est supérieure à 15 personnes. Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

#### Délivrer la note de facturation au client 

Le loueur doit délivrer au client une note lorsque le prix de la prestation est supérieur ou égal à 25 € (TVA comprise).

Pour les prestations de service dont le prix est inférieur à 25 euros (TVA comprise), la délivrance d'une note est facultative, mais celle-ci doit être remise au client s'il la demande.

La note doit obligatoirement mentionner :

- la date de rédaction de la note ;
- le nom et d'adresse du prestataire ;
- le nom du client, sauf opposition de celui-ci ;
- la date et le lieu d'exécution de la prestation.

*Pour aller plus loin* : arrêté du 3 octobre 1983 relatif à la publicité des prix de tous les services.

#### Payer la taxe de séjour 

Les communes peuvent demander aux vacanciers séjournant sur leur territoire de payer une taxe de séjour. Cette taxe permet aux communes de financer les dépenses liées à la fréquentation touristique ou à la protection de leurs espaces naturels.

La taxe de séjour s’applique aux personnes majeures qui ne sont pas domiciliées dans la commune et qui n'y possèdent pas de résidence pour lesquelles elles sont redevables de la taxe d'habitation.

Le tarif de la taxe de séjour est fixé, pour chaque nature et pour chaque catégorie d'hébergement, par personne et par nuitée de séjour.

Ce tarif est arrêté par délibération du conseil municipal prise avant le 1er octobre de l'année pour être applicable l'année suivante. La délibération fixe, le cas échéant, les dates de début et de fin des périodes de perception au sein de l'année. 

**À noter**

À compter du 1er janvier 2019, les professionnels qui, par voie électronique, assurent un service de réservation ou de location ou de mise en relation en vue de la location d'hébergements et qui sont intermédiaires de paiement (en premier lieu les plateformes numériques) ont l'obligation de collecter la taxe de séjour pour le compte de loueurs non professionnels et de la reverser à la commune.

*Pour aller plus loin* : articles L. 2333-26 et suivants du Code général des collectivités territoriales.

#### Compléter une fiche de police pour les clients étrangers 

Aux fins de prévention des troubles à l'ordre public, d'enquêtes judiciaires et de recherche dans l'intérêt des personnes, les loueurs de meublés de tourisme sont tenus de remplir, ou faire remplir, et signer par l'étranger, dès son arrivée, une fiche individuelle de police.

Les données personnelles ainsi collectées sont notamment :

- le nom et les prénoms ;
- la date et le lieu de naissance ;
- la nationalité ;
- le domicile habituel de l'étranger ;
- le numéro de téléphone mobile et l'adresse électronique de l'étranger ;
- la date d'arrivée au sein de l'établissement et la date de départ prévue.

Les enfants âgés de moins de 15 ans peuvent figurer sur la fiche d'un adulte qui les accompagne.

Les fiches ainsi établies doivent être conservées pendant une durée de six mois et remises, sur leur demande, aux services de police et unités de gendarmerie. Cette transmission peut s'effectuer sous forme dématérialisée.

Les personnes physiques ou morales louant des locaux nus ne sont pas astreintes à ces obligations.

*Pour aller plus loin* : arrêté du 1er octobre 2015 pris en application de l'article R. 611-42 du Code de l'entrée et du séjour des étrangers et du droit d'asile.

#### Publicité du gîte  

Pour faire connaître son gite, le loueur doit respecter les prescriptions relatives à l'installation et à l'entretien des enseignes publicitaires pour assurer la protection du cadre de vie.

*Pour aller plus loin* : articles L. 581-1 et suivants et articles R. 581-1 et suivants du Code de l’environnement.

#### Le cas échéant, respecter la réglementation relative aux piscines 

##### Déclarer l’installation d’une piscine

Toute personne qui procède à l'installation d'une piscine doit en faire, avant l'ouverture, la déclaration à la mairie du lieu de son implantation.

Cette déclaration, accompagnée d'un dossier justificatif, comporte l'engagement que l'installation de la piscine satisfait aux normes d'hygiène et de sécurité fixées par les articles D. 1332-1 et suivants du Code de la santé publique.

*Pour aller plus loin* : articles L. 1332-1 du Code de la santé publique.

##### Entretien de la piscine

La personne responsable d'une piscine est tenue de surveiller la qualité de l'eau et d'informer le public sur les résultats de cette surveillance, de se soumettre à un contrôle sanitaire, de respecter les règles et les limites de qualité fixées par décret, et de n'employer que des produits et procédés de traitement de l'eau, de nettoyage et de désinfection efficaces et qui ne constituent pas un danger pour la santé des baigneurs et du personnel chargé de l'entretien et du fonctionnement de la piscine ou de la baignade artificielle.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Exploitant piscine, lieu de baignade](https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/exploitant-piscine-lieu-de-baignade.html) ».

*Pour aller plus loin* : articles L. 1332-1 et suivants, D. 1332-1 et suivants du Code de la santé publique ; arrêté du 7 avril 1981 fixant les dispositions techniques applicables aux piscines.

Assurer la sécurité de la piscine 

La piscine doit être équipée d'un dispositif de sécurité afin de prévenir les risques de noyade.

Le dispositif est constitué par :

- une barrière de protection ;
- une couverture ;
- un abri ;
- une alarme.

Le non-respect des dispositions relatives à la sécurité des piscines est puni de 45 000 euros d'amende.

*Pour aller plus loin* : articles L. 128-1 à L. 128-3, R. 128-2 et L. 152-12 du Code de la construction et de l’habitation.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Un loueur, qu'il soit particulier ou professionnel, doit remplir une déclaration de début d'activité (Cerfa n° 11921*04 appelé aussi formulaire POi). Si l’activité est commerciale, le loueur doit déclarer l’ouverture du gîte pour être immatriculé au registre du commerce et des sociétés (RCS).

### b. Déclarer la location de meublé de tourisme

La personne qui offre à la location un meublé de tourisme classé ou non, doit effectuer préalablement une déclaration en mairie en remplissant le formulaire Cerfa n° 14004*04.

Lorsque le local à usage d'habitation constitue la résidence principale du loueur, il est dispensé de la déclaration préalable. Toutefois, dans les villes qui ont mis en place la procédure de l'enregistrement, toutes les locations touristiques, qu’il s’agisse de la résidence principale ou secondaire, devront disposer d’un numéro d’enregistrement à publier dans chaque annonce de location. Les plateformes intermédiaires de location (Airbnb, Abritel, etc.) ont l’obligation de déconnecter chaque annonce qui ne contient pas de numéro d’enregistrement.

Dans ces communes, toute personne qui offre à la location un meublé de tourisme qui est déclaré comme sa résidence principale ne peut le faire au-delà de cent vingt jours au cours d'une même année civile, sauf obligation professionnelle, raison de santé ou cas de force majeure.

**Bon à savoir**

Le fait de ne pas respecter cette obligation de déclaration est puni d’une amende de 450 euros au plus.

*Pour aller plus loin* : articles L. 324-1-1, D. 324-1-1 et R. 324-1-2 du Code du tourisme ; article 131-13 du Code pénal.

La résidence principale est entendue comme le logement occupé au moins huit mois par an.

Tout changement concernant les éléments d'information que comporte la déclaration fait l'objet d'une nouvelle déclaration en mairie.

#### Autorité compétente

Le maire de la commune où est situé le meublé.

#### Pièces justificatives : Cerfa 14004*04

La déclaration précise l'identité et l'adresse du déclarant, l'adresse du meublé de tourisme, le nombre de pièces composant le meublé, le nombre de lits, la ou les périodes prévisionnelles de location et, le cas échéant, la date de la décision de classement et le niveau de classement des meublés de tourisme.

*Pour aller plus loin* : articles L. 324-1-1 et D. 324-1-1 du Code du tourisme ; article 2 de la loi du 6 juillet 1989 tendant à améliorer les rapports locatifs et portant modification de la loi n° 86-1290 du 23 décembre 1986.

### c. Le cas échéant, obtenir l’autorisation de changer l’usage du logement

Dans certaines villes, il est obligatoire d’obtenir une autorisation de la mairie pour pouvoir modifier l'usage du logement en meublé de tourisme (passage de l'habitation principale en meublé touristique).

**À noter**

Une délibération du conseil municipal peut définir un régime d'autorisation temporaire de changement d'usage permettant à une personne physique de louer pour de courtes durées des locaux destinés à l'habitation à une clientèle de passage qui n'y élit pas domicile. Le local à usage d'habitation bénéficiant de cette autorisation temporaire ne change pas de destination.

Le fait de louer un local meublé destiné à l'habitation de manière répétée pour de courtes durées à une clientèle de passage qui n'y élit pas domicile constitue un changement d'usage.

Cette obligation d'autorisation concerne :

- les communes de plus de 200 000 habitants ;
- les communes des départements des Hauts-de-Seine, de Seine-Saint-Denis et du Val-de-Marne, quelle que soit leur taille.

Sur décision administrative, cette obligation peut concerner d’autres communes.

*Pour aller plus loin* : articles L. 631-7 à L. 631-9 du Code de la construction et de l’habitation.

### d. Le cas échéant, demander le classement du meublé

Le loueur du meublé ou son mandataire qui souhaite obtenir le classement transmet une demande de classement en meublé de tourisme à un organisme de son choix. Le classement permettra d’indiquer au client un niveau de confort et de prestation.

La décision de classement d'un meublé de tourisme dans une catégorie, est prononcée par l'organisme qui a effectué la visite de classement.

*Pour aller plus loin* : article D. 324-3 du Code du tourisme.

#### Autorité compétente

Cette visite de classement est effectuée :

- soit par des organismes évaluateurs accrédités par tout organisme européen équivalent signataire de l'accord multilatéral pris dans le cadre de la coordination européenne des organismes d'accréditation ;
- soit par les organismes qui, à la date du 22 juillet 2009, étaient titulaires de l'agrément requis pour la délivrance des certificats de visite des meublés de tourisme.

*Pour aller plus loin* : article L. 324-1 du Code du tourisme.

#### Délais

L'organisme qui a effectué la visite de classement dispose d'un délai d'un mois à compter de la date à laquelle s'est achevée la visite du meublé pour remettre au loueur du meublé ou à son mandataire le certificat de visite, qui comprend :

- un rapport de contrôle attestant la conformité au tableau de classement dans la catégorie demandée ;
- la grille de contrôle renseignée par l'organisme évaluateur ; 
- une proposition de décision de classement pour la catégorie indiquée dans le rapport de contrôle.

Le loueur du meublé ou son mandataire dispose d'un délai de quinze jours à compter de la réception de ce certificat de visite pour refuser la proposition de classement.  À l'expiration de ce délai et en l'absence de refus, le classement est acquis.  Le classement est prononcé pour une durée de cinq ans. 

#### Pièces justificatives

Cerfa 11819*03

*Pour aller plus loin* : article D. 324-4 du Code de tourisme.

#### Le cas échéant, obtenir un label (Gîte de France, Clévacances, Fleur de soleil, etc.)

Toute personne, disposant d’un espace en zone rurale, peut créer un hébergement et recevoir le label Gîtes de France si le logement présente les critères requis pour le classement en épis.

Pour plus d’informations, il est recommandé de consulter le [site des Gîtes de France](https://www.gites-de-france.com/obtenir-le-label.html).