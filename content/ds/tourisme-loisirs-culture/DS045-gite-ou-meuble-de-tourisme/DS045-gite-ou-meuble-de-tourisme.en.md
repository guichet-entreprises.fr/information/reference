﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS045" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Gîte or furnished tourist accommodation " -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="gite-or-furnished-tourist-accommodation" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/gite-or-furnished-tourist-accommodation.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="gite-or-furnished-tourist-accommodation" -->
<!-- var(translation)="Auto" -->




Gîte or furnished tourist accommodation 
==========================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The cottages are defined as tourist furnished. These are villas, apartments, or furnished studios, for the exclusive use of the tenant, offered for rent to a passing clientele who makes a stay characterized by a rental by day, week or month, and which does not elect residence there.

The tenancy must be concluded for a maximum and non-renewable duration of 90 consecutive days to the same person.

*For further information*: I of Article L. 324-1-1 of the Tourism Code; Article 1-1 of the Act of January 2, 1970 regulating the conditions of activities relating to certain transactions involving real estate and commercial funds.

**Good to know**

When tourist accommodations are located in rural areas, the term "rural cottage" is used.

### b. Competent Business Formalities Centre (CFE)

The relevant Business Formality Centre (CFE) depends on the nature of the activity:

- If the activity is accompanied by complementary services of hotel service (for example, cleaning of the cottage) or leisure (for example, the rental of ATVs), it is a commercial activity. The relevant CFE is therefore the Chamber of Commerce and Industry;
- If the rental of a cottage is linked to a farm or if it constitutes an extension of it, the activity must be declared with the chamber of agriculture;
- If the rental of a cottage is an extension of a farm but is not managed by the farmer, the competent CFE is the registry of the commercial court;
- If the activity is neither agricultural nor commercial, the competent CFE is the registry of the Commercial Court.

**Please note**

This is the registry of the district court in the departments of Lower Rhine, Upper Rhine and Moselle.

*For further information*: Article R. 123-3 of the Trade Code and Section L. 311-2-1 of the Rural Code and Marine Fisheries.

2°. Installation conditions
------------------------------------

### a. Condition of nationality

There is no nationality requirement to open a cottage.

However, once the renter serves alcoholic beverages, he must justify that he is either:

- French;
- a national of a European Union (EU) state or a state party to the European Economic Area (EEA) agreement;
- or a national of a state that has entered into a reciprocity treaty with France, such as Algeria or Canada, for example.

For more information, please refer to the "Beverage Flow" card.

*For further information*: Article L. 3332-3 of the Public Health Code.

### b. Conditions of honour and incompatibility related to the operation of the beverage flow

There is no condition of honorability or incompatibility to open a cottage.

On the other hand, as long as the restaurateur serves alcoholic beverages, he must respect the conditions of honourability and the incompatibilities relating to the profession of drinking. For more information, please refer to the "Beverage Flow" listing.

### c. If necessary, obtain an operating licence and licence for the operation of the beverage outlet

When the owner wishes to sell alcohol in his cottage, he must obtain a licence and a licence.

For more information, please refer to the "Beverage Flow" listing.

### d. Some peculiarities of the regulation of the activity

#### Declaring on honour its obligations

Any person who engages in or lends his assistance for a fee or free of charge, through an activity of intermediaries or negotiations or by the provision of a digital platform, to the rental of a furnished tourist subject to Article L. 324-1-1 Of the Tourism Code and Articles L. 631-7 and the following of the Building and Housing Code informs the renter of the prior reporting or authorisation obligations provided by these articles and obtains from him, prior to publication or implementation. online rental announcement, a declaration of honour attesting to compliance with these obligations, indicating whether or not the dwelling constitutes its primary residence, as well as, if applicable, the declaration number, obtained under the III of Article L. 324-1-1 of the Tourism Code. It publishes, in any advertisement relating to this furnished, its declaration number.

In the municipalities that have implemented the registration procedure mentioned in Article L. 324-1-1, the municipality may, until 31 December of the year following the one in which a tourist accommodation was rented, ask an intermediary, to pass on to him the number of days during which this tourist furnished was rented through him. It transmits this information within one month, recalling the address of the furnished and its declaration number. The municipality can request an individualised count for a list of tourist furnished within a given perimeter. In these same municipalities, the intermediary no longer offers a tourist accommodation declared as the main residence of the renter when it is aware, especially when it provides a digital platform to give it the knowledge or control of the stored data, that this furnished was rented, through it, more than one hundred 20 days in a single calendar year. It fulfills its obligations on the basis of the statement on honour mentioned to the same I. The offer withdrawal device can be pooled by several people mentioned in the same I. If necessary, this pooled scheme is certified every year before 31 December by an independent third party.

*For further information*: Article L. 324-2-1 of the Tourism Code.

#### View furnished ranking

The renter of the furnished or his agent can report the classification of his furnished (cf. "d. If necessary, ask for the classification of the furnished one" by displaying a sign. It must display, in a visible way inside the furnished, the decision to classify.

The number of stars on the sign corresponds to the number of stars awarded by the ranking decision.

*For further information*: Article D. 324-2 and following Tourism Code; Article 1 of the 22 December 2010 decree on the signs of tourist accommodation.

#### Provide a seasonal tenancy agreement with a description of the premises rented to the tenant 

Any offer or seasonal rental agreement must take written form and contain the indication of the asking price as well as a descriptive statement of the premises.

Good to know: will be punished with a fine of 3,750 euros, any person who, on the occasion of a seasonal rental or a seasonal rental offer of a furnished space, for the purpose of habitation, will have provided clearly inaccurate information about the location of the building, consistency and condition of the premises, comfort elements or furnishings. In the event of a repeat offence, the fine may be increased to 7,500 euros.

*For further information*: Article L. 324-2 of the Tourism Code; Decree 67-128 of February 14, 1967, repressing the production of inaccurate information in the event of a furnished offer or seasonal rental contract.

#### Comply with public institutions (ERP) regulations 

The renter must then comply with the safety and accessibility rules for ERPs when the maximum capacity of accommodation is more than 15 people. For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

#### Deliver billing to the customer 

The renter must give the customer a note when the price of the service is greater than or equal to 25 euros (including VAT).

For services priced below 25 euros (including VAT), the issuance of a note is optional, but it must be given to the customer if requested.

The note must mention:

- The date the note was written
- The provider's name and address
- The name of the client, unless the customer is opposed;
- The date and place of execution of the service.

*For further information*: decree of October 3, 1983 relating to the advertising of prices for all services.

#### Pay the residence tax 

Municipalities can ask holidaymakers staying in their territory to pay a residence tax. This tax allows municipalities to finance expenses related to tourist visits or the protection of their natural spaces.

The residence tax applies to adults who are not resident in the commune and who do not have a residence there for which they are liable for the residential tax.

The rate of the residence tax is set, for each nature and for each category of accommodation, per person and per night of stay.

This rate is decided by deliberation of the city council taken before October 1 of the year to be applicable the following year. Deliberation, if any, sets the start and end dates of the collection periods within the year.

**Please note**

As of January 1, 2019, professionals who, electronically, provide a booking or rental or connection service for the rental of accommodations and who are payment intermediaries (first of all the platforms digital) are obliged to collect the residence tax on behalf of non-professional renters and to pay it to the municipality.

*For further information*: Articles L. 2333-26 and following from the General Code of Local Authorities.

#### Complete a policy sheet for foreign customers 

For the purposes of preventing disturbances to public order, judicial investigations and research in the interests of individuals, renters of furnished tourists are required to fill out, or have it filled out, and signed by the foreigner, upon arrival, an individual card police.

The personal data collected in this way includes:

- names and surnames;
- Date and place of birth
- Nationality
- The usual home of the foreigner;
- The mobile phone number and email address of the foreigner;
- The date of arrival at the establishment and the expected departure date.

Children under the age of 15 may be listed on an adult's card.

The records thus established must be kept for a period of six months and handed over, at their request, to the police and gendarmerie units. This transmission can be done in a dematerialized form.

Individuals or corporations renting bare premises are not subject to these obligations.

*For further information*: order of 1 October 2015 under Article R. 611-42 of the Code of Entry and Residence of Foreigners and the Right of Asylum.

#### Advertising of the cottage 

In order to make his cottage known, the renter must comply with the requirements for the installation and maintenance of advertising signs to ensure the protection of the living environment.

*For further information*: Articles L. 581-1 and following articles R. 581-1 and following of the Environment Code.

#### If necessary, comply with pool regulations 

##### Declare the installation of a swimming pool

Anyone who installs a swimming pool must make the declaration to the town hall of the site of its installation before opening.

This statement, along with a supporting record, includes a commitment that the installation of the pool will meet the hygiene and safety standards set out in Articles D. 1332-1 and following of the Public Health Code.

*For further information*: Articles L. 1332-1 of the Public Health Code.

##### Pool maintenance

The person in charge of a swimming pool is required to monitor water quality and inform the public about the results of that monitoring, to submit to a health check, to comply with the rules and quality limits set by decree, and to use only effective water treatment, cleaning and disinfection products and processes that do not pose a health hazard to bathers and pool maintenance and bathing staff. Artificial.

For more information, please refer to the "Operator pool, swimming spot" sheet.

*For further information*: Articles L. 1332-1 and following, D. 1332-1 and beyond the Public Health Code; decree of 7 April 1981 setting out the technical provisions applicable to swimming pools.

Keeping the pool safe

The pool must be equipped with a safety device to prevent the risk of drowning.

The device consists of:

- A protective barrier
- A blanket
- Shelter
- an alarm.

Failure to comply with pool safety provisions is punishable by a fine of 45,000 euros.

*For further information*: Articles L. 128-1 to L. 128-3, R. 128-2 and L. 152-12 of the Building and Housing Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

A renter, whether individual or professional, must complete a declaration of start of activity (Cerfa No. 11921Also called form POi). If the activity is commercial, the renter must declare the opening of the deposit to be registered in the Register of Trade and Companies (RCS). For more information, it is advisable to refer to the formality sheet "declaration commercial activity".

### b. Declare tourist furnished rental

The person who offers a classified or unclassified tourist accommodation must make a declaration in the town hall by filling out the Form Cerfa No. 14004*04.

Where the residential space is the landlord's main residence, it is exempt from pre-reporting. However, in the cities that have the registration procedure in place, all tourist rentals, whether the main or secondary residence, will have to have a registration number to publish in each rental ad. . Intermediate rental platforms (Airbnb, Abritel, etc.) have an obligation to disconnect each ad that does not contain a registration number.

In these municipalities, any person who offers a tourist accommodation that is declared as his main residence cannot do so beyond a hundred and twenty days in the same calendar year, except professional obligation, health reason or case of force Major.

**Good to know**

Failure to comply with this reporting obligation is punishable by a fine of up to 450 euros.

*For further information*: Articles L. 324-1-1, D. 324-1-1 and R. 324-1-2 of the Tourism Code; Article 131-13 of the Penal Code.

The main residence is understood to be the dwelling occupied at least eight months a year.

Any changes to the information contained in the declaration are the subject of a new declaration in the town hall.

**Competent authority**

The mayor of the commune where the furnished is located.

**Exhibits: Cerfa 14004*04**

The declaration specifies the identity and address of the registrant, the address of the tourist accommodation, the number of rooms in the furnished area, the number of beds, the or the forecast rental periods and, if applicable, the date of the classification decision and the level of classification of tourist furnished.

*For further information*: Articles L. 324-1-1 and D. 324-1-1 of the Tourism Code; Article 2 of the Act of 6 July 1989 to improve rental relations and amending Act 86-1290 of December 23, 1986.

### c. If necessary, obtain permission to change the use of the dwelling

In some cities, it is mandatory to obtain an authorization from the town hall in order to be able to change the use of the accommodation in tourist furnished (passage of the main dwelling in tourist furnished).

**Please note**

A deliberation by the City Council may define a temporary change-of-use authorization scheme allowing a natural person to rent for short periods of space for residential accommodation to a passing clientele who does not elect a home there. The premises for residential use with this temporary authorization do not change their destination.

The fact that a furnished space is repeatedly used for short periods of time to a passing clientele who does not choose a home is a change of use.

This authorization requirement relates to:

- municipalities with more than 200,000 inhabitants;
- the municipalities of the hauts-de-Seine, Seine-Saint-Denis and Val-de-Marne departments, regardless of their size.

On an administrative decision, this obligation may apply to other municipalities.

*For further information*: Articles L. 631-7 to L. 631-9 of the Building and Housing Code.

### d. If necessary, ask for the classification of the furnished

The renter of the furnished or his agent who wishes to obtain the classification transmits a classification application in furnished tourism to an organization of his choice. The ranking will indicate to the customer a level of comfort and service.

The decision to classify a tourist furnished in a category, is pronounced by the organization that made the ranking visit.

*For further information*: Article D. 324-3 of the Tourism Code.

**Competent authority**

This ranking visit is done:

- either by accrediting bodies accredited by any equivalent European body that is a signatory to the multilateral agreement taken as part of the European coordination of accreditation bodies;
- or by organizations that, as of July 22, 2009, held the required accreditation for the issuance of tourist tour certificates.

*For further information*: Article L. 324-1 of the Tourism Code.

**Timeframe**

The organization that made the filing visit has one month from the date on which the visit of the furnished apartment ended to give the renter of the furnished or his agent the certificate of visitation, which includes:

- A control report attesting to compliance with the classification table in the requested category;
- The control grid provided by the evaluator
- a proposal for a classification decision for the category indicated in the control report.

The renter of the furnished or his agent has a period of fifteen days from the receipt of this certificate of visit to refuse the proposal of classification.  At the end of this period and in the absence of refusal, the classification is acquired.  The classification is pronounced for a period of five years.

**Supporting documents**

Cerfa 11819*03

*For further information*: Article D. 324-4 of the Tourism Code.

#### If necessary, obtain a label (Gîte de France, Clévacances, Fleur de soleil, etc.)

Anyone with a rural space can create accommodation and receive the Gîtes de France label if the accommodation has the required criteria for the list of cobs.

For more information, it is recommended that you consult[site of the Gîtes de France](https://www.gites-de-france.com/obtenir-le-label.html).

