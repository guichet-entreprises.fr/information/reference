﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS055" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Physical and sports activities centre operated for commercial purposes" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="physical-and-sports-activities-centre-operated-for-commercial-purposes" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/physical-and-sports-activities-centre-operated-for-commercial-purposes.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="physical-and-sports-activities-centre-operated-for-commercial-purposes" -->
<!-- var(translation)="Auto" -->


Physical and sports activities centre operated for commercial purposes
============================================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

A physical and sports activity facility (EAPS) is defined by the nature of its activity, which consists of organizing the practice of various sports activities. An EAPS can be identified as long as it includes fixed or mobile equipment, proposes the practice of one or more activities over a certain period of time (continuous or seasonal) and defines the modalities of its exercise.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out. As the activity is commercial in nature, the relevant CFE is the Chamber of Commerce and Industry (CCI).

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional engages in a buy-and-sell activity, his activity will be both commercial and artisanal.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To operate an EAPS, no special qualifications are required. On the other hand, if the professional engages in an animation, teaching, training or coaching activity, he will have to justify a diploma to carry out this activity.

For more information, please refer to the listing[Sports educator](https://www.guichet-entreprises.fr/fr/activites-reglementees/enseignement/educateur-sportif/) and "Sports Facilitator" cards on the activities involved.

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There is no provision for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement wishing to operate an EAPS in France on a temporary and casual basis as part of a (LPS) or permanent provision of services as part of a Freedom of establishment.

As such, the person concerned is subject to the same requirements as the French national (see "3o. Conditions of installation").

### c. Conditions of honorability

No one may operate an EAPS if it is or has been convicted of any crime or offence:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substances or plants classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 232-25 to L. 232-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups or if it has been the subject of an administrative measure to suspend these same functions.

In addition, if the professional breaches these obligations, he or she faces a one-year prison sentence and a fine of 15,000 euros.

*For further information*: Articles L. 322-1, L. 212-9 and A. 322-1 of the Code of Sport.

### d. Some peculiarities of the regulation of the activity

**Insurance obligation**

The operator of an EAPS is required to take out liability insurance to cover the risks associated with the exercise of his business. This insurance must also cover all teachers, persons admitted to the school on a regular or casual basis, and all operator's attendants.

The professional who has taken out such insurance receives a certificate stating:

- Reference texts (legal and regulatory);
- The name of the insurance company
- The number of insurance policies underwritten
- The validity period of the contract
- The policyholder's name and address
- The extent of the amount of guarantees.

**Please note**

The fact that the professional does not take out such insurance is punishable by six months' imprisonment and a fine of 7,500 euros.

*For further information*: Articles L. 321-7, D. 321-1 and D. 321-4 of the Code of Sport.

**Health and safety guarantees**

All EAPS must contain on its premises:

- an emergency kit containing all the products and devices to provide first aid and alert emergency services in the event of an accident;
- an emergency organization table listing the addresses and phone numbers of emergency responders.

In addition, the facility operator must display a copy within the premises and in a visible manner:

- all diplomas or training titles for teachers, animators or sports educators working within the EAPS, as well as their professional card or, if necessary, their trainee certificate;
- guarantees of hygiene and safety and technical standards applicable to the supervision of physical and sports activities;
- certification of the insurance contract.

*For further information*: Articles L. 322-1 and R. 322-4 and the following articles from the Code of Sport.

**Please note**

In the event of a breach of these insurance obligations or health and safety guarantees, the operator incurs a temporary or permanent closure of his establishment.

**Information requirement**

**To the prefect**

The operator of an EAPS is required to inform the prefect if a serious accident or situation arises that could pose a serious risk to practitioners.

*For further information*: Article R. 322-6 of the Code of Sport.

**Towards physical and sporting participants**

The operator of an EAPS must inform future practitioners of the necessary abilities required to perform an activity before the activity begins.

*For further information*: Article A. 322-3 of the Code of Sport.

**Please note**

Some requirements may also be required for certain activities due to their nature (nautical activity, equine-related activity, free fall activity, etc.). For more information, please refer to the fact sheet on the activity concerned.

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

It is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, it is recommended to refer to the "Declaration of a Commercial Company" and "Registration of an Individual Company in the Register of Trade and Companies."

### b. If necessary, make a statement as a sports educator

As long as the facility includes sports educators, the operator must ensure that they have made a prior declaration in order to carry out their activity and that they hold a professional card.

In addition, professionals must renew their returns every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or to the Departmental Directorate of Social Cohesion and Population Protection (DDCSPP) of the Main Practice or Exercise Department, or directly in line on the[official website of the Ministry of Sport](https://eaps.sports.gouv.fr.).

**Timeframe**

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

**Supporting documents**

The professional's request must contain the following documents:

- Cerfa declaration form 12699Completed and signed;
- A copy of a valid ID
- A photo ID
- A statement of honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles and certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned dating back less than one year.

If the return is renewed, he must attach:

- Form Cerfa No. 12699Completed and signed;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

The prefecture will itself request the communication of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

**Cost**

Free

*For further information*: Articles L. 212-11 and R. 212-85, and Articles A. 212-176 to A. 212-178 of the Code of Sport.

### c. Post-registration authorisation

**Sports equipment census statement**

The operator of an EAPS that owns sports equipment is required to make a declaration to the prefect of the department in which he wishes to practice within three months of the start of his activity.

All goods used or developed for physical and sporting activity are considered sports equipment.

This statement should identify:

- Sports equipment, its characteristics, its owner and, if necessary, its operator;
- If so, the nature of the changes made
- the transferee of the equipment in the event of a transfer.

**Please note**

If this reporting obligation is breached, the operator is fined 150 euros.

*For further information*: Article L. 312-2, and Articles R. 312-2 to R. 312-7 of the Code of Sport.

