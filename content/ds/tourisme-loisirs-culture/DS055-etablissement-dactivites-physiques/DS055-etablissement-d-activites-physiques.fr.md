﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS055" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Etablissement d’activités physiques et sportives (EAPS) à vocation commerciale" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="etablissement-d-activites-physiques" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/etablissement-d-activites-physiques-et-sportives-eaps-a-vocation.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="etablissement-d-activites-physiques-et-sportives-eaps-a-vocation" -->
<!-- var(translation)="None" -->

# Etablissement d’activités physiques et sportives (EAPS) à vocation commerciale

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Un établissement d'activités physiques et sportives (EAPS) se définit par la nature de son activité qui consiste à organiser la pratique de diverses activités sportives. Un EAPS peut être identifié dès lors qu'il comprend des équipements fixes ou mobiles, qu'il propose la pratique d'une ou plusieurs activités sur une certaine durée (continue ou saisonnière) et en définit les modalités d'exercice.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée. L'activité étant de nature commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exerce une activité d'achat-revente, son activité sera à la fois commerciale et artisanale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exploiter un EAPS, aucune qualification particulière n'est requise. En revanche, dès lors que le professionnel exerce une activité d'animation, d'enseignement, d'entraînement ou d'encadrement des activités physiques, il devra justifier d'un diplôme permettant d'exercer cette activité.

Pour plus de renseignements, il est conseillé de se reporter à la fiche « [Éducateur sportif](https://www.guichet-entreprises.fr/fr/ds/enseignement/educateur-sportif.html) » et aux fiches « Animateur sportif » relatives aux activités concernées.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services (LPS) ou Libre Établissement (LE))

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'espace économique européen (EEE) souhaitant exploiter un EAPS en France à titre temporaire et occasionnel dans le cadre d'une libre prestation de services (LPS) ou permanent dans le cadre d'un libre établissement (LE).

À ce titre, l'intéressé est soumis aux mêmes exigences que le ressortissant français (cf. « 3°. Conditions d'installation »).

### c. Conditions d’honorabilité

Nul ne peut exploiter un EAPS s'il fait ou a fait l'objet d’une condamnation pour tout crime ou pour l’un des délits suivants :

- torture et actes de barbarie ;
- agressions sexuelles ;
- trafic de stupéfiants ;
- mise en danger d’autrui ;
- proxénétisme et infractions qui en résultent ;
- mise en péril des mineurs ;
- usage illicite de substances ou plantes classées comme stupéfiants ou provocation à l’usage illicite de stupéfiants ;
- infractions prévues aux articles L. 232-25 à L. 232-28 du Code du sport ;
- à titre de peine complémentaire à une infraction en matière fiscale : condamnation à une interdiction temporaire d’exercer, directement ou par personne interposée, pour son compte ou le compte d’autrui, toute profession industrielle, commerciale ou libérale (article 1750 du Code général des impôts).

De plus, nul ne peut enseigner, animer ou encadrer une activité physique ou sportive auprès de mineurs s’il a fait l’objet d’une mesure administrative d’interdiction de participer, à quel titre que ce soit, à la direction et à l’encadrement d’institutions et d’organismes soumis aux dispositions législatives ou réglementaires relatives à la protection des mineurs accueillis en centre de vacances et de loisirs, ainsi que de groupements de jeunesse ou s’il a fait l’objet d’une mesure administrative de suspension de ces mêmes fonctions.

En outre, en cas de manquement à ces obligations, le professionnel encourt une peine d'un an d'emprisonnement et de 15 000 euros d'amende.

*Pour aller plus loin* : articles L. 322-1, L. 212-9 et A. 322-1 du Code du sport.

### d. Quelques particularités de la réglementation de l’activité

#### Obligation d'assurance

L'exploitant d'un EAPS est tenu de souscrire une assurance de responsabilité civile le couvrant contre les risques liés à l'exercice de son activité. Cette assurance doit également permettre de couvrir l'ensemble des enseignants, des personnes admises dans l'établissement à titre habituel ou occasionnel, et de tous les préposés de l'exploitant.

Le professionnel ayant souscrit une telle assurance reçoit une attestation mentionnant :

- les textes de référence (légale et réglementaire) ;
- la raison sociale de l'entreprise d'assurance ;
- le numéro de contrat d'assurance souscrit ;
- la période de validité du contrat ;
- le nom et l'adresse du souscripteur ;
- l'étendue du montant des garanties.

**À noter**

Le fait pour le professionnel de ne pas souscrire une telle assurance est puni de six mois d'emprisonnement et de 7 500 euros d'amende.

*Pour aller plus loin* : articles L. 321-7, D. 321-1 et D. 321-4 du Code du sport.

#### Garanties d'hygiène et de sécurité

Tout EAPS doit contenir dans ses locaux :

- une trousse de secours contenant l'ensemble des produits et dispositifs permettant de prodiguer les premiers soins et d'alerter les services de secours en cas d'accident ;
- un tableau d'organisation des secours mentionnant les adresses et numéros de téléphone des intervenants en cas d'urgence.

En outre, l'exploitant de l'établissement doit afficher au sein des locaux et de manière visible, une copie :

- de l'ensemble des diplômes ou titres de formation des enseignants, animateurs ou éducateurs sportifs exerçant au sein de l'EAPS ainsi que leur carte professionnelle ou, le cas échéant, leur attestation de stagiaire ;
- des garanties d'hygiène et de sécurité et des normes techniques applicables à l'encadrement des activités physiques et sportives ;
- de l'attestation du contrat d'assurance.

*Pour aller plus loin* : articles L. 322-1 et R. 322-4 et suivants du Code du sport.

**À noter**

En cas de manquement à ces obligations en matière d'assurance ou de garanties d'hygiène et de sécurité, l'exploitant encourt une fermeture temporaire ou définitive de son établissement.

#### Obligation d'informations

##### Vis-à-vis du préfet

L'exploitant d'un EAPS est tenu d'informer le préfet dès lors que survient un accident grave ou une situation susceptible de présenter des risques graves pour les pratiquants.

*Pour aller plus loin* : article R. 322-6 du Code du sport.

##### Vis-à-vis des pratiquants des activités physiques et sportives

L'exploitant d'un EAPS doit informer les futurs pratiquants des capacités nécessaires requises pour pratiquer une activité, et ce, avant le début de celle-ci.

*Pour aller plus loin* : article A. 322-3 du Code du sport.

**À noter**

Certaines exigences peuvent également être requises pour certaines activités en raison de leur nature (activité nautique, activité en lien avec des équidés, activité de chute libre etc.). Pour plus de renseignements, il convient de se reporter à la fiche relative à l'activité concernée.

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Le cas échéant, effectuer une déclaration d'éducateur sportif

Dès lors que l'établissement comprend des éducateurs sportifs, l'exploitant doit veiller à ce que ces derniers aient effectué une déclaration préalable en vue d'exercer leur activité et qu'ils soient titulaires d'une carte professionnelle.

En outre, les professionnels doivent procéder au renouvellement de leur déclaration tous les cinq ans.

#### Autorité compétente

La déclaration doit être adressée à la direction départementale de la cohésion sociale (DDCS) ou à la direction départementale de la cohésion sociale et de la protection des populations (DDCSPP) du département d’exercice ou d'exercice principal, ou directement en ligne sur le [site officiel du ministère chargé des sports](https://eaps.sports.gouv.fr.).

#### Délai

Dans le mois suivant le dépôt du dossier de déclaration, la préfecture envoie un accusé de réception au déclarant. La carte professionnelle, valable cinq ans, est ensuite adressée au déclarant.

#### Pièces justificatives

La demande du professionnel doit contenir les documents suivants :

- formulaire de déclaration Cerfa n° 12699*02 complété et signé ;
- une copie d’une pièce d’identité en cours de validité ;
- une photo d’identité ;
- une déclaration sur l’honneur attestant l’exactitude des informations figurant dans le formulaire ;
- une copie de chacun des diplômes, titres et certificats invoqués ;
- une copie de l’autorisation d’exercice, ou, le cas échéant, de l’équivalence de diplôme ;
- un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées datant de moins d’un an.

En cas de renouvellement de la déclaration, il doit joindre :

- le formulaire Cerfa n° 12699*02 complété et signé ;
- une photo d’identité ;
- une copie de l’attestation de révision en cours de validité pour les qualifications soumises à l’obligation de recyclage ;
- un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées, datant de moins d’un an.

La préfecture demandera elle-même aux services compétents la communication d’un extrait de moins de trois mois du casier judiciaire du déclarant pour vérifier l’absence d’incapacité ou d’interdiction d’exercer.

#### Coût

Gratuit

*Pour aller plus loin* : articles L. 212-11 et R. 212-85, et articles A. 212-176 à A. 212-178 du Code du sport.

### c. Autorisation post-immatriculation

#### Déclaration de recensement d'équipement sportif

L'exploitant d'un EAPS propriétaire d'équipements sportifs est tenu d'effectuer, dans un délai de trois mois suivant le début de son activité, une déclaration auprès du préfet du département au sein duquel il souhaite exercer.

Sont considérés comme des équipements sportifs l'ensemble des biens utilisés ou aménagés pour la pratique d'une activité physique et sportive.

Cette déclaration doit permettre d'identifier :

- l'équipement sportif, ses caractéristiques, son propriétaire et, le cas échéant, son exploitant ;
- le cas échéant, la nature des modifications apportées ;
- le cessionnaire de l'équipement en cas de cession.

**À noter**

En cas de manquement à cette obligation de déclaration, l'exploitant encourt une amende de 150 euros.

*Pour aller plus loin* : article L. 312-2, et articles R. 312-2 à R. 312-7 du Code du sport.