﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS062" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Artisan" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="artisan" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/artisan.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="artisan" -->
<!-- var(translation)="Auto" -->

Artisan
================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

The craftsman is a professional engaged in one of the craft activities listed in Annex of the decree of 24 December 2015 setting out the list of crafts, in accord with Article 20 of Law 96-603 of 5 July 1996 relating to the development and promotion of trade and crafts.

### b. Competent Business Formality Centre

The relevant Business Formalities Centre (CFE) depends on the nature of the structure in which the activity is carried out:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI).

If the professional has a buying and resale activity, his activity is both artisanal and commercial.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Professional qualifications
----------------------------------------

### a. Professional qualifications

**Craftsmanship**

The title of craftsman is reserved for those engaged in one of the activities cited in the december 24, 2015 decree, which:

- either hold a Certificate of Professional Qualification (CAP) or a Professional Studies Certificate (BEP);
- justify at least three years of professional experience in this activity.

*For further information*: decree of 24 December 2015 setting out the list of crafts, under Article 20 of Law 96-603 of 5 July 1996 relating to the development and promotion of trade and crafts;

**Title of master craftsman in craft**

The title of master craftsman is awarded to individuals, including the social leaders of legal entities:

- Registered in the trades directory;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

**Please note**

Individuals who do not hold the master's degree can apply to the Regional Qualifications Commission for the title of Master Craftsman under two assumptions:

- when they are registered in the trades directory, have a degree at least equivalent to the master's degree, and justify management and psycho-pedagogical knowledge equivalent to those of the corresponding value units of the master's degree and that they have two years of professional practice;
- when they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

The national of a Member State of the European Union (EU) or party to the agreement on the European Economic Area (EEA) can settle in France to carry out a permanent artisanal activity, therefore:

- He has a certificate of competency or a training certificate;
- that it justifies the exercise of the activity for at least one year in the last ten years as well as a certificate of competency or training certificate that prepared him to practice the profession when his practice is not regulated in that State.

The national will therefore have to apply to the president of the Chamber of Crafts and Crafts for the title of craftsman (see infra "3o). c. If necessary, apply for the status of craftsman for the EU or EEA national.

*For further information*: Article 5 of Decree 98-246 of 2 April 1998.

### c. Conditions of honorability and incompatibility

No one may engage in an artisanal activity of art if it is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 of Act 96-603 of July 5, 1996.

### d. Some peculiarities of the regulation of the activity

#### Compliance with safety and accessibility standards

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

It is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

*For further information*: order of 25 June 1980 approving the general provisions of the fire and panic safety regulations in the ERPs.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Artisanal Company Reporting Formalities," "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Follow the installation preparation course (SPI)

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including a degree in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who:

- have either worked for at least three years requiring a level of knowledge equivalent to that provided by the internship;
- either have knowledge acquired in an EU or EEA state or a third country during a professional experience that can fully or partially cover the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of their professional qualifications shows substantial differences with those required in France to run a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree 83-517 of June 24, 1983.

### c. If necessary, apply for the right to be an artisan for the EU or EEA national

**Competent authority**

The President of the Chamber of Trades and Crafts is competent to decide on the application for the award of the artistic craftsman's status of the national of an EU or EEA state.

**Supporting documents**

The application for attribution must include the following supporting documents:

- the applicant's marital status (name, first name, address, nationality);
- A copy of the national's ID
- Certificate of competency or diploma, title or certificate justifying the applicant's professional qualification.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

Upon receipt of the coins, the president of the Chamber of Trades and Crafts will have three months to decide and award or refuse the application. It may also ask the national to make a compensation measure if there are substantial differences between the training of the national and that required in France. He will then have the choice of taking an adjustment course or taking an aptitude test.

*For further information*: Articles 1 and 2 of the order of 28 October 2009 relating to the submission of the declaration and requests under Decree 98-246 of 2 April 1998 and Title I of Decree 98-247 of 2 April 1998; Article 5 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.