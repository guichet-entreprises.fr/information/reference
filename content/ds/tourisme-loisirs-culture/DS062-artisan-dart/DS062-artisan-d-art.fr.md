﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS062" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Artisan d'art" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="artisan-d-art" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/artisan-d-art.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="artisan-d-art" -->
<!-- var(translation)="None" -->

# Artisan d'art

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l'activité

### a. Définition

L'artisan d'art est un professionnel exerçant l'une des activités artisanales figurant en annexe de l'arrêté du 24 décembre 2015 fixant la liste des métiers d'art, en application de l'article 20 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat.

### b. Centre de formalités des entreprises compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour une activité commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

Si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Qualifications professionnelles

### a. Qualifications professionnelles

#### Qualité d'artisan d'art

Le titre d'artisan d'art est réservé aux personnes exerçant l'une des activités citées à l'arrêté du 24 décembre 2015 et qui :

- sont soit titulaires d'un certificat d'aptitude professionnelle (CAP) ou d'un brevet d'études professionnelles (BEP) ;
- soit justifient d'une expérience professionnelle de trois ans au moins dans cette activité.

*Pour aller plus loin* : arrêté du 24 décembre 2015 fixant la liste des métiers d'art, en application de l'article 20 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat; 

#### Titre de maître artisan en métier d'art

Le titre de maître artisan est attribué aux personnes physiques, y compris les dirigeants sociaux des personnes morales :

- immatriculées au répertoire des métiers ;
- titulaires du brevet de maîtrise dans le métier exercé ;
- justifiant d’au moins deux ans de pratique professionnelle.

**À noter**

Les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan à la commission régionale des qualifications dans deux hypothèses :

- lorsqu’elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de niveau de formation au moins équivalent au brevet de maîtrise, qu’elles justifient de connaissances en gestion et en psychopédagogie équivalentes à celles des unités de valeur correspondantes du brevet de maîtrise et qu’elles ont deux ans de pratique professionnelle ;
- lorsqu’elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles disposent d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

*Pour aller plus loin* : article 3 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Service ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) peut s'établir en France pour y exercer une activité artisanale de façon permanente, dès lors :

- qu'il est titulaire d'une attestation de compétences ou d'un titre de formation ;
- qu'il justifie de l'exercice de l'activité pendant au moins une année au cours des dix dernières années ainsi que d'une attestation de compétences ou d'un titre de formation l'ayant préparé à exercer la profession lorsque son exercice n'est pas réglementé dans cet État.

Le ressortissant devra, dès lors, solliciter du président de la chambre des métiers et de l'artisanat une demande d'attribution du titre d'artisan d'art (cf. infra « 3°. c. Le cas échéant, solliciter une demande d'attribution de la qualité d'artisan d'art pour le ressortissant de l'UE ou de l'EEE »).

*Pour aller plus loin* : article 5 du décret n° 98-246 du 2 avril 1998.

### c. Conditions d'honorabilité et incompatibilités

Nul ne peut exercer une activité artisanale d'art s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d’une peine d’interdiction d’exercer une activité professionnelle ou sociale pour l’un des crimes ou délits prévue au 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 de la loi n° 96-603 du 5 juillet 1996.

### d. Quelques particularités de la réglementation de l'activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les ERP.

## 3°. Démarches et formalités d'installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Le cas échéant, solliciter une demande d'attribution de la qualité d'artisan d'art pour le ressortissant de l'UE ou de l'EEE

#### Autorité compétente

Le président de la chambre des métiers et de l'artisanat est compétent pour se prononcer sur la demande d'attribution de la qualité d'artisan d'art du ressortissant d'un État de l'UE ou de l'EEE.

#### Pièces justificatives

La demande d'attribution doit comporter les pièces justificatives suivantes :

- l'état civil du demandeur (nom, prénom(s), adresse, nationalité) ;
- une copie de la pièce d'identité du ressortissant ;
- l'attestation de compétences ou le diplôme, titre ou certificat justifiant la qualification professionnelle du demandeur.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

À réception des pièces, le président de la chambre des métiers et de l'artisanat aura trois mois pour se prononcer et attribuer ou refuser la demande. Il pourra également demander au ressortissant d'effectuer une mesure de compensation dès lors qu'il existe des différences substantielles entre la formation du ressortissant et celle requise en France. Il aura alors le choix de suivre un stage d'adaptation ou de passer une épreuve d'aptitude.

*Pour aller plus loin* : articles 1 et 2 de l'arrêté du 28 octobre 2009 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre Ier du décret n° 98-247 du 2 avril 1998 ; article 5 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.