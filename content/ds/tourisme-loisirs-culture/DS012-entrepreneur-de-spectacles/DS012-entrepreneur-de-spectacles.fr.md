﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS012" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Entrepreneur de spectacles" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="entrepreneur-de-spectacles" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/entrepreneur-de-spectacles.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="entrepreneur-de-spectacles" -->
<!-- var(translation)="None" -->


# Entrepreneur de spectacles

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’activité d’entrepreneur de spectacles dits vivants (spectacles comprenant la présence physique d'au moins un artiste devant un public) consiste à :

- exploiter des lieux qui servent aux représentations publiques (catégorie 1) ;
- produire des spectacles ou des tournées, et avoir la responsabilité du plateau artistique (catégorie 2) ;
- diffuser des spectacles ou des tournées, sans être responsable du plateau artistique (catégorie 3).

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une profession artisanale (en cas de spectacle de marionnettes), le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d'industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’article L. 110-1 du Code du commerce dispose que l’activité d’entrepreneur de spectacles vivants est présumée acte de commerce. Cependant l’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Pour les entrepreneurs établis en France

Pour être titulaire d’un récépissé de déclaration valant licence permettant d'exercer l'activité d'entrepreneur de spectacles (cf. infra « 3°. b. Demander une licence d'entrepreneur de spectacles vivants »), l'intéressé (personne physique particulier employeur, ou ayant une entreprise en son nom) doit :

- justifier de compétence ou d’expérience professionnelles :
  - soit en étant titulaire d'un diplôme de l'enseignement supérieur ou d’un titre de même niveau inscrit au [Répertoire national des certifications professionnelles](https://certificationprofessionnelle.fr/),
  - soit en justifiant d'une expérience professionnelle de six mois au moins,
  - soit en ayant suivi une formation professionnelle de 125 heures au moins dans le domaine du spectacle ou justifier d’un ensemble de compétences, figurant dans un répertoire établi par la commission paritaire emploi-formation du spectacle vivant au plus tard le 1er octobre 2020 ;
- jouir de la capacité juridique d'exercer une activité commerciale et être immatriculé au registre du commerce et des sociétés ou au répertoire des métiers lorsqu’il est soumis à cette obligation ;
- établir que les obligations en matière de sécurité des lieux de spectacles sont respectées (pour les entrepreneurs exploitants de lieux de spectacles – catégorie 3) et avoir suivi une formation à la sécurité des spectacles figurant sur le répertoire accessible sur le [site de la Commission paritaire emploi-formation spectacle vivant](https://www.cpnefsv.org/formations-agreees/formation-securite-licence-dentrepreneurs-exploitants-lieux) ;
- être en règle en ce qui concerne les obligations de l’employeur prévue par le Code du travail, par le régime de sécurité sociale ou par les dispositions relatives à la protection de la propriété intellectuelle et artistique ;
- avoir effectué sa déclaration via le téléservice ad hoc du ministère de la Culture et respecté les délais prévus par le Code du travail en ce qui concerne la déclaration.

Pour les personnes morales, les conditions de compétence ou d’expérience doivent être remplies par une ou plusieurs personnes en responsabilité au sein de l’organisme.

*Pour aller plus loin :* articles L. 7122-4 et L. 7122-7 et R. 7122-2 et R. 7122-3 du Code du travail.

### b. Pour les ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### Pour un exercice temporaire et occasionnel (LPS)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité d'entrepreneur de spectacles peut exercer à titre temporaire et occasionnel la même activité en France.

À ce titre, le ressortissant devra, préalablement à sa première prestation, informer le préfet de région du lieu où il souhaite la réaliser (cf. infra « 3°. d. Le cas échéant, effectuer une déclaration écrite pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS) »). Cette déclaration doit être adressée via le [téléservice du ministère de la Culture](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_declaration_00/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7).

*Pour aller plus loin :* article L. 7122-6 du Code du travail.

#### Pour un exercice permanent (LE)

Tout ressortissant d'un État de l'UE ou de l'EEE peut s’établir en France pour exercer l’activité d’entrepreneur de spectacles vivants, sans déclarer son activité, sous réserve de produire un titre d’effet équivalent délivré dans un de ces États dans des conditions comparables (cf. infra « 3°. c. Le cas échéant, demander une reconnaissance d'effet équivalent de son titre pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) »).

*Pour aller plus loin :* article L. 7122-5 du Code du travail.

### c. Quelques particularités de la réglementation de l'activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin :* arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

#### Mentions obligatoires

L'entrepreneur de spectacles doit indiquer sur tous les supports faisant la promotion de son spectacle ainsi que sur la billetterie de tout spectacle le numéro de récépissé de déclaration valant licence, en cours de validité du ou des entrepreneurs de spectacles vivants qui produisent ou diffusent le spectacle

*Pour aller plus loin :* article R. 7122-12 du Code du travail.

#### Mise en place d'une billetterie 

À chaque entrée de spectacle, l'entrepreneur doit délivrer un billet à chacun des spectateurs. Pour cela, il doit mettre en place une billetterie manuelle ou informatisée et en faire la déclaration préalable auprès de la direction des services fiscaux dont il dépend.

Lorsque la billetterie est informatisée, il devra notamment lui transmettre les informations suivantes :

- le nom du logiciel, son numéro de version et, le cas échéant, sa date ainsi que l'identité de son concepteur ou le nom du progiciel ;
- la configuration informatique ;
- le système d'exploitation ;
- le langage de programmation ;
- le format du logiciel source ou exécutable fourni par le concepteur ;
- la description fonctionnelle du système ;
- le fac-similé d'un billet, d'un coupon de gestion et d'un relevé de recettes ;
- les sécurités mises en œuvre.

*Pour aller plus loin :* articles 50 sexies I et 290 quater du Code général des impôts.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur devra s’immatriculer au registre des métiers et de l’artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Déclarer une activité d'entrepreneur de spectacles vivants

L'entrepreneur de spectacles doit déclarer au moins un mois avant la première représentation son activité afin d’obtenir un récépissé valide valant licence qui lui permettra d'exercer son activité selon l'une des trois catégories visées ci-avant.

#### Autorité compétente

Le professionnel devra adresser sa demande au préfet de région du lieu dans lequel se trouve son établissement principal via le [téléservice en ligne sur le site du ministère de la Culture](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_declaration_00/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7).

#### Pièces justificatives

La demande se fait par l'[envoi d'un dossier en ligne](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_declaration_00/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7) comportant l’ensemble des informations et pièces justificatives suivantes :

- la précision sur la ou les catégories de licences demandées ;
- l’identification de l'entrepreneur personne physique ou personne morale (dénomination, forme juridique, adresse) ;
- tout document justifiant la capacité professionnelle du demandeur ou, pour les personnes morales, la présence d’une personne justifiant d’une telle capacité, qui peut être soit :
  - la copie du diplôme de l'enseignement supérieur ou d’un titre de même niveau inscrit au Répertoire national des certifications professionnelles,
  - les justificatifs d'une expérience professionnelle d'au moins six mois (contrats de travail ou autres documents officiels),
  - une attestation du suivi d'une formation professionnelle de 125 heures dans le domaine du spectacle ;
- les documents relatifs à l’identification de la personne physique ou morale et à la capacité de diriger une entreprise ou une activité commerciale :
  - le numéro d’identification d’entreprise,
  - le code de l'activité principale exercée ou envisagée et l’objet de la personne morale tel que figurant dans ses statuts,
  - le numéro d’inscription au registre du commerce et des sociétés, au répertoire des métiers (lorsqu’il est soumis à l’obligation d’immatriculation à l’un de ces registres) ou au Répertoire national des associations dans le cas des associations,
  - une attestation sur l'honneur (case à cocher) certifiant l'absence de condamnation ou de sanction interdisant l'exercice d'une activité commerciale ;
- la référence de la convention collective applicable ;
- un calendrier de la programmation envisagée et la description du projet en matière de spectacles vivants ;
- un engagement sur l’honneur (case à cocher) à s'affilier au guichet unique pour le spectacle vivant (GUSO) ou aux organismes de protection sociale du spectacle, ainsi qu'aux institutions auxquelles l'adhésion est rendue obligatoire par les conventions collectives nationales du spectacle vivant ou par accord collectif de travail ;
- pour les personnes ayant, préalablement à la déclaration, déjà exercé une activité de spectacles vivants :
  - une attestation sur l‘honneur (case à cocher) de comptes à jour des cotisations et contributions sociales (GUSO ou organismes de protection sociale du spectacle; FNAS, CASC) ou un protocole d'échelonnement et engagement à le respecter,
  - une attestation sur l'honneur (case à cocher) certifiant que l'entreprise n'a pas de dettes en ce qui concerne le paiement des droits d'auteurs, ou un protocole d’échelonnement de dettes et engagement à le respecter,
  - un calendrier des spectacles des trois années passées selon le modèle à télécharger ;
- pour les entrepreneurs de catégorie 3, la justification d‘avoir suivi une formation adaptée à la nature des lieux de spectacles (pour une personne morale justifier de la présence en son sein d'une ou plusieurs personnes physiques remplissant cette condition) ;
- pour les établissements soumis à l'obligation de contrôle de la commission pour la sécurité contre les risques d'incendie et de panique dans les établissements recevant du public :
  - le procès-verbal de visite, en cours de validité, délivré conformément à la réglementation en vigueur par ladite commission et comportant un avis favorable,
  - ou, lorsque la déclaration est déposée à une date antérieure à celle du passage de la commission, l'engagement sur l'honneur à ne pas exploiter le lieu en l'absence d'avis favorable de cette commission et la date de passage de la commission,
  - ou, pour les établissements du type chapiteaux, tentes et structures itinérantes, l'attestation de conformité mentionnée à l'article CTS 3 de l'arrêté du 25 juin 1980 susvisé ;
- pour les établissements de 5e catégorie, une attestation sur l'honneur (case à cocher) du classement en 5e catégorie ;
- une attestation sur l'honneur (case à cocher) selon laquelle, lorsque le lieu accueille des spectacles vivants diffusant de la musique amplifiée, il est équipé conformément aux règles de sécurité sanitaire en matière de risques sonores.

#### Procédure

L'autorité compétente délivre le récépissé de déclaration qui, sauf opposition de l’administration, vaudra licence pendant cinq ans. Au terme de ces cinq années, l'entrepreneur devra demander son renouvellement via le téléservice.

**À savoir**

En cas de silence du préfet de région dans un délai d’un mois, suivant réception du dossier, le récépissé est considéré comme valant licence.

*Pour aller plus loin :* articles R. 7122-2 à 7122-5 du Code du travail ; articles 1 et 2 de l'arrêté du 27 septembre 2019 pris en application du Code du travail (partie réglementaire) fixant la liste des documents et informations requis en vue de l’exercice de l’activité d’entrepreneur de spectacles vivants.

### c. Le cas échéant, demander une reconnaissance d'effet équivalent de son titre pour le ressortissant de l'UE ou de l'EEE en vue d'un établissement en France (LE)

#### Autorité compétente

Le préfet de région est compétent pour se prononcer sur la demande de reconnaissance.

#### Pièces justificatives

La demande se fait par le dépôt d'un dossier sur le [téléservice du ministère de la Culture](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_reconnaissance_00/?__CSRFTOKEN__=ad5455d4-c481-4c7e-96e1-cd93b63df8f4) comportant les pièces justificatives suivantes :

- le formulaire rempli, daté et signé ;
- l'état civil de l'entrepreneur ou, le cas échéant, les informations relatives à la société si le demandeur est une personne morale ;
- le ou les types d'activités d'entrepreneur envisagées (catégorie 1, 2 ou 3) ;
- un état descriptif des conditions de délivrance du titre d’effet équivalent produit et mentionné à l’article L. 7122-3 ainsi que les documents justificatifs ;
- la copie du titre pour lequel la reconnaissance est demandée.

#### Procédure

Le ressortissant adresse sa demande sur internet. Lorsqu'il reconnaît le titre d'effet équivalent, le préfet de région délivre un récépissé de déclaration pour la catégorie correspondant au titre dans un délai d'un mois à compter du dépôt du titre.

Dans le cas contraire, le préfet de région informe par tout moyen l'intéressé de son refus de reconnaître l'équivalence du titre par une décision motivée dans le même délai.

Le silence gardé par l'administration pendant un mois à compter du dépôt du titre vaut reconnaissance de l'équivalence. 

*Pour aller plus loin :* article R. 7122-7 du Code du travail ; article 5 de l'arrêté du 27 septembre 2019 pris en application du Code du travail (partie réglementaire) fixant la liste des documents et informations requis en vue de l’exercice de l’activité d’entrepreneur de spectacles vivants .

### d. Le cas échéant, effectuer une information préalable pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le préfet de région est compétent pour se prononcer sur la déclaration écrite du ressortissant.

#### Pièces justificatives

La demande se fait par le dépôt en ligne sur le [téléservice du ministère de la Culture](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_information_01/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7 ) d'un dossier comportant les pièces justificatives suivantes :

- l'état civil de l'entrepreneur ou, le cas échéant, les informations relatives à la société si le demandeur est une personne morale ;
- l'objet social de l'organisme et, le cas échéant, les références de son immatriculation à un registre professionnel ;
- la nature des spectacles, le nombre, la durée et les dates envisagées des représentations ;
- le nom ou la dénomination sociale, l'adresse de l'exploitant du ou des lieux de représentation envisagés ainsi que, le cas échéant, leur numéro de licence et l'adresse du ou des lieux, si elle est différente de celle de l'exploitant ;
- le nombre de salariés engagés et le nombre de salariés détachés en distinguant les personnels artistiques, techniques et administratifs et les artistes déclarés travailleurs indépendants ;
- le nombre de places maximum que comportent la ou les salles où aura lieu la représentation ;
- le numéro de TVA intracommunautaire de l'organisme.

#### Procédure

Le ressortissant adresse l’information préalable au préfet de région via le téléservice au moins un mois avant la date prévue du spectacle. 

*Pour aller plus loin :* article R. 7122-8 du Code du travail ; article 6 de l'arrêté du 27 septembre 2019 pris en application du Code du travail (partie réglementaire) fixant la liste des documents et informations requis en vue de l’exercice de l’activité d’entrepreneur de spectacles vivants.