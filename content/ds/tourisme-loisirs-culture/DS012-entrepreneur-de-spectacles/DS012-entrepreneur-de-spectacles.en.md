﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS012" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Entertainment promoter" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="entertainment-promoter" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/entertainment-promoter.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="entertainment-promoter" -->
<!-- var(translation)="Auto" -->


Entertainment promoter
==========================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The activity of entrepreneur of so-called live shows (shows including the physical presence of at least one artist in front of an audience) consists of:

- exploiting places used for public performances (category 1);
- produce shows or tours, and be responsible for the artistic stage (category 2);
- broadcast shows or tours, without being responsible for the art set (category 3).

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for a craft profession (in the case of a puppet show), the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

Article L. 110-1 of the Trade Code states that the activity of a live entertainment entrepreneur is presumed to be an act of commerce. However, the activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company on the condition it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. For entrepreneurs based in France

To hold a receipt of declaration worth a license to carry out the activity of entrepreneur of shows (see infra "3. b. Apply for a live entertainment contractor's licence"), the individual (a particular individual employer, or who has a business on his or her behalf) must:

- justify professional competence or experience:- either by holding a higher education degree or a degree of the same level registered in the[national directory of professional certifications](https://certificationprofessionnelle.fr/),
  - justifying a work experience of at least six months,
  - either by having undergoed a vocational training of at least 125 hours in the field of entertainment or justifying a set of skills, included in a directory established by the joint commission employment-training of the live show no later than the 1st October 2020;
- enjoy the legal capacity to carry out a commercial activity and be registered in the trade and corporate register or in the trades directory when subject to this obligation;
- establish that the safety obligations of venues are met (for performance venue operators - category 3) and have received safety training for shows on the repertoire available on The[joint commission website employment-training live show](https://www.cpnefsv.org/formations-agreees/formation-securite-licence-dentrepreneurs-exploitants-lieux) ;
- Be in good standing with regard to the employer's obligations under the Labour Code, the social security system or the provisions relating to the protection of intellectual and artistic property;
- having made his declaration via the ad hoc teleservice of the Ministry of Culture and complied with the deadlines set out in the Labour Code with regard to the declaration.

For legal entities, the conditions of competence or experience must be met by one or more persons in charge within the organization.

*To go further:* Articles L. 7122-4 and L. 7122-7 and R. 7122-2 and R. 7122-3 of the Labour Code.

### b. For EU or EEA nationals (free provision of services or Freedom of establishment)

#### For a temporary and casual exercise (LPS)

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement may perform the same temporary and occasional performance as a performance contractor. activity in France.

As such, the national must, prior to his first performance, inform the regional prefect of the place where he wishes to make it (see infra "3°. d. If necessary, make a written declaration for the EU or EEA national for a temporary and casual exercise (LPS)). This statement must be addressed via the[Ministry of Culture's teleservice](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_declaration_00/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7).

*To go further:* Article L. 7122-6 of the Labour Code.

#### For a permanent exercise (LE)

Any national of an EU or EEA state may settle in France to carry out the activity of entrepreneur of live shows, without declaring his activity, subject to producing an equivalent effect title issued in one of these states under conditions comparable (see infra "3.00). c. If necessary, request recognition of the equivalent effect of the title to the EU or EEA national for a permanent exercise (LE) ").

*To go further:* Article L. 7122-5 of the Labour Code.

### c. Some peculiarities of the regulation of the activity

#### Compliance with safety and accessibility standards

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*To go further:* Order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

It is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

#### Particulars

The performance contractor must indicate on all media promoting his show as well as on the ticket office of any show the receipt number of declaration worth license, valid of the director or entrepreneurs of the show living people who produce or broadcast the show

*To go further:* Article R. 7122-12 of the Labour Code.

#### Setting up a ticket office

At each show entrance, the contractor must issue a ticket to each of the spectators. To do so, it must set up a manual or computerized ticketing and make the prior declaration to the directorate of the tax services on which it depends.

When the ticket office is computerized, he will have to pass on the following information:

- The name of the software, its version number and, if applicable, its date, as well as the identity of its designer or the name of the software package;
- Computer setup
- The operating system
- Programming language
- The format of the source or executable software provided by the designer;
- Functional description of the system
- Facsimile a note, a management coupon and a receipt statement;
- security.

*To go further:* Articles 50 sexies I and 290 quater of the General Tax Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the entrepreneur will have to register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Formalities of Reporting a Commercial Company" or "Artisanal Company Reporting Formalities" for more information.

### b. Declare a live entertainment entrepreneur activity

The entertainment contractor must declare at least one month prior to the first performance his activity in order to obtain a valid receipt worth a licence that will allow him to carry out his activity in one of the three categories referred to above.

#### Competent authority

The professional will have to apply to the regional prefect of the place where his main establishment is located via the[online teleservice on the website of the Ministry of Culture](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_declaration_00/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7).

#### Supporting documents

The request is made by the[sending a folder online](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_declaration_00/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7) including all the following information and supporting documents:

- Accuracy on the licence categories requested
- Identification of the contractor person or legal person (denomination, legal form, address);
- any document justifying the applicant's professional capacity or, for corporations, the presence of a person justifying such capacity, which may be either:- copying the diploma of higher education or a title of the same level registered in the national directory of professional certifications,
  - proof of work experience of at least six months (employment contracts or other official documents),
  - a certificate of follow-up of 125 hours of professional training in the field of entertainment;
- documents relating to the identification of the individual or corporation and the ability to run a business or business activity:- The company identification number,
  - the code of the main activity carried out or envisaged and the purpose of the legal person as contained in its statutes,
  - the registration number in the register of trades and companies, the directory of trades (when it is subject to the registration requirement in one of these registers) or the national directory of associations in the case of associations,
  - a certificate on honour (checkbox) certifying the absence of a conviction or sanction prohibiting the exercise of a commercial activity;
- The reference to the applicable collective agreement;
- A schedule of planned programming and the project's description of live performances;
- a commitment on the honour (checkbox) to join the single desk for the live show (GUSO) or to the social protection organizations of the show, as well as to the institutions to which membership is made compulsory by the conventions national collectives of live performance or by collective labour agreement;
- for those who have, prior to the declaration, already engaged in live performances:- a certificate on the honour (checkbox) of up-to-date accounts of social contributions and contributions (GUSO or social protection organizations of the show; FNAS, CASC) or a protocol for staggering and committing to it,
  - a certificate of honour (checkbox) certifying that the company has no debts with respect to the payment of copyright, or a protocol for staggering debts and a commitment to comply with it,
  - A calendar of shows from the three years spent according to the model to be downloaded;
- for Category 3 contractors, the justification for having undergo taken training adapted to the nature of the venues (for a legal person justifying the presence within it of one or more individuals meeting this condition);
- for establishments subject to the commission's obligation to monitor fire and panic hazards in establishments receiving the public:- The valid visit report issued in accordance with the regulations in force by that commission and with a favourable opinion,
  - or, when the declaration is filed at a date prior to the commission's passage, the commitment on the honour of not exploiting the place in the absence of favourable notice from that commission and the date of passage of the commission,
  - or, for establishments such as marquees, tents and travelling structures, the certificate of compliance mentioned in Article CTS 3 of the order of 25 June 1980 above;
- for 5th category establishments, a certificate on the honour (checkbox) of the classification in 5th category;
- a certificate of honour (checkbox) according to which, when the venue hosts live performances broadcasting amplified music, it is equipped in accordance with the rules of health safety regarding noise risks.

#### Procedure

The competent authority issues the declaration receipt which, unless opposed by the administration, will be worth a licence for five years. At the end of these five years, the contractor will have to apply for renewal via teleservice.

**What to know**

In case of silence of the prefect of the region within one month, following receipt of the file, the receipt is considered to be worth a license.

*To go further:* Articles R. 7122-2 to 7122-5 of the Labour Code; Articles 1 and 2 of the order of 27 September 2019 taken under the Labour Code (regulatory part) setting out the list of documents and information required for the exercise of the activity of entrepreneur of live shows.

### c. If necessary, request recognition of the equivalent effect of his title for the EU or EEA national for establishment in France (LE)

#### Competent authority

The regional prefect is competent to decide on the application for recognition.

#### Supporting documents

The application is made by filing a file on the[Ministry of Culture's teleservice](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_reconnaissance_00/?__CSRFTOKEN__=ad5455d4-c481-4c7e-96e1-cd93b63df8f4) including the following supporting documents:

- The completed, dated and signed form;
- The contractor's marital status or, if applicable, company information if the applicant is a legal entity;
- the type or types of entrepreneurial activities envisaged (category 1, 2 or 3);
- A descriptive statement of the conditions for issuing the equivalent effect title produced and referred to in Article L. 7122-3 and supporting documents;
- copy of the title for which recognition is requested.

#### Procedure

The national sends his application on the internet. When the region prefect recognizes the equivalent effect title, he issues a return receipt for the title category within one month of the title being filed.

Otherwise, the prefect of the region informs by any means of his refusal to recognize the equivalence of the title by a reasoned decision within the same time frame.

The silence kept by the administration for one month from the filing of the title is worth recognition of equivalence.

*To go further:* Article R. 7122-7 of the Labour Code; Article 5 of the order of 27 September 2019 taken under the Labour Code (regulatory part) setting out the list of documents and information required for the exercise of the activity of entrepreneur of live shows .

### d. If necessary, provide advance information for the EU or EEA national for a temporary and casual exercise (LPS)

#### Competent authority

The regional prefect is responsible for deciding on the national's written statement.

#### Supporting documents

The application is made by filing online on the[Ministry of Culture's teleservice](https://mesdemarches.culture.gouv.fr/loc_fr/mcc/requests/THEAT_LICEN_information_01/?__CSRFTOKEN__=0733c021-13d0-49d7-82e7-04ec9de465e7) a file with the following supporting documents:

- The contractor's marital status or, if applicable, company information if the applicant is a legal entity;
- The social purpose of the organization and, if necessary, the references of its registration to a professional register;
- The nature of the performances, the number, duration and dates envisaged of the performances;
- the name or name, the address of the operator of the or places of representation envisaged and, if applicable, their licence number and the address of the premises, if it is different from that of the operator;
- the number of employees hired and the number of employees posted by distinguishing artistic, technical and administrative staff and artists declared self-employed;
- The maximum number of seats in the venue or halls where the performance will take place;
- the organization's intra-community VAT number.

#### Procedure

The national sends the information prior to the regional prefect via the teleservice at least one month before the scheduled date of the show.

*To go further:* Article R. 7122-8 of the Labour Code; Article 6 of the order of 27 September 2019 taken under the Labour Code (regulatory part) setting out the list of documents and information required for the exercise of the activity of entrepreneur of live shows.

