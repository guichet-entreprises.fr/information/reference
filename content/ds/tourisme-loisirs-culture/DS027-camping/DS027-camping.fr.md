﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS027" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourisme, Loisirs, Culture" -->
<!-- var(title)="Camping" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourisme-loisirs-culture" -->
<!-- var(title-short)="camping" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/tourisme-loisirs-culture/camping.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="camping" -->
<!-- var(translation)="None" -->

# Camping

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le camping est une activité touristique qui consiste à accueillir sur un terrain aménagé à cet effet des tentes, caravanes, résidences mobiles de loisirs et des habitations légères de loisirs. Ils peuvent faire l'objet d'une exploitation permanente (à l'année) ou saisonnière et accueillent une clientèle qui n'y élit pas domicile.

Ce terrain peut être constitué d'emplacements pourvus ou non d'équipements communs.

*Pour aller plus loin* : articles R. 331-1, D. 331-1-1 et suivants du Code du tourisme.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

- pour une activité commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI) ;
- pour une activité agricole, il s'agit de la chambre d'agriculture.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le professionnel qui souhaite exploiter un camping n'est soumis à aucune exigence de diplôme, toutefois, il doit :

- respecter les dispositions :
  - relatives à l'emplacement du terrain de camping,
  - en matière d'urbanisme et d'insertion dans les paysages ;
- selon le cas, être titulaire d'un permis d'aménager ou d'une déclaration préalable.

*Pour aller plus loin* : articles L. 443-1 et suivants et article R. 421-18 du Code de l'urbanisme.

#### Règlement national d'urbanisme

Le professionnel ne peut créer, installer ou aménager un camping dans certains lieux, notamment :

- les rivages de la mer et les sites classés et présentant un intérêt général (monuments historiques, artistiques, scientifiques, etc.) ;
- dans le périmètre des sites patrimoniaux remarquables ;
- dans certaines zones, dès lors qu'un plan local d'urbanisme le prévoit, et dès lors que cette pratique porte atteinte à l'ordre public ou à l'environnement.

**À noter**

Des dérogations peuvent être délivrées par les autorités compétentes selon la nature du site protégé.

*Pour aller plus loin* : articles R. 111-32 et suivants du Code de l'urbanisme ; articles L. 341-1 et suivants du Code de l'environnement ; article L. 631-1 du Code du patrimoine.

#### Urbanisme et insertion dans les paysages

Le professionnel est tenu, pour l'aménagement et l'installation de son camping :

- de limiter l'impact visuel de ses installations ;
- d'organiser son espace de manière à le rendre homogène, respectueux de l'environnement et sécurisé.

*Pour aller plus loin* : articles A. 111-7 et A. 111-8 du Code de l'urbanisme.

#### Permis d'aménager ou déclaration préalable

Le professionnel a l'obligation d'effectuer une demande de permis d'aménager ou une déclaration préalable à son installation. Cette demande varie selon la capacité d'accueil du camping :

- en cas d'aménagement d'un terrain de camping d'une capacité de vingt personnes ou six hébergements maximum, le professionnel doit effectuer une demande de déclaration préalable (cf. infra « 3°. a. Déclaration préalable ») ;
- en cas d'aménagement d'un terrain d'une capacité de plus de vingt personnes ou six hébergements maximum, le professionnel doit effectuer une demande de permis d'aménager (cf. infra « 3°. b. Demande de délivrance du permis d'aménager »).

Par ailleurs, le professionnel doit effectuer dans tous les cas une déclaration attestant l'achèvement et la conformité des travaux (DAACT) (cf. infra « 3°. c. Déclaration d'achèvement et de conformité des travaux (DAACT) »).

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services (LPS) ou Libre Établissement (LE))

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer l'activité d'exploitant d'un camping à titre temporaire et occasionnel ou permanent en France.

À ce titre, le professionnel ressortissant de l'UE est soumis aux mêmes exigences professionnelles que le ressortissant français (cf. supra « 2. a. Qualifications professionnelles »).

### c. Quelques particularités de la réglementation de l’activité

**Taxe de séjour**

Dès lors qu'une taxe de séjour est prévue dans la commune où se situe le camping, le professionnel doit la percevoir auprès de ses clients et la reverser à la collectivité territoriale.

Pour plus de précisions, il est conseillé de consulter le [site dédié à la taxe de séjour](http://taxesejour.impots.gouv.fr/DTS_WEB/FR/).

*Pour aller plus loin* : article L. 2333-26 du Code général des collectivités territoriales.

**Exigences sanitaires**

Le professionnel exploitant un camping est soumis au respect d'exigences sanitaires minimales relatives à :

- l'eau destinée à la consommation et les eaux usées ;
- la collecte des déchets ménagers ;
- l'entretien des espaces communs.

En cas de non-respect de ces exigences, le professionnel encourt la fermeture temporaire du camping et son évacuation.

*Pour aller plus loin* : arrêté du 17 juillet 1985 relatif aux conditions sanitaires minimales communes aux terrains aménagés pour l'accueil des campeurs et des caravanes et aux terrains affectés spécialement à l'implantation d'habitations légères de loisirs.

**Obligations d'alerte et évacuation**

Le professionnel exploitant un camping est tenu au respect des exigences en matière d'information, d'alerte et d'évacuation établies par l'autorité ayant délivré le permis d'aménager ou ayant procédé à l'enregistrement de sa déclaration préalable (cf. infra « 3°. Démarches et formalités d'installation »).

À ce titre, le professionnel doit :

- remettre dès l'arrivée de chaque client un document mentionnant les consignes de sécurité du camping ;
- afficher les informations sur les consignes de sécurité, à raison d'une affiche par tranche de 5 000 mètres carrés ;
- mettre à la disposition du client un cahier des prescriptions de sécurité.

*Pour aller plus loin* : articles R. 125-15 à R. 125-22 du Code de l'environnement.

**Règlement intérieur et notice d'information**

Le professionnel exploitant un terrain de camping doit être titulaire d'un règlement intérieur et d'une notice d'information conformes aux modèles établis aux annexes 1 et 2 de l'arrêté du 17 février 2014 relatif à l'obligation pour les terrains de camping ou de caravanage ainsi que pour les parcs résidentiels de loisirs de disposer d'un modèle de règlement intérieur et d'une notice d'information sur les conditions de location des emplacements à l'année.

Ces documents doivent être tenus à la disposition des clients.

*Pour aller plus loin* : article D. 331-1-1 du Code du tourisme ; arrêté du 17 février 2014 susvisé.

**Obligation d'information préalable du client**

Le professionnel doit remettre au client, avant tout contrat de location d'un emplacement de son camping, l'ensemble des informations relatives à son établissement et aux conditions de location. Ce document doit mentionner l'ensemble des éléments figurant en annexe de l'arrêté du 22 octobre 2008 relatif à l'information préalable du consommateur sur les caractéristiques des hébergements locatifs en hôtellerie de plein air.

*Pour aller plus loin* : article L. 113-3 du Code de la consommation.

**Facturation**

Le professionnel est soumis au respect des obligations en matière de facturation et doit délivrer une note au client en cas de prestation d'un montant supérieur ou égal à 25 euros. Cette note doit comporter les informations suivantes :

- sa date de rédaction ;
- le nom et l'adresse du prestataire ;
- le nom du client (si celui-ci ne s'y oppose pas) ;
- la date et le lieu d'exécution de la prestation ;
- le détail de chaque prestation ainsi que le montant total à payer (toutes taxes comprises et hors taxes).

Le professionnel doit conserver un double de ces notes, les classer chronologiquement et les conserver pendant deux ans.

*Pour aller plus loin* : arrêté n° 83-50/A du 3 octobre 1983 relatif à la publicité des prix de tous les services.

**Spécificités en cas de zones à risques**

Lorsque l'établissement se trouve dans une zone soumise à un risque (naturel ou technologique), le professionnel doit mettre en place un système d'information du client pour assurer l'alerte, leur information et leur évacuation.

*Pour aller plus loin* : articles L. 443-2 et R. 443-9 et suivants du Code de l'urbanisme.

**Respect des normes de sécurité et d'accessibilité**

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

**Entretien du terrain de camping**

Le professionnel doit assurer l'entretien de son établissement (parties communes, emplacements, végétation, etc.).

À défaut, le professionnel peut être mis en demeure par l'autorité compétente de réaliser cet entretien et de procéder aux aménagements et réparations nécessaires.

*Pour aller plus loin* : article R. 480-7 du Code de l'urbanisme.

**Demande de classement du camping**

Le professionnel peut effectuer une demande de classement au sein d'une catégorie d'établissements en fonction de critères fixés par un tableau de classement élaboré par l'agence du développement touristique en France ([Atout France](https://www.classement.atout-france.fr/)).

Pour cela, le professionnel doit remettre, par voie électronique, à Atout France, un certificat de visite après l'évaluation d'un organisme agréé précisant le nombre d'emplacements du terrain.

Le camping pourra alors être classé pendant cinq ans au sein de l'une des catégories suivantes :

- « Tourisme » si plus de la moitié des emplacements est destinée à la location pour une clientèle de passage (location pour une nuit, une semaine ou un mois) ;
- « Loisirs » si plus de la moitié des emplacements est destinée à la location d'une durée supérieure à un mois.

En outre, le professionnel exploitant un camping classé doit afficher un panonceau à l'entrée de son établissement :

- conforme à l'annexe 5 de l'arrêté du 22 décembre 2010 si son établissement est classé dans la catégorie « Tourisme » ;
- conforme à l'annexe de l'arrêté du 4 novembre 2014 si son établissement est classé dans la catégorie « Aire naturelle ».

## 3°. Démarches et formalités d’installation

### a. Déclaration préalable

**Autorité compétente**

Le professionnel doit déposer sa demande en deux exemplaires par pli recommandé avec avis de réception à la mairie de la commune dans laquelle il souhaite s'implanter.

**Pièces justificatives**

La demande doit contenir :

- le formulaire Cerfa n° 13404 complété et signé ;
- les pièces mentionnées dans le formulaire ainsi que leur bordereau de dépôt.

**Issue de la procédure**

Dès le dépôt de la demande, l'autorité compétente adresse un numéro d'enregistrement à la demande mentionnant la date à partir de laquelle les travaux peuvent débuter. L'autorité procède également à l'affichage en mairie de la déclaration et de la nature du projet.

En cas d'opposition au projet, le demandeur est informé par lettre recommandée avec avis de réception ou par voie électronique. Toutefois, l'absence de réponse durant le délai d'instruction prévu vaut autorisation d'exercer l'activité.

**Voies de recours**

Le professionnel dispose de deux voies de recours contre la décision :

- un recours administratif dans un délai de deux mois soit :
  - gracieux devant l'autorité compétente,
  - hiérarchique devant une autorité supérieure ;
- un recours contentieux devant le juge administratif dans un délai de deux mois.

*Pour aller plus loin* : articles R. 421-23 c), R. 423-1 et suivants, et articles A. 441-1 et suivants du Code de l'urbanisme.

### b. Demande de délivrance du permis d'aménager

**Autorité compétente**

Le professionnel doit déposer sa demande en quatre exemplaires par pli recommandé avec avis de réception à la mairie de la commune dans laquelle les aménagements sont prévus.

**À noter**

Dans certains cas le préfet peut être compétent pour délivrer le permis d'aménagement (cf. article R. 422-2 du Code de l'urbanisme).

**Pièces justificatives**

La demande doit contenir :

- le formulaire Cerfa n° 13409 complété et signé ;
- les pièces mentionnées dans le formulaire ainsi que leur bordereau de dépôt.

**Issue de la procédure**

Lors de son dépôt, le maire adresse un numéro d'enregistrement au professionnel et lui délivre un récépissé indiquant la date à laquelle le permis tacite est prévu. Il procède également à l'affichage en mairie, dans les quinze jours suivants le dépôt, d'un avis de dépôt de la demande précisant la nature du projet.

Le maire dispose d'un délai de trois mois pour prendre sa décision et la notifier au professionnel par lettre recommandée avec avis de réception ou par voie électronique. L'absence de réponse durant le délai d'instruction vaut acceptation de la demande de permis.

**À noter**

Le délai d'instruction peut être aménagé dans certains cas (cf. articles R. 423-24 à R. 423-33 du Code de l'urbanisme).

**Voies de recours**

La décision de l'autorité compétente peut faire l'objet des mêmes recours que ceux prévus pour la déclaration préalable (cf. supra « 3°. a. Voies de recours »).

*Pour aller plus loin* : articles R. 421-19 c), R. 423-1 et suivants, et articles A. 441-4 et suivants du Code de l'urbanisme.

### c. Déclaration d'achèvement et de conformité des travaux (DAACT)

Le professionnel titulaire d'un permis ou ayant effectué une déclaration préalable ne peut commencer les travaux qu'après avoir effectué la DAACT.

**Autorité compétente**

L'autorité compétente est celle ayant délivré le permis ou ayant procédé à l'enregistrement de la déclaration préalable.

**Pièces justificatives**

La demande doit contenir :

- le formulaire Cerfa n° 13408 complété et signé ;
- selon le cas :
  - une attestation constatant que les travaux sont conformes aux règles d'accessibilité applicables pour les établissements recevant du public,
  - une attestation délivrée par un contrôleur technique en cas de risques parasismiques.

*Pour aller plus loin* : article R. 443-8 du Code de l'urbanisme ; articles R. 511-19-27 et L. 112-19 du Code de la construction et de l'habitation.

### d. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit déclarer son activité auprès de la chambre d'agriculture ou s’immatriculer au registre du commerce et des sociétés (RCS).

### e. Le cas échéant, enregistrer les statuts de la société

L'exploitant du camping doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du service des impôts des entreprises (SIE) si :

- l'acte comporte une opération particulière soumise à un enregistrement ;
- si la forme même de l'acte l'exige.

**Autorité compétente**

L'autorité compétente en matière d'enregistrement est :

- le service de la publicité foncière du lieu de situation de l'immeuble, lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
- le pôle enregistrement du SIE pour tous les autres cas.

**Pièces justificatives**

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.