﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS027" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Tourism, culture and leisure activities" -->
<!-- var(title)="Camping" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="tourism-culture-and-leisure-activities" -->
<!-- var(title-short)="camping" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/tourism-culture-and-leisure-activities/camping.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="camping" -->
<!-- var(translation)="Auto" -->

Camping
=======

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

1°. Defining the activity
------------------------

### a. Definition

Camping is a tourist activity that consists of hosting tents, caravans, mobile leisure residences and light recreational dwellings on a purpose-built land. They can be permanently exploited (year-round) or seasonally and welcome a clientele that does not choose to live there.

This land may or may not be made up of sites with common equipment.

*For further information*: Articles R. 331-1, D. 331-1-1 and the following of the Tourism Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI);
- for an agricultural activity, it is the chamber of agriculture.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional who wishes to operate a campsite is not subject to any diploma requirement, however, he must:

- comply with the provisions:- related to the location of the campground,
  - in terms of urban planning and inclusion in landscapes;
- depending on the case, be licensed or pre-reported.

*For further information*: Articles L. 443-1 and following and Section R. 421-18 of the Planning Code.

#### National Planning Regulations

The professional may not create, install or set up a campsite in certain places, including:

- sea shores and sites of general interest (historical, artistic, scientific, etc.);
- Within the perimeter of outstanding heritage sites;
- in some areas, as long as a local urban planning plan provides for it, and as long as it harms public order or the environment.

**Please note**

Derogations may be issued by the relevant authorities depending on the nature of the protected site.

*For further information*: Articles R. 111-32 and the following articles of the Planning Code; Articles L. 341-1 and the following of the Environment Code; Section L. 631-1 of the Heritage Code.

#### Urbanism and integration into landscapes

The professional is required, for the layout and installation of his campsite:

- Limit the visual impact of its installations;
- to organize its space in a way that makes it homogeneous, environmentally friendly and secure.

*For further information*: Articles A. 111-7 and A. 111-8 of the Planning Code.

#### Planning permits or advance declarations

The professional is required to apply for a planning permit or a pre-installation declaration. This request varies depending on the capacity of the campsite:

- In the case of the development of a campground with a capacity of up to twenty people or six accommodations, the professional must make a request for prior declaration (see infra "3. a. Pre-declaration");
- In the case of the development of a land with a capacity of more than twenty people or up to six accommodations, the professional must apply for a development permit (see infra "3°. b. Application for planning permits").

In addition, the professional must make a declaration in all cases certifying the completion and compliance of the work (DAACT) (see infra "3.3. c. Declaration of Completion and Compliance of Work (DAACT)) ").

### b. Professional Qualifications - European Nationals (Freedom to provide services or Freedom of establishment)

There is no provision for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement to operate a campsite on a temporary and casual basis or permanent in France.

As such, the professional EU national is subject to the same professional requirements as the French national (see above "2. a. Professional qualifications").

### c. Some peculiarities of the regulation of the activity

**Residence tax**

As soon as a residence tax is provided in the commune where the campsite is located, the professional must collect it from his clients and pay it to the local authority.

For more details, it is advisable to consult the[website dedicated to the residence tax](http://taxesejour.impots.gouv.fr/DTS_WEB/FR/).

*For further information*: Article L. 2333-26 of the General Code of Local Authorities.

**Health requirements**

The professional operating a campsite is subject to the compliance with minimum sanitary requirements relating to:

- water for consumption and wastewater;
- Collecting household waste
- maintenance of common areas.

If these requirements are not met, the professional incurs the temporary closure of the campsite and its evacuation.

*For further information*: decree of 17 July 1985 relating to the minimum sanitary conditions common to the grounds developed for the reception of campers and caravans and the land specially earmarked for the establishment of light recreational dwellings.

**Warning and evacuation obligations**

The professional operating a campsite is required to comply with the information, alert and evacuation requirements established by the authority that issued the planning permit or having registered its advance declaration (see infra "3" . Installation procedures and formalities").

As such, the professional must:

- Submit a document on the arrival of each guest out of the campsite's safety guidelines.
- View safety information at a rate of one poster per 5,000 square metres;
- provide the customer with a safety requirement book.

*For further information*: Articles R. 125-15 to R. 125-22 of the Environment Code.

**Internal rules and information notices**

The campground professional must have an in-bye regulation and information leaflet consistent with the models established in Schedules 1 and 2 of the Stopped February 17, 2014 regarding the requirement for campgrounds or caravan parks as well as for residential recreational parks to have an internal regulation model and an information leaflet on the rental conditions of the pitches year.

These documents must be made available to clients.

*For further information*: Article D. 331-1-1 of the Tourism Code; February 17, 2014 aforementioned.

**Customer advance disclosure requirement**

The professional must give the client, before any contract to rent a location of his campsite, all the information relating to his establishment and the conditions of rental. This document should mention all the elements in the appendix of the Stopped October 22, 2008 on advance consumer information on the characteristics of rental accommodation in outdoor hotels.

*For further information*: Article L. 113-3 of the Consumer Code.

**Billing**

The professional is subject to compliance with the billing obligations and must issue a note to the customer in case of a benefit of an amount greater than or equal to 25 euros. This note should include the following information:

- Its date of writing;
- The provider's name and address
- The customer's name (if the customer does not object to it);
- The date and place of execution of the service
- the details of each benefit as well as the total amount payable (all taxes included and excluding taxes).

The professional must keep double these grades, classify them chronologically and keep them for two years.

*For further information*: ( Order 83-50/A of 3 October 1983 relating to the price advertising of all services.

**Specifics in case of high-risk areas**

When the facility is in a risk zone (natural or technological), the professional must set up a client information system to ensure the alert, their information and their evacuation.

*For further information*: Articles L. 443-2 and R. 443-9 and following of the Planning Code.

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

It is advisable to refer to the "Establishment receiving the public" sheet for more information.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

**Campground maintenance**

The professional must maintain his establishment (common areas, pitches, vegetation, etc.).

Failing this, the professional may be put on notice by the competent authority to carry out this maintenance and to carry out the necessary adjustments and repairs.

*For further information*: Article R. 480-7 of the Planning Code.

**Request for camping classification**

The professional can apply for a classification within a category of establishments according to criteria set by a ranking table drawn up by the tourism development agency in France ([Trump France](https://www.classement.atout-france.fr/)).

To do this, the professional must give, electronically, to Trump France, a certificate of visit after the evaluation of an approved body specifying the number of locations of the land.

The campsite can then be classified for five years in one of the following categories:

- "Tourism" if more than half of the pitches are intended for rent for a passing clientele (rental for one night, one week or one month);
- "Leisure" if more than half of the sites are for rental for more than one month.

In addition, the professional operating a classified campsite must display a sign at the entrance to his establishment:

- comply with Schedule 5 of the Stopped December 22, 2010 if its establishment is classified in the "Tourism" category;
- compliant with the annex of the Stopped November 4, 2014 if its establishment is classified in the "Natural Area" category.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Pre-declaration

**Competent authority**

The professional must file his application in two copies in a recommended fold with notice of receipt to the town hall of the commune in which he wishes to establish himself.

**Supporting documents**

The request must contain:

- The Form Cerfa No. 13404 completed and signed;
- the parts mentioned in the form as well as their filing slip.

**Outcome of the procedure**

Upon filing the application, the relevant authority sends a registration number to the application stating the date from which work can begin. The authority also displays the declaration and the nature of the project in the town hall.

In the event of opposition to the project, the applicant is informed by recommended letter with notice of receipt or electronically. However, the lack of response during the expected trial period is worth permission to carry out the activity.

**Remedies**

The professional has two avenues of appeal against the decision:

- administrative appeal within two months:- graceful before the competent authority,
  - hierarchically before a higher authority;
- a legal challenge to the administrative judge within two months.

*For further information*: Articles R. 421-23 c), R. 423-1 and subsequent articles, and articles A. 441-1 and following of the Planning Code.

### b. Application for planning permit

**Competent authority**

The professional must file his application in four copies in a recommended fold with notice of receipt to the town hall of the municipality in which the accommodations are planned.

**Please note**

In some cases the prefect may be responsible for issuing the planning permit (see Article R. 422-2 of the Planning Code).

**Supporting documents**

The request must contain:

- The Form Cerfa No. 13409 completed and signed;
- the parts mentioned in the form as well as their filing slip.

**Outcome of the procedure**

When it is filed, the mayor sends a registration number to the professional and issues a receipt indicating the date on which the tacit permit is provided. It also posts a notice of filing of the application in the town hall within a fortnight of filing specifying the nature of the project.

The mayor has three months to make his decision and notify the professional by recommended letter with notice of receipt or electronically. Failure to respond during the trial period is a result of acceptance of the permit application.

**Please note**

The trial period may be arranged in some cases (see Articles R. 423-24 to R. 423-33 of the Planning Code).

**Remedies**

The decision of the competent authority may be subject to the same remedies as those provided for the prior declaration (see supra "3.0). a. Remedies").

*For further information*: Articles R. 421-19 c), R. 423-1 and subsequent articles, and articles A. 441-4 and following of the Planning Code.

### c. Completion and Compliance Statement (DAACT)

The licensed or pre-reported professional may only begin the work after completing the DAACT.

**Competent authority**

The competent authority is the one that issued the permit or had registered the prior declaration.

**Supporting documents**

The request must contain:

- The Form Cerfa No. 13408 completed and signed;
- depending on the case:- a certificate that the work complies with the accessibility rules applicable to establishments receiving the public,
  - a certificate issued by a technical controller in case of seismic risks.

*For further information*: Article R. 443-8 of the Planning Code; Sections R. 511-19-27 and L. 112-19 of the Building and Housing Code.

### d. Company reporting formalities

Depending on the nature of the business, the contractor must report his activity to the Chamber of Agriculture or register in the Register of Trade and Companies (RCS).

It is advisable to refer to the "Formalities of Reporting a Commercial Company" and "Registration of a Commercial Individual Company to the SCN" and to the[Chamber of Agriculture](http://www.chambres-agriculture.fr/) for more information.

### e. If necessary, register the company's statutes

The operator of the campsite must, once the company's statutes have been dated and signed, register them with the corporate tax office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.