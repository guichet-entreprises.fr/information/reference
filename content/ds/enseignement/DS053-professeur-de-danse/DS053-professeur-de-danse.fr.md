﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS053" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseignement" -->
<!-- var(title)="Professeur de danse" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="enseignement" -->
<!-- var(title-short)="professeur-de-danse" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/enseignement/professeur-de-danse.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="professeur-de-danse" -->
<!-- var(translation)="None" -->

# Professeur de danse

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le professeur de danse est un professionnel qui enseigne, contre rétribution, la danse à un public varié (des jeunes enfants aux adultes). Il exerce aussi bien dans des établissements publics que privés et intervient dans la définition des composantes pédagogiques, éducatives et artistiques de l’enseignement.

Seules les activités de professeur de danse classique, danse contemporaine et danse jazz sont concernées par cette fiche et par la réglementation détaillée ci-dessous.

*Pour aller plus loin* : articles L. 362-1 et suivants du Code de l’éducation.

### b. Centre de formalités des entreprises compétent

Le centre de formalités des entreprises (CFE) compétent dépend de la nature de l’activité exercée et de la forme juridique de l’entreprise :

- pour les professions libérales, le CFE compétent est l’Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre du commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

Pour plus d’informations, il est conseillé de consulter les sites de l'Urssaf, de la CCI de Paris, de la CCI de Strasbourg ainsi que le site Service-public.fr

## 2°. Conditions d’installation

### a. Qualifications professionnelles

L’intéressé ne peut enseigner sous le titre de professeur de danse, ou un titre équivalent, que s’il détient certains diplômes ou qualifications obligatoires.

En effet, pour pouvoir exercer en qualité de professeur de danse, l’intéressé doit, au choix :

- avoir obtenu le diplôme de professeur de danse délivré par l'État ou le certificat d'aptitude aux fonctions de professeur de danse ;
- avoir obtenu un diplôme français ou étranger reconnu équivalent au diplôme d’État.

**Bon à savoir**

Certaines personnes sont titulaires de plein droit du diplôme de professeur de danse délivré par l’État. Il s’agit des artistes chorégraphiques qui ont suivi une formation pédagogique et qui justifient d'une activité professionnelle d'au moins trois ans au sein de l’un des établissements suivants :

- le ballet de l'Opéra national de Paris ;
- les ballets des théâtres de la réunion des théâtres lyriques municipaux de France ;
- les centres chorégraphiques nationaux ;
- les compagnies d'un État membre de l'Union européenne (UE) ou d'un autre État partie à l'accord sur l'Espace économique européen (EEE), dont la liste est fixée par arrêté du ministre chargé de la culture.

Si l’intéressé ne dispose pas de l’un de ces diplômes, il peut tout de même enseigner la danse à la condition de détenir, au choix :

- une dispense accordée en raison de sa renommée particulière ou de son expérience confirmée en matière d'enseignement de la danse ;
- la dispense accordée aux personnes qui enseignaient la danse depuis plus de trois ans au 11 juillet 1989.

Pour plus d’informations sur la délivrance de l’attestation de dispense ou sur les cas de reconnaissance d’équivalence de diplôme, il est conseillé de se rapprocher du ministère chargé de la culture et de la communication.

*Pour aller plus loin* : articles L. 362-1 et suivants du Code de l’éducation.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### En cas de Libre Établissement (LE)

Peuvent légalement s'établir en France pour enseigner la danse contre rétribution, ou faire usage du titre de professeur de danse, les ressortissants qui se trouvent dans l’une des situations suivantes :

- si l’intéressé dispose d’un diplôme ou titre obtenu dans un État de l’UE ou de l’EEE qui réglemente l'accès ou l’exercice de l’activité, une attestation de compétences ou un titre de formation délivré par les autorités compétentes est nécessaire ;
- si l’intéressé dispose d’un diplôme ou titre obtenu dans un État tiers (hors UE ou EEE), l’intéressé doit disposer d’un titre de formation ayant été reconnu par un État de l’UE ou de l’EEE qui lui a permis d'exercer légalement la profession dans cet État pendant une période minimale d’un an, à condition que cette expérience professionnelle soit certifiée par l'État dans lequel elle a été acquise ;
- si l’intéressé dispose d’un diplôme ou titre obtenu dans un État de l’UE ou de l’EEE, qui ne réglemente ni l'accès ni l’exercice de la profession, l’intéressé doit disposer d’une attestation de compétences ou un titre de formation délivré par les autorités compétentes de l’État permettant de certifier leur préparation à l'exercice de la profession, à la condition que l’intéressé justifie de l'exercice de cette activité pendant l’équivalent d’un an à temps plein au cours des dix dernières années dans un État de l’UE ou de l’EEE ou à temps partiel pendant une durée totale équivalente. Cette justification n'est pas requise lorsque la formation conduisant à cette profession est réglementée dans l'État membre ou partie à l'accord sur l’EEE dans lequel elle a été validée.

Après avoir examiné si les connaissances, aptitudes et compétences acquises par le demandeur, au cours de son expérience professionnelle ou de l'apprentissage tout au long de la vie et ayant fait l'objet, à cette fin, d'une validation en bonne et due forme par un organisme compétent, dans un État membre ou dans un pays tiers, ne sont pas de nature à combler, en tout ou en partie, des différences substantielles de formation, le ministre chargé de la culture peut exiger que le demandeur se soumette à des mesures de compensation, consistant, au choix de ce dernier, soit en un stage d'adaptation, soit en une épreuve d'aptitude. Si le demandeur est titulaire d'une attestation de compétence au sens du a de l'article 11 de la directive 2005/36/ CE du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles, le ministre peut prescrire le stage d'adaptation ou l'épreuve d'aptitude.

#### En cas de Libre Prestation de Services (LPS)

Le ressortissant d'un État de l’UE ou de l’EEE qui souhaite enseigner la danse en France à titre temporaire et occasionnel est réputé remplir les conditions de qualifications professionnelles requises à la double condition :

- d’être légalement établi dans l’un de ces États pour y exercer la même activité ;
- si l’activité ou la formation y conduisant n’est pas réglementée dans l’État dans lequel il est établi, d’avoir exercé l’activité de professeur de danse (dans l’une des options danse classique, contemporaine ou jazz) pendant au moins l’équivalent d’un an à temps complet ou partiel pendant une durée équivalente au cours des dix années précédant la prestation.

S’il remplit effectivement les conditions de qualifications professionnelles, l’intéressé doit uniquement effectuer une déclaration préalable d’activité avant sa première prestation, pour pouvoir exercer en tant que professeur de danse en France, de manière occasionnelle et temporaire.

*Pour aller plus loin* : article 5 de l’arrêté du 23 décembre 2008 relatif aux conditions d'exercice de la profession de professeur de danse applicables aux ressortissants d'un État membre de la Communauté européenne ou d'un autre État partie à l'accord sur l'Espace économique européen, modifié par l’arrêté du 25 juillet 2011 et article L. 362-1-1 du Code de l’éducation.

#### Accès partiel

Un accès partiel à une activité professionnelle relevant de la profession de professeur de danse, peut être accordé au cas par cas aux ressortissants de l'Union européenne ou d'un Etat partie à l'accord sur l'Espace économique européen par le ministre chargé de la culture lorsque les trois conditions suivantes sont remplies :

- le professionnel est pleinement qualifié pour exercer, dans l'État d'origine, l'activité professionnelle pour laquelle un accès partiel est sollicité ;
- les différences entre l'activité professionnelle légalement exercée dans l'Etat d'origine et la profession réglementée en France de professeur de danse sont si importantes que l'application de mesures de compensation reviendrait à imposer au demandeur de suivre le programme complet d'enseignement et de formation requis en France pour avoir pleinement accès à cette profession ;
- l'activité professionnelle peut objectivement être séparée des autres activités relevant de la profession de professeur de danse en France, dans la mesure où elle peut être exercée de manière autonome dans l'État d'origine.

L'accès partiel peut être refusé pour des raisons impérieuses d'intérêt général si ce refus est proportionné à la protection de cet intérêt.

Les demandes aux fins d'accès partiel sont examinées, selon le cas, comme des demandes à fin d'établissement ou de libre prestation de services temporaire et occasionnelle de la profession.

### c. Conditions d’honorabilité et incompatibilités

Pour pouvoir exercer en qualité de professeur de danse, l’intéressé ne doit pas avoir fait l’objet d’une condamnation :

- à une peine d'emprisonnement sans sursis supérieure à quatre mois ;
- pour une infraction de viol, d’agression sexuelle, d’atteinte sexuelle sur mineur ou de proxénétisme.

*Pour aller plus loin* : article L. 362-5 du Code de l’éducation et articles 222-22 et suivants du Code pénal.

### d. Assurance de responsabilité civile

Le professeur de danse peut souscrire une assurance de responsabilité civile professionnelle. Elle lui permet d’être couvert pour les dommages causés à autrui, qu’il en soit directement ou indirectement à l’origine.

**Bon à savoir**

L’exploitant de l’école de danse a l’obligation de souscrire un contrat d'assurance couvrant la responsabilité civile des enseignants qui y exercent.

*Pour aller plus loin* : article L. 462-1 du Code de l’éducation.

### e. Quelques particularités de la réglementation de l’activité

#### Obligation de vérifier que les élèves disposent d’un certificat médical

Le professeur de danse doit s’assurer, avant le début de chaque période d’enseignement, que les élèves disposent effectivement d’un certificat médical attestant l’absence de contre-indication à l’enseignement qui leur est dispensé. Ce certificat doit être renouvelé chaque année. À la demande de tout enseignant, un certificat attestant d’un examen médical supplémentaire peut être requis.

*Pour aller plus loin* : article R. 362-2 du Code de l’éducation.

#### Enseignement de la danse à des enfants

Exerçant le plus souvent aux côtés de mineurs, les professeurs de danse sont soumis au respect des obligations suivantes :

- les enfants de quatre et cinq ans ne peuvent pratiquer que des activités d’éveil corporel (c’est-à-dire excluant toutes les techniques propres à la discipline enseignée) ;
- les enfants de cinq et six ans ne peuvent pratiquer qu’une activité d’initiation dans la discipline choisie (c’est-à-dire excluant toutes les techniques propres à la discipline enseignée).

De manière générale, l’ensemble des activités pratiquées par les enfants de quatre à sept ans inclus ne doit comporter aucun travail contraignant pour le corps, ni extensions excessives, ni articulations forcées.

*Pour aller plus loin* : article R. 362-1 du Code de l’éducation.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Le cas échéant, effectuer une demande de reconnaissance de qualifications professionnelles ou de dispense de diplôme

#### Demander une reconnaissance des qualifications professionnelles

Les ressortissants d’un État de l’UE ou de l’EEE ayant obtenu dans l’un de ces États leurs qualifications professionnelles et qui souhaitent s’établir en France pour exercer en tant que professeur de danse, doivent demander la reconnaissance de leurs qualifications professionnelles dans une ou plusieurs options (danse classique, contemporaine, jazz).

##### Autorité compétente

La demande de reconnaissance des qualifications professionnelles doit être adressée à la direction générale de la création artistique.

##### Délais

La direction générale de la création artistique délivre un récépissé dans un délai d’un mois après réception de la demande de reconnaissance des qualifications professionnelles.

Le ministre chargé de la culture statue sur la demande de reconnaissance des qualifications professionnelles par une décision motivée dans un délai de quatre mois à compter de la réception du dossier complet.

##### Issues de la demande

Après examen de la demande, le ministre chargé de la culture peut, au choix :

- délivrer une attestation de reconnaissance des qualifications professionnelles au demandeur ;
- proposer au demandeur de se soumettre, soit à une épreuve d’aptitude, soit à un stage d’adaptation s’il constate des différences substantielles de formation.

En cas d’absence de réponse dans un délai de quatre mois, la demande de reconnaissance est considérée comme accordée.

##### Pièces justificatives

Le demandeur doit joindre à sa demande de reconnaissance de qualifications professionnelles un dossier composé du formulaire Cerfa 14531*01 et de pièces justificatives, traduites en français, le cas échéant. Ces pièces varient selon la situation du demandeur :

- si le demandeur est titulaire d’une attestation de compétence ou d’un titre de formation délivré par les autorités compétentes d’un État de l’UE ou de l’EEE qui réglemente l’accès à la profession ou son exercice et qui permet d’exercer légalement la profession dans cet État :
  - une copie de la pièce d’identité,
  - le descriptif de l’expérience professionnelle acquise par le demandeur,
  - une copie des attestations de compétences professionnelles, des titres de formation ou de tout document attestant d’une qualification professionnelle, délivrés par l’autorité compétente et permettant d’exercer légalement la profession,
  - le contenu des études et des stages suivis pendant la formation initiale, indiquant le nombre d’heures par matière pour les enseignements théoriques, la durée des stages, le domaine dans lequel ils ont été effectués ainsi que, s’il y a lieu, le résultat des évaluations réalisées ; le cas échéant, le relevé des stages de formation continue indiquant le contenu et la durée de ces stages. Ces éléments sont délivrés et attestés par la structure de formation concernée ;
- si le demandeur est titulaire d’un titre de formation délivré par un État tiers qui a été reconnu par un État de l’UE ou de l’EEE et qui a permis l’exercice de la profession dans cet État pendant au moins trois ans :
  - une copie de la pièce d’identité,
  - le descriptif de l’expérience professionnelle acquise par le demandeur,
  - la copie de la reconnaissance par un État de l’UE ou de l’EEE du titre de formation délivré par un État tiers,
  - la copie du document certifiant de l’exercice par le demandeur de la profession de professeur de danse pendant une période minimale de trois ans dans l’État qui a reconnu le titre de formation,
  - le contenu des études et des stages suivis pendant la formation initiale indiquant le nombre d'heures par matière pour les enseignements théoriques, la durée des stages, le domaine dans lequel ils ont été effectués ainsi que, s'il y a lieu, le résultat des évaluations réalisées ; le cas échéant, le relevé des stages de formation continue indiquant le contenu et la durée de ces stages. Ces éléments sont délivrés et attestés par la structure de formation concernée. Lorsque la période minimale d’un an n'a pas été effectuée dans l'État qui a reconnu le diplôme, certificat ou titre, le titulaire doit être reconnu qualifié par le ministre chargé de la culture au vu des connaissances et qualifications attestées par ce diplôme, certificat ou titre et par l'ensemble de la formation et de l'expérience professionnelle acquises ;
- si le demandeur est titulaire d’une attestation de compétence ou d’un titre de formation délivré par les autorités compétentes d’un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de la profession et que le demandeur justifie de l’exercice de cette activité à temps plein pendant un an au cours des dix dernières années :
  - une copie de la pièce d’identité,
  - le descriptif de l’expérience professionnelle acquise par le demandeur,
  - une copie des attestations de compétence professionnelle, des titres de formation ou de tout document attestant d'une qualification professionnelle, délivrés par l'autorité compétente d'un État membre de l’UE ou de l’EEE,
  - la preuve par tout moyen que l'intéressé a exercé des fonctions de professeur de danse pendant au moins l’équivalent de une année à temps plein ou à temps partiel pendant une durée équivalente au cours des dix années précédant la demande. Cette preuve n'est pas requise lorsque la formation conduisant à cette profession est réglementée dans l'État de l’UE ou de l’EEE dans lequel elle a été validée,
  - le contenu des études et des stages suivis pendant la formation initiale indiquant le nombre d'heures par matière pour les enseignements théoriques, la durée des stages, le domaine dans lequel ils ont été effectués ainsi que, s'il y a lieu, le résultat des évaluations réalisées ; le cas échéant, le relevé des stages de formation continue indiquant le contenu et la durée de ces stages. Ces éléments sont délivrés et attestés par la structure de formation concernée.

##### Coût

Gratuit.

*Pour aller plus loin* : articles 5 et suivants de l’arrêté du 23 décembre 2008 précité et articles L. 362-1-1 et suivants du Code de l’éducation.

#### Le cas échéant, demander une reconnaissance d'équivalence au diplôme d'État de professeur de danse

Cette demande permet d'obtenir l'équivalence d'un diplôme au diplôme d’État de professeur de danse.

##### Autorité compétente

La demande doit être adressée à la direction générale de la création artistique.

##### Délai de réponse

La réponse à la demande est notifiée dans un délai de dix mois à compter de la date de l'accusé de réception de celle-ci. La décision de refus doit être motivée.

##### Pièces justificatives

Le demandeur doit joindre à sa demande de reconnaissance d’équivalence un dossier comprenant :

- le formulaire Cerfa 10449*03 rempli, daté et signé ;
- la copie du diplôme de professeur de danse pour lequel la reconnaissance d’équivalence est demandée ;
- le curriculum vitae du demandeur ;
- toute pièce officielle décrivant la formation conduisant au diplôme : descriptif complet de la formation précisant l’organisation, la planification, le volume horaire et le programme étudié pour chaque matière, les conditions d’entrée en formation, le mode d’évaluation des enseignements, les modalités de délivrance du diplôme, etc. L’ensemble de ces documents est établi par l’établissement d’enseignement ayant délivré le diplôme.

**À noter**

Toute pièce écrite en langue étrangère doit être accompagnée de sa traduction en français par un traducteur agréé.

##### Coût

Gratuit

*Pour aller plus loin* : articles L. 362-1, article L. 362-4 du Code de l’éducation et l’article 25 de l’arrêté du 20 juillet 2015 relatif aux différentes voies d'accès à la profession de professeur de danse en application de l'article L. 362-1 du Code de l'éducation. 

#### Demander une dispense du diplôme d’État de professeur de danse

La dispense de diplôme peut être accordée dans deux cas.

***Si la personne a enseigné la danse depuis plus de trois ans au 11 juillet 1989***

##### Autorité compétente

La demande de dispense doit être adressée à la direction régionale des affaires culturelles du lieu de domiciliation du demandeur.

##### Délai de réponse

En cas d'absence de réponse dans les dix mois, la dispense est considérée comme accordée.

##### Pièces justificatives

Le demandeur doit joindre à sa demande de dispense un dossier comprenant :

- le formulaire Cerfa 10446*03 complété, daté et signé ;
- le curriculum vitae du demandeur (retraçant sa formation et ses activités professionnelles) ;
- la photocopie d’une pièce d’identité (ou de la carte de séjour pour les Etrangers) ;
- l’attestation de domicile (quittance de loyer, facture EDF-GDF, etc.) ;
- pour les professeurs salariés : un certificat établi par l’employeur attestant la (ou les) technique(s) enseignée(s) et le nombre d’heures d’enseignement effectuées dans cette (ou ces) technique(s) durant les trois années antérieures au 10 juillet 1989 (le nombre d’heures ne peut être inférieur à 100 par an) ;
- pour les professeurs non-salariés : un justificatif de la (ou des) technique(s) enseignée(s) au cours des trois années considérées et la feuille d’inscription à la caisse de retraite de l’enseignement et des arts appliqués (Crea).

**À noter**

Toute pièce écrite en langue étrangère doit être accompagnée de sa traduction en français par un traducteur agréé.

##### Coût

Gratuit.

*Pour aller plus loin* : article L. 362-4 du Code de l’éducation.

***Si la personne dispose d’une renommée particulière ou d’une expérience confirmée***

##### Autorité compétente

La demande de dispense doit être adressée à la direction générale de la création artistique.

##### Délai de réponse

En cas d'absence de réponse dans les dix mois, la dispense est considérée comme accordée.

##### Pièces justificatives

Le demandeur doit joindre à sa demande de dispense un dossier comprenant :

- le formulaire Cerfa 10450*03 complété, daté et signé ;
- la photocopie d’une pièce officielle d’identité ou de la carte de séjour pour les étrangers résidant en France ;
- le curriculum vitae détaillé du demandeur mentionnant notamment le suivi de la formation initiale et continue, ainsi que le répertoire dansé ;
- toutes pièces (coupures de presse, contrats, documents pédagogiques, diplômes, etc.) pouvant illustrer le parcours professionnel ;
- le cas échéant, le(s) certificat(s) d’employeur attestant la ou les technique(s) enseignée(s), leurs périodes, leur fréquence, les volumes horaires, etc. ;
- le cas échéant, la feuille d’inscription à la caisse de retraite de l’enseignement et des arts appliqués (Crea) ;
- le cas échéant, la copie de la dispense, pour les candidats ayant exercé une activité d’enseignement de la danse en France.

**À noter**

Toute pièce écrite en langue étrangère doit être accompagnée de sa traduction en français par un traducteur agréé.

##### Coût

Gratuit.

*Pour aller plus loin* : articles L. 362-1, article L. 362-4 du Code de l’éducation et l’article 25 de l’arrêté du 20 juillet 2015 relatif aux différentes voies d'accès à la profession de professeur de danse en application de l'article L. 362-1 du Code de l'éducation. 

### c. Effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité ponctuelle (Libre Prestation de Services)

Le ressortissant d’un État de l’UE ou de l’EEE souhaitant exercer en qualité de professeur de danse de manière temporaire et occasionnelle en France doit en faire la déclaration préalable.

La demande doit être renouvelée annuellement si le prestataire envisage d’exercer son activité au cours de l’année concernée ou en cas de changement matériel de sa situation.

#### Autorité compétente

La déclaration préalable d’activité doit être adressée à la direction générale de la création artistique.

#### Délai de réponse

À la réception du dossier de l’intéressé, un récépissé lui est délivré dans un délai d’un mois et l’informe, le cas échéant, de tout document manquant. Le ministre chargé de la culture statue sur la demande par une décision motivée dans un délai de quatre mois à compter de la réception du dossier complet. Sans réponse dans ce délai, la reconnaissance est considérée comme accordée.

#### Pièces justificatives

Le déclarant doit joindre à sa déclaration un dossier constitué des pièces suivantes :

- le formulaire Cerfa 14531*01 complété, daté et signé ;
- les informations relatives aux contrats d’assurance ou autres moyens de protection personnelle ou collective concernant la responsabilité personnelle (ces documents ne doivent pas dater de plus de trois mois) ;
- une attestation certifiant que le prestataire est légalement établi dans un État membre pour y exercer la profession de professeur de danse dans une ou plusieurs des options (danse classique, contemporaine ou jazz) ;
- la preuve des qualifications professionnelles dans une ou plusieurs des options (danse classique, contemporaine ou jazz) ;
- lorsque la profession de professeur de danse n’est pas réglementée dans l’État membre du lieu d’établissement du prestataire, la preuve que ce dernier a exercé son activité d’enseignement pendant au moins l’équivalent de un année à temps plein au cours des dix années précédant le dépôt de la déclaration. Cette justification n’est pas requise lorsque la formation conduisant à cette profession est réglementée dans l’État de l’UE ou de l’EEE dans lequel elle a été validée.

#### Coût

Gratuit.