﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS053" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Education" -->
<!-- var(title)="Dance teacher" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="education" -->
<!-- var(title-short)="dance-teacher" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/education/dance-teacher.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="dance-teacher" -->
<!-- var(translation)="Auto" -->


Dance teacher
=============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The dance teacher is a professional who teaches dance to a diverse audience (from young children to adults). He practises in both public and private institutions and intervenes in the definition of the pedagogical, educational and artistic components of teaching.

Only the activities of classical dance teacher, contemporary dance and jazz dance are affected by this sheet and by the detailed regulations below.

*To go further:* Articles L. 362-1 and the following of the Education Code.

### b. Competent Business Formality Centre

The relevant business formalities centre (CFE) depends on the nature of the activity and the legal form of the business:

- for the liberal professions, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

For more information, please visit the urssaf, the CCI in Paris, the Strasbourg ICC and the website Service-public.fr

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The person concerned may only teach under the title of dance teacher, or an equivalent title, if he holds certain compulsory diplomas or qualifications.

In order to be able to practise as a dance teacher, the person concerned must, at the choice:

- have obtained a state-issued dance teacher diploma or a certificate of fitness for dance teacher duties;
- have obtained a recognized French or foreign diploma equivalent to the state diploma.

**Good to know**

Some people hold the state-issued dance teacher diploma. These are choreographic artists who have received educational training and justify a professional activity of at least three years at one of the following institutions:

- the ballet of the Paris National Opera;
- the ballets of the theatres of the meeting of the municipal opera theatres of France;
- National choreographic centres;
- companies from a Member State of the European Union (EU) or another State party to the Agreement on the European Economic Area (EEA), the list of which is set by decree of the Minister responsible for culture.

If the person does not have one of these diplomas, he can still teach dance on the condition that he or she has, at your choice:

- an exemption granted because of his particular fame or confirmed experience in teaching dance;
- exemption for those who had been teaching dance for more than three years as of July 11, 1989.

For more information on the issuance of the waiver certificate or on cases of recognition of diploma equivalency, it is advisable to get closer to the Ministry responsible for culture and communication.

*To go further:* Articles L. 362-1 and the following of the Education Code.

### b. Professional Qualifications - European Nationals (LPS or LE)

#### In case of Freedom of establishment

Nationals who are in one of the following situations may legally settle in France to teach dance for a fee, or make use of the title of dance teacher.

- If the person has a diploma or title obtained in an EU or EEA state that regulates access or exercise of the activity, a certificate of competency or training certificate issued by the competent authorities is required;
- If the person has a diploma or title obtained in a third country (outside the EU or EEA), the person concerned must have a training qualification which has been recognised by an EU or EEA state which has enabled him to legally practise in that state for a period of a minimum period of one year, provided that this work experience is certified by the state in which it was acquired;
- If the person has a diploma or title obtained in an EU or EEA state, which does not regulate access or the practice of the profession, the person must have a certificate of competency or a training certificate issued by the authorities State to certify their readiness to practise the profession, provided that the person is justified in carrying out this activity for the equivalent of one year full-time in the last ten years in an EU state or EEA or part-time for an equivalent total period of time. This justification is not required when the training leading to this profession is regulated in the Member State or party to the EEA agreement in which it has been validated.

After examining whether knowledge, skills and skills acquired by the applicant, during his professional experience or lifelong learning and having been properly validated by a competent body, in a Member State or in a third country, are not likely to fill, in whole or in part, substantial differences in training, the Minister responsible for culture may require that the applicant submit to compensation measures, the latter's choice, either as an adjustment course or as an aptitude test. If the applicant holds a certificate of competency within the meaning of Article 11 of directive 2005/36/ CE of September 7, 2005 relating to the recognition of professional qualifications, the Minister may prescribe the adjustment course or the aptitude test.

#### In case of free provision of services (LPS)

A national of an EU or EEA state who wishes to teach dance in France on a temporary and casual basis is deemed to meet the requirements of professional qualifications required on both conditions:

- to be legally established in one of these states to carry out the same activity;
- if the activity or training leading to it is not regulated in the state in which it is established, to have exercised the activity of dance teacher (in one of the classical, contemporary or jazz dance options) for at least the equivalent of one year in time for an equivalent period of time in the ten years prior to the benefit.

If he does fulfil the conditions of professional qualifications, the person concerned must only make a prior declaration of activity before his first performance, in order to be able to practice as a dance teacher in France, in a manner occasional and temporary.

*To go further:* Article 5 of the decree of 23 December 2008 on the conditions of practice of the profession of dance teacher applicable to nationals of a Member State of the European Community or another State party to the agreement on the European Economic Area, amended by the July 25, 2011 order and Section L. 362-1-1 of the Education Code.

#### Partial access

Partial access to a professional activity under the profession of dance teacher, may be granted on a case-by-case basis to nationals of the European Union or a State party to the agreement on the European Economic Area by the Minister responsible for the culture when the following three conditions are met:

- the professional is fully qualified to carry out, in the State of origin, the professional activity for which partial access is requested;
- the differences between the professional activity legally carried out in the State of origin and the regulated profession in France of a dance teacher are so important that the application of compensation measures would be tantamount to imposing on the applicant follow the comprehensive education and training programme required in France to have full access to this profession;
- professional activity can objectively be separated from other activities of the dance teacher profession in France, as it can be carried out independently in the State of origin.

Partial access may be denied for compelling reasons of public interest if this refusal is proportionate to the protection of that interest.

Applications for partial access are considered, as appropriate, as applications for the purpose of establishing or providing temporary and occasional services to the profession.

### c. Conditions of honorability and incompatibility

In order to be able to practise as a dance teacher, the person concerned must not have been the subject of a conviction:

- a sentence of imprisonment without a conditional sentence of more than four months;
- for an offence of rape, sexual assault, sexual assault of a minor or pimping.

*To go further:* Article L. 362-5 of the Education Code and articles 222-22 and following of the Penal Code.

### d. Liability insurance

The dance teacher can take out professional liability insurance. It allows it to be covered for damage to others, whether directly or indirectly at the origin.

**Good to know**

The operator of the dance school is obliged to take out an insurance contract covering the civil liability of the teachers who practice there.

*To go further:* Article L. 462-1 of the Education Code.

### e. Some peculiarities of the regulation of the activity

#### Obligation to verify that students have a medical certificate

The dance teacher must ensure, before the start of each teaching period, that the students do have a medical certificate attesting to the absence of contraindication to the teaching provided to them. This certificate must be renewed every year. At the request of any teacher, a certificate attesting to an additional medical examination may be required.

*To go further:* Article R. 362-2 of the Education Code.

#### Teaching children about dance

Most often practising alongside minors, dance teachers are subject to the following obligations:

- four- and five-year-olds can only engage in physical arousal activities (i.e. excluding all techniques specific to the discipline taught);
- five- and six-year-olds can only engage in an introductory activity in the chosen discipline (i.e. exclude all techniques specific to the discipline being taught).

In general, all activities performed by children aged four to seven years included must not involve any burdensome work for the body, excessive extensions or forced joints.

*To go further:* Article R. 362-1 of the Education Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, it is advisable to refer to the activity sheets "Formality of reporting a commercial company" or "Registration of an individual company in the register of trade and companies."

### b. If necessary, apply for recognition of professional qualifications or diploma waiver

#### Request recognition of professional qualifications

Nationals of an EU or EEA state who have obtained their professional qualifications in one of these states and who wish to settle in France to practise as a dance teacher, must apply for recognition of their qualifications in one or more options (classical, contemporary, jazz).

**Competent authority**

The request for recognition of professional qualifications must be addressed to the Directorate General of Artistic Creation.

**Timeframe**

The Directorate General of Artistic Creation issues a receipt within one month after receiving the application for recognition of professional qualifications.

The Minister responsible for culture decides on the application for recognition of professional qualifications by a reasoned decision within four months of receipt of the complete file.

**Issues of demand**

After reviewing the application, the Minister for Culture may, at your choice:

- issue a certificate of recognition of professional qualifications to the applicant;
- offer the applicant to undergo either an aptitude test or an adjustment course if he or she sees substantial differences in training.

If there is no response within four months, the application for recognition is considered to be granted.

**Supporting documents**

Applicants must attach a file consisting of the Cerfa 14531 form to his application for recognition of professional qualifications.and supporting documents, translated into French, if any. These exhibits vary depending on the applicant's situation:

- if the applicant holds a certificate of competency or a training certificate issued by the competent authorities of an EU or EEA state which regulates access to the profession or its practice and which allows the profession to be legally practised in State:- A copy of the ID,
  - The description of the applicant's professional experience,
  - A copy of certificates of professional competence, training documents or any document attesting to a professional qualification, issued by the competent authority and allowing the profession to be legally practised,
  - the content of the studies and internships taken during the initial training, indicating the number of hours per subject for the theoretical teachings, the duration of the internships, the field in which they were carried out and, if applicable, the result of the evaluations carried out; if necessary, the survey of continuing education courses showing the content and duration of these internships. These elements are issued and attested by the training structure concerned;
- if the applicant holds a training degree issued by a third state that has been recognised by an EU or EEA state and which has allowed the profession to be practised in that state for at least three years:- A copy of the ID,
  - The description of the applicant's professional experience,
  - copying the recognition by an EU or EEA state of the training certificate issued by a third state,
  - a copy of the document certifying the exercise by the applicant of the profession of dance teacher for a minimum of three years in the state that has recognized the training title,
  - the content of the studies and internships taken during the initial training indicating the number of hours per subject for the theoretical teachings, the duration of the internships, the field in which they were carried out and, if applicable, the results of the evaluations achieved; if necessary, the survey of continuing education courses showing the content and duration of these internships. These elements are issued and attested by the training structure concerned. Where the minimum period of one year has not been carried out in the state that has recognized the diploma, certificate or title, the holder must be recognized as qualified by the Minister responsible for culture in view of the knowledge and qualifications attested by that diploma, certificate or title and by all the training and professional experience acquired;
- if the applicant holds a certificate of competency or a training certificate issued by the competent authorities of an EU or EEA state which does not regulate access or the practice of the profession and the applicant justifies the exercise of this full-time activity for one year over the past ten years:- A copy of the ID,
  - The description of the applicant's professional experience,
  - A copy of certificates of professional competence, training documents or any document attesting to a professional qualification, issued by the competent authority of an EU or EEA Member State,
  - evidence by any means that the person has served as a dance teacher for at least the equivalent of one year full-time or part-time for an equivalent period of time in the ten years prior to the application. This evidence is not required when training leading to this occupation is regulated in the EU or EEA state in which it has been validated,
  - the content of the studies and internships taken during the initial training indicating the number of hours per subject for the theoretical teachings, the duration of the internships, the field in which they were carried out and, if applicable, the results of the evaluations achieved; if necessary, the survey of continuing education courses showing the content and duration of these internships. These elements are issued and attested by the training structure concerned.

**Cost**

Free.

*To go further:* Articles 5 and following of the order of December 23, 2008 and articles L. 362-1-1 and following of the Education Code.

#### If necessary, apply for recognition of equivalency in the state diploma of dance teacher

This application provides an equivalency of a diploma with the state diploma of dance teacher.

**Competent authority**

The request must be addressed to the Directorate General of Artistic Creation.

**Response time**

The response to the request is notified within ten months of the date of the request's acknowledgement. The decision to refuse must be justified.

**Supporting documents**

Applicants must attach a file to their application for equivalency recognition that includes:

- Form Cerfa 1044903 filled, dated and signed;
- A copy of the dance teacher diploma for which recognition of equivalence is required;
- The applicant's resume
- any official exhibit describing the training leading to the diploma: a complete description of the training detailing the organization, planning, time volume and curriculum studied for each subject, the conditions of entry into training, the mode assessment of teachings, how the diploma is issued, etc. All of these documents are prepared by the educational institution that has issued the diploma.

**Please note**

Any piece written in a foreign language must be accompanied by a certified translator.

**Cost**

Free

*To go further:* Articles L. 362-1, Section L. 362-4 of the Education Code and Section 25 of the July 20, 2015 Order on Different Pathways to the Dance Teacher Profession under Section L. 362-1 of the Education Code.

#### Apply for exemption from the state diploma of dance teacher

The diploma waiver may be granted in two cases.

***If the person has been teaching dance for more than three years as of July 11, 1989***

**Competent authority**

The request for exemption must be addressed to the regional directorate of cultural affairs of the applicant's place of residence.

**Response time**

If there is no response within ten months, the exemption is considered granted.

**Supporting documents**

Applicants must attach a file to their application for exemption that includes:

- Form Cerfa 1044603 completed, dated and signed;
- The applicant's resume (tracing his training and professional activities);
- Photocopying of an ID (or residence card for foreigners);
- Proof of residence (rent release, EDF-GDF invoice, etc.);
- for salaried teachers: a certificate issued by the employer attesting to the technical (s) taught and the number of hours of instruction worked in this (or these) technical (or these) years prior to 10 July 1989 (the number of hours cannot be less than 100 per year);
- for non-salary teachers: a proof of the technical (or) taught over the three years under review and the registration sheet to the Education and Applied Arts Pension Fund (Crea).

**Please note**

Any piece written in a foreign language must be accompanied by a certified translator.

**Cost**

Free.

*To go further:* Article L. 362-4 of the Education Code.

***If the person has a particular reputation or confirmed experience***

**Competent authority**

The request for exemption must be addressed to the Directorate General of Artistic Creation.

**Response time**

If there is no response within ten months, the exemption is considered granted.

**Supporting documents**

Applicants must attach a file to their application for exemption that includes:

- Form Cerfa 1045003 completed, dated and signed;
- Photocopying an official ID or residence card for foreigners residing in France;
- The applicant's detailed resume, including follow-up to initial and continuing training, as well as the dance directory;
- all documents (press clippings, contracts, educational documents, diplomas, etc.) that can illustrate the career path;
- If applicable, the employer's certificate attesting to the technique taught, their periods, frequency, hourly volumes, etc.
- If applicable, the registration sheet to the Education and Applied Arts Pension Fund (Crea);
- if necessary, a copy of the exemption, for candidates who have worked in teaching dance in France.

**Please note**

Any piece written in a foreign language must be accompanied by a certified translator.

**Cost**

Free.

*To go further:* Articles L. 362-1, Section L. 362-4 of the Education Code and Section 25 of the July 20, 2015 Order on Different Pathways to the Dance Teacher Profession under Section L. 362-1 of the Education Code.

### c. Make a prior declaration of activity for EU nationals engaged in a one-off activity (free provision of services)

A national of an EU or EEA state wishing to practise as a dance teacher on a temporary and occasional basis in France must make a prior declaration.

The application must be renewed annually if the claimant plans to carry out his activity during the year concerned or in the event of a material change in his situation.

**Competent authority**

The prior declaration of activity must be addressed to the Directorate General of Artistic Creation.

**Response time**

Upon receipt of the person's file, a receipt is issued within one month and informs him, if necessary, of any missing documents. The Minister responsible for culture decides on the application by a reasoned decision within four months of receiving the full file. Without a response within this time frame, recognition is considered to be granted.

**Supporting documents**

The registrant must attach to his statement a file consisting of the following documents:

- Form Cerfa 14531Completed, dated and signed;
- information about insurance contracts or other means of personal or collective protection regarding personal liability (these documents must not be more than three months old);
- a certificate certifying that the claimant is legally established in a Member State to practise as a dance teacher in one or more of the options (classical, contemporary or jazz dance);
- proof of professional qualifications in one or more of the options (classical, contemporary or jazz dance);
- where the profession of dance teacher is not regulated in the Member State of the claimant's place of establishment, evidence that the claimant has been teaching for at least the equivalent of one year full-time in the ten years prior to filing the return. This justification is not required when training leading to this occupation is regulated in the EU state or EEA in which it has been validated.

**Cost**

Free.

