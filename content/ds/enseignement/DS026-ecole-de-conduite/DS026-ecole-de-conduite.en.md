﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS026" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Road safety" -->
<!-- var(title)="Driving school" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="road-safety" -->
<!-- var(title-short)="driving-school" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/road-safety/driving-school.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="driving-school" -->
<!-- var(translation)="Auto" -->


Driving school
==============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The driving school is a driving and road safety educational institution. Its operator is in charge of its administrative, legal and tax management.

He recruits and coordinates the activity of driving and road safety teachers ("car school instructor"). The operator is not necessarily a driving instructor, but he may be.

In accordance with the provisions of Section D. 114-12 of the Public Relations Code and the Administration, any user may obtain an information certificate on the standards applicable to the professions relating to the teaching of expensive driving and road safety awareness. To do so, you should send your application to the following address: bfper-dsr@interieur.gouv.fr.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for a liberal profession, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The operation of a driving school is reserved for holders of prefectural accreditation.

The latter is obtained if the person concerned meets the following conditions:

- conditions of honourability (see infra "2. c. Conditions of honorability");
- Be at least 23 years old
- justify the ability to manage an institution by holding either:- a state diploma, a higher or technological degree or a degree at least equal to a level III (legal, economic, accounting or commercial) or a comparable foreign diploma,
  - Certificate of Professional Qualification (CQP) "responsible for road safety and driving education units"
  - a professional qualification acquired in a Member State of the European Union (EU) or party to the agreement on the European Economic Area (EEA);
- justify minimum guarantees regarding the institution's means of training. These guarantees cover premises, vehicles, material means and how training is organised;
- to justify the professional qualification of teachers who must be licensed to practice.

*For further information*: Articles L. 213-1 and R. 213-2 of the Highway Traffic Act; 13 September 2017 decree on the recognition of qualifications acquired in a Member State of the European Union or in another State party to the European Economic Area Agreement by persons wishing to practise in the professions regulated road education

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

#### For a temporary and casual exercise (LPS)

There are no regulations for a national of an EU or EEA who wishes to practise as a driving school operator in France, either on a temporary or casual basis.

Therefore, only the measures taken for the Freedom of establishment of EU or EEA nationals (see infra "3°. c. Request a prefectural approval") will find to apply.

#### For a permanent exercise (LE)

Any national of an EU or EEA state who is established and legally operates a driving school in that state may carry out the same activity in France on a permanent basis if he:

- holds a certificate issued by the competent authority of an EU or EEA state that regulates access to or exercise of the profession;
- has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

Once the national fulfils one of these conditions, he or she must first apply for recognition of his professional qualifications from the prefect of the department where he plans to open his establishment (see infra "3o). b. Request recognition of the EU or EEA national's professional qualifications for a permanent exercise (LE) ").

Recognition of his professional qualifications will allow him to apply for administrative approval from the same competent authority (see infra "3o). c. Request prefectural approval").

However, if the examination of the professional qualifications attested by the training credentials and the work experience shows substantial differences with the qualifications required for access to the profession and its exercise in France, the person concerned will have to submit to a compensation measure.

*For further information*: Article R. 213-2-1 of the Highway Traffic Act; 13 September 2017 decree on the recognition of qualifications acquired in a Member State of the European Union or in another State party to the European Economic Area Agreement by persons wishing to practise in the professions regulated road education.

### c. Conditions of honorability

The operator of a driving school must meet conditions of honour and justify:

- not having been sentenced to a criminal or correctional sentence under section R. 212-4 of the Highway Traffic Act;
- have not been the subject of a licence withdrawal in the last three years.

**Please note**

Operating a driving school without administrative approval is punishable by one year's imprisonment and a fine of 15,000 euros.

*For further information*: Articles L. 213-3 and L. 213-6 and R. 212-4 of the Highway Traffic Act.

### d. Some peculiarities of the regulation of the activity

#### Justifying the qualification of teachers

Driving and road safety teachers must be licensed to teach as long as they are involved in theoretical and practical education.

Permission is issued to the professional who justifies:

- Have a valid driver's licence and an expired probationary period, valid for the categories of vehicles he wishes to teach driving;
- hold a title or diploma as a driving and road safety teacher or, depending on the conditions, be in training for its preparation. In the latter case, the authorization will be issued on a temporary and restrictive basis;
- If applicable, recognition of professional qualifications for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement;
- Be at least 20 years old
- meet the physical, cognitive and sensory skills requirements required for driver's licences in categories C1, C, D1, D, C1E, CE, D1E and DE. To do so, the individual will need to obtain the advice of a medical officer.

*For further information*: Article L. 212-2 of the Highway Traffic Act.

#### Compliance with safety and accessibility standards

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERP) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

It is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) for more information.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

#### Obligation to take out professional liability insurance

The operator is required to take out professional liability insurance covering him and his employees for any damage caused to third parties by acts carried out during their professional activity.

In addition, it will also be required to insure vehicles in case of damage caused by students at the school to third parties during driving instruction.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The contractor must register with the Trade and Corporate Register (SCN). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### b. Request recognition of professional qualifications for EU or EEA nationals for permanent exercise (LE)

**Competent authority**

The prefect of the department where the national wishes to settle is competent to decide on the application for recognition of professional qualifications.

**Supporting documents**

The application is made by filing a file that includes the following supporting documents:

- The request for recognition, dated and signed;
- A photocopy of an ID
- An identity photograph less than six months old
- proof of residence of less than six months
- Photocopying of certificates of competency or training documents obtained for the exercise of the activity of operating a driving school;
- A statement regarding his knowledge of the French language for the practice of the profession concerned;
- if applicable, a certificate of competency or a training document justifying that the national has been active for at least one year in the last ten years in a state that does not regulate the training or its exercise.

**Procedure**

The prefect acknowledges receipt of the file within one month and has two months from that same date to decide on the application.

In the event of substantial differences between the training he has received and that required to carry out this activity in France, the prefect may submit him to an aptitude test or an adaptation course.

The aptitude test, carried out within a maximum of six months, is in the form of an interview before the jury of the certificate of professional qualification "responsible for teaching road safety and driving". The adaptation course is carried out for a minimum of two months in an accredited driving school.

After checking the documents in the file and, if necessary, after receiving the results of the chosen compensation measure, the prefect will issue recognition of qualifications whose model is specified in the[Annex](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=D3225762F0A0C9E9EA272A874A63EF2F.tplgfr42s_2?cidTexte=JORFTEXT000035589366&dateTexte=20170920) September 13, 2017.

*For further information*: decree of 13 September 2017 relating to the recognition of qualifications acquired in a Member State of the European Union or in another State party to the agreement on the European Economic Area by persons wishing to practise the professions regulated road education.

### c. Request prefectural approval

**Competent authority**

The prefect of the department of the place of the driving school is competent to decide on the application for accreditation of the national.

**Supporting documents**

The application is made by filing a file that includes the following supporting documents:

- The accreditation form to be obtained from the prefecture of the department;
- A valid piece of identification
- Proof of residence
- An identity photograph
- A document justifying the ability to manage an institution
- the justification for the declaration of the territorial economic contribution or, failing that, a declaration of registration with the Urssaf;
- The name and quality of the establishment (social reason, Siren or Siret number, etc.);
- Photocopying the title or lease of the premises
- The plan and a description of the activity room
- the justification of the ownership or rental of the teaching vehicle or vehicles, as well as the insurance certificate covering damages caused to third parties with these vehicles;
- list of driving and road safety teachers with their permission to teach.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

Upon receipt of the file, the prefect will have one month to inform the professional of any missing documents. The accreditation is issued for five years renewable. In the event of a renewal, the professional must apply at least two months before it expires.

*For further information*: order of 8 January 2001 relating to the operation of educational institutions, for a fee, of motor vehicle driving and road safety.

### d. Authorisation (s) post-installation

#### If necessary, obtain permission to teach

**Competent authority**

The prefect of the department where the teacher resides or the one in which he wishes to practice (for applicants residing abroad) is competent to issue the authorization to teach.

**Supporting documents**

The professional must attach the following supporting documents to the application:

- The application for teaching permission completed, dated and signed;
- Two photo IDs
- A photocopy of an ID
- if he is a foreign national not belonging to a European Union state or a party to the agreement on the European Economic Area, the justification that he is in good standing with regard to legislation and regulations concerning foreigners in France;
- Proof of residence
- Photocopying of certificates of competency or training documents obtained for the exercise of the driving teaching activity;
- If necessary, recognition of professional qualifications;
- a medical certificate issued by a licensed physician.

The authorization is issued for a renewable five-year term. Two months before its expiry, the professional must apply for renewal by submitting the same supporting documents to the prefect of the department.

*For further information*: Articles R. 212-1 and the following of the Highway Traffic Act; order of 8 January 2001 relating to the authorisation to teach, for a fee, the driving of motor vehicles and road safety.

**Temporary and restrictive authorisation to teach**

Temporary and restrictive authorization is granted to the person who is said to be in the process of training in the driving profession. The prefect of the department where the driving school with which the applicant plans to practise is located is competent to issue the authorization.

In addition to meeting the above conditions, it will also have to:

- Have signed an employment contract with a licensed driving and road safety education institution;
- Hold one of the professional competency certificates that make up the professional designation issued by the Minister for Employment;
- be enrolled in an examination session to complete the validation of the skills required to obtain the title of driving and road safety teacher.

**What to know**

This authorization is only granted for twelve months.

*For further information*: Article R. 212-2 I bis of the Highway Traffic Act and order of 13 April 2016 relating to the temporary and restrictive authorization to practise referred to in Article R. 212-1 of the Highway Traffic Act.

