﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS026" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseignement" -->
<!-- var(title)="École de conduite" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="enseignement" -->
<!-- var(title-short)="ecole-de-conduite" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/enseignement/ecole-de-conduite.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="ecole-de-conduite" -->
<!-- var(translation)="None" -->

# École de conduite

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L'école de conduite est un établissement d'enseignement de la conduite et de la sécurité routière. Son exploitant est en charge de sa gestion administrative, juridique et fiscale.

Il recrute et coordonne l'activité des enseignants de la conduite et de la sécurité routière (« moniteur d'auto-école »). L'exploitant n'est pas obligatoirement moniteur d'auto-école, mais il peut l'être.

Conformément aux dispositions de l'article D. 114-12 du Code des relations entre le public et l'Administration, tout usager peut obtenir un certificat d'information sur les normes applicables aux professions relatives à l'enseignement de la conduite à titre onéreux et la sensibilisation à la sécurité routière. Pour ce faire, il convient d'adresser votre demande à l'adresse suivante : bfper-dsr@interieur.gouv.fr.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une profession libérale, le CFE compétent est l'Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d'industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

L'exploitation d'une école de conduite est réservée aux titulaires d'un agrément préfectoral.

Ce dernier est obtenu dès lors que l'intéressé remplit les conditions suivantes :

- respecter les conditions d'honorabilité (cf. infra « 2°. c. Conditions d'honorabilité ») ;
- être âgé d'au moins 23 ans ;
- justifier de la capacité à gérer un établissement en étant titulaire soit :
  - d'un diplôme d’État, d'un titre ou d'un diplôme de l'enseignement supérieur ou technologique au moins égal à un niveau III (formation juridique, économique, comptable ou commerciale) ou d'un diplôme étranger comparable,
  - du certificat de qualification professionnelle (CQP) « responsable d'unité(s) d'enseignement de la sécurité routière et de la conduite »,
  - d'une qualification professionnelle acquise dans un État membre de l’Union Européenne (UE) ou partie à l'accord sur l’Espace économique européen (EEE) ;
- justifier de garanties minimales concernant les moyens de formation de l'établissement. Ces garanties concernent les locaux, les véhicules, les moyens matériels et les modalités d'organisation de la formation ;
- justifier de la qualification professionnelle du personnel enseignant qui doit être titulaire de l'autorisation d'exercer.

*Pour aller plus loin* : articles L. 213-1 et R. 213-2 du Code de la route ; arrêté du 13 septembre 2017 relatif à la reconnaissance des qualifications acquises dans un État membre de l'Union européenne ou dans un autre Etat partie à l'accord sur l'Espace économique européen par les personnes souhaitant exercer les professions réglementées de l'éducation routière 

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### Pour un exercice temporaire et occasionnel (LPS)

Aucune disposition réglementaire n'est prévue pour le ressortissant d'un État de l'UE ou de l'EEE souhaitant exercer la profession d'exploitant d'une école de conduite en France, à titre temporaire ou occasionnel.

Dès lors, seules les mesures prises pour le libre établissement des ressortissants de l'UE ou de l'EEE (cf. infra « 3°. c. Demander un agrément préfectoral ») trouveront à s'appliquer.

#### Pour un exercice permanent (LE)

Tout ressortissant d'un État de l'UE ou de l'EEE qui est établi et exerce légalement l'activité d'exploitant d'une école de conduite dans cet État peut exercer la même activité en France de manière permanente s’il :

- est titulaire d'une attestation délivrée par l'autorité compétente d'un État de l'UE ou de l'EEE qui réglemente l'accès à la profession ou son exercice ;
- a exercé la profession à temps plein ou à temps partiel pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation ni l'exercice de la profession.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant devra au préalable demander la reconnaissance de ses qualifications professionnelles auprès du préfet du département où il envisage d'ouvrir son établissement (cf. infra « 3°. b. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE) »).

La reconnaissance de ses qualifications professionnelles lui permettra de demander, par la suite, l'agrément administratif auprès de la même autorité compétente (cf. infra « 3°. c. Demander un agrément préfectoral »).

Toutefois, si l’examen des qualifications professionnelles attestées par les titres de formation et l’expérience professionnelle fait apparaître des différences substantielles avec les qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé devra se soumettre à une mesure de compensation.

*Pour aller plus loin* : article R. 213-2-1 du Code de la route ; arrêté du 13 septembre 2017 relatif à la reconnaissance des qualifications acquises dans un État membre de l'Union européenne ou dans un autre Etat partie à l'accord sur l'Espace économique européen par les personnes souhaitant exercer les professions réglementées de l'éducation routière.

### c. Conditions d'honorabilité

L'exploitant d'une école de conduite doit respecter des conditions d'honorabilité et justifier :

- ne pas avoir fait l'objet d'une condamnation à une peine criminelle ou correctionnelle visée à l'article R. 212-4 du Code de la route ;
- ne pas avoir fait l'objet d'un retrait d'agrément dans les trois dernières années.

**À noter**

Le fait d'exploiter une école de conduite sans avoir obtenu l'agrément administratif est puni d'un an d'emprisonnement et de 15 000 euros d'amende.

*Pour aller plus loin* : articles L. 213-3 et L. 213-6 et R. 212-4 du Code de la route.

### d. Quelques particularités de la réglementation de l'activité

#### Justifier de la qualification du personnel enseignant

Les enseignants de la conduite et de la sécurité routière doivent être titulaire de l'autorisation d'enseigner dès lors qu'ils s'occupent de l'enseignement théorique et pratique.

L'autorisation est délivrée au professionnel qui justifie :

- être titulaire du permis de conduire en cours de validité et dont le délai probatoire est expiré, valable pour les catégories de véhicules dont il souhaite enseigner la conduite ;
- être titulaire d'un titre ou d'un diplôme d'enseignant de la conduite et de la sécurité routière ou, selon conditions, être en cours de formation pour sa préparation. Dans ce dernier cas, l'autorisation sera délivrée à titre temporaire et restrictive ;
- le cas échéant, de la reconnaissance de qualifications professionnelles pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) ;
- être âgé d'au moins 20 ans ;
- remplir des conditions d'aptitudes physiques, cognitives et sensorielles requises pour les permis de conduire des catégories C1, C, D1, D, C1E, CE, D1E et DE. Pour cela, l'intéressé devra obtenir l'avis d'un médecin agréé.

*Pour aller plus loin* : article L. 212-2 du Code de la route.

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

Il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » pour de plus amples informations.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

#### Obligation de souscrire une assurance de responsabilité civile professionnelle

L'exploitant est tenu de souscrire une assurance de responsabilité civile professionnelle le couvrant lui et ses employés de tous dommages causés à des tiers par des actes effectués à l'occasion de leur activité professionnelle.

De plus, il devra également assurer les véhicules en cas de dommages causés par des élèves de l'établissement à des tiers au cours de l'enseignement de la conduite.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

L’entrepreneur doit s’immatriculer au registre du commerce et des sociétés (RCS).

### b. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

**Autorité compétente** 

Le préfet du département où le ressortissant souhaite s'établir est compétent pour se prononcer sur la demande de reconnaissance de qualifications professionnelles.

**Pièces justificatives**

La demande se fait par le dépôt d'un dossier comprenant les pièces justificatives suivantes :

- la demande de reconnaissance, datée et signée ;
- une photocopie d'une pièce d'identité ;
- une photographie d'identité datant de moins de six mois ;
- un justificatif de domicile de moins de six mois ;
- la photocopie des attestations de compétences ou titres de formation obtenus pour l'exercice de l'activité d'exploitant d'une école de conduite ;
- une déclaration concernant sa connaissance de la langue française pour l'exercice de la profession concernée ;
- le cas échéant, une attestation de compétences ou un titre de formation justifiant que le ressortissant a exercé l'activité pendant au moins un an au cours des dix dernières années dans un État qui ne réglemente ni la formation ni son exercice.

**Procédure**

Le préfet accuse réception du dossier dans un délai d'un mois et dispose d'un délai de deux mois à compter de cette même date pour se prononcer sur la demande.

En cas de différences substantielles entre la formation qu'il a reçue et celle requise pour exercer cette activité en France, le préfet pourra le soumettre à une épreuve d'aptitude ou un stage d'adaptation.

L'épreuve d'aptitude, réalisée dans un délai maximal de six mois, se présente sous la forme d'un entretien devant le jury du certificat de qualification professionnelle « responsable d'unité(s) d'enseignement de la sécurité routière et de la conduite ». Le stage d'adaptation est quant à lui est effectué pendant deux mois au minimum dans une école de conduite agréée.

Après vérification des pièces du dossier et, le cas échéant, après avoir reçu les résultats de la mesure de compensation choisie, le préfet délivrera la reconnaissance de qualifications dont un modèle est précisé dans l'annexe de l'arrêté du 13 septembre 2017.

*Pour aller plus loin* : arrêté du 13 septembre 2017 relatif à la reconnaissance des qualifications acquises dans un État membre de l'Union européenne ou dans un autre État partie à l'accord sur l'Espace économique européen par les personnes souhaitant exercer les professions réglementées de l'éducation routière.

### c. Demander un agrément préfectoral

**Autorité compétente**

Le préfet du département du lieu de l'école de conduite est compétent pour se prononcer sur la demande d'agrément du ressortissant.

**Pièces justificatives**

La demande se fait par le dépôt d'un dossier comprenant les pièces justificatives suivantes :

- le formulaire d'agrément à se procurer auprès de la préfecture du département ;
- une pièce d'identité en cours de validité ;
- un justificatif de domicile ;
- une photographie d'identité ;
- un document justifiant de la capacité à gérer un établissement ;
- la justification de la déclaration de la contribution économique territoriale ou, à défaut, une déclaration d'inscription à l'Urssaf ;
- le nom et la qualité de l'établissement (raison sociale, numéro Siren ou Siret, etc.) ;
- la photocopie du titre de propriété ou du bail de location du local ;
- le plan et un descriptif du local d'activité ;
- la justification de la propriété ou de la location du ou des véhicules d'enseignement, ainsi que l'attestation d'assurance couvrant les dommages résultant d'accidents causés à des tiers avec ces véhicules ;
- la liste des enseignants de la conduite et de la sécurité routière avec leur autorisation d'enseigner.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

**Procédure** 

À la réception du dossier, le préfet aura un mois pour informer le professionnel de tout document manquant. L'agrément est délivré pour cinq ans renouvelable. En cas de renouvellement, le professionnel devra faire sa demande au moins deux mois avant son expiration.

*Pour aller plus loin* : arrêté du 8 janvier 2001 relatif à l'exploitation des établissements d'enseignement, à titre onéreux, de la conduite des véhicules à moteur et de la sécurité routière.

### d. Autorisation(s) post-installation

#### Le cas échéant, obtenir une autorisation d'enseigner

**Autorité compétente** 

Le préfet du département où l'enseignant réside ou celui dans lequel il souhaite exercer (pour les demandeurs résidant à l'étranger) est compétent pour délivrer l'autorisation d'enseigner.

**Pièces justificatives** 

Le professionnel doit joindre à la demande, les pièces justificatives suivantes :

- la demande d'autorisation d'enseigner remplie, datée et signée ;
- deux photos d'identité ;
- une photocopie d'une pièce d'identité ;
- s'il est ressortissant étranger n'appartenant pas à un Etat de l'Union européenne ou partie à l'accord sur l'Espace économique européen, la justification qu'il est en règle à l'égard de la législation et de la réglementation concernant les étranger en France ;
- un justificatif de domicile ;
- la photocopie des attestations de compétences ou titres de formation obtenus pour l'exercice de l'activité d'enseignement de la conduite ;
- le cas échéant, la reconnaissance de qualifications professionnelles ;
- un certificat médical délivré par un médecin agréé.

L'autorisation est délivrée pour une durée de cinq ans renouvelable. Deux mois avant son expiration, le professionnel devra en solliciter le renouvellement en transmettant les mêmes pièces justificatives au préfet de département.

*Pour aller plus loin* : articles R. 212-1 et suivants du Code de la route ; arrêté du 8 janvier 2001 relatif à l'autorisation d'enseigner, à titre onéreux, la conduite des véhicules à moteur et la sécurité routière.

**Autorisation temporaire et restrictive d'enseigner**

L'autorisation temporaire et restrictive est accordée à l'intéressé qui serait en cours de formation à la profession d'enseignant de la conduite. Le préfet du département où se situe l'école de conduite avec lequel le demandeur envisage d'exercer est compétent pour délivrer l'autorisation.

Outre remplir les conditions ci-dessus, il devra également :

- avoir souscrit un contrat de travail avec un établissement agréé d'enseignement de la conduite et de la sécurité routière ;
- être titulaire d'un des certificats de compétences professionnelles composant le titre professionnel délivré par le ministre chargé de l'emploi ;
- être inscrit à une session d'examen permettant de compléter la validation des compétences nécessaires à l'obtention du titre d'enseignant de la conduite et de la sécurité routière.

**À savoir**

Cette autorisation n'est accordée que pour douze mois.

*Pour aller plus loin* : article R. 212-2 I bis du Code la route et arrêté du 13 avril 2016 relatif à l'autorisation temporaire et restrictive d'exercer mentionnée à l'article R. 212-1 du Code de la route.