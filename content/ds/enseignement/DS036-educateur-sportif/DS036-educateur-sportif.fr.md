﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS036" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseignement" -->
<!-- var(title)="Educateur sportif" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="enseignement" -->
<!-- var(title-short)="educateur-sportif" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/enseignement/educateur-sportif.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="educateur-sportif" -->
<!-- var(translation)="None" -->

# Educateur sportif

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’éducateur sportif est un professionnel qui enseigne, anime et encadre une activité physique ou sportive contre rémunération. Il peut également entraîner des pratiquants de façon habituelle, saisonnière ou occasionnelle.

Il est responsable de la sécurité du public dont il a la charge et veille au bon état du matériel utilisé.

L’éducateur sportif s’adapte aux caractéristiques de son public, il donne des indications, corrige les gestes et les postures pour l’aider à progresser.

*Pour aller plus loin* : article L. 212-1 du Code du sport.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- en cas de création d’une entreprise individuelle, le CFE compétent est l’Urssaf ;
- en cas création d’une société commerciale, le CFE compétent est la chambre de commerce et d’industrie (CCI) ;
- en cas de création d’une société civile, le CFE compétent est le greffe du tribunal de commerce (une exception étant prévue pour les départements du Bas-Rhin, du Haut-Rhin et de la Moselle dans lesquels l’intéressé doit s’enregistrer au greffe du tribunal d’instance).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer en qualité d’éducateur sportif, l’intéressé doit être titulaire d’un diplôme, d’un titre à finalité professionnelle ou d’un certificat de qualification :

- garantissant sa compétence en matière de sécurité des pratiquants et des tiers dans l’activité physique ou sportive considérée ;
- enregistré au [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Parmi les diplômes, titres ou certificats de qualification qui remplissent ces deux critères, on peut citer pour exemple :

- le certificat de qualification professionnelle (CQP) ;
- le brevet professionnel de la jeunesse, de l’éducation populaire et du sport (BPJEPS) ;
- le diplôme d’État de la jeunesse, de l’éducation populaire et du sport (DEJEPS).

Chacun d’entre eux est décerné pour une discipline sportive déterminée (escrime, judo-jujitsu, golf, etc.) ou un groupe d’activités (activités équestres, loisirs tous publics, sport automobile, etc.).

La liste des diplômes, titres ou certificats permettant d’exercer en tant qu’éducateur sportif en France est établie à l’annexe II-1 de l’article A212-1 du Code du sport. Cette liste précise également les conditions d’exercice attachées à chaque diplôme (les limites géographiques d’exercice, la catégorie de public que peut encadrer l’intéressé, la nature exacte de l’activité sportive concernée, la durée annuelle maximale d’exercice, etc.).

**Bon à savoir : l’éducateur sportif stagiaire**

Les personnes qui sont en cours de formation menant à l’obtention d’un diplôme, titre ou certificat de qualification peuvent exercer sous l’appellation d’éducateur sportif stagiaire et créer leur entreprise. Cependant, dans ce cas, elles doivent respecter la réglementation attachée à ces diplômes, titres ou certificats de qualification, et être placées sous l’autorité d’un tuteur et avoir satisfait aux exigences préalables à leur mise en situation pédagogique.

*Pour aller plus loin* : article L. 212-1 et suivants du Code du sport ; annexe II-1 (article A212-1) du Code du sport.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### Pour une Libre Prestation de Services (LPS)

Les ressortissants de l’Union européenne (UE) ou de l’Espace économique européen (EEE) légalement établis dans un de ces États peuvent exercer la même activité en France de manière temporaire et occasionnelle à la condition d’avoir adressé au préfet de département du lieu d’exécution de la prestation une déclaration préalable d’activité.

Si l’activité ou la formation y conduisant n’est pas réglementée dans l’État membre d’origine ou l’État du lieu d’établissement, le ressortissant doit également justifier y avoir exercé cette activité pendant au moins l’équivalent de deux années à temps complet au cours des dix dernières années précédant la prestation.

Les ressortissants européens désireux d’exercer en France de manière temporaire ou occasionnelle doivent posséder les connaissances linguistiques nécessaires à l’exercice de l’activité en France, en particulier afin de garantir la sécurité des activités physiques et sportives et sa capacité à alerter les secours.

*Pour aller plus loin* : articles L. 212-7 et R. 212-92 à R. 212-94 du Code du sport.

#### Pour un Libre Établissement (LE)

Le ressortissant d’un État de l’UE ou de l’EEE peut s’établir en France pour y exercer de façon permanente, s’il remplit l’une des quatre conditions suivantes :

##### Si l’État membre d’origine réglemente l’accès ou l’exercice de l’activité :

- être titulaire d’une attestation de compétences ou d’un titre de formation délivré par l’autorité compétente d’un État de l’UE ou de l’EEE qui atteste d’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France ;
- être titulaire d’un titre acquis dans un État tiers et admis en équivalence dans un État de l’UE ou de l’EEE et justifier avoir exercé cette activité pendant au moins deux ans à temps complet dans cet État.

##### Si l’État membre d’origine ne réglemente ni l’accès, ni l’exercice de l’activité :

- justifier avoir exercé l’activité dans un État de l’UE ou de l’EEE, à temps complet pendant deux ans au moins au cours des dix dernières années, ou, en cas d’exercice à temps partiel, justifier d’une activité d’une durée équivalente et être titulaire d’une attestation de compétences ou d’un titre de formation délivré par l’autorité compétente d’un de ces États, qui atteste d’une préparation à l’exercice de l’activité, ainsi qu’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France ;
- être titulaire d’un titre attestant d’un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France, délivré par l’autorité compétente d’un État de l’UE ou de l’EEE et sanctionnant une formation réglementée visant spécifiquement l’exercice de tout ou partie des activités mentionnées à l’article L. 212-1 du Code du sport et consistant en un cycle d’études complété, le cas échéant, par une formation professionnelle, un stage ou une pratique professionnelle.

Si le ressortissant remplit l’une des quatre conditions précitées, l’obligation de qualification requise pour exercer est réputée satisfaite.

Néanmoins, si les qualifications professionnelles du ressortissant présentent une différence substantielle avec les qualifications requises en France qui ne permettrait pas de garantir la sécurité des pratiquants et des tiers, il peut être amené à se soumettre à une épreuve d’aptitude ou accomplir un stage d’adaptation (cf. infra « Bon à savoir : mesures de compensation »).

Le ressortissant doit posséder la connaissance de la langue française nécessaire à l’exercice de son activité en France, en particulier afin de garantir la sécurité des activités physiques et sportives et sa capacité à alerter les secours.

*Pour aller plus loin* : articles L. 212-7 et R. 212-88 à R. 212-90 du Code du sport.

### c. Conditions d’honorabilité et incompatibilités

Il est interdit d’exercer en tant qu’éducateur sportif en France pour les personnes ayant fait l’objet d’une condamnation pour tout crime ou pour l’un des délits suivants :

- torture et actes de barbarie ;
- agressions sexuelles ;
- trafic de stupéfiants ;
- mise en danger d’autrui ;
- proxénétisme et infractions qui en résultent ;
- mise en péril des mineurs ;
- usage illicite de substances ou plantes classées comme stupéfiants ou provocation à l’usage illicite de stupéfiants ;
- infractions prévues aux articles L. 235-25 à L. 232-28 du Code du sport ;
- à titre de peine complémentaire à une infraction en matière fiscale : condamnation à une interdiction temporaire d’exercer, directement ou par personne interposée, pour son compte ou le compte d’autrui, toute profession industrielle, commerciale ou libérale (article 1750 du Code général des impôts).

De plus, nul ne peut enseigner, animer ou encadrer une activité physique ou sportive auprès de mineurs s’il a fait l’objet d’une mesure administrative d’interdiction de participer, à quelque titre que ce soit, à la direction et à l’encadrement d’institutions et d’organismes soumis aux dispositions législatives ou réglementaires relatives à la protection des mineurs accueillis en centre de vacances et de loisirs, ainsi que de groupements de jeunesse ou s’il a fait l’objet d’une mesure administrative de suspension de ces mêmes fonctions.

*Pour aller plus loin* : article L. 212-9 du Code du sport.

## 3°. Démarches et formalités d’installation

### a. Effectuer la déclaration d’éducateur sportif

Toute personne souhaitant exercer l’une des professions régies par l’article L. 212-1 du Code du sport doit déclarer son activité au préfet du département du lieu où elle compte exercer son activité à titre principal. Cette déclaration déclenche l’obtention d’une carte professionnelle.

La déclaration doit être renouvelée tous les cinq ans.

**Autorité compétente**

La déclaration doit être adressée à la direction départementale de la cohésion sociale (DDCS) ou direction départementale de la cohésion sociale et de la protection des populations (DDCSPP) du département d’exercice ou du principal exercice, ou directement en ligne sur le [site officiel du ministère des Sports](https://eaps.sports.gouv.fr.).

**Délai**

Dans le mois suivant le dépôt du dossier de déclaration, la préfecture envoie un accusé de réception au déclarant. La carte professionnelle, valable cinq ans, est ensuite adressée au déclarant.

**Pièces justificatives**

Les pièces justificatives nécessaires sont :

- le formulaire de déclaration Cerfa 12699*02 ;
- une copie d’une pièce d’identité en cours de validité ;
- une photo d’identité ;
- une déclaration sur l’honneur attestant de l’exactitude des informations figurant dans le formulaire ;
- une copie de chacun des diplômes, titres certificats invoqués ;
- une copie de l’autorisation d’exercice, de l’équivalence de diplôme le cas échéant ;
- un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées, datant de moins d’un an.

En cas de renouvellement de la déclaration, il faut joindre :

- le formulaire Cerfa 12699*02 ;
- une photo d’identité ;
- une copie de l’attestation de révision en cours de validité pour les qualifications soumises à l’obligation de recyclage ;
- un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées, datant de moins d’un an.

La préfecture demandera elle-même aux services compétents la communication d’un extrait de moins de trois mois du casier judiciaire du déclarant pour vérifier l’absence d’incapacité ou d’interdiction d’exercer.

**Coût**

Gratuit

*Pour aller plus loin* : articles L. 212-11, R. 212-85, A. 212-176 à A. 212-178 du Code du sport.

### b. Procéder aux formalités de déclaration de l’entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### c. Effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité ponctuelle (Libre Prestation de Service)

Les ressortissants de l’UE ou de l’EEE légalement établis dans l’un de ces États et souhaitant exercer en France de manière temporaire ou occasionnelle doivent effectuer une déclaration préalable d’activité, avant la première prestation de services.

Si le prestataire souhaite effectuer une nouvelle prestation en France, cette déclaration préalable doit être renouvelée.

Afin d’éviter des dommages graves pour la sécurité des bénéficiaires, le préfet peut, lors de la première prestation, procéder à une vérification préalable des qualifications professionnelles du prestataire.

**Autorité compétente**

La déclaration préalable d’activité doit être adressée à la direction départementale en charge de la cohésion sociale (DDCS) ou à la direction départementale en charge de la cohésion sociale et de la protection des populations (DDCSPP) du département où le déclarant souhaite effectuer sa prestation.

**Délai**

Dans le mois suivant la réception du dossier de déclaration, le préfet notifie au prestataire soit :

- une demande d’informations complémentaires (dans ce cas, le préfet dispose de deux mois pour donner sa réponse) ;
- un récépissé de déclaration de prestation de services s’il ne procède pas à la vérification des qualifications. Dans ce cas, la prestation de services peut débuter ;
- qu’il procède à la vérification des qualifications. Dans ce cas, le préfet délivre ensuite au prestataire un récépissé lui permettant de débuter sa prestation ou, si la vérification des qualifications fait apparaître des différences substantielles avec les qualifications professionnelles requises en France, le préfet soumet le prestataire à une épreuve d’aptitude (cf. infra « Bon à savoir : mesures de compensation »).

Dans tous les cas, en l’absence de réponse dans les délais précités, le prestataire est réputé exercer légalement son activité en France.

**Pièces justificatives**

Le dossier de déclaration préalable d’activité doit contenir :

- un exemplaire du formulaire de déclaration dont le modèle est fourni à l’annexe II-12-3 du Code du sport ;
- une photo d’identité ;
- une copie d’une pièce d’identité ;
- une copie de l’attestation de compétences ou du titre de formation ;
- une copie des documents attestant que le déclarant est légalement établi dans l’État membre d’établissement et qu’il n’encourt aucune interdiction, même temporaire, d’exercer (traduits en français par un traducteur agréé) ;
- dans le cas où ni l’activité, ni la formation, conduisant à cette activité n’est réglementée dans l’État membre d’établissement, une copie de toutes pièces justifiant que le déclarant a exercé cette activité dans cet État pendant au moins l’équivalent de deux ans à temps complet au cours des dix dernières années (traduites en français par un traducteur agréé) ;
- l’un de ces trois documents au choix (à défaut, un entretien sera organisé) :
  - une copie d’une attestation de qualification délivrée à l’issue d’une formation en français,
  - une copie d’une attestation de niveau en français délivré par une institution spécialisée,
  - une copie d’un document attestant d’une expérience professionnelle acquise en France.

**Coût**

Gratuit.

**Voies de recours**

Tout recours contentieux doit être exercé dans les deux mois suivant la notification de la décision auprès du tribunal administratif compétent.

*Pour aller plus loin* : articles R. 212-92 et suivants, articles A. 212-182-2 et suivants et annexe II-12-3 du Code du sport.

### d. Effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité permanente (Libre Établissement)

Tout ressortissant de l’UE ou de l’EEE qualifié pour y exercer tout ou partie des activités mentionnées à l’article L. 212-1 du Code du sport, et souhaitant s’établir en France, doit en faire préalablement la déclaration au préfet du département dans lequel il compte exercer à titre principal.

Cette déclaration permet au déclarant d’obtenir une carte professionnelle et ainsi d’exercer en toute légalité en France dans les mêmes conditions que les ressortissants français.

La déclaration doit être renouvelée tous les cinq ans.

En cas de différence substantielle avec la qualification requise en France, le préfet peut saisir, pour avis, la commission de reconnaissance des qualifications placée auprès du ministre chargé des sports. Il peut aussi décider de soumettre le ressortissant à une épreuve d’aptitude ou à un stage d’adaptation (cf. infra « Bon à savoir : mesures de compensation »).

**Autorité compétente**

La déclaration doit être adressée à la direction départementale en charge de la cohésion sociale (DDCS) ou à la direction départementale en charge de la cohésion sociale et de la protection des populations (DDCSPP).

**Délai**

La décision du préfet de délivrer la carte professionnelle intervient dans un délai de trois mois à compter de la présentation du dossier complet par le déclarant. Ce délai peut être prorogé d’un mois sur décision motivée. Si le préfet décide de ne pas délivrer la carte professionnelle ou de soumettre le déclarant à une mesure de compensation (épreuve d’aptitude ou stage), sa décision doit être motivée.

**Pièces justificatives pour la première déclaration d’activité**

Le dossier de déclaration d’activité doit contenir :

* un exemplaire du formulaire de déclaration dont le modèle est fourni à l’annexe II-12-2-a du Code du sport ;
* une photo d’identité ;
* une copie d’une pièce d’identité en cours de validité ;
* un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives, datant de moins d’un an (traduit par un traducteur agréé) ;
* une copie de l’attestation de compétences ou du titre de formation, accompagnée de documents décrivant le cursus de formation (programme, volume horaire, nature et durée des stages effectués), traduit en français par un traducteur agréé ;
* le cas échéant, une copie de toutes pièces justifiant de l’expérience professionnelle (traduites en français par un traducteur agréé) ;
* si le titre de formation a été obtenu dans un État tiers, les copies des pièces attestant que ce titre a été admis en équivalence dans un État de l’UE ou de l’EEE qui réglemente l’activité ;
* l’un des trois documents au choix (à défaut, un entretien sera organisé) :
  * une copie d’une attestation de qualification délivrée à l’issue d’une formation en français,
  * une copie d’une attestation de niveau en français délivré par une institution spécialisée,
  * une copie d’un document attestant d’une expérience professionnelle acquise en France ;
* les documents attestant que le déclarant n’a pas fait l’objet, dans l’État membre d’origine, d’une des condamnations ou mesures mentionnées aux articles L. 212-9 et L. 212-13 du Code du sport (traduits en français par un traducteur agréé).

**Pièces justificatives pour un renouvellement de déclaration d’activité**

Le dossier de renouvellement de déclaration d’activité doit contenir :

* un exemplaire du formulaire de renouvellement de déclaration dont le modèle est fourni à l’annexe II-12-2-b du Code du sport ;
* une photo d’identité ;
* un certificat médical de non-contre-indication à la pratique et à l’encadrement des activités physiques ou sportives, datant de moins d’un an.

**Coût**

Gratuit.

**Voies de recours**

Tout recours contentieux doit être exercé dans les deux mois de la notification de la décision auprès du tribunal administratif compétent.

*Pour aller plus loin* : articles R. 212-88 à R. 212-91, articles A. 212-182 et annexes II-12-2-a et II-12-b du Code du sport.

**Bon à savoir : mesures de compensation**

S’il existe une différence substantielle entre la qualification du requérant et celle requise en France pour exercer la même activité, le préfet saisit la commission de reconnaissance des qualifications, placée auprès du ministre chargé des sports. Cette commission, après examen et instruction du dossier, émet, dans le mois de sa saisine, un avis qu’elle adresse au préfet. 

Dans son avis, la commission peut :

* estimer qu’il existe effectivement une différence substantielle entre la qualification du déclarant et celle requise en France. Dans ce cas, la commission propose de soumettre le déclarant à une épreuve d’aptitude ou un stage d’adaptation. Elle définit la nature et les modalités précises de ces mesures de compensation (nature des épreuves, modalités de leur organisation et de leur évaluation, période d’organisation, contenu et durée du stage, types de structures pouvant accueillir le stagiaire, etc.) ;
* estimer qu’il n’y a pas de différence substantielle entre la qualification du déclarant et celle requise en France.

À réception de l’avis de la commission, le préfet notifie sa décision motivée au déclarant (il n’est pas obligé de suivre l’avis de la commission) :

* s’il exige qu’une mesure de compensation soit effectuée, le déclarant dispose d’un délai d’un mois pour choisir entre la ou les épreuve(s) d’aptitude et le stage d’adaptation. Le préfet délivre ensuite une carte professionnelle au déclarant qui a satisfait aux mesures de compensation. En revanche, si le stage ou l’épreuve d’aptitude ne sont pas satisfaisants, le préfet notifie sa décision motivée de refus de délivrance de la carte professionnelle à l’intéressé ;
* s’il n’exige pas de mesure de compensation, le préfet délivre une carte professionnelle à l’intéressé.

*Pour aller plus loin* : articles R. 212-84 et D. 212-84-1 du Code du sport.