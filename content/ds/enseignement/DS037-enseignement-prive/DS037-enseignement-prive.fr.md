﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS037" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Enseignement" -->
<!-- var(title)="Enseignement privé" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="enseignement" -->
<!-- var(title-short)="enseignement-prive" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/enseignement/enseignement-prive.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="enseignement-prive" -->
<!-- var(translation)="None" -->

# Enseignement privé

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Un établissement d'enseignement privé a pour objectif de fournir un enseignement commun à un certain nombre d'élèves pour le primaire, secondaire, technique ou supérieur.

**À noter**

Il ne sera pas traité ici du cas des établissements d'enseignement privés sous contrat avec l’État.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour une activité libérale, il s'agit de l'Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Les qualifications professionnelles de l'intéressé souhaitant ouvrir un établissement privé diffèrent selon le niveau d'enseignement.

Pour l'enseignement primaire, l'intéressé doit être titulaire d'un brevet de capacité de l'enseignement primaire.

Lorsqu'il choisit d'ouvrir un établissement du secondaire, il devra :

- être âgé d'au moins 25 ans ;
- justifier d'un certificat de stage constatant qu'il a rempli pendant cinq ans au moins les fonctions de professeur ou de surveillant dans un établissement d'enseignement du second degré public ou privé d'un État membre de l'Union européenne (UE) ou partie à l'espace économique européen (EEE) ;
- être titulaire du diplôme du baccalauréat, de celui de licence, ou d'un des certificats d'aptitude à l'enseignement secondaire.

Dans le cas des établissements de l'enseignement technique, le demandeur devra être titulaire soit :

- d’un certificat de stage constatant qu'il a exercé pendant au moins cinq ans les fonctions de professeur ou de surveillant dans un établissement public ou privé du second degré ;
- d’un diplôme donnant droit de postuler à un emploi de professeur d’enseignement général ou technique théorique dans un établissement scolaire public donnant des enseignements de même niveau que celui dont l'ouverture est demandée ;
- d’un diplôme d’ingénieur figurant sur une liste établie en application du décret n° 56-931 du 14 septembre 1956.

Enfin, l'ouverture d'un établissement d'enseignement supérieur requiert de son demandeur qu'il :

- soit âgé d'au moins 25 ans ;
- justifie des conditions requises pour l'exercice des professions de médecin ou de pharmacien ou de chirurgien-dentiste ou de sage-femme lorsqu'il propose l'enseignement de la médecine, de la pharmacie, de l'odontologie et de la maïeutique.

*Pour aller plus loin* : article L. 914-3 du Code de l'éducation.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Le ressortissant d'un État de l’UE ou de l’EEE est soumis aux mêmes conditions de qualifications professionnelles que les Français.

### c. Honorabilité et incompatibilité

Tout intéressé qui souhaite ouvrir un établissement d'enseignement primaire, secondaire ou technique privé ne doit pas :

- avoir fait l'objet d'une condamnation pour crime ou délit contraire à la probité et aux mœurs ;
- avoir été privé par jugement de tout ou partie des droits civils, civiques et de famille, ou avoir été déchus de l'autorité parentale ;
- avoir été frappé d'interdiction définitive d'enseigner ;
- avoir été révoqué d'un établissement d'enseignement du second degré public ou privé.

Pour ouvrir un établissement de l'enseignement primaire privé, le professionnel devra être âgé d'au moins 21 ans, contre 25 ans pour un établissement d'enseignement secondaire, technique et supérieur.

*Pour aller plus loin* : article L.911-5 du Code de l'éducation.

### d. Quelques particularités de la réglementation de l'activité

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Les locaux étant ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au registre du commerce et des sociétés (RCS).

### b. Effectuer une déclaration de création d'un établissement d'enseignement privé

Avant le 15 avril 2018, il existait trois régimes spécifiques pour les établissements de l'enseignement primaire, secondaire et technique. Une loi du 13 avril 2018, entrée en vigueur le 15 avril 2018, a fusionné ces trois régimes . Depuis, il existe un régime commun pour l'ouverture de ces trois établissements et un régime particulier pour l'ouverture d'un établissement d'enseignement supérieur.

#### Le régime commun pour l'ouverture d'un établissement primaire, secondaire et technique

##### La déclaration préalable de l'ouverture d'un établissement primaire, secondaire, et technique

Toute personne qui respecte les conditions de capacité (voir l'article L.911-5 du Code de l'éducation) et de nationalité (nationalité française ou ressortissant de l'UE ou d'un autre État partie à l'accord sur l'EEE) peut ouvrir un établissement primaire, secondaire ou technique.

La création de tels établissements est soumise à la déclaration préalable à l'autorité compétente en matière d'éducation, soit le recteur d'académie. Ce dernier la transmettra au maire de la commune dans laquelle l'établissement est situé, au préfet de département et au procureur de la République.

Toutefois, une opposition à cette ouverture peut être formée par le recteur d'académie, le maire, le représentant de l'État dans le département et le procureur de la République pour différents motifs.

*Pour aller plus loin* : article L. 441-1 du Code de l'éducation.

##### Le contenu du dossier de déclaration d'ouverture

S’agissant de la ou des personnes physiques déclarant l’ouverture et dirigeant l’établissement, le dossier doit comporter :

- une déclaration :
  - mentionnant la volonté d’ouvrir et de diriger un établissement accueillant des élèves, et présentant l’objet de l’enseignement qui doit être conforme au seuil commun,
  - précisant l’âge des élèves ainsi que, le cas échéant, les diplômes ou les emplois auxquels l’établissement les préparera,
  - précisant les horaires,
  - précisant les disciplines si l’établissement prépare à des diplômes d’enseignement technique ;
- la ou les pièces attestant de leur identité, de leur âge, et de leur nationalité ;
- l’original du bulletin de leur casier judiciaire daté de moins de trois mois lors du dépôt de dossier ;
- l’ensemble des pièces attestant que la personne qui ouvre l’établissement et, le cas échéant, celle qui le dirigera :
  - est capable (voir l'article L.911-5 du Code de l'éducation),
  - est de nationalité française ou ressortissant d'un État membre de l'UE ou d'un État partie à l'accord sur l'EEE,
  - a exercé pendant au moins cinq ans des fonctions de direction, d’enseignement ou de surveillance dans un établissement d’enseignement public ou privé d’un État membre de l’UE ou d’un autre État partie à l’accord sur l’EEE ;

S’agissant de l’établissement, il conviendra de transmettre :

- le plan des locaux et, le cas échéant, de tout terrain destiné à recevoir les élèves, indiquant au moins la dimension de chacune des surfaces et leur destination ;
- ses modalités de financement ;
- le cas échéant, l'attestation du dépôt de la demande d'autorisation (voir l'article L.111-8 du Code de la construction et de l’habitation).

Si le déclarant est une personne morale, il devra également transmettre ses statuts.

**À noter**

Le recteur d'académie délivre à cette personne un accusé de réception.

Le recteur d'académie indique à la personne que le dossier est incomplet dans l’accusé de réception, dans un délai au plus égal à quinze jours après sa délivrance. En même temps qu'il donne l'indication que le dossier est incomplet et qu'il reçoit les pièces requises, le recteur d'académie en effectue la transmission au maire, au préfet de département et au procureur de la République.

Cette même déclaration doit être faite en cas de changement de locaux ou d’admission d’élèves internes.

Le fait d'ouvrir un établissement d'enseignement privé en dépit d'une opposition formulée par les autorités compétentes ou sans remplir les conditions prescrites à l'article L. 441-4 du Code de l'éducation est puni de 15 000 € d'amende et de la fermeture de l'établissement.

##### Établissements de l'enseignement supérieur privés

Lorsqu'une association souhaite ouvrir un établissement d'enseignement supérieur, elle doit en faire la déclaration auprès du recteur, du préfet de département et du procureur général de la cour du ressort ou au procureur de la République.

Lorsqu'une personne physique souhaite ouvrir un établissement d'enseignement supérieur, elle doit en faire la déclaration au recteur dans les départements où est établi le chef-lieu de l'académie ainsi qu'à l'autorité de l'État compétente en matière d'éducation dans les autres départements. Elle doit être de nationalité française ou ressortissante d’un État membre de l’UE ou d’un État partie à l’accord de l’EEE 

**À noter**

Un établissement d'enseignement supérieur doit être administré par au moins trois personnes.

La déclaration d'ouverture se fait par l'envoi d'un dossier comprenant :

- une pièce d'identité en cours de validité de tous les administrateurs ;
- un curriculum vitae ;
- une copie des diplômes ;
- un extrait du casier judiciaire datant de moins de trois mois ;
- le cas échéant, les noms, professions et domiciles des fondateurs et administrateurs et une copie des statuts de l'association.

En cas d'opposition à l'ouverture, le procureur de la République se prononcera dans un délai de dix jours pour notifier sa décision de refus. Le demandeur pourra, dès lors, former un recours en cassation dans les quinze jours suivant la notification.

*Pour aller plus loin* : articles L. 731-1 à L. 731-18 du Code de l'éducation.

### d. Autorisation(s) post-installation

**Déclaration d'ouverture de chaque cours d'enseignement supérieur privé**

L'ouverture d'un cours dans un établissement de l'enseignement supérieur privé doit être soumis à une déclaration préalable comportant :

- les nom, qualité et domicile du responsable de l’association ;
- les locaux où ont lieu les cours ;
- l’objet de l’enseignement ;
- une photocopie d'une pièce d'identité en cours de validité ;
- un curriculum vitae du demandeur ;
- une copie des diplômes ;
- un extrait du bulletin n° 3 du casier judiciaire.

Cette déclaration devra être transmise au recteur d'académie ainsi qu'à la direction académique des services de l'éducation nationale agissant sur recteur d'académie. À réception du récépissé, l'ouverture du cours pourra s'opérer au moins 10 jours plus tard.

Tout changement des éléments constitutifs de la déclaration doit être actualisé.

*Pour aller plus loin* : articles L. 731-3 et L. 731-4 du Code de l'éducation.