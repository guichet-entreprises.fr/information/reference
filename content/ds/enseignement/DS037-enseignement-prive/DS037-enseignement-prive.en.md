﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS037" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Education" -->
<!-- var(title)="Private education" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="education" -->
<!-- var(title-short)="private-education" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/education/private-education.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="private-education" -->
<!-- var(translation)="Auto" -->


Private education
=================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

A private school aims to provide a common education to a certain number of pupils for primary, secondary, technical or higher education.

**Please note**

The case of private educational institutions under contract with the state will not be dealt with here.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for a liberal activity, it is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional qualifications of the person wishing to open a private school differ according to the level of education.

For primary education, the person concerned must hold a primary education capacity certificate.

When he chooses to open a secondary school, he will have to:

- Be at least 25 years old
- justify an internship certificate stating that he has served for at least five years as a teacher or supervisor in a public or private second degree educational institution of a Member State of the European Union (EU) or a party to space European Economic Economic Index (EEA);
- hold a bachelor's degree, a bachelor's degree, a bachelor's degree, or a high school certificate.

In the case of technical education institutions, the applicant must hold either:

- an internship certificate stating that he has served for at least five years as a teacher or supervisor in a public or private second degree institution;
- a degree entitling you to apply for a job as a teacher of general or theoretical technical education in a public school giving teachings of the same level as the one that is requested to be opened;
- an engineering degree on a list drawn up under Decree 56-931 of September 14, 1956.

Finally, the opening of a higher education institution requires the applicant to:

- be at least 25 years old;
- justifies the requirements for the professions of physician or pharmacist or dental surgeon or midwife when proposing the teaching of medicine, pharmacy, odontology and maïeutics.

*For further information*: Article L. 914-3 of the Education Code.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

The national of an EU or EEA state is subject to the same conditions of professional qualification as the French.

### c. Honorability and incompatibility

Anyone interested in opening a private primary, secondary or technical school should not:

- have been convicted of a felony or misdemeanour contrary to probity and morals;
- have been deprived by judgment of all or part of civil, civil and family rights, or have been stripped of parental authority;
- have been permanently banned from teaching;
- have been removed from a public or private second degree educational institution.

To open a private primary school, the professional must be at least 21 years old, compared to 25 years for a secondary, technical and higher education institution.

*For further information*: Article L.911-5 of the Education Code.

### d. Some peculiarities of the regulation of the activity

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

As the premises are open to the public, the professional must comply with the rules relating to public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

Depending on the nature of the business, the contractor must register with the Register of Trade and Companies (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company with the RCS" for more information.

### b. Make a declaration of the creation of a private educational institution

Prior to 15 April 2018, there were three specific schemes for primary, secondary and technical schools. A law of April 13, 2018, which came into force on April 15, 2018, merged these three regimes. Since then, there has been a common regime for the opening of these three institutions and a special regime for the opening of a higher education institution.

#### The common scheme for the opening of a primary, secondary and technical school

**The prior declaration of the opening of a primary, secondary, and technical school**

Anyone who complies with the conditions of capacity (see Article L.911-5 of the Education Code) and nationality (French nationality or EU national or other state party to the EEA agreement) can open a primary school, secondary or technical.

The creation of such institutions is subject to the declaration prior to the competent authority on education, the rector of the academy. The latter will pass it on to the mayor of the commune in which the establishment is located, to the prefect of department and to the prosecutor of the Republic.

However, an opposition to this opening can be formed by the academy rector, the mayor, the state representative in the department and the prosecutor of the Republic for various reasons.

*For further information*: Article L. 441-1 of the Education Code.

**The contents of the opening statement file**

For the individual or individuals declaring the opening and running the institution, the file must include:

- a statement:- mentioning the desire to open and run a student-friendly school, and presenting the purpose of teaching that must be in line with the common threshold,
  - specifying the age of the students and, if applicable, the diplomas or jobs to which the school will prepare them,
  - specifying the timetables,
  - specifying the disciplines if the institution is preparing for technical education degrees;
- the document or documents attesting to their identity, age, and nationality;
- The original criminal record report card dated less than three months at the time of file;
- all the documents attesting that the person who opens the establishment and, if so, the person who will run it:- is capable (see Article L.911-5 of the Education Code),
  - is a French national or a national of an EU member state or a state party to the EEA agreement,
  - has held leadership, teaching or supervisory positions in a public or private school in an EU Member State or other state party to the EEA agreement for at least five years;

As far as the establishment is concerned, it will be necessary to pass on:

- The map of the premises and, if necessary, any land intended to receive the students, indicating at least the size of each surface and their destination;
- Its financing arrangements;
- if necessary, the certification of the application for authorization (see Article L.111-8 of the Building and Housing Code).

If the registrant is a legal entity, he will also have to pass on his statutes.

**Please note**

The academy rector issues this person an acknowledgement.

The academy rector tells the person that the file is incomplete in the acknowledgment, within a maximum of fifteen days after its issuance. At the same time as he gives the indication that the file is incomplete and that he receives the required documents, the rector of the academy transmits it to the mayor, the prefect of department and the prosecutor of the Republic.

The same statement must be made in the event of a change of premises or the admission of internal students.

Opening a private school in spite of opposition by the competent authorities or without meeting the conditions of Article L. 441-4 of the Education Code is punishable by a fine of 15,000 euros and the closure of establishment.

**Private higher education institutions**

When an association wishes to open a higher education institution, it must make the declaration to the rector, the prefect of the department and the attorney general of the court of the jurisdiction or to the prosecutor of the Republic.

When a natural person wishes to open a higher education institution, he must make the declaration to the rector in the departments where the capital of the academy is established and to the authority of the state responsible for the matter education in other departments. She must be a French national or a national of an EU member state or a state party to the EEA agreement

**Please note**

A higher education institution must be administered by at least three people.

The opening statement is by sending a file that includes:

- A valid ID for all directors;
- A resume
- A copy of the diplomas
- an extract from the criminal record less than three months old;
- if necessary, the names, professions and residences of the founders and directors and a copy of the association's statutes.

In the event of opposition to the opening, the public prosecutor will decide within ten days to notify his decision to refuse. The applicant will therefore be able to file an appeal for cassation within a fortnight of notification.

*For further information*: Articles L. 731-1 to L. 731-18 of the Education Code.

### d. Authorisation (s) post-installation

**Statement of opening of each private higher education course**

The opening of a course in a private higher education institution must be subject to a prior declaration including:

- The name, quality and residence of the head of the association;
- The premises where the classes are held;
- The purpose of teaching
- A photocopy of a valid ID
- A student's resume
- A copy of the diplomas
- an excerpt from bulletin 3 of the criminal record.

This statement should be forwarded to the academy rector as well as to the academic directorate of the national education services acting on academy rector. Upon receipt, the course may open at least 10 days later.

Any changes to the components of the declaration must be updated.

*For further information*: Articles L. 731-3 and L. 731-4 of the Education Code.

