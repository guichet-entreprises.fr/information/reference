﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS005" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentation" -->
<!-- var(title)="Boulanger-pâtissier" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentation" -->
<!-- var(title-short)="boulanger-patissier" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/alimentation/boulanger-patissier.html" -->
<!-- var(last-update)="2020" -->
<!-- var(url-name)="boulanger-patissier" -->
<!-- var(translation)="None" -->

# Boulanger-pâtissier

Dernière mise à jour : <!-- begin-var(last-update) -->2020<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

L’appellation de boulanger et l’enseigne commerciale de boulangerie concernent les professionnels qui assurent eux-mêmes, à partir de matières premières choisies, le pétrissage de la pâte, sa fermentation et sa mise en forme ainsi que la cuisson du pain sur le lieu de vente au consommateur final.

La pâte et les pains ne peuvent à aucun stade de la production ou de la vente être surgelés ou congelés.

L’appellation de boulanger et l’enseigne commerciale de boulangerie peuvent aussi être utilisées lorsque le pain est vendu de façon itinérante par le professionnel, ou sous sa responsabilité, lorsque les conditions précédentes sont respectées.

*Pour aller plus loin* : articles L. 122-17 et L. 122-18 du Code de la consommation.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de l’activité exercée, de la forme juridique de l’entreprise et du nombre de salariés employés :

- pour les artisans et pour les sociétés commerciales exerçant une activité artisanale, à condition qu’ils n’emploient pas plus de dix salariés, le CFE compétent est la chambre de métiers et de l’artisanat (CMA) ;
- pour les artisans et pour les sociétés commerciales qui emploient plus de dix salariés, il s’agit de la chambre du commerce et de l’industrie (CCI) ;
- dans les départements du Haut-Rhin et du Bas-Rhin, la chambre des métiers d’Alsace est compétente.

**À noter**

Dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle, l’activité reste artisanale, quel que soit le nombre de salariés employés, dès lors que l’entreprise n’utilise pas de procédé industriel. Le CFE compétent est donc la CMA ou la chambre des métiers d’Alsace. Pour plus d’informations, il est conseillé de se reporter [site officiel de la chambre des métiers d'Alsace](http://www.cm-alsace.fr/decouvrir-la-cma/lartisanat-en-alsace).

Pour plus d’informations, il est conseillé de consulter [le site de la CCI Paris](http://www.entreprises.cci-paris-idf.fr/web/formalites/competence-cfe).

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Seule une personne qualifiée professionnellement ou placée sous le contrôle effectif et permanent d’une personne qualifiée peut exercer l’activité de boulanger.

Les personnes qui exercent l’activité de boulanger ou qui en contrôlent l'exercice doivent :

- soit être titulaires d'un certificat d'aptitude professionnelle (CAP), d'un brevet d'études professionnelles (BEP) ou d'un diplôme ou d'un titre de niveau égal ou supérieur homologué ou enregistré lors de sa délivrance au [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP) ;
- soit justifier d’une expérience professionnelle de trois années effectives sur le territoire de l’Union européenne (UE) ou d’un autre État partie à l’accord sur l'Espace économique européen (EEE) acquise en qualité de dirigeant d'entreprise, de travailleur indépendant ou de salarié dans l'exercice du métier.

Dans cette deuxième hypothèse, le professionnel peut obtenir la délivrance d’une attestation de qualification professionnelle par la CMA de région ou par la chambre régionale de métiers et de l’artisanat dans le ressort de laquelle il exerce (cf. infra « 3° c. Le cas échéant, solliciter une attestation de qualification professionnelle »).

Les diplômes permettant l’exercice de l’activité de boulanger sont notamment :

- le CAP spécialité « boulanger » ;
- le brevet spécialité « boulanger » ;
- le baccalauréat professionnel « boulanger - pâtissier ».

Pour plus d’informations sur chacun de ces diplômes, il est conseillé de se reporter aux dispositions de l’arrêté du 21 février 2014 portant création de la spécialité « boulanger » du CAP, l'arrêté du 15 février 2012 portant création de la spécialité « boulanger » du brevet professionnel et l'arrêté du 2 juillet 2009 portant création de la spécialité « boulanger-pâtissier » du baccalauréat professionnel.

*Pour aller plus loin* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat ; article 1 du décret n° 98-246 du 2 avril 1998.

### b. Qualifications professionnelles – Ressortissants européens

#### Pour une Libre Prestation de Services (LPS)

Le professionnel ressortissant de l’UE ou de l’EEE peut exercer en France, à titre temporaire et occasionnel l’activité de boulanger à condition d’être légalement établi dans un de ces États pour y exercer la même activité.

En outre, lorsque cette activité ou la formation y conduisant ne sont pas réglementées dans l’État d’établissement, il doit avoir exercé son activité de boulanger pendant au moins deux années au cours des dix années qui précèdent la prestation qu’il entend réaliser en France.

**À noter**

Le professionnel qui répond à ces conditions est dispensé des exigences relatives à l’immatriculation au répertoire des métiers (RM) ou au registre des entreprises.

*Pour aller plus loin* : article 17-1 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat ; article 2 du décret n° 98-246 du 2 avril 1998.

#### Pour un Libre Établissement (LE)

Pour exercer en France à titre permanent l’activité de boulanger, le professionnel ressortissant de l’UE ou de l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- être titulaire d’une attestation de compétences ou d’un titre de formation requis pour l’exercice de l’activité de boulanger dans un État de l’UE ou de l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- disposer d’une attestation de compétences ou d’un titre de formation qui certifie sa préparation à l’exercice de l’activité de boulanger lorsque cette attestation ou ce titre a été obtenu dans un État de l’UE ou de l’EEE qui ne réglemente ni l’accès ni l’exercice de cette activité ;
- être titulaire d’un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l’UE ou de l’EEE à la condition supplémentaire que l’intéressé ait exercé pendant trois années l’activité de boulanger dans l’État membre qui a admis l’équivalence.

**À noter**

Le ressortissant d’un État de l’UE ou de l’EEE qui remplit l’une des conditions précitées peut solliciter une attestation de reconnaissance de qualification professionnelle (cf. infra « 3°. b. Le cas échéant, demander une attestation de qualification professionnelle ».)

Si l’intéressé ne remplit aucune des conditions précitées, la CMA saisie peut lui demander d’accomplir une mesure de compensation dans les cas suivants :

- si la durée de la formation attestée est inférieure d’au moins un an à celle exigée pour obtenir l’une des qualifications professionnelles requises en France pour exercer l’activité de boulanger ;
- si la formation reçue porte sur des matières substantiellement différentes de celles couvertes par l’un des titres ou diplômes requis pour exercer en France l’activité de boulanger ;
- si le contrôle effectif et permanent de l’activité de boulanger nécessite, pour l’exercice de certaines de ses attributions, une formation spécifique qui n’est pas prévue dans l’État membre d’origine et porte sur des matières substantiellement différentes de celles couvertes par l'attestation de compétences ou le titre de formation dont le demandeur fait état.

La mesure de compensation consiste, au choix du demandeur, en un stage d'adaptation ou en une épreuve d'aptitude (cf. infra « 3° c. Le cas échéant, solliciter une attestation de qualification professionnelle »).

*Pour aller plus loin* : articles 16 et 17 de la loi n° 96-603 du 5 juillet 1996 précitée ; articles 1 et 3 du décret n° 98-246 du 2 avril 1998 précité.

### c. Conditions d’honorabilité et incompatibilités

Pour exercer l’activité en France, le boulanger ne doit pas être sous l’emprise :

- d’une peine d’interdiction d’exercer cette activité (cette interdiction s’applique sur une durée de cinq ans au plus) ;
- d’une interdiction de diriger, gérer, administrer ou contrôler, directement ou indirectement, soit toute entreprise commerciale ou artisanale, toute exploitation agricole et toute personne morale.

*Pour aller plus loin* : article 19 de la loi n° 96-603 du 5 juillet 1996 précitée ; article 131-6 du Code pénal ; article L. 653-8 du Code de commerce.

### d. Quelques particularités de la réglementation de l’activité

#### La qualité d’artisan

Peuvent se prévaloir de la qualité d’artisan les personnes justifiant :

- soit d’un CAP, d’un BEP ou d’un titre homologué ou enregistré lors de sa délivrance au RNCP d’un niveau au moins équivalent (cf. supra « 2°. a. Qualification professionnelle ») ;
- soit d’une expérience professionnelle dans ce métier d’une durée de trois ans au moins.

*Pour aller plus loin* : article 1 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au RM.

#### Le titre de maître artisan

Le titre de maître artisan est attribué aux personnes physiques, y compris les dirigeants sociaux des personnes morales :

- immatriculées au RM ;
- titulaires du brevet de maîtrise dans le métier exercé ;
- justifiant d’au moins deux ans de pratique professionnelle.

**À noter**

Dans certains cas, les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan à la commission régionale des qualifications.

*Pour aller plus loin* : article 3 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au RM.

#### Le titre de meilleur ouvrier de France (MOF)

Le diplôme professionnel « un des meilleurs ouvriers de France » est un diplôme d'État qui atteste l'acquisition d'une haute qualification dans l'exercice d'une activité professionnelle dans le domaine artisanal, commercial, de service, industriel ou agricole.

Le diplôme est classé au niveau III de la nomenclature interministérielle des niveaux de formation.

Il est délivré à l’issue d’un examen dénommé « concours un des meilleurs ouvriers de France » au titre d'une profession dénommée « classe », rattachée à un groupe de métiers.

*Pour aller plus loin* : article D. 338-9 du Code de l’éducation.

#### Respecter la réglementation relative aux établissements recevant du public (ERP)

Le boulanger doit se conformer aux règles de sécurité et d’accessibilité relatives aux ERP. Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

#### Respecter les normes sanitaires

Le boulanger doit appliquer les règles issues du règlement CE n° 852/2004 du 29 avril 2004 relatif à l'hygiène des denrées alimentaires qui concernent :

- les locaux utilisés pour les denrées alimentaires ;
- les équipements ;
- les déchets alimentaires ;
- l’alimentation en eau ;
- l’hygiène personnelle ;
- les denrées alimentaires, leur conditionnement et leur emballage.

Ce règlement prévoit la mise en place de procédures basées sur les principes de l’HACCP *(Hazard Analysis Critical Control Point).*

Le boulanger doit également appliquer les normes édictées par l’arrêté du 21 décembre 2009 qui précisent les températures de conservation des produits d’origine animale périssables. Celles-ci concernent notamment :

- les conditions de température de conservation ;
- les modalités de congélation ;
- les modalités de préparation des viandes hachées et de réception des gibiers.

Enfin, il doit respecter les règles relatives aux températures de conservation des denrées alimentaires périssables d’origine végétale édictées par l’arrêté du 8 octobre 2013.

*Pour aller plus loin* : règlement CE n° 852/2004 du 29 avril 2004 relatif à l'hygiène des denrées alimentaires ; arrêté du 21 décembre 2009 relatif aux règles sanitaires applicables aux activités de commerce de détail, d'entreposage et de transport de produits d'origine animale et denrées alimentaires en contenant ; arrêté du 8 octobre 2013 relatif aux règles sanitaires applicables aux activités de commerce de détail, d'entreposage et de transport de produits et denrées alimentaires autres que les produits d'origine animale et les denrées alimentaires en contenant.

#### Afficher les prix et l’étiquetage des produits

Le boulanger doit informer le consommateur, par voie de marquage, d'étiquetage, d'affichage ou par tout autre procédé approprié, sur les prix et les conditions particulières de la vente.

**À noter**

Le boulanger détermine librement le prix de vente de ses produits mais la vente à perte est interdite.

*Pour aller plus loin* : articles L. 410-2 et L. 442-2 et suivants du Code de la consommation.

En outre, une denrée alimentaire ne peut être commercialisée que si elle est accompagnée d'une mention qui permet d'identifier le lot auquel elle appartient.

##### Écriteau

Chaque catégorie de pain exposée à la vue du public, doit être accompagnée d’un écriteau d’une longueur d’au moins 15 cm et d’une hauteur d’au moins 2,5 cm.

Sur cet écriteau doit figurer :

- la dénomination exacte de la catégorie de pain ;
- le poids en grammes pour les pains vendus à la pièce (cette mention n’est pas obligatoire pour les pains d’un poids inférieur à 200 g) ;
- le prix de vente à la pièce ou au kilogramme selon qu’il s’agit de pains vendus à la pièce ou au poids ;
- le prix de vente rapporté au kilogramme pour les pains vendus à la pièce d’un poids supérieur à 200 g.

##### Affiche

Une affiche blanche imprimée en noir d’une hauteur d’au moins 40 cm et d’une largeur d’au moins 30 cm doit être apposée dans tous les points de vente au détail à une hauteur maximale de 2 mètres au-dessus du sol sans qu’un obstacle puisse gêner la vue des consommateurs.

Cette affiche a pour titre : « Prix du Pain » et énumère à raison d’un article par ligne, toutes les catégories de pains mises en vente avec :

- la dénomination précise ;
- leur poids ;
- leur prix à la pièce ;
- leur prix au kg (pour les pains vendus à la pièce d’un poids égal ou supérieur à 200 g).

Pour le titre, les lettres doivent avoir une hauteur minimale de 2,5 cm et une largeur minimale de 1,5 cm.

Pour les textes, les chiffres doivent avoir une hauteur minimale de 2 cm et une largeur minimale de 1 cm, les lettres doivent avoir une hauteur minimale de 1 cm et une largeur minimale de 0,5 cm.

Une seconde affiche similaire à la précédente mais dont les dimensions et celles des caractères peuvent être réduites de moitié doit être apposée en vitrine et être visible de l’extérieur.

Pour plus d’informations, il est conseillé de consulter le [site de la Confédération nationale de la boulangerie-pâtisserie française](http://www.boulangerie.org/reglementations-economiques-0).

*Pour aller plus loin* : articles L. 112-1, R. 412-3 à R. 412-10 du Code de la consommation ; arrêté n° 78-89/P du 9 août 1978.

#### Afficher les ingrédients reconnus comme allergènes

L'utilisation dans la fabrication ou la préparation d'une denrée alimentaire de denrées alimentaires provoquant des allergies ou des intolérances doit être portée à la connaissance du consommateur final.

Cette information doit être indiquée sur la denrée elle-même ou à proximité de celle-ci de façon qu'il n'existe aucune incertitude quant à la denrée à laquelle elle se rapporte.

*Pour aller plus loin* : articles R. 412-12 à R. 412-16 et annexe IV du Code de la consommation.

#### Appellation des pains

Peuvent seuls être mis en vente ou vendus sous la dénomination de « pain maison » ou sous une dénomination équivalente les pains :

- entièrement pétris, façonnés et cuits sur leur lieu de vente au consommateur final ;
- vendus au consommateur final, de façon itinérante, par le professionnel qui a assuré sur le même lieu les opérations de pétrissage, de façonnage et de cuisson.

Peuvent seuls être mis en vente ou vendus sous la dénomination de « pain de tradition française », « pain traditionnel français », « pain traditionnel de France » ou sous une dénomination combinant ces termes les pains, quelle que soit leur forme :

- n'ayant subi aucun traitement de surgélation au cours de leur élaboration ;
- ne contenant aucun additif ;
- et résultant de la cuisson d'une pâte qui présente des caractéristiques définies.

Peuvent seuls être mis en vente ou vendus sous une dénomination comportant la mention complémentaire « au levain » les pains :

- respectant les caractéristiques relatives aux deux appellations précédentes ;
- et présentant un potentiel hydrogène (pH) maximal de 4,3 et une teneur en acide acétique endogène de la mie d'au moins neuf cents parties par million.

*Pour aller plus loin* : décret n° 93-1074 du 13 septembre 1993 pris pour l'application de la loi du 1er août 1905 en ce qui concerne certaines catégories de pains.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Le boulanger doit être immatriculé au RM.

### b. Le cas échéant, solliciter une attestation de qualification professionnelle

Le ressortissant de l’UE ou de l’EEE peut solliciter la délivrance d’une attestation de qualification professionnelle à exercer le contrôle effectif et permanent de l’activité de boulanger.

#### Autorité compétente

La demande doit être adressée à la CMA de région ou à la chambre régionale de métiers et de l’artisanat dans le ressort de laquelle le ressortissant souhaite exercer.

#### Procédure et délai de réponse

La chambre délivre un récépissé mentionnant la date de réception de la demande complète dans un délai d’un mois à compter de sa réception.

En cas de demande incomplète, elle notifie au demandeur la liste des pièces manquantes dans un délai de quinze jours à compter de la réception de celle-ci et délivre le récépissé dès que le dossier est complet.

En l’absence de notification de la décision de la chambre dans un délai de quatre mois à compter de la réception de la demande complète, la reconnaissance de la qualification professionnelle est réputée acquise au demandeur.

La chambre peut, par décision motivée :

- délivrer une attestation de reconnaissance de qualification lorsqu’elle reconnaît la qualification ;
- informer par écrit qu’une mesure de compensation est exigée.

#### Pièces justificatives

La demande d’attestation de qualification professionnelle doit être accompagnée :

- du diplôme, du titre de formation professionnelle, de l’attestation de compétences ou de l’attestation d’activité (cf. supra) ;
- d’une preuve de la nationalité du demandeur ;
- pour les documents non établis en français, d’une traduction certifiée conforme à l’original par un traducteur assermenté ou habilité.

La CMA peut demander la communication d’informations complémentaires concernant le niveau, la durée et le contenu de sa formation propres à lui permettre de déterminer l’existence éventuelle de différences substantielles avec la formation française exigée.

Elle peut également demander la preuve par tout moyen d’une expérience professionnelle de nature à compenser, totalement ou partiellement, la différence substantielle.

#### Mesures de compensation

La CMA, saisie d’une demande de reconnaissance de qualification professionnelle, peut décider de soumettre le demandeur à une mesure de compensation. Sa décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est essentielle pour exercer l’activité de boulanger. Seules ces matières peuvent faire l'objet de l'épreuve d'aptitude ou du stage d'adaptation, dont la durée ne peut être supérieure à trois ans.

Le demandeur informe la CMA de son choix de suivre un stage d'adaptation ou de passer une épreuve d'aptitude.

L’épreuve d’aptitude prend la forme d’un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA de la décision du demandeur d’opter pour cette épreuve. À défaut, la qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

À l'issue de l'épreuve d'aptitude, la chambre délivre, dans un délai d'un mois, une attestation de qualification professionnelle au demandeur ayant réussi l'épreuve.

Si le demandeur choisit d’effectuer un stage d’adaptation, la CMA lui adresse la liste de l'ensemble des organismes susceptibles d'organiser ce stage, dans un délai d'un mois à compter de la réception de la décision du demandeur. À défaut, la reconnaissance de la qualification professionnelle est réputée acquise et la chambre établit une attestation de qualification professionnelle.

À l'issue du stage d'adaptation, le demandeur adresse à la chambre une attestation certifiant qu'il a accompli ce stage, accompagnée d'une évaluation de l'organisme qui l'a organisé. Sur la base de cette attestation, la chambre délivre à l'intéressé, dans un délai d'un mois, une attestation de qualification professionnelle.

**À noter**

Tout recours contentieux contre la décision de la CMA tendant à demander une mesure de compensation est précédé, à peine d'irrecevabilité, d'un recours administratif exercé, dans un délai de deux mois à compter de la notification de cette décision, auprès du préfet du département où la chambre a son siège.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 précité ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-146 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de la Communauté européenne ou d’un autre État partie à l’accord sur l’EEE.

### c. Le cas échéant, effectuer une déclaration en cas de préparation ou de vente de denrées animales ou d’origine animale

Tout exploitant d’un établissement produisant, manipulant ou entreposant des denrées d’origine animale ou des denrées comportant des ingrédients d’origine animale (viandes, produits laitiers, produits de la pêche, œufs, miel), destinées à la consommation humaine, doit satisfaire à l’obligation de déclarer chacun des établissements dont il a la responsabilité, ainsi que les activités qui s'y déroulent. 

La déclaration doit être faite avant l'ouverture de l'établissement et renouvelée en cas de changement d'exploitant, d'adresse ou de nature de l'activité.

#### Autorité compétente

La déclaration doit être adressée au préfet du département d’implantation de l’établissement ou, pour les établissements relevant de l'autorité ou de la tutelle du ministre de la défense, le service de santé des armées, selon les modalités prévues par arrêté du ministre de la défense.

#### Pièces justificatives 

Le demandeur doit remplir le formulaire Cerfa 13984*03.

*Pour aller plus loin* : articles R. 231-4 et R. 233-4 du Code rural et de la pêche maritime ; article 2 de l’arrêté du 28 juin 1994 relatif à l'identification et à l'agrément sanitaire des établissements mettant sur le marché des denrées animales ou d'origine animale et au marquage de salubrité.