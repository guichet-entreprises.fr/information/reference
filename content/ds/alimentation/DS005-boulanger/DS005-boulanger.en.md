﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS005" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Food industry" -->
<!-- var(title)="Baker - Pastry chef" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="food-industry" -->
<!-- var(title-short)="baker-pastry-chef" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/food-industry/baker-pastry-chef.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="baker-pastry-chef" -->
<!-- var(translation)="Auto" -->


Baker - Pastry chef
===================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The baker's name and the bakery trade brand concern the professionals who, from selected raw materials, ensure the kneading of the dough, its fermentation and its shaping, as well as the baking of the bread on the premises. sales to the end consumer.

Dough and breads cannot be frozen or frozen at any stage of production or sale.

The baker's name and the bakery trade sign can also be used when bread is sold mobilely by the professional, or under his responsibility, when the previous conditions are met.

*For further information*: Articles L. 122-17 and L. 122-18 of the Consumer Code.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the activity carried out, the legal form of the company and the number of employees employed:

- for craftsmen and commercial companies engaged in artisanal activity, provided they do not employ more than ten employees, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for craftsmen and commercial companies that employ more than ten employees, this is the Chamber of Commerce and Industry (CCI);
- in the departments of the Upper and Lower Rhine, the Alsace Chamber of Trades is competent.

**Please note**

In the departments of Lower Rhine, Upper Rhine and Moselle, the activity remains artisanal, regardless of the number of employees employed, as long as the company does not use an industrial process. The competent CFE is therefore the CMA or the Chamber of Trades of Alsace. For more information, it is advisable to refer[official website of the Chamber of Trades of Alsace](http://www.cm-alsace.fr/decouvrir-la-cma/lartisanat-en-alsace).

For more information, it is advisable to consult[ICC Paris website](http://www.entreprises.cci-paris-idf.fr/web/formalites/competence-cfe).

2°. Installation conditions
------------------------------------

### a. Professional qualifications

Only a qualified person who is professionally qualified or placed under the effective and permanent control of a qualified person can carry out the activity of baker.

People who are or control the activity of a baker must:

- either hold a Certificate of Professional Qualification (CAP), a Professional Studies Certificate (BEP) or a diploma or a higher level certified or registered at the time of its issuance at the [national directory of professional certifications](http://www.rncp.cncp.gouv.fr/) (RNCP);
- or justify an effective three-year professional experience in the territory of the European Union (EU) or another State party to the European Economic Area (EEA) agreement acquired as a business manager, worker self-employed or employed in the line of duty.

In this second hypothesis, the professional may obtain the issuance of a certificate of professional qualification by the regional CMA or by the regional chamber of trades and crafts in the jurisdiction of which he practises (see infra "3 c). If necessary, apply for a certificate of professional qualification").

The diplomas for the exercise of the activity of baker are in particular:

- CAP specialty "baker";
- The specialty "baker" patent;
- the professional bachelor's degree "baker - pastry chef."

For more information on each of these diplomas, it is advisable to refer to the provisions of the[decree of 21 February 2014 establishing the speciality "baker" of the CAP](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028699556&fastPos=1&fastReqId=1909042753&categorieLien=cid&oldAction=rechTexte),[The decree of 15 February 2012 establishing the specialty "baker" of the professional patent](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025431441&fastPos=1&fastReqId=290217794&categorieLien=cid&oldAction=rechTexte) And[the decree of July 2, 2009 creating the specialty "baker and pastry" of the professional bachelor's degree](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020858999&fastPos=1&fastReqId=1274048211&categorieLien=cid&oldAction=rechTexte).

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 1 of Decree 98-246 of 2 April 1998.

### b. Professional qualifications - European nationals

#### For Freedom to provide services

The professional who is a member of the EU or the EEA may work in France on a temporary and occasional basis as a baker provided that he is legally established in one of these states to carry out the same activity.

In addition, where this activity or training is not regulated in the State of establishment, he must have worked as a baker for at least two years in the ten years preceding the performance he intends to perform. France.

**Please note**

The professional who meets these conditions is exempt from the registration requirements of the trades directory (RM) or the register of companies.

*For further information*: Article 17-1 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 2 of Decree 98-246 of 2 April 1998.

#### For a Freedom of establishment

In order to carry out a permanent baker's activity in France, the professional national of the EU or the EEA must meet one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above "2 degrees). a. Professional qualifications");
- hold a certificate of competency or training document required for the exercise of the activity of baker in an EU or EEA state when that state regulates access or the exercise of this activity on its territory;
- have a certificate of competency or a training certificate that certifies its preparation for the exercise of the baker's activity when this certificate or title has been obtained in an EU or EEA state which does not regulate access or exercise of This activity
- have a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has been a baker for three years in the Member State which has admitted equivalence.

**Please note**

A national of an EU or EEA state that meets one of the above conditions may apply for a certificate of recognition of professional qualification (see below: "3. b. If necessary, apply for a certificate of professional qualification.")

If the individual does not meet any of the above conditions, the CMA may ask him to perform a compensation measure in the following cases:

- if the duration of the certified training is at least one year less than that required to obtain one of the professional qualifications required in France to carry out the activity of baker;
- If the training received covers subjects substantially different from those covered by one of the titles or diplomas required to carry out the activity of baker in France;
- If the effective and permanent control of the baker's activity requires, for the exercise of some of its responsibilities, specific training which is not provided in the Member State of origin and covers substantially different subjects of those covered by the certificate of competency or training designation referred to by the applicant.

The compensation measure consists, in the applicant's choice, of an adjustment course or an aptitude test (see infra "3 c). If necessary, apply for a certificate of professional qualification").

*For further information*: Articles 16 and 17 of Law 96-603 of 5 July 1996; Articles 1 and 3 of Decree 98-246 of 2 April 1998.

### c. Installation Preparation Stage (SPI)

Before registering it at the RM or, for the departments of Lower Rhine, Upper Rhine and Moselle, in the register of companies, the future entrepreneur must have followed an SPI.

**Please note**

If a reason of force majeure prevents it, the professional is allowed to fulfill his obligation within one year of his registration or registration.

The SPI is composed of:

- a first part devoted to the introduction to general accounting and analytic accounting, as well as information on the economic, legal and social environment of the artisanal business and on social and social responsibility and environmental issues;
- a second part that includes a post-registration follow-up period.

The Chamber of Trades, the establishment or the centre seized by an application for an internship is required to start the internship within 30 days. After this period, the registration of the future business owner cannot be refused or deferred, without prejudice to the other obligations conditioning the registration.

#### Internship expense

The future entrepreneur may be excused from following the SPI:

- if he has a level 3-certified degree or degree with a degree in economics and business management or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge at least equivalent to that provided by the internship.

#### Internship exemption for EU nationals

To settle in France, a qualified professional who is a national of the EU or the EEA is exempt from following the SPI.

The CMA may ask the CMA to submit to an aptitude test or an adjustment course when the examination of his professional qualifications reveals that the national has not received management training or that his training relates to substantially different materials from those covered by the SPI.

However, the CMA cannot ask the professional to submit to an aptitude test or an adjustment course:

- when he has been engaged in a professional activity requiring at least the same level of knowledge as the SPI for at least three years;
- or when, for at least two consecutive years, he has worked independently or as a business leader, after receiving training for this activity sanctioned by a diploma, title or certificate recognized by the State, member or party, which has issued it, or deemed fully valid by a competent professional body;
- or where, after verification, the board finds that the knowledge acquired by the applicant during his professional experience in a Member State or party, or in a third state, is likely to cover, in full or partially, the difference substantial in terms of content.

*For further information*: Article 2 of Law 82-1091 of 23 December 1982 on the vocational training of craftsmen; Articles 6 and 6-1 of Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

### d. Conditions of honorability and incompatibility

In order to carry out the activity in France, the baker must not be under the influence:

- a prohibition penalty for this activity (this prohibition applies for up to five years);
- a prohibition on directing, managing, administering or controlling, directly or indirectly, any commercial or artisanal enterprise, any farm and any legal entity.

*For further information*: Article 19 of Act 96-603 of 5 July 1996 above; Article 131-6 of the Penal Code; Article L. 653-8 of the Code of Commerce.

### e. Some peculiarities of the regulation of the activity

#### Craftsmanship

The persons who justify:

- either a CAP, a BEP or a certified or registered title when it was issued to the RNCP at least an equivalent level (see supra "2." (a. Professional qualification));
- or work experience in this occupation lasting at least three years.

*For further information*: Article 1 of Decree 98-247 of 2 April 1998 on artisanal qualification and RM.

#### The title of master craftsman

The title of master craftsman is awarded to individuals, including the social leaders of legal entities:

- RM-registered;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

**Please note**

In some cases, persons who do not hold the master's degree may apply for the title of Master Craftsman from the Regional Qualifications Commission.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and RM.

#### The title of best worker in France (MOF)

The professional diploma "one of the best workers in France" is a state diploma that attests to the acquisition of a high qualification in the exercise of a professional activity in the field of crafts, commercial, service, industrial or agricultural.

The diploma is classified at level III of the interdepartmental nomenclature of training levels.

It is issued after an examination called "one of the best workers in France" under a profession called "class" attached to a group of trades.

*For further information*: Article D. 338-9 of the Education Code.

#### Comply with public institutions (ERP) regulations

The baker must comply with the safety and accessibility rules for ERP. For more information, it is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

#### Meeting health standards

The baker must apply the rules of EC Regulation 852/2004 of 29 April 2004 relating to food hygiene which concern:

- The premises used for food;
- Equipment
- Food waste
- Water supply
- Personal hygiene
- food, packaging and packaging.

This regulation provides for the implementation of procedures based on the principles of HACCP*(Hazard Analysis Critical Control Point).*

The baker must also apply the standards set out in the decree of 21 December 2009 which specify the temperatures of preservation of perishable animal products. These include:

- Preservation temperature conditions
- Freezing procedures
- how ground meats are prepared and game received.

Finally, it must comply with the rules on the conservation temperatures of perishable foodstuffs of plant origin enacted by the decree of 8 October 2013.

*For further information*: REGULATION EC 852/2004 of 29 April 2004 on food hygiene; 21 December 2009 order on health rules for retailing, storing and transporting animal products and food in containers; 8 October 2013 order on health rules for retailing, storing and transporting products and foodstuffs other than animal products and food in containers.

#### View prices and product labelling

The baker must inform the consumer, by way of marking, labelling, display or any other appropriate process, about the prices and the specific conditions of the sale.

**Please note**

The baker freely determines the selling price of his products but the sale at a loss is prohibited.

*For further information*: Articles L. 410-2 and L. 442-2 and the following of the Consumer Code.

In addition, a food item can only be marketed if it is accompanied by a reference to identify the lot to which it belongs.

##### Sign

Each category of bread, which is in public view, must be accompanied by a sign at least 15 cm long and at least 2.5 cm high.

On this sign must be:

- The exact name of the bread category
- the weight in grams for breads sold piecemeal (this is not mandatory for breads weighing less than 200 g);
- The price of selling by piece or kilogram depending on whether they are breads sold piecemeal or by weight;
- the reported selling price per kilogram for breads sold on a piecemeal basis weighing more than 200 g.

##### Displays

A white black-printed poster at least 40 cm high and at least 30 cm wide must be affixed to all retail outlets at a maximum height of 2 metres above the ground without an obstacle impeding the view of consumers.

This poster is titled "Bread Price" and lists all categories of breads on sale with:

- The precise name
- Their weight
- their price per piece;
- their price per kg (for breads sold piecemeal weighing 200 g or more).

For the title, the letters must have a minimum height of 2.5 cm and a minimum width of 1.5 cm.

For texts, the numbers must have a minimum height of 2 cm and a minimum width of 1 cm, the letters must have a minimum height of 1 cm and a minimum width of 0.5 cm.

A second poster similar to the previous one but whose dimensions and characters can be halved must be displayed in a display case and visible from the outside.

For more information, it is advisable to consult the[site of the National Confederation of French Bakery](http://www.boulangerie.org/reglementations-economiques-0).

*For further information*: Articles L. 112-1, R. 412-3 to R. 412-10 of the Consumer Code; 78-89/P of 9 August 1978.

#### Show ingredients recognized as allergens

The use in the manufacture or preparation of a food item of foodstuffs causing allergies or intolerances must be made known to the end consumer.

This information must be indicated on or near the commodity itself so that there is no uncertainty as to the commodity to which it relates.

*For further information*: Articles R. 412-12 to R. 412-16 and Appendix IV of the Consumer Code.

#### Calling breads

Breads can only be sold for sale or sold under the name "home bread" or under an equivalent name:

- fully kneaded, shaped and cooked at the place of sale to the end consumer;
- sold to the end consumer, on a mobile basis, by the professional who carried out the kneading, shaping and cooking operations at the same location.

Can only be put up for sale or sold under the name of "French traditional bread," "traditional French bread," "traditional bread of France" or under a name combining these terms breads, whatever their form:

- have not undergone any freezing treatment during their development;
- containing no additives
- and resulting from the cooking of a dough with defined characteristics.

Can only be put up for sale or sold under a name with the additional label "leaven" breads:

- respecting the characteristics of the two previous appellations;
- and with a maximum hydrogen potential (pH) of 4.3 and an endogenous acetic acid content of at least nine hundred parts per million.

*For further information*: Decree 93-1074 of 13 September 1993 for the application of the law of 1 August 1905 with regard to certain categories of bread.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The baker must be registered with the RM. For more information, please refer to the "Artisanal Company Reporting Formalities" sheet.

### b. If necessary, apply for an exemption from SPI

To be exempt from SPI, the person concerned (French or EU or EEA national) must apply for an exemption from internship.

#### Competent authority 

The request must be addressed to the president of the Chamber of Trades and Crafts of the region.

#### Supporting documents 

The applicant must accompany his mail:

- Copying the Grade III-approved diploma;
- Copying the master's degree
- proof of a professional activity requiring an equivalent level of knowledge.

#### Time 

If the exemption is not answered within one month of receiving the application, the exemption is considered to be granted.

*For further information*: Articles 6 and 6-1 of Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

### c. If necessary, apply for a certificate of professional qualification

The EU or EEA national may apply for a certificate of professional qualification to exercise effective and permanent control over the baker's activity.

#### Competent authority

The request must be addressed to the regional CMA or to the regional chamber of trades and crafts in which the national wishes to practice.

#### Procedure and response time

The board issues a receipt stating the date of receipt of the full application within one month of receipt.

In the event of an incomplete application, it notifies the applicant of the list of missing documents within a fortnight of receipt and issues the receipt as soon as the file is complete.

In the absence of notification of the board's decision within four months of receipt of the full application, recognition of the professional qualification is deemed to be acquired from the applicant.

The board may, by reasoned decision:

- issue a certificate of qualification when it recognises qualification;
- inform in writing that compensation is required.

#### Supporting documents

The application for certification of professional qualification must be accompanied by:

- diploma, vocational training, certificate of competency or certificate of activity (see above);
- Proof of the applicant's nationality;
- for documents not drawn up in French, a translation certified in accordance with the original by a sworn or authorized translator.

The CMA may request the disclosure of additional information regarding the level, duration and content of its training to enable it to determine the possible existence of substantial differences with the required French training.

It may also request proof by any means of professional experience that can compensate, in full or in part, for the substantial difference.

#### Compensation measures

The CMA, which is applying for recognition of professional qualification, may decide to subject the applicant to a compensation measure. Its decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is essential to carry out the activity of baker. Only these subjects can be subject to the aptitude test or the adaptation course, which cannot last more than three years.

The applicant informs the CMA of his choice to take an adjustment course or to take an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organised within six months of the CMA's receipt of the applicant's decision to opt for the event. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the aptitude test, the board issues a certificate of professional qualification to the successful applicant within one month.

If the applicant chooses to do an accommodation internship, the CMA sends him a list of all the organizations likely to organize this internship, within one month of receiving the applicant's decision. Failing that, the recognition of the professional qualification is deemed acquired and the board establishes a certificate of professional qualification.

At the end of the adjustment course, the applicant sends the board a certificate certifying that he has completed this internship, accompanied by an evaluation of the organization that organized it. On the basis of this certification, the board issues a certificate of professional qualification to the person concerned within one month.

**Please note**

Any legal action against the CMA's decision to seek compensation is preceded, barely inadmissibility, by an administrative appeal exercised, within two months of notification of that decision, with the prefect department where the room is headquartered.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-146 of 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the Community other state party to the EEA agreement.

### d. If necessary, make a declaration in case of preparation or sale of animal or animal products

Any operator of an establishment producing, handling or storing animal products or foodstuffs containing animal ingredients (meat, dairy products, fish products, eggs, honey), intended for consumption must meet the obligation to declare each of the establishments for which it is responsible, as well as the activities that take place there.

The declaration must be made before the opening of the establishment and renewed in the event of a change of operator, address or nature of the activity.

#### Competent authority

The declaration must be addressed to the prefect of the establishment's implementation department or, for establishments under the authority or guardianship of the Minister of Defence, the army health service, according to the terms provided by the decree of the Minister of Defence.

#### Supporting documents 

Applicant must complete The Cerfa 13984 form*03.

*For further information*: Articles R. 231-4 and R. 233-4 of the Rural code and marine fisheries; Article 2 of the 28 June 1994 decree on the identification and health accreditation of establishments on the market for animal or animal products and safety markings.

