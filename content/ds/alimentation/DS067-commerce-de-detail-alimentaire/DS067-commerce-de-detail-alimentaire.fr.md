﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS067" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentation" -->
<!-- var(title)="Commerce de détail alimentaire" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentation" -->
<!-- var(title-short)="commerce-de-detail-alimentaire" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/alimentation/commerce-de-detail-alimentaire.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="commerce-de-detail-alimentaire" -->
<!-- var(translation)="None" -->

# Commerce de détail alimentaire

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l'activité

### a. Définition

Le commerce de détail alimentaire consiste en l'achat de marchandises destinées à être consommées par des particuliers. Cette activité se fait en magasin de taille plus ou moins importante mais également par internet ou sur des marchés.

Les produits à consommer ne sont pas transformés avant d'être revendus.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Qualifications professionnelles

### a. Qualifications professionnelles

Aucun diplôme spécifique n'est requis pour l'exploitation d'un commerce de détail alimentaire. Toutefois, il est conseillé d'avoir des connaissances en matière comptable et en gestion.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

Le ressortissant d'un État membre de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE) n'est soumis à aucune condition de diplôme ou de certification, au même titre que les Français.

### c. Quelques particularités de la réglementation de l'activité

#### Affichage des prix

L'exploitant d'un commerce de détail alimentaire doit informer le public des prix pratiqués en utilisant les voies du marquage, de l'étiquetage, de l'affichage ou par tout autre procédé approprié.

Chaque produit doit comporter un prix à l'unité de mesure, c'est à dire un prix au kilo ou au litre, et un prix de vente.

#### Le cas échéant, respecter la réglementation générale applicable à tous les établissements recevant du public (ERP)

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

#### Vidéosurveillance

Les commerces de détail alimentaire peuvent être équipés de systèmes permettant la vidéosurveillance des clients. L'exploitant devra dès lors indiquer leur présence afin de ne pas porter atteinte à la vie privée de ces derniers.

L'exploitant devra également déclarer son installation auprès de la préfecture du lieu de l'établissement et remplir le formulaire [Cerfa n° 13806](https://www.service-public.fr/particuliers/vosdroits/R13984), accompagné de tout document détaillant le système utilisé.

## 3°. Démarches et formalités d'installation

### a. Formalités de déclaration de l’entreprise

L’entrepreneur doit s’immatriculer au registre du commerce et des sociétés (RCS).

### b. Le cas échéant, demander une autorisation d'exploitation

L'intéressé doit demander une autorisation pour exploiter son commerce de détail alimentaire et, le cas échéant, avant l'octroi du permis de construire dans les cas suivants :

- création d'un magasin de commerce de détail d'une surface de vente supérieure à 1 000 mètres carrés, résultant soit d'une construction nouvelle, soit de la transformation d'un immeuble existant ;
- extension de la surface de vente d'un magasin de commerce de détail ayant déjà atteint le seuil des 1 000 mètres carrés ou devant le dépasser par la réalisation du projet ;
- tout changement de secteur d'activité d'un commerce d'une surface de vente supérieure à 1 000 mètres carrés lorsque l'activité nouvelle du magasin est alimentaire ;
- création d'un ensemble commercial et dont la surface de vente totale est supérieure à 1 000 mètres carrés ;
- extension de la surface de vente d'un ensemble commercial ayant déjà atteint le seuil des 1 000 mètres carrés ou devant le dépasser par la réalisation du projet ;
- réouverture au public, sur le même emplacement, d'un magasin de commerce de détail d'une surface de vente supérieure à 1 000 mètres carrés dont les locaux ont cessé d'être exploités pendant trois ans ;
- création ou extension d'un point permanent de retrait par la clientèle d'achats au détail commandés par voie télématique, organisé pour l'accès en automobile.

La demande est adressée en douze exemplaires à la commission départementale d'aménagement commercial (CDAC) de la préfecture du département où se situe le commerce qui dispose d'un délai de deux mois pour autoriser l'exploitation.

La demande est accompagnée de l'ensemble des pièces justificatives suivantes :

- un plan indicatif faisant apparaître la surface de vente des commerces ;
- les renseignements suivants :
  - la délimitation de la zone de chalandise du projet,
  - les transports collectifs et accès pédestres et cyclistes qui sont desservis autour du commerce ;
- une étude comportant les éléments permettant d'apprécier les effets du projet sur :
  - l'accessibilité de l'offre commerciale,
  - les flux de voitures particulières et de véhicules de livraison ainsi que sur les accès sécurisés à la voie publique,
  - la gestion de l'espace,
  - les consommations énergétiques et la pollution,
  - les paysages et les écosystèmes.

En cas de refus, la décision peut faire l'objet d'un recours devant la Commission nationale d'aménagement commercial qui se prononcera dans un délai de quatre mois.

### c. Autorisation(s) post-installation

#### Effectuer une déclaration en cas de préparation ou de vente de denrées animales ou d'origine animale

Tout exploitant d’un établissement produisant, manipulant ou entreposant des denrées d’origine animale ou des denrées comportant des ingrédients d’origine animale (viandes, produits laitiers, produits de la pêche, œufs, miel) destinées à la consommation humaine doit satisfaire à l’obligation de déclarer chacun des établissements dont il a la responsabilité, ainsi que les activités qui s’y déroulent.

Elle doit être faite avant l’ouverture de l’établissement et renouvelée en cas de changement d’exploitant, d’adresse ou de nature de l’activité.

La déclaration doit être adressée au préfet du département d’implantation de l’établissement ou, pour les établissements relevant de l’autorité ou de la tutelle du ministre chargé de la défense, au service de santé des armées.

Pour cela, l'exploitant devra remplir le formulaire [Cerfa n° 13984](https://www.service-public.fr/professionnels-entreprises/vosdroits/R17520) de déclaration qu'il enverra à la direction départementale de la cohésion sociale et de la protection des populations (DDCSPP).

#### Demander un agrément sanitaire en cas de vente de denrées à des intermédiaires

Les établissements préparant, transformant, manipulant ou entreposant des produits d’origine animale ou des denrées alimentaires en contenant destinés à la consommation humaine (lait cru, viandes, œufs, etc.) doivent obtenir un agrément sanitaire pour commercialiser leurs produits auprès d’autres établissements.

##### Autorité compétente

La demande d’agrément doit être adressée à la DDPP du lieu d’implantation de l’établissement.

##### Délai

Si le dossier de demande d’agrément est complet et recevable, un agrément conditionnel est accordé pour une période de trois mois au demandeur.

Au cours de cette période, un contrôle officiel a lieu. Si les conclusions de cette visite sont favorables, un agrément définitif est accordé. Dans le cas contraire, les points de non-conformité sont notifiés au demandeur et l’agrément conditionnel peut être renouvelé pour une nouvelle période de trois mois. La durée totale de l’agrément conditionnel ne peut excéder six mois.

##### Pièces justificatives

La demande d’agrément sanitaire est effectuée au moyen du formulaire Cerfa n° 13983 complété, daté et signé et accompagné de l’ensemble des pièces justificatives listées en annexe II de l'arrêté du 8 juin 2006 relatif à l'agrément sanitaire des établissements mettant sur le marché des produits d'origine animale ou des denrées contenant des produits d'origine animale.

Ces pièces concernent la présentation de l’entreprise, la description de ses activités et le plan de maîtrise sanitaire.

*Pour aller plus loin* : articles L. 233-2 et R. 233-1 du Code rural et de la pêche maritime ; note de service DGAL/SDSSA/N2012-8119 du 12 juin 2012 relative à la procédure d’agrément et à la composition du dossier.

**Bon à savoir**

Il est conseillé de se renseigner auprès de la DDCSPP pour s’assurer de la nécessité de solliciter une demande d’agrément et, le cas échéant, obtenir de l’aide pour établir le dossier de demande.

#### Le cas échéant, obtenir une dérogation à l’obligation d’agrément sanitaire

Les établissements concernés par la dérogation sont les commerces :

- cédant des quantités limitées de denrées à d’autres commerces de détail ;
- dont les distances de livraison vers d’autres commerces de détail n’excèdent pas 80 kilomètres.

Les catégories de produits couvertes par la dérogation concernent notamment :

- les produits laitiers ;
- les laits traités thermiques ;
- les produits à base d’œuf « coquille » ou de lait cru ayant subi un traitement assainissant.

L'exploitant peut obtenir cette dérogation si la distance avec les établissements livrés ne dépasse pas 80 km (ou plus sur décision préfectorale) et :

- pour les laits traités thermiquement, s’il cède au maximum 800 litres par semaine lorsqu’il vend 30% ou moins de sa production totale ou 250 litres par semaine lorsqu’il vend plus de 30% de sa production totale ;
- pour les produits laitiers et pour les produits à base d’œuf coquille ou de lait cru ayant subi un traitement assainissant, s’il cède au maximum 250 kg par semaine lorsqu’il vend 30% ou moins de sa production totale ou 100 kg s’il vend plus de 30% de sa production totale.

##### Autorité compétente

La demande de dérogation doit être adressée à la DDCSPP du département dans lequel est situé l’établissement.

##### Pièces justificatives

La demande de dérogation à l’obligation d’agrément est effectuée au moyen du formulaire Cerfa n° 13982 complété, daté et signé.

##### Coût

Gratuit.

*Pour aller plus loin* : article L. 233-2 du Code rural et de la pêche maritime ; articles 12, 13, annexes 3 et 4 de l’arrêté du 8 juin 2006 relatif à l’agrément sanitaire des établissements mettant sur le marché des produits d’origine animale ou des denrées contenant des produits d’origine animale ; instruction technique DGAL/SDSSA/2014-823 du 10 octobre 2014.