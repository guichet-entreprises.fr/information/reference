﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS067" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Food industry" -->
<!-- var(title)="Food retailer" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="food-industry" -->
<!-- var(title-short)="food-retailer" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/food-industry/food-retailer.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="food-retailer" -->
<!-- var(translation)="Auto" -->


Food retailer
===========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Food retailing consists of the purchase of goods for consumption by individuals. This activity is done in stores of more or less size but also by internet or in markets.

Products to be consumed are not processed before being resold.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- for civil societies, this is the registry of the Commercial Court;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Professional qualifications
----------------------------------------

### a. Professional qualifications

No specific diploma is required for the operation of a food retail business. However, it is advisable to have knowledge of accounting and management.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

A national of a Member State of the European Union (EU) or party to the Agreement on the European Economic Area (EEA) is not subject to any qualification or certification requirements, as are the French.

### c. Some peculiarities of the regulation of the activity

#### Price display

The operator of a food retail business must inform the public of the prices charged using the marking, labelling, display or any other appropriate process.

Each product must have a unit-of-measurement price, i.e. a price per kilo or litre, and a selling price.

#### If necessary, comply with the general regulations applicable to all public institutions (ERP)

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, please refer to the "Public Receiving Establishment (ERP)" sheet.

#### Video surveillance

Food retail stores can be equipped with systems for video surveillance of customers. The operator will therefore have to indicate their presence in order not to infringe on their privacy.

The operator will also have to declare his installation with the prefecture of the establishment and complete the form[Cerfa 13806*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13806.do), accompanied by any document detailing the system used.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The contractor must register with the Trade and Corporate Register (SCN). It is advisable to refer to the "Artisanal Company Reporting Formalities" for more information.

### b. If necessary, seek an operating authorization

The person must apply for permission to operate his food retail business and, if necessary, before granting the building permit in the following cases:

- The creation of a retail store with a retail area of more than 1,000 square metres, resulting either from a new construction or from the transformation of an existing building;
- Expanding the retail space of a retail store that has already reached the 1,000 square metre threshold or is expected to exceed it by completing the project;
- any change in the business line of a business with a sales area of more than 1,000 square metres when the store's new business is food;
- Creating a commercial complex with a total sales area of more than 1,000 square metres;
- extending the sales area of a commercial complex that has already reached the threshold of 1,000 square metres or is expected to exceed it by the project' completion;
- re-opening to the public on the same site of a retail store with a retail area of more than 1,000 square metres whose premises have ceased to operate for three years;
- creation or extension of a permanent point of withdrawal by customers of telematics-controlled retail purchases, organized for automobile access.

The request is sent in twelve copies to the Departmental Commission for Commercial Planning (CDAC) of the prefecture of the department where the trade is located, which has two months to authorize the operation.

The application is accompanied by all the following supporting documents:

- an indicative plan showing the sales area of the shops;
- Information:- delineation of the project's catchment area,
  - public transport and pedestrian and cycling access that are served around the trade;
- a study with the elements to assess the effects of the project on:- Accessibility of the commercial offer,
  - the flow of passenger cars and delivery vehicles as well as secure access to public roads,
  - Space management,
  - energy consumption and pollution,
  - landscapes and ecosystems.

If refused, the decision may be appealed to the National Commercial Planning Commission, which will rule within four months.

### c. Post-installation authorisation

#### Make a declaration in case of preparation or sale of animal or animal products

Any operator of an establishment producing, handling or storing animal products or foodstuffs containing animal ingredients (meat, dairy products, fish products, eggs, honey) for consumption must meet the obligation to declare each of the institutions for which it is responsible, as well as the activities that take place there.

It must be done before the establishment opens and renewed in the event of a change of operator, address or nature of the activity.

The declaration must be addressed to the prefect of the establishment's implementation department or, for establishments under the authority or guardianship of the Minister in charge of defence, to the health service of the armed forces.

To do this, the operator will have to complete the form[Cerfa No. 13984*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13984.do) which it will send to the Departmental Directorate for Social Cohesion and Population Protection (DDCSPP).

#### Ask for health approval if food is sold to intermediaries

Establishments preparing, processing, handling or storing animal products or foodstuffs into containers for human consumption (raw milk, meats, eggs, etc.) must obtain a health approval for market their products to other establishments.

**Competent authority**

The application for accreditation must be addressed to the DDPP of the establishment's location.

**Timeframe**

If the application for accreditation is complete and admissible, a conditional approval is granted for a period of three months to the applicant.

During this period, an official check takes place. If the conclusions of this visit are favourable, final approval is granted. Otherwise, the points of non-compliance are notified to the applicant and the conditional approval may be renewed for a further three months. The total duration of conditional approval may not exceed six months.

**Supporting documents**

The application for health accreditation is made using form CERFA No. 1398303 Completed, dated and signed and accompanied by all the supporting documents listed in Appendix II of the decree of 8 June 2006 relating to the health accreditation of establishments on the market of animal products or foodstuffs containing animal products.

These documents relate to the presentation of the company, the description of its activities and the health management plan.

*To go further* Articles L. 233-2 and R. 233-1 of the Rural code and marine fisheries; DGAL/SDSSA/N2012-8119 of June 12, 2012 relating to the accreditation procedure and the composition of the file.

**Good to know**

It is advisable to check with the SDCSPP to ensure the need to apply for accreditation and, if necessary, to obtain assistance in establishing the application file.

#### If necessary, obtain a waiver from the requirement for health accreditation

The establishments affected by the exemption are the shops:

- ceding limited quantities of food to other retail outlets;
- delivery distances to other retail outlets do not exceed 80 kilometres.

The product categories covered by the exemption include:

- Dairy products
- heat-treated milks
- products made from eggshells or raw milk that have undergone a sanitizing treatment.

The operator may obtain this exemption if the distance to the establishments delivered does not exceed 80 km (or more by prefectural decision) and:

- for heat-treated milks, if it yields a maximum of 800 litres per week when it sells 30% or less of its total production or 250 litres per week when it sells more than 30% of its total production;
- for dairy products and for shell or raw milk egg products that have undergone a sanitizing treatment, if it yields a maximum of 250 kg per week when it sells 30% or less of its total production or 100 kg if it sells more than 30% of its total production.

**Competent authority**

The request for a waiver must be addressed to the DDCSPP of the department in which the institution is located.

**Supporting documents**

The application for waiver of the grant is made using form Cerfa No. 13982Completed, dated and signed.

**Cost**

Free.

*To go further* Article L. 233-2 of the Rural Code and Marine Fisheries; Articles 12, 13, Annexes 3 and 4 of the 8 June 2006 Order on the Health Accreditation of Establishments offering animal products or foodstuffs containing animal products; DGAL/SDSSA/2014-823 from 10 October 2014.

