﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS054" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Food industry" -->
<!-- var(title)="Fast-food/Take-away restaurant" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="food-industry" -->
<!-- var(title-short)="fast-food-take-away-restaurant" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/food-industry/fast-food-take-away-restaurant.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="fast-food-take-away-restaurant" -->
<!-- var(translation)="Auto" -->


Fast-food/Take-away restaurant
====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Fast food and take-out are for establishments serving meals presented in disposable packages to be consumed on site or to take away. This type of activity includes fast food, pizzerias that offer take-out sales, sandwich shops, friteries, etc.

### b. Competent Business Formality Centre

The relevant business formalities centre (CFE) depends on the nature of the activity, the legal form of the company and the number of employees employed:

- for craftsmen and commercial companies engaged in artisanal activity, provided they do not employ more than ten employees, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for craftsmen and commercial companies that employ more than ten employees, this is the Chamber of Commerce and Industry (CCI);
- in the departments of upper and lower Rhine, the Alsace Chamber of Trades is competent.

**Good to know**

In the departments of Lower Rhine, Upper Rhine and Moselle, the activity remains artisanal, regardless of the number of employees employed, as long as the company does not use an industrial process. The competent CFE is therefore the CMA or the Chamber of Trades of Alsace. For more information, it is advisable to refer to the[official website of the Chamber of Trades of Alsace](http://www.cm-alsace.fr/).

*For further information*: Article R. 123-3 of the Code of Commerce.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

#### Hygiene training

No diploma is required to open or manage a restaurant. However, fast food establishments are required to have at least one person on their workforce who can justify food hygiene training.

**Please note**

They are deemed to have met this training obligation:

- Individuals who can justify at least three years of professional experience with a food company as a manager or operator;
- holders of Level V diplomas and professional titles registered in the[national directory of professional certifications](http://www.rncp.cncp.gouv.fr/) (RNCP) which are included in the annex of the decree of 25 November 2011 relating to the list of diplomas and titles for professional purposes whose holders are deemed to meet the specific training obligation in food hygiene adapted to commercial catering establishments.

The training can be delivered by any training organization declared to the regional prefect. It lasts 14 hours and aims to acquire the necessary capacity to organize and manage catering activities in hygienic conditions in accordance with the expectations of the regulations and offering satisfaction to the customer.

At the end of the training, trainees must be able to:

- Identify the main principles of regulation in relation to commercial catering;
- to analyze the risks associated with poor hygiene in commercial catering;
- to implement hygiene principles in commercial catering.

In order to achieve these goals, the training focuses on:

- Food and risk to the consumer
- The fundamentals of EU and national regulation (targeted at commercial catering activity);
- Health control plan.

*For further information*: Articles L. 233-4 and D. 233-11 to D. 233-13 of the Rural code and marine fisheries; Decree of 5 October 2011 relating to the specifications of specific training in food hygiene adapted to the activity of commercial catering establishments; annex to the decree of 25 November 2011 relating to the list of diplomas and titles for professional purposes whose holders are deemed to meet the specific training requirement in food hygiene adapted to the activity of the commercial catering establishments.

### b. For beverage outlets, nationality requirement

There is no nationality requirement to open a fast food restaurant. However, once the restaurateur serves alcoholic beverages, he must justify that he is either:

- French;
- a national of a European Union (EU) state or a state party to the European Economic Area (EEA) agreement;
- a national of a state that has entered into a reciprocity treaty with France, such as Algeria or Canada.

Persons of another nationality cannot open a restaurant serving alcoholic beverages under any circumstances. However, this nationality requirement is not necessary in the event of a temporary debit. For more information, it is advisable to refer to the activity sheet[Drinking](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*For further information*: Articles L. 3334-1 and L. 3352-3 of the Public Health Code.

### c. For beverage outlets, conditions of honourability and incompatibility

There is no condition of honorability or incompatibility to open a fast food restaurant. On the other hand, as long as the restaurateur serves alcoholic beverages, he must respect the conditions of honourability and the incompatibilities relating to the profession of drinking.

#### Incompatibilities and disabilities

Non-emancipated minors and adults under guardianship cannot practice the profession of drinking on their own.

**Please note**

The practice of drinking by a non-emancipated minor or by a major under guardianship is an offence punishable by a fine of 3,750 euros. The court may order the closure of the facility for up to five years.

Moreover, the practice of certain professions is incompatible with the operation of a drinking establishment: bailiffs, notaries, civil servants, etc.

*For further information*: Articles L. 3336-1 and L. 3352-8 of the Public Health Code.

#### Integrity

Do not operate on-site beverage outlets:

- Persons convicted of common crimes or pimping;
- persons sentenced to at least one month's imprisonment for theft, fraud, breach of trust, receiving, spinning, receiving criminals, public indecency, holding a gambling house, taking illegal bets on horse racing, selling falsified or harmful goods, drug offences or for re-assault and public drunkenness. In these cases, the disability ceases in the event of a pardon and when the person has not incurred a correctional sentence of imprisonment for the five years following the conviction.

Incapacity may also be imposed on those convicted of the offence of endangering minors.

Convictions for crimes and for the aforementioned offences against a drink-consuming debit to be consumed on the spot, entail, as a result, rightfully against him and for the same period of time, the prohibition of operating a debit, from the day that those convictions have become final. This debitant cannot be used, in any capacity, in the establishment he operated, nor in the establishment operated by his or her even separated spouse. Nor can he work in the service of the person to whom he has sold or rented, or by whom he manages that establishment.

**Please note**

Violation of these prohibitions is an offence punishable by a fine of 3,750 euros and the permanent closure of the establishment.

*For further information*: Articles L. 3336-1 to L. 3336-3 and L. 3352-9 of the Public Health Code; Articles 225-5 and the following of the Penal Code.

### d. If necessary, obtain a licence to operate

Anyone wishing to open an establishment with a "small restaurant license" or "restaurant license" must undergo specific training that includes:

- knowledge of the legislation and regulations applicable to on-site beverage outlets and restaurants;
- public health and public order obligations.

This training is mandatory and results in the issuance of a 10-year operating licence. At the end of this period, participation in knowledge-update training extends the validity of the operating licence for a further 10 years.

This training is also mandatory in case of transfer (change of ownership), translation (moving) or transfer from one commune to another.

The training programme consists of teachings of a minimum duration of:

- 20 hours spread over at least 3 days;
- 6 hours if the person is justified by ten years of professional experience as an operator;
- 6 hours for knowledge-update training.

For more information, it is advisable to refer to the activity sheet[Drinking](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*For further information*: Articles L. 3332-1-1 and R. 3332-4-1 and the following of the Public Health Code; decree of 22 July 2011 setting out the programme and organisation of the training required to obtain the certificates provided for in Article R. 3332-4-1 of the Public Health Code.

### e. If necessary, obtain a licence

#### License of on-site beverage outlets and restaurateurs

The licence depends on the category of alcohol sold and varies depending on whether the beverages are consumed on-site or sold as a takeaway.

On-site beverage outlets are divided into two categories depending on the extent of the licence they are associated with:

- The 3rd category licence, known as the "restricted licence," includes the authorization to sell group 1 and 3 drinks on site;
- The 4th category licence, known as a "large licence" or "full-year licence," includes the authorization to sell all beverages that remain permitted for consumption indoors, including those of the 4th and 5th group.

**Please note**

Ordinance No. 2015-1682 of December 17, 2015 removed the Second Class Licence. As a result, 2nd category licences become the full right of 3rd category licences.

Restaurants that do not hold an on-site beverage licence must have one of two licensing categories to sell alcoholic beverages:

- the "small restaurant license" which allows the sale of the drinks of the 3rd group for consumption on site, but only on the occasion of the main meals and as accessories of food;
- the "restaurant license" itself, which allows all drinks that are permitted to be consumed on site, but only on the occasion of major meals and as food accessories, to be sold on site.

Establishments with an on-site consumer license or restaurant license may sell drinks in the category of their licence to take away.

*For further information*: Articles L. 3331-1 and the following from the Public Health Code.

**Classifying beverages into four groups**

The beverages are divided into four groups for the regulation of their manufacture, sale and consumption:

- soft drinks (group 1): mineral or carbonated water, fruit or vegetable juices that are not fermented or do not contain, following the start of fermentation, traces of alcohol greater than 1.2 degrees, lemonades, syrups, infusions, milk, coffee, tea, chocolate;
- und distilled fermented beverages and natural sweet wines (group 3): wine, beer, cider, pear, mead, to which are joined natural sweet wines, as well as blackcurrant creams and fermented fruit or vegetable juices with 1.2 to 3 degrees of alcohol, liqueur wines, wine-based appetizers and strawberry liqueurs, raspberries, blackcovers or cherries, with no more than 18 degrees of pure alcohol;
- group 4: rums, tafias, spirits from the distillation of wines, ciders, pears or fruit, and not supporting any addition of gasoline as well as liqueurs sweetened with sugar, glucose or honey at a minimum of 400 grams per litre for aniseed liqueurs and a minimum of 200 grams per litre for other liqueurs and containing no more than half a gram of gasoline per litre;
- Group 5: all other alcoholic beverages.

*For further information*: Article L. 3321-1 of the Public Health Code.

#### Licenses for take-out beverages

Other take-out beverage outlets must, in order to sell alcoholic beverages, be provided with one of the following two licensing categories:

- The "small take-out licence" includes the authorization to sell to take away the drinks of the 3rd group;
- the "take-out licence" itself includes the authorization to sell all beverages for sale.

*For further information*: Article L. 3331-3 of the Public Health Code.

### f. Some peculiarities of the regulation of the activity

#### Comply with public institutions (ERP) regulations

For more information, it is advisable to refer to the activity sheet[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

#### Respecting health and hygiene rules

The restaurateur must apply the rules of EC Regulation 852/2004 of 29 April 2004 relating to food hygiene which concern:

- The premises used for food;
- Equipment
- Food waste
- Water supply
- Personal hygiene
- food, packaging and packaging.

This regulation provides for the implementation of procedures based on the principles of HACCP*(Hazard Analysis Critical Control Point).*

The restaurateur must also apply the standards set out in the December 21, 2009 order on sanitary rules for retailing, storing and transporting animal products and food in Containing.

These include:

- Preservation temperature conditions
- Freezing procedures
- how ground meats are prepared and game received.

Finally, it must comply with the rules on the conservation temperatures of perishable foodstuffs of plant origin enacted by the decree of 8 October 2013.

*For further information*: REGULATION EC 852/2004 of 29 April 2004 on food hygiene; 21 December 2009 order on health rules for retailing, storing and transporting animal products and food in containers; 8 October 2013 order on health rules for retailing, storing and transporting products and foodstuffs other than animal products and food in containers.

#### View prices and mandatory product information

##### Prices of the most commonly served beverages and foodstuffs

Restaurateurs must display in a visible and legible manner from outside the property and on the outdoor pitches reserved for customers, the prices charged, regardless of the place of consumption, of the most common drinks and foodstuffs listed below and named:

- The cup of black coffee
- half a lot of draught beer;
- one bottle of beer (contained served);
- fruit juice (containment served);
- a soda (contained)
- flat or sparkling mineral water (contained)
- aniseed aperitif (contained served);
- a dish of the day;
- A sandwich.

The name and prices must be indicated by letters and figures with a minimum height of 1.5 cm.

*For further information*: order of 27 March 1987 relating to the display of prices in establishments serving meals, foodstuffs or drinks to be consumed on site.

##### Menus and maps

In establishments serving meals, menus or menus of the day, as well as a menu with at least the prices of five wines or otherwise the prices of wines if served less than five, must be displayed in a visible and readable way from the outside during duration of service and at least from 11:30 a.m. for lunch and 6 p.m. for dinner.

In the event that certain menus are served only at certain times of the day, this feature must be clearly mentioned in the document displayed.

In non-wine establishments, a menu with at least the nature and prices of five commonly served beverages will be displayed.

Within these establishments, menus or cards identical to those displayed outside must be made available to customers.

Cards and menus must include, for each service, the price as well as the word "drink included" or "drink not included" and, in all cases, indicate for the drinks the nature and capacity offered.

*For further information*: order of 27 March 1987 relating to the display of prices in establishments serving meals, foodstuffs or drinks to be consumed on site.

##### Mention "homemade"

Restaurateurs must state on their cards or any other medium visible to all consumers that a proposed dish is "homemade".

A dish is "homemade" when made on site from raw products.

*For further information*: Articles L. 122-19 at L. 122-21 and D. 122-1 to D. 122-3 of the Consumer Code.

##### Origin of meats

The origin of beef must be indicated by any of the following:

- "origin: (name of the country)" when the birth, rearing and slaughter of the cattle from which the meats originated took place in the same country;
- "born and raised: (name of country of birth and name of the country of breeding) and slaughtered: (name of the country of slaughter)" when birth, breeding and slaughter took place in different countries.

These mentions are made to the consumer's attention, in a readable and visible way, by display, indication on cards and menus, or on any other medium.

**Please note**

The sale of beef whose origin is not made known to the consumer constitutes a 5th class ticket. The fine is 1,500 euros at most and, in the event of a repeat offence, 3,000 euros.

*For further information*: decree of 17 December 2002 on the labelling of beef in foodservice establishments; Article 131-13 of the Penal Code.

##### Ingredients that can cause allergies or intolerances

In places where meals are offered to be consumed on site, are brought to the consumer's attention, in written form, in a readable and visible way of the places where the public is admitted:

- information on the use of foodstuffs that cause allergies or intolerances;
- how this information is made available to it. In this case, the consumer is able to access the information directly and freely, available in written form.

*For further information*: Articles R. 412-12 to R. 412-16 of the Consumer Code.

#### Comply with regulations on the suppression of public drunkenness and the protection of minors

##### Prohibitions on the sale of beverages

It is forbidden:

- to sell alcoholic beverages to minors or to offer them free of charge. The person issuing the drink requires the customer to establish proof of his or her majority;
- to offer, free or expensive, to a minor any object directly inciting the excessive consumption of alcohol;
- retail on credit for 3rd, 4th and 5th groups to be consumed on-site or to take away;
- selling group 3 to 5 drinks in stadiums, physical education rooms, gymnasiums and, in general, all physical and sports facilities;
- to offer free alcoholic beverages for commercial purposes free of charge or to sell them as a principal for a lump sum, except in the context of declared traditional parties and fairs or news parties and fairs authorized by the State representative when it comes to tastings for sale;
- to offer discounted alcoholic beverages for a limited period of time without also offering reduced-price non-alcoholic beverages.

*For further information*: Articles L. 3335-4, L. 3342-1, L. 3342-3, L. 3322-9 and R. 3335-9 of the Public Health Code; Article 131-1 of the Penal Code.

##### Show off non-alcoholic beverages

In all beverage outlets, a display of non-alcoholic beverages for sale at the establishment is mandatory. The display should include at least ten bottles or containers and, as long as the outlet is supplied, a sample of at least each category of the following beverages:

- fruit juices, vegetable juices;
- carbonated fruit juice drinks;
- sodas;
- lemonades;
- syrups;
- artificially or unreaseded ordinary waters;
- carbonated or non-carbonated mineral waters.

This display, separate from that of other beverages, must be installed prominently in the premises where consumers are served.

*For further information*: Article L. 3323-1 of the Public Health Code.

##### Show provisions on the suppression of public drunkenness and the protection of minors

Restaurateurs selling alcoholic beverages must put up a poster reminding them of the provisions of the Public Health Code relating to the suppression of public drunkenness and the protection of minors.

*For further information*: Article L. 3342-4 of the Public Health Code.

#### Respect the smoking ban

Smoking is not allowed in restaurants. The smoking ban applies in all enclosed and covered areas that are open to the public or that are workplaces.

A**Signalling** should recall the principle of a smoking ban.

However, this rule does not apply to terraces that are outdoor spaces.

In addition, the manager of the establishment may decide to create smoking-only sites. These are closed rooms, used for tobacco consumption and where no services are provided.

For more information on the regulations applicable to terraces and reserved pitches, you can refer to the activity sheets[Drinking](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/) And[Traditional catering](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/restaurant-traditionnel/).

*For further information*: Articles L. 3512-8 and R. 3512-2 to R. 3512-7 of the Public Health Code; circular 2008-292 of 17 September 2008 on the modalities of the application of the second phase of the ban on smoking in places for collective use.

#### If necessary, seek permission to broadcast music

Restaurants wishing to broadcast sound music must obtain permission from the Society of Authors, Composers and Music Publishers (Sacem).

They must pay him two royalties:

- copyrights that pay for the work of creators and publishers. The amount depends on the municipality in which the establishment is located, the number of places and the number of broadcasting devices installed;
- for broadcasting via recorded media, fair remuneration, intended to be distributed between performers and music producers. This fee is collected by Sacem on behalf of the Fair Compensation Collection Corporation (SPRE). It is determined by the number of seats and the number of inhabitants of the commune of the establishment.

For more information, it is advisable to refer to the official websites of the[Sacem](https://clients.Sacem.fr/autorisations/etablissement-de-restauration-rapide?keyword=Restauration+rapide) and the[Spre](http://www.spre.fr/).

*For further information*: Articles L. 214-1 and the following articles of the Intellectual Property Code; November 30, 2011 decision of the Commission under Article L. 214-4 of the Intellectual Property Code amending the January 5, 2010 decision.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, it is recommended to refer to the activity sheets "Declaration of a Commercial Company" and "Registration of an Individual Company in the Register of Trade and Companies."

### b. Declare establishments distributing food products containing animal products

Any operator of an establishment producing, handling or storing animal products (meat, dairy products, fishing products, eggs, honey) for human consumption must satisfy the obligation to declare each of the institutions for which it is responsible, as well as the activities that take place there.

The declaration must be made before the opening of the establishment and renewed in the event of a change of operator, address or nature of the activity.

#### Competent authority 

The declaration must be addressed to the prefect of the establishment's implementation department or, for establishments under the authority or guardianship of the Minister of Defence, to the health service of the armed forces.

#### Supporting documents

Applicant must file Form Cerfa 1398403 filled.

*For further information*: Article R. 233-4 of the Rural Code and Marine Fisheries; Article 2 of the 28 June 1994 decree on the identification and health accreditation of establishments on the market for animal or animal products and safety markings.

### c. If necessary, declare the opening of the restaurant in case of sale of alcohol

A person who wants to open a restaurant and sell alcohol there is required to make a pre-administrative statement. This pre-reporting requirement also applies in the case of:

- Change in the person of the owner or manager
- translation from one place to another. However, reporting is not mandatory when translation is carried out by the owner of the trade fund or its rights holders provided that it does not increase the number of existing debits in the commune and when it is carried out in a unprotected area.

**Please note**

Opening a liquor store without making this prior declaration in the required forms is an offence punishable by a fine of 3,750 euros.

*For further information*: Article L. 3352-3 of the Public Health Code.

**Good to know**

This pre-reporting requirement does not apply in the departments of the Upper Rhine, Lower Rhine and Moselle. In these departments, Article 33 of the Local Code of Professions of 26 July 1900 remains in force. It is therefore up to the conservator to complete an application form to operate a liquor license available in the prefecture and sub-prefecture departments of these three departments.

For more information, you can see the activity sheet [Drinking](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*For further information*: Articles L. 3332-3 to L. 3332-7 of the Public Health Code.

#### Competent authority

The declaration must be addressed to the town hall of the municipality of the place of establishment. In Paris, it is carried out at the police prefecture.

#### Time

The declaration must be made at least 15 days before the opening. In the case of a mutation by death, the reporting period is one month. A receipt is issued immediately.

Within 3 days of the declaration, the mayor of the commune where it was made sends a full copy to the public prosecutor and the prefect.

#### Supporting documents

The necessary supporting documents are:

- Deer 11542 printCompleted;
- The operating licence
- License
- proof of nationality.

