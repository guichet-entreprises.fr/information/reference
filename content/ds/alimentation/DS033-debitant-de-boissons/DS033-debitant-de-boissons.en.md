﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS033" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Food industry" -->
<!-- var(title)="Establishment selling alcoholic drinks" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="food-industry" -->
<!-- var(title-short)="establishment-selling-alcoholic-drinks" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/food-industry/establishment-selling-alcoholic-drinks.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="establishment-selling-alcoholic-drinks" -->
<!-- var(translation)="Auto" -->

Establishment selling alcoholic drinks
========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
(1) Definition of activity
--------------------------

### a. Definition

Anyone who retails non-production alcoholic beverages operates as a beverage outlet.

These include:

- on-site drinking establishments: cafes, pubs, nightclubs, restaurants;
- take-out drinks outlets: supermarkets, grocery stores, wine shops, distance sales or internet sales.

### b. Competent Business Formalities Centre (CFE)

The beverage flow business is commercial in nature. The relevant CFE is therefore the Chamber of Commerce and Industry (CCI).

*For further information*: Article R. 123-3 of the Trade Code.

(2) Installation conditions
---------------------------

### a. Condition of nationality

In order to exercise as a beverage user, the professional must justify that he is either:

- French;
- a national of a European Union (EU) state or a state party to the European Economic Area (EEA) agreement;
- a national of a state that has entered into a reciprocity treaty with France, for example Algeria, Canada.

Persons of another nationality cannot, under any circumstances, open a liquor store.

However, this nationality requirement does not apply to the opening of beverage outlets of any kind to be consumed on site in the grounds of exhibitions or fairs organised by the State, public authorities or recognised associations. public service establishments during the duration of the events (see the box infra "Temporary Beverage Debits").

*For further information*: Article L. 3332-3 and L. 3334-1 of the Public Health Code.

**Good to know**

The opening of a café, cabaret or a debit of drinks to be consumed on site, selling alcohol without justifying French nationality, that of another EU Member State or another State party to the EEA agreement is an offence punishable a fine of 3,750 euros.

*For further information*: Article L. 3352-3 of the Public Health Code.

### b. Conditions of honorability and incompatibility

#### Disabilities

Non-emancipated minors and adults under guardianship cannot practice the profession of drinking on their own.

**Please note**

The practice of drinking by a non-emancipated minor or by a major under guardianship is an offence punishable by a fine of 3,750 euros. The court may order the closure of the facility for up to five years.

*For further information*: Articles L. 3336-1 and L. 3352-8 of the Public Health Code.

#### Incompatibilities

The practice of certain professions is incompatible with the operation of a liquor store, including the professions of judicial officer, notary, civil servants and minor delegate.

*For further information*: Article 20 of Decree No. 56-222 of 29 February 1956 for the application of the ordinance of 2 November 1945 relating to the status of judicial officers; Article 13 of Decree No. 45-0117 of 19 December 1945 for the application of notarial status; Article 25 septies of Law 83-634 of 13 July, bearing the rights and obligations of civil servants; Section 251-19 of the Mining Code.

#### Integrity

Do not operate on-site beverage outlets:

- persons convicted of common crimes or pimping. In this case, the disability is perpetual;
- persons sentenced to at least one month's imprisonment for theft, fraud, breach of trust, receiving, spinning, receiving criminals, public indecency, holding a gambling house, taking illegal bets on horse racing, selling falsified or harmful goods, drug offences or for re-assault and public drunkenness. In these cases, the disability ceases in the event of a pardon and when the person has not incurred a correctional sentence of imprisonment for the five years following the conviction.

Incapacity may also be imposed on those convicted of the offence of endangering minors.

Convictions for crimes and for the aforementioned offences against a drink-consuming debit to be consumed on the spot, entail, as a result, rightfully against him and for the same period of time, the prohibition of operating a debit, from the day that those convictions have become final. This debitant cannot be used, in any capacity, in the establishment he operated, nor in the establishment operated by his or her even separated spouse. Nor can he work in the service of the person to whom he has sold or rented, or by whom he manages that establishment, nor in the establishment which is operated by his or her even separated spouse.

*For further information*: Articles L. 3336-1 to L. 3336-3 of the Public Health Code; Articles 225-5 and the following of the Penal Code.

**Good to know**

Violation of these prohibitions is an offence punishable by a fine of 3,750 euros and the permanent closure of the establishment.

*For further information*: Article L. 3352-9 of the Public Health Code.

### c. Conditions on location

#### Restricting the number of flows

A 3rd category on-site consumption cannot be opened in municipalities where the total number of establishments of this nature and 4th category establishments reaches or exceeds the proportion of one debit per 450 inhabitants, or fraction of that number.

However, this prohibition does not apply to the opening of an establishment when it occurs as a result of a transfer made:

- In the region where it is located;
- within a hotel or a campground and caravan park classified within the meaning of Article L. 311-6 of the Tourism Code.

In addition, the opening of a new 4th category establishment is prohibited outside of cases of opening a temporary outlet (see box infra "Temporary Beverage Debits").

The opening of a liquor store in violation of these provisions is an offence punishable by a fine of 3,750 euros.

*For further information*: Articles L. 3332-1, L. 3332-2 and L. 3352-1 of the Public Health Code.

**Good to know**

These prohibitions do not apply to restaurant owners with the "small restaurant licence" or "restaurant licence."

*For further information*: Article L. 3331-2 of the Public Health Code.

#### Protected areas

A local drinking establishment cannot be opened in protected areas determined by prefectural order. The order sets the distance at which flow cannot be established around certain buildings and establishments, including:

- buildings dedicated to any cult;
- cemeteries;
- public education institutions and private schools as well as all youth training or leisure institutions;
- Prisons;
- barracks, camps, arsenals and all buildings occupied by army, sea and air personnel;
- Buildings assigned to the operation of state-owned transport companies;
- health facilities, retirement homes and any public or private facilities for prevention, treatment and care involving hospitalization as well as departmental dispensaries;
- stadiums, swimming pools, public or private sports fields.

The latter two zones are necessarily protected by prefectural decree.

The protection zones determined by order include the interior of the buildings and establishments in question.

A prefectural decree may also prohibit the establishment of a beverage outlet in industrial or commercial enterprises, given the size of the employees' workforce or their working conditions. Companies that usually have more than a thousand employees are required to be protected.

The opening of a liquor store in violation of these provisions is an offence punishable by a fine of 3,750 euros.

*For further information*: Articles L. 3335-1 and subsequent articles, and Section L. 3352-2 of the Public Health Code.

**Good to know**

These prohibitions do not apply to restaurant owners with the "small restaurant licence" or "restaurant licence."

*For further information*: Article L. 3331-2 of the Public Health Code.

### D. Get a licence to operate

#### Selling drinks to consume on site

Anyone wishing to open a 3rd and 4th grade beverage outlet or an establishment with a "small restaurant license" or "restaurant license" must undergo specific training that includes:

- knowledge of the legislation and regulations applicable to on-site beverage outlets and restaurants;
- public health and public order obligations.

This training is mandatory and results in the issuance of a 10-year operating licence. At the end of this period, participation in knowledge-update training extends the validity of the operating licence for a further 10 years.

This training is also mandatory in case of transfer (change of ownership), translation (moving) or transfer from one commune to another.

The training programme consists of teachings of a minimum duration of:

- 20 hours spread over at least three days;
- 6 hours if the person is justified, on the date of opening, transferring, translation or transfer of a ten-year professional experience as an operator;
- 6 hours for knowledge-update training.

*For further information*: Articles L. 3332-1-1 and R. 3332-4-1 and the following of the Public Health Code; decree of 22 July 2011 setting out the programme and organisation of the training required to obtain the certificates provided for in Article R. 3332-4-1 of the Public Health Code.

#### Sales of takeaway drinks

In all shops other than on-site beverage outlets, anyone who wants to sell alcoholic beverages between 10 p.m. and 8 a.m. must first undergo specific training on:

- knowledge of the legislation and regulations applicable to retail, take-out and distance selling;
- public health and public order obligations.

**Please note**

Distance selling is considered a takeaway.

This training results in the issuance of a licence to sell alcoholic beverages at night. It lasts 7 hours in one day.

*For further information*: Articles L. 3331-4, L. 3332-1-1 and R. 3332-7 of the Public Health Code.

### e. Get a license

The licence depends on the category of alcohol sold and varies depending on the mode of consumption, on-site or take-out.

Temporary beverage outlets are not required to be licensed.

#### Licenses for on-site beverage outlets and restaurateurs

On-site beverage outlets are divided into two categories depending on the extent of the licence they are associated with:

- The 3rd category licence, known as the "restricted licence," includes the authorization to sell group 1 and 3 drinks on site;
- the 4th category "large licence" or "full-year licence" includes the authorization to sell all beverages that remain permitted for consumption indoors, including those of the 4th and 5th group, to consume on-site.

**Please note**

Ordinance No. 2015-1682 of December 17, 2015 removed the Second Class Licence. As a result, 2nd category licences become the right of charge for 3rd category licences.

*For further information*: Article 502 of the General Tax Code; Section L. 3333-1 of the Public Health Code.

**Classifying beverages into four groups**

The beverages are divided into four groups for the regulation of their manufacture, sale and consumption:

- soft drinks (group 1): mineral or carbonated water, fruit or vegetable juices that are not fermented or do not contain, following the start of fermentation, traces of alcohol greater than 1.2 degrees, lemonades, syrups, infusions, milk, coffee, tea, chocolate;
- und distilled fermented beverages and natural sweet wines (group 3): wine, beer, cider, pear, mead, to which are joined natural sweet wines, as well as blackcurrant creams and fermented fruit or vegetable juices with 1.2 to 3 degrees of alcohol, liqueur wines, wine-based appetizers and strawberry liqueurs, raspberries, blackcovers or cherries, with no more than 18 degrees of pure alcohol;
- group 4: rums, tafias, spirits from the distillation of wines, ciders, pears or fruit, and not supporting any addition of gasoline as well as liqueurs sweetened with sugar, glucose or honey at a minimum of 400 grams per litre for aniseed liqueurs and a minimum of 200 grams per litre for other liqueurs and containing no more than half a gram of gasoline per litre;
- Group 5: all other alcoholic beverages.

Restaurants that do not hold an on-site beverage licence must have one of two licensing categories to sell alcoholic beverages:

- the "small restaurant license" which allows the sale of the drinks of the 3rd group for consumption on site, but only on the occasion of the main meals and as accessories of food;
- the "restaurant license" itself, which allows all drinks that are permitted to be consumed on site, but only on the occasion of major meals and as food accessories, to be sold on site.

Establishments with an on-site consumer license or restaurant license may sell drinks in the category of their licence to take away.

*For further information*: Articles L. 3331-1 to L. 3331-3 of the Public Health Code.

#### Licenses for take-out beverages

Other take-out beverage outlets must, in order to sell alcoholic beverages, be provided with one of the following two licensing categories:

- The "small take-out licence" includes the authorization to sell to take away the drinks of the 3rd group;
- the "take-out licence" itself includes the authorization to sell all beverages for sale.

*For further information*: Articles L. 3331-1 and L. 3331-2 of the Public Health Code.

### f. Some peculiarities of the regulation of the activity

#### Comply with regulations for institutions receiving the public

For more information, it is recommended to refer to the listing " [Institutions receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) ».

#### Prohibitions on the sale of alcoholic beverages

It is forbidden to:

- to sell alcoholic beverages to minors or to offer them free of charge. The person issuing the drink requires the customer to establish proof of his or her majority;
- offering, free or expensive, to a minor any object directly inciting the excessive consumption of alcohol is also prohibited;
- receive under the age of 16 in drinking establishments who are not accompanied by their father, mother, guardian or any other person over the age of 18 in charge or supervision. However, minors over the age of thirteen, even unaccompanied, may be received in beverage outlets with a first-class licence;
- retail on credit for 3rd, 4th and 5th groups to be consumed on-site or to take away;
- offer free at will alcoholic beverages for commercial purposes or sell them as a principal for a lump sum, except in the context of declared traditional parties and fairs or news parties and fairs authorized by the State representative in the department or when it comes to tastings for sale;
- at fuel outlets, sell take-away alcoholic beverages between 6 p.m. and 8 a.m. and chilled alcoholic beverages;
- offer discounted alcoholic beverages for a limited period of time without also offering reduced-price non-alcoholic beverages;
- sell and distribute group 3 to 5 drinks in stadiums, physical education rooms, gymnasiums and, in general, in all physical and sports facilities, unless waived by ministerial decree or Municipal.

*For further information*: Articles L. 3342-1, L. 3342-3, L. 3335-4 and L. 3322-9 of the Public Health Code.

#### Bans on the use of minors

It is forbidden to employ or receive minors on an internship in the drinks outlets to be consumed on site, with the exception of the spouse of the debitant and his parents or allies up to the 4th degree inclusively.

In licensed beverage outlets, this prohibition does not apply to minors over the age of 16 who are receiving training with one or more periods of business to acquire qualification sanctioned by a diploma or an approved designation.

*For further information*: Article L. 3336-4 of the Public Health Code.

#### Show off non-alcoholic beverages

In all beverage outlets, a display of non-alcoholic beverages for sale at the establishment is mandatory. The display should include at least ten bottles or containers and, as long as the outlet is supplied, a sample of at least each category of the following beverages:

- fruit juices, vegetable juices;
- carbonated fruit juice drinks;
- sodas;
- lemonades;
- syrups;
- artificially or unreaseded ordinary waters;
- carbonated or non-carbonated mineral waters.

This display, separate from that of other beverages, must be installed prominently in the premises where consumers are served.

*For further information*: Article L. 3323-1 of the Public Health Code.

#### Show provisions on the suppression of public drunkenness and the protection of minors

Restaurateurs selling alcoholic beverages must put up a poster reminding them of the provisions of the Public Health Code relating to the suppression of public drunkenness and the protection of minors.

*For further information*: Articles L. 3342-4 of the Public Health Code.

#### View the prices of the most commonly served beverages and foodstuffs

Facilities, including hotel operators, who serve meals, food or beverages to be consumed on site, are required to display the prices actually payable by the consumer.

**Please note**

In establishments where a service is collected, the posted price is included in taxes and service. Documents posted or made available to customers must include the words "Service Price included," followed by the instructions, in parentheses, of the rate charged for the remuneration of this service.

Beverage operators must display the prices charged, regardless of where they are consumed, drinks and food prices in any place of consumption, from outside the establishment and on the outside pitches reserved for customers. more commonly served, listed below and named:

- The cup of black coffee
- half a lot of draught beer;
- one bottle of beer (contained served);
- fruit juice (containment served);
- a soda (contained)
- flat or sparkling mineral water (contained)
- aniseed aperitif (contained served);
- a dish of the day;
- A sandwich.

The name and prices must be indicated by letters and figures with a minimum height of 1.5 cm.

**Good to know**

For establishments that offer entertainment facilities or services such as entertainment and music, are displayed, in a visible and readable way from the outside, in place of these mentions the prices of the following services designated:

- ticket and, if the price of it includes a drink, the nature and capacity of it;
- a soft drink (nature and content served);
- an alcoholic beverage served by the glass (nature and capacity served);
- a bottle of whisky (brand and capacity);
- a bottle of vodka or gin (brand and capacity);
- a bottle of champagne (brand and capacity).

Inside the establishment, the display consists of the indication on a document exposed to public view and directly readable by the clientele of the list drawn up by heading, the drinks and goods offered for sale and the price of each service.

In restaurants and for drinks served during meals, these documents can be replaced by a card made available to customers and containing the prices of all the services offered. This card can be a separate document from the menu and, if necessary, can be readable on the back of the menu.

*For further information*: order of 27 March 1987 relating to the display of prices in establishments serving meals, foodstuffs or drinks to be consumed on site.

#### Respect the smoking ban

Smoking is not allowed in restaurants. The smoking ban applies in all enclosed and covered areas that are open to the public or that are workplaces.

Apparent signage should recall the principle of a smoking ban.

*For further information*: Articles L. 3512-8, R. 3512-2 and R. 3512-7 of the Public Health Code.

**Terraces**

The smoking ban does not apply to terraces as it is a site on the sidewalk of a public road where tables and chairs are available for consumers, in front of an establishment and therefore an outdoor space.

Are considered as outdoor spaces:

- the terraces totally discovered, even if they would be closed on their sides;
- covered terraces but whose main side would be fully open (usually the front façade).

In any case, the terrace must be physically separated from the interior of the property. It is therefore forbidden to smoke on a "terrace" which would only be an extension of the establishment, of which no partition would separate it.

*For further information*: circular of 17 September 2008 No. 2008-292 on how to apply the second phase of the ban on smoking in places for collective use.

**Smoking-only locations**

The manager of the establishment may decide to create smoking-only sites. These reserved locations are enclosed rooms, used for tobacco consumption and in which no service is provided. No maintenance and maintenance tasks can be carried out without the air being renewed, in the absence of any occupant, for at least 1 hour.

These locations must:

- be equipped with a mechanical ventilation air extraction device that allows for a minimum air renewal of ten times the volume of the site per hour. This device is completely independent of the building's ventilation or air conditioning system. The room is kept in continuous depression of at least five easteres compared to the connecting parts;
- Have automatic closures without the possibility of unintentional opening
- Not to be a place of passage;
- have an area no more than 20% of the total area of the facility in which the sites are developed without the size of a site exceeding 35 square metres.

*For further information*: Articles R. 3512-3 and R. 3512-4 of the Public Health Code.

#### If necessary, seek permission to broadcast music

Restaurants wishing to broadcast sound music must obtain permission from the Society of Authors, Composers and Music Publishers (Sacem).

They must pay him two royalties:

- Copyright that pays for the work of creators and publishers;
- for broadcasting via recorded media, fair remuneration, intended to be distributed between performers and music producers. This fee is collected by Sacem on behalf of the Fair Compensation Collection Corporation (SPRE).

The amount of copyright depends on:

- The commune in which the establishment is located;
- The number of seats
- The number of broadcast devices installed.

Fair remuneration is determined by the number of seats and the number of inhabitants of the commune of the establishment.

For more information, it is advisable to refer to the official websites of the [Sacem](http://www.Sacem.fr) and the [Spre](http://www.spre.fr).

*For further information*: decision of November 30, 2011 of the commission under Article L. 214-4 of the Intellectual Property Code amending the decision of January 5, 2010.

#### If necessary, comply with health regulations

The food-eating beverage user must apply the rules of EC Regulation 852/2004 of 29 April 2004 relating to food hygiene, which concern:

- The premises used for food;
- Equipment
- Food waste
- Water supply
- Personal hygiene
- food, packaging and packaging.

It must also apply the standards set out in the 21 December 2009 decree on health rules for the retail, warehousing and transport of animal products and food containing, Particular:

- Preservation temperature conditions
- Freezing procedures
- how ground meats are prepared and game received.

In addition, traditional commercial foodservice establishments are required to have at least one person in their workforce who can justify food hygiene training. For more information, please refer to the "Traditional Restoration" listing.

Three degrees. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

#### Registration at the RCS

For more information, please refer to the "Declaration of a Business" fact sheet.

### b. Declare the opening of the beverage outlet, a change of ownership or place of operation

A person who wants to open a café, a cabaret, a liquor store to be consumed on site, a restaurant or a takeaway and sell alcohol there is required to make a pre-declaration to the administrative authority.

This pre-reporting requirement also applies in the case of:

- mutation in the person of the owner or manager of a café or liquor store selling alcohol to be consumed on site;
- translation from one place to another.

On the other hand, the translation on the territory of a commune of an already existing flow is not considered as opening a new debit:

- if it is carried out by the owner of the trading fund or its rightful owners and if it does not increase the number of debits existing in that commune;
- if it is not operated in an area protected by prefectural decree (see supra "conditions relating to the location of implantation").

*For further information*: Articles L. 3332-3 and the following of the Public Health Code.

**Good to know**

Opening a liquor store without making this prior declaration in the required forms is an offence punishable by a fine of 3,750 euros.

*For further information*: Article L. 3352-3 of the Public Health Code.

**Competent authority**

The competent authority is:

- The town hall of the commune of the site of the site;
- in Paris, the police prefecture.

**Please note**

For speeds operated in aircraft and rail vehicles, the declaration is made at the place where the company has its headquarters or principal establishment. If the head office and principal establishment are abroad, the declaration must be made at the place where the company has its main establishment in France. In the case of flows operated on ships and vessels, the declaration is made instead of registration.

*For further information*: Article R. 3332-2 of the Public Health Code.

**Timeframe**

The declaration must be made at least 15 days before the opening. In the case of a mutation by death, the reporting period is one month.

A receipt is issued immediately.

Within three days of the declaration, the mayor of the commune where it was made sends a full copy to the public prosecutor and the prefect.

**Supporting documents**

The supporting documents are:

- Deer 11542 printCompleted, dated and signed;
- The operating licence
- License
- proof of nationality.

**Please note**

This pre-reporting requirement does not apply in the departments of the Upper Rhine, Lower Rhine and Moselle. In these departments, Article 33 of the Local Code of Professions of 26 July 1900 remains in force. It is therefore up to the conservator to complete an application form to operate a liquor license available in the prefecture and sub-prefecture departments of these three departments.

*For further information*: Article L. 3332-5 of the Public Health Code

**Temporary beverage debits**

**Exhibitions and fairs organized by public organisations**

The opening of drinks outlets for consumption on site is permitted within the grounds of exhibitions or fairs organised by the State, public authorities or associations recognized as public institutions for the duration of the Events. By derogation from the rules of common law:

- there is no nationality requirement;
- opening a 4th category flow is possible.

Each opening is subject to the compliant opinion of the general curator of the exhibition or fair or any person with the same quality. This notice is attached to the declaration signed to the town hall, or to the police prefecture in Paris, and to the buralist recipe of indirect contributions.

*For further information*: Article L. 3334-1 of the Public Health Code.

**Fairs, sales and public holidays**

Are not required to report beforehand:

- People who set up coffee shops or beverage outlets at a fair, sale or public party;
- associations that establish coffee shops or beverage outlets for the duration of the public events they organize.

In these two hypotheses, the opening of the temporary debit requires an authorization from the municipal authority, up to the limit, for events organized by the associations, of five annual authorizations per association.

In the outlets and cafes open for these events, it can only be sold or offered drinks from groups 1 and 3. In the departments of Guadeloupe, Guyana and Martinique, an order may authorize the sale of 4th group drinks within the limit of four days per year.

*For further information*: Article L. 3334-2 of the Public Health Code.

**Please note**

The opening of a debit on a temporary basis outside the listed cases constitutes the offence of unlawful opening punishable by a fine of 3,750 euros, depending on the category of the open debit and according to the rule violated.

*For further information*: Articles L. 3352-2 and L. 3352-3 of the Public Health Code.

