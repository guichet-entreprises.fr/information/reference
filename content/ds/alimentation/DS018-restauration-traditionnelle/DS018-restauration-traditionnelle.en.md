﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS018" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Food industry" -->
<!-- var(title)="Traditional restaurant" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="food-industry" -->
<!-- var(title-short)="traditional-restaurant" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/food-industry/traditional-restaurant.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="traditional-restaurant" -->
<!-- var(translation)="Auto" -->


Traditional restaurant
====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Traditional catering refers to establishments that serve meals and beverages to be consumed exclusively on-site for remuneration.

*For further information*: Article L. 3331-2 of the Public Health Code.

### b. Competent Business Formalities Centre (CFE)

The beverage flow business is commercial in nature. The relevant CFE is therefore the Chamber of Commerce and Industry (CCI).

On the other hand, when the main activity of the restaurateur is agricultural in nature, the competent CFE is the chamber of agriculture.

*For further information*: Article R. 123-3 of the Trade Code.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

#### Hygiene training

No diploma is required to open or manage a restaurant. However, traditional commercial foodservice establishments are required to have at least one person in their workforce who can justify food hygiene training.

**Please note**

They are deemed to have met this training obligation:

- Individuals who can justify at least three years of professional experience with a food company as a manager or operator;
- holders of Level V diplomas and professional titles listed in the national directory of professional certifications.

The training can be delivered by any training organization declared to the regional prefect. It lasts 14 hours and aims to acquire the necessary capacity to organize and manage catering activities in hygienic conditions in accordance with the expectations of the regulations and offering satisfaction to the customer.

At the end of the training, trainees must be able to:

- Identify the main principles of regulation in relation to commercial catering;
- to analyze the risks associated with poor hygiene in commercial catering;
- to implement hygiene principles in commercial catering.

In order to achieve these goals, the training focuses on:

- Food and risk to the consumer
- The fundamentals of EU and national regulation (targeted commercial catering);
- Health control plan.

*For further information*: Articles L. 233-4, D. 233-11 and following of the Rural code and marine fisheries; decree of 5 October 2011 relating to the specifications of specific training in food hygiene adapted to the activity of commercial catering establishments.

### b. For beverage outlets, nationality requirement

There is no nationality requirement to open a traditional restaurant. However, once the restaurateur serves alcoholic beverages, he must justify that he is either:

- French;
- a national of a European Union (EU) state or a state party to the European Economic Area (EEA) agreement;
- a national of a state that has a reciprocal treaty with France, such as Algeria or Canada, for example.

Persons of another nationality may not, under any circumstances, open a restaurant serving alcoholic beverages.

For more information, it is advisable to refer to the listing[Drinking](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*For further information*: Article L. 3332-3 of the Public Health Code.

### c. For beverage outlets, conditions of honourability and incompatibility

There is no condition of honorability or incompatibility to open a traditional restaurant. On the other hand, as long as the restaurateur serves alcoholic beverages, he must respect the conditions of honourability and the incompatibilities relating to the profession of drinking.

#### Incompatibilities and disabilities

Non-emancipated minors and adults under guardianship cannot practice the profession of drinking on their own.

**Please note**

The practice of drinking by a non-emancipated minor or by a major under guardianship is an offence punishable by a fine of 3,750 euros. The court may order the closure of the facility for up to five years.

Moreover, the practice of certain professions is incompatible with the operation of a drinking establishment: bailiffs, notaries, civil servants.

*For further information*: Articles L. 3336-1 and L. 3352-8 of the Public Health Code.

#### Integrity

Do not operate on-site beverage outlets:

- persons convicted of common crimes or pimping. In this case, the disability is perpetual;
- persons sentenced to at least one month's imprisonment for theft, fraud, breach of trust, receiving, spinning, receiving criminals, public indecency, holding a gambling house, taking illegal bets on horse racing, selling falsified or harmful goods, drug offences or for re-assault and public drunkenness. In these cases, the disability ceases in the event of a pardon and when the person has not incurred a correctional sentence of imprisonment for the five years following the conviction.

Incapacity may also be imposed on those convicted of the offence of endangering minors.

Convictions for crimes and for the aforementioned offences against a drink-consuming debit to be consumed on the spot, entail, as a result, rightfully against him and for the same period of time, the prohibition of operating a debit, from the day that those convictions have become final. This debitant cannot be used, in any capacity, in the establishment he operated, nor in the establishment operated by his or her even separated spouse. Nor can he work in the service of the person to whom he has sold or rented, or by whom he manages that establishment.

**Please note**

Violation of these prohibitions is an offence punishable by a fine of 3,750 euros and the permanent closure of the establishment.

*For further information*: Articles L. 3336-1 to L. 3336-3 and L. 3352-9 of the Public Health Code; Articles 225-5 and the following of the Penal Code.

### d. If necessary, obtain a licence to operate

Anyone wishing to open an establishment with a "small restaurant license" or "restaurant license" must undergo specific training that includes:

- knowledge of the legislation and regulations applicable to on-site beverage outlets and restaurants;
- public health and public order obligations.

This training is mandatory and results in the issuance of a 10-year operating licence. At the end of this period, participation in knowledge-update training extends the validity of the operating licence for a further 10 years.

This training is also mandatory in case of transfer (change of ownership), translation (moving) or transfer from one commune to another.

The training programme consists of teachings of a minimum duration of:

- 20 hours spread over at least three days;
- 6 hours if the person is justified by ten years of professional experience as an operator;
- 6 hours for knowledge-update training.

For more information, it is advisable to refer to the listing[Drinking](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*For further information*: Articles L. 3332-1-1 and R. 3332-4-1 and the following of the Public Health Code; decree of 22 July 2011 setting out the programme and organisation of the training required to obtain the certificates provided for in Article R. 3332-4-1 of the Public Health Code.

### e. If necessary, obtain a licence

The licence depends on the category of alcohol sold and varies depending on whether the beverages are consumed on-site or sold as a takeaway.

On-site beverage outlets are divided into two categories depending on the extent of the licence they are associated with:

- The 3rd category licence, known as the "restricted licence," includes the authorization to sell on-site beverages from groups one and three;
- The 4th category licence, known as a "large licence" or "full-year licence," includes the authorization to sell all beverages that remain permitted for consumption indoors, including those of the 4th and 5th group.

**Please note**

Second-tier licences become the full right of 3rd category licences.

Restaurants that do not hold an on-site beverage licence must have one of two licensing categories to sell alcoholic beverages:

- the "small restaurant license" which allows the sale of the drinks of the 3rd group for consumption on site, but only on the occasion of the main meals and as accessories of food;
- the "restaurant license" itself, which allows all drinks that are permitted to be consumed on site, but only on the occasion of major meals and as food accessories, to be sold on site.

Establishments with an on-site consumer license or restaurant license may sell drinks in the category of their licence to take away.

*For further information*: Article 502 of the General Tax Code; Articles L. 3333-1 and the following from the Public Health Code.

**Classifying beverages into four groups**

The beverages are divided into four groups for the regulation of their manufacture, sale and consumption:

- soft drinks (group 1): mineral or carbonated water, fruit or vegetable juices that are not fermented or do not contain, following the start of fermentation, traces of alcohol greater than 1.2 degrees, lemonades, syrups, infusions, milk, coffee, tea, chocolate;
- und distilled fermented beverages and natural sweet wines (group 3): wine, beer, cider, pear, mead, to which are joined natural sweet wines, as well as blackcurrant creams and fermented fruit or vegetable juices with 1.2 to 3 degrees of alcohol, liqueur wines, wine-based appetizers and strawberry liqueurs, raspberries, blackcovers or cherries, with no more than 18 degrees of pure alcohol;
- group 4: rums, tafias, spirits from the distillation of wines, ciders, pears or fruit, and not supporting any addition of gasoline as well as liqueurs sweetened with sugar, glucose or honey at a minimum of 400 grams per litre for aniseed liqueurs and a minimum of 200 grams per litre for other liqueurs and containing no more than half a gram of gasoline per litre;
- Group 5: all other alcoholic beverages.

### f. Some peculiarities of the regulation of the activity

#### Comply with public institutions (ERP) regulations

It is advisable to refer to the listing[Institutions receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

#### Respecting health and hygiene rules

The restaurateur must apply the rules of EC Regulation 852/2004 of 29 April 2004 relating to food hygiene which concern:

- The premises used for food;
- Equipment
- Food waste
- Water supply
- Personal hygiene
- food, packaging and packaging.

The restaurateur must also apply the standards set out in the December 21, 2009 order on sanitary rules for retailing, storing and transporting animal products and food in Containing.

These include:

- Preservation temperature conditions
- Freezing procedures
- how ground meats are prepared and game received.

#### View prices

Facilities, including hotel operators, who serve meals, food or beverages to be consumed on site, are required to display the prices actually payable by the consumer.

**Please note**

In establishments where a service is collected, the posted price is included in taxes and service. Documents posted or made available to customers must include the words "Service Price included," followed by the instructions, in parentheses, of the rate charged for the remuneration of this service.

##### Most commonly served beverages and foodstuffs

Restaurateurs must display in a visible and legible manner from outside the property and on the outdoor pitches reserved for customers, the prices charged, regardless of the place of consumption, of the most common drinks and foodstuffs listed below and named:

- The cup of black coffee
- half a lot of draught beer;
- one bottle of beer (contained served);
- fruit juice (containment served);
- a soda (contained)
- flat or sparkling mineral water (contained)
- aniseed aperitif (contained served);
- a dish of the day;
- A sandwich.

The name and prices must be indicated by letters and figures with a minimum height of 1.5 cm.

**Good to know**

For establishments that offer entertainment facilities or services such as entertainment and music, are displayed, in a visible and readable way from the outside, in place of these mentions the prices of the following services designated:

- ticket and, if the price of it includes a drink, the nature and capacity of it;
- a soft drink (nature and content served);
- an alcoholic beverage served by the glass (nature and capacity served);
- a bottle of whisky (brand and capacity);
- a bottle of vodka or gin (brand and capacity);
- a bottle of champagne (brand and capacity).

Inside the establishment, the display consists of the indication on a document exposed to public view and directly readable by the clientele of the list drawn up by heading, the drinks and goods offered for sale and the price of each service.

In restaurants and for drinks served during meals, these documents can be replaced by a card made available to customers and containing the prices of all the services offered. This card can be a separate document from the menu and, if necessary, can be readable on the back of the menu.

*For further information*: order of 27 March 1987 relating to the display of prices in establishments serving meals, foodstuffs or drinks to be consumed on site.

##### Menus and maps

In establishments serving meals, menus or menus of the day, as well as a menu with at least the prices of five wines or otherwise the prices of wines if served less than five, must be displayed in a visible and readable way from the outside during duration of service and at least from 11:30 a.m. for lunch and 6 p.m. for dinner.

In the event that certain menus are served only at certain times of the day, this feature must be clearly mentioned in the document displayed.

In non-wine establishments, a menu with at least the nature and prices of five commonly served beverages will be displayed.

Within these establishments, menus or cards identical to those displayed outside must be made available to customers.

Cards and menus must include, for each service, the price as well as the word "drink included" or "drink not included" and, in all cases, indicate for the drinks the nature and capacity offered.

*For further information*: order of 27 March 1987 relating to the display of prices in establishments serving meals, foodstuffs or drinks to be consumed on site.

#### View "homemade"

Restaurateurs must state on their cards or any other medium visible to all consumers that a proposed dish is "homemade" by the following statement: "homemade" dishes are made on site from raw products.

**Good to know**

A "homemade" dish is made on site from raw products. A raw product is a food product that has not undergone any significant changes, including heating, marining, assembling or a combination of these processes.

*For further information*: Articles L. 122-19 and following, D. 121-13-1 and the following of the Consumer Code.

#### Show the origin of meats

The origin of beef must be indicated by any of the following:

- "origin: (name of the country)" when the birth, rearing and slaughter of the cattle from which the meats originated took place in the same country;
- "born and raised: (name of country of birth and name of the country of breeding) and slaughtered: (name of the country of slaughter)" when birth, breeding and slaughter took place in different countries.

These mentions are made to the consumer's attention, in a readable and visible way, by display, indication on cards and menus, or on any other medium.

**Please note**

The sale of beef whose origin is not made known to the consumer constitutes a 5th class ticket. The fine is 1,500 euros at most and, in the event of a repeat offence, 3,000 euros.*For further information*: Article 131-13 of the Penal Code.

*For further information*: decree of 17 December 2002 on the labelling of beef in foodservice establishments.

#### Show the use of ingredients that can cause allergies or intolerances

In places where meals are offered to be consumed on site, are brought to the consumer's attention, in written form, in a readable and visible way of the places where the public is admitted:

- information on the use of foodstuffs that cause allergies or intolerances;
- how this information is made available to it. In this case, the consumer is able to access the information directly and freely, available in written form.

*For further information*: Articles R. 412-12 and the following of the Consumer Code.

#### Comply with regulations on the suppression of public drunkenness and the protection of minors

##### Prohibitions on the sale of beverages

It is forbidden to:

- to sell alcoholic beverages to minors or to offer them free of charge. The person issuing the drink requires the customer to establish proof of his or her majority;
- offering, free or expensive, to a minor any object directly inciting the excessive consumption of alcohol is also prohibited;
- retail on credit for 3rd, 4th and 5th groups to be consumed on-site or to take away;
- offer free at will alcoholic beverages for commercial purposes or sell them as a principal for a lump sum, except in the context of declared traditional parties and fairs or news parties and fairs authorized by the State representative in the department or when it comes to tastings for sale;
- offer discounted alcoholic beverages for a limited period of time without also offering reduced-price non-alcoholic beverages.

*For further information*: Articles L. 3342-1, L. 3342-3, and L. 3322-9 of the Public Health Code.

##### Show off soft drinks

In all beverage outlets, a display of non-alcoholic beverages for sale at the establishment is mandatory. The display should include at least ten bottles or containers and, as long as the outlet is supplied, a sample of at least each category of the following beverages:

- fruit juices, vegetable juices;
- carbonated fruit juice drinks;
- sodas;
- lemonades;
- syrups;
- artificially or unreaseded ordinary waters;
- carbonated or non-carbonated mineral waters.

This display, separate from that of other beverages, must be installed prominently in the premises where consumers are served.

*For further information*: Article L. 3323-1 of the Public Health Code.

##### Show provisions on the suppression of public drunkenness and the protection of minors

Restaurateurs selling alcoholic beverages must put up a poster reminding them of the provisions of the Public Health Code relating to the suppression of public drunkenness and the protection of minors.

*For further information*: Articles L. 3342-4 of the Public Health Code.

#### Respect the smoking ban

Smoking is not allowed in restaurants. The smoking ban applies in all enclosed and covered areas that are open to the public or that are workplaces.

Apparent signage should recall the principle of a smoking ban.

*For further information*: Articles L. 3512-8, R. 3512-2 and R. 3512-7 of the Public Health Code.

**Terraces**

The smoking ban does not apply to terraces that are sidewalk locations on a public road where tables and chairs are available for consumers in front of an establishment. So these are outdoor spaces.

Are considered as outdoor spaces:

- the terraces totally discovered, even if they would be closed on their sides;
- covered terraces but whose main side would be fully open (usually the front façade).

In any case, the terrace must be physically separated from the interior of the property. It is therefore forbidden to smoke on a "terrace" which would only be an extension of the establishment, of which no partition would separate it.

*For further information*: circular of 17 September 2008 No. 2008-292 on how to apply the second phase of the ban on smoking in places for collective use.

**Smoking-only locations**

The manager of the establishment may decide to create smoking-only sites. These reserved locations are enclosed rooms, used for tobacco use and in which no services are provided. No maintenance and maintenance tasks can be carried out without the air being renewed, in the absence of any occupant, for at least 1 hour.

These locations must:

- be equipped with a mechanical ventilation air extraction device that allows for a minimum air renewal of ten times the volume of the site per hour. This device is completely independent of the building's ventilation or air conditioning system. The room is kept in continuous depression of at least five easteres compared to the connecting parts;
- Have automatic closures without the possibility of unintentional opening
- Not to be a place of passage;
- have an area no more than 20% of the total area of the facility in which the sites are developed without the size of a site exceeding 35 square metres.

*For further information*: Articles R. 3512-3 and R. 3512-4 of the Public Health Code.

#### If necessary, seek permission to broadcast music

Restaurants wishing to broadcast sound music must obtain permission from the Society of Authors, Composers and Music Publishers (Sacem).

They must pay him two royalties:

- Copyright that pays for the work of creators and publishers;
- for broadcasting via recorded media, fair remuneration, intended to be distributed between performers and music producers. This fee is collected by Sacem on behalf of the Fair Compensation Collection Corporation (SPRE).

The amount of copyright depends on:

- The commune in which the establishment is located;
- The number of seats
- The number of broadcast devices installed.

Fair remuneration is determined by the number of seats and the number of inhabitants of the commune of the establishment.

For more information, please refer to the official website of the [Sacem](http://www.sacem.fr) and the [Spre](http://www.spre.fr).

*For further information*: decision of November 30, 2011 of the commission under Article L. 214-4 of the Intellectual Property Code amending the decision of January 5, 2010.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

To give the establishment a legal existence, the operator must declare its opening to the CFE to be registered in the Register of Trade and Companies (RCS). For more information, it is advisable to refer to the formality sheet "Declaration of Commercial Activity".

#### b. Declare establishments distributing food products containing animal products

Any operator of an establishment producing, handling or storing animal products or foodstuffs containing animal ingredients (meat, dairy products, fish products, eggs, honey), intended for consumption must meet the obligation to declare each of the establishments for which it is responsible, as well as the activities that take place there.

**Competent authority**

The prefect of the establishment's implementation department or, for establishments under the authority or guardianship of the Minister of Defence, the health service of the armed forces, according to the terms provided by the decree of the Minister of Defence.

**Supporting documents**

The Cerfa 13984 form03 filled.

*For further information*: Article 6 of Regulation (EC) 852/2004; Article R. 233-4 of the Rural code and marine fisheries; order of 28 June 1994 relating to the identification and health accreditation of establishments on the market of animal or animal products and safety markings.

### c. If necessary, declare the opening of the restaurant in case of sale of alcohol

A person who wants to open a restaurant and sell alcohol there is required to make a pre-administrative statement.

This pre-reporting requirement also applies in the case of:

- Change in the person of the owner or manager
- translation from one place to another.

**Please note**

Opening a liquor store without making this prior declaration in the required forms is an offence punishable by a fine of 3,750 euros.

*For further information*: Article L. 3352-3 of the Public Health Code.

On the other hand, the translation on the territory of a commune of an already existing flow is not considered as opening a new debit:

- if it is carried out by the owner of the trading fund or its rightful owners and if it does not increase the number of existing debits in that commune;
- if it is not operated in a protected area.

**Good to know**

This pre-reporting requirement does not apply in the departments of the Upper Rhine, Lower Rhine and Moselle. In these departments, Article 33 of the Local Code of Professions of 26 July 1900 remains in force. It is therefore up to the conservator to complete an application form to operate a liquor license available in the prefecture and sub-prefecture departments of these three departments.

**Competent authority**

The competent authority is:

- The town hall of the commune of the site of the site;
- in Paris, the police prefecture.

**Timeframe**

The declaration must be made at least 15 days before the opening. In the case of a mutation by death, the reporting period is one month.

A receipt is issued immediately.

Within three days of the declaration, the mayor of the commune where it was made sends a full copy to the public prosecutor and the prefect.

**Supporting documents**

The necessary supporting documents are:

- Deer 11542 printCompleted;
- The operating licence
- License
- proof of nationality.

For more information, it is advisable to refer to the listing[Drinking](https://www.guichet-entreprises.fr/fr/activites-reglementees/alimentation/debitant-de-boissons/).

*For further information*: Articles L. 3332-3 and the following of the Public Health Code.

