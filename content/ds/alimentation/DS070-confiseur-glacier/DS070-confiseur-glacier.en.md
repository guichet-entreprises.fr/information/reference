﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS070" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Food industry" -->
<!-- var(title)="Confectioner-Ice cream parlour" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="food-industry" -->
<!-- var(title-short)="confectioner-ice-cream-parlour" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/food-industry/confectioner-ice-cream-parlour.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="confectioner-ice-cream-parlour" -->
<!-- var(translation)="Auto" -->

Confectioner-Ice cream parlour
====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->

<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The confectioner is a professional whose specialty is the manufacture of confectionery, a sugar-based product, ranging from candy to chocolate, from dredges to fruit pies.

The glacier is a professional whose specialty is the transformation of food products into ice creams, ice creams, sorbets or water ice creams.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI).

If the professional has a buying and resale activity, his activity is both artisanal and commercial.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

Only a qualified person who is professionally qualified or placed under the effective and permanent control of a qualified person can carry out the activity of confectioner-glacier.

The person who performs one of the activities covered in paragraph "1." a. Definition" or who controls the exercise, must be the holder of the choice:

- A Certificate of Professional Qualification (CAP);
- A Professional Studies Patent (BEP);
- a diploma or a degree of equal or higher level approved or registered when it was issued to the national directory of professional certifications.

In the absence of one of these diplomas, the person concerned will have to justify an effective three years' professional experience, in a European Union (EU) state or party to the agreement on the European Economic Area (EEA), acquired as a leader self-employed or salaried in the job of confectioner-icer. In this case, the person concerned will be able to apply to the relevant CMA for a certificate of professional qualification.

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 1 of Decree 98-246 of 2 April 1998; Decree 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act 96-603 of 5 July 1996.

### b. Professional Qualifications: EU or EEA nationals (Free Service Or Freedom of establishment)

#### For a Freedom to provide services

The professional who is a national of the EU or the EEA may work in France, on a temporary and casual basis, as a confectioner-icer on the condition that he is legally established in one of these states to carry out the same activity.

He will first have to request it by written declaration to the relevant CMA (see infra "3°. b. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)).

If neither the activity nor the training leading to it is regulated in the state in which the professional is legally established, the person must also prove that he or she has been in the State for at least one year full-time or part-time during the ten years before the performance he wants to perform in France.

**Please note**

The professional who meets these conditions is exempt from the registration requirements of the Directory of Trades (RM) or the register of companies.

*For further information*: Section 17-1 of Act 96-603 of July 5, 1996.

#### For a Freedom of establishment

In order to carry out the activity of confectioner-glacier in France on a permanent basis, the professional who is a national of the EU or the EEA must fulfil one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above: "2. a. Professional qualifications");
- Hold a certificate of competency or training degree required for the exercise of the activity in an EU or EEA state when that state regulates access or exercise of this activity on its territory;
- have a certificate of competency or a training document that certifies its preparation for the exercise of the activity when this certificate or title has been obtained in an EU or EEA state that does not regulate access or the exercise of this activity;
- be a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has been active for three years in the state that has admitted equivalence.

**Please note**

A national of an EU or EEA state that meets one of the above conditions may apply for a certificate of recognition of professional qualification (see below: "3. c. If necessary, apply for a certificate of professional qualification.")

If the individual does not meet any of the above conditions, the CMA may ask him to perform a compensation measure in the following cases:

- if the duration of the certified training is at least one year less than that required to obtain one of the professional qualifications required in France to carry out the activity;
- If the training received covers subjects substantially different from those covered by one of the qualifications required to carry out the activity in France;
- If the effective and permanent control of the activity requires, for the exercise of some of its remits, specific training which is not provided in the Member State of origin and covers subjects substantially different from those covered by the certificate of competency or training designation referred to by the applicant.

*For further information*: Articles 17 and 17-1 of Act 96-603 of July 5, 1996, Articles 3 to 3-2 of Decree 98-246 of April 2, 1998.

**Good to know: compensation measures**

The CMA, which is applying for a certificate of recognition of professional qualification, notifies the applicant of his decision to have him perform one of the compensation measures. This decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose between an adjustment course of up to three years or an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organised within six months of the receipt by the CMA of the applicant's decision to opt for this test. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the adaptation course, the applicant sends the CMA a certificate certifying that he has validly completed this internship, accompanied by an evaluation of the organization that supervised him. The CMA issues, on the basis of this certificate, a certificate of professional qualification within one month.

The decision to use a compensation measure may be challenged by the person concerned who must file an administrative appeal with the prefect within two months of notification of the decision. If his appeal is dismissed, he can then initiate a legal challenge.

*For further information*: Articles 3 and 3-2 of Decree 98-246 of 2 April 1998, Article 6-1 of Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

### c. Conditions of honorability and incompatibility

No one may practise as a confectioner-icer if he is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 of Act 96-603 of July 5, 1996.

### d. Some peculiarities of the regulation of the activity

#### Regulations on the quality of craftsman and the titles of master craftsman and best worker in France

**Craftsmanship**

To claim the status of craftsman, the person must justify either:

- a CAP, a BEP or a certified or registered title when it was issued to the RNCP at least equivalent (see above: "2. (a)
- professional experience in this trade for at least three years.

*For further information*: Article 1 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of master craftsman**

This title is given to individuals, including the social leaders of legal entities:

- Registered in the trades directory;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

**Please note**

Individuals who do not hold the master's degree can apply to the Regional Qualifications Commission for the title of Master Craftsman under two assumptions:

- when they are registered in the trades directory, have a degree at least equivalent to the master's degree, and justify management and psycho-pedagogical knowledge equivalent to those of the corresponding value units of the master's degree and that they have two years of professional practice;
- when they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

**The title of best worker in France (MOF)**

The professional diploma "one of the best workers in France" is a state diploma that attests to the acquisition of a high qualification in the exercise of a professional activity in the artisanal, commercial, industrial or agricultural field.

The diploma is classified at level III of the interdepartmental nomenclature of training levels. It is issued after an examination called "one of the best workers in France" under a profession called "class" attached to a group of trades.

For more information, it is recommended that you consult[official website](http://www.meilleursouvriersdefrance.info/) "one of the best workers in France."

*For further information*: Article D. 338-9 of the Education Code.

#### Comply with public institutions (ERP) regulations

The confectioner must comply with the safety and accessibility rules for ERP. For more information, it is advisable to refer to the "ERP" activity sheet.

**Meeting health standards**

The confectioner must apply the rules of REGULATION 852/2004 of 29 April 2004 which concern:

- The premises used for food;
- Equipment
- Food waste
- Water supply
- Personal hygiene
- food, packaging and packaging.

It must also respect the principles of the Hazard Analysis Critical Control Point (HACCP) of the above regulation.

The confectioner must also apply the standards set out in the December 21, 2009 decree that specify the storage temperatures of perishable animal products.

These include:

- Preservation temperature conditions
- freezing procedures.

Finally, it must respect the conservation temperatures of perishable foodstuffs of plant origin.

*For further information*: regulation of 29 April 2004 and decree of 21 December 2009 relating to the sanitary rules applicable to the activities of retail, warehousing and transport of animal products and food in containers; 8 October 2013 order on health rules for retailing, storing and transporting products and foodstuffs other than animal products and food in containers.

#### View prices and product labelling

The confectioner must inform the consumer, through marking, labelling, display or any other appropriate process, about the prices and the specific conditions of the sale and execution of the services.

*For further information*: Article L. 112-1 of the Consumer Code.

**Please note**

The confectioner freely determines the selling price of its products but the sale at a loss is prohibited.

*For further information*: Articles L. 410-2, L. 442-1 and the following consumer code.

#### Show ingredients recognized as allergens

The use in the manufacture or preparation of foodstuffs causing allergies or intolerances must be made known to the end consumer.

This information must be indicated on or near the commodity itself so that there is no uncertainty as to the commodity to which it relates.

*For further information*: Articles R. 412-12 to R. 412-16 and Appendix IV of the Consumer Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Follow the installation preparation course (SPI)

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

Registration is done upon presentation of a piece of identification with the territorially competent CMA. The internship has a minimum duration of 30 hours and is in the form of courses and practical work. Its objective is to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

The participant will receive a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**
The internship pays off. As an indication, the training cost about 260 euros in 2017.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including a degree in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who:

- have either worked for at least three years, requiring a level of knowledge equivalent to that provided by the internship;
- either have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, fully or partially, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France for the management of a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship.

He must accompany his mail with the following supporting documents:

- Copying the Level III-approved diploma;
- Copy of the master's degree;
- proof of a professional activity requiring an equivalent level of knowledge;
- paying variable fees.

Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of December 23, 1982, Article 6-1 of Decree 83-517 of June 24, 1983.

### b. Request a pre-declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)

**Competent authority**

The CMA of the place in which the national wishes to carry out the benefit, is competent to issue the prior declaration of activity.

**Supporting documents**

The request for a pre-report of activity is accompanied by a complete file containing the following supporting documents:

- A photocopy of a valid ID
- a certificate justifying that the national is legally established in an EU or EEA state;
- a document justifying the professional qualification of the national who may be, at your choice:- A copy of a diploma, title or certificate,
  - A certificate of competency,
  - any documentation attesting to the national's professional experience.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Please note**

When the file is incomplete, the CMA has a period of fifteen days to inform the national and request all the missing documents.

**Outcome of the procedure**

Upon receipt of all the documents in the file, the CMA has one month to decide:

- either to authorise the benefit where the national justifies three years of work experience in an EU or EEA state, and to attach to that decision a certificate of professional qualification;
- or to authorize the provision when the national's professional qualifications are deemed sufficient;
- or to impose an aptitude test on him when there are substantially different differences between the professional qualifications of the national and those required in France. In the event of a refusal to perform this compensation measure or if it fails to perform, the national will not be able to perform the service in France.

The silence kept by the competent authority in these times is worth authorisation to begin the delivery of service.

*For further information*: Article 2 of the decree of 2 April 1998; Article 2 of the decree of 17 October 2017 relating to the submission of the declaration and requests provided for by Decree 98-246 of 2 April 1998 and Title I of Decree 98-247 of 2 April 1998.

### c. If necessary, request a certificate of recognition of professional qualification

The person concerned wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of recognition of professional qualification.

**Competent authority**

The request must be addressed to the territorially competent CMA.

**Procedure**

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the person concerned to complete it within a fortnight of filing the file. A receipt is issued as soon as the file is complete.

**Supporting documents**

The folder should contain the following parts:

- Applying for a certificate of professional qualification
- A certificate of competency or diploma or vocational training designation; Proof of the applicant's nationality
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, the proofs of the exercise of the activity for three years.

The CMA may request further information about its training or professional experience to determine the possible existence of substantial differences with the professional qualification required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (CIEP) to obtain additional information on the level of training of a diploma or certificate or a foreign designation, the applicant will have to pay a fee Additional.

**What to know**

If necessary, all supporting documents must be translated into French .

**Response time**

Within three months of the receipt, the CMA may:

- Recognise professional qualification and issue certification of professional qualification;
- decide to subject the applicant to a compensation measure and notify him of that decision;
- refuse to issue the certificate of professional qualification.

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

**Remedies**

If the CMA refuses to issue the recognition of professional qualification, the applicant may initiate, within two months of notification of the refusal of the CMA, a legal challenge before the relevant administrative court. Similarly, if the person concerned wishes to challenge the CMA's decision to submit it to a compensation measure, he must first initiate a graceful appeal with the prefect of the department in which the CMA is based, within two months of notification of the decision. CMA. If he does not succeed, he may opt for a litigation before the relevant administrative tribunal.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the Community or another state party to the European Economic Area agreement.

### d. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Formalities of Declaration of Craft Enterprise.

### e. If necessary, make a declaration in case of preparation or sale of animal or animal products

Any operator of an establishment producing, handling or storing animal products or foodstuffs containing animal ingredients (meat, dairy products, fish products, eggs, honey), intended for consumption must meet the obligation to declare each of the establishments for which it is responsible, as well as the activities that take place there.

It must be done before the establishment opens and renewed in the event of a change of operator, address or nature of the activity.

The declaration must be addressed to the prefect of the establishment's implementation department or, for establishments under the authority or guardianship of the Minister of Defence, to the health service of the armed forces.

To do this, the confectioner-glacier will have to fill out the form Cerfa No. 13984*03 it will send to the Departmental Directorate of Social Cohesion and Population Protection (DDCSPP).

*For further information*: Articles R. 231-4 and R. 233-4 of the Rural Code and Marine Fisheries; Article 2 of the 28 June 1994 decree on the identification and health accreditation of establishments on the market for animal or animal products and safety markings.

### f. If necessary, seek health approval in the event of the sale of food to intermediaries

Establishments preparing, processing, handling or storing animal products or foodstuffs into containers for human consumption (raw milk, meats, eggs, etc.) must obtain a health approval for market their products to other establishments.

**Competent authority**

The application for accreditation must be sent to the SDCSPP at the establishment's location.

**Timeframe**

If the application for accreditation is complete and admissible, a conditional approval is granted for a period of three months to the applicant.

During this period, an official check takes place. If the conclusions of this visit are favourable, final approval is granted. Otherwise, the points of non-compliance are notified to the applicant and the conditional approval may be renewed for a further three months. The total duration of conditional approval may not exceed six months.

**Supporting documents**

The application for health accreditation is made using the form Cerfa 13983*03 completed, dated and signed and accompanied by all the supporting documents listed in Appendix II of the decree of 8 June 2006 on the health accreditation of establishments that market animal products or foodstuffs containing animal products.

These documents relate to the presentation of the company, the description of its activities and the health management plan.

*For further information*: Articles L. 233-2 and R. 233-1 of the Rural code and marine fisheries; DGAL/SDSSA/N2012-8119 of June 12, 2012 relating to the accreditation procedure and the composition of the file.

**Good to know**

It is advisable to check with the SDCSPP to ensure the need to apply for accreditation and, if necessary, to obtain assistance in establishing the application file.

### f. If necessary, obtain a waiver from the requirement for health accreditation

The establishments affected by the exemption are the shops:

- ceding limited quantities of food to other retail outlets;
- delivery distances to other retail outlets do not exceed 80 kilometres.

The product categories covered by the exemption include:

- Dairy products
- heat-treated milks
- products made from eggshells or raw milk that have undergone a sanitizing treatment.

The confectioner can obtain this exemption if the distance with the establishments delivered does not exceed 80 km (or more by prefectural decision) and:

- for heat-treated milks, if it yields a maximum of 800 litres per week when it sells 30% or less of its total production or 250 litres per week when it sells more than 30% of its total production;
- for dairy products and for shell or raw milk egg products that have undergone a sanitizing treatment, if it yields a maximum of 250 kg per week when it sells 30% or less of its total production or 100 kg if it sells more than 30% of its total production;

**Competent authority**

The request for a waiver must be addressed to the DDCSPP of the department in which the institution is located.

**Supporting documents**

The application for an exemption from the grant is made using the Cerfa 13982*05 completed, dated and signed.

**Cost**

Free

*For further information*: Article L. 233-2 of the Rural Code and Marine Fisheries; Articles 12, 13, Annexes 3 and 4 of the 8 June 2006 Order on the Health Accreditation of Establishments offering animal products or foodstuffs containing animal products; DGAL/SDSSA/2014-823 from 10 October 2014.

