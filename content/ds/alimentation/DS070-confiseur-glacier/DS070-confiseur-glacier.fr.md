﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS070" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentation" -->
<!-- var(title)="Confiseur-glacier" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentation" -->
<!-- var(title-short)="confiseur-glacier" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/alimentation/confiseur-glacier.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="confiseur-glacier" -->
<!-- var(translation)="None" -->

# Confiseur-glacier

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l'activité

### a. Définition

Le confiseur est un professionnel dont la spécialité est la fabrication de la confiserie, produit à base de sucre, allant du bonbon au chocolat, des dragées aux pÃ¢tes de fruits.

Le glacier est un professionnel dont la spécialité est la transformation de produits alimentaires en glaces, crèmes glacées, sorbets ou encore glaces à l'eau.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l'activité est exercée :

- pour une activité artisanale, le CFE compétent est la chambre des métiers et de l'artisanat (CMA) ;
- pour une activité commerciale, le CFE compétent est la chambre de commerce et d'industrie (CCI).

Si le professionnel a une activité d'achat-revente, son activité est à la fois artisanale et commerciale.

**Bon à savoir**

L'activité est considérée comme commerciale dès lors que l'entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle oÃ¹ l'activité demeure artisanale quel que soit le nombre de salariés de l'entreprise à la condition qu'elle n'utilise pas de procédé industriel). En revanche, si l'entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d'achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d'installation

### a. Qualifications professionnelles

Seule une personne qualifiée professionnellement ou placée sous le contrôle effectif et permanent d'une personne qualifiée peut exercer l'activité de confiseur-glacier.

La personne qui exerce l'une des activités visées au paragraphe « 1°. a. Définition » ou qui en contrôle l'exercice, doit être titulaire au choix :

- d'un certificat d'aptitude professionnelle (CAP) ;
- d'un brevet d'études professionnelles (BEP) ;
- d'un diplôme ou d'un titre de niveau égal ou supérieur homologué ou enregistré lors de sa délivrance au Répertoire national des certifications professionnelles.

À défaut de l'un de ces diplômes, l'intéressé devra justifier d'une expérience professionnelle de trois années effectives, dans un État de l'Union européenne (UE) ou partie à l'accord sur l'Espace économique européen (EEE), acquise en qualité de dirigeant d'entreprise, de travailleur indépendant ou de salarié dans l'exercice du métier de confiseur-glacier. Dans ce cas, l'intéressé pourra effectuer une demande d'attestation de reconnaissance de qualification professionnelle auprès de la CMA compétente.

*Pour aller plus loin* : article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l'artisanat ; article 1 du décret n° 98-246 du 2 avril 1998 ; décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l'exercice des activités prévues à l'article 16 de la loi n° 96-603 du 5 juillet 1996.

### b. Qualifications professionnelles : Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### Pour une Libre Prestation de Services (LPS)

Le professionnel ressortissant de l'UE ou de l'EEE peut exercer en France, à titre temporaire et occasionnel, l'activité de confiseur-glacier à la condition d'être légalement établi dans un de ces États pour y exercer la même activité.

Il devra au préalable en faire la demande par déclaration écrite adressée à la CMA compétente (cf. infra « 3°. b. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS) »).

Si ni l'activité ni la formation y conduisant n'est réglementée dans l'État dans lequel le professionnel est légalement établi, l'intéressé doit en outre prouver qu'il a exercé l'activité dans cet État pendant au moins un an à temps plein ou à temps partiel au cours des dix dernières années précédant la prestation qu'il veut effectuer en France.

**À noter**

Le professionnel qui répond à ces conditions est dispensé des exigences relatives à l'immatriculation au Répertoire des Métiers (RM) ou au registre des entreprises.

*Pour aller plus loin* : article 17-1 de la loi n° 96-603 du 5 juillet 1996.

#### Pour un Libre Établissement (LE)

Pour exercer en France, à titre permanent, l'activité de confiseur-glacier, le professionnel ressortissant de l'UE ou de l'EEE doit remplir l'une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles ») ;
- être titulaire d'une attestation de compétence ou d'un titre de formation requis pour l'exercice de l'activité dans un État de l'UE ou de l'EEE lorsque cet État réglemente l'accès ou l'exercice de cette activité sur son territoire ;
- disposer d'une attestation de compétence ou d'un titre de formation qui certifie sa préparation à l'exercice de l'activité lorsque cette attestation ou ce titre a été obtenu dans un État de l'UE ou de l'EEE qui ne réglemente ni l'accès, ni l'exercice de cette activité ;
- être titulaire d'un diplôme, titre ou certificat acquis dans un État tiers et admis en équivalence par un État de l'UE ou de l'EEE à la condition supplémentaire que l'intéressé ait exercé pendant trois années l'activité dans l'État qui a admis l'équivalence.

**À noter**

Le ressortissant d'un État de l'UE ou de l'EEE qui remplit l'une des conditions précitées peut solliciter une attestation de reconnaissance de qualification professionnelle (cf. infra « 3°. c. Le cas échéant, demander une attestation de qualification professionnelle ».)

Si l'intéressé ne remplit aucune des conditions précitées, la CMA saisie peut lui demander d'accomplir une mesure de compensation dans les cas suivants :

- si la durée de la formation attestée est inférieure d'au moins un an à celle exigée pour obtenir l'une des qualifications professionnelles requises en France pour exercer l'activité ;
- si la formation reçue porte sur des matières substantiellement différentes de celles couvertes par l'un des titres ou diplômes requis pour exercer en France l'activité ;
- si le contrôle effectif et permanent de l'activité nécessite, pour l'exercice de certaines de ses attributions, une formation spécifique qui n'est pas prévue dans l'État membre d'origine et porte sur des matières substantiellement différentes de celles couvertes par l'attestation de compétences ou le titre de formation dont le demandeur fait état.

*Pour aller plus loin* : articles 17 et 17-1 de la loi n° 96-603 du 5 juillet 1996, articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998.

#### Bon à savoir : les mesures de compensation

La CMA, saisie d'une demande de délivrance d'une attestation de reconnaissance de qualification professionnelle, notifie au demandeur sa décision tendant à lui faire accomplir l'une des mesures de compensation. Cette décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est impérative pour exercer en France.

Le demandeur doit alors choisir entre un stage d'adaptation d'une durée maximale de trois ans ou une épreuve d'aptitude.

L'épreuve d'aptitude prend la forme d'un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA, de la décision du demandeur d'opter pour cette épreuve. À défaut, la qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

À l'issue du stage d'adaptation, le demandeur adresse à la CMA une attestation certifiant qu'il a valablement accompli ce stage, accompagnée d'une évaluation de l'organisme qui l'a encadré. La CMA délivre, sur la base de cette attestation, une attestation de qualification professionnelle dans un délai d'un mois.

La décision de recourir à une mesure de compensation peut être contestée par l'intéressé qui doit former un recours administratif auprès du préfet dans un délai de deux mois à compter de la notification de la décision. En cas de rejet de son recours, il peut alors initier un recours contentieux.

*Pour aller plus loin* : articles 3 et 3-2 du décret n° 98-246 du 2 avril 1998, article 6-1 du décret n° 83-517 du 24 juin 1983 fixant les conditions d'application de la loi 82-1091 du 23 décembre 1982 relative à la formation professionnelle des artisans.

### c. Conditions d'honorabilité et incompatibilités

Nul ne peut exercer la profession de confiseur-glacier s'il fait l'objet :

- d'une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale ;
- d'une peine d'interdiction d'exercer une activité professionnelle ou sociale pour l'un des crimes ou délits prévue au 11° de l'article 131-6 du Code pénal.

*Pour aller plus loin* : article 19 de la loi n° 96-603 du 5 juillet 1996.

### d. Quelques particularités de la réglementation de l'activité

#### Réglementation relative à la qualité d'artisan et aux titres de maître artisan et de meilleur ouvrier de France

##### La qualité d'artisan

Pour se prévaloir de la qualité d'artisan, la personne doit justifier soit :

- d'un CAP, d'un BEP ou d'un titre homologué ou enregistré lors de sa délivrance au RNCP d'un niveau au moins équivalent (cf. supra « 2°. a qualifications professionnelles ») ;
- d'une expérience professionnelle dans ce métier de trois ans au moins.

*Pour aller plus loin* : article 1 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

##### Le titre de maître artisan

Ce titre est attribué aux personnes physiques, y compris les dirigeants sociaux des personnes morales :

- immatriculées au répertoire des métiers ;
- titulaires du brevet de maîtrise dans le métier exercé ;
- justifiant d'au moins deux ans de pratique professionnelle.

**À noter**

Les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l'obtention du titre de maître artisan à la commission régionale des qualifications dans deux hypothèses :

- lorsqu'elles sont immatriculées au répertoire des métiers, qu'elles sont titulaires d'un diplôme de niveau de formation au moins équivalent au brevet de maîtrise, qu'elles justifient de connaissances en gestion et en psychopédagogie équivalentes à celles des unités de valeur correspondantes du brevet de maîtrise et qu'elles ont deux ans de pratique professionnelle ;
- lorsqu'elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu'elles disposent d'un savoir-faire reconnu au titre de la promotion de l'artisanat ou de la participation à des actions de formation.

*Pour aller plus loin* : article 3 du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

##### Le titre de meilleur ouvrier de France (MOF)

Le diplôme professionnel « un des meilleurs ouvriers de France » est un diplôme d'État qui atteste l'acquisition d'une haute qualification dans l'exercice d'une activité professionnelle dans le domaine artisanal, commercial, industriel ou agricole.

Le diplôme est classé au niveau III de la nomenclature interministérielle des niveaux de formation. Il est délivré à l'issue d'un examen dénommé « concours un des meilleurs ouvriers de France » au titre d'une profession dénommée « classe », rattachée à un groupe de métiers.

Pour plus d'informations, il est recommandé de consulter le [site officiel](http://www.meilleursouvriersdefrance.info/) du concours « un des meilleurs ouvriers de France ».

*Pour aller plus loin* : article D. 338-9 du Code de l'éducation.

#### Respecter la réglementation relative aux établissements recevant du public (ERP)

Le confiseur-glacier doit se conformer aux règles de sécurité et d'accessibilité relatives aux ERP. Pour plus d'informations, il est conseillé de se reporter à la fiche activité « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) ».

#### Respecter les normes sanitaires

Le confiseur-glacier doit appliquer les règles issues du règlement CE n° 852/2004 du 29 avril 2004 qui concernent :

- les locaux utilisés pour les denrées alimentaires ;
- les équipements ;
- les déchets alimentaires ;
- l'alimentation en eau ;
- l'hygiène personnelle ;
- les denrées alimentaires, leur conditionnement et leur emballage.

Il doit aussi respecter les principes de l'HACCP (Hazard Analysis Critical Control Point) issus du règlement précité.

Le confiseur-glacier doit également appliquer les normes édictées par l'arrêté du 21 décembre 2009 qui précisent les températures de conservation des produits d'origine animale périssables.

Celles-ci concernent notamment :

- les conditions de température de conservation ;
- les modalités de congélation.

Il doit, enfin, respecter les températures de conservation des denrées alimentaires périssables d'origine végétale.

*Pour aller plus loin* : règlement du 29 avril 2004 et arrêté du 21 décembre 2009 relatifs aux règles sanitaires applicables aux activités de commerce de détail, d'entreposage et de transport de produits d'origine animale et denrées alimentaires en contenant ; arrêté du 8 octobre 2013 relatif aux règles sanitaires applicables aux activités de commerce de détail, d'entreposage et de transport de produits et denrées alimentaires autres que les produits d'origine animale et les denrées alimentaires en contenant.

#### Afficher les prix et l'étiquetage des produits

Le confiseur-glacier doit informer le consommateur, par voie de marquage, d'étiquetage, d'affichage ou par tout autre procédé approprié, sur les prix et les conditions particulières de la vente et de l'exécution des services.

*Pour aller plus loin* : article L. 112-1 du Code de la consommation.

**À noter**

Le confiseur-glacier détermine librement le prix de vente de ses produits mais la vente à perte est interdite.

*Pour aller plus loin* : articles L. 410-2, L. 442-1 et suivants du Code de la consommation.

#### Afficher les ingrédients reconnus comme allergènes

L'utilisation dans la fabrication ou la préparation de denrées alimentaires provoquant des allergies ou des intolérances doit être portée à la connaissance du consommateur final.

Cette information doit être indiquée sur la denrée elle-même ou à proximité de celle-ci de façon à ce qu'il n'existe aucune incertitude quant à la denrée à laquelle elle se rapporte.

*Pour aller plus loin* : articles R. 412-12 à R. 412-16 et annexe IV du Code de la consommation.

## 3°. Démarches et formalités d'installation

### a. Suivre le stage de préparation à l'installation (SPI)

Le stage de préparation à l'installation (SPI) est un prérequis obligatoire pour toutes personnes sollicitant leur immatriculation au répertoire des métiers.

#### Modalités du stage

L'inscription se fait sur présentation d'une pièce d'identité auprès de la CMA territorialement compétente. Le stage a une durée minimale de 30 heures et se présente sous la forme de cours et de travaux pratiques. Il a pour objectif de faire acquérir les connaissances essentielles dans les domaines juridique, fiscal, social, comptable nécessaires à la créationd'une entreprise artisanale.

#### Report exceptionnel du début du stage

En cas de force majeure, l'intéressé est autorisé à effectuer le SPI dans un délai d'un an à compter de l'immatriculation de son entreprise au répertoire des métiers. Il ne s'agit pas d'une dispense mais simplement d'un report du stage qui demeure obligatoire.

#### Issue du stage

Le participant recevra une attestation de suivi de stage qu'il devra obligatoirement joindre à son dossier de déclaration d'entreprise.

#### Coût

Le stage est payant. À titre indicatif, la formation coûtait environ 260 euros en 2017.

#### Cas de dispense de stage

L'intéressé peut être dispensé d'effectuer le stage dans deux situations :

- s'il a déjà bénéficié d'une formation sanctionnée par un titre ou un diplôme homologué au niveau III (bac+2), comportant un enseignement en économie et gestion d'entreprise, ou le brevet de maîtrise délivré par une CMA ;
- s'il a exercé, pendant au moins trois ans, une activité professionnelle requérant un niveau de connaissance équivalent à celui fourni par le stage.

#### Dispense de stage pour les ressortissants de l'UE ou de l'EEE

Par principe, un professionnel qualifié ressortissant de l'UE ou de l'EEE est dispensé du SPI s'il justifie auprès de la CMA d'une qualification en gestion d'entreprise lui conférant un niveau de connaissance équivalent à celui fourni par le stage.

La qualification en gestion d'entreprise est reconnue comme équivalente à celle fournie par le stage pour les personnes qui :

- ont soit exercé, pendant au moins trois ans, une activité professionnelle requérant un niveau de connaissances équivalent à celui fourni par le stage ;
- soit disposent de connaissances acquises dans un État de l'UE ou de l'EEE ou un État tiers au cours d'une expérience professionnelle de nature à couvrir, totalement ou partiellement la différence substantielle en termes de contenu.

Pour les personnes ne répondant pas à ces conditions, la chambre consulaire peut exiger qu'elles se soumettent à une mesure de compensation si l'examen de ses qualifications professionnelles fait apparaître des différences substantielles avec celles requises en France pour la direction d'une entreprise artisanale.

#### Modalités de la dispense de stage

Pour être dispensé de SPI, l'intéressé (français ou ressortissant de l'UE ou de l'EEE) doit adresser au président de la CMA concernée une demande de dispense de stage.

Il doit accompagner son courrier des pièces justificatives suivantes :

- copie du diplôme homologué au niveau III ;
- copie du brevet de maîtrise ;
- justificatifs d'exercice d'une activité professionnelle requérant un niveau de connaissance équivalent ;
- l'acquittement de frais variables.

L'absence de réponse dans un délai d'un mois suivant la réception de la demande vaut acceptation de la demande de dispense de stage.

*Pour aller plus loin* : article 2 de la loi n° 82-1091 du 23 décembre 1982, article 6-1 du décret n° 83-517 du 24 juin 1983.

### b. Demander une déclaration préalable d'activité pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

La CMA du lieu dans lequel le ressortissant souhaite réaliser la prestation, est compétente pour délivrer la déclaration préalable d'activité.

#### Pièces justificatives

La demande de déclaration préalable d'activité est accompagnée d'un dossier complet comprenant les pièces justificatives suivantes :

- une photocopie d'une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE ;
- un document justifiant la qualification professionnelle du ressortissant qui peut être, au choix :
  - une copie d'un diplôme, titre ou certificat,
  - une attestation de compétence,
  - tout document attestant de l'expérience professionnelle du ressortissant.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

**À noter**

Lorsque le dossier est incomplet, la CMA dispose d'un délai de quinze jours pour en informer le ressortissant et demander l'ensemble des pièces manquantes.

#### Issue de la procédure

À réception de l'ensemble des pièces du dossier, la CMA dispose d'un délai d'un mois pour décider :

- soit d'autoriser la prestation lorsque le ressortissant justifie d'une expérience professionnelle de trois ans dans un État de l'UE ou de l'EEE, et de joindre à cette décision une attestation de qualification professionnelle ;
- soit d'autoriser la prestation lorsque les qualifications professionnelles du ressortissant sont jugées suffisantes ;
- soit de lui imposer une épreuve d'aptitude lorsqu'il existe des différences substantiellement différentes entre les qualifications professionnelles du ressortissant et celles exigées en France. En cas de refus d'accomplir cette mesure de compensation ou en cas d'échec dans son exécution, le ressortissant ne pourra pas effectuer la prestation de service en France.

Le silence gardé de l'autorité compétente dans ces délais vaut autorisation de débuter la prestation de service.

*Pour aller plus loin* : article 2 du décret du 2 avril 1998 ; article 2 de l'arrêté du 17 octobre 2017 relatif à la présentation de la déclaration et des demandes prévues par le décret n° 98-246 du 2 avril 1998 et le titre Ier du décret n° 98-247 du 2 avril 1998.

### c. Le cas échéant, demander une attestation de reconnaissance de qualification professionnelle

L'intéressé souhaitant faire reconnaître un diplôme autre que celui exigé en France ou son expérience professionnelle peut demander une attestation de reconnaissance de qualification professionnelle.

#### Autorité compétente

La demande doit être adressée à la CMA territorialement compétente.

#### Procédure

Un récépissé de remise de demande est adressé au demandeur dans un délai d'un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l'intéressé de le compléter dans les quinze jours du dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

#### Pièces justificatives

Le dossier doit contenir les pièces suivantes :

- la demande d'attestation de qualification professionnelle ;
- une attestation de compétences ou le diplôme ou titre de formation professionnelle ; la preuve de la nationalité du demandeur ;
- si l'expérience professionnelle a été acquise sur le territoire d'un État de l'UE ou de l'EEE, une attestation portant sur la nature et de la durée de l'activité délivrée par l'autorité compétente dans l'État membre d'origine ;
- si l'expérience professionnelle a été acquise en France, les justificatifs de l'exercice de l'activité pendant trois années.

La CMA peut demander la communication d'informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l'existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du centre international d'études pédagogiques (CIEP) pour obtenir des informations complémentaires sur le niveau de formation d'un diplôme ou d'un certificat ou d'un titre étranger, le demandeur devra s'acquitter de frais supplémentaires.

**À savoir**

Le cas échéant, toutes les pièces justificatives doivent être traduites en français (traduction agrée).

#### Délai de réponse

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA peut :

- reconnaître la qualification professionnelle et délivrer l'attestation de qualification professionnelle ;
- décider de soumettre le demandeur à une mesure de compensation et lui notifier cette décision ;
- refuser de délivrer l'attestation de qualification professionnelle.

En l'absence de décision dans le délai de quatre mois, la demande d'attestation de qualification professionnelle est réputée acquise.

#### Voies de recours

Si la CMA refuse de délivrer la reconnaissance de qualification professionnelle, le demandeur peut initier, dans les deux mois suivant la notification du refus de la CMA, un recours contentieux devant le tribunal administratif compétent. De même, si l'intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d'abord initier un recours gracieux auprès du préfet du département dans lequel la CMA a son siège, dans les deux mois suivant la notification de la décision de la CMA. S'il n'obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin* : articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d'un professionnel ressortissant d'un État membre de la Communauté européenne ou d'un autre État partie à l'accord sur l'Espace économique européen.

### d. Formalités de déclaration de l'entreprise

Suivant la nature de son activité, l'entrepreneur doit s'immatriculer au registre des métiers et de l'artisanat (RMA) ou au registre du commerce et des sociétés (RCS).

### e. Le cas échéant, effectuer une déclaration en cas de préparation ou de vente de denrées animales ou d'origine animale

Tout exploitant d'un établissement produisant, manipulant ou entreposant des denrées d'origine animale ou des denrées comportant des ingrédients d'origine animale (viandes, produits laitiers, produits de la pêche, œufs, miel), destinées à la consommation humaine, doit satisfaire à l'obligation de déclarer chacun des établissements dont il a la responsabilité, ainsi que les activités qui s'y déroulent.

Elle doit être faite avant l'ouverture de l'établissement et renouvelée en cas de changement d'exploitant, d'adresse ou de nature de l'activité.

La déclaration doit être adressée au préfet du département d'implantation de l'établissement ou, pour les établissements relevant de l'autorité ou de la tutelle du ministre de la défense, au service de santé des armées.

Pour cela, le confiseur-glacier devra remplir le formulaire Cerfa n° 13984*03 qu'il enverra à la direction départementale de la cohésion sociale et de la protection des populations (DDCSPP).

*Pour aller plus loin* : articles R. 231-4 et R. 233-4 du Code rural et de la pêche maritime ; article 2 de l'arrêté du 28 juin 1994 relatif à l'identification et à l'agrément sanitaire des établissements mettant sur le marché des denrées animales ou d'origine animale et au marquage de salubrité.

### f. Le cas échéant, solliciter un agrément sanitaire en cas de vente de denrées alimentaires à des intermédiaires

Les établissements préparant, transformant, manipulant ou entreposant des produits d'origine animale ou des denrées alimentaires en contenant destinées à la consommation humaine (lait cru, viandes, œufs, etc.) doivent obtenir un agrément sanitaire pour commercialiser leurs produits auprès d'autres établissements.

#### Autorité compétente

La demande d'agrément doit être adressée à la DDCSPP du lieu d'implantation de l'établissement.

#### Délai

Si le dossier de demande d'agrément est complet et recevable, un agrément conditionnel est accordé pour une période de trois mois au demandeur.

Au cours de cette période, un contrôle officiel a lieu. Si les conclusions de cette visite sont favorables, un agrément définitif est accordé. Dans le cas contraire, les points de non-conformité sont notifiés au demandeur et l'agrément conditionnel peut être renouvelé pour une nouvelle période de trois mois. La durée totale de l'agrément conditionnel ne peut excéder six mois.

#### Pièces justificatives

La demande d'agrément sanitaire est effectuée au moyen du formulaire Cerfa 13983*03 complété, daté et signé et accompagné de l'ensemble des pièces justificatives listées en annexe II de l'arrêté du 8 juin 2006 relatif à l'agrément sanitaire des établissements mettant sur le marché des produits d'origine animale ou des denrées contenant des produits d'origine animale.

Ces pièces concernent la présentation de l'entreprise, la description de ses activités et le plan de maîtrise sanitaire.

*Pour aller plus loin* : articles L. 233-2 et R. 233-1 du Code rural et de la pêche maritime ; note de service DGAL/SDSSA/N2012-8119 du 12 juin 2012 relative à la procédure d'agrément et à la composition du dossier.

**Bon à savoir**

Il est conseillé de se renseigner auprès de la DDCSPP pour s'assurer de la nécessité de solliciter une demande d'agrément et, le cas échéant, obtenir de l'aide pour établir le dossier de demande.

### f. Le cas échéant, obtenir une dérogation à l'obligation d'agrément sanitaire

Les établissements concernés par la dérogation sont les commerces :

- cédant des quantités limitées de denrées à d'autres commerces de détail ;
- dont les distances de livraison vers d'autres commerces de détail n'excèdent pas 80 kilomètres.

Les catégories de produits couvertes par la dérogation concernent notamment :

- les produits laitiers ;
- les laits traités thermiques ;
- les produits à base d'oeuf « coquille » ou de lait cru ayant subi un traitement assainissant.

Le confiseur-glacier peut obtenir cette dérogation si la distance avec les établissements livrés ne dépasse pas 80 km (ou plus sur décision préfectorale) et :

- pour les laits traités thermiquement, s'il cède au maximum 800 litres par semaine lorsqu'il vend 30 % ou moins de sa production totale ou 250 litres par semaine lorsqu'il vend plus de 30 % de sa production totale ;
- pour les produits laitiers et pour les produits à base d'œuf coquille ou de lait cru ayant subi un traitement assainissant, s'il cède au maximum 250 kg par semaine lorsqu'il vend 30 % ou moins de sa production totale ou 100 kg s'il vend plus de 30% de sa production totale ;

#### Autorité compétente

La demande de dérogation doit être adressée à la DDCSPP du département dans lequel est situé l'établissement.

#### Pièces justificatives

La demande de dérogation à l'obligation d'agrément est effectuée au moyen du formulaire Cerfa 13982*05 complété, daté et signé.

#### Coût

Gratuit

*Pour aller plus loin* : article L. 233-2 du Code rural et de la pêche maritime ; articles 12, 13, annexes 3 et 4 de l'arrêté du 8 juin 2006 relatif à l'agrément sanitaire des établissements mettant sur le marché des produits d'origine animale ou des denrées contenant des produits d'origine animale ; instruction technique DGAL/SDSSA/2014-823 du 10 octobre 2014.