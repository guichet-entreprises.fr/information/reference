﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS097" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Food industry" -->
<!-- var(title)="Fishmonger" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="food-industry" -->
<!-- var(title-short)="fishmonger" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/food-industry/fishmonger.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="fishmonger" -->
<!-- var(translation)="Auto" -->


Fishmonger
==========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The fishmonger receives, stores, processes and markets seafood and certain freshwater fish in accordance with the rules of hygiene, health, occupational safety and traceability, in accordance with the current regulatory requirements.

After the purchase, he prepares the fish and shellfish that he presents and enhances. He ensures the act of sale, in particular by welcoming and advising the clientele. At the customer's request, he practices skimming, flaking, evisceration, slicing and filleting the fish.

**Please note**

Some species are kept alive, especially crustaceans.

He works in fishmongers or in the markets.

The fishmonger can also have a catering activity and make prepared meals and terrines.

*For further information*: Appendix I of the March 20, 2007 decree creating the "fishmonger" Certificate of Professional Skills (CAP).

### b. Competent Business Formality Centre

The relevant business formalities centre (CFE) depends on the nature of the activity, the legal form of the company and the number of employees employed:

- for craftsmen and commercial companies engaged in artisanal activity, provided they do not employ more than ten employees, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for craftsmen and commercial companies that employ more than ten employees, this is the Chamber of Commerce and Industry (CCI);
- in the departments of upper and lower Rhine, the Alsace Chamber of Trades is competent.

**Please note**

In the departments of Lower Rhine, Upper Rhine and Moselle, the activity remains artisanal, regardless of the number of employees employed, as long as the company does not use an industrial process. The competent CFE is therefore the CMA or the Chamber of Trades of Alsace. For more information, it is advisable to refer to the[official website of the Chamber of Trades of Alsace](http://www.cm-alsace.fr/decouvrir-la-cma/lartisanat-en-alsace).

*For further information*: Article R. 123-3 of the Code of Commerce.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

Only a qualified person who is professionally qualified or placed under the effective and permanent control of a qualified person can carry out the activity of fishmonger.

People who are engaged in fishmonger's activity or who control the exercise of fishmongers should:

- either hold a Certificate of Professional Qualification (CAP), a Professional Studies Certificate (BEP) or a diploma or a higher level certified or registered at the time of its issuance at the [national directory of professional certifications](http://www.rncp.cncp.gouv.fr/) (RNCP);
- or justify an effective three-year professional experience in the territory of the European Union (EU) or another State party to the European Economic Area (EEA) agreement acquired as a business manager, worker self-employed or employed in the line of duty.

The diplomas for the practice of fishmonger's activity include:

- CAP specialty "fishmonger";
- professional bachelor's degree in fishmonger-scaly-processors.

For more information on each of these diplomas, it is advisable to refer to the provisions of the decree of June 24, 2009 creating the specialty of the professional bachelor's "fish-scaling-processor" and setting its conditions for issuance and decree of March 20, 2007 creating the "fishmonger" certificate of professional aptitude.

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 1 of Decree 98-246 of 2 April 1998.

### b. Professional qualifications - European nationals

#### For a Freedom to provide services

The professional who is a member of the EU or the EEA may work in France on a temporary and occasional basis as a fishmonger provided that he is legally established in one of these states to carry out the same activity.

In addition, where this activity or training is not regulated in the State of Establishment, he must have been a fishmonger for at least two years in the ten years preceding the benefit he intends. to achieve in France.

**Please note**

The professional who meets these conditions is exempt from the registration requirements of the trades directory (RM) or the register of companies.

*For further information*: Article 17-1 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 2 of Decree 98-246 of 2 April 1998.

#### For a Freedom of establishment

In order to carry out fishmonger activity in France on a permanent basis, the professional who is an EU or EEA national must meet one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above "2 degrees). a. Professional qualifications");
- hold a certificate of competency or training degree required for fishmonger's activity in an EU or EEA state when that state regulates access or exercise of this activity on its territory;
- have a certificate of competency or a training certificate that certifies its readiness to carry out fishmonger's activity when this certificate or title has been obtained in an EU or EEA state which does not regulate access or exercise of This activity
- have a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has been fishmonger in the Member State which has admitted for three years equivalence.

**Please note**

A national of an EU or EEA state that meets one of the above conditions may apply for a certificate of recognition of professional qualifications (see below '3'. b. If necessary, apply for a certificate of professional qualification").

If the individual does not meet any of the above conditions, the CMA may ask him to perform a compensation measure in the following cases:

- if the duration of the certified training is at least one year less than that required to obtain one of the professional qualifications required in France to carry out the fishmonger's activity;
- If the training received covers subjects substantially different from those covered by one of the titles or diplomas required to work as a fishmonger in France;
- If the effective and permanent control of fishmonger's activity requires, for the exercise of some of its remits, specific training which is not provided for in the Member State of origin and covers substantially different subjects of those covered by the certificate of competency or training designation referred to by the applicant.

The compensation measure consists, in the applicant's choice, of an adjustment course or an aptitude test (see infra "3 c). If necessary, apply for a certificate of professional qualification").

*For further information*: Articles 16 and 17 of Law 96-603 of 5 July 1996; Articles 1 and 3 of Decree 98-246 of 2 April 1998.

### c. Conditions of honorability and incompatibility

To carry out the activity in France, the fishmonger must not be under the influence:

- a ban on doing so (this ban applies for up to five years);
- a prohibition on directing, managing, administering or controlling, directly or indirectly, any commercial or artisanal enterprise, farm or corporation.

*For further information*: Article 19 of Act 96-603 of 5 July 1996 above; Article 131-6 of the Penal Code; Article L. 653-8 of the Code of Commerce.

### d. Some peculiarities of the regulation of the activity

#### Regulations on the quality of craftsman and the titles of master craftsman and best worker in France

##### Craftsmanship

The persons who justify:

- either a CAP, a BEP or a certified or registered title when it was issued to the RNCP at least an equivalent level (see supra "2." a. Professional qualifications");
- or work experience in this trade for at least three years.

*For further information*: Article 1 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

##### The title of master craftsman

This title is given to individuals, including the social leaders of legal entities:

- Registered in the trades directory;
- holders of a master's degree in the trade;
- justifying at least two years of professional practice.

**Please note**

Individuals who do not hold the master's degree can apply to the Regional Qualifications Commission for the title of Master Craftsman in two situations:

- when they are registered in the trades directory, have a degree at least equivalent to the master's degree, and justify management and psycho-pedagogical knowledge equivalent to those of the corresponding value units of the master's degree and that they have two years of professional practice;
- when they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

*For further information*: Article 3 of Decree 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

##### The title of best worker in France (MOF)

The professional diploma "one of the best workers in France" is a state diploma that attests to the acquisition of a high qualification in the exercise of a professional activity in the artisanal, commercial, industrial or agricultural field.

The diploma is classified at level III of the interdepartmental nomenclature of training levels. It is issued after an examination called "one of the best workers in France" under a profession called "class" attached to a group of trades.

For more information, it is recommended that you consult[official website of the competition "one of the best workers in France"](http://www.meilleursouvriersdefrance.info/).

*For further information*: Article D. 338-9 of the Education Code.

#### Comply with public institutions (ERP) regulations

The fishmonger must comply with the safety and accessibility rules for ERPs. For more information, it is advisable to refer to the activity sheet[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/).

#### Meeting health standards

The fishmonger must apply the rules of REGULATION 852/2004 of 29 April 2004 which concern:

- The premises used for food;
- Equipment
- Food waste
- Water supply
- Personal hygiene
- food, packaging and packaging.

It must also respect the principles of the Hazard Analysis Critical Control Point (HACCP) of the above regulation.

The fishmonger must also apply the standards set out in the 21 December 2009 decree that specify the temperatures of the conservation of perishable animal products. These include:

- Preservation temperature conditions
- Freezing procedures
- how ground meats are prepared and game received.

Finally, it must respect the conservation temperatures of perishable foodstuffs of plant origin.

*For further information*: regulation of 29 April 2004 and decree of 21 December 2009 relating to the sanitary rules applicable to the activities of retail, warehousing and transport of animal products and food in containers; 8 October 2013 order on health rules for retailing, storing and transporting products and foodstuffs other than animal products and food in containers.

#### View prices and mandatory product information

##### Price

The fishmonger must inform the consumer, through marking, labelling, display or any other appropriate process, about the prices and the specific conditions of the sale and execution of the services.

*For further information*: Article L. 112-1 of the Consumer Code.

**Please note**

The fishmonger freely determines the selling price of his products but the sale at a loss is prohibited.

*For further information*: Articles L. 410-2 and L. 442-2 of the Consumer Code.

##### Lot identification

A food item can only be marketed if it is accompanied by a reference to identify the lot to which it belongs. The terms and conditions differ depending on whether or not the product is pre-packaged.

*For further information*: Articles R. 412-3 to R. 412-10 of the Consumer Code; Regulation (EU) 1169/2011 of the European Parliament and the Council of 25 October 2011 on consumer information on food.

##### Origin of products

Fishing and aquaculture products may only be offered for sale to the end consumer or community if appropriate signage or labelling indicates:

- The commercial name of the species and its scientific name;
- the production method, in particular the following references: "fished" or "fished in fresh water" or "high";
- The catch or rearing area of the product and the category of fishing gear used for capture;
- except for one exception, if the product has been thawed;
- if so, the minimum durability date.

This information can be shared using business information such as billboards or posters.

For more information, you can see[Directorate-General for Competition, Consumer Affairs and Fraud Enforcement](http://www.economie.gouv.fr/dgccrf/linformation-des-consommateurs-sur-produits-peche-et-laquaculture) (DGCCRF).

*For further information*: Regulation (EU) 1379/2013 of the European Parliament and the Council of 11 December 2013 providing a common organisation of markets in the fisheries and aquaculture sector.

##### Ingredients recognized as allergens

The use in the manufacture or preparation of a food item of foodstuffs causing allergies or intolerances must be made known to the end consumer.

This information must be indicated on or near the commodity itself so that there is no uncertainty as to the commodity to which it relates.

*For further information*: Articles R. 412-12 to R. 412-16 of the Consumer Code.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The fishmonger must be registered in the Trades and Crafts (RMA) directory and, if necessary, in the Trade and Corporate Register (RCS). For more information, it is advisable to refer to the activity sheets "Artisanal Company Reporting Formalities".

### b. Follow the installation preparation course (SPI)

Before registering in the trades directory or, for the departments of Lower Rhine, Upper Rhine and Moselle, in the register of companies, the future entrepreneur must have followed an SPI.

**Please note**

If a reason of force majeure prevents it, the professional is allowed to fulfill his obligation within one year of his registration or registration.

The SPI is composed of:

- a first part devoted to the introduction to general accounting and analytic accounting as well as information on the economic, legal and social environment of the artisanal business and on social and social responsibility and environmental issues;
- a second part that includes a post-registration follow-up period.

The purpose of the internship is, through courses and practical work, to enable future craftsmen to know the conditions of their installation, financing problems, techniques for forecasting and controlling their operation, to measure knowledge necessary to the sustainability of their business and to inform them about the opportunities for continuing education adapted to their situation.

The chamber of trades, the establishment or the centre seized by an application for an internship is required to start the internship within thirty days. After this period, the registration of the future business owner cannot be refused or deferred, without prejudice to the other obligations conditioning the registration.

#### Internship expense

The future entrepreneur may be excused from following the SPI:

- if he has a level 3-certified degree or degree with a degree in economics and business management or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge at least equivalent to that provided by the internship.

#### Internship exemption for EU nationals

To settle in France, a qualified professional who is a national of the EU or the EEA is exempt from following the SPI.

The CMA may ask the CMA to submit to an aptitude test or an adjustment course when the examination of his professional qualifications reveals that the national has not received management training or that his training relates to substantially different materials from those covered by the SPI.

However, the CMA cannot ask the professional to submit to such a compensation measure:

- when he has been engaged in a professional activity requiring at least the same level of knowledge as the SPI for at least three years;
- when, for at least two consecutive years, he has worked independently or as a business leader, after receiving training for this activity sanctioned by a recognized diploma, title or certificate by the State, member or party, which has issued it, or deemed fully valid by a competent professional body;
- when, after verification, the board finds that the knowledge acquired by the applicant during his professional experience in a Member State or party, or in a third state, is likely to cover, in full or in part, the difference substantial in terms of content.

*For further information*: Article 2 of Law 82-1091 of 23 December 1982 on the vocational training of craftsmen; Articles 6 and 6-1 of Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

#### Terms of exemption

The request for exemption must be addressed to the President of the Regional CMA.

The applicant must accompany his mail:

- Copying the Grade III-approved diploma;
- Copying the master's degree
- proof of a professional activity requiring an equivalent level of knowledge.

If the exemption is not answered within one month of receiving the application, the exemption is considered to be granted.

*For further information*: Article 2 of Law 82-1091 of 23 December 1982 on the vocational training of craftsmen; Articles 1 to 7 of Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

### c. If necessary, apply for a certificate of professional qualification

The EU or EEA national may apply for a certificate of professional qualification to exercise effective and permanent control over fishmonger's activity.

#### Competent authority

The request must be addressed to the regional CMA or to the regional chamber of trades and crafts in which the national wishes to practice.

#### Procedure and response time

The board issues a receipt stating the date of receipt of the full application within one month of receipt.

In the event of an incomplete application, it notifies the applicant of the list of missing documents within 15 days of receipt and issues the receipt as soon as the file is complete.

In the absence of notification of the board's decision within four months of receipt of the full application, recognition of the professional qualification is deemed to be acquired from the applicant.

The board may, by reasoned decision:

- issue a certificate of recognition of professional qualification when it recognises professional qualification;
- inform in writing that compensation is required.

#### Supporting documents

The application for certification of professional qualification must be accompanied by:

- diploma, vocational training, certificate of competency or certificate of activity (see supra "2.2). b. Professional qualifications - European nationals");
- Proof of the applicant's nationality;
- for documents not drawn up in French, a translation certified in accordance with the original by a sworn or authorized translator.

The CMA may request the disclosure of additional information regarding the level, duration and content of its training to enable it to determine the possible existence of substantial differences with the required French training.

It may also request proof by any means of professional experience that can compensate, in full or in part, for the substantial difference.

#### Compensation measures

The CMA, which is applying for recognition of professional qualification, may decide to subject the applicant to a compensation measure. Its decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is essential to carry out the activity of fishmonger. Only these subjects can be subject to the aptitude test or the adaptation course, which cannot last more than three years.

The applicant informs the CMA of his choice to take an adjustment course or to take an aptitude test.

**The aptitude test** takes the form of a panel exam. It is organised within six months of the CMA's receipt of the applicant's decision to opt for the event. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the aptitude test, the board issues a certificate of professional qualification to the successful applicant within one month.

If the applicant chooses to perform a**adaptation course**, the CMA sends him a list of all the organizations likely to organize this internship, within one month of receiving the applicant's decision. Failing that, the recognition of the professional qualification is deemed acquired and the board establishes a certificate of professional qualification.

At the end of the adjustment course, the applicant sends the board a certificate certifying that he has completed this internship, accompanied by an evaluation of the organization that organized it. On the basis of this certification, the board issues a certificate of professional qualification to the person concerned within one month.

**Please note**

Any legal action against the CMA's decision to seek compensation is preceded, on pain of inadmissibility, by an administrative appeal exercised, within two months of notification of that decision, with the prefect of the department where the chamber is headquartered.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-146 of 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the Community other state party to the EEA agreement.

### d. Make a declaration in case of preparation or sale of animal or animal products

Any operator of an establishment producing, handling or storing animal products or foodstuffs containing animal ingredients (meat, dairy products, fish products, eggs, honey), intended for consumption must meet the obligation to declare each of the establishments for which it is responsible, as well as the activities that take place there.

The declaration must be made before the opening of the establishment and renewed in the event of a change of operator, address or nature of the activity.

#### Competent authority

The declaration must be addressed to the prefect of the establishment's implementation department or, for establishments under the authority or guardianship of the Minister of Defence, to the health service of the armed forces.

#### Supporting documents 

Applicant must complete The Cerfa 13984 form*03.

*For further information*: Articles R. 231-4 and R. 233-4 of the Rural code and marine fisheries; Article 2 of the 28 June 1994 decree on the identification and health accreditation of establishments on the market for animal or animal products and safety markings.

### e. If necessary, seek health approval in the event of the sale of food to intermediaries

Establishments preparing, processing, handling or storing animal products for human consumption (raw milk, meat, eggs, fishing products, honey) must obtain a health approval to market their products products from other establishments.

#### Competent authority

The application for accreditation must be addressed to the Departmental Directorate of Social Cohesion and Population Protection (DDCSPP) at the establishment's location.

#### Response time

If the application for accreditation is complete and admissible, a conditional approval is granted for a period of three months to the applicant.

During this period, an official check takes place. If the conclusions of this visit are favourable, final approval is granted. Otherwise, the points of non-compliance are notified to the applicant and the conditional approval may be renewed for a further three months. The total duration of conditional approval may not exceed six months.

#### Supporting documents

The application for health accreditation is made using the Form Cerfa 13983Completed, dated and signed and accompanied by all the supporting documents listed by the[memo DGAL/SDSSA/N2012-8119](http://agriculture.gouv.fr/ministere/note-de-service-dgalsdssan2012-8119-du-12062012) June 12, 2012 relating to the accreditation process and the composition of the file.

These documents relate to the presentation of the company, the description of its activities and the health management plan.

**Good to know**

It is advisable to check with the DCSPP to ensure the need to apply for accreditation and, if necessary, to obtain assistance in establishing the application file.

*For further information*: Articles L. 233-2 and R. 233-1 of the Rural code and marine fisheries; 8 June 2006 order on the health accreditation of establishments that market animal products or foodstuffs containing animal products; DGAL/SDSSA/N2012-8119 of June 12, 2012 relating to the accreditation procedure and the composition of the file.

### f. If necessary, obtain a waiver from the requirement for health accreditation

The establishments affected by the exemption are the shops:

- ceding limited quantities of food to other retail outlets;
- delivery distances to other retail outlets do not exceed 80 kilometres.

The product categories covered by the exemption include:

- unprocessed (chilled or frozen, prepared or whole) or processed (salted, smoked, cooked, etc.);
- dairy products, heat-treated milks, shell egg products and/or raw milk that have undergone a sanitizing treatment;
- pre-prepared meals or cooking preparations that are the main course of a meal.

The fishmonger can obtain this exemption when the distance with the establishments delivered does not exceed 80 km (or more by prefectural decision) and if he gives up to the maximum:

- 250 kg per week when it sells 30% or less of its total production;
- 100 kg if it sells more than 30% of its total production.

**Please note**

The delivery limit and the quantitative limits under the exemption from the health accreditation requirement do not apply in the case of the free transfer of food to charitable establishments.

**Good to know**

A new declaration must be sent to the SDCSPP when any significant changes to the products or quantities delivered, except in the context of food donation, or delivered establishments.

*For further information*: :[DGAL/SDSSA/2014-823 technical instruction](https://info.agriculture.gouv.fr/gedei/site/bo-agri/instruction-2014-823) October 10, 2014.

#### Competent authority

The request for a waiver must be addressed to the DDCSPP of the department in which the institution is located.

#### Supporting documents

The application for waiver of the grant is made using form Cerfa 13982Completed, dated and signed.

#### Cost

Free

*For further information*: Article L. 233-2 of the Rural Code and Marine Fisheries; Articles 12, 13, Annexes 3 and 4 of the 8 June 2006 Order on the Health Accreditation of Establishments offering animal products or foodstuffs containing animal products;[DGAL/SDSSA/2014-823 technical instruction](https://info.agriculture.gouv.fr/gedei/site/bo-agri/instruction-2014-823) October 10, 2014.

