﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS004" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentation" -->
<!-- var(title)="Boucher" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentation" -->
<!-- var(title-short)="boucher" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/alimentation/boucher.html" -->
<!-- var(last-update)="2020-12" -->
<!-- var(url-name)="boucher" -->
<!-- var(translation)="None" -->

# Boucher

Dernière mise à jour : <!-- begin-var(last-update) -->2020-12<!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le boucher est un professionnel de la vente de la viande. Il peut être amené à la transformer et à préparer des produits à base de viande, qu’il a préalablement achetés chez des grossistes ou dans des abattoirs. Il stocke les pièces, les travaille et les vend à ses clients.

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de l’activité exercée, de la forme juridique de l’entreprise et du nombre de salariés employés :

- pour les artisans et pour les sociétés commerciales exerçant une activité artisanale, à condition qu’ils emploient moins de onze salariés, le CFE compétent est la chambre de métiers et de l’artisanat (CMA) ;
- pour les artisans et pour les sociétés commerciales qui emploient au moins onze salariés, il s’agit de la chambre du commerce et de l’industrie (CCI) ;
- dans les départements du Haut-Rhin et du Bas-Rhin, la chambre des métiers d’Alsace est compétente.

**À noter**

Dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle, l’activité reste artisanale, quel que soit le nombre de salariés employés, dès lors que l’entreprise n’utilise pas de procédé industriel. Le CFE compétent est celui de la chambre des métiers d’Alsace ou de la CMA de Moselle. Pour plus d’informations, il est conseillé de se reporter au site officiel de la chambre des métiers d’Alsace.

Enfin, si l’artisan a également une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Seule une personne qualifiée professionnellement ou placée sous le contrôle effectif et permanent d’une personne qualifiée peut exercer l’activité de boucher.

Les personnes qui exercent l’activité de boucher ou qui en contrôlent l’exercice doivent :

- soit être titulaires d’un diplôme français de niveau égal ou supérieur à un certificat d’aptitude professionnelle (CAP), homologué ou enregistré lors de sa délivrance au Répertoire national des certifications professionnelles (RNCP). Le site de la Commission nationale de la certification professionnelle (CNCP) dresse une liste de l’ensemble des qualifications professionnelles relatives à cette activité, et notamment le CAP « boucher », le bac pro « boucher charcutier traiteur » ou le brevet professionnel « boucher » ;
- soit justifier d’une expérience professionnelle de trois années effectives, dans un État de l’Union européenne (UE) ou partie à l’accord sur l’Espace économique européen (EEE), acquise en qualité de dirigeant d’entreprise, de travailleur indépendant ou de salarié dans l’exercice du métier de boucher. Dans ce cas, l’intéressé peut effectuer une demande d’attestation de qualification professionnelle auprès de la CMA compétente.

*Pour aller plus loin :* article 16 de la loi n° 96-603 du 5 juillet 1996 relative au développement et à la promotion du commerce et de l’artisanat ; article 1er du décret n° 98-246 du 2 avril 1998 relatif à la qualification professionnelle exigée pour l’exercice des activités prévues à l’article 16 de la loi n° 96-603 du 5 juillet 1996 précitée.

### b. Qualifications professionnelles – Ressortissants européens (LPS ou LE)

#### Pour une Libre Prestation de Services (LPS)

Le professionnel ressortissant d’un État membre de l’Union européenne (UE) ou d’un autre État partie à l’accord sur l’Espace économique européen (EEE) peut exercer en France, à titre temporaire et occasionnel, l’activité de boucher sous réserve d’être légalement établi dans un de ces États pour y exercer la même activité.

Toutefois, lorsque l’activité de boucher ou la formation y conduisant ne sont pas réglementées dans l'État d'établissement, il doit en outre avoir exercée l’activité dans un ou plusieurs États membres de l'UE ou États parties à l'accord sur l'EEE pendant au moins une année à temps plein, ou pendant une durée équivalente à temps partiel au cours des dix années qui précèdent la prestation qu'il entend réaliser en France.

*Pour aller plus loin :* article 17-1 de la loi n° 96-603 du 5 juillet 1996 précitée.

#### Pour un Libre Établissement (LE)

Pour exercer en France à titre permanent l’activité de boucher, le professionnel ressortissant d’un État membre de l’UE ou d’un autre État partie à l’accord sur l’EEE doit remplir l’une des conditions suivantes :

- disposer des mêmes qualifications professionnelles que celles exigées pour un Français (cf. supra « 2°. a. Qualifications professionnelles » : être titulaire d’un diplôme français de niveau égal ou supérieur à un CAP ou justifier d’une expérience professionnelle de 3 ans) ;
- être titulaire d’une attestation de compétence ou d’un titre de formation requis pour l’exercice de l’activité de boucher dans un État membre de l’UE ou partie à l’accord sur l’EEE lorsque cet État réglemente l’accès ou l’exercice de cette activité sur son territoire ;
- être titulaire d’une attestation de compétence ou d’un titre de formation et justifier d’une expérience professionnelle du métier de boucher (exercice à temps plein pendant une année ou à temps partiel pendant une durée équivalente au cours des dix années précédentes) lorsque ce métier n’est pas réglementé dans l’État, membre ou partie, d’origine ;
- être titulaire d'un titre de formation obtenu dans un État, membre ou partie, lorsque la formation est réglementée dans cet État.

**À noter**

Le ressortissant d’un État de l’UE ou d’un autre État partie à l’accord sur l’EEE qui remplit l’une des conditions précitées peut obtenir la reconnaissance de ses qualifications professionnelles et la délivrance d’une attestation de qualification professionnelle (cf. infra « 3°. d. Demander une attestation de reconnaissance de qualification professionnelle par le ressortissant d’un État membre de l’UE ou d’un État partie à l’accord sur l’EEE en cas d’exercice permanent (LE) ».)

Si l’intéressé ne remplit pas une des trois dernières conditions précitées, la CMA saisie peut lui demander d’accomplir une mesure de compensation lorsque la formation reçue porte sur des matières dont la connaissance, les aptitudes et les compétences acquises sont essentielles pour exercer ou contrôler de manière effective et permanente le métier et pour lesquelles la formation reçue par le demandeur présente des différences substantielles en termes de contenu avec l'un des diplômes ou titres requis pour l’exercice du métier de boucher. 

La mesure de compensation consiste, au choix du demandeur, en un stage d'adaptation ou en une épreuve d'aptitude. À l’issue de cette mesure, la CMA délivre une attestation de qualification professionnelle au demandeur l’ayant réussi.

*Pour aller plus loin :* article 17 de la loi n° 96-603 du 5 juillet 1996 précitée ; articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 précité.

#### Bon à savoir : mesures de compensation

La CMA, saisie d’une demande de délivrance d’une attestation de reconnaissance de qualification professionnelle, notifie au demandeur sa décision tendant à lui faire accomplir une des mesures de compensation. Cette décision énumère les matières non couvertes par la qualification attestée par le demandeur et dont la connaissance est impérative pour exercer en France.

Le demandeur doit alors choisir entre un stage d’adaptation d’une durée maximale de trois ans ou une épreuve d’aptitude.

Si le demandeur opte pour l’épreuve d’aptitude, celle-ci prend la forme d’un examen devant un jury. Elle est organisée dans un délai de six mois à compter de la réception par la CMA de la décision du demandeur d’opter pour cette épreuve. À défaut, la qualification est réputée acquise et la CMA établit une attestation de qualification professionnelle.

Si le demandeur opte pour le stage d’adaptation, à la fin de celui-ci, le demandeur adresse à la CMA une attestation certifiant qu’il a valablement accompli ce stage, accompagnée d’une évaluation de l’organisme qui l’a encadré. La CMA délivre, sur la base de cette attestation et du résultat de l’évaluation, une attestation de qualification professionnelle dans un délai d’un mois.

La décision de recourir à une mesure de compensation peut être contestée par l’intéressé qui doit former un recours administratif auprès du préfet dans un délai de deux mois à compter de la notification de la décision. En cas de rejet de son recours, il peut alors initier un recours contentieux.

*Pour aller plus loin :* articles 3 et 3-2 du décret n° 98-246 du 2 avril 1998 précité.

### c. Conditions d’honorabilité

Nul ne peut exercer la profession s’il fait l’objet :

- d’une interdiction de diriger, gérer, administrer ou contrôler directement ou indirectement une entreprise commerciale ou artisanale en application de l’article L. 653-8 du Code de commerce ;
- d’une peine alternative d’interdiction d’exercer une activité professionnelle ou sociale en application du 11° de l’article 131-6 du Code pénal.

*Pour aller plus loin :* article 19 III de la loi n° 96-603 du 5 juillet 1996 précitée.

### d. Quelques particularités de la réglementation de l’activité

#### Réglementation concernant la qualité d’artisan et les titres de maître artisan ou de meilleur ouvrier de France

##### La qualité d’artisan

Pour pouvoir se prévaloir de la qualité d’artisan, il faut :

- être une personne physique, entrepreneur individuel ou dirigeant social d’une personne morale ;
- être immatriculé au répertoire des métiers ;
- exercer une activité artisanale ;
- être qualifié professionnellement, c’est-à-dire soit être titulaire d’un diplôme enregistré au RNCP de niveau au moins équivalent à un CAP, soit disposer de trois ans d’expérience professionnelle dans le métier exercé. 

*Pour aller plus loin :* article 21 de la loi du 5 juillet 1996 précité ; article 1er du décret n° 98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

##### Le titre de maître artisan

Pour obtenir le titre de maître artisan, l’intéressé (personne physique ou dirigeant d’une société artisanale) doit :

- être immatriculé au répertoire des métiers (RM) ;
- être titulaire du brevet de maîtrise ;
- justifier d’au moins deux ans de pratique professionnelle.

La demande doit être adressée au président de la CMA compétente.

**À savoir**

Les personnes qui ne sont pas titulaires du brevet de maîtrise peuvent solliciter l’obtention du titre de maître artisan dans deux situations distinctes :

- si elles sont immatriculées au répertoire des métiers, qu’elles sont titulaires d’un diplôme de niveau de formation au moins équivalent au brevet de maîtrise, qu’elles justifient de connaissances en gestion et psychopédagogie équivalentes à celles des  unités de valeur du brevet de maîtrise et qu’elles justifient de plus de deux ans de pratique professionnelle ;
- si elles sont immatriculées au répertoire des métiers depuis au moins dix ans et qu’elles justifient d’un savoir-faire reconnu au titre de la promotion de l’artisanat ou de la participation à des actions de formation.

Dans ces deux cas, le titre de maître artisan peut être accordé par la commission régionale des qualifications (CRQ).

Pour plus de précisions, il est conseillé de se rapprocher de la CMA territorialement compétente.

*Pour aller plus loin :* article 3 du décret n° 98-247 du 2 avril 1998 précité.

##### Le titre de meilleur ouvrier de France

Le titre de meilleur ouvrier de France (MOF) est réservé aux personnes titulaires du diplôme professionnel dénommé « un des meilleurs ouvriers de France ». Ce diplôme national atteste l’acquisition d’une haute qualification dans l’exercice d’une activité professionnelle  notamment artisanale.

*Pour aller plus loin :* site officiel du concours « un des meilleurs ouvriers de France » ; articles D. 338-9 et suivants du Code de l’éducation.

#### Respect des normes sanitaires

En application de l’arrêté du 21 décembre 2009, le boucher est tenu de se conformer aux règles sanitaires applicables aux activités de commerce de détail, d’entreposage et de transport de produit d’origine animale et denrées alimentaires en contenant.

Ces normes concernent notamment les températures de conservation des denrées alimentaires, les méthodes de décongélation, les obligations en matière de viande hachée et de gibier sauvage, les obligations documentaires visant à assurer la traçabilité des produits, etc.

*Pour aller plus loin :* arrêté du 21 décembre 2009 relatif aux règles sanitaires applicables aux activités de commerce de détail, d’entreposage et de transport de produits d’origine animale et denrées alimentaires en contenant.

#### Réglementation relative aux produits

L’origine de la viande (bovine, porcine, ovine, volaille, etc.) doit être indiquée au consommateur qui doit ainsi pouvoir identifier le lieu de provenance ou le pays d’origine de la viande.

De plus, les viandes doivent obligatoirement provenir d’établissements agréés (ou titulaire d’une dérogation à cette obligation).

Enfin, les bouchers doivent se conformer aux règles d’étiquetage des denrées alimentaires.

**Bon à savoir**

Les consommateurs doivent être informés de toute présence de substances ou produits provoquant des allergies ou intolérances dans la fabrication ou la préparation d’une denrée alimentaire.

*Pour aller plus loin :* réglementation européenne « Paquet Hygiène », Règlement (CE) n° 1760/2000 du Parlement européen et du Conseil du 17 juillet 2000 établissant un système d’identification des bovins et d’étiquetage de la viande bovine ; décret n° 2015-447 du 17 avril 2015 relatif à l’information des consommateurs sur les allergènes et les denrées alimentaires non préemballées ; articles R. 412-12 et suivants du Code de la consommation.

#### Réglementation concernant l’établissement

Si les locaux sont ouverts au public, le professionnel doit se conformer aux règles relatives aux établissements recevant du public (ERP) :

- en matière d’incendie ;
- en matière d’accessibilité.

Pour plus d’informations, il est conseillé de se reporter à la fiche « [Établissement recevant du public](https://www.guichet-entreprises.fr/fr/ds/autres-services/etablissement-recevant-du-public.html) » (ERP).

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l’entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au répertoire des métiers (RM) ou au registre du commerce et des sociétés (RCS).

### b. Faculté de suivre un stage de préparation à l’installation (SPI)

Les créateurs d’entreprises artisanales qui sollicitent leur immatriculation au RM ont la possibilité de suivre un stage de préparation à l’installation (SPI) qui, en application des dispositions de la loi n° 2019-486 du 22 mai 2019 relative à la croissance et la transformation des entreprises, est facultatif. 

Pour garantir l’accès à ce stage à tous les créateurs d’entreprises artisanales qui le souhaitent, les CMA sont tenues de le proposer. Ce stage, d’une durée de l’ordre de 30 heures, est ouvert à toutes personnes ayant préalablement effectué l’immatriculation de leur entreprise au RM. À l’issue du stage, chaque participant reçoit une attestation.

Le stage est payant pour un montant à titre indicatif de l’ordre de 194 euros en 2020. 

### c. Faculté pour un ressortissant d’un État membre de l’UE ou d’un autre État partie à l’accord sur l’EEE de demander la délivrance d’une attestation de qualification professionnelle en vue d’un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

La CMA du lieu dans lequel le ressortissant souhaite exercer son activité est compétente pour délivrer une attestation de qualification professionnelle.

#### Procédure

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les quinze jours du dépôt du dossier. Un récépissé est délivré dès que ce dernier est complet.

#### Pièces justificatives

La demande d’attestation de qualification professionnelle est accompagnée des pièces justificatives suivantes :

- une copie d’une pièce d’identité en cours de validité ;
- une attestation certifiant que le demandeur est légalement établi dans un État membre de l’UE ou partie à l’accord sur l’EEE ;
- tous documents attestant de l’expérience professionnelle du demandeur. 

**À savoir**

Les documents non établis en français doivent être accompagnés d'une traduction certifiée conforme à l'original par un traducteur assermenté ou habilité à intervenir auprès des autorités judiciaires ou administratives d'un État membre de l'UE ou partie à l'accord sur l'EEE.

### d. Demande de reconnaissance de qualification professionnelle par le ressortissant d’un État membre de l’UE ou d’un État partie à l’accord sur l’EEE en cas d’exercice permanent (LE)

Pour s’établir en France, un boucher qui ne dispose pas d’un diplôme français ou de l’expérience professionnelle requise doit préalablement demander la reconnaissance de ses qualifications professionnelles. 

#### Autorité compétente

La demande doit être adressée à la CMA dans le ressort de laquelle le boucher souhaite exercer son activité.

#### Procédure

Un récépissé de remise de demande est adressé au demandeur dans un délai d’un mois suivant sa réception par la CMA. Si le dossier est incomplet, la CMA demande à l’intéressé de le compléter dans les 15 jours du dépôt du dossier. Un récépissé est délivré dès que le dossier est complet.

#### Pièces justificatives

La demande de reconnaissance de qualification professionnelle est accompagnée des pièces justificatives suivantes :

- une copie d’une pièce d’identité du demandeur en cours de validité ;
- un justificatif de la qualification professionnelle du demandeur :
  - diplôme, titre ou certificat ;
  - ou attestation de compétence ;
  - ou attestation portant sur la nature et la durée de l'activité délivrée par l'autorité ou l'organisme compétent de l'État membre ou partie à l'accord sur l'EEE dans lequel l'expérience a été effectuée ;
  - ou tous documents attestant de l’expérience professionnelle. 

La CMA peut demander la communication d’informations complémentaires concernant sa formation ou son expérience professionnelle pour déterminer l’existence éventuelle de différences substantielles avec la qualification professionnelle exigée en France. De plus, si la CMA doit se rapprocher du Centre international d’études pédagogiques (Ciep) pour obtenir des informations complémentaires sur le niveau de formation d’un diplôme ou d’un certificat ou d’un titre étranger, le demandeur devra s’acquitter de frais supplémentaires.

**À savoir**

Les documents non établis en français doivent être accompagnés d'une traduction certifiée conforme à l'original par un traducteur assermenté ou habilité à intervenir auprès des autorités judiciaires ou administratives d'un État membre de l'UE ou partie à l'accord sur l'EEE.

#### Délai de réponse

Dans un délai de trois mois suivant la délivrance du récépissé, la CMA :

- reconnaît la qualification professionnelle du demandeur et lui délivre une attestation de qualification professionnelle ;
- décide de soumettre le demandeur à une mesure de compensation et lui notifie cette décision ;
- décide de ne pas reconnaître la qualification professionnelle du demandeur.

En l’absence de notification de la décision de la CMA dans un délai de trois mois à compter de la réception de la demande complète, la reconnaissance de qualification professionnelle est réputée acquise au demandeur.

#### Voies de recours

Si la CMA refuse de reconnaître la qualification professionnelle, le demandeur peut initier, dans les deux mois suivant la notification du refus, un recours contentieux devant le tribunal administratif compétent. 

Si l’intéressé veut contester la décision de la CMA de le soumettre à une mesure de compensation, il doit d’abord initier un recours administratif auprès du préfet du département dans lequel la CMA a son siège, dans les deux mois suivant la notification de la décision de la CMA. S’il n’obtient pas gain de cause, il pourra opter pour un recours contentieux devant le tribunal administratif compétent.

*Pour aller plus loin :* articles 3 à 3-2 du décret n° 98-246 du 2 avril 1998 précité ; arrêté du 28 octobre 2009 pris en application des décrets n° 97-558 du 29 mai 1997 et n° 98-246 du 2 avril 1998 et relatif à la procédure de reconnaissance des qualifications professionnelles d’un professionnel ressortissant d’un État membre de l'Union européenne ou d’un autre État partie à l’accord sur l’Espace économique européen.

### e. Autres autorisations

#### Effectuer une déclaration de manipulation de denrées alimentaires d’origine animale

Tout établissement qui produit ou commercialise des denrées alimentaires contenant des produits d’origine animale doit effectuer une déclaration, avant ouverture et à chaque changement d’exploitant, d’adresse ou d’activité.

##### Autorité compétente

La déclaration doit être adressée à la direction départementale de la cohésion sociale et de la protection des populations (DDCSPP) du lieu d’implantation de l’établissement.

##### Pièces justificatives

La déclaration est réalisée sous la forme du formulaire Cerfa 13984\*05 ([démarche en ligne](https://agriculture-portail.6tzen.fr/default/requests/Cerfa13984/)) ou Cerfa 13984\*06 (démarche par voie postale) complété, daté et signé.

##### Coût

Gratuit.

*Pour aller plus loin :* article R. 233-4 du Code rural et de la pêche maritime et arrêté du 28 juin 1994 relatif à l’identification et à l’agrément sanitaire des établissements mettant sur le marché des denrées animales ou d’origine animale et au marquage de salubrité.

#### Le cas échéant, demander une autorisation de détention et de désossage de carcasses et parties de carcasses bovines contenant de l’os vertébral

Seuls les ateliers de boucherie ayant obtenu une autorisation peuvent détenir et désosser des carcasses et parties de carcasses issues de bovins de plus de 30 mois et contenant de l’os vertébral.

##### Autorité compétente

La demande d’autorisation doit être adressée à la direction départementale de la cohésion sociale et de la protection des populations (DDCSPP) du lieu d’établissement de l’atelier de boucherie.

##### Délai de réponse

La DDCSPP adresse sa réponse dans le délai de deux mois suivant la réception de la demande. L’absence de réponse dans ce délai équivaut à un refus d’autorisation.

##### Pièces justificatives

L’intéressé doit adresser une demande d’autorisation dont le modèle figure à l’appendice A de l’annexe V de l’arrêté du 21 décembre 2009.

##### Coût

Gratuit.

*Pour aller plus loin :* annexe V de l’arrêté du 21 décembre 2009 relatif aux règles sanitaires applicables aux activités de commerce de détail, d’entreposage et de transport de produits d’origine animale et denrées alimentaires en contenant.

#### Le cas échéant, demander un agrément sanitaire en cas de vente de denrées à des intermédiaires

Les établissements préparant, transformant, manipulant ou entreposant des produits d’origine animale (lait cru, viandes, œufs, etc.) doivent obtenir un agrément sanitaire pour commercialiser leurs produits auprès d’autres établissements.

##### Autorité compétente

La demande d’agrément doit être adressée à la DDCSPP du lieu d’implantation de l’établissement.

##### Délai de réponse

Si le dossier de demande d’agrément est complet et recevable, un agrément conditionnel est accordé pour une période de trois mois au demandeur. Au cours de cette période, un contrôle officiel a lieu. Si les conclusions de cette visite sont favorables, un agrément définitif est accordé. Dans le cas contraire, les points de non-conformité sont notifiés au demandeur et l’agrément conditionnel peut être renouvelé pour une nouvelle période de trois mois. La durée totale de l’agrément conditionnel ne peut excéder six mois.

##### Pièces justificatives

La demande d’agrément sanitaire est effectuée au moyen du formulaire Cerfa 13983*03 complété, daté et signé et de l’ensemble des pièces justificatives listées dans l’instruction DGAL/SDSSA/2019-728 du 22-10-2019.

*Pour aller plus loin :* arrêté du 8 juin 2006 relatif à l’agrément sanitaire des établissements mettant sur le marché des produits d’origine animale ou des denrées contenant des produits d’origine animale ; Instruction DGAL/SDSSA/2019-728 du 22-10-2019 précitée ; articles L. 233-2 et R. 233-1 du Code rural et de la pêche maritime.

**Bon à savoir**

Il est conseillé de se renseigner auprès de la DDCSPP pour s’assurer de la nécessité de solliciter une demande d’agrément et, le cas échéant, obtenir de l’aide pour établir le dossier de demande.

#### Le cas échéant, solliciter une dérogation à l’obligation d’agrément sanitaire

Certains commerces de détail peuvent bénéficier d’une dérogation à l’obligation d’agrément sanitaire. Il s’agit par exemple des commerces cédant des quantités limitées de denrées à d’autres commerces de détail, des commerces dont les distances de livraison vers d’autres commerces de détail n’excèdent pas 80 kilomètres, etc.

Tout exploitant d’un commerce de détail, qui fournit des denrées d’origine animale qu’il a produites à destination d’autres établissements de vente au détail, peut solliciter une dérogation à l’obligation d’agrément, si cette activité est exercée de manière marginale, localisée et restreinte. Pour plus de précisions quant aux quantités, pouvant être cédées dans le cadre de la dérogation, il est conseillé de se reporter aux annexes 3 et 4 de l’arrêté du 8 juin 2006, relatif à l’agrément sanitaire des établissements mettant sur le marché des produits d’origine animale ou des denrées contenant des produits d’origine animale.

##### Autorité compétente

La demande de dérogation doit être adressée à la DDCSPP du département dans lequel est situé l’établissement.

##### Pièces justificatives

La demande de dérogation à l’obligation d’agrément est effectuée au moyen du formulaire Cerfa 13982*06 complété, daté et signé.

##### Coût

Gratuit

*Pour aller plus loin :* article L. 233-2 du Code rural et de la pêche maritime ; articles 12 et 13 de l’arrêté du 8 juin 2006 précité ; Instruction DGAL/SDSSA/2019-728 du 22-10-2019 précitée.