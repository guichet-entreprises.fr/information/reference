﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS004" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Food industry" -->
<!-- var(title)="Butcher" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="food-industry" -->
<!-- var(title-short)="butcher" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/food-industry/butcher.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="butcher" -->
<!-- var(translation)="Auto" -->


Butcher
=======

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The butcher is a professional in the sale of meat. They may be required to process and prepare meat products, which they have previously purchased from wholesalers or slaughterhouses. He stores the parts, works them and sells them to his customers.

*For further information*: Act 96-603 of 5 July 1996 on the development and promotion of trade and crafts.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For a craft activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for a commercial activity, the relevant CFE is the Chamber of Commerce and Industry (CCI).

If the professional has a buying and resale activity, his activity is both artisanal and commercial.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

Only a qualified person who is professionally qualified or placed under the effective and permanent control of a qualified person can carry out the activity of butcher.

Individuals who carry out or control the activity of butcher must be incumbents, of their choice:

- A Certificate of Professional Qualification (CAP);
- A Professional Studies Patent (BEP);
- a diploma or a degree of equal or higher level approved or registered at the time of its issuance to the [national directory of professional certifications](http://www.rncp.cncp.gouv.fr/).

The [national professional certification commission website](http://www.cncp.gouv.fr/) (CNCP) offers a list of all of these professional qualifications. These include the CAP "butcher", the bac pro "butcher butcher caterer" or the professional "butcher" patent.

In the absence of one of these diplomas, the person must justify an effective three-year professional experience in a European Union (EU) state or party to the European Economic Area (EEA) agreement, acquired as a leader self-employed or salaried in the labourer's trade. In this case, the person concerned can apply to the relevant CMA for a certificate of professional qualification.

*For further information*: Article 16 of Law 96-603 of 5 July 1996 on the development and promotion of trade and crafts; Article 1 of Decree 98-246 of 2 April 1998; Decree 98-246 of 2 April 1998 relating to the professional qualification required for the activities of Article 16 of Act 96-603 of 5 July 1996.

### b. Professional Qualifications - European Nationals (LPS or LE)

#### For Freedom to provide services

The professional who is a member of the EU or the EEA may work in France, on a temporary and casual basis, as a butcher on the condition that he is legally established in one of these states to carry out the same activity.

If neither the activity nor the training leading to it is regulated in the State of The Establishment, the person must also prove that he or she has been a butcher in that state for at least the equivalent of two years full-time in the last ten years. years before the performance he wants to perform in France.

*For further information*: Article 17-1 of Act 96-603 of July 5, 1996.

#### For a Freedom of establishment

In order to carry out a permanent butcher's activity in France, the professional who is an EU or EEA national must meet one of the following conditions:

- have the same professional qualifications as those required for a Frenchman (see above: "2. a. Professional qualifications");
- hold a certificate of competency or training certificate required for the exercise of butchering activity in an EU or EEA state when that state regulates access or exercise of this activity on its territory;
- have a certificate of competency or a training certificate that certifies its readiness to carry out the butcher's activity when this certificate or title has been obtained in an EU or EEA state which does not regulate access or exercise of this Activity
- have a diploma, title or certificate acquired in a third state and admitted in equivalency by an EU or EEA state on the additional condition that the person has been a butcher for three years in the state which has admitted equivalence .

**Please note**

A national of an EU or EEA state that meets one of the above conditions may apply for a certificate of recognition of professional qualification (see below: "3. b. If necessary, apply for a certificate of professional qualification.")

If the individual does not meet any of the above conditions, the CMA may ask him to perform a compensation measure in the following cases:

- if the duration of the certified training is at least one year less than that required to obtain one of the professional qualifications required in France to carry out the activity of butcher;
- If the training received covers subjects substantially different from those covered by one of the titles or diplomas required to carry out the activity of butcher in France;
- If the effective and permanent control of the butcher's activity requires, for the exercise of some of its remits, specific training which is not provided in the Member State of origin and covers substantially different subjects of those covered by the certificate of competency or training designation referred to by the applicant.

*For further information*: Articles 17 and 17-1 of Law 96-603 of 5 July 1996; Articles 3 to 3-2 of Decree 98-246 of 2 April 1998.

*Good to know: compensation measures

The CMA, which is applying for a certificate of recognition of professional qualification, notifies the applicant of his decision to have him perform one of the compensation measures. This decision lists the subjects not covered by the qualification attested by the applicant and whose knowledge is imperative to practice in France.

The applicant must then choose between an adjustment course of up to three years or an aptitude test.

The aptitude test takes the form of an examination before a jury. It is organised within six months of the CMA's receipt of the applicant's decision to opt for the event. Failing that, the qualification is deemed to have been acquired and the CMA establishes a certificate of professional qualification.

At the end of the adjustment course, the applicant sends the CMA a certificate certifying that he has validly completed this internship, accompanied by an evaluation of the organization that supervised him. The CMA issues, on the basis of this certificate, a certificate of professional qualification within one month.

The decision to use a compensation measure may be challenged by the person concerned who must file an administrative appeal with the prefect within two months of notification of the decision. If his appeal is dismissed, he can then initiate a legal challenge.

*For further information*: Articles 3 and 3-2 of Decree 98-246 of 2 April 1998; Article 6-1 of Decree 83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

### c. Conditions of honorability

No one may practise the profession if he is the subject of:

- a ban on directly or indirectly running, managing, administering or controlling a commercial or artisanal enterprise;
- a penalty of prohibition of professional or social activity for any of the crimes or misdemeanours provided for in Article 131-6 of the Penal Code.

*For further information*: Article 19 III of Act 96-603 of July 5, 1996.

### d. Some peculiarities of the regulation of the activity

#### Regulation concerning the quality of craftsman and the titles of master craftsman or best worker in France

**Craftsmanship**

Persons who have a specific professional qualification can automatically claim the status of craftsman (see above: "2. a. Professional qualification").

**What to know**

Persons who do not have the required professional qualifications can claim the status of craftsman if they justify three years of activity (excluding apprenticeship).

*For further information*: Article 1 of Decree 98-247 of 2 April 1998.

**The title of master craftsman**

To obtain the title of master craftsman, the person concerned (a natural person or a manager of a craft company) must:

- Be registered in the trades directory
- Hold a master's degree
- justify at least two years of professional practice.

The request must be addressed to the president of the relevant CMA.

**What to know**

Individuals who do not hold the master's degree can apply for the title of master craftsman in two distinct situations:

- if they are registered in the trades directory, have a training degree equivalent to the master's degree, and have management and psycho-pedagogical knowledge equivalent to the units of value of the master's degree and that they warrant more than two years of professional practice;
- if they have been registered in the trades repertoire for at least ten years and have a know-how recognized for promoting crafts or participating in training activities.

In both cases, the title of master craftsman may be granted by the Regional Qualifications Commission.

For more details, it is advisable to get closer to the CMA under consideration.

*For further information*: Decree 98-247 of 2 April 1998.

**The title of best worker in France**

The title of best worker in France (MOF) is reserved for those who have passed the exam called "one of the best workers in France". It is a state diploma that attests to the acquisition of a high qualification in the course of a professional activity.

*For further information*: [official website of the competition "one of the best workers in France"](http://www.meilleursouvriersdefrance.org/) ; Articles D. 338-9 and the following of the Education Code and arrested on December 27, 2012.

#### Compliance with health standards

Under the December 21, 2009 order, the butcher is required to comply with the sanitary rules applicable to the retail, storage and transport of animal products and food in containers.

These standards include food conservation temperatures, thawing methods, ground meat and wild game obligations, documentary obligations to ensure traceability of products, etc.

*For further information*: decree of 21 December 2009 relating to the sanitary rules applicable to the activities of retail, storage and transport of animal products and food contained in containers.

#### Product regulations

The origin of the meat (beef, swine, sheep, poultry, etc.) must be indicated to the consumer who must be able to identify the place of origin or country of origin of the meat.

In addition, meat must come from licensed establishments (or have a waiver of this obligation).

Finally, butchers must comply with food labelling rules.

**Good to know**

Consumers should be informed of any presence of substances or products causing allergies or intolerances in the manufacture or preparation of a food item.

*For further information*: European Regulation 'Hygiene Package', Regulation (EC) 1760/2000 of the European Parliament and the Council of 17 July 2000 establishing a system for identifying cattle and labelling beef; Decree No. 2015-447 of 17 April 2015 on consumer information on allergens and unpackaged foodstuffs; Article R. 412-12 and the following of the Consumer Code.

#### Settlement regulations

If the premises are open to the public, the professional must comply with the rules on public institutions (ERP):

- Fire
- accessibility.

For more information, it is advisable to refer to the listing[Establishment receiving the public](https://www.guichet-entreprises.fr/fr/activites-reglementees/autres-services/etablissement-recevant-du-public/) (ERP).

3°. Installation procedures and formalities
------------------------------------------------------

### a. Follow the installation preparation course (SPI)

The installation preparation course (SPI) is a mandatory prerequisite for anyone applying for registration in the trades directory.

**Terms of the internship**

- Registration is made upon presentation of a piece of identification with the territorially competent CMA;
- It has a minimum duration of 30 hours;
- It comes in the form of courses and practical work;
- it aims to acquire the essential knowledge in the legal, tax, social and accounting fields necessary to create a craft business.

**Exceptional postponement of the start of the internship**

In the event of a force majeure, the person concerned is allowed to carry out the SPI within one year of the registration of his company in the trades directory. This is not an exemption but simply a postponement of the internship, which remains mandatory.

**The result of the internship**

At the end of the internship, the participant receives a certificate of follow-up internship which he must attach to his business declaration file.

**Cost**

The internship pays off. As an indication, the training costs about 236 euros in 2016.

**Case of internship waiver**

The person concerned may be excused from completing the internship in two situations:

- if he has already received a level III-approved degree or diploma, including a degree in economics and business management, or a master's degree from a CMA;
- if he has been in a professional activity for at least three years requiring a level of knowledge equivalent to that provided by the internship.

**Internship exemption for EU or EEA nationals**

As a matter of principle, a qualified professional who is a national of the EU or the EEA is exempt from the SPI if he justifies with the CMA a qualification in business management giving him a level of knowledge equivalent to that provided by the internship.

The qualification in business management is recognized as equivalent to that provided by the internship for people who:

- have been engaged in a professional activity requiring a level of knowledge equivalent to that provided by the internship for at least three years;
- or who have knowledge acquired in an EU or EEA state or a third country during a professional experience that would cover, fully or partially, the substantial difference in content.

For those who do not meet these conditions, the Consular Chamber may require them to submit to a compensation measure if the examination of his professional qualifications shows substantial differences with those required in France for the management of a craft company.

**Terms of the internship waiver**

In order to be exempt from SPI, the person concerned (French or EU or EEA national) must apply to the CMA President concerned for an exemption from internship. He must accompany his mail with the following supporting documents: copy of the diploma approved at level III, copy of the master's certificate, proof of a professional activity requiring an equivalent level of knowledge. In addition, the person concerned must pay a variable fee (25 euros in 2016). Failure to respond within one month of receiving the application is worth accepting the application for an internship waiver.

*For further information*: Article 2 of Act 82-1091 of 23 December 1982; Article 6-1 of Decree 83-517 of 24 June 1983.

### b. If necessary, apply for a certificate of recognition of professional qualification

The person concerned wishing to have a diploma recognised other than that required in France or his professional experience may apply for a certificate of recognition of professional qualification.

**Competent authority**

The request must be addressed to the territorially competent CMA.

**Procedure**

An application receipt is sent to the applicant within one month of receiving it from the CMA. If the file is incomplete, the CMA asks the applicant to complete it within 15 days of filing the file. A receipt is issued as soon as the file is complete.

**Supporting documents**

The folder should contain:

- Applying for a certificate of professional qualification
- Proof of professional qualification: a certificate of competency or a diploma or vocational training certificate;
- Proof of the applicant's nationality
- If work experience has been acquired on the territory of an EU or EEA state, a certificate on the nature and duration of the activity issued by the competent authority in the Member State of origin;
- if the professional experience has been acquired in France, the proofs of the exercise of the activity for three years.

The CMA may request further information about its training or professional experience to determine the possible existence of substantial differences with the professional qualification required in France. In addition, if the CMA is to approach the International Centre for Educational Studies (Ciep) to obtain additional information on the level of training of a diploma or certificate or a foreign designation, the applicant will have to pay a fee Additional.

**What to know**

If necessary, all supporting documents must be translated into French .

**Response time**

Within three months of the receipt, the CMA:

- recognises professional qualification and issues certification of professional qualification;
- decides to subject the applicant to a compensation measure and notifies him of that decision;
- refuses to issue the certificate of professional qualification.

In the absence of a decision within four months, the application for a certificate of professional qualification is deemed to have been acquired.

**Remedies**

If the CMA refuses to issue the recognition of professional qualification, the applicant may initiate, within two months of notification of the refusal of the CMA, a legal challenge before the relevant administrative court. Similarly, if the person concerned wishes to challenge the CMA's decision to submit it to a compensation measure, he must first initiate a graceful appeal with the prefect of the department in which the CMA is based, within two months of notification of the decision. CMA. If he does not succeed, he may opt for a litigation before the relevant administrative tribunal.

*For further information*: Articles 3 to 3-2 of Decree 98-246 of 2 April 1998; decree of 28 October 2009 under Decrees 97-558 of 29 May 1997 and No. 98-246 of 2 April 1998 relating to the procedure for recognising the professional qualifications of a professional national of a Member State of the Community or another state party to the European Economic Area agreement.

### c. Company reporting formalities

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Formalities of Declaration of Craft Enterprise.

### d. Other permissions

#### Make a declaration of handling of animal foodstuffs

Any establishment that produces or markets foodstuffs containing animal products must make a declaration, before opening and at each change of operator, address or activity.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion and Population Protection (DDPP) at the establishment's location.

**Supporting documents**

The declaration is made in the form of form Cerfa 13984Completed, dated and signed.

**Cost**

Free.

*For further information*: Article R. 233-4 of the Rural and Marine Fisheries Code and a decree of 28 June 1994 relating to the identification and health accreditation of establishments on the market for animal or animal products and safety markings.

#### If necessary, apply for permission to hold and deboning carcasses and parts of bovine carcasses containing vertebral bone

Only licensed butcher shops may hold and bone carcasses and carcasses from cattle over 30 months of age containing vertebral bone.

**Competent authority**

The application for authorisation must be addressed to the Departmental Directorate of Social Cohesion and Population Protection (DDCSPP) at the location of the butcher's shop.

**Response time**

The DDCSPP sends its response within two months of receiving the request. Failure to respond within this time frame is tantamount to a refusal of authorization.

**Supporting documents**

The applicant must apply for leave, the model of which appears in Appendix A of Appendix V of the December 21, 2009 order.

**Cost**

Free.

*For further information*: Appendix V of the December 21, 2009 order on health rules for retail, storage and transportation of animal products and food in containers.

#### If necessary, seek health approval in the event of the sale of food to intermediaries

Establishments preparing, processing, handling or storing animal products (raw milk, meats, eggs, etc.) must obtain health approval to market their products to other establishments.

**Competent authority**

The application for accreditation must be sent to the SDCSPP at the establishment's location.

**Response time**

If the application for accreditation is complete and admissible, a conditional approval is granted for a period of three months to the applicant. During this period, an official check takes place. If the conclusions of this visit are favourable, final approval is granted. Otherwise, the points of non-compliance are notified to the applicant and the conditional approval may be renewed for a further three months. The total duration of conditional approval may not exceed six months.

**Supporting documents**

The application for health accreditation is made using the Form Cerfa 13983Completed, dated and signed and all supporting documents listed in the DGAL/SDSSA/N2012-8119 memo of June 12, 2012.

*For further information*: decree of 8 June 2006 relating to the health accreditation of establishments on the market of animal products or foodstuffs containing animal products; DGAL/SDSSA/N2012-8119 above; Articles L. 233-2 and R. 233-1 of the Rural Code and Marine Fisheries.

**Good to know**

It is advisable to check with the SDCSPP to ensure the need to apply for accreditation and, if necessary, to obtain assistance in establishing the application file.

#### If necessary, seek a waiver from the requirement for health accreditation

Some retail businesses may benefit from a waiver from the requirement for health accreditation. These include businesses that sell limited quantities of food to other retail businesses, businesses with delivery distances to other retail businesses of no more than 80 kilometres, etc.

Any operator of a retail business, who supplies animal products that he has produced for other retail establishments, may apply for a waiver from the licensing requirement, if this activity is carried out in a manner marginal, localized and restricted. For more details on the quantities, which may be transferred as part of the exemption, it is advisable to refer to Schedules 3 and 4 of the decree of 8 June 2006, relating to the health accreditation of establishments putting products of origin on the market or foodstuffs containing animal products.

**Competent authority**

The request for a waiver must be addressed to the DDCSPP of the department in which the institution is located.

**Supporting documents**

The application for waiver of the grant is made using form Cerfa 13982Completed, dated and signed.

**Cost**

Free

*For further information*: Article L. 233-2 of the Rural Code and Marine Fisheries; Articles 12 and 13 of the order of 8 June 2006 mentioned above; DGAL/SDSSA/2014-823 from 10 October 2014.

