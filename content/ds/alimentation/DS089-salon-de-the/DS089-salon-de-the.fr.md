﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS089" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Alimentation" -->
<!-- var(title)="Salon de thé" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="alimentation" -->
<!-- var(title-short)="salon-de-the" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/alimentation/salon-de-the.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="salon-de-the" -->
<!-- var(translation)="None" -->

# Salon de thé

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le salon de thé est un établissement ouvert au public dont l'activité consiste à proposer à sa clientèle des boissons sans alcool et potentiellement des denrées alimentaires à consommer sur place.

**À noter**

Si l'établissement propose des pâtisseries fabriquées sur place des dispositions spécifiques sont applicables. Le cas échéant, il est conseillé de se reporter à la fiche « [Pâtissier](https://www.guichet-entreprises.fr/fr/ds/alimentation/patissier.html) ». De même, pour en savoir plus sur la réglementation applicable à l'ouverture d'un restaurant il est conseillé de se reporter aux fiches « [Restauration traditionnelle](https://www.guichet-entreprises.fr/fr/ds/alimentation/restauration-traditionnelle.html) » ou « [Restauration rapide](https://www.guichet-entreprises.fr/fr/ds/alimentation/restauration-rapide-vente-a-emporter.html) ».

### b. Centre de formalités des entreprises (CFE) compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

* pour les activités artisanales, le CFE compétent est la chambre de métiers et de l'artisanat (CMA) ;
* pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel exploitant a une activité de restauration rapide associée à la fabrication de produits artisanaux, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Le professionnel qui souhaite ouvrir un salon de thé n'est soumis à aucune exigence de diplôme. Cependant, il doit être immatriculé au répertoire des métiers ou au registre des entreprises en cas d'installation dans les départements du Bas-Rhin, du Haut-Rhin ou de la Moselle.

#### Demande d'immatriculation au répertoire des métiers

##### Autorité compétente

Le professionnel doit adresser sa demande d'immatriculation à la CMA de son siège social dans un délai d'un mois avant le début de son activité. Toutefois, la demande peut être adressée au plus tard un mois après le début de l'activité dès lors que le professionnel à notifié au président de la CMA, la date de début de celle-ci, au plus tard la veille par lettre recommandée avec avis de réception.

##### Pièces justificatives

La liste des pièces justificatives à adresser à la CMA dépend de la nature de l'activité exercée.

##### Délai

Dès le dépôt de son dossier complet, la CMA adresse un récépissé de dépôt de dossier de création d'entreprise au demandeur. L'absence de réponse dans un délai de deux mois vaut immatriculation.

*Pour aller plus loin* : articles 9 et suivants du décret n°98-247 du 2 avril 1998 relatif à la qualification artisanale et au répertoire des métiers.

### b. Qualifications professionnelles - Ressortissants européens (Libre Prestation de Services ou Libre Établissement)

#### Pour une Libre Prestation de Services (LPS)

Aucune disposition n'est prévue pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) en vue d'exercer l'activité d'exploitant de salon de thé à titre temporaire et occasionnel en France.

A ce titre, le professionnel ressortissant UE est soumis aux mêmes exigences professionnelles que le ressortissant français (cf. supra « 2. a. Qualifications professionnelles »).

#### Pour un Libre Établissement (LE)

Le ressortissant d'un État membre de l'UE ou l'EEE exerçant l'activité d'exploitant de salon de thé peut, s'il est légalement établi dans cet État, exercer la même activité en France. Le ressortissant UE est dispensé de l'obligation d'effectuer un SPI (cf. supra « 2°. a. Stage de préparation à l'installation »).

Toutefois, la CMA peut décider de soumettre le ressortissant à une épreuve d'aptitude ou un stage d'adaptation dès lors qu'elle constate lors de l'examen de ses qualifications professionnelles, des différences substantielles entre sa formation et celle requise pour l'exercice de l'activité en France.

*Pour aller plus loin* : article 2 de la loi du 23 décembre 1982 précitée ; article 6-1 du décret du 24 juin 1983 susvisé.

### d. Quelques particularités de la réglementation de l’activité

#### Respect des normes de sécurité et d'accessibilité

Dès lors que les locaux dans lesquels le professionnel exerce son activité sont ouverts au public, il doit s'assurer du respect des normes de sécurité et d'accessibilité applicables à l'ensemble des Établissements recevant du public (ERP).

Ainsi, des mesures de prévention en cas d'incendie et d'accès aux locaux pour les personnes à mobilité réduite doivent notamment être prises.

*Pour aller plus loin* : arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public (ERP).

#### Respect des normes sanitaires

Le professionnel exploitant un salon de thé proposant des produits ou denrées alimentaires d'origine animale ou en contenant doit respecter des normes sanitaires notamment :

* les conditions d'approvisionnement des produits ;
* la température de conservation et de congélation de ces produits ;
* la température des produits.

*Pour aller plus loin* : arrêté du 21 décembre 2009 relatif aux règles sanitaires applicables aux activités de commerce de détail, d'entreposage et de transport de produits d'origine animale et de denrées alimentaires en contenant.

#### Formation en matière d'hygiène alimentaire

L'établissement de restauration rapide doit comprendre au sein de son personnel, au moins une personne ayant suivi une formation en matière d'hygiène alimentaire d'une durée de quatorze heures.

Toutefois, sont dispensés de suivre une telle formation, les professionnels :

* bénéficiant d'une expérience professionnelle d'au moins trois ans au sein d'une entreprise du secteur alimentaire en qualité de gestionnaire ou exploitant ;
* titulaires de l'un des diplômes figurant en annexe de l'arrêté du 25 novembre 2011 relatif à la liste des diplômes et titres à finalité professionnelle dont les détenteurs sont réputés satisfaire à l'obligation de formation spécifique en matière d'hygiène alimentaire adaptée à l'activité des établissements de restauration commerciale.

*Pour aller plus loin* : article D. 233-11 du code rural et de la pêche maritime ; arrêté du 5 octobre 2011 relatif au cahier des charges de la formation spécifique en matière d'hygiène alimentaire adaptée à l'activité des établissements de restauration commerciale.

#### Diffusion de musique

L'exploitant qui souhaite diffuser de la musique dans son salon de thé doit adresser en ligne une demande d'autorisation préalable à la Société des auteurs compositeurs et éditeurs de musique (Sacem) et s'acquitter du montant de la redevance. Le professionnel doit s'inscrire et remplir le formulaire d'autorisation en ligne sur le site de la [Sacem]((%3Chttps://clients.sacem.fr/autorisations%3E)).

#### Publicité des prix et mentions obligatoires

Le professionnel exploitant un établissement qui sert des boissons et des denrées à consommer sur place doit procéder à l'affichage de manière lisible des prix à payer par le consommateur sur l'ensemble de ses cartes et menus ainsi qu'à l'extérieur de l'établissement.

En outre, le professionnel exploitant un salon de thé doit rendre visible les informations relatives aux éléments allergènes contenus dans les produits.

*Pour aller plus loin* : arrêté du 27 mars 1987 relatif à l'affichage des prix dans les établissements servant des repas, denrées ou boissons à consommer sur place.

#### Réglementation en matière d'horaires d'ouverture

Le salon de thé en tant qu'établissement de restauration rapide est soumis aux dispositions relatives aux horaires d'ouverture et de fermeture des débits de boissons à consommer sur place, fixés par arrêté préfectoral.

Pour plus d'informations il est conseillé de se rapprocher de la préfecture du lieu d'établissement de l'activité.

#### Affichage des zones fumeur

Le professionnel exploitant un salon de thé doit se soumettre à l'obligation de signaler l'interdiction de fumer propre aux lieux affectés à un usage collectif.

*Pour aller plus loin* : articles R. 3512-2 et suivants du Code de la santé publique.

#### Titres-restaurant

Les exploitants de salon de thé qui exercent l'activité de restaurateur ou assimilé sont soumis aux dispositions légales relatives à l'utilisation des titres-restaurants.

*Pour aller plus loin* : décret n°2010-1460 du 30 novembre 2010 relatif aux conditions d'utilisation du titre-restaurant ; site officiel de la [Commission nationale des titres-restaurants (CNTR)](http://www.cntr.fr/V2/home.php).

## 3°. Démarches et formalités d’installation

### a. Immatriculation de l'entreprise

Suivant la nature de son activité, l’entrepreneur doit s’immatriculer au répertoire des métiers (RMA) ou au registre du commerce et des sociétés (RCS).

### b. Formalités de déclaration de l’entreprise

#### Autorité compétente

L'exploitant de salon de thé doit procéder à la déclaration de son entreprise, et pour cela, doit, effectuer une déclaration auprès de la chambre de commerce et de l'industrie.

#### Pièces justificatives

L'intéressé doit fournir les pièces justificatives requises selon la nature de son activité.

#### Délais

Le centre des formalités des entreprises de la chambre de commerce et de l'industrie adresse le jour même, un récépissé au professionnel mentionnant les pièces manquantes au dossier. Le cas échéant, le professionnel dispose d'un délai de quinze jours pour le compléter. Une fois le dossier complet, la chambre de commerce et de l'industrie indique au demandeur les organismes auxquels son dossier a été transmis.

#### Voies de recours

Le demandeur peut obtenir la restitution de son dossier dès lors qu'il n'a pas été transmis durant les délais ci-dessus.

Dès lors que le centre des formalités des entreprises refuse de recevoir le dossier, le demandeur dispose d'un recours devant le tribunal administratif.

#### Coût

Le coût de cette déclaration dépend de la forme juridique de la société.

*Pour aller plus loin* : article 635 du Code général des impôts (CGI).

### c. Le cas échéant, enregistrer les statuts de la société

L'exploitant de salon de thé doit, une fois les statuts de la société datés et signés, procéder à leur enregistrement auprès du Service des Impôts des Entreprises (SIE) si :

* l'acte comporte une opération particulière soumise à un enregistrement ;
* si la forme même de l'acte l'exige.

#### Autorité compétente

L'autorité compétente en matière d'enregistrement est :

* le service de la publicité foncière du lieu de situation de l'immeuble,lorsque les actes comportent un apport d'immeuble ou de droit immobilier ;
* le pôle enregistrement du SIE pour tous les autres cas.

#### Pièces justificatives

Le professionnel doit remettre deux exemplaires des statuts au SIE.

*Pour aller plus loin* : article 635 du Code général des impôts.