﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS089" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Food industry" -->
<!-- var(title)="Tea room" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="food-industry" -->
<!-- var(title-short)="tea-room" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/food-industry/tea-room.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="tea-room" -->
<!-- var(translation)="Auto" -->


Tea room
========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The tea room is open to the public whose activity is to offer its customers soft drinks and potentially food to be consumed on site.

**Please note**

If the establishment offers on-site pastries, specific provisions are applicable. If so, it is advisable to refer to the "Pastry" card. Similarly, to learn more about the regulations applicable to the opening of a restaurant it is advisable to refer to the "Traditional Restaurant" or "Fast Food" cards.

### b. Competent Business Formalities Centre (CFE)

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- For craft activities, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for commercial companies, it is the Chamber of Commerce and Industry.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional operator has a fast food activity associated with the manufacture of handicrafts, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The professional who wishes to open a tea room is not subject to any diploma requirement. However, it must:

- Take an installation preparation course (SPI)
- be registered in the trades register or the register of companies in case of installation in the departments of lower Rhine, Upper Rhine or Moselle.

**Installation Preparation Course (SPI)**

Before registration, the professional must undergo an SPI consisting of two parts:

- the first part aims to train the professional in the management of his business by introducing him in particular to the general accounting and the economic, legal and social environment of a craft company;
- the second part of the internship is to accompany the professional after his registration in the trades register.

However, the professional may be excused from performing an SPI as soon as he or she:

- is prevented by a case of force majeure. If necessary, he must complete this internship within one year of his registration;
- completed a management training course of at least thirty hours and organized by the Chamber of Trades and Crafts regional, departmental or interdepartmental;
- received support for the creation of a business lasting a minimum of thirty hours delivered by a network of help to set up a business. As such, the professional must:- have received management training,
  - and be registered in the national directory of professional certifications;
- has been engaged in a professional activity requiring at least the same level as that provided by the SPI for at least three years.

*For further information*: Law No.82-1091 of 23 December 1982 on the vocational training of craftsmen; Decree No.83-517 of 24 June 1983 setting out the conditions for the application of Law 82-1091 of 23 December 1982 relating to the vocational training of craftsmen.

**Application for registration in the trades directory**

**Competent authority**

The professional must apply to the CMA for registration at his head office within one month of starting his activity. However, the request can be made no later than one month after the start of the activity as soon as the professional has notified the president of the CMA, the start date of the CMA, no later than the day before by letter recommended with notice of receipt.

**Supporting documents**

The list of supporting documents to be sent to the CMA depends on the nature of the activity carried out.

**Timeframe**

As soon as the CMA files its complete file, it sends a receipt for the application of a business creation file to the applicant. Failure to respond within two months is worth registration.

*For further information*: Articles 9 and following of Decree No. 98-247 of 2 April 1998 on artisanal qualification and the repertoire of trades.

### b. Professional qualifications - European nationals (Free provision of services or Freedom of establishment)

#### For Freedom to provide services

There is no provision for the national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement to operate as a tea shop operator on a temporary and occasional basis in France.

As such, the professional EU national is subject to the same professional requirements as the French national (see above "2. a. Professional qualifications").

#### For a Freedom of establishment

A national of an EU Member State or the EEA operating a tea shop may, if legally established in that state, carry out the same activity in France. The EU national is exempt from the obligation to perform an SPI (see above "2.0). a. Installation preparation course").

However, the CMA may decide to subject the national to an aptitude test or an adjustment course if it finds substantial differences between their training and that required during the examination of their professional qualifications. for the exercise of the activity in France.

*For further information*: Article 2 of the aforementioned Act of 23 December 1982; Article 6-1 of the decree of 24 June 1983.

### d. Some peculiarities of the regulation of the activity

**Compliance with safety and accessibility standards**

As long as the premises in which the professional operates are open to the public, he must ensure that the safety and accessibility standards applicable to all public institutions (ERPs) are respected.

For example, fire prevention measures and access to premises for people with reduced mobility must be taken.

*For further information*: order of 25 June 1980 approving the general provisions of the Fire and Panic Safety Regulation in public institutions (ERP).

**Compliance with health standards**

The professional operating a tea room offering or containing animal products or foodstuffs must comply with sanitary standards including:

- Product supply conditions
- The temperature at which these products are stored and frozen;
- the temperature of the products.

*For further information*: decree of 21 December 2009 relating to the sanitary rules applicable to the activities of retail, storage and transport of animal products and food containing.

**Food hygiene training**

The fast food establishment must include at least one person with 14-hour food hygiene training on its staff.

However, professionals are exempt from taking such training:

- with at least three years of professional experience with a food company as a manager or operator;
- holders of one of the diplomas listed in Annex of the decree of 25 November 2011 relating to the list of diplomas and titles for professional purposes whose holders are deemed to meet the specific training requirement in food hygiene adapted to the activity of the establishments of commercial catering.

*For further information*: Article D. 233-11 of the Rural Code and Marine Fisheries; decree of 5 October 2011 relating to the specifications of specific training in food hygiene adapted to the activity of commercial catering establishments.

**Music broadcast**

The operator wishing to broadcast music in his tea room must apply online for prior authorization to the Society of Composers and Music Publishers (Sacem) and pay the amount of the royalty. The professional must register and complete the online authorisation form on the[Sacem]((%3Chttps://clients.sacem.fr/autorisations%3E)).

**Price advertising and mandatory mentions**

The professional operating an establishment that serves drinks and food to be consumed on-site must make the prices paid by the consumer on all of his cards and menus as well as outside of establishment.

In addition, the professional operating a tea room must make visible the information about the allergens contained in the products.

*For further information*: order of 27 March 1987 relating to the display of prices in establishments serving meals, foodstuffs or drinks to be consumed on site.

**Regulations on opening hours**

The tea room as a fast food establishment is subject to the provisions relating to the opening and closing hours of the drinks outlets to be consumed on site, set by prefectural decree.

For more information it is advisable to approach the prefecture of the place where the activity is established.

**Viewing smoking areas**

The professional operating a tea room must comply with the obligation to report the smoking ban specific to places for collective use.

*For further information*: Articles R. 3512-2 and the following of the Public Health Code.

**Restaurant titles**

Tea-room operators who work as restaurateurs or assimilated are subject to the legal provisions relating to the use of restaurant titles.

*For further information*: Decree No.2010-1460 of 30 November 2010 on the terms of use of the restaurant title; The company's official website [National Restaurant Titles Commission (CNTR)](http://www.cntr.fr/V2/home.php).

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company registration

Depending on the nature of the business, the entrepreneur must register with the Trades and Crafts Register (RMA) or the Trade and Corporate Register (RCS). It is advisable to refer to the "Commercial Company Reporting Formalities" and "Registration of a Commercial Individual Company at the RCS" for more information.

### a. Company reporting formalities

**Competent authority**

The tea room operator must report his company, and for this, must make a declaration to the Chamber of Commerce and industry.

**Supporting documents**

The person concerned must provide the supporting documents depending on the nature of its activity.

**Timeframe**

The Chamber of Commerce and Industry's Business And Business Formalities Centre sends a receipt to the professional on the same day mentioning the missing documents on file. If necessary, the professional has a period of fifteen days to complete it. Once the file is complete, the Chamber of Commerce and Industry will tell the applicant which organizations their file has been forwarded to.

**Remedies**

The applicant may obtain the return of his file as long as it has not been submitted during the above deadlines.

If the business formalities centre refuses to receive the file, the applicant has an appeal to the administrative court.

**Cost**

The cost of this declaration depends on the legal form of the company.

*For further information*: Section 635 of the General Tax Code (CGI).

### b. If necessary, register the company's statutes

The tea room operator must, once the company's statutes have been dated and signed, register them with the Corporate Tax Office ([Sie](http://www2.impots.gouv.fr/sie/ifu.htm)) if:

- The act involves a particular transaction subject to registration;
- if the very form of the act requires it.

**Competent authority**

The registration authority is:

- The land advertising service of the location of the building, where the acts involve a contribution of real estate or real estate law;
- IES registration centre for all other cases.

**Supporting documents**

The professional must submit two copies of the statutes to the EIS.

*For further information*: Section 635 of the General Tax Code.

