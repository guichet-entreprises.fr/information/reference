﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS020" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Animal sector" -->
<!-- var(title)="Veterinary surgeon" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="animal-sector" -->
<!-- var(title-short)="veterinary-surgeon" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/animal-sector/veterinary-surgeon.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="veterinary-surgeon" -->
<!-- var(translation)="Auto" -->


Veterinary surgeon
==========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

The veterinarian is a professional specializing in animal medicine and surgery.

Its mission includes:

- Protect and care for animals
- Diagnose physical and behavioural illnesses, injuries, pain and malformations of animals;
- Secure prescriptions for medicines
- administering medications by parenteral means;
- ensure food safety and public health, including by contributing to hygiene control in the agri-food industries;
- To preserve the environment
- to develop research and training.

### b. competent CFE

The relevant business formalities centre (CFE) differs from the activity carried out by the person concerned to set up shop:

- If the activity is carried out in an individual undertaking: the competence of the Urssaf;
- If the activity is carried out in the form of a trading company: the jurisdiction of the Chamber of Commerce and Industry (CCI);
- if the activity is carried out in the form of a civil society or a liberal practising company: the jurisdiction of the registry of the commercial court, or the registry of the district court in the departments of the Lower Rhine, Upper Rhine and Moselle.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

To practise as a veterinarian in France, the person concerned must:

- hold the state veterinary diploma issued as of December 21, 1980 in France or another training title listed by the[decreed from 21 May 2004](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000019327053) ;
- be a French national or a national of a Member State of the European Union (EU)or part of the European Economic Area (EEA) agreement;
- register your diploma with one of the regional councils of the College of Veterinarians prior to its inclusion on the Order's board.

*For further information*: Articles L. 241-1 and L. 241-2 of the Rural Code and Marine Fisheries (Rural Code and Marine Fisheries).

**Please note**

The illegal practice of animal medicine or surgery is punishable by two years' imprisonment and a fine of 30,000 euros.

*For further information*: Articles L. 243-1 and L. 243-4 of the Rural Code and Marine Fisheries.

Veterinarians can work as part of:

- An individual company
- Professional Civil Society (SCP);
- a liberal operating society;
- all forms of companies under national law (E.SA, SAS, SARL for example) or companies incorporated in accordance with EU or EEA state legislation and having their statutory headquarters, headquarters or principal institution, therefore they do not give their associates the status of a trader.

In the case of a company-based exercise, the following cumulative conditions are required to:

- more than half of the social capital and voting rights must be held, directly or through companies registered with the Regional College of Veterinarians ("Order"), by persons legally practising the veterinary profession in exercise within society
- direct or indirect ownership of shares or shares of the share capital is prohibited:- individuals or legal entities who, not practising as veterinarians, provide services, products or equipment used in veterinary professional practice,
  - individuals or legal entities engaged, professionally or in accordance with their social purpose, in the breeding, production or transfer, free of charge or expensive, of animals or processing animal products;
- The managers, the chairman of the simplified company, the chairman of the board of directors or the members of the board of directors must be persons who are legally practising the profession of veterinarian;
- the identity of the partners is known and the admission of any new partner is subject to prior approval by collective decision made by the majority of the partners.

**Please note**

The veterinarian will have to pay an annual ordinal dues, the total amount of which varies depending on whether it is a natural person or a legal person.

For more information on the amount of this fee, get closer to the Regional College of Veterinarians.

*For further information*: Article L241-17 of the Rural Code and Marine Fisheries.

### b. Professional Qualifications - EU or EEA nationals (Free Service Or Freedom of establishment)

#### In case of Freedom to provide services

The national of an EU or EEA state, legally practising as a veterinarian in one of these states, may use his professional title in France, either temporarily or occasionally. He must apply for it, prior to his first performance, by declaration addressed to the National Order of Veterinarians (see infra "5°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

In the event that the profession is not regulated in the country in which the professional is legally established, either as part of the activity or as part of the training, he must have carried out this activity for at least one year, in the last ten years years before the benefit, in one or more EU or EEA states.

**What to know**

In order to practice the veterinary profession on a temporary or casual basis, the national must have the necessary language skills.

*For further information*: Article L. 241-3 of the Rural Code and Marine Fisheries.

#### In case of Freedom of establishment

An EU or EEA national wishing to work permanently in France falls under two separate regimes. In both cases, he must have the necessary language skills to carry out the activity in France.

**The automatic recognition scheme for diplomas**

Article L. 241-1 of the Rural and Maritime Fisheries Code provides for a regime of automatic recognition in France of titles and diplomas obtained in an EU or EEA state and subject to the decree of 21 May 2004.

It is up to the regional council of the order of veterinarians to check the regularity of diplomas and other training titles, to grant automatic recognition and then to decide on the application for inclusion on the Order's list, formalities are specified in paragraphs "3." a. Request registration with the College of Veterinarians."

*For further information*: Article L241-2 of the Rural and Marine Fisheries Code, and ordered on 21 May 2004 setting out the list of veterinary diplomas, certificates or titles mentioned in Article L. 241-2 of the Rural Code.

**The derogatory regime: the authorisation to practice**

In order to practise in France, a national with a veterinary degree not mentioned by the decree of 21 May 2004 and Article L. 241-2 of the Rural Code and Maritime Fisheries must obtain an individual authorisation issued by the Minister agriculture (see below. "3°.(a) If necessary, apply for a licence to practise for the EU or EEA national engaged in permanent activity (LE)).

This authorization is subject to a successful knowledge check carried out at the[National Veterinary School of Nantes (ONIRIS)](http://www.oniris-nantes.fr/etudes/examen-de-controle-des-connaissances/examen-de-controle-des-connaissances-pour-lexercice-du-metier-de-veterinaire-en-france/) and the terms of which are set by the decree of 3 May 2010.

*For further information*: Article R. 241-25 and R. 241-26 of the Rural and Marine Fisheries Code; decree of 3 May 2010 relating to the organisation of knowledge control for veterinarians whose diplomas do not receive automatic recognition in France.

### c. Conditions of honorability and incompatibility

The provisions of the code of ethics are imposed on veterinarians practising in France.

As such, they must respect the principles of morality, probity and dedication essential to the exercise of the activity. They are also subject to medical secrecy and must exercise independently.

In addition, the quality of associate veterinarian of professional civil society is incompatible with the manufacture, import, export, distribution and exploitation of veterinary medicines, as well as the manufacture, import and distribution of drugs subject to clinical trials.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Corporate reporting formalities

#### Save your diploma or equivalent title as a veterinarian

The person concerned must, prior to the practice of the veterinary profession in France, register his diploma.

**Competent authority**

The application for registration of the diploma must be addressed to the President of the Order's Regional Council in the region where the person wishes to settle.

**Supporting documents**

In support of his application, the veterinarian must send a file containing:

- A copy of a valid piece of identification or a document attesting to the applicant's nationality;
- certified copy of the State Veterinary Doctor Diploma or one of the titles covered by the May 21, 2004 order.

**Outcome of the procedure**

Once he has registered his diploma, he must then register on the board of the College of Veterinarians.

**Cost**

Free.

*For further information*: Article R. 241-27 of the Rural Code and Marine Fisheries.

#### Apply for registration with the College of Veterinarians

Registration for the College of Veterinarians is mandatory to make the practice of the profession lawful.

**Competent authority**

The application for registration is addressed to the President of the Regional Council of the College of Veterinarians in the region where the person wishes to establish his professional residence.

**Procedure**

It can be filed directly with the regional council of the Order concerned or sent to it by registered mail with notice of receipt.

**Supporting documents**

The[application for registration](https://www.veterinaire.fr/fileadmin/user_upload/documents/exercer-metier/inscription/dossier-inscription-individuel.pdf) on the order of the vets' table is accompanied by a file of supporting documents including:

- proof of knowledge of the French language and the system of weights and measures used on French territory;
- A copy of a valid piece of identification or a document attesting to the applicant's nationality;
- a copy of the state diploma of veterinary doctor or any other title sanctioning training and recognised in a Member State, enabling the exercise in France;
- The application for registration of the diploma as referred to above;
- A criminal record extract less than 3 months old;
- A proof of administrative professional residence;
- a handwritten statement written in the French language in which, under oath, he declares that he has knowledge of the veterinary code of ethics and undertakes to practice his profession with conscience, honour and probity;
- If necessary, a copy of the employment contract between the veterinarian and his employer or when he wishes to practice the profession in shared activity, a copy of the contract concerning him;
- A completed and signed census report card;
- A photo ID.

In the case of an exercise in the form of a company as referred to in paragraph "2." a. Professional qualifications," the veterinarian must provide:

- A copy of the company's statutes
- Certification of the registration on the order of each partner;
- A completed and signed census report card;
- in the case of exercise in the form of SEL or any other company authorized to exercise:- proof of the total or partial release of cash inflows,
  - a note indicating the amount of the share capital, the number of shares, their amount and distribution, the names of veterinarians with their shares, the criteria for repairing profits and the date of start of activity,
  - proof of administrative professional residence;
- in the case of an exercise in the form of CPS, a certificate from the clerk of the court who has ruled on the head office of the company, noting the filing at the registry of the application and the documents used for its registration in the register of trade and companies.

**Please note**

All documents produced in support of the application must be accompanied, if they are not written in French, with a translation certified by a translator sworn or empowered to intervene with the judicial authorities or another EU state, the EEA or the Swiss Confederation.

*For further information*: Articles R. 242-85 and R. 241-40 of the Rural Code and Marine Fisheries.

**Timeframe**

The Chair acknowledges receipt of the full file within one month of its registration.

The College's regional council must decide on the application for registration within three months of receipt of the full application file. If a response is not answered within this time frame, the application for registration is deemed rejected.

This period is increased to six months for nationals of third countries when an investigation is to be carried out outside metropolitan France. The person concerned is then notified.

It may also be extended for a period of no more than two months by the regional council when an investigation outside the national territory has been ordered.

**Outcome of the procedure**

Once the Order has validated the registration, the national is entitled to practice his profession in France.

**Remedy**

In the event of a refusal to register, an appeal can be lodged with the National Council of the Order within two months of notification of the refusal to register.

*For further information*: Articles R. 242-85 and the following of the Rural Code and Marine Fisheries.

#### Make a prior declaration of activity for EU nationals engaged in temporary or occasional activity (LPS)

Any EU or EEA national who is established and legally performs veterinary activities in one of these states may exercise in France on a temporary or occasional basis if he makes the prior declaration.

**Competent authority**

The declaration must be sent by mail or email, before the first service, to the higher council of the National Veterinary College.

**Renewal of pre-declaration**

The prior declaration must be renewed every year and in case of a change in employment status.

**Supporting documents**

The pre-declaration must be accompanied by a complete file with the following supporting documents:

- A copy of a valid piece of identification or a document attesting to the applicant's nationality;
- The[Form](https://www.veterinaire.fr/exercer-le-metier/les-conditions-dexercice-en-france/la-libre-prestation-de-service-lps.html) Prior declaration of service delivery, completed, dated and signed;
- a certificate from the competent authority of the EU member state of establishment or party to the EEA certifying that the person concerned is legally established in that state and that he is not prohibited from practising, accompanied, if necessary, by a translation in Established by a certified translator;
- proof of professional qualifications
- Where access or practice of the veterinary profession does not require the possession of a certificate of capacity in the Member State, proof by any means of practising the profession for at least two years, in the previous ten years.

**Timeframe**

Within one month of receiving the declaration, the College's Senior Council informs the applicant:

- Whether or not he can start delivering services;
- when the verification of professional qualifications shows a substantial difference with the training required in France, he must prove to have acquired the missing knowledge and skills by submitting to an ordeal aptitude. If he meets this check, he is informed within one month that he can begin the provision of services;
- when the file review highlights a difficulty requiring further information, the reasons for the delay in reviewing the file. He then has one month to obtain the requested additional information. In this case, before the end of the second month from the receipt of this information, the national council informs the claimant, after reviewing his file:- whether or not it can begin service delivery,
  - when the verification of the claimant's professional qualifications shows a substantial difference with the training required in France, he must demonstrate that he has acquired the missing knowledge and skills, including subjecting to an aptitude test. If they meet this control, they are informed within one month that they can begin providing services. Otherwise, he is informed that he cannot begin the delivery of services.

In the absence of a response from the College's Senior Council within these timeframes, service delivery may begin.

*For further information*: Article R. 204-1 of the Rural Code and Marine Fisheries.

**Cost**

Registration on the Order's board is free, but it creates an obligation to submit to the compulsory ordinal dues, the amount of which is set annually by the College's higher council.

#### If necessary, apply for a licence to practise for the EU or EEA national engaged in permanent activity (LE)

If the national is not under the automatic recognition scheme, he must apply for a licence to practise.

**Competent authority**

The application for authorisation to exercise must be sent to the Ministry of Agriculture, Agri-Food and Forestry

**Procedure**

The authorisation to exercise is subject to the verification of all knowledge, the terms of which are set out in the order of 3 May 2010.

The[Review](http://www.oniris-nantes.fr/fileadmin/redaction/Etude/Controle_des_connaissances/2017/Octobre_2017/notice_modalitesderoulesexamenCC-V18.pdf) consists of an eligibility test in the form of four multiple-choice questionnaires (MQCs) and oral and practical tests.

To access oral and practical tests, the candidate must obtain an average score of 10 out of 20 on all MQs, with no score of less than 5 out of 20.

**Supporting documents**

Admission to the examination sanctioning the knowledge check is subject to the sending of a complete file addressed to ONIRIS. The file must include the following supporting documents:

- A completed, dated and signed fact sheet of the candidate;
- A letter of request for authorisation to practise for the Minister for Agriculture;
- A resume
- A photocopy of a valid ID
- A copy of an extract from the criminal record (bulletin 3);
- A certified copy of the veterinary degree and its translation into French by a certified translator;
- a form mentioning the disciplines chosen.

The complete file must be mailed to ONIRIS by 31 December of the year before the inspection.

**Outcome of the procedure**

The success of the knowledge control tests is made public by an order of the Minister responsible for agriculture, worth the authorization to practice the profession of veterinary in France. For the exercise to be effective, the national will also have to register with the College of Veterinarians (see supra "5.0). b. Apply for registration with the College of Veterinarians").

*For further information*: Articles R. 241-25 and R. 241-26 of the Rural and Marine Fisheries Code.

**Cost**

The registration fee for the examination is set at 250 euros and must be paid to the ACCOUNTANT of the ONIRIS before 31 December of the year before the inspection.

### b. If necessary, register the company's statutes

When the veterinarian creates a company to carry out his activity, he must register his statutes, once dated and signed, with the Corporate Tax Office (SIE) of the company's headquarters.

In the case of the creation of a commercial company, this formality can be carried out after the file has been submitted to the CFE, but within the maximum period of one month following their signing.

*For further information*: Sections 635 and 862 of the General Tax Code.

**Competent authority**

The Business Tax Office (IES) - The registration centre of the head office is responsible for registering the statutes.

**Supporting documents**

The application for registration of the statutes is a file containing the 4 copies of the statutes.

**Cost**

Free.

### c. Proceed with corporate declaration formalities

The purpose of this formality is to give the company a legal existence (individual company or company).

**Competent authority**

The Centre for Business Formalities (CFE)

**Timeframe**

The CFE sends a receipt to the registrant on the day the file is received (or the first business day following) stating:

- If he feels incompetent, the name of the relevant CFE to which the file was sent on the same day;
- if he considers himself competent:- for an incomplete file, the supporting documents that must be brought in within a fortnight of receiving the receipt,
  - for a complete file, the organizations to which it is transmitted on the same day.

**Remedy**

If the file is not forwarded by the CFE at the end of these deadlines, the registrant may obtain immediate return of his file in order to refer the matter directly to the receiving agencies (Insee, tax administration, social agencies, etc.).

When the CFE refuses to receive the file, the registrant can appeal to the Administrative Court within two months of notification of refusal.

**Supporting documents**

[List of supporting documents](https://www.afecreation.fr/pid14820/pj-prof-liberale-reglementee.html) to be provided for a regulated liberal activity.

**Cost**

The cost of this formality varies, in particular, depending on the legal form.

