﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS020" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Secteur animalier" -->
<!-- var(title)="Vétérinaire" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="secteur-animalier" -->
<!-- var(title-short)="veterinaire" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/secteur-animalier/veterinaire.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="veterinaire" -->
<!-- var(translation)="None" -->

# Vétérinaire

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

Le vétérinaire est un professionnel spécialisé dans la médecine et la chirurgie des animaux.

À ce titre, il a pour mission notamment :

- de protéger et soigner les animaux ;
- de diagnostiquer les maladies physiques et comportementales, les blessures, les douleurs et les malformations des animaux ;
- de sécuriser les prescriptions de médicaments ;
- d'administrer les médicaments par voie parentérale ;
- d'assurer la sécurité sanitaire des aliments et garantir la santé publique, notamment en contribuant au contrôle de l'hygiène dans les industries agroalimentaires ;
- de préserver l’environnement ;
- de développer la recherche et la formation.

### b. CFE compétent

Le centre des formalités de l’entreprise (CFE) compétent diffère de l’activité exercée par l’intéressé pour s’installer :

- si l'activité est exercée en entreprise individuelle : compétence de l'Urssaf ;
- si l’activité est exercée sous la forme d’une société commerciale : compétence de la chambre de commerce et d’industrie (CCI) ;
- si l’activité est exercée sous la forme d’une société civile ou d'une société d'exercice libéral : compétence du greffe du tribunal de commerce, ou greffe du tribunal d’instance dans les départements du Bas-Rhin, du Haut-Rhin et de la Moselle.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

Pour exercer la profession de vétérinaire en France, l'intéressé doit :

- être titulaire du diplôme d’État de vétérinaire délivré à compter du 21 décembre 1980 en France ou d'un autre titre de formation listé par l'arrêté du 21 mai 2004 ;
- être de nationalité française ou ressortissant d'un État membre de l’Union Européenne (UE) ou partie à l'accord de l’Espace économique européen- (EEE) ;
- faire enregistrer son diplôme auprès de l'un des conseils régionaux de l'Ordre des vétérinaires préalablement à son inscription sur le tableau de l'Ordre.

*Pour aller plus loin* : articles L. 241-1 et L. 241-2 du Code rural et de la pêche maritime (Code rural et de la pêche maritime).

**À noter**

L'exercice illégal de la médecine ou de la chirurgie des animaux est puni de deux ans d'emprisonnement et d'une amende de 30 000 €.

*Pour aller plus loin* : articles L. 243-1 et L. 243-4 du Code rural et de la pêche maritime.

Les vétérinaires peuvent exercer leur activité dans le cadre soit :

- d’une entreprise individuelle ;
- d’une société civile professionnelle (SCP) ;
- d’une société d’exercice libéral ;
- de toutes formes de sociétés de droit national (SA, SAS, SARL par exemple) ou de sociétés constituées en conformité avec la législation d’un État l’UE ou de l'EEE et y ayant leur siège statutaire, leur administration centrale ou leur principal établissement, dès lors qu’elles ne confèrent pas à leurs associés la qualité de commerçant.

En cas d’exercice sous forme de société, le respect des conditions cumulatives suivantes est requis :

- plus de la moitié du capital social et des droits de vote doit être détenue, directement ou par l’intermédiaire des sociétés inscrites auprès de l’Ordre régional des vétérinaires (« Ordre »), par des personnes exerçant légalement la professHion de vétérinaire en exercice au sein de la société ;
- la détention, directe ou indirecte, de parts ou d’actions du capital social est interdite :
  - aux personnes physiques ou morales qui, n’exerçant pas la profession de vétérinaire, fournissent des services, produits ou matériels utilisés à l’occasion de l’exercice professionnel vétérinaire,
  - aux personnes physiques ou morales exerçant, à titre professionnel ou conformément à leur objet social, une activité d’élevage, de production ou de cession, à titre gratuit ou onéreux, d’animaux ou de transformation des produits animaux ;
- les gérants, le président de la société par actions simplifiée, le président du conseil d’administration ou les membres du directoire doivent être des personnes exerçant légalement la profession de vétérinaire ;
- l’identité des associés est connue et l’admission de tout nouvel associé est subordonnée à un agrément préalable par décision collective prise à la majorité des associés.

**À noter**

Le vétérinaire devra verser une cotisation ordinale annuelle dont le montant total varie selon qu’il s’agit d’une personne physique ou d’une personne morale.

Pour plus d’informations sur le montant de cette cotisation, se rapprocher de l’Ordre régional des vétérinaires.

*Pour aller plus loin* : article L241-17 du Code rural et de la pêche maritime.

### b. Qualifications professionnelles – Ressortissants de l'UE ou de l'EEE (Libre Prestation de Services ou Libre Établissement)

#### En cas de Libre Prestation de Services (LPS)

Le ressortissant d’un État de l’UE ou de l'EEE, exerçant légalement l’activité de vétérinaire dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel. Il doit en faire la demande, préalablement à sa première prestation, par déclaration adressée à l'Ordre national des vétérinaires (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

Dans le cas où la profession n'est pas réglementée dans le pays dans lequel le professionnel est légalement établi, soit dans le cadre de l'activité, soit dans le cadre de la formation, il doit avoir exercé cette activité pendant au moins un an, au cours des dix dernières années précédant la prestation, dans un ou plusieurs États de l'UE ou de l'EEE.

**À savoir**

Pour exercer à titre temporaire ou occasionnel la profession de vétérinaire, le ressortissant doit posséder les connaissances linguistiques nécessaires.

*Pour aller plus loin* : article L. 241-3 du Code rural et de la pêche maritime.

#### En cas de Libre Établissement (LE)

Le ressortissant de l'UE ou de l'EEE souhaitant exercer à titre permanent en France relève de deux régimes distincts. Dans les deux cas, il devra posséder les connaissances linguistiques nécessaires à l'exercice de l'activité en France.

##### Le régime de reconnaissance automatique des diplômes

L'article L. 241-1 du Code rural et de la pêche maritime prévoit un régime de reconnaissance automatique en France des titres et des diplômes obtenus dans un État de l’UE ou de l’EEE et visés par l'arrêté du 21 mai 2004.

Il appartient au conseil régional de l’Ordre des vétérinaires compétent de vérifier la régularité des diplômes et autres titres de formation, d’en accorder la reconnaissance automatique puis de statuer sur la demande d’inscription au tableau de l’Ordre dont les formalités sont précisées aux paragraphe « 3°. a. Demander son inscription à l'Ordre des vétérinaires ».

*Pour aller plus loin* : article L241-2 du Code rural et de la pêche maritime, et arrêté du 21 mai 2004 fixant la liste des diplômes, certificats ou titres de vétérinaire mentionnée à l'article L. 241-2 du code rural.

##### Le régime dérogatoire : l'autorisation d'exercer

Pour pouvoir exercer en France, le ressortissant titulaire d'un diplôme de vétérinaire non-mentionné par l'arrêté du 21 mai 2004 et par l'article L. 241-2 du Code rural et de la pêche maritime, doit obtenir une autorisation individuelle délivrée par le ministre chargé de l'agriculture (cf. infra. « 3°. a. Le cas échéant, demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE exerçant une activité permanente (LE) »).

Cette autorisation est soumise à la réussite à un contrôle des connaissances effectué à l’[École nationale vétérinaire de Nantes (ONIRIS)](http://www.oniris-nantes.fr/etudes/examen-de-controle-des-connaissances/examen-de-controle-des-connaissances-pour-lexercice-du-metier-de-veterinaire-en-france/) et dont les modalités sont fixées par l'arrêté du 3 mai 2010.

*Pour aller plus loin* : article R. 241-25 et R. 241-26 du Code rural et de la pêche maritime ; arrêté du 3 mai 2010 relatif à l'organisation du contrôle des connaissances pour les vétérinaires dont le diplôme ne bénéficie pas d'une reconnaissance automatique en France.

### c. Conditions d'honorabilité et incompatibilités

Les dispositions du code de déontologie s’imposent aux vétérinaires exerçant en France.

À ce titre, ils doivent notamment respecter les principes de moralité, de probité et de dévouement indispensables à l’exercice de l'activité. Ils sont également soumis au secret médical et doivent exercer en toute indépendance.

De plus, la qualité de vétérinaire associé de société civile professionnelle est incompatible avec la fabrication, l’importation, l’exportation, la distribution et l'exploitation de médicaments vétérinaires, ainsi que la fabrication, l’importation et la distribution de médicaments soumis à des essais cliniques.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration d’entreprise

#### Enregistrer son diplôme ou titre équivalent de vétérinaire

L'intéressé doit, préalablement à l'exercice de la profession de vétérinaire en France, faire enregistrer son diplôme.

##### Autorité compétente

La demande d'enregistrement du diplôme doit être adressée au président du conseil régional de l'Ordre dans la région où l'intéressé souhaite s'établir.

##### Pièces justificatives

À l'appui de sa demande, le vétérinaire doit envoyer un dossier comportant :

- une copie d’une pièce d’identité en cours de validité ou d’un document attestant la nationalité du demandeur ;
- la copie certifiée conforme du diplôme d’État de docteur vétérinaire ou de l'un des titres visés par l'arrêté du 21 mai 2004.

##### Issue de la procédure

Une fois qu'il a enregistré son diplôme, l'intéressé doit ensuite s'inscrire au tableau de l'Ordre des vétérinaires.

##### Coût

Gratuit.

*Pour aller plus loin* : article R. 241-27 du Code rural et de la pêche maritime.

#### Demander son inscription à l'Ordre des vétérinaires

L'inscription à l'Ordre des vétérinaires est obligatoire pour rendre licite l'exercice de la profession.

##### Autorité compétente

La demande d’inscription est adressée au président du conseil régional de l’Ordre des vétérinaires de la région où l’intéressé souhaite établir sa résidence professionnelle.

##### Procédure

Elle peut être directement déposée au conseil régional de l’Ordre concerné ou lui être envoyé par courrier recommandé avec avis de réception.

##### Pièces justificatives

La [demande d'inscription](https://www.veterinaire.fr/fileadmin/user_upload/documents/exercer-metier/inscription/dossier-inscription-individuel.pdf) au tableau de l'ordre des vétérinaires est accompagnée d'un dossier de pièces justificatives comportant :

- la preuve des connaissances de la langue française et du système de poids et de mesures utilisés sur le territoire français ;
- une copie d’une pièce d’identité en cours de validité ou d’un document attestant la nationalité du demandeur ;
- une copie du diplôme d’État de docteur vétérinaire ou tout autre titre sanctionnant la formation et reconnu dans un État membre, habilitant à l’exercice en France ;
- la demande d’enregistrement du diplôme telle que visée ci-dessus ;
- un extrait de casier judiciaire datant de moins de 3 mois ;
- un justificatif de domicile professionnel administratif ;
- une déclaration manuscrite rédigée en langue française par laquelle, sous la foi du serment, l'intéressé déclare avoir eu connaissance du code de déontologie vétérinaire et s'engage à exercer sa profession avec conscience, honneur et probité ;
- le cas échéant, la copie du contrat de travail établi entre le vétérinaire et son employeur ou lorsqu'il souhaite exercer la profession en partage d'activité, une copie du contrat la concernant ;
- un bulletin de recensement complété et signé ;
- une photo d'identité.

En cas d'exercice sous forme de société telle que visée par le paragraphe « 2°. a. Qualifications professionnelles », le vétérinaire doit fournir :

- une copie des statuts de la société ;
- une certification de l'inscription au tableau de l'Ordre de chaque associé ;
- un bulletin de recensement complété et signé ;
- en cas d'exercice sous forme de SEL ou toute autre société habilitée à l'exercice :
  - la preuve de la libération totale ou partielle des apports en numéraire,
  - une note indiquant le montant du capital social, le nombre de parts sociales, leur montant et leur répartition, le nom des vétérinaires possédant ses parts, les critères de réparation des bénéfices et la date de début d'activité,
  - justificatif de domicile professionnel administratif ;
- en cas d’exercice sous forme de SCP, une attestation du greffier de la juridiction ayant statué sur le siège social de la société, constatant le dépôt au greffe de la demande et des pièces ayant servi à son immatriculation au registre du commerce et des sociétés.

**À noter**

Tous les documents produits à l’appui de la demande d’inscription doivent être accompagnés, s’ils ne sont pas rédigés en français, d’une traduction certifiée par un traducteur assermenté ou habilité à intervenir auprès des autorités judiciaires ou administratives d’un autre État de l'UE, de l'EEE ou de la Confédération suisse.

*Pour aller plus loin* : articles R. 242-85 et R. 241-40 du Code rural et de la pêche maritime.

##### Délai

Le président accuse réception du dossier complet dans un délai d’un mois à compter de son enregistrement.

Le conseil régional de l’Ordre doit statuer sur la demande d’inscription dans un délai maximum de trois mois à compter de la réception du dossier complet de demande. À défaut de réponse dans ce délai, la demande d’inscription est réputée rejetée.

Ce délai est porté à six mois pour les ressortissants des États tiers lorsqu’il y a lieu de procéder à une enquête hors de la France métropolitaine. L’intéressé en est alors avisé.

Il peut également être prorogé d’une durée qui ne peut excéder deux mois par le conseil régional lorsqu’une enquête hors du territoire national a été ordonné.

##### Issue de la procédure

Lorsque l'Ordre a validé l'inscription, le ressortissant est habilité à exercer sa profession en France.

##### Voie de recours

En cas de refus d’inscription, un recours peut être déposé auprès du Conseil National de l'Ordre dans un délai de deux mois à compter de la notification du refus d‘inscription.

*Pour aller plus loin* : articles R. 242-85 et suivants du Code rural et de la pêche maritime.

#### Effectuer une déclaration préalable d’activité pour les ressortissants européens exerçant une activité temporaire ou occasionnelle (LPS)

Tout ressortissant de l’UE ou de l’EEE qui est établi et exerce légalement les activités de vétérinaire dans l’un de ces États peut exercer en France de manière temporaire ou occasionnelle s’il en fait la déclaration préalable.

##### Autorité compétente

La déclaration doit être adressée par courrier ou par email, avant la première prestation de service, au conseil supérieur de l'Ordre national des vétérinaires.

##### Renouvellement de la déclaration préalable

La déclaration préalable doit être renouvelée tous les ans et en cas de changement de situation professionnelle.

##### Pièces justificatives

La déclaration préalable doit être accompagnée d'un dossier complet comportant les pièces justificatives suivantes :

- une copie d’une pièce d’identité en cours de validité ou d’un document attestant la nationalité du demandeur ;
- le [formulaire](https://www.veterinaire.fr/exercer-le-metier/les-conditions-dexercice-en-france/la-libre-prestation-de-service-lps.html) de déclaration préalable de prestation de services, complété, daté et signé ;
- une attestation de l’autorité compétente de l’État d’établissement membre de l’UE ou partie à l’EEE certifiant que l’intéressé est légalement établi dans cet État et qu’il n’encourt aucune interdiction d’exercer, accompagnée, le cas échéant, d’une traduction en français établie par un traducteur agréé ;
- une preuve des qualifications professionnelles ;
- Lorsque l'accès ou l'exercice de la profession de vétérinaire n'exige pas la possession d'un certificat de capacité dans l’État membre, la preuve par tout moyen de l'exercice de la profession pendant au moins deux ans, au cours des dix années précédentes.

##### Délai

Dans un délai d’un mois à compter de la réception de la déclaration, le conseil supérieur de l’Ordre informe le demandeur :

- qu’il peut ou non débuter la prestation de services ;
- lorsque la vérification des qualifications professionnelles met en évidence une différence substantielle avec la formation exigée en France, qu’il doit prouver avoir acquis les connaissances et les compétences manquantes en se soumettant à une épreuve d’aptitude. S’il satisfait à ce contrôle, il est informé dans un délai d’un mois qu’il peut débuter la prestation de services ;
- lorsque l’examen du dossier met en évidence une difficulté nécessitant un complément d’informations, des raisons du retard pris dans l’examen de son dossier. Il dispose alors d’un délai d’un mois pour obtenir les compléments d’informations demandés. Dans ce cas, avant la fin du deuxième mois à compter de la réception de ces informations, le conseil national informe le prestataire, après réexamen de son dossier :
  - qu’il peut ou non débuter la prestation de services,
  - lorsque la vérification des qualifications professionnelles du prestataire met en évidence une différence substantielle avec la formation exigée en France, qu’il doit démontrer qu’il a acquis les connaissances et compétences manquantes, notamment en se soumettant à une épreuve d’aptitude. S’il satisfait à ce contrôle, il est informé dans le délai d’un mois qu’il peut débuter la prestation de services. Dans le cas contraire, il est informé qu’il ne peut pas débuter la prestation de services.

En l’absence de réponse du conseil supérieur de l’Ordre dans ces délais, la prestation de service peut débuter.

*Pour aller plus loin* : article R. 204-1 du Code rural et de la pêche maritime.

##### Coût

L'inscription au tableau de l'Ordre est gratuite mais elle engendre l'obligation de se soumettre à la cotisation ordinale obligatoire dont le montant est fixé tous les ans par le conseil supérieur de l'Ordre.

#### Le cas échéant, demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE exerçant une activité permanente (LE)

Si le ressortissant ne relève pas du régime de reconnaissance automatique de son diplôme, il doit solliciter une autorisation d'exercer.

##### Autorité compétente

La demande d'autorisation d'exercice doit être adressée au ministère de l'agriculture, de l'agroalimentaire et de la forêt 

##### Procédure

L'autorisation d'exercice est soumise à la vérification de l'ensemble des connaissances dont les modalités sont fixées par l'arrêté du 3 mai 2010.

L'[examen](http://www.oniris-nantes.fr/fileadmin/redaction/Etude/Controle_des_connaissances/2017/Octobre_2017/notice_modalitesderoulesexamenCC-V18.pdf) se compose d'une épreuve d'admissibilité sous la forme de quatre questionnaires à choix multiple (QCM) puis d'épreuves orales et pratiques.

Pour accéder aux épreuves orales et pratiques, le candidat doit obtenir une note moyenne supérieure ou égale à 10 sur 20 sur l'ensemble des QCM, et ce, sans note inférieure à 5 sur 20.

##### Pièces justificatives

L'admission à l'examen sanctionnant le contrôle des connaissances est soumis à l'envoi d'un dossier complet adressé à l'ONIRIS. Le dossier doit comporter les pièces justificatives suivantes :

- une fiche de renseignements dûment complétée, datée et signée du candidat ;
- une lettre de demande d'autorisation d'exercer à l'attention du ministre chargé de l'agriculture ;
- un curriculum vitae ;
- une photocopie d'une pièce d'identité en cours de validité ;
- une copie d'un extrait du casier judiciaire (bulletin n° 3) ;
- une copie certifiée conforme du diplôme de vétérinaire et sa traduction en français par un traducteur agréé ;
- un formulaire mentionnant les disciplines choisies.

Le dossier complet doit impérativement être transmis par courrier à l'ONIRIS avant le 31 décembre de l'année précédent le contrôle.

##### Issue de la procédure

La réussite aux épreuves du contrôle des connaissances est rendue publique par un arrêté du ministre chargé de l'agriculture, valant autorisation d'exercer la profession de vétérinaire en France. Pour que l'exercice soit effectif, le ressortissant devra également s'inscrire auprès de l'Ordre des vétérinaires (cf. supra « 5°. b. Demander son inscription à l'Ordre des vétérinaires »).

*Pour aller plus loin* : articles R. 241-25 et R. 241-26 du Code rural et de la pêche maritime.

##### Coût

Les frais d'inscription à l'examen sont fixés à 250 € et à régler à l'agent comptable de l'ONIRIS avant le 31 décembre de l'année précédent le contrôle.

### b. Le cas échéant, enregistrer les statuts de la société

Lorsque le vétérinaire crée une société pour exercer son activité, il doit enregistrer ses statuts, une fois datés et signés, auprès du Service des Impôts des Entreprises (SIE) du siège de la société.

Dans le cas de la création d’une société commerciale, cette formalité peut être effectuée après le dépôt du dossier au CFE, mais en respectant le délai maximum d’un mois suivant leur signature.

*Pour aller plus loin* : articles 635 et 862 du code général des impôts.

#### Autorité compétente

Le Service des Impôts des Entreprises (SIE)–Pôle enregistrement du lieu du siège social est compétent pour procéder à l'enregistrement des statuts.

#### Pièces justificatives

La demande d'enregistrement des statuts est un dossier comportant les 4 exemplaires des statuts.

#### Coût

Gratuit.

### c. Procéder aux formalités de déclaration d’entreprise

Cette formalité a pour objet de donner une existence légale à l’entreprise (entreprise individuelle ou société).

#### Autorité compétente

Le Centre de Formalités des Entreprises (CFE)

#### Délai

Le CFE envoie au déclarant le jour même de la réception du dossier (ou le premier jour ouvrable suivant) un récépissé indiquant :

- s’il s’estime incompétent, le nom du CFE compétent auquel le dossier a été transmis le jour même ;
- s’il s’estime compétent :
  - pour un dossier incomplet, les pièces justificatives qui doivent être apportées dans les quinze jours ouvrables à compter de la réception du récépissé,
  - pour un dossier complet, les organismes auxquels il est transmis le jour même.

#### Voie de recours

À défaut de transmission de son dossier par le CFE à l’expiration de ces délais, le déclarant peut obtenir la restitution immédiate de son dossier afin de saisir directement les organismes destinataires (Insee, administration fiscale, organismes sociaux, etc.)

Lorsque le CFE refuse de recevoir le dossier, le déclarant peut faire un recours devant le tribunal administratif dans les deux mois suivant la notification de refus.

#### Pièces justificatives

[Liste des pièces justificatives](https://www.afecreation.fr/pid14820/pj-prof-liberale-reglementee.html) à fournir pour une activité libérale réglementée.

#### Coût

Le coût de cette formalité varie notamment en fonction de la forme juridique.