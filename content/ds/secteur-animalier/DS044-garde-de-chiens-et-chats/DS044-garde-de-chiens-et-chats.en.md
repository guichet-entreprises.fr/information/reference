﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS044" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Animal sector" -->
<!-- var(title)="Dog and cat sitting" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="animal-sector" -->
<!-- var(title-short)="dog-and-cat-sitting" -->
<!-- var(url)="https://www.guichet-entreprises.fr/en/ds/animal-sector/dog-and-cat-sitting.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="dog-and-cat-sitting" -->
<!-- var(translation)="Auto" -->


Dog and cat sitting
=====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

### a. Definition

Dog and cat care, also known as pet-sitting, is a paid activity that consists of keeping dogs and/or cats in premises and with equipment adapted to their needs and condition.

### b. competent CFE

The relevant CFE depends on the nature of the structure in which the activity is carried out:

- In case of artisanal activity, the competent CFE is the Chamber of Trades and Crafts (CMA);
- for the liberal professions, the competent CFE is the Urssaf;
- for commercial companies, it is the Chamber of Commerce and Industry (CCI);
- for civil societies, this is the registry of the Commercial Court;
- for civil societies in the departments of the Lower Rhine, Upper Rhine and Moselle, this is the registry of the district court.

**Good to know**

The activity is considered commercial as long as the company has more than ten employees (except in the Lower Rhine, Upper Rhine and Moselle where the activity remains artisanal regardless of the number of employees of the company provided that it does not use an industrial process). On the other hand, if the company has ten or fewer employees, its activity is considered artisanal. Finally, if the professional has a buying and resale activity, his activity is both artisanal and commercial.

2°. Installation conditions
------------------------------------

### a. Professional qualifications

The dog and cat care activity is reserved for the professional who:

- made the declaration to the prefect of the place of installation;
- has set up and uses facilities that comply with health and animal protection regulations;
- when in direct contact with animals, justifies:- either have a professional certification,
  - either have undergon training in an institution authorized by the Minister of Agriculture to acquire knowledge of the biological, physiological, behavioural and maintenance needs of pets, and has a certificate of knowledge,
  - or hold a certificate of capacity issued by the administrative authority issued prior to the publication of the October 7, 2015 pet trade and protection order.

*To go further* Articles L. 214-6-1 and R. 214-25 of the Rural code and marine fisheries; Decree of 4 February 2016 relating to the training action and updating of the necessary knowledge for persons engaged in activities related to pets of domestic species and the authorisation of training bodies; Site[mesdémarches](http://mesdemarches.agriculture.gouv.fr/demarches/particulier/vivre-avec-un-animal-de-compagnie/article/obtenir-un-certificat-de-capacite-409?id_rubrique=54) Ministry of Agriculture and Food .

### b. Professional Qualifications-EU or EEA Nationals (Free Service Or Freedom of establishment)

#### For a Freedom to provide services

The national of an EU or EEA Member State, who is a child care worker in one of these states, may use his or her professional title in France on a temporary or casual basis.

He must apply, before his first performance, by declaration to the Regional Director of Food, Agriculture and Forestry (see infra "3o). c. Make a pre-declaration of activity for the EU national for a temporary and casual exercise (LPS)).

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional must have carried it out in one or more Member States for at least one year, during the ten years which precede the performance.

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its exercise in France, the person concerned may be subjected to an aptitude test in a one month from the receipt of the request for declaration by the competent authority.

*To go further* Articles L. 214-6-1 and R. 214-25-2 of the Rural Code and Marine Fisheries.

#### For a Freedom of establishment

A national of an EU or EEA state may settle in France to practice permanently if:

- it holds a training certificate or certificate of competency issued by a competent authority in another Member State that regulates access to or exercise of the profession;
- he has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

From then on, he will be able to request a certificate of knowledge from the regional director of food, agriculture and forestry (see infra "3°. d. Request a certificate of knowledge for the EU or EEA national for a permanent exercise (LE) )

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its practice in France, the person concerned may be subjected to an aptitude test or a adaptation course.

*To go further* Articles L. 214-6-1 and R. 214-25-1 of the Rural Code and Marine Fisheries.

### c. Conditions of honorability and incompatibility

The professional is required to keep dogs and cats in premises and with the help of appropriate facilities and equipment. In case of unsanitary conditions, the prefect may issue a ban on keeping animals in the premises.

The professional will have to ensure that the care of the animals does not cause any suffering or adverse effects on them.

*To go further* Articles L. 214-1 and R. 214-33 of the Rural code and marine fisheries.

### d. Some peculiarities of the regulation of the activity

#### Continuous vocational training

The professional engaged in a dog and cat care activity is required to undergo continuous vocational training in order to maintain his knowledge, no later than ten years after the date of issuance of the required training documents (see supra "2. (a) or after the evaluation date.

The training, which lasts 7 hours, must be carried out with an authorized training organization. The trainee will be given a certificate of training to be submitted on request to the control services.

*To go further* Article 4 of the February 4, 2016 order.

#### Requirement to keep specific records of guarded animals

The professional who cares for dogs and cats is required to complete an animal entry and exit register with the names and addresses of the owners, as well as a health monitoring register for the health of the animals kept.

*To go further* Article R. 214-30-3 of the Rural Code and Marine Fisheries.

#### Developing a health regulation

A health regulation must be drafted by the professional in collaboration with a health veterinarian and must include:

- A plan to clean and disinfect premises and equipment;
- Hygiene rules to be followed by staff or the public
- animal maintenance and care procedures including health monitoring, prophylaxis and measures to be taken in the event of a health event;
- the length of periods of isolation of animals.

*To go further* Article R. 214-30 of the Rural Code and Marine Fisheries.

3°. Installation procedures and formalities
------------------------------------------------------

### a. Company reporting formalities

The formalities depend on the legal nature of the business. For more information, please refer to the "Formality of Reporting of a Commercial Company," "Individual Business Registration in the Register of Trade and Companies" and "Corporate Reporting Formalities. artisanal work."

### b. Make a declaration of activity

**Competent authority**

The Departmental Directorate of Population Protection (DDPP) is responsible for deciding on the request for activity declaration.

**Supporting documents**

The[Application](http://www.mesdemarches.agriculture.gouv.fr/demarches/particulier/vivre-avec-un-animal-de-compagnie/article/declarer-un-etablissement-d-302) can be sent by mail or directly online, along with all the following supporting documents:

- The form[Cerfa No. 15045*02](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15045.do) Completed and signed;
- consent of a health veterinarian.

*To go further* Article R. 214-28 of the Rural Code and Marine Fisheries.

### c. Make a pre-declaration of activity for EU or EEA nationals for a temporary and casual exercise (LPS)

**Competent authority**

The Regional Director of Food, Agriculture and Forestry is responsible for deciding on the national's request for a temporary and occasional exercise.

**Renewal of pre-declaration**

It must be renewed once a year and with each change in employment status.

**Supporting documents**

In support of his request for a declaration, the national must, by any means, forward to the competent authority a file containing the following supporting documents:

- Proof of the professional's nationality
- a certificate certifying that it:- is legally established in an EU or EEA state,
  - practises one or more professions whose practice in France requires the holding of a certificate of ability,
  - and does not incur a ban on practising, even temporarily, when issuing the certificate;
- proof of his professional qualifications
- A certificate of professional liability insurance;
- moreover, where neither professional activity nor training is regulated in the EU or EEA State, evidence by any means that the national has been engaged in this activity for one year, full-time or part-time, in the last ten years.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Timeframe**

The regional prefect has one month from the time the file is received to make his decision:

- allow the national to provide his or her first service;
- to subject the person to a compensation measure in the form of an aptitude test, if it turns out that the qualifications and work experience he uses are substantially different from those required for the exercise of the profession in France;
- inform them of one or more difficulties that may delay decision-making. In this case, he will have two months to decide, as of the resolution of the difficulties.

In the absence of a response from the competent authority within these timeframes, service delivery may begin.

*To go further* Article R. 204-1 of the Rural Code and Marine Fisheries.

### d. Request a certificate of knowledge for the EU or EEA national for a permanent exercise

**Competent authority**

The Regional Director of Food, Agriculture and Forestry is responsible for issuing the certificate of knowledge.

**Supporting documents**

The application must be accompanied by the following supporting documents:

- a certificate of competency or a training certificate of a level equivalent to or immediately lower than that required in France to carry out this activity;
- if access or practice of this activity is not regulated in the State of origin, a proof of one year of full-time work experience in the last ten years.

**Cost**

There is a fixed fee for processing the file and implementing:

- or an evaluation in the form of a multiple-choice questionnaire, to the tune of 61 euros;
- or a new test in the form of a multiple-choice questionnaire, to the tune of 31 euros.

*To go further* Articles L. 204-1 and R. 214-25-1 of the Rural code and marine fisheries; decree of 15 January 2002 setting out the terms of the collection of the fee owed by applicants for the issuance of the certificate of knowledge required for the exercise of activities related to domestic pets.

