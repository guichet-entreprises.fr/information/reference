﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DS044" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Services" -->
<!-- var(domain)="Secteur animalier" -->
<!-- var(title)="Garde de chiens et chats" -->
<!-- var(url-domain)="www.guichet-entreprises.fr" -->
<!-- var(url-domain-short)="ge" -->
<!-- var(category-short)="ds" -->
<!-- var(domain-short)="secteur-animalier" -->
<!-- var(title-short)="garde-de-chiens-et-chats" -->
<!-- var(url)="https://www.guichet-entreprises.fr/fr/ds/secteur-animalier/garde-de-chiens-et-chats.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="garde-de-chiens-et-chats" -->
<!-- var(translation)="None" -->

# Garde de chiens et chats

Dernière mise à jour : <!-- begin-var(last-update) --><!-- end-var -->

## 1°. Définition de l’activité

### a. Définition

La garde de chiens et de chats, encore appelé « pet-sitting », est une activité rémunérée qui consiste à garder des chiens et/ou des chats dans des locaux et avec des équipements adaptés à leurs besoins et à leur condition.

### b. CFE compétent

Le CFE compétent dépend de la nature de la structure dans laquelle l’activité est exercée :

- en cas d’activité artisanale, le CFE compétent est la chambre des métiers et de l’artisanat (CMA) ;
- pour les professions libérales, le CFE compétent est l’Urssaf ;
- pour les sociétés commerciales, il s’agit de la chambre de commerce et d’industrie (CCI) ;
- pour les sociétés civiles, il s’agit du greffe du tribunal de commerce ;
- pour les sociétés civiles des départements du Bas-Rhin, du Haut-Rhin et de la Moselle, il s’agit du greffe du tribunal d’instance.

**Bon à savoir**

L’activité est considérée comme commerciale dès lors que l’entreprise compte plus de dix salariés (sauf dans le Bas-Rhin, le Haut-Rhin et la Moselle où l’activité demeure artisanale quel que soit le nombre de salariés de l’entreprise à la condition qu’elle n’utilise pas de procédé industriel). En revanche, si l’entreprise compte dix salariés ou moins, son activité est considérée comme artisanale. Enfin, si le professionnel a une activité d’achat-revente, son activité est à la fois artisanale et commerciale.

## 2°. Conditions d’installation

### a. Qualifications professionnelles

L'activité de garde de chiens et de chats est réservée au professionnel qui :

- en a fait la déclaration auprès du préfet du lieu d'installation ;
- a mis en place et utilise des installations conformes à la réglementation sanitaire et de protection animale ;
- lorsqu'il est en contact direct avec les animaux, justifie :
  - soit être titulaire d'une certification professionnelle,
  - soit avoir suivi une formation dans un établissement habilité par le ministre chargé de l'agriculture lui permettant d'acquérir les connaissances relatives aux besoins biologiques, physiologiques, comportementaux et à l'entretien des animaux de compagnie, et possède une attestation de connaissance,
  - soit être titulaire d'un certificat de capacité délivré par l'autorité administrative délivré avant la publication de l'ordonnance du 7 octobre 2015 relative au commerce et à la protection des animaux de compagnie.

*Pour aller plus loin* : articles L. 214-6-1 et R. 214-25 du Code rural et de la pêche maritime ; arrêté du 4 février 2016 relatif à l'action de formation et à l'actualisation des connaissances nécessaires aux personnes exerçant des activités liées aux animaux de compagnie d'espèces domestiques et à l'habilitation des organismes de formation ; site [mesdémarches](http://mesdemarches.agriculture.gouv.fr/demarches/particulier/vivre-avec-un-animal-de-compagnie/article/obtenir-un-certificat-de-capacite-409?id_rubrique=54) du ministère chargé de l'agriculture et de l'alimentation .

### b. Qualifications professionnelles–Ressortissants de l'UE ou de l'EEE (Libre Prestation de Service ou Libre Établissement)

#### Pour une Libre Prestation de Services (LPS)

Le ressortissant d’un État membre de l’UE ou de l’EEE, exerçant l’activité de garde de chiens et de chats dans l’un de ces États, peut faire usage de son titre professionnel en France, à titre temporaire ou occasionnel.

Il doit en faire la demande, préalablement à sa première prestation, par déclaration adressée au directeur régional de l'alimentation, de l'agriculture et de la forêt (cf. infra « 3°. c. Effectuer une déclaration préalable d’activité pour le ressortissant de l’UE en vue d'un exercice temporaire et occasionnel (LPS) »).

Lorsque ni l'activité, ni la formation conduisant à cette activité ne sont réglementées dans l'État dans lequel il est légalement établi, le professionnel devra l’avoir exercée dans un ou plusieurs États membres pendant au moins un an, au cours des dix années qui précèdent la prestation.

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude dans un délai d'un mois à compter de la réception de la demande de déclaration par l’autorité compétente.

*Pour aller plus loin* : articles L. 214-6-1 et R. 214-25-2 du Code rural et de la pêche maritime.

#### Pour un Libre Établissement (LE)

Le ressortissant d'un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente si :

- il est titulaire d'un titre de formation ou d'une attestation de compétence délivré(e) par une autorité compétente d'un autre État membre qui réglemente l'accès à la profession ou son exercice ;
- il a exercé la profession à temps plein ou à temps partiel pendant un an au cours des dix dernières années dans un autre État membre qui ne réglemente ni la formation, ni l'exercice de la profession.

Dès lors, il pourra demander une attestation de connaissance au directeur régional de l'alimentation, de l'agriculture et de la forêt (cf. infra « 3°. d. Demander une attestation de connaissance pour le ressortissant de l’UE ou de l'EEE en vue d'un exercice permanent (LE) »)

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une épreuve d’aptitude ou un stage d’adaptation.

*Pour aller plus loin* : articles L. 214-6-1 et R. 214-25-1 du Code rural et de la pêche maritime.

### c. Conditions d'honorabilité et incompatibilités

Le professionnel est tenu de garder les chiens et les chats dans des locaux et à l'aide d'installations et d'équipements adaptés. En cas d'insalubrité, le préfet pourra prononcer l'interdiction de garder les animaux dans les lieux.

Le professionnel devra s'assurer que la garde des animaux n'engendre aucune souffrance, ni aucun effet néfaste sur eux.

*Pour aller plus loin* : articles L. 214-1 et R. 214-33 du Code rural et de la pêche maritime.

### d. Quelques particularités de la réglementation de l'activité

#### Formation professionnelle continue

Le professionnel exerçant une activité de garde de chiens et de chats est tenu de suivre une formation professionnelle continue visant à maintenir ses connaissances, au plus tard dix ans après la date de délivrance des titres de formations requis (cf. supra « 2°. a Qualifications professionnelles ») ou après la date d'évaluation.

La formation, d'une durée de 7 heures, doit être effectuée auprès d'un organisme de formation habilité. Le stagiaire se verra remettre une attestation de formation à présenter sur demande aux services de contrôle.

*Pour aller plus loin* : article 4 de l'arrêté du 4 février 2016.

#### Obligation de tenir des registres spécifiques sur les animaux gardés

Le professionnel exerçant la garde des chiens et des chats est tenu de remplir un registre d'entrée et de sortie des animaux avec le nom et l'adresse des propriétaires ainsi qu'un registre de suivi sanitaire de la santé des animaux gardés.

*Pour aller plus loin* : article R. 214-30-3 du Code rural et de la pêche maritime.

#### Élaboration d'un règlement sanitaire

Un règlement sanitaire doit être rédigé par le professionnel en collaboration avec un vétérinaire sanitaire et doit comprendre notamment :

- un plan de nettoyage et de désinfection des locaux et du matériel ;
- les règles d’hygiène à respecter par le personnel ou le public ;
- les procédures d’entretien et de soins des animaux incluant la surveillance sanitaire, la prophylaxie et les mesures à prendre en cas de survenance d’un événement sanitaire ;
- la durée des périodes d’isolement des animaux.

*Pour aller plus loin* : article R. 214-30 du Code rural et de la pêche maritime.

## 3°. Démarches et formalités d’installation

### a. Formalités de déclaration de l'entreprise

Les formalités dépendent de la nature juridique de l’entreprise.

### b. Effectuer une déclaration d'activité

#### Autorité compétente

La direction départementale de la protection des populations (DDPP) est compétente pour se prononcer sur la demande déclaration d'activité.

#### Pièces justificatives

La [demande](http://www.mesdemarches.agriculture.gouv.fr/demarches/particulier/vivre-avec-un-animal-de-compagnie/article/declarer-un-etablissement-d-302) peut être transmise par courrier ou directement en ligne, accompagnée de l'ensemble des pièces justificatives suivantes :

- le formulaire [Cerfa N° 15045](https://www.service-public.fr/professionnels-entreprises/vosdroits/R36866) complété et signé ;
- le consentement d'un vétérinaire sanitaire.

*Pour aller plus loin* : article R. 214-28 du Code rural et de la pêche maritime.

### c. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le directeur régional de l'alimentation, de l'agriculture et de la forêt est compétent pour se prononcer sur la demande déclaration du ressortissant en vue d'un exercice temporaire et occasionnel.

#### Renouvellement de la déclaration préalable

Elle doit être renouvelée une fois par an et à chaque changement de situation professionnelle.

#### Pièces justificatives

À l'appui de sa demande de déclaration, le ressortissant devra transmettre, par tout moyen, à l'autorité compétente un dossier comprenant les pièces justificatives suivantes :

- une preuve de la nationalité du professionnel ;
- une attestation certifiant qu'il :
  - est légalement établi dans un État de l’UE ou de l’EEE,
  - exerce une ou plusieurs professions dont l'exercice en France nécessite la détention d'un certificat de capacité,
  - et n'encourt pas d’interdiction d'exercer, même temporaire, lors de la délivrance de l'attestation ;
- une preuve de ses qualifications professionnelles ;
- une attestation d'assurance responsabilité civile professionnelle ;
- de plus, lorsque ni l'activité professionnelle ni la formation ne sont réglementées dans l'État de l’UE ou de l’EEE, la preuve par tout moyen que le ressortissant a exercé cette activité pendant un an, à temps plein ou à temps partiel, au cours des dix dernières années.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Délai

Le préfet de région dispose d’un délai d’un mois à compter de la réception du dossier pour rendre sa décision :

- d’autoriser le ressortissant à faire sa première prestation de service ;
- de soumettre l’intéressé à une mesure de compensation sous la forme d’une épreuve d’aptitude, s’il s’avère que les qualifications et l’expérience professionnelle dont il se prévaut sont substantiellement différentes de celles requises pour l’exercice de la profession en France ;
- de l’informer d’une ou plusieurs difficultés susceptibles de retarder la prise de décision. Dans ce cas, il aura deux mois pour se décider, à compter de la résolution de la ou des difficultés.

En l'absence de réponse de l'autorité compétente dans ces délais, la prestation de service peut débuter.

*Pour aller plus loin* : article R. 204-1 du Code rural et de la pêche maritime.

### d. Demander une attestation de connaissance pour le ressortissant de l’UE ou de l'EEE en vue d'un exercice permanent

#### Autorité compétente

Le directeur régional de l'alimentation, de l'agriculture et de la forêt est compétent pour délivrer l'attestation de connaissance.

#### Pièces justificatives

La demande devra être accompagnée des pièces justificatives suivantes :

- une attestation de compétences ou un titre de formation d’un niveau équivalent ou immédiatement inférieur à celui exigé en France pour exercer cette activité ;
- si l’accès ou l’exercice de cette activité n’est pas réglementé dans l’État d’origine, un justificatif d'une année d’expérience professionnelle à temps plein au cours des dix dernières années.

#### Coût

Des frais fixes existent pour le traitement du dossier et de la mise en œuvre :

- soit d'une évaluation sous forme de questionnaire à choix multiple, à hauteur de 61 euros ;
- soit d'une nouvelle épreuve sous forme de questionnaire à choix multiple, à hauteur de 31 euros.

*Pour aller plus loin* : articles L. 204-1 et R. 214-25-1 du Code rural et de la pêche maritime ; arrêté du 15 janvier 2002 fixant les modalités de perception de la redevance due par les candidats pour la délivrance de l'attestation de connaissances requise pour l'exercice d'activités liées aux animaux de compagnie d'espèces domestiques.