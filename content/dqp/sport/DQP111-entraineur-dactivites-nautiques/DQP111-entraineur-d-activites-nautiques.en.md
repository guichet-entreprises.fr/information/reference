﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP111" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Water sports coach" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="water-sports-coach" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/water-sports-coach.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="water-sports-coach" -->
<!-- var(translation)="Auto" -->


Water sports coach
======================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The boating coach is a professional who provides teaching, training, development and training in one of the disciplines for which he is a graduate: rowing and related disciplines, canoeing and disciplines associated with calm or white water, sailing floats, water aerotracted slides (e.g. kitesurfing), water skiing and associated disciplines (wakeboarding for example) and sailing within 200 nautical miles.

It accompanies beginners as well as practising audiences. The coach designs sports development programs and conducts training activities. It ensures the safety of the practitioners it accompanies as well as that of third parties.

For more information: Articles 2 of the decrees creating the various mentions relating to the nautical activities of the State Diploma of Youth, Popular Education and Specialty Sport "Sports Development" namely:

- ordered from 1 July 2008 for mentions "rowing and associated disciplines," "canoeing and associated disciplines in calm water," "canoeing and associated whitewater disciplines," "sailing tank" and "sailing";
- decreed on 5 January 2010 for the word "nautical aerotractés";
- June 29, 2009 for the word "water skiing and associated disciplines."

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Coaching activity is subject to the application of Article L. 212-1 of the Code of Sport, which requires specific certifications, including the State Diploma of Youth, Popular Education and Sport (DEJEPS).

As a sports teacher, the sports coach must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*To go further* Articles L. 212-1 and R. 212-84 of the Code of Sport.

#### Training

The DEJEPS sports development, regardless of the mention considered ("sailing," "water skiing and associated disciplines," "nautical aerotracted slides," etc.) is classified as Level III, i.e. bac level 2.

DEJEPS is prepared through initial training, continuing education or validation of experience (VAE). For more information, you can see[official website](http://www.vae.gouv.fr/) VAE.

###### Conditions for access to training leading to DEJEPS mention "rowing and associated disciplines"

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- Hold the Level 1 Civic Prevention and Relief Unit (PSC1) or its equivalent, up-to-date with continuing education;
- pass the tests for access to training;
- communicate a medical certificate of non-contradictory to the teaching and practice of rowing, less than three months old;
- meet the educational requirements required:- be licensed to drive the coastal option motorboats and the "inland waters" option,
  - be able to assess the risks associated with the practice, put in place a safety device on the water and on the ground, safely supervise a regional educational session and intervene with a practitioner in difficulty. These requirements are verified when setting up a 45-minute safety instructional session for a regional crew, followed by a 15-minute interview,
  - produce a certificate, issued by a person holding a certification of aquatic activity supervision in accordance with Article L. 212-1 of the Code of Sport, of success in a 100-metre freestyle test, dived start with recovery of an object submerged at a depth of 2 metres,
  - produce a certificate, issued by the National Technical Director of Rowing, of passing a technical test including a practical test to verify the candidate's autonomy in skiff;
  - produce a certificate, issued by the National Technical Director of Rowing, of success in a technical test including the supervision of an educational session in rowing, followed by an interview to verify the skills of the candidate to train a crew operating at the regional level.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to articles 4, 6 and following of the decree of 1 July 2008 mentioned above.

###### Conditions for access to training leading to DEJEPS mentions "canoe-kayak and associated disciplines in calm water" or "canoeing and disciplines associated with whitewater"

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- certificate of first aid training (PSC1 type, AFPS);
- pass the tests for access to training;
- communicate a medical certificate of non-contradictory to the teaching and practice of canoeing and disciplines associated with calm or white water (depending on the mention chosen), less than three months old;
- meet the required pedagogical requirements: be able to assess the objective risks associated with the practice of the discipline as well as to anticipate potential risks to the practitioner, to master the behaviour and actions to be carried out in the event of an incident or and be able to implement a sports animation session in a situation in calm or white water (depending on the mention chosen). These prerequisites are checked when setting up an educational session, in calm water, followed by an interview;
- produce a certificate, issued by the national technical director of canoeing and related disciplines, of autonomous supervision of a minimum of 600 hours or three sports seasons;
- produce a certificate, issued by the National Technical Director of Canoe-Kayak and Associated Disciplines, of participation in a single-seater boat in a regional or first-level national competition in two different environments, one of which is calm water or white water (depending on the mention chosen);
- produce a certificate, issued by the National Technical Director of Canoe-Kayak and associated disciplines, of success in a technical and pedagogical test organised by the French Canoe-Kayak Federation and associated disciplines, aimed at demonstrating the The candidate's ability to demonstrate technical gestures incorporating propulsion, balance and direction;
- produce a certificate, issued by a person who holds a certification of aquatic activity coaching in accordance with the requirements of Article L. 212-1 of the Code of Sport, of success in a 100-metre free-swimming test with a dive start and recovery of an object submerged at a depth of 2 meters.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to articles 4, 6 and following of the orders of 12 July 2007 mentioned above.

**Good to know**

DeJEPS holders mention "canoe-kayak and associated disciplines in calm water" or "canoe-kayak and associated disciplines in white water" can expand their prerogatives by passing the Certificate of Specialization (CS) "canoe-kayak and disciplines associated with them at sea." For more details on the conditions under which this certificate is obtained, it is advisable to refer to the [official website of this certificate](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/dejeps/Reglementation-11077/La-specialite-perfectionnement-sportif-du-DE-JEPS-et-les-mentions-unites-capitalisables-complementaires-et-certificats-de-specialisation-s-y-rapportant/Les-certificats-de-specialisation-du-DE-JEPS-specialite-perfectionnement-sportif/article/Canoe-kayak-et-disciplines-associees-en-mer).

###### Conditions for access to training leading to the DEJEPS designation "sailing tank"

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- certify the follow-up of "Civic Prevention and Relief 1" (PSC1) education;
- pass the tests for access to training;
- communicate a medical certificate of non-contradictory to the teaching and practice of the sailing tank, less than three months old;
- meet the required prior pedagogical requirements: being able to assess the objective risks associated with the practice of the discipline, to set up and organize a safety device, to be able to safely supervise an educational session for a first level of competition as well as to intervene with a practitioner in difficulty. These prerequisites are checked when setting up a teaching session for a group of pilots, followed by an interview;
- produce a certificate, issued by the national technical director of the sailing tank, of success in a test including a technical test to verify range in a sailing tank (for this event, the candidate must demonstrate his abilities on two types of tank that he chooses among the following three types: "elongated/seated," "standing" or "aerotracté");
- produce a certificate, issued by the national technical director of the sailing tank, of passing a test including the supervision of an educational session, followed by an interview to verify the candidate's skills in training a team of pilots playing at a first level of competition.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to articles 4, 6 and following of the decree of 1 July 2008 mentioned above.

###### Conditions for access to training leading to deJEPS mention "nautical aerotracted slips"

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- certificate of first aid training (PSC1 type, AFPS);
- pass the tests for access to training;
- communicate a medical certificate of non-contradictory to the teaching and practice of aerotracted gliding in question, less than three months old;
- meet the required pedagogical requirements: be able to assess the objective risks associated with the practice of the discipline in the aerological and maritime fields and to intervene technically on incidents, organize the practice area and monitor activity, organise the rescue trigger in accordance with current regulations, prevent risky behaviours including doping and implement a formative situation. These prerequisites are checked when setting up a nautical aerotracté session with three practitioners, either from the boat or from the beach, and followed by maintenance;
- produce the licence to drive pleasure boats with a "coastal" option and the certificate authorizing the use of radio telephony;
- produce a certificate, issued by the National Director of Free Flight, of success at a course of at least 400 meters, at sea, swimming from the edge, including one or more wave crossings as well as a rescue action of a practitioner based on sea conditions;
- produce, as a choice, a certificate, issued by the technical director of free flight:- effective participation in three national competitions in the field of nautical aerotracted slides,
  - or passing a technical test consisting of a performance evaluated by the National Technical Director of Free Flight,
  - produce a certificate, issued by the National Technical Director of Free Flight, justifying experience in coaching, teaching, training, tutoring, training or coordinating projects in the field of sport during a season Sporty;
  - produce a certificate, issued by the National Technical Director of Free Flight, of success in an interview based on a file prepared by the candidate, relating to his or her experiences of coaching, teaching, training, tutoring, training or project coordination in the sports field.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to articles 4, 6 and following of the decree of 5 January 2010 mentioned above.

**Good to know**

DeJEPS holders with the term "nautical aerotractés" can expand their prerogatives by passing the "kite" CS. For more details on the conditions under which this certificate is obtained, it is advisable to refer to the [official website of this certificate](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/dejeps/Reglementation-11077/La-specialite-perfectionnement-sportif-du-DE-JEPS-et-les-mentions-unites-capitalisables-complementaires-et-certificats-de-specialisation-s-y-rapportant/Les-certificats-de-specialisation-du-DE-JEPS-specialite-perfectionnement-sportif/article/Cerf-volant-13105).

###### Conditions for access to training leading to the DEJEPS "sailing" designation (for a practice less than 200 nautical miles)

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- certificate of first aid training (PSC1) or its equivalent;
- pass the tests for access to training;
- communicate a medical certificate of non-contradictory to the teaching and practice of sailing, less than three months old;
- meet the required pedagogical requirements: to be able to ensure the physical and moral integrity of the host audiences, to organize a practice area, to monitor activity in the practice area, to respond to an incident or accident of appropriate way with a group of practitioners or a crew, to analyse a surveillance and intervention device in accordance with current regulations and to control the current anti-doping system. These requirements are verified when setting up a pedagogical session, followed by an interview;
- Hold the driver's license of motor pleasure boats, a "coastal" option or its equivalent;
- produce one or more certificates, issued by the competition organiser or the national technical director of sailing, of participation in six minimum competition events, of which at least one level equivalent to the fourth grade and one of equivalent to the third grade, as defined by the French Sailing Federation;
- produce a certificate, issued by the National Technical Director of Sailing, attesting to the candidate's level of practical performance on a medium he chooses (dinghy, windsurfing, catamaran or habitable) allowing him to rank in the first third of an event equivalent to the fourth grade or in the first half of an event equivalent to the third grade, as defined by the French Sailing Federation;
- produce a certificate, issued by the national technical director of sailing, of supervision (volunteer or professional) carried out independently on at least two supports (drifter, windsurfing, catamaran or habitable).

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Articles 4 and 6 of the decree of 1 July 2008 mentioned above.

**Good to know**

DeJEPS holders can expand their prerogatives by passing the "kite" CS. For more details on the conditions under which this certificate is obtained, it is advisable to refer to the [official website of this certificate](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/dejeps/Reglementation-11077/La-specialite-perfectionnement-sportif-du-DE-JEPS-et-les-mentions-unites-capitalisables-complementaires-et-certificats-de-specialisation-s-y-rapportant/Les-certificats-de-specialisation-du-DE-JEPS-specialite-perfectionnement-sportif/article/Cerf-volant-13105).

###### Conditions for access to training leading to DEJEPS mention "water skiing and related disciplines"

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- certificate of first aid training (PSC1 type, AFPS);
- pass the tests for access to training;
- communicate a medical certificate of non-contradictory to the teaching and practice of water skiing or an associated discipline, less than three months old;
- meet the required educational requirements: having a motor pleasure boat driver's licence, a "coastal" option and an "inland water" option, being able to tow a skier in regional competitive conditions, being able to assess the objective risks associated with the practice, to put in place a safety device on the water and on the ground, to be able to intervene with a practitioner in difficulty and to safely supervise a technical development session of a competitors playing at the regional level. These pre-requirements are verified through the production of the aforementioned driver's licence, a pilot test of a regional competition level and the implementation of a technical development session of up to 20 minutes for a competitor. regionally, followed by a 30-minute interview;
- produce a certificate, issued by a person holding a certification of aquatic activity supervision in accordance with Article L. 212-1 of the Code of Sport, of success in a 100-metre freestyle test, dived start, with recovery of a submerged dummy at a depth of 1.80 metres;
- produce a certificate, issued by the National Technical Director of Water Skiing and Wakeboarding, of success in a "black propeller" pilot test organised by the French Water Ski and Wakeboarding Federation;
- produce a certificate, issued by the National Technical Director of Water Skiing and Wakeboarding, of success in a technical test of a "silver rudder" level in water skiing or wakeboarding boat or cable, organized by the French Ski Federation nautical and wakeboarding, including a practical test to check the candidate's level in classic skiing or an associated discipline;
- produce a certificate, issued by the National Technical Director in water skiing, of success in an educational test including the supervision of an introductory session, followed by an interview to verify the candidate's skills in coaching a competitor playing at a first degree of competition.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to articles 4, 6 and following of the order of 29 June 2009 mentioned above.

**Good to know**

Individuals with the Senior State Diploma of Youth, Popular Education and Sport (DESJEPS) specialty sports performance may also practice as a sports coach. This Level II state diploma is issued by the Regional Director of Youth and Sports. To practice in water sports, the trainer will opt for one of the following desJEPS mentions: "rowing," "canoeing and disciplines associated with calm water," "canoeing and associated disciplines in white water and at sea," "sailing tank," " nautical skiing, "water skiing and associated disciplines" or "sailing."[More information](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/des-jeps/Reglementation-11081/La-specialite-performance-sportive-du-DES-JEPS-et-les-mentions-unites-capitalisables-complementaire-et-certificats-de-specialisation-s-y-rapportant/) on admission requirements and training leading up to graduation.

*To go further* Articles D. 212-35 and following of the Code of Sport, ordered on 20 November 2006 organising the State Diploma of Youth, Popular Education and Specialty Sport "Sports Development" issued by the Ministry for Youth and Youth sports and all of the above-mentioned decrees creating the various mentions relating to nautical activities.

#### Costs associated with qualification

Training at DEJEPS sports development, regardless of the mention chosen, is paid (variable cost depending on the mentions). For [more details](https://foromes.calendrier.sports.gouv.fr/#/formation), it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as a coach of nautical activities in France for persons who have been convicted of any crime or for one of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

4°. Reporting requirement (for the purpose of obtaining the professional sports educator card)
------------------------------------------------------------------------------------------------------------------

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

#### Competent authority

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the [official website](https://eaps.sports.gouv.fr).

#### Time

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

#### Supporting documents

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

#### Cost

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

5°. Qualifications process and formalities
----------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

#### Competent authority

The prior declaration of activity should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP) of the department where the declarer wants to perform his performance.

#### Time

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

#### Supporting documents

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

#### Competent authority

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

#### Time

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

#### Supporting documents for the first declaration of activity

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France,
  - documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

#### Evidence for a renewal of activity declaration

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

**Good to know: compensation measures**

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

