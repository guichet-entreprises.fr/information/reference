﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP117" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Team sports coach" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="team-sports-coach" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/team-sports-coach.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="team-sports-coach" -->
<!-- var(translation)="Auto" -->


Team sports coach
==================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The team sports coach is a professional who provides teaching, coaching, development and training in one of the group sports for which he is a graduate: baseball, softball, cricket, basketball, American football, handball, hockey, rugby (xiii or XV), volleyball and beach volleyball, ice hockey, polo, water polo, rowing and football.

It accompanies beginners as well as audiences practicing the competition. The coach designs sports development programs and conducts training activities. It ensures the safety of the practitioners it accompanies as well as that of third parties.

For more information: Articles 2 of the decrees creating the various mentions of the State Diploma of Youth, Popular Education and Specialty Sport Sport "Sports Development":

- arrested on 15 April 2009 for "baseball, softball and cricket";
- decreed on 8 May 2010 for the word "basketball";
- decree of 29 June 2009 for the word "American football";
- decreed on 4 January 2008 for the word "handball";
- ordered from 1 July 2008 for mentions "hockey," "rugby league," "volleyball (and beach volleyball), "rowing and associated disciplines" and "polo";
- decreed on 15 December 2006 for the word "rugby union";
- january 25, 2011 for "ice hockey";
- decree of 15 March 2010 for the word "water polo";
- decree of 26 May 2016 registering the national directory of professional certifications of the "football coach" diploma.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Coaching activity is subject to the application of Article L. 212-1 of the Code of Sport, which requires specific certifications, including the State Diploma of Youth, Popular Education and Sport (DEJEPS) and the Patent football coach.

As a sports teacher, the coach must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*To go further* Articles L. 212-1 and R. 212-84 of the Code of Sport.

#### Training

The DEJEPS sports development, regardless of the mention considered (basketball, hockey, volleyball, etc.) as well as the football coach's certificate are classified as Level III, i.e. level bac 2.

The DEJEPS and the football coach's certificate are prepared through initial training, continuing education or validation of experience (VAE). For more information, you can see[official website](http://www.vae.gouv.fr/) VAE.

##### Common conditions of access to training leading to obtaining one of the DEJEPS of group sports, regardless of the mention chosen

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- certify that first aid training is being followed (PSC1 type, AFPS);
- pass the tests for access to training;
- communicate a medical certificate of non-contradictory to the teaching and sport practice in question, less than three months old;
- meet the required pedagogical requirements: be able to assess the objective risks associated with the practice of the discipline, anticipate potential risks to the practitioner and master the behaviour and actions to be carried out in the event of an incident or accident. These skills are checked in a 30-minute session, followed by a 20-minute interview.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

##### Additional DEJEPS-specific conditions mentioning "baseball, solftball and cricket"

The person concerned must:

- Be able to perform a technical demonstration and analyze it (during the implementation of a 30-minute introductory session, followed by a 20-minute interview);
- produce a certificate, issued by the National Technical Director of Baseball, Softball and Cricket, of coaching in one of these disciplines of at least 200 hours during a sports season;
- produce a certificate, issued by the National Technical Director of Baseball, Softball and Cricket, to practice one of these disciplines for at least three sporting seasons.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Articles 4 and 6 of the order of 15 April 2009 mentioned above.

##### Additional deJEPS-specific conditions mentioning "basketball"

The person concerned must:

- Be able to implement an opposition sequence in basketball (during the implementation of a training session, followed by an interview);
- produce a certificate, issued by the national technical director of basketball, of coaching experience for at least three seasons in one or more teams playing in the French championship, young or senior, or in level teams similar in foreign league;
- produce a certificate, issued by the National Technical Director of Basketball, to practice this discipline for three sports seasons;
- produce a certificate of success in the oral test organized by the French Basketball Federation and issued by the National Technical Director of Basketball. This event consists of analysing a video document tracing a match sequence lasting up to 2 minutes for a competition chosen from the following championships: "National Male or Female 2", "National Male or Female 3" or French youth championship. The candidate must demonstrate his or her ability to observe, analyze and diagnose in order to develop training for a national level player or for a group playing in a national level competition.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Articles 4 and 6 of the order of 8 May 2010 mentioned above.

##### Additional deJEPS-specific conditions mentioning "American football"

The person concerned must:

- be able to perform a technical demonstration, analyze it and implement an introductory session (these requirements are monitored during the implementation of a 30-minute introductory session in American football, followed by an interview with 20 minutes);
- produce a certificate, issued by the National Technical Director of American Football, of 150 hours of introductory experience in American football during a sports season;
- produce a certificate, issued by the National Technical Director of American Football, of practicing for at least one season of sports in American football.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Articles 4 and 6 of the order of 29 June 2009 mentioned above.

##### Additional deJEPS-specific conditions mentioning "handball"

The person concerned must:

- Be able to implement an opposition sequence in handball;
- produce a certificate, issued by the National Technical Director of Handball, of responsible handball coaching for 450 hours, or at least three sporting seasons in the last five years;
- produce a certificate, issued by the National Technical Director of Handball, of practice of the discipline covering three seasons in the last five years.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Articles 4 and 6 of the decree of 4 January 2008 mentioned above.

##### Additional deJEPS-specific conditions mentioning "hockey"

The person concerned must:

- be able to assess one's own ability to perform a technical demonstration in hockey as well as conduct a safe learning session (these requirements are monitored when setting up a safe hockey learning session 30 minutes maximum, followed by a 15-minute interview);
- produce a certificate, issued by the National Technical Director of Hockey, of coaching hockey during a sports season and of a minimum duration of 100 hours over the past five years.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Articles 4 and 6 of the decree of 1 July 2008 mentioned above.

##### Additional deJEPS-specific conditions mentioning "rugby league"

The person concerned must:

- Be able to implement a training situation (this requirement is monitored during the setting up of an educational session, followed by an interview);
- produce a certificate, issued by the national technical director of rugby league, licensed to the French Rugby League Federation or a nation member of the International Rugby League Board, for at least three sporting seasons ;
- produce a certificate, issued by the National Technical Director of Rugby League, of technical supervision of a rugby league team (youth rugby school, seniors) for at least one sports season.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Articles 4 and 6 of the decree of 1 July 2008 mentioned above.

##### Additional deJEPS-specific conditions mentioning "rugby union"

The person concerned must:

- Be able to implement a formative situation
- produce one or more certificates of participation in rugby union competitions for at least three sporting seasons;
- produce one or more certificates of participation in the coaching of a team (rugby school, youth or senior) in rugby union for at least one sports season.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Articles 3, 4 and 6 of the order of 15 December 2006 mentioned above.

##### Additional deJEPS-specific conditions mentioning "volleyball"

The person concerned must:

- Be able to implement an animation sequence in volleyball or beach volleyball (this requirement is controlled when setting up an educational session, followed by an interview);
- produce a certificate, issued by the National Technical Director of Volleyball, Volleyball or Beach Volleyball Coaching;
- produce a certificate, issued by the National Technical Director of Volleyball, of practicing volleyball or beach volleyball for at least two sports seasons.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Articles 4 and 6 of the decree of 1 July 2008 mentioned above.

##### Additional deJEPS-specific conditions mentioning "polo"

The person concerned must:

- produce a certificate, issued by the legal manager of the structure or structures in which the activity or which the activity was carried out or by the national technical director of polo, justifying a two-year professional or volunteer experience Group coaching;
- produce a certificate, issued by the national technical director of polo, deding the candidate's ability to master the technique of polo, to perform the technical analysis of an unknown player-horse couple from a video and to propose situations training sessions. These abilities are verified using a technical test organised by the French Polo Federation.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Articles 4 and 6 of the decree of 1 July 2008 mentioned above.

##### Additional deJEPS-specific conditions referred to "ice hockey"

The person concerned must:

- Produce a certificate of coaching experience for a minimum 450-hour ice hockey team acquired over the past three sporting seasons over the past five years;
- justify experience as a competitive participant in ice hockey during three sports seasons.

To verify these two pre-requirements, the candidate must provide certifications prepared by the National Technical Director of Ice Hockey.

Under certain conditions, exemptions can be obtained for certain events. For more details, it is advisable to refer to Articles 4 and 6 of the order of 25 January 2011 mentioned above.

##### Additional deJEPS-specific conditions mentioning "water polo"

The person concerned must:

- Hold the First Aid Certificate in a Level 1 team (or its equivalent) on the day of continuing education;
- be able to rescue the water polo player, present the federal sports program and the polo player's career plan concepts. These skills are checked during the conduct of a 20-minute water polo sports development session, followed by a maximum of 30 minutes of interview;
- produce a certificate of success, issued by the Technical Director of Swimming, to a safety test conducted without swimming goggles, nose clips or ladder aid. This test is carried out over a distance of 50 meters and breaks down as follows:- free departure from the edge of the basin,
  - 25-metre freestyle course,
  - so-called duck dive and search for a regulatory dummy submerged 25 meters from the starting point at a depth between 1.80 and 3 meters,
  - the dummy's ascent to the surface,
  - towing a person for a distance of 25 metres to the edge of the basin,
  - The victim's water
- to carry out a shooting course justifying a technical level corresponding to the skills targeted in the "water polo" competition pass issued by the French Swimming Federation. The success of this test is the subject of a certificate issued by the National Technical Director of Swimming;
- produce a certificate of experience of a minimum water polo practice over three sports seasons in a team equivalent to the "National 3," "National 2" or "National 1" and justifying effective participation in the ten-game field matches at least per sports season. This certificate is issued by the National Technical Director of Swimming;
- produce a certificate of educational experience, whether voluntary or professional, in water polo of 800 hours either within a club of an accredited sports federation or within a pole on the list drawn up by the Minister responsible for sports in Article R. 221-26 of the Code of Sport, over a minimum of three years over the past five years. This certification is established by the National Technical Director of Swimming for structures affiliated with the French Swimming Federation, or by the National Technical Director or, failing that, by the President of one of the Council's Member Federations. interfederal aquatic activities in agreement with the French Swimming Federation for their affiliated structures.

Under certain conditions, exemptions can be obtained for certain events. For more details, it is advisable to refer to articles 4, 6 and following of the order of 15 March 2010 mentioned above.

###### Additional deJEPS-specific conditions mention "rowing and associated disciplines"

The person concerned must:

- Be licensed to drive pleasure craft with a "coastal" option and "inland waters" option;
- be able to put a safety device in place on the water and on the ground and know how to intervene with a practitioner in difficulty. These requirements are verified when setting up a 45-minute safety instructional session for a crew operating at the regional level, followed by a 15-minute interview;
- produce a certificate of success, issued by a person holding a certification of aquatic activity coaching in accordance with Article L. 212-1 of the Code of Sport, to a 100-metre freestyle test diving with recovery of an object submerged at a depth of 2 metres;
- produce a certificate of success, issued by the National Technical Director of Rowing, to a first technical test organised by the French Federation of Rowing Societies including a practical event to verify the autonomy of the candidate in skiff;
- produce a certificate of success, issued by the National Technical Director of Rowing, to a second technical test organised by the French Federation of Rowing Societies, including the supervision of an educational session in rowing followed by a to verify the candidate's skills in training a crew operating at the regional level.

Under certain conditions, exemptions can be obtained for certain events. For more details, it is advisable to refer to Articles 4 and 6 of the order of 1 July 2008 mentioned above.

###### Conditions for access to training leading to the certification of a football coach

The person concerned must:

- Be of age on the day of entry into training;
- be licensed to the French Football Federation for the current season (and provide a copy of its licence);
- hold the First Aid Training Certificate (AFPS) or the Level 1 Civic Prevention and Relief Certificate (PSC1);
- complete the application form, attach a photocopy of his valid ID, a photo ID and a certificate of honorability;
- communicate a medical certificate of non-contradictory to the practice and coaching of football, less than three months old;
- to produce the certificate of success in the selection tests, issued by the training organization. This test includes:- a written exam of up to 45 minutes on the candidate's animation and practice experience,
  - A maximum of 20-minute interview to assess the candidate's motivations, training skills and ability to express him,
  - Hold the regional level of play certificate issued by the French Football Federation;
- meet one of the following conditions and provide a copy of the corresponding diploma or the corresponding certificate, if any:
- Be a football instructor's license obtained after April 2, 2008;
- be or have been a high-level sportsman on the ministerial list mentioned in Article L. 221-2 of the Code of Sport;
- be or have been a national player in Ligue 1 or League 2 or National or CFA or CFA2 for 100 senior matches;
- be or have played nationally in women's D1 or D2 for 100 senior matches;
- Hold the state certificate of first-degree sports educator football option;
- hold the professional certificate of youth and sports mention "football";
- hold at least one capitalized unit of the football coach's certificate obtained as part of an application for validation of experience (VAE).

Under certain conditions, exemptions can be obtained for certain events. For more details, it is advisable to refer to the order of 26 May 2016 registering in the national directory of professional certifications under the title "football coach".

**Good to know**

Individuals with the Senior State Diploma of Youth, Popular Education and Sport (DESJEPS) specialty sports performance may also practice as a sports coach. This Level II state diploma is issued by the Regional Director of Youth and Sports. To practice in the field of group sports, the coach will opt for one of the following desJEPS mentions: "rowing," "baseball, softball," "basketball," "cricket," "football," "American football," "handball," "hockey," "ice hockey," rugby league, rugby union, volleyball, rowing or water polo. Finally, the holder of the DESJEPS mention "volleyball" can specialize in the discipline of beach volleyball by opting for the certificate of specialization "beach volleyball". For more information on admission requirements and training leading to graduation, please refer to the [Ministry of Sport website](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/des-jeps/Reglementation-11081/La-specialite-performance-sportive-du-DES-JEPS-et-les-mentions-unites-capitalisables-complementaire-et-certificats-de-specialisation-s-y-rapportant/).

*To go further* Articles D. 212-35 and following of the Code of Sport, ordered on 20 November 2006 organising the State Diploma of Youth, Popular Education and Specialty Sport "Sports Development" issued by the Ministry for Youth and Youth sports and all of the aforementioned decrees creating the various mentions relating to collective sports.

#### Costs associated with qualification

The deJEPS diploma training in sports development mentions collective sports is paid (variable cost depending on the mentions). The training leading to the football coach's certificate costs about 2,800 euros. For [more details](https://foromes.calendrier.sports.gouv.fr/#/formation), it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as a coach of collective sports in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

4°. Reporting requirement (for the purpose of obtaining the professional sports educator card)
------------------------------------------------------------------------------------------------------------------

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

#### Competent authority

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the [official website](https://eaps.sports.gouv.fr).

#### Time

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

#### Supporting documents

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

#### Cost

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

5°. Qualifications process and formalities
----------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

#### Competent authority

The prior declaration of activity should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP) of the department where the declarer wants to perform his performance.

#### Time

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

#### Supporting documents

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

#### Competent authority

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

#### Time

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

#### Supporting documents for the first declaration of activity

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France,
  - documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

#### Evidence for a renewal of activity declaration

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

**Good to know: compensation measures**

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

