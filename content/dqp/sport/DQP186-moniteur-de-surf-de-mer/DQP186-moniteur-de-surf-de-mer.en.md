﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP186" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Surf instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="surf-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/surf-instructor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="surf-instructor" -->
<!-- var(translation)="Auto" -->


Surf instructor
================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The sea surf instructor supervises and independently animates surfing activities (surfing, shortboarding, longboarding, bodyboarding, bodysurfing, kneeboard, skimboard) for any public. It ensures the safety of both practitioners and third parties, as well as the maintenance and maintenance of equipment.

*For further information*: decree of 9 July 2002 establishing the speciality nautical activities of the professional certificate of youth, popular education and sport (BPJEPS), decree of 27 April 2007 creating the mention "surf" of the State Diploma of Youth, Popular Education and Sport (DEJEPS), decree of 27 April 2007 creating the "surf" designation of the State Diploma of Youth, Popular Education and Sport (DESJEPS) specialty "sports performance".

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The surf instructor activity is subject to the application of Article L. 212-1 of the Code of Sport, which requires specific certifications. As a sports teacher, a sea surf instructor must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the [national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Qualifications for surfing instructors are BPJEPS, DEJEPS and DESJEPS.

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*For further information*: Articles L. 212-1 and R. 212-84 of the Code of Sport.

**Good to know: the specific environment**

The practice of sea surfing, regardless of the area of evolution, constitutes an activity carried out in a specific environment involving the respect of specific safety measures. Therefore, only organizations under the tutelage of the Ministry of Sport can train future professionals.

*For further information*: Articles L. 212-2 and R. 212-7 of the Code of Sport.

#### Training

##### BPJEPS "water activities" mention monovalent surfing

The BPJEPS nautical activities attest to the possession of the professional skills essential to the exercise of nautical activity instructor. It is a level 4 degree (such as a technical bachelor's degree, a traditional bachelor's degree or a technician's certificate).

To practice as a surf instructor, the candidate must choose the monovalent "surf" designation of the specialty BPJEPS "water activities."

The specialty BPJEPS "water activities" mention monovalent "surfing" is prepared alternately by initial training, learning or continuing education. In initial training, the minimum duration in a training centre is 600 hours.

For more information on the validation of experience (VAE), you can visit the[official website](http://www.vae.gouv.fr/) VAE.

**Prerogatives****

The BPJEPS water activities mention surfing allows to supervise surfing activities (shortboard, longboard, bodyboard, bodysurfing, kneeboard, skimboard) for any public and at any place of practice of the activity.

###### Conditions for access to training leading to the PJEPS

The person concerned must:

- Be of age on the day of entry into training;
- produce a medical certificate of non-contradictory to the practice and teaching of surfing activities, dating back less than three months to the date of entry into training;
- Hold the Level 1 Civic Prevention and Relief (PSC1) teaching unit or its equivalent;
- Hold the Certificate of Supplemental First Aid Training with Equipment (AFCPSAM) or its equivalent;
- report a certificate of success to a 100-metre freestyle test, dive start and recovery of a submerged object at a depth of 2 meters, issued by a person holding the state certificate of sports educator option "swimming activities", a lifeguard (or someone with an equivalent degree);
- produce the certificate of success in an event consisting of performing one or more manoeuvres on a wave using its height and functional length, carried out on a support to the candidate's choice among the following: shortboard, longboard, Bodyboard. The modalities of this event, evaluated by the national technical directorate of the French Surfing Federation, are defined according to sea conditions;
- produce the proof of success in the event consisting of a course of at least 400 meters at sea from the edge, including one or more bodysurf bar crossings. The terms of this test are defined by the jury according to the sea conditions. The use of fins and a suit, with the exception of any other material, is permitted;
- meet the pre-educational requirements:- Be able to offer practitioners situations that promote the learning of active safety,
  - know how to manage the practice area in the face of security constraints,
  - Be able to respond appropriately to manage the group's security,
  - be able to identify weather and natural elements to adapt animation projects,
  - Be able to enforce regulations related to the supervision of nautical activities and comply with regulations relating to places of practice,
  - be able to identify specific risks related to behaviours and risks of practice,
  - be able to apply the hygiene and safety principles of equipment and equipment, manage the safety of practitioners and implement rescue and assistance in the nautical environment.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events.

*For further information*: decree of 9 July 2002 establishing the speciality "nautical activities" of the professional certificate of youth, popular education and sport.

##### DeJEps specialty sports development mention surfing

DEJEPS is a Level 3-certified diploma (Bac 2/3). It attests to the acquisition of a qualification in the exercise of a professional activity of coordination and supervision for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in initial training by taking 700 hours of training in a training centre and 500 hours in alternating structure or through the VAE. For more information, you can see[official website](http://www.vae.gouv.fr/) VAE.

**Please note**

The holder of the DEJEPS mention surfing is considered to supervise an activity operating in a specific environment. As such, it is subject to the specific safety measures referred to in section L. 212-2 of the Code of Sport.

###### Conditions for access to training leading to deJEPS "surfing"

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age on the day of entry into training;
- certify training at the "First Aid Level 1 Team" (PSE1) training unit, or its equivalent, up-to-date with continuing education with the production of the annual retraining certificate;
- communicate a medical certificate of non-contradictory to the teaching and practice of surfing, less than three months old;
- meet the requirements of the pedagogical situation: to be able to assess the objective risks associated with the activity for the practitioner, to anticipate the potential risks for the practitioner, to master the behavior and the actions to be carried out In the event of an incident or accident, assess objective risks related to the practice environment and prevent risky behaviours;
- communicate the certificate, issued by a person with a diploma conferring the title of lifeguard, of success to a 100-metre freestyle course with a dive start and recovery of an underwater object at a depth of two metres;
- produce a certificate of success in an event consisting of a performance evaluated by the national technical directorate of the French Surfing Federation according to the criteria of judgment in force in competition, carried out on a support to the choice of the candidate shortboard, longboard or bodyboarding;
- produce a certificate of success on a minimum 400-metre sea-level swim from the edge, including one or more bar crossings and a life-saving action with a board. The terms of this route are defined by the jury according to the sea conditions;
- produce a certificate of success in an interview conducted from a file given to the jury by the candidate at the beginning of the test. This file traces an experience, as attested by the national technical director of surfing, in team coordination, teaching, training or training.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to articles 4, 6 and following of the decree of 27 April 2007 creating the word "surf" of DEJEPS, a speciality of sports development.

##### DESJEPS specialty sports performance mention surfing

The DESJEPS is a degree certified at level 2 (bac 3/4). It is prepared as an alternating in initial training, apprenticeship or continuing education through the VAE.

###### Conditions for access to training leading to the DESJEPS "surfing" designation

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age on the day of entry into training;
- certify training at the PSE1 teaching unit, or its equivalent, up-to-date with continuing education with the production of the annual retraining certificate;
- communicate a medical certificate of non-contradictory to the teaching and practice of surfing, less than three months old;
- meet the requirements of the pedagogical situation: to be able to assess the objective risks associated with the activity for the practitioner, to anticipate the potential risks he incurs, to master the behavior and the actions to be carried out in case incident or accident, assess objective risks associated with the practice environment, and prevent risky behaviours;
- produce a certificate, issued by a person with a diploma conferring the title of lifeguard, of success in a 100-metre freestyle course with a dive start and recovery of a submerged object at a depth of 2 metres;
- produce a certificate of success, issued by the National Technical Director of the French Surfing Federation, of an oral test based on the analysis of a video document. This test assesses the candidate's ability to observe, analyze and diagnose to design a surfer's training;
- pass an event consisting of a performance evaluated by the national technical directorate of the French Surfing Federation according to the judgments in force in competition and carried out on the support chosen by the candidate from the shortboard, the longboard and the bodyboard;
- complete an event consisting of a minimum 400-metre sea course from the edge, including one or more bar crossings and a rescue action with a board. The terms of this test are defined by the jury according to sea conditions;
- pass an interview from a file submitted by the candidate to the jury at the beginning of the test, recounting an experience in team coordination, teaching, training or training. The experience in question is attested by the national technical director of surfing.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to articles 4, 6 and following of the decree of 27 April 2007 creating the "surf" designation of the SPECIALty "sports performance" DESJEPS.

#### Costs associated with qualification

Trainings leading to the specialty "water activities" PJEPS, the deJEPS mentioning "surfing" or the DESJEPS mentioning "surfing" are paid. Their costs vary depending on the training organization. For [more details](http://foromes.calendrier.sports.gouv.fr/#/formation), it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*For further information*: Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*For further information*: Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as a sea surf instructor in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*For further information*: Article L. 212-9 of the Code of Sport.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Advance Declaration/Professional Card

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

#### Competent authority

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the [official website](https://eaps.sports.gouv.fr).

#### Time

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

#### Supporting documents

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

#### Cost

Free.

*For further information*: Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

### b. EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

#### Competent authority

The prior declaration of activity should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP) of the department where the declarer wants to perform his performance.

**Timeframe**

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

#### Supporting documents

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*For further information*: Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### c. EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

#### Competent authority

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

#### Time

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

#### Supporting documents for the first declaration of activity

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

#### Evidence for a renewal of activity declaration

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*For further information*: Articles R. 212-88 to R. 212-91, A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

### d. Compensation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*For further information*: Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

