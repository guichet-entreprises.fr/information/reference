﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP162" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Lifeguard" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="lifeguard" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/lifeguard.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="lifeguard" -->
<!-- var(translation)="Auto" -->


Lifeguard
=========

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The lifeguard is a professional responsible for ensuring the physical and health safety of swimmers. He or she may be able to rescue a swimmer in difficulty at any time, apply resuscitation techniques and implement first aid, if necessary.

The lifeguard also designs and conducts actions of awakening, discovery, multidisciplinary learning and teaching of codified swimming swimming. It organizes the safety of water activities and practitioners, and manages a rescue station. It has to intervene with all audiences (from very young children to seniors).

*To go further* : Appendix I of the amended November 8, 2010 decree creating the specialty "aquatic activities and swimming" of the professional certificate for youth, popular education and sport.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Lifeguard activity is subject to the application of Article L. 212-1 of the Code of Sport, which requires specific certifications, including the Professional Youth, Popular Education and Sport ( BPJEPS) or the State Diploma of Youth, Popular Education and Sport (DEJEPS).

As a sports teacher, the lifeguard must hold a diploma, a professional title (accompanied by a certificate of specialization "rescue and safety in aquatic environments"):

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*To go further* Articles L. 212-1 and R. 212-84 of the Code of Sport.

#### Training

The lifeguard must hold the specialty "aquatic activities and swimming" PJEPS and the Certificate of Specialization (CS) "Water Rescue and Safety." This diploma is classified as a level IV, i.e. a bachelor's degree. It attests to the possession of professional skills essential to the practice of the profession of animator in the field of the specialty concerned.

Holders of one of the DEJEPS sports development mention "swimming race," "synchronized swimming," "diving" or "water polo" may also practice as a lifeguard provided they are also holders of the Aquatic Rescue and Safety CS. This diploma is classified as Level III, i.e. bac level 2.

In all cases, the diploma (BPJEPS or DEJEPS) can be obtained through an apprenticeship contract, continuing education, a professionalization contract or validation of experience (VAE). For more information, you can see[official website](http://www.vae.gouv.fr/) VAE.

**Conditions for access to training leading to the "aquatic activities and swimming" PJEPS**:

The person concerned must:

- Be of age
- Hold a Certificate of First Aid Training in Level 1 Teams (PSE1 or its equivalent), up-to-date with continuing education, with the production of the annual retraining certificate;
- pass the entrance exams in the training organization;
- produce a medical certificate, less than three months old, established in accordance with the model in Schedule III of the order of 8 November 2010 mentioned above;
- to meet sports events (pre-requirement tests) detailed in Schedule III of the November 8, 2010 order, which are as follows:- a sports performance test (go 800 metres in freestyle in less than 16 minutes). The success of this event is attested by the Regional Director of Youth, Sports and Social Cohesion,
  - three separate water rescue tests. For more details, it is advisable to refer to the order of 8 November 2010 mentioned above.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to Appendix V of the order of 8 November 2010 mentioned above.

Training for the profession of aquatic activities and swimming is normally alternated and lasts between nine and eighteen months.

*To go further* : decree of 8 November 2010 creating the speciality "aquatic activities and swimming" of the professional certificate of youth, popular education and sport and its annexes.

###### Conditions for access to training leading to the DEJEPS designation "swimming race"

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- produce level 1 first aid training (PSE1) or its equivalent, up-to-date training;
- produce a medical certificate of non-contradictory to the practice and teaching of "swimming race" less than three months old;
- pass the tests for access to training;
- meet the required educational requirements: to be able to assess the risks associated with the practice of the discipline, to rescue the practitioner and to present the federal sports program in "swimming race", the concepts of the swimmer's career plan and the fundamental principles of training. These skills are checked during the conduct of a 20-minute sports training session followed by a maximum of 30 minutes of interviewing;
- produce the certificate of success, issued by the technical director of swimming, to the safety test carried out over a distance of 50 meters (without swimming goggles, nose clip or use of the ladder) which consists of a free start from the edge of the pool, a 25-metre free-swimming course, a "duck" dive with a search for a submerged regulatory dummy 25 metres from the starting point at a depth of between 1.80 and 3 metres, the dummy's ascent to the surface, a towing of a person for a distance of 25 metres to the edge of the pool and then to the victim's exit;
- to produce the certificate of success in one of the three tests (the contents of which are detailed in Article 3 of the decree of 15 March 2010 above) to justify a technical level and a mastery of the competition environment in "swimming race", " swimming with fins" or "handisport swimming";
- to produce the certificate of success, issued by the National Technical Director of Swimming or by the National Technical Director handisport, to the 400-metre medley test carried out in accord with the rules of the International Swimming Federation or, for persons with a disability under the French Handisport Federation, according to the rules, adapted, of the International Swimming Federation;
- produce the certificate of success, issued by the National Technical Director of Swimming or the National Technical Director of Studies and Underwater Sports, to one of the two performance tests corresponding to the time schedule defined as the march 15, 2010 order on "swimming race" or "swimming with fins";
- produce a certificate, issued by the National Technical Director of Swimming, the National Technical Director or, failing that, by the president of a federation that is a member of the Interfederal Council of Aquatic Activities in agreement with the Federation French swimming, justifying an educational experience (volunteer or professional) in swimming 800 hours, either in a club of an accredited sports federation or within a pole on the list drawn up by the Minister responsible for Under Section R. 221-26 of the Code of Sport for a minimum of three years in the last five years prior to entry into training.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more details, it is advisable to refer to articles 4, 6 and following of the decree of 15 March 2010 mentioned above.

**Conditions for access to training leading to deJEPS designation "synchronized swimming":**

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- produce level 1 first aid training (PSE1) or its equivalent, up-to-date training;
- produce a medical certificate of non-contradictory to the practice and teaching of synchronized swimming, less than three months old;
- pass the tests for access to training;
- meet the required educational requirements: to be able to assess the risks associated with the practice of the discipline, to rescue the practitioner and to present the federal sports program in synchronized swimming, the concepts of career plans and the fundamental principles of training. These skills are checked during the conduct of a 20-minute sports training session, followed by a maximum of 30 minutes of interviewing;
- produce the certificate of success, issued by the technical director of swimming, to the safety test carried out over a distance of 50 meters (without swimming goggles, nose clip or use of the ladder) which consists of a free start from the edge of the pool, a 25-metre free-swimming course, a "duck" dive with a search for a submerged regulatory dummy 25 metres from the starting point at a depth of between 1.80 and 3 metres, the dummy's ascent to the surface, a towing of a person for a distance of 25 metres to the edge of the pool and then to the victim's exit from the water;
- produce the certificate of success, issued by the National Technical Director of Swimming, of a course justifying a technical level corresponding to the skills targeted in the "synchronized swimming" competition pass;
- produce the certificate of success, issued by the National Technical Director of Swimming, to a test corresponding to a technical solo lasting one minute and 30 seconds including technical elements carried out according to the rules of the Federation International Swimming in an established order (for more details on the content of this test, it is advisable to refer to Article 3 of the decree of 15 March 2010 above);
- produce the certificate of success, issued by the National Technical Director of Swimming, to a test comprising four imposed figures, as defined according to the rules of the International Swimming Federation:- Figure 420: back walk,
  - Figure 355 e: 360-degree spin porpoise,
  - Figure 301 d: barracuda spins 180 degrees,
  - Figure 140: flamenco bent leg;
- produce the certificate, issued either by the National Technical Director of Swimming for structures affiliated with the French Swimming Federation, or by the National Technical Director or, failing that, by the president of a member federation of the Interfederal council of aquatic activities in agreement with the French Swimming Federation for their affiliated structures, justifying an educational experience (volunteer or professional) in synchronized swimming of 800 hours either within an approved sports federation club or within a pole on the list drawn up by the Minister for Sport under Article R. 221-26 of the Code of Sport for a minimum of three years in the last five years preceding entry into training.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more details, it is advisable to refer to articles 4, 6 and following of the decree of 15 March 2010 mentioned above.

**Conditions for access to training leading to the DEJEPS "diving" designation:**

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- produce level 1 first aid training (PSE1) or its equivalent, up-to-date training;
- produce a medical certificate of non-contradictory to the practice and teaching of diving, less than three months old;
- pass the tests for access to training;
- meet the required educational requirements: to be able to anticipate the risks associated with the practice of the discipline, to rescue the practitioner and to present the federal diving sports program, the diver's career plan concepts and the fundamental principles of training. These skills are checked during the conduct of a sports training session lasting 1 hour and 15 minutes, followed by an interview of up to 45 minutes;
- produce the certificate of success, issued by the technical director of swimming, to the safety test No. 1 carried out over a distance of 50 meters (without swimming goggles, nose clip or use of the ladder) which consists of a free start from the edge of the pool , a 25-metre free-swimming course, a "duck" dive with a search for a submerged regulatory dummy 25 metres from the starting point at a depth between 1.80 and 3 metres, the dummy's ascent to the surface, a towing of a person for a distance of 25 metres to the edge of the pool and then to the victim's exit from the water;
- produce the certificate of success, issued by the Technical Director of Swimming, to the safety test No. 2 including the lift of a submerged regulatory dummy at a depth of between 3 and 5 meters (without using swimming goggles or swimming goggles, or nose clip)
- produce the certificate of success, issued by the National Technical Director of Swimming, to carry out five exercises carried out in the 1 metre springboard, a level corresponding to the skills covered in the "diving" competition pass issued by the French Swimming Federation;
- produce a certificate, issued by the National Technical Director of Swimming, justifying an official practice in diving competition;
- produce a certificate, issued by the National Technical Director of Swimming, justifying a demonstration of four dives at 1 meter and two dives taken at 3 meters according to the rules of the International Swimming Federation ( For more details on the content of this demonstration, it is advisable to refer to Article 3 of the decree of 15 March 2010 above);
- produce a certificate, issued either by the National Technical Director of Swimming for structures affiliated with the French Swimming Federation or by the national technical director or, failing that, by the president of a member federation of the interfederal advice of aquatic activities in agreement with the French Swimming Federation for their affiliated structures, justifying a 500-hour diving pedagogical experience (volunteer or professional), or within a club of an accredited sports federation either within a pole on the list drawn up by the Minister for Sport under Article R. 221-26 of the Code of Sport, lasting a minimum of three years in the five years prior to entering training .

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more details, it is advisable to refer to articles 4, 6 and following of the decree of 15 March 2010 mentioned above.

**Conditions for access to training leading to the DEJEPS "water polo" designation:**

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- Hold the First Aid Certificate in a Level 1 team (or its equivalent) on the day of continuing education;
- pass the tests for access to training;
- communicate a medical certificate of non-contradictory to the teaching and practice of water polo, less than three months old;
- meet the required pedagogical requirements: be able to assess the objective risks associated with the practice of the discipline, anticipate potential risks to the practitioner and master the behaviour and actions to be carried out in the event of an incident or accident. These skills are checked in a 30-minute session, followed by a 20-minute interview;
- be able to rescue the water polo player, present the federal sports program and the polo player's career plan concepts. These skills are checked during the conduct of a 20-minute water polo sports development session, followed by a maximum of 30 minutes of interview;
- produce the certificate of success, issued by the technical director of swimming, to the safety test No. 1 carried out over a distance of 50 meters (without swimming goggles, nose clip or use of the ladder) which consists of a free start from the edge of the pool , a 25-metre free-swimming course, a "duck" dive with a search for a submerged regulatory dummy 25 metres from the starting point at a depth between 1.80 and 3 metres, the dummy's ascent to the surface, a towing of a person for a distance of 25 metres to the edge of the pool and then to the victim's exit from the water;
- to carry out a shooting course justifying a technical level corresponding to the skills targeted in the "water polo" competition pass issued by the French Swimming Federation. The success of this test is the subject of a certificate issued by the National Technical Director of Swimming;
- produce a certificate of experience of a minimum water polo practice over three sports seasons within a team equivalent to the "national 3," "national 2" or "national 1" level and justifying effective participation in the field of 10 games per season. This certificate is issued by the National Technical Director of Swimming;
- produce a certificate of educational experience (volunteer or professional) in water polo of 800 hours either within a club of an accredited sports federation or within a pole on the list drawn up by the Minister responsible for sports in Article R. 221-26 of the Code of Sport, over a minimum of three years over the past five years. This certification is established by the national technical director of swimming for structures affiliated with the French Swimming Federation or by the national technical director or, failing that, by the president of a federation that is a member of the board. interfederal aquatic activities in agreement with the French Swimming Federation for their affiliated structures.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more details, it is advisable to refer to articles 4, 6 and following of the order of 15 March 2010 mentioned above.

**Conditions for access to training leading to the CS "Water Rescue and Safety" for PJEPS "Swimming Aquatic Activities" or for DEJEPS holders mentioning "swimming race," "synchronized swimming," " "diving" or "water polo"**  :

Individuals who hold a "water and swimming activities" or a specialty DEJEPS "swimming course," "synchronized swimming," "diving" or "water polo" must also hold the CS "Water Rescue and Safety" to be able to practice as a lifeguard.

Conditions for access to training leading to the Certificate of Specialization:

- produce a medical certificate, less than three months old, attesting to the physical skills associated with the practice of water rescue, the model of which is attached to the march 15, 2010 decree establishing the CS;
- produce level 1 first aid training (PSE1) or its equivalent, up-to-date training;
- produce the certificates of success in three technical tests described in Schedule IV of the March 15, 2010 order consisting of:- a 100-metre continuous water course in a swimming pool,
  - a lifeline with fins, mask and snorkel, continuous, 250 meters, in swimming pool,
  - an ordeal of rescuing a person in an aquatic environment.

Some training organizations may require the release of other documents upon registration (such as a census certificate). For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events. For more details, it is advisable to refer to Articles 7 and 8 of the decree of 15 March 2010 mentioned above.

*To go further* : decree of March 15, 2010 creating the certificate of specialization "rescue and safety in aquatic environments" associated with the specialty BPJEPS "aquatic activities and swimming" and deJEPS specialty sports development (...).

**Good to know**

People with the Senior State Diploma of Youth, Popular Education and Sport (DESJEPS) specialty sports performance can also practice as a lifeguard. This Level II state diploma is issued by the Regional Director of Youth and Sports. To practice in the field, he will opt for one of the "swimming race," "synchronized swimming," "diving" or "water polo" mentions of the DESJEPS and will also have to hold the CS "Water Rescue and Safety".[More information](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/des-jeps/Reglementation-11081/La-specialite-performance-sportive-du-DES-JEPS-et-les-mentions-unites-capitalisables-complementaire-et-certificats-de-specialisation-s-y-rapportant/) on admission requirements and training leading up to graduation.

*To go further* Articles D. 212-35 and following of the Code of Sport, ordered on 20 November 2006 organising the State Diploma of Youth, Popular Education and Specialty Sport "Sports Development" issued by the Ministry for Youth and Youth 1 July 2008.

###### The Lifeguard Certificate (CAEPMNS)

The Lifeguard Certificate (CAEPMNS) certifies that persons with a lifeguard diploma continue to provide sufficient assurance of lifeguard water rescue and public safety.

The ability to practice as a lifeguard must be assessed and checked periodically. This is the purpose of obtaining or renewing the CAEPMNS.

In principle, the fitness to practice the profession is verified before December 31 of the fifth year after graduation conferring the title of lifeguard (i.e. BPJEPS, DEJEPS or DESJEPS accompanied by the CS "rescue and safety in the aquatic environment") or the issuance of the last certificate of fitness to practice as a lifeguard.

###### Competent authority

The CAEPMNS is delivered by the Regional Director of Youth, Sports and Social Cohesion or by the Director of Youth, Sports and Social Cohesion, following a training session followed by an evaluation.

###### CaEPMNS validity

The CAEPMNS is issued for a period of five years. However, in the case of a legitimate reason, the validity of the certificate may be extended for up to four months, i.e. until April 30 of the following year. It is the regional director of youth, sports and social cohesion or the director of youth, sports and social cohesion who decides, if necessary, to extend the validity of the CAEPMNS.

###### CAEPMNS effective date

The validity of the CAEPMNS runs from January 1 of the year following its issuance. However, if registration for the training session occurs after the expiry of the validity period of the previous certificate, the validity period runs from the date of issuance.

###### Registration requirements for caEPMNS training

To obtain CAEPMNS, the candidate must register for a training session, followed by an evaluation. The registration file must include:

- A free-to-do application for registration;
- The completed copy of the registration file prepared by the training organization accompanied by a photo ID;
- Photocopy of the person's identity document
- a photocopy of the diploma conferring the title of lifeguard (BPJEPS, DEJEPS or DESJEPS, accompanied by the CS);
- a photocopy of the "Level 1 team first aid" certificate or its equivalent, with a photocopy of the annual continuing education certificate;
- a medical certificate of non-contradictory to the profession of lifeguard, less than three months old, based on the model in Appendix II of the order of 23 October 2015 relating to the certificate of fitness to exercise lifeguard profession;
- photocopy of the latest CAEPMNS, if any.

Some additional parts may be requested (freed envelopes... For more information, it is advisable to check with the training centre in question.

The file must be filed with the session organizer, at least two months before the start date of the session.

###### Terms of the training and evaluation session leading to the completion or renewal of CAEPMNS

Training up to 21 hours. It aims to maintain skills and acquire new knowledge related to the evolution of the profession. At the end of the training session, an evaluation is organised. It includes two tests, performed by the candidate wearing t-shirt and shorts (wearing the suit, pool goggles, mask, nose clip or any other equipment being prohibited):

- a free swim event with fins carried out continuously, over a distance of 250 metres;
- a rescue course, the details of which are specified in Article 7 of the decree of 23 October 2015 above.

*To go further* : order of October 23, 2015 relating to the certificate of fitness to practice the profession of lifeguard.

#### Costs associated with qualification

Training for BPJEPS, DEJEPS or DESJEPS is paid for and the cost varies depending on the reference chosen and the training organization. Similarly, the training leading to obtaining the CAEPMNS is paid (about 200 euros, as an indication). For [more details](https://foromes.calendrier.sports.gouv.fr/#/formation), it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as a lifeguard in France for persons who have been convicted of any crime or for one of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

4°. Reporting requirement (for the purpose of obtaining the professional sports educator card)
------------------------------------------------------------------------------------------------------------------

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

#### Competent authority

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the [official website](https://eaps.sports.gouv.fr).

#### Time

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

#### Supporting documents

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

#### Cost

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

5°. Qualifications process and formalities
----------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

#### Competent authority

The prior declaration of activity should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP) of the department where the declarer wants to perform his performance.

#### Time

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

#### Supporting documents

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

#### Competent authority

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

#### Time

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

#### Supporting documents for the first declaration of activity

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

#### Evidence for a renewal of activity declaration

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

**Good to know: compensation measures**

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

