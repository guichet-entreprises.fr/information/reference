﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP029" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Specialised sports instructor for health and motor functions" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="specialised-sports-instructor-for-health-and-motor-functions" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/specialised-sports-instructor-for-health-and-motor-functions.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="specialised-sports-instructor-for-health-and-motor-functions" -->
<!-- var(translation)="Auto" -->


Specialised sports instructor for health and motor functions
================================================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The health and motor education sports facilitator is a professional in sports animation whose activity is to supervise and facilitate physical and sports activities in order to improve motor skills (voluntary movements and automatic body), the health and well-being of practitioners. This activity can be done as usual, seasonal or occasional.

*For further information*: Article L. 212-1 of the Code of Sport.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of sports health and motor education facilitator, the professional must be:

- professionally qualified. To do so, he must hold a diploma, a professional title or a certificate of qualification:- to justify its safety skills of practitioners and third parties,
  - registered in the national directory of professional certifications ([RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/)) ;
- to make a declaration of his activity in order to obtain a professional sports educator card (see infra "5°. a. Declaration of activity in order to obtain the professional sports educator card").

**Please note**

The professional who is being trained to obtain one of the qualifying diplomas may also perform this activity.

*For further information*: Article L. 212-1 of the Code of Sport.

#### Training

To be recognized as a professionally qualified, the professional must hold a bachelor's degree in science and technology in physical and sports activities (STAPS) marked "Education and Motor Skills".

This three-year course is accessible:

- after a training course as a student;
- Apprenticeships
- after a continuous training course;
- professionalised;
- individual application or through the experience validation process (VAE). For more information, you can see[VAE's official website](http://www.vae.gouv.fr/).

**Good to know**

The list of institutions delivering this training is available on the[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

*For further information*: Article A. 212-1 and Appendix II-1 of the Code of Sport.

#### Costs associated with qualification

Training leading to the profession of health and motor education sports facilitator is paid and its cost varies according to the course envisaged. For more information, it is advisable to get close to the institutions concerned.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a European Union (EU) state or a member of the European Economic Area (EEA) agreement who is involved in the sport health and motor education activities may perform the same activity on a temporary and casual basis. France.

Where neither access to the activity nor its exercise is regulated in the Member State, the professional must justify having exercised it for at least one year in the last ten years.

The professional must, before his first service delivery, make a prior declaration with the prefect of the department in which he wishes to practice (see infra "5°. c. Pre-declaration for the EU national for a temporary and casual exercise (LPS)").

*For further information*: Article L. 212-7 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any freely established EU or EEA national who is a health and motor education facilitator may carry out this activity in France on a permanent basis.

To do so, the national must either:

- Have a certificate of competency or a training certificate allowing it to carry out this activity in a Member State that regulates its access and exercise;
- Where the Member State does not regulate access to the activity or its training, justify having carried out this activity for at least one year in the last ten years;
- be the holder of a training degree issued by a Member State which does not regulate access to the profession or its exercise allowing it to carry out all or part of this activity and sanctioning regulated training consisting of a cycle of study and a work experience (training, internship or professional practice)
- to hold a training certificate issued by a third state but admitted in equivalence in a Member State which regulates access to the activity or its exercise, and to justify having carried out this activity for at least two years in that Member State.

Once the national fulfils one of these conditions, he or she will have to make a prior declaration of his activity with the prefect of the department in which he wishes to practice as principal (see infra "5o). b. Pre-declaration for EU national for permanent exercise (LE)").

*For further information*: Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

No one may act as a facilitator if he or she is or has been convicted of a felony or misdemeanor for:

- an attack on the person (physical integrity, life, freedoms, endangerment);
- extortion or embezzlement;
- against the state, the nation and public peace;
- Drug use
- possession of weapons or ammunition in violation of Title I of Book III of the Internal Security Code;
- endangering the health and physical or moral safety of practitioners;
- The use of doping substances
- illegal introduction of alcoholic beverages into a sports venue during a sporting event.

**Please note**

If the title of sports facilitator or educator is used in ignorance of these provisions, the professional faces a one-year prison sentence and a fine of 15,000 euros.

*For further information*: Articles L. 212-9 and L. 212-10 of the Code of Sport.

4°. Sanctions
---------------------------------

The professional faces a one-year prison sentence and a fine of 15,000 euros if he:

- does business without being qualified professionally;
- employs an unqualified person or an EU national who is unaware of the provisions relating to the temporary or occasional and permanent exercise of this activity in France (see supra "2°. Professional qualifications").

*For further information*: Article L. 212-8 of the Code of Sport.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Statement of activity to obtain professional sports educator card

**Competent authority**

The professional must submit his request to the prefect of the department in which he wishes to carry out his activity.

**Supporting documents**

His application must include:

- The[Exercise reporting form](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=29D52BEE6C97415E0338E1B83C12C433.tplgfr21s_1?idArticle=LEGIARTI000032674633&cidTexte=LEGITEXT000006071318&dateTexte=20180409) Schedule II-12 of the Code of Sport;
- A copy of his ID
- An identity photograph
- A statement on honour certifying the accuracy of the information provided;
- A copy of all of his diplomas, titles or certificates of qualification;
- if necessary, an authorization to practice, the equivalence of his diploma or a certificate justifying that he has the minimum knowledge to practice;
- a medical certificate of non-contradictory to the practice and supervision of a physical or sporting activity dating back less than one year.

**Delays and procedures**

The prefect acknowledges receipt of his request within one month. He then issues a professional sports educator card to the professional.

**Please note**

This declaration must be renewed every five years. To do so, he will have to send a medical certificate of non-contradictory presented during the declaration less than one year old.

*For further information*: Articles L. 212-11 and R. 212-85, and Articles A. 212-176 and following of the Code of Sport.

### b. Pre-declaration for EU national for permanent exercise (LE)

**Competent authority**

The professional must submit his request to the prefect of the department in which he wishes to carry out his activity in the main capacity.

**Supporting documents**

His application must include:

- The[Activity reporting form](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=29D52BEE6C97415E0338E1B83C12C433.tplgfr21s_1?idArticle=LEGIARTI000036153612&cidTexte=LEGITEXT000006071318&dateTexte=20180409) modeled in Schedule II-12-2 a. of the Code of Sport and the supporting documents mentioned.

**Procedure and deadlines**

The prefect acknowledges receipt of the request within one month of receiving it. After acknowledging receipt of his request, the prefect issues him a professional sports educator card stating the conditions of his activity.

**Please note**

This declaration must be renewed by the national every five years. In order to do this, the professional must refer the[renewal application form](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=29D52BEE6C97415E0338E1B83C12C433.tplgfr21s_1?idArticle=LEGIARTI000036153622&cidTexte=LEGITEXT000006071318&dateTexte=20180409) and the supporting documents mentioned in this document.

*For further information*: Articles R. 212-88 and A. 212-182 of the Code of Sport.

**Good to know: compensation measures**

Where there is a substantial difference between the training received by the professional and that required to carry out this activity in France, the prefect may, after the advice of a qualification recognition commission, decide to submit it to a aptitude test or an adjustment course of up to three years.

*For further information*: Article R. 212-90-1 of the Code of Sport.

### c. Pre-declaration for EU national for temporary and casual exercise (LPS)

**Competent authority**

The national must apply to the prefect of the department where he wishes to carry out most of his activity.

**Supporting documents**

His application must include:

- The[Activity reporting form](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=29D52BEE6C97415E0338E1B83C12C433.tplgfr21s_1?idArticle=LEGIARTI000036153628&cidTexte=LEGITEXT000006071318&dateTexte=20180409), filled and signed;
- all the supporting documents mentioned in this form.

**Delays and procedures**

The prefect acknowledges receipt of his request within one month. In the absence of a response from the prefect beyond two months, the free provision of services may begin.

**Good to know**

In case of serious doubt about the national's level of knowledge of French, the prefect may decide that he submits to a check in order to allow a safe exercise of physical and sporting activities and to verify his ability to alert Rescue.

**Please note**

Its declaration must be renewed annually.

*For further information*: Articles R. 212-92 to R. 212-94 and A.212-182-2 of the Code of Sport.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

