﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP183" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Moniteur de ski alpin" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="moniteur-de-ski-alpin" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sport/moniteur-de-ski-alpin.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="moniteur-de-ski-alpin" -->
<!-- var(translation)="None" -->

# Moniteur de ski alpin

Dernière mise à jour : <!-- begin-var(last-update) -->Janvier 2024<!-- end-var -->

## Définition de la profession <!-- collapsable:open -->

Le moniteur national de ski alpin encadre tous types de public dans la pratique du ski alpin et des activités assimilées (notamment le snowboard) dans l'ensemble des classes de la progression du ski alpin.

Il peut exercer sur le domaine sécurisé des pistes et hors des pistes, à l'exception des zones glaciaires non balisées et des terrains dont la fréquentation fait appel aux techniques de l'alpinisme.

## Qualifications professionnelles requises en France <!-- collapsable:open -->

L'activité de moniteur de ski alpin est soumise à l'application de l'article L. 212-1 du Code du sport qui exige l'obtention de certifications spécifiques.

En qualité d'enseignant du sport, le moniteur de ski alpin doit être titulaire d'un diplôme, d'un titre à finalité professionnelle ou d'un certificat de qualification :

- garantissant sa compétence en matière de sécurité des pratiquants et des tiers dans l'activité physique ou sportive considérée ;
- enregistré au [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

Les titres permettant d'exercer comme moniteur de ski sont le diplôme d'État de ski-moniteur national de ski alpin et le diplôme d'État de ski-moniteur national de ski alpin spécialisé en entraînement. Le diplôme d'État d'alpinisme-guide de haute montagne permet également à son titulaire de conduire et d'accompagner des personnes dans des excursions de ski alpinisme et de ski hors-piste et d'enseigner les techniques de ski. Pour plus d'informations sur ce dernier diplôme, il est recommandé de consulter la fiche [Guide de haute montagne](https://www.guichet-qualifications.fr/fr/dqp/sport/guide-de-haute-montagne.html).

Les diplômes étrangers peuvent être admis en équivalence aux diplômes français par le ministre chargé des sports, après avis de la commission de reconnaissance des qualifications placée auprès du ministre.

La pratique du ski alpin constitue une activité s'exerçant dans un environnement spécifique impliquant le respect de mesures de sécurité particulières. Par conséquent, seuls les organismes sous la tutelle du ministère des Sports peuvent former les futurs professionnels.

Le diplôme d'État de ski-moniteur national de ski alpin est enregistré au niveau III du [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

Ce diplôme est délivré à l'issue d'un cursus comprenant un cycle général commun aux métiers d'enseignement, d'encadrement et d'entraînement des sports de montagne et une formation spécifique au ski alpin. Les programmes respectent le principe de l'alternance fondé sur l'articulation de périodes d'apprentissage en centre de formation et de mise en situation professionnelle sous tutorat pédagogique.

## Particularités de la réglementation de la profession <!-- collapsable:open -->

### Conditions d'honorabilité

Il est interdit d'exercer en tant qu'éducateur sportif en France pour les personnes ayant fait l'objet d'une condamnation pour tout crime ou pour l'un des délits suivants :

- torture et actes de barbarie ;
- agressions sexuelles ;
- trafic de stupéfiants ;
- mise en danger d'autrui ;
- proxénétisme et infractions qui en résultent ;
- mise en péril des mineurs ;
- usage illicite de substance ou plante classées comme stupéfiants ou provocation à l'usage illicite de stupéfiants ;
- infractions prévues aux articles L. 235-25 à L. 235-28 du Code du sport ;
- à titre de peine complémentaire à une infraction en matière fiscale : condamnation à une interdiction temporaire d'exercer, directement ou par personne interposée, pour son compte ou le compte d'autrui, toute profession industrielle, commerciale ou libérale (article 1750 du Code général des impôts).

De plus, nul ne peut enseigner, animer ou encadrer une activité physique ou sportive auprès de mineurs, s'il a fait l'objet d'une mesure administrative d'interdiction de participer, à quelque titre que ce soit, à la direction et à l'encadrement d'institutions et d'organismes soumis aux dispositions législatives ou réglementaires relatives à la protection des mineurs accueillis en centre de vacances et de loisirs, ainsi que de groupements de jeunesse, ou s'il a fait l'objet d'une mesure administrative de suspension de ces mêmes fonctions.

#### A noter

En cas d'usage du titre d'éducateur sportif en méconnaissance de ces dispositions, le professionnel encourt une peine d'un an d'emprisonnement et de 15 000 euros d'amende.

Pour aller plus loin : articles L. 212-9 et L. 212-10 du Code du sport.

### Sanctions

Le professionnel encourt une peine d'un an d'emprisonnement et de 15 000 euros d'amende dès lors qu'il :

- exerce son activité sans être qualifié professionnellement ;
- emploie une personne qui n'est pas qualifiée ou un ressortissant de l'UE en méconnaissances des dispositions relatives à l'exercice temporaire ou occasionnel et permanent de cette activité en France (cf. supra « Qualifications professionnelles »).

Pour aller plus loin : article L. 212-8 du Code du sport.

### Aptitude physique

Le demandeur doit fournir un certificat médical de non contre-indication à la pratique et à l'enseignement du ski datant de moins d'un an à la date de clôture de la première inscription.

### Obligation de formation professionnelle continue

Les titulaires du diplôme d'État de ski-moniteur national de ski alpin sont soumis tous les six ans à un stage de recyclage organisé par l'École nationale des sports de montagne, site de l'ENSA. Le recyclage doit intervenir avant le 31 décembre de la sixième année suivant l'obtention du diplôme ou du dernier recyclage.

### Activité réglementée

Toute personne souhaitant exercer la profession de moniteur de ski régie par l'article L. 212-1 du Code du sport, doit déclarer son activité au préfet de l'Isère. Cette déclaration déclenche l'obtention d'une carte professionnelle. La déclaration doit être renouvelée tous les cinq ans. 

## Démarches de reconnaissance de qualification professionnelle <!-- collapsable:open -->

### Libre Établissement (exercice stable et continu)

Les ressortissants de l'UE ou de l'EEE légalement établis dans l'un de ces États et souhaitant exercer en France de manière permanente doivent effectuer une demande de reconnaissance de qualification professionnelle.

Tout ressortissant de l'UE ou de l'EEE qualifié pour y exercer tout ou partie des activités mentionnées à l'article L. 212-1 du Code du sport, et souhaitant s'établir en France, doit en faire préalablement la déclaration au service national des métiers de l'encadrement du ski et de l'alpinisme. Cette déclaration permet au déclarant d'obtenir par le préfet de l'Isère une carte professionnelle et ainsi d'exercer en toute légalité en France dans les mêmes conditions que les ressortissants français. La déclaration doit être renouvelée tous les cinq ans.

En cas de différence substantielle avec la qualification requise en France, le service national des métiers de l'encadrement du ski et de l'alpinisme peut saisir, pour avis, la commission de reconnaissance des qualifications placée auprès du ministre chargé des sports. Il peut aussi décider de soumettre le ressortissant à une épreuve d'aptitude ou à un stage d'adaptation (cf. infra « Bon à savoir : mesures de compensation »).

#### Autorité compétente

- En ligne sur l'[application Arquedi](https://www.arquedi.sports.gouv.fr/) (sports.gouv.fr), ou
- Préfet de l'Isère via le SDJES 38 **Service départemental à la jeunesse, à l'engagement et aux sports** (SDJES ISERE, Cité administrative,1 rue Joseph Chanrion - Bâtiment 2, 38000 GRENOBLE - Téléphone : 04.76.74.79.79 - Mail : [ddcs@isere.gouv.fr](ddcs@isere.gouv.fr))
- Service national des métiers de l'encadrement du ski et de l'alpinisme (Rectorat de l' Académie de Grenoble, P.N.M.E.S.A - 7 place Bir-Hakeim – CS 81065 - 38021 GRENOBLE cedex 1 - Téléphone : 04.76.74.70.00)

#### Procédure

Le service national des métiers de l'encadrement du ski et de l'alpinisme accuse réception de la demande dans un délai d'un mois suivant sa réception. Après avoir accusé réception de sa demande, et après avoir émis un avis favorable sur la reconnaissance des qualifications étrangères, le préfet lui délivre une carte professionnelle d'éducateur sportif mentionnant les conditions d'exercice de son activité. La décision du préfet de délivrer la carte professionnelle intervient dans un délai de trois mois à compter de la présentation du dossier complet par le déclarant. Ce délai peut être prorogé d'un mois sur décision motivée. Le service national des métiers de l'encadrement du ski et de l'alpinisme, en cas de différence substantielle peut décider de soumettre le déclarant à une mesure de compensation (épreuve d'aptitude ou stage), sa décision doit être motivée.

#### Pièces justificatives

Le dossier de déclaration d'activité doit contenir : 

- un exemplaire du formulaire de déclaration dont le modèle est fourni à l'annexe II-12-2-a du Code du sport ;
- une copie d'une pièce d'identité en cours de validité ;
- un certificat médical de non contre-indication à la pratique et à l'encadrement des activités physiques ou sportives, datant de moins d'un an (traduit par un traducteur agréé) ;
- une copie de l'attestation de compétences ou du titre de formation, accompagnée de documents décrivant le cursus de formation (programme, volume horaire, nature et durée des stages effectués), traduit en français par un traducteur agréé ;
- le cas échéant, une copie de toutes pièces justifiant de l'expérience professionnelle (traduites en français par un traducteur agréé) ;
- si le titre de formation a été obtenu dans un État tiers, les copies des pièces attestant que ce titre a été admis en équivalence dans un État de l'UE ou de l'EEE qui réglemente l'activité ;
- l'un des trois documents au choix (à défaut, un entretien sera organisé) :
  - une copie d'une attestation de qualification délivrée à l'issue d'une formation en français,
  - une copie d'une attestation de niveau en français délivré par une institution spécialisée,
  - une copie d'un document attestant d'une expérience professionnelle acquise en France,
- les documents attestant que le déclarant n'a pas fait l'objet, dans l'État membre d'origine, d'une des condamnations ou mesures mentionnées aux articles L. 212-9 et L. 212-13 du Code du sport (traduits en français par un traducteur agréé).

Le dossier de renouvellement de déclaration d'activité doit contenir:

- un exemplaire du formulaire de renouvellement de déclaration dont le modèle est fourni à l'annexe II-122-b du Code du sport ;
- un certificat médical de non contre-indication à la pratique et à l'encadrement des activités physiques ou sportives, datant de moins d'un an.

#### Délais de réponse

Le délai total maximum d'instruction à partir de la présentation du dossier de déclaration complet est de 3 mois. * Ce délai peut être prorogé d'1 mois, par décision motivée. 

Cette instruction se décompose comme suit :

**1.1** Délai maximum entre le dépôt de la déclaration et l'accusé de réception de la déclaration :

**1 mois**, Si le dossier est incomplet : demande d'informations complémentaires à transmettre dans un délai de 1 mois, à peine de voir le dossier de déclaré irrecevable (article R.212-88). 

**1.2** Délai maximum entre la présentation du dossier complet du déclarant et la saisine de la Commission de reconnaissance des qualifications, dans le cas où le préfet estime qu'il existe une différence substantielle : **1 mois**.

**1.3** Délai maximum dans lequel le déclarant doit faire connaître son choix entre l'épreuve d'aptitude et le stage d'adaptation: **1 mois**.

\* *L'avis de la commission doit être transmis au préfet dans le délai maximum d'1 mois.*

#### Coût

Gratuit.

#### Mesures de compensation

S'il existe une différence substantielle entre la qualification du requérant et celle requise en France pour exercer la même activité, le service national des métiers de l'encadrement du ski et de l'alpinisme saisit la commission de reconnaissance des qualifications, placée auprès du ministre chargé des sports. Cette commission saisit la section permanente du ski alpin de la commission de la formation et de l'emploi du Conseil supérieur des sports de montagne après examen et instruction du dossier. Elle émet, dans le mois de sa saisine, un avis qu'elle adresse au service national des métiers de l'encadrement du ski et de l'alpinisme.

Dans son avis, la commission peut :

- estimer qu'il existe effectivement une différence substantielle entre la qualification du déclarant et celle requise en France. Dans ce cas, la commission propose de soumettre le déclarant à une épreuve d'aptitude ou un stage d'adaptation. Elle définit la nature et les modalités précises de ces mesures de compensation (nature des épreuves, modalités de leur organisation et de leur évaluation, période d'organisation, contenu et durée du stage, types de structures pouvant accueillir le stagiaire, etc.) ;
- estimer qu'il n'y a pas de différence substantielle entre la qualification du déclarant et celle requise en France.

À réception de l'avis de la commission, le préfet notifie sa décision motivée au déclarant (il n'est pas obligé de suivre l'avis de la commission) :

- s'il exige qu'une mesure de compensation soit effectuée, le déclarant dispose d'un délai d'un mois pour choisir entre la ou les épreuve(s) d'aptitude et le stage d'adaptation. Le préfet délivre ensuite une carte professionnelle au déclarant qui a satisfait aux mesures de compensation. En revanche, si le stage ou l'épreuve d'aptitude ne sont pas satisfaisants, le préfet notifie sa décision motivée de refus de délivrance de la carte professionnelle à l'intéressé ;
- s'il n'exige pas de mesure de compensation, le préfet délivre une carte professionnelle à l'intéressé.

#### Voies de recours

Si refus de délivrance de la reconnaissance de qualification professionnelle, le demandeur peut initier un recours contentieux devant le tribunal administratif compétent dans les deux mois suivant la notification du refus. De même, si l'intéressé veut contester la décision de le soumettre à une mesure de compensation, il doit d'abord initier un recours gracieux auprès du service national des métiers de l'encadrement du ski et de l'alpinisme, dans les deux mois suivant la notification de la décision. S'il n'obtient pas gain de cause, il pourra alors opter pour un recours contentieux devant le tribunal administratif compétent. 

### Libre Prestation de Services (exercice temporaire et occasionnel)

Les ressortissants de l'UE ou de l'EEE légalement établis dans l'un de ces États et souhaitant exercer en France de manière temporaire ou occasionnelle doivent effectuer une déclaration préalable d'activité, avant la première prestation de services. Afin d'éviter des dommages graves pour la sécurité des bénéficiaires, le service national des métiers de l'encadrement du ski et de l'alpinisme peut, lors de la première prestation, procéder à une vérification préalable des qualifications professionnelles du prestataire. 

#### Autorité compétente

- En ligne sur l'[application Arquedi](https://www.arquedi.sports.gouv.fr/) (sports.gouv.fr), ou
- Préfet de l'Isère via le SDJES 38 **Service départemental à la jeunesse, à l'engagement et aux sports** (SDJES ISERE, Cité administrative,1 rue Joseph Chanrion - Bâtiment 2, 38000 GRENOBLE - Téléphone : 04.76.74.79.79 - Mail : [ddcs@isere.gouv.fr](ddcs@isere.gouv.fr))
- Service national des métiers de l'encadrement du ski et de l'alpinisme (Rectorat de l'Académie de Grenoble, P.N.M.E.S.A - 7 place Bir-Hakeim – CS 81065 - 38021 GRENOBLE cedex 1 - Téléphone : 04.76.74.70.00)

#### Procédure

Le service national des métiers de l'encadrement du ski et de l'alpinisme accuse réception de sa demande dans un délai d'un mois. En l'absence de réponse du service national des métiers de l'encadrement du ski et de l'alpinisme au-delà d'un délai de deux mois, la libre prestation de services peut débuter.

Dans le mois suivant la réception du dossier de déclaration et l'avis favorable du service national des métiers de l'encadrement du ski et de l'alpinisme sur la reconnaissance des qualifications étrangères, le préfet notifie au prestataire :

- soit une demande d'informations complémentaires (dans ce cas, le préfet dispose de deux mois pour donner sa réponse) ;
- soit un récépissé de déclaration de prestation de services s'il ne procède pas à la vérification des qualifications. Dans ce cas, la prestation de services peut débuter ;
- soit qu'il procède à la vérification des qualifications. Dans ce cas, le préfet délivre ensuite au prestataire un récépissé lui permettant de débuter sa prestation ou, si la vérification des qualifications fait apparaître des différences substantielles avec les qualifications professionnelles requises en France, le préfet soumet le prestataire à une épreuve d'aptitude (cf. infra « Bon à savoir : mesures de compensation »).

Dans tous les cas, en l'absence de réponse dans les délais précités, le prestataire est réputé exercer légalement son activité en France.


#### Pièces justificatives

Le dossier de déclaration préalable d'activité doit contenir :

- un exemplaire du formulaire de déclaration dont le modèle est fourni à l'[annexe II-12-3 du Code du sport](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000045058326#:~:text=3.,4.) (legifrance.gouv.fr) ;
- une copie d'une pièce d'identité ;
- une copie de l'attestation de compétences ou du titre de formation ;
- une copie des documents attestant que le déclarant est légalement établi dans l'État membre d'établissement et qu'il n'encourt aucune interdiction, même temporaire, d'exercer (traduits en français par un traducteur agréé) ;
- dans le cas où ni l'activité, ni la formation, conduisant à cette activité n'est réglementée dans l'État membre d'établissement, une copie de toutes pièces justifiant que le déclarant a exercé cette activité dans cet État pendant au moins l'équivalent de deux ans à temps complet au cours des dix dernières années (traduites en français par un traducteur agréé) ;
- l'un des trois documents au choix (à défaut, un entretien sera organisé):
  - une copie d'une attestation de qualification délivrée à l'issue d'une formation en français,
  - une copie d'une attestation de niveau en français délivré par une institution spécialisée,
  - une copie d'un document attestant d'une expérience professionnelle acquise en France.

#### Délais de réponse

Le délai total maximum d'instruction à partir de la présentation du dossier de déclaration complet est de 3 mois*.

L'instruction se décompose comme suit :

**1.1** Délai maximum entre le dépôt du dossier de déclaration et une demande éventuelle d'informations complémentaires : **1 mois**.

**1.2** Délai maximum entre le dépôt de la déclaration et la décision du service national des métiers de l'encadrement du ski et de l'alpinisme, dans le cas où il demande un complément d'informations : **2 mois à compter de la réception du complément d'information.**

**1.3** Délai maximum entre le dépôt du dossier de déclaration et la délivrance du récépissé de déclaration de prestation de services par le préfet, dans le cas où le service national des métiers de l'encadrement du ski et de l'alpinisme ne procède pas à la vérification des qualifications ou dans le cas où le service national des métiers de l'encadrement du ski et de l'alpinisme procède à cette vérification et n'identifie pas de différence substantielle : **1 mois**.

**1.4** Délai maximum entre le dépôt de la déclaration et la notification de la mesure de compensation, dans le cas où le service national des métiers de l'encadrement du ski et de l'alpinisme procède à la vérification des qualifications et identifie une différence substantielle : **1 mois**.

\* *Le silence gardé par le service national des métiers de l'encadrement du ski et de l'alpinisme dans le mois qui suit la réception du dossier de déclaration, même incomplet, vaut tacite acceptation : le prestataire est réputé exercer légalement sur le territoire national.*

#### Coût

Gratuit.

#### Mesures de compensation

S'il existe une différence substantielle entre la qualification du requérant et celle requise en France pour exercer la même activité, le service national des métiers de l'encadrement du ski et de l'alpinisme saisit la commission de reconnaissance des qualifications, placée auprès du ministre chargé des sports. Cette commission saisit la section permanente du ski alpin de la commission de la formation et de l'emploi du Conseil supérieur des sports de montagne après examen et instruction du dossier. Elle émet, dans le mois de sa saisine, un avis qu'elle adresse au service national des métiers de l'encadrement du ski et de l'apinisme.

Dans son avis, la commission peut :

- estimer qu'il existe effectivement une différence substantielle entre la qualification du déclarant et celle requise en France. Dans ce cas, la commission propose de soumettre le déclarant à une épreuve d'aptitude ou un stage d'adaptation. Elle définit la nature et les modalités précises de ces mesures de compensation (nature des épreuves, modalités de leur organisation et de leur évaluation, période d'organisation, contenu et durée du stage, types de structures pouvant accueillir le stagiaire, etc.) ;
- estimer qu'il n'y a pas de différence substantielle entre la qualification du déclarant et celle requise en France.

À réception de l'avis de la commission, le préfet notifie sa décision motivée au déclarant (il n'est pas obligé de suivre l'avis de la commission) :

- s'il exige qu'une mesure de compensation soit effectuée, le déclarant dispose d'un délai d'un mois pour choisir entre la ou les épreuve(s) d'aptitude et le stage d'adaptation. Le préfet délivre ensuite une carte professionnelle au déclarant qui a satisfait aux mesures de compensation. En revanche, si le stage ou l'épreuve d'aptitude ne sont pas satisfaisants, le préfet notifie sa décision motivée de refus de délivrance de la carte professionnelle à l'intéressé ;
- si le service national des métiers de l'encadrement du ski et de l'alpinisme n'exige pas de mesure de compensation, le préfet délivre une carte professionnelle à l'intéressé.

#### Voies de recours

Si refus de délivrance de la reconnaissance de qualification professionnelle, le demandeur peut initier un recours contentieux devant le tribunal administratif compétent dans les deux mois suivant la notification du refus. De même, si l'intéressé veut contester la décision de le soumettre à une mesure de compensation, il doit d'abord initier un recours gracieux auprès du service national des métiers de l'encadrement du ski et de l'alpinisme dans les deux mois suivant la notification de la décision. S'il n'obtient pas gain de cause, il pourra alors opter pour un recours contentieux devant le tribunal administratif compétent. 

### Accès partiel à la profession de moniteur de snowboard

Le métier de moniteur de snowboard (activité dérivée du ski alpin) implique la transmission de compétences techniques, de sécurité et de pédagogie dans la pratique du snowboard. L'accès partiel à cette profession est autorisé pour les migrants communautaires européens souhaitant exercer en France conformément à la directive 2005/36 du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles et l'article L.212-7 du Code du sport.

Ce processus de reconnaissance implique l'évaluation des compétences et qualifications du demandeur par rapport aux standards français. Une fois reconnus, ces moniteurs peuvent enseigner dans des établissements dédiés en respectant les réglementations en vigueur. Le Service national des métiers de l'encadrement du ski et de l'alpinisme (SNMESA) est l'organisme compétent pour cette reconnaissance.

## Service d'assistance <!-- collapsable:open -->

### Centre d'information français

Le [Centre ENIC-NARIC](https://www.france-education-international.fr/expertises/enic-naric?langue=fr) est le centre français d'information sur la reconnaissance académique et professionnelle des diplômes.

### SOLVIT

[SOLVIT](https://sgae.gouv.fr/sites/SGAE/accueil/leurope-au-service-des-citoyens/reseau-solvit--resolution-effica.html) est un service fourni par l'Administration nationale de chaque État membre de l'UE ou partie à l'accord sur l'EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l'UE à l'Administration d'un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

#### Conditions

L'intéressé ne peut recourir à SOLVIT que s'il établit :

- que l'Administration publique d'un État de l'UE n'a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d'un autre État de l'UE ;
- qu'il n'a pas déjà initié d'action judiciaire (le recours administratif n'est pas considéré comme tel).

#### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](https://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web). Une fois son dossier transmis, SOLVIT le contacte dans un délai d'une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

#### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l'ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l'autorité administrative concernée).

#### Délai

SOLVIT s'engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

#### Coût

Gratuit.

#### Issue de la procédure

À l'issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l'application du droit européen, la solution est acceptée et le dossier est clos ;
- s'il n'y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

#### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris.

## Liens utiles <!-- collapsable:open -->

### Textes de référence

Articles L. 212-1 et suivants, R. 212-85 et suivants, A. 212-1, A. 212-176 et suivants et annexes II-1 et II-12-3 du Code du sport.

### Autres liens utiles

Le service invite le demandeur à établir un dossier de demande d'équivalence via l'[application Arquedi](https://www.arquedi.sports.gouv.fr/) (sports.gouv.fr).