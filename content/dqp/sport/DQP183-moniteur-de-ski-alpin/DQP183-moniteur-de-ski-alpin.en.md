﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP183" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Alpine ski instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="alpine-ski-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/alpine-ski-instructor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="alpine-ski-instructor" -->
<!-- var(translation)="Auto" -->


Alpine ski instructor
=====================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The national alpine ski instructor supervises all types of public in the practice of alpine skiing and related activities (including snowboarding) in all classes of alpine ski progression.

It can exercise on the secure area of the slopes and off the slopes, with the exception of unmarked glacial zones and terrain whose use uses mountaineering techniques.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The alpine ski instructor activity is subject to the application of Article L. 212-1 of the Code of Sport, which requires specific certifications.

As a sports teacher, the alpine ski instructor must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- registered in the National Register of Professional Certifications (RNCP). For more information, it is advisable to refer to the[RNCP website](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

The qualifications for practising as a ski instructor are the State Diploma of National Ski Instructor and the State Diploma of National Ski Instructor specialized in training. The State Diploma of Mountaineering-Guide mountain also allows its holder to drive and accompany people on alpine skiing and off-piste skiing trips and to teach skiing techniques. For more information on this latest diploma, it is recommended to consult the "High Mountain Guide" sheet.

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*To go further* Articles L. 212-1, R. 212-84, A. 212-1 and Appendix II-1 of the Code of Sport.

**Good to know: the specific environment**

Alpine skiing is an activity in a specific environment involving compliance with specific safety measures. Therefore, only organizations under the tutelage of the Ministry of Sport can train future professionals.

*To go further* Articles L. 212-2 and R. 212-7 of the Code of Sport.

#### Training

##### State Ski Diploma - National Alpine Ski Instructor

The state diploma of national alpine ski instructor is registered at Level III of the[National Directory of Professional Certifications (RNCP)](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

This diploma is issued at the end of a course comprising a general cycle common to the teaching, coaching and training of mountain sports and training specific to alpine skiing. The programmes respect the principle of alternation based on the articulation of learning periods in training centers and professional situations under pedagogical tutoring.

The training is provided by the National Ski and Mountaineering School (ENSA).

The "competitive practices" training unit and the general training common to the teaching, coaching and training of mountain sports can be obtained through the validation of experience (VAE).

*To go further* Articles D. 212-68 and the following articles of the Code of Sport; Article 27 of the decree of 11 April 2012 relating to the specific training of the State Diploma of National Ski Instructor of Alpine Ski.

**Prerogatives**

The State Diploma of National Ski Instructor for Alpine Skiing attests to the skills required for the supervision, animation, teaching and safety training of alpine skiing and its spin-off activities. The coaching and animation activity involves accompaniment on the ski area.

This diploma allows the holder to practice independently and independently, with any type of alpine ski equipment and any type of gear derived from this equipment, on and off the slopes, with the exception of unmarked glacial zones and terrain. whose attendance uses mountaineering techniques.

*To go further* Article 1 of the 11 April 2012 decree on the specific training of the State Diploma of National Ski Instructor for Alpine Skiing.

**Good to know: derivative activities**

Activities derived from alpine skiing are:

- gravity sliding or moving on snow using gear of various shapes for any type of audience;
- which are practised in snowy mountain environments, excluding unmarked glacial areas and land whose use uses mountaineering techniques.

In addition to the usual forms of alpine skiing (skiing, off-piste skiing, ski racing, free ride, free style, freestyle skiing, ski cross, jumping), the most frequently practiced spin-off activities are snowboarding in all its activities. telemark, ski biking and snowshoeing.

*To go further* : Appendix VIII of the decree of 11 April 2012 relating to the specific training of the State Diploma of National Ski Instructor of Alpine Ski.

**Training unwinding**

The training is preceded by a technical access test consisting of a slalom. The training course takes place in the following chronological order:

- preparatory cycle, lasting a minimum of 70 hours over two weeks. An examination at the end of this cycle ensures that the candidate meets the pre-educational requirements. A training booklet consisting of the three successive training times is issued to the candidate admitted to the preparatory cycle by the regional director of youth, sports and social cohesion;
- the educational awareness course, which lasts a minimum of 25 days. It allows the subject of technical and pedagogical content and takes place in ski schools and federal training structures of the French Ski Federation (FFS) under the authority of the school director or the president of the federal structure. training sessions. It is available to holders of a valid training booklet including the first training time;
- Eurotest: this is a performance event that validates the technical ability and consists of a giant slalom in alpine skiing. The success of this test opens the door to the opening of the second training time;
- the undergraduate cycle, with a minimum duration of 140 hours spread over four weeks, consisting of a training unit on the fundamentals of alpine skiing education in snowy mountain environments;
- The application course, which lasts a minimum of 25 days. It takes place in a ski school or in a federal training structure of the FFS under the authority of the principal of the school or the president of the federal training structure, after validation of an internship agreement. It is available to holders of a valid training booklet;
- the second cycle, lasting five weeks, consisting of three training units (UF):- UF "competitive practices," with a minimum duration of 35 hours spread over a week,
  - UF "technical and pedagogical mastery of alpine skiing education, technical mastery in safety of derivative activities, including snowboarding," lasting a minimum of 70 hours spread over two weeks,
  - UF "deepening safety on runways, off the slopes and snowy mountain environment, including Eurosecurity testing," lasting a minimum of 70 hours over two weeks.

**Conditions of access to the technical access test**

The person concerned must:

- Be seventeen years old by 31 December of the calendar year in which the test takes place;
- provide the application for registration on a standardized print with a recent photo ID, a photocopy of the national ID card or passport, and three 23 x 16 cm sticker envelopes with a rate of in force and worded in the name and address of the candidate;
- Provide a medical certificate of non-contradictory to the practice and teaching of skiing less than one year old on the closing date of the first registration;
- for candidates of French nationality born from 1979 for men and from 1983 for women, provide a photocopy of the census certificate or the individual certificate of participation in the day of defence and citizenship;
- for minors, provide parental permission or that of the legal guardian.

**Conditions for access to the preparatory cycle**

The person concerned must:

- Be at least 18 years old on the first day of training
- Provide the application for registration on a standardized print with two recent identity photographs and a photocopy of the national ID card or passport;
- Provide the certificate of success in the technical access test less than three years old on the closing date of the first registration;
- Provide a photocopy of the Level 1 Civic Prevention and Relief (PSC1) teaching unit or its equivalent;
- provide a medical certificate of non-contradictory to the practice and teaching of alpine skiing less than one year old on the closing date of the first registration;
- provide three 23 x 16 cm sticker envelopes, postaged at the current rate and labelled in the candidate's name and address, and a 21 x 29.7 cm sticker envelope stamped at the current rate for sending a recommended with accused of Reception.

**Postgraduate conditions**

The person concerned must:

- Have a training booklet, including the second training time
- have validated the Eurotest for less than five years or have been in possession of a eurotest exemption certificate issued for less than five years;
- have completed and validated at least 25 days of the educational awareness course;
- Pass the preparatory cycle exam;
- Provide the certificate or certificates of internship issued by the ski school's technical director;
- have validated the Eurotest for less than 5 years or have been in possession of a certificate of exemption issued for less than five years.

**Conditions for access to the 2nd cycle**

The person concerned must:

- be certified to pass the test of the general training common to the teaching, coaching and training professions of mountain sports or the certificate of success in the tests of the examination of the common general training mountain sports trades or certification of success in exams of the common part of the state sports educator's patent;
- have met the UF's "competitive practices" assessment;
- for the UF "technical mastery and pedagogy of alpine skiing education, technical mastery in safety of activities derived from snowboarding", having satisfied the UF's assessment of "competitive practices" and having carried out and validated at least 25 days of the Educational internship for application;
- for the UF "deepening safety on slopes, off the slopes and snowy mountain environment including the Eurosecurity test", having carried out at least six outings of off-piste skiing or ski touring.

*To go further* Articles 7, 9, 13, Appendix II of the April 11, 2012 order on the specific training of the State Diploma of National Alpine Ski Instructor.

**Recycling**

Holders of the State Diploma of National Ski Instructor for Alpine Ski are subject every six years to a refresher course organized by the National School of Mountain Sports, site of the ENSA. Recycling must take place before 31 December of the 6th year following graduation or last retraining.

*To go further* Article 1 of the 11 April 2012 decree on the specific training of the State Diploma of National Ski Instructor for Alpine Skiing.

##### State Ski Diploma - National Alpine Ski Instructor specializing in training

The state diploma of national alpine ski instructor specialized in training is registered at level II of the RNCP.

This diploma is issued at the end of a course comprising a general cycle common to the teaching, coaching and training of mountain sports and training specific to alpine skiing. The programmes respect the principle of alternation based on the articulation of learning periods in training centers and professional situations under pedagogical tutoring.

The training is provided by ENSA.

*To go further* Articles D. 212-68 and the following articles of the Code of Sport.

**Prerogatives**

The state diploma of alpine ski instructor specialized in training attests to the skills required in alpine skiing and its activities derived for the training, coaching and development of ski racing. It allows the incumbent to provide training for the coaches who compete in it, as well as the management and promotion of training structures.

**Training unwinding**

The training is a total of 490 hours, including 350 hours at ENSA and 140 hours in a reception facility. The training course takes place in the following chronological order:

- UF "performance optimization in alpine skiing and spin-off activities" lasting 245 hours;
- the 140-hour educational course in a situation. It is available to candidates with a valid training booklet that has certified one of the modules of the 1st UF;
- UF "manages a 500-hour competitive training-development training project in alpine skiing and spin-off activities."

**Terms of access**

The person concerned must:

- Provide a copy of the State Diploma of National Alpine Ski Instructor or the State Certificate of 1st degree sports educator option "alpine skiing";
- Provide a copy of the FFS regional tracer diploma;
- provide a certificate of the FFS certifying a ranking in FIS points of seventy-five points or less than seventy-five points for women and eighty points for men in downhill, slalom, giant or super giant or a ranking of less than eighty-five points for women and less than ninety points for men in two of the four disciplines mentioned above. Candidates who cannot justify such a classification may benefit on request from a waiver granted by the Director General of the ENSA, after notice from the permanent section of alpine skiing of the Committee on Training and Employment of the Higher Council mountain sports;
- Provide a five- to ten-page dossier on the candidate's experience during his or her sporting and professional career, as well as his motivation and professional project;
- Provide a standardized registration form with a photocopy of an ID, a photo ID and two stamped envelopes;
- provide a medical certificate of non-contradictory to the practice, teaching and training of alpine skiing less than one year old on the closing date of registration.

#### Costs associated with qualification

Training leading to the state diploma of national alpine ski instructor is paid. Its cost is about 4,500 euros.

For more details, it is advisable to get closer to the[Ensa](http://www.ensa.sports.gouv.fr/).

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that he has sent a prior declaration of activity to the prefect of the Department of Isère.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport; Article 29-1 of the Decree of 11 April 2012 on the specific training of the State Diploma of National Ski Instructor of Alpine Ski.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

#### If the Member State of origin regulates access or the exercise of the activity:

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

#### If the Member State of origin does not regulate access or the exercise of the activity:

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as an alpine ski instructor in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Reporting obligation (for the purpose of obtaining the professional sports educator card)

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Timeframe**

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

**Supporting documents**

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

**Cost**

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

**Competent authority**

The prior declaration of activity must be addressed to the prefect of the Department of Isère. The declaration file is forwarded by the prefect to the National Trades Of Ski and Mountaineering, which sends it for advice to the permanent section of alpine skiing of the Training and Employment Committee of the Higher Sports Council of Mountain.

**Timeframe**

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

**Supporting documents**

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport; Articles 29-1 and following of the order of 11 April 2012 relating to the specific training of the State Diploma of National Ski-Alpine Ski Instructor.

### c. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

**Competent authority**

The declaration must be addressed to the prefect of the Department of Isère. The declaration file is forwarded by the prefect to the National Trades Of Ski and Mountaineering, which sends it for advice to the permanent section of alpine skiing of the Training and Employment Committee of the Higher Sports Council of Mountain.

**Timeframe**

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

**Supporting documents for the first declaration of activity**

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

**Evidence for a renewal of activity declaration**

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182 and following, annexes II-12-2a and II-12-2b of the Code of Sport; Articles 29-1 and following of the order of 11 April 2012 relating to the specific training of the State Diploma of National Ski-Alpine Ski Instructor.

### d. Compensation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission refers the permanent section of alpine skiing to the Training and Employment Committee of the Higher Council of Mountain Sports after review and investigation of the case. In the month of her referral, she issued a notice to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-90-1 and R. 212-92 of the Code of Sport; Articles 29-1 and following of the decree of 11 April 2012 relating to the specific training of the State Diploma of National Ski-Alpine Ski Instructor.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

