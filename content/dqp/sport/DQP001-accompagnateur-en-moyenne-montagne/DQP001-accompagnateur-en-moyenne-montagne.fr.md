﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP001" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Accompagnateur en moyenne montagne" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="accompagnateur-en-moyenne-montagne" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sport/accompagnateur-en-moyenne-montagne.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="accompagnateur-en-moyenne-montagne" -->
<!-- var(translation)="None" -->

# Accompagnateur en moyenne montagne

Dernière mise à jour : <!-- begin-var(last-update) -->Juin 2023<!-- end-var -->

## Définition de la profession <!-- collapsable:open -->

L'accompagnateur en moyenne montagne est un professionnel qui conduit et encadre des personnes ou des groupes en espace rural montagnard, à l'exclusion des zones glaciaires, de rochers, des canyons et des terrains nécessitant l'utilisation du matériel ou des techniques d'alpinisme pour la progression.

Par ailleurs, l'accompagnateur en moyenne montagne dispose de compétences spécifiques en fonction de l'option qu'il choisit :

- l'option « moyenne montagne enneigée » lui permet d'exercer sur des reliefs vallonnés excluant tout accident de terrain important. La pratique de toutes les disciplines du ski et activités dérivées est exclue, à l'exception de la raquette à neige ;
- l'option « moyenne montagne tropicale et équatoriale », lui permet d' exercer dans des régions à climat tropical et équatorial sur des terrains escarpés et détrempés en période, fixées par l'autorité publique compétente, de fortes précipitations.

Ses compétences doivent lui permettre de :

- gérer le risque, dans une logique de sécurité, lié à la randonnée en moyenne montagne ;
- concevoir, coordonner et conduire un projet d'action en initiation, perfectionnement et entraînement à la randonnée en moyenne montagne pour tout public ;
- concevoir et mettre en œuvre des programmes d'actions éducatives et formatives dans les domaines de l'éducation à l'environnement montagnard naturel et humain et dans le domaine de la formation professionnelle ;
- développer durablement les pratiques de la randonnée en moyenne montagne dans le respect du milieu montagnard et des dispositions réglementaires ;
- assurer des fonctions de management, de communication et conception de projets liés aux activités de montagne et notamment à l'organisation de sa profession.

Le métier d'accompagnateur en moyenne montagne peut s'exercer à temps plein ou à titre d'occupation saisonnière.

## Qualifications professionnelles requises en France <!-- collapsable:open -->

Pour exercer en qualité d'accompagnateur en moyenne montagne, l'intéressé doit disposer du diplôme d'État d'alpinisme – accompagnateur en moyenne montagne, ce qui lui confère la qualité d'éducateur sportif.

Le diplôme d'État d'alpinisme – accompagnateur en moyenne montagne est classé niveau III (bac +2). Il est préparé après un parcours de formation sous statut d'élève ou d'étudiant, par la voie d'un contrat d'apprentissage, après un parcours de formation continue, en contrat de professionnalisation, par candidature individuelle ou par la voie de la validation des acquis de l'expérience (VAE). Pour plus d'informations, vous pouvez consulter le [site officiel de la VAE](http://www.vae.gouv.fr/) ainsi que l'article 23 de l'arrêté du 25 septembre 2014 relatif à la formation spécifique du diplôme d'État d'alpinisme – accompagnateur en moyenne montagne.
L'intéressé doit opter pour l'une des deux options suivantes :

- moyenne montagne enneigée ;
- moyenne montagne tropicale et équatoriale.

La formation menant à l'obtention du diplôme d'État d'alpinisme – accompagnateur en moyenne montagne est dispensée par l'école nationale des sports de montagne (ENSM), sur le [site du centre national de ski nordique et de moyenne montagne](http://cnsnmm.sports.gouv.fr/) (CNSNMM) à Prémanon. La formation spécifique du diplôme d'État d'alpinisme – accompagnateur en moyenne montagne intégrant l'unité de formation « moyenne montagne tropicale et équatoriale » est organisée dans les départements et régions d'outre-mer.

La formation menant à l'obtention du diplôme d'État d'alpinisme – accompagnateur en moyenne montagne est précédée d'un examen probatoire. Une fois cet examen effectué, la formation alterne entre périodes de formation, un stage en situation, une période d'observation et se conclut par un examen final.

## Particularités de la réglementation de la profession <!-- collapsable:open -->

### Conditions d'honorabilité

Il est interdit d'exercer en tant qu'accompagnateur en moyenne montagne en France pour les personnes ayant fait l'objet d'une condamnation pour tout crime ou pour l'un des délits suivants :

- torture et actes de barbarie ;
- agressions sexuelles ;
- trafic de stupéfiants ;
- mise en danger d'autrui ;
- proxénétisme et infractions qui en résultent ;
- mise en péril des mineurs ;
- usage illicite de substance ou plante classées comme stupéfiants ou provocation à l'usage illicite de stupéfiants ;
- infractions prévues aux articles L. 235-25 à L. 235-28 du Code du sport ;
- à titre de peine complémentaire à une infraction en matière fiscale : condamnation à une interdiction temporaire d'exercer, directement ou par personne interposée, pour son compte ou le compte d'autrui, toute profession industrielle, commerciale ou libérale (article 1750 du Code général des impôts).

De plus, nul ne peut enseigner, animer ou encadrer une activité physique ou sportive auprès de mineurs, s'il a fait l'objet d'une mesure administrative d'interdiction de participer, à quelque titre que ce soit, à la direction et à l'encadrement d'institutions et d'organismes soumis aux dispositions législatives ou réglementaires relatives à la protection des mineurs accueillis en centre de vacances et de loisirs, ainsi que de groupements de jeunesse, ou s'il a fait l'objet d'une mesure administrative de suspension de ces mêmes fonctions.

### Sanctions

L'éducateur exerçant son activité sans s'être déclaré commet une infraction réprimée par l'article L. 212-12 du Code du sport d'un an d'emprisonnement et de 15 000 euros d'amende. En outre, en cas de condamnation pour manquements à l'honneur ou à la probité, le professionnel encourt une interdiction temporaire ou définitive d'exercer son activité.

### Aptitude physique

Pour être admis à la formation menant à l’obtention du diplôme d’État d’alpinisme – accompagnateur en moyenne montagne, le candidat doit réussir un examen probatoire composé de différentes épreuves, dont certaines sont éliminatoires.

L’examen probatoire comprend trois épreuves qui se déroulent dans l’ordre chronologique suivant :

- une épreuve de marche, orientation et parcours en terrain varié (cette épreuve est éliminatoire). L'épreuve individuelle est effectuée en autonomie de navigation et, pour tout ou partie, hors sentier. Le parcours a une durée comprise entre 6 et 8 heures et comprend un dénivelé positif cumulé d'environ 1 400 mètres. Pendant le parcours, le candidat porte un sac à dos de 10 kilogrammes pour les hommes et 7 kilogrammes pour les femmes ;
- un questionnaire portant sur l'environnement montagnard naturel et humain, d'une durée maximale d'une heure. Cette épreuve écrite vise à s'assurer que le candidat possède les connaissances de base relatives à l'environnement montagnard naturel (écologie générale, faune, flore, géologie, géographie, météorologie) et humain (économie, habitat et vie sociale, protection de l'environnement) ;
- un entretien de 20 minutes, mené à partir de la liste de randonnées communiquée dans le dossier d'inscription à l'examen probaoire. Cette épreuve tend à s'assurer de la réalité des expériences du candidat et à apprécier son expérience de la vie en montagne ainsi que son aptitude à la communication.

Pour être admis à l'examen probatoire, il faut avoir réussi la première épreuve et validé les deux suivantes. Le cas échéant, une attestation de réussite à l'examen probatoire est adressée au candidat par le directeur régional de la jeunesse, des sports et de la cohésion sociale qui organise l'examen.

### Obligation de formation professionnelle continue

Le stage de recyclage, d'une durée minimale de 24 heures, vise à actualiser les compétences professionnelles des accompagnateurs en moyenne montagne, en particulier dans les domaines de la gestion de la sécurité, de l'obligation de moyens et de la réglementation, à partir de l'analyse préalable des pratiques professionnelles et de leurs évolutions : analyse des risques, accidentologie, évolution du cadre juridique ou sociétal, mise à jour des savoirs et savoir-faire.

À l'issue de chaque session, les attestations de recyclage sont délivrées par le directeur général de l'École nationale des sports de montagne, site du centre national de ski nordique et de moyenne montagne.

Le stage de recyclage doit intervenir au plus tard le 31 décembre de la sixième année suivant l’obtention du diplôme ou du précédent recyclage.

### Activité réglementée 

Toute personne souhaitant exercer la profession d'accompagnateur en moyenne montagne, régie par l'article L. 212-1 du Code du sport, doit déclarer son activité au préfet du département du lieu où elle compte exercer à titre principal. Cette déclaration déclenche l'obtention d'une carte professionnelle. La déclaration doit être renouvelée tous les cinq ans.

### Activité s'exerçant dans un environnement spécifique

L'alpinisme et ses activités associées sont considérés comme des activités s'exerçant dans un « environnement spécifique » (ES), quelle que soit la zone d'évolution. De ce fait, la pratique de ces activités impose le respect de mesures de sécurité particulières mentionnées à l'article L. 212-2 du Code du sport.

## Démarches de reconnaissance de qualification professionnelle <!-- collapsable:open -->

### Libre Établissement (exercice stable et continu)

Le ressortissant d’un État de l'UE ou de l'EEE peut s'établir en France pour y exercer de façon permanente, s'il remplit l'une des quatre conditions suivantes :

**Si l'État membre d'origine réglemente l'accès ou l'exercice de l'activité**

- Être titulaire d'une attestation de compétences ou d'un titre de formation délivré par l'autorité compétente d'un État de l'UE ou de l'EEE qui atteste d'un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France.
- Être titulaire d'un titre acquis dans un État tiers et admis en équivalence dans un État de l'UE ou de l'EEE et justifier avoir exercé cette activité pendant au moins deux ans à temps complet dans cet État.

**Si l'État membre d'origine ne réglemente ni l'accès, ni l'exercice de l'activité**

- Justifier avoir exercé l'activité dans un État de l'UE ou de l'EEE, à temps complet pendant deux ans au moins au cours des dix dernières années, ou, en cas d'exercice à temps partiel, justifier d'une activité d'une durée équivalente et être titulaire d'une attestation de compétences ou d'un titre de formation délivré par l'autorité compétente d'un de ces États, qui atteste d'une préparation à l'exercice de l'activité, ainsi qu'un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France.
- Être titulaire d'un titre attestant d'un niveau de qualification au moins équivalent au niveau immédiatement inférieur à celui requis en France, délivré par l'autorité compétente d'un État de l'UE ou de l'EEE et sanctionnant une formation réglementée visant spécifiquement l'exercice de tout ou partie des activités mentionnées à l'article L. 212-1 du Code du sport et consistant en un cycle d'études complété, le cas échéant, par une formation professionnelle, un stage ou une pratique professionnelle.

Si le ressortissant remplit l'une des quatre conditions précitées, l'obligation de qualification requise pour exercer est réputée satisfaite.

#### Autorité compétente

- En ligne sur l'[application Arquedi](https://www.arquedi.sports.gouv.fr/) (sports.gouv.fr), ou
- Préfet de l'Isère via le SDJES 38 **Service départemental à la jeunesse, à l'engagement et aux sports** (SDJES ISERE, Cité administrative,1 rue Joseph Chanrion - Bâtiment 2, 38000 GRENOBLE - Téléphone : 04.76.74.79.79 - Mail : [ddcs@isere.gouv.fr](mailto:ddcs@isere.gouv.fr)).
- Pôle National des Métiers de l'Encadrement du Ski et de l'Alpinisme (Rectorat de l'Académie de Grenoble, P.N.M.E.S.A - 7 place Bir-Hakeim – CS 81065 - 38021 GRENOBLE cedex 1 - Téléphone :04.76.74.70.00).

#### Procédure

Le professionnel dépose une déclaration à l'autorité compétente. Dans le mois suivant le dépôt du dossier de déclaration, la préfecture envoie un accusé de réception au déclarant.

#### Pièces justificatives

- Formulaire de déclaration [Cerfa 12699*02](https://www.formulaires.service-public.fr/gf/cerfa_12699.do) (service-public.fr) ;
- Document attestant de l’absence de condamnation dans l’état membre d’origine 
- Une copie d’une pièce d’identité en cours de validité ;
- Une déclaration sur l’honneur attestant de l’exactitude des informations figurant dans le formulaire ;
- Une copie de chacun des diplômes, titres, certificats invoqués ;
- Une copie de l’autorisation d’exercice, ou, le cas échéant, de l’équivalence de diplôme ;
- Un certificat médical de non contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées, datant de moins d’un an.

En cas de renouvellement de déclaration, il faut joindre :

- Le formulaire Cerfa 12699*02 ; 
- Une copie de l’attestation de révision en cours de validité pour les qualifications soumises à l’obligation de recyclage ;
- Un certificat médical de non contre-indication à la pratique et à l’encadrement des activités physiques ou sportives concernées, datant de moins d’un an.

De plus, dans tous les cas, la préfecture demandera elle-même la communication d’un extrait de moins de trois mois du casier judiciaire du déclarant pour vérifier l’absence d’incapacité ou d’interdiction d’exercer. 

#### Délais de réponse

A compter de la date de présentation du dossier complet par le déclarant, le préfet dispose d'un délai de trois mois pour rendre sa décision de délivrer, ou non, la carte professionnelle. Ce délai peut être prorogé d'un mois par décision motivée. L'épreuve d'aptitude se déroule dans un délai de six mois à compter de la décision.

Ce dispositif a fait l'objet d'une dérogation (décret 2014-1306 du 23 octobre 2014) au principe du silence vaut accord.

#### Coût

Gratuit.

#### Mesures de compensation

S'il existe une différence substantielle entre la qualification du requérant et celle requise en France pour exercer la même activité, le préfet saisit la commission de reconnaissance des qualifications placée auprès du ministre chargé des sports. Cette commission, avant d'émettre son avis, saisit pour avis les organismes de concertation spécialisés.

Dans son avis, la commission peut :

- estimer qu'il existe effectivement une différence substantielle entre la qualification du déclarant et celle requise en France. Dans ce cas, la commission propose de soumettre le déclarant à une épreuve d'aptitude dont elle définit la nature et les modalités précises (nature des épreuves, modalités de leur organisation et de leur évaluation, période d'organisation, contenu et durée du stage, types de structures pouvant accueillir le stagiaire etc.) ;
- estimer qu'il n'y a pas de différence substantielle entre la qualification du déclarant et celle requise en France.

À réception de l'avis de la commission, le préfet notifie sa décision motivée au déclarant (il n'est pas obligé de suivre l'avis de la commission) :

- s'il exige qu'une mesure de compensation soit effectuée, le déclarant doit se soumettre à une épreuve d'aptitude. Le préfet délivre ensuite une carte professionnelle au déclarant qui a satisfait aux mesures de compensation. En revanche, si le stage ou l'épreuve d'aptitude ne sont pas satisfaisants, le préfet notifie sa décision motivée de refus de délivrance de la carte professionnelle à l'intéressé ;
- s'il n'exige pas de mesure de compensation, le préfet délivre une carte professionnelle à l'intéressé.

#### Voies de recours

Si refus de délivrance de la reconnaissance de qualification professionnelle, le demandeur peut initier un recours contentieux devant le tribunal administratif compétent dans les deux mois suivant la notification du refus. De même, si l'intéressé veut contester la décision de le soumettre à une mesure de compensation, il doit d'abord initier un recours gracieux auprès du préfet du département, dans les deux mois suivant la notification de la décision. S'il n'obtient pas gain de cause, il pourra alors opter pour un recours contentieux devant le tribunal administratif compétent.

### Libre Prestation de Services (exercice temporaire et occasionnel)

Les ressortissants de l'UE ou de l'EEE légalement établis dans l'un de ces États et souhaitant exercer en France de manière temporaire ou occasionnelle doivent effectuer une déclaration préalable d'activité, avant la première prestation de services. Afin d'éviter des dommages graves pour la sécurité des bénéficiaires, le préfet peut, lors de la première prestation, procéder à une vérification préalable des qualifications professionnelles du prestataire.

#### Autorité compétente

- En ligne sur l'[application Arquedi](https://www.arquedi.sports.gouv.fr/) (sports.gouv.fr)], ou
- Préfet de l'Isère via le SDJES 38 **Service départemental à la jeunesse, à l'engagement et aux sports** (SDJES ISERE, Cité administrative,1 rue Joseph Chanrion- Bâtiment 2, 38000 GRENOBLE).

#### Procédure

Dans le mois suivant la réception du dossier de déclaration, le préfet notifie au prestataire :

- soit une demande d’informations complémentaires (dans ce cas, le préfet dispose de deux mois pour donner sa réponse) ;
- soit un récépissé de déclaration de prestation de services s’il ne procède pas à la vérification des qualifications. Dans ce cas, la prestation de services peut débuter ;
- soit qu’il procède à la vérification des qualifications. Dans ce cas, le préfet délivre ensuite au prestataire un récépissé lui permettant de débuter sa prestation ou, si la vérification des qualifications fait apparaître des différences substantielles avec les qualifications professionnelles requises en France, le préfet soumet le prestataire à une épreuve d’aptitude (cf. infra « Bon à savoir : mesures de compensation »).

Dans tous les cas, en l’absence de réponse dans les délais précités, le prestataire est réputé exercer légalement son activité en France.

#### Pièces justificatives

Le dossier de déclaration préalable d'activité doit contenir :

- un exemplaire du formulaire de déclaration dont le modèle est fourni à l'[annexe II-12-3 du Code du sport](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000045058326#:~:text=3.,4.) (legifrance.gouv.fr) ;
- une copie d'une pièce d'identité ;
- une copie de l'attestation de compétences ou du titre de formation ;
- une copie des documents attestant que le déclarant est légalement établi dans l'État membre d'établissement et qu'il n'encourt aucune interdiction, même temporaire, d'exercer (traduits en français par un traducteur agréé) ;
- dans le cas où ni l'activité, ni la formation, conduisant à cette activité n'est réglementée dans l'État membre d'établissement, une copie de toutes pièces justifiant que le déclarant a exercé cette activité dans cet État pendant au moins l'équivalent de deux ans à temps complet au cours des dix dernières années (traduites en français par un traducteur agréé) ;
- l'un des trois documents au choix (à défaut, un entretien sera organisé):
  - une copie d'une attestation de qualification délivrée à l'issue d'une formation en français,
  - une copie d'une attestation de niveau en français délivré par une institution spécialisée,
  - une copie d'un document attestant d'une expérience professionnelle acquise en France.

#### Délais de réponse

Le préfet dispose d'un mois pour notifier au prestataire soit la délivrance du récépissé, soit une demande d'information complémentaire, soit une vérification de ses compétences par le moyen d'une épreuve d'aptitude.

La prestation doit pouvoir intervenir dans les deux mois qui suivent la réception du dossier initial de déclaration complet, sauf difficulté particulière justifiant que ce délai soit porté à trois mois. 

Faute de réponse initiale dans le délai d'un mois, ou faute de décision définitive dans le délai de deux mois, le prestataire est réputé exercer légalement son activité en France. 

#### Coût

Gratuit.

#### Mesures de compensation

S'il existe une différence substantielle entre la qualification du requérant et celle requise en France pour exercer la même activité, le préfet saisit la commission de reconnaissance des qualifications placée auprès du ministre chargé des sports. Cette commission, avant d'émettre son avis, saisit pour avis les organismes de concertation spécialisés.

Dans son avis, la commission peut :

- estimer qu'il existe effectivement une différence substantielle entre la qualification du déclarant et celle requise en France. Dans ce cas, la commission propose de soumettre le déclarant à une épreuve d'aptitude dont elle définit la nature et les modalités précises (nature des épreuves, modalités de leur organisation et de leur évaluation, période d'organisation, contenu et durée du stage, types de structures pouvant accueillir le stagiaire etc.) ;
- estimer qu'il n'y a pas de différence substantielle entre la qualification du déclarant et celle requise en France.

À réception de l'avis de la commission, le préfet notifie sa décision motivée au déclarant (il n'est pas obligé de suivre l'avis de la commission) :

- s'il exige qu'une mesure de compensation soit effectuée, le déclarant doit se soumettre à une épreuve d'aptitude. Le préfet délivre ensuite un récépissé au déclarant qui a satisfait aux mesures de compensation. En revanche, si le stage ou l'épreuve d'aptitude ne sont pas satisfaisants, le préfet notifie sa décision motivée de refus de délivrance du récépissé à l'intéressé ;
- s'il n'exige pas de mesure de compensation, le préfet délivre un récépissé à l'intéressé.

#### Voies de recours

Si refus de délivrance de la reconnaissance de qualification professionnelle, le demandeur peut initier un recours contentieux devant le tribunal administratif compétent dans les deux mois suivant la notification du refus. De même, si l'intéressé veut contester la décision de le soumettre à une mesure de compensation, il doit d'abord initier un recours gracieux auprès du préfet du département, dans les deux mois suivant la notification de la décision. S'il n'obtient pas gain de cause, il pourra alors opter pour un recours contentieux devant le tribunal administratif compétent.

## Service d'assistance <!-- collapsable:open -->

### Centre d'information français

Le [Centre ENIC-NARIC](https://www.france-education-international.fr/expertises/enic-naric?langue=fr) est le centre français d'information sur la reconnaissance académique et professionnelle des diplômes.

### SOLVIT

[SOLVIT](https://sgae.gouv.fr/sites/SGAE/accueil/leurope-au-service-des-citoyens/reseau-solvit--resolution-effica.html) est un service fourni par l'Administration nationale de chaque État membre de l'UE ou partie à l'accord sur l'EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l'UE à l'Administration d'un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

#### Conditions

L'intéressé ne peut recourir à SOLVIT que s'il établit :

- que l'Administration publique d'un État de l'UE n'a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d'un autre État de l'UE ;
- qu'il n'a pas déjà initié d'action judiciaire (le recours administratif n'est pas considéré comme tel).

#### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](https://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web). Une fois son dossier transmis, SOLVIT le contacte dans un délai d'une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

#### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l'ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l'autorité administrative concernée).

#### Délai

SOLVIT s'engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

#### Coût

Gratuit.

#### Issue de la procédure

À l'issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l'application du droit européen, la solution est acceptée et le dossier est clos ;
- s'il n'y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

#### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris.

## Liens utiles <!-- collapsable:open -->

### Textes de référence

- Code du sport, articles L.212-1, L.212-7, L. 212-9, R.212-84, R.212-88 à R.212-94, D. 212-67, D.212-84-1, A212-182 à A.212-182-2, et annexe II-1
- Arrêté du 11 mars 2015 relatif au contenu et aux modalités d'organisation du recyclage des titulaires des diplômes d'accompagnateur en moyenne montagne

### Autres liens utiles

Le service invite le demandeur à établir un dossier de demande d'équivalence via l'[application Arquedi](https://www.arquedi.sports.gouv.fr/) (sports.gouv.fr).

Dans le cas d'un dépôt de dossier de demande d'équivalence en version papier, le service destinataire doit en accuser réception et saisir les informations de ce dossier dans l'application Arquedi. 
