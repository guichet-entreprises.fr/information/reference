﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP001" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Mid-mountain guide" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="mid-mountain-guide" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/mid-mountain-guide.html" -->
<!-- var(last-update)="2020-04-15 17:22:31" -->
<!-- var(url-name)="mid-mountain-guide" -->
<!-- var(translation)="Auto" -->


Mid-mountain guide
=====================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:31<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The medium mountain accompanist is a professional who leads and supervises people or groups in rural mountain areas, excluding glacial areas, rocks, canyons and land requiring the use of equipment or mountaineering techniques for progression.

In addition, the medium mountain guide has specific skills depending on the option he chooses:

- the "medium snowy mountain" option allows it to exercise on hilly terrain excluding any major terrain accidents. The practice of all skiing disciplines and spin-off activities is excluded, with the exception of snowshoeing;
- the "medium tropical and equatorial mountain" option allows it to exercise heavy rainfall in tropical and equatorial climate regions on steep and soggy terrain during periods, fixed by the competent public authority.

His skills should enable him to:

- manage the risk, in a safety logic, associated with hiking in the middle mountains;
- Design, coordinate and conduct an action project in initiation, development and training in the middle mountain for all public;
- Design and implement educational and training programmes in the areas of education in the natural and human environment and in the field of vocational training;
- develop sustainable mid-mountain hiking practices in accordance with the mountain environment and regulations;
- to perform management, communication and project design functions related to mountain activities and in particular to the organization of its profession.

The occupation of medium mountain accompanist may be practised full-time or as a seasonal occupation.

**Good to know: the recycling obligation**

Permission to work as an escort in the middle mountains is limited to a period of six years, renewable. At the end of this period, the renewal of the licence to practise is granted to holders of the diploma of medium mountain escort who have completed a refresher course organized by the National School of Mountain Sports (see below: "Training recycling").

*To go further* : Article 1 and following of the decree of 25 September 2014 relating to the specific training of the State Diploma of Mountaineering-accompanying in the mid-mountain, decree of 11 March 2015 relating to the content and organisation of the recycling of holders of middle mountain escort diplomas.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The medium mountain escort activity is subject to the application of Article L. 212-1 of the Code of Sport, which requires specific certifications.

As a sports teacher, the middle mountain accompanist must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- and recorded at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

**Good to know: the specific environment**

Mountaineering and its associated activities are considered to be activities in a "specific environment" (ES), regardless of the area of evolution. As a result, the practice of these activities requires compliance with specific safety measures referred to in section L. 212-2 of the Code of Sport.

*To go further* Articles L. 212-1, R. 212-84 and D. 212-67 of the Code of Sport.

#### Training

To work as an assistant in the middle mountains, the person concerned must have the state diploma of mountaineering - an assistant in the middle mountains, which gives him the quality of a sports educator.

The state diploma of mountaineering - medium mountain guide is classified as level III (bac 2). It is prepared after a training course under student or student status, through an apprenticeship contract, after a continuing education course, in a professionalisation contract, by individual application or by way of validation of (VAE). For more information, you can see[VAE's official website](http://www.vae.gouv.fr/) as well as Article 23 of the decree of 25 September 2014 relating to the specific training of the State Diploma of Mountaineering - medium mountain guide.

The individual must choose one of two options:

- medium snow-covered mountain;
- tropical and equatorial mountain.

The training leading to the State Diploma of Mountaineering - medium mountain guide is provided by the National School of Mountain Sports (ENSM), on the site of the[Nordic and mid-mountain ski centre](http://cnsnmm.sports.gouv.fr/) (CNSNMM) in Premanon. The specific training of the State Mountaineering Diploma - medium mountain guide integrating the "medium tropical and equatorial mountain" training unit is organised in overseas departments and regions.

The training leading to the state diploma of mountaineering - an aidman in the middle mountains is preceded by a probationary examination. Once this examination is completed, the training alternates between training periods, a situational internship, an observation period and concludes with a final examination.

**Conditions for access to training leading to the state diploma of mountaineering - medium mountain guide: the probationary examination**

To be admitted to the training leading up to the State Diploma of Mountaineering - medium mountain guide, the candidate must pass a probationary exam consisting of different tests, some of which are elimination.

**Probation requirements**

- Be seventeen years old by January 1 of the year of the exam;
- Send a registration file containing:- A registration application on a standardized form,
  - a recent photo ID,
  - a two-sided photocopy of the national ID card or passport,
  - A photocopy of the census certificate or the individual certificate of participation in the defence and citizenship day, if any,
  - parental permission (or legal guardian) if the applicant is a minor,
  - a certificate of non-contradictory to the practice of the medium mountain escort profession, less than one year old on the closing date of the first registration,
  - photocopying of the "Level 1 Civic Prevention and Relief" (PSC1) teaching unit, or its equivalent,
  - three envelopes stamped according to the rates detailed in Schedule I of the order of 25 September 2014 mentioned above,
  - a list of forty hikes carried out by the candidate, drawn up on a standardized form developed by the National School of Mountain Sports, site of the National Nordic and Mid-Mountain Ski Centre. The minimum content of this list is specified in Schedule I of the order of 25 September 2014 mentioned above.

**Content and conduct of probationary review**

The probationary examination consists of three tests that take place in the following chronological order:

- a walking event, orientation and course in varied terrain (this event is elimination). The individual test is carried out in autonomy of navigation and, for all or part, off the trail. The course lasts between 6 and 8 hours and includes a cumulative positive gradient of about 1,400 metres. During the course, the candidate carries a backpack of 10 kilograms for men and 7 kilograms for women;
- a questionnaire on the natural and human mountain environment, lasting up to one hour. This written test aims to ensure that the candidate has the basic knowledge of the natural mountain environment (general ecology, fauna, flora, geology, geography, meteorology) and human (economy, habitat and social life, protection of the environment);
- a 20-minute interview, conducted from the list of hikes provided in the probationary exam registration file. This test tends to ensure the reality of the candidate's experiences and to appreciate his experience of life in the mountains as well as his ability to communicate.

To be admitted to the probationary exam, you must have passed the first test and validated the next two. If necessary, a certificate of success on the probationary examination is addressed to the candidate by the Regional Director of Youth, Sports and Social Cohesion who organises the examination.

*To go further* : Description of the certification "DE: state diploma of mountaineering - medium mountain accompanist" of the national directory of professional certifications (RNCP), decree of 25 September 2014 relating to the specific training of the state diploma mountaineering - medium mountain guide.

###### Recycling course

The retraining course, which lasts a minimum of 24 hours, aims to update the professional skills of mid-mountain chaperones, particularly in the areas of safety management, means obligation and regulation, based on the preliminary analysis of professional practices and their evolution: risk analysis, accidentology, changes in the legal or societal framework, updating of knowledge and know-how.

At the end of each session, the recycling certificates are issued by the Director General of the National School of Mountain Sports, site of the National Nordic and Mid-Mountain Ski Centre.

The retraining course must take place no later than December 31 of the sixth year after graduation or previous retraining.

*To go further* : decree of 11 March 2015 relating to the content and organisation of the retraining of holders of mid-mountain escort diplomas.

#### Costs associated with qualification

Training leading to the state diploma of mountaineering - an accompaniment in the middle mountains - is paid for. As an indication, it costs about 4,500 euros. For more details, it is advisable to get closer to the[National School of Mountain Sports](http://www.ensm.sports.gouv.fr/).

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

**Good to know: the substantial difference**

For the supervision of mountaineering as an accompanist in the middle mountains, the substantial difference, likely to exist between the professional qualification of the registrant and the professional qualification required in the territory is appreciated in reference to the training leading to the state diploma of mountaineering - an accompaniment in the middle mountains, as it integrates:

- Theoretical and practical safety knowledge
- technical security skills.

In the context of the free provision of services, when the prefect considers, after the advice of the permanent section of mountaineering of the Committee on Training and Employment of the Higher Council of Mountain Sports forwarded to the National Trades Centre of skiing and mountaineering, whether there is a substantial difference, he may decide to subject the declarant to all or part of the aptitude test or an adaptation course.

*To go further* Articles A. 212-222 and A. 212-224 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity**

- To hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state which certifies a level of qualification at least equivalent to the level immediately lower than that required in France.
- To hold a title acquired in a third state and admitted in equivalence in an EU or EEA state and to justify having carried out this activity for at least two years full time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity**

- To justify having carried out the activity in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justify an activity of equivalent duration and hold a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France.
- To hold a qualification holder at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

**Good to know: the substantial difference**

For the supervision of mountaineering as an accompanist in the middle mountains, the substantial difference likely to exist between the professional qualification of the registrant and the professional qualification required in the territory is appreciated in reference to the training leading to the state diploma of medium mountain accompanist as it integrates:

- Theoretical and practical safety knowledge
- technical security skills.

In the context of the freedom of establishment, when the prefect considers, after the advice of the permanent section of mountaineering of the Committee on Training and Employment of the Higher Council of Mountain Sports, forwarded to the National Trades Centre of the management of skiing and mountaineering, that there is a substantial difference, he referred the matter to the recognition commission, attaching the opinion of the permanent section to the file.

After ruling on the existence of a substantial difference, the Qualifications Recognition Commission proposes, if necessary, to the prefect to submit the declarant to all or part of the aptitude test or to an adjustment course.

On the other hand, if the prefect believes that there is no substantial difference or where a substantial difference has been identified and the registrant has met the aptitude test, the prefect issues a certificate of freeness to the declarant. establishment and a professional sports educator card.

*To go further* Articles A. 212-222, A. 212-223 and A. 212-228, R. 212-88 to R. 212-91 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as an average mountain escort in France for persons who have been convicted of any crime or for one of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

4°. Reporting requirement (for the purpose of obtaining the professional sports educator card)
------------------------------------------------------------------------------------------------------------------

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the [official website](https://eaps.sports.gouv.fr).

**Timeframe**

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

**Supporting documents**

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

**Cost**

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

5°. Qualifications recognition procedures and formalities
-------------------------------------------------------------------

### a. Make a prior declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

**Competent authority**

The prior declaration of activity should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP) of the department where the declarer wants to perform his performance.

**Timeframe**

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

**Supporting documents**

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

**Competent authority**

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

**Timeframe**

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

**Supporting documents for the first declaration of activity**

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

**Evidence for a renewal of activity declaration**

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

**Good to know: compensation measures (activity in a specific environment)**

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications placed with the Minister responsible for sports. This commission, before issuing its opinion, refers the specialist consultation bodies for advice.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test, the nature of which it defines (the nature of the tests, the terms of their organisation and evaluation, the period of organization, the content and duration of the internship. , types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- if the registrant requires compensation, he or she must submit to an aptitude test. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-84, R. 212-90-1 and D. 212-84-1 of the Code of Sport.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

