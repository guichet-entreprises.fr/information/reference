﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP107" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Combined activities coach" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="combined-activites-coach" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/combined-activites-coach.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="combined-activites-coach" -->
<!-- var(translation)="Auto" -->


Combined activities coach
=======================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The combined activities include sports consisting of several events in different disciplines or under the jurisdiction of athletics.

Biathlon is an event combining the two disciplines of cross-country skiing and rifle shooting.

Triathlon is a sport consisting of three endurance events: swimming, cycling and running. Are associated with triathlon:

- duathlon that alternates running, cycling and running;
- aquathlon that combines swimming and running;
- bike and run.

Modern pentathlon combines fencing, swimming, horseback riding, pistol shooting and running.

Finally, the combined athletics events include pentathlon, heptathlon, octathlon and decathlon.

In all cases, the coach independently supervises his activity and trains one or more athletes by guaranteeing safe practice conditions. He is also involved in the project of the structure that employs him.

For more information, it is advisable to consult the[FFA website](http://www.athle.fr/pdf/formation/FICHE_infos_diverses_CQP_TSA.pdf).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The activity of coaching combined activities is governed by articles L. 212-1 and following of the Code of Sport that require specific certifications.

As a sports teacher, the sports coach must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

Qualifications to coach combined activities are:

- The Certificate of Professional Qualification (CQP) "Athletics Sports Technician" option "combined events";
- the State Diploma of Youth, Popular Education and Sport (DEJEPS) speciality "sports development" mention "triathlon";
- the Senior State Diploma of Youth, Popular Education and Sport (DESJEPS) specialty "sports performance" mention "athletics: combined events";
- desJEPS speciality "sports performance" mention "modern pentathlon."

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*To go further* Articles L. 212-1, R. 212-84, D. 212-11 and subsequent articles and Appendix II-1 (Article A. 212-1) of the Code of Sport.

#### Training

##### CQP "Athletics Sports Technician" option "combined events"

This diploma is managed by the French Athletics Federation (FFA).

It allows its holder to coach the combined events and train a group of athletes in this specialty.

It validates skills in coaching athletes over the age of sixteen, specialized and oriented towards achieving performance with a competitive objective.

In initial training, the minimum duration is 150 hours in a training centre, and 100 hours in alternating structure (club, committee, league, etc.).

The CQP is accessible through vocational training, through the professionalisation contract and through the validation of experience (VAE).

**What to know**

The holder of the CQP intervenes on an hourly volume of work that cannot exceed 360 hours per year.

###### Prerogatives of the CQP

The prerogatives of the CQP are:

- Provide athletes with a comprehensive training program to improve performance in combined events;
- transmit a specific technique suitable for athletes and provide for a physical improvement adapted to the discipline to apply effectively and securely, the techniques recommended by the FFA in the disciplines chosen by The athlete
- guarantee safe practice conditions in the environment to practitioners and third parties.

The CQP holder supervises athletes from initiation to activity to development and preparation for competition in combined events: pentathlon, heptathlon and decathlon.

For more information, it is advisable to consult the[FFA website](http://www.athle.fr/pdf/formation/FICHE_infos_diverses_CQP_TSA.pdf).

###### Conditions for access to the CQP

The conditions for access to the CQP are:

- attest to at least 100 hours of coaching experience in athletics;
- Show minimal federal involvement
- Have a basic knowledge of the discipline
- submit a medical certificate of non-contradictory to the practice and supervision of athletics dating back less than three months;
- hold Level 1 Civic Prevention and Relief (or equivalent).

For more information, it is advisable to consult the[FFA website](http://www.athle.fr/pdf/formation/FICHE_infos_diverses_CQP_TSA.pdf).

##### DEJEPS specialty "sports development" mention "triathlon"

DEJEPS is a Level 3-certified diploma (Bac 2/3), registered in the[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/). It attests to the acquisition of a qualification in the exercise of a professional activity of coordination and supervision for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma is prepared alternately by initial training, apprenticeship or continuing education. It can be obtained in its entirety through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr/).

###### Prerogatives

The possession of the DEJEPS gives its holder, in the field of triathlon (triathlon, duathlon, aquathlon and bike and run) the skills attested in the certification repository relating to:

- Designing and coordinating sports development programs
- conducting a development approach in triathlon;
- conducting training actions.

###### Conditions for access to training

The conditions for access to training are:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- Hold the Certificate of Supplemental First Aid Training with Equipment (AFCPSAM) or its equivalent;
- Holding the general theoretical test of driving on the road (code);
- pass the water rescue test;
- produce certificates of participation in five short- and long-distance triathlons and three duathlons, distance of choice, in a period of three years prior to entry into training, dated and signed by the organizer;
- meet the requirement to be able to supervise triathlon safely.

*To go further* : decree of December 15, 2006 creating the deJEPS specialty "sports development" dejePS" designation.

##### DESJEPS specialty "sports performance" mention "athletics: combined events"

The DESJEPS is a higher state diploma enrolled at Level II of the[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

It attests to the acquisition of a qualification in the exercise of a professional activity of technical expertise and management for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in full through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr) and Article D. 212-56 of the Code of Sport.

*To go further* Articles D. 212-51 and D. 212-56 of the Code of Sport.

###### DESJEPS prerogatives

The prerogatives of the DESJEPS are:

- Prepare a strategic performance project
- Piloting a training system
- Running a sports project
- Evaluate a training system
- organize training activities for trainers.

*To go further* Article 2 of the November 8, 2010 order creating the "athletics: combined events" of the SPECIALty DEJEPS "sports performance."

###### Conditions for access to training leading to DESJEPS:

The conditions for access to training are:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- provide certification of 300-hour coaching experience over the past five years in one of the athletics disciplines of the combined speciality group, delivered by the National Technical Director of Athletics
- provide the certificate, issued by the National Technical Director of Athletics, of the success of the pedagogical test consisting of conducting a 30-minute sports development session followed by a 20-minute interview in one of the athletics disciplines of the specialty group combined events;
- produce the test's certification of success consisting of an analysis of a video document of a competition in one of the specialty group's athletics disciplines combined events to assess the candidate's ability to observe, analyze and diagnose to provide training for an athlete or group of national athletes;
- meet the requirements for pedagogical situations, verified during the implementation of a 30-minute training session followed by a 20-minute interview. The candidate must be able to:- assess the objective risks associated with safe athletics practice
  - assess the objective risks associated with the activity for the practitioner;
  - control the behaviour and actions to be performed in the event of an incident or accident;
  - implement a training session integrated into an annual program.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* Articles A. 212-35 and A. 212-36 of the Code of Sport; Articles 3 and 5 of the November 8, 2010 order creating the "athletics: combined events" designation of the SPECIALty DEJEPS "sports performance."

##### DESJEPS specialty "sports performance" mention "modern pentathlon"

###### Prerogatives

The prerogatives are:

- Prepare a strategic performance project
- Piloting a training system
- Running a sports project
- Evaluate a training system
- organize training activities for trainers.

###### Conditions for access to training

The conditions for access to training are:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- produce a certificate of training and training activity issued by the National Technical Director of Modern Pentathlon;
- produce the certificate, issued by the National Technical Director of modern pentathlon, of the success of the test consisting of an event organised by the French Federation of Modern Pentathlon consisting of the analysis of a video sequence relating to one modern pentathlon events;
- complete a 40-minute training session followed by a 30-minute interview to verify the pre-pedagogical requirements. The candidate must be able to:- assess the objective risks associated with the practice of the discipline,
  - to assess the objective risks associated with the activity for the practitioner,
  - to control the behaviour and actions to be taken in the event of an incident or accident,
  - implement a modern pentathlon training situation.

#### Costs associated with qualification

Training to obtain CQP, DEJEPS and DESJEPS is paid for. Their costs vary from training organization to training organization. For more information, it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals* European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity there for at least the equivalent of a full-time year in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or perform an adjustment course (see below "d. compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as a coach of combined activities in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Reporting obligation (for the purpose of obtaining the professional sports educator card)

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Timeframe**

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

**Supporting documents**

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

**Cost**

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Timeframe**

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra" "compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

**Supporting documents**

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and following, A. 212-209 and Appendix II-12-3 of the Code of Sport.

### c. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. It may also decide to subject the national to an aptitude test or an adjustment course (see below: "d. compensation measures").

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Timeframe**

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

**Supporting documents for the first declaration of activity**

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France,
  - documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

**Evidence for a renewal of activity declaration**

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182, , A. 212-209 and Schedules II-12-2-a and II-12-b of the Code of Sport.

### d. Compensation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

