﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP187" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Sailing instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="sailing-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/sailing-instructor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="sailing-instructor" -->
<!-- var(translation)="Auto" -->


Sailing instructor
===============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The sailing instructor supervises multihull, dinghy and cruise activities up to 12 miles from a shelter and windsurfing activities. He practises with any public and at any place where the activity is practiced.

Beyond 12 miles of a shelter, the sailing instructor can cruise in a flotilla under the responsibility of a fleet leader, with the required diplomas.

*For further information*: Appendix 1 of the 9 July 2002 decree creating the speciality nautical activities of the professional youth, popular education and sport patent (BPJEPS).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

As a sports teacher, the sailing instructor must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

The activity of sailing instructor is governed by articles L. 212-1 and following of the Code of Sport which require certifications including the BPJEPS "nautical activities", the state diploma of youth, popular education and sport (DEJEPS) specialty "sports development" mention "sailing" and the state diploma of youth, popular education and sport (DESJEPS) specialty "sports performance" mention "sailing".

**Please note**

The Certificate of Professional Qualification (CQP) assistant sailing instructor allows to supervise the sailing activity under the responsibility of another instructor holding a state diploma.

*For further information*: Endorsement No.75 of October 4, 2002 relating to the CQP "sailing assistant monitor."

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*For further information*: Articles A. 212-1 Appendix II-1, L. 212-1 and R. 212-84 of the Code of Sport.

**Good to know: the specific environment**

The practice of sailing beyond 200 nautical miles from a shelter is an activity in a specific environment, involving compliance with specific safety measures. Therefore, only organizations under the tutelage of the Ministry of Sport can train future professionals.

*For further information*: Articles L. 212-2 and R. 212-7 of the Code of Sport.

#### Training

##### BPJEPS specialty "nautical activities" monovalent mention "sailing"

The BPJEPS "nautical activities" attests to the possession of the professional skills essential to the exercise of nautical activity instructor. It is a level 4 degree (such as a technical bachelor's degree, a traditional bachelor's degree or a technician's certificate).

Training may be common to several disciplines of nautical activities, but the candidate must choose the appropriate mention for his or her project. As far as sailing is concerned, this is the monovalent mention of "sail."

*For further information*: Article D. 212-20 of the Code of Sport and decree of 9 July 2002 establishing the speciality "nautical activities" of the BPJEPS.

###### Good to know

Holders of the "nautical activities" BPJEPS can pass the Certificate of Specialization (CS) "cruise" which allows to supervise the coastal or offshore cruise up to 200 miles from a shelter. For more information, it is advisable to refer to Appendix V of the decree of 9 July 2002 creating the speciality "nautical activities" of the BPJEPS.

**Prerogatives****

The BPJEPS "nautical activities" allows to practice the trade all year round and with all the public. The places where activities are conducted are varied: association, sports club, business, local authority, institution welcoming the elderly etc. The audiences that can be supervised are diverse: children, adults, the elderly, sportsmen, tourists, etc.

This diploma confers skills related to:

- coaching and involving discovery and initiation activities, including the first levels of competition in the field of sailing;
- Participation in the organization and management of its business;
- Participation in the operation of the organising structure of activities;
- participation in the maintenance and maintenance of equipment related to sailing activities.

*For further information*: Article 3 of the July 9, 2002 order creating the specialty "nautical activities" of the BPJEPS.

**Conditions for access to training leading to the PJEPS**

The person concerned must:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- produce the certificate or certificates justifying the relief of certain tests set by the decree creating the specialty, the mention of the diploma, or the supplementary certificate concerned;
- provide a medical certificate of non-contradictory to the practice of nautical activities less than three months old, upon entry into training;
- provide, for persons with disabilities, the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the The need to develop pre-certification tests, if necessary;
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- produce, for a supplementary certificate, a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- Licensed to drive "coastal" or "inland" sea pleasure craft;
- Hold the "Prevention and Civic Relief Level 1" (PSC1) teaching unit or its equivalent;
- produce a certificate of 100 metres freestyle, dive start and recovery of an underwater object at a depth of 2 metres, issued by a person who holds a certification of aquatic activities;
- produce the certificate of success with the prior requirements related to the candidate's personal practice in the media or media, issued by the national technical director of the French Sailing Federation;
- meet the pre-educational requirements:- transmit verbal instructions and ensure an oral presentation of a task,
  - ensure the physical and moral integrity of the public,
  - organize the practice procedures of the group or crew to optimize individual activity time,
  - organize and monitor a practice area in relation to security obligations,
  - prevent and intervene appropriately to ensure the safety of a group of practitioners or a crew,
  - perform an intervention with a practitioner or crew in difficulty,
  - mastering the operation of motorized vehicles necessary to supervise the practice.

For more information, it is advisable to refer to Schedules 3 and 4 of the 9 July 2002 decree creating the speciality "nautical activities" of the BPJEPS.

Additional exhibits may be required when enrolling in one of the training organizations. For more information, it is advisable to get closer to the relevant training organization.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to the decree of 9 July 2002 mentioned above.

*For further information*: Articles L. 212-2, D. 212-20 and following, A. 212-35 and A. 212-36 of the Code of Sport; order of 9 July 2002 establishing the speciality "nautical activities" of the BPJEPS.

##### DEJEPS specialty "sports development" mention "sail"

DEJEPS is a state diploma registered at Level III of the national directory of professional certifications.

It attests to the acquisition of a qualification in the exercise of a professional activity of coordination and supervision for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in full by the voice of the VAE. For more information, you can see[official website](http://www.vae.gouv.fr/) 212-40 of the Code of Sport.

*For further information*: Article D. 212-35 of the Code of Sport.

**DeJEPS sailing prerogatives**

The possession of DEJEPS attests, in the field of sailing within 200 nautical miles of a shelter, of the following skills:

- conduct a sport development approach for national competitors;
- Coordinating the implementation of a development project
- Develop activities for sports and educational purposes;
- Coordinating teams of volunteers and professionals
- conduct training activities with professionals and coaching volunteers;
- carry out tutoring actions.

*For further information*: Article 2 of the July 1, 2008 decree creating the "sail" designation of deJEPS specialty "sports development."

**Conditions for access to training leading to deJEPS sailing**

The person concerned must:

- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- produce the certificate or certificates justifying the relief of certain tests set by the decree creating the specialty, the mention of the diploma, or the supplementary certificate concerned;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Arrange pre-testing, if necessary, according to the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- produce, for a supplementary certificate, a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- Have a motor pleasure boat driver's licence, coastal option or equivalent;
- hold a certificate of success at a 100-metre freestyle course, with a dive start and a one metre floating obstacle on the surface. This certificate is issued by a person who holds a certification to supervise nautical activities;
- Hold a first aid training certificate (PSC1) or its equivalent;
- produce certificates of participation in the six competition events, of which at least one is equivalent to the 4th grade and one equivalent to the 3rd grade, as defined by the French Sailing Federation, issued by the organiser of these competitions or by the national technical director of sailing;
- produce a certificate of practical performance level issued by the National Sailing Technical Director. The level is assessed by at least two technical sailing frames appointed by the national technical director of sailing, on a support chosen by the candidate between a dinghy, a windsurfer, a catamaran or a habitable one;
- produce a certificate of voluntary or professional coaching carried out independently on at least two of the media mentioned above, issued by the national technical director of sailing;
- meet the pre-educational requirements verified when setting up an educational session followed by an interview:- be able to ensure the physical and moral integrity of the host audiences,
  - Be able to organize a practice area,
  - Be able to monitor activity in the practice area,
  - Be able to respond to an incident or accident in an appropriate manner with a group of practitioners or a crew,
  - Be able to analyze a monitoring and response device in accordance with current regulations,
  - be able to master the existing anti-doping system.

*For further information*: Articles A. 212-35 and A. 212-36 of the Code of Sport and Articles 3 and 5 of the July 1, 2008 Order creating the "sailing" designation of deJEPS specialty "sports development."

Under certain conditions, exemptions can be obtained for certain events.

*For further information*: Article 4 of the order of 1 and July 2008 creating the "sail" of the SPECIALty DEJEPS "sports development".

##### DESJEPS specialty "sports performance" mention "sailing"

DesJEPS is a top state diploma registered at Level II of the National Directory of Professional Certifications.

It attests to the acquisition of a qualification in the exercise of a professional activity of technical expertise and management for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

*For further information*: Article D. 212-51 of the Code of Sport.

This diploma can be obtained in full through the VAE. For more information, you can see[official website](http://www.vae.gouv.fr/) 212-56 of the Code of Sport.

**DesJEPS sailing prerogatives**

The possession of the DESJEPS attests, in the field of sailing within 200 nautical miles of a shelter, of the following skills:

- Prepare a strategic sailing performance project including territorial development;
- lead a sailing training system
- Optimize the performance of a work organization
- Organize training and trainers as part of the organisation's professional networks;
- run a sports organization.

*For further information*: Article 2 of the July 1, 2008 decree creating the "sail" designation of the SPECIALty "sports performance" desJEPS.

**Conditions for access to training leading to the DESJEPS Sailing**

The person concerned must:

- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- produce the certificate or certificates justifying the relief of certain tests set by the decree creating the specialty, the mention of the diploma, or the supplementary certificate concerned;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- provide, for persons with disabilities, the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the The need to develop pre-test tests as required in accord with the certification in question;
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- produce, for a supplementary certificate, a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- Hold the driver's licence for motor pleasure boats, a "coastal" option;
- Hold a certificate authorizing the use of radio
- hold a certificate of success at a 100-metre freestyle course, with a dive start and a one metre floating obstacle on the surface. This certificate is issued by a person who holds a certification to supervise aquatic activities;
- Hold a first aid training certificate (PSC1);
- produce the certificate of professional or volunteer sports coaching experience issued by the technical director of sailing to justify a professional or volunteer experience of sports coaching either 36 months combined and 2,400 18 months and 1,200 hours when the applicant justifies a minimum Level III coaching degree. This professional or volunteer experience in sports coaching should include three areas:- The responsibility for coordinating a team of instructors or coaches with a cumulative minimum duration of 8 months or 6 months for the candidate with a minimum Level III coaching diploma,
  - conducting training actions for sports coaching for a cumulative minimum of 30 days,
  - conducting training actions with a cumulative minimum duration of 30 days;
- produce certificates of participation in the six competition events, of which at least one is equivalent to the 4th grade and one equivalent to the 3rd grade, as defined by the French Sailing Federation, issued by the organiser of these competitions or by the national technical director of sailing;
- produce a certificate of practical performance level issued by the National Sailing Technical Director. The level is assessed by at least two technical sailing frames appointed by the technical director of the sail on a support chosen by the candidate between a dinghy, a windsurfer, a catamaran or a habitable one;
- produce a certificate of professional or volunteer sports coaching experience issued by the sailing technical director;
- meet the pre-educational requirements verified when setting up an educational session followed by an interview:- be able to ensure the physical and moral integrity of the host audiences,
  - Be able to organize a practice area,
  - Be able to monitor activity in the practice area,
  - Be able to respond to an incident or accident in an appropriate manner with a group of practitioners or a crew,
  - Be able to analyze a monitoring and response device in accordance with current regulations,
  - be able to master the existing anti-doping system.

*For further information*: Articles A. 212-35 and A. 212-36 of the Code of Sport; decree of 1 July 2008 creating the "sail" designation of the SPECIALty DESJEPS "sports performance".

Under certain conditions, exemptions can be obtained for certain events.

*For further information*: Articles 4 and 6 of the July 1, 2008 decree creating the "sail" designation of the SPECIALty "sports performance" desJEPS.

#### Costs associated with qualification

Training to obtain BPJEPS, DEJEPS or DESJEPS is paid for. Their costs vary depending on the mentions and the training organizations. For more details, it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*For further information*: Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*For further information*: Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as a sailing instructor in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*For further information*: Article L. 212-9 of the Code of Sport.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Reporting obligation (for the purpose of obtaining the professional sports educator card)

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

#### Competent authority

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the [official website](https://eaps.sports.gouv.fr).

#### Time

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

#### Supporting documents

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

#### Cost

Free.

*For further information*: Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

#### Competent authority

The prior declaration of activity should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP) of the department where the declarer wants to perform his performance.

#### Time

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

#### Supporting documents

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*For further information*: Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### c. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

#### Competent authority

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

#### Time

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

#### Supporting documents for the first declaration of activity

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

#### Evidence for a renewal of activity declaration

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*For further information*: Articles R. 212-88 to R. 212-91, A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

### d. Compensation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*For further information*: Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

