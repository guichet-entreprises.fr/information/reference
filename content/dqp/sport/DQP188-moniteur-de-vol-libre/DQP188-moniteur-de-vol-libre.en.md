﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP188" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Hang-gliding instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="hang-gliding-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/hang-gliding-instructor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="hang-gliding-instructor" -->
<!-- var(translation)="Auto" -->


Hang-gliding instructor
===================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The free-flying activity includes hang-gliding, paragliding, kite-flying and kitesurfing.

In hang glider and paraglider, the instructor designs and implements, independently, actions of animation, initiation and progression (hang glider or paraglider depending on the mention of the chosen diploma) until autonomy, ensuring the safety of the practising and third parties. He practices two-seater pedagogical flight, accompanies practitioners in the discovery and respect of the practice framework of free flight and participates in the operation of the structure.

The nautical aerotracté instructor supervises and animates the discovery and initiation activities including the first levels of competition in aerotracted skiing, for any public and on the nautical or terrestrial places of activity practice. The nautical aerotracted glide covers the activities of kite, land-based kite, water-drawn kite and towed nautical board called kitesurfing.

*To go further* : decree of 27 December 2007 establishing the speciality "free flight" of the professional certificate of youth, popular education and sport (BPJEPS), decree of 9 July 2002 establishing the speciality "nautical activities" of the BPJEPS.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The free flight monitor activity is subject to the application of Article L. 212-1 of the Code of Sport, which requires specific certifications. As a sports teacher, the free-flying instructor must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the [national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

Qualifications to practice as an instructor of:

- hang-gliding: the specialty "free flight" specialty "hang glider" designation, the state diploma of youth, popular education and sport (DEJEPS, specialty "sports development" mention "hang glider", the senior state diploma of youth, Popular Education and Sport (DESJEPS) speciality "sports performance" mention "hang glider";
- paraglider: the speciality BPJEPS "free flight" mention "paragliding", the SPECIALty DEJEPS "sports development mention "paraglider" and the SPECIALty DESJEPS "sports performance" mention "paragliding";
- kitesurfing: BPJEPS specialty "nautical activities" mention "aerotracté glide", the SPECIALty DEJEPS "sports development" mention "nautical aerotracted slides" and the SPECIALty DESJEPS "sports performance" mention "aerotracted slides" nautical watering cars";
- kite: BPJEPS specialty "nautical activities" mention "aerotracté glide", the SPECIALty DEJEPS "sports development" mention "nautical aerotracted slides" and the SPECIALty DESJEPS "sports performance" mention "aerotracted slides" Certificate of Specialization (CS) "kite" associated with BPJEPS, DEJEPS and DESJEPS.

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*To go further* Articles L. 212-1 and R. 212-84 of the Code of Sport.

**Good to know: the specific environment**

The practice of free flight, regardless of the area of evolution, with the exception of the aerobatic and combat kite, constitutes an activity carried out in a specific environment, involving the respect of specific safety measures. Therefore, only organizations under the tutelage of the Ministry of Sport can train future professionals.

*To go further* Articles L. 212-2 and R. 212-7 of the Code of Sport.

#### Training

##### BPJEPS specialty "free flight" mentions "paragliding" and "hang glider"

The BPJEPS free flight is a Level IV diploma. This specialty is issued under two mentions:

- paragliding;
- Gliding.

It can be prepared through initial training, learning or continuing education

In the case of a professional degree, the holder may practise either as a self-employed person or as an employee of a company or association.

Organized on the principle of alternation, the training takes place on an ongoing basis, lasting between 6 and 10 months (depending on the skills already acquired by the candidate before entering training).

**Prerogatives**

The holder of the BPJEPS specialty free flight can frame, in his mention (deltaplane or paraglider), pilots over the age of 14 (except public with reduced mobility) from the discovery of the activity to the autonomy of flight.

It can also carry out two-seater flights to all public (except public with reduced mobility). Its intervention is subject to compliance with the rules set by the authorities in the airspace in which it is located.

###### Conditions for access to training leading to the specialty "free flight" PJEPS

The person concerned must:

- Be of age on the day of entry into training;
- produce a medical certificate of non-contradictory to sports practice less than three months old;
- Hold the level 1 civic prevention and relief (PSC1) education unit on annual recycling and produce the annual recycling certificate;
- produce the certificate issued by the National Technical Director of Free Flight certifying participation, over the past three years and in the activity constituting the mention, at three rounds of competition in a national calendar, with a result, for each, in the first half of the ranking;
- meet the pre-educational requirements:- be able to identify in the environment the elements relevant to a safety practice (meteorology, aerology, site specificity and specific regulations),
  - Be able to present the characteristics of the material,
  - be able to fly in two-seaters,
  - Be able to lead a session to discover the activity,
  - Be able to use the teaching guidance tools,
  - be able to react when the safety of practitioners is put at stake.

For more information, it is advisable to get closer to the training organization in question.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* : order of 27 December 2007 establishing the speciality "free flight" of the BPJEPS.

##### BPJEPS specialty "nautical activities" mention monovalent "aerotractés"

The BPJEPS "nautical activities" attests to the possession of the professional skills essential to the exercise of nautical activity instructor. It is a level 4 degree (such as a technical bachelor's degree, a traditional bachelor's degree or a technician's certificate).

Training can be common to several disciplines, but the candidate must choose the appropriate mention for his or her project. For kitesurfing and kite flying, this is the monovalent reference to "aerotracted slides."

**Prerogatives**

The specialty "water activities" speciality mention "aerotractés" allows the instructor to supervise and animate discovery and initiation activities including the first levels of competition in kite, kite traction, water-drawn kite and kitesurfing.

**Conditions of access to training leading to obtaining the BPJEPS nautical activities mention aerotracted slides**:

- Be of age on the day of entry into training;
- produce a medical certificate of non-contradictory to sports practice less than three months old;
- Hold the PSC1 annual recycling unit and produce the annual recycling certificate;
- Holding the driver's licence for motor pleasure boats, a "coastal" option;
- produce a 100-metre freestyle swimming certificate, dived start, with recovery of an underwater object at a depth of 2 metres;
- pass the competency assessment:- insert themselves into the practice space, navigate in groups over a small space,
  - equip themselves with all the elements of personal protection, depending on the weather conditions of the site,
  - take off, land and immobilize its traction kite without safety assistance,
  - return to the preparation area after triggering the kite safety system for traction and emergency folding in the evolution zone,
  - make an emergency stop, operate with ease on different types of glide racks and pull kite,
  - navigate at all speeds with ease, fluidity and efficiency,
  - make 180-degree turns to the rolling machine on an edge without loss of glide,
  - evolve in the waves, perform various expression figures of a regional level of competition.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* : decree of 9 July 2002 establishing the speciality nautical activities of the BPJEPS.

##### DEJEPS specialty "sports development" mention "hang glider," "paraglider" and mention "nautical aerotracted slides"

DEJEPS is a Level 3-certified diploma (Bac 2/3). It attests to the acquisition of a qualification in the exercise of a professional activity of coordination and supervision for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma is prepared alternately by initial training, apprenticeship or continuing education. It can be obtained in its entirety through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr/).

**Prerogatives**

The possession of the DEJEPS, which is marked "hang glider," "paraglider" or "nautical aerotractés" attests to the following skills in the chosen area:

- Design sports development and competition programs
- Coordinating the implementation of a sports development and competition access project;
- conducting a sports development approach and access to competition;
- conduct training activities
- for the mention of "aerotractés slides", develop sports and educational activities, conduct actions towards specific audiences, develop activities for specific audiences, coordinate a team of professionals and volunteers at scale of a project.

###### Conditions for access to training leading to DEJEPS mentions "hang glider" and "paraglider"

The person concerned must:

- Be of age on the day of entry into training;
- produce a medical certificate of non-contradictory to sports practice less than three months old;
- produce certificates of participation in five rounds of paragliding or hang glider competition, depending on the chosen mention, in a national calendar over the past three years, with one result for each in the first third of the classification, issued by the National Technical Director of Free Flight;
- produce certificates to justify a 400-hour experience in the field of teaching supervision over the past three years;
- successfully carry out the interview based on an experience of paragliding or hang glider teaching coaching;
- be able to frame the paraglider or hang glider, depending on the chosen mention, in safety (pre-pedagogical requirement).

Under certain conditions, exemptions can be obtained for certain events.

*To go further* : Orders of December 27, 2007 creating the "paraglider" of the SPECIALty DEJEPS "sports development" and creating the "hang glider" designation of the SPECIALty DEJEPS "sports development".

###### Conditions for access to training leading to deJEPS mention "nautical aerotracted slips"

The person concerned must:

- Be of age on the day of entry into training;
- produce a medical certificate of non-contradictory to sports practice less than three months old;
- produce a certificate of effective participation in three national level competitions in the field of nautical aerotracted skies or a certificate of success in a technical test consisting of a performance evaluated by the technical director national free flight (the test is organised by the French Free Flight Federation and the certificate issued by the national technical director of free flight);
- produce a certificate justifying in the field of aerotracted slides in the last five years of a total of 1,000 hours of experience in teaching, training, tutoring, training, competition or project coordination . The certificate is issued by the national technical director of free flight;
- pass an interview based on a dossier prepared by the candidate relating to his experience coaching, teaching, training, tutoring, training, or project coordination, in the field of nautical aerotracted slides;
- pass a coaching session with three people for progression and followed by an interview to verify the requirements for the following pedagogical situation: to assess the objective risks associated with the practice of the discipline in aerological and maritime areas and technically intervene on incidents, organize the practice area and monitor the activity, organise the rescue trigger in accordance with current regulations, prevent risky behaviours including doping and implementing a formative situation.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* : decree of January 5, 2010 creating the "nautical aerotractés" designation of the SPECIALty DEJEPS "sports development".

##### DESJEPS specialty "sports performance" mention "hang glider," "paraglider" and mention "nautical aerotracted slides"

The DESJEPS is a degree certified at level 2 (bac 3/4). It is prepared as an alternating in initial training, apprenticeship or continuing education. It can be obtained in its entirety through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr/).

**Prerogatives**

The possession of the speciality DESJEPS "sports performance" mention "hang glider," "paraglider" or mention "nautical aerotracted slips" attests, in the chosen field, to the following skills:

- Prepare a strategic performance project
- Piloting a training system
- Running a sports project
- Evaluate a training system
- Organize training activities for free-flying trainers;
- for the words "nautical aerotracated slides," conduct sports training activities.

###### Conditions for access to training leading to the DESJEPS designation "hang glider" and "paragliding"

The person concerned must:

- Be of age on the day of entry into training;
- produce a medical certificate of non-contradictory to sports practice less than three months old;
- produce certificates of participation in five rounds of hang glider or paragliding competition, depending on the mention chosen, in a national calendar over the past five years, with one result, for each, in the first third of the classification, issued by the National Technical Director of Free Flight;
- produce certificates to justify 800 hours of experience over the past five years in the areas of teaching, training, tutoring, training or free flight project coordination;
- successful interview based on a record of teaching, training, tutoring, training or coordination of a free flight project;
- be able to frame the paraglider or hang glider, depending on the chosen mention, in safety (pre-pedagogical requirement).

Under certain conditions, exemptions can be obtained for certain events.

*To go further* : Orders of 27 December 2007 creating the "paraglider" of the SPECIALty "sports performance" DESJEPS and creating the "hang glider" designation of the SPECIALty "sports performance" DESJEPS.

###### Conditions for access to training leading to the DESJEPS designation "nautical aerotracated slips"

The person concerned must:

- Be of age on the day of entry into training;
- produce a medical certificate of non-contradictory to sports practice less than three months old;
- produce a certificate of effective participation in three national level competitions in the field of nautical aerotracted skies and a certificate of success in a technical test consisting of a performance evaluated by the technical director National Free Flight;
- produce a certificate justifying in the field of aerotracted slides in the last five years of a total of one thousand hours of experience in teaching, training, tutoring, training, competition or coordination of Project;
- successfully conduct an interview based on a dossier prepared by the candidate relating to his experience in coaching, teaching, training, tutoring, training or project coordination, in the field of nautical aerotracted slides;
- pass a three-person coaching session, aiming for progression and followed by an interview to verify the following pre-pedagogical requirements: assess the objective risks associated with the practice of the discipline in the aerological and maritime areas and technically intervene on incidents, organize the practice area and monitor activity, organise the rescue trigger in accordance with current regulations, prevent risky behaviours including doping, implement a formative situation.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* : decree of January 5, 2010 creating the label "nautical aerotractés" of the specialty DESJEPS "sports performance".

##### CS "kite" associated with BPJEPS, DEJEPS and DESJEPS

The kite CS attests to the skills of the holder to be ensured in the field of kite, in educational autonomy, the conduct of discovery, animation and initiation services as well as learning cycles up to the first level of competition in kite activities.

The CS kite can be associated with:

- the BPJEPS specialty "physical activity for all," specialty "water activities" and specialty "free flight";
- specialty DEJEPS "sports development" mention "sail," "nautical aerotractés," "hang glider" and "paragliding";
- desJEPS speciality "sports performance" mention "sailing" and mention "nautical aerotractés.

*To go further* : decree of January 5, 2010 establishing the CS "kite" associated with BPJEPS, DEJEPS specialty "sports development" and DESJEPS specialty "sports performance".

#### Costs associated with qualification

Training to obtain BPJEPS, DEJEPS and DESJEPS is paid for. Their costs vary depending on the training organization. For [more details](http://foromes.calendrier.sports.gouv.fr/#/formation), it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

#### If the Member State of origin regulates access or the exercise of the activity:

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

#### If the Member State of origin does not regulate access or the exercise of the activity:

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as a free flight instructor in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Advance Declaration/Professional Card

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

#### Competent authority

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

#### Time

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

#### Supporting documents

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

#### Cost

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

### b. Pre-declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

#### Competent authority

The prior declaration of activity should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP) of the department where the declarer wants to perform his performance.

#### Time

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

#### Supporting documents

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### c. Pre-declaration of activity for EU nationals for permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

#### Competent authority

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

#### Time

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

#### Supporting documents for the first declaration of activity

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

#### Evidence for a renewal of activity declaration

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

### d. Compensation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### Outcome of the procedure

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

