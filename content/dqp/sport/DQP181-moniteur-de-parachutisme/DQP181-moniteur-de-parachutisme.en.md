﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP181" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Skydiving instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="skydiving-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/skydiving-instructor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="skydiving-instructor" -->
<!-- var(translation)="Auto" -->


Skydiving instructor
=================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The skydiving instructor independently supervises the tandem, the accompanied progression in free fall or the traditional progression according to the chosen mention. The instructor designs an animation project in the skydiving field. He conducts discovery and learning activities in the field of jumping. Finally, it organizes the safety of the public and participates in the operation of the structure.

A jump involves dropping an aircraft (e.g. an aircraft) with a parachute, and sometimes other accessories, from a height that can be 1,000 to 6,000 meters from the ground, depending on the discipline practiced. After the outing, the paratrooper is in free fall for a longer or shorter period depending on the discipline practiced and the height at which he was dropped. He can perform figures alone or with other people before opening his parachute. The minimum regulatory threshold for opening is 850 metres.

The skydiving activity also includes the activities of nautical ascent and wind tunnel flying.

*To go further* : Appendix I of the decree of 11 July 2011 establishing the speciality "parachuting" of the professional certificate of youth of popular education and sport (BPJEPS); Appendix I of the 9 July 2002 decree establishing the speciality "nautical activities" of the BPJEPS; Appendix II-1 (Article A. 212-1) of the Code of Sport.

2°. Professional qualifications
----------------------------------------

### National requirements

#### National legislation

The parachuting instructor activity is subject to the application of Article L. 212-1 of the Code of Sport, which requires specific certifications.

As a sports teacher, the skydiving instructor must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

Qualifications for parachuting are the specialty "parachuting" BPJEPS, the State Diploma of Youth, Popular Education and Sport (DEJEPS) specialty "sports development" mention "parachuting" and the State diploma of youth, popular education and sport (DESJEPS) specialty "sports performance" mention "parachuting".

The qualification to practice as an ascent parachute instructor is the specialty BPJEPS "water activities".

The qualification for the wind tunnel is the Certificate of Professional Qualification (CQP) "flat wind tunnel monitor" with, if necessary, the additional qualification "3D wind tunnel flight".

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*To go further* Articles A. 212-1 Appendix II-1, L. 212-1 and R. 212-84 of the Code of Sport.

**Good to know**

The specific environment. The practice of skydiving is an activity carried out in a specific environment regardless of the area of evolution. It involves compliance with special security measures. Therefore, only organizations under the tutelage of the Ministry of Sport can train future professionals.

*To go further* Articles L. 212-2 and R. 212-7 of the Code of Sport.

#### Training

##### CQP "flat flight monitor in wind tunnel"

This is a diploma managed by the French Parachute Federation (FFP).

The training is carried out alternately between the training organisation and the host structure. In-house training lasts at least 140 hours. The total duration of the training is 320 hours.

This diploma is open to validation of experience (VAE).

**Prerogatives**

The CQP "flat-flying breather monitor" allows for the autonomous supervision of flat-to-wind flight activities for any public.

*To go further* Appendix II-1 (Article A. 212-1) of the Code of Sport.

**Conditions for access to training leading to the CQP**

The person concerned must:

- Be eighteen on the day of registration
- submit a medical certificate of non-contradictory to the practice less than three months old;
- Hold the Level 1 Civic Prevention and Relief Education Unit (PSC1) or its equivalent;
- submit a certificate of success to the six technical and physical tests of entry into training defined by the French Parachute Federation and issued by it.

For more information, it is advisable to consult the[FFP website](http://www.ffp.asso.fr/wp-content/uploads/2013/03/presentation-CQP-soufflerie.pdf).

**Good to know**

The CQP "flat wind tunnel monitor" can be accompanied by the additional "3D wind tunnel flight" qualification, which allows 3D flight activities to be monitored in the wind tunnel.

##### BPJEPS specialty "parachuting"

The BPJEPS is a state diploma registered in the[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) and classified at level IV of the certification level nomenclature.

It attests to the acquisition of a qualification in the exercise of a professional activity in responsibility for educational or social purposes, in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in full through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr). In the context of an VAE, the candidate will also have to be put into a professional situation.

*To go further* Articles D. 212-20 and D. 212-24 of the Code of Sport; Appendix VIII of the July 11, 2011 decree creating the specialty "parachuting" of the BPJEPS.

Three mentions are possible:

- the so-called traditional method training (TRAD) that begins with automatic opening jumps at a minimum height of 1,000 metres, followed by "control handles" (jumps in which the student makes the gesture of pulling on the opening handle of his parachute, but where it is actually opened by the automatic aperture strap that connects it to the aircraft, allowing the wing to be opened even if the student's gesture is incorrect). After two consecutive successful jumps in a control handle, the student is allowed to jump in manual opening and higher and higher, until gradually reaching the height of 4,000 meters;
- the accompanied fall progression (PAC) that allows the student to jump in free fall from a minimum height of 3,000 meters. For the first jump, the student is accompanied by two instructors, who monitor and correct his position during the fall. The next five jumps can be accompanied by a single instructor, the objective being to be able to jump alone at the 7th jump;
- Tandem jumping involves jumping on an already experienced instructor. This allows you to discover the sensations of free fall for anyone over the age of fifteen. The jump takes place at an altitude of at least 3,000 meters for a free fall of about fifty seconds, before the parachute opens at an altitude of 1,500 meters. It allows you to discover the sport in one jump.

**BPJEPS prerogatives**

- Design and implement actions of animation, initiation, teaching, supervision and progression to autonomy, ensuring the safety of practitioners and third parties;
- accompany practitioners in discovering and respecting the skydiving practice framework;
- participate in the operation of the structure.

*To go further* : order of 11 July 2011 establishing the speciality "parachuting" of the BPJEPS.

**Conditions for access to training leading to the PJEPS:**

The person concerned must:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- for a supplementary certificate, produce a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- produce a certificate of three years of continuous experience in skydiving practice, delivered by the National Technical Director of Skydiving;
- for the "traditional" designation, provide the certificate of five hundred free-fall jumps, including one hundred jumps in the last twelve months prior to the date of the registration file, issued by the National Technical Director of Skydiving;
- for the mention of "accompanied progress in a fall", provide the certificate of eight hundred free-fall jumps, including one hundred jumps in the last twelve months prior to the date of the filing of the registration file, issued by the National Technical Director of the skydiving;
- for the "tandem" designation, provide the certificate of one thousand free-fall jumps, including one hundred jumps in the last twelve months prior to the date of the registration file, and a tandem jump in a student position dating back less than six months under the guidance of a tandem instructor authorized by the National Technical Director of Skydiving;
- provide the certificate of success in the two-part technical and safety tests, issued by the National Technical Director of Skydiving;
- meet the pre-educational requirements attested by the head of the training organization during a jump session based on the reference. The candidate must be able to:- to mobilize safety-related knowledge in practice,
  - to mobilize the technical knowledge to verify the airworthiness of the equipment,
  - to prepare the materials and teaching tools,
  - to demonstrate technical mastery,
  - to explain the different techniques,
  - mastering professional techniques,
  - to frame the practice of the discovery of the activity up to autonomy,
  - to introduce the practice framework,
  - to assess its action.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* Articles A. 212-35 and A. 212-36 of the Code of Sport; Appendixes III, IV and V of the July 11, 2011 decree creating the specialty "parachuting" of the BPJEPS.

##### BPJEPS specialty "nautical activities" multivalent mention "nautical ascent parachute"

The BPJEPS "nautical activities" attests to the possession of the professional skills essential to the exercise of nautical activity instructor. It is a level IV degree (such as a technical bachelor's degree, a traditional bachelor's degree or a technician's certificate).

This diploma can be obtained in full through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr).

Training may be common to several disciplines of nautical activities, but the candidate must choose the appropriate mention for his or her project. With regard to nautical ascent skydiving, this is the multivalent reference to "nautical ascent parachuting" (Group G).

*To go further* Article D. 212-20 of the Code of Sport; Articles 1 and 2 of the July 9, 2002 order creating the specialty "nautical activities" of the PJEPS.

**Good to know**

The complementary capitalized unit (UCC) "nautical ascent skydiving" allows to frame this activity and can be associated with each of the mentions of the BPJEPS "nautical activities".

**BPJEPS prerogatives:**

- mentoring and animate discovery and initiation activities, including the first levels of competition;
- Participate in the organization and management of its business
- Participate in the operation of the organising structure of the activities;
- participate in the maintenance and maintenance of equipment.

*To go further* Article 3 of the July 9, 2002 order creating BPJEPS' specialty of "nautical activities."

**Conditions for access to training leading to the PJEPS:**

The person concerned must:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- produce the certificate or certificates justifying the lightening of certain tests;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- for a supplementary certificate, produce a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- provide a medical certificate of non-contradictory to the practice of nautical activities less than three months old, upon entry into training;
- Holding the PSC1 teaching unit or its equivalent;
- produce a certificate of success with the prior requirements related to the candidate's personal practice and issued by the national technical director of the French Parachute Federation concerned or by an expert appointed by the regional director of the youth, sports and leisure;
- Be a "coastal" sea or inland water licence holder;
- Be able to manage the player's flight, on a minimum of five flights, in the chosen mode of practice (beach or platform boat);
- meet the pre-educational requirements. The candidate must be able to:- manage the different phases of nautical ascent skydiving flight, in a mode of practice of your choice (beach or platform boat),
  - to know the specific material of the chosen mode of practice,
  - recall current regulations and safety rules,
  - to take charge of a group in an introductory session,
  - manage the area of practice and the evolution of the tractor boat taking into account safety constraints,
  - to perform an intervention with a practitioner in difficulty.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* Articles A. 212-35 and A. 212-36 of the Code of Sport; Article 5, Appendix III and IV of the July 9, 2002 order creating the specialty "nautical activities" of the PJEPS.

##### DEJEPS specialty "sports development" mention "parachuting"

DeJEPS a state diploma registered at Level III of the[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

It attests to the acquisition of a qualification in the exercise of a professional activity of coordination and supervision for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in full through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr) and Article D. 212-40 of the Code of Sport.

*To go further* Article D. 212-35 of the Code of Sport.

**DEJEPS prerogatives:**

- Designing a skydiving school project
- Coordinating the implementation of the skydiving school project;
- Mentoring practitioners of all levels in a process of sports development;
- Organize a safe jump session
- manage the equipment used by practitioners to ensure their safety.

**Conditions for access to training leading to DEJEPS:**

The person concerned must:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- for a supplementary certificate, produce a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- provide certification of a continuous practice of skydiving for 4 years including at least eight hundred jumps issued by the National Technical Director of Skydiving;
- provide certification of coaching experience in skydiving lasting a minimum of 600 hours in the last four years prior to entry into training, delivered by the National Technical Director of Skydiving;
- meet the pre-educational requirements verified when setting up a half-day pedagogical session followed by a 30-minute interview. The candidate must be able to:- assess the objective risks associated with the practice,
  - to anticipate the potential risks associated with the activity for the practitioner,
  - to control the behaviour and actions to be performed in the event of an accident or incident,
  - to conduct a safe "school jumps" session.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* Articles A. 212-35 and A. 212-36 of the Code of Sport; Articles 3, 4 and 6 of the July 11, 2011 decree creating the deJEPS specialty "sports development" designation "parachuting."

##### DESJEPS specialty "sports performance" mention "parachuting"

The DESJEPS is a higher state diploma enrolled at Level II of the[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

It attests to the acquisition of a qualification in the exercise of a professional activity of technical expertise and management for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in full through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr) and Article D. 212-56 of the Code of Sport.

*To go further* Article D. 212-51 of the Code of Sport.

**DESJEPS prerogatives:**

- Prepare a strategic performance project
- Piloting a training system
- Running a sports project
- Evaluate a training system
- organize, facilitate and evaluate training actions for trainers.

*To go further* : decree of 11 July 2011 creating the word "parachuting" of the DESJEPS.

**Conditions for access to training leading to DESJEPS:**

The person concerned must:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- for a supplementary certificate, produce a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- produce the certification of a continuous practice of skydiving for five years including at least a thousand jumps issued by the National Technical Director of Skydiving;
- produce a test certification consisting of the technical analysis of a video document on a parachuting competition discipline issued by the National Technical Director of Skydiving;
- produce a certificate of training experience in the field of skydiving lasting a minimum of 150 hours during the last five years issued by the National Technical Director of Skydiving;
- meet the pre-educational requirements verified when setting up a training session or a half-day training action followed by a 30-minute interview. The candidate must be able to:- assess the objective risks associated with the practice of the discipline,
  - to assess the objective risks associated with the activity for the practitioner,
  - to control the behaviour and actions to be taken in the event of an incident or accident,
  - implement a training session or training action.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* Articles A. 212-35 and A. 212-36 of the Code of Sport; Articles 3, 4 and 5 of the July 11, 2011 decree creating the "parachuting" designation of the specialty DESJEPS "sports development."

#### Costs associated with qualification

Training for CQP, BPJEPS, DEJEPS and DESJEPS is paid for. The cost varies depending on the mentions and the training organizations.

For more details, it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as a skydiving instructor in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Reporting obligation (for the purpose of obtaining the professional sports educator card)

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Timeframe**

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

**Supporting documents**

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

**Cost**

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

**Competent authority**

The prefect of the Provence-Alpes-Côte d'Azur region.

**Timeframe**

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

**Supporting documents**

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and following, A. 212-209 and Appendix II-12-3 of the Code of Sport.

### c. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

**Competent authority**

The prefect of the Provence-Alpes-Côte d'Azur region.

**Timeframe**

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

**Supporting documents for the first declaration of activity**

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

**Evidence for a renewal of activity declaration**

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182, , A. 212-209 and Schedules II-12-2-a and II-12-b of the Code of Sport.

### d. Compensation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

