﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP179" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Canoe-kayak instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="canoe-kayak-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/canoe-kayak-instructor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="canoe-kayak-instructor" -->
<!-- var(translation)="Auto" -->


Canoe-kayak instructor
===================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The canoe-kayak instructor prepares, organizes and conducts introductory and advanced canoeing sessions in calm water, white water or at sea, depending on his training. It independently supervises walks and hikes based on the educational references defined by the French Canoe-Kayak Federation (FFCK) by ensuring the safety of practitioners and third parties.

The canoe-kayak instructor organizes educational situations adapted in time and space to the specifics and expectations of the practitioners. He is involved in the organization and evaluation of technical level crossing sessions.

Depending on his professional qualifications, the canoe-kayak instructor may have limited prerogatives (depending on his diploma, he will be able to supervise any public within certain geographical limits and in certain weather conditions).

*For further information*: order of August 12, 2013 registering in the National Directory of Professional Certifications (RNCP) of the "Professional Qualification Certificate (CQP) canoe-kayak instructor option "canoe-kayak in calm water - white water" and "canoe-kayak in water calm - sea," decree of 9 July 2002 establishing the speciality "nautical activities" of the professional certificate of youth, popular education and sport (BPJPS), decreed from 1 July 2008 bearing the creation of the words "canoe-kayak and disciplines associated with calm water" and "canoe-kayak and related disciplines in white water" from the State Diploma of Youth, Popular Education and Sport (DEJEPS) and decreed from 1 July 2008 with the creation of the words "canoe-kayak and disciplines" associated with calm water" and "canoeing and associated disciplines in white water and at sea" of the Senior State Diploma of Youth, Popular Education and Sport (DESJEPS).

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

As a sports teacher, the canoe-kayak instructor must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

The canoe-kayak instructor activity is governed by articles L. 212-1 and the following articles of the Sports Code that require certifications including the CQP, the professional aptitude certificate of assistant facilitator technician of the youth and sports (BAPAAT), the BPJEPS "nautical activities", the SPECIALty DEJEPS "sports development" mention "canoe-kayak and associated disciplines (CKDA) in calm water" or "CKDA in white water" and the DESJEPS "speciality sports performance" mention " CKDA in calm water" or "CKDA in white water and sea."

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*For further information*: Articles A. 212-1 Appendix II-1, L. 212-1 and R. 212-84 of the Code of Sport.

**Good to know: the specific environment**

The practice of canoeing and associated disciplines in the upper three-class river is an activity in a specific environment involving compliance with specific safety measures. Therefore, only organizations under the tutelage of the Ministry of Sport can train future professionals.

*For further information*: Articles L. 212-2 and R. 212-7 of the Code of Sport.

#### Training

##### CQP canoe-kayak instructor

The CQP canoe-kayak instructor is a professional and casual coaching diploma managed by the FFCK.

There are two options:

- calm water - white water;
- calm water - sea.

This diploma can be obtained through continuing vocational training, apprenticeship or validation of experience (VAE). For more information, you can see[official website](http://www.vae.gouv.fr/) VAE.

**Please note**

The CQP is also a springboard to the teaching professions of paddle sports. This is a first step towards state degrees and in particular towards the BPJEPS, which has wider prerogatives (supports and limits of conditions of practice) and allows full-time supervision.

**Prerogatives of the CQP**

The CQP canoe-kayak instructor holder can supervise, animate and teach paddle sports within 360 hours per year:

- options calm water - white water and calm water - sea: in calm water, the instructor supervises all paddle sports;
- Calm water option - white water: in white water, the instructor supervises all paddle sports except the paddle raft in difficulty courses reaching class II included with non-successive Class III passages;
- calm water-sea option: at sea, the instructor supervises all paddle sports up to a mile from a shelter in conditions of difficulty wind of maximum force 3 Beaufort and swell of one meter on the site of evolution.

**Conditions for access to training leading to the CQP:**

The person concerned must:

- Be 18 years old on the day of entry into training;
- attest to a level of canoeing at least equal to that of the "blue calm water paddle";
- pass the entrance tests
- Provide a copy of the national ID card
- communicate a medical certificate of non-contradictory to the practice and teaching of canoeing and paddle sports;
- Holding the Level 1 Civic Prevention and Relief Education Unit (PSC1);
- certification of a swimming-rescue level consistent with the standard form to be attached to the registration file.

Additional documents may be required when registering with one of the training organizations (e.g. census certificate). For more information, it is advisable to get closer to the organization concerned.

Under certain conditions, exemptions can be obtained for certain tests, especially for those with a federal diploma. For more information, it is advisable to refer to the [FFCK website](http://www.ffck.org/federation/formation/encadrement-professionnel/#1454081500030-1e5f308b-6d61).

*For further information*: Articles L. 212-1 and R. 212-84 of the Code of Sport.

##### BAPAAT option "outdoor recreation", technical support "water hiking, canoeing" or "water hiking, raft" or "water hiking, white water swimming" or "water hiking, sea kayaking"

It is a state diploma approved at level V (CAP, BEP, BEPC), common to the socio-cultural and sports sector. The BAPAAT represents the first level of qualification for the animation and supervision of sports and socio-cultural activities.

The options of BAPAAT opening access to the canoe-kayak frame are the BAPAAT outdoor leisure option, technical support:

- water-hiking, canoeing;
- water-hiking, raft;
- water hiking, white water swimming;
- or water hiking, sea kayaking.

The training takes place alternately. The volume of training includes 1,500 to 2,000 hours of general, technological and professional instruction.

**Conditions for access to training leading to BAPAAT**

- Be at least 16 years old
- no prerequisite for a degree, but a good level of personal practice is recommended.

*For further information*: Articles D. 212-11 and following of the Code of Sport, A. 212-2 and following, order of 4 March 1993 relating to the creation and organisation of BAPAAT's professional options, order of 10 August 1993 relating to the list of socio-cultural activities that can be used as a support for the professional activity of BAPAAT holders.

##### BPJEPS "nautical activities"

The BPJEPS nautical activities attest to the possession of the professional skills essential to the exercise of nautical activity instructor. It is a level 4 degree (such as a technical bachelor's degree, a traditional bachelor's degree or a technician's certificate).

Training may be common to several disciplines, but the candidate must choose the "mentions" that are appropriate for his or her project. He can choose a single mention to be a specialist in a discipline (that's the monovalent designation) or several options to respond to a more generalist profile (that's the multivalent mention).

The monovalent reference to paddle sports is called "BPJEPS nautical activities mention CKDA". The generalist candidate can choose at least 2 multi-candidate options in two different groups:

- Group A: canoeing "calm water, sea and waves" or canoeing "calm water and whitewater river";
- Group B: sea rowing - initiation and discovery rowing;
- Group C: sailing tank;
- Group D: coastal cruise - multihull and dinghy - windsurfing;
- Group E: initiation and discovery water skiing;
- Group F: jet - initiation and discovery motorboat - towed gear;
- Group G: nautical ascent skydiving;
- Group H: Aerotracted slide.

The candidate is in vocational training and works under the authority of a tutor. The training takes place alternately between the training centre and the structure for the reception of professional situations. It lasts a total of 600 hours in a training centre and 600 hours in structure for initial training, spread over 12 to 24 months depending on the organization.

**Please note**

Holders of the "BEES 1 canoe kayak and related disciplines" can obtain the BPJEPS water activities as "canoe kayak and related disciplines" as "canoeing and related disciplines". It is possible to apply to the Regional Directorate of Youth Sports and Social Cohesion (DRJSCS).

**Prerogatives****

The BPJEPS nautical activities allows to supervise for remuneration in full autonomy. The places where activities are conducted are diverse and varied: association, sports club, business, local authority, institution for the elderly, etc. The audiences that can be supervised are diverse: children, adults, the elderly, sportsmen, tourists, etc.

Exercise limits:

- CKDA monovalent mention: The instructor supervises and conducts discovery and initiation activities, including the first levels of competition on any support or boat powered by paddle or swim. It occurs only in calm water, at sea in maximum force 4 wind on the site of evolution, in white water up to Class III included as well as in white water up to class III included and in canyons listed up to including V1, A5 and E II;
- multivalent mention "canoe-kayak calm water, sea and waves": the monitor supervises the activities of the CKDA for all public, in calm water and at sea, within the limits of navigation in sixth category on known and recognized routes, at most by force wind 4 on the evolution site. He also conducts introductory sessions in wave kayaking;
- multivalent mention "canoe-kayak calm water and whitewater river": the instructor supervises the activities of the CKDA for all public, in calm water and river, up to Class III included.

**Conditions for access to training leading to the PJEPS**

The person concerned must:

- Be of age
- Have a certificate of first aid training (PSC1, AFPS or equivalent);
- pass the entrance exams at the training institute;
- produce a medical certificate of non-contradictory to the practice of nautical activities, dating back less than three months to the date of entry into training;
- produce a certificate issued by a lifeguard of 100 meters freestyle, dive start and recovery of an object submerged at a depth of 2 meters;
- to meet sports events (called pre-requirement tests) detailed in Schedule III of the 9 July 2002 order and whose content depends on the chosen mention or mentions.

Additional documents may be required when registering with one of the training organizations (such as a census certificate). For more information, it is advisable to get closer to the relevant training organization.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to the decree of 9 July 2002 mentioned above.

*For further information*: Articles L. 212-2 and D. 212-20 and following of the Code of Sport, order of 9 July 2002 establishing the specialty "nautical activities" of the BPJEPS.

**Good to know**

BpJEPS holders specialty "water activities" can expand their prerogatives by passing a complementary capitalized unit (UCC) "canoe-kayak, calm water and whitewater river" or "canoe-kayak, calm water, sea and waves". For more information, it is advisable to refer to the [Site](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/BPJPES/Reglementation-11011/Les-unites-capitalisables-complementaires/) Ministry of Sport.

##### DEJEPS specialty "sports development" mention "CKDA in calm water" or "CKDA in white water"

DEJEPS is a Level III-approved diploma (Bac 2/3). It attests to the acquisition of a qualification in the exercise of a professional activity of coordination and supervision for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in initial training by taking a training of 700 hours in the centre and 500 hours in the alternating structure and by validating the experience.

DeJEPS diplomas that allow you to carry out the canoe-kayak instructor activity are:

- DEJEPS mentions CKDA calm water;
- DEJEPS mentions CKDA white water;
- and the Sea Specialization Certificate "CKDA at Sea."

The diploma is prepared through initial training, learning or continuing education.

**DEJEPS prerogatives**

The skills targeted are:

- sports training in the clubs and structures of the FFCK's Sports Excellence Course (PES);
- those of river and/or sea guide technician, mainly in sports leisure facilities.

**Conditions for access to training leading to the DEJEPS specialty "sports development" mention "CKDA in calm water" or "CKDA in white water":**

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- certificate of first aid training (PSC1 type, AFPS);
- pass the tests for access to training;
- communicate a medical certificate of non-contradictory to the teaching and practice of CKDA in calm/white water (as indicated chosen), less than three months old;
- produce a certificate of independent coaching of a minimum of six hundred hours or three sports seasons, issued by the national technical director of the CKDA (DTNCKDA);
- produce a certificate, issued by the DTNCKDA, of participation in a regional or first-level national boat competition in two different environments, one of which is either in white water or in calm water according to the chosen mention;
- produce a certificate of success of the technical and pedagogical test organised by the FFCK;
- produce a certificate of success of a 100-metre free-swimming test with a dive start and recovery of a submerged object at a depth of 2 metres.

Additional documents may be requested (copy of the national identity card, certificate of participation in the day of appeal to the defence, certificate of civil liability, etc.). For more information, it is advisable to check with the training centre in question.

Under certain conditions, exemptions can be obtained for certain events. For more information, it is advisable to refer to articles 4, 6 and following of the orders of 12 July 2007 mentioned above.

*For further information*: Articles D. 212-35 and the following of the Code of Sport, ordered on July 1, 2008 creating the "CKDA in calm water" designation of the SPECIALty DEJEPS "sports development" and ordered from July 1, 2008 creating the word "CKDA in white water" of DEJEPS.

**Good to know**

DeJEPS holders mention "canoe-kayak and associated disciplines in calm water" or "canoe-kayak and associated disciplines in white water" can expand their prerogatives by passing the Certificate of Specialization (CS) "canoe-kayak and disciplines associated with them at sea." For more details on the conditions under which this CS is obtained, it is advisable to refer to the [official website of this certificate](http://www.sports.gouv.fr/).

##### DESJEPS specialty "sports performance" mention "CKDA in calm water" or "CKDA in white water and sea"

The DESJEPS is a level II-approved diploma (B.A. 3/4).

This diploma can be obtained in initial training by taking a training of 700 hours in the centre and 500 hours in the alternating structure and by validating the experience.

The diplomas that allow you to carry out the canoe-kayak instructor activity are:

- DESJEPS mentions CKDA calm water;
- DESJEPS mentions CKDA white water and sea;
- CKDA Sea Certificate of Specialization (CS).

The training course is based on the principle of alternating between the training centre and the company (where practical learning situations are the responsibility of a tutor). The diploma can also be obtained through the VAE.

**Good to know**

DesJEPS holders are marked "canoe-kayak and associated disciplines in calm water" or "canoeing and associated disciplines in white water and sea" can expand their prerogatives by passing the Certificate of Specialization (CS) "canoe-kayak and associated disciplines at sea." For more details on the conditions under which this CS is obtained, it is advisable to refer to the [official website of this certificate](http://www.sports.gouv.fr/emplois-metiers/decouvrir-nos-offres-de-formations/des-jeps/Reglementation-11081/La-specialite-performance-sportive-du-DES-JEPS-et-les-mentions-unites-capitalisables-complementaire-et-certificats-de-specialisation-s-y-rapportant/Les-certificats-de-specialisation-du-DES-JEPS-specialite-performance-sportive/article/Canoe-kayak-et-disciplines-associees-en-mer-12403).

**DESJEPS prerogatives**

It gives the incumbent the following skills:

- Preparing the strategic performance project in a disciplinary field;
- piloting a training system, directing a sports project;
- Evaluation of the training system
- organising training actions for trainers within the framework of the organisation's professional networks.

**Conditions for access to training leading to the DESJEPS mention "CKDA in calm water" and "CKDA in white water and sea":**

The person concerned must:

- Fill out the standard registration form with a photo ID
- Be of age
- certificate of first aid training (PSC1 type, AFPS);
- communicate a medical certificate of non-contradictory to the teaching and practice of CKDA in calm water/white water and sea (according to the chosen mention), less than three months old;
- produce a six-hundred-hour or three-season sports training certificate issued by the DTNCKDA;
- produce a certificate of participation in a single-seater boat in a national level competition either in calm water or in white water and at sea, according to the chosen mention, issued by the DTNCKDA;
- produce a certificate of success of the technical test organized by the FFCK, consisting of the analysis of a video document to assess the candidate's ability to observe, analyze and establish a diagnosis in order to propose a training of canoeing either in calm water, white water or at sea depending on the mention chosen, for an athlete or a national level team, issued by the DTNCKDA;
- produce a certificate of success of a 100-metre free-swimming dive and recovery of a submerged object at a depth of two metres, issued by a person with a certification of aquatic activities.

Additional documents may be requested (copy of the national identity card, certificate of participation in the day of appeal to the defence, certificate of civil liability, etc.). For more information, it is advisable to check with the training centre in question.

Under certain conditions, exemptions can be obtained for certain events.

*For further information*: Articles D. 212-51 and the following of the Code of Sport, order of 1 July 2008 creating the "CKDA in white water and sea" of the SPECIALty "sports performance" and decreed on 1 July 2008 creating the word "CKDA in calm water" of the DESJEPS specialty "sports performance."

#### Costs associated with qualification

Training to obtain the CQP "canoe-kayak instructor" (regardless of the mention chosen) is paid. Its cost is about 2,100 euros (indicative cost). For more details, it is advisable to get closer to the training centre in question.

Training to obtain BPJEPS, DEJEPS or DESJEPS also pays off. Their costs vary depending on the mentions and the training organizations. For [more details](https://foromes.calendrier.sports.gouv.fr/#/formation), it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*For further information*: Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*For further information*: Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as a canoe-kayak instructor in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*For further information*: Article L. 212-9 of the Code of Sport.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Reporting obligation (for the purpose of obtaining the professional sports educator card)

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

#### Competent authority

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the [official website](https://eaps.sports.gouv.fr).

#### Time

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

#### Supporting documents

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

#### Cost

Free.

*For further information*: Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

#### Competent authority

The prior declaration of activity should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP) of the department where the declarer wants to perform his performance.

#### Time

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

#### Supporting documents

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*For further information*: Articles R. 212-92 and following, A. 212-182-2 and subsequent articles and Appendix II-12-3 of the Code of Sport.

### c. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

#### Competent authority

The declaration should be addressed to the Departmental Directorate in charge of Social Cohesion (DDCS) or to the Departmental Directorate in charge of Social Cohesion and Population Protection (DDCSPP).

#### Time

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

#### Supporting documents for the first declaration of activity

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

#### Evidence for a renewal of activity declaration

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

#### Cost

Free.

#### Remedies

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*For further information*: Articles R. 212-88 to R. 212-91, A. 212-182 and Schedules II-12-2-a and II-12-b of the Code of Sport.

### d. Compensation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*For further information*: Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

##### Conditions

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

##### Procedure

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

##### Supporting documents

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

##### Time

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

##### Cost

Free.

##### More information

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

