﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP185" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Caving instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="caving-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/caving-instructor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="caving-instructor" -->
<!-- var(translation)="Auto" -->


Caving instructor
==============

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
(1) Defining the activity
-------------------------

The caving instructor is a professional who supervises and accompanies any type of public in the practice of caving. He teaches techniques to overcome more or less vertical obstacles to the exploration of this environment.

Caving is a multidisciplinary activity with a strong educational value, it combines scientific, environmental, sports and leisure aspects. Its objective is to explore karst and underground, natural, artificial or anthropogenic environments in order to actively contribute to the study, knowledge and conservation of the practice grounds of caving, while taking into account elements of the surface heritage.

This discipline requires progression and crossings that can involve walking in varied terrain, reptation, swimming, underwater diving, slips, climbing and de-escalation, descent and ascent on a fractional, or not, and other techniques of evolution on agrès (handrail, lifeline, zip line, fixed scales) that may require the implementation of insurance techniques of all types. Opening certain cavities and crossing narrow passages can lead to the implementation of disobstruction techniques.

In accordance with specific techniques related to the diversity of obstacles, the discipline requires suitable equipment, including downhillers, blockers, harnesses, loins, helmets, spikes, fall control devices, lighting devices, insulated clothing, stand-alone suits, ropes, cables, connectors, etc.

For more information, it is advisable to visit[the website of the French Caving Federation](http://www.ffspeleo.fr/speleologie-16.html) (FFS).

2°. Professional qualifications
========================================

### National requirements

#### National legislation

The caving monitor activity is subject to the application of Article L. 212-1 of the Code of Sport, which requires specific certifications.

As a sports teacher, the caving instructor must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

The diplomas for the exercise of the activity of caving instructor are the State Diploma of Youth, Popular Education and Sport (DEJEPS) specialty "sports development" mention "caving" and the higher state diploma of the Youth, Popular Education and Sport (DESJEPS) speciality "sports performance" mention "caving".

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*For further information*: Articles L. 212-1, R. 212-84, A. 212-1 and Appendix II-1 of the Code of Sport.

**Please note**

The training of professionals is provided by the CREPS (Centre for Resources of Expertise and Sports Performance) Rhône-Alpes. For more information, you can see[FFS website](http://www.ffspeleo.fr/devenir-professionnel-28.html).

**Good to know**

The specific environment. The practice of caving, regardless of the area of evolution, constitutes an activity carried out in a specific environment. It involves compliance with special security measures. Therefore, only organizations under the tutelage of the Ministry of Sport can train future professionals.

*For further information*: Articles L. 212-2 and R. 212-7 of the Code of Sport.

#### Training

#### DEJEPS specialty "sports development" mention "caving"

DEJEPS is a state diploma registered at Level III[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

It attests to the acquisition of a qualification in the exercise of a professional activity of coordination and supervision for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in full through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr) and Article D. 212-40 of the Code of Sport.

*For further information*: Article D. 212-35 of the Code of Sport.

**DEJEPS prerogatives:**

- Design sports development programs
- Coordinating the implementation of a sports development project;
- conducting a safe approach to sports development in caving;
- conduct training actions that take into account all the safety requirements associated with the environment;
- Design and coordinate the implementation of caving development programs;
- organize, animate and teach caving activities and techniques in all cavities, in all natural and artificial training places, for all audiences and with respect for the natural environment;
- conducting a progression on ropes and antles in caving in all cavities, in all natural and artificial training places, for all public and with respect for the natural environment;
- initiate exploration, development and management projects for cave sites;
- contribute to technical, educational and environmental expertise in caving;
- participate in the sustainable management of practice sites.

*For further information*: Article 2 of the December 29, 2011 decree creating the "caving" designation of deJEPS specialty "sports development."

Conditions for access to training leading to DEJEPS:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- for a supplementary certificate, produce a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- produce a list of twenty races in class four cavities, performed in four different karst massifs. This list is attested by the National Technical Director of Caving and is the subject of a 30 to 45 minute interview during which the candidate must demonstrate his experience and practice in caving;
- provide the certificate of success of the technical test consisting of a technical test in a cliff or artificial structure and a practical test of exploration in cavity. This certificate is issued by the National Technical Director of Caving;
- produce a list of four caving races, with equipment and cavity equipment and equipment followed by a 20-minute interview;
- set up a caving safety introductory session with a group lasting at least 2 hours in a minimum class three cavity followed by an interview on the analysis of this session lasting 20 to 30 minutes;
- perform a major exploration involving a crew clearance on blockers, using the balance method, with a technique of its choice in less than 3 minutes and 30 seconds. These last three conditions allow you to check the requirements for pedagogical setting.

Under certain conditions, exemptions can be obtained for certain events.

*For further information*: Articles A. 212-35 and A. 212-36 of the Code of Sport and ordered on December 29, 2011 creating the "caving" of deJEPS specialty "sports development."

**Good to know**

Holders of the SPECIALty DEJEPS "sports development" mention "canyonism" can supervise caving activities. For this diploma, the instructors and instructors of canyonism of the French Federation of caving are exempt from the validation of the requirements before entering training (entry test).

For more information, it is advisable to consult the "canyonism monitor" sheet and visit the[FFS website](http://www.ffspeleo.fr/devenir-professionnel-28.html).

**Recycling course**

The licence to practise is limited to a term of six years, renewable. At the end of this period, the renewal of the license to practice is granted to holders of the DEJEPS specialty "sports development", mention "caving" who have taken during this last period, a refresher course.

*For further information*: Article 10 of the December 29, 2011 decree creating the "caving" label of the SPECIALty DEJEPS "sports development."

##### DESJEPS specialty "sports performance" mention "caving"

The DESJEPS is a higher state diploma enrolled at Level II of the[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/).

It attests to the acquisition of a qualification in the exercise of a professional activity of technical expertise and management for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in full through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr) and Article D. 212-56 of the Code of Sport.

*For further information*: Article D. 212-51 of the Code of Sport.

**DESJEPS prerogatives**

The possession of the DESJEPS attests to the skills related to preparation, piloting, management and evaluation:

- exploration, development and site management projects;
- technical, pedagogical and environmental expertise in caving;
- Practice development projects
- coaching groups of all levels, in all cavities, practice sites, all natural and artificial training sites and all training sites.

*For further information*: Article 3 of the December 15, 2006 decree creating the "caving" label of the specialty "sports performance" DESJEPS.

**Conditions for access to training leading to DESJEPS**

The person concerned must:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- for a supplementary certificate, produce a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- Be able to evolve and evolve a group safely in all cavities and training sites. This requirement is verified by a technical test in a cliff or artificial structure and an interview about the candidate's cave experience, his project, his experience, his knowledge of technique, technology, environment, pedagogy, organisation and management in relation to caving;
- produce a list of achievements of four caving races, with equipment and unsealing of the cavity followed by an interview on the analysis of this duration lasting 20 to 30 minutes;
- set up a safety educational session in caving with a group lasting a minimum of 2 hours in a minimum Class 4 cavity followed by an interview on the analysis of this session lasting 20 to 30 minutes;
- perform a major exploration involving a crew clearance on blockers, using a balance method, with a technique of his choice, in less than 30 minutes and 30 seconds. These last three conditions allow us to verify compliance with the pre-educational requirements.

Under certain conditions, exemptions can be obtained for certain events.

*For further information*: Articles A. 212-35 and A. 212-36 of the Code of Sport; decree of 15 December 2006 creating the "caving" designation of the SPECIALty DESJEPS "sports performance".

**Recycling course**

The licence to practise is limited to a term of six years, renewable. At the end of this period, the renewal of the license to practice is granted to the holders of the SPECIALty DESJEPS "sports performance", mention "caving" who have taken a refresher course during this last period.

*For further information*: Article 7-1 of the December 15, 2006 decree creating the "caving" of the DESJEPS "sports performance."

#### Costs associated with qualification

The training leading to the DEJEPS costs about 10,000 euros. For the DESJEPS, the cost is about 3,900 euros.

For more details, it is advisable to get closer to the[Rhône-Alpes Centre for Resource, Expertise and Sport Performance (CREPS)](http://www.creps-rhonealpes.sports.gouv.fr/).

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*For further information*: Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*For further information*: Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as a caving monitor in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*For further information*: Article L. 212-9 of the Code of Sport.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Reporting obligation (for the purpose of obtaining the professional sports educator card)

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Timeframe**

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

**Supporting documents**

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

**Cost**

Free.

*For further information*: Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

**Competent authority**

The prefect of the Department of Isère.

**Timeframe**

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

**Supporting documents**

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*For further information*: Articles R. 212-92 and following, A. 212-182-2 and following, A. 212-215 and Appendix II-12-3 of the Code of Sport.

### c. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

**Competent authority**

The prefect of the department of isère.

**Timeframe**

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

**Supporting documents for the first declaration of activity**

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

**Evidence for a renewal of activity declaration**

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*For further information*: Articles R. 212-88 to R. 212-91, A. 212-182, A. 212-215 and Schedules II-12-2-a and II-12-b of the Code of Sport.

### d. Compensation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*For further information*: Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

