﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP182" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sport" -->
<!-- var(title)="Underwater diving instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sport" -->
<!-- var(title-short)="underwater-diving-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/sport/underwater-diving-instructor.html" -->
<!-- var(last-update)="" -->
<!-- var(url-name)="underwater-diving-instructor" -->
<!-- var(translation)="Auto" -->


Underwater diving instructor
============================

Latest update: <!-- begin-var(last-update) --><!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
(1) Defining the activity
-------------------------

The underwater diving instructor leads and supervises, independently, the activities of learning, discovery and teaching in underwater diving in scuba diving or without a scuba diver according to his specialty.

Underwater diving is about exploring the underwater world. It covers scuba diving activities (an individual device that allows a diver to freely dive with a reserve of compressed breathable gas), without scuba diving and underwater hiking.

The scuba diving instructor helps divers practice their activity, in all environments, natural or artificial. It makes them discover, protect and enhance underwater environments. He ensures their safety, supervises them and accompanies them in exploration. It can also introduce them to other cultural or sports activities related to the practice of underwater diving. He participates in the operation of the dive centre that employs him, especially in the areas of customer reception, the administration of the centre, the implementation and maintenance of nautical equipment and supports. He may be involved in the operation or management of a dive store associated with it.

As for the scuba-free diving instructor, he intervenes on the underwater activities of apnea, underwater fishing, swimming with fins, underwater hiking, whitewater swimming, underwater hockey, target shooting and other activities without a scuba suit, depending on the technical support for which it is specialized.

*To go further* : decree of 1 December 2016 creating the "underwater diving" designation of the professional certificate of youth, popular education and sport (BPJEPS) specialty "sports educator".

2°. Professional qualifications
----------------------------------------

### National requirements

#### National legislation

The underwater diving instructor activity is subject to the application of Article L. 212-1 of the Code of Sport, which requires specific certifications.

As a sports teacher, the underwater diving instructor must hold a diploma, a professional title or a certificate of qualification:

- guaranteeing its competence in terms of the safety of practitioners and third parties in the physical or sports activity under consideration;
- recorded at the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP).

The diplomas to practice the activity of underwater diving instructor are the specialty BPJEPS "sports educator" mention "underwater diving", the state diploma of youth, popular education and sport (DEJEPS) specialty " sports development" mention "underwater diving" and the State Diploma of Youth, Popular Education and Sport (DESJEPS) specialty "sports performance" mention "underwater diving".

Foreign diplomas may be admitted in equivalence to French diplomas by the Minister responsible for sports, after the opinion of the Commission for the Recognition of Qualifications placed with the Minister.

*To go further* Articles A. 212-1 Appendix II-1, L. 212-1 and R. 212-84 of the Code of Sport.

**Good to know**

The specific environment.*The practice of underwater diving in scuba diving, in all places, and in apnea, in the natural environment and in a diving pit, is an activity carried out in a specific environment. It involves compliance with special security measures. Therefore, only organizations under the tutelage of the Ministry of Sport can train future professionals.

*To go further* Articles L. 212-2 and R. 212-7 of the Code of Sport.

#### Training

##### BPJEPS specialty "sports educator" mention "underwater diving"

The BPJEPS is a state diploma registered in the[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) and classified at level IV of the certification level nomenclature. It attests to the acquisition of a qualification in the exercise of a professional activity in responsibility for educational or social purposes, in the fields of physical, sports, socio-educational or cultural activities.

There are two options: option A "in a suit" and option B "without a scuba suit."

This diploma can be obtained in full through the validation of experience (VAE). For more information, you can see[VAE's official website](http://www.vae.gouv.fr). In the context of an VAE, the candidate will also have to be put into a professional situation.

*To go further* Articles D. 212-20 and D. 212-24 of the Code of Sport; Article 10 of the December 1, 2016 decree creating the "underwater diving" designation of the specialty "sports educator" BPJEPS.

**Common prerogatives for "scuba" and "no scuba diving" options:**

- Design an educational project for the public
- organize and supervise underwater hiking practices in autonomy;
- Ensure the safety of the practice in all public access bathing areas used for underwater activities and that of the practitioners in his care;
- Manage emergency and communication equipment to raise the alarm
- participate in the operation of the organisational structure of the activities as well as the use, maintenance and maintenance of the equipment and ship supporting dives.

*To go further* Article 3 of the December 1, 2016 decree creating the "underwater diving" designation of the specialty "sports educator" BPJEPS.

**Special prerogatives for "scuba diving" option:**

Under the authority of a minimum holder of a state certificate of sports educator option "underwater diving", a specialty DEJEPS "sports development" mention "underwater diving" or a SPECIALty DESJEPS "sports performance" "underwater diving," the holder of the BPJEPS is entitled to:

- conduct activities to supervise and facilitate learning, discovery and teaching activities up to 20 metres in underwater diving "in scuba diving";
- participate in the organization of the safety of underwater diving activities.

*To go further* Article 3 of the December 1, 2016 decree creating the "underwater diving" designation of the specialty "sports educator" BPJEPS.

**Specific prerogatives to option B "without scuba diving":**

- conduct autonomously the activities of coaching and animation of learning, discovery, teaching and training activities in underwater diving "without a scuba diver";
- ensure the safety of underwater activities.

*To go further* Article 3 of the December 1, 2016 decree creating the "underwater diving" designation of the specialty "sports educator" BPJEPS.

**Conditions of access to training common to both options**

The person concerned must:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- for a supplementary certificate, produce a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- Present the "First Aid Level 1 Team" certificate (PSE1) or its valid equivalent;
- Present the licence to drive pleasure boats in marine waters, coastal option or its equivalent;
- submit a medical certificate of non-contradictory to the practice and teaching of underwater diving with or without a scuba diving, depending on the option chosen less than three months old.

**Access conditions for Option A "in a spacesuit"**

The person concerned must:

- justify a technical level of PA-40 aptitude and at least sixty dives in the natural environment, fifteen of which are more than 30 metres in the last three years, attested by the dive log;
- to produce the certificate of success in the pre-entry technical test, which consists of:- snorkeling up to 10 metres deep,
  - provide an assistance less than 20 metres deep using any appropriate equipment,
  - immersion in a hoist at a depth of less than 40 metres,
  - demonstrate, in a French-language exchange, his knowledge of the symptoms, prevention and conduct to be held in the event of an accident related to the practice of scuba diving and his knowledge of air management , planning dives, using a diving computer and existing dive tables;
- meet the following pre-work requirements:- initiate the use of a mask, flippers and snorkel (PMT) in swimming pools and in the natural environment,
  - safely accompany a group on an underwater hike,
  - master the main individual techniques in scuba diving,
  - tell a group of divers the technical and safety rules for the practice of the activity,
  - accompany in safety, in a natural environment in the space of 0 to 20 meters,
  - take immediate decisions to protect the safety of the public in the event of an accident or incident that could lead to safety problems.

**Access conditions for option B "no spacesuits"**

The person concerned must:

- produce a qualification attesting to a skill to initiate, teach or train an underwater diving activity "without scuba diving" (excluding underwater hiking);
- to produce the certificate of success in the pre-entry technical test, which consists of:- snorkeling up to 15 metres deep in constant weight by moving at least 10 metres to the bottom,
  - swim 800 metres with flippers, mask and snorkel in less than 13 minutes for men, 14 minutes for women,
  - conduct an emergency management test with a standardized free-diving dummy,
  - demonstrate, in the course of a French-language exchange, his knowledge of the symptoms, prevention and conduct to be held in the event of an accident related to underwater diving activities without scuba diving, as well as on safety rules,
- meet the following pre-situation requirements:- initiate the use of a mask, flippers and snorkel (PMT) in swimming pools and in the natural environment,
  - safely accompany a group on an underwater hike,
  - master the main individual diving techniques without scuba diving,
  - provide a group of practitioners with the technical and safety rules for the practice of activity without a scuba suit,
  - set up a diving organisation without scuba diving, in natural and artificial environments,
  - safely supervise a group of scuba diving practitioners, in natural and artificial environments,
  - take immediate decisions to protect the safety of the public in the event of an accident or incident that could lead to safety problems.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* Articles A. 212-35, A. 212-36 and Appendix III-14a of the Code of Sport; Appendixes IV and V of the BEJEPS Order of 1 December creating the word "underwater diving".

##### DEJEPS specialty "sports development" mention "underwater diving"

DEJEPS is a state diploma registered at Level III[RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/). It attests to the acquisition of a qualification in the exercise of a professional activity of coordination and supervision for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in full through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr) and Article D. 212-40 of the Code of Sport.

*To go further* Article D. 212-35 of the Code of Sport.

**Good to know**

The complementary certificate (CC) "deep diving and tutoring"**. This CC may be associated with the deJEPS specialty "sports development" designation of "underwater diving." It attests to the following skills:

- provide technical direction for underwater diving activities in the 40-60 metre depth area of the underwater dive site;
- conduct animation, initiation and development in underwater diving in the 40 to 60 metre depth zone;
- provide in-situation tutoring for trainees up to Level III of professional training in underwater diving.

The candidate must justify an experience of twenty dives in the natural environment at a depth of more than 40 metres carried out in a period of five years prior to entry into training, by means of a certificate issued by the National Technical Director of the French Federation of Underwater Studies and Sports.

*To go further* : Decree of December 1, 2016 establishing the CC "deep diving and tutoring" associated with the mention "underwater diving" of the SPECIALty DEJEPS "sports development".

**DEJEPS prerogatives:**

- Design programs for organization, animation, initiation and development in underwater diving;
- coordinate the implementation of projects to organize, facilitate, initiate, develop and develop underwater diving;
- Provide technical direction of activities at the underwater dive site;
- conduct animation, initiation and development in underwater diving;
- provide direct tutoring for trainees up to Level III of professional training in underwater diving;
- manage the logistics functions of an underwater diving structure.

*To go further* Article 2 of the July 6, 2011 decree creating the deJEPS specialty "sports development" "underwater diving" designation.

**Conditions for access to training leading to DEJEPS**

The person concerned must:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- for a CC registration, produce a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- Hold the Level 1 First Aid Certificate (PSE1) or its equivalent;
- Be licensed to drive pleasure boats in marine waters, coastal option, or equivalent;
- justify the experience of one hundred dives in the natural environment, thirty of which at a depth of at least 30 meters obtained in a period of five years prior to the entry into training. The number of dives is attested by the National Technical Director of Underwater Sports;
- justify a technical level of PA-40 skills;
- produce certificates (issued by the Regional Director of Youth, Sports and Social Cohesion) of success in the tests of:- management of an emergency with a standard diving dummy,
  - management of an emergency of a scuba diver,
  - conducting an exploration dive,
  - verification of theoretical and practical knowledge in underwater diving in the French language;
- meet the requirements of the pedagogical situation. The candidate must be able to:- to ensure the safe accompaniment of divers in underwater environments, in self-contained spacesuits, up to a depth of 40 metres,
  - to rescue, in the event of an incident or accident, a diver in an underwater environment, up to a depth of 40 metres,
  - to mobilise the warning and first aid procedures,
  - plan the organization of scuba diving in the air using a computer or a dive table,
  - detect, prevent and behave appropriately to avoid any incidents or accidents that may occur in the context of recreational underwater diving,
  - to safely drive in immersion training actions of divers in space from 0 to 20 meters.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* Articles A. 212-35 and A. 212-36 of the Code of Sport; order of July 6, 2011 creating the deJEPS specialty "underwater diving" designation of "sports development."

**Recycling course**

DeJEPS holders are subject to a retraining course every five years. It is organized by one of the institutions responsible for the implementation of the DEJEPS, which is referred to as "underwater diving".

*To go further* Article 7 of the July 6, 2011 decree creating the deJEPS specialty "sports development" "underwater diving" designation.

##### DESJEPS specialty "sports development" mention "underwater diving"

The DESJEPS is a higher state diploma enrolled at Level II of the [RNCP](https://www.francecompetences.fr/recherche_certificationprofessionnelle/). It attests to the acquisition of a qualification in the exercise of a professional activity of technical expertise and management for educational purposes in the fields of physical, sports, socio-educational or cultural activities.

This diploma can be obtained in full through the VAE. For more information, you can see[VAE's official website](http://www.vae.gouv.fr) and Article D. 212-56 of the Code of Sport.

*To go further* Article D. 212-51 of the Code of Sport.

**DESJEPS prerogatives:**

- Develop exploration, development and management projects for underwater dive sites;
- carry out technical, educational and environmental expertise in underwater diving;
- Provide technical direction for underwater diving activities;
- mentoring and training divers of all levels, in all environments and underwater diving sites;
- manage and tutor trainees in professional training in underwater diving;
- Manage the logistics of an underwater diving structure
- organize and coordinate training actions for trainers.

*To go further* Article 2 of the Order of 6 July 2011 creating the "underwater diving" designation of the SPECIALty "sports performance" desJEPS.

**Conditions for access to training leading to DESJEPS**

The person concerned must:

- Be of age
- Provide a registration form with a photograph
- Provide a photocopy of a valid ID
- If necessary, produce the certificate or certificates justifying the lightening of certain tests;
- Provide a medical certificate of non-contradictory to sports practice less than one year old;
- for people with disabilities, provide the opinion of a doctor approved by the French Handisport Federation or by the French Federation of Adapted Sport or appointed by the Committee on the Rights and Autonomy of Persons with Disabilities on the need Develop pre-certification tests, if necessary, in accord with the certification.
- Provide copies of the census certificate and the individual certificate of participation in the Defence and Citizenship Day;
- If necessary, provide the documents justifying exemptions and equivalencies of law;
- for a supplementary certificate, produce a photocopy of the diploma authorizing registration in training or a certificate of registration for the training leading to that diploma;
- Hold the certificate of 1st aid in Level 1 team (PSE1) or its equivalent;
- Be licensed to drive pleasure boats in marine waters, coastal option or equivalent;
- Hold the ability of a confirmed nitrox diver (PN-C);
- justify the experience of one hundred dives in the natural environment, thirty of which at a depth of at least 30 meters, obtained in a period of five years prior to the entry into training and attested by the national technical director of underwater sports;
- justify the experience of 800 hours of underwater diving instruction, obtained in a five-year period prior to entry into training and certified by the National Technical Director for Underwater Sports;
- justify a technical level of PA-40 skills;
- produce the certificates of success, issued by the Regional Director of Youth, Sports and Social Cohesion, on the tests of:- management of an emergency with a standard free-diving dummy,
  - management of an emergency of a scuba diver,
  - organizing and conducting a teaching dive in deep space,
  - verification of theoretical and practical knowledge;
- meet the requirements of the pedagogical situation. The candidate must be able to:- to ensure the safe accompaniment of all public in underwater environment, in autonomous spacesuits in air or nitrox, from 0 to 40 meters,
  - to rescue, in the event of an incident or accident, a diver in an underwater environment, in the space of 0 to 40 metres,
  - to mobilise alert and rescue procedures,
  - plan the organization of scuba diving in the air using a computer or a dive table,
  - detect, prevent and behave appropriately in the face of any accidents that may occur in the context of recreational underwater diving,
  - to safely drive in immersion training actions of divers in space of 0-40 meters.

Under certain conditions, exemptions can be obtained for certain events.

*To go further* Articles A. 212-35 and A. 212-36 of the Code of Sport; Articles 3 and 4 of the July 6, 2011 decree creating the "underwater diving" designation of the SPECIALty "sports performance" desJEPS.

**Recycling course**

DesJEPS holders are subject to a retraining course every five years. It is organised by one of the public institutions of the Ministry of Sports responsible for providing training in underwater diving, under the authority of the regional director of youth, sports and social cohesion.

*To go further* Article 7 of the Order of 6 July 2011 creating the "underwater diving" designation of the SPECIALty "sports performance" desJEPS.

#### Costs associated with qualification

Training for BPJEPS, DEJEPS and DESJEPS is paid for. The cost varies depending on the mentions and the training organizations.

For more details, it is advisable to get closer to the training organization in question.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

European Union (EU) nationals*European Economic Area (EEA)* legally established in one of these states may carry out the same activity in France on a temporary and occasional basis on the condition that it has sent a prior declaration of activity to the prefect of the department of the delivery.

If the activity or training leading there is not regulated in the Member State of origin or the state of the place of establishment, the national must also justify having carried out this activity for at least the equivalent of two full-time years in the in the last ten years prior to the benefit.

European nationals wishing to practise in France on a temporary or occasional basis must have the language skills necessary to carry out the activity in France, in particular in order to guarantee the safety of the activities and its ability to alert emergency services.

*To go further* Articles L. 212-7 and R. 212-92 to R. 212-94 of the Code of Sport.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may settle in France to practice permanently, if he fulfils one of the following four conditions:

**If the Member State of origin regulates access or the exercise of the activity:**

- hold a certificate of competency or a training certificate issued by the competent authority of an EU or EEA state that certifies a level of qualification at least equivalent to the level immediately lower than that required in France;
- be the holder of a title acquired in a third state and admitted in equivalence to an EU or EEA state and justify having carried out this activity for at least two years full-time in that state.

**If the Member State of origin does not regulate access or the exercise of the activity:**

- justify having been active in an EU or EEA state, full-time for at least two years in the last ten years, or, in the case of part-time exercise, justifying an activity of equivalent duration and holding a certificate qualification issued by the competent authority of one of these states, which attests to a readiness for the exercise of the activity, as well as a level of qualification at least equivalent to the level immediately lower than that required in France;
- be holder of a qualification certificate at least equivalent to the level immediately lower than that required in France, issued by the competent authority of an EU or EEA state and sanctioning regulated training aimed at specifically the exercise of all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, consisting of a cycle of study supplemented, if necessary, by vocational training, internship or professional practice.

If the national meets one of the four conditions mentioned above, the qualification requirement required to practise is deemed satisfied.

However, if the professional qualifications of the national differ substantially from the qualifications required in France which would not guarantee the safety of practitioners and third parties, he may be required to submit to an aptitude test or completing an adjustment course (see below "Good to know: compensation measures").

The national must have the knowledge of the French language necessary to carry out his activity in France, in particular in order to guarantee the safety of physical and sporting activities and his ability to alert the emergency services.

*To go further* Articles L. 212-7 and R. 212-88 to R. 212-90 of the Code of Sport.

3°. Conditions of honorability
-----------------------------------------

It is forbidden to practice as an underwater diving instructor in France for persons who have been convicted of any crime or for any of the following offences:

- torture and acts of barbarism;
- Sexual assaults;
- drug trafficking;
- Endangering others;
- pimping and the resulting offences;
- endangering minors;
- illicit use of substance or plant classified as narcotics or provocation to the illicit use of narcotics;
- violations of Articles L. 235-25 to L. 235-28 of the Code of Sport;
- as a punishment complementary to a tax offence: a temporary ban on practising, directly or by person interposed, on behalf of himself or others, any industrial, commercial or liberal profession ( Section 1750 of the General Tax Code).

In addition, no one may teach, facilitate or supervise a physical or sporting activity with minors, if he has been the subject of an administrative measure prohibiting him from participating, in any capacity, in the management and supervision of institutions and bodies subject to legislation or regulations relating to the protection of minors in a holiday and leisure centre, as well as youth groups, or if it has been the subject of an administrative measure to suspend these same functions.

*To go further* Article L. 212-9 of the Code of Sport.

4°. Qualifications process and formalities
--------------------------------------------------------------

### a. Reporting obligation (for the purpose of obtaining the professional sports educator card)

Anyone wishing to practice any of the professions governed by Article L. 212-1 of the Code of Sport must declare his activity to the prefect of the department of the place where he intends to practice as principal. This declaration triggers the obtaining of a business card.

The declaration must be renewed every five years.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Timeframe**

Within one month of filing the declaration file, the prefecture sends an acknowledgement to the registrant. The business card, valid for five years, is then addressed to the registrant.

**Supporting documents**

The supporting documents to be provided are:

- Cerfa 12699*02 ;
- A copy of a valid ID
- A photo ID
- A statement on the honour attesting to the accuracy of the information in the form;
- A copy of each of the diplomas, titles, certificates invoked;
- A copy of the authorization to practice, or, if necessary, the equivalency of the diploma;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

If you have a return renewal, you should contact:

- Form Cerfa 12699*02 ;
- A photo ID
- A copy of the valid review certificate for qualifications subject to the recycling requirement;
- a medical certificate of non-contradictory to the practice and supervision of the physical or sporting activities concerned, less than one year old.

In addition, in all cases, the prefecture will itself request the release of an extract of less than three months from the registrant's criminal record to verify that there is no disability or prohibition of practice.

**Cost**

Free.

*To go further* Articles L. 212-11, R. 212-85 and A. 212-176 to A. 212-178 of the Code of Sport.

### b. Make a pre-declaration of activity for EU nationals engaged in temporary and occasional activity (LPS)

EU or EEA nationals legally established in one of these states who wish to practise in France on a temporary or occasional basis must make a prior declaration of activity before the first provision of services.

If the claimant wishes to make a new benefit in France, this prior declaration must be renewed.

In order to avoid serious damage to the safety of the beneficiaries, the prefect may, during the first benefit, carry out a preliminary check of the claimant's professional qualifications.

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Timeframe**

Within one month of receiving the declaration file, the prefect notifies the claimant:

- or a request for further information (in this case, the prefect has two months to give his answer);
- or a receipt for a service delivery statement if it does not conduct a qualifications check. In this case, service delivery may begin;
- or that it is conducting the qualifications check. In this case, the prefect then issues the claimant a receipt allowing him to start his performance or, if the verification of the qualifications reveals substantial differences with the professional qualifications required in France, the prefect submits the claimant to an aptitude test (see infra "Good to know: compensation measures").

In all cases, in the absence of a response within the aforementioned deadlines, the claimant is deemed to be legally active in France.

**Supporting documents**

The activity pre-report file must include:

- A copy of the declaration form provided in Schedule II-12-3 of the Code of Sport;
- A photo ID
- A copy of an ID
- A copy of the certificate of competency or training title;
- A copy of the documents attesting that the registrant is legally established in the Member State of the institution and that he does not incur any prohibition, even temporary, from practising (translated into French by a certified translator);
- in the event that neither the activity nor the training leading to this activity is regulated in the Establishment Member State, a copy of any documents justifying that the registrant has carried out this activity in that State for at least the equivalent of two years full time over the past ten years (translated into French by a certified translator);
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - a copy of a document attesting to professional experience acquired in France.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-92 and following, A. 212-182-2 and following, A. 212-209 and Appendix II-12-3 of the Code of Sport.

### c. Make a pre-declaration of activity for EU nationals for a permanent exercise (LE)

Any EU or EEA national qualified to carry out all or part of the activities mentioned in Article L. 212-1 of the Code of Sport, and wishing to settle in France, must make a statement to the prefect of the department in which he intends exercise as a principal.

This declaration allows the registrant to obtain a professional card and thus to practice legally in France under the same conditions as French nationals.

The declaration must be renewed every five years.

In the event of a substantial difference from the qualification required in France, the prefect may refer the qualifications recognition committee to the Minister for Sport for advice. They may also decide to subject the national to an aptitude test or an accommodation course (see below: "Good to know: compensation measures").

**Competent authority**

The declaration should be addressed to the Departmental Directorate of Social Cohesion (DDCS) or Departmental Directorate for Social Cohesion and Population Protection (DDCSPP) of the practice department or the main exercise, or directly in line on the[official website](https://eaps.sports.gouv.fr).

**Timeframe**

The prefect's decision to issue the business card comes within three months of the submission of the full file by the registrant. This period may be extended by one month on a reasoned decision. If the prefect decides not to issue the professional card or to subject the declarant to a compensation measure (fitness test or internship), his decision must be motivated.

**Supporting documents for the first declaration of activity**

The activity report file should include:

- A copy of the declaration form provided in Schedule II-12-2-a of the Code of Sport;
- A photo ID
- A copy of a valid ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old (translated by a certified translator);
- A copy of the certificate of competency or training title, accompanied by documents describing the training course (program, hourly volume, nature and duration of internships carried out), translated into French by a certified translator;
- If so, a copy of any evidence justifying work experience (translated into French by a certified translator);
- If the training document has been obtained in a third state, copies of the documents attesting that the title has been admitted as an equivalency in an EU or EEA state that regulates the activity;
- one of the three documents to choose from (if not, an interview will be held):- A copy of a certificate of qualification issued after training in French,
  - A copy of a French-level certificate issued by a specialized institution,
  - A copy of a document attesting to professional experience acquired in France;
- documents attesting that the registrant was not the subject of any of the convictions or measures referred to in Articles L. 212-9 and L. 212-13 of the Code of Sport (translated into French by a certified translator) in the Member State of origin.

**Evidence for a renewal of activity declaration**

The activity renewal file must include:

- A copy of the return renewal form, modeled on Schedule II-12-2-b of the Code of Sport;
- A photo ID
- a medical certificate of non-contradictory to the practice and supervision of physical or sporting activities, less than one year old.

**Cost**

Free.

**Remedies**

Any litigation must be exercised within two months of notification of the decision to the relevant administrative court.

*To go further* Articles R. 212-88 to R. 212-91, A. 212-182, A. 212-209 and Schedules II-12-2-a and II-12-b of the Code of Sport.

### d. Compensation measures

If there is a substantial difference between the qualification of the applicant and that required in France to carry out the same activity, the prefect refers the commission for recognition of qualifications, placed with the Minister in charge of sports. This commission, after reviewing and investigating the file, issues, within the month of its referral, a notice which it sends to the prefect.

In its opinion, the commission can:

- believe that there is indeed a substantial difference between the qualification of the registrant and that required in France. In this case, the commission proposes to subject the registrant to an aptitude test or an adjustment course. It defines the nature and precise modalities of these compensation measures (the nature of the tests, the terms of their organisation and evaluation, the period of organisation, the content and duration of the internship, the types of structures that can accommodate the trainee, etc.) ;
- that there is no substantial difference between the qualification of the registrant and that required in France.

Upon receipt of the commission's opinion, the prefect notifies the registrant of his reasoned decision (he is not obliged to follow the commission's advice):

- If the registrant requires compensation, he or she has one month to choose between the aptitude test and the accommodation course. The prefect then issues a business card to the registrant who has complied with the compensation measures. On the other hand, if the course or the aptitude test is not satisfactory, the prefect notifies his reasoned decision to refuse to issue the professional card to the person concerned;
- if he does not require compensation, the prefect issues a business card to the person concerned.

*To go further* Articles R. 212-84 and D. 212-84-1 of the Code of Sport.

### e. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a [online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

