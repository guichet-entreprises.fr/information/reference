﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP086" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sécurité routière" -->
<!-- var(title)="Contrôleur technique de véhicules" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="securite-routiere" -->
<!-- var(title-short)="controleur-technique-de-vehicules" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/securite-routiere/controleur-technique-de-vehicules.html" -->
<!-- var(last-update)="2020-04-28 17:38:39" -->
<!-- var(url-name)="controleur-technique-de-vehicules" -->
<!-- var(translation)="None" -->

# Contrôleur technique de véhicules

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28 17:38:39<!-- end-var -->

## 1°. Définition de l’activité

Le contrôleur technique de véhicules est un professionnel de l'automobile agréé par l’État dont l'activité consiste à s'assurer du bon état de fonctionnement et de l'entretien des véhicules automobiles légers (dont le poids n'excède pas 3,5 tonnes) et lourds.

À ce titre il doit effectuer l'ensemble des contrôles suivants :

- pour les véhicules légers, ceux prévus à l'annexe I de l'arrêté du 18 juin 1991 relatif à la mise en place et à l'organisation du contrôle technique des véhicules dont le poids n'excède pas 3,5 tonnes ;
- pour les véhicules lourds, ceux prévus à l'annexe I de l'arrêté du 27 juillet 2004 relatif au contrôle technique des véhicules lourds.

**À noter**

Le professionnel peut exercer son activité de manière indépendante ou au sein du réseau national de contrôle agréé par le ministère chargé des transports.

*Pour aller plus loin* : article L. 323-1 du Code de la route.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité de contrôleur technique de véhicules, le professionnel doit :

- être qualifié professionnellement ;
- être titulaire d'un agrément (cf. infra « 5°. a. Demande d'agrément ») ;
- exercer au sein d'installations comportant les moyens matériels (techniques et informatiques) permettant d'effectuer les contrôles techniques. Ces installations doivent être agréées par le préfet du département dans les conditions fixées aux articles 16 à 17-2 de l'arrêté du 18 juin 1991 pour les véhicules légers et dans les conditions des articles 21 à 25-1 de l'arrêté du 27 juillet 2004 pour les véhicules lourds.

*Pour aller plus loin* : articles L. 323-1 et R. 323-13 à R. 323-15 du Code de la route.

#### Formation

##### Pour exercer l'activité de contrôleur technique de véhiculess légers

Le professionnel doit être titulaire de l'un des diplômes suivants :

- un diplôme de niveau V (de type CAP, BEP, brevet des collèges) ou d'un diplôme équivalent inscrit au [Répertoire national des certifications professionnelles](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP) dans l'une des disciplines de l'automobile suivantes :
  - la mécanique automobile,
  - la carrosserie,
  - la tôlerie,
  - l'électricité automobile,
  - la maintenance automobile. En outre, le professionnel doit avoir suivi une formation spécialisée d'au moins 900 heures dans le contrôle technique ;
- un diplôme de niveau IV (niveau bac) dans l'une des disciplines de l'automobile visées ci-dessus ou dans l'un des secteurs de l'industrie automobile (mécanique, productique, automatisme électronique, électromécanique ou maintenance aéronautique). Il doit également avoir suivi une formation complémentaire au contrôle technique d'une durée minimale de 175 heures ;
- un diplôme de niveau V ou d'un équivalent inscrit au RNCP dans le domaine de l'automobile ainsi que d'une expérience professionnelle d'au mois deux ans dans la réparation ou la maintenance automobile. De plus, il doit avoir suivi une formation spécialisée concernant le contrôle technique d'une durée minimale de 175 heures ;
- un certificat de qualification professionnelle (CQP) ou un titre professionnel de contrôleur technique automobile délivré dans les conditions de l'arrêté du 6 décembre 2017 relatif au titre professionnel de contrôleur technique automobile de véhicule léger.

**À noter**

Peut également exercer le professionnel qui justifie d'une expérience d'au moins cinq ans dans la réparation automobile et d'une formation spécialisée complémentaire dans le contrôle technique d'une durée minimale de 175 heures.

*Pour aller plus loin* : article 12 et annexe IV de l'arrêté du 18 juin 1991.

##### Pour exercer l'activité de contrôleur technique de véhiculess lourds

Le professionnel doit être titulaire de l'un des diplômes suivants :

- un diplôme de niveau V au sein d'une discipline automobile ou, à défaut, un diplôme équivalent au sens du RNCP. Ainsi que d'une formation spécialisée au contrôle technique automobile des véhicules lourds d'une durée minimale de 280 heures ;
- un diplôme de niveau V ou un équivalent au RNCP dans le domaine de l'automobile ainsi que d'une expérience d'au moins trois ans dans cette discipline ou dans le contrôle technique automobile de véhicules légers et une formation spécialisée au contrôle technique de véhicules lourds d'au moins 280 heures (comprenant 175 heures de formation théorique et 105 heures de formation pratique) ;
- un diplôme de niveau IV ou un équivalent au RNCP dans le domaine de l'automobile et avoir suivi une formation spécialisée au contrôle technique de véhicules lourds d'une durée minimale de 280 heures (comprenant une formation théorique de 175 heures et d'une partie pratique de 105 heures) ;
- un CQP ou un titre professionnel de contrôleur technique automobile.

*Pour aller plus loin* : annexe IV de l'arrêté du 27 juillet 2004 relatif au contrôle technique des véhicules lourds.

#### Formation continue

Chaque année, le professionnel est tenu de suivre une formation continue en vue de maintenir ses compétences professionnelles à jour. À ce titre, il doit justifier avoir effectué :

- un complément de formation d'au moins 20 heures pour les véhicules légers et d'au moins 24 heures pour les véhicules lourds, au sein d'un organisme reconnu et désigné par le réseau ;
- au moins 300 visites techniques périodiques pour les véhicules légers ou 500 contrôles techniques pour les véhicules lourds ;
- au moins un audit tous les deux ans, sur la réalisation d'une visite technique périodique.

*Pour aller plus loin* : 4. de l'annexe IV de l'arrêté du 18 juin 1991 ; 2.1 de l'annexe IV de l'arrêté du 27 juillet 2004 susvisé.

#### Coûts associés à la qualification

Le coût de la formation en vue d'exercer la profession de contrôleur technique de véhicules varie selon le cursus envisagé. Il est conseillé de se rapprocher des établissements concernés pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) légalement établi et exerçant l'activité de contrôleur technique de véhicules peut exercer à titre temporaire et occasionnel cette activité en France.

Lorsque l’État membre ne réglemente ni l'exercice de l'activité ni sa formation, le ressortissant doit avoir exercé cette activité dans un ou plusieurs État(s) membre(s) pendant au moins un an au cours des dix dernières années.

Dès lors qu'il remplit ces conditions, il doit, avant toute prestation de services, effectuer une déclaration préalable auprès de l'autorité compétente (cf. infra « 5°. b. Déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS) »).

*Pour aller plus loin* : II de l'article L. 323-1 du Code de la route.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Aucune mesure n'est prévue pour le ressortissant de l'UE ou de l'EEE légalement établi dans un État membre en vue d'exercer l'activité de contrôleur technique de véhicules à titre temporaire en France.

À ce titre, le ressortissant est soumis aux mêmes exigences que le ressortissant français (cf. infra « 5°. a. Demande d'agrément »).

## 3°. Conditions d’honorabilité et incompatibilités

### Incompatibilités

Le contrôleur technique de véhicules ne peut exercer aucune autre activité relative à la réparation ou au commerce automobile, que ce soit à titre indépendant ou en tant que salarié.

*Pour aller plus loin* : article L. 323-1 et R. 323-17 du Code de la route.

### Honorabilité

Pour exercer, le professionnel ne doit avoir fait l'objet d'aucune condamnation inscrite au bulletin n° 2 de son casier judiciaire.

En outre, le ressortissant UE exerçant cette activité ne doit avoir aucun lien de nature à porter atteinte à son indépendance avec les personnes, les organismes ou entreprises exerçant une activité de réparation ou de commerce dans le secteur automobile.

*Pour aller plus loin* : articles L. 323-1 et R. 323-17 du Code de la route.

## 4°. Sanctions

Le professionnel encourt une amende de 750 euros dès lors qu'il effectue un contrôle technique sans respecter la réglementation en la matière.

En outre, l'utilisation des résultats du contrôle technique effectué à des fins autres que celles prévues par la réglementation est interdite.

*Pour aller plus loin* : articles R. 323-19 et R. 323-20 du Code de la route.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande d'agrément

#### Autorité compétente

Le professionnel doit adresser sa demande en deux exemplaires au préfet du département où est implanté le centre de contrôle au sein duquel il exerce.

#### Pièces justificatives

Sa demande prend la forme d'un dossier comprenant :

- sa demande d'agrément en tant que contrôleur indiquant le centre de contrôle au sein duquel il souhaite exercer (le cas échéant, le réseau de contrôle agréé) ;
- le bulletin n° 2 de son casier judiciaire ;
- une pièce d'identité en cours de validité ;
- un justificatif de ses qualifications professionnelles ;
- une fiche récapitulative dont le modèle est fixée à l'appendice 1 de l'annexe VII. Lorsque le demandeur est un ressortissant de l'UE ou de l'EEE, il doit fournir un document équivalent datant de moins de trois mois et rédigé en français ou à défaut, assorti d'une traduction agréée ;
- le cas échéant, l'avis du réseau de contrôle agréé dont il dépend, ou, s'il n'est pas rattaché à un réseau, l'avis de l'organisme technique central suivant le modèle fixé à l'appendice 2 de l'annexe VII de l'arrêté du 18 juin 1991 ;
- si le demandeur est salarié, une copie de son contrat de travail ou une lettre d'engagement de son employeur ;
- une déclaration sur l'honneur :
  - certifiant l'exactitude des informations fournies,
  - attestant qu'il ne fait l'objet d'aucune mesure de suspension ou de retrait d'agrément,
  - attestant qu'il s'engage à ne pas exercer une autre activité dans la réparation ou le commerce automobile et à ne pas utiliser les informations obtenues au cours de son activité à d'autres fins que celles prévues par la réglementation.

#### Procédure et délais

L'autorisation d'agrément est notifiée au demandeur ainsi qu'au centre de contrôle auquel il est rattaché.

**À noter**

Le professionnel peut effectuer une demande en vue d'obtenir les deux agréments, à savoir l'un pour les véhicules légers et l'autre pour les véhicules lourds.

*Pour aller plus loin* : article R. 323-18 du Code de la route ; article 13 et annexe VII de l'arrêté du 18 juin 1991 précité ; annexe VII de l'arrêté du 27 juillet 2004 précité.

### b. Déclaration préalable pour le ressortissant UE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa demande par écrit, au préfet du département dans lequel il envisage d'exercer sa prestation de services.

#### Pièces justificatives

Sa demande doit comporter les documents suivants, assortis de leur traduction en français :

- un justificatif de sa nationalité ;
- une attestation certifiant qu'il est légalement établi dans un État membre de l'UE ou de l'EEE pour y exercer l'activité de contrôleur technique de véhicules et qu'il n'encourt aucune interdiction d'exercer ;
- un justificatif de ses qualifications professionnelles ;
- lorsque ni l'accès à l'activité ni son exercice ne sont réglementés dans cet État membre, la preuve qu'il a exercé cette activité pendant au moins un an au cours des dix dernières années ;
- une copie de son contrat de travail ou, à défaut, une lettre d'engagement du centre de contrôle l'employant.

#### Délais

Dans un délai d'un mois, le préfet adresse un récépissé de déclaration au ressortissant.

*Pour aller plus loin* : article R. 323-18-1 du Code de la route ; article 26-6 de l'arrêté du 18 juin 1991.

### c. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’Union européenne ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).