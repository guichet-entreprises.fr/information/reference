﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP086" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Road safety" -->
<!-- var(title)="Vehicle inspector" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="road-safety" -->
<!-- var(title-short)="vehicle-inspector" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/road-safety/vehicle-inspector.html" -->
<!-- var(last-update)="2020-04-15 17:22:23" -->
<!-- var(url-name)="vehicle-inspector" -->
<!-- var(translation)="Auto" -->


Vehicle inspector
============================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:23<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The vehicle technical controller is a state-certified automotive professional whose activity is to ensure the proper operation and maintenance of light motor vehicles (weighing no more than 3.5 tons) and heavy vehicles.

As such, it must carry out all of the following checks:

- for light-powered vehicles, those planned for the[Appendix I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0CDFD1D0ECCF66C346F803B51C8737D0.tplgfr41s_2?idArticle=LEGIARTI000034159662&cidTexte=LEGITEXT000020559004&dateTexte=20180330) the decree of 18 June 1991 on the establishment and organisation of the technical control of vehicles weighing no more than 3.5 tonnes;
- for heavy vehicles, those planned for the[Appendix I](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=A8F5D581C452730D90D4F300AAFBCEDC.tplgfr22s_2?idArticle=LEGIARTI000034200988&cidTexte=LEGITEXT000005838877&dateTexte=20180403) 27 July 2004 on the technical control of heavy vehicles.

**Please note**

The professional may operate independently or within the national control network approved by the Ministry of Transport.

*For further information*: Article L. 323-1 of the Highway Traffic Act.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

To carry out the activity of vehicle technical controller, the professional must:

- Be professionally qualified
- be accredited (see infra "5°). a. Application for accreditation");
- practice in facilities with the physical (technical and computer) means to carry out technical checks. These facilities must be approved by the prefect of the department under the conditions set out in Articles 16 to 17-2 of the[decreed on 18 June 1991](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000020559004&dateTexte=20180330) for light vehicles and under the conditions of Articles 21 to 25-1 of the[ordered from 27 July 2004](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=A8F5D581C452730D90D4F300AAFBCEDC.tplgfr22s_2?cidTexte=JORFTEXT000000626890&dateTexte=20180403) heavy vehicles.

*For further information*: Articles L. 323-1 and R. 323-13 to R. 323-15 of the Highway Traffic Act.

#### Training

**To carry out the activity of technical controller of light vehicles**

The professional must hold one of the following degrees:

- a Level V diploma (CAP, BEP, college certificate) or an equivalent diploma registered in the[national directory of professional certifications](https://www.francecompetences.fr/recherche_certificationprofessionnelle/) (RNCP) in one of the following automotive disciplines:- automotive mechanics,
  - The bodywork,
  - sheet metal,
  - automotive electricity,
  - car maintenance. In addition, the professional must have undergon at least 900 hours of specialized training in technical control;
- a Level IV (bac level) degree in one of the automotive disciplines referred to above or in one of the sectors of the automotive industry (mechanics, production, electronic automation, electromechanics or aeronautical maintenance). He must also have completed additional technical training for a minimum of 175 hours;
- a Level V diploma or an equivalent registered with the RNCP in the automotive field as well as a working experience of at least two years in automotive repair or maintenance. In addition, he must have undergon specialized technical training for a minimum of 175 hours;
- a Certificate of Professional Qualification (CQP) or a professional title of automotive technical controller issued under the conditions of the[Stopped](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000036468664&categorieLien=id) December 6, 2017 relating to the professional title of light vehicle automotive technical controller.

**Please note**

Can also practice the professional who justifies at least five years of experience in automotive repair and additional specialized training in technical control for a minimum of 175 hours.

*For further information*: Article 12 and[Appendix IV](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=0CDFD1D0ECCF66C346F803B51C8737D0.tplgfr41s_2?idArticle=LEGIARTI000034159670&cidTexte=LEGITEXT000020559004&dateTexte=20180330) 18 June 1991.

**To carry out the activity of technical controller of heavy vehicles**

The professional must hold one of the following degrees:

- a V-level diploma in an automotive discipline or, failing that, a diploma equivalent to the meaning of the RNCP. As well as specialized training in the technical control of heavy vehicles with a minimum duration of 280 hours;
- a V-level diploma or an equivalent to the RNCP in the automotive field as well as at least three years of experience in this discipline or in the technical control of light vehicles and specialized training in technical control of the heavy vehicles of at least 280 hours (including 175 hours of theoretical training and 105 hours of hands-on training);
- a Level IV diploma or an equivalent to the RNCP in the automotive field and have received specialized training in the technical control of heavy vehicles with a minimum duration of 280 hours (including a theoretical training of 175 hours and part of the 105-hour practice);
- CQP or a professional title of automotive technical controller.

*For further information*: Appendix IV of the 27 July 2004 order on the technical control of heavy vehicles.

**Continuous training**

Each year, the professional is required to undergo continuous training in order to keep his professional skills up to date. As such, it must justify having carried out:

- Additional training of at least 20 hours for light vehicles and at least 24 hours for heavy vehicles, in an organization recognized and designated by the network;
- at least 300 periodic technical visits for light vehicles or 500 technical checks for heavy vehicles;
- at least one audit every two years, on the completion of a periodic technical visit.

*For further information*: 4. Appendix IV of the order of 18 June 1991; 2.1 of Schedule IV of the order of 27 July 2004 as a case.

#### Costs associated with qualification

The cost of training to practice as a vehicle technical controller varies according to the course envisaged. It is advisable to get close to the institutions concerned for more information.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the legally established European Economic Area (EEA) agreement may exercise temporary and occasional vehicle controller activity in France.

Where the Member State does not regulate the exercise of the activity or its training, the national must have carried out this activity in one or more Member States for at least one year in the last ten years.

Once it fulfils these conditions, it must, before any provision of services, make a prior declaration to the competent authority (see infra "5°. b. Pre-declaration for the EU national for a temporary and casual exercise (LPS)").

*For further information*: Ii of Section L. 323-1 of the Highway Traffic Act.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

There are no measures provided for the EU or EEA national legally established in a Member State to carry out the activity of technical controller of vehicles on a temporary basis in France.

As such, the national is subject to the same requirements as the French national (see infra "5°. a. Application for accreditation").

3°. Conditions of honorability and incompatibility
-------------------------------------------------------------

**Incompatibilities**

The vehicle technical controller may not engage in any other activity related to repair or auto trade, either independently or as an employee.

*For further information*: Article L. 323-1 and R. 323-17 of the Highway Traffic Act.

**Integrity**

In order to practise, the professional must not have been the subject of any conviction on the second ballot of his criminal record.

In addition, the EU national engaged in this activity must have no connection likely to interfere with his independence with persons, organizations or companies engaged in repair or trade activity in the automotive sector.

*For further information*: Articles L. 323-1 and R. 323-17 of the Highway Traffic Act.

4°. Sanctions
---------------------------------

The professional faces a fine of 750 euros if he carries out a technical check without complying with the regulations.

In addition, the use of the results of the technical inspection carried out for purposes other than those provided for by the regulations is prohibited.

*For further information*: Articles R. 323-19 and R. 323-20 of the Highway Traffic Act.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Application for accreditation

**Competent authority**

The professional must send his application in two copies to the prefect of the department where the control centre within which he practices is located.

**Supporting documents**

His application takes the form of a file that includes:

- his application for accreditation as a controller indicating the control centre in which he wishes to exercise (if applicable, the approved control network);
- Bulletin 2 of his criminal record;
- A valid piece of identification
- proof of his professional qualifications
- a summary sheet with the model attached to Appendix 1 of Appendix VII. Where the applicant is an EU or EEA national, he must provide an equivalent document less than three months old and written in French or, failing that, with an approved translation;
- if, the opinion of the approved control network on which it depends, or, if not attached to a network, the opinion of the central technical body following the model set out in Appendix 2 of Schedule VII of the 18 June 1991 decree;
- If the applicant is an employee, a copy of his employment contract or a letter of commitment from his employer;
- a statement on the honour:- certifying the accuracy of the information provided,
  - certifying that it is not subject to any suspension or withdrawal of accreditation,
  - certifying that it undertakes not to engage in any other activity in the repair or auto trade and not to use the information obtained during its activity for purposes other than those provided for by regulation.

**Procedure and deadlines**

The authorization for accreditation is notified to the applicant and to the control centre to which it is attached.

**Please note**

The professional can apply for both approvals, one for light vehicles and the other for heavy vehicles.

*For further information*: Article R. 323-18 of the Highway Traffic Act; Article 13 and Appendix VII of the order of 18 June 1991 mentioned above; Appendix VII of the order of 27 July 2004 above.

### b. Pre-declaration for EU national for temporary and casual exercise (LPS)

**Competent authority**

The national must send his request in writing to the prefect of the department in which he plans to carry out his services.

**Supporting documents**

His application must include the following documents, with their translation into French:

- proof of nationality
- a certificate certifying that it is legally established in an EU or EEA Member State to carry out the activity of vehicle technical controller and that it is not prohibited from practising;
- proof of his professional qualifications
- where neither access to the activity nor its exercise is regulated in that Member State, proof that it has been engaged in this activity for at least one year in the last ten years;
- a copy of his employment contract or, failing that, a letter of commitment from the control centre employing him.

**Timeframe**

Within one month, the prefect sends a receipt of a declaration to the national.

*For further information*: Article R. 323-18-1 of the Highway Traffic Act; Article 26-6 of the June 18, 1991 order.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each Member State of the European Union or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

