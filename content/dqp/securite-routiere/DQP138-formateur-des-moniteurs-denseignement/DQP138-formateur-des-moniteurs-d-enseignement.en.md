﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP138" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Road safety" -->
<!-- var(title)="Driving instructor trainer" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="road-safety" -->
<!-- var(title-short)="driving-instructor-trainer" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/road-safety/driving-instructor-trainer.html" -->
<!-- var(last-update)="2020-04-15 17:22:26" -->
<!-- var(url-name)="driving-instructor-trainer" -->
<!-- var(translation)="Auto" -->


Driving instructor trainer
============================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:26<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The motor vehicle driving instructor trainer is a professional responsible for training driving teachers on different categories of vehicles (motor vehicles, bicycles, motorcycles or HGVs).

In accordance with the provisions of Section D. 114-12 of the Public Relations Code and the Administration, any user may obtain an information certificate on the standards applicable to the professions relating to the teaching of expensive driving and road safety awareness. To do so, you should send your application to the following address: bfper-dsr@interieur.gouv.fr.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The driving and road safety teacher, who wishes to become a trainer of instructors, must have a teaching licence and obtain a state diploma specific to the training of instructors.

To obtain a teaching licence, the professional must meet the following conditions:

- Be a valid driver's licence with an expired probationary period, valid for the categories of vehicles he wishes to teach driving;
- hold a title or diploma as a driving and road safety teacher (see infra "2." Teacher diploma in driving and road safety");
- Be at least 20 years old
- meet the physical, cognitive and sensory fitness requirements required for driver's licenses in categories C1, C, D1, D, C1E, CE, D1E and DE. To do so, the individual will need to obtain the advice of a medical officer.

Once they meet these conditions, they will have to apply for administrative permission to teach (see below "5. a. Request for permission to teach").

In order to become a instructor trainer, the professional must also hold a certificate of fitness to train instructors in the driving instruction of ground-based motor vehicles (BAFM).

*For further information*: Article L. 212-1 of the Highway Traffic Act.

#### Training

**Teacher diploma in driving and road safety**

To be a driving and road safety teacher, the professional must hold one of the following diplomas or titles:

- The professional title of driving and road safety teacher;
- The certificate for the practice of driving and road safety (BEPECASER) taught before 31 December 2016.

For the teaching of driving land-based motor vehicles under the category of driver's licence, B, B1 and BE:

- Certificate of Professional Fitness to Teach Driving Ground Motor Vehicles (CAPEC),
- professional card and certificate of professional and pedagogical aptitude (CAPP),
- military titles or diplomas defined in the order of 13 September 1996 setting out the list of military diplomas recognized as equivalent to the certificate for the practice of teaching driving and road safety,
- driving education diplomas issued by overseas communities and New Caledonia.

*For further information*: Article R. 212-3 of the Highway Traffic Act.

**Certificate of Fitness to Train Driving Instructors (BAFM)**

In order to obtain this diploma, the professional must apply a[Form](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E1519F08621A0D1B2597D65D131F5A77.tplgfr41s_1?idArticle=LEGIARTI000036496244&cidTexte=LEGITEXT000006075067&dateTexte=20180424) registration for the examination, completed and signed, as well as the supporting documents mentioned, to the prefect in charge of the examination centre from which he wishes to present himself.

The list of departments attached to each of the examination centres is set at the[Appendix III](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E1519F08621A0D1B2597D65D131F5A77.tplgfr41s_1?idArticle=LEGIARTI000036496276&cidTexte=LEGITEXT000006075067&dateTexte=20180424) of the order of 23 August 1971 on the certificate of fitness to train instructors in the driving of ground-powered vehicles.

The exam for this diploma consists of written eligibility tests and oral and admission practices for the[Annex](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E1519F08621A0D1B2597D65D131F5A77.tplgfr41s_1?idArticle=LEGIARTI000028907015&cidTexte=LEGITEXT000006075067&dateTexte=20180424) I of the order of August 23, 1971 aforementioned.

**Please note**

The professional may be excused from taking written eligibility tests if he or she meets the following requirements:

- a driving teacher diploma less than one year old on the date of the eligibility tests;
- a national graduate degree or which justifies having been a teacher for at least five years in a general, technical or agricultural educational institution (secondary or higher).

*For further information*: order of 23 August 1971 relating to the certificate of fitness to train instructors teaching the driving of ground-based motor vehicles.

#### Costs associated with qualification

Training leading to the BAFM is paid for, the cost varies depending on the appropriate examination centres. It is advisable to get closer to the relevant examination centres for more information.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement, legally established and practising the activity of teaching driving and road safety to future drivers or monitors, may perform the same activity on a temporary and casual basis in France.

In order to do so, he will have to send a declaration in advance of his first performance, to the prefect of the department in which he wishes to practice (see infra "5°. b. Make a pre-declaration of activity for EU or EEA nationals for a temporary and casual exercise (LPS)).

Where the Member State does not regulate access to the activity or its exercise, the professional must justify having carried out this activity for at least one year, in the last ten years.

*For further information*: Article L. 212-1 II of the Highway Traffic Act.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any EU or EEA national, legally established and practising the activity of driving and road safety teacher or trainer, may carry out this activity on a permanent basis, in France if it justifies either:

- Hold a certificate of competency issued by a Member State that regulates the profession;
- have been doing this for a year in the last ten years in a Member State that does not regulate training or the practice of the profession.

Once the national fulfils one of these conditions, he must apply to the prefect of the department where he plans to practise (see infra "5). c. Application for qualification recognition for the EU national for a permanent exercise (LE)).

*For further information*: Article R. 212-3-1 of the Highway Traffic Act.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The driving and road safety teacher must meet conditions of honour, including not being sentenced to a criminal or correctional sentence.

The fact that a professional is a trainer of motor vehicle driving instructors without having the authorisation to teach is punishable by one year's imprisonment and a fine of 15,000 euros. He may also be barred from practising.

*For further information*: Articles L. 212-2 and L. 212-4 of the Highway Traffic Act.

4°. Insurance
---------------------------------

The professional practising in a liberal capacity must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. It is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Request for permission to teach

**Competent authority**

The prefect of the department where the national wishes to teach is competent to issue the authorization to teach.

**Supporting documents**

The national must attach the following supporting documents to the request:

- The application for teaching permission completed, dated and signed;
- Two photo IDs
- A photocopy of an ID
- proof of residence of less than six months
- Photocopying of certificates of competency or training documents obtained for the exercise of the driving teaching activity;
- Recognition of professional qualifications;
- A medical certificate issued by a licensed physician
- an extract from the criminal record or any equivalent document of less than three months.

Permission to teach, the model of which is set at the[Annex](https://www.legifrance.gouv.fr/jo_pdf.do?numJO=0&dateJO=20130117&numTexte=16&pageDebut=01099&pageFin=01100) of the decree of 10 January 2013 amending the decree of 8 January 2001 on the authorisation to teach for a fee motor vehicle and road safety, is issued for a period of five years renewable under the same conditions.

*For further information*: Article R. 212-1 of the Highway Code; order of 8 January 2001 relating to the authorisation to teach, for a fee, the driving of motor vehicles and road safety.

### b. Make a pre-declaration of activity for EU or EEA nationals for a temporary and casual exercise (LPS)

**Competent authority**

The national must submit his application to the prefect of the department in which he wishes to practice.

**Supporting documents**

Its application must include the following documents, if any, with their approved translation into French:

- A valid piece of identification
- a certificate justifying that the national is legally established in an EU or EEA state to carry out this activity, and that he does not incur any temporary or permanent ban on practising;
- proof of his professional qualifications
- any document justifying that the national has practised in an EU or EEA state for one year in the last ten years, when that state does not regulate training, access to the requested profession or its exercise.

**Time and procedure**

Upon receipt of the supporting documents, the prefect has one month to inform him, i.e.:

- That he can begin his service delivery;
- to subject him to an aptitude test if there are substantial differences between the training or professional experience of the national and those required in France. In this case, the decision on whether to authorise the benefit will take place within one month of the test;
- to inform him of any difficulties that might delay his decision. In this case, the prefect may make his decision within one month of the resolution of this difficulty, and no later than two months after notification to the national.

**Please note**

The prefect's silence within these deadlines is worth permission to perform the service.

*For further information*: II and III of Section R. 212-2 of the Highway Traffic Act.

### c. Application for recognition of qualification for the EU national for a permanent exercise (LE)

**Competent authority**

The national must submit his application to the prefect of the department in which he wishes to practice.

**Supporting documents**

His application must include:

- A request for recognition, dated and signed;
- A photocopy of an ID
- An identity photograph less than six months old
- proof of residence of less than six months
- Photocopying the two-sided driver's licence
- Photocopying of certificates of competency or training documents obtained for the exercise of the driving teaching activity;
- A statement regarding his knowledge of the French language for the practice of the profession concerned;
- if applicable, a certificate of competency or a training document justifying that the national has been engaged in the activity for at least one year in the last ten years, in a state that does not regulate access to the profession or its exercise.

**Procedure**

The prefect acknowledges receipt of the file and has one month to decide on the application.

In the event of substantial differences between the training he has received and that required in France to practice, the prefect may submit him to either an aptitude test or an adjustment course.

The aptitude test, carried out within a maximum of six months, is in the form of an interview before a jury. The two-month aptitude course must be carried out in a driving school.

After checking the documents in the file and, if necessary, after receiving the results of the chosen compensation measure, the prefect will issue the certificate of recognition of qualifications in accordance with the model set out in the[Annex](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=D3225762F0A0C9E9EA272A874A63EF2F.tplgfr42s_2?cidTexte=JORFTEXT000035589366&dateTexte=20170920) September 13, 2017.

*For further information*: decree of 13 September 2017 relating to the recognition of qualifications acquired in a Member State of the European Union or in another State party to the agreement on the European Economic Area by persons wishing to practise the professions regulated road education.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form.

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

