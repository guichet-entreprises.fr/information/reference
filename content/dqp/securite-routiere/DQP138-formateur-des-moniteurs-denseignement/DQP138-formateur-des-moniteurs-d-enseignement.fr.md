﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP138" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sécurité routière" -->
<!-- var(title)="Formateur des moniteurs d'enseignement de la conduite des véhicules à moteur" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="securite-routiere" -->
<!-- var(title-short)="formateur-des-moniteurs-d-enseignement" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/securite-routiere/formateur-des-moniteurs-d-enseignement-de-la-conduite-des-vehicules.html" -->
<!-- var(last-update)="2020-04-28" -->
<!-- var(url-name)="formateur-des-moniteurs-d-enseignement-de-la-conduite-des-vehicules" -->
<!-- var(translation)="None" -->

# Formateur des moniteurs d'enseignement de la conduite des véhicules à moteur

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-28<!-- end-var -->

## 1°. Définition de l’activité

Le formateur des moniteurs d'enseignement de la conduite des véhicules à moteur est un professionnel chargé d'assurer la formation des enseignants de la conduite sur différentes catégories de véhicules (automobile, cyclo, moto ou poids lourds).

Conformément aux dispositions de l'article D. 114-12 du Code des relations entre le public et l'Administration, tout usager peut obtenir un certificat d'information sur les normes applicables aux professions relatives à l'enseignement de la conduite à titre onéreux et la sensibilisation à la sécurité routière. Pour ce faire, il convient d'adresser votre demande à l'adresse suivante : bfper-dsr@interieur.gouv.fr.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'enseignant de la conduite et de la sécurité routière qui souhaite devenir formateur de moniteurs, doit être titulaire d'une autorisation d'enseigner et obtenir un diplôme d’État spécifique à la formation des moniteurs.

Pour obtenir une autorisation d'enseigner, le professionnel doit remplir les conditions suivantes :

- être titulaire du permis de conduire, en cours de validité et dont la délai probatoire est expiré, valable pour les catégories de véhicules dont il souhaite enseigner la conduite ;
- être titulaire d'un titre ou d'un diplôme d'enseignant de la conduite et de la sécurité routière (cf. infra « 2°. Diplôme d'enseignant de la conduite et de la sécurité routière ») ;
- être âgé d'au moins 20 ans ;
- remplir des conditions d'aptitude physique, cognitive et sensorielle, requises pour les permis de conduire des catégories C1, C, D1, D, C1E, CE, D1E et DE. Pour cela, l'intéressé devra obtenir l'avis d'un médecin agréé.

Dès lors qu'il remplit ces conditions, il devra effectuer une demande en vue d'obtenir une autorisation administrative d'enseigner (cf. infra « 5. a. Demande en vue d'obtenir une autorisation d'enseigner »).

Pour devenir formateur de moniteur, le professionnel devra en outre, être titulaire d'un brevet d'aptitude à la formation des moniteurs d'enseignement de la conduite des véhicules terrestres à moteur (BAFM).

*Pour aller plus loin* : article L. 212-1 du Code de la route.

#### Formation

##### Diplôme d'enseignant de la conduite et de la sécurité routière

Pour exercer l'activité d'enseignant de la conduite et de la sécurité routière, le professionnel doit être titulaire de l'un des diplômes ou des titres suivants :

- le titre professionnel d'enseignant de la conduite automobile et de la sécurité routière ;
- le brevet pour l'exercice de la profession d'enseignant de la conduite automobile et de la sécurité routière (BEPECASER) délivré avant le 31 décembre 2016.

Pour l'enseignement de la conduite des véhicules terrestres à moteur relevant des catégorie du permis de conduire, B, B1 et BE :

- le certificat d'aptitude professionnelle à l'enseignement de la conduite des véhicules terrestres à moteur (CAPEC),
- la carte professionnelle et le certificat d'aptitude professionnelle et pédagogique (CAPP),
- les titres ou diplômes militaires définis à l'arrêté du 13 septembre 1996 fixant la liste des diplômes militaires reconnus équivalents au brevet pour l'exercice de la profession d'enseignant de la conduite automobile et de la sécurité routière,
- les diplômes d'enseignement de la conduite délivrés par les collectivités d'outre-mer et la Nouvelle-Calédonie.

*Pour aller plus loin* : article R. 212-3 du Code de la route.

##### Brevet d'aptitude à la formation des moniteurs d'enseignement de la conduite des véhicules terrestres à moteur (BAFM)

Pour obtenir ce diplôme, le professionnel doit adresser un [formulaire d'inscription à l'examen](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=E1519F08621A0D1B2597D65D131F5A77.tplgfr41s_1?idArticle=LEGIARTI000036496244&cidTexte=LEGITEXT000006075067&dateTexte=20180424), complété et signé, ainsi que les pièces justificatives mentionnées, au préfet responsable du centre d'examen auprès duquel il souhaite se présenter.

La liste des départements rattachés à chacun des centres d'examen, est fixée à l'annexe III de l'arrêté du 23 août 1971 relatif au brevet d'aptitude à la formation des moniteurs d'enseignement de la conduite des véhicules terrestres à moteur.

L'examen en vue d'obtenir ce diplôme est composé d'épreuves écrites d'admissibilité et d'épreuves orales et pratiques d'admission portant sur le programme fixé à l'annexe I de l'arrêté du 23 août 1971 susvisé.

**À noter**

Le professionnel peut être dispensé de subir les épreuves écrites d’admissibilité dès lors qu'il remplit les conditions suivantes :

- d'un diplôme d'enseignant de la conduite datant de moins d'un an à la date des épreuves d'admissibilité ;
- d'un diplôme national de second cycle d'études supérieures ou qui justifie avoir exercé l'activité d'enseignant pendant au moins cinq ans dans un établissement d'enseignement général, technique ou agricole (secondaire ou supérieur).

*Pour aller plus loin* : arrêté du 23 août 1971 relatif au brevet d'aptitude à la formation des moniteurs d'enseignement de la conduite des véhicules terrestres à moteur.

#### Coûts associés à la qualification

La formation menant au BAFM est payante, son coût varie selon les centres d'examen compétents. Il est conseillé de se rapprocher des centres d'examen concernés pour de plus amples informations.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE), légalement établi et exerçant l'activité d'enseignant de la conduite et de la sécurité routière auprès des futurs conducteurs ou des moniteurs, peut exercer à titre temporaire et occasionnel, la même activité en France.

Pour cela, il devra adresser une déclaration préalablement à sa première prestation, au préfet du département dans lequel il souhaite exercer (cf. infra « 5°. b. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS) »).

Lorsque l’État membre ne réglemente ni l'accès à l'activité, ni son exercice, le professionnel doit justifier avoir exercé cette activité pendant au moins un an, au cours des dix dernières années.

*Pour aller plus loin* : article L. 212-1 II du Code de la route.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant de l'UE ou de l'EEE, légalement établi et exerçant l'activité d'enseignant de la conduite et de la sécurité routière ou de formateur, peut exercer cette activité à titre permanent, en France dès lors qu'il justifie soit :

- être titulaire d'une attestation de compétences délivrée par un État membre qui réglemente la profession ;
- avoir exercé cette activité pendant un an au cours des dix dernières années dans un État membre qui ne réglemente ni la formation, ni l'exercice de la profession.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant doit effectuer une demande de reconnaissance de ses qualifications auprès du préfet du département où il envisage d'exercer (cf. infra « 5. c. Demande de reconnaissance de qualification pour le ressortissant de l'UE en vue d'un exercice permanent (LE) »).

*Pour aller plus loin* : article R. 212-3-1 du Code de la route.

## 3°. Conditions d’honorabilité, règles déontologiques, éthique

L'enseignant de la conduite et de la sécurité routière doit remplir des conditions d'honorabilité et notamment de ne pas avoir fait l'objet d'une condamnation à une peine criminelle ou correctionnelle.

Le fait, pour un professionnel, d'exercer l'activité de formateur de moniteurs d'enseignement de la conduite des véhicules à moteur, sans être titulaire de l'autorisation d'enseigner est puni d'un an d'emprisonnement et de 15 000 euros d'amende. Il pourra également se voir interdire d'exercer son activité.

*Pour aller plus loin* : articles L. 212-2 et L. 212-4 du Code de la route.

## 4°. Assurances

Le professionnel exerçant à titre libéral doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. C'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.
 
## 5°. Démarches et formalités de reconnaissance de qualification

### a. Demande en vue d'obtenir une autorisation d'enseigner

#### Autorité compétente

Le préfet du département où le ressortissant souhaite enseigner est compétent pour délivrer l'autorisation d'enseigner.

#### Pièces justificatives

Le ressortissant doit joindre à demande, les pièces justificatives suivantes :

- la demande d'autorisation d'enseigner remplie, datée et signée ;
- deux photos d'identité ;
- une photocopie d'une pièce d'identité ;
- un justificatif de domicile de moins de six mois ;
- la photocopie des attestations de compétences ou titres de formation obtenus pour l'exercice de l'activité d'enseignement de la conduite ;
- la reconnaissance de qualifications professionnelles ;
- un certificat médical délivré par un médecin agréé ;
- un extrait du casier judiciaire ou tout document équivalent de moins de trois mois.

L'autorisation d'enseigner, dont le modèle est fixé à l'annexe de l'arrêté du 10 janvier 2013 modifiant l'arrêté du 8 janvier 2001 relatif à l'autorisation d'enseigner à titre onéreux la conduite des véhicules à moteur et la sécurité routière, est délivrée pour une durée de cinq ans renouvelable dans les mêmes conditions.

*Pour aller plus loin* : article R. 212-1 du Code de la route ; arrêté du 8 janvier 2001 relatif à l'autorisation d'enseigner, à titre onéreux, la conduite des véhicules à moteur et la sécurité routière.

### b. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa demande au préfet du département au sein duquel il souhaite exercer.

#### Pièces justificatives

Sa demande doit comprendre les documents suivants, le cas échéant, assortis de leur traduction agréée en français :

- une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE pour y exercer cette activité, et qu'il n'encourt aucune interdiction temporaire ou définitive d'exercer ;
- une preuve de ses qualifications professionnelles ;
- tout document justifiant que le ressortissant a exercé la profession dans un État de l'UE ou de l'EEE, pendant un an au cours des dix dernières années, lorsque cet État ne réglemente ni la formation, ni l'accès à la profession demandée ou son exercice.

#### Délai et procédure

Dès la réception des pièces justificatives, le préfet dispose d'un délai d'un mois pour l'informer, soit :

- qu'il peut commencer sa prestation de services ;
- de le soumettre à une épreuve d'aptitude dès lors qu'il existe des différences substantielles entre la formation ou l'expérience professionnelle du ressortissant et celles exigées en France. Dans ce cas, la décision d'autoriser ou non la prestation interviendra dans un délai d'un mois après l'épreuve ;
- de l'informer de toute difficulté pouvant retarder sa décision. Dans ce cas, le préfet pourra rendre sa décision dans le mois suivant la résolution de cette difficulté, et au plus tard dans les deux mois suivant sa notification au ressortissant.

**À noter** 

Le silence gardé du préfet dans ces délais vaut autorisation d'exercer la prestation.

*Pour aller plus loin* : II et III de l'article R. 212-2 du Code de la route.

### c. Demande de reconnaissance de qualification pour le ressortissant de l'UE en vue d'un exercice permanent (LE)

#### Autorité compétente

Le ressortissant doit adresser sa demande au préfet du département au sein duquel il souhaite exercer.

#### Pièces justificatives

Sa demande doit comporter :

- une demande de reconnaissance, datée et signée ;
- une photocopie d'une pièce d'identité ;
- une photographie d'identité datant de moins de six mois ;
- un justificatif de domicile de moins de six mois ;
- la photocopie recto verso du permis de conduire ;
- la photocopie des attestations de compétences ou titres de formation obtenus pour l'exercice de l'activité d'enseignement de la conduite ;
- une déclaration concernant sa connaissance de la langue française pour l'exercice de la profession concernée ;
- le cas échéant, une attestation de compétence ou un titre de formation justifiant que le ressortissant a exercé l'activité pendant au moins un an au cours des dix dernières années, dans un État qui ne réglemente ni l'accès à la profession, ni son exercice.

#### Procédure

Le préfet accuse réception du dossier et dispose d'un délai d'un mois pour se prononcer sur la demande.

En cas de différences substantielles entre la formation qu'il a reçue et celle requise en France pour exercer, le préfet pourra le soumettre soit à une épreuve d'aptitude ou à un stage d'adaptation.

L'épreuve d'aptitude, réalisée dans un délai maximal de six mois, se présente sous la forme d'un entretien devant un jury. Le stage d'aptitude d'une durée de deux mois, doit être effectué dans une école de conduite.

Après vérification des pièces du dossier et, le cas échéant, après avoir reçu les résultats de la mesure de compensation choisie, le préfet délivrera l'attestation de reconnaissance de qualifications conformément au modèle fixé à l'annexe de l'arrêté du 13 septembre 2017.

*Pour aller plus loin* : arrêté du 13 septembre 2017 relatif à la reconnaissance des qualifications acquises dans un État membre de l'Union européenne ou dans un autre État partie à l'accord sur l'Espace économique européen par les personnes souhaitant exercer les professions réglementées de l'éducation routière.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne.

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).