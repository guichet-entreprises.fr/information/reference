﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP037" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sécurité routière" -->
<!-- var(title)="Animateur de stages de sensibilisation à la sécurité routière" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="securite-routiere" -->
<!-- var(title-short)="animateur-de-stages-de-sensibilisation" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/securite-routiere/animateur-de-stages-de-sensibilisation-a-la-securite-routiere.html" -->
<!-- var(last-update)="2020-04-15 17:22:22" -->
<!-- var(url-name)="animateur-de-stages-de-sensibilisation-a-la-securite-routiere" -->
<!-- var(translation)="None" -->

# Animateur de stages de sensibilisation à la sécurité routière

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:22<!-- end-var -->

## 1°. Définition de l’activité

L'animateur de stages de sensibilisation à la sécurité routière est un professionnel dont l'activité consiste notamment à dispenser à des conducteurs ayant perdu des points sur leur permis de conduire, des stages de récupération des points et de sensibilisation à la sécurité routière.

Ces stages sont composés de modules ayant pour objectif de changer les attitudes de ces conducteurs afin qu'ils ne réitèrent pas leur comportement dangereux.

L'animation de ces stages est assurée par une équipe composée d'un enseignant de la conduite et de la sécurité routière et un psychologue, tous deux titulaires du brevet d'aptitude à la formation des moniteurs d'enseignement de la conduite des véhicules terrestres à moteur (BAFM) et de l'autorisation d'animer.

Conformément aux dispositions de l'article D. 114-12 du Code des relations entre le public et l'Administration, tout usager peut obtenir un certificat d'information sur les normes applicables aux professions relatives à l'enseignement de la conduite à titre onéreux et la sensibilisation à la sécurité routière. Pour ce faire, il convient d'adresser votre demande à l'adresse suivante : bfper-dsr@interieur.gouv.fr.

*Pour aller plus loin* : articles L. 212-1 à L. 212-5 et R. 223-5 à R. 223-13 du Code de la route.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Pour exercer l'activité d'animateur de stages de sensibilisation à la sécurité routière le professionnel doit être âgé d'au moins 25 ans et être titulaire :

- du permis de conduire en cours de validité et dont le délai probatoire a expiré ;
- d'un diplôme d'enseignant de la conduite et de la sécurité routière pour les experts en sécurité routière ;
- d'une autorisation d'exercice (cf. infra « Autorisation d'exercice ») ;
- pour les pyschologues, un justificatif de son inscription au registre national des psychologues.

*Pour aller plus loin* : article L. 212-1 du Code de la route.

#### Formations

Pour être reconnu en tant qu'expert en sécurité routière, le professionnel devra remplir toutes les conditions prévues pour exercer les fonctions d'enseignements de la conduite et de la sécurité routière. Vous pouvez obtenir plus d'information sur la fiche « [Enseignant de la conduite et de la sécurité routière](https://www.guichet-qualifications.fr/fr/dqp/securite-routiere/enseignant-de-la-conduite-et-de-la-securite-routiere.html) ».

*Pour aller plus loin* : article R. 212-2 du Code de la route ; arrêté du 26 juin 2012 relatif à l'autorisation d'animer les stages de sensibilisation à la sécurité routière.

#### Formations initiale et continue obligatoires

##### Formation initiale

Avant d'exercer, le professionnel doit effectuer une formation initiale. Pour y accéder il doit en plus de sa qualification professionnelle (cf. supra « Formation ») être titulaire de l'un des diplômes complémentaires suivants :

- pour l'enseignant, un brevet d'aptitude à la formation des moniteurs d'enseignement de la conduite des véhicules terrestres à moteur (BAFM) délivré par le ministère chargé de la sécurité routière ;
- pour l'enseignant, un brevet d'animateur pour la formation des conducteurs responsables d'infractions (BAFCRI) délivré par le ministère chargé de la sécurité routière ;
- pour l'animateur psychologue, un diplôme lui permettant de faire usage professionnel du titre de psychologue (dont la liste est fixée à l'article 1er du décret n°90-255 du 22 mars 1990 fixant la liste des diplômes permettant de faire usage du titre de psychologue).

Cette formation initiale dont l'objectif est de lui permettre d'animer des stages de sensibilisation à la sécurité routière est délivrée par l'Institut national de sécurité routière et de recherches (INSERR).

Elle se compose d'une formation théorique de cinq semaines et d'une formation pratique en alternance en vue d'observer et d'animer des séquences de stages.

Le contenu de cette formation est fixé à l'annexe 5 de l'arrêté du 26 juin 2012 susvisé.

##### Formation continue

Le professionnel doit au cours de son activité, effectuer une formation continue en vue d'actualiser ses connaissances. Cette formation d'une durée de deux à cinq jours est également dispensée par l'INSERR. Elle doit être réalisée tous les cinq ans avant tout renouvellement de l'autorisation d'animer.

Les modalités et le contenu de cette formation sont fixés à l'annexe 6 de l'arrêté du 26 juin 2012 susvisé.

*Pour aller plus loin* : article 4 de l'arrêté du 26 juin 2012 précité.

#### Autorisation d'exercice

Pour obtenir cette autorisation le professionnel doit :

- avoir au moins 25 ans ;
- être titulaire d'une attestation de suivi de formation initiale à l'animation de stages de sensibilisation à la sécurité routière ;
- être titulaire soit :
  - de l'autorisation d'enseigner en cours de validité (dont les conditions de délivrance sont fixées au I de l'article R. 212-2 du Code de la route) et d'un diplôme complémentaire dans le domaine de la formation à la sécurité routière,
  - d'un diplôme permettant de faire usage du titre de psychologue et du permis de conduire en cours de validité et dont la période probatoire est expirée ;
  - ne pas avoir fait l'objet d'une condamnation à une peine criminelle ou correctionnelle prévue à l'article R. 212-4 du Code de la route.

Dès lors qu'il remplit ces conditions, il doit effectuer une demande d'autorisation d'exercice (cf. infra « 5°. b. Demande d'autorisation d'exercice »).

**À noter**

L'autorisation d'exercice peut être retirée au professionnel dès lors, qu'il ne remplit plus les exigences requises à sa délivrance ;

L'autorisation peut être suspendue pour une durée maximale de six mois s'il encourt une des condamnations ou peines prévues à l'article R.212-4 du Code de la route.

*Pour aller plus loin* : articles L. 212-3 à L. 212-5 et II de l'article R. 212-2 du Code de la route.

#### Coûts associés à la qualification

La formation en vue d'exercer l'activité d'animateur de stages de sensibilisation à la sécurité routière est payante et son coût varie selon la voie choisie.

Pour plus de renseignements, il est conseillé de se rapprocher des établissements concernés.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE), légalement établi et exerçant l'activité d'animateur de stages de sensibilisation à la sécurité routière, peut exercer à titre temporaire et occasionnel, la même activité en France.

Lorsque ni l'accès à la profession, ni son exercice ne sont réglementés dans cet État membre, le professionnel doit justifier avoir exercé cette activité pendant au mois un an au cours des dix dernières années.

Dès lors qu'il remplit cette condition, l'intéressé devra préalablement à sa première prestation en France, effectuer une déclaration préalable en vue d'exercer cette activité (cf. « 5°. b. Déclaration préalable en vue d'un exercice temporaire et occasionnel (LPS) »).

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État de l'UE ou de l'EEE légalement établi dans cet État membre et exerçant l'activité d'animateur de stages de sensibilisation à la sécurité routière peut, exercer à titre permanent en France, la même activité.

Pour cela, il doit :

- être titulaire d'une attestation de compétences ou d'un titre de formation délivré par un État membre réglementant l'activité ou par un État tiers et reconnu par un État membre ;
- lorsque l’État ne réglemente pas la profession, avoir exercé cette activité pendant au moins un an au cours des dix dernières années et être titulaire d'au moins une attestation de compétences ou un titre de formation justifiant qu'il a été préparé à l'exercice de cette profession.

Dès lors qu'il remplit ces conditions, le professionnel doit effectuer une demande d'autorisation d'exercice (cf. « 5°. a. Demande d'autorisation d'exercice ») dans les mêmes conditions que le ressortissant français.

*Pour aller plus loin* : article R. 212-3-1 du Code de la route.

## 3°. Conditions d’honorabilité

Pour exercer, le professionnel ne doit pas avoir fait l'objet d'une condamnation pour crime ou d'une peine correctionnelle pour :

- atteinte à la personne humaine (par exemple, atteinte involontaire à la vie, mise en danger d'autrui, trafic de stupéfiants etc.) ;
- atteinte aux biens (vol, escroquerie, recel etc.) ;
- atteinte à l'autorité de l’État et à la confiance publique (outrage et rébellion avec une personne dépositaire de l'autorité publique, faux et usage de faux en écriture, faux témoignage etc.) ;
- fraude à un examen ou un concours public ;
- manquements aux obligations en droit du travail (travail dissimulé, atteinte à l'égalité entre les hommes et les femmes, emploi d'étranger en situation irrégulière etc.) ;
- un délit prévu par le Code de la route (entrave à la circulation, conduite sans permis de conduire, délit de fuite, défaut d'assurance, etc.) ;
- usage de stupéfiants.

*Pour aller plus loin* : articles L. 212-2 et R. 212-4 du Code de la route.

## 4°. Sanctions

Le professionnel encourt une peine d'un an d'emprisonnement et de 15 000 euros d'amende dès lors qu'il exerce l'activité d'animateur de stage de sensibilisation à la sécurité routière sans être titulaire de l'autorisation d'animer.

*Pour aller plus loin* : article L. 212-4 du Code de la route.

## 5°. Démarche et formalités de reconnaissance de qualification

### a. Demande d'autorisation d'exercice

#### Autorité compétente

Le professionnel doit adresser sa demande au préfet du département de son lieu de résidence sur papier libre.

#### Pièces justificatives

Sa demande doit être accompagnée des documents suivants :

- un justificatif d'identité ;
- un justificatif de domicile ou, s'il n'est pas salarié, une déclaration d'établissement sur le territoire national ;
- une photocopie recto verso de son permis de conduire en cours de validité ;
- une photocopie de ses diplômes ou qualifications ;
- pour le ressortissant étranger, un titre de séjour en cours de validité ;
- une photocopie de son autorisation d'enseigner en cours de validité dès lors que le professionnel est animateur expert en sécurité routière ;
- si le professionnel exerce en qualité de psychologue, une photocopie de son justificatif d'inscription au registre national des psychologues (Fichier Répertoire partagé des professionnels de santé (RPPS) ou anciennement ADELI) ;
- une photocopie de son attestation de suivi de la formation initiale de stage à la sensibilisation à la sécurité routière dont le modèle est fixé à l'annexe 2 de l'arrêté du 26 juin 2012 précité.

#### Procédure et délais

Le préfet accuse réception de la demande dans un délai d'un mois. L'autorisation d'exercice dont le modèle est fixé à l'annexe 3 de l'arrêté du 26 juin 2012 est délivrée pour une durée de cinq ans.

**À noter**

L'autorisation peut faire l'objet d'une demande de renouvellement qui doit être adressée par le professionnel au moins deux mois avant son expiration.

*Pour aller plus loin* : arrêté du 26 juin 2012 précité.

### b. Déclaration préalable en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le professionnel doit adresser sa demande par tout moyen, au préfet du département au sein duquel il souhaite exercer.

#### Pièces justificatives

Sa demande doit comporter les documents suivants, le cas échéant assortis de leur traduction en français :

- une pièce d'identité ou un justificatif de sa nationalité pour le ressortissant UE ;
- pour le ressortissant de l'UE, une attestation certifiant qu'il est légalement établi dans un pays membre pour y exercer l'activité d'animateur de stages de sensibilisation à la sécurité routière et qu'il n'encourt aucune interdiction temporaire ou définitive d'exercer ni aucune condamnation pénale pour une infraction au Code de la route ;
- un justificatif de ses qualifications professionnelles ;
- le cas échéant, la preuve qu'il a exercé cette activité pendant au moins un an au cours des dix dernières années dans un État membre.

#### Procédure

Dans un délai d'un mois suivant la réception de la demande, le préfet peut soit autoriser le demandeur à exercer l'activité, soit en vue de l'examen de ses qualifications, décider de le soumettre à une épreuve d'aptitude (cf. ci-après « Bon à savoir : mesures de compensation »).

Le silence gardé par le préfet au delà d'un mois vaut autorisation d'exercer. Cette autorisation est délivrée pour une durée de cinq ans.

#### Bon à savoir : mesures de compensation

Lorsqu'il existe des différences substantielles entre la formation reçue par le ressortissant et celle exigée en France pour exercer l'activité d'animateur de stage de sensibilisation à la sécurité routière, le préfet peut décider de le soumettre à une épreuve d'aptitude. Cette épreuve d'aptitude prend la forme d'un examen écrit dont les résultats lui seront communiqués dans un délai de trente jours.

*Pour aller plus loin* : articles R. 212-1 et R. 212-2 du Code de la route.

### c. Voies de recours

#### Centre d’assistance Français

Le Centre ENIC-NARIC est le centre français d’informations sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).