﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP037" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Road safety" -->
<!-- var(title)="Road safety awareness trainer" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="road-safety" -->
<!-- var(title-short)="road-safety-awareness-trainer" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/road-safety/road-safety-awareness-trainer.html" -->
<!-- var(last-update)="2020-04-15 17:22:22" -->
<!-- var(url-name)="road-safety-awareness-trainer" -->
<!-- var(translation)="Auto" -->


Road safety awareness trainer
============================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:22<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The facilitator of road safety awareness courses is a professional whose activity includes providing drivers who have lost points on their driver's licence with points in points and points awareness road safety.

These courses are composed of modules aimed at changing the attitudes of these drivers so that they do not repeat their dangerous behavior.

These courses are facilitated by a team consisting of a driving and road safety teacher and a psychologist, both of which hold the certificate of fitness to train land-based vehicle driving instructors at (BAFM) and the authorisation to animate.

In accordance with the provisions of Section D. 114-12 of the Public Relations Code and the Administration, any user may obtain an information certificate on the standards applicable to the professions relating to the teaching of expensive driving and road safety awareness. To do so, you should send your application to the following address: bfper-dsr@interieur.gouv.fr.

*For further information*: Articles L. 212-1 to L. 212-5 and R. 223-5 to R. 223-13 of the Highway Traffic Act.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

In order to carry out the activity of facilitator of road safety awareness courses the professional must be at least 25 years old and hold:

- valid driver's licence and whose probationary period has expired;
- a driving and road safety teacher's degree for road safety experts;
- an exercise authorization (see below "Authorization for Exercise");
- for pyschologists, a proof of its registration in the National Register of Psychologists.

*For further information*: Article L. 212-1 of the Highway Traffic Act.

#### Training

To be recognized as a road safety expert, the professional must meet all the requirements to carry out the teaching functions of driving and road safety. You can get more information on the listing[Teaching driving and road safety](https://www.guichet-qualifications.fr/fr/professions-reglementees/enseignant-de-la-conduite-et-de-la-securite-routiere/).

*For further information*: Article R. 212-2 of the Highway Traffic Act; order of 26 June 2012 on authorisation to lead road safety awareness courses.

#### Mandatory initial and continuing training

**Initial training**

Before practising, the professional must complete initial training. In order to gain access, he must have one of the following additional degrees in addition to his professional qualification (see above "Training"):

- for the teacher, a certificate of fitness to train instructors teaching driving ground-based motor vehicles (BAFMs) issued by the Ministry of Road Safety;
- for the teacher, a facilitator's certificate for the training of drivers responsible for offences (BAFCRI) issued by the Ministry of Road Safety;
- for the psychologist facilitator, a diploma allowing him to make professional use of the title of psychologist (the list of which is fixed in Article 1 of Decree No. 90-255 of 22 March 1990 setting out the list of diplomas to make use of the title of psychologist).

This initial training, which aims to enable him to lead road safety awareness courses, is delivered by the National Institute for Road Safety and Research (INSERR).

It consists of five weeks of theoretical training and practical training on an alternating basis to observe and animate sequences of internships.

The content of this training is fixed on Schedule 5 of the order of 26 June 2012 as noted.

**Continuous training**

The professional must carry out continuous training during his activity in order to update his knowledge. This two- to five-day training is also provided by inSERR. It must be carried out every five years before any renewal of the authorisation to animate.

The terms and contents of this training are set out in Schedule 6 of the order of 26 June 2012 as noted.

*For further information*: Article 4 of the order of 26 June 2012 above.

#### Authorization to exercise

To obtain this authorization the professional must:

- Be at least 25 years old
- Have a certificate of initial training training in the development of road safety awareness courses;
- be the holder:- valid teaching authorization (the conditions of which are set out in I of Article R. 212-2 of the Highway Code) and a further diploma in the field of road safety training,
  - a diploma to use the title of psychologist and the valid driver's license and whose probationary period has expired;
  - not have been sentenced to a criminal or correctional sentence under section R. 212-4 of the Highway Traffic Act.

Once he meets these conditions, he must apply for an exercise authorization (see infra "5°. b. Application for permission to practice").

**Please note**

The authorisation to exercise may be withdrawn from the professional, as long as he no longer meets the requirements required for its issuance;

Permission may be suspended for up to six months if it incurs one of the convictions or penalties under section R.212-4 of the Highway Traffic Act.

*For further information*: Articles L. 212-3 to L. 212-5 and II of Section R. 212-2 of the Highway Traffic Act.

#### Costs associated with qualification

Training to carry out road safety awareness training courses is paid for and the cost varies depending on the route chosen.

For more information, it is advisable to get close to the institutions concerned.

### b. EU nationals: for temporary and casual exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) or a State party to the European Economic Area (EEA) agreement, which is legally established and is involved in the work of a road safety awareness course, may work as a member of the European Economic Area (EEA) temporary and casual, the same activity in France.

Where neither access to the profession nor its exercise is regulated in that Member State, the professional must justify having carried out this activity for a month to one year in the last ten years.

Once he fulfils this condition, the person concerned must, in advance of his first performance in France, make a prior declaration in order to carry out this activity (cf. "5o. b. Pre-declaration for temporary and casual exercise (LPS)").

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU state or the EEA legally established in that Member State who is a facilitator of road safety awareness courses may, on a permanent basis, carry out the same activity in France.

To do this, he must:

- Have a certificate of competency or training certificate issued by a Member State regulating the activity or by a third state and recognised by a Member State;
- where the state does not regulate the profession, have been engaged in this activity for at least one year in the past ten years and hold at least one certificate of competency or training document justifying that it has been prepared for the exercise of this profession.

Once the professional meets these conditions, he or she must apply for an exercise permit (see "5.00. (a.) application for authorisation to exercise) under the same conditions as the French national.

*For further information*: Article R. 212-3-1 of the Highway Traffic Act.

3°. Conditions of honorability
-----------------------------------------

To practice, the professional must not have been convicted of a felony or a correctional sentence for:

- harm to the human person (e.g., involuntary harm to life, endangering others, trafficking in narcotics, etc.);
- damage to property (theft, fraud, receiving, etc.);
- infringement of state authority and public trust (outrage and rebellion with a person who is a custodian of public authority, forgery and the use of forgery in writing, false testimony, etc.);
- fraud in a public examination or competition
- breaches of labour law obligations (hidden work, violation of equality between men and women, employment of illegal aliens, etc.);
- a traffic offence (impeding traffic, driving without a driver's licence, hit-and-run, lack of insurance, etc.);
- use of narcotics.

*For further information*: Articles L. 212-2 and R. 212-4 of the Highway Traffic Act.

4°. Sanctions
---------------------------------

The professional faces a one-year prison sentence and a fine of 15,000 euros if he works as a road safety awareness course facilitator without having the authorisation to lead.

*For further information*: Article L. 212-4 of the Highway Traffic Act.

5°. Qualification recognition process and formalities
---------------------------------------------------------------

### a. Application for permission to practice

**Competent authority**

The professional must submit his request to the prefect of the department of his place of residence on free paper.

**Supporting documents**

His application must be accompanied by the following documents:

- Proof of identity
- a proof of residence or, if not an employee, a declaration of establishment on the national territory;
- A two-sided photocopy of his valid driver's licence
- A photocopy of his diplomas or qualifications
- for the foreign national, a valid residence permit;
- A photocopy of his valid teaching authorization as long as the professional is an expert road safety facilitator;
- If the professional is practising as a psychologist, a photocopy of his or her proof of registration in the National Register of Psychologists (Shared Directory file of health professionals (RPPS) or formerly ADELI);
- a photocopy of his certificate of follow-up to the initial training course in road safety awareness, the model of which is set out in Schedule 2 of the order of 26 June 2012 mentioned above.

**Procedure and deadlines**

The prefect acknowledges receipt of the request within one month. The exercise authorization, modelled on Schedule 3 of the June 26, 2012 order, is issued for a period of five years.

**Please note**

The authorization may be subject to a renewal application that must be submitted by the professional at least two months before its expiry.

*For further information*: 26 June 2012.

### b. Pre-declaration for temporary and casual exercise (LPS)

**Competent authority**

The professional must apply by any means to the prefect of the department in which he wishes to practice.

**Supporting documents**

Its application must include the following documents, if any, with their translation into French:

- A piece of identification or proof of nationality for the EU national;
- for the EU national, a certificate certifying that he is legally established in a member country to carry out the activity of facilitator of road safety awareness courses and that he does not incur any temporary or permanent bans to practise or criminal convictions for a traffic violation;
- proof of his professional qualifications
- if so, proof that he has been engaged in this activity for at least one year in the last ten years in a Member State.

**Procedure**

Within one month of receiving the application, the prefect may either authorize the applicant to carry out the activity or for the examination of his qualifications, decide to submit him to an aptitude test (see below "Good to know: measures of compensation").

The silence kept by the prefect beyond one month is worth permission to practice. This authorization is issued for a period of five years.

**Good to know: compensation measures**

Where there are substantial differences between the training received by the national and that required in France to carry out the activity of a road safety awareness course, the prefect may decide to submit it to an event aptitude. This aptitude test takes the form of a written exam, the results of which will be communicated to him within thirty days.

*For further information*: Articles R. 212-1 and R. 212-2 of the Highway Traffic Act.

### c. Remedies

#### French Assistance Centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

