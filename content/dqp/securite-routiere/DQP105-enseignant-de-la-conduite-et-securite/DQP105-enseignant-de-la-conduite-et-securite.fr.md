﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP105" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Sécurité routière" -->
<!-- var(title)="Enseignant de la conduite et de la sécurité routière" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="securite-routiere" -->
<!-- var(title-short)="enseignant-de-la-conduite-et-securite" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/securite-routiere/enseignant-de-la-conduite-et-de-la-securite-routiere.html" -->
<!-- var(last-update)="2020-04-15 17:22:23" -->
<!-- var(url-name)="enseignant-de-la-conduite-et-de-la-securite-routiere" -->
<!-- var(translation)="None" -->

# Enseignant de la conduite et de la sécurité routière

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:22:23<!-- end-var -->

## 1°. Définition de l'activité

L'enseignant de la conduite et de la sécurité routière est un professionnel dont la mission est d'assurer la formation théorique et pratique des conducteurs sur différentes catégories de véhicules (automobile, cyclo, moto ou poids lourd).

Il permet aux élèves de développer des compétences techniques et comportementales afin d'en faire des conducteurs responsables.

Les leçons théoriques sont généralement données lors de cours collectifs pendant lesquels l'enseignant se concentrera sur l'apprentissage des règles de conduite et des panneaux de signalisation.

Conformément aux dispositions de l'article D. 114-12 du Code des relations entre le public et l'Administration, tout usager peut obtenir un certificat d'information sur les normes applicables aux professions relatives à l'enseignement de la conduite à titre onéreux et la sensibilisation à la sécurité routière. Pour ce faire, il convient d'adresser votre demande à l'adresse suivante : bfper-dsr@interieur.gouv.fr.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'exercice de la profession d'enseignant de la conduite et de la sécurité routière est réservé aux titulaires d'une autorisation administrative d'enseigner, qui remplissent les conditions suivantes :

- être titulaire du permis de conduire, en cours de validité dont le délai probatoire est expiré, valable pour les catégories de véhicules dont il souhaite enseigner la conduite ;
- être titulaire d'un titre ou d'un diplôme d'enseignant de la conduite et de la sécurité routière ou, selon conditions, être en cours de formation pour sa préparation. Dans ce dernier cas, l'autorisation sera délivrée à titre temporaire et restrictive ;
- le cas échéant, la reconnaissance de qualifications professionnelles pour le ressortissant d'un État membre de l'Union européenne (UE) ou d'un État partie à l'accord sur l'Espace économique européen (EEE) ;
- être âgé d'au moins 20 ans ;
- remplir des conditions d'aptitude physique, cognitive et sensorielle, requises pour les permis de conduire des catégories C1, C, D1, D, C1E, CE, D1E et DE. Pour cela, l'intéressé devra obtenir l'avis d'un médecin agréé.

*Pour aller plus loin* : article L. 212-1, 212-2 et R. 212-2 du Code de la route.

#### Formation

Pour être enseignant de la conduite et de la sécurité routière, l'intéressé doit être titulaire d'un des titres ou diplômes suivants pour enseigner la conduite des véhicules des catégorie B, B1 et BE du Code de la route :

- le titre professionnel d'enseignant de la conduite automobile et de la sécurité routière ;
- le brevet pour l'exercice de la profession d'enseignant de la conduite automobile et de la sécurité routière (BEPECASER) délivré avant le 31 décembre 2016 ;
- le certificat d'aptitude professionnelle à l'enseignement de la conduite des véhicules terrestres à moteur (CAPEC) ;
- la carte professionnelle et le certificat d'aptitude professionnelle et pédagogique (CAPP) ;
- les titres ou diplômes militaires définis à l'arrêté du 13 septembre 1996 fixant la liste des diplômes militaires reconnus équivalents au brevet pour l'exercice de la profession d'enseignant de la conduite automobile et de la sécurité routière ;
- les diplômes d'enseignement de la conduite délivrés par les collectivités d'outre-mer et la Nouvelle-Calédonie.

Pour enseigner la conduite des véhicules des catégories AM, A1, A2, C1, C, D1, D, C1E, CE, D1E et DE du permis de conduire, l'intéressé doit être titulaire d'un des titres ou diplômes suivants :

- le titre professionnel d'enseignant de la conduite automobile et de la sécurité routière et les certificats de spécialisation de ce titre délivrés par le ministre chargé de l'emploi en application des articles R. 338-1 et suivants du Code de l'éducation,
- le brevet pour l'exercice de la profession d'enseignant de la conduite automobile et de la sécurité routière (BEPECASER) obtenu avant le 31 décembre 2016 et les mentions «deux roues" et "groupe lourd» de ce même diplôme obtenues avant le 31 décembre 2019, dans des conditions fixées par un arrêté du ministre chargé de la sécurité routière,
- le certificat d'aptitude professionnelle à l'enseignement de la conduite des véhicules terrestres à moteur (CAPEC), pour les personnes ayant subi avec succès la ou les épreuves correspondantes auxdites mentions,
- les titres ou diplômes mentionnés aux b, c et d du 1° du II à la condition que les titulaires aient été en possession, le 1er janvier 1982, des catégories de permis de conduire correspondantes,
- une qualification professionnelle satisfaisant aux conditions prévues à l'article R. 212-3-1,
- un diplôme d'enseignement de la conduite délivré par un État qui n'est ni membre de l'Union européenne ni partie à l'accord sur l'Espace économique européen et reconnu pour l'exercice de la profession d'enseignant de la conduite automobile et de la sécurité routière par décision du ministre chargé de la sécurité routière.

*Pour aller plus loin* : arrêté du 20 avril 2016 relatif au titre professionnel d'enseignant(e) de la conduite et de la sécurité routière; article R. 212-3 du Code de la route.

#### Coûts associés à la qualification

La formation menant à l'un de ces titres et notamment le titre professionnel est payante. Pour plus d'informations, il est conseillé de se rapprocher des organismes la délivrant.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État de l'UE ou de l'EEE, légalement établi et exerçant l'activité d'enseignant de la conduite et de la sécurité routière, peut exercer à titre temporaire et occasionnel, la même activité en France.

Il devra en faire la demande, préalablement à sa première prestation, par déclaration adressée au préfet du département dans lequel il souhaite faire la prestation (cf. infra  « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS) »).

Lorsque la profession n'est réglementée ni dans le cadre de l'activité, ni de la formation, dans le pays dans lequel le professionnel est légalement établi, il doit avoir exercé cette activité pendant au moins un an, au cours des dix dernières années précédant la prestation, dans un ou plusieurs États de l'UE ou de l'EEE.

*Pour aller plus loin* : article L. 212-1 II et R.212-3-1 du Code de la route.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE, qui est établi et exerce légalement l'activité d'enseignant de la conduite et de la sécurité routière dans cet État, peut exercer la même activité en France de manière permanente s’il :

- est titulaire d'une attestation délivrée par l'autorité compétente d'un État de l'UE ou de l'EEE qui réglemente l'accès à la profession ou son exercice ;
- a exercé la profession à temps plein ou à temps partiel, pendant un an au cours des dix dernières années dans un autre État qui ne réglemente ni la formation, ni l'exercice de la profession.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant devra, au préalable, demander la reconnaissance de ses qualifications professionnelles auprès du préfet du département où il envisage d'exercer (cf. infra  « 5°. b. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant de l'UE ou de l'EEE en vue d'un exercer permanent (LE) »).

La reconnaissance de ses qualifications professionnelles lui permettra de demander, par la suite, l'autorisation d'enseigner auprès de la même autorité compétente (cf. infra  « 5°. c. Obtenir une autorisation administrative d'enseigner »).

Toutefois, si l’examen des qualifications professionnelles attestées par les titres de formation et l’expérience professionnelle fait apparaître des différences substantielles avec les qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé devra se soumettre à une mesure de compensation.

*Pour aller plus loin* : article R. 212-3-1 du Code de la route.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

L'enseignant de la conduite et de la sécurité routière doit remplir des conditions d'honorabilité et notamment ne pas avoir fait l'objet d'une condamnation à une peine criminelle ou correctionnelle prévues à l'article R. 212-4 du Code de la route.

Le fait de ne pas être titulaire de l'autorisation d'enseigner est puni d'un an d'emprisonnement et de 15 000 euros d'amende. L'enseignant de la conduite et de la sécurité routière pourra également se voir interdire d'exercer son activité.

*Pour aller plus loin* : articles L. 212-2, L. 212-4 et R. 212-4 du Code de la route.

## 4°. Assurance

L'enseignant de la conduite et de la sécurité routière exerçant à titre libéral doit souscrire à une assurance de responsabilité civile professionnelle.

En revanche, s'il exerce en tant que salarié, cette assurance n'est que facultative. C'est à l'employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l'occasion de leur activité professionnelle.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants de l’UE ou de l'EEE exerçant une activité temporaire et occasionnelle (LPS)

#### Autorité compétente

Le préfet du département dans lequel le ressortissant souhaite enseigner la conduite est compétent pour se prononcer sur la demande de déclaration préalable d'activité.

#### Pièces justificatives

La demande s'effectue par le dépôt d'un dossier comprenant les documents suivants :

- une pièce d'identité en cours de validité ;
- une attestation justifiant que le ressortissant est légalement établi dans un État de l'UE ou de l'EEE pour y exercer cette activité, et qu'il n'encourt aucune interdiction temporaire ou définitive d'exercer ;
- une preuve de ses qualifications professionnelles ;
- tout document justifiant que le ressortissant a exercé la profession dans un État de l'UE ou de l'EEE, pendant un an au cours des dix dernières années, lorsque cet État ne réglemente ni la formation, ni l'accès à la profession demandée ou son exercice.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

A réception des pièces justificatives, le préfet dispose d'un délai d'un mois pour informer le ressortissant de sa décision qui peut être soit :

- d'autoriser la prestation ;
- de le soumettre à une épreuve d'aptitude dès lors qu'il existe des différences substantielles entre la formation ou l'expérience professionnelle du ressortissant et celles exigées en France. Dans ce cas, la décision d'autoriser ou non la prestation interviendra dans un délai d'un mois après l'épreuve ;
- de l'informer de toute difficulté pouvant retarder sa décision. Dans ce cas, le préfet pourra rendre sa décision dans le mois suivant la résolution de cette difficulté, et au plus tard dans les deux mois suivant sa notification au ressortissant.

Le silence gardé du préfet dans ces délais vaut autorisation d'exercer la prestation.

*Pour aller plus loin* : article R. 212-2 du Code de la route.

### b. Demander la reconnaissance de ses qualifications professionnelles pour le ressortissant de l'UE ou de l'EEE en vue d'un exercice permanent (LE)

#### Autorité compétente

Le préfet du département où le ressortissant souhaite enseigner est compétent pour se prononcer sur la demande de reconnaissance de qualifications professionnelles.

#### Pièces justificatives

La demande se fait par le dépôt d'un dossier comprenant les pièces justificatives suivantes :

- la demande de reconnaissance à solliciter auprès des services du préfet de département, datée et signée ;
- une photocopie d'une pièce d'identité ;
- une photographie d'identité datant de moins de six mois ;
- un justificatif de domicile de moins de six mois ;
- la photocopie recto verso du permis de conduire ;
- la photocopie des attestations de compétences ou titres de formation obtenus pour l'exercice de l'activité d'enseignement de la conduite ;
- une déclaration concernant sa connaissance de la langue française pour l'exercice de la profession concernée ;
- le cas échéant, une attestation de compétence ou un titre de formation justifiant que le ressortissant a exercé l'activité pendant au moins un an au cours des dix dernières années, dans un État qui ne réglemente ni la formation ni son exercice.

#### Procédure

Le préfet accuse réception du dossier dans un délai d'un mois et dispose d'un délai de deux mois à compter de cette même date pour se prononcer sur la demande.

En cas de différences substantielles entre la formation qu'il a reçue et celle requise en France, le préfet pourra le soumettre à une épreuve d'aptitude ou un stage d'adaptation.

L'épreuve d'aptitude, réalisée dans un délai maximal de six mois, se présente sous la forme d'un entretien devant un jury. Le stage d'aptitude, quant à lui, est effectué pendant deux mois minimum, dans une école de conduite agréée.

Après vérification des pièces du dossier et, le cas échéant, après avoir reçu les résultats de la mesure de compensation choisie, le préfet délivrera la reconnaissance de qualifications dont le modèle est précisé dans l'annexe de l'arrêté du 13 septembre 2017.

*Pour aller plus loin* : arrêté du 13 septembre 2017 relatif à la reconnaissance des qualifications acquises dans un État membre de l'Union européenne ou dans un autre État partie à l'accord sur l'Espace économique européen par les personnes souhaitant exercer les professions réglementées de l'éducation routière.

### c. Obtenir une autorisation administrative d'enseigner

#### Autorisation d'enseigner

##### Autorité compétente

Le préfet du département où le ressortissant réside ou souhaite enseigner (pour les demandeurs domiciliés à l'étranger) est compétent pour délivrer l'autorisation d'enseigner.

##### Pièces justificatives

Le ressortissant doit joindre à demande, les pièces justificatives suivantes :

- la demande d'autorisation d'enseigner remplie à solliciter auprès des services du préfet du département, datée et signée ;
- deux photos d'identité identiques et récentes;
- une photocopie d'une pièce d'identité ;
- la justification qu'il est en règle à l'égard de la législation et de la réglementation concernant les étrangers en France ;
- un justificatif de domicile ;
- la photocopie des attestations de compétences ou titres de formation obtenus pour l'exercice de l'activité d'enseignement de la conduite ;
- la reconnaissance de qualifications professionnelles ;
- un certificat médical délivré par un médecin agréé.

L'autorisation est délivrée pour une durée de cinq ans, renouvelable. Deux mois avant son expiration, le ressortissant devra en solliciter le renouvellement en transmettant les mêmes pièces justificatives au préfet de département.

*Pour aller plus loin* : articles R. 212-1 et suivants du Code de la route ; arrêté du 8 janvier 2001 relatif à l'autorisation d'enseigner, à titre onéreux, la conduite des véhicules à moteur et la sécurité routière.

#### Autorisation temporaire et restrictive d'enseigner

L'autorisation temporaire et restrictive est accordée à l'intéressé qui serait en cours de formation à la profession d'enseignant de la conduite.

Le préfet du département où se situe l'établissement d'enseignement de la conduite avec lequel le demandeur envisage d'exercer, est compétent pour délivrer l'autorisation.

Outre remplir les conditions prévues au  « 2°. a. Exigences nationales », il devra également :

- avoir souscrit un contrat de travail avec un établissement agréé d'enseignement de la conduite ;
- être titulaire des certificats de compétences professionnelles composant le titre professionnel d'enseignant de la conduite délivré par le ministre chargé de l'emploi ;
- être inscrit à une session d'examen permettant de compléter la validation des compétences nécessaires à l'obtention du titre d'enseignant de la conduite et de la sécurité routière.

**À savoir**

Cette autorisation n'est accordée que pour douze mois.

*Pour aller plus loin* : Arrêté du 13 avril 2016 relatif à l'autorisation temporaire et restrictive d'exercer mentionnée à l'article R. 212-1 du Code de la route ; article R. 212-2 I bis du Code la route.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

A l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Information supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).