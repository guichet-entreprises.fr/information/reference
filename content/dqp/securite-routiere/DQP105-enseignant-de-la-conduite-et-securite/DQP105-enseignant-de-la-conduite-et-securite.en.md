﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP105" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Road safety" -->
<!-- var(title)="Driving instructor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="road-safety" -->
<!-- var(title-short)="driving-instructor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/road-safety/driving-instructor.html" -->
<!-- var(last-update)="2020-04-15 17:22:24" -->
<!-- var(url-name)="driving-instructor" -->
<!-- var(translation)="Auto" -->


Driving instructor
===============================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:22:24<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The driving and road safety teacher is a professional whose mission is to provide theoretical and practical training for drivers on different categories of vehicles (motor, cycling, motorcycle or HGV).

It allows students to develop technical and behavioural skills in order to make them responsible drivers.

Theoretical lessons are usually given in group classes where the teacher will focus on learning the rules of conduct and road signs.

In accordance with the provisions of Section D. 114-12 of the Public Relations Code and the Administration, any user may obtain an information certificate on the standards applicable to the professions relating to the teaching of expensive driving and road safety awareness. To do so, you should send your application to the following address: bfper-dsr@interieur.gouv.fr.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The practice of teaching driving and road safety is reserved for those with an administrative authorization to teach, who meet the following conditions:

- Hold a valid driver's licence with an expired probationary period, valid for the categories of vehicles he wishes to teach driving;
- hold a title or diploma as a driving and road safety teacher or, depending on the conditions, be in training for its preparation. In the latter case, the authorization will be issued on a temporary and restrictive basis;
- If necessary, the recognition of professional qualifications for the national of a Member State of the European Union (EU) or of a State party to the Agreement on the European Economic Area (EEA);
- Be at least 20 years old
- meet the physical, cognitive and sensory fitness requirements required for driver's licenses in categories C1, C, D1, D, C1E, CE, D1E and DE. To do so, the individual will need to obtain the advice of a medical officer.

*To go further* Article L. 212-1, 212-2 and R. 212-2 of the Highway Traffic Act.

#### Training

To be a driving and road safety teacher, the person must hold one of the following qualifications or diplomas to teach the driving of The B, B1 and BE Vehicles of the Highway Traffic Act:

- The professional title of driving and road safety teacher;
- The certificate for the practice of driving and road safety (BEPECASER) taught before 31 December 2016;
- Certificate of professional fitness to teach ground-based motor vehicles (CAPEC);
- The professional card and the Certificate of Professional and Educational Aptitude (CAPP);
- military titles or diplomas defined in the order of 13 September 1996 setting out the list of military diplomas recognized as equivalent to the certificate for the practice of teaching driving and road safety;
- driving education diplomas issued by overseas communities and New Caledonia.
- to teach the driving of vehicles in the AM, A1, A2, C1, C, D1, D, C1E, CE, D1E and DE driver's license vehicles.

  - the professional title of driving and road safety teacher and the certificates of specialization of this title issued by the Minister responsible for employment in according articles R. 338-1 and following of the Education Code,
  - the certificate for the teaching profession of driving and road safety (BEPECASER) obtained before 31 December 2016 and the "two wheels" and "heavy group" mentions of the same diploma obtained before 31 December 2019, in conditions set by an order of the Minister for Road Safety,
  - The Certificate of Professional Fitness to Teach Driving Ground Motor Vehicles (CAPEC), for persons who have successfully passed the tests or tests corresponding to these mentions,
  - the titles or diplomas mentioned in the b, c and 1st of the II provided that the holders were in possession, on January 1, 1982, of the corresponding categories of driver's licence,
  - a professional qualification that meets the conditions of Article R. 212-3-1,
  - a driving education diploma issued by a state that is neither a member of the European Union nor a party to the Agreement on the European Economic Area and recognised for the practice of teaching driving and road safety by decision of the Minister responsible for road safety.

*To go further* : order of 20 April 2016 relating to the professional title of teacher of driving and road safety; Section R. 212-3 of the Highway Traffic Act.

#### Costs associated with qualification

The training leading to one of these titles and in particular the professional title pays off. For more information, it is advisable to get closer to the organizations issuing it.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of an EU or EEA state, legally established and practising the activity of driving and road safety teacher, may carry out the same activity on a temporary and casual basis in France.

He will have to apply for it, before his first performance, by declaration addressed to the prefect of the department in which he wishes to make the delivery (see infra "5°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

Where the profession is not regulated in the course of activity or training in the country in which the professional is legally established, he must have carried out this activity for at least one year, in the last ten years prior to the one or more EU or EEA states.

*To go further* Article L. 212-1 II and R.212-3-1 of the Highway Traffic Act.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

The national of an EU or EEA state, who is established and legally practises the activity of driving and road safety teacher in that state, may carry out the same activity in France on a permanent basis if he:

- holds a certificate issued by the competent authority of an EU or EEA state that regulates access to or exercise of the profession;
- has worked full-time or part-time for one year in the past ten years in another state that does not regulate training or the practice of the profession.

Once the national fulfils one of these conditions, he must first apply for recognition of his professional qualifications from the prefect of the department where he plans to practise (see infra "5o). b. Request recognition of the EU or EEA national's professional qualifications for permanent practice (LE) ").

Recognition of his professional qualifications will allow him to apply for permission to teach with the same competent authority (see infra "5o). c. Obtain administrative permission to teach").

However, if the examination of the professional qualifications attested by the training credentials and the work experience shows substantial differences with the qualifications required for access to the profession and its exercise in France, the person concerned will have to submit to a compensation measure.

*To go further* Article R. 212-3-1 of the Highway Traffic Act.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The driving and road safety teacher must meet conditions of honour, including not being sentenced to a criminal or correctional sentence under section R. 212-4 of the Highway Traffic Act.

Failure to hold a teaching licence is punishable by one year's imprisonment and a fine of 15,000 euros. The driving and road safety teacher may also be prohibited from practising.

*To go further* Articles L. 212-2, L. 212-4 and R. 212-4 of the Highway Traffic Act.

4°. Insurance
---------------------------------

The liberal driving and road safety teacher must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. It is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

**Competent authority**

The prefect of the department in which the national wishes to teach driving is competent to decide on the request for prior declaration of activity.

**Supporting documents**

The application is made by filing a file that includes the following documents:

- A valid piece of identification
- a certificate justifying that the national is legally established in an EU or EEA state to carry out this activity, and that he does not incur any temporary or permanent ban on practising;
- proof of his professional qualifications
- any document justifying that the national has practised in an EU or EEA state for one year in the last ten years, when that state does not regulate training, access to the requested profession or its exercise.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

Upon receipt of the supporting documents, the prefect has one month to inform the national of his decision which can be either:

- Authorize the delivery;
- to subject him to an aptitude test if there are substantial differences between the training or professional experience of the national and those required in France. In this case, the decision on whether to authorise the benefit will take place within one month of the test;
- to inform him of any difficulties that might delay his decision. In this case, the prefect may make his decision within one month of the resolution of this difficulty, and no later than two months after notification to the national.

The prefect's silence within these deadlines is worth permission to perform the service.

*To go further* Article R. 212-2 of the Highway Traffic Act.

### b. Request recognition of professional qualifications for EU or EEA nationals for permanent exercise (LE)

**Competent authority**

The prefect of the department where the national wishes to teach is competent to decide on the application for recognition of professional qualifications.

**Supporting documents**

The application is made by filing a file that includes the following supporting documents:

- The request for recognition to be sought from the services of the department prefect, dated and signed;
- A photocopy of an ID
- An identity photograph less than six months old
- proof of residence of less than six months
- Photocopying the two-sided driver's licence
- Photocopying of certificates of competency or training documents obtained for the exercise of the driving teaching activity;
- A statement regarding his knowledge of the French language for the practice of the profession concerned;
- if applicable, a certificate of competency or a training document justifying that the national has been engaged in the activity for at least one year in the last ten years, in a state that does not regulate the training or its exercise.

**Procedure**

The prefect acknowledges receipt of the file within one month and has two months from that same date to decide on the application.

In the event of substantial differences between the training he has received and that required in France, the prefect may submit him to an aptitude test or an adaptation course.

The aptitude test, carried out within a maximum of six months, is in the form of an interview before a jury. The aptitude course is carried out for a minimum of two months at an accredited driving school.

After checking the documents in the file and, if necessary, after receiving the results of the chosen compensation measure, the prefect will issue the recognition of qualifications whose model is specified in the[Annex](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=D3225762F0A0C9E9EA272A874A63EF2F.tplgfr42s_2?cidTexte=JORFTEXT000035589366&dateTexte=20170920) September 13, 2017.

*To go further* : decree of 13 September 2017 relating to the recognition of qualifications acquired in a Member State of the European Union or in another State party to the agreement on the European Economic Area by persons wishing to practise the professions regulated road education.

### c. Obtaining administrative permission to teach

#### Permission to teach

**Competent authority**

The prefect of the department where the national resides or wishes to teach (for applicants domiciled abroad) is competent to issue the authorization to teach.

**Supporting documents**

The national must attach the following supporting documents to the request:

- The request for a teaching authorization filled to be sought from the services of the prefect of the department, dated and signed;
- Two identical and recent identity photos;
- A photocopy of an ID
- the justification it is in good standing with regard to the legislation and regulations concerning foreigners in France;
- Proof of residence
- Photocopying of certificates of competency or training documents obtained for the exercise of the driving teaching activity;
- Recognition of professional qualifications;
- a medical certificate issued by a licensed physician.

The authorization is issued for a period of five years, renewable. Two months before its expiry, the national must apply for renewal by submitting the same supporting documents to the prefect of the department.

*To go further* Articles R. 212-1 and the following of the Highway Traffic Act; order of 8 January 2001 relating to the authorisation to teach, for a fee, the driving of motor vehicles and road safety.

#### Temporary and restrictive authorisation to teach

Temporary and restrictive authorization is granted to the person who is said to be in the process of training in the driving profession.

The prefect of the department where the driving school with which the applicant plans to practice is located, is competent to issue the authorization.

In addition to meeting the conditions set out in the "2nd. a. National requirements" it will also have to:

- Have signed an employment contract with a licensed driving education institution;
- Hold the certificates of professional competency that make up the professional title of driving teacher issued by the Minister responsible for employment;
- be enrolled in an examination session to complete the validation of the skills required to obtain the title of driving and road safety teacher.

**What to know**

This authorization is only granted for twelve months.

*To go further* : Order of 13 April 2016 relating to the temporary and restrictive authorisation to practise referred to in Article R. 212-1 of the Highway Traffic Act; Article R. 212-2 I bis of the Highway Traffic Act.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**Additional information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

