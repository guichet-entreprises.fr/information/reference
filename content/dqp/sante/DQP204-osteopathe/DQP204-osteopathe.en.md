﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP204" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Osteopath" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="osteopath" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/osteopath.html" -->
<!-- var(last-update)="2020-04-15 17:21:54" -->
<!-- var(url-name)="osteopath" -->
<!-- var(translation)="Auto" -->


Osteopath
=========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:54<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

An osteopath is a health professional whose activity consists of analyzing, assessing the functional disorders of the body of his patients and treating them by performing manual and external manipulations. These manipulations may be musculoskeletal or myo-fascial (local pain from the muscles).

**Please note**

The osteopath professional is required, if he is not a doctor, to refer his patient to a professional as long as the symptoms require a diagnosis or medical treatment.

*For further information*: Decree No. 2007-435 of 25 March 2007 on the acts and conditions of osteopathy.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Can exercise osteopath activity:

- doctors, midwives, massage therapists with a university or inter-university degree from a medical university and recognized by the National Council of the Order of Physicians. It is advisable to refer to the records relating to these professions for more information;
- holders of a diploma sanctioning training in osteopathy issued by an accredited institution under the conditions of Decree No. 2014-1043 of 12 September 2014 relating to the accreditation of osteopathy training institutions (see infra "Training").

Once he fulfils one of these conditions, the person concerned must register his training document with the Regional Health Agency (see infra "5o). a. Proceed to register his title as an osteopath").

#### Osteopathy training

To practice, the professional must have been trained in osteopathy.

This five-year course is available to applicants who are at least seventeen years old on December 31 of the year they enter the training and hold a bachelor's degree.

The candidate must submit an application file to the institution in which he wishes to carry out his training. This file includes:

- A resume with a cover letter
- a school record with all its results and evaluations;
- If necessary, a certificate of work
- A copy of his bachelor's degree or equivalent title
- a certificate of schooling for senior students.

Once selected, the candidate must be interviewed to assess his or her abilities and motivations.

Each year of training is divided into theoretical and practical teaching units, the contents of which will be the subject of an annex published shortly in the official health, social protection and solidarity bulletin. In addition, the candidate must complete several practical internships during his clinical practical training and at the end of his training support a dissertation.

At the end of the course, the diploma is awarded to applicants who have:

- validated all teaching units;
- successfully supported their graduation dissertation;
- 150 full consultations and having acquired all the skills in clinical practical training.

*For further information*: decree of 12 December 2014 relating to training in osteopathy.

#### Costs associated with qualification

The cost of training varies depending on the course envisaged. For more information it is advisable to get closer to the institutions concerned.

### b. EU nationals: for temporary and occasional exercise (Freedom to provide services)

Any national of a Member State of the European Union (EU) of a State party to the Agreement on the European Economic Area (EEA) practising as an osteopath may carry out the same activity on a temporary and casual basis in France.

In order to do so, the person concerned must make a prior declaration before his first service in France (see infra "5°. b. Pre-declaration for the national for a temporary and casual exercise (LPS)").

*For further information*: Article 10 and following of Decree No. 2007-435 of March 25, 2007 relating to the practice and conditions of osteopathy.

### c. EU nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA member state can, if legally established in that state, make use of his title of osteopath in France.

To do this, the national must:

- holding a training degree issued by a Member State regulating osteopath activity and allowing it to carry out this activity;
- Where neither access to training nor exercise is regulated in that Member State, hold a training degree in osteopathy and a certificate justifying that he has been engaged in this activity for at least two years in the last ten years;
- a training certificate issued by a third state and recognised in a Member State other than France.

Once the national fulfils these conditions, he can apply for permission to use his professional title with the Director General of the Regional Health Agency (ARS) (see infra "5°. c. Application for authorization to exercise for a permanent exercise (LE)).

*For further information*: Articles 6 to 9 of Decree No. 2007-435 of March 25, 2007 on the practices and conditions of osteopathy.

3°.Ethical rules
----------------------------

The osteopath is bound by the rules of ethics applicable to his profession.

As such, he must:

- Keep ad terms of scientific progress and ensure that you maintain your skills for the exercise of your business;
- providing all of its care to its patients without discrimination;
- Exercise independently and ensure respect for the patient's professional confidentiality and right to information;
- maintain good relations with colleagues and do not act for the purpose of unfair competition.

*For further information*: Code of Ethics is available on the[Site](http://www.afosteo.org/) Association of Osteopathy (AFO).

4°. Insurance
---------------------------------

The liberal osteopath professional is required to take out liability insurance for any damage suffered by third parties during the course of his or her activity.

The minimum limits on the guarantee cannot be less than:

- eight million euros per claim per professional;
- 15 million euros per year of insurance per professional.

*For further information*: Article 1 of Decree No. 2014-1347 of November 10, 2014 on the guarantee limits of insurance contracts underwritten by osteopaths and chiropractors.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Proceed to record his title of osteopath

**Competent authority**

The professional must register his diploma, title, certificate or authorization with the director of the regional health agency of his professional residence.

**Supporting documents**

His application must include:

- The[form Cerfa No. 13777*03](https://www.formulaires.modernisation.gouv.fr/gf/cerfa_13777.do) Completed and signed interactive;
- ID
- for the French national: the original diploma, title, certificate or authorisation;
- for the EU national:- the original of his degree,
  - translation, certified by a certified translator of his diploma in French,
  - authorisation to carry out its activity in France.

**Procedure**

Once registered, the professional is registered on the register of osteopaths called Adeli (Automation of Lists) and receives an identification number.

**Cost**

Free.

*For further information*: Article 5 of the decree of 25 March 2007 above.

### b. Pre-declaration for the national for a temporary and casual exercise (LPS)

**Competent authority**

The national must send his statement by letter recommended with request for notice of receipt, to the director general of the regional health agency of his choice.

**Supporting documents**

Its application must include the following documents, if any, with their translation into French:

- A civil registration card and nationality;
- Copying his diplomas, certificates or training titles;
- a proof of the authority that has issued its diploma or title certifying that the training was carried out in a higher education institution mentioning its duration;
- The duration and content of the studies and internships carried out during the training;
- when the national has acquired his diploma, title or certificate in a third country and recognised by a Member State:- A certificate from the Member State certifying the duration and dates of the exercise of its activity,
  - if necessary, a survey of continuing education courses carried out.

**Procedure**

The Director General of the ARS decides after the opinion of the osteopaths committee composed of the director of the LRA, a doctor, a massage therapist, two osteopaths including a teacher. Within one month of receiving the application, the Director General informs the applicant:

- That it can begin service delivery
- It cannot begin service delivery;
- where there are substantial differences between his training and that required in France, he must submit to an aptitude test.

In the event of an incomplete file, the Director General of the LRA informs the applicant who has one month to provide the missing information.

In addition, in the absence of a response from the Director General of the LRA, the national may begin providing services.

**Please note**

The national is on a special list and receives a receipt and registration number. The declaration is renewable every year under the same conditions.

*For further information*: Articles 10 to 10-5 of the decree of 25 March 2007 mentioned above; decree of 25 March 2007 relating to the composition of the file and the organisation of the aptitude test and the adaptation course provided for osteopaths by Decree No. 2007-435 of 25 March 2007 relating to the acts and conditions of practice of osteopathy.

### c. Application for authorisation to exercise for the EU national for a permanent exercise (LE)

**Competent authority**

The national must submit his request to the Director General of the LRA who will decide after the opinion of the committee of osteopaths mentioned above.

**Supporting documents**

Its application must include the following documents, if any, with their translation into French:

- A valid birth certificate and ID
- A resume and cover letter
- A copy of all diplomas, certificates or titles obtained;
- A document justifying that the national has completed his training in a higher education institution;
- The content of the studies and internships carried out during its training as well as the hourly volume of teachings and internships;
- when the national has obtained his diploma or training qualification in a third country but recognised by an EU Member State or when the Member State of the country of obtainment does not regulate the exercise of the activity or its access, a certificate from the Member State certifying the length of the applicant's professional practice and the corresponding dates.

**Delays and procedures**

The Director General of the LRA acknowledges receipt of the request within one month and submits the request to the opinion of the regional commission of osteopaths, which will be responsible for verifying all the information.

**Please note**

Failure to respond within four months is a decision to dismiss the application.

**Good to know: compensation measures**

Where the training received by the national is at least one year less than that required for the French national or when it relates to subjects substantially different from those required in France, the commission responsible for assessing his professional qualifications may decide to submit it to an aptitude test or an adjustment course of no more than three years and carried out under the responsibility of a qualified professional.

*For further information*: Articles 6 to 9 of the decree of 25 March 2007 mentioned above;[official website](https://www.ars.sante.fr/) ARS.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete an online complaint form. Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

