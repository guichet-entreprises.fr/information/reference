﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP204" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Ostéopathe" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="osteopathe" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/osteopathe.html" -->
<!-- var(last-update)="2020-04-15 17:21:54" -->
<!-- var(url-name)="osteopathe" -->
<!-- var(translation)="None" -->

# Ostéopathe

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:54<!-- end-var -->

## 1°. Définition de l’activité

Un ostéopathe est un professionnel de santé dont l'activité consiste à analyser,évaluer les troubles fonctionnels du corps de ses patients et les traiter en réalisant des manipulations manuelles et externes. Ces manipulations peuvent être de nature musculo-squelettique ou myo-fasciales (douleur locale provenant des muscles).

**À noter**

Le professionnel ostéopathe est tenu, s'il n'est pas médecin, d'orienter son patient vers un professionnel dès lors que les symptômes nécessitent un diagnostic ou un traitement médical.

*Pour aller plus loin* : décret n°2007-435 du 25 mars 2007 relatif aux actes et aux conditions d'exercice de l'ostéopathie.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

Peuvent exercer l'activité d'ostéopathe les :

- médecins, sages-femmes, masseurs kinésithérapeutes titulaires d'un diplôme universitaire ou inter-universitaire délivré par une université de médecine et reconnu par le Conseil national de l'ordre des médecins. Il est conseillé de se reporter aux fiches relatives à ces professions pour de plus amples informations ;
- titulaires d'un diplôme sanctionnant une formation en ostéopathie délivré par un établissement agréé dans les conditions du décret n° 2014-1043 du 12 septembre 2014 relatif à l'agrément des établissements de formation en ostéopathie (cf. infra « Formation »).

Dès lors qu'il remplit l'une de ces conditions, l'intéressé doit procéder à l'enregistrement de son titre de formation auprès de l'Agence régionale de la santé (cf. infra « 5°. a. Procéder à l'enregistrement de son titre d'ostéopathe »).

#### Formation en ostéopathie

Pour exercer, le professionnel doit avoir suivi une formation en ostéopathie.

Cette formation, d'une durée de cinq ans est accessible aux candidats âgés d'au moins dix-sept ans le 31 décembre de l'année de leur entrée en formation et titulaires du baccalauréat.

Le candidat doit adresser un dossier de candidature à l'établissement au sein duquel il souhaite effectuer sa formation. Ce dossier comprend :

- un curriculum vitae accompagné d'une lettre de motivation ;
- un dossier scolaire avec l'ensemble de ses résultats et appréciations ;
- le cas échéant une attestation de travail ;
- une copie de son baccalauréat ou d'un titre équivalent ;
- un certificat de scolarité pour les étudiants en terminale.

Une fois retenu, le candidat doit faire l'objet d'un entretien en vue d'évaluer ses aptitudes et motivations.

Chaque année de formation est répartie en unités d'enseignements théoriques et pratiques dont le contenu fera l'objet d'une annexe publiée prochainement au bulletin officiel santé, protection sociale et solidarité. De plus, le candidat doit réaliser plusieurs stages pratiques au cours de sa formation pratique clinique et à l'issue de sa formation soutenir un mémoire.

À l'issue de la formation, le diplôme est délivré aux candidats ayant :

- validé l'ensemble des unités d'enseignement ;
- soutenu avec succès leur mémoire de fin d'études ;
- effectué cent cinquante consultations complètes et ayant acquis l'ensemble des compétences en formation pratique clinique.

*Pour aller plus loin* : arrêté du 12 décembre 2014 relatif à la formation en ostéopathie.

#### Coûts associés à la qualification

Le coût de la formation varie selon le cursus envisagé. Pour plus de renseignements il est conseillé de se rapprocher des établissements concernés.

### b. Ressortissants UE : en vue d’un exercice temporaire et occasionnel (Libre Prestation de Services)

Tout ressortissant d'un État membre de l'Union européenne (UE) d'un État partie à l'accord sur l'Espace économique européen (EEE) exerçant l'activité d'ostéopathe peut exercer à titre temporaire et occasionnel, la même activité en France.

Pour cela l'intéressé doit effectuer une déclaration préalable avant sa première prestation de services en France (cf. infra « 5°. b. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS) »).

*Pour aller plus loin* : article 10 et suivants du décret n° 2007-435 du 25 mars 2007 relatif aux actes et aux conditions d'exercice de l'ostéopathie.

### c. Ressortissants UE : en vue d’un exercice permanent (Libre Établissement)

Tout ressortissant d'un État membre de l'UE ou de l'EEE peut, s'il est légalement établi dans cet État faire usage de son titre d'ostéopathe en France.

Pour cela, le ressortissant doit :

- être titulaire d'un titre de formation délivré par un État membre réglementant l'activité d'ostéopathe et lui permettant d'exercer cette activité ;
- lorsque ni l'accès à la formation ni exercice ne sont réglementés dans cet État membre, être titulaire d'un titre de formation en ostéopathie et d'une attestation justifiant qu'il a exercé cette activité pendant au moins deux ans au cours des dix dernières années ;
- d'un titre de formation délivré par un État tiers et reconnu dans un État membre autre que la France.

Dès lors qu'il remplit ces conditions, le ressortissant peut effectuer une demande d'autorisation d'user de son titre professionnel auprès du directeur général de l'agence régionale de santé (ARS) (cf. infra « 5°. c. Demande d'autorisation d'exercice en vue d'un exercice permanent (LE) »).

*Pour aller plus loin* : articles 6 à 9 du décret n° 2007-435 du 25 mars 2007 relatif aux actes et aux conditions d'exercice de l'ostéopathie.

## 3°. Règles déontologiques

L'ostéopathe est tenu au respect des règles déontologiques applicables à sa profession.

À ce titre il doit notamment :

- se tenir informé des progrès scientifiques et veiller à maintenir ses compétences pour l'exercice de son activité ;
- prodiguer l'ensemble de ses soins à ses patients sans aucune discrimination ;
- exercer en toute indépendance et veiller au respect du secret professionnel et du droit à l'information du patient ;
- entretenir de bonnes relations avec ses confrères et ne pas agir dans un but de concurrence déloyale.

*Pour aller plus loin* : le Code de déontologie est disponible sur le [site](http://www.afosteo.org/) de l'Association française d'Ostéopathie (AFO).

## 4°. Assurance

Le professionnel ostéopathe exerçant à titre libéral est tenu de souscrire une assurance de responsabilité civile pour les éventuels dommages subis par des tiers au cours de l'exercice de son activité.

Les plafonds minimums de la garantie ne peuvent être inférieurs à :

- huit millions d'euros par sinistre et par professionnel ;
- quinze millions d'euros par année d'assurance et par professionnel.

*Pour aller plus loin* : article 1er du décret n° 2014-1347 du 10 novembre 2014 relatif aux plafonds de garantie des contrats d'assurance souscrits par les ostéopathes et les chiropracteurs.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Procéder à l'enregistrement de son titre d'ostéopathe

#### Autorité compétente

Le professionnel doit faire enregistrer son diplôme, titre, certificat ou autorisation auprès du directeur de l'agence régionale de santé de sa résidence professionnelle.

#### Pièces justificatives

Sa demande doit comporter :

- le [formulaire Cerfa n° 13777](https://www.service-public.fr/professionnels-entreprises/vosdroits/R42305) interactif complété et signé ;
- une pièce d'identité ;
- pour le ressortissant français : l'original de son diplôme, titre, certificat ou autorisation ;
- pour le ressortissant UE :
  - l'original de son diplôme,
  - la traduction, certifiée par un traducteur agréé de son diplôme en français,
  - l'autorisation d'exercice de son activité en France.

#### Procédure

Une fois enregistré, le professionnel est inscrit sur le registre des ostéopathes dit Adeli (Automatisation des listes) et reçoit un numéro d'identification.

#### Coût

Gratuit.

*Pour aller plus loin* : article 5 du décret du 25 mars 2007 précité.

### b. Déclaration préalable pour le ressortissant en vue d'un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

Le ressortissant doit adresser sa déclaration par lettre recommandée avec demande d’avis de réception, au directeur général de l'agence régionale de santé du ressort de son choix.

#### Pièces justificatives

Sa demande doit comporter les documents suivants, le cas échéant assortis de leur traduction en français :

- une fiche d'état civil et sa nationalité ;
- la copie de ses diplômes, certificats ou titres de formation ;
- un justificatif de l'autorité ayant délivré son diplôme ou titre attestant que cette formation a été effectuée dans un établissement d'enseignement supérieur mentionnant sa durée ;
- la durée et le contenu des études et des stages effectués durant la formation ;
- lorsque le ressortissant a acquis son diplôme, titre ou certificat dans un pays tiers et reconnu par un État membre :
  - une attestation de l’État membre certifiant la durée et les dates de l'exercice de son activité,
  - le cas échéant, un relevé des stages de formation permanente effectués.

#### Procédure

Le directeur général de l'ARS se prononce après avis de la commission des ostéopathes composée du directeur de l'ARS, d'un médecin, d'un masseur-kinésithérapeute, de deux ostéopathes dont un enseignant. Dans un délai d'un mois à compter de la réception de la demande, le directeur général informe le demandeur soit :

- qu'il peut débuter la prestation de services ;
- qu'il ne peut pas débuter la prestation de services ;
- lorsqu'il existe des différences substantielles entre sa formation et celle exigée en France, qu'il doit se soumettre à une épreuve d'aptitude.

En cas de dossier incomplet, le directeur général de l'ARS informe le demandeur qui dispose d'un délai d'un mois pour fournir les informations manquantes.

En outre, en l'absence de réponse de la part du directeur général de l'ARS, le ressortissant peut débuter sa prestation de services.

**À noter**

Le ressortissant est inscrit sur une liste particulière et reçoit un récépissé et un numéro d'enregistrement. La déclaration est renouvelable tous les ans dans les mêmes conditions.

*Pour aller plus loin* : articles 10 à 10-5 du décret du 25 mars 2007 précité ; arrêté du 25 mars 2007 relatif à la composition du dossier et aux modalités d'organisation de l'épreuve d'aptitude et du stage d'adaptation prévues pour les ostéopathes par le décret n° 2007-435 du 25 mars 2007 relatif aux actes et aux conditions d'exercice de l'ostéopathie.

### c. Demande d'autorisation d'exercice pour le ressortissant UE en vue d'un exercice permanent (LE)

#### Autorité compétente

Le ressortissant doit adresser sa demande au directeur général de l'ARS qui se prononcera après avis de la commission des ostéopathes précédemment citée.

#### Pièces justificatives

Sa demande doit comporter les documents suivants, le cas échéant assortis de leur traduction en français :

- un acte de naissance et une pièce d'identité en cours de validité ;
- un curriculum vitae et une lettre de motivation ;
- une copie de l'ensemble des diplômes, certificats ou titres obtenus ;
- un document justifiant que le ressortissant a effectué sa formation dans un établissement d'enseignement supérieur ;
- le contenu des études et stages effectués au cours de sa formation ainsi que le volume horaire des enseignements et des stages ;
- lorsque le ressortissant a obtenu son diplôme ou titre de formation au sein d'un pays tiers mais reconnu par un État membre de l'UE ou lorsque l’État membre du pays d'obtention ne réglemente pas l'exercice de l'activité ni son accès, une attestation de l’État membre certifiant de la durée de l'exercice professionnel du demandeur ainsi que les dates correspondantes.

#### Délais et procédure

Le directeur général de l'ARS accuse réception de la demande dans un délai d'un mois et soumet la demande à l'avis de la commission régionale des ostéopathes qui sera chargée de vérifier l'ensemble des informations.

**À noter**

L'absence de réponse dans un délai de quatre mois vaut décision de rejet de la demande.

#### Bon à savoir : mesures de compensation

Lorsque la formation reçue par le ressortissant est inférieure d'au moins un an à celle requise pour le ressortissant français ou lorsqu'elle porte sur des matières substantiellement différentes de celles requises en France, la commission chargée d'évaluer ses qualifications professionnelles pourra décider de le soumettre à une épreuve d'aptitude ou un stage d'adaptation n'excédant pas trois ans et effectué sous la responsabilité d'un professionnel qualifié.

*Pour aller plus loin* : articles 6 à 9 du décret du 25 mars 2007 précité ; [site officiel](https://www.ars.sante.fr/) de l'ARS.

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un formulaire de plainte en ligne. Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html)).