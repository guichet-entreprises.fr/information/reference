﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP126" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Craniofacial prosthetist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="craniofacial-prosthetist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/craniofacial-prosthetist.html" -->
<!-- var(last-update)="2020-04-15 17:21:28" -->
<!-- var(url-name)="craniofacial-prosthetist" -->
<!-- var(translation)="Auto" -->


Craniofacial prosthetist
==========

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:28<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The epithetist is a health professional who specializes in the design, manufacture and adaptation of face equipment by external prostheses (or epitheses).

Working in collaboration with cosmetic, plastic, stomatologist or maxillofacial surgeons, he performs the prostheses once the patient has been operated on, taking into account physiological and anatomical imperatives.

He will take the prints, sculpt and mold the prosthesis, and then be able to take care of his final pose on the patient.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The profession of epithetist is reserved for the holder of the state diploma of epithetist.

However, may also practice the profession, the persons justifying either:

- to hold a university degree in applied facial prosthetics who has a certificate validating a three-year professional experience with an epithetist or a health service specializing in the manufacture of prosthesis;
- a professional competency recognized by the Ministry of Veterans and Victims of War, or health insurance agencies;
- a professional competence recognised by a national commission of disabled equipment professionals.

*For further information*: Articles L. 4364-1, D. 4363-7 and the following of the Public Health Code; Articles 6 and 8 of the February 1, 2011 order on the professions of prosthetist and orthotist for the device of persons with disabilities.

#### Training

To date, only the Pierre and Marie Curie Medical School, located in Paris, has issued the diploma of applied facial prosthetics that leads to the profession of epithetist. Its access is reserved:

- holders of the state medical degree, specialists in stomatology or maxillofacial surgery;
- Holders of the state degree in dental surgery;
- holders of a degree to practice stomatology or odontology, issued by a Member State of the European Union (EU) or party to the European Economic Area (EEA);
- holders of a Certificate of Professional Qualification (CAP) or a Certificate of Professional Studies (BEP) as a dental prosthetist;
- holders of the professional certificate (BP), the professional bachelor's degree or the technical patent of the trades (BTM) of dentures;
- persons who justify five years of professional activity in a denture laboratory, an epithiates laboratory or a large equipment;
- holders of the ocularist's licence.

The student will have to submit a file to the university concerned and will be subjected to a practical test.

The selected student, having successfully passed this test, will undergo a theoretical training of 72 hours and 45 hours of practical work. At the end of his training, he will be subjected to theoretical and practical tests whose success will allow him to acquire the diploma of applied facial prosthesis.

#### Costs associated with qualification

Training leading to the diploma of applied facial prosthetics is paid for. For more information, it is advisable to get close to Pierre and Marie Curie University.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A national of a European Union (EU) or European Economic Area (EEA) state, legally practising as an epithist in one of these states, may use his or her professional title in France on a temporary or casual basis.

He will have to apply, before his first performance, by declaration addressed to the prefect of the region in which he wishes to make the delivery (see infra "5°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

Where neither the activity nor the training leading to this activity is regulated in the State in which it is legally established, the professional will have to justify having carried it out in one or more Member States for at least one year, in the ten years before the performance.

*For further information*: Articles L. 4364-6 and R. 4364-11-3 of the Public Health Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state, who is established and legally practises epithetist activity in that state, may carry out the same activity in France on a permanent basis if he:

- holds a training certificate issued by a competent authority in another Member State, which regulates access to or exercise of the profession;
- has worked full-time or part-time for one year in the last ten years in another Member State which does not regulate training or the practice of the profession.

Once the national fulfils one of these conditions, he or she will be able to apply for an individual authorisation to practise from the regional prefect in which he wishes to practice his profession (see infra "5o). b. Request an exercise permit for the EU or EEA national for permanent activity (LE) ").

*For further information*: Article L. 4364-5 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

A national who wishes to practise as an epithist in France is obliged to respect rules of good practice, including:

- the one relating to professional secrecy;
- practising in premises accessible to people with disabilities, preserving the patient's privacy and complying with hygiene and safety rules;
- perform the medical prescription without making changes unless they are medically or technically necessary;
- to register his diploma in the Adeli repertoire (see infra "5°). v. Registration with the Adeli directory").

*For further information*: Articles D. 4364-12 to D. 4364-18 of the Public Health Code; Articles 13 to 26 of the February 1, 2011 order.

4°. Insurance
---------------------------------

As a health professional, a liberal epithetist must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

#### Competent authority

The regional prefect is responsible for deciding on the request for a prior declaration of activity.

#### Supporting documents

The application is made by filing a file that includes the following documents:

- A copy of a valid ID
- A copy of the training title allowing the profession to be practised in the state of obtainment;
- a certificate, less than three months old, from the competent authority of the EU State or the EEA, certifying that the person concerned is legally established in that state and that, when the certificate is issued, there is no prohibition, even temporary, Exercise
- any evidence justifying that the national has practised the profession in an EU or EEA state for one year in the last ten years, when that state does not regulate training, access to the requested profession or its exercise;
- where the training certificate has been issued by a third state and recognised in an EU or EEA state other than France:- recognition of the training title established by the state authorities that have recognised this title,
  - any evidence justifying that the national has practiced the profession in that state for three years;
- If so, a copy of the previous statement as well as the first statement made;
- a certificate of professional civil liability.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

#### Time

Upon receipt of the file, the regional prefect will have one month to decide on the application and will inform the national:

- that he can start the performance. From then on, the prefect will register the applicant in the Adeli directory;
- that he will be subject to a compensation measure if there are substantial differences between the training or professional experience of the national and those required in France;
- he will not be able to start the performance;
- any difficulty that could delay its decision. In the latter case, the prefect will be able to make his decision within two months of the resolution of this difficulty, and no later than three months of notification to the national.

The prefect's silence within these deadlines will be worth accepting the request for declaration.

**Please note**

The return is renewable every year or at each change in the applicant's situation.

*For further information*: Order of 8 December 2017 relating to the prior declaration of service provision for genetic counsellors, medical physicists and pharmacy and hospital pharmacy preparers, as well as for occupations in Book III of Part IV of the Public Health Code.

### b. Requesting an exercise permit for the EU or EEA national for permanent activity (LE)

#### Competent authority

The authorisation to exercise is issued by the regional prefect, after advice from the Commission of Prosthetists and Orthotists for the fitting of persons with disabilities.

#### Supporting documents

The application for authorization is made by filing a file containing all of the following documents:

- A copy of a valid piece of identification and a document mentioning the national's contact information;
- A letter of application for authorisation to practice;
- A detailed description of the national's professional activity;
- references of the doctors with whom he is used to working and their assessments of the benefits provided by the candidate;
- any documentation to attest to the employer's work experience;
- as far as possible, a letter from his employer describing his activity with his assessments.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

#### Procedure

The prefect acknowledges receipt of the file within one month and will decide after having the opinion of the Commission of Prosthetists and Orthotists for the fitting of persons with disabilities. The latter is responsible for examining the knowledge and skills of the national acquired during his training or during his professional experience. It may subject the national to a compensation measure.

The silence kept by the prefect of the region within four months is a decision to reject the application for authorisation.

*For further information*: Articles R. 4364-11 to R. 4364-11-2 of the Public Health Code Articles 10-12 of the February 1, 2011 Order.

**Good to know: compensation measures**

If the examination of the professional qualifications attested by the training credentials and the professional experience shows substantial differences with the qualifications required for access to the profession of epithetist and its exercise in France, the person concerned will have to submit to a compensation measure.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority may either:

- Offer the applicant a choice between an adjustment course or an aptitude test;
- impose an adjustment course or aptitude test
- impose an adjustment course and an ordeal.

*For further information*: Article L. 4364-5 of the Public Health Code.

### c. Registration with the Adeli directory

A national wishing to practise as an epithist in France is required to register his authorisation to practice on the Adeli directory ("Automation of The Lists").

#### Competent authority

Registration in the Adeli directory is done with the regional health agency (ARS) of the place of practice.

#### Time

The application for registration is submitted within one month of taking office of the national, regardless of the mode of practice (liberal, salaried, mixed).

#### Supporting documents

In support of his application for registration, the epithetist must provide a file containing:

- the original diploma or title attesting to the training of epithetist issued by the EU State or the EEA (translated into French by a certified translator, if applicable);
- ID
- Deer Form 10906-06 completed, dated and signed.

#### Outcome of the procedure

The national's Adeli number will be directly mentioned on the receipt of the file, issued by the ARS.

#### Cost

Free.

*For further information*: Articles L. 4364-2 and D. 4364-18 of the Public Health Code.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

