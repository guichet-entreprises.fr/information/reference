﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP170" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Physiotherapist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="physiotherapist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/physiotherapist.html" -->
<!-- var(last-update)="2020-04-15 17:21:39" -->
<!-- var(url-name)="physiotherapist" -->
<!-- var(translation)="Pro" -->


# Physiotherapist

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:39<!-- end-var -->


<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->

## 1°. Definition of the profession

A physiotherapist is a professional responsible for establishing physiotherapy diagnoses and treating movement disorders and functional impairment.

He or she provides massage and remedial gymnastics, either manually or with equipment, particularly for rehabilitation on prescription, with the aim of preventing functional impairment and helping maintain functional abilities, or, if they are impaired, restoring or replacing them.

For more information, please refer to Article L. 4321-1 of the French Public Health Code.

## 2°. Professional qualifications

### a. National requirements

#### National legislation

In France the profession of physiotherapist is restricted to holders of a French state diploma (*diplôme d'État français*) in physiotherapy.

For more information, please refer to Article L. 4321-2 of the French Public Health Code.

#### Training

The French state diploma (*diplôme d’État*) entails four years of study in a physiotherapy training school (*institut de formation en masso-kinésithérapie*, IFMK) approved by the regional prefect, after completion of one year of study at university level.

To be accepted for the first year of preparatory study for the *diplôme d’État* in physiotherapy, which is subject to a *numerus clausus*, the candidate must have completed one of the following:

* The first common core year for healthcare studies (*Première année commune aux études de santé*, PACES)
* The first year of a science degree specialising in sports science (*Sciences et techniques des activités physiques et sportives*, STAPS)
* The first year of a degree in science, technology and health.

Training in an IFMK is split into two two-year cycles: the first is devoted to the basics, the science of physiotherapy and cross-disciplinary knowledge; the second is more vocational.

Upon completion of the training needed to obtain the *diplôme d’État*, practitioners will have 240 credits from their IFMK training and 60 credits from their initial year at university.

For more information, please refer to the Decree of 2 September 2015 on the *diplôme d’État* in physiotherapy and the Order of 2 September 2015 on the *diplôme d’État* in physiotherapy.

#### Costs of qualifying as a physiotherapist

The cost of training varies according to the university (for the initial year at university) and the physiotherapy school (regional councils cover training fees to varying extents).

As a guide, the national average for IFMK fees is 3,775 euros a year, according to a 2013 survey by the French Federation of Physiotherapy Students.

### b. EU citizens: temporary and occasional practice (freedom to provide services)

Citizens of the European Union (EU) or the European Economic Area (EEA) who are lawfully established in one of these states may practise their profession in France on a temporary and occasional basis provided that they send a prior declaration of practice to the prefect of the *département* in which the service will be provided.

If access to or training for the profession is not regulated in the home Member State or the Member State of establishment, the citizen must also provide proof of having practised this profession for at least two years full-time during the ten years preceding the provision of services.

The service provider’s professional qualifications must be checked prior to the first provision of services. Where there is a substantial difference between the professional qualifications of the service provider and the training required in France, such as to be harmful to public health, the competent authority will ask the service provider to provide proof that he or she has acquired the knowledge or competence lacking, in particular by means of compensation measures (see below: “Useful tip: Compensation measures”).

In every instance, an EU citizen wishing to practise in France on a temporary or occasional basis must have the knowledge of languages necessary for practising the profession and understand the system of weights and measures used in France.

For more information, please refer to Article L. 4321-11 of the French Public Health Code.

### c. EU citizens: permanent practice (freedom of establishment)

EU citizens wishing to practise permanently in France must obtain an authorisation to practise.

EU and EEA citizens may be authorised to practise in France if they have successfully completed a post-secondary course of education and hold one of the following:

* Evidence of formal qualifications issued by an EU or EEA Member State in which access to or practice of the profession is regulated and which allows the holder to practise there lawfully
* Evidence of formal qualifications issued by an EU or EEA Member State in which access to or practice of the profession is not regulated, and proof of having practised the profession in that State for at least two years full-time during the previous ten years
* Evidence of formal qualifications issued by a third country and recognised in an EU or EEA Member State other than France, allowing the holder to practise his/her profession there lawfully.

When checking of professional qualifications reveals substantial differences in comparison with the qualifications required for access to and practice of the profession in France, the holder may have to undertake either an aptitude test or an adaptation period, according to preference (see below “Useful tip: Compensation measures”).

For more information, please refer to Articles L. 4321-2 et seq. and L. 4321-4 of the French Public Health Code.

## 3°. Good character, code of conduct, ethics

### a. Compliance with the physiotherapy code of conduct

The code of conduct is binding on all physiotherapists registered with the French Chartered Society of Physiotherapy.

Physiotherapists are thus bound by the principles of good conduct and probity. They must also respect patient confidentiality.

For more information, please refer to Articles R. 4321-51 and R. 4321-54 of the French Public Health Code.

### b. Multiple activities

Physiotherapists may engage in another activity on the twofold condition that:

* Multiple activities will not jeopardise their independence, morality or professional dignity
* Multiple activities will not allow them to derive benefit from their medical prescriptions or advice.

For more information, please refer to Article R. 4321-68 of the French Public Health Code.

### c. Conditions of good repute

To be able to practise, a physiotherapist must not be:

* prohibited temporarily or permanently from practising his or her profession in France or abroad
* suspended owing to the serious risk to which practice of his or her profession would expose patients.

For more information, please refer to Articles L. 4321-10 and L. 4311-16 of the French Public Health Code.

### d. Duty of ongoing professional development

Physiotherapists must take part in a continuing professional development programme annually. This programme is designed to maintain their knowledge and skills, bring the latter up to date and improve their professional practices.

Consequently, healthcare professionals (whether employed or self-employed) must provide proof that they are undertaking professional development. The programme takes the form of training courses (classroom, non-classroom and mixed) to study, assess and improve practices and risk management. All training courses completed are recorded in a personal document certifying this training.

For more information, please refer to Articles L. 4021-1 and R. 4382-1 of the French Public Health Code.

### e. Physical aptitude

To practise his or her profession, a physiotherapist must not present an infirmity or pathology that would make practice of the profession unsafe.

For more information, please refer to Article L. 4311-18, implemented by Article L. 4321-10 of the French Public Health Code.

## 4°. Social legislation and insurance

### a. Obligation to take out professional liability insurance

As a healthcare professional, a physiotherapist practising in a private capacity must take out professional liability insurance. Conversely, for physiotherapists who are employees such insurance is optional. In this case, it is up to the employer to take out insurance for its employees to cover actions carried out in the course of their professional duties.

For more information, please refer to Article L. 1142-2 of the Public Health Code.

### b. Obligation to join the Medical Auxiliaries’ Pension Fund (*Caisse autonome de retraite et de prévoyance des auxiliaires médicaux*, CARPIMKO)

Any physiotherapist practising in a private capacity, even if part-time and employed elsewhere, is required to join CARPIMKO (a pension fund for nurses, physiotherapists, podiatrists and speech therapists).

**Supporting documents**

The applicant must send CARPIMKO the following without delay:

* The membership questionnaire [downloadable](https://www.carpimko.com/document/pdf/affiliation_declaration.pdf) from the CARPIMKO website, or a letter indicating the starting date of private practice
* A photocopy of his/her *diplôme d’État*
* A photocopy of the registration number in the Adeli (*Automatisation DEs LIstes*) automated register of the diploma issued by the regional health agency (Adeli No.) or a photocopy of the reverse side of the diploma.

## 5°. Process and procedures for recognition of professional qualifications

### a. Making a prior declaration of practice for EU citizens intending to practise on a temporary and occasional basis (freedom to provide services)

**Competent authority**

The prior declaration of practice must be sent to the National Board (*Conseil national*) of the French Chartered Society of Physiotherapy (*Ordre des masseurs-kinésithérapeutes*) before the first provision of services.

**Renewal of prior declaration**

The prior declaration must be renewed once a year if the service provider intends to provide services in France during that year.

**Supporting documents**

* The completed form for prior declaration of practice, a template for which is provided in the appendix to the Order of 20 January 2010
* A certificate confirming that professional liability insurance has been taken out for actions performed on French territory
* A photocopy of an ID document providing proof of the applicant’s nationality
* A photocopy of formal qualifications
* A certificate from the competent authority of the EU or EEA Member State in which the applicant is established, attesting to the fact that he/she is lawfully established in that country and not subject to any ban, even temporary, on practising.

**Useful tip**

Supporting documents other than the ID document must be translated into French by a sworn translator approved by the French courts or authorised to work with the judicial or administrative authorities of an EU or EEA Member State or of Switzerland.

**Timeframe**

Within one month of receiving the declaration the National Board of the French Chartered Society of Physiotherapy:

* must inform the service provider whether or not he/she can begin to provide services. It may also ask the service provider, when checking of his/her professional qualifications reveals a significant disparity with the training required in France, to provide proof that he/she has acquired the knowledge or competence lacking, in particular by means of an aptitude test or an adaptation period
* may request further information from the service provider. In this case, the initial one-month timeframe for a ruling on the prior declaration is extended by an extra month. Thus within two months of receiving the declaration the National Board must inform the service provider whether or not he/she can begin to provide services. If the checking of the service provider’s professional qualifications reveals a significant disparity with the training required in France, the National Board may also ask for proof that the service provider has acquired the knowledge or competence lacking, in particular by means of an aptitude test or an adaptation period.

Should the National Board of the French Chartered Society of Physiotherapy fail to reply within these timeframes, the services may be provided.

**Issue of receipt**

The National Board of the French Chartered Society of Physiotherapy will register the service provider in a specific list (a membership fee is not required for this) and send him/her a receipt with the registration number. The service provider must send a copy of this receipt to the national health insurance body to notify it in advance of the services provided.

**Remedies**

Decisions of the National Board of the French Chartered Society of Physiotherapy concerning freedom to provide services can be challenged within two months of their notification by lodging an appeal with the administrative court with territorial jurisdiction. The time-limit for challenging decisions will be extended to four months if the applicant is not resident in France.

**Cost**

Free of charge.

For more information, please refer to Article R. 4311-38, implemented by Article R. 4321-30 of the French Public Health Code, and the Order of 20 January 2010 on prior declaration of provision of services for practising the professions of physiotherapist, etc.

### b. Formalities for EU citizens intending to practise on a permanent basis (freedom of establishment)

#### Applying for an authorisation to practise one's profession

**Competent authority**

The application for authorisation to practise on a permanent basis must be sent by registered letter with acknowledgment of receipt to the Regional and *Département* Directorate for Youth, Sport and Social Cohesion (*Direction régionale et départementale de la jeunesse, des sports et de la cohésion sociale*, DRDJSCS) of the region where the applicant wishes to practise.

**Timeframe**

The DRDJSCS has one month from receipt of the application to check that the application file is complete. The full application file must be considered by the regional practice authorisation board within four months of the date of confirmation that the file is complete.

**Supporting documents**

The application for authorisation to practise must be in duplicate and contain:

* An application form for authorisation to practise in France, available from the website of the DRDJSCS concerned
* A self-declaration that the applicant has not filed an application for authorisation to practise in another region
* A photocopy of a valid ID document
* A copy of the formal qualifications allowing the profession to be practised in the country where they were obtained
* Any relevant documents providing proof of in-service training, experience and skills acquired in the course of professional practice
* A declaration no more than one year old from the competent authority of the EU or EEA Member State in which the applicant is established, certifying the absence of any sanctions against the applicant
* A copy of certificates from the authorities having issued the formal qualifications, specifying the details of training
* For an applicant having practised in an EU or EEA Member State that does not regulate access to or practice of the profession, any documents proving that he/she has practised in that country as a physiotherapist for at least two years full-time during the previous ten years.

These documents must be translated into French by a sworn translator where necessary.

Some DRDJSCSs may require other documents. For more details, applicants are advised to contact the DRDJSCS in question.

**End of the procedure**

The regional practice authorisation board may:

* Issue an immediate authorisation to practise
* Request additional information and postpone a decision until it has been received
* Refuse authorisation to practise. In this case, the decision must give reasons and include information about remedies and time-limits for appeal
* Request compensatory measures to supplement the applicant’s training (see below “Useful tip: Compensation measures”).

If the regional prefect does not reply within four months of receipt of the complete application, the application for authorisation is deemed to be rejected.

**Remedies**

An applicant may challenge a (formal or implied) decision to reject his/her application for authorisation to practise. To this end, he/she may, within two months of notification of the rejection decision, lodge one of the following:

* An application to the regional prefect to reconsider the decision
* An appeal to the Minister for Health
* An appeal to the administrative court with territorial jurisdiction.

**Cost**

Varies according to DRDJSCS.

**Useful tip: Compensation measures**

To obtain the authorisation to practise, the applicant may have to take an aptitude test or complete an adaptation period if his or her formal qualifications and professional experience prove to be substantially different from what is required to practise the profession in France. These are known as compensation measures.

**Competent authority**

If compensation measures are deemed necessary, the regional prefect responsible for issuing the authorisation to practise must notify the applicant that he/she has a period of two months in which to choose between the aptitude test and the adaptation period.

**Aptitude test**

The DRJSCS organising the aptitude tests must send the applicant notification by registered letter with acknowledgement of receipt at least one month before the start of the tests. This notification must include the date, time and place of the test.

The aptitude test may take the form of written or oral questions marked out of twenty and covering each of the subject areas not originally taught or not learnt through professional experience.

To pass the test, an applicant must have an average mark of at least 10 out of 20 with no mark below 8. The results of the test will be notified to the applicant by the regional prefect.

If successful, the applicant will be authorised to practise his/her profession by the regional prefect.

**Adaptation period**

This will be spent in a public or private healthcare establishment approved by the regional health agency (ARS).  The applicant will be placed under the educational supervision of a qualified professional having practised the profession for at least three years, who will write the assessment report.

The adaptation period, which may include supplementary theoretical training, is validated by the head of the host institution on the proposal of the qualified professional assessing the applicant.

The results of the adaptation period will be notified to the applicant by the regional prefect.

A decision on authorisation to practise is then taken after a further opinion from the committee referred to in Article L. 4311-4 of the French Public Health Code.

For more information, please refer to the Order of 24 March 2010 laying down the organisational arrangements for the aptitude test and adaptation period for practice of the profession of physiotherapist in France by citizens of EU Member States or States party to the EEA Agreement.

#### Applying for registration with the French Chartered Society of Physiotherapists

Persons holding a *diplôme d’État* or authorisation to practise must register with the French Chartered Society of Physiotherapists in order to appear on the Society’s list. This registration makes practice of physiotherapy on French territory lawful.

For more information, please refer to Articles L. 4321-10 and L. 4112-5 of the French Public Health Code.

**Competent authority**

The registration application should be made by registered letter with acknowledgement of receipt to the Chair of the Society’s *département* board in the *département* where the applicant wishes to practise.

**Procedure**

The Chair of the Society’s *département* board has three months from receipt of the complete application to take a decision on the application for registration on the list. The board will notify the applicant of its decision by registered letter. Grounds must be given for a refusal.

**Supporting documents**:

* A photocopy of a valid ID document
* For French nationals born abroad, a nationality certificate issued by the competent authority
* An ID photograph
* A photocopy of proof of address no more than three months old
* A copy of the *diplôme d’État* or authorisation to practise
* A photocopy of other diplomas
* A photocopy of the Adeli listing or health professionals card (*carte de professionnel de santé*)
* Formal declaration of awareness of the code of conduct and a sworn written undertaking to comply with it
* If the applicant provides a diploma issued by a foreign country whose validity is recognised in France: a copy of the formal qualifications upon which this recognition may be contingent
* For foreign nationals: a criminal record certificate or equivalent document dating back less than three months and issued by a competent authority of the home country. Citizens EU or EEA Member States that require proof of good character or repute to carry on the profession can provide a certificate no more than three months old from the competent authority of the home country attesting to the fact that the good character and repute requirements have been met
* A sworn declaration from the applicant certifying that no proceedings which may lead to sentencing or a sanction that may affect registration with the French Medical Association are ongoing against him/her
* A certificate of deregistration or disenrolment issued by the authority with which the applicant was previously registered or enrolled (or, failing this, a self-declaration stating that the applicant has never been registered or enrolled)
* Any evidence to establish the fact that the applicant has a knowledge of languages necessary for practice of the profession
* A curriculum vitae.

Other supporting documents may be required depending on how the profession is practised (employed, self-employed, both). For more details, applicants are advised to contact the relevant *département* board of the Society.

**Remedies**

The applicant may challenge within 30 days a decision to refuse his or her registration with the Society before the regional or interregional board with jurisdiction for the area of the *département* board that refused the registration.

**Cost**

The registration process is free of charge, but registration entails a mandatory membership subscription (*cotisation ordinale obligatoire*) whose amount is determined on a yearly basis by the Society’s national board.

For more information, please refer to Articles L. 4112-3, L. 4112-4, L. 4321-10, L. 4321-16, L. 4321-19 and R. 4112-1, and Article R. 4112-4 implemented by Article R. 4323-1 of the French Public Health Code.

#### If setting up an SEL or an SCP: applying to register the company with the French Chartered Society of Physiotherapists

If a professional practice (*société d’exercice libéral*, SEL) or a professional partnership (*société civile professionnelle*, SCP) is being set up, this company must also be registered with the Society at the location of its registered office.

**Competent authority**

The application for registration with the French Chartered Society of Physiotherapists must be sent by registered letter with acknowledgement of receipt to its *département* board for the location of the company’s registered office.

**Procedure**

The *département* board will take a decision on the registration application within three months of receipt of the full application. Grounds must be given for a refusal, which must be notified to the applicant by registered letter with acknowledgement of receipt.

**Supporting documents**

* For an SEL, the supporting documents required are listed in Article R. 4113-4 of the French Public Health Code
* For an SCP, the supporting documents required are listed in Article R. 4113-28 of the French Public Health Code.

**Remedies**

The applicant may challenge within 30 days a decision to refuse registration with the Society before the regional or interregional board with jurisdiction for the area of the *département* board that refused the registration.

For more information, please refer to Articles L. 4321-19, L. 4112-4 and R. 4112-4 of the French Public Health Code.

**Cost**

The registration process is free of charge, but registration entails a mandatory membership subscription (*cotisation ordinale obligatoire*) whose amount is determined on a yearly basis by the Society’s national board.

For more information, please refer to Articles R. 4113-4 and R. 4113-28, implemented by Article R. 4323-2 of the French Public Health Code.

#### Applying for registration of the *diplôme d’État* or authorisation to practise (Adeli No.)

Individuals with a *diplôme d’État* or, for non-French nationals, an authorisation to practise in France, must register their diploma or authorisation in the Adeli register.

Physiotherapists practising on a temporary and occasional basis are exempted from applying for registration of their diplomas.

**Competent authority**

The regional health agency (ARS) of the place of where the profession is practised is responsible for registering the diploma or authorisation to practice.

**Timeframe**

The registration application must be submitted in the month following the start of practice, whatever form the practice takes (employed, self-employed, both). The receipt issued by the ARS will include the Adeli number. The ARS will then send the applicant an application form for a health professionals card (*carte de professionnel de santé*).

**Supporting documents**

* For French diplomas: the original diploma
* For EU or EEA diplomas: the diploma, a French translation by a sworn translator, and the authorisation to practise in France
* A photocopy of a valid ID document
* CERFA Form 10906\*06
* Proof of registration with the French Chartered Society of Physiotherapists in the *département* where the profession is being practised.

Some ARSs may require other documents. For more details, applicants are advised to contact the ARS in question.

**Cost**

Free of charge.

For more information, please refer to Article L. 4321-10 of the French Public Health Code.

### c. European Professional Card (EPC)

The European Professional Card is an electronic procedure to have professional qualifications recognised in another EU Member State.

The EPC procedure can be used if the citizen wishes to practise as a physiotherapist in another EU country:

* either temporarily and occasionally,
* or permanently.

The EPC is valid:

* indefinitely for long-term establishment
* normally 18 months for temporary provision of services (or 12 months for professions affecting public health and safety)

To apply for an EPC the applicant must:

* create a user account with the European Commission Authentication Service (ECAS)
* complete his/her EPC profile (personal information, contact details, etc.)
* create an EPC application by uploading scanned copies of supporting documents.

**Cost**

For each EPC application both home country and host country authorities may charge fees, the amount of which will depend on the circumstances.

**Timeframe**

*For an EPC application for temporary and occasional practice*

Within one week the home country authority will acknowledge receipt of the EPC application, state whether any documents are missing and provide information about any fees. The host country authority then checks the application.

* If host country checks are not needed, the home country authority reviews the application and takes a final decision within three weeks.
* If host country checks are needed, the home country authority has one month to review the application and forward it to the host country. The host country then takes a final decision within three months.

*For an EPC application for permanent practice*

Within one week the home country authority will acknowledge receipt of the EPC application, state whether any documents are missing and provide information about any fees. The home country then has one month to review the application and forward it to the host country. The latter will take a final decision within three months.

If the host country authorities find that the applicant’s education, training or professional experience does not meet the standards required in the host country, they may ask the applicant to take an aptitude test or complete an adaptation period.

*Outcomes of EPC application*

* If the EPC application is granted, it is then possible to generate an EPC certificate online.
* If the host country authorities fail to take a decision within the time allowed, the applicant’s qualifications are implicitly recognised and an EPC is issued. The applicant can then generate an EPC certificate from his or her online account.
* If the EPC application is rejected, reasons must be given and information provided on how to challenge this decision.

### d. Remedies

#### French support centre

The ENIC-NARIC is the French centre for information on the academic and professional recognition of qualifications.

#### SOLVIT

SOLVIT is a service provided nationally by each Member State of the European Union or State party to the EEA Agreement. It aims to find solutions to problems between an EU citizen and the government of another of these countries. SOLVIT is heavily involved in the recognition of professional qualifications.

**Conditions**

An applicant may use SOLVIT’s services only if he/she is able to prove:

* that the public authorities of an EU Member State have breached their EU rights as a citizen or business of another EU Member State
* that they have not yet taken the case to court (administrative appeals do not count).

**Procedure**

The citizen must complete an [online complaints form]( http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=en&origin=solvit-web).

Once the form has been submitted, SOLVIT contacts him/her within one week to ask for further information, if necessary, and to check whether the problem falls within its remit.

**Supporting documents**

To refer a problem to SOLVIT, the citizen must provide:

* his/her full contact details
* a detailed description of the problem
* all relevant evidence(e.g. correspondence and decisions received from the administrative authority in question)

**Timeframe**

SOLVIT aims to find solutions within 10 weeks starting on the day the case is taken on by the SOLVIT centre in the country where the problem has occurred.

**Cost**

Free of charge.

**Outcome of procedure**

By the end of ten weeks, SOLVIT will put forward a solution:

* If the solution resolves the problem concerning the application of EU law, it is accepted and the case is closed
* If the problem is not settled, the case is closed as unresolved and referred to the European Commission

**Further information**

SOLVIT in France: Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).