﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP058" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Dental surgeon (dental practitioner)" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="dental-surgeon-dental-practitioner" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/dental-surgeon-dental-practitioner.html" -->
<!-- var(last-update)="2020-04-15 17:21:18" -->
<!-- var(url-name)="dental-surgeon-dental-practitioner" -->
<!-- var(translation)="Auto" -->


Dental surgeon (dental practitioner)
====================================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:18<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The dental surgeon is a health professional practicing odontology, that is, the study of the dental organ, maxillas and adjoining tissues. Thus, it ensures the prevention, diagnosis and treatment of abnormalities related to these parts of the body.

He may also have to perform operations requiring anesthesia, such as extracting wisdom teeth.

More often than not, he will take care of the scaling of his patients' teeth, treat cavities, prepare the installation of dentures by making mouldings, and share his oral hygiene advice.

*For further information*: Article L. 4141-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

Under Article L. 4111-1 of the Public Health Code, in order to legally practise as a dental surgeon in France, those concerned must meet cumulatively the following three conditions:

- hold the French state diploma of dental surgeon, the French state diploma of doctor of dental surgery or a diploma, certificate or other title mentioned in Article L. 4141-3 of the Code of Public Health (see below "Good to know: automatic diploma recognition");
- be of French nationality, Andorran citizenship or national of a Member State of the European Union (EU) or party to the agreement on the European Economic Area (EEA) or Morocco, subject to the application of the rules derived from the Health Code public or international commitments;
- with exceptions, be listed on the board of the College of Dental Surgeons (see infra "5°. b. Request inclusion on the order of dental surgeons" list").

*For further information*: Articles L. 4111-1 and L. 4141-3 of the Public Health Code.

###### Good to know

Automatic diploma recognition

Under Article L. 4141-3 of the Public Health Code, EU or EEA nationals may practise as a dental surgeon if they hold one of the following titles:

- Dental practitioner training certificates issued by one of these states in accordance with Community obligations and listed by the Order of 13 July 2009 setting out the list and conditions for the recognition of training as specialist dental practitioners issued by the Member States of the European Community or parties to the EEA agreement covered by Article L. 4141-3 of the Public Health Code;
- dental practitioner training certificates issued by an EU or EEA state, in accordance with EU obligations, not on the list of the 13 July 2009 decree if accompanied by a certificate from that state certifying that they sanction training in accordance with these obligations and that they are assimilated by him to the diplomas, certificates and titles on that list;
- dental practitioner training certificates issued by an EU or EEA state sanctioning training as a dental practitioner begun in that state prior to the dates in the order referred to in the order of 13 July 2009 and non-compliant with Community obligations, if accompanied by a certificate from one of these states certifying that the holder of the training titles has devoted himself, in that state, effectively and lawfully to the activities of a dental practitioner or, if necessary, a specialist dental practitioner for at least three consecutive years in the five years prior to the issuance of the certificate;
- dental practitioner training certificates issued by the former Soviet Union or the former Yugoslavia or which sanction training begun before the date of independence of Estonia, Latvia, Lithuania or Slovenia, if they are accompanied by a certificate from the competent authorities of Estonia, Latvia or Lithuania for training documents issued by the former Soviet Union, Slovenia for training documents issued by the former Yugoslavia, certifying that they have the same legal validity as the training certificates issued by that state. This certificate is accompanied by a certificate issued by the same authorities indicating that the holder has exercised in that state, effectively and lawfully, the profession of practitioner of the art of dentistry or practitioner of the dental art specialist during the at least three consecutive years in the five years prior to the issuance of the certificate;
- Dental practitioner training certificates issued by a State, Member or Party, sanctioning training as a dental practitioner begun in that state prior to the dates in the order referred to in the order of 13 July 2009 and not in accordance with Community obligations but allowing to practice legally the profession of practitioner of the dentistry in the state that issued them, if the practitioner of the art of dentistry justifies having performed in France during the five years three consecutive full-time years of hospital functions, if any in the specialty corresponding to training titles, as associate attaché, associate practitioner, associate assistant or functions academics as associate clinic head of universities or associate assistant of universities, provided they have been in charge of hospital functions at the same time;
- a doctor's training certificate issued in Italy, Spain, Austria, the Czech Republic, Slovakia and Romania sanctioning training that has begun no later than the dates set by decree of the Ministers responsible for higher education and health, if accompanied by a certificate from the competent authorities of that state certifying that it is entitled in that state to the practice of the profession of practitioner of the art of dentistry and that its holder has devoted himself, in that state, effectively and lawfully , activities of a dental practitioner for at least three consecutive years in the five years prior to the issuance of the certificate;
- Dental practitioner training certificates issued by a Member or Party State, sanctioning training that began before 18 January 2016;
- Spanish-issued doctor training certificates sanctioning medical training begun in that state between 1 January 1986 and 31 December 1997, if accompanied by a certificate issued by the competent authorities of that state indicating that the holder successfully completed at least three years of study in accordance with the community basic training obligations of the profession of dental practitioner, and that he exercised, in an effective, lawful and principal capacity, the profession of dental practitioner for at least three consecutive years in the five years prior to the issuance of the certificate and that he is authorized to practice or exercise, effectively, lawful and principally, this profession in the same conditions as the holders of the training titles listed in the Order of July 13, 2009.

#### Training

Pharmacy studies consist of three cycles with a total duration of between six and nine years depending on the chosen range.

**General education diploma in odontological sciences**

The first cycle is sanctioned by the general training diploma in odontological sciences. It consists of six semesters and corresponds to the license level. The first two semesters correspond to the[first year common to health studies](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021276755&dateTexte=20180119).

The aim of the training is to:

- the acquisition of a foundation of scientific knowledge essential to the subsequent mastery of the knowledge and know-how necessary for the practice of the profession of dental surgeon. This scientific basis encompasses biology, certain aspects of the exact sciences and several disciplines of the humanities and social sciences;
- learning in the fields of medical semeiology, pharmacology and odontological disciplines;
- learning teamwork and communication techniques necessary for professional practice.

It also allows students to learn how to communicate, diagnose, design a therapeutic proposal, understand a coordinated approach to care and ensure emergency actions.

The training includes theoretical, methodological, applied and practical teachings as well as the completion of a four-week full-time introductory course.

*For further information*: :[Stopped](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023850748&dateTexte=20180119) March 22, 2011 on the curriculum for the general training degree in odontological sciences.

**In-depth training degree in odontological sciences**

The second cycle of dental surgeon studies is sanctioned by the diploma of in-depth training in odontological sciences and includes four semesters corresponding to the master level.

Its objective is to:

- acquiring the scientific, medical and odontological knowledge that complements and deepens those acquired in the previous cycle and necessary to acquire the skills for all prevention and diagnostic activities and treatment of congenital or acquired diseases, real or assumed, of the mouth, teeth, maxillas and adjoining tissues;
- acquiring practical knowledge and clinical skills through internships and practical and clinical training;
- training in the scientific process
- Learning clinical reasoning
- learning to work as a multi-professional team, especially with other odontologists;
- Acquiring communication techniques essential to professional practice;
- awareness of ongoing professional development including the evaluation of professional practices and the continuous deepening of knowledge.

In addition to theoretical and practical teachings, the training includes the completion of hospital internships.

The second cycle is validated by the success of the knowledge of the teachings taught during the training, as well as the issuance of a certificate of clinical and therapeutic synthesis.

*For further information*: Articles 4 to 15 of the[Stopped](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027343802&dateTexte=20180119) April 8, 2013 relating to the education regime for the State Doctor of Dental Surgery.

**State Doctor's Diploma in Dental Surgery**

The third cycle is sanctioned by the issuance of the state diploma of doctor of dental surgery. It includes:

- A short cycle of two semesters of training;
- a six- to eight-semester cycle of training for students who have been awarded the odontology internship;
- the defence of a thesis.

The two-semester third cycle is devoted to the overall patient approach and preparation for the independent practice of the profession.

It is accompanied by a 250-hour professional life initiation course with a dental surgeon.

The student will have to support a thesis before a jury from the second semester of the third cycle and until its validation. The DOCTOR's ED in dental surgery will be given to the student who validated the postgraduate teachings and validated his thesis.

*For further information*: Articles 16 and following of the April 8, 2013 order on the education regime for the State Doctor's Diploma in Dental Surgery.

#### Costs associated with qualification

The training leading to obtaining the DOCTOR's DE in dental surgery is paid for. Its cost varies depending on the universities that provide the teachings. For more information, it is advisable to get closer to the university in question.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

The professional who is a member of an EU or EEA state who is established and legally practises in one of these states may carry out the same activity in France on a temporary and occasional basis, without being included in the Order of dental surgeons.

To do this, the professional must make a prior declaration, as well as a declaration justifying that he has the necessary language skills to practice in France (see infra "5°. a. Make a prior declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)").

**What to know**

Registration on the College of Dental Surgeons is not required for the free-service professional (LPS). It is therefore not required to pay ordinal dues. The dental surgeon is simply registered on a specific list maintained by the National Council of the Order.

The pre-declaration must be accompanied by a statement regarding the language skills necessary to carry out the service. In this case, the control of language proficiency must be proportionate to the activity to be carried out and carried out once the professional qualification has been recognised.

When training titles do not receive automatic recognition (see supra "2.0). a. National legislation"), the provider's professional qualifications are checked before the first service is provided. In the event of substantial differences between the qualifications of the person concerned and the training required in France which would be likely to harm public health, the claimant is subjected to an aptitude test.

The dental surgeon in the situation of LPS is obliged to respect the professional rules applicable in France, including all ethical rules (See infra "3°." Conditions of honorability, ethical rules, ethics"). It is subject to the disciplinary jurisdiction of the College of Dental Surgeons.

**Please note**

The performance is performed under the French professional title of dental surgeon. However, where training qualifications are not recognised and qualifications have not been verified, the performance is carried out under the professional title of the State of Establishment, in order to avoid confusion With the French professional title.

*For further information*: Article L. 4112-7 of the Public Health Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

**The automatic diploma recognition scheme**

Article L. 4141-3 of the Public Health Code creates a system of automatic recognition in France of certain diplomas or titles, if any accompanied by certificates, obtained in an EU or EEA state (see "supra"2." a. National Legislation").

It is the responsibility of the National Council of the College of Dental Surgeons to verify the regularity of diplomas, titles, certificates and certificates, to grant automatic recognition and then to rule on the application for inclusion on the Order's roster.

*For further information*: Article L. 4151-5 of the Public Health Code.

**The regime of individual authorisation to practise**

If the EU or EEA national does not qualify for the automatic recognition of his or her credentials, he or she falls under an authorisation scheme (see below "5o). b. If necessary, seek an individual authorization to exercise").

Individuals who do not receive automatic recognition but who hold a training degree to legally practise as a dental surgeon may be individually authorised to practice in France by the Minister for Health, after advice from a commission made up of professionals.

If the examination of the professional qualifications attested by the training credentials and the professional experience shows substantial differences with the qualifications required for access to the profession and its exercise in France, the person must submit to a compensation measure.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority can either:

- Offer the applicant a choice between an adjustment course or an aptitude test;
- require an adjustment course and/or aptitude test.

*For further information*: Articles L. 4141-3-1, R. 4111-14 and following of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Compliance with the Dental Surgeons Code of Ethics

The provisions of the Code of Ethics are imposed on all dental surgeons practising in France, whether they are on the Order's board or exempt from this obligation (see supra "5." b. Request inclusion on the order of dental surgeons" list").

**What to know**

All provisions of the Code of Ethics are codified in sections R. 4127-201 to R. 4127-284 of the Public Health Code.

As such, dental surgeons must respect the principles of dignity, non-discrimination, professional secrecy or independence.

### b. Obligation for continuous professional development

Dental surgeons must participate annually in an ongoing professional development program. This program aims to maintain and update their knowledge and skills as well as to improve their professional practices.

As such, the health professional (salary or liberal) must justify his commitment to professional development. The program is in the form of training (present, mixed or non-present) in analysis, evaluation and improvement of practices and risk management. All training is recorded in a personal document containing training certificates.

*For further information*: Article R. 4127-214 of the Public Health Code.

4°. Insurance
---------------------------------

### a. Obligation to take out professional liability insurance

As a health professional, a liberal dental surgeon must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

*For further information*: Article L. 1142-2 of the Public Health Code.

### b. Self-employed retirement fund membership obligation for dental surgeons and midwives (CARCDSF)

Any dental surgeon registered on the board of the College of Dental Surgeons and practising in the liberal form (even part-time and even if he is also employed) has an obligation to join the CARCDSF.

The individual must report to carCDSF within one month of the start of his Liberal activity.

*For further information*: Article R. 643-1 of the Social Security Code; the site of the[CARCDSF](http://www.carcdsf.fr/).

### c. Health Insurance Reporting Obligation

Once on the Order's roster, the dental surgeon practising in liberal form must declare his activity with the Primary Health Insurance Fund (CPAM).

**Terms**

Registration with the CPAM can be made online on the official website of Medicare.

**Supporting documents**

The registrant must provide a complete file including:

- Copying a valid piece of identification
- The certificate of registration on the Order's board;
- a professional bank identity statement (RIB)
- if necessary, the notification of the radiological installation.

For more information, please refer to the section on the installation of dental surgeons on the Health Insurance website.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU or EEA nationals engaged in temporary and occasional activity (LPS)

Any EU or EEA national who is established and legally practises the activities of dental surgeon in one of these states may practice in France on a temporary or occasional basis if he makes the prior declaration (see supra "2." b. EU and EEA nationals: for a temporary and casual exercise (Freedom to provide services)").

The advance declaration must be renewed every year.

**Please note**

Any change in the applicant's situation must be notified under the same conditions.

#### Competent authority

The declaration must be addressed to the National Council of the College of Dental Surgeons prior to the first service delivery.

#### Terms of reporting and receipt

The declaration can be sent by mail or directly made online on the official website of the College of Dental Surgeons.

When the National Council of the Order receives the declaration and all the necessary supporting documents, it sends the claimant a receipt specifying its registration number as well as the discipline exercised.

**Please note**

The service provider informs the relevant national health insurance agency of its provision of services by sending a copy of the receipt or by any other means.

#### Time

Within one month of receiving the declaration, the National Council of the Order informs the applicant:

- Whether or not he can start delivering services;
- that he must prove that he has acquired the missing knowledge and skills by submitting to an aptitude test when the verification of professional qualifications reveals a substantial difference with the training required in France. If he meets this check, he is informed within one month that he can begin the provision of services;
- reasons for the delay in reviewing his file when the file review highlights a difficulty requiring further information. He then has one month to obtain the requested additional information. In this case, before the end of the second month from the receipt of this information, the National Council informs the claimant, after reviewing his file:- whether or not it can begin service delivery,
  - that he must demonstrate that he has acquired the missing knowledge and skills, including by submitting to an aptitude test, when the verification of the claimant's professional qualifications shows a substantial difference with training required in France.

In the latter case, if he meets this control, he is informed within one month that he can begin the provision of services. Otherwise, he is informed that he cannot begin the delivery of services. In the absence of a response from the National Council of the Order within these timeframes, service delivery may begin.

#### Supporting documents

The pre-declaration must be accompanied by a statement regarding the language skills required to carry out the service and the following supporting documents:

- The advance service delivery form
- Copying a valid piece of identification or a document attesting to the applicant's nationality;
- Copying the training document or titles, accompanied, if necessary, by a translation by a certified translator;
- a certificate from the competent authority of the EU State of Settlement or the EEA certifying that the person is legally established in that state and that he is not prohibited from practising, accompanied, if necessary, by a french translation established by a certified translator.

**Please note**

The control of language proficiency must be proportionate to the activity to be carried out and carried out once the professional qualification has been recognised.

#### Cost

Free.

*For further information*: Articles L. 4112-7, R. 4112-9 and the following of the Public Health Code; January 20, 2010 order on the prior declaration of the provision of services for the practice of physician, dental surgeon and midwife.

### b. Formalities for EU or EEA nationals for a permanent exercise (LE)

#### If necessary, seek individual authorisation to exercise

If the national is not under the automatic recognition scheme, he must apply for a licence to practise.

##### Competent authority

The request is addressed in two copies, by letter recommended with request for notice of receipt, to the unit responsible for the commissions of exercise authorization (CAE) of the National Management Centre (NMC).

##### Supporting documents

The application file must contain all of the following supporting documents:

- The application form for authorisation to practice the profession;
- A photocopy of a valid ID
- A copy of the training title allowing the practice of the profession in the state of obtaining as well as, if necessary, a copy of the specialist training title;
- If necessary, a copy of the additional diplomas;
- any useful evidence justifying continuous training, experience and skills acquired during the professional exercise in an EU or EEA state, or in a third state (certificates of functions, activity report, operational assessment, etc. ) ;
- in the context of functions performed in a state other than France, a statement by the competent authority of that state dates back less than one year attesting to the absence of sanctions against the applicant.

Depending on the applicant's situation, additional supporting documentation is required. For more information, please visit the NMC's official website.

**What to know**

Supporting documents must be written in French or translated by a certified translator.

##### Time

The NMC acknowledges receipt of the request within one month of receipt.

The silence kept for a certain period of time from the receipt of the full file is worth the decision to dismiss the application. This delay is increased to:

- four months for applications from EU or EEA nationals with a degree from one of these states;
- six months for applications from third-party nationals with a diploma from an EU or EEA state;
- one year for other applications.

This period may be extended by two months, by decision of the ministerial authority notified no later than one month before the expiry of the latter, in the event of a serious difficulty in assessing the candidate's professional experience.

*For further information*: decree of 25 February 2010 setting out the composition of the file to be provided to the competent authorisation commissions for the examination of applications submitted for the exercise in France of the professions of doctor, dental surgeon, midwife and Pharmacist.

**Good to know: compensation measures**

Where there are substantial differences between the training and work experience of the national and those required to practise in France, the NMC may decide either:

- Suggest that the applicant choose between an adjustment course or an aptitude test;
- to impose an adjustment course and/or an aptitude test.

The purpose of the aptitude test is to verify, through written or oral tests or practical exercises, the applicant's fitness to practise as a dental surgeon. It deals with subjects that are not covered by the applicant's training or training credentials or professional experience.

The purpose of the adaptation course is to enable interested parties to acquire the skills necessary to practice the profession of dental surgeon. It is performed under the responsibility of a dental surgeon and can be accompanied by optional additional theoretical training. The duration of the internship does not exceed three years. It can be done part-time.

*For further information*: Article R. 4111-14 and Articles R. 4111-17 to R. 4111-20 of the Public Health Code.

##### Request registration on the order of dental surgeons' list

Registration on the Order's board is mandatory to legally practice the activity of dental surgeon in France.

The registration does not apply:

- EU or EEA nationals who are established and who are legally a dental surgeon in a Member State or party, when they perform acts of their profession on a temporary and occasional basis in France (see supra "2°. b. EU and EEA nationals: for temporary and occasional exercise);
- dental surgeons belonging to the active executives of the Military Health Service;
- dental surgeons who, having the status of a public servant or a holding agent of a local community, are not called upon, in the course of their duties, to perform dental surgery.

**Please note**

Registration on the Order's board allows for the automatic and free issuance of the Health Professional Card (CPS). The CPS is an electronic business identity card. It is protected by a confidential code and contains, among other things, the identification of a dental surgeon (identity, profession, specialty). For more information, it is recommended to refer to the government website of the French Digital Health Agency.

##### Competent authority

The application for registration is addressed to the Chairman of the Board of the Order of Dental Surgeons of the Department in which the person wished to establish his professional residence.

The application can be submitted directly to the departmental council of the Order concerned or sent to it by registered mail with request for notice of receipt.

**What to know**

In the event of a transfer of his professional residence outside the department, the practitioner is required to request his removal from the order of the department where he was practising and his registration on the order of his new professional residence.

##### Procedure

Upon receipt of the application, the county council appoints a rapporteur who conducts the application and makes a written report. The board verifies the candidate's titles and requests disclosure of bulletin 2 of the applicant's criminal record. In particular, it verifies that the candidate:

- fulfils the necessary conditions of morality and independence;
- meets the necessary competency requirements;
- does not present a disability or a medical condition incompatible with the practice of the profession.

In case of serious doubt about the applicant's professional competence or the existence of a disability or pathological condition incompatible with the practice of the profession, the county council refers the matter to the regional or inter-regional council expertise. If, in the opinion of the expert report, there is a professional inadequacy that makes the practice of the profession dangerous, the departmental council refuses registration and specifies the training obligations of the practitioner.

No decision to refuse registration can be made without the person being invited at least a fortnight in advance by a recommended letter requesting notice of receipt to appear before the Board to explain.

The decision of the College Council is notified within a week to the individual, the National Council of the Order of Dental Surgeons and the Director General of the Regional Health Agency (ARS). The notification is by recommended letter with request for notice of receipt.

The notification mentions remedies against the decision. The decision to refuse must be justified.

##### Time

The Chair acknowledges receipt of the full file within one month of its registration.

The College's departmental council must decide on the application for registration within three months of receipt of the full application file. If a response is not answered within this time frame, the application for registration is deemed rejected.

This period is increased to six months for nationals of third countries when an investigation is to be carried out outside metropolitan France. The person concerned is then notified.

It may also be extended for a period of no more than two months by the departmental council when an expert opinion has been ordered.

##### Supporting documents

The applicant must submit a complete application file including:

- two copies of the standardized questionnaire completed, dated and signed, accompanied by a photo ID. The questionnaire is available in the College's departmental councils;
- A photocopy of a valid ID or, if necessary, a certificate of nationality issued by a competent authority;
- If applicable, a photocopy of a valid EU citizen's family residence card, the valid long-term resident-EC card or the resident card with valid refugee status;
- If so, a photocopy of a valid nationality certificate;
- a copy accompanied, if necessary, of a translation by a certified translator, training courses to which are attached:- certificates (see above "2." (a.) (a) where the applicant is an EU or EEA national,
  - copy of the individual exercise authorization (see supra "2." c. EU and EEA nationals: for a permanent exercise") where the applicant has such a permit,
  - copying the titles to which this recognition may be subordinated when the applicant presents a diploma issued in a foreign state whose validity is recognized on French territory;
- for nationals of a third state, an extract from a criminal record or an equivalent document less than three months old issued by a competent authority of the State of origin. This part may be replaced, for EU or EEA nationals who require proof of morality or honourability for access to the medical activity, by a certificate less than three months old issued by the competent The State of Origin certifying that these moral or honourability conditions are met;
- a statement on the applicant's honour certifying that no proceedings that could give rise to a conviction or sanction that could affect the listing on the board are against him;
- a certificate of registration or registration issued by the authority with which the applicant was previously registered or registered or, failing that, a declaration of honour from the applicant certifying that he or she was never registered or registered or, failing that, a certificate of registration or registration in an EU or EEA state;
- all the evidence that the applicant has the language skills necessary to practice the profession;
- a resume.

##### Remedies

The applicant or the National Council of the College of Dental Surgeons may challenge the decision to register or refuse registration within 30 days of notification of the decision or the implied decision to reject it. The appeal is brought before the territorially competent regional council.

The regional council must decide within two months of receiving the application. In the absence of a decision within this period, the appeal is deemed dismissed.

The decision of the regional council is also subject to appeal, within 30 days, to the National Council of the Order of Dental Surgeons. The decision itself can be appealed to the Council of State.

##### Cost

Registration on the College's board is free, but it creates an obligation to pay the mandatory ordinal dues, the amount of which is set annually and which must be paid in the first quarter of the current calendar year. Payment can be made online on the official website of the National Council of the Order of Dental Surgeons. As an indication, the amount of this contribution was 422 euros in 2017.

*For further information*: Articles L. 4112-1 to L. 4112-6, and Articles R. 4112-1 to R. 4112-20 of the Public Health Code.

### c. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

