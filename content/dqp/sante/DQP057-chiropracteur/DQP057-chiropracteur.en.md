﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP057" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Chiropractor" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="chiropractor" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/chiropractor.html" -->
<!-- var(last-update)="2020-04-15 17:21:17" -->
<!-- var(url-name)="chiropractor" -->
<!-- var(translation)="Auto" -->


Chiropractor
============

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:17<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The chiropractor is a health professional practicing manual, instrumental or assisted manipulation and mobilization, devoting himself to the nervous and musculoskeletal systems including the spine.

It performs an analysis by identifying the blocking points, detects the movement of bone structures and performs manipulations.

*For further information*: Article 1 of Decree No. 2011-32 of January 7, 2011 on the acts and conditions of chiropractic.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The practice of chiropractor is reserved:

- holders of the chiropractic diploma issued by a training institution accredited by the Minister responsible for health, after advice from the National Accreditation Commission;
- nationals of a Member State of the European Union (EU) or party to the European Economic Area (EEA) who hold an authorisation to practise chiropractic or to use the title of chiropractor issued by the competent administrative authority of the This state;
- to physicians, midwives, massage therapists and registered nurses, holders of a university or inter-university degree sanctioning training in this field within a training and research unit of medical practice issued by a medical university and recognized by the National Council of the College of Physicians.

*For further information*: Article 75 of Law 2002-303 of 4 March 2002 on the rights of the sick and the quality of the health system; Article 4 of Decree No. 2011-32 of January 7, 2011 relating to the acts and conditions of practice of chiropractic.

#### Training

Entry to a chiropractic training institution is open to holders of a French baccalaureate or a title issued by another state and recognized as equivalent.

Theoretical training of at least 2,120 hours and practical clinical training of at least 1,400 hours are required to take the exams leading to the issuance of the chiropractor diploma.

In France, the[Franco-European Institute of Chiropractic](https://www.ifec.net/) (IFEC) provides training and awards the chiropractic diploma.

*For further information*: Articles 1 to 7 of the March 24, 2014 order on the training of chiropractors and the accreditation of chiropractic training institutions.

#### Costs associated with qualification

The cost of this training pays off. For more information, it is advisable to get closer to the IFEC.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A national of an EU or EEA state legally practising chiropractor activity in one of these states may use his or her professional title in France on a temporary and casual basis.

He will have to apply for it, before his first performance, by declaration addressed to the director general of the regional health agency of Ile-de-France (see infra "5°. a. Make a prior declaration of activity for the EU or EEA national for a temporary and casual exercise (LPS)).

Where neither the activity nor the training leading to this activity is regulated in the state in which it is legally established, the professional will have to justify having carried it out in one or more Member States for at least two years in the ten years before the performance.

*For further information*: Articles 11 and 12 of Decree No. 2011-32 of January 7, 2011 on the acts and conditions of chiropractic.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

Any national of an EU or EEA state may settle in France and carry out the chiropractor activity on a permanent basis if he or she holds:

- a training certificate issued by an EU or EEA state that regulates access to the activity or its exercise;
- a training certificate issued by a third state but recognised by the competent authority of an EU or EEA state;
- any evidence to justify that the national has been active for two years in the last ten years in an EU or EEA state that does not regulate access or the exercise of this activity on its territory.

Once the national fulfils one of these conditions, he can apply for an individual authorisation to exercise from the Director General of the regional health agency of Ile-de-France (see infra "5°. b. Request an individual exercise permit for the EU or EEA national for a permanent exercise (LE)).

If the examination of the professional qualifications attested by the training credentials and the professional experience shows substantial differences with the qualifications required for access to the profession and its exercise in France, the person must submit to a compensation measure (see infra "5°. b. Good to know: compensation measures").

*For further information*: Articles 6 and 7 of Decree No. 2011-32 of January 7, 2011 on the acts and conditions of chiropractic.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

### a. Ethical rules and incompatibilities

Although uncodified,[ethical rules](http://chiropraxie.com/wp-content/uploads/2013/07/20130406-AFC-RI.pdf) apply to the chiropractor, including:

- Ensure that the professional confidentiality of its patients is respected;
- Not using its elective mandate or administrative function to increase its clientele;
- examine, counsel or treat each patient in the same way, regardless of their origin, manners or family situation;
- performing acts with the patient's free and informed consent.

The chiropractor cannot perform certain acts including:

- gyneco-obstetric manipulation;
- pelvic touches.

*For further information*: Article 3 of Decree No. 2011-32 of January 7, 2011 on the acts and conditions of chiropractic.

### b. Obligation to undergo continuing vocational training

Chiropractors must participate in a multi-year program of continuous professional development. The program focuses on assessing professional practices, improving skills, improving the quality and safety of care, maintaining and updating knowledge and skills. All actions carried out by chiropractors under their obligation to develop continuously are traced in a specific document attesting to compliance with this obligation.

*For further information*: Article 75 of Law 2002-303 of 4 March 2002 on the rights of the sick and the quality of the health system; Articles L. 4021-1 and subsequent articles R. 4021-4 and the following of the Public Health Code.

4°. Insurance
---------------------------------

As a health professional, a chiropractor practising on a liberal basis must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

**What to know**

The fact of doing this activity without being covered by professional liability insurance is punishable by a fine of 45,000 euros.

*For further information*: Articles 1 and 2 of Law No. 2014-201 of 24 February 2014 covering various provisions for adaptation to EU health law.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration for the EU or EEA national for a temporary and casual exercise (LPS)

#### Competent authority

The Director General of the regional health agency of Ile-de-France is responsible for issuing the prior declaration of activity to a national who wishes to practice his profession in France on a temporary and casual basis.

#### Supporting documents

The request for a pre-report of activity is accompanied by a complete file containing the following supporting documents:

- The[Form](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=75C65DA9206264782345D43D208F57AC.tplgfr39s_3?idArticle=LEGIARTI000023449766&cidTexte=LEGITEXT000023449667&dateTexte=20180208) full, dated and signed statement;
- A photocopy of the valid ID
- a certificate of professional liability insurance;
- A photocopy of the training titles
- a certificate from the competent authority of the EU State or the EEA certifying that the national is not subject to any ban on practising.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

#### Procedure

The Director General acknowledges receipt of the file and has one month to decide and inform the national:

- He can start the performance.
- that he cannot start it;
- that he will be subjected to an aptitude test in case of a substantial difference between the training required in France and his professional qualifications;
- that the file is being reviewed and that more information is needed.

**Please note**

The declaration is renewable every year and in case of a change in the national's situation.

*For further information*: Appendix 3 of the decree of 7 January 2011 relating to the composition of the file and the modalities of the organisation of the aptitude test and the adaptation course provided for chiropractors by Decree No. 2011-32 of 7 January 2011 relating to acts and acts chiropractic exercise conditions

### b. Request an individual exercise permit for the EU or EEA national for a permanent exercise (LE)

#### Competent authority

The Director General of the regional health agency of Ile-de-France is responsible for issuing the individual authorisation to practice to a national who wishes to settle in France to carry out the activity of chiropractor.

#### Supporting documents

To obtain this authorization, the national must send the competent authority a two-copy file per recommended letter with acknowledgement with the following supporting documents:

- The[Form](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=8E0C3323BA88B61EACBF39F205F5CEAF.tplgfr39s_3?idArticle=LEGIARTI000023449673&cidTexte=LEGITEXT000023449667&dateTexte=20180208) request for individual permission, completed, dated and signed;
- A photocopy of the valid ID
- A copy of the training title for the practice of the profession;
- If necessary, any additional diploma;
- any documentation justifying the follow-up of continuing education, experience and skills acquired in that state;
- a certificate from the competent authority of the state justifying that the national does not have a sanction against him;
- A copy of the certificates justifying the level of training and, year by year, the details and hourly volume of the courses followed, as well as the content and duration of the validated internships;
- If applicable, any document justifying the practice of the profession in an EU or EEA state for two years in the last ten years when that state does not regulate access or the practice of the profession of chiropractor;
- if necessary, the recognition of a training certificate issued by an EU or EEA state when the title has been acquired in a third state.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

#### Procedure

The Director General will acknowledge receipt of the file within one month. If compensation measures are deemed necessary, the director will give the national two months to choose between an aptitude test or an adjustment course.

The silence kept by the competent authority within four months is worth rejecting the application for leave.

**Good to know: compensation measures**

The aptitude test takes the form of a written or oral exam scored out of 20. Its validation is pronounced when the national has obtained an average score of 10 out of 20 or higher, without a score of less than 8 out of 20. If successful in the test, the national will be allowed to use the chiropractor title.

The internship is done in a public or private health facility, or at a professional level, and must not last more than three years. It also includes theoretical training that will be validated by the internship manager. The decision to allow the national to use the title of chiropractor will be submitted to the opinion of a commission composed of the Director General of the regional health agency of Ile-de-France and four chiropractors known for their skills and Experience.

*For further information*: Articles 6 to 10 of Decree No. 2011-32 of January 7, 2011 on the acts and conditions of chiropractic; decree of 7 January 2011 relating to the composition of the file and the modalities of the organisation of the aptitude test and the adaptation course planned for chiropractors by Decree No. 2011-32 of 7 January 2011 relating to the acts and conditions of practice chiropractic.

### c. Registration with the Adeli directory

A national wishing to practise as a chiropractor in France is required to register his authorisation to practice on the Adeli directory ("Automation of The Lists").

#### Competent authority

Registration in the Adeli directory is done with the regional health agency (ARS) of the place of practice.

#### Time

The application for registration is submitted within one month of taking office of the national, regardless of the mode of practice (liberal, salaried or mixed).

#### Supporting documents

In support of the application for registration, the chiropractor must provide a file containing:

- the original diploma or title attesting to the training of epithist issued by the EU state or the EEA (translated into French by a certified translator, if applicable);
- ID
- Form Cerfa 13777Completed, dated and signed.

#### Outcome of the procedure

The national's Adeli number will be directly mentioned on the receipt of the file, issued by the ARS.

#### Cost

Free.

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the public administration of one EU state has not respected its rights under EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700 Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html)).

