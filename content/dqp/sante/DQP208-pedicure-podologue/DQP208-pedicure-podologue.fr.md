﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP208" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="fr" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Santé" -->
<!-- var(title)="Pédicure-podologue" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="sante" -->
<!-- var(title-short)="pedicure-podologue" -->
<!-- var(url)="https://www.guichet-qualifications.fr/fr/dqp/sante/pedicure-podologue.html" -->
<!-- var(last-update)="2020-04-15 17:21:56" -->
<!-- var(url-name)="pedicure-podologue" -->
<!-- var(translation)="None" -->

# Pédicure-podologue

Dernière mise à jour : <!-- begin-var(last-update) -->2020-04-15 17:21:56<!-- end-var -->

## 1°. Définition de l'activité

Le pédicure-podologue est un professionnel de la santé spécialisé dans le traitement des affections épidermiques et unguéales (relatif à l'ongle) du pied.

Il est donc compétent pour pratiquer les soins d'hygiène (activité de pédicurie) mais également pour confectionner et adapter des semelles orthopédiques pour soulager ces affections (activité de podologie).

*Pour aller plus loin* : articles L. 4322-1 et R. 4322-1 du Code de la santé publique.

## 2°. Qualifications professionnelles

### a. Exigences nationales

#### Législation nationale

L'exercice de la profession de pédicure-podologue est réservé aux titulaires du diplôme d’État de pédicure-podologue.

*Pour aller plus loin* : article L. 4322-2 du Code de la santé publique.

#### Formation

Le diplôme d’État de pédicure-podologue est un titre de formation de niveau licence qui se prépare en trois ans après l'obtention du baccalauréat.

**À noter**

L'admission à certains des établissements dispensant la formation peut se faire par concours externe.

Une formation théorique et pratique de 2 028 heures et une formation clinique de 1 170 heures doivent permettre à l'étudiant de développer ses connaissances et ses compétences pour notamment :

- analyser et évaluer une situation et élaborer un diagnostic dans le domaine de la pédicurie-podologie ;
- concevoir et conduire une démarche de conseil, d’éducation, de prévention en pédicurie-podologie et en santé publique ;
- concevoir, conduire et évaluer un projet thérapeutique en pédicurie-podologie ;
- mettre en œuvre des activités thérapeutiques dans le domaine de la pédicurie-podologie ;
- gérer une structure et ses ressources.

Le candidat est déclaré admis à recevoir le diplôme d’État dès lors qu'il a obtenu la moyenne aux épreuves et validé les stages obligatoires.

#### Coûts associés à la qualification

La formation menant à l'obtention du diplôme d’État est payante et son coût varie selon les établissements. Pour plus d'informations, il est conseillé de se rapprocher de ces établissements.

### b. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice temporaire et occasionnel (Libre Prestation de Services)

Le ressortissant d'un État membre de l’Union européenne (UE) ou partie à l'accord sur l’Espace économique européen (EEE), légalement établi dans un de ces États, peut exercer la même activité en France de manière temporaire et occasionnelle.

Pour cela, il devra en faire la déclaration écrite, préalablement à la première prestation, auprès du préfet de département du lieu d’exécution de la prestation (cf. infra « 5°. a. Effectuer une déclaration préalable d’activité pour les ressortissants européens en vue d’un exercice temporaire et occasionnel (LPS) »).

Si ni l’accès, ni la formation y conduisant ne sont réglementés dans l’État membre d’origine ou l’État du lieu d’établissement, le ressortissant doit justifier y avoir exercé cette activité pendant au moins un an au cours des dix dernières années précédant la prestation.

Dans tous les cas, le ressortissant européen désireux d’exercer en France de manière temporaire ou occasionnelle doit posséder les connaissances linguistiques nécessaires à l’exercice de l’activité et maîtriser les systèmes de poids et mesures utilisés en France.

*Pour aller plus loin* : article L. 4322-15 du Code de la santé publique.

### c. Ressortissant de l'UE ou de l'EEE : en vue d'un exercice permanent (Libre Établissement)

Le ressortissant d'un État de l'UE ou de l'EEE peut exercer l'activité de pédicure-podologue à titre permanent en France, dès lors qu'il a suivi avec succès un cycle d’études postsecondaires et qu'il est titulaire soit :

- d’un titre de formation délivré par un État de l’UE ou de l’EEE dans lequel l’accès à la profession ou son exercice est réglementé et qui permet d’y exercer légalement ;
- d’un titre de formation délivré par un État de l’UE ou de l’EEE dans lequel ni l’accès à la profession ni son exercice n’est réglementé, et justifier avoir exercé, dans cet État, cette activité pendant au moins l’équivalent de deux ans à temps complet au cours des dix dernières années ;
- d’un titre de formation délivré par un État tiers reconnu dans un État de l’UE ou de l’EEE, autre que la France, permettant d’y exercer légalement la profession.

Dès lors qu'il remplit l'une de ces conditions, le ressortissant pourra demander une autorisation individuelle d'exercice auprès du préfet de région du lieu dans lequel il souhaite exercer sa profession (cf. infra « 5°. b. Demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE) »).

Lorsque l’examen des qualifications professionnelles fait apparaître des différences substantielles au regard des qualifications requises pour l’accès à la profession et son exercice en France, l’intéressé pourra être soumis à une mesure de compensation (cf. infra « Bon à savoir : mesures de compensation »).

*Pour aller plus loin* : article L. 4322-4 du Code de la santé publique.

## 3°. Conditions d'honorabilité, règles déontologiques, éthique

Les dispositions du Code de déontologie de la profession s'imposent à tous les pédicures-podologues exerçant en France.

**À savoir**

L'ensemble des dispositions du Code de déontologie est codifié aux articles R. 4322-31 à R. 4322-99 du Code de la santé publique.

À ce titre, le professionnel doit :

- respecter le secret professionnel ;
- exercer sa profession en toute probité ;
- prodiguer des soins éclairés et conformes à ce que prévoit la profession ;
- perfectionner ses connaissances au cours de sa carrière.

*Pour aller plus loin* : article L. 4322-14 du Code de la santé publique.

## 4°. Assurance

En qualité de professionnel de santé, le pédicure-podologue exerçant à titre libéral doit souscrire une assurance de responsabilité civile professionnelle.

En revanche, s’il exerce en tant que salarié, cette assurance n’est que facultative. En effet, dans cette hypothèse, c’est à l’employeur de souscrire pour ses salariés une telle assurance pour les actes effectués à l’occasion de leur activité professionnelle.

## 5°. Démarches et formalités de reconnaissance de qualification

### a. Effectuer une déclaration préalable d’activité pour les ressortissants européens en vue d’un exercice temporaire et occasionnel (LPS)

#### Autorité compétente

La déclaration préalable d’activité doit être adressée au Conseil national de l’ordre des pédicures-podologues, avant la première prestation de services.

#### Renouvellement de la déclaration préalable

La déclaration préalable doit être renouvelée une fois par an si le prestataire souhaite effectuer une nouvelle prestation en France.

#### Pièces justificatives

La demande se fait par le dépôt d'un dossier comprenant l'ensemble des pièces justificatives suivantes :

- une copie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation permettant l'exercice de la profession dans l’État d'obtention ;
- une attestation, datant de moins de trois mois, de l'autorité compétente de l’État de l'UE ou de l'EEE, certifiant que l'intéressé est légalement établi dans cet État et qu'il n'encourt, lorsque l'attestation est délivrée, aucune interdiction, même temporaire, d'exercer ;
- toutes pièces justifiant que le ressortissant a exercé la profession dans un État de l'UE ou de l'EEE, pendant un an au cours des dix dernières années, lorsque cet État ne réglemente ni la formation, ni l'accès à la profession demandée ou son exercice ;
- lorsque le titre de formation a été délivré par un État tiers et reconnu dans un État de l'UE ou de l'EEE autre que la France :
  - la reconnaissance du titre de formation établie par les autorités de l’État ayant reconnu ce titre,
  - toutes pièces justifiant que le ressortissant a exercé la profession dans cet État pendant trois ans ;
- le cas échéant, une copie de la déclaration précédente ainsi que de la première déclaration effectuée ;
- une attestation de responsabilité civile professionnelle.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Délai de réponse

À réception du dossier, le préfet de la région disposera d'un délai d'un mois pour se prononcer sur la demande et informera le ressortissant ;

- qu'il peut débuter la prestation ;
- qu'il sera soumis à une mesure de compensation dès lors qu'il existe des différences substantielles entre la formation ou l'expérience professionnelle du ressortissant et celles exigées en France ;
- qu'il ne pourra pas débuter la prestation ;
- de toute difficulté pouvant retarder sa décision. Dans ce dernier cas, le préfet pourra rendre sa décision dans les deux mois suivant la résolution de cette difficulté, et au plus tard dans les trois mois de sa notification au ressortissant.

Le silence gardé du préfet dans ces délais vaudra acceptation de la demande de déclaration.

#### Voies de recours

Les décisions du Conseil national de l’Ordre en matière de libre prestation de services peuvent être contestées dans un délai de deux mois à compter de leur notification par un recours formé près le tribunal administratif territorialement compétent. Le délai de contestation est porté à quatre mois si le demandeur ne demeure pas en France.

#### Coût

Gratuit.

*Pour aller plus loin* : article R. 4322-17 du Code de la santé publique ; arrêté du 8 décembre 2017 relatif à la déclaration préalable de prestation de services pour les conseillers en génétique, les physiciens médicaux et les préparateurs en pharmacie et en pharmacie hospitalière, ainsi que pour les professions figurant au livre III de la partie IV du Code de la santé publique.

### b. Demander une autorisation d'exercice pour le ressortissant de l'UE ou de l'EEE en vue d'une activité permanente (LE)

#### Autorité compétente

L'autorisation d'exercice est délivrée par le préfet de région, après avis de la commission des pédicures-podologues.

#### Pièces justificatives

La demande d'autorisation s'effectue par le dépôt d'un dossier comprenant l'ensemble des documents suivants :

- le formulaire de demande d'autorisation d'exercice ;
- une copie d'une pièce d'identité en cours de validité ;
- une copie du titre de formation ;
- le cas échéant, une copie des diplômes complémentaires ;
- toute pièce justifiant des formations continues, des expériences et des compétences acquises dans l’État de l'UE ou de l'EEE ;
- une déclaration de l'autorité compétente de l'État de l'UE ou de l'EEE justifiant l'absence de sanction à l'encontre du ressortissant ;
- une copie des attestations des autorités spécifiant le niveau de formation, le détail et le volume horaire des enseignements suivis ainsi que le contenu et la durée des stages validés ;
- tout document justifiant que le ressortissant a exercé l'activité de pédicure-podologue pendant un an au cours des dix dernières années, dans un État de l'UE ou de l'EEE, lorsque ni l'accès, ni l'exercice ne sont réglementés dans cet État.

**À savoir**

Le cas échéant, les pièces doivent être traduites en français par un traducteur agréé.

#### Procédure

Le préfet accuse réception du dossier dans le délai d'un mois et se prononcera après avoir eu l'avis de la commission. Cette dernière est chargée d'examiner les connaissances et les compétences du ressortissant acquises lors de sa formation ou au cours de son expérience professionnelle. Elle pourra soumettre le ressortissant à une mesure de compensation.

Le silence gardé du préfet de région dans un délai de quatre mois vaut décision de rejet de la demande d'autorisation.

#### Bon à savoir : mesures de compensation

Si l’examen des qualifications professionnelles, attestées par les titres de formation et l’expérience professionnelle, fait apparaître des différences substantielles avec les qualifications requises pour l’accès à la profession de pédicure-podologue et son exercice en France, l’intéressé devra se soumettre à une mesure de compensation.

Selon le niveau de qualification exigé en France et celui détenu par l’intéressé, l’autorité compétente pourra soit :

- proposer au demandeur de choisir entre un stage d’adaptation et une épreuve d’aptitude ;
- imposer un stage d’adaptation et/ou une épreuve d’aptitude.

*Pour aller plus loin* : articles R. 4322-14 à R. 4322-16 du Code de la santé publique ; arrêté du 20 janvier 2010 fixant la composition du dossier à fournir aux commissions d'autorisation d'exercice compétentes pour l'examen des demandes présentées en vue de l'exercice en France des professions de conseiller en génétique, infirmier, masseur-kinésithérapeute, pédicure-podologue, ergothérapeute, manipulateur en électroradiologie médicale et diététicien.

### c. Demander son enregistrement au RPPS

Le ressortissant qui souhaite exercer la profession de pédicure-podologue en France est tenu de faire enregistrer son titre auprès du répertoire partagé des professionnels de santé (RPPS) (répertoire qui va progressivement remplacer le numéro ADELI).

L'Ordre national des pédicures-podologues se charge d'enregistrer le professionnel sur le RPPS au moment de son inscription à l'Ordre et de lui remettre son numéro RPPS.

#### Coût

Gratuit.

*Pour aller plus loin* : arrêté du 18 avril 2017 modifiant l'arrêté du 6 février 2009 modifié portant création d'un traitement de données à caractère personnel dénommé « Répertoire partagé des professionnels intervenant dans le système de santé » (RPPS).

### d. Voies de recours

#### Centre d’assistance français

Le Centre ENIC-NARIC est le centre français d’information sur la reconnaissance académique et professionnelle des diplômes.

#### SOLVIT

SOLVIT est un service fourni par l’Administration nationale de chaque État membre de l’UE ou partie à l’accord sur l’EEE. Son objectif est de trouver une solution à un différend opposant un ressortissant de l’UE à l’Administration d’un autre de ces États. SOLVIT intervient notamment en matière de reconnaissance des qualifications professionnelles.

##### Conditions

L’intéressé ne peut recourir à SOLVIT que s’il établit :

- que l’Administration publique d’un État de l’UE n’a pas respecté les droits que la législation européenne lui confère en tant que citoyen ou entreprise d’un, autre État de l’UE ;
- qu’il n’a pas déjà initié d’action judiciaire (le recours administratif n’est pas considéré comme tel).

##### Procédure

Le ressortissant doit remplir un [formulaire de plainte en ligne](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Une fois son dossier transmis, SOLVIT le contacte dans un délai d’une semaine pour demander, si besoin, des informations supplémentaires et pour vérifier que le problème relève bien de sa compétence.

##### Pièces justificatives

Pour saisir SOLVIT, le ressortissant doit communiquer :

- ses coordonnées complètes ;
- la description détaillée de son problème ;
- l’ensemble des éléments de preuve du dossier (par exemple, la correspondance et les décisions reçues de l’autorité administrative concernée).

##### Délai

SOLVIT s’engage à trouver une solution dans un délai de dix semaines à compter du jour de la prise en charge du dossier par le centre SOLVIT du pays dans lequel est survenu le problème.

##### Coût

Gratuit.

##### Issue de la procédure

À l’issue du délai de dix semaines, le SOLVIT présente une solution :

- si cette solution règle le différend portant sur l’application du droit européen, la solution est acceptée et le dossier est clos ;
- s’il n’y a pas de solution, le dossier est clos comme non résolu et renvoyé vers la Commission européenne.

##### Informations supplémentaires

SOLVIT en France : Secrétariat général des affaires européennes, 68 rue de Bellechasse, 75700, Paris ([site officiel](https://sgae.gouv.fr/sites/SGAE/accueil.html))