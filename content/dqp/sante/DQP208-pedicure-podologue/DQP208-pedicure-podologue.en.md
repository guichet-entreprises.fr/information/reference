﻿<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- include-file(generated.txt)
+-----------------------------------------------------------------------------+
| 
| +-----------------------------------------------------------------------------+
| |                                                                             |
| |         Code generated. Automatically generated file; DO NOT EDIT           |
| |                                                                             |
| +-----------------------------------------------------------------------------+
| 
+-------------------------------------------------------------------------- -->
<!-- var(key)="DQP208" -->
<!-- var(author)="Guichet Entreprises" -->
<!-- var(lang)="en" -->
<!-- var(category)="Directive Qualification Professionnelle" -->
<!-- var(domain)="Healthcare" -->
<!-- var(title)="Podiatrist" -->
<!-- var(url-domain)="www.guichet-qualifications.fr" -->
<!-- var(url-domain-short)="gq" -->
<!-- var(category-short)="dqp" -->
<!-- var(domain-short)="healthcare" -->
<!-- var(title-short)="podiatrist" -->
<!-- var(url)="https://www.guichet-qualifications.fr/en/dqp/healthcare/podiatrist.html" -->
<!-- var(last-update)="2020-04-15 17:21:56" -->
<!-- var(url-name)="podiatrist" -->
<!-- var(translation)="Auto" -->


Podiatrist
===================

Latest update: <!-- begin-var(last-update) -->2020-04-15 17:21:56<!-- end-var -->



<!-- begin-include(disclaimer-trans-en) -->

**Notice regarding the quality of machine translation**<!-- alert-start:warning --><!-- fa:exclamation-triangle fa-2x-->

This page was translated using a machine translation tool and may contain errors. Users are advised to check the accuracy of the information provided on this page prior to starting any procedure.

The Guichet Entreprises service cannot be held accountable should the information provided be inaccurate due to a translation mistake.<!-- alert-end:warning -->

<!-- end-ref -->
1°. Defining the activity
------------------------

The pedicure-podologist is a health professional specializing in the treatment of epidermal and ungueal (fingernail) conditions of the foot.

He is therefore competent to practice hygiene care (pedicuria activity) but also to make and adapt orthopaedic insoles to relieve these ailments (podiatry activity).

*For further information*: Articles L. 4322-1 and R. 4322-1 of the Public Health Code.

2°. Professional qualifications
----------------------------------------

### a. National requirements

#### National legislation

The practice of pedicure-podologist is reserved for holders of the state diploma of pedicure-podologist.

*For further information*: Article L. 4322-2 of the Public Health Code.

#### Training

The state pedicure-podologist diploma is a bachelor's degree that is prepared in three years after graduation.

**Please note**

Admission to some of the training institutions can be done by external competition.

A theoretical and practical training of 2,028 hours and a clinical training of 1,170 hours should enable the student to develop his knowledge and skills to include:

- analyze and assess a situation and develop a diagnosis in the field of pedicuria-podology;
- Design and conduct advice, education, prevention in pedicuria-podology and public health;
- Designing, conducting and evaluating a therapeutic project in pedicuria-podology;
- implement therapeutic activities in the field of pedicuria-podology;
- manage a structure and its resources.

The candidate is declared eligible to receive the state diploma once he has obtained the average for the tests and validated the compulsory internships.

#### Costs associated with qualification

Training leading to a state diploma pays off and the cost varies from institution to institution. For more information, it is advisable to get closer to these establishments.

### b. EU or EEA nationals: for temporary and occasional exercise (Freedom to provide services)

A national of a Member State of the European Union (EU) or party to the European Economic Area (EEA) agreement, legally established in one of these states, may carry out the same activity in France on a temporary and occasional basis.

In order to do so, he will have to make a written declaration, prior to the first performance, to the prefect of the department of the place of execution of the delivery (see infra "5°. a. Make a prior declaration of activity for EU nationals for a temporary and casual exercise (LPS)).

If neither access nor training is regulated in the Member State of origin or the state of the place of establishment, the national must justify having carried out this activity there for at least one year in the last ten years preceding the Benefit.

In all cases, the European national wishing to practise in France on a temporary or occasional basis must possess the necessary language skills to carry out the activity and master the weight and measurement systems used in France. France.

*For further information*: Article L. 4322-15 of the Public Health Code.

### c. EU or EEA nationals: for a permanent exercise (Freedom of establishment)

A national of an EU or EEA state may carry out pedicure-podologist activity on a permanent basis in France, as long as he has successfully completed a post-secondary education cycle and holds either:

- a training certificate issued by an EU or EEA state in which access to the profession or its practice is regulated and which allows legal practice to be practised there;
- a training certificate issued by an EU or EEA state in which neither access to the profession nor its exercise is regulated, and justify having carried out this activity in that state for at least the equivalent of two full-time years in the ten In recent years
- a training certificate issued by a recognised third state in an EU or EEA state, other than France, allowing the profession to be legally practised there.

Once the national fulfils one of these conditions, he or she will be able to apply for an individual authorisation to practise from the regional prefect of the place in which he wishes to practice his profession (see below "5o). b. Request an exercise permit for the EU or EEA national for permanent activity (LE) ").

When the examination of professional qualifications reveals substantial differences in the qualifications required for access to the profession and its exercise in France, the person concerned may be subject to a compensation measure (cf. infra "Good to know: compensation measures").

*For further information*: Article L. 4322-4 of the Public Health Code.

3°. Conditions of honorability, ethical rules, ethics
----------------------------------------------------------------

The provisions of the Code of Ethics of the profession are imposed on all pedicures-podologists practising in France.

**What to know**

All provisions of the Code of Ethics are codified in sections R. 4322-31 to R. 4322-99 of the Public Health Code.

As such, the professional must:

- Respect professional secrecy
- practising your profession with probity;
- Provide informed care that is consistent with the profession
- to improve his knowledge during his career.

*For further information*: Article L. 4322-14 of the Public Health Code.

4°. Insurance
---------------------------------

As a health professional, a liberal pedicure-podologist must take out professional liability insurance.

On the other hand, if he practises as an employee, this insurance is only optional. In this case, it is up to the employer to take out such insurance for its employees for the acts carried out during their professional activity.

5°. Qualification recognition procedures and formalities
------------------------------------------------------------------

### a. Make a pre-declaration of activity for EU nationals for a temporary and casual exercise (LPS)

**Competent authority**

The prior declaration of activity must be addressed to the National Council of the Order of Pedicures-Podologists, prior to the first provision of services.

**Renewal of pre-declaration**

The advance declaration must be renewed once a year if the claimant wishes to make a new benefit in France.

**Supporting documents**

The application is made by filing a file containing all the following supporting documents:

- A copy of a valid ID
- A copy of the training title allowing the profession to be practised in the state of obtainment;
- a certificate, less than three months old, from the competent authority of the EU State or the EEA, certifying that the person concerned is legally established in that state and that, when the certificate is issued, there is no prohibition, even temporary, Exercise
- any evidence justifying that the national has practised the profession in an EU or EEA state for one year in the last ten years, when that state does not regulate training, access to the requested profession or its exercise;
- where the training certificate has been issued by a third state and recognised in an EU or EEA state other than France:- recognition of the training title established by the state authorities that have recognised this title,
  - any evidence justifying that the national has practiced the profession in that state for three years;
- If so, a copy of the previous statement as well as the first statement made;
- a certificate of professional civil liability.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Response time**

Upon receipt of the file, the prefect of the region will have one month to decide on the application and will inform the national;

- He can start the performance.
- that he will be subject to a compensation measure if there are substantial differences between the training or professional experience of the national and those required in France;
- he will not be able to start the performance;
- any difficulty that could delay its decision. In the latter case, the prefect will be able to make his decision within two months of the resolution of this difficulty, and no later than three months of notification to the national.

The prefect's silence within these deadlines will be worth accepting the request for declaration.

**Remedies**

The decisions of the National Council of the Order regarding the free provision of services can be challenged within two months of their notification by an appeal lodged with the territorially competent administrative court. The time to challenge is increased to four months if the applicant does not remain in France.

**Cost**

Free.

*For further information*: Article R. 4322-17 of the Public Health Code; December 8, 2017 order on the prior declaration of service provision for genetic counsellors, medical physicists and pharmacy and hospital pharmacy preparers, as well as for occupations in Book III of Part IV of the Public Health Code.

### b. Requesting an exercise permit for the EU or EEA national for permanent activity (LE)

**Competent authority**

The authorisation to exercise is issued by the regional prefect, after advice from the committee of pedicures-podologists.

**Supporting documents**

The application for authorization is made by filing a file containing all of the following documents:

- The application form for authorisation to practice
- A copy of a valid ID
- A copy of the training title
- If necessary, a copy of the additional diplomas;
- any evidence justifying ongoing training, experience and skills acquired in the EU or EEA State;
- a statement from the competent authority of the EU State or the EEA justifying the absence of sanctions against the national;
- A copy of the authorities' certificates specifying the level of training, the detail and the hourly volume of the courses followed, as well as the content and duration of the validated internships;
- any document justifying that the national has been a pedicure-podologist for one year in the last ten years, in an EU or EEA state, where neither access nor exercise is regulated in that state.

**What to know**

If necessary, the pieces must be translated into French by a certified translator.

**Procedure**

The prefect acknowledges receipt of the file within one month and will decide after having the opinion of the commission. The latter is responsible for examining the knowledge and skills of the national acquired during his training or during his professional experience. It may subject the national to a compensation measure.

The silence kept by the prefect of the region within four months is a decision to reject the application for authorisation.

**Good to know: compensation measures**

If the examination of professional qualifications, attested by training credentials and work experience, shows substantial differences with the qualifications required for access to the profession of pedicure-podologist and in France, the person concerned will have to submit to a compensation measure.

Depending on the level of qualification required in France and that held by the person concerned, the competent authority may either:

- Offer the applicant a choice between an accommodation course and an aptitude test;
- require an adjustment course and/or aptitude test.

*For further information*: Articles R. 4322-14 to R. 4322-16 of the Public Health Code; decree of 20 January 2010 setting out the composition of the file to be provided to the relevant authorisation commissions for the examination of applications submitted for the exercise in France of the professions of genetic counsellor, nurse, masseur-kinesitherapist, pedicure-podologist, occupational therapist, manipulator in medical electroradiology and dietician.

### c. Request registration from the RPPS

A national who wishes to practise as a pedicure-podologist in France is required to register his title with the shared directory of health professionals (RPPS) (a directory that will gradually replace the ADELI number).

The National College of Pedicures-podiatrists is responsible for registering the professional on the RPPS at the time of registration and giving him his RPPS number.

**Cost**

Free.

*For further information*: Decree of 18 April 2017 amending the amended 6 February 2009 decree creating a personal data processing processing called "Shared Directory of Professionals in the Health System" (RPPS).

### d. Remedies

#### French assistance centre

The ENIC-NARIC Centre is the French centre for information on academic and professional recognition of diplomas.

#### Solvit

SOLVIT is a service provided by the National Administration of each EU member state or party to the EEA agreement. Its aim is to find a solution to a dispute between an EU national and the administration of another of these states. SOLVIT intervenes in particular in the recognition of professional qualifications.

**Conditions**

The person concerned can only use SOLVIT if he establishes:

- that the Public Administration of an EU State has not respected the rights conferred by EU law as a citizen or business of another EU state;
- that it has not already initiated legal action (administrative action is not considered as such).

**Procedure**

The national must complete a[online complaint form](http://ec.europa.eu/eu-rights/enquiry-complaint-form/home?languageCode=fr&origin=solvit-web).

Once his file has been submitted, SOLVIT contacts him within a week to request, if necessary, additional information and to verify that the problem is within his competence.

**Supporting documents**

To enter SOLVIT, the national must communicate:

- Full contact details
- Detailed description of his problem
- all the evidence in the file (for example, correspondence and decisions received from the relevant administrative authority).

**Timeframe**

SOLVIT is committed to finding a solution within ten weeks of the day the case was taken over by the SOLVIT centre in the country in which the problem occurred.

**Cost**

Free.

**Outcome of the procedure**

At the end of the 10-week period, SOLVIT presents a solution:

- If this solution resolves the dispute over the application of European law, the solution is accepted and the case is closed;
- if there is no solution, the case is closed as unresolved and referred to the European Commission.

**More information**

SOLVIT in France: General Secretariat for European Affairs, 68 rue de Bellechasse, 75700, Paris ([official website](https://sgae.gouv.fr/sites/SGAE/accueil.html))

